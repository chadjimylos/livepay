<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Relateddocs_List.aspx.cs"
    Inherits="CMSModules_Content_CMSDesk_Properties_Relateddocs_List" Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Relationship - list</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>
</head>
<body class="VerticalTabsBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel ID="pnlBody" runat="server" CssClass="VerticalTabsPageBody">
        <asp:Panel runat="server" ID="pnlTab" CssClass="ContentEditMenu">
            <table width="100%">
                <tr>
                    <td>
                        <div style="height: 24px; padding: 5px;">
                        </div>
                    </td>
                    <td class="TextRight">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlNewItem" runat="server">
            <asp:Panel ID="pnlNewLink" runat="server" Style="padding: 10px;">
                <cms:CMSImage ID="imgNewRelationship" runat="server" ImageAlign="AbsMiddle" CssClass="NewItemImage"
                    EnableViewState="false" />
                <cms:LocalizedHyperlink ID="lnkNewRelationship" runat="server" CssClass="NewItemLink"
                    EnableViewState="false" ResourceString="Relationship.AddRelatedDocument" />
            </asp:Panel>
            <asp:Panel ID="pnlNewLine" runat="server" Style="margin-bottom: 15px; border-bottom: solid 1px #cccccc;" />
        </asp:Panel>
        <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
            <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />
            <cms:UniGrid ID="UniGridRelationship" runat="server" GridName="Relateddocs_List.xml"
                OrderBy="RelationshipDisplayName" IsLiveSite="false" />
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
