CREATE TABLE [CMS_State] (
		[StateID]               int NOT NULL IDENTITY(1, 1),
		[StateDisplayName]      nvarchar(200) NOT NULL,
		[StateName]             nvarchar(200) NOT NULL,
		[StateCode]             nvarchar(100) NULL,
		[CountryID]             int NOT NULL,
		[StateGUID]             uniqueidentifier NOT NULL,
		[StateLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_State]
	ADD
	CONSTRAINT [PK_CMS_State]
	PRIMARY KEY
	NONCLUSTERED
	([StateID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_State]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Country]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_State_CountryID_CMS_Country]') IS NULL
BEGIN
		ALTER TABLE [CMS_State]
			ADD CONSTRAINT [FK_CMS_State_CountryID_CMS_Country]
			FOREIGN KEY ([CountryID]) REFERENCES [CMS_Country] ([CountryID])
END
CREATE CLUSTERED INDEX [IX_CMS_State_CountryID_StateDisplayName]
	ON [CMS_State] ([CountryID], [StateDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
