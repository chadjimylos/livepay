CREATE PROCEDURE [Proc_CMS_ScheduledTask_FetchTasksToRun]
	@TaskSiteID int,
	@DateTime datetime,
	@TaskServerName nvarchar(100)
AS
BEGIN
	DECLARE @TaskIDs TABLE (TaskID int);
	BEGIN TRAN
	INSERT INTO @TaskIDs SELECT TaskID FROM CMS_ScheduledTask WHERE TaskNextRunTime IS NOT NULL AND TaskNextRunTime <= @DateTime AND TaskEnabled = 1 AND (TaskSiteID = @TaskSiteID OR TaskSiteID IS NULL) AND (TaskServerName IS NULL OR TaskServerName = '' OR TaskServerName = @TaskServerName);	
	
	UPDATE CMS_ScheduledTask SET TaskNextRunTime = NULL WHERE TaskID IN (SELECT TaskID FROM @TaskIDs);
	COMMIT TRAN
	SELECT * FROM @TaskIDs;
END
