using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.MessageBoard;
using CMS.UIControls;
using CMS.Community;

public partial class CMSModules_Groups_Tools_MessageBoards_Boards_Board_Edit_Subscriptions : CMSGroupPage
{    
    // Current board ID
    private int mBoardId = 0;
    private int mGroupId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get current board ID
        mBoardId = QueryHelper.GetInteger("boardid", 0);
        
        mGroupId = QueryHelper.GetInteger("groupid", 0);

        this.boardSubscriptions.BoardID = mBoardId;
        this.boardSubscriptions.GroupID = mGroupId;
        this.boardSubscriptions.OnCheckPermissions += new CMSAdminControl.CheckPermissionsEventHandler(boardSubscriptions_OnCheckPermissions);
        this.boardSubscriptions.OnAction += new CommandEventHandler(boardSubscriptions_OnAction);

        // Initialize the master page
        InitializeMasterPage();
    }


    void boardSubscriptions_OnAction(object sender, CommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "edit")
        {
            // Redirect to edit page with subscription ID specified
            UrlHelper.Redirect("Board_Edit_Subscription_Edit.aspx?subscriptionid=" + e.CommandArgument.ToString() + "&boardid=" + mBoardId.ToString() + 
                ((this.mGroupId > 0) ? "&groupid=" + this.mGroupId : ""));
        }  
    }


    void boardSubscriptions_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        int groupId = 0;
        BoardInfo bi = BoardInfoProvider.GetBoardInfo(mBoardId);
        if (bi != null)
        {
            groupId = bi.BoardGroupID;
        }

        CheckPermissions(groupId, CMSAdminControl.PERMISSION_MANAGE);
    }


    #region "Private methods"

    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    private void InitializeMasterPage()
    {
        // Setup master page action element
        string[,] actions = new string[1,6];
        actions[0,0] = HeaderActions.TYPE_HYPERLINK;
        actions[0,1] = ResHelper.GetString("board.subscriptions.newitem");
        actions[0,3] = "~/CMSModules/Groups/Tools/MessageBoards/Boards/Board_Edit_Subscription_Edit.aspx?boardid=" + mBoardId.ToString() + "&groupid=" + mGroupId;
        actions[0, 5] = GetImageUrl("CMSModules/CMS_MessageBoards/newsubscription.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }

    #endregion
}

