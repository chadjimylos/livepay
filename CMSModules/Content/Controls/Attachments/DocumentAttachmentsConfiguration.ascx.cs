using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;

public partial class CMSModules_Content_Controls_Attachments_DocumentAttachmentsConfiguration : CMSUserControl, ICallbackEventHandler, IFieldEditorControl
{
    #region "Public properties"
    
    /// <summary>
    /// Indicates if advanced settings should be shown
    /// </summary>
    public bool AdvancedMode 
    { 
        get
        {
            return plcAdvanced.Visible;
        }
        set
        {
            plcAdvanced.Visible = value;
        }
    }
    
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Registred scripts
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "DocAttachments_EnableDisableForm", GetScriptEnableDisableForm());
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "DocAttachments_ReceiveExtensions", GetScriptReceiveExtensions());
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "DocAttachments_LoadSiteSettings", ScriptHelper.GetScript("function GetExtensions(txtAllowedExtensions){ return " + this.Page.ClientScript.GetCallbackEventReference(this, "txtAllowedExtensions", "ReceiveExtensions", null) + " } \n"));

        // Initialize form
        chkInehrit.Attributes.Add("onclick", GetEnableDisableFormDefinition());
        chkAllowPaging.Attributes.Add("onclick", "document.getElementById('" + txtPageSize.ClientID + "').disabled=!document.getElementById('" + chkAllowPaging.ClientID + "').checked;");
        EnableDisableForm();

        //if (!RequestHelper.IsPostBack())
        //{
        //    ClearControl();
        //}
    }


    #region "Private methods"

    private string GetScriptReceiveExtensions()
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("function ReceiveExtensions(rValue, context){");
        builder.Append("var extensions = rValue.split(\"|\");");
        builder.Append("var txtExtensions = document.getElementById(extensions[1]);\n");
        builder.Append("if (txtExtensions != null)\n");
        builder.Append("{ txtExtensions.value = extensions[0]; }}\n");

        return ScriptHelper.GetScript(builder.ToString());
    }


    private string GetScriptEnableDisableForm()
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("function EnableDisableDocForm(chkAllowedExtensions, txtAllowedExtensions){");
        builder.Append("var txtAllowedExt = document.getElementById(txtAllowedExtensions);\n");
        builder.Append("var chkAllowedExt = document.getElementById(chkAllowedExtensions);\n");
        builder.Append("if (txtAllowedExt != null) {\n");
        builder.Append("var disabled = chkAllowedExt.checked;\n");
        builder.Append("txtAllowedExt.disabled = disabled;\n");
        builder.Append("if (disabled){ GetExtensions(txtAllowedExtensions); }\n");
        builder.Append("}}\n");

        return ScriptHelper.GetScript(builder.ToString());
    }


    private string GetEnableDisableFormDefinition()
    {
        return string.Format("EnableDisableDocForm('{0}', '{1}');", this.chkInehrit.ClientID, this.txtAllowedExtensions.ClientID);
    }


    private void EnableDisableForm()
    {
        ScriptHelper.RegisterStartupScript(this, typeof(string), "DocAttachments_EnableDisableFormStartup", ScriptHelper.GetScript(GetEnableDisableFormDefinition()));
    }


    private object GetKeyValue(Hashtable config, string key)
    {
        if (config.ContainsKey(key))
        {
            return config[key];
        }
        return null;
    }

    #endregion


    #region "Public methods"

    /// <summary>
    /// Sets inner controls according to the parameters and their values included in configuration collection. Parameters collection will be passed from Field editor
    /// </summary>
    /// <param name="config">Parameters collection.</param>
    public void LoadConfiguration(Hashtable config)
    {
        // Load auto resize control configuration
        ucAutoResize.LoadConfiguration(config);

        // Load allow change order
        object value = GetKeyValue(config, "changeorder");
        bool boolValue = ValidationHelper.GetBoolean(value, true);
        chkAllowChangeOrder.Checked = boolValue;

        // Load allow paging
        value = GetKeyValue(config, "paging");
        boolValue = ValidationHelper.GetBoolean(value, true);
        chkAllowPaging.Checked = boolValue;

        // Load page size
        value = GetKeyValue(config, "pagingsize");
        txtPageSize.Text = ValidationHelper.GetString(value, "");

        value = GetKeyValue(config, "extensions");
        if (ValidationHelper.GetString(value, "").ToLower() == "custom")
        {
            // Use custom settings
            chkInehrit.Checked = false;

            // Load allowed extensions
            value = GetKeyValue(config, "allowed_extensions");
            string allowedExt = ValidationHelper.GetString(value, "");
            txtAllowedExtensions.Text = allowedExt; 
        }
        else
        {
            // Use site settings
            chkInehrit.Checked = true;

            string siteName = CMSContext.CurrentSiteName;
            txtAllowedExtensions.Text = SettingsKeyProvider.GetStringValue(siteName + ".CMSUploadExtensions");
        }

        EnableDisableForm();
    }


    /// <summary>
    /// Updates parameters collection of parameters and values according to the values of the inner controls. Parameters collection which should be updated will be passed from Field editor.
    /// </summary>
    /// <param name="config">Parameters collection.</param>
    public void UpdateConfiguration(Hashtable config)
    {
        // Update autresize configuration
        ucAutoResize.UpdateConfiguration(config);

        config.Remove("extensions");
        config.Remove("changeorder");
        config.Remove("paging");
        config.Remove("pagingsize");
        config.Remove("allowed_extensions");

        config["changeorder"] = chkAllowChangeOrder.Checked.ToString();
        config["paging"] = chkAllowPaging.Checked.ToString();

        if (txtPageSize.Text.Trim() != "")
        {
            config["pagingsize"] = txtPageSize.Text.Trim();
        }

        if (!chkInehrit.Checked)
        {
            config["extensions"] = "custom";

            if (txtAllowedExtensions.Text.Trim() != "")
            {
                config["allowed_extensions"] = txtAllowedExtensions.Text.Trim();
            }
        }

    }


    /// <summary>
    /// Clears inner controls - sets them to the default values.
    /// </summary>
    public void ClearControl()
    {
        LoadConfiguration(new Hashtable());      
    }


    /// <summary>
    /// Returns TRUE if input values are valid, otherwise returns FALSE and dislays error message.
    /// </summary>    
    public string Validate()
    {
        // Validate page size
        if (txtPageSize.Text.Trim() != "")
        {
            if (ValidationHelper.GetInteger(txtPageSize.Text.Trim(), 0) <= 0)
            {
                // Display error message                   
                return ResHelper.GetString("dialogs.resize.wrongformat");
            }
        }
        return "";
    }

    #endregion


    #region "Callback handling"

    string extensions = "";

    public string GetCallbackResult()
    {
        return extensions;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        // Get site settings
        string siteName = CMSContext.CurrentSiteName;
        extensions = SettingsKeyProvider.GetStringValue(siteName + ".CMSUploadExtensions");

        // Returns site settings back to the client
        extensions = string.Format("{0}|{1}", extensions, eventArgument); 
    }

    #endregion
}
