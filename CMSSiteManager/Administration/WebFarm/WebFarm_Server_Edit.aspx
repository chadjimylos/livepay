<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebFarm_Server_Edit.aspx.cs"
    Inherits="CMSSiteManager_Administration_WebFarm_WebFarm_Server_Edit" Title="Web farm server - Edit"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="Default" %>

<asp:Content ID="cntBody" ContentPlaceHolderID="plcContent" runat="server">
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" Visible="false" />
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" Visible="false" />
    <asp:PlaceHolder ID="plcEditForm" runat="server">
        <table>
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblDisplayName" runat="server" CssClass="ContentLabel" ResourceString="WebFarmServers_Edit.DisplayName" />
                </td>
                <td>
                    <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                    <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblCodeName" runat="server" CssClass="ContentLabel" ResourceString="WebFarmServers_Edit.CodeName" />
                </td>
                <td>
                    <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                    <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtCodeName"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblURL" runat="server" CssClass="ContentLabel" ResourceString="WebFarmServers_Edit.URL" />
                </td>
                <td>
                    <asp:TextBox ID="txtURL" runat="server" CssClass="TextBoxField" MaxLength="2000" />
                    <asp:RequiredFieldValidator ID="rfvURL" runat="server" ControlToValidate="txtURL"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <cms:LocalizedLabel ID="lblEnabled" runat="server" CssClass="ContentLabel" ResourceString="WebFarmServers_Edit.Enabled" />
                </td>
                <td>
                    <asp:CheckBox ID="chkEnabled" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click" />
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>
</asp:Content>
