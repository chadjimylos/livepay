CREATE TABLE [CMS_WidgetCategory] (
		[WidgetCategoryID]                   int NOT NULL IDENTITY(1, 1),
		[WidgetCategoryName]                 nvarchar(100) NOT NULL,
		[WidgetCategoryDisplayName]          nvarchar(100) NOT NULL,
		[WidgetCategoryParentID]             int NULL,
		[WidgetCategoryPath]                 nvarchar(450) NOT NULL,
		[WidgetCategoryLevel]                int NOT NULL,
		[WidgetCategoryOrder]                int NULL,
		[WidgetCategoryChildCount]           int NULL,
		[WidgetCategoryWidgetChildCount]     int NULL,
		[WidgetCategoryImagePath]            nvarchar(450) NULL,
		[WidgetCategoryGUID]                 uniqueidentifier NOT NULL,
		[WidgetCategoryLastModified]         datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WidgetCategory]
	ADD
	CONSTRAINT [PK_CMS_WidgetCategory]
	PRIMARY KEY
	NONCLUSTERED
	([WidgetCategoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_WidgetCategory]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WidgetCategory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WidgetCategory_WidgetCategoryParentID_CMS_WidgetCategory]') IS NULL
BEGIN
		ALTER TABLE [CMS_WidgetCategory]
			ADD CONSTRAINT [FK_CMS_WidgetCategory_WidgetCategoryParentID_CMS_WidgetCategory]
			FOREIGN KEY ([WidgetCategoryParentID]) REFERENCES [CMS_WidgetCategory] ([WidgetCategoryID])
END
CREATE UNIQUE CLUSTERED INDEX [IX_CMS_WidgetCategory_CategoryPath]
	ON [CMS_WidgetCategory] ([WidgetCategoryPath])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WidgetCategory_WidgetCategoryID_WidgetCategoryOrder]
	ON [CMS_WidgetCategory] ([WidgetCategoryParentID], [WidgetCategoryOrder])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
