﻿// Register the related command.
FCKCommands.RegisterCommand( 'InsertRating', new FCKDialogCommand( 'InsertRating', FCKLang.InsertRatingTitle, FCKPlugins.Items['InsertRating'].Path + 'fck_InsertRating.aspx?' + FCKConfig.DialogParameters, 430, 418 ) ) ;

// Create the "Placeholder" toolbar button.
var oInsertRating = new FCKToolbarButton( 'InsertRating', FCKLang.InsertRatingButton ) ;
oInsertRating.IconPath = FCKPlugins.Items['InsertRating'].Path + 'fckRating.gif' ;
FCKToolbarItems.RegisterItem( 'InsertRating', oInsertRating ) ;
