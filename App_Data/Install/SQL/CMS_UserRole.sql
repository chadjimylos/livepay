CREATE TABLE [CMS_UserRole] (
		[UserID]     int NOT NULL,
		[RoleID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_UserRole]
	ADD
	CONSTRAINT [PK_CMS_UserRole]
	PRIMARY KEY
	([UserID], [RoleID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_UserRole]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserRole_RoleID_CMS_Role]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserRole]
			ADD CONSTRAINT [FK_CMS_UserRole_RoleID_CMS_Role]
			FOREIGN KEY ([RoleID]) REFERENCES [CMS_Role] ([RoleID])
END
IF OBJECT_ID(N'[CMS_UserRole]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserRole_UserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserRole]
			ADD CONSTRAINT [FK_CMS_UserRole_UserID_CMS_User]
			FOREIGN KEY ([UserID]) REFERENCES [CMS_User] ([UserID])
END
