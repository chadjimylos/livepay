﻿<%@ WebHandler Language="VB" Class="Companies" %>

Imports System
Imports System.Web
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Script.Serialization
Imports System.Linq
Public Class Companies : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim EventID As String = My.Request("event")
        If Not IsNothing(EventID) andalso EventID.Length > 0 Then
            Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("APPConnectionString_New").ToString
            Dim objConnection As SqlConnection = New SqlConnection(strConnection)
            Dim query As String = "proc_sync_Company_Search"
            objConnection.Open()
            Dim objCommand As SqlCommand = New SqlCommand(query, objConnection)
            objCommand.CommandType = CommandType.StoredProcedure
            objCommand.Parameters.Add("@ExhibitionID", SqlDbType.SmallInt).Value = My.Request("event")
        
        
            'If ddlCountry.SelectedValue <> 0 Then
            '    objCommand.Parameters.Add("@CountryID", SqlDbType.Int).Value = ddlCountry.SelectedValue
            'End If
            'If tbCompanyName.Text <> String.Empty Then
            '    objCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar, 255).Value = tbCompanyName.Text
            'End If
            'If ddlProducts.SelectedValue <> 0 Then
            '    objCommand.Parameters.Add("@productId", SqlDbType.Int).Value = ddlProducts.SelectedValue
            'End If
            'If SearchLetter.Text <> String.Empty Then
            '    objCommand.Parameters.Add("@CatalogueLetter", SqlDbType.NVarChar, 5).Value = SearchLetter.Text
            'End If
            'If tbStands.Text <> String.Empty Then
            '    objCommand.Parameters.Add("@StandNumber", SqlDbType.NVarChar, 63).Value = tbStands.Text
            'End If
        
        
        
            Dim ds As DataSet = New DataSet()
            Dim objAdapter As SqlDataAdapter = New SqlDataAdapter(objCommand)
            objAdapter.Fill(ds)
            objCommand.ExecuteScalar()

            objConnection.Close()
       
            Dim dt As DataTable = ds.Tables(0)
            If dt.Rows.Count > 0 Then
                context.Response.Write("{""data"":" & GetJson(dt) & ",""status"":""ok"",""error"":""""}")
            Else
                context.Response.Write("{""data"":[],""status"":""ok"",""error"":""""}")
            End If
        Else
            context.Response.Write("{""data"":[],""status"":""error"",""error"":""?event= missing from querystring""}")
        End If
        
     
        
    End Sub
    Public Function GetJson(ByVal dt As DataTable) As String
        Return New JavaScriptSerializer().Serialize(From dr As DataRow In dt.Rows Select dt.Columns.Cast(Of DataColumn)().ToDictionary(Function(col) col.ColumnName, Function(col) dr(col)))
    End Function
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class