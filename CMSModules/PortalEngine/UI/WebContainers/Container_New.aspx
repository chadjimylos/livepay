<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Container_New.aspx.cs" Inherits="CMSModules_PortalEngine_UI_WebContainers_Container_New"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="Default" ValidateRequest="false"
    Title="Container properties" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td style="vertical-align: top; padding-top: 5px" class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblContainerDisplayName" ResourceString="general.displayname"
                    DisplayColon="true" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtContainerDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtContainerDisplayName" />
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; padding-top: 5px" class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblContainerName" ResourceString="general.codename"
                    DisplayColon="true" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtContainerName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtContainerName" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblContainerTextBefore" ResourceString="Container_Edit.ContainerTextBeforeLabel"
                    DisplayColon="true" EnableViewState="false" />
            </td>
            <td>
                <cms:ExtendedTextArea ID="txtContainerTextBefore" runat="server" TextMode="MultiLine"
                    CssClass="TextAreaCode" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblContainerTextAfter" ResourceString="Container_Edit.ContainerTextAfterLabel"
                    DisplayColon="true" EnableViewState="false" />
            </td>
            <td>
                <cms:ExtendedTextArea ID="txtContainerTextAfter" runat="server" TextMode="MultiLine"
                    CssClass="TextAreaCode" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:LocalizedButton runat="server" ID="btnOk" OnClick="btnOK_Click" ResourceString="general.ok"
                    EnableViewState="false" CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
