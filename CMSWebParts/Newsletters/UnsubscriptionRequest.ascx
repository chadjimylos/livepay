<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Newsletters/UnsubscriptionRequest.ascx.cs"
    Inherits="CMSWebParts_Newsletters_UnsubscriptionRequest" %>
<asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
<div>
    <asp:Label ID="lblInfo" runat="server" AssociatedControlID="txtEmail" CssClass="InfoLabel" Visible="false" EnableViewState="false" />
    <asp:TextBox ID="txtEmail" runat="server" CssClass="UnsubscriptionEmail" />
    <cms:CMSButton ID="btnSubmit" runat="server" EnableViewState="false" CssClass="ContentButton" />
</div>
