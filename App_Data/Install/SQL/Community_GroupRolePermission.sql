CREATE TABLE [Community_GroupRolePermission] (
		[GroupID]          int NOT NULL,
		[RoleID]           int NOT NULL,
		[PermissionID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Community_GroupRolePermission]
	ADD
	CONSTRAINT [PK_Community_GroupRolePermission]
	PRIMARY KEY
	([GroupID], [RoleID], [PermissionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Community_GroupRolePermission]
	ADD
	CONSTRAINT [DEFAULT_community_GroupRolePermission_PermissionID]
	DEFAULT ((0)) FOR [PermissionID]
ALTER TABLE [Community_GroupRolePermission]
	ADD
	CONSTRAINT [DEFAULT_community_GroupRolePermission_RoleID]
	DEFAULT ((0)) FOR [RoleID]
IF OBJECT_ID(N'[Community_GroupRolePermission]') IS NOT NULL
	AND OBJECT_ID(N'[Community_Group]') IS NOT NULL
	AND OBJECT_ID(N'[FK_community_GroupRolePermission_GroupID_Community_Group]') IS NULL
BEGIN
		ALTER TABLE [Community_GroupRolePermission]
			ADD CONSTRAINT [FK_community_GroupRolePermission_GroupID_Community_Group]
			FOREIGN KEY ([GroupID]) REFERENCES [Community_Group] ([GroupID])
END
IF OBJECT_ID(N'[Community_GroupRolePermission]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Permission]') IS NOT NULL
	AND OBJECT_ID(N'[FK_community_GroupRolePermission_PermissionID_CMS_Permission]') IS NULL
BEGIN
		ALTER TABLE [Community_GroupRolePermission]
			ADD CONSTRAINT [FK_community_GroupRolePermission_PermissionID_CMS_Permission]
			FOREIGN KEY ([PermissionID]) REFERENCES [CMS_Permission] ([PermissionID])
END
IF OBJECT_ID(N'[Community_GroupRolePermission]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[FK_community_GroupRolePermission_RoleID_CMS_Role]') IS NULL
BEGIN
		ALTER TABLE [Community_GroupRolePermission]
			ADD CONSTRAINT [FK_community_GroupRolePermission_RoleID_CMS_Role]
			FOREIGN KEY ([RoleID]) REFERENCES [CMS_Role] ([RoleID])
END
