<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Country_Edit_State_Edit.aspx.cs"
    Inherits="CMSSiteManager_Development_Countries_Country_Edit_State_Edit" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Country edit - State edit" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblStateDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtStateDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvStateDisplayName" runat="server" ControlToValidate="txtStateDisplayName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblStateName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtStateName" runat="server" CssClass="TextBoxField" MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvStateName" runat="server" ControlToValidate="txtStateName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblStateCode" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtStateCode" runat="server" CssClass="TextBoxField" MaxLength="100" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvStateCode" runat="server" ControlToValidate="txtStateCode"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
