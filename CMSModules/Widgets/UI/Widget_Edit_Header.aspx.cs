﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSModules_Widgets_UI_Widget_Edit_Header : SiteManagerPage
{
    private int widgetId = 0;
    private WidgetInfo widget = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check "read" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Widget", "Read"))
        {
            RedirectToAccessDenied("CMS.Widget", "Read");
        }

        widgetId = QueryHelper.GetInteger("widgetId", 0);
        
        widget = WidgetInfoProvider.GetWidgetInfo(widgetId);
        if (widget != null)
        {
            string currentWidget = widget.WidgetDisplayName;

            // Initialize PageTitle breadcrumbs
            string[,] pageTitleTabs = new string[2, 3];
            pageTitleTabs[0, 0] = ResHelper.GetString("widgets.title");
            pageTitleTabs[0, 1] = "~/CMSModules/Widgets/UI/WidgetFrameset.aspx";
            pageTitleTabs[0, 2] = "frameMain";
            pageTitleTabs[1, 0] = currentWidget;
            pageTitleTabs[1, 1] = "";
            pageTitleTabs[1, 2] = "";

            // Set masterpage        
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Widget/object.png");
            this.CurrentMaster.Title.HelpTopicName = "widget_general";
            this.CurrentMaster.Title.HelpName = "helpTopic";
            this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;

            // Tabs
            InitalizeTabs();
        }
    }


    /// <summary>
    /// Initializes tabs
    /// </summary>
    protected void InitalizeTabs()
    {
        string[,] tabs = new string[3, 4];
        tabs[0, 0] = ResHelper.GetString("general.general");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'widget_general');";
        tabs[0, 2] = "Widget_Edit_General.aspx?widgetId=" + widgetId;
        tabs[1, 0] = ResHelper.GetString("general.properties");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'widget_properties');";
        tabs[1, 2] = "Widget_Edit_Properties.aspx?widgetId=" + widgetId;
        tabs[2, 0] = ResHelper.GetString("general.security");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'widget_security');";
        tabs[2, 2] = "Widget_Edit_Security.aspx?widgetId=" + widgetId;

        CurrentMaster.Tabs.UrlTarget = "widgeteditcontent";
        CurrentMaster.Tabs.Tabs = tabs;
    }
}
