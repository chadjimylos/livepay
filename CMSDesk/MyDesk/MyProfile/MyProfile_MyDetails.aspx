<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyProfile_MyDetails.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSDesk_MyDesk_MyProfile_MyProfile_MyDetails"
    Theme="Default" %>

<%@ Register Src="~/CMSModules/Membership/Controls/MyProfile.ascx" TagName="MyProfile"
    TagPrefix="cms" %>
    
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:MyProfile ID="ucMyDetails" runat="server" Visible="true" AlternativeFormName="cms.user.EditProfileMyDesk" />
    <asp:Literal ID="ltlScript" runat="server" Visible="false" EnableViewState="false" />
</asp:Content>
