<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebPartProperties.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_WebParts_WebpartProperties" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CMS Desk - Web Part properties</title>

    <script type="text/javascript">
        //<![CDATA[
        function ChangeWebPart(zoneId, webPartId, aliasPath) {
            window.close();
            wopener.ConfigureWebPart(zoneId, webPartId, aliasPath);
        }
        //]]>
    </script>

</head>
<frameset border="0" rows="72,*" runat="server" id="rowsFrameset">
        <frame name="webpartpropertiesheader" scrolling="no" noresize="noresize" frameborder="0" runat="server" id="frameHeader" />
        <frame name="webpartpropertiescontent" frameborder="0" noresize="noresize" scrolling="no" runat="server" id="frameContent" />
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
    </frameset>
</html>
