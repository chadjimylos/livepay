using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.ExtendedControls;

public partial class CMSModules_Newsletters_Tools_EmailQueue_NewsletterEmailQueue : CMSNewsletterPage
{
    #region "Private variables"

    protected int siteId = 0;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get site ID from query string or current site
        siteId = QueryHelper.GetInteger("siteid", 0);
        if (siteId == 0)
        {
            siteId = CMSContext.CurrentSite.SiteID;
        }

        lblIntro.Text = ResHelper.GetString("NewsletterEmailQueue_List.Intro");

        InitializeActionMenu();

        // Initialize unigrid
        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);

        gridElem.HideControlForZeroRows = false;
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
        gridElem.WhereCondition = "EmailSiteID = @SiteID";

        object[,] parameters = new object[1, 3];
        parameters[0, 0] = "@SiteID";
        parameters[0, 1] = siteId;

        gridElem.QueryParameters = parameters;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        InitializeActionMenu();
        CurrentMaster.HeaderActions.ReloadData();
    }

    #endregion


    #region "Unigrid events"

    /// <summary>
    /// Handles Unigrid's OnExternalDataBound event.
    /// </summary>
    object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "subject":
                return TextHelper.LimitLength(HTMLHelper.HTMLEncode(parameter.ToString()), 50);

            case "result":
                return TextHelper.LimitLength(parameter.ToString(), 50);

            case "subjecttooltip":
            case "resulttooltip":
                return parameter.ToString().Replace("\r\n", "<br />").Replace("\n", "<br />");
        }

        return null;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "configure"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "configure");
        }

        if (actionName == "resend")
        {
            EmailQueueManager.ResendEmail(Convert.ToInt32(actionArgument));
        }
        else if (actionName == "delete")
        {
            // Delete EmailQueueItem object from database
            EmailQueueManager.DeleteEmailQueueItem(Convert.ToInt32(actionArgument));
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes action menu.
    /// </summary>
    protected void InitializeActionMenu()
    {
        bool enabled = (gridElem.GridView.Rows.Count > 0);
        bool resending = enabled && (ThreadEmailSender.SendingThreads <= 0);

        // Resend all failed
        string[,] actions = new string[5, 10];
        actions[0, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[0, 1] = ResHelper.GetString("NewsletterEmailQueue_List.ResendAllFailed");
        if (resending)
        {
            actions[0, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("NewsletterEmailQueue_List.ResendAllFailedConfirmationMessage")) + ")) return false;";
        }
        actions[0, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/resendallfailed.png");
        actions[0, 6] = "resendallfailed";
        actions[0, 9] = resending.ToString();

        // Resend all
        actions[1, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[1, 1] = ResHelper.GetString("NewsletterEmailQueue_List.ResendAll");
        if (resending)
        {
            actions[1, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EmailQueue.ResendAllConfirmation")) + ")) return false;";
        }
        actions[1, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/resendall.png");
        actions[1, 6] = "resendall";
        actions[1, 9] = resending.ToString();

        // Delete all failed
        actions[2, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[2, 1] = ResHelper.GetString("NewsletterEmailQueue_List.DeleteAllFailed");
        if (enabled)
        {
            actions[2, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("NewsletterEmailQueue_List.DeleteAllFailedConfirmationMessage")) + ")) return false;";
        }
        actions[2, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/deleteallfailed.png");
        actions[2, 6] = "deleteallfailed";
        actions[2, 9] = enabled.ToString();

        // Delete all
        actions[3, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[3, 1] = ResHelper.GetString("NewsletterEmailQueue_List.DeleteAll");
        if (enabled)
        {
            actions[3, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("NewsletterEmailQueue_List.DeleteAllConfirmationMessage")) + ")) return false;";
        }
        actions[3, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/deleteall.png");
        actions[3, 6] = "deleteall";
        actions[3, 9] = enabled.ToString();

        // Refresh
        actions[4, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[4, 1] = ResHelper.GetString("general.refresh");
        actions[4, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/refresh.png");
        actions[4, 6] = "refresh";

        CurrentMaster.HeaderActions.Actions = actions;
        CurrentMaster.HeaderActions.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);
        CurrentMaster.HeaderActions.HelpTopicName = "e_mail_queue_tab";
        CurrentMaster.HeaderActions.HelpName = "helpTopic";
    }

    #endregion


    #region "Header action evets"

    void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        // Check user permission (for complex operations only)
        if (e.CommandName != "refresh")
        {
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "configure"))
            {
                RedirectToCMSDeskAccessDenied("cms.newsletter", "configure");
            }
        }

        switch (e.CommandName.ToLower())
        {
            case "resendall":
                EmailQueueManager.SendAllEmails(true, true, 0);

                gridElem.ReloadData();
                this.lblIntro.Text = "<strong>" + ResHelper.GetString("EmailQueue.SendingEmails") + "</strong>";
                break;
            case "resendallfailed":
                EmailQueueManager.SendAllEmails(true, false, 0);
                gridElem.ReloadData();
                this.lblIntro.Text = "<strong>" + ResHelper.GetString("EmailQueue.SendingEmails") + "</strong>";
                break;
            case "deleteall":
                EmailQueueManager.DeleteAllEmailQueueItems(siteId);
                gridElem.ReloadData();
                break;
            case "deleteallfailed":
                EmailQueueManager.DeleteFailedEmailQueueItems(siteId);
                gridElem.ReloadData();
                break;
            case "refresh":
                gridElem.ReloadData();
                break;
        }
    }

    #endregion
}
