<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletter_New.aspx.cs" Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_New"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Tools - New newsletter" %>

<%@ Register Src="~/CMSModules/Newsletters/FormControls/NewsletterTemplateSelector.ascx"
    TagName="NewsletterTemplateSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/Selectors/ScheduleInterval.ascx" TagName="ScheduleInterval"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewsletterDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterDisplayName" runat="server" CssClass="TextBoxField"
                    MaxLength="250" />
                <asp:RequiredFieldValidator ID="rfvNewsletterDisplayName" runat="server" ControlToValidate="txtNewsletterDisplayName"
                    Display="dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewsletterName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterName" runat="server" CssClass="TextBoxField" MaxLength="250" />
                <asp:RequiredFieldValidator ID="rfvNewsletterName" runat="server" ControlToValidate="txtNewsletterName"
                    Display="dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblSubscriptionTemplate" EnableViewState="false" />
            </td>
            <td>
                <cms:NewsletterTemplateSelector ID="subscriptionTemplate" runat="server" />
                <cms:LocalizedLabel ID="lblSubscriptionError" runat="server" ResourceString="Newsletter_Edit.NoSubscriptionTemplateSelected"
                    Visible="false" CssClass="ErrorLabel" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblUnsubscriptionTemplate" EnableViewState="false" />
            </td>
            <td>
                <cms:NewsletterTemplateSelector ID="unsubscriptionTemplate" runat="server" />
                <cms:LocalizedLabel ID="lblUnsubscriptionError" runat="server" ResourceString="Newsletter_Edit.NoUnsubscriptionTemplateSelected"
                    Visible="false" CssClass="ErrorLabel" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewsletterSenderName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterSenderName" runat="server" CssClass="TextBoxField"
                    MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvNewsletterSenderName" runat="server" ErrorMessage=""
                    ControlToValidate="txtNewsletterSenderName" EnableViewState="false"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewsletterSenderEmail" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterSenderEmail" runat="server" CssClass="TextBoxField"
                    MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvNewsletterSenderEmail" runat="server" ErrorMessage=""
                    ControlToValidate="txtNewsletterSenderEmail" EnableViewState="false"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr style="height: 20px;">
        </tr>
        <tr>
            <td colspan="2">
                <asp:RadioButton ID="radTemplateBased" runat="server" GroupName="NewsletterType"
                    AutoPostBack="True" OnCheckedChanged="radTemplateBased_CheckedChanged" />
            </td>
        </tr>
        <tr style="height: 5px;">
        </tr>
        <tr>
            <td class="FieldLabel">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="lblIssueTemplate"
                    EnableViewState="false" />
            </td>
            <td>
                <cms:NewsletterTemplateSelector ID="issueTemplate" runat="server" />
                <cms:LocalizedLabel ID="lblIssueError" runat="server" ResourceString="Newsletter_Edit.NoEmailTemplateSelected"
                    Visible="false" CssClass="ErrorLabel" EnableViewState="false" />
            </td>
        </tr>
        <tr style="height: 15px;">
        </tr>
        <tr>
            <td colspan="2">
                <asp:RadioButton ID="radDynamic" runat="server" GroupName="NewsletterType" AutoPostBack="True"
                    OnCheckedChanged="radDynamic_CheckedChanged" />
            </td>
        </tr>
        <tr style="height: 5px;">
        </tr>
        <tr>
            <td class="FieldLabel">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="lblNewsletterDynamicURL"
                    EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterDynamicURL" runat="server" CssClass="TextBoxField"
                    MaxLength="500" />
            </td>
        </tr>
        <asp:PlaceHolder ID="plcInterval" runat="server">
            <tr>
                <td class="FieldLabel" style="vertical-align: top;">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="lblSchedule" EnableViewState="false" />
                </td>
                <td>
                    <asp:CheckBox ID="chkSchedule" runat="server" Checked="true" OnCheckedChanged="chkSchedule_CheckedChanged"
                        AutoPostBack="true" /><br />
                    <cms:ScheduleInterval ID="ScheduleInterval1" runat="server" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
