<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForumGroupList.ascx.cs"
    Inherits="CMSModules_Forums_Controls_Forums_ForumGroupList" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" TagName="PageTitle"
    TagPrefix="cms" %>
<asp:Panel ID="pnlGroups" runat="server">
    <cms:UniGrid runat="server" ID="gridGroups" GridName="~/CMSModules/Forums/Controls/Forums/Group_List.xml"
        OrderBy="GroupOrder" Columns="GroupID, GroupDisplayName" DelayedReload="true" />
</asp:Panel>
<br />
<%--Approve/Reject--%>
<asp:Panel runat="server" ID="pnlApprove">
    <asp:Panel runat="server" ID="pnlTitle" CssClass="PageHeader">
        <cms:PageTitle runat="server" ID="titleElem" TitleCssClass="SubTitleHeader" />
    </asp:Panel>
    <asp:Panel ID="pnlApproveRejectGrid" runat="server">
        <br />
        <cms:UniGrid runat="server" ID="gridApprove" GridName="~/CMSModules/Forums/Controls/Forums/Approve_List.xml"
            Columns="PostID, PostForumID, PostUserName, PostSubject, PostText" OrderBy="PostID"
            DelayedReload="true" />
    </asp:Panel>
</asp:Panel>
