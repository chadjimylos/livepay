using System;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.FormEngine;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSSiteManager_Administration_Users_User_New : CMSUsersPage
{
    #region "Variables"

    private int? mSiteId = null;
    private string mSiteName = null;

    #endregion

    #region "Properties"

    /// <summary>
    /// Site name
    /// </summary>
    public string SiteName
    {
        get
        {
            if (mSiteName == null)
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(SiteId);
                if (si != null)
                {
                    mSiteName = si.SiteName;
                }
                else
                {
                    mSiteName = "";
                }
            }
            return mSiteName;
        }
    }

    
    /// <summary>
    /// Site ID
    /// </summary>
    public int SiteId
    {
        get
        {
            if (mSiteId == null)
            {
                mSiteId = GetSiteID(Request.QueryString["siteid"]);
            }
            return mSiteId.Value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify") || CMSContext.CurrentUser.IsInRole("eBusinessOps", CMSContext.CurrentSite.SiteName.ToString()))
        {
            RedirectToAccessDenied("CMS.Users", "Modify");
        }

        LabelConfirmPassword.Text = ResHelper.GetString("Administration-User_New.ConfirmPassword");
        LabelIsEditor.Text = ResHelper.GetString("Administration-User_New.IsEditor");
        LabelFullName.Text = ResHelper.GetString("Administration-User_New.FullName");
        LabelPassword.Text = ResHelper.GetString("Administration-User_New.Password");
        ButtonOK.Text = ResHelper.GetString("general.ok");
        RequiredFieldValidatorFullName.ErrorMessage = ResHelper.GetString("Administration-User_New.RequiresFullName");

        if (!RequestHelper.IsPostBack())
        {
            CheckBoxEnabled.Checked = true;
            CheckBoxIsEditor.Checked = true;
        }

        string users = ResHelper.GetString("general.users");
        string currentUser = ResHelper.GetString("Administration-User_New.CurrentUser");
        string title = ResHelper.GetString("Administration-User_New.Title");
        
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = users;
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Administration/Users/Users_Frameset.aspx?siteid=" + SiteId;
        pageTitleTabs[0, 2] = "_parent";
        pageTitleTabs[1, 0] = currentUser;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.HelpTopicName = "new_user";
    }
    

    protected void ButtonOK_Click(object sender, EventArgs e)
    {
        // Email format validation
        if ((TextBoxEmail.Text.Trim() != "") && (!ValidationHelper.IsEmail(TextBoxEmail.Text)))
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Administration-User_New.WrongEmailFormat");
            return;
        }

        // Find whether user name is valid
        string result = null;
        if (!ucUserName.IsValid())
        {
            result = ucUserName.ValidationError;
        }

        // Additional validation
        if (String.IsNullOrEmpty(result))
        {
            result = new Validator().NotEmpty(TextBoxFullName.Text, ResHelper.GetString("Administration-User_New.RequiresFullName")).Result;
        }

        if (result == "")
        {
            if (TextBoxConfirmPassword.Text == TextPassword.Text)
            {
                if (UserInfoProvider.GetUserInfo((string)ucUserName.Value) == null)
                {
                    int userId = SaveNewUser();
                    if (userId != -1)
                    {
                        UrlHelper.Redirect("User_Edit_Frameset.aspx?userId=" + userId + "&siteid=" + SiteId);
                    }
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Administration-User_New.UserExists");
                }
            }
            else
            {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Administration-User_Edit_Password.PasswordsDoNotMatch");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }


    /// <summary>
    /// Saves new user's data into DB.
    /// </summary>
    /// <returns>Returns ID of created user</returns>
    protected int SaveNewUser()
    {
        UserInfo ui = new UserInfo();

        // Load default values
        FormHelper.LoadDefaultValues("cms.user", ui.DataClass.DataRow);

        ui.PreferredCultureCode = "";
        ui.Email = TextBoxEmail.Text;
        ui.FirstName = "";
        ui.FullName = TextBoxFullName.Text;
        ui.LastName = "";
        ui.MiddleName = "";
        ui.UserName = (string)ucUserName.Value;
        ui.Enabled = CheckBoxEnabled.Checked;
        ui.IsEditor = CheckBoxIsEditor.Checked;
        ui.IsExternal = false;
        ui.IsGlobalAdministrator = false;

        // Check license limitations only in cmsdesk
        if (SiteId > 0)
        {
            // Check limitations for Global administrator
            if (ui.IsGlobalAdministrator)
            {
                if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.GlobalAdmininistrators, VersionActionEnum.Insert, false))
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("License.MaxItemsReachedGlobal");
                }
            }

            // Check limitations for editors
            if (ui.IsEditor)
            {
                if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Editors, VersionActionEnum.Insert, false))
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("License.MaxItemsReachedEditor");
                }
            }

            // Check limitations for site members
            if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.SiteMembers, VersionActionEnum.Insert, false))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("License.MaxItemsReachedSiteMember");
            }
        }
        

        // Check whether email is unique if it is required
        if (!UserInfoProvider.IsEmailUnique(TextBoxEmail.Text.Trim(), SiteName, 0))
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("UserInfo.EmailAlreadyExist");
            return -1;
        }


        if (!lblError.Visible)
        {
            // Create the user with password
            UserInfoProvider.SetPassword(ui, TextPassword.Text);
            UserInfoProvider.SetUserInfo(ui);
            
            // Add user to current site
            if (SiteId > 0)
            {
                UserInfoProvider.AddUserToSite(ui.UserName, SiteName);
            }

            return ui.UserID;
        }

        return -1;
    }
}
