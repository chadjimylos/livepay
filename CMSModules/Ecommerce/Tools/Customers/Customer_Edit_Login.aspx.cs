using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Customers_Customer_Edit_Login : CMSEcommercePage
{
    protected int customerId = 0;
    protected int userId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        chkHasLogin.CheckedChanged += new EventHandler(chkHasLogin_CheckedChanged);
        rqvUserName.ErrorMessage = ResHelper.GetString("Customer_Edit_Login_Edit.rqvUserName");
        rqvPassword1.ErrorMessage = ResHelper.GetString("Customer_Edit_Login_Edit.rqvPassword1");
        rqvPassword2.ErrorMessage = ResHelper.GetString("Customer_Edit_Login_Edit.rqvPassword2");


        // control initializations				
        lblHasLogin.Text = ResHelper.GetString("Customer_Edit_Login_Edit.lblHasLogin");
        lblPassword1.Text = ResHelper.GetString("Customer_Edit_Login_Edit.lblPassword1");
        lblPassword2.Text = ResHelper.GetString("Customer_Edit_Login_Edit.lblPassword2");
        lblHasLogin1.Text = ResHelper.GetString("Customer_Edit_Login_Edit.lblHasLogin");

        btnOk.Text = ResHelper.GetString("General.OK");

        // get customer id from querystring		
        customerId = ValidationHelper.GetInteger(Request.QueryString["customerId"], 0);

        CustomerInfo ci = CustomerInfoProvider.GetCustomerInfo(customerId);
        if (ci != null)
        {
            // If the customer already has the record in the CMS_User table, display only UserName            
            UserInfo ui = UserInfoProvider.GetUserInfo(ci.CustomerUserID);
            if (ui != null)
            {
                pnlEdit.Visible = false;
                pnlStatic.Visible = true;                
                lblUserNameStaticValue.Text = HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(ui.UserName));
            }
            else
            {
                pnlEdit.Visible = true;
                pnlStatic.Visible = false;
            }
        }

        if (!RequestHelper.IsPostBack())
        {
            // show that the address was created or updated successfully
            if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
            {
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
        }

        // Enable or disable the controls
        ModifyControlStatus(chkHasLogin.Checked);
    }

    void chkHasLogin_CheckedChanged(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        if (chkHasLogin.Checked)
        {
            ModifyControlStatus(true);
        }
        else
        {
            ModifyControlStatus(false);
        }
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        if (customerId != 0)
        {
            string errorMessage = new Validator().NotEmpty(txtUserName.Text, "Customer_Edit_Login_Edit.rqvUserName").NotEmpty(txtPassword1.Text, "Customer_Edit_Login_Edit.rqvPassword1").NotEmpty(txtPassword2.Text, "Customer_Edit_Login_Edit.rqvPassword2").Result;

            if (txtPassword1.Text != txtPassword2.Text)
            {
                errorMessage = ResHelper.GetString("Customer_Edit_Login_Edit.DifferentPasswords");
            }

            if (errorMessage == "")
            {
                if (chkHasLogin.Checked)
                {
                    try
                    {
                        CustomerInfo ci = CustomerInfoProvider.GetCustomerInfo(customerId);

                        UserInfo ui = new UserInfo();
                        ui.UserName = txtUserName.Text.Trim();
                        ui.FullName = ci.CustomerFirstName + " " + ci.CustomerLastName;
                        ui.IsGlobalAdministrator = false;
                        ui.SetValue("UserEnabled", true);
                        UserInfoProvider.SetUserInfo(ui);
                        UserInfoProvider.SetPassword(ui.UserName, txtPassword1.Text);
                        UserInfoProvider.AddUserToSite(ui.UserName, CMSContext.CurrentSiteName);

                        ci.CustomerUserID = ui.UserID;

                        CustomerInfoProvider.SetCustomerInfo(ci);

                        UrlHelper.Redirect("Customer_Edit_Login.aspx?customerId=" + customerId + "&saved=1");
                    }
                    catch (Exception ex)
                    {
                        lblError.Visible = true;
                        lblError.Text = ex.Message;
                    }
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = errorMessage;
            }
        }
    }


    private void ModifyControlStatus(bool enabled)
    {
        txtUserName.Enabled = enabled;
        txtPassword1.Enabled = enabled;
        txtPassword2.Enabled = enabled;
        btnOk.Enabled = enabled;
    }
}
