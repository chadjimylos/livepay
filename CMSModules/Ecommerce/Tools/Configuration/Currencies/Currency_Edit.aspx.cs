using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_Currencies_Currency_Edit : CMSEcommerceConfigurationPage
{
    protected int currencyid = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        rfvDisplayName.ErrorMessage = ResHelper.GetString("Currency_Edit.ErrorDisplayName");
        rfvCodeName.ErrorMessage = ResHelper.GetString("Currency_Edit.ErrorCodeName");
        rfvRoundTo.ErrorMessage = ResHelper.GetString("Currency_Edit.ErrorRoundTo");
        revRoundTo.ErrorMessage = ResHelper.GetString("Currency_Edit.ErrorRoundTo");
        rfvCurrencyCode.ErrorMessage = ResHelper.GetString("Currency_Edit.rfvCurrencyCode");
        rfvFormatString.ErrorMessage = ResHelper.GetString("Currency_Edit.rfvFormatString");

        revRoundTo.ValidationExpression = @"^([0-9])+$";

        // control initializations				
        lblCurrencyDisplayName.Text = ResHelper.GetString("Currency_Edit.CurrencyDisplayNameLabel");
        lblCurrencyName.Text = ResHelper.GetString("Currency_Edit.CurrencyNameLabel");
        lblCurrencyCode.Text = ResHelper.GetString("Currency_Edit.CurrencyCodeLabel");
        lblCurrencyRoundTo.Text = ResHelper.GetString("Currency_Edit.CurrencyRoundToLabel");
        lblFormatString.Text = ResHelper.GetString("Currency_Edit.lblFormatString");
        lblFormatStringnInfo.Text = ResHelper.GetString("Currency_Edit.lblFormatStringInfo");

        btnOk.Text = ResHelper.GetString("General.OK");

        imgHelp.ImageUrl = GetImageUrl("General/HelpSmall.png");
        imgHelp.ToolTip = ResHelper.GetString("currencyedit.currencycode");

        string currentCurrency = ResHelper.GetString("Currency_Edit.NewItemCaption");

        // get currency id from querystring
        if ((Request.QueryString["currencyid"] != null) && (Request.QueryString["currencyid"] != ""))
        {
            currencyid = ValidationHelper.GetInteger(Request.QueryString["currencyid"], 0);
            CurrencyInfo currencyObj = CurrencyInfoProvider.GetCurrencyInfo(currencyid);
            currentCurrency = ResHelper.LocalizeString(currencyObj.CurrencyDisplayName);

            // fill editing form
            if (!RequestHelper.IsPostBack())
            {
                LoadData(currencyObj);

                // show that the currency was created or updated successfully
                if (Request.QueryString["saved"] == "1")
                {
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
            }

            this.CurrentMaster.Title.TitleText = ResHelper.GetString("Currency_Edit.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_Currency/object.png");
        }
        else
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("Currency_New.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_Currency/new.png");
        }

        this.CurrentMaster.Title.HelpTopicName = "newedit_currency";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Currency_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/Currencies/Currency_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentCurrency;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// Load data of editing currency.
    /// </summary>
    /// <param name="currencyObj">Currency object.</param>
    protected void LoadData(CurrencyInfo currencyObj)
    {
        txtCurrencyDisplayName.Text = currencyObj.CurrencyDisplayName;
        txtCurrencyName.Text = currencyObj.CurrencyName;
        txtCurrencyCode.Text = currencyObj.CurrencyCode;
        txtFormatString.Text = currencyObj.CurrencyFormatString;
        txtCurrencyRoundTo.Text = "";
        if (currencyObj.CurrencyRoundTo != -1)
        {
            txtCurrencyRoundTo.Text = Convert.ToString(currencyObj.CurrencyRoundTo);
        }
        chkCurrencyEnabled.Checked = currencyObj.CurrencyEnabled;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        string errorMessage = ValidateForm();

        // Check whether the main currency is being disabled
        CurrencyInfo main = CurrencyInfoProvider.GetMainCurrency();
        if ((main != null) && (main.CurrencyID == currencyid) && !chkCurrencyEnabled.Checked)
        {
            errorMessage = String.Format(ResHelper.GetString("ecommerce.disablemaincurrencyerror"), main.CurrencyDisplayName);
        }

        if (errorMessage == "")
        {

            // currencyName must to be unique
            CurrencyInfo currencyObj = CurrencyInfoProvider.GetCurrencyInfo(txtCurrencyName.Text.Trim());

            // if currencyName value is unique														
            if ((currencyObj == null) || (currencyObj.CurrencyID == currencyid))
            {
                // if currencyName value is unique -> determine whether it is update or insert 
                if ((currencyObj == null))
                {
                    // get CurrencyInfo object by primary key
                    currencyObj = CurrencyInfoProvider.GetCurrencyInfo(currencyid);
                    if (currencyObj == null)
                    {
                        // create new item -> insert
                        currencyObj = new CurrencyInfo();
                        currencyObj.CurrencyIsMain = false;
                    }
                }

                currencyObj.CurrencyDisplayName = txtCurrencyDisplayName.Text.Trim();
                currencyObj.CurrencyName = txtCurrencyName.Text.Trim();
                currencyObj.CurrencyCode = txtCurrencyCode.Text.Trim();
                currencyObj.CurrencyFormatString = txtFormatString.Text;
                // Set null if not specified
                currencyObj.CurrencyRoundTo = ValidationHelper.GetInteger(txtCurrencyRoundTo.Text.Trim(), -1);
                currencyObj.CurrencyEnabled = chkCurrencyEnabled.Checked;

                CurrencyInfoProvider.SetCurrencyInfo(currencyObj);

                UrlHelper.Redirect("Currency_Edit.aspx?currencyid=" + Convert.ToString(currencyObj.CurrencyID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Currency_Edit.CurrencyNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    /// <summary>
    /// Validates form and retursn error message if some error occurs, otherwise returns empty string.
    /// </summary>
    private string ValidateForm()
    {
        string errorMessage = new Validator()
               .NotEmpty(txtCurrencyName.Text.Trim(), ResHelper.GetString("Currency_Edit.ErrorCodeName"))
               .NotEmpty(txtCurrencyDisplayName.Text.Trim(), ResHelper.GetString("Currency_Edit.ErrorDisplayName"))
               .NotEmpty(txtFormatString.Text, ResHelper.GetString("Currency_Edit.ErrorFormatString"))
               .IsRegularExp(txtCurrencyRoundTo.Text, "^([0-9]){1,2}$", ResHelper.GetString("Currency_Edit.ErrorRoundTo")).Result;

        int digits = ValidationHelper.GetInteger(txtCurrencyRoundTo.Text.Trim(), -1);
        if (digits > 15 || digits < 0)
        {
            errorMessage = ResHelper.GetString("Currency_Edit.ErrorRoundTo");
        }

        // Validate currency code name
        if ((errorMessage == "") && (!ValidationHelper.IsCodeName(txtCurrencyName.Text.Trim())))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

        // Validate currency format string
        if (errorMessage == "")
        {
            try
            {
                //test for double exception
                string.Format(txtFormatString.Text.Trim(), (double)1.234);

                string.Format(txtFormatString.Text.Trim(), "12.12");
            }
            catch
            {
                errorMessage = ResHelper.GetString("Currency_Edit.ErrorCurrencyFormatString");
            }
        }

        return errorMessage;
    }
}
