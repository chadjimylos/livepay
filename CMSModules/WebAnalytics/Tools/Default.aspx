<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="CMSModules_WebAnalytics_Tools_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Analytics</title>
    
    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
</head>
<frameset border="0" cols="220,*" id="colsFramesetAnalytics" runat="server">
    <frame name="analtree" src="Analytics_Statistics.aspx" scrolling="no" frameborder="0" />
    <frameset border="0" rows="111, 154, *" runat="server" ID="rowsFrameset" >
                <frame name="analmonths" src="../../../CMSDesk/blank.htm" scrolling="auto" frameborder="0" />
                <frame name="analdays" src="../../../CMSDesk/blank.htm" scrolling="auto" frameborder="0" />
                <frame name="analreport" src="../../../CMSDesk/blank.htm" scrolling="auto" frameborder="0" />
            </frameset>
	<noframes>
		<p id="p1">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		</p>
	</noframes>
</frameset>
</html>
