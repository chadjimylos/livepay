<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RecycleBin.aspx.cs" Inherits="CMSDesk_MyDesk_RecycleBin_RecycleBin"
    Theme="Default" EnableEventValidation="false" MaintainScrollPositionOnPostback="true"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="MyDesk - Recycle bin" %>

<%@ Register Src="~/CMSAdminControls/AsyncControl.ascx" TagName="AsyncControl" TagPrefix="cms" %>
<%@ Register Src="~/CMSSiteManager/Administration/RecycleBin/Controls/RecycleBin.ascx" TagName="RecycleBin" TagPrefix="cms" %>
<asp:Content ID="cntBody" ContentPlaceHolderID="plcBeforeContent" runat="server">
    <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cms:RecycleBin ID="recycleBin" runat="server" IsCMSDesk="true" />
        </ContentTemplate>
    </cms:CMSUpdatePanel>
</asp:Content>
