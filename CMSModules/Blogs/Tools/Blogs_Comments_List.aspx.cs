using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.SettingsProvider;
using CMS.Blogs;
using CMS.UIControls;
using CMS.TreeEngine;
using CMS.WorkflowEngine;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Blogs_Tools_Blogs_Comments_List : CMSBlogsPage
{
    protected CurrentUserInfo currentUser = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        // No cms.blog doc. type
        if (DataClassInfoProvider.GetDataClass("cms.blog") == null)
        {
            RedirectToInformation(ResHelper.GetString("blog.noblogdoctype"));
        }

        currentUser = CMSContext.CurrentUser;
        if (currentUser == null)
        {
            return;
        }

        // Set the page title when existing category is being edited
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("myblogs.comments.header");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Blog_Comment/object.png");
        this.CurrentMaster.Title.HelpTopicName = "blogs_comments";

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        this.gridComments.OnAction += new OnActionEventHandler(gridComments_OnAction);
        this.gridComments.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridComments_OnExternalDataBound);
        this.gridComments.OnBeforeSorting += new OnBeforeSorting(gridComments_OnBeforeSorting);
        this.gridComments.OnPageChanged += new EventHandler<EventArgs>(gridComments_OnPageChanged);
        this.gridComments.OnDataReload += new UniGrid.OnDataReloadEventHandler(gridComments_OnDataReload);
        this.gridComments.OnPageSizeChanged += new OnPageSizeChanged(gridComments_OnPageSizeChanged);
        this.gridComments.HideControlForZeroRows = true;

        this.btnAction.Text = ResHelper.GetString("General.Ok");
        this.btnAction.OnClientClick = "return MassConfirm('" + this.drpAction.ClientID + "'," + ScriptHelper.GetString(ResHelper.GetString("MyBlogs.Comments.ConfirmDelete")) + ");";

        this.filterElem.SearchPerformed += new EventHandler(filterElem_SearchPerformed);

        // Load action dropdown
        if (!UrlHelper.IsPostback())
        {
            // Actions dropdown
            drpAction.Items.Add(new ListItem(ResHelper.GetString("General.SelectAction"), ""));
            drpAction.Items.Add(new ListItem(ResHelper.GetString("General.Approve"), "approve"));
            drpAction.Items.Add(new ListItem(ResHelper.GetString("General.Reject"), "reject"));
            drpAction.Items.Add(new ListItem(ResHelper.GetString("MyBlogs.Comments.IsSpam"), "spam"));
            drpAction.Items.Add(new ListItem(ResHelper.GetString("MyBlogs.Comments.IsNoSpam"), "nospam"));
            drpAction.Items.Add(new ListItem(ResHelper.GetString("General.Delete"), "delete"));
        }
    }


    protected void gridComments_OnPageSizeChanged()
    {
        ReloadData();
    }


    protected void filterElem_SearchPerformed(object sender, EventArgs e)
    {
        ReloadData();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!UrlHelper.IsPostback())
        {
            // Load comments according to filter.
            ReloadData();
        }

        // Hide actions dropdown and button if no data
        this.pnlActions.Visible = !DataHelper.DataSourceIsEmpty(gridComments.GridView.DataSource);
    }


    protected void gridComments_OnPageChanged(object sender, EventArgs e)
    {
        ReloadData();
    }


    protected void gridComments_OnBeforeSorting(object sender, EventArgs e)
    {
        ReloadData();
    }


    protected DataSet gridComments_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        return BlogCommentInfoProvider.GetComments(0, null, this.filterElem.CommentWhereCondition, this.filterElem.BlogWhereCondition, "CommentID, CommentDate, CommentApproved, CommentUserName, CommentText, CommentIsSpam", currentTopN, currentOrder, currentOffset, currentPageSize, ref totalRecords);
    }


    protected void gridComments_OnAction(string actionName, object actionArgument)
    {
        int commentId = ValidationHelper.GetInteger(actionArgument, 0);
        switch (actionName.ToLower())
        {
            case "delete":
                // Delete specified comment
                BlogCommentInfoProvider.DeleteBlogCommentInfo(commentId);
                ReloadData();
                break;

            case "approve":
                BlogCommentInfo bci = BlogCommentInfoProvider.GetBlogCommentInfo(commentId);
                if (bci != null)
                {
                    if (bci.CommentApproved)
                    {
                        // Set comment as 'rejected'
                        bci.CommentApproved = false;
                        bci.CommentApprovedByUserID = 0;
                    }
                    else
                    {
                        // Set comment as 'approved'
                        bci.CommentApproved = true;
                        bci.CommentApprovedByUserID = currentUser.UserID;
                    }
                    BlogCommentInfoProvider.SetBlogCommentInfo(bci);
                }
                ReloadData();
                break;

            case "edit":
                // JavaScript
                break;
        }
    }


    protected object gridComments_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        bool approve = false;
        switch (sourceName.ToLower())
        {
            case "commentusername":
                return HTMLHelper.HTMLEncode(Convert.ToString(parameter));

            case "commenttext":
                string text = Convert.ToString(parameter);
                if (text.Length > 50)
                {
                    text = text.Substring(0, 50) + "...";
                }
                return HTMLHelper.HTMLEncode(text);

            case "commentapproved":
                return UniGridFunctions.ColoredSpanYesNo(parameter);

            case "commentisspam":

                return UniGridFunctions.ColoredSpanYesNoReversed(parameter);

            case "approve":
                approve = ValidationHelper.GetBoolean(((DataRowView)((GridViewRow)parameter).DataItem).Row["CommentApproved"], false);
                if (!approve)
                {
                    ImageButton button = ((ImageButton)sender);
                    button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Approve.png");
                    button.ToolTip = ResHelper.GetString("general.approve");
                }
                else
                {
                    ImageButton button = ((ImageButton)sender);
                    button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Reject.png");
                    button.ToolTip = ResHelper.GetString("general.reject");
                }
                break;

            case "edit":
                string commentId = ((DataRowView)((GridViewRow)parameter).DataItem).Row["CommentID"].ToString();

                ImageButton editButton = ((ImageButton)sender);
                editButton.OnClientClick = "modalDialog('" + ResolveUrl("~/CMSModules/Blogs/Controls/Comment_Edit.aspx") + "?commentID=" + commentId + this.filterElem.FilterQueryString + "', 'Comment Edit', 420, 430); return false;";
                break;
        }

        return parameter;
    }


    /// <summary>
    /// Handle mass actions
    /// </summary>
    protected void btnAction_Click(object sender, EventArgs e)
    {
        if (drpAction.SelectedValue != "SELECT")
        {
            ArrayList list = gridComments.SelectedItems;
            if (list.Count > 0)
            {
                foreach (string commnentId in list)
                {
                    BlogCommentInfo bci = BlogCommentInfoProvider.GetBlogCommentInfo(Convert.ToInt32(commnentId));
                    switch (drpAction.SelectedValue.ToLower())
                    {
                        case "delete":
                            // Delete specified comment
                            BlogCommentInfoProvider.DeleteBlogCommentInfo(bci);
                            break;

                        case "approve":
                            if (!bci.CommentApproved)
                            {
                                // Set comment as 'approved'
                                bci.CommentApproved = true;
                                bci.CommentApprovedByUserID = CMSContext.CurrentUser.UserID;
                                BlogCommentInfoProvider.SetBlogCommentInfo(bci);
                            }
                            break;

                        case "reject":
                            if (bci.CommentApproved)
                            {
                                // Set comment as 'reject'
                                bci.CommentApproved = false;
                                bci.CommentApprovedByUserID = 0;
                                BlogCommentInfoProvider.SetBlogCommentInfo(bci);
                            }
                            break;

                        case "spam":
                            if (!bci.CommentIsSpam)
                            {
                                bci.CommentIsSpam = true;
                                BlogCommentInfoProvider.SetBlogCommentInfo(bci);
                            }
                            break;

                        case "nospam":
                            if (bci.CommentIsSpam)
                            {
                                bci.CommentIsSpam = false;
                                BlogCommentInfoProvider.SetBlogCommentInfo(bci);
                            }
                            break;
                    }
                }
            }
            this.gridComments.ResetSelection();
            ReloadData();
            // Reload unigrid for first page if no data for current page
            if (DataHelper.DataSourceIsEmpty(gridComments.GridView.DataSource))
            {
                gridComments.Pager.UniPager.CurrentPage = 1;
                ReloadData();
            }
        }
    }


    /// <summary>
    /// Load data according to filter setings
    /// </summary>
    protected void ReloadData()
    {
        this.gridComments.FilterIsSet = true;
        this.gridComments.ReloadData();
    }
}
