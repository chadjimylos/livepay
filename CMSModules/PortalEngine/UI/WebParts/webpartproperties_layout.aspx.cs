using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_WebParts_webpartproperties_layout : CMSWebPartPropertiesPage
{
    #region "Variables"

    private string aliasPath = "";
    private string webpartId = "";
    private string zoneId = "";

    protected string mSave = null;
    protected string mCheckIn = null;
    protected string mCheckOut = null;
    protected string mUndoCheckOut = null;

    WebPartInfo webPartInfo = null;

    /// <summary>
    /// Current page info
    /// </summary>
    PageInfo pi = null;

    /// <summary>
    /// Page template info
    /// </summary>
    PageTemplateInfo pti = null;


    /// <summary>
    /// Current web part
    /// </summary>
    WebPartInstance webPart = null;

    string layoutCodeName = "";

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check permissions for web part properties UI
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        if (!currentUser.IsAuthorizedPerUIElement("CMS.Content", "WebPartProperties.Layout"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "WebPartProperties.Layout");
        }

        // Check saved
        if (QueryHelper.GetBoolean("saved", false))
        {
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }

        // Init GUI
        mCheckOut = ResHelper.GetString("WebPartLayout.CheckOut");
        mCheckIn = ResHelper.GetString("WebPartLayout.CheckIn");
        mUndoCheckOut = ResHelper.GetString("WebPartLayout.DiscardCheckOut");
        mSave = ResHelper.GetString("General.Save");

        this.imgCheckIn.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkin.png");
        this.imgCheckOut.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkout.png");
        this.imgUndoCheckOut.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/undocheckout.png");
        this.btnUndoCheckOut.OnClientClick = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("General.ConfirmUndoCheckOut")) + ");";

        aliasPath = ValidationHelper.GetString(Request.QueryString["aliaspath"], "");
        webpartId = ValidationHelper.GetString(Request.QueryString["webpartid"], "");
        zoneId = ValidationHelper.GetString(Request.QueryString["zoneid"], "");
        Guid instanceGuid = ValidationHelper.GetGuid(Request.QueryString["instanceguid"], Guid.Empty);

        if ((webpartId != "") && (aliasPath != ""))
        {
            // Get pageinfo
            pi = PageInfoProvider.GetPageInfo(CMSContext.CurrentSiteName, aliasPath, CMSContext.PreferredCultureCode, null, CMSContext.CurrentSite.CombineWithDefaultCulture, null);
            if (pi == null)
            {
                this.lblInfo.Text = ResHelper.GetString("WebPartProperties.WebPartNotFound");
                this.pnlFormArea.Visible = false;
                return;
            }

            // Get page template
            pti = pi.GetInheritedTemplateInfo(CMSContext.PreferredCultureCode, CMSContext.CurrentSite.CombineWithDefaultCulture);

            // Get web part
            webPart = pti.GetWebPart(instanceGuid, webpartId);
            if (webPart == null)
            {
                this.lblInfo.Text = ResHelper.GetString("WebPartProperties.WebPartNotFound");
                this.pnlFormArea.Visible = false;
                return;
            }

            layoutCodeName = ValidationHelper.GetString(webPart.GetValue("WebPartLayout"), "");
        }


        WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webPart.WebPartType);
        if (wpi != null)
        {
            webPartInfo = wpi;
            bool loaded = false;

            if (!RequestHelper.IsPostBack())
            {
                pnlMenu.Visible = false;
                pnlCheckOutInfo.Visible = false;

                if (layoutCodeName != "")
                {
                    WebPartLayoutInfo wpli = WebPartLayoutInfoProvider.GetWebPartLayoutInfo(wpi.WebPartName, layoutCodeName);
                    if (wpli != null)
                    {
                        pnlMenu.Visible = true;
                        pnlCheckOutInfo.Visible = true;

                        // Read-only code text area
                        etaCode.ReadOnly = false;

                        // Set checkout panel
                        SetCheckPanel(wpli);

                        etaCode.Text = wpli.WebPartLayoutCode;
                        loaded = true;
                    }
                }

                if (!loaded)
                {
                    string fileName = webPartInfo.WebPartFileName;
                    if (webPartInfo.WebPartParentID > 0)
                    {
                        WebPartInfo pwpi = WebPartInfoProvider.GetWebPartInfo(webPartInfo.WebPartParentID);
                        if (pwpi != null)
                        {
                            fileName = pwpi.WebPartFileName;
                        }
                    }

                    if (!fileName.StartsWith("~"))
                    {
                        fileName = "~/CMSWebparts/" + fileName;
                    }
                    // Check if filename exist
                    if (!File.Exists(Server.MapPath(fileName)))
                    {
                        lblError.Text = ResHelper.GetString("WebPartProperties.FileNotExist");
                        lblError.Visible = true;
                        plcContent.Visible = false;
                    }
                    else
                    {
                        etaCode.Text = File.ReadAllText(Server.MapPath(fileName));
                    }
                }
            }
        }

        // Strings
        lblLayouts.Text = ResHelper.GetString("WebPartPropertise.LayoutList");

        if (!RequestHelper.IsPostBack())
        {
            LoadLayouts();
        }

        // Add default drop down items
        selectLayout.ShowDefaultItem = true;       

        // Add new item
        if (SettingsKeyProvider.UsingVirtualPathProvider)
        {
            selectLayout.ShowNewItem = true;
        }

        // Where condition
        selectLayout.WhereCondition = "WebPartLayoutWebPartID =" + webPartInfo.WebPartID;

        btnOnOK.Click += new EventHandler(btnOnOK_Click);
        btnOnApply.Click += new EventHandler(btnOnApply_Click);

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ApplyButton", ScriptHelper.GetScript(
               "function SetRefresh(refreshpage) { document.getElementById('" + this.hidRefresh.ClientID + "').value = refreshpage; } \n" +
               "function OnApplyButton(refreshpage) { SetRefresh(refreshpage); " + Page.ClientScript.GetPostBackEventReference(btnOnApply, "") + "} \n" +
               "function OnOKButton(refreshpage) { SetRefresh(refreshpage); " + Page.ClientScript.GetPostBackEventReference(btnOnOK, "") + "} \n"
           ));
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!SettingsKeyProvider.UsingVirtualPathProvider)
        {
            this.lblVirtualInfo.Text = ResHelper.GetString("WebPartLayout.ProviderNotRunning");
            this.plcVirtualInfo.Visible = true;
            this.pnlCheckOutInfo.Visible = false;
            this.pnlMenu.Visible = false;
            etaCode.Enabled = false;
        }
    }


    /// <summary>
    /// Load layouts
    /// </summary>
    private void LoadLayouts()
    {
        if ((!RequestHelper.IsPostBack()) && (layoutCodeName != ""))
        {            
            selectLayout.Value = layoutCodeName;          
        }
    }


    /// <summary>
    /// Selected index changed
    /// </summary>
    protected void drpLayouts_Changed()
    {

    if (webPartInfo != null)
    {
        if (ValidationHelper.GetString(selectLayout.Value, "") == "1")
        {
            plcDescription.Visible = true;
            plcValues.Visible = true;
            etaCode.ReadOnly = false;
            etaCode.Rows = 19;
            pnlMenu.Visible = false;
            pnlCheckOutInfo.Visible = false;
        }
        else
        {
            etaCode.Rows = 24;
            plcDescription.Visible = false;
            plcValues.Visible = false;
            etaCode.ReadOnly = false;

            if (ValidationHelper.GetString(selectLayout.Value, "") == "0")
            {
                etaCode.ReadOnly = true;
                etaCode.Text = File.ReadAllText(Server.MapPath("~/CMSWebparts/" + webPartInfo.WebPartFileName));
                pnlMenu.Visible = false;
                pnlCheckOutInfo.Visible = false;
            }
            else
            {
                etaCode.Rows = 18;

                pnlMenu.Visible = true;
                pnlCheckOutInfo.Visible = true;
                etaCode.Text = "Loading...";

                WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webPart.WebPartType);
                if (wpi != null)
                {
                    WebPartLayoutInfo wpli = WebPartLayoutInfoProvider.GetWebPartLayoutInfo(wpi.WebPartName, ValidationHelper.GetString(selectLayout.Value, string.Empty));
                    if (wpli != null)
                    {
                        SetCheckPanel(null);

                        etaCode.Text = wpli.WebPartLayoutCode;
                    }
                }
            }
        }
    }
    }


    /// <summary>
    /// Save new layout
    /// </summary>
    protected bool Save()
    {
        if (webPartInfo != null)
        {
            // Remove "." due to virtual path provider replacement
            txtLayoutName.Text = txtLayoutName.Text.Replace(".", "");

            string result = new Validator().NotEmpty(txtLayoutName.Text, ResHelper.GetString("WebPartPropertise.errCodeName")).NotEmpty(txtLayoutDisplayName.Text, ResHelper.GetString("WebPartPropertise.errDisplayName")).IsCodeName(txtLayoutName.Text, ResHelper.GetString("WebPartPropertise.errCodeNameFormat")).Result;

            if (result == "")
            {
                WebPartLayoutInfo tmpLayInfo = WebPartLayoutInfoProvider.GetWebPartLayoutInfo(webPartInfo.WebPartName, txtLayoutName.Text.Trim());
                if (tmpLayInfo == null)
                {
                    WebPartLayoutInfo wpli = new WebPartLayoutInfo();

                    wpli.WebPartLayoutCodeName = txtLayoutName.Text.Trim();

                    wpli.WebPartLayoutDescription = txtDescription.Text;
                    wpli.WebPartLayoutDisplayName = txtLayoutDisplayName.Text;
                    wpli.WebPartLayoutCode = etaCode.Text;
                    wpli.WebPartLayoutWebPartID = webPartInfo.WebPartID;
                    WebPartLayoutInfoProvider.SetWebPartLayoutInfo(wpli);

                    LoadLayouts();

                    txtDescription.Text = "";
                    txtLayoutDisplayName.Text = "";
                    txtLayoutName.Text = "";

                    selectLayout.Value = wpli.WebPartLayoutCodeName;
                    plcDescription.Visible = false;
                    plcValues.Visible = false;
                    etaCode.Rows = 17;

                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");

                    pnlMenu.Visible = true;
                    pnlCheckOutInfo.Visible = true;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("WebPartPropertise.CodeNameAllreadyExists");
                    etaCode.Rows = 17;
                    return false;
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = result;
                etaCode.Rows = 17;
                return false;
            }
        }

        return true;
    }


    /// <summary>
    /// Set current layout
    /// </summary>
    protected void SetCurrentLayout(bool saveToWebPartInstance)
    {
        if ((webPart != null) && (ValidationHelper.GetString(selectLayout.Value, "") != "1"))
        {
            if (saveToWebPartInstance)
            {
                if (ValidationHelper.GetString(selectLayout.Value, "") == "0")
                {
                    webPart.SetValue("WebPartLayout", "");
                }
                else
                {
                    webPart.SetValue("WebPartLayout", selectLayout.Value);
                }

                // Update page template
                PageTemplateInfoProvider.SetPageTemplateInfo(pti);
            }

            string parameters = this.aliasPath + "/" + this.zoneId + "/" + this.webpartId;
            string cacheName = "CMSVirtualWebParts|" + parameters.ToLower().TrimStart('/');

            CacheHelper.Remove(cacheName);

            WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webPart.WebPartType);
            if (wpi != null)
            {
                WebPartLayoutInfo wpli = WebPartLayoutInfoProvider.GetWebPartLayoutInfo(wpi.WebPartName, ValidationHelper.GetString(selectLayout.Value, string.Empty));
                if (wpli != null)
                {
                    wpli.WebPartLayoutCode = etaCode.Text;
                    WebPartLayoutInfoProvider.SetWebPartLayoutInfo(wpli);
                }
            }
        }
    }


    /// <summary>
    /// Saves the webpart properties and closes the window.
    /// </summary>
    protected void btnOnOK_Click(object sender, EventArgs e)
    {
        bool err = true;

        if (ValidationHelper.GetString(selectLayout.Value, "") == "1")
        {
            err = Save();
        }

        SetCurrentLayout(true);

        bool refresh = ValidationHelper.GetBoolean(this.hidRefresh.Value, false);

        string script = "";
        if (refresh)
        {
            script = "RefreshPage(); \n";
        }

        // Close the window
        if (err)
        {
            ltlScript.Text += ScriptHelper.GetScript(script + "top.window.close();");
        }
    }


    /// <summary>
    /// Saves the webpart properties
    /// </summary>
    protected void btnOnApply_Click(object sender, EventArgs e)
    {
        // New layout
        bool err = true;
        if (ValidationHelper.GetString(selectLayout.Value, "") == "1")
        {
            err = Save();
        }

        SetCurrentLayout(true);

        if (err)
        {
            string url = Request.Url.AbsoluteUri;
            url = UrlHelper.UpdateParameterInUrl(url, "saved", "1") ;
            UrlHelper.Redirect(url);           
        }             
    }


    /// <summary>
    /// Check out
    /// </summary>
    protected void btnCheckOut_Click(object sender, EventArgs e)
    {
        SetCurrentLayout(false);

        try
        {
            string layoutCodeName = ValidationHelper.GetString(selectLayout.Value, string.Empty);

            WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webPart.WebPartType);
            if (wpi != null)
            {
                WebPartLayoutInfo wpli = WebPartLayoutInfoProvider.GetWebPartLayoutInfo(wpi.WebPartName, layoutCodeName);
                if (wpli != null)
                {
                    SiteManagerFunctions.CheckOutWebPartLayout(wpli.WebPartLayoutID);
                }
            }
        }
        catch (Exception ex)
        {
            this.lblError.Text = ResHelper.GetString("WebPartLayout.ErrorCheckout") + ": " + ex.Message;
            this.lblError.Visible = true;
            return;
        }

        SetCheckPanel(null);
        // UrlHelper.Redirect(Request.Url.AbsoluteUri);
    }


    /// <summary>
    /// Undo check out
    /// </summary>
    protected void btnUndoCheckOut_Click(object sender, EventArgs e)
    {
        try
        {
            string layoutCodeName = ValidationHelper.GetString(selectLayout.Value, string.Empty);

            WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webPart.WebPartType);
            if (wpi != null)
            {
                WebPartLayoutInfo wpli = WebPartLayoutInfoProvider.GetWebPartLayoutInfo(wpi.WebPartName, layoutCodeName);
                if (wpli != null)
                {
                    SiteManagerFunctions.UndoCheckOutWebPartLayout(wpli.WebPartLayoutID);
                    etaCode.ReadOnly = false;
                    etaCode.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            this.lblError.Text = ResHelper.GetString("WebPartLayout.ErrorUndoCheckout") + ": " + ex.Message;
            this.lblError.Visible = true;
            return;
        }


        SetCheckPanel(null);
        // UrlHelper.Redirect(Request.Url.AbsoluteUri);
    }


    /// <summary>
    /// Check in
    /// </summary>
    protected void btnCheckIn_Click(object sender, EventArgs e)
    {
        try
        {
            string layoutCodeName = ValidationHelper.GetString(selectLayout.Value, string.Empty);

            WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webPart.WebPartType);
            if (wpi != null)
            {
                WebPartLayoutInfo wpli = WebPartLayoutInfoProvider.GetWebPartLayoutInfo(wpi.WebPartName, layoutCodeName);
                if (wpli != null)
                {
                    SiteManagerFunctions.CheckInWebPartLayout(wpli.WebPartLayoutID);
                    etaCode.ReadOnly = false;
                    etaCode.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            this.lblError.Text = ResHelper.GetString("WebPartLayout.ErrorCheckin") + ": " + ex.Message;
            this.lblError.Visible = true;
            return;
        }


        SetCheckPanel(null);
        // UrlHelper.Redirect(Request.Url.AbsoluteUri);
    }


    /// <summary>
    /// Sets check out/in/undo panel
    /// </summary>
    protected void SetCheckPanel(WebPartLayoutInfo mwpli)
    {
        WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webPart.WebPartType);
        WebPartLayoutInfo wpli = mwpli;
        if (wpi != null)
        {
            if (wpli == null)
            {
                wpli = WebPartLayoutInfoProvider.GetWebPartLayoutInfo(wpi.WebPartName, ValidationHelper.GetString(selectLayout.Value, string.Empty));
            }
        }

        if (wpli != null)
        {
            this.pnlCheckOutInfo.Visible = true;

            if (wpli.WebPartLayoutCheckedOutByUserID > 0)
            {
                etaCode.ReadOnly = true;

                string username = null;
                UserInfo ui = UserInfoProvider.GetUserInfo(wpli.WebPartLayoutCheckedOutByUserID);
                if (ui != null)
                {
                    username = HTMLHelper.HTMLEncode(ui.FullName);
                }

                plcCheckOut.Visible = false;

                // Checked out by current machine
                if (wpli.WebPartLayoutCheckedOutMachineName.ToLower() == HTTPHelper.MachineName.ToLower())
                {
                    this.plcCheckIn.Visible = true;

                    this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("WebPartEditLayoutEdit.CheckedOut"), Server.MapPath(wpli.WebPartLayoutCheckedOutFilename));
                }
                else
                {
                    this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("WebPartEditLayoutEdit.CheckedOutOnAnotherMachine"), wpli.WebPartLayoutCheckedOutMachineName, username);
                }

                if (CMSContext.CurrentUser.IsGlobalAdministrator)
                {
                    this.plcUndoCheckOut.Visible = true;
                }
            }
            else
            {
                wpi = WebPartInfoProvider.GetWebPartInfo(wpli.WebPartLayoutWebPartID);
                if (wpi != null)
                {
                    this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("WebPartEditLayoutEdit.CheckOutInfo"), Server.MapPath(WebPartLayoutInfoProvider.GetWebPartLayoutUrl(wpi.WebPartName, wpli.WebPartLayoutCodeName, null)));

                    this.plcCheckOut.Visible = true;
                    this.plcCheckIn.Visible = false;
                    this.plcUndoCheckOut.Visible = false;
                }
            }
        }
    }
}
