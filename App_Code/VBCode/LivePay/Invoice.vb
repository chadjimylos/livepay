﻿Imports Microsoft.VisualBasic
Imports System.Data

Namespace LivePay
    Public Class Invoice

        Private _CompanyName As String
        Public Property CompanyName() As String
            Get
                Return _CompanyName
            End Get
            Set(ByVal value As String)
                _CompanyName = value
            End Set
        End Property

        Private _Profession As String
        Public Property Profession() As String
            Get
                Return _Profession
            End Get
            Set(ByVal value As String)
                _Profession = value
            End Set
        End Property

        Private _Address As String
        Public Property Address() As String
            Get
                Return _Address
            End Get
            Set(ByVal value As String)
                _Address = value
            End Set
        End Property

        Private _PostalCode As String
        Public Property PostalCode() As String
            Get
                Return _PostalCode
            End Get
            Set(ByVal value As String)
                _PostalCode = value
            End Set
        End Property

        Private _City As String
        Public Property City() As String
            Get
                Return _City
            End Get
            Set(ByVal value As String)
                _City = value
            End Set
        End Property

        Private _VATNumber As String
        Public Property VATNumber() As String
            Get
                Return _VATNumber
            End Get
            Set(ByVal value As String)
                _VATNumber = value
            End Set
        End Property

        Private _TaxOffice As String
        Public Property TaxOffice() As String
            Get
                Return _TaxOffice
            End Get
            Set(ByVal value As String)
                _TaxOffice = value
            End Set
        End Property

        Public Sub New()
        End Sub

        Shared Function GetInvoice(ByVal TransactionID As Integer) As Invoice

            Dim dt As DataTable = DBConnection.GetInvoice(TransactionID)

            If (Not dt Is Nothing) AndAlso (dt.Rows.Count > 0) Then
	        Dim inv As New Invoice

                inv.CompanyName = dt.Rows(0)("Inv_CompanyName").ToString()
                inv.Profession = dt.Rows(0)("Inv_Occupation").ToString()
                inv.Address = dt.Rows(0)("Inv_Addrees").ToString()
                inv.PostalCode = dt.Rows(0)("Inv_TK").ToString()
                inv.City = dt.Rows(0)("Inv_City").ToString()
                inv.VATNumber = dt.Rows(0)("Inv_AFM").ToString()
                inv.TaxOffice = dt.Rows(0)("TaxOffice").ToString()

		Return inv
            End If

            Return Nothing
        End Function
    End Class
End Namespace
