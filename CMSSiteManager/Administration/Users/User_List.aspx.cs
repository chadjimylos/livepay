using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.EventLog;
using System.Windows.Forms;

public partial class CMSSiteManager_Administration_Users_User_List : CMSUsersPage
{
    protected int siteId = 0;


    protected override void OnInit(EventArgs e)
    {
        siteId = GetSiteID(Request.QueryString["siteid"]);
        userFilterElem.SiteID = siteId;
        base.OnInit(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        btn_CheckCards.Click += new EventHandler(this.btn_CheckCards_Click);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ManageRoles", ScriptHelper.GetScript(
        "function manageRoles(userId) {" +
        "    modalDialog('User_ManageRoles.aspx?userId=' + userId, 'ManageUserRoles', 650, 400);" +
        "}"));

        // Page title initialization
        CurrentMaster.HeaderActions.HelpTopicName = "users_list";

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Administration-User_List.NewUser");
        actions[0, 2] = null;
        actions[0, 3] = siteId > 0 ? ResolveUrl("user_new.aspx?siteid=" + siteId) : ResolveUrl("user_new.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/CMS_User/add.png");
        CurrentMaster.HeaderActions.Actions = actions;


        gridElem.Query = "cms.user.selectallview";
        gridElem.OnBeforeDataReload += new OnBeforeDataReload(gridElem_OnBeforeDataReload);

        // Unigrid initialization
        gridElem.OnAction += gridElem_OnAction;
        gridElem.OnExternalDataBound += gridElem_OnExternalDataBound;
    }


    /// <summary>
    /// Handles setting the grid where condition before data binding
    /// </summary>    
    protected void gridElem_OnBeforeDataReload()
    {
        if (!RequestHelper.IsAsyncPostback())
        {
            gridElem.WhereCondition = userFilterElem.WhereCondition;
            gridElem.FilterIsSet = true;
        }
    }


    /// <summary>
    /// Handles Unigrid's OnExternalDataBound event.
    /// </summary>
    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        UserInfo ui = null;

        switch (sourceName.ToLower())
        {
            case "userenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);

            case "edit":
                ui = UserInfoProvider.GetUserInfo(ValidationHelper.GetInteger(((DataRowView)((GridViewRow)parameter).DataItem).Row["UserID"], 0));
                if (ui != null)
                {
                    if ((!currentUser.IsGlobalAdministrator || (currentUser.IsGlobalAdministrator && currentUser.UserSiteManagerDisabled)) && ui.UserIsGlobalAdministrator && (ui.UserID != currentUser.UserID))
                    {
                        ImageButton button = ((ImageButton)sender);
                        button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Editdisabled.png");
                        button.Enabled = false;
                    }
                }
                break;

            case "delete":
                ui = UserInfoProvider.GetUserInfo(ValidationHelper.GetInteger(((DataRowView)((GridViewRow)parameter).DataItem).Row["UserID"], 0));
                if (ui != null)
                {
                    if ((!currentUser.IsGlobalAdministrator || (currentUser.IsGlobalAdministrator && currentUser.UserSiteManagerDisabled)) && ui.UserIsGlobalAdministrator)
                    {
                        ImageButton button = ((ImageButton)sender);
                        button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Deletedisabled.png");
                        button.Enabled = false;
                    }
                }
                break;

            case "roles":
                ui = UserInfoProvider.GetUserInfo(ValidationHelper.GetInteger(((DataRowView)((GridViewRow)parameter).DataItem).Row["UserID"], 0));
                if (ui != null)
                {
                    if ((!currentUser.IsGlobalAdministrator || (currentUser.IsGlobalAdministrator && currentUser.UserSiteManagerDisabled)) && ui.UserIsGlobalAdministrator && (ui.UserID != currentUser.UserID))
                    {
                        ImageButton button = ((ImageButton)sender);
                        button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Managerolesdisabled.png");
                        button.Enabled = false;
                    }
                }
                break;

            case "formattedusername":
                return HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(Convert.ToString(parameter)));

        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that threw event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("User_Edit_Frameset.aspx?userid=" + actionArgument + "&siteid=" + siteId);

        }
        else if (actionName == "delete")
        {
            // Check "modify" permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify"))
            {
                RedirectToAccessDenied("CMS.Users", "Modify");
            }

            try
            {
                int userId = Convert.ToInt32(actionArgument);
                UserInfo delUser = UserInfoProvider.GetUserInfo(userId);
                CurrentUserInfo currentUser = CMSContext.CurrentUser;
               
                if (delUser != null)
                {
                    // Global administrator account could be deleted only by global administrator
                    if (delUser.IsGlobalAdministrator && (!currentUser.IsGlobalAdministrator || (currentUser.IsGlobalAdministrator && currentUser.UserSiteManagerDisabled)))
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("Administration-User_List.ErrorNoGlobalAdmin");
                        return;
                    }

                    // It is not possible to delete own user account
                    if (userId == currentUser.UserID)
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("Administration-User_List.ErrorOwnAccount");
                        return;
                    }
                    DataTable dt = DBConnection.GetUserCards(userId);
                    EventLogProvider eventLog = new EventLogProvider();
                    if (dt.Rows.Count > 0)
                    {
                        // Log the event
                        eventLog.LogEvent("I", DateTime.Now, "User", "Prospathia diagrafis xristi User! ( " + delUser.UserName + " ) exei apothikevmenes kartes", CMSContext.CurrentSite.SiteID);
                        //ScriptHelper.RegisterStartupScript(this, GetType(), "DeleteError", ScriptHelper.GetScript("alert('���� ������������� ������');"));
                        //ScriptHelper.RegisterStartupScript(this, GetType(), "xaxaxa", ScriptHelper.GetScript("confirm('� ������� ���� ������������� ������. ����� �������� ��� ������ �� ����������?')"));
                        //ScriptHelper.RegisterStartupScript(this, GetType(), "xaxaxa", ScriptHelper.GetScript("if (confirm('� ������� ���� ������������� ������. ����� �������� ��� ������ �� ����������?')) {alert('true');}else {alert('false');return false;}"));

                        ScriptHelper.RegisterStartupScript(this, GetType(), "xaxaxa", ScriptHelper.GetScript("CheckUserResponse();"));
                        lbl_selectedUser.Text = userId.ToString();
                    
                    }
                    else
                    {
                        SessionManager.RemoveUser(userId);
                        //SessionTrack.RemoveUser(userId);
                        UserInfoProvider.DeleteUser(delUser.UserName);

                        // Log the event
                        eventLog.LogEvent("I", DateTime.Now, "User", "Delete User! ( " + delUser.UserName + " )", CMSContext.CurrentSite.SiteID);
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
            }
        }
    }

    void btn_CheckCards_Click(Object sender, EventArgs e)
    {
        int UserID = Convert.ToInt32(lbl_selectedUser.Text);
        DataTable dt = DBConnection.GetUserCards(UserID);

        if (dt.Rows.Count == 0)
        {
            //den exei kartes
        }
        else
        {
            //delete kartes
            foreach (DataRow dtRow in dt.Rows)
            {
                if (!String.IsNullOrEmpty(dtRow["CardId"].ToString()))
                {
                    DBConnection.DeleteUserCards(UserID, dtRow["CardId"].ToString());
                }
            }
            //delete kartes
            
            //delete user
            UserInfo delUser = UserInfoProvider.GetUserInfo(UserID);
            EventLogProvider eventLog = new EventLogProvider();
            SessionManager.RemoveUser(UserID);
            UserInfoProvider.DeleteUser(delUser.UserName);
            eventLog.LogEvent("I", DateTime.Now, "User", "Delete User! ( " + delUser.UserName + " )", CMSContext.CurrentSite.SiteID);
            //delete user
            gridElem.ReloadData();
        }
    }
}
