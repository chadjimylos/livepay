using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_SystemTables_SystemTable_List : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("SystemTable_List.Title");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_SystemTable/object.png");
        this.CurrentMaster.Title.HelpTopicName = "system_tables_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        UniGridSysTables.OnAction += new OnActionEventHandler(UniGridSysTables_OnAction);
        UniGridSysTables.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that threw event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGridSysTables_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "edit":
                UrlHelper.Redirect("SystemTable_Frameset.aspx?classid=" + Convert.ToString(actionArgument));
                break;
        }
    }
}
