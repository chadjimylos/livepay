using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SiteProvider;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSDesk_MyDesk_Recent_Recent : CMSMyDeskPage
{
    #region "Private variables"

    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;
    private TimeZoneInfo usedTimeZone = null;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check UIProfile
        if ((CMSContext.CurrentUser == null) || (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.MyDesk", "RecentDocs")))
        {
            RedirectToUINotAvailable();
        }

        // Setup page title text and image
        CurrentMaster.Title.TitleText = ResHelper.GetString("MyDesk.RecentTitle");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_MyDesk/RecentDocuments/module.png");

        CurrentMaster.Title.HelpTopicName = "recent_documents";
        CurrentMaster.Title.HelpName = "helpTopic";


        object[,] parameters = new object[2, 3];
        parameters[0, 0] = "@UserID";
        parameters[0, 1] = CMSContext.CurrentUser.UserID;
        parameters[1, 0] = "@SiteID";
        parameters[1, 1] = CMSContext.CurrentSite.SiteID;

        // Initialize unigrid
        ugRecentDocs.QueryParameters = parameters;
        ugRecentDocs.OnExternalDataBound += ugRecentDocs_OnExternalDataBound;
        ugRecentDocs.HideControlForZeroRows = false;
        ugRecentDocs.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// External data binding handler
    /// </summary>
    protected object ugRecentDocs_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        int nodeId = 0;
        string culture = string.Empty;
        DataRowView data = null;
        sourceName = sourceName.ToLower();
        switch (sourceName)
        {
            case "classname":
                data = (DataRowView)parameter;
                nodeId = ValidationHelper.GetInteger(data["NodeID"], 0);
                culture = ValidationHelper.GetString(data["DocumentCulture"], string.Empty);
                string className = ValidationHelper.GetString(data["ClassName"], string.Empty);
                string imageUrl = GetDocumentTypeIconUrl(className);
                string toReturn = "<img src=\"" + imageUrl + "\" border=\"0\"/>";
                // Check permissions
                if (IsUserAuthorizedPerContent())
                {
                    toReturn = "<a href=\"javascript: SelectItem(" + nodeId + ", '" + culture + "');\">" + toReturn + "</a>";
                }
                return toReturn;

            case "documentname":
                data = (DataRowView)parameter;
                string name = ValidationHelper.GetString(data["DocumentName"], string.Empty);
                nodeId = ValidationHelper.GetInteger(data["NodeID"], 0);
                culture = ValidationHelper.GetString(data["DocumentCulture"], string.Empty);

                if (name == string.Empty)
                {
                    name = "-";
                }
                string result = string.Empty;
                if (IsUserAuthorizedPerContent())
                {
                    result = "<a href=\"javascript: SelectItem(" + nodeId + ", '" + culture + "');\">" + HTMLHelper.HTMLEncode(TextHelper.LimitLength(name, 50)) + "</a>";
                    bool isLink = (data["NodeLinkedNodeID"] != DBNull.Value);
                    if (isLink)
                    {
                        result += UIHelper.GetDocumentMarkImage(this, DocumentMarkEnum.Link);
                    }
                }
                else
                {
                    result = "<span>" + HTMLHelper.HTMLEncode(TextHelper.LimitLength(name, 50)) + "</span>";
                }
                return result;

            case "documentnametooltip":
                data = (DataRowView)parameter;
                return UniGridFunctions.DocumentNameTooltip(data);

            case "stepdisplayname":
                string stepName = ValidationHelper.GetString(parameter, string.Empty);
                if (stepName == string.Empty)
                {
                    stepName = "-";
                }
                return stepName;

            case "culture":
                DataRowView drv = (DataRowView)parameter;

                // Add icon
                return UniGridFunctions.DocumentCultureFlag(drv, this.Page);

            case "modifiedwhen":
            case "modifiedwhentooltip":
                if (string.IsNullOrEmpty(parameter.ToString()))
                {
                    return "";
                }
                else
                {
                    if (currentUserInfo == null)
                    {
                        currentUserInfo = CMSContext.CurrentUser;
                    }
                    if (currentSiteInfo == null)
                    {
                        currentSiteInfo = CMSContext.CurrentSite;
                    }

                    if (sourceName == "modifiedwhen")
                    {
                        return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME),
                            currentUserInfo, currentSiteInfo, out usedTimeZone);
                    }
                    else
                    {
                        if (TimeZoneHelper.TimeZonesEnabled() && (usedTimeZone == null))
                        {
                            TimeZoneHelper.GetCurrentTimeZoneDateTimeString(ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME),
                                currentUserInfo, currentSiteInfo, out usedTimeZone);
                        }
                        return TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone);
                    }

                }
        }

        return parameter;
    }
}
