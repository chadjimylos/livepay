﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.SettingsProvider
Imports CMS.SiteProvider
Imports CMS.DataEngine

Partial Class CMSTemplates_LivePay_UCNewPay
    Inherits CMSUserControl

    Public searchText As String

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        searchText = ResHelper.LocalizeString("{$=Πληκτρολογήστε Επωνυμία Επιχείρησης|en-us=Πληκτρολογήστε Επωνυμία Επιχείρησης en$}")
        Me.txtSearch.Text = searchText
        Me.txtSearch.Attributes.Add("onfocus", "if(this.value=='" & searchText & "'){this.value='';}")
        Me.txtSearch.Attributes.Add("onBlur", "if (this.value == '') {this.value = '" & searchText & "';}")

    End Sub


    'Public Sub Bind()
    '    GetItemsCustomTable("LivePay.Merchant", "MerchantID", "CompanyName")
    'End Sub

    'Private Sub GetItemsCustomTable(ByVal CustomTable As String, ByVal ColumeValue As String, ByVal ColumeText As String)
    '    Dim key As String = Replace(txtSearch.Text, "'", String.Empty)
    '    key = Replace(key, """", String.Empty)
    '    If key <> String.Empty Then
    '        lnkViewMoreResults.HRef = String.Concat("~/Search.aspx?searchtext=", key, "")
    '        ' Get data class using custom table name
    '        Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass(CustomTable)
    '        If customTableClassInfo Is Nothing Then
    '            Throw New Exception("Given custom table does not exist.")
    '        End If
    '        ' Initialize custom table item provider with current user info and general connection
    '        Dim ctiProvider As New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())
    '        Dim where As String = String.Concat("CompanyName", " like '", key, "%'")
    '        ' Get custom table items
    '        Dim jsname As Guid = New Guid

    '        Dim dsItems As DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, where, ColumeText, 3)
    '        ' Check if DataSet is not empty
    '        ' If Not DataHelper.DataSourceIsEmpty(dsItems) Then
    '        rptQuickSrch.DataSource = dsItems
    '        rptQuickSrch.DataBind()
    '        txtSearch.Focus()
    '        If dsItems.Tables(0).Rows.Count > 0 Then
    '            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsname.ToString, String.Concat("ShowHideQuickRes(true,", dsItems.Tables(0).Rows.Count, ");SetFocus()"), True)
    '        Else
    '            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsname.ToString, "ShowHideQuickRes(false,0);SetFocus()", True)
    '        End If
    '    End If
    'End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim key As String = Replace(txtSearch.Text, "'", String.Empty)
        key = Replace(key, """", String.Empty)
        If key <> String.Empty Then
            Response.Redirect(String.Concat("~/Search.aspx?searchtext=", key, ""))
        End If
    End Sub

    'Protected Sub hidbtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hidBtn.Click
    '    Bind()
    'End Sub

End Class
