﻿using System;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.PortalEngine;

public partial class CMSWebParts_GoogleServices_GoogleAnalytics : CMSAbstractWebPart
{
    #region "Properties"

    /// <summary>
    /// Get or set tracking code for google analytics
    /// </summary>
    public string TrackingCode
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("TrackingCode"), "");
        }
        set
        {
            this.SetValue("TrackingCode", value);
        }
    }


    /// <summary>
    /// Get or set type of analytics tracking
    /// Single domain – value 0
    /// One domain with multiple subdomains – value 1
    /// Multiple top-level domains – value 2
    /// </summary>
    public int TrackingType
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("TrackingType"), 0);
        }
        set
        {
            this.SetValue("TrackingType", value);
        }
    }

    #endregion


    #region "Webpart events"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (!this.StopProcessing)
        {
            if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
            {
                // Register necessary google scripts
                string googleScript = "var gaJsHost = ((\"https:\" == document.location.protocol) ? \"https://ssl.\" : \"http://www.\");\n" +
                                      "document.write(unescape(\"%3Cscript src='\" + gaJsHost + \"google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E\"));";
                // Register final script to a page
                ScriptHelper.RegisterStartupScript(this, typeof(string), "GoogleAnalyticsScript", ScriptHelper.GetScript(googleScript));

                // Create custom tracking script
                string trackingScript = "if ( (parent == null) || (!parent.IsCMSDesk) ) {\ntry { \n" +
                                        "var pageTracker = _gat._getTracker('" + this.TrackingCode + "');\n";

                switch (this.TrackingType)
                {
                    // One domain with multiple subdomains
                    case 1:
                        string[] domainParts = UrlHelper.GetCurrentDomain().Split('.');
                        string mainDomain = String.Empty;

                        // Check if the domain name consists of more than 3 domain levels
                        int moreDomains = 0;
                        if (domainParts.Length > 3)
                        {
                            moreDomains = domainParts.Length - 3;
                        }

                        for (int i = domainParts.Length - 1; i >= 0; i--)
                        {         
                            // Main domain consist of generic and 2nd level domain
                            if (i > (domainParts.Length - 3 - moreDomains))
                            {
                                mainDomain = "." + domainParts[i] + mainDomain;
                            }
                            else
                            {
                                break;
                            }
                        }

                        trackingScript += "pageTracker._setDomainName('" + mainDomain + "');\n";
                        break;

                    // Multiple top-level domains 
                    case 2:
                        trackingScript += "pageTracker._setDomainName(\"none\");\n" +
                                          "pageTracker._setAllowLinker(true);\n";
                        break;

                    // Single domain
                    default:
                        break;
                }

                trackingScript += "pageTracker._trackPageview();\n" +
                                  "} catch(err) {}\n}";

                // Register final script to a page
                ScriptHelper.RegisterStartupScript(this, typeof(string), "GoogleAnalyticsTracking" + "_" + this.ClientID, ScriptHelper.GetScript(trackingScript));
            }
        }
    }

    #endregion
}
