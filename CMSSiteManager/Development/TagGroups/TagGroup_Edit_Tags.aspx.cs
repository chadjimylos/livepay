using System;
using System.Data;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_TagGroups_TagGroup_Edit_Tags : SiteManagerPage
{
    #region "Variables"

    private int groupId = 0;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize the grid view                
        gridTags.GridName = "~/CMSSiteManager/Development/TagGroups/TagGroup_Edit_Tags.xml";
        gridTags.ZeroRowsText = ResHelper.GetString("tags.taggroup_edit_tags.notags");
        gridTags.OnExternalDataBound += gridTags_OnExternalDataBound;

        // Look for group ID in the query string
        groupId = QueryHelper.GetInteger("groupid", 0);

        gridTags.WhereCondition = "(TagGroupID = " + groupId + ")";
    }

    #endregion


    #region "UniGrid events"

    protected object gridTags_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "view":
                string result = "<a href=\"Tags_Documents.aspx?groupid=" + groupId + "&siteid=" + QueryHelper.GetInteger("siteid", 0) + "&tagid=" + parameter + "\" >";
                result += "<img src=\"" + GetImageUrl("Design/Controls/UniGrid/Actions/View.png") + "\"";
                result += "alt=\"" + ResHelper.GetString("general.view") + "\" border=\"0\" /></a>";

                return result;

            case "tagname":
                return HTMLHelper.HTMLEncode(Convert.ToString(parameter));
        }
        return parameter;
    }

    #endregion
}
