<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Role.ascx.cs" Inherits="CMSSiteManager_Administration_Roles_Controls_Role" %>
<%@ Register Src="~/CMSSiteManager/Administration/Roles/Controls/RoleEdit.ascx" TagName="RoleEdit"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSSiteManager/Administration/Roles/Controls/RoleUsers.ascx"
    TagName="RoleUsers" TagPrefix="cms" %>
    <asp:Literal runat="server" ID="ltlScript"></asp:Literal>
<asp:Panel runat="server" ID="pnlBody" CssClass="PollEdit">
    <div class="TabsHeader">
        <cms:BasicTabControl ID="tabMenu" runat="server" />
    </div>
    <asp:Panel runat="server" ID="pnlRoleLinks" CssClass="TabsContent">
        <div>
            <cms:RoleEdit ID="RoleEdit" runat="server" />
            <cms:RoleUsers ID="RoleUsers" runat="server" />
        </div>
    </asp:Panel>
<cms:CMSButton ID="btnUpdate" runat="server" Text="Button" CssClass="HiddenButton" OnClick="btnUpdate_Click" />
</asp:Panel>
