using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_WebParts_Development_WebPart_Edit_Layout_New : SiteManagerPage
{
    // Path to web parts
    const string WEBPART_PATH = "~/CMSWebParts/";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Init GUI
        lblDisplayName.Text = ResHelper.GetString("WebPartEditLayoutNew.lblDisplayName");
        lblCodeName.Text = ResHelper.GetString("WebPartEditLayoutNew.lblCodeName");
        lblCode.Text = ResHelper.GetString("WebPartEditLayoutNew.lblCode");
        lblDescription.Text = ResHelper.GetString("WebPartEditLayoutNew.lblDescription");
        btnOk.Text = ResHelper.GetString("general.ok");

        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");
         
        // Get web part info
        int webPartId = ValidationHelper.GetInteger(Request.QueryString["webpartId"], 0);
        WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webPartId);

        // Check webpartid
        if (wpi == null)
        {
            lblError.Text = ResHelper.GetString("WebPartEditLayoutNew.InvalidWebPartID");
            lblError.Visible = true;
            return;
        }

        if (!RequestHelper.IsPostBack())
        {
            // Get default layout code
            string srcFile = this.Server.MapPath(WEBPART_PATH + wpi.WebPartFileName);
            if (System.IO.File.Exists(srcFile))
            {
                tbCode.Text = System.IO.File.ReadAllText(srcFile);
            }
        }
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Remove "." due to virtual path provider replacement
        txtCodeName.Text = txtCodeName.Text.Replace(".", "");

        txtDisplayName.Text = txtDisplayName.Text.Trim();
        txtCodeName.Text = txtCodeName.Text.Trim();

        string errorMessage = new Validator().NotEmpty(txtCodeName.Text, rfvCodeName.ErrorMessage)
        .NotEmpty(txtDisplayName.Text, rfvDisplayName.ErrorMessage)
        .IsCodeName(txtCodeName.Text, ResHelper.GetString("general.invalidcodename"))
        .Result;

        if (errorMessage != String.Empty)
        {
            lblError.Text = errorMessage;
            lblError.Visible = true;
            return;
        }

        // Check web part id
        int webPartId = ValidationHelper.GetInteger(Request.QueryString["webpartId"], 0);
        WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webPartId);
        if (wpi == null) {
            lblError.Text = ResHelper.GetString("WebPartEditLayoutNew.InvalidWebPartID");
            lblError.Visible = true;
            return;
        }

        // Check web part layout code name
        WebPartLayoutInfo tempwpli = WebPartLayoutInfoProvider.GetWebPartLayoutInfo(wpi.WebPartName, txtCodeName.Text);
        if (tempwpli != null)
        {
            lblError.Text = ResHelper.GetString("WebPartEditLayoutNew.CodeNameAlreadyExist");
            lblError.Visible = true;
            return;
        }


        // Create and fill info structure
        WebPartLayoutInfo wpli = new WebPartLayoutInfo();

        wpli.WebPartLayoutID = 0;
        wpli.WebPartLayoutVersionGUID = Guid.NewGuid().ToString();
        wpli.WebPartLayoutCodeName = txtCodeName.Text;
        wpli.WebPartLayoutDisplayName = txtDisplayName.Text;
        wpli.WebPartLayoutCode = tbCode.Text;
        wpli.WebPartLayoutDescription = txtDescription.Text;
        wpli.WebPartLayoutWebPartID = webPartId;

        WebPartLayoutInfoProvider.SetWebPartLayoutInfo(wpli);
        UrlHelper.Redirect("WebPart_Edit_Layout_Edit.aspx?layoutID=" + wpli.WebPartLayoutID
            + "&webpartID=" + webPartId);
    }
}
