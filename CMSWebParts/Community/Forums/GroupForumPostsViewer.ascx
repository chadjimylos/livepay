<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Community/Forums/GroupForumPostsViewer.ascx.cs"
    Inherits="CMSWebParts_Community_Forums_GroupForumPostsViewer" %>
<%@ Register TagPrefix="cms" Namespace="CMS.Forums" Assembly="CMS.Forums" %>
<cms:BasicRepeater runat="server" ID="repLatestPosts" />
<cms:ForumPostsDataSource runat="server" ID="forumDataSource" />
<div class="Pager">
    <cms:UniPager ID="pagerElem" runat="server" />
</div>
