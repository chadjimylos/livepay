using System;

using CMS.GlobalHelper;
using CMS.Blogs;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Blogs_Controls_Comment_Edit : CMSModalPage
{
    protected int commentId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        commentId = QueryHelper.GetInteger("commentID", 0);

        // Get comment info
        BlogCommentInfo commentObj = BlogCommentInfoProvider.GetBlogCommentInfo(commentId);
        if (commentObj != null)
        {
            // Get parent blog            
            TreeNode blogNode = BlogHelper.GetParentBlog(commentObj.CommentPostDocumentID, false);

            bool isAuthorized = BlogHelper.IsUserAuthorizedToManageComments(blogNode);

            // Check "manage" permission
            if (!isAuthorized)
            {
                RedirectToAccessDenied("cms.blog", "Manage");
            }

            ctrlCommentEdit.CommentId = commentId;
        }

        btnOk.Click += btnOk_Click;
        btnOk.Text = ResHelper.GetString("General.OK");

        ctrlCommentEdit.IsLiveSite = false;
        ctrlCommentEdit.OnAfterCommentSaved += new OnAfterCommentSavedEventHandler(ctrlCommentEdit_OnAfterCommentSaved);

        CurrentMaster.Title.TitleText = ResHelper.GetString("Blog.CommentEdit.Title");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Blog_Comment/object.png");
    }


    protected void btnOk_Click(object sender, EventArgs e)
    {
        ctrlCommentEdit.PerformAction();
    }


    protected void ctrlCommentEdit_OnAfterCommentSaved(BlogCommentInfo commentObj)
    {
        // Get filter parameters
        string filterParams = "?user=" + QueryHelper.GetString("user", "") + "&comment=" + QueryHelper.GetString("comment", "") +
                    "&approved=" + QueryHelper.GetString("approved", "") + "&isspam=" + QueryHelper.GetString("isspam", "");

        ltlScript.Text = ScriptHelper.GetScript("wopener.RefreshPage('" + filterParams + "');window.close();");
    }
}
