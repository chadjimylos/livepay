<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Customer_Edit_Credit_List.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Ecommerce_Tools_Customers_Customer_Edit_Credit_List"
    Theme="Default" Title="Customer credit events" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="conten" runat="server">
    <div style="padding-bottom: 5px;">
        <asp:Label ID="lblCredit" runat="server" EnableViewState="false" />
        <asp:Label ID="lblCreditValue" runat="server" EnableViewState="false" />
    </div>
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Customer_Edit_Credit_List.xml"
        IsLiveSite="false" Columns="EventID,EventDate,EventName,EventCreditChange,EventDescription" />
</asp:Content>
