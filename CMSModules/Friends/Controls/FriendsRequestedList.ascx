<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FriendsRequestedList.ascx.cs"
    Inherits="CMSModules_Friends_Controls_FriendsRequestedList" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<%@ Register Src="~/CMSModules/Friends/Controls/RequestFriendship.ascx" TagName="RequestFriendship"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Friends/Controls/FriendsSearchBox.ascx" TagName="FriendsSearchBox"
    TagPrefix="cms" %>
<asp:Panel ID="pnlBody" runat="server" CssClass="Panel" EnableViewState="true">
    <div>
        <cms:LocalizedLinkButton ID="btnRemoveSelected" OnClick="btnRemoveSelected_Click"
            runat="server" ResourceString="friends.friendremoveall" EnableViewState="false"
            CssClass="ContentLinkButton" />
    </div>
    <br />
    <cms:FriendsSearchBox ID="searchElem" runat="server" />
    <cms:UniGrid runat="server" ID="gridElem" GridName="~/CMSModules/Friends/Controls/FriendsRequestedList.xml" />
    <asp:HiddenField runat="server" ID="hdnRefresh" />
</asp:Panel>
