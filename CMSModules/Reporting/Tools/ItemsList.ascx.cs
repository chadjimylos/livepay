using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Reporting;
using CMS.UIControls;


/// <summary>
/// Report items enumeration
/// </summary>
public enum ReportItemTypes
{
    ///<summary>Graph</summary>
    Graph,
    ///<summary>Table</summary>
    Table,
    ///<summary>Value</summary>
    Value
}


public partial class CMSModules_Reporting_Tools_ItemsList : CMSUserControl
{
    private string mEditUrl = "";
    private int mReportId = 0;
    private ReportInfo mReport = null;
    private string mReportName = "";
    private ReportItemTypes mItemType = ReportItemTypes.Graph;


    #region "Public properties"

    /// <summary>
    /// Url of the edit page
    /// </summary>
    public string EditUrl
    {
        get
        {
            return mEditUrl;
        }
        set
        {
            mEditUrl = value;
        }
    }


    /// <summary>
    /// Gets or sets report id, which is used for loading the report if Report property is null
    /// </summary>
    public int ReportId
    {
        get
        {
            return mReportId;
        }
        set
        {
            mReportId = value;
        }
    }


    /// <summary>
    /// Gets or sets the report object to load the items
    /// </summary>
    public ReportInfo Report
    {
        get
        {
            return mReport;
        }
        set
        {
            mReport = value;
        }
    }


    /// <summary>
    /// Item type
    /// </summary>
    public ReportItemTypes ItemType
    {
        get
        {
            return mItemType;
        }
        set
        {
            mItemType = value;
        }
    }

    #endregion


    /// <summary>
    /// Page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'Read' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Read"))
        {
            CMSReportingPage.RedirectToCMSDeskAccessDenied("cms.reporting", "Read");
        }
    }


    /// <summary>
    /// Pre render
    /// </summary>
    protected void Page_PreRender(object sender, EventArgs e)
    {
        LoadData();
        Initialize();
    }


    /// <summary>
    /// Set buttons actions
    /// </summary>
    protected void Initialize()
    {
        // Return selected item in list
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "GetSelectedItem" + mItemType.ToString(), ScriptHelper.GetScript(
            "function getSelectedItem_" + mItemType.ToString() + "() { \n" +
            "   if (document.getElementById('" + this.lstItems.ClientID + "') != null) { \n" +
            "      return document.getElementById('" + this.lstItems.ClientID + "').value; " +
            "   } \n" +
            "   return 0; } \n\n" +
            "function DeleteItem_" + mItemType.ToString() + "() { \n" +
            "   if ((document.getElementById('" + this.lstItems.ClientID + "') != null) && (document.getElementById('" + this.lstItems.ClientID + "').value != '')) { \n" +
            "       if (confirm(" + ScriptHelper.GetString(ResHelper.GetString("general.confirmdelete")) + ")) { \n" +
            "           document.getElementById('" + this.hdnItemId.ClientID + "').value = getSelectedItem_" + mItemType.ToString() + "();  " + this.Page.ClientScript.GetPostBackEventReference(btnHdnDelete, null) + " } \n" +
            "   } else { alert('" + ResHelper.GetString("Reporting_General.SelectObjectFirst") + "'); } \n" +
            "} \n\n" +
            "function InserMacro_" + mItemType.ToString() + "() { \n" +
            "   if ((document.getElementById('" + this.lstItems.ClientID + "') != null) && (document.getElementById('" + this.lstItems.ClientID + "').value != '')) { \n" +
            "       InsertHTML('%%control:Report" + ItemType.ToString() + "?" + mReportName + ".' + getSelectedItem_" + mItemType.ToString() + "() + '%%'); \n" +
            "   } else { alert('" + ResHelper.GetString("Reporting_General.SelectObjectFirst") + "'); } \n" +
            "} \n"
            ));

        string modalHeight = "570";
        string modalWidth = "800";
        switch (mItemType)
        {
            case ReportItemTypes.Graph:
                modalWidth = "1050";
                modalHeight = "760";
                break;

            case ReportItemTypes.Table:
                modalHeight = "610";
                break;
        }

        if (this.Report != null)
        {
            btnAdd.OnClientClick = "modalDialog('" + ResolveUrl(mEditUrl) + "?reportId=" + Report.ReportID + "','ReportItemEdit'," + modalWidth + "," + modalHeight + ");return false;";
            btnEdit.OnClientClick = "if (getSelectedItem_" + mItemType.ToString() + "() != '') { modalDialog('" + ResolveUrl(mEditUrl) + "?reportId=" + Report.ReportID + "&itemName='+ getSelectedItem_" + mItemType.ToString() + "(),'ReportItemEdit'," + modalWidth + "," + modalHeight + "); } else { alert('" + ResHelper.GetString("Reporting_General.SelectObjectFirst") + "');} return false;";
            btnDelete.OnClientClick = "DeleteItem_" + mItemType.ToString() + "(); return false;";
            btnInsert.OnClientClick = "InserMacro_" + mItemType.ToString() + "(); return false;";
        }
    }


    /// <summary>
    /// Load data
    /// </summary>
    public void LoadData()
    {
        ReportInfo ri = null;

        // Try to load report 
        if (this.Report == null)
        {
            ri = ReportInfoProvider.GetReportInfo(this.ReportId);
        }
        else
        {
            ri = this.Report;
        }

        if (ri != null)
        {
            mReportName = ri.ReportName;


            DataSet ds = null;
            string selected = lstItems.SelectedValue;
            lstItems.Items.Clear();

            if (mItemType == ReportItemTypes.Graph)
            {
                ds = ReportGraphInfoProvider.GetGraphs("GraphReportID = " + ri.ReportID, "GraphDisplayName", 0, "GraphName, GraphDisplayName");
                lstItems.DataTextField = "GraphDisplayName";
                lstItems.DataValueField = "GraphName";
            }
            else if (mItemType == ReportItemTypes.Table)
            {
                ds = ReportTableInfoProvider.GetTables("TableReportID = " + ri.ReportID, "TableDisplayName", 0, "TableName, TableDisplayName");
                lstItems.DataTextField = "TableDisplayName";
                lstItems.DataValueField = "TableName";
            }
            else if (mItemType == ReportItemTypes.Value)
            {
                ds = ReportValueInfoProvider.GetValues("ValueReportID = " + ri.ReportID, "ValueDisplayName", 0, "ValueName, ValueDisplayName");
                lstItems.DataTextField = "ValueDisplayName";
                lstItems.DataValueField = "ValueName";
            }

            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                lstItems.DataSource = ds;
                lstItems.DataBind();


                if (!RequestHelper.IsPostBack())
                {
                    lstItems.SelectedIndex = 0;
                }
                else
                {
                    try
                    {
                        lstItems.SelectedValue = selected;
                    }
                    catch
                    {
                    }
                }
            }
        }
    }


    protected void btnHdnDelete_Click(object sender, EventArgs e)
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
        {
            CMSReportingPage.RedirectToCMSDeskAccessDenied("cms.reporting", "Modify");
        }

        string itemName = "";

        if (hdnItemId.Value != "")
        {
            itemName = mReportName + "." + ValidationHelper.GetString(hdnItemId.Value, "");

            if (mItemType == ReportItemTypes.Graph)
            {
                ReportGraphInfo rgi = ReportGraphInfoProvider.GetReportGraphInfo(itemName);

                if (rgi != null)
                {
                    ReportGraphInfoProvider.DeleteReportGraphInfo(rgi.GraphID);
                }
            }
            else if (mItemType == ReportItemTypes.Table)
            {
                ReportTableInfo rti = ReportTableInfoProvider.GetReportTableInfo(itemName);

                if (rti != null)
                {
                    ReportTableInfoProvider.DeleteReportTableInfo(rti.TableID);
                }
            }
            else if (mItemType == ReportItemTypes.Value)
            {
                ReportValueInfo rvi = ReportValueInfoProvider.GetReportValueInfo(itemName);

                if (rvi != null)
                {
                    ReportValueInfoProvider.DeleteReportValueInfo(rvi.ValueID);
                }
            }
            this.LoadData();
        }
    }
}
