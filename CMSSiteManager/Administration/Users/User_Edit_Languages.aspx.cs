using System;
using System.Data;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSSiteManager_Administration_Users_User_Edit_Languages : CMSUsersPage
{
    #region "Protected variables"

    protected int userId = 0;
    protected int siteId = 0;

    protected SiteInfo si = null;
    protected UserInfo user = null;

    protected string currentValues = "";

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register script for pendingCallbacks repair
        ScriptHelper.FixPendingCallbacks(this.Page);

        // If languages on site not ok, redirect
        if (!CultureInfoProvider.LicenseVersionCheck())
        {            
            UrlHelper.Redirect(ResolveUrl("~/CMSMessages/LicenseLimit.aspx"));
        }

        userId = QueryHelper.GetInteger("userid", 0);
        user = UserInfoProvider.GetUserInfo(userId);
        if (user != null)
        {
            if (!CheckGlobalAdminEdit(user))
            {
                pnlLanguages.Visible = false;
                lblError.Text = ResHelper.GetString("Administration-User_List.ErrorGlobalAdmin");
                lblError.Visible = true;
                return;
            }

            if (!RequestHelper.IsPostBack())
            {
                // Initialize radio buttons
                radAllLanguages.Checked = !user.UserHasAllowedCultures;
                radSelectedLanguages.Checked = user.UserHasAllowedCultures;
            }

            // Set site selector
            siteSelector.DropDownSingleSelect.AutoPostBack = true;
            siteSelector.AllowAll = false;
            siteSelector.OnlyRunningSites = false;
            siteSelector.UserId = user.UserID;
            siteSelector.UniSelector.OnSelectionChanged += UniSelector_OnSelectionChanged;

            if (!RequestHelper.IsPostBack())
            {
                siteSelector.Value = CMSContext.CurrentSiteID;
            }
            siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);

            // Show site selection in CMSDesk only for global administrator
            if (siteId > 0)
            {
                si = SiteInfoProvider.GetSiteInfo(siteId);
                plcSite.Visible = CMSContext.CurrentUser.IsGlobalAdministrator && radSelectedLanguages.Checked;
            }
            else
            {
                plcSite.Visible = radSelectedLanguages.Checked;
            }

            // Set grid visibility
            plcCultures.Visible = radSelectedLanguages.Checked;

            // Load user cultures
            DataTable dt = UserCultureInfoProvider.GetUserCultures(userId, siteId, null, null);
            currentValues = String.Join(";", SqlHelperClass.GetStringValues(dt, "CultureID"));

            if (!IsPostBack)
            {
                uniSelector.Value = currentValues;
                uniSelector.Reload(true);
            }
        }

        uniSelector.WhereCondition = "CultureID IN (SELECT CultureID FROM CMS_SiteCulture WHERE SiteID = " + siteId + ")";
        uniSelector.OnSelectionChanged += uniSelector_OnSelectionChanged;

        radAllLanguages.CheckedChanged += radAllLanguages_CheckedChanged;
        radSelectedLanguages.CheckedChanged += radSelectedLanguages_CheckedChanged;
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (!RequestHelper.IsPostBack())
        {
            // Causes site selector to load data
            if (siteSelector.UniSelector.HasData)
            {
                // Get first user site id
                siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
                si = SiteInfoProvider.GetSiteInfo(siteId);
            }
            else
            {
                pnlLanguages.Visible = false;
                plcSite.Visible = false;
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Administration-User_Edit_Roles.UserWithNoSites");
            }
        }

        base.OnPreRender(e);
    }

    #endregion


    #region "Control events"

    protected void uniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        bool reloadNeeded = !CheckModifyPermissions();

        if (!reloadNeeded)
        {
            bool invalidateUser = false;

            // Remove old items
            string newValues = ValidationHelper.GetString(uniSelector.Value, null);
            string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
            if (!String.IsNullOrEmpty(items))
            {
                string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (newItems != null)
                {
                    // Add all new cultures to word
                    foreach (string item in newItems)
                    {
                        int cultureId = ValidationHelper.GetInteger(item, 0);
                        UserCultureInfoProvider.RemoveUserFromCulture(userId, cultureId, siteId);
                    }
                    invalidateUser = true;
                }
            }

            // Add new items
            items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
            if (!String.IsNullOrEmpty(items))
            {
                string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (newItems != null)
                {
                    // Add all new cultures to word
                    foreach (string item in newItems)
                    {
                        int cultureId = ValidationHelper.GetInteger(item, 0);
                        UserCultureInfoProvider.AddUserToCulture(userId, cultureId, siteId);
                    }
                    invalidateUser = true;
                }
            }

            // Invalidate user object
            if (invalidateUser)
            {
                user.Invalidate();
            }

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }
        else
        {
            ReloadCultures();
        }
    }


    protected void radSelectedLanguages_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckModifyPermissions())
        {
            // Set value indicating whether the user have allowed cultures
            if (user != null)
            {
                user.UserHasAllowedCultures = radSelectedLanguages.Checked;
                UserInfoProvider.SetUserInfo(user);

                uniSelector.Value = "";
                uniSelector.Reload(true);

                plcCultures.Visible = true;

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
        }
    }


    protected void radAllLanguages_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckModifyPermissions())
        {
            // Set value indicating whether the user have allowed cultures
            if (user != null)
            {
                // Remove user's allowed cultures
                UserCultureInfoProvider.RemoveUserFromAllCultures(userId);

                user.UserHasAllowedCultures = radSelectedLanguages.Checked;
                UserInfoProvider.SetUserInfo(user);

                plcCultures.Visible = false;

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
        }
    }

    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        uniSelector.Value = currentValues;
        uniSelector.Reload(true);
        pnlUpdate.Update();
    }

    #endregion


    #region "Protected methods"

    /// <summary>
    /// Reloads the cultures in UniSelector.
    /// </summary>
    protected void ReloadCultures()
    {
        DataTable dt = UserCultureInfoProvider.GetUserCultures(userId, siteId, null, null);
        if (!DataHelper.DataSourceIsEmpty(dt))
        {
            currentValues = String.Join(";", SqlHelperClass.GetStringValues(dt, "CultureID"));
            uniSelector.Value = currentValues;
            uniSelector.Reload(true);
        }
    }


    /// <summary>
    /// Checks modify permissions.
    /// </summary>
    protected bool CheckModifyPermissions()
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify"))
        {
            RedirectToAccessDenied("CMS.Users", "Modify");
        }

        // Check permissions
        string result = ValidateGlobalAndDeskAdmin(userId);
        if (result != String.Empty)
        {
            lblError.Visible = true;
            lblError.Text = result;
            return false;
        }

        return true;
    }


    /// <summary>
    /// Check whether current user is allowed to modify another user.
    /// </summary>
    /// <param name="userId">Modified user</param>
    /// <returns>"" or error message.</returns>
    protected static string ValidateGlobalAndDeskAdmin(int userId)
    {
        string result = String.Empty;

        if (CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            return result;
        }

        UserInfo userInfo = UserInfoProvider.GetUserInfo(userId);
        if (userInfo == null)
        {
            result = ResHelper.GetString("Administration-User.WrongUserId");
        }
        else
        {
            if (userInfo.IsGlobalAdministrator)
            {
                result = ResHelper.GetString("Administration-User.NotAllowedToModify");
            }
        }
        return result;
    }

    #endregion
}
