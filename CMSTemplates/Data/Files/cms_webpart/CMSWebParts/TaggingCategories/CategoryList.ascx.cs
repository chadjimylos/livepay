using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.Controls;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMS.TreeEngine;

public partial class CMSWebParts_TaggingCategories_CategoryList : CMSAbstractWebPart
{
    #region "Private variables"

    private bool mUseCompleteWhere = false;

    #endregion


    #region "Properties"

    /// <summary>
    /// Document list url
    /// </summary>
    public string DocumentListUrl
    {
        get
        {
            return ValidationHelper.GetString(GetValue("DocumentListUrl"), "");
        }
        set
        {
            SetValue("DocumentListUrl", value);
        }
    }


    /// <summary>
    /// Gets or sets the name of the transforamtion which is used for displaying the results
    /// </summary>
    public string TransformationName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("TransformationName"), "");
        }
        set
        {
            SetValue("TransformationName", value);
        }
    }


    /// <summary>
    /// Gets or sets the where condition
    /// </summary>
    public string WhereCondition
    {
        get
        {
            return ValidationHelper.GetString(GetValue("WhereCondition"), "");
        }
        set
        {
            SetValue("WhereCondition", value);
        }
    }


    /// <summary>
    /// Gets or sets the order by clause
    /// </summary>
    public string OrderBy
    {
        get
        {
            return ValidationHelper.GetString(GetValue("OrderBy"), "");
        }
        set
        {
            SetValue("OrderBy", value);
        }
    }


    /// <summary>
    /// Gets or sets the TOP N value.
    /// </summary>
    public int SelectTopN
    {
        get
        {
            return ValidationHelper.GetInteger(GetValue("SelectTopN"), 0);
        }
        set
        {
            SetValue("SelectTopN", value);
        }
    }


    /// <summary>
    ///  Gets or sets the value that indicates whether control is not visible for empty datasource
    /// </summary>
    public bool HideControlForZeroRows
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("HideControlForZeroRows"), true);
        }
        set
        {
            SetValue("HideControlForZeroRows", value);
        }
    }


    /// <summary>
    /// Gets or sets the text value which is displayed for zero rows result
    /// </summary>
    public string ZeroRowsText
    {
        get
        {
            return ValidationHelper.GetString(GetValue("ZeroRowsText"), "");
        }
        set
        {
            SetValue("ZeroRowsText", value);
        }
    }


    /// <summary>
    /// Gets or sets the name of the query string parameter which will pass the CategoryID to the DocumentListURL
    /// </summary>
    public string QueryStringName
    {
        get
        {
            string name = Convert.ToString(GetValue("QueryStringName"));
            if (String.IsNullOrEmpty(name))
            {
                name = "categoryid";
            }
            return name;
        }
        set
        {
            SetValue("QueryStringName", value);
        }
    }


    /// <summary>
    /// Gets or sets if the global categories should be displayed  
    /// </summary>
    public bool DisplayGlobalCategories
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayGlobalCategories"), true);
        }
        set
        {
            SetValue("DisplayGlobalCategories", value);
        }
    }


    /// <summary>
    /// Gets or sets if the user categories should be displayed  
    /// </summary>
    public bool DisplayCustomCategories
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayCustomCategories"), true);
        }
        set
        {
            SetValue("DisplayCustomCategories", value);
        }
    }


    /// <summary>
    /// Gets or sets the site name
    /// </summary>
    public string SiteName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("SiteName"), "");
        }
        set
        {
            SetValue("SiteName", value);
        }
    }


    /// <summary>
    /// Gets or sets the alias path
    /// </summary>
    public string AliasPath
    {
        get
        {
            return ValidationHelper.GetString(GetValue("AliasPath"), "");
        }
        set
        {
            SetValue("AliasPath", value);
        }
    }


    /// <summary>
    /// Gets or sets the culture code
    /// </summary>
    public string CultureCode
    {
        get
        {
            return ValidationHelper.GetString(GetValue("CultureCode"), "");
        }
        set
        {
            SetValue("CultureCode", value);
        }
    }


    /// <summary>
    /// Gets or sets combine with default culture
    /// </summary>
    public bool CombineWithDefaultCulture
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("CombineWithDefaultCulture"), true);
        }
        set
        {
            SetValue("CombineWithDefaultCulture", value);
        }
    }


    /// <summary>
    /// Gets or sets select only published
    /// </summary>
    public bool SelectOnlyPublished
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("SelectOnlyPublished"), true);
        }
        set
        {
            SetValue("SelectOnlyPublished", value);
        }
    }


    /// <summary>
    /// Gets or sets max relative level
    /// </summary>
    public int MaxRelativeLevel
    {
        get
        {
            return ValidationHelper.GetInteger(GetValue("MaxRelativeLevel"), -1);
        }
        set
        {
            SetValue("MaxRelativeLevel", value);
        }
    }

    #endregion


    #region "Overridden methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
        rptCategoryList.ReloadData(true);
    }


    /// <summary>
    /// OnPrerender override (Set visibility)
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if ((HideControlForZeroRows) && ((DataHelper.DataSourceIsEmpty(rptCategoryList.DataSource)) || (lblInfo.Visible)))
        {
            Visible = false;
        }
    }

    #endregion


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        // If displaying something
        if ((DisplayGlobalCategories) || (DisplayCustomCategories))
        {
            rptCategoryList.ItemDataBound += rptCategoryList_ItemDataBound;

            string where = GetCompleteWhereCondition();
            if (mUseCompleteWhere)
            {
                rptCategoryList.DataSource = CategoryInfoProvider.GetDocumentCategories(where, OrderBy, SelectTopN);
            }
            else
            {
                rptCategoryList.DataSource = CategoryInfoProvider.GetCategories(where, OrderBy, SelectTopN);
            }
            if (!String.IsNullOrEmpty(TransformationName))
            {
                rptCategoryList.ItemTemplate = CMSDataProperties.LoadTransformation(this, TransformationName, false);
            }
            rptCategoryList.HideControlForZeroRows = HideControlForZeroRows;
            rptCategoryList.ZeroRowsText = ZeroRowsText;
            rptCategoryList.DataBindByDefault = false;
            rptCategoryList.DataBind();
        }
        // Else show zero rows text
        else
        {
            lblInfo.Text = ZeroRowsText;
            lblInfo.Visible = true;
        }
    }


    /// <summary>
    /// Item databound handler
    /// </summary>
    protected void rptCategoryList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        // Set post count as text to the label control in repeater transformation
        if (e.Item.Controls.Count > 0)
        {
            // Try find hyperlink control with id 'lnkCategoryList'
            HyperLink lnkCategoryList = e.Item.Controls[0].FindControl("lnkCategoryList") as HyperLink;
            if (lnkCategoryList != null)
            {
                lnkCategoryList.Text = HTMLHelper.HTMLEncode(((DataRowView)(e.Item.DataItem)).Row["CategoryDisplayName"].ToString());
                lnkCategoryList.NavigateUrl = UrlHelper.AddParameterToUrl((String.IsNullOrEmpty(DocumentListUrl) ? UrlHelper.CurrentURL : CMSContext.GetUrl(DocumentListUrl)), QueryStringName, ((DataRowView)(e.Item.DataItem)).Row["CategoryID"].ToString());
            }
        }
    }


    /// <summary>
    /// Returns complete WHERE condition.
    /// </summary>
    protected string GetCompleteWhereCondition()
    {
        string where = "";
        string comleteWhere = "";

        string customWhere = ValidationHelper.GetString(GetValue("WhereCondition"), "");

        // Global where condition
        if (DisplayGlobalCategories)
        {
            where = " (CategoryUserID IS NULL) ";
        }

        // User where condition
        if (DisplayCustomCategories)
        {
            if ((CMSContext.CurrentUser != null) && (CMSContext.CurrentUser.UserID > 0))
            {
                if (where.Trim() != "")
                {
                    where = " (" + where + " OR (CategoryUserID = " + CMSContext.CurrentUser.UserID + ")) ";
                }
                else
                {
                    where = " (CategoryUserID = " + CMSContext.CurrentUser.UserID + ") ";
                }
            }
        }


        // Get complete where condition
        if ((!String.IsNullOrEmpty(SiteName)) || (!String.IsNullOrEmpty(AliasPath)) || (!String.IsNullOrEmpty(CultureCode)) || (MaxRelativeLevel > -1))
        {
            if (String.IsNullOrEmpty(SiteName))
            {
                SiteName = CMSContext.CurrentSiteName;
            }
            if (String.IsNullOrEmpty(AliasPath))
            {
                AliasPath = "/%";
            }
            comleteWhere = TreeProvider.GetCompleteWhereCondition(SiteName, AliasPath, CultureCode, CombineWithDefaultCulture, null, SelectOnlyPublished, MaxRelativeLevel);
            mUseCompleteWhere = true;
        }

        // Add complete where condition if is specified
        if (mUseCompleteWhere)
        {
            if (String.IsNullOrEmpty(where))
            {
                where = comleteWhere + ")) ";
            }
            else
            {
                where = comleteWhere + ")) AND " + where;
            }
        }

        // Add customWhere if is specified
        if (customWhere.Trim() != "")
        {
            if (where.Trim() != "")
            {
                where += " AND (" + customWhere + ") ";
            }
            else
            {
                where = customWhere;
            }
        }

        // Display only enabled
        if (where.Trim() != "")
        {
            where += " AND ";
        }
        where += "CategoryEnabled = 1";

        return where;
    }
}
