<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fck_InsertBizForms.aspx.cs"
    Inherits="CMSModules_BizForms_Controls_FCKEditor_fck_InsertBizForms" Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Insert BizForms</title>

    <script type="text/javascript">
        //<![CDATA[
        var oEditor = window.parent.InnerDialogLoaded();

        function insertBizForms(charValue) {
            oEditor.FCK.InsertHtml(charValue || "");
            window.parent.Cancel();
        }
        //]]>
    </script>

</head>
<body style="background-color: White;" runat="server" id="bodyElem">
    <form id="form1" runat="server">
        <cms:UniGrid runat="server" ID="uniGrid" GridName="~/CMSModules/BizForms/Controls/FCKEditor/InsertBizForms.xml"
            OrderBy="FormDisplayName" IsLiveSite="true" />
    </form>
</body>
</html>
