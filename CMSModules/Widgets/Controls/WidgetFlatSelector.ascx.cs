﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.UIControls;
using CMS.CMSHelper;
using CMS.SettingsProvider;

public partial class CMSModules_Widgets_Controls_WidgetFlatSelector : CMSAdminControl
{
    #region "Variables"

    private bool mSelectGroupWidgets = false;
    private bool mSelectEditorWidgets = false;
    private bool mSelectUserWidgets = false;
    private int mGroupID = 0;
    private string mTreeSelectedItem = null;
    private WidgetCategoryInfo mSelectedCategory = null;

    #endregion


    #region "Widget flat selector properties"

    /// <summary>
    /// Determines whether the flat selector will only display widgets available for group admins
    /// </summary>
    public bool SelectGroupWidgets
    {
        get
        {
            return mSelectGroupWidgets;
        }
        set
        {
            mSelectGroupWidgets = value;
        }
    }


    /// <summary>
    /// Determines whether the flat selector will display widgets available for editors
    /// </summary>
    public bool SelectEditorWidgets
    {
        get
        {
            return mSelectEditorWidgets;
        }
        set
        {
            mSelectEditorWidgets = value;
        }
    }


    /// <summary>
    /// Determines whether the flat selector will only display widgets available for authenticated users
    /// </summary>
    public bool SelectUserWidgets
    {
        get
        {
            return mSelectUserWidgets;
        }
        set
        {
            mSelectUserWidgets = value;
        }
    }


    /// <summary>
    /// Gets or sets the group ID. Should be set with enabled SelectOnlyGroupWidget property.
    /// </summary>
    public int GroupID
    {
        get
        {
            return mGroupID;
        }
        set
        {
            mGroupID = value;
        }
    }

    #endregion


    #region "Flat selector properties"

    /// <summary>
    /// Retruns inner instance of UniFlatSelector control.    
    /// </summary>
    public UniFlatSelector UniFlatSelector
    {
        get
        {
            return flatElem;
        }
    }


    /// <summary>
    /// Gets or sets selected item in flat selector
    /// </summary>
    public string SelectedItem
    {
        get
        {
            return flatElem.SelectedItem;
        }
        set
        {
            flatElem.SelectedItem = value;
        }
    }


    /// <summary>
    /// Gets or sets the current widget category
    /// </summary>
    public WidgetCategoryInfo SelectedCategory
    {
        get
        {
            // If not loaded yet
            if (mSelectedCategory == null)
            {
                int categoryId = ValidationHelper.GetInteger(this.TreeSelectedItem, 0);
                if (categoryId > 0)
                {
                    mSelectedCategory = WidgetCategoryInfoProvider.GetWidgetCategoryInfo(categoryId);
                }
            }

            return mSelectedCategory;
        }
        set
        {
            mSelectedCategory = value;
            // Update ID
            if (mSelectedCategory != null)
            {
                mTreeSelectedItem = mSelectedCategory.WidgetCategoryID.ToString();
            }
        }
    }


    /// <summary>
    /// Gets or sets the selected item in tree, ususaly the category id
    /// </summary>
    public string TreeSelectedItem
    {
        get
        {
            return mTreeSelectedItem;
        }
        set
        {
            // Clear loaded category if change
            if (value != mTreeSelectedItem)
            {
                mSelectedCategory = null;
            }
            mTreeSelectedItem = value;
        }
    }


    /// <summary>
    /// Indicates if the control should perform the operations
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return base.StopProcessing;
        }
        set
        {
            base.StopProcessing = value;
            flatElem.StopProcessing = value;
            this.EnableViewState = !value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            return;
        }

        // Setup flat selector
        flatElem.QueryName = "cms.widget.selectallview";
        flatElem.ValueColumn = "WidgetID";
        flatElem.SearchLabelResourceString = "widget.widgetname";
        flatElem.SearchColumn = "WidgetDisplayName";
        flatElem.SelectedColumns = "WidgetName, MetafileGUID, WidgetDisplayName, WidgetID";
        flatElem.PageSize = 15;
        flatElem.OrderBy = "WidgetDisplayName";
        flatElem.NotAvailableImageUrl = GetImageUrl("Objects/CMS_Widget/notavailable.png");
        flatElem.NoRecordsMessage = "widgets.norecordsincategory";
        flatElem.NoRecordsSearchMessage = "widgets.norecords";

        flatElem.OnItemSelected += new UniFlatSelector.ItemSelectedEventHandler(flatElem_OnItemSelected);
    }


    /// <summary>
    /// On PreRender.
    /// </summary>  
    protected override void OnPreRender(EventArgs e)
    {
        if (this.StopProcessing)
        {
            return;
        }

        // Security
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        if (SelectGroupWidgets)
        {
            // Shows group widgets without other security checks
            string where = "WidgetForGroup = 1";

            // But user must be group admin, otherwise show nothing
            if (!currentUser.IsGroupAdministrator(this.GroupID))
            {
                flatElem.ErrorText = ResHelper.GetString("widget.notgroupadmin");
            }

            flatElem.WhereCondition = SqlHelperClass.AddWhereCondition(flatElem.WhereCondition, where);
        }
        else
        {
            // Create security where condition
            string securityWhere = "";
            if (SelectEditorWidgets)
            {
                securityWhere += "WidgetForEditor = 1 ";
            }
            else if (SelectUserWidgets)
            {
                securityWhere += "WidgetForUser = 1 ";
            }

            securityWhere += " AND ((WidgetSecurity = 0)"; // Allowed for all        
            if (currentUser.IsAuthenticated())
            {
                securityWhere += " OR (WidgetSecurity = 1)"; // Authenticated
                securityWhere += " OR ((WidgetSecurity = 2) AND (WidgetID IN ( SELECT WidgetID FROM CMS_WidgetRole WHERE RoleID IN (SELECT RoleID FROM CMS_UserRole WHERE UserID = " + currentUser.UserID + ")))))"; // Authorized roles
            }
            else
            {
                securityWhere += ")";
            }
            flatElem.WhereCondition = SqlHelperClass.AddWhereCondition(flatElem.WhereCondition, securityWhere);
        }

        // Restrict to items in selected category (if not root)
        if ((this.SelectedCategory != null) && (this.SelectedCategory.WidgetCategoryParentID > 0))
        {
            flatElem.WhereCondition = SqlHelperClass.AddWhereCondition(flatElem.WhereCondition, "WidgetCategoryID = " + SelectedCategory.WidgetCategoryID + " OR WidgetCategoryID IN (SELECT WidgetCategoryID FROM CMS_WidgetCategory WHERE WidgetCategoryPath LIKE '" + SelectedCategory.WidgetCategoryPath + "/%')");
        }

        // Recently used items
        if (this.TreeSelectedItem.ToLower(CultureHelper.EnglishCulture) == "recentlyused")
        {
            flatElem.WhereCondition = SqlHelperClass.AddWhereCondition(flatElem.WhereCondition, SqlHelperClass.GetWhereCondition("WidgetName", currentUser.UserSettings.UserUsedWidgets.Split(';')));
        }

        // Description area and recently used
        litCategory.Text = ShowInDescriptionArea(this.SelectedItem);

        base.OnPreRender(e);
    }

    #endregion


    #region "Event handling"

    /// <summary>
    /// Updates description after item is selected in flat selector
    /// </summary>
    protected string flatElem_OnItemSelected(string selectedValue)
    {
        return ShowInDescriptionArea(selectedValue);
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Reloads data.
    /// </summary>
    public override void ReloadData()
    {
        flatElem.ReloadData();
        pnlUpdate.Update();
    }


    /// <summary>
    /// Generates HTML text to be used in description area
    /// </summary>
    ///<param name="selectedValue">Selected item for which generate description</param>
    private string ShowInDescriptionArea(string selectedValue)
    {
        string name = String.Empty;
        string description = String.Empty;

        if (!String.IsNullOrEmpty(selectedValue))
        {
            int widgetId = ValidationHelper.GetInteger(selectedValue, 0);
            WidgetInfo wi = WidgetInfoProvider.GetWidgetInfo(widgetId);
            if (wi != null)
            {
                name = wi.WidgetDisplayName;
                description = wi.WidgetDescription;
            }
        }
        // No selection show selected category
        else if (this.SelectedCategory != null)
        {
            name = this.SelectedCategory.WidgetCategoryDisplayName;
        }
        // Recently used
        else
        {
            name = ResHelper.GetString("widgets.recentlyused");
        }


        string text = "<div class=\"ItemName\">" + HTMLHelper.HTMLEncode(name) + "</div>";
        if (description != null)
        {
            text += "<div class=\"Description\">" + HTMLHelper.HTMLEncode(description) + "</div>";
        }

        return text;
    }

    #endregion
}
