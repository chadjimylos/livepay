﻿Imports LivePay_ESBBridge
Imports System.Data
Imports System.Collections.Generic

Partial Class LivePay_GetTransaction
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim w As New LivePay_ESBBridge.Bridge
        'Dim TransResponse As New TransactionDetailsResponse
        'Dim TransactionID As Integer = 640
        'TransResponse = w.GetTransactionDetails(TransactionID, LivePay_ESBBridge.SystemEnum.Merchants)

        'Dim TransInfo As TransactionDetailsInfo = TransResponse.TransactionDetails

        'If Not IsNothing(TransInfo) Then
        '    Response.Write("TransInfo.transactionDate -> " & TransInfo.transactionDate)
        'End If


        'Dim TransactionsObj As New GetTransactionsObj
        ''Response.Write("Price  --> " & Price)
        ''Response.End()
        'TransactionsObj.CardNumber = 4792750762293862
        ''TransactionsObj.CustomerId = UserID
        'TransactionsObj.AmountFrom = 12
        'TransactionsObj.amountTo = 12
        'TransactionsObj.MerchantId = 1025
        'TransactionsObj.TransactionDateFrom = DateTime.Now.AddHours(-3)
        'TransactionsObj.TransactionDateTo = DateTime.Now.AddHours(3)
        'TransactionsObj.info1 = 111111111111
        'TransactionsObj.PageSize = 10

        'Dim TransactionsResp As New GetTransactionsResponse

        'TransactionsResp = w.GetTransactions(TransactionsObj)
        'Dim PublicTransID As String = String.Empty
        'If True Then
        '    If TransactionsResp.Transactions IsNot Nothing Then
        '        If TransactionsResp.Transactions.Length > 0 Then
        '            Response.Write("TransactionsResp.Transactions(0).transactionId() -> " & TransactionsResp.Transactions(0).transactionId())
        '            PublicTransID = TransactionsResp.Transactions(0).transactionId()
        '        End If
        '    End If
        'End If

        'Dim req As New LivePay_ESBBridge.PaymentObj
        'req.TransactionAmount = 1.0
        ''req.transactionAmountSpecified = True
        'req.CardNumber = "4792750686324751"
        'req.CardExpiryDate = "0613"
        'req.CardCvv2 = "003"
        'req.Installments = 2
        ''req.installmentsSpecified = True
        'req.MerchantId = "1005"
        'req.CustomerId = "888"
        'req.CustomerComments = "descriptionReason"
        'req.Info1 = "000429538"
        'req.Info2 = "20110005"
        'req.Public = True
        ''req.publicSpecified = True

        'Dim res As LivePay_ESBBridge.PaymentResponse = w.MakePayment(req)
        'Dim Result As Boolean = res.Result

        'Response.Write("Result -> " & res.Result & "<BR />")
        'Response.Write("ErrorCode -> " & res.ErrorCode & "<BR />")
        'Response.Write("ErrorDetails -> " & res.ErrorDetails & "<BR />")
        'Response.Write("ErrorMessage -> " & res.ErrorMessage & "<BR />")

        Dim customTableClassName As String = "LivePay.FormControls"
        Dim Type As String = String.Empty

        ' Get data class using custom table name
        Dim customTableClassInfo As CMS.SettingsProvider.DataClassInfo = CMS.SettingsProvider.DataClassInfoProvider.GetDataClass(customTableClassName)
        If customTableClassInfo Is Nothing Then
            Throw New Exception("Given custom table does not exist.")
        End If

        ' Initialize custom table item provider with current user info and general connection
        Dim ctiProvider As New CMS.SiteProvider.CustomTableItemProvider(CMSContext.CurrentUser, CMS.DataEngine.ConnectionHelper.GetConnection())

        ' Get custom table items
        Dim dsItems As System.Data.DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, "ItemID = ", Nothing, -1, "ItemName")

        ' Check if DataSet is not empty
        If Not DataHelper.DataSourceIsEmpty(dsItems) Then
            ' Handle the retrieved data
            Type = dsItems.Tables(0).Rows(0)("ItemName").ToString()
        End If

        Response.Write(Type)

    End Sub
End Class
