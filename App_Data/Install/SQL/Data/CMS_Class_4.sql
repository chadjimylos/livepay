set identity_insert [cms_class] on
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Exchange rate', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="ExchagneRateID" fieldcaption="ExchagneRateID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="52028791-7170-424b-b587-3f0f3892084a" /><field column="ExchangeRateToCurrencyID" fieldcaption="ExchangeRateToCurrencyID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ae3af6c9-5fd8-44a7-b93c-9e42115aeff4" /><field column="ExchangeRateValue" fieldcaption="ExchangeRateValue" visible="true" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c68f77b3-fdb2-4f52-bb6f-848e143a836e" /><field column="ExchangeTableID" fieldcaption="ExchangeTableID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="008e3a69-256a-498e-9333-88d7c3d9e02e" /><field column="ExchangeRateGUID" fieldcaption="ExchangeRateGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a94dd2fb-461a-4433-b3f0-554ce6da1f20" /><field column="ExchangeRateLastModified" fieldcaption="ExchangeRateLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="147c537e-88d2-4a8e-9fd0-35ccf666780f" /></form>', '20081229 09:07:00', 1202, N'ecommerce.exchangerate', 0, 0, 0, '8cae6e0b-2da2-46a7-9a0b-fa73aed5e96b', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_CurrencyExchangeRate', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_CurrencyExchangeRate">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ExchagneRateID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ExchangeRateToCurrencyID" type="xs:int" />
              <xs:element name="ExchangeRateValue" type="xs:double" />
              <xs:element name="ExchangeTableID" type="xs:int" />
              <xs:element name="ExchangeRateGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="ExchangeRateLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_CurrencyExchangeRate" />
      <xs:field xpath="ExchagneRateID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Polls - Poll', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="PollID" fieldcaption="PollID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="2017eced-1133-44d2-be9e-0271eaa136f0" /><field column="PollCodeName" fieldcaption="PollCodeName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="07bdbe7e-259c-4b7a-81ce-e215b3900278" /><field column="PollDisplayName" fieldcaption="PollDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4d725c85-2113-4963-a259-2f3f71cb929b" /><field column="PollTitle" fieldcaption="PollTitle" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="66317519-d26b-4dae-8c23-6a7e2a8ff1e7" /><field column="PollOpenFrom" fieldcaption="PollOpenFrom" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="70f42b69-fb44-4801-98af-4b9d7bfe4422" /><field column="PollOpenTo" fieldcaption="PollOpenTo" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="136186d2-5e14-4878-bca1-a24d6e0b3659" /><field column="PollAllowMultipleAnswers" fieldcaption="PollAllowMultipleAnswers" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ae1ce3ca-db21-493a-9180-12cf78b0b701" /><field column="PollQuestion" fieldcaption="PollQuestion" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="972d4368-36b0-4fef-b44d-574f7cf18cec" /><field column="PollAccess" fieldcaption="PollAccess" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9b4f22c7-7702-40f7-a86e-219118b55527" /><field column="PollResponseMessage" fieldcaption="PollResponseMessage" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cd01bedf-1c05-42c4-9e3e-272e157c56ba" /><field column="PollGUID" fieldcaption="PollGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0cf99ca0-4a0a-4121-ab1c-4a083feb3b94" /><field column="PollLastModified" fieldcaption="PollLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="61add291-4cc4-43b8-b72e-eb9fa0a408ef" /><field column="PollGroupID" fieldcaption="PollGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="16f8e1a1-7af7-4c7e-bfcb-afb2ba399af4" /><field column="PollSiteID" fieldcaption="PollSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c6bf0202-7332-4176-8a24-efc1712f1e34" /></form>', '20100426 15:49:15', 1330, N'polls.poll', 0, 0, 0, '35aefb4e-1944-48af-8725-0aedf4bb17be', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Polls_Poll', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Polls_Poll">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="PollID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="PollCodeName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PollDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PollTitle" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PollOpenFrom" type="xs:dateTime" minOccurs="0" />
              <xs:element name="PollOpenTo" type="xs:dateTime" minOccurs="0" />
              <xs:element name="PollAllowMultipleAnswers" type="xs:boolean" />
              <xs:element name="PollQuestion">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PollAccess" type="xs:int" />
              <xs:element name="PollResponseMessage" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PollGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="PollLastModified" type="xs:dateTime" />
              <xs:element name="PollGroupID" type="xs:int" minOccurs="0" />
              <xs:element name="PollSiteID" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Polls_Poll" />
      <xs:field xpath="PollID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Polls - PollAnswer', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="AnswerID" fieldcaption="AnswerID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="85aa83fe-c59b-4848-808b-e4933cc873f0" /><field column="AnswerText" fieldcaption="AnswerText" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="462e0935-c9a8-42ef-a8b4-8c9de332491d" /><field column="AnswerOrder" fieldcaption="AnswerOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="641e9f4c-7afb-45e5-8e74-dbf316fa5fe2" /><field column="AnswerCount" fieldcaption="AnswerCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ea95f511-6c0f-431c-a3ac-b6f7824bc5ab" /><field column="AnswerEnabled" fieldcaption="AnswerEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="75e6c0d8-b0ca-41d5-b960-a3c3d25ef4a7" /><field column="AnswerPollID" fieldcaption="AnswerPollID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="18311baa-1a61-4aaf-9256-56d7b3be5afd" /><field column="AnswerGUID" fieldcaption="AnswerGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5a0d3438-6e13-4ebf-b306-fae1d444f5f3" /><field column="AnswerLastModified" fieldcaption="AnswerLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8c7645ba-c9da-4440-af55-9765b8e8f056" /></form>', '20090102 11:33:22', 1331, N'polls.pollanswer', 0, 0, 0, '6f77a516-ab75-412e-a75f-2602e0dcb293', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Polls_PollAnswer', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Polls_PollAnswer">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="AnswerID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="AnswerText">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AnswerOrder" type="xs:int" minOccurs="0" />
              <xs:element name="AnswerCount" type="xs:int" minOccurs="0" />
              <xs:element name="AnswerEnabled" type="xs:boolean" minOccurs="0" />
              <xs:element name="AnswerPollID" type="xs:int" />
              <xs:element name="AnswerGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="AnswerLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Polls_PollAnswer" />
      <xs:field xpath="AnswerID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Report', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ReportID" fieldcaption="ReportID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="54774329-35c0-4d7e-8e70-107db2efcc01" /><field column="ReportName" fieldcaption="ReportName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fcb403e6-9944-4782-a409-d6b0e7845ce9" /><field column="ReportDisplayName" fieldcaption="ReportDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9b994700-6a2d-451e-9ef8-8013fd3365ef" columnsize="440" visibility="none" ismacro="false" /><field column="ReportLayout" fieldcaption="ReportLayout" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fd743157-93fe-4dbc-8db1-6b0218205c2a" /><field column="ReportParameters" fieldcaption="ReportParameters" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1c648af7-cf8d-49ff-8d2d-d152aaff301e" /><field column="ReportCategoryID" fieldcaption="ReportCategoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8c8f484d-4f01-4359-8cb6-1a5ae63e1d78" /><field column="ReportAccess" fieldcaption="ReportAccess" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3b798284-728b-46da-a669-069988e7f440" /><field column="ReportGUID" fieldcaption="ReportGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bd7e8bd6-a0fe-4a0e-bce4-ab6d84aef49c" /><field column="ReportLastModified" fieldcaption="ReportLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="639711b2-c908-449d-8946-ce9b2ef87ac8" /></form>', '20100325 18:30:53', 1338, N'Reporting.Report', 0, 0, 0, '8e5b3d49-5cf9-487a-afc3-164d26fdaa2e', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Reporting_Report', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Reporting_Report">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ReportID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ReportName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ReportDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="440" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ReportLayout">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ReportParameters">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ReportCategoryID" type="xs:int" />
              <xs:element name="ReportAccess" type="xs:int" />
              <xs:element name="ReportGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ReportLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Reporting_Report" />
      <xs:field xpath="ReportID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'ReportGraph', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="GraphID" fieldcaption="GraphID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="eed4199f-210d-4306-b919-28477fee1a6f" /><field column="GraphName" fieldcaption="GraphName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cfdc850b-9ac0-4671-98e8-2bd0ebe42b5e" /><field column="GraphDisplayName" fieldcaption="GraphDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="92eec597-ebd4-44c8-ab5a-39f72b089a2e" /><field column="GraphQuery" fieldcaption="GraphQuery" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e201f69e-2c44-457b-b944-21c4958d588a" /><field column="GraphQueryIsStoredProcedure" fieldcaption="GraphQueryIsStoredProcedure" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="95801c24-5c35-435d-889d-a39f003ebd1c" /><field column="GraphType" fieldcaption="GraphType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="075c428a-5a6e-4922-a490-23c5d36e77c0" /><field column="GraphReportID" fieldcaption="GraphReportID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ebb98f15-b8e5-49df-9730-d96f455ab409" /><field column="GraphTitle" fieldcaption="GraphTitle" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="60cc5183-b44c-44f2-9c65-83e667e4b0e3" /><field column="GraphXAxisTitle" fieldcaption="GraphXAxisTitle" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="dc023e32-ae9a-4564-9d1e-f04659ea61fa" /><field column="GraphYAxisTitle" fieldcaption="GraphYAxisTitle" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c0370641-69cb-43c0-ad3d-a5322d9791b7" /><field column="GraphWidth" fieldcaption="GraphWidth" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="938fbf3b-145e-4577-baca-146528b53315" /><field column="GraphHeight" fieldcaption="GraphHeight" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3f076698-3fa5-4b0e-aec0-5e27ec801ac7" /><field column="GraphLegendPosition" fieldcaption="GraphLegendPosition" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9b38f89d-4d0d-4dcc-ac84-e39d6e906e92" /><field column="GraphSettings" fieldcaption="GraphSettings" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c8ddc397-c8bc-4909-b715-4db752b69d0c" /><field column="GraphGUID" fieldcaption="GraphGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6d952912-2be9-4b73-885b-53159198072d" /><field column="GraphLastModified" fieldcaption="GraphLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4cd06d92-b729-47f2-8b52-4a034e80cedb" /></form>', '20081229 09:07:21', 1339, N'Reporting.ReportGraph', 0, 0, 0, 'f4e23a8d-9b99-4fdc-8e82-8ceb69fa5cd7', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Reporting_ReportGraph', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Reporting_ReportGraph">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="GraphID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="GraphName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GraphDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GraphQuery">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GraphQueryIsStoredProcedure" type="xs:boolean" />
              <xs:element name="GraphType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GraphReportID" type="xs:int" />
              <xs:element name="GraphTitle" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GraphXAxisTitle" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GraphYAxisTitle" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GraphWidth" type="xs:int" minOccurs="0" />
              <xs:element name="GraphHeight" type="xs:int" minOccurs="0" />
              <xs:element name="GraphLegendPosition" type="xs:int" minOccurs="0" />
              <xs:element name="GraphSettings" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GraphGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="GraphLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Reporting_ReportGraph" />
      <xs:field xpath="GraphID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'ReportTable', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TableID" fieldcaption="TableID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="01ec14c3-a29d-4d45-8991-00c753d51f1b" /><field column="TableName" fieldcaption="TableName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b5bc8fb2-4fac-4de3-8417-893b1c570458" /><field column="TableDisplayName" fieldcaption="TableDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="db0bbc1e-9682-4ba2-b02e-cb94f28bcfe2" /><field column="TableQuery" fieldcaption="TableQuery" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5a207185-1804-4523-83f0-53e232b487fd" /><field column="TableQueryIsStoredProcedure" fieldcaption="TableQueryIsStoredProcedure" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="da5e0111-43ae-445e-89aa-5cef08f0da2e" /><field column="TableReportID" fieldcaption="TableReportID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e1b60a01-8bcc-4982-be5d-b0c36c91ef99" /><field column="TableSettings" fieldcaption="TableSettings" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4d6d2a3d-b807-4eeb-896d-137fde611c10" /><field column="TableGUID" fieldcaption="TableGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c25fbaa2-44aa-4d61-826f-33eb597ad524" /><field column="TableLastModified" fieldcaption="TableLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5ab4d460-8534-41ab-a4da-79940336ee0c" /></form>', '20081229 09:07:22', 1341, N'Reporting.ReportTable', 0, 0, 0, 'dc3c643e-3f7b-455a-88e5-4b95cf964756', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Reporting_ReportTable', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Reporting_ReportTable">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TableID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="TableName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TableDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TableQuery">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TableQueryIsStoredProcedure" type="xs:boolean" />
              <xs:element name="TableReportID" type="xs:int" />
              <xs:element name="TableSettings" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TableGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="TableLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Reporting_ReportTable" />
      <xs:field xpath="TableID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'ReportValue', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="ValueID" fieldcaption="ValueID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="f1e02451-4522-48b3-9ae4-afdfd9b4ead8" /><field column="ValueName" fieldcaption="ValueName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d72af990-71b1-4cb4-a94c-f9285a75bd53" /><field column="ValueDisplayName" fieldcaption="ValueDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="641aeca8-f17c-4bd1-bbfe-ddb77396d1a9" /><field column="ValueQuery" fieldcaption="ValueQuery" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="82b749ae-b6c5-41ff-90fd-9f5965ab70f0" /><field column="ValueQueryIsStoredProcedure" fieldcaption="ValueQueryIsStoredProcedure" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4709fd4f-4fda-4fb3-bd45-f08a3c6024d8" /><field column="ValueFormatString" fieldcaption="ValueFormatString" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3ba99cb4-40ae-4e91-947d-d280be7a825b" /><field column="ValueReportID" fieldcaption="ValueReportID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="aabffc3a-2a0a-4321-9540-351f16510842" /><field column="ValueGUID" fieldcaption="ValueGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8601ebe4-5f0a-4e76-9b96-e2a9fe92a0c3" /><field column="ValueLastModified" fieldcaption="ValueLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="933bc040-9b40-4524-91e2-9535d339d7e3" /></form>', '20081229 09:07:22', 1342, N'Reporting.ReportValue', 0, 0, 0, 'a131b916-b2e3-4a9a-ae29-29d12bb6bbbf', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Reporting_ReportValue', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Reporting_ReportValue">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ValueID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ValueName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ValueDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ValueQuery">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ValueQueryIsStoredProcedure" type="xs:boolean" />
              <xs:element name="ValueFormatString" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ValueReportID" type="xs:int" />
              <xs:element name="ValueGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="ValueLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Reporting_ReportValue" />
      <xs:field xpath="ValueID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'ReportingSavedGraph', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="SavedGraphID" fieldcaption="SavedGraphID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="efd03c4f-4299-4abb-b589-af20b0b74a9b" /><field column="SavedGraphSavedReportID" fieldcaption="SavedGraphSavedReportID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d2562dcd-50e6-4a23-aacc-c34c931d84ae" /><field column="SavedGraphGUID" fieldcaption="SavedGraphGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c453fcc2-d4b0-40c2-ba6c-071f1da64242" /><field column="SavedGraphBinary" fieldcaption="SavedGraphBinary" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8d10643c-ed3d-4eea-8603-34b058fb0a87" /><field column="SavedGraphMimeType" fieldcaption="SavedGraphMimeType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7e1dcef8-bf43-4300-819b-cf4aca678884" /><field column="SavedGraphLastModified" fieldcaption="SavedGraphLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3b124fbb-878f-4586-8cdb-5adf09eb571d" /></form>', '20081229 09:07:21', 1349, N'Reporting.SavedGraph', 0, 0, 0, 'a9b8cccf-a725-4101-8ba3-41cada06c8d4', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Reporting_SavedGraph', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Reporting_SavedGraph">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SavedGraphID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SavedGraphSavedReportID" type="xs:int" />
              <xs:element name="SavedGraphGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="SavedGraphBinary" type="xs:base64Binary" />
              <xs:element name="SavedGraphMimeType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SavedGraphLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Reporting_SavedGraph" />
      <xs:field xpath="SavedGraphID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'ReportingSavedReport', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="SavedReportID" fieldcaption="SavedReportID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="0575d5aa-5090-47cd-ab7e-a09f906d11f4" /><field column="SavedReportReportID" fieldcaption="SavedReportReportID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="65296e8c-a796-475f-96c8-7e37d7ad4bf1" /><field column="SavedReportGUID" fieldcaption="SavedReportGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1423e2f0-1274-40f7-ad2a-4599e0a8d6a1" /><field column="SavedReportTitle" fieldcaption="SavedReportTitle" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="64328433-acbc-40de-a208-cadeb5a4d4ca" /><field column="SavedReportDate" fieldcaption="SavedReportDate" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="935d44a6-b553-4e7c-ab84-16db554c92a9" /><field column="SavedReportHTML" fieldcaption="SavedReportHTML" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e9416b58-29ee-4fa4-9bf9-4cdc2adefa4a" /><field column="SavedReportParameters" fieldcaption="SavedReportParameters" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2acae724-da8e-42a9-84b1-7ec1e546ea61" /><field column="SavedReportCreatedByUserID" fieldcaption="SavedReportCreatedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c12cd366-c374-4538-9e2d-29d6b57b287d" /><field column="SavedReportLastModified" fieldcaption="SavedReportLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="66f9efe1-0ac0-43bc-a6af-010b9d0e564b" /></form>', '20091125 16:37:14', 1350, N'Reporting.SavedReport', 0, 0, 0, '0d57ce56-d0bf-49f3-8a94-240268deec66', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Reporting_SavedReport', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Reporting_SavedReport">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SavedReportID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SavedReportReportID" type="xs:int" />
              <xs:element name="SavedReportGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="SavedReportTitle" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SavedReportDate" type="xs:dateTime" />
              <xs:element name="SavedReportHTML">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SavedReportParameters">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SavedReportCreatedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="SavedReportLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Reporting_SavedReport" />
      <xs:field xpath="SavedReportID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Blog comment', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="CommentID" fieldcaption="CommentID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="8ab031c9-df3d-4c65-9016-289e5c8469e7" /><field column="CommentUserName" fieldcaption="CommentUserName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="6a4c697c-0881-4e38-a2e6-b1a619af658c" /><field column="CommentUserID" fieldcaption="CommentUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c9d0fc02-d016-4f5a-870c-5a607c338a6b" /><field column="CommentUrl" fieldcaption="CommentUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="d828d315-4a6d-4919-80ab-f080ac4507bb" /><field column="CommentText" fieldcaption="CommentText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1189c1f2-8391-4dc0-8019-05cfa2bdd97e" /><field column="CommentEmail" fieldcaption="E-mail" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="973875a2-3429-4562-a631-eb41f6baa8d1" /><field column="CommentApprovedByUserID" fieldcaption="CommentApprovedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f275bdd1-6b44-489e-87d8-f362f6fc9583" /><field column="CommentPostDocumentID" fieldcaption="CommentPostDocumentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fac813e1-53c1-4ded-8f1e-11940b812653" /><field column="CommentDate" fieldcaption="CommentDate" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5b67362f-b86f-4f6e-bbdd-ad6d7b425600" /><field column="CommentIsSpam" fieldcaption="CommentIsSpam" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b66f6add-2e5f-412c-b386-56b5f677b89b" /><field column="CommentApproved" fieldcaption="CommentApproved" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7f112078-00c8-4747-b218-ca779f0a3fb2" /><field column="CommentIsTrackBack" fieldcaption="CommentIsTrackBack" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2d213593-81e7-4d70-9fff-6623294b09d2" defaultvalue="false" /></form>', '20100303 13:48:22', 1360, N'blog.comment', 0, 0, 0, 'ff247c39-574a-4807-bbdb-e42b512f9898', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Blog_Comment', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Blog_Comment">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CommentID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="CommentUserName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CommentUserID" type="xs:int" minOccurs="0" />
              <xs:element name="CommentUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CommentText">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CommentApprovedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="CommentPostDocumentID" type="xs:int" />
              <xs:element name="CommentDate" type="xs:dateTime" />
              <xs:element name="CommentIsSpam" type="xs:boolean" minOccurs="0" />
              <xs:element name="CommentApproved" type="xs:boolean" minOccurs="0" />
              <xs:element name="CommentIsTrackBack" type="xs:boolean" minOccurs="0" />
              <xs:element name="CommentEmail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Blog_Comment" />
      <xs:field xpath="CommentID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'<search><item tokenized="False" name="CommentApproved" content="False" searchable="True" id="dae64ba0-89b3-4cd9-a726-56cbc30f90e8"></item><item tokenized="True" name="CommentEmail" content="True" searchable="True" id="1075417d-2313-4cdd-a057-ccf33164a191"></item><item tokenized="False" name="CommentIsTrackBack" content="False" searchable="True" id="e4062876-5ca1-4c34-a6b7-ca3165464444"></item><item tokenized="False" name="CommentPostDocumentID" content="False" searchable="True" id="9d9ab8c6-5de6-4745-a1b1-f09d45c5c0ba"></item><item tokenized="False" name="CommentDate" content="False" searchable="True" id="50f07302-cf70-49eb-a07b-0d67d43231ad"></item><item tokenized="False" name="CommentApprovedByUserID" content="False" searchable="True" id="490785a7-7059-40ee-9903-5ffb6894f3b6"></item><item tokenized="True" name="CommentUrl" content="True" searchable="True" id="d60a2aeb-cb62-488a-b85e-3696d2818fba"></item><item tokenized="True" name="CommentText" content="True" searchable="True" id="21e30c19-dd1b-4ba8-8cc5-510fdc9f6dfc"></item><item tokenized="False" name="CommentIsSpam" content="False" searchable="True" id="96ff6705-0ea6-4dba-8ea8-d02d5713c489"></item><item tokenized="True" name="CommentUserName" content="True" searchable="True" id="7f2528b2-9109-4d45-9d89-e165edadb225"></item><item tokenized="False" name="CommentID" content="False" searchable="True" id="549a65eb-3852-4b89-b003-c3ad117187fa"></item><item tokenized="False" name="CommentUserID" content="False" searchable="True" id="30c8c336-1cd0-4454-8f4e-7389c71195b7"></item></search>', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Analytics statistics', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="StatisticsID" fieldcaption="StatisticsID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="304c2ad5-41fd-4954-920b-1b8d9876e48a" /><field column="StatisticsSiteID" fieldcaption="StatisticsSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43c6424f-8930-4abb-82c7-139aff98871d" /><field column="StatisticsCode" fieldcaption="StatisticsCode" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b58d2acd-05c6-48d3-a19e-8b720bcf18b1" /><field column="StatisticsObjectName" fieldcaption="StatisticsObjectName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="03afa08a-bfdf-41de-ac95-03da490da18b" /><field column="StatisticsObjectID" fieldcaption="StatisticsObjectID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="effde454-0f22-4a16-9a9c-4510b8c9fe16" /><field column="StatisticsObjectCulture" fieldcaption="StatisticsObjectCulture" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f9ab8359-7261-47c6-b368-21d3c3439b71" /></form>', '20100215 16:12:54', 1366, N'analytics.statistics', 0, 0, 0, '75b95c91-bc3e-45a7-b40d-27581d8b67bc', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Analytics_Statistics', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Analytics_Statistics">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="StatisticsID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="StatisticsSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="StatisticsCode">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StatisticsObjectName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StatisticsObjectID" type="xs:int" minOccurs="0" />
              <xs:element name="StatisticsObjectCulture" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="10" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Analytics_Statistics" />
      <xs:field xpath="StatisticsID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Analytics hour hits', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="HitsID" fieldcaption="HitsID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="2b4c7f1f-b65e-49e3-b9e3-1b5f6cec367a" /><field column="HitsStatisticsID" fieldcaption="HitsStatisticsID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="dab8b16a-40a5-46a1-b9b6-ef48a36b665f" /><field column="HitsStartTime" fieldcaption="HitsStartTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="01b9f2d8-7c62-42e3-9137-4c65fc20dcf5" /><field column="HitsEndTime" fieldcaption="HitsEndTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4767e166-32e9-4630-afcc-78711f95960a" /><field column="HitsCount" fieldcaption="HitsCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7500e99c-be1c-4ef3-8626-4dd97c1f9c08" /></form>', '20081229 09:06:53', 1367, N'analytics.hitshour', 0, 0, 0, 'b97a7c1e-5193-4388-938b-a212e8f2c28a', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Analytics_HourHits', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Analytics_HourHits">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="HitsID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="HitsStatisticsID" type="xs:int" />
              <xs:element name="HitsStartTime" type="xs:dateTime" />
              <xs:element name="HitsEndTime" type="xs:dateTime" />
              <xs:element name="HitsCount" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Analytics_HourHits" />
      <xs:field xpath="HitsID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Analytics day hits', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="HitsID" fieldcaption="HitsID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="2b1d184c-b95c-407e-b253-cd81d0a1a5a9" /><field column="HitsStatisticsID" fieldcaption="HitsStatisticsID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4301af00-35b2-4f73-90c2-dfc987809658" /><field column="HitsStartTime" fieldcaption="HitsStartTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="dc4a60fc-3b3c-44a5-b42e-50e14f3da88a" /><field column="HitsEndTime" fieldcaption="HitsEndTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="89b49a8b-e2ff-4c3a-a1f4-eff3f623aaa7" /><field column="HitsCount" fieldcaption="HitsCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="41d7b63a-80d2-45ff-b9bf-0fb3c173980e" /></form>', '20081229 09:06:53', 1368, N'analytics.hitsday', 0, 0, 0, '97040785-3faf-4d17-9b77-7436f7b10844', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Analytics_DayHits', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Analytics_DayHits">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="HitsID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="HitsStatisticsID" type="xs:int" />
              <xs:element name="HitsStartTime" type="xs:dateTime" />
              <xs:element name="HitsEndTime" type="xs:dateTime" />
              <xs:element name="HitsCount" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Analytics_DayHits" />
      <xs:field xpath="HitsID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Analytics month hits', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="HitsID" fieldcaption="HitsID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="d191d9b3-52ef-43c1-ae3d-7779125897a0" /><field column="HitsStatisticsID" fieldcaption="HitsStatisticsID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9ad191da-6058-4916-a7d4-1b70b0f6b66d" /><field column="HitsStartTime" fieldcaption="HitsStartTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="290752d5-924a-441f-8450-f057d653ac70" /><field column="HitsEndTime" fieldcaption="HitsEndTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6c89aad0-8284-4ec7-96e7-d4ee954a5de3" /><field column="HitsCount" fieldcaption="HitsCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b28a32d6-ac63-4e3f-9747-1061714809ec" /></form>', '20081229 09:06:53', 1369, N'analytics.hitsmonth', 0, 0, 0, '7528e30b-8884-4e8f-8a49-085f8069ffea', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Analytics_MonthHits', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Analytics_MonthHits">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="HitsID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="HitsStatisticsID" type="xs:int" />
              <xs:element name="HitsStartTime" type="xs:dateTime" />
              <xs:element name="HitsEndTime" type="xs:dateTime" />
              <xs:element name="HitsCount" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Analytics_MonthHits" />
      <xs:field xpath="HitsID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Analytics week hits', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="HitsID" fieldcaption="HitsID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="a2831edb-3d81-4a7e-82ca-bcb63d676364" /><field column="HitsStatisticsID" fieldcaption="HitsStatisticsID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f10ff8a2-e7d7-4713-bfc4-bbb7459b89f0" /><field column="HitsStartTime" fieldcaption="HitsStartTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="816e8b8b-f28e-4152-860f-2141cdf6a88b" /><field column="HitsEndTime" fieldcaption="HitsEndTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="de5eb681-92ea-49cf-939a-615b856c7dae" /><field column="HitsCount" fieldcaption="HitsCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="25dc690e-9106-44d5-b81b-bab6e271cf50" /></form>', '20081229 09:06:53', 1370, N'analytics.hitsweek', 0, 0, 0, '32eb47b8-2e30-4094-8c20-be3b355b31d1', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Analytics_WeekHits', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Analytics_WeekHits">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="HitsID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="HitsStatisticsID" type="xs:int" />
              <xs:element name="HitsStartTime" type="xs:dateTime" />
              <xs:element name="HitsEndTime" type="xs:dateTime" />
              <xs:element name="HitsCount" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Analytics_WeekHits" />
      <xs:field xpath="HitsID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Analytics year hits', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="HitsID" fieldcaption="HitsID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e424b08c-9d5c-4fde-98f9-fa5df680ba40" /><field column="HitsStatisticsID" fieldcaption="HitsStatisticsID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="12a87691-0b02-4676-93c6-6949fbf5a7dd" /><field column="HitsStartTime" fieldcaption="HitsStartTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4360ade8-e25f-4466-91d5-5da81683e6e3" /><field column="HitsEndTime" fieldcaption="HitsEndTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="69ebdbff-6467-465a-a329-5b1ee5ccdda4" /><field column="HitsCount" fieldcaption="HitsCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1c3d328b-4ac0-41b5-968c-f54fa796cf5d" /></form>', '20081229 09:06:53', 1371, N'analytics.hitsyear', 0, 0, 0, '3f74ab7f-4d2e-40cd-8a94-78ba0a84f366', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Analytics_YearHits', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Analytics_YearHits">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="HitsID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="HitsStatisticsID" type="xs:int" />
              <xs:element name="HitsStartTime" type="xs:dateTime" />
              <xs:element name="HitsEndTime" type="xs:dateTime" />
              <xs:element name="HitsCount" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Analytics_YearHits" />
      <xs:field xpath="HitsID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Volume discount', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="VolumeDiscountID" fieldcaption="VolumeDiscountID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="0c00f4b8-c620-47c9-b210-560ccad5de07" /><field column="VolumeDiscountSKUID" fieldcaption="VolumeDiscountSKUID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="40cf73c1-36e7-4ba1-a430-99edc9882288" /><field column="VolumeDiscountMinCount" fieldcaption="VolumeDiscountMinCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fec331ea-76d5-4b7b-9d07-712a73f010df" /><field column="VolumeDiscountValue" fieldcaption="VolumeDiscountValue" visible="true" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="85872dc6-b413-4a5c-b534-193c93175e94" /><field column="VolumeDiscountIsFlatValue" fieldcaption="VolumeDiscountIsFlatValue" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="87ded51f-d9b2-4650-88a4-53c3e09c4735" /><field column="VolumeDiscountGUID" fieldcaption="VolumeDiscountGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="51cb9858-7105-4c5e-8f23-58b9299b37de" /><field column="VolumeDiscountLastModified" fieldcaption="VolumeDiscountLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1709e8fd-993e-44a3-9e74-eab513d45a78" /></form>', '20091103 08:57:47', 1424, N'ecommerce.volumediscount', 0, 0, 0, 'c363a8ef-9b91-4e8b-860f-1e058dfc66ec', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_VolumeDiscount', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_VolumeDiscount">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="VolumeDiscountID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="VolumeDiscountSKUID" type="xs:int" />
              <xs:element name="VolumeDiscountMinCount" type="xs:int" />
              <xs:element name="VolumeDiscountValue" type="xs:double" />
              <xs:element name="VolumeDiscountIsFlatValue" type="xs:boolean" />
              <xs:element name="VolumeDiscountGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="VolumeDiscountLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_VolumeDiscount" />
      <xs:field xpath="VolumeDiscountID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Discount level', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="DiscountLevelID" fieldcaption="DiscountLevelID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="7ad32772-5c73-4ff7-ba95-1ffb0166a2d3" /><field column="DiscountLevelDisplayName" fieldcaption="DiscountLevelDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="83d2a289-1cf3-4d43-9b15-869f31f647f1" /><field column="DiscountLevelName" fieldcaption="DiscountLevelName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="564457b5-62b5-4e2d-a34f-3b3f96a2d601" /><field column="DiscountLevelValue" fieldcaption="DiscountLevelValue" visible="true" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a1882af8-6ca2-411e-b3f4-c0ed83448af8" /><field column="DiscountLevelEnabled" fieldcaption="DiscountLevelEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7d41d785-b1b3-4faa-83b9-8d5be49569f1" /><field column="DiscountLevelValidFrom" fieldcaption="DiscountLevelValidFrom" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bc2fca5f-aa50-43be-8306-f172d826ad86" /><field column="DiscountLevelValidTo" fieldcaption="DiscountLevelValidTo" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="69a5c64e-e5f1-4a2d-85da-58a83eaaa9b9" /><field column="DiscountLevelGUID" fieldcaption="DiscountLevelGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d0e040df-1faa-4e45-b7dc-183b5f3e742a" /><field column="DiscountLevelLastModified" fieldcaption="DiscountLevelLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a54ed583-3013-484d-b2c6-768c2d6f3ea0" /></form>', '20100104 15:52:53', 1429, N'ecommerce.discountlevel', 0, 0, 0, 'bffc1dfe-82c4-4612-a3dc-e7a40cb26985', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_DiscountLevel', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_DiscountLevel">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="DiscountLevelID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="DiscountLevelDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DiscountLevelName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DiscountLevelValue" type="xs:double" />
              <xs:element name="DiscountLevelEnabled" type="xs:boolean" />
              <xs:element name="DiscountLevelValidFrom" type="xs:dateTime" minOccurs="0" />
              <xs:element name="DiscountLevelValidTo" type="xs:dateTime" minOccurs="0" />
              <xs:element name="DiscountLevelGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="DiscountLevelLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_DiscountLevel" />
      <xs:field xpath="DiscountLevelID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'ReportCategory', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="CategoryID" fieldcaption="CategoryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="2d4af13c-4087-468e-8971-5e63c038e867" /><field column="CategoryDisplayName" fieldcaption="CategoryDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e0fcb4bf-d573-4e0e-8eb5-9a47e810b13a" /><field column="CategoryCodeName" fieldcaption="CategoryCodeName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="dc0ecf7d-0953-447f-a8eb-8876d759d0e5" /><field column="CategoryGUID" fieldcaption="CategoryGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c843471e-1877-4fcb-ba2c-ee55c3413248" /><field column="CategoryLastModified" fieldcaption="CategoryLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b917a7d8-8d58-4dbf-83f1-9f5e32eed1a2" /></form>', '20081229 09:07:21', 1431, N'Reporting.ReportCategory', 0, 0, 0, 'f2763c6c-d3d8-4e39-89f9-8f71c21f9368', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Reporting_ReportCategory', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Reporting_ReportCategory">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CategoryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="CategoryDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryCodeName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="CategoryLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Reporting_ReportCategory" />
      <xs:field xpath="CategoryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Tax class state', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TaxClassID" fieldcaption="TaxClassID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="c3503fa7-38e1-4450-b245-cece59d8b765" /><field column="StateID" fieldcaption="StateID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c3ac6edb-e895-4f76-8469-68a3b02d2e37" /><field column="TaxValue" fieldcaption="TaxValue" visible="true" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3f9b4b7c-a74c-4a29-acff-f5e4359b5206" /><field column="IsFlatValue" fieldcaption="IsFlatValue" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1318a545-531e-4d9a-b121-ca3f4867895f" /></form>', '20081229 09:07:03', 1448, N'ecommerce.taxclassstate', 0, 0, 0, '25c47e7c-7eb0-4edb-8509-960cddbda2e1', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_TaxClassState', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_TaxClassState">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TaxClassID" type="xs:int" />
              <xs:element name="StateID" type="xs:int" />
              <xs:element name="TaxValue" type="xs:double" />
              <xs:element name="IsFlatValue" type="xs:boolean" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_TaxClassState" />
      <xs:field xpath="TaxClassID" />
      <xs:field xpath="StateID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
set identity_insert [cms_class] off
