using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.ExtendedControls;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_DiscountCoupon_Discount_Edit_General : CMSEcommercePage
{
	protected int discountid = 0;
	
	protected void Page_Load(object sender, EventArgs e)
	{
        rfvCouponCode.ErrorMessage = ResHelper.GetString("DiscounCoupon_Edit.errorCode");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("DiscounCoupon_Edit.errorDisplay");
		
        // control initializations				
	
        lblDiscountCouponValue.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponAbsoluteValueLabel");
		lblDiscountCouponValidTo.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponValidToLabel");
		lblDiscountCouponCode.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponCodeLabel");

		lblDiscountCouponDisplayName.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponDisplayNameLabel");
		lblDiscountCouponValidFrom.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponValidFromLabel");

        radFixed.Text = ResHelper.GetString("DiscounCoupon_Edit.radFixed");
        radPercentage.Text = ResHelper.GetString("DiscounCoupon_Edit.radPercentage");

		btnOk.Text = ResHelper.GetString("General.OK");	
		dtPickerDiscountCouponValidTo.SupportFolder = "~/CMSAdminControls/Calendar";
		dtPickerDiscountCouponValidFrom.SupportFolder = "~/CMSAdminControls/Calendar";

		string currentDiscountCoupon = ResHelper.GetString("DiscounCoupon_Edit.NewItemCaption");

		// get discountCoupon id from querystring		
        discountid = ValidationHelper.GetInteger(Request.QueryString["discountid"], 0);
        if (discountid > 0)
        {
            DiscountCouponInfo discountCouponObj = DiscountCouponInfoProvider.GetDiscountCouponInfo(discountid);
            if (discountCouponObj != null)
            {
                currentDiscountCoupon = discountCouponObj.DiscountCouponCode;

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(discountCouponObj);

                    // show that the discountCoupon was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
      
        }
	}


	/// <summary>
	/// Load data of editing discountCoupon.
	/// </summary>
	/// <param name="discountCouponObj">DiscountCoupon object.</param>
	protected void LoadData(DiscountCouponInfo discountCouponObj)
	{
	
		txtDiscountCouponValue.Text = Convert.ToString(discountCouponObj.DiscountCouponValue);
		dtPickerDiscountCouponValidTo.SelectedDateTime = discountCouponObj.DiscountCouponValidTo;
		txtDiscountCouponCode.Text = discountCouponObj.DiscountCouponCode;

        if (!ValidationHelper.GetBoolean(discountCouponObj.DiscountCouponIsFlatValue, false))
        {
            radPercentage.Checked = true;
        }

        //	chkDiscountCouponIsFlatValue.Checked = ;
		txtDiscountCouponDisplayName.Text = discountCouponObj.DiscountCouponDisplayName;
		dtPickerDiscountCouponValidFrom.SelectedDateTime = discountCouponObj.DiscountCouponValidFrom;
	 }


	/// <summary>
	/// Sets data to database.
	/// </summary>
	protected void btnOK_Click(object sender, EventArgs e)
	{
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        string errorMessage = new Validator().NotEmpty(txtDiscountCouponDisplayName.Text, ResHelper.GetString("DiscounCoupon_Edit.errorDisplay")).NotEmpty(txtDiscountCouponCode.Text, ResHelper.GetString("DiscounCoupon_Edit.errorCode")).Result;

        // Discount coupon must be positive number
        if (errorMessage == "")
        {
            if (!ValidationHelper.IsPositiveNumber(txtDiscountCouponValue.Text.Trim()))
            {
                errorMessage = ResHelper.GetString("DiscountCoupon_Edit.errorValue");
            }
        }

        // from/to date validation
        if (errorMessage == "")
        {
            if ((dtPickerDiscountCouponValidFrom.SelectedDateTime != DateTime.MinValue) &&
            (dtPickerDiscountCouponValidTo.SelectedDateTime != DateTime.MinValue) &&
            (dtPickerDiscountCouponValidFrom.SelectedDateTime >= dtPickerDiscountCouponValidTo.SelectedDateTime))
            {
                errorMessage = ResHelper.GetString("General.DateOverlaps");
            }
        }

		if (errorMessage == "")
		{
			
			// discountCouponCode must to be unique
			DiscountCouponInfo discountCouponObj = DiscountCouponInfoProvider.GetDiscountCouponInfo(txtDiscountCouponCode.Text.Trim());

			// if discountCouponCode value is unique														
			if ((discountCouponObj == null) || (discountCouponObj.DiscountCouponID == discountid))			
			{
				// if discountCouponCode value is unique -> determine whether it is update or insert 
				if ((discountCouponObj == null))
				{
					// get DiscountCouponInfo object by primary key
					discountCouponObj = DiscountCouponInfoProvider.GetDiscountCouponInfo(discountid);
					if (discountCouponObj == null)
					{
						// create new item -> insert
						discountCouponObj = new DiscountCouponInfo();                        
					}			        									
				}
															
                discountCouponObj.DiscountCouponValue = ValidationHelper.GetDouble(txtDiscountCouponValue.Text.Trim(), 0.0);
                discountCouponObj.DiscountCouponCode = txtDiscountCouponCode.Text.Trim();

                discountCouponObj.DiscountCouponIsFlatValue = true;

                if (radPercentage.Checked)
                {
                    discountCouponObj.DiscountCouponIsFlatValue = false;
                }
 
				discountCouponObj.DiscountCouponDisplayName = txtDiscountCouponDisplayName.Text.Trim();
                
                discountCouponObj.DiscountCouponValidFrom = dtPickerDiscountCouponValidFrom.SelectedDateTime;
                discountCouponObj.DiscountCouponValidTo = dtPickerDiscountCouponValidTo.SelectedDateTime;
                
                DiscountCouponInfoProvider.SetDiscountCouponInfo(discountCouponObj);

				UrlHelper.Redirect("Discount_Edit_General.aspx?discountid=" + Convert.ToString(discountCouponObj.DiscountCouponID) + "&saved=1");
			}
			else
			{
				lblError.Visible = true;
				lblError.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponCodeExists");
			}
		}
		else
		{
			lblError.Visible = true;
			lblError.Text = errorMessage;
		}
	}
}
