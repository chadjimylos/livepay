<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Group_Edit.aspx.cs" Inherits="CMSModules_Groups_Tools_Group_Edit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Groups frameset</title>
</head>

	<frameset border="0" rows="102, *" id="rowsFrameset">
		<frame name="menu" src="Group_Edit_Header.aspx?groupID=<%=Request.QueryString["groupID"] %> " scrolling="no" frameborder="0" noresize="noresize" />		
	    <frame name="content" src="Group_Edit_General.aspx?groupID=<%=Request.QueryString["groupID"]%> " frameborder="0" noresize="noresize" />
	<noframes>
		<body>
		 <p id="p2">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		 </p>
		</body>
	</noframes>
</frameset>
</html>
