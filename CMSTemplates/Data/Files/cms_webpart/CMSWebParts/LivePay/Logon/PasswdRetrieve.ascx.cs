using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.ExtendedControls;
using CMS.DataEngine;
using CMS.EmailEngine;
using CMS.SiteProvider;
using CMS.EventLog;
using CMS.URLRewritingEngine;
using CMS.MembershipProvider;
using CMS.PortalEngine;

public partial class CMSWebParts_LivePay_Logon_PasswdRetrieve : CMSAbstractWebPart
{
    #region "Private properties"

    //private string mDefaultTargetUrl = "";

    #endregion
    
    #region "Public properties"

    /// <summary>
    /// Gets or sets the sender e-mail (from)
    /// </summary>
    public string SendEmailFrom
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("SendEmailFrom"), SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSSendPasswordEmailsFrom"));
        }
        set
        {
            this.SetValue("SendEmailFrom", value);
        }
    }


    ///// <summary>
    ///// Gets or sets the default target url (rediredction when the user is logged in)
    ///// </summary>
    //public string DefaultTargetUrl
    //{
    //    get
    //    {
    //        return ValidationHelper.GetString(this.GetValue("DefaultTargetUrl"), mDefaultTargetUrl);
    //    }
    //    set
    //    {
    //        this.SetValue("DefaultTargetUrl", value);
    //        this.mDefaultTargetUrl = value;
    //    }
    //}

    ///// <summary>
    ///// Label HeaderTitle
    ///// </summary>
    //public string HeaderTitle
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("HeaderTitle"), "");
    //    }
    //    set
    //    {
    //        this.SetValue("HeaderTitle", value);
    //    }
    //}

    /// <summary>
    /// PasswdRetrieval Text
    /// </summary>
    public string PasswdRetrievalText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("PasswdRetrievalText"), "");
        }
        set
        {
            this.SetValue("PasswdRetrievalText", value);
        }
    }

    /// <summary>
    /// PasswdRetrieval Button
    /// </summary>
    public string PasswdRetrievalButton
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("PasswdRetrievalButton"), "");
        }
        set
        {
            this.SetValue("PasswdRetrievalButton", value);
        }
    }
    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            this.rqValue.Visible = false;
            // Do not process
        }
        else
        {
            if (!CMSContext.CurrentUser.IsAuthenticated())
            {
                lblPasswdRetrieval.Text = PasswdRetrievalText;
                Panel1.Visible = true;
                rqValue.ErrorMessage = ResHelper.GetString("LogonForm.rqValue");
                btnPasswdRetrieval.Text = PasswdRetrievalButton;

                btnPasswdRetrieval.Click += new EventHandler(btnPasswdRetrieval_Click);
                CMSBtnSendAns.Click += new EventHandler(btnSendAns_Click);
                rqValue.ErrorMessage = "�������� �� email ���";
                if (!RequestHelper.IsPostBack())
                {
                    //Login1.UserName = ValidationHelper.GetString(Request.QueryString["username"], "");
                    // Set SkinID properties
                    //if (!this.StandAlone && (this.PageCycle < PageCycleEnum.Initialized) && (ValidationHelper.GetString(this.Page.StyleSheetTheme, "") == ""))
                    //{
                        //SetSkinID(this.SkinID);
                    //}
                }
            }
            else
            {
                // Set HeaderTitleLogout text
                //lblHeaderTitleLogout.Text = HeaderTitleLogout;

                Panel1.Visible = false;
                //Panel_Logout.Visible = true;
                //lblFullName.Text = CMSContext.CurrentUser.FullName;
            }
        }
    }

    ///// <summary>
    ///// OnLoad override (show hide password retrieval)
    ///// </summary>
    //protected override void OnLoad(EventArgs e)
    //{
    //    base.OnLoad(e);

    //    if (hdnPasswDisplayed.Value == "")
    //    {
    //        this.pnlPasswdRetrieval.Attributes.Add("style", "display:none;");
    //    }
    //    else
    //    {
    //        this.pnlPasswdRetrieval.Attributes.Add("style", "display:block;");
    //    }
    //}

    /// <summary>
    /// Retrieve the user password
    /// </summary>
    /// 
    

   void btnSendAns_Click(object sender, EventArgs e)
   {
       string value = txtPasswordRetrieval.Text.Trim();
       if (UserInfoProvider.GetUserInfo(value) != null)
           {
               UserInfo usr = UserInfoProvider.GetUserInfo(value);
               QuestDiv.Style.Add("display", "");
               txtPasswordRetrieval.Style.Add("display", "none");
               lblPasswdRetrieval.Style.Add("display", "none");
               int QuestID = Convert.ToInt32((usr.GetValue("UserQuestions").ToString()));

               DataTable dt = DBConnection.GetSecQuestion(QuestID);
               if (dt.Rows.Count > 0)
               {
                   quest.InnerHtml = ResHelper.LocalizeString("{$=" + dt.Rows[0]["QuestGR"].ToString() + "|en-us=" + dt.Rows[0]["QuestEN"].ToString() + "$}");
               }
               lblResult.Visible = false;
               btnPasswdRetrieval.Visible = false;
               EmailDiv.Visible = false;

               string Answertxt = txtAnswer.Text.ToLower();
               if (Answertxt == usr.GetValue("UserAnswers").ToString().ToLower())
               {
                   lblResult.Text = UserInfoProvider.ForgottenEmailRequest(value, CMSContext.CurrentSiteName, "LOGONFORM", this.SendEmailFrom, CMSContext.CurrentResolver);
                   lblResult.Text = ResHelper.LocalizeString("{$=� ���� ��� ������� ���� ������ ���� ���������� ���|en-us=� ���� ��� ������� ���� ������ ���� ���������� ���$}"); 
                   //� ���� ��� ������� ���� ������ ���� ���������� ���
                   lblResult.Style.Add("color", "green");
                   lblResult.Visible = true;
                   QuestDiv.Style.Add("display", "none");
                   QuestAnsDescr.Visible = false;
               }
               else
               {
                   QuestAnsDescr.Visible = true;
                   lblEroorAns.Text = "<font color='red'>" + ResHelper.LocalizeString("{$=� �������� ��� ��� ����� �����|en-us=� �������� ��� ��� ����� �����$}") + "</font>";
                   lblEroorAns.Visible = true;
               }

           }

       }



    void btnPasswdRetrieval_Click(object sender, EventArgs e)
    {
        string value = txtPasswordRetrieval.Text.Trim();
        
        if (value != String.Empty)
        {
            //---- GetUserByEmail
            if (UserInfoProvider.GetUserInfo(value) != null)
            {
                QuestAnsDescr.Visible = true;
                UserInfo usr = UserInfoProvider.GetUserInfo(value);
                QuestDiv.Style.Add("display", "");
                txtPasswordRetrieval.Style.Add("display", "none");
                lblPasswdRetrieval.Style.Add("display", "none");

                int QuestID = Convert.ToInt32((usr.GetValue("UserQuestions").ToString()));

                DataTable dt = DBConnection.GetSecQuestion(QuestID);
                if (dt.Rows.Count > 0)
                {
                    quest.InnerHtml = ResHelper.LocalizeString("{$=" + dt.Rows[0]["QuestGR"].ToString() + "|en-us=" + dt.Rows[0]["QuestEN"].ToString() + "$}");
                }
                lblResult.Visible = false;
                btnPasswdRetrieval.Visible = false;
                EmailDiv.Visible = false;
            }
            else
            {
                QuestDiv.Style.Add("display", "none");
                lblResult.Visible = true;
                lblResult.Text = "<font color='red'>" + ResHelper.LocalizeString("{$=��� ������� �������������  ������� �� ����  �� email|en-us=��� ������� �������������  ������� �� ����  �� email$}") + "</font>"; 
            }
            

            

            //this.pnlPasswdRetrieval.Attributes.Add("style", "display:block;");            
        }
    }
}
