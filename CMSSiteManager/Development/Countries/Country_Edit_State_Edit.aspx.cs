using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Countries_Country_Edit_State_Edit : SiteManagerPage
{
	protected int stateID = 0;
    protected int countryID = 0;
	
	protected void Page_Load(object sender, EventArgs e)
	{
				
		// control initializations				
		lblStateName.Text = ResHelper.GetString("Country_State_Edit.StateNameLabel");
		lblStateCode.Text = ResHelper.GetString("Country_State_Edit.StateCodeLabel");
		lblStateDisplayName.Text = ResHelper.GetString("Country_State_Edit.StateDisplayNameLabel");
        rfvStateDisplayName.ErrorMessage = ResHelper.GetString("Country_Edit_State_Edit.ErrorEmptyStateDisplayName");
        rfvStateName.ErrorMessage = ResHelper.GetString("Country_Edit_State_Edit.ErrorEmptyStateCodeName");
        rfvStateCode.ErrorMessage = ResHelper.GetString("Country_Edit_State_Edit.ErrorEmptyStateCode");
        
		btnOk.Text = ResHelper.GetString("General.OK");	
        
		string currentState = ResHelper.GetString("Country_State_Edit.NewItemCaption");

        // get parematers from url
        stateID = QueryHelper.GetInteger("stateID", 0);
        countryID = QueryHelper.GetInteger("countryID", 0);        
		
        if (stateID > 0)
		{
			StateInfo stateObj = StateInfoProvider.GetStateInfo(stateID);
            if (stateObj != null)
            {
                currentState = ResHelper.LocalizeString(stateObj.StateDisplayName);

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(stateObj);

                    // show that the state was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
		}

		// initializes page title control		
		string[,] pageTitleTabs = new string[2, 3];
		pageTitleTabs[0, 0] = ResHelper.GetString("Country_State_Edit.ItemListLink");
		pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/Countries/Country_Edit_State_List.aspx?countryID=" + countryID;
		pageTitleTabs[0, 2] = "";
		pageTitleTabs[1, 0] = currentState;
		pageTitleTabs[1, 1] = "";
		pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.HelpTopicName = "newedit_state";
        this.CurrentMaster.Title.HelpName = "helpTopic";
	}


	/// <summary>
	/// Load data of editing state.
	/// </summary>
	/// <param name="stateObj">State object.</param>
	protected void LoadData(StateInfo stateObj)
	{
	
		txtStateName.Text = stateObj.StateName;
		txtStateCode.Text = stateObj.StateCode;
		txtStateDisplayName.Text = stateObj.StateDisplayName;
	 }


	/// <summary>
	/// Sets data to database.
	/// </summary>
	protected void btnOK_Click(object sender, EventArgs e)
	{		
        // finds whether required fields are not empty
        string errorMessage = new Validator().NotEmpty(txtStateDisplayName.Text, rfvStateDisplayName.ErrorMessage).NotEmpty(txtStateName.Text, rfvStateName.ErrorMessage).NotEmpty(txtStateCode.Text, rfvStateCode.ErrorMessage)
            .IsCodeName(txtStateName.Text, ResHelper.GetString("general.invalidcodename"))
            .Result;

		if (errorMessage == "")
		{
			
			// stateName must to be unique
			StateInfo stateObj = StateInfoProvider.GetStateInfo(txtStateName.Text.Trim());

			// if stateName value is unique														
			if ((stateObj == null) || (stateObj.StateID == stateID))			
			{
				// if stateName value is unique -> determine whether it is update or insert 
				if ((stateObj == null))
				{
					// get StateInfo object by primary key
					stateObj = StateInfoProvider.GetStateInfo(stateID);
					if (stateObj == null)
					{
						// create new item -> insert
						stateObj = new StateInfo();
                        stateObj.CountryID = countryID;
					}			        									
				}
															
				stateObj.StateName = txtStateName.Text.Trim();
				stateObj.StateCode = txtStateCode.Text.Trim();
				stateObj.StateDisplayName = txtStateDisplayName.Text.Trim();
				
				StateInfoProvider.SetStateInfo(stateObj);

				UrlHelper.Redirect("Country_Edit_State_Edit.aspx?countryID=" + countryID + "&stateID=" + Convert.ToString(stateObj.StateID) + "&saved=1");
			}
			else
			{
				lblError.Visible = true;
				lblError.Text = ResHelper.GetString("Country_State_Edit.StateNameExists");
			}
		}
		else
		{
			lblError.Visible = true;
			lblError.Text = errorMessage;
		}
	}
}
