<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Widgets/WidgetActions.ascx.cs"
    Inherits="CMSWebParts_Widgets_WidgetActions" %>
<asp:Panel runat="server" ID="pnlWidgetActions">
    <asp:HyperLink ID="lnkAddWidget" NavigateUrl="#" CssClass="AddWidget" runat="server"
        EnableViewState="false" />
    <asp:LinkButton ID="btnReset" runat="server" CssClass="ResetWidget" EnableViewState="false" />
</asp:Panel>
