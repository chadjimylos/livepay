﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.Controls
Imports System.Net
Imports System.IO
Imports System.Text

Partial Class CMSTemplates_AegeanPower_DownLoad
    Inherits System.Web.UI.Page

    Protected ReadOnly Property MyFile() As String
        Get
            Dim sMyFile As String = String.Empty
            If Not String.IsNullOrEmpty(Request("File")) Then
                sMyFile = Request("File")
            End If
            Return sMyFile
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.MyFile <> String.Empty Then
            Try
                Dim wr As HttpWebRequest = CType(WebRequest.Create("http://79.129.50.253/pdf/" & MyFile), HttpWebRequest)
                wr.Timeout = 120000
                wr.KeepAlive = False
                Dim ws As HttpWebResponse = CType(wr.GetResponse(), HttpWebResponse)
                Dim str As Stream = ws.GetResponseStream()
                Dim inBuf(1000000) As Byte
                Dim bytesToRead As Integer = CInt(inBuf.Length)
                Dim bytesRead As Integer = 0
                While bytesToRead > 0
                    Dim n As Integer = str.Read(inBuf, bytesRead, bytesToRead)
                    If n = 0 Then
                        Exit While
                    End If
                    bytesRead += n
                    bytesToRead -= n
                End While
                With Response
                    .Clear()
                    .ContentType = "application/octet-stream"
                    .AddHeader("content-disposition", _
                      String.Format("attachment; filename={0}", MyFile))
                    .BinaryWrite(inBuf)
                    .End()
                End With
                str.Close()
            Catch ex As Exception
                Response.Write(ex.Message)
                Response.End()
            End Try
        Else
            Response.Write("File cannot be found!")
            Response.End()
        End If
    End Sub
End Class
