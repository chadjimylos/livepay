<%@ Page Language="C#" AutoEventWireup="true" CodeFile="New.aspx.cs" Inherits="CMSModules_Content_CMSDesk_New_New"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Content - New" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>


<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <asp:Label ID="lblInfo" runat="server" CssClass="ContentLabel" EnableViewState="false"
        Font-Bold="true" /><br />
    <asp:Label ID="lblError" runat="server" CssClass="ContentLabel" ForeColor="Red" EnableViewState="false" />
    <br />
    <div class="ContentNewClasses UniGridClearPager">
        <cms:UniGrid runat="server" ID="gridClasses" GridName="new.xml" IsLiveSite="false" />
    </div>
    <br />
    <asp:Panel runat="server" ID="pnlFooter" CssClass="PageSeparator">
        <asp:HyperLink runat="server" ID="lnkNewLink" CssClass="ContentNewLink" EnableViewState="false">
            <asp:Image ID="imgNewLink" runat="server" Width="16" EnableViewState="false" />
            <asp:Label ID="lblNewLink" runat="server" EnableViewState="false" />
        </asp:HyperLink>
    </asp:Panel>

    <script type="text/javascript" src="New.js"></script>

</asp:Content>
