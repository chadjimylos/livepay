using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;
using System.Data.SqlClient;
using System.Collections.Generic;

public partial class CMSFormControls_HOL : LivePayFormEngineUserControl
{
    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.HOLcode.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (IsEmpty())
            {
                return "keno";
            }
            return this.HOLcode.Text;
        }
        set
        {
            this.HOLcode.Text = (string)value;
        }
    }


    /// <summary>
    /// Clears current value
    /// </summary>
    //public void Clear()
    //{
    //    IDTransaction.Text = "";
    //}


    /// <summary>
    /// Returns true if the number is empty
    /// </summary>
    public bool IsEmpty()
    {
        return (DataHelper.IsEmpty(this.HOLcode.Text));
    }


    /// <summary>
    /// Returns true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        if (IsEmpty() && this.Mandatory)
        {
            return false;
        }

        Validator val = new Validator();
        HOLcode.Text = Right("0000000000000" + HOLcode.Text, 13);
        string result = val.IsRegularExp(HOLcode.Text, @"(9|1){1}\d{12}", "error").Result;

        if (result != "")
        {
            return false;
        }

        List<int> list = new List<int>();

        //� ���������� ����������� ��� ������ ����� ����� � ����: 
        //���������� ��� ��������, ���������������� ���� ��� ��� �� ������ ����� ����� ��� ��� ������ ��� ����� ��� 
        //��� ���������� �� ��������� ��������. 
        for (int i = 0; i <= HOLcode.Text.Length - 2; i++)
        {
            list.Add(Convert.ToInt32(HOLcode.Text.Substring(i, 1)) * (i + 1));
        }

        //�� check digit ����� �� ����� ��� ������� ��� ������� �����������.
        int sum = 0;
        list.ForEach(delegate(int name)
        {
            sum += name;
        });

        int cd = Convert.ToInt16(Right(sum.ToString(), 1));
        int LastNum = Convert.ToInt16(Right(HOLcode.Text, 1));

        if (cd == LastNum)
            return true;
        else
            return false;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomCloud_div.InnerHtml = string.Concat("<a id='CustomCloud_", this.RowCustomField, "' href='#'></a>");

        cv_HOLcode.Attributes.Add("ControlValidated", HOLcode.ClientID);
        cv_HOLcode.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", this.RowCustomField));
        cv_HOLcode.Attributes.Add("CustomValidatorErrorMessage", this.ValidationError);        
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }
}

