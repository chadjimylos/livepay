<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Membership/Users/UsersViewer.ascx.cs"
    Inherits="CMSWebParts_Membership_Users_UsersViewer" %>
<%@ Register Src="~/CMSWebparts/Membership/Users/UsersFilter_files/UsersFilterControl.ascx"
    TagName="UsersFilterControl" TagPrefix="uc1" %>
<uc1:UsersFilterControl ID="filterUsers" runat="server" />
<cms:BasicRepeater ID="repUsers" runat="server" />
<cms:UsersDataSource ID="srcUsers" runat="server" />
<div class="Pager">
    <cms:UniPager ID="pagerElem" runat="server" />
</div>
