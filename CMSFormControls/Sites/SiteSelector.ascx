<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteSelector.ascx.cs"
    Inherits="CMSFormControls_Sites_SiteSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server" RenderMode="InLine">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" ObjectType="cms.site" ResourcePrefix="siteselect" />        
    </ContentTemplate>
</cms:CMSUpdatePanel>
