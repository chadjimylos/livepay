﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;

using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.FileManager;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.Staging;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;
using CMS.MediaLibrary;

using TreeNode = CMS.TreeEngine.TreeNode;
using CMS.DirectoryUtilities;

/// <summary>
/// Image editor for media files
/// </summary>
public partial class CMSModules_MediaLibrary_Controls_MediaLibrary_ImageEditor_Control : CMSUserControl
{
    #region "Variables"

    private Guid mediafileGuid = Guid.Empty;
    private MediaFileInfo mfi = null;
    private string mCurrentSiteName = null;
    private int siteId = 0;
    private bool isPreview = false;
    private string previewPath = null;
    private byte[] previewFile = null;
    private string oldPreviewExt = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Returns the site name from query string 'sitename' or 'siteid' if present, otherwise CMSContext.CurrentSiteName.
    /// </summary>
    private string CurrentSiteName
    {
        get
        {
            if (mCurrentSiteName == null)
            {
                mCurrentSiteName = QueryHelper.GetString("sitename", CMSContext.CurrentSiteName);

                siteId = QueryHelper.GetInteger("siteid", 0);

                SiteInfo site = SiteInfoProvider.GetSiteInfo(siteId);
                if (site != null)
                {
                    mCurrentSiteName = site.SiteName;
                }
            }
            return mCurrentSiteName;
        }
    }

    #endregion


    #region "Events"

    /// <summary>
    /// Loads image type from querystring.
    /// </summary>
    void baseImageEditor_LoadImageType()
    {
        if (mediafileGuid != Guid.Empty)
        {
            baseImageEditor.AttUrl = "~/CMSPages/GetMediaFile.aspx?fileguid=" + mediafileGuid + "&sitename=" + this.CurrentSiteName;

            // Load preview image if preview is edited
            if (isPreview)
            {
                baseImageEditor.IsPreview = true;
                baseImageEditor.AttUrl = UrlHelper.UpdateParameterInUrl(baseImageEditor.AttUrl, "preview", "1");
            }
            baseImageEditor.ImageType = ImageHelper.ImageTypeEnum.MediaFile;
        }
    }


    /// <summary>
    /// Initializes common properties used for processing image. 
    /// </summary>
    void baseImageEditor_InitializeProperties()
    {
        CurrentUserInfo currentUser = CMSContext.CurrentUser;

        // Process media file
        if (baseImageEditor.ImageType == ImageHelper.ImageTypeEnum.MediaFile)
        {

            // Get mediafile
            mfi = MediaFileInfoProvider.GetMediaFileInfo(mediafileGuid, this.CurrentSiteName);
            // If file is not null 
            if (mfi != null)
            {
                MediaLibraryInfo mli = MediaLibraryInfoProvider.GetMediaLibraryInfo(mfi.FileLibraryID);

                if ((mli != null) && (MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(mli, "filemodify")))
                {
                    // Load media file thumbnail
                    if (isPreview)
                    {
                        previewPath = MediaFileInfoProvider.GetPreviewFilePath(mfi);
                        if (previewPath != null)
                        {
                            oldPreviewExt = Path.GetExtension(previewPath);
                            previewFile = File.ReadAllBytes(previewPath);
                            baseImageEditor.ImgHelper = new ImageHelper(previewFile);
                        }
                        else
                        {
                            baseImageEditor.LoadingFailed = true;
                            baseImageEditor.LblLoadFailed.ResourceString = "img.errors.loading";
                        }
                    }
                    // Load media file
                    else
                    {
                        mfi.FileBinary = MediaFileInfoProvider.GetFile(mfi, mli.LibraryFolder, this.CurrentSiteName);
                        // Ensure metafile binary data
                        if (mfi.FileBinary != null)
                        {
                            baseImageEditor.ImgHelper = new ImageHelper(mfi.FileBinary);
                        }
                        else
                        {
                            baseImageEditor.LoadingFailed = true;
                            baseImageEditor.LblLoadFailed.ResourceString = "img.errors.loading";
                        }
                    }
                }
                else
                {
                    baseImageEditor.LoadingFailed = true;
                    baseImageEditor.LblLoadFailed.ResourceString = "img.errors.filemodify";
                }
            }
            else
            {
                baseImageEditor.LoadingFailed = true;
                baseImageEditor.LblLoadFailed.ResourceString = "img.errors.loading";
            }
        }

        // Check that image is in supported formats
        if ((!baseImageEditor.LoadingFailed) && (baseImageEditor.ImgHelper.ImageFormatToString() == null))
        {
            baseImageEditor.LoadingFailed = true;
            baseImageEditor.LblLoadFailed.ResourceString = "img.errors.format";
        }
    }


    /// <summary>
    /// Initialize labels according to current image type.
    /// </summary>
    void baseImageEditor_InitializeLabels(bool reloadName)
    {
        double doubleValue = 0;

        //Initialize strings depending on image type
        if (baseImageEditor.ImageType == ImageHelper.ImageTypeEnum.MediaFile)
        {
            // Initialize media file thumbnail
            if (isPreview && (previewFile != null) && !String.IsNullOrEmpty(previewPath))
            {
                baseImageEditor.TxtFileName.Text = Path.GetFileName(previewPath);
                baseImageEditor.LblExtensionValue.Text = Path.GetExtension(previewPath);
                doubleValue = previewFile.Length / (double)1024;
                baseImageEditor.LblImageSizeValue.Text = doubleValue.ToString("F");
                baseImageEditor.LblWidthValue.Text = baseImageEditor.ImgHelper.ImageWidth.ToString();
                baseImageEditor.LblHeightValue.Text = baseImageEditor.ImgHelper.ImageHeight.ToString();
            }
            // Initialize regular media file
            else if ((mfi != null) && (mfi.FileBinary != null))
            {
                if (!RequestHelper.IsPostBack() || reloadName)
                {
                    baseImageEditor.TxtFileName.Text = mfi.FileName;
                }
                baseImageEditor.LblExtensionValue.Text = mfi.FileExtension;
                doubleValue = mfi.FileBinary.Length / (double)1024;
                baseImageEditor.LblImageSizeValue.Text = doubleValue.ToString("F");
                baseImageEditor.LblWidthValue.Text = mfi.FileImageWidth.ToString();
                baseImageEditor.LblHeightValue.Text = mfi.FileImageHeight.ToString();
            }
        }
    }


    /// <summary>
    /// Saves modified image data.
    /// </summary>
    /// <param name="name">Image name</param>
    /// <param name="extension">Image extension</param>
    /// <param name="mimetype">Image mimetype</param>
    /// <param name="binary">Image binary data</param>
    /// <param name="width">Image width</param>
    /// <param name="height">Image height</param>
    void baseImageEditor_SaveImage(string name, string extension, string mimetype, byte[] binary, int width, int height)
    {
        SaveImage(name, extension, mimetype, binary, width, height);
    }


    /// <summary>
    /// Returns image name according to image type.
    /// </summary>
    /// <returns>Image name</returns>
    void baseImageEditor_GetName()
    {
        string name = "";
        if (mfi != null)
        {
            name = mfi.FileName;
        }

        string path = this.mfi.FilePath;
        path = path.Remove(path.LastIndexOf(this.mfi.FileName));
        path = path.Replace("\\", "/");
        if (path.Contains("/"))
        {
            path = path.Remove(path.LastIndexOf("/"));
        }
        baseImageEditor.LtlScript.Text += ScriptHelper.GetScript("FolderRefresh('" + path.Replace("\'", "\\'") + "');");
        baseImageEditor.GetNameResult = name;
    }


    /// <summary>
    /// Sets file name according to image type.
    /// </summary>
    /// <param name="name">New name</param>
    void baseImageEditor_SetName(string name)
    {
        name = UrlHelper.GetSafeFileName(name, this.mCurrentSiteName);
        string newName = name + "." + baseImageEditor.CurrentFormat;
        string path = this.mfi.FilePath;
        path = path.Remove(path.LastIndexOf(this.mfi.FileName));
        path = path.Replace("\\", "/");
        if (path.Contains("/"))
        {
            path = path.Remove(path.LastIndexOf("/"));
        }
        baseImageEditor.LtlScript.Text += ScriptHelper.GetScript("FolderRefresh('" + path.Replace("\'", "\\'") + "', '" + newName + "');");
        SaveImage(newName, "", "", null, 0, 0);
        baseImageEditor.PropagateChanges(true);
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        mediafileGuid = QueryHelper.GetGuid("mediafileguid", Guid.Empty);
        isPreview = QueryHelper.GetBoolean("isPreview", false);

        baseImageEditor.LoadImageType += new CMSAdminControls_ImageEditor_BaseImageEditor.OnLoadImageType(baseImageEditor_LoadImageType);
        baseImageEditor.InitializeProperties += new CMSAdminControls_ImageEditor_BaseImageEditor.OnInitializeProperties(baseImageEditor_InitializeProperties);
        baseImageEditor.InitializeLabels += new CMSAdminControls_ImageEditor_BaseImageEditor.OnInitializeLabels(baseImageEditor_InitializeLabels);
        baseImageEditor.SaveImage += new CMSAdminControls_ImageEditor_BaseImageEditor.OnSaveImage(baseImageEditor_SaveImage);
        baseImageEditor.GetName += new CMSAdminControls_ImageEditor_BaseImageEditor.OnGetName(baseImageEditor_GetName);
        baseImageEditor.SetName += new CMSAdminControls_ImageEditor_BaseImageEditor.OnSetName(baseImageEditor_SetName);
    }



    /// <summary>
    /// Saves modified image data.
    /// </summary>
    /// <param name="name">Image name</param>
    /// <param name="extension">Image extension</param>
    /// <param name="mimetype">Image mimetype</param>
    /// <param name="binary">Image binary data</param>
    /// <param name="width">Image width</param>
    /// <param name="height">Image height</param>
    private void SaveImage(string name, string extension, string mimetype, byte[] binary, int width, int height)
    {
        // Process media file
        if (mfi != null)
        {
            MediaLibraryInfo mli = MediaLibraryInfoProvider.GetMediaLibraryInfo(mfi.FileLibraryID);
            if (mli != null)
            {
                string path = Path.GetDirectoryName(MediaLibraryInfoProvider.GetMediaLibraryFolderPath(mli.LibraryID) + "\\" + mfi.FilePath);
                bool permissionsOK = DirectoryHelper.CheckPermissions(path, false, true, true, true);

                if (permissionsOK)
                {
                    try
                    {
                        if (isPreview && !String.IsNullOrEmpty(previewPath))
                        {
                            // Delete old preview files with thumbnails
                            MediaFileInfoProvider.DeleteMediaFilePreview(CMSContext.CurrentSiteName, mli.LibraryID, mfi.FilePath, false);
                            MediaFileInfoProvider.DeleteMediaFilePreviewThumbnails(mfi);

                            // Change path if extension changed
                            if (!String.IsNullOrEmpty(extension) && (extension != oldPreviewExt))
                            {
                                previewPath = previewPath.Remove(previewPath.LastIndexOf(oldPreviewExt));
                                previewPath += extension;
                            }

                            // Save updated file
                            FileStream stream = File.Create(previewPath);
                            stream.Write(binary, 0, binary.Length);
                            stream.Close();

                            SiteInfo si = SiteInfoProvider.GetSiteInfo(mfi.FileSiteID);
                            if (si != null)
                            {
                                // Log synchronization task
                                TaskInfoProvider.LogSynchronization(mfi, TaskTypeEnum.UpdateObject, mfi.Connection, si.SiteName);
                            }
                        }
                        else
                        {
                            string newExt = null;
                            string newName = null;
                            if (!String.IsNullOrEmpty(extension))
                            {
                                newExt = extension;
                            }
                            if (!String.IsNullOrEmpty(mimetype))
                            {
                                mfi.FileMimeType = mimetype;
                            }
                            if (width > 0)
                            {
                                mfi.FileImageWidth = width;
                            }
                            if (height > 0)
                            {
                                mfi.FileImageHeight = height;
                            }
                            if (binary != null)
                            {
                                mfi.FileBinary = binary;
                                mfi.FileSize = binary.Length;
                            }
                            // Test all parameters to empty values and update new value if available
                            if (!String.IsNullOrEmpty(name))
                            {
                                newName = name.Substring(0, name.LastIndexOf("."));
                            }
                            // If filename changed move preview file and remove all ald thumbnails
                            if ((!String.IsNullOrEmpty(newName) && (mfi.FileName != newName)) || (!String.IsNullOrEmpty(newExt) && (mfi.FileExtension != newExt)))
                            {
                                SiteInfo si = SiteInfoProvider.GetSiteInfo(mfi.FileSiteID);
                                if (si != null)
                                {
                                    string fileName = (newName != null ? newName : mfi.FileName);
                                    string fileExt = (newExt != null ? newExt : mfi.FileExtension);

                                    // Remove old thumbnails
                                    MediaFileInfoProvider.DeleteMediaFileThumbnails(mfi);
                                    MediaFileInfoProvider.DeleteMediaFilePreviewThumbnails(mfi);

                                    // Rename original preview file if exists
                                    string filePreviewPath = MediaFileInfoProvider.GetPreviewFilePath(mfi);
                                    if (File.Exists(filePreviewPath))
                                    {
                                        string filePreviewExt = Path.GetExtension(filePreviewPath);
                                        string newPreviewPath = Path.GetDirectoryName(filePreviewPath) + "\\" + MediaLibraryHelper.GetPreviewFileName(fileName, fileExt, filePreviewExt, si.SiteName);
                                        File.Move(filePreviewPath, newPreviewPath);
                                    }

                                    // Remove original media file
                                    string filePath = MediaFileInfoProvider.GetMediaFilePath(mfi.FileLibraryID, mfi.FilePath);
                                    if (File.Exists(filePath))
                                    {
                                        File.Delete(filePath);
                                    }

                                    // Set new file name or extension
                                    mfi.FileName = fileName;
                                    mfi.FileExtension = fileExt;
                                }
                            }
                            else
                            {
                                // Remove old thumbnails
                                MediaFileInfoProvider.DeleteMediaFileThumbnails(mfi);

                                // Remove original media file before save
                                string filePath = MediaFileInfoProvider.GetMediaFilePath(mfi.FileLibraryID, mfi.FilePath);
                                if (File.Exists(filePath))
                                {
                                    File.Delete(filePath);
                                }
                            }

                            // Save new data
                            MediaFileInfoProvider.SetMediaFileInfo(mfi);
                        }
                    }
                    catch (Exception e)
                    {
                        baseImageEditor.LblLoadFailed.Visible = true;
                        baseImageEditor.LblLoadFailed.ResourceString = "img.errors.processing";
                        ScriptHelper.AppendTooltip(baseImageEditor.LblLoadFailed, e.Message, "help");
                        baseImageEditor.LoadingFailed = true;
                        return;
                    }
                }
            }
        }
    }

    #endregion
}
