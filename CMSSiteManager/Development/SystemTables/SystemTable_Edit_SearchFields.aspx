﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SystemTable_Edit_SearchFields.aspx.cs" Inherits="CMSSiteManager_Development_SystemTables_SystemTable_Edit_SearchFields" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="System table search fields" %>

<%@ Register Src="~/CMSModules/SmartSearch/Controls/Edit/SearchFields.ascx" TagName="SearchFields"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:SearchFields ID="searchFields" IsLiveSite="false" runat="server" />
</asp:Content>
