﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Summary description for AegeanPower_DBConnection
/// </summary>
public class AegeanPower_DBConnection
{
    public static void UpdateTest(int ID, decimal Longitude, decimal Latitude)
    {
        //System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString() 
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();

        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "proc_Wind_Stores_Edit";

        objConnection.Open();

        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;

        objCommand.Parameters.Add("@NodeID", SqlDbType.BigInt).Value = ID;
        objCommand.Parameters.Add("@StoresLongitude", SqlDbType.Decimal).Value = Longitude;
        objCommand.Parameters.Add("@StoresLatitude", SqlDbType.Decimal).Value = Latitude;

        objCommand.ExecuteScalar();

        objConnection.Close();
    }


    public static DataTable SetActiveVatNumbers()
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "AegeanPower_SetActiveVatNumbers"; // onoma procedure
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }


    public static DataTable InserNewAFM(int UserID,String Afm,String ProviderNumber)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "AegeanPower_InserNewAFM"; // onoma procedure
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
        objCommand.Parameters.Add("@Afm", SqlDbType.VarChar,10).Value = Afm;
        objCommand.Parameters.Add("@ProviderNo", SqlDbType.VarChar, 20).Value = ProviderNumber;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable GetNewVatToSendEmail()
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "AegeanPower_GetNewVatToSendEmail"; // onoma procedure
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    

   enum BillsTableType {
       CutomerBills, 
       PagerYears
   };

   public static DataTable GetCustomerBills(String ProviderNo, int BillYear, int ShowLastBill, int TableType)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "AegeanPower_GetBills"; // onoma procedure
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;

        //-- apo edo kai kato vazis tis metablites pou stelneis stin procedure
        objCommand.Parameters.Add("@ProviderNo", SqlDbType.VarChar,50).Value = ProviderNo;
        objCommand.Parameters.Add("@Year", SqlDbType.Int).Value = BillYear;
        objCommand.Parameters.Add("@ShowLastBill", SqlDbType.Bit).Value = ShowLastBill;
        //-- eos edo

        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataSet objDataSet = new DataSet();
        objAdapter.Fill(objDataSet, "CustomerBills");
                    
        DataTable objDataTable = new DataTable();
        if (TableType == 1)
        {
            objDataTable = objDataSet.Tables[0];
        }
        else
        {
            objDataTable = objDataSet.Tables[1];
        }
        
        objConnection.Close();
        return objDataTable;
    }

   public static DataTable GetUserProviders(int UserID)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "AegeanPower_GetUserProviders"; // onoma procedure
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

   public static DataTable GetProviderDetails(String Provider_Number)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "AegeanPower_GetProviderDetails"; // onoma procedure
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        //-- apo edo kai kato vazis tis metablites pou stelneis stin procedure
        objCommand.Parameters.Add("@Provider_Number", SqlDbType.VarChar,20).Value = Provider_Number;
        //-- eos edo
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable; 
        }


   public static DataTable GetAfmByUser(int UserID)
   {
       string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
       SqlConnection objConnection = new SqlConnection(strConnection);
       string query = "AegeanPower_GetAfmByUser"; // onoma procedure
       objConnection.Open();
       SqlCommand objCommand = new SqlCommand(query, objConnection);
       objCommand.CommandType = CommandType.StoredProcedure;
       objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
       SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
       DataTable objDataTable = new DataTable();
       objAdapter.Fill(objDataTable);
       objConnection.Close();
       return objDataTable;
   }

   public static DataTable GetArchiveProvidersByAfm(String Afm, int SelectedPage)
   {
       string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
       SqlConnection objConnection = new SqlConnection(strConnection);
       string query = "AegeanPower_GetArchiveProvidersByAfm"; // onoma procedure
       objConnection.Open();
       SqlCommand objCommand = new SqlCommand(query, objConnection);
       objCommand.CommandType = CommandType.StoredProcedure;
       objCommand.Parameters.Add("@Afm", SqlDbType.VarChar,10).Value = Afm;
       objCommand.Parameters.Add("@PageIndex", SqlDbType.Int).Value = SelectedPage;
       objCommand.Parameters.Add("@PageSize", SqlDbType.Int).Value = 32;
       SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
       DataTable objDataTable = new DataTable();
       objAdapter.Fill(objDataTable);
       objConnection.Close();
       return objDataTable;
   }


   public static DataTable GetHistory(String ProviderNo,int Year,int ViewType)
   {
       string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
       SqlConnection objConnection = new SqlConnection(strConnection);
       string query = "AegeanPower_GetHistory"; // onoma procedure
       objConnection.Open();
       SqlCommand objCommand = new SqlCommand(query, objConnection);
       objCommand.CommandType = CommandType.StoredProcedure;

       objCommand.Parameters.Add("@ProviderNo", SqlDbType.VarChar, 20).Value = ProviderNo;
       objCommand.Parameters.Add("@Year", SqlDbType.Int).Value = Year;
       objCommand.Parameters.Add("@ShowLastBill", SqlDbType.Bit).Value = ViewType;

       SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
       DataTable objDataTable = new DataTable();
       objAdapter.Fill(objDataTable);
       objConnection.Close();
       return objDataTable;
   }

   public static DataTable GetReferenceOfCounts(String ProviderNo,String FullName,String FromDtm,String ToDtm)
   {
       string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
       SqlConnection objConnection = new SqlConnection(strConnection);
       string query = "AegeanPower_GetReferenceOfCounts"; // onoma procedure
       objConnection.Open();
       SqlCommand objCommand = new SqlCommand(query, objConnection);
       objCommand.CommandType = CommandType.StoredProcedure;

       if (ProviderNo != string.Empty)
            objCommand.Parameters.Add("@ProviderNo", SqlDbType.VarChar, 20).Value = ProviderNo;

       if (FullName != string.Empty)
            objCommand.Parameters.Add("@FullName", SqlDbType.VarChar, 250).Value = FullName;

       if (FromDtm != string.Empty)
           objCommand.Parameters.Add("@From", SqlDbType.VarChar,10).Value = FromDtm;

       if (ToDtm != string.Empty)
           objCommand.Parameters.Add("@To", SqlDbType.VarChar, 10).Value = ToDtm;

       SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
       DataTable objDataTable = new DataTable();
       objAdapter.Fill(objDataTable);
       objConnection.Close();
       return objDataTable;
   }

     public static void TransactionLogAdd(String MerchantRef ,String Message ){
        String strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();

        SqlConnection objConnection = new SqlConnection(strConnection);
        objConnection.Open();

        SqlCommand objCommand = new SqlCommand("AegeanPower_AddTransactionLog", objConnection);
        objCommand.Parameters.AddWithValue("@MerchantRef", MerchantRef);
        objCommand.Parameters.AddWithValue("@Message", Message);
        objCommand.CommandType = CommandType.StoredProcedure;

        objCommand.ExecuteNonQuery();
        objConnection.Close();
     }

    public static String GetMerchantRef(String Amount, String PaymentCode ){
        String strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();

        SqlConnection objConnection = new SqlConnection(strConnection);
        objConnection.Open();

        SqlCommand objCommand = new SqlCommand("AegeanPower_GetMerchantRef", objConnection);
        objCommand.Parameters.AddWithValue("@PaymentCode",PaymentCode);
        objCommand.Parameters.AddWithValue("@Amount", Amount);
        objCommand.CommandType = CommandType.StoredProcedure;

        String MerchantRef = objCommand.ExecuteScalar().ToString();
        objConnection.Close();
        return MerchantRef;
    }
}