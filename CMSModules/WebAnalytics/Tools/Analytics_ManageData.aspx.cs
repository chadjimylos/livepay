using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.WebAnalytics;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.LicenseProvider;
using CMS.SettingsProvider;

public partial class CMSModules_WebAnalytics_Tools_Analytics_ManageData : CMSToolsModalPage
{
    const string VISIT_CODE_NAME = "visitors";
    const string MULTILINGUAL_SUFFIX = ".multilingual";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), "") != "")
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.WebAnalytics);
        }

        CurrentUserInfo user = CMSContext.CurrentUser;

        // Check permissions for CMS Desk -> Tools -> Web analytics tab
        if (!user.IsAuthorizedPerUIElement("CMS.Tools", "WebAnalytics"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Tools", "WebAnalytics");
        }

        // Check 'Read' permission
        if (!user.IsAuthorizedPerResource("CMS.WebAnalytics", "Read"))
        {
            RedirectToCMSDeskAccessDenied("CMS.WebAnalytics", "Read");
        }

        string title = ResHelper.GetString("AnayticsManageData.ManageData");
        this.Page.Title = title;
        this.CurrentMaster.Title.TitleText = title;
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Reporting/managedata.png");

        // Confirmation message for deleting
        string deleteFromToMessage = ScriptHelper.GetString(ResHelper.GetString("webanal.deletefromtomsg"));
        deleteFromToMessage = deleteFromToMessage.Replace("##FROM##", "' + elemFromStr + '");
        deleteFromToMessage = deleteFromToMessage.Replace("##TO##", "' + elemToStr + '");

        string script =
            " var elemTo = document.getElementById('" + pickerTo.ClientID + "_txtDateTime'); " +
            " var elemFrom = document.getElementById('" + pickerFrom.ClientID + "_txtDateTime'); " +
            " var elemToStr = " + ScriptHelper.GetString(ResHelper.GetString("webanal.now")) + "; " +
            " var elemFromStr = " + ScriptHelper.GetString(ResHelper.GetString("webanal.beginning")) + "; " +
            " var deleteAll = 1; " +
            " if (elemTo.value != '') { deleteAll = 0; elemToStr = elemTo.value; }; " +
            " if (elemFrom.value != '') { deleteAll = 0; elemFromStr = elemFrom.value; }; " +
            " if (deleteAll == 1) { return confirm(" + ScriptHelper.GetString(ResHelper.GetString("webanal.deleteall")) + "); } " +
            " else { return confirm(" + deleteFromToMessage + "); }; ";
        btnDelete.OnClientClick = script + ";  return false;";
    }


    public void btnDelete_Click(object sender, EventArgs e)
    {
        // Check 'ManageData' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.WebAnalytics", "ManageData"))
        {
            RedirectToCMSDeskAccessDenied("CMS.WebAnalytics", "ManageData");
        }

        string statCodeName = QueryHelper.GetString("statCodeName", String.Empty);
        if (statCodeName == String.Empty)
        {
            return;
        }

        DateTime fromDate = pickerFrom.SelectedDateTime;
        DateTime toDate = pickerTo.SelectedDateTime;
        bool invalidFromDate = false;
        bool invalidToDate = false;

        // Use min/max values if not selected
        if (fromDate == DateTimeHelper.ZERO_TIME)
        {
            fromDate = new DateTime(1754, 1, 1);
            // Check if user typed invalid date
            invalidFromDate = pickerFrom.DateTimeTextBox.Text.Trim() != "";
        }

        if (toDate == DateTimeHelper.ZERO_TIME)
        {
            toDate = new DateTime(9997, 12, 31);
            // Check if user typed invalid date
            invalidToDate = pickerTo.DateTimeTextBox.Text.Trim() != "";
        }

        if (invalidFromDate)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("analt.invalidfromdate");
            return;
        }

        if (invalidToDate)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("analt.invalidtodate");
            return;
        }

        if (fromDate > toDate)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("analt.invalidinterval");
            return;
        }

        ltlScript.Text = ScriptHelper.GetScript(" window.close(); wopener.RefreshPage(); ");
  
        // Strip timestamp
        fromDate = DateTimeHelper.GetDayStart(fromDate);
        toDate = DateTimeHelper.GetDayStart(toDate).AddDays(1);

        // Calculate time boundary for each interval (week, month, day)
        DateTime week1Start = DateTimeHelper.GetWeekStart(fromDate, AnalyticsHelper.AnalyticsCulture);
        DateTime week1End = week1Start.AddDays(7);
        DateTime week2Start = DateTimeHelper.GetWeekStart(toDate, AnalyticsHelper.AnalyticsCulture);
        DateTime week2End = week2Start.AddDays(7);

        DateTime month1Start = DateTimeHelper.GetMonthStart(fromDate);
        DateTime month1End = month1Start.AddMonths(1);
        DateTime month2Start = DateTimeHelper.GetMonthStart(toDate);
        DateTime month2End = month2Start.AddMonths(1);

        DateTime year1Start = DateTimeHelper.GetYearStart(fromDate);
        DateTime year1End = year1Start.AddYears(1);
        DateTime year2Start = DateTimeHelper.GetYearStart(toDate);
        DateTime year2End = year2Start.AddYears(1);
        
        // Stats for visitors needs special manipulation (it consist of two types
        // of statistics with different code names - new visitor and returning visitor)
        if (statCodeName.ToLower() != HitLogProvider.VISITORS_FIRST)
        {
            // Ingore multilingual suffix (multilingual stats use the same data as "base" stats)
            if (statCodeName.ToLower().EndsWith(MULTILINGUAL_SUFFIX))
            {
                statCodeName = statCodeName.Remove(statCodeName.Length - MULTILINGUAL_SUFFIX.Length);
            }
            // Recalculate/delete ordinary stats
            HitsInfoProvider.DeleteHitsInfos(statCodeName, CMSContext.CurrentSite.SiteID,
               fromDate, toDate,
               week1Start, week1End, week2Start, week2End,
               month1Start, month1End, month2Start, month2End,
               year1Start, year1End, year2Start, year2End);
        }
        else
        {
            // Recalculate/delete visitors stats (two different types - two different code names)
            HitsInfoProvider.DeleteHitsInfos(HitLogProvider.VISITORS_FIRST, CMSContext.CurrentSite.SiteID,
               fromDate, toDate, week1Start, week1End, week2Start, week2End,
               month1Start, month1End, month2Start, month2End, year1Start, year1End, year2Start, year2End);
            HitsInfoProvider.DeleteHitsInfos(HitLogProvider.VISITORS_RETURNING, CMSContext.CurrentSite.SiteID,
               fromDate, toDate, week1Start, week1End, week2Start, week2End,
               month1Start, month1End, month2Start, month2End, year1Start, year1End, year2Start, year2End);
        }
    }
}
