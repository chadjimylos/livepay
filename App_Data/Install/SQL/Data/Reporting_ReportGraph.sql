SET IDENTITY_INSERT [Reporting_ReportGraph] ON
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 13:59:27', 700, 'db6106a1-8b34-411c-93b7-e04a1c9923c5', N'{$reports_filedownloads.label_numofdownloads$}', N'bar', N'graph', 300, 0, N'SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, 
	SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
	) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <=@ToDayFirst))
 GROUP BY IndexID, T1.SubCount
 UNION ALL
SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, 
	SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 55, N'GraphDayFileDownloads', N'{$reports_general.label_days$}', 145, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 14:16:12', 700, '6c560c49-e1d8-4e62-9079-bb11c97d6a88', N'{$reports_filedownloads.label_numofdownloads$}', N'bar', N'graph', 300, 0, N'SELECT IndexMonthName, T1.hits
 FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(month,HitsStartTime) AS months, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(month,HitsStartTime)) AS T1
 ON Analytics_Index.IndexMonthName=T1.months 
 WHERE (IndexID>=1) AND (IndexID<=12)
 ORDER BY IndexID', -1, 56, N'GraphMonthFileDownloads', N'{$reports_general.label_months$}', 146, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080410 17:23:28', 700, '3c6a1506-4f3b-4d03-8d76-b21fc4733e9a', N'{$reports_filedownloads.label_numofdownloads$}', N'bar', N'graph', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 57, N'GraphYearFileDownloads', N'{$reports_general.label_years$}', 147, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 14:09:40', 700, '18dc54ce-39ea-4d3f-8eb7-840db48b7210', N'{$reports_filedownloads.label_numofdownloads$}', N'bar', N'graph', 300, 0, N'SELECT IndexZero, T1.hits
 FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
 ON Analytics_Index.IndexZero=T1.hours 
 WHERE (IndexZero>=0) AND (IndexZero<24) 
 ORDER BY IndexZero', -1, 58, N'GraphHourFileDownloads', N'{$reports_general.label_hours$}', 148, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100216 16:03:00', 700, 'a3f753a8-78e0-4924-93e1-ec72c78adc56', N'{$reports_pageviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <= @ToDayFirst))
 GROUP BY IndexID, T1.SubCount
 UNION ALL
 SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID)
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 59, N'GraphDayPageViews', N'{$reports_general.label_days$}', 149, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100216 17:23:34', 700, 'd1f354c6-8a87-48de-82cc-ab596bf3f7d6', N'{$reports_pageviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexMonthName,
 T1.hits
 FROM Analytics_Index
 LEFT JOIN
    (SELECT DATENAME(month,HitsStartTime) AS months,
    SUM(HitsCount) AS hits 
    FROM Analytics_Statistics
    INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
    WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND
    (StatisticsCode=@CodeName) AND
    (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
    GROUP BY DATENAME(month,HitsStartTime)) AS T1
 ON Analytics_Index.IndexMonthName=T1.months
 WHERE (IndexID>=1) AND (IndexID<=12)
 ORDER BY IndexID', -1, 60, N'GraphMonthPageViews', N'{$reports_general.label_months$}', 150, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080410 17:23:29', 700, '72a50baf-033f-4626-a0c5-f397f1ce19c7', N'{$reports_pageviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 61, N'GraphYearPageViews', N'{$reports_general.label_years$}', 151, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100216 16:15:56', 700, '7cb0c307-688f-4066-aa71-4a57e658f7a8', N'{$reports_pageviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexZero, T1.hits
 FROM Analytics_Index 
 LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
 ON Analytics_Index.IndexZero=T1.hours 
 WHERE (IndexZero>=0) AND (IndexZero<24) 
 ORDER BY IndexZero', -1, 62, N'GraphHoursPageViews', N'{$reports_general.label_hours$}', 152, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:46:45', 700, '257a811a-aa60-4ce8-a2ac-8bdd19f4c03e', N'{$reports_pagenotfound.label_numoffaults$}', N'bar', N'graph', 300, 0, N'SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, 
	SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <=@ToDayFirst))
 GROUP BY IndexID, T1.SubCount
 UNION ALL
 SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, 
	SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 63, N'GraphDayPageNotFound', N'{$reports_general.label_days$}', 153, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:57:04', 700, '374ec976-2161-4cd2-a0b6-ebb7c6fbf6d8', N'{$reports_pagenotfound.label_numoffaults$}', N'bar', N'graph', 300, 0, N'SELECT IndexMonthName, T1.hits
FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(month,HitsStartTime) AS months, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(month,HitsStartTime)) AS T1
ON Analytics_Index.IndexMonthName=T1.months WHERE (IndexID>=1) AND (IndexID<=12)
ORDER BY IndexID', -1, 64, N'GraphMonthPageNotFound', N'{$reports_general.label_months$}', 154, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080410 17:23:29', 700, 'd671eeb9-d52b-4387-9cd2-2f42011363e1', N'{$reports_pagenotfound.label_numoffaults$}', N'bar', N'graph', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 65, N'GraphYearPageNotFound', N'{$reports_general.label_years$}', 155, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:51:54', 700, 'a1933df5-9ace-455c-a1a9-aa3c738321f5', N'{$reports_pagenotfound.label_numoffaults$}', N'bar', N'graph', 300, 0, N'SELECT IndexZero, T1.hits
FROM Analytics_Index 
LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
ON Analytics_Index.IndexZero=T1.hours 
WHERE (IndexZero>=0) AND (IndexZero<24) 
ORDER BY IndexZero', -1, 66, N'GraphHourPageNotFound', N'{$reports_general.label_hours$}', 156, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:26:41', 700, '6d273871-b6ee-4873-8223-efd32a9f67c7', N'{$reports_visits.label_numofvisits$}', N'bar', N'visitors', 300, 0, N'SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, 
	SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND ((StatisticsCode = @FirstCategory) OR (StatisticsCode = @SecondCategory))
	AND (HitsStartTime >= @FromDate)
 AND (HitsEndTime <= @ToDate)
 GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <=@ToDayFirst))
 GROUP BY IndexID, T1.SubCount
UNION ALL
 SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND ((StatisticsCode = @FirstCategory) OR (StatisticsCode = @SecondCategory))
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 71, N'GraphDayVisitors', N'{$reports_general.label_days$}', 157, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:14:34', 700, '9ba5a349-19b2-4182-acbe-a62fdb5a8636', N'', N'pie', N'new and returning visitors', 500, 0, N'DECLARE @myselection TABLE (
   Name varchar(400),
   Count float
)
INSERT INTO @myselection 
	SELECT ''{''+''$analytics_codename.'' + StatisticsCode + ''$}'' AS Name, 
	SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND 
	((StatisticsCode = @FirstCategory) OR (StatisticsCode = @SecondCategory)) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsCode
SELECT Name, 100*Count / NULLIF((SELECT SUM(Count) FROM @myselection),0) FROM @myselection', 5, 71, N'GraphDayNewReturnVisitors', N'', 158, N'{$reports_visits.label_newandreturningvisitors$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:38:37', 700, '22a3e2fe-0360-415b-ada9-1e1d726205c0', N'{$reports_visits.label_numofvisits$}', N'bar', N'visitors', 300, 0, N'SELECT IndexMonthName, T1.hits
FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(month,HitsStartTime) AS months, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND ((StatisticsCode = @FirstCategory) OR (StatisticsCode = @SecondCategory)) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(month,HitsStartTime)) AS T1
ON Analytics_Index.IndexMonthName=T1.months WHERE (IndexID>=1) AND (IndexID<=12)
ORDER BY IndexID', -1, 72, N'GraphMonthVisitors', N'{$reports_general.label_months$}', 159, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:34:28', 700, '8c28e0ee-6518-45f4-80e1-9db5769af627', N'', N'pie', N'new and returning visitors', 500, 0, N'DECLARE @myselection TABLE (
   Name varchar(400),
   Count float
)
INSERT INTO @myselection 
	SELECT ''{''+''$analytics_codename.'' + StatisticsCode + ''$}'' as Name, 
	SUM(HitsCount) as Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND ((StatisticsCode = @FirstCategory) OR (StatisticsCode = @SecondCategory)) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsCode
SELECT Name, 100*Count / NULLIF((SELECT SUM(Count) FROM @myselection),0) FROM @myselection', 5, 72, N'GraphMonthNewReturnVisitors', N'', 160, N'{$reports_visits.label_newandreturningvisitors$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080410 17:23:29', 700, '37e8390a-fb47-4f61-9e7f-e82bb5146a69', N'{$reports_visits.label_numofvisits$}', N'bar', N'visitors', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND ((StatisticsCode = @FirstCategory) OR (StatisticsCode = @SecondCategory)) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 73, N'GraphYearVisitors', N'{$reports_general.label_years$}', 161, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20081021 14:24:03', 700, '3c792e6f-fb55-4b19-8c32-42bba7e185c2', N'', N'pie', N'new and returning visitors', 500, 0, N'DECLARE @myselection TABLE (
   Name varchar(400),
   Count float
)
INSERT INTO @myselection SELECT ''{''+''$analytics_codename.'' + StatisticsCode + ''$}'' as Name, SUM(HitsCount) as Count FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND ((StatisticsCode = @FirstCategory) OR (StatisticsCode = @SecondCategory)) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsCode
SELECT Name, 100*Count / NULLIF((SELECT SUM(Count) FROM @myselection),0) FROM @myselection', 5, 73, N'GraphYearNewReturnVisitors', N'', 162, N'{$reports_visits.label_newandreturningvisitors$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:31:24', 700, '263bc324-ad0c-4535-876d-bbac99ab903f', N'{$reports_visits.label_numofvisits$}', N'bar', N'visitors', 300, 0, N'SELECT IndexZero, T1.hits
FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND ((StatisticsCode=@FirstCategory) OR (StatisticsCode=@SecondCategory))
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
ON Analytics_Index.IndexZero=T1.hours 
WHERE (IndexZero>=0) AND (IndexZero<24) ORDER BY IndexZero', -1, 74, N'GraphHourVisitors', N'{$reports_general.label_hours$}', 163, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:29:50', 700, '4e398ffe-44a5-453b-872c-e51d0d370b83', N'', N'pie', N'new and returning visitors', 500, 0, N'DECLARE @myselection TABLE (
   Name varchar(400),
   Count float
)
INSERT INTO @myselection 
	SELECT ''{''+''$analytics_codename.'' + StatisticsCode + ''$}'' as Name, 
	SUM(HitsCount) as Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND ((StatisticsCode = @FirstCategory) OR (StatisticsCode = @SecondCategory)) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsCode
SELECT Name, 100*Count / NULLIF((SELECT SUM(Count) FROM @myselection),0) FROM @myselection', 5, 74, N'GraphHourNewReturnVisitors', N'', 164, N'{$reports_visits.label_newandreturningvisitors$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 13:16:38', 700, '8d405e91-3d9f-474a-8fb1-d0ed6803ac74', N'{$reports_filedownloads.label_numofdownloads$}', N'bar', N'graph', 300, 0, N'SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
	) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <=@ToDayFirst))
 GROUP BY IndexID, T1.SubCount
 UNION ALL
 SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
	) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 86, N'GraphDayFileDownloadsCulture', N'{$reports_general.label_days$}', 171, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 14:03:43', 700, '8546cc7c-c135-49c7-85e3-6be4fc57b5e0', N'', N'pie', N'graph pie', 500, 0, N'SELECT StatisticsObjectCulture, SUM(HitsCount) 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID  
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate)  AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectCulture ORDER BY SUM(HitsCount) DESC', 5, 86, N'GraphPieDayFileDownloadsCulture', N'', 172, N'{$reports_filedownloadscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 13:49:26', 700, '46a16014-15d0-4cb4-99e5-5adda988edb4', N'{$reports_filedownloads.label_numofdownloads$}', N'bar', N'graph', 300, 0, N'SELECT IndexMonthName, T1.hits
 FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(month,HitsStartTime) AS months, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(month,HitsStartTime)) AS T1
 ON Analytics_Index.IndexMonthName=T1.months WHERE (IndexID>=1) AND (IndexID<=12)
 ORDER BY IndexID', -1, 87, N'GraphMonthFileDownloadsCulture', N'{$reports_general.label_months$}', 173, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 14:06:56', 700, 'b0a6dc65-afb5-4b16-823b-9eb1a77d6b26', N'', N'pie', N'graph pie', 500, 0, N'SELECT StatisticsObjectCulture, SUM(HitsCount) 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectCulture 
 ORDER BY SUM(HitsCount) DESC', 5, 87, N'GraphPieMonthFileDownloadsCulture', N'', 174, N'{$reports_filedownloadscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080410 17:23:30', 700, '595a90f7-0ddb-4e98-89fd-17d2572f375e', N'{$reports_filedownloads.label_numofdownloads$}', N'bar', N'graph', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 88, N'GraphYearFileDownloadsCulture', N'{$reports_general.label_years$}', 175, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080410 17:23:30', 700, '22c20d43-ca15-46ba-8ef5-67a4cf0b22a3', N'', N'pie', N'graph pie', 500, 0, N'SELECT StatisticsObjectCulture, SUM(HitsCount) FROM 
Analytics_Statistics, Analytics_YearHits, View_CMS_Tree_Joined WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (StatisticsObjectID = NodeID) AND (StatisticsObjectCulture = DocumentCulture) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectCulture ORDER BY SUM(HitsCount) DESC', -1, 88, N'GraphPieYearFileDownloadsCulture', N'', 176, N'{$reports_filedownloadscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 13:37:22', 700, 'cbb64120-03c3-4f5f-bbb0-44f8eb76c9b3', N'{$reports_filedownloads.label_numofdownloads$}', N'bar', N'graph', 300, 0, N'SELECT IndexZero, T1.hits
FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
ON Analytics_Index.IndexZero=T1.hours 
WHERE (IndexZero>=0) AND (IndexZero<24) ORDER BY IndexZero', -1, 89, N'GraphHourFileDownloadsCulture', N'{$reports_general.label_hours$}', 177, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 14:05:23', 700, '7f5832e2-aa3a-407e-9819-c1c6ac424dc2', N'', N'pie', N'graph pie', 500, 0, N'SELECT StatisticsObjectCulture, SUM(HitsCount) 
 FROM Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectCulture 
 ORDER BY SUM(HitsCount)', 5, 89, N'GraphPieHourFileDownloadsCulture', N'', 178, N'{$reports_filedownloadscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 10:04:51', 700, '39170999-1a43-4254-be68-7889760c717e', N'{$reports_pageviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexID, T1.SubCount
FROM Analytics_Index 
LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
	) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <=@ToDayFirst))
 GROUP BY IndexID, T1.SubCount
 UNION ALL
 SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
	) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 90, N'GraphDayPageViewsCulture', N'{$reports_general.label_days$}', 179, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 13:56:11', 700, '0349bf51-ad89-430c-b599-4c9cbf4520c3', N'', N'pie', N'graph pie', 500, 0, N'DECLARE @myselection TABLE (
    StatisticsObjectCulture varchar(400),
    Count float
);
INSERT INTO @myselection 
	SELECT StatisticsObjectCulture, SUM(HitsCount) AS Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture 
	ORDER BY SUM(HitsCount) DESC
 SELECT StatisticsObjectCulture, 
 CAST((100*Count/(SELECT SUM(Count) 
 FROM @myselection)) as decimal(10,2)) 
 FROM @myselection', 5, 90, N'GraphPieDayPageViewsCulture', N'', 180, N'{$reports_pageviewscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 09:48:59', 700, 'd43d5146-44b8-46cb-b707-cdd3c72e69bd', N'{$reports_pageviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexMonthName, T1.hits
 FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(month,HitsStartTime) AS months, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	LEFT JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(month,HitsStartTime)) AS T1
 ON Analytics_Index.IndexMonthName=T1.months 
 WHERE (IndexID>=1) AND (IndexID<=12)
 ORDER BY IndexID', -1, 91, N'GraphMonthPageViewsCulture', N'{$reports_general.label_months$}', 181, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 13:53:03', 700, '992c99d2-1983-4fd6-acaa-59505c740a62', N'', N'pie', N'graph pie', 500, 0, N'DECLARE @myselection TABLE (
    StatisticsObjectCulture varchar(400),
    Count float
);
INSERT INTO @myselection 
	SELECT StatisticsObjectCulture, 
	SUM(HitsCount) as Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture 
	ORDER BY SUM(HitsCount) DESC
SELECT StatisticsObjectCulture, 
	CAST(100*Count/(SELECT SUM(Count) FROM @myselection) as decimal(10,2)) 
FROM @myselection', 5, 91, N'GraphPieMonthPageViewsCulture', N'', 182, N'{$reports_pageviewscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080410 17:23:30', 700, 'c5804ad1-7cd4-44c9-b309-3e20ddf71479', N'{$reports_pageviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 92, N'GraphYearPageViewsCulture', N'{$reports_general.label_years$}', 183, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080410 17:23:30', 700, '8d00b358-2f3b-4a64-a4f5-028e02b7e347', N'', N'pie', N'graph pie', 500, 0, N'DECLARE @myselection TABLE (
    StatisticsObjectCulture varchar(400),
    Count float
);
INSERT INTO @myselection SELECT StatisticsObjectCulture, SUM(HitsCount) as Count FROM 
Analytics_Statistics, Analytics_YearHits, View_CMS_Tree_Joined WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (StatisticsObjectID = NodeID) AND (StatisticsObjectCulture = DocumentCulture) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectCulture ORDER BY SUM(HitsCount) DESC
SELECT StatisticsObjectCulture, CAST((100*Count/(SELECT SUM(Count) FROM @myselection)) as decimal(10,2)) FROM @myselection', 5, 92, N'GraphPieYearPageViewsCulture', N'', 184, N'{$reports_pageviewscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 10:23:56', 700, 'fcf96a0a-f51b-437b-a149-2b4d28f534e3', N'{$reports_pageviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexZero, T1.hits
FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
ON Analytics_Index.IndexZero=T1.hours 
WHERE (IndexZero>=0) AND (IndexZero<24) 
ORDER BY IndexZero', -1, 93, N'GraphHourPageViewsCulture', N'{$reports_general.label_hours$}', 185, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 14:00:38', 700, '74772fbe-4835-4aea-a266-d903594e2b32', N'', N'pie', N'graph pie', 500, 0, N'DECLARE @myselection TABLE (
    StatisticsObjectCulture varchar(400),
    Count float
);
INSERT INTO @myselection 
	SELECT StatisticsObjectCulture,
	SUM(HitsCount) AS Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture 
	ORDER BY SUM(HitsCount)
 SELECT StatisticsObjectCulture, 
 CAST((100*Count/(SELECT SUM(Count) 
 FROM @myselection)) as decimal(10,2)) 
 FROM @myselection', 5, 93, N'GraphPieHourPageViewsCulture', N'', 186, N'{$reports_pageviewscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 16:11:13', 700, '7459346d-e455-4944-9a3b-04f92a5ecb4d', N'{$reports_campaign.label_num$}', N'bar', N'graph', 300, 0, N'SELECT IndexID, T1.SubCount
FROM Analytics_Index 
LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, 
	SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <=@ToDayFirst))
 GROUP BY IndexID, T1.SubCount
UNION ALL
 SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, 
	SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics 
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 94, N'GraphDayCampaign', N'{$reports_general.label_days$}', 187, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 10:41:54', 700, '28d6ef49-a8cd-4c2f-8e80-ab68a1846b69', N'{$reports_campaign.label_num$}', N'bar', N'graph', 300, 0, N'SELECT IndexMonthName, T1.hits
FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(month,HitsStartTime) AS months, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(month,HitsStartTime)) AS T1
ON Analytics_Index.IndexMonthName=T1.months WHERE (IndexID>=1) AND (IndexID<=12)
ORDER BY IndexID', -1, 95, N'GraphMonthCampaign', N'{$reports_general.label_months$}', 188, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080724 14:07:42', 700, '68ce06e8-15b2-49fe-9585-b4088b8b092a', N'{$reports_campaign.label_num$}', N'bar', N'graph', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 96, N'GraphYearCampaign', N'{$reports_general.label_years$}', 189, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 10:36:06', 700, '27da4e41-2deb-4f03-9f74-d4d4e7fbb2ce', N'{$reports_campaign.label_num$}', N'bar', N'graph', 300, 0, N'SELECT IndexZero, T1.hits
FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
ON Analytics_Index.IndexZero=T1.hours 
WHERE (IndexZero>=0) AND (IndexZero<24) ORDER BY IndexZero', -1, 97, N'GraphHourCampaign', N'{$reports_general.label_hours$}', 190, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 15:40:32', 700, '330514c1-2b53-4086-bf26-6fae557d8a89', N'{$reports_conversion.label_num$}', N'bar', N'graph', 300, 0, N'SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, 
	SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <=@ToDayFirst))
 GROUP BY IndexID, T1.SubCount
 UNION ALL
 SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 98, N'GraphDayConversion', N'{$reports_general.label_days$}', 191, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 15:48:50', 700, '4ac711b2-afd1-418a-8803-db686a9466fd', N'{$reports_conversion.label_num$}', N'bar', N'graph', 300, 0, N'SELECT IndexMonthName, T1.hits
 FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(month,HitsStartTime) AS months, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(month,HitsStartTime)) AS T1
 ON Analytics_Index.IndexMonthName=T1.months 
 WHERE (IndexID>=1) AND (IndexID<=12)
 ORDER BY IndexID', -1, 99, N'GraphMonthConversion', N'{$reports_general.label_months$}', 192, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080410 17:23:31', 700, 'c7fc7ab7-0eb0-49be-8117-aebe8f6f37e2', N'{$reports_conversion.label_num$}', N'bar', N'graph', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 100, N'GraphYearConversion', N'{$reports_general.label_years$}', 193, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 15:44:19', 700, '5a53e5c7-8e6b-4cd3-ab67-37257704d6af', N'{$reports_conversion.label_num$}', N'bar', N'graph', 300, 0, N'SELECT IndexZero, T1.hits
 FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
 ON Analytics_Index.IndexZero=T1.hours 
 WHERE (IndexZero>=0) AND (IndexZero<24) 
 ORDER BY IndexZero', -1, 101, N'GraphHourConversion', N'{$reports_general.label_hours$}', 194, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080313 09:35:00', 700, 'c7bcf879-5d60-4345-9f14-46044d0847dc', N'{$ecommerce.report_numberoforders$}', N'bar', N'GraphEcommNumOfOrdersPerYear', 400, 0, N'SELECT DATENAME(year, OrderDate) as Year, COUNT(OrderDate) FROM COM_Order
WHERE OrderSiteID = @CMSContextCurrentSiteID
GROUP BY DATENAME(year, OrderDate)
ORDER BY MIN(OrderDate)', -1, 102, N'GraphEcommNumOfOrdersPerYear', N'{$ecommerce.report_year$}', 195, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080428 14:57:11', 700, '236a8656-bb92-4a12-842e-8d71e1a4accf', N'{$ecommerce.report_numberoforders$}', N'bar', N'GraphEcommNumOfOrdersPerMonth', 400, 0, N'SELECT Orders.OrderMonth + '' '' + Orders.OrderYear as MonthYear,
COUNT(Orders.OrderDate) as Count
FROM  (SELECT OrderDate, DATENAME(year, OrderDate) as OrderYear, DATENAME(month, OrderDate) as OrderMonth FROM COM_Order 
WHERE OrderSiteID = @CMSContextCurrentSiteID) as Orders
GROUP BY Orders.OrderYear, Orders.OrderMonth
ORDER BY Min(Orders.OrderDate)', -1, 103, N'GraphEcommNumOfOrdersPerMonth', N'{$ecommerce.report_month$}', 196, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080313 09:35:00', 700, '03d0cbb3-256f-4f36-8f76-503531edadd6', N'{$ecommerce.report_numberoforders$}', N'bar', N'GraphEcommNumOfOrdersPerDay', 400, 0, N'SELECT DATENAME(day,OrderDate), COUNT(OrderDate) as Cnt FROM COM_Order
WHERE @SelectedMonth = DATENAME(year,OrderDate) + '' '' + DATENAME(month,OrderDate)
AND OrderSiteID = @CMSContextCurrentSiteID
GROUP BY DATENAME(day,OrderDate)
ORDER BY MIN(OrderDate)', -1, 104, N'GraphEcommNumOfOrdersPerDay', N'{$ecommerce.report_day$}', 197, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080313 09:35:00', 700, '0cf8a464-3380-4c46-a06c-42ce15000d36', N'{$ecommerce.report_income$}', N'bar', N'GraphEcommSalesPerYear', 400, 0, N'SELECT DATENAME(year, OrderDate) as Year, SUM(OrderTotalPrice) FROM COM_Order
WHERE OrderSiteID = @CMSContextCurrentSiteID
GROUP BY DATENAME(year, OrderDate)
ORDER BY MIN(OrderDate)', -1, 106, N'GraphEcommSalesPerYear', N'{$ecommerce.report_year$}', 198, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080428 15:03:17', 700, 'aa8b9de8-0953-40ac-986e-3c6ecbb312fe', N'{$ecommerce.report_income$}', N'bar', N'GraphEcommSalesPerMonth', 400, 0, N'SELECT  Orders.OrderMonth + '' '' + Orders.OrderYear as OrderMonth, CAST(SUM(Orders.OrderTotalPrice) as decimal(38,2)) as Income 
FROM  (SELECT OrderTotalPrice, OrderDate, DATENAME(year, OrderDate) as OrderYear, DATENAME(month, OrderDate) as OrderMonth FROM COM_Order WHERE OrderSiteID = @CMSContextCurrentSiteID) as Orders
GROUP BY Orders.OrderYear, Orders.OrderMonth
ORDER BY Min(Orders.OrderDate)', -1, 107, N'GraphEcommSalesPerMonth', N'{$ecommerce.report_month$}', 199, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080313 09:35:00', 700, '560fac5c-5e28-4aaa-9cc2-da57e6e30f23', N'{$ecommerce.report_income$}', N'bar', N'GraphEcommSalesPerDay', 400, 0, N'SELECT DATENAME(day,OrderDate), SUM(OrderTotalPrice) as Cnt FROM COM_Order
WHERE @SelectedMonth = DATENAME(year,OrderDate) + '' '' + DATENAME(month,OrderDate) AND
OrderSiteID = @CMSContextCurrentSiteID
GROUP BY DATENAME(day,OrderDate)
ORDER BY MIN(OrderDate)', -1, 108, N'GraphEcommSalesPerDay', N'{$ecommerce.report_day$}', 200, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 10:49:29', 700, 'e2c1cf6c-c36e-4bd4-9983-38306d9c0e65', N'', N'pie', N'graph', 500, 0, N'(SELECT StatisticsObjectName, CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS Count 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode = @CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
 GROUP BY StatisticsObjectName
 HAVING ((100*SUM(HitsCount)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) >= 5))
 UNION
 
 (SELECT ''Others'' AS StatisticsObjectName, CAST(SUM(HlpTbl.SubCount) AS decimal(3)) AS Count 
 FROM 
	(SELECT StatisticsObjectName, 100*CAST(SUM(HitsCount) AS float)/NULLIF((
		SELECT SUM(HitsCount) 
		FROM Analytics_Statistics
		INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY StatisticsObjectName
	HAVING ((100*SUM(HitsCount)/NULLIF((
		SELECT SUM(HitsCount) 
		FROM Analytics_Statistics
		INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) < 5)) AS HlpTbl)
ORDER BY Count', 5, 110, N'GraphDayBrowserType', N'', 201, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 11:52:07', 700, 'd542aa16-7a34-4f62-9d2d-ffe2790a9ed3', N'', N'pie', N'graph', 500, 0, N'(SELECT StatisticsObjectName, CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS Count 
 FROM Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode = @CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
 GROUP BY StatisticsObjectName
 HAVING ((100*SUM(HitsCount)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) >= 5))
	
 UNION
 
 (SELECT ''Others'' AS StatisticsObjectName, 
 CAST(SUM(HlpTbl.SubCount) AS decimal(3)) AS Count 
 FROM 
	(SELECT StatisticsObjectName, 100*CAST(SUM(HitsCount) AS float)/NULLIF((
		SELECT SUM(HitsCount) 
		FROM Analytics_Statistics
		INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY StatisticsObjectName
	HAVING ((100*SUM(HitsCount)/NULLIF((
		SELECT SUM(HitsCount) 
		FROM Analytics_Statistics
		INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) < 5)) AS HlpTbl)
ORDER BY Count', 5, 111, N'GraphHourBrowserType', N'', 202, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100219 12:00:25', 700, 'f245327b-c42d-452d-89f2-9f38e4038d59', N'', N'pie', N'graph', 500, 0, N'(SELECT StatisticsObjectName, CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS Count 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode = @CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
 GROUP BY StatisticsObjectName
 HAVING ((100*SUM(HitsCount)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) >= 5))
	
 UNION
 
 (SELECT ''Others'' AS StatisticsObjectName, CAST(SUM(HlpTbl.SubCount) AS decimal(3)) AS Count 
 FROM 
	(SELECT StatisticsObjectName, 100*CAST(SUM(HitsCount) AS float)/NULLIF((
		SELECT SUM(HitsCount) 
		FROM Analytics_Statistics
		INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY StatisticsObjectName
	HAVING ((100*SUM(HitsCount)/NULLIF((
		SELECT SUM(HitsCount) 
		FROM Analytics_Statistics
		INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) < 5)) AS HlpTbl)
	ORDER BY Count', 5, 112, N'GraphMonthBrowserType', N'', 203, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080910 12:33:43', 700, '638703d8-25ae-43be-9739-633c1cc805d0', N'', N'pie', N'graph', 500, 0, N'(SELECT StatisticsObjectName, CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((SELECT SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS Count 
FROM Analytics_Statistics, Analytics_YearHits 
WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
GROUP BY StatisticsObjectName
HAVING ((100*SUM(HitsCount)/NULLIF((SELECT SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) >= 5))
UNION
(SELECT ''Others'' AS StatisticsObjectName, CAST(SUM(HlpTbl.SubCount) AS decimal(3)) AS Count FROM 
(SELECT StatisticsObjectName, 100*CAST(SUM(HitsCount) AS float)/NULLIF((SELECT SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS SubCount
FROM Analytics_Statistics, Analytics_YearHits 
WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
GROUP BY StatisticsObjectName
HAVING ((100*SUM(HitsCount)/NULLIF((SELECT SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) < 5)) AS HlpTbl)
ORDER BY Count', 5, 113, N'GraphYearBrowserType', N'', 204, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 15:15:32', 700, '3dcf156b-9da0-49ff-8f4c-64c80e71a41c', N'', N'pie', N'graph', 500, 0, N'(SELECT StatisticsObjectName, CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((SELECT SUM(HitsCount) 
	 FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS Count 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode = @CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
 GROUP BY StatisticsObjectName
 HAVING ((100*SUM(HitsCount)/NULLIF((SELECT SUM(HitsCount) 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode = @CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) >= 5))
 
 UNION
 
 (SELECT ''Others'' AS StatisticsObjectName, CAST(SUM(HlpTbl.SubCount) AS decimal(3)) AS Count FROM 
	(SELECT StatisticsObjectName, 100*CAST(SUM(HitsCount) AS float)/NULLIF((
		SELECT SUM(HitsCount) FROM Analytics_Statistics
		INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY StatisticsObjectName
	HAVING ((100*SUM(HitsCount)/NULLIF((SELECT SUM(HitsCount) 
		FROM Analytics_Statistics
		INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) < 5)) AS HlpTbl)
	ORDER BY Count', -1, 126, N'GraphDayCountries', N'', 235, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 15:23:52', 700, 'fd214795-6023-42c3-a54c-e40c4c609ff0', N'', N'pie', N'graph', 500, 0, N'(SELECT StatisticsObjectName, CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS Count 
 FROM Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode = @CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
 GROUP BY StatisticsObjectName
 HAVING ((100*SUM(HitsCount)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) >= 5))
	
 UNION
 
 (SELECT ''Others'' AS StatisticsObjectName, CAST(SUM(HlpTbl.SubCount) AS decimal(3)) AS Count FROM 
	(SELECT StatisticsObjectName, 100*CAST(SUM(HitsCount) AS float)/NULLIF((
		SELECT SUM(HitsCount) 
		FROM Analytics_Statistics
		INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY StatisticsObjectName
 HAVING ((100*SUM(HitsCount)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) < 5)) AS HlpTbl)
ORDER BY Count', -1, 128, N'GraphHourCountries', N'', 236, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 15:31:09', 700, '45ea0ba6-f5e3-4cf8-9986-7525c5eb9998', N'', N'pie', N'graph', 500, 0, N'(SELECT StatisticsObjectName, CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS Count 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode = @CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
 GROUP BY StatisticsObjectName
 HAVING ((100*SUM(HitsCount)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) >= 5))
 UNION
 
 (SELECT ''Others'' AS StatisticsObjectName, CAST(SUM(HlpTbl.SubCount) AS decimal(3)) AS Count 
 FROM 
	(SELECT StatisticsObjectName, 100*CAST(SUM(HitsCount) AS float)/NULLIF((
		SELECT SUM(HitsCount) 
		FROM Analytics_Statistics
		INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode = @CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
	GROUP BY StatisticsObjectName
	HAVING ((100*SUM(HitsCount)/NULLIF((
		SELECT SUM(HitsCount) 
		FROM Analytics_Statistics 
		INNER JOIN Analytics_MonthHits ON  Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
		WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
		AND (StatisticsCode = @CodeName) 
		AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) < 5)) AS HlpTbl)
	ORDER BY Count', -1, 129, N'GraphMonthCountries', N'', 237, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080910 13:11:17', 700, '8dac44c3-0140-4e7f-bb5a-1ca05fd80595', N'', N'pie', N'graph', 500, 0, N'(SELECT StatisticsObjectName, CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((SELECT SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS Count 
FROM Analytics_Statistics, Analytics_YearHits 
WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
GROUP BY StatisticsObjectName
HAVING ((100*SUM(HitsCount)/NULLIF((SELECT SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) >= 5))
UNION
(SELECT ''Others'' AS StatisticsObjectName, CAST(SUM(HlpTbl.SubCount) AS decimal(3)) AS Count FROM 
(SELECT StatisticsObjectName, 100*CAST(SUM(HitsCount) AS float)/NULLIF((SELECT SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS SubCount
FROM Analytics_Statistics, Analytics_YearHits 
WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
GROUP BY StatisticsObjectName
HAVING ((100*SUM(HitsCount)/NULLIF((SELECT SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode = @CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0)) < 5)) AS HlpTbl)
ORDER BY Count', -1, 130, N'GraphYearCountries', N'', 238, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20081130 17:14:44', 800, 'c3e6b1a3-2baa-4dcd-8f4a-b6ab3ac2bf5b', N'Count', N'bar', N'New users by day', 600, 0, N'select CONVERT(varchar(10), usercreated, 103) , count(*) from cms_user
where usercreated >= @From AND usercreated <= @To
group by CONVERT(varchar(10), usercreated, 103)', -1, 143, N'NewUsersByDay', N'Days', 271, N'Number of new users by day')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080822 11:43:38', 800, '204ec301-23b6-432b-9fbf-379117b29e16', N'Count', N'bar', N'New users by month', 600, 0, N'select CONVERT(varchar, datepart(month, usercreated))+''/''+ CONVERT(varchar, datepart(year, usercreated)) , count(*) from cms_user
where usercreated >= @From AND usercreated <= @To
group by datepart(year, usercreated), datepart(month, usercreated)', -1, 144, N'NewUsersByMonth', N'Months', 272, N'Number of new users by month')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080822 11:46:53', 800, '14c1772b-6cf8-41df-9cef-c0b1148c496e', N'Count', N'bar', N'New users by year', 600, 0, N'select datepart(year, usercreated) , count(*) from cms_user
where usercreated >= @From AND usercreated <= @To
group by datepart(year, usercreated)', -1, 145, N'NewUsersByYear', N'Years', 273, N'Number of new users by year')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:08:16', 700, '308b10b1-e7f3-45e3-8733-ce906f524dec', N'{$reports_pageviews.label_numofRegisteredUsers$}', N'bar', N'graph', 300, 0, N'SELECT IndexMonthName, T1.hits
 FROM Analytics_Index 
 LEFT JOIN
	(SELECT DATENAME(month,HitsStartTime) AS months, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(month,HitsStartTime)) AS T1
 ON Analytics_Index.IndexMonthName=T1.months WHERE (IndexID>=1) AND (IndexID<=12)
 ORDER BY IndexID', -1, 146, N'GraphMonthRegisteredUsers', N'{$reports_general.label_months$}', 274, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 10:54:12', 700, 'd1c5e876-fa55-41f7-8b21-cef55193282f', N'{$reports_pageviews.label_numofRegisteredUsers$}', N'bar', N'graph', 300, 0, N'SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
	LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
	) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <=@ToDayFirst))
 GROUP BY IndexID, T1.SubCount
UNION ALL
SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID  = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
	) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 147, N'GraphDayRegisteredUsers', N'{$reports_general.label_days$}', 275, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100217 12:02:14', 700, '5ad6ae29-0ae3-4ab1-87c9-fb655991d30a', N'{$reports_pageviews.label_numofRegisteredUsers$}', N'bar', N'graph', 300, 0, N'SELECT IndexZero, T1.hits
 FROM Analytics_Index 
 LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
 ON Analytics_Index.IndexZero=T1.hours 
 WHERE (IndexZero>=0) AND (IndexZero<24) 
 ORDER BY IndexZero', -1, 148, N'GraphHourRegisteredUsers', N'{$reports_general.label_hours$}', 276, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20080822 17:14:42', 700, 'e8e03c30-efa0-43c7-8fc3-f7abaaf31faa', N'{$reports_pageviews.label_numofRegisteredUsers$}', N'bar', N'graph', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 149, N'GraphYearRegisteredUsers', N'{$reports_general.label_years$}', 277, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20081130 16:52:52', 800, '7745e64c-df0c-4d3e-b0f1-6cb45a5e0d79', N'{$Forums.NumberOfPosts$}', N'bar', N'Forum posts by month graph', 400, 0, N'SELECT  Cast(Year(PostTime) AS nvarchar(20))+'' '' + Cast( Month(PostTime) AS nvarchar(20)) AS ''Month'', COUNT(*) as ''Number of posts'' FROM Forums_ForumPost 
WHERE PostForumID IN (SELECT ForumID FROM Forums_Forum WHERE ForumGroupID IN (SELECT GroupID FROM Forums_ForumGroup WHERE GroupSiteID = @CMSContextCurrentSiteID)) AND DateAdd(month, -1 * @LastXmonths, GetDate()) < PostTime
GROUP BY  Year(PostTime), Month(PostTime) ORDER BY Year(PostTime), Month(PostTime)', -1, 168, N'ForumPostsByMonthGraph', N'{$Forums.Months$}', 312, N'{$Forums.NewPostsByMonth$} - {%CMSContextCurrentSiteName%}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20081130 17:03:00', 800, 'ae5287eb-3b29-4a39-8411-00523be8abd7', N'{$Forums.NumberOfPosts$}', N'bar', N'Forum posts by year graph', 400, 0, N'SELECT Year(PostTime) AS ''Year'', COUNT(*) as ''Number of posts'' FROM Forums_ForumPost 
WHERE PostForumID IN (SELECT ForumID FROM Forums_Forum WHERE ForumGroupID IN (SELECT GroupID FROM Forums_ForumGroup WHERE GroupSiteID = @CMSContextCurrentSiteID)) AND DateAdd(month, -1 * @LastXyears, GetDate()) < PostTime
GROUP BY  Year(PostTime) ORDER BY Year(PostTime)', -1, 169, N'ForumPostsByYearGraph', N'{$Forums.Years$}', 313, N'{$Forums.NewPostsByYear$} - {%CMSContextCurrentSiteName%}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20081130 16:57:37', 800, 'ce29ccef-fb9a-4e97-9830-553cf947f758', N'{$Forums.NumberOfPosts$}', N'bar', N'New posts by week graph', 400, 0, N'SELECT   Cast(Year(PostTime) AS nvarchar(20))+'' '' + Cast( DatePart(week,PostTime) AS nvarchar(20)) AS ''Month'', COUNT(*) as ''Number of posts'' FROM Forums_ForumPost 
WHERE PostForumID IN (SELECT ForumID FROM Forums_Forum WHERE ForumGroupID IN (SELECT GroupID FROM Forums_ForumGroup WHERE GroupSiteID = @CMSContextCurrentSiteID)) AND DateAdd(week, -1 * @LastXweeks, GetDate()) < PostTime
GROUP BY  Year(PostTime), DatePart(week,PostTime) ORDER BY Year(PostTime), DatePart(week,PostTime)', -1, 170, N'NewPostsByWeekGraph', N'{$Forums.Weeks$}', 314, N'{$Forums.NewPostsByWeek$} - {%CMSContextCurrentSiteName%}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20081130 16:55:36', 800, '5031d222-58ed-4bb0-9a62-3ffb0fe7ca86', N'{$Forums.NumberOfPosts$}', N'bar', N'New posts by day graph', 400, 0, N'SELECT   Cast(Year(PostTime) AS nvarchar(20))+'' '' + Cast( Month(PostTime) AS nvarchar(20))+'' '' + Cast( Day(PostTime) AS nvarchar(20)) AS ''Day'', COUNT(*) as ''Number of posts'' FROM Forums_ForumPost 
WHERE PostForumID IN (SELECT ForumID FROM Forums_Forum WHERE ForumGroupID IN (SELECT GroupID FROM Forums_ForumGroup WHERE GroupSiteID = @CMSContextCurrentSiteID)) AND DateAdd(day, -1 * @LastXDays, GetDate()) < PostTime
GROUP BY  Year(PostTime), Month(PostTime), Day(PostTime) ORDER BY Year(PostTime), Month(PostTime), Day(PostTime)', -1, 171, N'NewPostsByDayGraph', N'{$Forums.Days$}', 315, N'{$Forums.NewPostsByDay$} - {%CMSContextCurrentSiteName%}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 09:23:19', 700, '4314f75b-476d-4cf8-8c26-c5d2f6827dcf', N'{$reports_aggviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexZero, T1.hits
 FROM Analytics_Index 
 LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
 ON Analytics_Index.IndexZero=T1.hours 
 WHERE (IndexZero>=0) AND (IndexZero<24) 
 ORDER BY IndexZero', -1, 187, N'GraphHoursAggViews', N'{$reports_general.label_hours$}', 324, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 09:24:23', 700, '6ebc8a28-abc3-4e1f-8124-c1709384ea3d', N'{$reports_aggviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <= @ToDayFirst))
 GROUP BY IndexID, T1.SubCount
 UNION ALL
 SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID)
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
 ) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 188, N'GraphDayAggViews', N'{$reports_general.label_days$}', 325, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 09:34:46', 700, 'd7549a8a-0e29-46e0-969f-d98defab4b7f', N'{$reports_aggviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexMonthName,
 T1.hits
 FROM Analytics_Index
 LEFT JOIN
    (SELECT DATENAME(month,HitsStartTime) AS months,
    SUM(HitsCount) AS hits 
    FROM Analytics_Statistics
    INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
    WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND
    (StatisticsCode=@CodeName) AND
    (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
    GROUP BY DATENAME(month,HitsStartTime)) AS T1
 ON Analytics_Index.IndexMonthName=T1.months
 WHERE (IndexID>=1) AND (IndexID<=12)
 ORDER BY IndexID', -1, 189, N'GraphMonthAggViews', N'{$reports_general.label_months$}', 326, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 09:42:52', 700, 'e860c778-6056-4669-b794-c5f358683a55', N'{$reports_aggviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 190, N'GraphYearAggViews', N'{$reports_general.label_years$}', 327, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 10:10:16', 700, 'bd459e0d-d86f-4f1f-86ce-34abd5f7b9a7', N'{$reports_aggviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexMonthName, T1.hits
 FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(month,HitsStartTime) AS months, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	LEFT JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(month,HitsStartTime)) AS T1
 ON Analytics_Index.IndexMonthName=T1.months 
 WHERE (IndexID>=1) AND (IndexID<=12)
 ORDER BY IndexID', -1, 191, N'GraphMonthAggViewsCulture', N'{$reports_general.label_months$}', 328, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 10:10:31', 700, 'ef2a0de8-fc4b-4d88-a326-20acdbc0eb63', N'', N'pie', N'graph pie', 500, 0, N'DECLARE @myselection TABLE (
    StatisticsObjectCulture varchar(400),
    Count float
);
INSERT INTO @myselection 
	SELECT StatisticsObjectCulture, 
	SUM(HitsCount) as Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture 
	ORDER BY SUM(HitsCount) DESC
SELECT StatisticsObjectCulture, 
	CAST(100*Count/(SELECT SUM(Count) FROM @myselection) as decimal(10,2)) 
FROM @myselection', 5, 191, N'GraphPieMonthAggViewsCulture', N'', 329, N'{$reports_aggviewscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 10:11:38', 700, '3bae9128-49ac-4b1c-bbb0-d06134a49f5e', N'{$reports_aggviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexID, T1.SubCount
FROM Analytics_Index 
LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
	) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE ((IndexID >= @FromDayFirst) AND (IndexID <=@ToDayFirst))
 GROUP BY IndexID, T1.SubCount
 UNION ALL
 SELECT IndexID, T1.SubCount
 FROM Analytics_Index 
 LEFT JOIN (
	SELECT DATENAME(day,HitsStartTime) AS days, SUM(HitsCount) AS SubCount
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE
	(StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName)
	AND (HitsStartTime >= @FromDate)
	AND (HitsEndTime <= @ToDate)
	GROUP BY HitsStartTime
	) AS T1 ON Analytics_Index.IndexID=T1.days
 WHERE (IndexID <= @ToDaySecond)
 GROUP BY IndexID, T1.SubCount', -1, 192, N'GraphDayAggViewsCulture', N'{$reports_general.label_days$}', 330, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 10:11:49', 700, '1737c887-14f9-4fa7-b18a-4eb70b70791e', N'', N'pie', N'graph pie', 500, 0, N'DECLARE @myselection TABLE (
    StatisticsObjectCulture varchar(400),
    Count float
);
INSERT INTO @myselection 
	SELECT StatisticsObjectCulture, SUM(HitsCount) AS Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture 
	ORDER BY SUM(HitsCount) DESC
 SELECT StatisticsObjectCulture, 
 CAST((100*Count/(SELECT SUM(Count) 
 FROM @myselection)) as decimal(10,2)) 
 FROM @myselection', 5, 192, N'GraphPieDayAggViewsCulture', N'', 331, N'{$reports_aggviewscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 10:12:08', 700, 'e510cfb6-3591-4a63-8787-9b568fdfbe6f', N'{$reports_aggviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT IndexZero, T1.hits
FROM Analytics_Index LEFT JOIN
	(SELECT DATENAME(hour,HitsStartTime) AS hours, 
	SUM(HitsCount) AS hits 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY DATENAME(hour,HitsStartTime)) AS T1
ON Analytics_Index.IndexZero=T1.hours 
WHERE (IndexZero>=0) AND (IndexZero<24) 
ORDER BY IndexZero', -1, 193, N'GraphHourAggViewsCulture', N'{$reports_general.label_hours$}', 332, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 10:12:15', 700, '559e4eb1-8379-4fba-aa20-dca33d42a6fc', N'', N'pie', N'graph pie', 500, 0, N'DECLARE @myselection TABLE (
    StatisticsObjectCulture varchar(400),
    Count float
);
INSERT INTO @myselection 
	SELECT StatisticsObjectCulture,
	SUM(HitsCount) AS Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture 
	ORDER BY SUM(HitsCount)
 SELECT StatisticsObjectCulture, 
 CAST((100*Count/(SELECT SUM(Count) 
 FROM @myselection)) as decimal(10,2)) 
 FROM @myselection', 5, 193, N'GraphPieHourAggViewsCulture', N'', 333, N'{$reports_aggviewscult.piechart_caption$}')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 10:12:31', 700, 'd3233eb7-554b-40b9-8561-07c0ad477e9c', N'{$reports_aggviews.label_numofpageviews$}', N'bar', N'graph', 300, 0, N'SELECT DATENAME(year,HitsStartTime), SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DATENAME(year,HitsStartTime)
ORDER BY MIN(HitsStartTime)', -1, 194, N'GraphYearAggViewsCulture', N'{$reports_general.label_years$}', 334, N'')
INSERT INTO [Reporting_ReportGraph] ([GraphLastModified], [GraphWidth], [GraphGUID], [GraphYAxisTitle], [GraphType], [GraphDisplayName], [GraphHeight], [GraphQueryIsStoredProcedure], [GraphQuery], [GraphLegendPosition], [GraphReportID], [GraphName], [GraphXAxisTitle], [GraphID], [GraphTitle]) VALUES ('20100304 10:12:37', 700, 'c24b7a0c-8efa-49e4-8c60-4d6a651e9d64', N'', N'pie', N'graph pie', 500, 0, N'DECLARE @myselection TABLE (
    StatisticsObjectCulture varchar(400),
    Count float
);
INSERT INTO @myselection SELECT StatisticsObjectCulture, SUM(HitsCount) as Count FROM 
Analytics_Statistics, Analytics_YearHits, View_CMS_Tree_Joined WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (StatisticsObjectID = NodeID) AND (StatisticsObjectCulture = DocumentCulture) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectCulture ORDER BY SUM(HitsCount) DESC
SELECT StatisticsObjectCulture, CAST((100*Count/(SELECT SUM(Count) FROM @myselection)) as decimal(10,2)) FROM @myselection', 5, 194, N'GraphPieYearAggViewsCulture', N'', 335, N'{$reports_aggviewscult.piechart_caption$}')
SET IDENTITY_INSERT [Reporting_ReportGraph] OFF
