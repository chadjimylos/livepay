<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_New.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Products_Product_New"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="Default" Title="Product properties" %>

<%@ Register Src="~/CMSModules/Ecommerce/Controls/UI/ProductEdit.ascx" TagName="NewProduct"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:NewProduct ID="NewProduct1" runat="server" />
</asp:Content>
