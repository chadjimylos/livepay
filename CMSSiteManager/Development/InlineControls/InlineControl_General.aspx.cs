using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.ExtendedControls.Dialogs;

public partial class CMSSiteManager_Development_InlineControls_InlineControl_General : SiteManagerPage
{
    protected int controlid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // control initializations				
        lblControlDisplayName.Text = ResHelper.GetString("InlineControl_Edit.ControlDisplayNameLabel");
        lblControlFileName.Text = ResHelper.GetString("InlineControl_Edit.ControlFileNameLabel");
        lblControlParameterName.Text = ResHelper.GetString("InlineControl_Edit.ControlParameterNameLabel");
        lblControlName.Text = ResHelper.GetString("InlineControl_Edit.ControlNameLabel");

        rfvDisplayName.ErrorMessage = ResHelper.GetString("InlineControl_Edit.ErrorDisplayName");
        rfvName.ErrorMessage = ResHelper.GetString("InlineControl_Edit.ErrorName");
        

        btnOk.Text = ResHelper.GetString("General.OK");

        if ((Request.QueryString["saved"] != null) && (Request.QueryString["saved"] == "1"))
        {
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            lblInfo.Visible = true;
        }

        // get inlineControl id from querystring
        if ((Request.QueryString["inlinecontrolid"] != null) && (Request.QueryString["inlinecontrolid"] != ""))
        {
            controlid = Convert.ToInt32(Request.QueryString["inlinecontrolid"]);
            InlineControlInfo inlineControlObj = InlineControlInfoProvider.GetInlineControlInfo(controlid);

            // fill editing form
            if (!RequestHelper.IsPostBack())
            {
                LoadData(inlineControlObj);
            }
        }
        FileSystemDialogConfiguration config = new FileSystemDialogConfiguration();
        config.DefaultPath = "CMSInlineControls";
        config.AllowedExtensions = "ascx";
        config.ShowFolders = false;
        FileSystemSelector.DialogConfig = config;
        FileSystemSelector.AllowEmptyValue = false;
        FileSystemSelector.SelectedPathPrefix = "~/CMSInlineControls/";
        FileSystemSelector.ValidationError = ResHelper.GetString("InlineControl_Edit.ErrorFileName");
    }


    /// <summary>
    /// Load data of editing inlineControl.
    /// </summary>
    /// <param name="inlineControlObj">InlineControl object.</param>
    protected void LoadData(InlineControlInfo inlineControlObj)
    {
        txtControlDescription.Text = inlineControlObj.ControlDescription;
        txtControlDisplayName.Text = inlineControlObj.ControlDisplayName;
        txtControlParameterName.Text = inlineControlObj.ControlParameterName;
        FileSystemSelector.Value = inlineControlObj.ControlFileName;
        txtControlName.Text = inlineControlObj.ControlName;
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        string errorMessage = new Validator().NotEmpty(txtControlDisplayName, rfvDisplayName.ErrorMessage).NotEmpty(txtControlName, rfvName.ErrorMessage).NotEmpty(txtControlName.Text.Trim(), rfvName.ErrorMessage).NotEmpty(FileSystemSelector.Value.ToString().Trim(), FileSystemSelector.ValidationError).Result;

        if (errorMessage == "")
        {
            // Get object
            InlineControlInfo inlineControlObj = InlineControlInfoProvider.GetInlineControlInfo(controlid);
            if (inlineControlObj == null)
            {
                inlineControlObj = new InlineControlInfo();
            }

            inlineControlObj.ControlDescription = txtControlDescription.Text.Trim();
            inlineControlObj.ControlDisplayName = txtControlDisplayName.Text.Trim();
            inlineControlObj.ControlParameterName = txtControlParameterName.Text.Trim();
            inlineControlObj.ControlName = txtControlName.Text.Trim();
            inlineControlObj.ControlFileName = FileSystemSelector.Value.ToString().Trim();
            inlineControlObj.ControlID = controlid;

            try
            {
                InlineControlInfoProvider.SetInlineControlInfo(inlineControlObj);
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
            catch(Exception ex)
            {
                lblError.Text = ex.Message.Replace("%%name%%", txtControlName.Text);
                lblError.Visible = true;
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
