<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Module_Edit_PermissionNames.aspx.cs"
    Inherits="CMSSiteManager_Development_Modules_Module_Edit_PermissionNames" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Module Edit - Permission Names" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:UniGrid ID="unigridPermissionList" runat="server" GridName="Permission_List.xml"
        OrderBy="PermissionDisplayName" Columns="PermissionID, PermissionDisplayName, PermissionName"
        IsLiveSite="false" />
</asp:Content>
