CREATE TABLE [CMS_WidgetRole] (
		[WidgetID]         int NOT NULL,
		[RoleID]           int NOT NULL,
		[PermissionID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WidgetRole]
	ADD
	CONSTRAINT [PK_CMS_WidgetRole]
	PRIMARY KEY
	([WidgetID], [RoleID], [PermissionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_WidgetRole]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Permission]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WidgetRole_PermissionID_CMS_Permission]') IS NULL
BEGIN
		ALTER TABLE [CMS_WidgetRole]
			ADD CONSTRAINT [FK_CMS_WidgetRole_PermissionID_CMS_Permission]
			FOREIGN KEY ([PermissionID]) REFERENCES [CMS_Permission] ([PermissionID])
END
IF OBJECT_ID(N'[CMS_WidgetRole]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WidgetRole_RoleID_CMS_Role]') IS NULL
BEGIN
		ALTER TABLE [CMS_WidgetRole]
			ADD CONSTRAINT [FK_CMS_WidgetRole_RoleID_CMS_Role]
			FOREIGN KEY ([RoleID]) REFERENCES [CMS_Role] ([RoleID])
END
IF OBJECT_ID(N'[CMS_WidgetRole]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Widget]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WidgetRole_WidgetID_CMS_Widget]') IS NULL
BEGIN
		ALTER TABLE [CMS_WidgetRole]
			ADD CONSTRAINT [FK_CMS_WidgetRole_WidgetID_CMS_Widget]
			FOREIGN KEY ([WidgetID]) REFERENCES [CMS_Widget] ([WidgetID])
END
