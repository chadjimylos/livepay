<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GradientControl.ascx.cs" Inherits="CMSModules_Reporting_Controls_GradientControl" %>

<asp:Label ID="lblFromColor" runat="server" />
<asp:TextBox ID="txtFromColor" runat="server" CssClass="VerySmallTextBox" />&nbsp;
<asp:Label ID="lblToColor" runat="server" />
<asp:TextBox ID="txtToColor" runat="server" CssClass="VerySmallTextBox" />&nbsp;
<asp:Label ID="lblAngle" runat="server" />
<asp:TextBox ID="txtAngle" runat="server" CssClass="SuperSmallTextBox" />
