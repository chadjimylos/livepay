using System;

using CMS.TreeEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.WorkflowEngine;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.EventLog;

using TreeNode = CMS.TreeEngine.TreeNode;
using CMS.LicenseProvider;
using CMS.PortalEngine;

public partial class CMSModules_Content_Controls_EditMenu : CMSUserControl
{
    #region "Variables"

    private int nodeId = 0;
    private string mAction = null;

    private bool newdocument = false;
    private bool newlink = false;
    private TreeNode node = null;
    private TreeProvider mTree = null;
    private VersionManager mVersionMan = null;
    private WorkflowManager mWorkflowMan = null;
    private bool mAllowEdit = true;
    private bool mAllowSave = true;
    private bool mEnablepasiveRefresh = true;
    private string mWorkflowInfo = "";
    private int mMenuItems = 0;

    public event EventHandler LocalSave = null;

    private bool mShowSave = true;
    private bool mShowCheckOut = true;
    private bool mShowCheckIn = true;
    private bool mShowUndoCheckOut = true;
    private bool mShowApprove = true;
    private bool mShowSubmitToApproval = true;
    private bool mShowReject = true;
    private bool mShowProperties = true;
    private bool mShowSpellCheck = false;
    private bool mShowDelete = false;
    private bool mShowCreateAnother = true;

    #endregion


    #region "Events"

    /// <summary>
    /// On before check in event handler 
    /// </summary>
    public event EventHandler OnBeforeCheckIn;


    /// <summary>
    /// On before check out event handler 
    /// </summary>
    public event EventHandler OnBeforeCheckOut;


    /// <summary>
    /// On before undo check out event handler 
    /// </summary>
    public event EventHandler OnBeforeUndoCheckOut;


    /// <summary>
    /// On before approve event handler 
    /// </summary>
    public event EventHandler OnBeforeApprove;


    /// <summary>
    /// On before reject event handler 
    /// </summary>
    public event EventHandler OnBeforeReject;


    /// <summary>
    /// On before save event handler 
    /// </summary>
    public event EventHandler OnBeforeSave;


    /// <summary>
    /// Raises on before check in event
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event arguments</param>
    public void RaiseOnBeforeCheckIn(object sender, EventArgs e)
    {
        if (OnBeforeCheckIn != null)
        {
            OnBeforeCheckIn(sender, e);
        }
    }


    /// <summary>
    /// Raises on before check out event
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event arguments</param>
    public void RaiseOnBeforeCheckOut(object sender, EventArgs e)
    {
        if (OnBeforeCheckOut != null)
        {
            OnBeforeCheckOut(sender, e);
        }
    }


    /// <summary>
    /// Raises on before undo check out event
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event arguments</param>
    public void RaiseOnBeforeUndoCheckOut(object sender, EventArgs e)
    {
        if (OnBeforeUndoCheckOut != null)
        {
            OnBeforeUndoCheckOut(sender, e);
        }
    }


    /// <summary>
    /// Raises on before approve event
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event arguments</param>
    public void RaiseOnBeforeApprove(object sender, EventArgs e)
    {
        if (OnBeforeApprove != null)
        {
            OnBeforeApprove(sender, e);
        }
    }


    /// <summary>
    /// Raises on before reject event
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event arguments</param>
    public void RaiseOnBeforeReject(object sender, EventArgs e)
    {
        if (OnBeforeReject != null)
        {
            OnBeforeReject(sender, e);
        }
    }


    /// <summary>
    /// Raises on before save event
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event arguments</param>
    public void RaiseOnBeforeSave(object sender, EventArgs e)
    {
        if (OnBeforeSave != null)
        {
            OnBeforeSave(sender, e);
        }
    }

    #endregion


    #region "Button display options"

    /// <summary>
    /// Show the Delete button?
    /// </summary>
    public bool ShowDelete
    {
        get
        {
            return mShowDelete;
        }
        set
        {
            mShowDelete = value;
        }
    }


    /// <summary>
    /// Show the Save button?
    /// </summary>
    public bool ShowSave
    {
        get
        {
            return mShowSave;
        }
        set
        {
            mShowSave = value;
        }
    }


    /// <summary>
    /// Show the Check Out button?
    /// </summary>
    public bool ShowCheckOut
    {
        get
        {
            return mShowCheckOut;
        }
        set
        {
            mShowCheckOut = value;
        }
    }


    /// <summary>
    /// Show the Check In button?
    /// </summary>
    public bool ShowCheckIn
    {
        get
        {
            return mShowCheckIn;
        }
        set
        {
            mShowCheckIn = value;
        }
    }


    /// <summary>
    /// Show the Undo CheckOut button?
    /// </summary>
    public bool ShowUndoCheckOut
    {
        get
        {
            return mShowUndoCheckOut;
        }
        set
        {
            mShowUndoCheckOut = value;
        }
    }


    /// <summary>
    /// Show the Approve button?
    /// </summary>
    public bool ShowApprove
    {
        get
        {
            return mShowApprove;
        }
        set
        {
            mShowApprove = value;
        }
    }


    /// <summary>
    /// Show the Reject button?
    /// </summary>
    public bool ShowReject
    {
        get
        {
            return mShowReject;
        }
        set
        {
            mShowReject = value;
        }
    }


    /// <summary>
    /// Show the Submit To Approval button?
    /// </summary>
    public bool ShowSubmitToApproval
    {
        get
        {
            return mShowSubmitToApproval;
        }
        set
        {
            mShowSubmitToApproval = value;
        }
    }


    /// <summary>
    /// Show the Properties button?
    /// </summary>
    public bool ShowProperties
    {
        get
        {
            return mShowProperties;
        }
        set
        {
            mShowProperties = value;
        }
    }


    /// <summary>
    /// Show spell check button
    /// </summary>
    public bool ShowSpellCheck
    {
        get
        {
            return mShowSpellCheck;
        }
        set
        {
            mShowSpellCheck = value;
        }
    }


    /// <summary>
    /// If true, create another button can be displayed
    /// </summary>
    public bool ShowCreateAnother
    {
        get
        {
            return mShowCreateAnother;
        }
        set
        {
            mShowCreateAnother = value;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Tree provider
    /// </summary>
    public TreeProvider Tree
    {
        get
        {
            if (mTree == null)
            {
                mTree = new TreeProvider(CMSContext.CurrentUser);
                mTree.CombineWithDefaultCulture = false;
            }
            return mTree;
        }
    }


    /// <summary>
    /// Version manager
    /// </summary>
    public VersionManager VersionMan
    {
        get
        {
            if (mVersionMan == null)
            {
                mVersionMan = new VersionManager(Tree);
            }
            return mVersionMan;
        }
    }


    /// <summary>
    /// Worklfow manager
    /// </summary>
    public WorkflowManager WorkflowMan
    {
        get
        {
            if (mWorkflowMan == null)
            {
                mWorkflowMan = new WorkflowManager(Tree);
            }
            return mWorkflowMan;
        }
    }


    /// <summary>
    /// Enable passive refresh
    /// </summary>
    public bool EnablePassiveRefresh
    {
        get
        {
            return mEnablepasiveRefresh;
        }
        set
        {
            mEnablepasiveRefresh = value;
        }
    }


    /// <summary>
    /// Returns true if the document editing is allowed
    /// </summary>
    public bool AllowEdit
    {
        get
        {
            return mAllowEdit;
        }
    }


    /// <summary>
    /// Save action allowed
    /// </summary>
    public bool AllowSave
    {
        get
        {
            return mAllowSave;
        }
        set
        {
            mAllowSave = value;
        }
    }


    /// <summary>
    /// If true, the access permissions to the items are checked.
    /// </summary>
    public bool CheckPermissions
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["CheckPermissions"], true);
        }
        set
        {
            ViewState["CheckPermissions"] = value;
        }
    }


    /// <summary>
    /// Returns the 
    /// </summary>
    public string WorkflowInfo
    {
        get
        {
            return mWorkflowInfo;
        }
    }


    /// <summary>
    /// Count of the menu items currently displayed
    /// </summary>
    public int MenuItems
    {
        get
        {
            return mMenuItems;
        }
    }


    /// <summary>
    /// Node ID
    /// </summary>
    public int NodeID
    {
        get
        {
            return nodeId;
        }
        set
        {
            nodeId = value;
        }
    }


    /// <summary>
    /// Action
    /// </summary>
    public string Action
    {
        get
        {
            return mAction;
        }
        set
        {
            mAction = value;
        }
    }


    /// <summary>
    /// Render the menu scripts?
    /// </summary>
    public bool RenderScript
    {
        get
        {
            return plcScript.Visible;
        }
        set
        {
            plcScript.Visible = value;
        }
    }


    /// <summary>
    /// Prefix of the action functions
    /// </summary>
    public string FunctionsPrefix
    {
        get
        {
            return ValidationHelper.GetString(ViewState["FunctionsPrefix"], "");
        }
        set
        {
            ViewState["FunctionsPrefix"] = value;
        }
    }


    /// <summary>
    /// Document node
    /// </summary>
    public TreeNode Node
    {
        get
        {
            return node;
        }
        set
        {
            node = value;
            nodeId = node.NodeID;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        btnApprove.Attributes.Add("style", "display: none;");
        btnCheckIn.Attributes.Add("style", "display: none;");
        btnCheckOut.Attributes.Add("style", "display: none;");
        btnReject.Attributes.Add("style", "display: none;");
        btnSave.Attributes.Add("style", "display: none;");
        btnUndoCheckout.Attributes.Add("style", "display: none;");

        // Get the document ID
        if (nodeId <= 0)
        {
            nodeId = QueryHelper.GetInteger("nodeid", 0);
        }

        // Get information whether new document is created
        if (mAction == null)
        {
            mAction = QueryHelper.GetString("action", string.Empty).ToLower();
        }

        ReloadMenu(false);

        if (RenderScript)
        {
            string script = "function " + FunctionsPrefix + "LocalSave(nodeId) { " + Page.ClientScript.GetPostBackEventReference(btnSave, null) + "; } \n" +
                            "function " + FunctionsPrefix + "LocalApprove(nodeId) { " + Page.ClientScript.GetPostBackEventReference(btnApprove, null) + "; } \n" +
                            "function " + FunctionsPrefix + "LocalReject(nodeId) { " + Page.ClientScript.GetPostBackEventReference(btnReject, null) + "; } \n" +
                            "function " + FunctionsPrefix + "CheckOut(nodeId) { " + Page.ClientScript.GetPostBackEventReference(btnCheckOut, null) + "; } \n" +
                            "function " + FunctionsPrefix + "LocalCheckIn(nodeId) { " + Page.ClientScript.GetPostBackEventReference(btnCheckIn, null) + "; } \n" +
                            "function " + FunctionsPrefix + "UndoCheckOut(nodeId) { if(!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EditMenu.UndoCheckOutConfirmation")) + ")) return false; " + Page.ClientScript.GetPostBackEventReference(btnUndoCheckout, null) + "; } \n";

            // Add control frame name
            script += "var controlFrame = 'editview'; \n";
            script += "var spellURL = '" + ((CMSContext.ViewMode == ViewModeEnum.LiveSite) ?  CMSContext.ResolveDialogUrl("~/CMSFormControls/LiveSelectors/SpellCheck.aspx") : CMSContext.ResolveDialogUrl("~/CMSModules/Content/CMSDesk/Edit/SpellCheck.aspx")) + "'; \n";

            ltlScript.Text += ScriptHelper.GetScript(script);
        }

        menuElem.Layout = CMS.skmMenuControl.MenuLayout.Horizontal;
        menuElem.ItemPadding = 0;
        menuElem.ItemSpacing = 0;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        // Register the scripts
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SpellCheck_" + ClientID, ltlScript.Text);
    }


    public void ReloadMenu()
    {
        ReloadMenu(true);
    }


    public void ReloadMenu(bool forceReload)
    {
        newlink = (mAction == "newlink");
        newdocument = ((mAction == "new") || newlink);

        CMS.skmMenuControl.MenuItem newItem = null;

        // Create the edit menu
        menuElem.Items.Clear();

        mMenuItems = 0;

        // Save button
        CMS.skmMenuControl.MenuItem saveItem = null;
        if (mShowSave)
        {
            saveItem = new CMS.skmMenuControl.MenuItem();
            saveItem.ToolTip = ResHelper.GetString("EditMenu.Save");
            saveItem.JavascriptCommand = FunctionsPrefix + "SaveDocument(" + nodeId + ");";
            saveItem.ImageAltText = saveItem.ToolTip;
            saveItem.Text = ResHelper.GetString("general.save");
            saveItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
            saveItem.MouseOverImage = saveItem.Image;
            saveItem.MouseOverCssClass = "MenuItemEdit";
            saveItem.CssClass = "MenuItemEdit";
            menuElem.Items.Add(saveItem);
            mMenuItems += 1;
        }

        mWorkflowInfo = "";

        mAllowEdit = true;
        mAllowSave = true;
        bool showDelete = false;

        if (!newdocument)
        {
            // Get the document
            if ((node == null) || forceReload)
            {
                node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, Tree);
            }

            if (node != null)
            {
                bool authorized = true;

                // If the perrmisions should be checked
                if (CheckPermissions)
                {
                    // Check read permissions
                    if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                    {
                        RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
                    }
                    // Check modify permissions
                    else if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                    {
                        mAllowSave = false;
                        mWorkflowInfo = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                        authorized = false;
                    }
                }

                // If authorized
                if (authorized)
                {
                    WorkflowInfo wi = WorkflowMan.GetNodeWorkflow(node);
                    // If workflow not null, process the workflow information to display the items
                    if (wi != null)
                    {
                        // Get current step info, do not update document
                        WorkflowStepInfo si = WorkflowMan.GetStepInfo(node, false) ??
                                              WorkflowMan.GetFirstWorkflowStep(node);

                        bool allowApproval = true;

                        bool canApprove = true;

                        // If license does not allow custom steps, can approve check is useless
                        if (WorkflowInfoProvider.IsCustomStepAllowed())
                        {
                            canApprove = WorkflowMan.CanUserApprove(node, CMSContext.CurrentUser);
                        }
                        string stepName = si.StepName.ToLower();
                        if (!(canApprove || (stepName == "edit") || (stepName == "published") || (stepName == "archived")))
                        {
                            mAllowSave = false;
                        }

                        // Check-in, Check-out
                        if (VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName))
                        {
                            if (node.GetValue("DocumentCheckedOutByUserID") == null)
                            {
                                mAllowSave = false;
                                mAllowEdit = false;

                                // If not checked out, add the check-out button
                                if (mShowCheckOut && (canApprove || (stepName == "edit") || (stepName == "published") || (stepName == "archived")))
                                {
                                    newItem = new CMS.skmMenuControl.MenuItem();
                                    newItem.ToolTip = ResHelper.GetString("EditMenu.CheckOut");
                                    newItem.JavascriptCommand = FunctionsPrefix + "CheckOut(" + nodeId + ");";
                                    newItem.ImageAltText = newItem.ToolTip;
                                    newItem.Text = ResHelper.GetString("EditMenu.IconCheckOut");
                                    newItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkout.png");
                                    newItem.MouseOverImage = newItem.Image;
                                    newItem.MouseOverCssClass = "MenuItemEdit";
                                    newItem.CssClass = "MenuItemEdit";
                                    menuElem.Items.Add(newItem);
                                    mMenuItems += 1;
                                }
                                // Set workflow info
                                // If not checked out, add the check-out information
                                if (canApprove || (stepName == "edit") || (stepName == "published") || (stepName == "archived"))
                                {
                                    mWorkflowInfo += ResHelper.GetString("EditContent.DocumentCheckedIn");
                                }
                            }
                            else
                            {
                                // If checked out by current user, add the check-in button
                                int checkedOutBy = ValidationHelper.GetInteger(node.GetValue("DocumentCheckedOutByUserID"), 0);
                                if (checkedOutBy == CMSContext.CurrentUser.UserID)
                                {
                                    if (mShowCheckIn)
                                    {
                                        newItem = new CMS.skmMenuControl.MenuItem();
                                        newItem.ToolTip = ResHelper.GetString("EditMenu.CheckIn");
                                        newItem.JavascriptCommand = FunctionsPrefix + "CheckIn(" + nodeId + ");";
                                        newItem.ImageAltText = newItem.ToolTip;
                                        newItem.Text = ResHelper.GetString("EditMenu.IconCheckIn");
                                        newItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkin.png");
                                        newItem.MouseOverImage = newItem.Image;
                                        newItem.MouseOverCssClass = "MenuItemEdit";
                                        newItem.CssClass = "MenuItemEdit";
                                        menuElem.Items.Add(newItem);
                                        mMenuItems += 1;
                                    }

                                    if (mShowUndoCheckOut)
                                    {
                                        newItem = new CMS.skmMenuControl.MenuItem();
                                        newItem.ToolTip = ResHelper.GetString("EditMenu.UndoCheckout");
                                        newItem.JavascriptCommand = FunctionsPrefix + "UndoCheckOut(" + nodeId + ");";
                                        newItem.ImageAltText = newItem.ToolTip;
                                        newItem.Text = ResHelper.GetString("EditMenu.IconUndoCheckout");
                                        newItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/undocheckout.png");
                                        newItem.MouseOverImage = newItem.Image;
                                        newItem.MouseOverCssClass = "MenuItemEdit";
                                        newItem.CssClass = "MenuItemEdit";
                                        menuElem.Items.Add(newItem);
                                        mMenuItems += 1;
                                    }
                                    // Set workflow info

                                    mWorkflowInfo += ResHelper.GetString("EditContent.DocumentCheckedOut");
                                }
                                else
                                {
                                    mAllowSave = false;
                                    mAllowEdit = false;
                                    // Set workflow info
                                    mWorkflowInfo += ResHelper.GetString("EditContent.DocumentCheckedOutByAnother");
                                }
                                allowApproval = false;
                            }
                        }

                        // Document approval
                        if (allowApproval)
                        {
                            CMS.skmMenuControl.MenuItem approveItem = null;

                            switch (stepName)
                            {
                                case "edit":
                                    // When edit step, display submit to approval
                                    if (mShowSubmitToApproval)
                                    {
                                        approveItem = new CMS.skmMenuControl.MenuItem();
                                        approveItem.ToolTip = ResHelper.GetString("EditMenu.SubmitToApproval");
                                        approveItem.JavascriptCommand = FunctionsPrefix + "Approve(" + nodeId + ");";
                                        approveItem.ImageAltText = approveItem.ToolTip;
                                        approveItem.Text = ResHelper.GetString("EditMenu.IconSubmitToApproval");
                                        approveItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/approve.png");
                                        approveItem.MouseOverImage = approveItem.Image;
                                        approveItem.MouseOverCssClass = "MenuItemEdit";
                                        approveItem.CssClass = "MenuItemEdit";
                                        menuElem.Items.Add(approveItem);
                                        mMenuItems += 1;
                                    }
                                    break;

                                case "published":
                                case "archived":
                                    // No workflow options in these steps
                                    break;

                                default:
                                    // If the user is authorized to perform the step, display the approve and reject buttons
                                    if (canApprove)
                                    {
                                        if (mShowApprove)
                                        {
                                            approveItem = new CMS.skmMenuControl.MenuItem();
                                            approveItem.ToolTip = ResHelper.GetString("EditMenu.Approve");
                                            approveItem.JavascriptCommand = FunctionsPrefix + "Approve(" + nodeId + ");";
                                            approveItem.ImageAltText = approveItem.ToolTip;
                                            approveItem.Text = ResHelper.GetString("general.approve");
                                            approveItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/approve.png");
                                            approveItem.MouseOverImage = approveItem.Image;
                                            approveItem.MouseOverCssClass = "MenuItemEdit";
                                            approveItem.CssClass = "MenuItemEdit";
                                            menuElem.Items.Add(approveItem);
                                            mMenuItems += 1;
                                        }

                                        if (mShowReject)
                                        {
                                            newItem = new CMS.skmMenuControl.MenuItem();
                                            newItem.ToolTip = ResHelper.GetString("EditMenu.Reject");
                                            newItem.JavascriptCommand = FunctionsPrefix + "Reject(" + nodeId + ");";
                                            newItem.ImageAltText = newItem.ToolTip;
                                            newItem.Text = ResHelper.GetString("general.reject");
                                            newItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/reject.png");
                                            newItem.MouseOverImage = newItem.Image;
                                            newItem.MouseOverCssClass = "MenuItemEdit";
                                            newItem.CssClass = "MenuItemEdit";
                                            menuElem.Items.Add(newItem);
                                            mMenuItems += 1;
                                        }
                                    }
                                    break;
                            }

                            if (approveItem != null)
                            {
                                // Get next step info
                                WorkflowStepInfo nsi = WorkflowMan.GetNextStepInfo(node);
                                if (nsi.StepName.ToLower() == "published")
                                {
                                    approveItem.ToolTip = ResHelper.GetString("EditMenu.Publish");
                                    approveItem.Text = ResHelper.GetString("EditMenu.IconPublish");
                                }
                            }

                            // Set workflow info
                            if (!(canApprove || (stepName == "edit") || (stepName == "published") || (stepName == "archived")))
                            {
                                if (!string.IsNullOrEmpty(mWorkflowInfo))
                                {
                                    mWorkflowInfo += "<br />";
                                }
                                mWorkflowInfo += ResHelper.GetString("EditContent.NotAuthorizedToApprove");
                            }
                            if (!string.IsNullOrEmpty(mWorkflowInfo))
                            {
                                mWorkflowInfo += "<br />";
                            }
                            mWorkflowInfo += String.Format(ResHelper.GetString("EditContent.CurrentStepInfo"), si.StepDisplayName);
                        }
                    }
                }

                if (mShowProperties)
                {
                    newItem = new CMS.skmMenuControl.MenuItem();
                    newItem.ToolTip = ResHelper.GetString("EditMenu.Properties");
                    newItem.JavascriptCommand = FunctionsPrefix + "Properties(" + nodeId + ");";
                    newItem.ImageAltText = newItem.ToolTip;
                    newItem.Text = ResHelper.GetString("EditMenu.IconProperties");
                    newItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/properties.png");
                    newItem.MouseOverImage = newItem.Image;
                    newItem.MouseOverCssClass = "MenuItemEdit";
                    newItem.CssClass = "MenuItemEdit";
                    menuElem.Items.Add(newItem);
                    mMenuItems += 1;
                }

                if (mShowDelete)
                {
                    showDelete = true;
                }
            }

            // If not allowed to save, disable the save item
            if (!mAllowSave && mShowSave && (saveItem != null))
            {
                saveItem.JavascriptCommand = "";
                saveItem.MouseOverCssClass = "MenuItemEditDisabled";
                saveItem.CssClass = "MenuItemEditDisabled";
                saveItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/savedisabled.png");
                saveItem.MouseOverImage = saveItem.Image;
            }
        }
        else if (mAllowSave && mShowCreateAnother)
        {
            newItem = new CMS.skmMenuControl.MenuItem();
            newItem.ToolTip = ResHelper.GetString("EditMenu.SaveAndAnother");
            newItem.JavascriptCommand = FunctionsPrefix + "SaveDocument(" + nodeId + ", true);";
            newItem.ImageAltText = newItem.ToolTip;
            newItem.Text = ResHelper.GetString("editmenu.iconsaveandanother");
            newItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/saveandanother.png");
            newItem.MouseOverImage = newItem.Image;
            newItem.MouseOverCssClass = "MenuItemEdit";
            newItem.CssClass = "MenuItemEdit";
            menuElem.Items.Add(newItem);
            mMenuItems += 1;
        }

        if (mAllowSave && mShowSave && mShowSpellCheck && !newlink)
        {
            newItem = new CMS.skmMenuControl.MenuItem();
            newItem.ToolTip = ResHelper.GetString("EditMenu.SpellCheck");
            newItem.JavascriptCommand = FunctionsPrefix + "SpellCheck(spellURL);";
            newItem.ImageAltText = newItem.ToolTip;
            newItem.Text = ResHelper.GetString("EditMenu.IconSpellCheck");
            newItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/spellcheck.png");
            newItem.MouseOverImage = newItem.Image;
            newItem.MouseOverCssClass = "MenuItemEdit";
            newItem.CssClass = "MenuItemEdit";
            menuElem.Items.Add(newItem);
            mMenuItems += 1;
        }

        if (showDelete)
        {
            newItem = new CMS.skmMenuControl.MenuItem();
            newItem.ToolTip = ResHelper.GetString("EditMenu.Delete");
            newItem.JavascriptCommand = FunctionsPrefix + "Delete(" + nodeId + ");";
            newItem.ImageAltText = newItem.ToolTip;
            newItem.Text = ResHelper.GetString("general.delete");
            newItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/delete.png");
            newItem.MouseOverImage = newItem.Image;
            newItem.MouseOverCssClass = "MenuItemEdit";
            newItem.CssClass = "MenuItemEdit";
            menuElem.Items.Add(newItem);
            mMenuItems += 1;
        }
    }


    protected void btnApprove_Click(object sender, EventArgs e)
    {
        if (node != null)
        {
            // Raise event
            RaiseOnBeforeApprove(sender, e);

            // Approve the document - Go to next workflow step
            // Get original step
            WorkflowStepInfo originalStep = WorkflowMan.GetStepInfo(node);

            // Approve the document
            WorkflowStepInfo nextStep = WorkflowMan.MoveToNextStep(node, "");

            if (nextStep != null)
            {
                // Send workflow e-mails
                if (SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSSendWorkflowEmails"))
                {
                    if (nextStep.StepName.ToLower() == "published")
                    {
                        // Publish e-mails
                        WorkflowMan.SendWorkflowEmails(node, CMSContext.CurrentUser, originalStep, nextStep, WorkflowActionEnum.Published, "");
                    }
                    else
                    {
                        // Approve e-mails
                        WorkflowMan.SendWorkflowEmails(node, CMSContext.CurrentUser, originalStep, nextStep, WorkflowActionEnum.Approved, "");
                    }
                }
            }
        }

        ReloadMenu(false);

        if (EnablePassiveRefresh)
        {
            ltlScript.Text += ScriptHelper.GetScript("if(PassiveRefresh){ PassiveRefresh(" + nodeId + "); }");
        }
    }


    protected void btnReject_Click(object sender, EventArgs e)
    {
        // Reject the document - Go to previous workflow step
        if (node != null)
        {
            // Raise event
            RaiseOnBeforeReject(sender, e);

            // Get original step
            WorkflowStepInfo originalStep = WorkflowMan.GetStepInfo(node);

            // Reject the document
            WorkflowStepInfo previousStep = WorkflowMan.MoveToPreviousStep(node, "");

            // Send workflow e-mails
            if (SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSSendWorkflowEmails"))
            {
                WorkflowMan.SendWorkflowEmails(node, CMSContext.CurrentUser, originalStep, previousStep, WorkflowActionEnum.Rejected, "");
            }
        }

        ReloadMenu(false);

        if (EnablePassiveRefresh)
        {
            ltlScript.Text += ScriptHelper.GetScript("if(PassiveRefresh){ PassiveRefresh(" + nodeId + "); }");
        }
    }


    protected void btnCheckIn_Click(object sender, EventArgs e)
    {
        try
        {
            // Check in the document
            if (node != null)
            {
                // Raise event
                RaiseOnBeforeCheckIn(sender, e);

                // Check in the document        
                VersionMan.CheckIn(node, null, null);
            }

            ReloadMenu(false);
        }
        catch (WorkflowException)
        {
            ltlScript.Text += ScriptHelper.GetAlertScript(ResHelper.GetString("EditContent.DocumentCannotCheckIn"));
        }
        catch (Exception ex)
        {
            // Log exception
            EventLogProvider ep = new EventLogProvider(Tree.Connection);
            ep.LogEvent("Content", "CHECKIN", ex);
            ltlScript.Text += ScriptHelper.GetAlertScript(ex.Message);
        }

        if (EnablePassiveRefresh)
        {
            ltlScript.Text += ScriptHelper.GetScript("if(PassiveRefresh){ PassiveRefresh(" + nodeId + "); }");
        }
    }


    protected void btnCheckOut_Click(object sender, EventArgs e)
    {
        try
        {
            // Check out the document
            if (node != null)
            {
                // Raise event
                RaiseOnBeforeCheckOut(sender, e);

                VersionMan.EnsureVersion(node, node.IsPublished);

                bool wasArchived = (node.WorkflowStepName.ToLower() == "archived");

                // Check out the document
                VersionMan.CheckOut(node);

                if (wasArchived)
                {
                    ltlScript.Text += ScriptHelper.GetScript("if (window.RefreshTree) { RefreshTree(" + node.NodeParentID + ", " + node.NodeID + "); }");
                }
            }

            ReloadMenu(false);
        }
        catch (WorkflowException)
        {
            ltlScript.Text += ScriptHelper.GetAlertScript(ResHelper.GetString("EditContent.DocumentCannotCheckOut"));
        }
        catch (Exception ex)
        {
            // Log exception
            EventLogProvider ep = new EventLogProvider(Tree.Connection);
            ep.LogEvent("Content", "CHECKOUT", ex);
            ltlScript.Text += ScriptHelper.GetAlertScript(ex.Message);
        }

        if (EnablePassiveRefresh)
        {
            ltlScript.Text += ScriptHelper.GetScript("if (window.PassiveRefresh) { PassiveRefresh(" + nodeId + "); }");
        }
    }


    protected void btnUndoCheckout_Click(object sender, EventArgs e)
    {
        // Check out the document
        if (node != null)
        {
            try
            {
                // Raise event
                RaiseOnBeforeUndoCheckOut(sender, e);

                // Undo check out
                VersionMan.UndoCheckOut(node);

                ReloadMenu(false);
            }
            catch (WorkflowException)
            {
                ltlScript.Text += ScriptHelper.GetAlertScript(ResHelper.GetString("EditContent.DocumentCannotUndoCheckOut"));
            }
            catch (Exception ex)
            {
                // Log exception
                EventLogProvider ep = new EventLogProvider(Tree.Connection);
                ep.LogEvent("Content", "UNDOCHECKOUT", ex);
                ltlScript.Text += ScriptHelper.GetAlertScript(ex.Message);
            }

            if (EnablePassiveRefresh)
            {
                ltlScript.Text += ScriptHelper.GetScript("if(PassiveRefresh){ PassiveRefresh(" + nodeId + "); } if(RefreshTree){ RefreshTree(" + node.NodeParentID + ", " + nodeId + "); }");
            }
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (LocalSave != null)
        {
            // Raise event
            RaiseOnBeforeSave(sender, e);

            LocalSave(sender, e);
        }
    }
}
