FCKLang.InsertQuoteButton			= 'Vložit citaci' ;
FCKLang.InsertQuoteTitle			= 'Vkládaní citace' ;

FCKLang.InsertUrlButton	            = 'Vložit odkaz' ;
FCKLang.InsertUrlTitle		    	= 'Vkládaní odkazu' ;
FCKLang.InsertUrlPromptUrl          = 'Vložte URL:'
FCKLang.InsertUrlPromptDesc         = 'Vložte popis odkazu:'

FCKLang.InsertImageButton			= 'Vložit obrázek' ;
FCKLang.InsertImageTitle			= 'Vkládaní obrázku' ;
FCKLang.InsertImagePromptUrl        = 'Vložte URL obrázku:'