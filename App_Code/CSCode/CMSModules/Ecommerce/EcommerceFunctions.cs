using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Caching;

using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.TreeEngine;
using CMS.SiteProvider;

using TreeNode = CMS.TreeEngine.TreeNode;

/// <summary>
/// Summary description for Functions
/// </summary>
public static class EcommerceFunctions
{
    private static string mApplicationPath = "";


    /// <summary>
    /// Returns exchange rate between customer preffered currency and main currency
    /// </summary>    
    /// <param name="currencyId">Customer preffered currency</param>
    private static double GetPrefferedCurrencyExchangeRate(int currencyId)
    {
        // If customer preffered currency exchange rate is already specified withing the current request, get it from request stock
        double rate = ValidationHelper.GetDouble(RequestStockHelper.GetItem("CustomerPrefferedCurrencyRate"), 0);

        if (rate == 0)
        {
            // Get customer preffered currency exchange rate from DB
            rate = CurrencyInfoProvider.GetLastValidExchangeRate(currencyId);

            // Save it to request stock
            RequestStockHelper.Add("CustomerPrefferedCurrencyRate", rate);
        }

        return rate;
    }


    /// <summary>
    /// Application path
    /// </summary>        
    private static string ApplicationPath
    {
        get
        {
            if (mApplicationPath == "")
            {
                if ((HttpContext.Current != null) && (HttpContext.Current.Request != null))
                {
                    mApplicationPath = HttpContext.Current.Request.ApplicationPath.Trim();
                }
            }
            return mApplicationPath;
        }
    }


    /// <summary>
    /// Returns formatted price using current shopping cart data from CMS context. Customer discount level is not applied to the product price.
    /// </summary>
    /// <param name="SKUPrice">SKU price</param>        
    public static string GetFormatedPrice(object SKUPrice)
    {
        return GetFormatedPrice(SKUPrice, 0, null, 0, true);
    }


    /// <summary>
    /// Returns formatted price using current shopping cart data from CMS context. Customer discount level is applied to the product price when this discount level is assigned to the specified product department.
    /// </summary>
    /// <param name="SKUPrice">SKU price</param>    
    /// <param name="SKUDepartmentId">SKU department ID.</param>
    public static string GetFormatedPrice(object SKUPrice, object SKUDepartmentId)
    {
        return GetFormatedPrice(SKUPrice, SKUDepartmentId, null, 0, true);
    }


    /// <summary>
    /// Returns formatted price using specified shopping cart data.
    /// </summary>
    /// <param name="SKUPrice">SKU price</param>
    /// <param name="SKUDepartmentId">SKU department ID.</param>
    /// <param name="cart">Shopping cart object with required data for price formatting, if it is NULL shopping cart data from CMS context are used</param>
    public static string GetFormatedPrice(object SKUPrice, object SKUDepartmentId, ShoppingCartInfo cart)
    {
        return GetFormatedPrice(SKUPrice, SKUDepartmentId, cart, 0, true);
    }


    /// <summary>
    /// Returns formatted price using specified shopping cart data.
    /// </summary>
    /// <param name="SKUPrice">SKU price</param>
    /// <param name="SKUDepartmentId">SKU department ID.</param>
    /// <param name="SKUId">Product ID.</param>
    public static string GetFormatedPrice(object SKUPrice, object SKUDepartmentId, object SKUId)
    {
        return GetFormatedPrice(SKUPrice, SKUDepartmentId, null, SKUId, true);
    }


    /// <summary>
    /// Returns formatted price using specified shopping cart data.
    /// </summary>
    /// <param name="SKUPrice">SKU price</param>
    /// <param name="SKUDepartmentId">SKU department ID.</param>
    /// <param name="cart">Shopping cart object with required data for price formatting, if it is NULL shopping cart data from CMS context are used</param>
    /// <param name="SKUId">Product ID.</param>
    public static string GetFormatedPrice(object SKUPrice, object SKUDepartmentId, ShoppingCartInfo cart, object SKUId)
    {
        return GetFormatedPrice(SKUPrice, SKUDepartmentId, cart, SKUId, true);
    }


    /// <summary>
    /// Returns product price including all its taxes.
    /// </summary>
    /// <param name="SKUPrice">SKU price.</param>
    /// <param name="SKUDepartmentId">SKU department ID.</param>
    /// <param name="cart">Shopping cart with shopping data.</param>
    /// <param name="SKUId">SKU ID.</param>
    public static double GetPrice(object SKUPrice, object SKUDepartmentId, ShoppingCartInfo cart, object SKUId)
    {
        string price = GetFormatedPrice(SKUPrice, SKUDepartmentId, cart, SKUId, false);
        return Convert.ToDouble(price);
    }


    /// <summary>
    /// Returns product price without its taxes.
    /// </summary>
    /// <param name="SKUPrice">SKU price.</param>
    /// <param name="SKUDepartmentId">SKU department ID.</param>
    /// <param name="cart">Shopping cart with shopping data.</param>
    public static double GetPrice(object SKUPrice, object SKUDepartmentId, ShoppingCartInfo cart)
    {
        return GetPrice(SKUPrice, SKUDepartmentId, cart, 0);
    }


    /// <summary>
    /// Returns formatted price using specified shopping cart data.
    /// </summary>
    /// <param name="SKUPrice">SKU price</param>
    /// <param name="SKUDepartmentId">SKU department ID.</param>
    /// <param name="cart">Shopping cart object with required data for price formatting, if it is NULL shopping cart data from CMS context are used</param>
    /// <param name="SKUId">Product ID.</param>
    /// <param name="FormatPrice">Format output price.</param>
    private static string GetFormatedPrice(object SKUPrice, object SKUDepartmentId, ShoppingCartInfo cart, object SKUId, bool FormatPrice)
    {
        double price = ValidationHelper.GetDouble(SKUPrice, 0);
        int departmentId = ValidationHelper.GetInteger(SKUDepartmentId, 0);
        int skuId = ValidationHelper.GetInteger(SKUId, 0);

        ShoppingCartInfo currentShoppingCart = null;
        UserInfo currentUser = null;
        CustomerInfo currentCustomer = null;


        // Customer billing information
        int stateId = 0;
        int countryId = 0;
        bool isTaxIDSupplied = false;


        // When on the live site
        if (cart == null)
        {
            currentShoppingCart = ECommerceContext.CurrentShoppingCart;
            currentUser = CMSContext.CurrentUser;
            currentCustomer = ECommerceContext.CurrentCustomer;
        }
        // Other cases
        else
        {
            currentShoppingCart = cart;
            currentUser = cart.UserInfoObj;
            currentCustomer = cart.CustomerInfoObj;
        }

        if (departmentId > 0)
        {
            // Try to apply customer discount level value to product price
            if ((currentUser != null) && !currentUser.IsPublic() && (currentCustomer != null))
            {
                // Get discount level
                int discountLevelId = currentCustomer.CustomerDiscountLevelID;
                if (discountLevelId > 0)
                {
                    // Get discount level info
                    DiscountLevelInfo dli = DiscountLevelInfoProvider.GetDiscountLevelInfo(discountLevelId);

                    // Apply discount to product price 
                    if ((dli != null) && (dli.IsInDepartment(departmentId)) && (dli.IsValid) && (dli.DiscountLevelValue > 0))
                    {
                        // Remember price before discount
                        double oldPrice = price;

                        // Get new price after discount
                        price = price * (1 - dli.DiscountLevelValue / 100);

                        if ((oldPrice > 0) && (price < 0))
                        {
                            price = 0;
                        }
                    }
                }
            }
        }

        CurrencyInfo currentCurrency = null;
        double currentRate = 1.0;

        // Try to get shopping cart currency
        if ((currentShoppingCart != null) && (currentShoppingCart.CurrencyInfoObj != null))
        {
            currentCurrency = currentShoppingCart.CurrencyInfoObj;
            currentRate = currentShoppingCart.ExchangeRate;
        }
        // Try to get customer preffered currency
        else if (currentCustomer != null)
        {
            currentCurrency = CurrencyInfoProvider.GetCurrencyInfo(currentCustomer.CustomerPreferredCurrencyID);
            if (currentCurrency != null)
            {
                currentRate = GetPrefferedCurrencyExchangeRate(currentCurrency.CurrencyID);
                if (currentRate <= 0)
                {
                    currentRate = 1.0;
                }
            }
        }

        // If no currency found use default currency
        if (currentCurrency == null)
        {
            currentCurrency = CurrencyInfoProvider.GetMainCurrency();
            currentRate = 1.0;
        }

        // Try to get customer billing information from existing shopping cart
        if (currentShoppingCart != null)
        {
            stateId = currentShoppingCart.StateID;
            countryId = currentShoppingCart.CountryID;
            isTaxIDSupplied = ((currentShoppingCart.CustomerInfoObj != null) && (currentShoppingCart.CustomerInfoObj.CustomerTaxRegistrationID != ""));
        }
        // Use default values
        else
        {
            stateId = 0;
            countryId = 0;
            isTaxIDSupplied = false;

            // Use site default country
            SiteInfo currentSite = CMSContext.CurrentSite;
            if (currentSite != null)
            {
                countryId = currentSite.SiteDefaultCountryID;
            }
        }

        // USE FOLLOWING BLOCK OF CODE TO SHOW PRICE INCLUDING TAX OPTIONALLY
        //if ((currentUser != null) && (currentUser.IsInRole("vip", CMSContext.CurrentSiteName)))
        //{
        //    // DO NOT SHOW PRICE INLUCLUDING TAX FOR USERS IN "vip" ROLE
        //}
        //else if ((skuId > 0) && ((stateId > 0) || (countryId > 0)))
        //{
        //    // Apply taxes
        //    price += GetSKUTotalTax(price, skuId, stateId, countryId, isTaxIDSupplied);
        //}

        if ((skuId > 0) && ((stateId > 0) || (countryId > 0)))
        {
            // Apply taxes
            price += GetSKUTotalTax(price, skuId, stateId, countryId, isTaxIDSupplied);
        }

        // Apply exchange rate
        price = ExchangeTableInfoProvider.ApplyExchangeRate(price, currentRate);

        if (FormatPrice)
        {
            // Round price
            price = CurrencyInfoProvider.RoundTo(price, currentCurrency);

            // Return formatted price
            return CurrencyInfoProvider.GetFormatedPrice(price, currentCurrency);
        }
        else
        {
            // Return price without formating
            return price.ToString();
        }
    }


    /// <summary>
    /// Returns product total tax.
    /// </summary>
    /// <param name="skuPrice">SKU price.</param>
    /// <param name="skuId">SKU ID.</param>
    /// <param name="stateId">Customer billing address state ID.</param>
    /// <param name="countryId">Customer billing addres country ID.</param>
    /// <param name="isTaxIDSupplied">Indicates if customer tax registration ID is supplied.</param>    
    private static double GetSKUTotalTax(double skuPrice, int skuId, int stateId, int countryId, bool isTaxIDSupplied)
    {
        double totalTax = 0;
        int cacheMinutes = 0;

        // Try to get data from cache
        using (CachedSection<double> cs = new CachedSection<double>(ref totalTax, cacheMinutes, true, null, "skutotaltax|", skuId, skuPrice, stateId, countryId, isTaxIDSupplied))
        {
            if (cs.LoadData)
            {
                // Get all the taxes and their values which are applied to the specified product
                DataSet ds = TaxClassInfoProvider.GetSKUTaxClasses(skuId, stateId, countryId);
                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        bool zeroTax = ValidationHelper.GetBoolean(dr["TaxClassZeroIfIDSupplied"], false);
                        if (isTaxIDSupplied && zeroTax)
                        {
                            // Zero tax
                            break;
                        }
                        else
                        {
                            double taxValue = ValidationHelper.GetDouble(dr["TaxValue"], 0);
                            bool isFlat = ValidationHelper.GetBoolean(dr["IsFlatValue"], false);

                            // Add tax value                        
                            totalTax += CMS.CMSEcommerce.TaxClassInfoProvider.GetTaxValue(skuPrice, taxValue, isFlat);
                        }
                    }
                }

                // Cache the data
                cs.Data = totalTax;
            }
        }

        return totalTax;
    }


    /// <summary>
    /// Return link to "add to shoppingcart"
    /// </summary>
    /// <param name="productId">Product ID.</param>
    /// <param name="enabled">Indicates whether product is enabled or not.</param>
    public static string GetAddToShoppingCartLink(object productId, object enabled)
    {
        if (ValidationHelper.GetBoolean(enabled, false) && (ValidationHelper.GetInteger(productId, 0) != 0))
        {
            return "<img src=\"" + UIHelper.GetImageUrl(null, "CMSModules/CMS_Ecommerce/addorder.png") + "\" alt=\"Add to cart\" /><a href=\"" + ShoppingCartURL(CMSContext.CurrentSiteName) + "?productId=" + Convert.ToString(productId) + "&amp;quantity=1\">" + ResHelper.GetString("EcommerceFunctions.AddToShoppingCart") + "</a>";
        }
        else
        {
            return "";
        }
    }


    /// <summary>
    /// Return link to "add to shoppingcart"
    /// </summary>
    /// <param name="productId">Product ID.</param>
    public static string GetAddToShoppingCartLink(object productId)
    {
        return GetAddToShoppingCartLink(productId, true);
    }


    /// <summary>
    /// Returns link to add specified product to the user's wishlist.
    /// </summary>
    /// <param name="productId">Product ID.</param>
    public static string GetAddToWishListLink(object productId)
    {
        if (ValidationHelper.GetInteger(productId, 0) != 0)
        {
            return "<img src=\"" + UIHelper.GetImageUrl(null, "CMSModules/CMS_Ecommerce/addtowishlist.png") + "\" alt=\"Add to wishlist\" /><a href=\"" + WishlistURL(CMSContext.CurrentSiteName) + "?productId=" + Convert.ToString(productId) + "\">" + ResHelper.GetString("EcommerceFunctions.AddToWishlist") + "</a>";
        }
        else
        {
            return "";
        }
    }


    /// <summary>
    /// Returns link to remove specified product from the user's wishlist.
    /// </summary>
    /// <param name="productId">Product ID.</param>
    public static string GetRemoveFromWishListLink(object productId)
    {
        if ((productId != DBNull.Value) && (!CMSContext.CurrentUser.IsPublic()))
        {
            return "<a href=\"javascript:onclick=RemoveFromWishlist(" + Convert.ToString(productId) + ")\" class=\"RemoveFromWishlist\">" + ResHelper.GetString("Wishlist.RemoveFromWishlist") + "</a>";
        }
        else
        {
            return "";
        }
    }


    /// <summary>
    /// Returns the public SKU status display name
    /// </summary>
    /// <param name="statusId">Status ID</param>
    public static string GetPublicStatusName(object statusId)
    {
        int sid = ValidationHelper.GetInteger(statusId, 0);
        if (sid > 0)
        {
            PublicStatusInfo si = PublicStatusInfoProvider.GetPublicStatusInfo(sid);
            if (si != null)
            {
                return si.PublicStatusDisplayName;
            }
        }
        return "";
    }


    /// <summary>
    /// Returns URL to the shopping cart.
    /// </summary>
    /// <param name="siteName">Site name.</param>
    public static string ShoppingCartURL(string siteName)
    {
        string settingsKey = "CMSShoppingCartURL";

        // Create settingkey
        if (ValidationHelper.GetString(siteName, "") != "")
        {
            settingsKey = siteName + "." + settingsKey;
        }

        return UrlHelper.ResolveUrl(SettingsKeyProvider.GetStringValue(settingsKey));
    }


    /// <summary>
    /// Returns URL to the wish list.
    /// </summary>
    /// <param name="siteName">Site name</param>
    public static string WishlistURL(string siteName)
    {
        string settingsKey = "CMSWishlistURL";

        // Create settingkey
        if (ValidationHelper.GetString(siteName, "") != "")
        {
            settingsKey = siteName + "." + settingsKey;
        }

        return UrlHelper.ResolveUrl(SettingsKeyProvider.GetStringValue(settingsKey));
    }


    /// <summary>
    /// Returns product URL.
    /// </summary>
    /// <param name="SKUID">SKU ID</param>
    public static string GetProductUrl(object SKUID)
    {
        return UrlHelper.ResolveUrl("~/CMSPages/GetProduct.aspx?productId=" + Convert.ToString(SKUID));
    }


    /// <summary>
    /// Returns user friendly URL of the specified SKU and site name.
    /// </summary>
    /// <param name="skuGuid">SKU Guid</param>
    /// <param name="skuName">SKU Name</param>
    /// <param name="siteNameObj">Site Name</param>
    public static string GetProductUrl(object skuGuid, object skuName, object siteNameObj)
    {
        Guid guid = ValidationHelper.GetGuid(skuGuid, Guid.Empty);
        string name = Convert.ToString(skuName);
        string siteName = ValidationHelper.GetString(siteNameObj, null);
        return UrlHelper.ResolveUrl(SKUInfoProvider.GetSKUUrl(guid, name, siteName));
    }


    /// <summary>
    /// Returns user friendly URL of the specified SKU.
    /// </summary>
    /// <param name="skuGuid">SKU Guid</param>
    /// <param name="skuName">SKU Name</param>
    public static string GetProductUrl(object skuGuid, object skuName)
    {
        Guid guid = ValidationHelper.GetGuid(skuGuid, Guid.Empty);
        string name = Convert.ToString(skuName);
        return UrlHelper.ResolveUrl(SKUInfoProvider.GetSKUUrl(guid, name));
    }


    /// <summary>
    /// Sets different css styles to enabled and disabled dropdownlist items.
    /// </summary>
    /// <param name="drpTemp">Dropdownlist control.</param>
    /// <param name="valueFieldName">Field name with ID value.</param>
    /// <param name="statusFieldName">Field name with status value.</param>
    /// <param name="itemEnabledStyle">Enabled item style.</param>
    /// <param name="itemDisabledStyle">Disabled item style.</param>
    public static void MarkEnabledAndDisabledItems(DropDownList drpTemp, string valueFieldName, string statusFieldName, string itemEnabledStyle, string itemDisabledStyle)
    {
        itemEnabledStyle = (itemEnabledStyle == "") ? "DropDownItemEnabled" : itemEnabledStyle;
        itemDisabledStyle = (itemDisabledStyle == "") ? "DropDownItemDisabled" : itemDisabledStyle;

        if (!DataHelper.DataSourceIsEmpty(drpTemp.DataSource))
        {
            if (drpTemp.DataSource is DataSet)
            {
                ListItem li = null;

                foreach (DataRow row in ((DataSet)drpTemp.DataSource).Tables[0].Rows)
                {
                    li = drpTemp.Items.FindByValue(Convert.ToString(row[valueFieldName]));
                    if ((li != null) && (li.Value != "0"))
                    {
                        // Item is enabled
                        if (ValidationHelper.GetBoolean(row[statusFieldName], false))
                        {
                            li.Attributes.Add("class", itemEnabledStyle);
                        }
                        // Item is disabled
                        else
                        {
                            li.Attributes.Add("class", itemDisabledStyle);
                        }
                    }
                }
            }
        }
    }


    /// <summary>
    /// Sets different css styles to enabled and disabled dropdownlist items.
    /// </summary>
    /// <param name="drpTemp">Dropdownlist control.</param>
    /// <param name="valueFieldName">Field name with ID value.</param>
    /// <param name="statusFieldName">Field name with status value.</param>
    public static void MarkEnabledAndDisabledItems(DropDownList drpTemp, string valueFieldName, string statusFieldName)
    {
        MarkEnabledAndDisabledItems(drpTemp, valueFieldName, statusFieldName, "", "");
    }


    /// <summary>
    /// Gets object from the specified column of the manufacturer with specific ID.
    /// </summary>
    /// <param name="Id">Manufacturer ID.</param>
    /// <param name="column">Column name.</param>
    public static object GetManufacturer(object Id, string column)
    {
        int id = ValidationHelper.GetInteger(Id, 0);

        if (id > 0 && !DataHelper.IsEmpty(column))
        {
            // Get manufacturer
            ManufacturerInfo mi = ManufacturerInfoProvider.GetManufacturerInfo(id);

            if (mi != null)
            {
                // Return datarow value if specified column exists
                if (mi.DataClass.DataRow.Table.Columns.Contains(column))
                {
                    return DataHelper.GetDataRowValue(mi.DataClass.DataRow, column);
                }
                else
                {
                    return "";
                }
            }
        }

        return "";
    }


    /// <summary>
    /// Gets object from the specified column of the department with specific ID.
    /// </summary>
    /// <param name="Id">Department ID.</param>
    /// <param name="column">Column name.</param>
    public static object GetDepartment(object Id, string column)
    {
        int id = ValidationHelper.GetInteger(Id, 0);

        if (id > 0 && !DataHelper.IsEmpty(column))
        {
            // Get department
            DepartmentInfo di = DepartmentInfoProvider.GetDepartmentInfo(id);

            if (di != null)
            {
                // Return datarow value if specified column exists
                if (di.DataClass.DataRow.Table.Columns.Contains(column))
                {
                    return DataHelper.GetDataRowValue(di.DataClass.DataRow, column);
                }
                else
                {
                    return "";
                }
            }
        }

        return "";
    }


    /// <summary>
    /// Gets object from the specified column of the supplier with specific ID.
    /// </summary>
    /// <param name="Id">Supplier ID.</param>
    /// <param name="column">Column name.</param>
    public static object GetSupplier(object Id, string column)
    {
        int id = ValidationHelper.GetInteger(Id, 0);

        if (id > 0 && !DataHelper.IsEmpty(column))
        {
            // Get supplier
            SupplierInfo si = SupplierInfoProvider.GetSupplierInfo(id);

            if (si != null)
            {
                // Return datarow value if specified column exists
                if (si.DataClass.DataRow.Table.Columns.Contains(column))
                {
                    return DataHelper.GetDataRowValue(si.DataClass.DataRow, column);
                }
                else
                {
                    return "";
                }
            }
        }

        return "";
    }


    /// <summary>
    /// Gets object from the specified column of the internal status with specific ID.
    /// </summary>
    /// <param name="Id">Internal status ID.</param>
    /// <param name="column">Column name.</param>
    public static object GetInternalStatus(object Id, string column)
    {
        int id = ValidationHelper.GetInteger(Id, 0);

        if (id > 0 && !DataHelper.IsEmpty(column))
        {
            // Get internal status
            InternalStatusInfo isi = InternalStatusInfoProvider.GetInternalStatusInfo(id);

            if (isi != null)
            {
                // Return datarow value if specified column exists
                if (isi.DataClass.DataRow.Table.Columns.Contains(column))
                {
                    return DataHelper.GetDataRowValue(isi.DataClass.DataRow, column);
                }
                else
                {
                    return "";
                }
            }
        }

        return "";
    }


    /// <summary>
    /// Gets object from the specified column of the public status with specific ID.
    /// </summary>
    /// <param name="Id">Public status ID.</param>
    /// <param name="column">Column name.</param>
    public static object GetPublicStatus(object Id, string column)
    {
        int id = ValidationHelper.GetInteger(Id, 0);

        if (id > 0 && !DataHelper.IsEmpty(column))
        {
            // Get public status
            PublicStatusInfo psi = PublicStatusInfoProvider.GetPublicStatusInfo(id);

            if (psi != null)
            {
                // Return datarow value if specified column exists
                if (psi.DataClass.DataRow.Table.Columns.Contains(column))
                {
                    return DataHelper.GetDataRowValue(psi.DataClass.DataRow, column);
                }
                else
                {
                    return "";
                }
            }
        }

        return "";
    }


    /// <summary>
    /// Gets document name of specified nodeid
    /// </summary>
    public static string GetDocumentName(object nodeIdent)
    {
        int nodeId = ValidationHelper.GetInteger(nodeIdent, 0);
        if (nodeId != 0)
        {
            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
            TreeNode node = tree.SelectSingleNode(nodeId, CMSContext.PreferredCultureCode);
            if (node != null)
            {
                return node.DocumentName;
            }
        }
        return String.Empty;
    }


    /// <summary>
    /// Returns complet HTML code of the specified product image, if not such image exists, default image is returned.
    /// </summary>
    /// <param name="imageUrl">Product image url.</param>    
    /// <param name="alt">Image alternate text.</param>
    public static string GetProductImage(object imageUrl, object alt)
    {
        return GetImage(imageUrl, 0, 0, 0, alt);
    }


    /// <summary>
    /// Returns complet HTML code of the specified resized product image, if not such image exists, default image is returned.
    /// </summary>
    /// <param name="imageUrl">Product image url.</param>
    /// <param name="height">Height of image.</param>
    /// <param name="alt">Image alternate text.</param>
    /// <param name="width">Width of image.</param>
    public static string GetProductImage(object imageUrl, object width, object height, object alt)
    {
        return GetImage(imageUrl, width, height, 0, alt);
    }


    /// <summary>
    /// Returns complet HTML code of the specified resized product image, if not such image exists, default image is returned.
    /// </summary>
    /// <param name="imageUrl">Product image url.</param>
    /// <param name="maxSideSize">Max side size.</param>   
    /// <param name="alt">Image alternate text.</param>
    public static string GetProductImage(object imageUrl, object maxSideSize, object alt)
    {
        return GetImage(imageUrl, 0, 0, maxSideSize, alt);
    }


    private static string GetImage(object imageUrl, object width, object height, object maxSideSize, object alt)
    {
        string queryString = "";

        int iMaxSideSize = ValidationHelper.GetInteger(maxSideSize, 0);
        int iWidth = ValidationHelper.GetInteger(width, 0);
        int iHeight = ValidationHelper.GetInteger(height, 0);

        // Get image alternate text
        string sAlt = ValidationHelper.GetString(alt, "");
        if (sAlt != "")
        {
            sAlt = ResHelper.LocalizeString(sAlt);
        }

        // Add max side size
        if (iMaxSideSize > 0)
        {
            queryString = "?maxSideSize=" + iMaxSideSize;
        }
        else
        {
            // Add width
            if (iWidth > 0)
            {
                queryString += "?width=" + iWidth;
            }

            // Add height
            if (iHeight > 0)
            {
                queryString += (queryString == "") ? "?" : "&";
                queryString += "height=" + iHeight;
            }
        }

        // Get product image url        
        string url = ValidationHelper.GetString(imageUrl, "");

        // If product image not found
        if (url == "")
        {
            // Get default product image url                      
            url = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSDefaultProductImageURL");
        }

        // Get url with application path
        if (!String.IsNullOrEmpty(url))
        {
            url = UrlHelper.ResolveUrl(url);
            int slashIndex = url.LastIndexOf('/');
            if (slashIndex >= 0)
            {
                string urlStartPart = url.Substring(0, slashIndex);
                string urlEndPart = url.Substring(slashIndex);

                url = urlStartPart + HttpUtility.UrlPathEncode(urlEndPart) + queryString;
            }

            return "<img alt=\"" + HTMLHelper.HTMLEncode(sAlt) + "\" src=\"" + HTMLHelper.HTMLEncode(url) + "\" border=\"0\" />";
        }

        return "";
    }
}
