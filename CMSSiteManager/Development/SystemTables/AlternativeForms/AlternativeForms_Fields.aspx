<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AlternativeForms_Fields.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="ALternative forms - fields"
    Inherits="CMSSiteManager_Development_SystemTables_AlternativeForms_AlternativeForms_Fields"
    Theme="Default" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/AlternativeFormFieldEditor.ascx" TagName="AlternativeFormFieldEditor" TagPrefix="cms" %>    

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:AlternativeFormFieldEditor ID="altFormFieldEditor" runat="server" /> 
</asp:Content>

