using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using CMS.SettingsProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Settings_categories : SiteManagerPage
{
    protected int siteId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureHelper.IsUICultureRTL())
        {
            TreeViewCategories.LineImagesFolder = GetImageUrl("RTL/Design/Controls/Tree", false, true);
        }
        else
        {
            TreeViewCategories.LineImagesFolder = GetImageUrl("Design/Controls/Tree", false, true);
        }
        TreeViewCategories.ImageSet = TreeViewImageSet.Custom;
        TreeViewCategories.ExpandImageToolTip = ResHelper.GetString("General.Expand");
        TreeViewCategories.CollapseImageToolTip = ResHelper.GetString("General.Collapse");

        lblSite.Text = ResHelper.GetString("general.site") + ResHelper.Colon;

        // Style site selector 
        siteSelector.DropDownSingleSelect.CssClass = "";
        siteSelector.DropDownSingleSelect.Attributes.Add("style", "width: 100%");

        // Set site selector
        siteSelector.DropDownSingleSelect.AutoPostBack = true;
        siteSelector.AllowAll = false;
        siteSelector.UniSelector.SpecialFields = new string[1, 2] { { ResHelper.GetString("general.global"), "0" } };
        siteSelector.OnlyRunningSites = false;

        // Get selected site id
        siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);

        CreateCategoryTreeView();
    }


    /// <summary>
    /// Creates catogory tree view
    /// </summary>
    private void CreateCategoryTreeView()
    {
        TreeViewCategories.Nodes.Clear();

        // Fill in the categories
        TreeNode rootNode = new TreeNode();
        rootNode.Text = ResHelper.GetString("Settings-Categories.RootNodeText");
        rootNode.Expanded = true;
        rootNode.NavigateUrl = "#";
        TreeViewCategories.Nodes.Add(rootNode);

        int categoryId = 0;
        if (Request.Params["selectedCategoryId"] != null)
        {
            categoryId = ValidationHelper.GetInteger(Request.Params["selectedCategoryId"], 0);
        }
        // Initialize tree view with categories of site selected in sites.aspx frame.
        DataSet categoriesSet = SettingsKeyProvider.GetSettingsCategories(siteId);
        DataTable categoriesTable = categoriesSet.Tables[0];
        bool categorySelected = false;

        foreach (DataRow categoriesRow in categoriesTable.Rows)
        {
            string categoryDisplayName = ResHelper.LocalizeString(categoriesRow["CategoryDisplayName"].ToString());

            // Icon
            string categoryName = ValidationHelper.GetString(categoriesRow["CategoryName"], "");
            string imageUrl = GetImageUrlForTree(categoryName);


            TreeNode categoryNode = new TreeNode();
            int currentId = ValidationHelper.GetInteger(categoriesRow["CategoryID"], 0);
            if (currentId == categoryId)
            {
                categoryNode.Text = "<span class=\"ContentTreeSelectedItem\" id=\"treeSelectedNode\" onclick=\"RefreshKeys(" + categoriesRow["CategoryID"] + ", " + siteId + ", this); \"><img class=\"TreeItemImage\" src=\"" + imageUrl + "\" alt=\"\" /><span class=\"Name\">" + HTMLHelper.HTMLEncode(categoryDisplayName) + "</span></span>";
                categorySelected = true;
            }
            else
            {
                categoryNode.Text = "<span class=\"ContentTreeItem\" onclick=\"RefreshKeys(" + categoriesRow["CategoryID"] + ", " + siteId + ", this); \"><img class=\"TreeItemImage\" src=\"" + imageUrl + "\" alt=\"\" /><span class=\"Name\">" + HTMLHelper.HTMLEncode(categoryDisplayName) + "</span></span>";
            }
            categoryNode.Value = categoriesRow["CategoryID"].ToString();
            categoryNode.NavigateUrl = "#";

            TreeViewCategories.Nodes[0].ChildNodes.Add(categoryNode);
        }

        if ((!categorySelected) && (TreeViewCategories.Nodes[0].ChildNodes.Count > 0))
        {
            TreeViewCategories.Nodes[0].ChildNodes[0].Text = TreeViewCategories.Nodes[0].ChildNodes[0].Text.Replace("class=\"ContentTreeItem\"", "class=\"ContentTreeSelectedItem\" id=\"treeSelectedNode\"");
        }

        ScriptHelper.RegisterStartupScript(Page, typeof(string), "currentNode", ScriptHelper.GetScript("var currentNode = document.getElementById('treeSelectedNode');"));

        if ((categoryId > 0) && categorySelected)
        {
            ScriptHelper.RegisterStartupScript(Page, typeof(string), "AddNewItem", ScriptHelper.GetScript(" \t RefreshKeys(" + categoryId + ", " + siteId + ");"));
        }
        else
        {
            if (TreeViewCategories.Nodes[0].ChildNodes.Count > 0)
            {
                ScriptHelper.RegisterStartupScript(Page, typeof(string), "AddNewItem", ScriptHelper.GetScript(" \t RefreshKeys(" + TreeViewCategories.Nodes[0].ChildNodes[0].Value + ", " + siteId + ");"));
            }
        }
    }
    

    /// <summary>
    /// Returns image URL for category
    /// </summary>
    /// <param name="categoryName">Category name</param>
    private string GetImageUrlForTree(string categoryName)
    {
        string imageUrl = String.Empty;

        if (categoryName != "")
        {
            imageUrl = GetImageUrl("CMSModules/CMS_Settings/Categories/") + categoryName.Replace(".", "_") + "/list.png";

            // Check if category icon exists and then use it
            if (File.Exists(UrlHelper.GetPhysicalPath(imageUrl)))
            {
                imageUrl = ResolveUrl(imageUrl);
            }
            // Otherwise use default icon
            else
            {
                imageUrl = ResolveUrl(GetImageUrl("CMSModules/CMS_Settings/Categories/list.png"));
            }
        }

        return imageUrl;
    }
}
