using System;
using System.IO;
using System.Data;
using System.Collections;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.WebAnalytics;
using CMS.CMSHelper;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_WebAnalytics_Tools_Analytics_Statistics : CMSWebAnalyticsPage
{
    const string VISIT_FIRST_CODE = "visitfirst";
    const string VISIT_RETURN_CODE = "visitreturn";
    const string VISIT_CODE_NAME = "visitors";
    const string ANAL_CODE_PREFIX = "analytics_codename.";
    const string MULTILINGUAL_SUFFIX = ".multilingual";

    readonly string[] DEFAULT_STATS = {
        HitLogProvider.AGGREGATED_VIEWS,
        HitLogProvider.AGGREGATED_VIEWS + MULTILINGUAL_SUFFIX,
        HitLogProvider.PAGE_VIEWS,
        HitLogProvider.PAGE_VIEWS + MULTILINGUAL_SUFFIX,
        HitLogProvider.VISITORS_FIRST,
        HitLogProvider.VISITORS_RETURNING,
        HitLogProvider.FILE_DOWNLOADS,
        HitLogProvider.FILE_DOWNLOADS + MULTILINGUAL_SUFFIX,
        HitLogProvider.PAGE_NOT_FOUND,
        HitLogProvider.URL_REFERRALS,
        HitLogProvider.CONVERSIONS,
        HitLogProvider.CAMPAIGNS,
        HitLogProvider.BROWSER_TYPE,
        HitLogProvider.COUNTRIES };


    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureHelper.IsUICultureRTL())
        {
            treeStats.LineImagesFolder = GetImageUrl("RTL/Design/Controls/Tree", false, true);
        }
        else
        {
            treeStats.LineImagesFolder = GetImageUrl("Design/Controls/Tree", false, true);
        }

        // Clear all nodes
        treeStats.Nodes.Clear();

        // Create root node
        TreeNode rootNode = new TreeNode(ResHelper.GetString("Analytics.Root"));
        rootNode.Expanded = true;
        rootNode.NavigateUrl = "#";

        treeStats.Nodes.Add(rootNode);
        string imagesUrl = "CMSModules/CMS_WebAnalytics/";
        bool isFirst = true;
        bool isVisit = true;

        // Creating code name list (default + code names from database)
        DataSet ds = StatisticsInfoProvider.GetCodeNames();

        ArrayList a = new ArrayList();
        a.AddRange(DEFAULT_STATS);                             // Default code names
        foreach (DataRow dr in ds.Tables[0].Rows)              // Code names from database
        {
            a.Add(dr["StatisticsCode"].ToString().ToLower());
        }

        a.Sort();

        // Discard duplicate items if any
        for (int i = a.Count - 1; i > 0; i--)
        {
            if (a[i - 1].ToString() == a[i].ToString())
            {
                a.RemoveAt(i);
            }
        }

        if (a.Count > 0)
        {
            string itm = null;
            string imgPath = null;
            TreeNode childNode = null;
            string label = null;
            string visitsCodeName = null;

            // Go through selected modules and add them to the tree menu
            foreach (object item in a)
            {
                label = null;
                visitsCodeName = null;    // Additional code name for visits stats (visits stats consist of two categories)
                childNode = new TreeNode();
                itm = ValidationHelper.GetString(item, "");

                if ((itm == VISIT_FIRST_CODE) || (itm == VISIT_RETURN_CODE))
                {
                    if (isVisit)
                    {
                        label = ResHelper.GetString(ANAL_CODE_PREFIX + "visits");
                        isVisit = false;
                        visitsCodeName = "&additionalCodeName=" + (itm == VISIT_FIRST_CODE ? VISIT_RETURN_CODE : VISIT_FIRST_CODE);
                    }
                }
                else
                {
                    label = ResHelper.GetString(ANAL_CODE_PREFIX + itm);
                }

                if (label != null)
                {
                    if (isFirst)
                    {
                        ltlScript.Text = ScriptHelper.GetScript("parent.frames['analmonths'].location.href= 'Analytics_Months.aspx?statCodeName=" +
                            itm + "&dataCodeName=" + GetDataCodeName(itm) +
                            "&reportCodeName=" + GetReportCodeNames(itm) + "'");

                        childNode.Text = "<span id=\"treeSelectedNode\" class=\"ContentTreeSelectedItem\"" +
                            " onclick=\"ShowDesktopContent('Analytics_Months.aspx?statCodeName=" +
                            itm + "&dataCodeName=" + GetDataCodeName(itm) +
                            "&reportCodeName=" + GetReportCodeNames(itm) +
                            visitsCodeName + "', this);\"><span class=\"Name\">" +
                                     label + "</span></span>";
                        isFirst = false;
                    }
                    else
                    {
                        childNode.Text = "<span class=\"ContentTreeItem\" onclick=\"ShowDesktopContent('Analytics_Months.aspx?statCodeName=" +
                            itm + "&dataCodeName=" + GetDataCodeName(itm) +
                            "&reportCodeName=" + GetReportCodeNames(itm) +
                            visitsCodeName + "', this);\"><span class=\"Name\">" +
                                         label + "</span></span>";
                    }

                    imgPath = GetImageUrl(imagesUrl + itm.Replace(".", "_") + ".png");
                    if (File.Exists(Server.MapPath(imgPath)))
                    {
                        childNode.ImageUrl = imgPath;
                    }
                    else
                    {
                        childNode.ImageUrl = GetImageUrl(imagesUrl + "statistics.png");
                    }

                    childNode.NavigateUrl = "#";
                    rootNode.ChildNodes.Add(childNode);
                }
            }
        }
    }


    /// <summary>
    /// Returns generic report code names (based on analytics code name).
    /// </summary>
    /// <param name="statCodeName">Analytics code name (pageviews, pageviews.multilingual...)</param>
    private static string GetReportCodeNames(string statCodeName)
    {
        string result = "";

        if ((VISIT_FIRST_CODE == statCodeName) || (VISIT_RETURN_CODE == statCodeName))
        {
            statCodeName = VISIT_CODE_NAME;
        }

        result += statCodeName + ".yearreport";
        result += ";" + statCodeName + ".monthreport";
        result += ";" + statCodeName + ".weekreport";
        result += ";" + statCodeName + ".dayreport";
        result += ";" + statCodeName + ".hourreport";
        return result;
    }


    /// <summary>
    /// Returns data code name from analytics code name.
    /// </summary>
    /// <param name="statCodeName">Analytics code name (pageviews, pageviews.multilingual...)</param>
    private static string GetDataCodeName(string statCodeName)
    {
        int pos = statCodeName.IndexOf('.');
        if (pos > 0)
        {
            return statCodeName.Substring(0, pos);
        }

        return statCodeName;
    }
}
