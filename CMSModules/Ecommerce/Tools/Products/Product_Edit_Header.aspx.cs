using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.FormControls;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Products_Product_Edit_Header : CMSEcommercePage
{
    protected int productId = 0;
    protected string productName = "";
    protected int optionCategoryId = 0;
    

    protected void Page_PreInit(object sender, EventArgs e)
    {
        int hideBreadcrumbs = ValidationHelper.GetInteger(Request.QueryString["hidebreadcrumbs"], 0);

        // Get product name ane option category ID
        productId = ValidationHelper.GetInteger(Request.QueryString["productId"], 0);
        if (productId > 0)
        {
            SKUInfo productInfoObj = SKUInfoProvider.GetSKUInfo(productId);
            if (productInfoObj != null)
            {
                productName = ResHelper.LocalizeString(productInfoObj.SKUName);
                optionCategoryId = productInfoObj.SKUOptionCategoryID;
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        string productList = "";
        string productListText = "";
        string targetFrame = "";  
        string titleText = "";
        string titleImage = "";

        // When editing product option
        if (optionCategoryId > 0)
        {
            productListText = ResHelper.GetString("Prodect_Edit_Header.ProductOptionsLink");
            productList = "~/CMSModules/Ecommerce/Tools/ProductOptions/OptionCategory_Edit_Options.aspx?categoryid=" + optionCategoryId;
            targetFrame = "OptionCategoryEdit";
        }
        // When editing product
        else
        {
            productListText = ResHelper.GetString("Product_Edit_Header.ItemListLink");
            productList = "~/CMSModules/Ecommerce/Tools/Products/Product_List.aspx";            
            targetFrame = "ecommerceContent";

            titleText = ResHelper.GetString("Product_Edit_Header.HeaderCaption");
            titleImage = GetImageUrl("Objects/Ecommerce_SKU/object.png");
        }        

        int hideBreadcrumbs = ValidationHelper.GetInteger(Request.QueryString["hidebreadcrumbs"], 0);

        if (hideBreadcrumbs == 0)
        {
            // initializes page title control		
            IntializeBreadcrumbs(productListText, productList, targetFrame, productName);
        }

        // custom fields
        DataForm df = new DataForm();
        df.ClassName = "Ecommerce.SKU";
        df.ItemID = productId;

        int showCustomFields = (df.BasicForm.FormInformation.GetFormElements(true, false).Count <= 0 ? 0 : 1); ;
        
        // Initialize the master page elements
        IntializeMasterPage(titleImage, titleText, showCustomFields);
    }


    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    /// <param name="productListText">Text of the product list</param>
    /// <param name="productList">Product list</param>
    /// <param name="targetFrame">Name of the target frame</param>
    /// <param name="productName">Name of the current product</param>
    private void IntializeBreadcrumbs(string productListText, string productList, string targetFrame, string productName)
    {
        // Set the master page breadcrumb property
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = productListText;
        pageTitleTabs[0, 1] = productList;
        pageTitleTabs[0, 2] = targetFrame;
        pageTitleTabs[1, 0] = productName;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    /// <param name="titleImage">Title of the page title element</param>
    /// <param name="titleText">URL of the image of the page title element</param>
    private void IntializeMasterPage(string titleImage, string titleText, int showCustomFields)
    {
        // Set the master page title
        this.CurrentMaster.Title.HelpTopicName = "general_tab15";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        int showDocs = ((optionCategoryId == 0) ? 3 : 0);

        // Set the tabs of the master page
        string[,] tabs = new string[2 + showCustomFields + showDocs, 4];
        int lastTabIndex = 0;

        tabs[lastTabIndex, 0] = ResHelper.GetString("general.general");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab15');";
        tabs[lastTabIndex, 2] = "Product_Edit_General.aspx?productId=" + productId.ToString();
        lastTabIndex++;

        if (showCustomFields != 0)
        {
            tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.CustomFields");
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'custom_fields_tab3');";
            tabs[lastTabIndex, 2] = "Product_Edit_CustomFields.aspx?productId=" + productId.ToString();
            lastTabIndex++;
        }

        tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.Tax");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'tax_classes');";
        tabs[lastTabIndex, 2] = "Product_Edit_Tax.aspx?productId=" + productId.ToString();
        lastTabIndex++;

        if (optionCategoryId == 0)
        {
            tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.VolumeDiscounts");
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'volume_discount');";
            tabs[lastTabIndex, 2] = "Product_Edit_VolumeDiscount_List.aspx?productId=" + productId.ToString();
            lastTabIndex++;

            tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.Options");
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'options_tab');";
            tabs[lastTabIndex, 2] = "Product_Edit_Options.aspx?productId=" + productId.ToString();
            lastTabIndex++;

            tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.Documents");
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'documents');";
            tabs[lastTabIndex, 2] = "Product_Edit_Documents.aspx?productId=" + productId.ToString();
            lastTabIndex++;
        }

        this.CurrentMaster.Tabs.UrlTarget = "ProductContent";
        this.CurrentMaster.Tabs.Tabs = tabs;
    }
}
