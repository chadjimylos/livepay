<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AlternativeForms_Fields.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="ALternative forms - fields"
    Inherits="CMSModules_BizForms_Tools_AlternativeForms_AlternativeForms_Fields"
    Theme="Default" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/AlternativeFormFieldEditor.ascx" TagName="AlternativeFormFieldEditor" TagPrefix="cms" %>    

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" Visible="false" />
    <cms:AlternativeFormFieldEditor ID="altFormFieldEditor" runat="server" /> 
</asp:Content>

