<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Documentation.aspx.cs" Inherits="CMSPages_Dialogs_Documentation"
     %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>WebParts documentation</title>
    <style media="print" type="text/css">
        .noprint 
        {
            display:none;
        }
    </style>
    <link href="~/App_Themes/Default/CMSDesk.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <a name="top"></a>
        <table style="width: 100%; background-color: #EDF4F9;">
            <tr>
                <td>
                    <h1>
                        Viva CMS Web Parts</h1>
                </td>
                <td class="TextRight">
                    <a href="<%= querystringvalue  %>" class="noprint">Full documentation</a>
                </td>
            </tr>
        </table>
        <asp:Literal runat="server" ID="ltrContent" />
    </form>
</body>
</html>
