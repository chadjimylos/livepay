using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.ExtendedControls;

public partial class CMSAdminControls_UI_UniSelector_UniSelector : UniSelector, IPostBackEventHandler, ICallbackEventHandler
{
    #region "Private variables"

    private string checkBoxClass = null;
    private IInfoObject objectType = null;
    private DataSet result = null;
    private bool enabled = true;
    TextHelper th = new TextHelper();
    Hashtable parameters = new Hashtable();
    private bool mLoaded = false;
    private bool mPageChanged = false;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Dialog control identificator
    /// </summary>
    protected string Identificator
    {
        get
        {
            string identificator = (string)ViewState["Identificator"];
            if (identificator == null)
            {
                identificator = Guid.NewGuid().ToString();
                ViewState["Identificator"] = identificator;
            }

            return identificator;
        }
    }


    /// <summary>
    /// Gets the value that indicates whether current selector in multiple mode displays some data or whether the dropdown contains some data
    /// </summary>
    public override bool HasData
    {
        get
        {
            // Ensure the data
            if (!StopProcessing)
            {
                Reload(false);
            }

            return ValidationHelper.GetBoolean(ViewState["HasData"], false);
        }
        protected set
        {
            ViewState["HasData"] = value;
        }
    }


    /// <summary>
    /// Zero rows text
    /// </summary>
    public string ZeroRowsText
    {
        get
        {
            return ValidationHelper.GetString(ViewState["ZeroRowsText"], null);
        }
        set
        {
            ViewState["ZeroRowsText"] = value;
        }
    }


    /// <summary>
    /// Value of the "(all)" DDL record, -1 by default.
    /// </summary>
    public string AllRecordValue
    {
        get
        {
            return ValidationHelper.GetString(ViewState["AllRecordValue"], "-1");
        }
        set
        {
            ViewState["AllRecordValue"] = value;
        }
    }


    /// <summary>
    /// Value of the "(none)" DDL record, 0 by default.
    /// </summary>
    public string NoneRecordValue
    {
        get
        {
            return ValidationHelper.GetString(ViewState["NoneRecordValue"], "0");
        }
        set
        {
            ViewState["NoneRecordValue"] = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            switch (SelectionMode)
            {
                // Dropdown mode
                case SelectionModeEnum.SingleDropDownList:
                    if (!String.IsNullOrEmpty(hdnDialogSelect.Value))
                    {
                        return hdnDialogSelect.Value;
                    }
                    else
                    {
                        return drpSingleSelect.SelectedValue;
                    }

                // Textbox mode
                case SelectionModeEnum.SingleTextBox:
                case SelectionModeEnum.MultipleTextBox:
                    if (AllowEditTextBox)
                    {
                        return txtSingleSelect.Text.Trim(ValuesSeparator.ToCharArray());
                    }
                    else
                    {
                        return hiddenField.Value.Trim(ValuesSeparator.ToCharArray());
                    }

                // Other modes
                default:
                    return hiddenField.Value.Trim(ValuesSeparator.ToCharArray());
            }
        }
        set
        {
            switch (SelectionMode)
            {
                // Dropdown mode
                case SelectionModeEnum.SingleDropDownList:
                    hdnDialogSelect.Value = ValidationHelper.GetString(value, "");
                    break;

                // Textbox mode
                case SelectionModeEnum.SingleTextBox:
                case SelectionModeEnum.MultipleTextBox:
                    if (AllowEditTextBox)
                    {
                        txtSingleSelect.Text = ValidationHelper.GetString(value, "").Trim(ValuesSeparator.ToCharArray());
                    }
                    else
                    {
                        hiddenField.Value = ";" + ValidationHelper.GetString(value, "").Trim(ValuesSeparator.ToCharArray()) + ";";
                    }
                    break;

                // Other modes
                default:
                    hiddenField.Value = ";" + ValidationHelper.GetString(value, "").Trim(ValuesSeparator.ToCharArray()) + ";";
                    break;
            }
        }
    }


    /// <summary>
    /// Gets or sets if control is enabled.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return enabled;
        }
        set
        {
            enabled = value;

            btnClear.Enabled = enabled;
            btnSelect.Enabled = enabled;
            btnDialog.Enabled = enabled;
            lnkDialog.Enabled = enabled;
            drpSingleSelect.Enabled = enabled;
            txtSingleSelect.Enabled = enabled;
            btnRemoveSelected.Enabled = enabled;
            btnRemoveAll.Enabled = enabled;
            btnAddItems.Enabled = enabled;
        }
    }

    #endregion


    #region "Controls properties"

    /// <summary>
    /// Gets the single select drop down field
    /// </summary>
    public DropDownList DropDownSingleSelect
    {
        get
        {
            return drpSingleSelect;
        }
    }


    /// <summary>
    /// Drop down list selection edit button
    /// </summary>
    public Button ButtonDropDownEdit
    {
        get
        {
            return btnDropEdit;
        }
    }


    /// <summary>
    /// Gets the Select button control
    /// </summary>
    public Button ButtonSelect
    {
        get
        {
            return btnSelect;
        }
    }


    /// <summary>
    /// Gets the Clear button control
    /// </summary>
    public Button ButtonClear
    {
        get
        {
            return btnClear;
        }
    }


    /// <summary>
    /// Gets the Remove selected items button
    /// </summary>
    public Button ButtonRemoveSelected
    {
        get
        {
            return btnRemoveSelected;
        }
    }


    /// <summary>
    /// Gets the Remove all items button
    /// </summary>
    public Button ButtonRemoveAll
    {
        get
        {
            return btnRemoveAll;
        }
    }


    /// <summary>
    /// Gets the Add items button control
    /// </summary>
    public Button ButtonAddItems
    {
        get
        {
            return btnAddItems;
        }
    }


    /// <summary>
    /// Gets the text box selection control
    /// </summary>
    public TextBox TextBoxSelect
    {
        get
        {
            return txtSingleSelect;
        }
    }


    /// <summary>
    /// Textbox selection edit button
    /// </summary>
    public Button ButtonEdit
    {
        get
        {
            return btnEdit;
        }
    }


    /// <summary>
    /// Multiple selection grid
    /// </summary>
    public UniGrid UniGrid
    {
        get
        {
            return uniGrid;
        }
    }


    /// <summary>
    /// Button selection control
    /// </summary>
    public Button ButtonDialog
    {
        get
        {
            return btnDialog;
        }
    }


    /// <summary>
    /// Link selection control
    /// </summary>
    public HyperLink LinkDialog
    {
        get
        {
            return lnkDialog;
        }
    }


    /// <summary>
    /// Link selection image control
    /// </summary>
    public Image ImageDialog
    {
        get
        {
            return imgDialog;
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Return true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        return true;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register script for pendingCallbacks repair
        ScriptHelper.FixPendingCallbacks(this.Page);

        checkBoxClass = Guid.NewGuid().ToString().Replace("-", "");

        // Bound events
        drpSingleSelect.SelectedIndexChanged += drpSingleSelect_SelectedIndexChanged;

        uniGrid.OnExternalDataBound += uniGrid_OnExternalDataBound;
        uniGrid.OnPageChanged += new EventHandler<EventArgs>(uniGrid_OnPageChanged);
        uniGrid.IsLiveSite = IsLiveSite;

        btnClear.Click += btnClear_Click;

        // If control is enabled, then display content
        if (!StopProcessing && (SelectionMode == SelectionModeEnum.Multiple))
        {
            Reload(false);
        }

        if ((ObjectType == "cms.role" || ObjectType == "cms.user") && CMSContext.CurrentUser.IsInRole("eBusinessOps", CMSContext.CurrentSite.SiteName.ToString()))
        {
            btnAddItems.Visible = false;
            btnRemoveSelected.Visible = false;
            btnRemoveAll.Visible = false;
        }
    }



    /// <summary>
    /// Change header title for multiple selection
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        // If control is enabled, then display content
        if (!StopProcessing && (SelectionMode != SelectionModeEnum.Multiple))
        {
            Reload(false);
        }

        // Display two columns when in multiple selection
        if ((SelectionMode == SelectionModeEnum.Multiple) && (uniGrid.GridView.HeaderRow != null))
        {
            CheckBox chkAll = new CheckBox();
            chkAll.ID = "chkAll";
            chkAll.ToolTip = ResHelper.GetString("General.CheckAll");
            chkAll.InputAttributes.Add("onclick", "US_SelectAllItems('" + ClientID + "', '" + ValuesSeparator + "', this, 'chk" + checkBoxClass + "')");

            uniGrid.GridView.HeaderRow.Cells[0].Controls.Clear();
            uniGrid.GridView.HeaderRow.Cells[0].Controls.Add(chkAll);
            uniGrid.GridView.Columns[0].ItemStyle.CssClass = "UnigridSelection";

            uniGrid.GridView.HeaderRow.Cells[1].Text = ResHelper.GetString(ResourcePrefix + ".itemname|general.itemname");
        }

        lblStatus.Visible = !String.IsNullOrEmpty(lblStatus.Text);

        // If the page was not changed, deselect all
        if (!mPageChanged)
        {
            hiddenSelected.Value = "";
        }

        // Always reset the new value from dialog
        hdnDialogSelect.Value = "";

        base.OnPreRender(e);
    }


    protected override void Render(HtmlTextWriter writer)
    {
        // Render starting tag of wrapping element
        switch (SelectionMode)
        {
            case SelectionModeEnum.SingleButton:
            case SelectionModeEnum.SingleDropDownList:
            case SelectionModeEnum.SingleTextBox:
                writer.Write("<span id=\"" + ClientID + "\">");
                break;

            case SelectionModeEnum.Multiple:
            case SelectionModeEnum.MultipleButton:
            case SelectionModeEnum.MultipleTextBox:
                writer.Write("<div id=\"" + ClientID + "\">");
                break;
        }
        // Render child controls
        RenderChildren(writer);

        // Render ending tag of wrapping element
        switch (SelectionMode)
        {
            case SelectionModeEnum.SingleButton:
            case SelectionModeEnum.SingleDropDownList:
            case SelectionModeEnum.SingleTextBox:
                writer.Write("</span>");
                break;

            case SelectionModeEnum.Multiple:
            case SelectionModeEnum.MultipleButton:
            case SelectionModeEnum.MultipleTextBox:
                writer.Write("</div>");
                break;
        }
    }


    /// <summary>
    /// Reloads all controls.
    /// </summary>
    /// <param name="forceReload">Indicates if data should be loaded from DB</param>
    public void Reload(bool forceReload)
    {
        if (!mLoaded || forceReload)
        {
            LoadObjects(forceReload);
            if (!StopProcessing)
            {
                SetupControls();
                RegisterScripts();
                ReloadData(forceReload);
            }

            mLoaded = true;
        }
    }


    /// <summary>
    /// Displays data according to selection mode.
    /// </summary>
    private void ReloadData(bool forceReload)
    {
        // Check that object type is not empty
        if (objectType != null)
        {
            // Display form control content according to selection mode
            switch (SelectionMode)
            {
                case SelectionModeEnum.SingleTextBox:
                    DisplayTextBox();
                    break;

                case SelectionModeEnum.SingleDropDownList:
                    DisplayDropDownList(forceReload);
                    break;

                case SelectionModeEnum.Multiple:
                    DisplayMultiple(forceReload);
                    break;

                case SelectionModeEnum.MultipleTextBox:
                    DisplayMultipleTextBox();
                    break;

                case SelectionModeEnum.SingleButton:
                case SelectionModeEnum.MultipleButton:
                    DisplayButton();
                    break;
            }
        }
    }


    /// <summary>
    /// Setups controls.
    /// </summary>
    private void SetupControls()
    {
        // Add resource strings
        btnClear.ResourceString = ResourcePrefix + ".clear|general.clear";
        btnSelect.ResourceString = ResourcePrefix + ".select|general.select";
        btnDialog.ResourceString = ResourcePrefix + ".select|general.select";
        lnkDialog.ResourceString = ResourcePrefix + ".select|general.select";

        // Add event handlers
        string selScript = "US_SelectionDialog_" + ClientID + "(); return false;";

        switch (SelectionMode)
        {
            // Dropdownlist mode
            case SelectionModeEnum.SingleDropDownList:
                if (!String.IsNullOrEmpty(EditItemPageUrl))
                {
                    btnDropEdit.Visible = true;
                    btnDropEdit.ResourceString = ResourcePrefix + ".edit|general.edit";
                    btnDropEdit.OnClientClick = "US_EditItem_" + ClientID + "(document.getElementById('" + drpSingleSelect.ClientID + "').value); return false;";
                }
                break;

            // Multiple selection mode
            case SelectionModeEnum.Multiple:
                btnRemoveSelected.ResourceString = ResourcePrefix + ".removeselected|general.removeselected";
                btnRemoveAll.ResourceString = ResourcePrefix + ".removeall|general.removeall";

                btnAddItems.ResourceString = ResourcePrefix + ".additems|general.additems";

                string confirmation = RemoveConfirmation;
                if (confirmation == null)
                {
                    confirmation = ResHelper.GetString("general.confirmremove");
                }
                if (!String.IsNullOrEmpty(confirmation))
                {
                    btnRemoveSelected.OnClientClick = "return confirm(" + ScriptHelper.GetString(confirmation) + ")";
                }
                btnRemoveAll.OnClientClick = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("general.confirmremoveall")) + ")";

                btnAddItems.OnClientClick = selScript;
                break;

            // Button mode
            case SelectionModeEnum.SingleButton:
            case SelectionModeEnum.MultipleButton:
                // Setup button
                if (ButtonImage == null)
                {
                    btnDialog.OnClientClick = selScript;
                }
                else
                {
                    lnkDialog.Attributes.Add("onclick", selScript);
                    lnkDialog.NavigateUrl = "#";
                    if (ButtonImage != "")
                    {
                        imgDialog.ImageUrl = ButtonImage;
                        imgDialog.AlternateText = ResHelper.GetString(ResourcePrefix + ".select|general.select");
                        imgDialog.ToolTip = ResHelper.GetString(ResourcePrefix + ".select|general.select");
                    }
                }
                break;

            // Single textbox mode
            case SelectionModeEnum.SingleTextBox:
                {
                    // Select button
                    if (AllowEditTextBox)
                    {
                        btnSelect.OnClientClick = "US_SelectionDialog_" + ClientID + "('$|' + document.getElementById('" + txtSingleSelect.ClientID + "').value); return false;";
                    }
                    else
                    {
                        btnSelect.OnClientClick = selScript;
                    }

                    // Edit button
                    if (!String.IsNullOrEmpty(EditItemPageUrl))
                    {
                        btnEdit.Visible = true;
                        btnEdit.ResourceString = ResourcePrefix + ".edit|general.edit";
                        if (AllowEditTextBox)
                        {
                            btnEdit.OnClientClick = "US_EditItem_" + ClientID + "(document.getElementById('" + txtSingleSelect.ClientID + "').value); return false;";
                        }
                        else
                        {
                            btnEdit.OnClientClick = "US_EditItem_" + ClientID + "(document.getElementById('" + hiddenField.ClientID + "').value); return false;";
                        }
                    }

                    // New button
                    if (!String.IsNullOrEmpty(NewItemPageUrl))
                    {
                        btnNew.Visible = true;
                        btnNew.ResourceString = ResourcePrefix + ".new|general.new";
                        btnNew.OnClientClick = "US_NewItem_" + ClientID + "(); return false;";
                    }
                }
                break;

            // Multiple textbox
            case SelectionModeEnum.MultipleTextBox:
                // Select button
                if (AllowEditTextBox)
                {
                    btnSelect.OnClientClick = "US_SelectionDialog_" + ClientID + "('$|' + document.getElementById('" + txtSingleSelect.ClientID + "').value); return false;";
                }
                else
                {
                    btnSelect.OnClientClick = selScript;
                }
                break;

            default:
                btnSelect.OnClientClick = selScript;
                break;
        }
    }


    /// <summary>
    /// Loads objects from DB and stores it to variables.
    /// </summary>
    private void LoadObjects(bool forceReload)
    {
        // Make sure that objects specified by given object type exist
        if (!String.IsNullOrEmpty(ObjectType))
        {
            objectType = CMSObjectHelper.GetObject(ObjectType);
            if (objectType != null)
            {
                // Set return column name
                if (String.IsNullOrEmpty(ReturnColumnName))
                {
                    ReturnColumnName = objectType.IDColumn;
                }

                string javaScript = null;

                // Open selection dialog depending if UniSelector is on live site
                string url = null;
                if (IsLiveSite)
                {
                    url = "~/CMSAdminControls/UI/UniSelector/LiveSelectionDialog.aspx";
                }
                else
                {
                    url = "~/CMSAdminControls/UI/UniSelector/SelectionDialog.aspx";
                }

                url += "?SelectionMode=" + SelectionMode + "&hidElem=" + hiddenField.ClientID + "&lblElem=" + lblStatus.ClientID + "&params=" + Identificator + "&clientId=" + ClientID;

                // Create modal dialogs and datasets according to selection mode
                switch (SelectionMode)
                {
                    // Single text box seleciton mode
                    case SelectionModeEnum.SingleTextBox:
                        url += "&txtElem=" + txtSingleSelect.ClientID;
                        break;

                    // Single drop down list selection mode
                    case SelectionModeEnum.SingleDropDownList:
                        if (drpSingleSelect.Items.Count == 0)
                        {
                            forceReload = true;
                        }
                        result = GetResultSet(null, MaxDisplayedItems + 1, 0, forceReload);

                        url += "&selectElem=" + hdnDialogSelect.ClientID;
                        break;

                    // Multiple selection mode
                    case SelectionModeEnum.Multiple: 
                        uniGrid.GridName = GridName;
                        uniGrid.LoadGridDefinition();
                        int currentOffset = (uniGrid.Pager.CurrentPage - 1) * uniGrid.Pager.CurrentPageSize;
                        int totalRecords = 0;
                        result = GetResultSet(null, 0, 0, forceReload, currentOffset, uniGrid.Pager.CurrentPageSize, ref totalRecords);
                        // If not first page and no data loaded load first page
                        if (DataHelper.DataSourceIsEmpty(result) && (currentOffset > 0))
                        {
                            // Set unigird page to 1 and reload data
                            uniGrid.Pager.UniPager.CurrentPage = 1;
                            result = GetResultSet(null, 0, 0, forceReload, 0, uniGrid.Pager.CurrentPageSize, ref totalRecords);
                        }

                        uniGrid.PagerForceNumberOfResults = totalRecords;
                        break;

                    // Multiple text box seleciton mode
                    case SelectionModeEnum.MultipleTextBox:
                        url += "&txtElem=" + txtSingleSelect.ClientID;
                        break;

                    // Button selection
                    case SelectionModeEnum.SingleButton:
                    case SelectionModeEnum.MultipleButton:
                        break;

                    default:
                        url = null;
                        result = null;
                        break;
                }

                // Selection dialog window
                if (url != null)
                {
                    javaScript =
                        "function US_SelectionDialog_" + ClientID + "(values) { " + Page.ClientScript.GetCallbackEventReference(this, "values", "US_SelectionDialogReady_" + ClientID, "'" + ResolveUrl(url) + "'") + "; } \n";
                }

                // Create selection changed function
                if (OnSelectionChangedAvailable())
                {
                    javaScript +=
                        "function US_SelectionChanged_" + ClientID + "() { " + Page.ClientScript.GetPostBackEventReference(this, "selectionchanged") + "; } \n";
                }

                // New item window
                if (!String.IsNullOrEmpty(NewItemPageUrl))
                {
                    string newUrl = UrlHelper.AddParameterToUrl(NewItemPageUrl, "selectorId", ClientID);
                    javaScript +=
                        "function US_NewItem_" + ClientID + "() { modalDialog('" + ResolveUrl(newUrl) + "', 'NewItem', 1000, 700); } \n";
                }

                // Edit item window
                if (!String.IsNullOrEmpty(EditItemPageUrl))
                {
                    string newUrl = UrlHelper.AddParameterToUrl(EditItemPageUrl, "selectorId", ClientID);
                    javaScript +=
                        "function US_EditItem_" + ClientID + "(selectedItem) { if (selectedItem == '') { alert('" + ResHelper.GetString(ResourcePrefix + ".pleaseselectitem|general.pleaseselectitem") + "'); return false; } var url = '" + ResolveUrl(newUrl) + "'; modalDialog(url.replace(/##ITEMID##/i, selectedItem), 'EditItem', 1000, 700); } \n";
                }

                javaScript +=
                    "function US_ReloadPage_" + ClientID + "(){ " + Page.ClientScript.GetPostBackEventReference(this, "reload") + "; return false; } \n" +
                    "function US_SelectItems_" + ClientID + "(items){ document.getElementById('" + hiddenField.ClientID + "').value = items; " + Page.ClientScript.GetPostBackEventReference(this, "selectitems") + "; return false; } \n";

                // Register JavaScript
                ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SelectionDialogCode" + ClientID, ScriptHelper.GetScript(javaScript));
                ScriptHelper.RegisterDialogScript(Page);
            }
            else
            {
                lblStatus.Text = "[UniSelector]: Object type '" + ObjectType + "' not found.";
                StopProcessing = true;
            }
        }
    }


    /// <summary>
    /// Sets the dialog parameters to the context
    /// </summary>
    protected void SetDialogParameters(string values)
    {
        parameters["SelectionMode"] = SelectionMode;
        parameters["ResourcePrefix"] = ResourcePrefix;
        parameters["ObjectType"] = ObjectType;
        parameters["ReturnColumnName"] = ReturnColumnName;
        parameters["IconPath"] = IconPath;
        parameters["AllowEmpty"] = AllowEmpty;
        parameters["AllowAll"] = AllowAll;
        parameters["NoneRecordValue"] = NoneRecordValue;
        parameters["AllRecordValue"] = AllRecordValue;
        parameters["FilterControl"] = FilterControl;
        parameters["UseDefaultNameFilter"] = UseDefaultNameFilter;
        parameters["WhereCondition"] = WhereCondition;
        parameters["OrderBy"] = OrderBy;
        parameters["ItemsPerPage"] = ItemsPerPage;
        parameters["EmptyReplacement"] = EmptyReplacement;
        parameters["Values"] = ";" + values + ";";
        parameters["DisplayNameFormat"] = DisplayNameFormat;
        parameters["DialogGridName"] = DialogGridName;
        parameters["AdditionalColumns"] = AdditionalColumns;
        parameters["CallbackMethod"] = CallbackMethod;
        parameters["AllowEditTextBox"] = AllowEditTextBox;
        parameters["FireOnChanged"] = OnSelectionChangedAvailable();

        WindowHelper.Add(Identificator, parameters);
    }


    /// <summary>
    /// Displays single selection textbox.
    /// </summary>
    private void DisplayTextBox()
    {
        plcTextBoxSelect.Visible = true;

        if (!AllowEmpty)
        {
            btnClear.Visible = false;
        }

        if (AllowEditTextBox)
        {
            // Load the selected value
            txtSingleSelect.ReadOnly = false;
        }
        else
        {
            // Get the item
            string id = ValidationHelper.GetString(Value, null);
            if (!String.IsNullOrEmpty(id))
            {
                // Load textbox with data
                DataSet item = GetResultSet(id, 1, 0, true);
                if (!DataHelper.DataSourceIsEmpty(item))
                {
                    txtSingleSelect.Text = GetItemName(item.Tables[0].Rows[0]);
                }
            }
            else
            {
                txtSingleSelect.Text = "";
            }
        }
    }


    /// <summary>
    /// Displays single selection drop down list.
    /// </summary>
    private void DisplayDropDownList(bool forceReload)
    {
        hiddenField.Visible = false;

        plcDropdownSelect.Visible = true;

        object selectedValue = this.Value;

        if (!IsPostBack || forceReload || (drpSingleSelect.Items.Count == 0) || !String.IsNullOrEmpty(this.EnabledColumnName))
        {
            // Prepare controls and variables
            drpSingleSelect.Items.Clear();

            bool hasData = !DataHelper.DataSourceIsEmpty(result);

            // Load data to drop-down list
            if (hasData && (objectType != null))
            {
                drpSingleSelect.Items.Clear();

                // Populate the dropdownlist
                foreach (DataRow dr in result.Tables[0].Rows)
                {
                    drpSingleSelect.Items.Add(NewListItem(dr));
                }

                // Check if all items were displayed or if '(more items)' item should be added
                if (result.Tables[0].Rows.Count > MaxDisplayedItems)
                {
                    drpSingleSelect.Items.RemoveAt(MaxDisplayedItems);
                    drpSingleSelect.Items.Add(new ListItem(ResHelper.GetString(ResourcePrefix + ".moreitems|general.moreitems"), "-2"));
                }
            }

            // Display special items
            if ((SpecialFields != null) && (SpecialFields.Length > 0))
            {
                for (int i = 0; i < SpecialFields.Length / 2; i++)
                {
                    string text = SpecialFields[i, 0];
                    string value = SpecialFields[i, 1];

                    // Insert only items that are not empty
                    if (!String.IsNullOrEmpty(text))
                    {
                        drpSingleSelect.Items.Insert(i, new ListItem(text, value));
                    }
                }
            }

            // Display '(none)' item
            if (AllowEmpty)
            {
                // Get item name
                string name = ResHelper.GetString(ResourcePrefix + ".empty|general.empty");

                drpSingleSelect.Items.Insert(0, new ListItem(name, this.NoneRecordValue));
            }

            // Display '(all)' item
            if (AllowAll)
            {
                // Get item name
                string name = ResHelper.GetString(ResourcePrefix + ".all|general.all");

                drpSingleSelect.Items.Insert(0, new ListItem(name, this.AllRecordValue));
            }

            // Load selected value to drop-down list
            if (!String.IsNullOrEmpty((string)selectedValue))
            {
                // Pre-select item from Value field
                string id = ValidationHelper.GetString(selectedValue, null);
                ListItem selectedItem = ControlsHelper.FindItemByValue(drpSingleSelect, id, false);

                // Select item which is already loaded in drop-down list
                if (selectedItem != null)
                {
                    drpSingleSelect.SelectedValue = selectedItem.Value;
                }
                // Select item which is not in drop-down list
                else
                {
                    // Find item by ID
                    DataSet item = GetResultSet(id, 1, 0, true);

                    if (!DataHelper.DataSourceIsEmpty(item))
                    {
                        ListItem newItem = NewListItem(item.Tables[0].Rows[0]);

                        // Add selected item to drop down list
                        if (!drpSingleSelect.Items.Contains(newItem))
                        {
                            drpSingleSelect.Items.Add(newItem);
                        }
                        drpSingleSelect.SelectedValue = newItem.Value;
                    }
                    else
                    {
                        try
                        {
                            drpSingleSelect.SelectedValue = id;
                        }
                        catch { }
                    }
                }
            }

            // New item link
            if (!String.IsNullOrEmpty(NewItemPageUrl))
            {
                drpSingleSelect.Items.Add(new ListItem(ResHelper.GetString(ResourcePrefix + ".newitem|general.newitem"), "-3"));
            }

            HasData = hasData;
        }

        // Add open modal window JavaScript event
        drpSingleSelect.Attributes.Add("onchange", "if (!US_ItemChanged(this, '" + ClientID + "')) return false;");

        ScriptHelper.RegisterStartupScript(this, typeof(string), "UniSelector_" + ClientID, ScriptHelper.GetScript("US_InitDropDown(document.getElementById('" + drpSingleSelect.ClientID + "'))"));

        // Enable / disable the edit button
        switch (drpSingleSelect.SelectedValue)
        {
            case "":
            case "0":
            case "-1":
            case "-2":
            case "-3":
                btnDropEdit.Enabled = false;
                break;

            default:
                btnDropEdit.Enabled = true;
                break;
        }
    }


    /// <summary>
    /// Creates a new list item based on the given DataRow
    /// </summary>
    private ListItem NewListItem(DataRow dr)
    {
        string itemname = GetItemName(dr);
        // Create new item
        ListItem newItem = new ListItem(itemname, dr[ReturnColumnName].ToString());

        // Set disabled if disabled
        if (this.EnabledColumnName != null)
        {
            bool isEnabled = ValidationHelper.GetBoolean(dr[this.EnabledColumnName], false);
            if (isEnabled)
            {
                newItem.Attributes.Add("class", "DropDownItemEnabled");
            }
            else
            {
                newItem.Attributes.Add("class", "DropDownItemDisabled");
            }
        }

        return newItem;
    }


    /// <summary>
    /// Displays mulitple selection grid.
    /// </summary>
    private void DisplayMultiple(bool forceReload)
    {
        pnlGrid.Visible = true;

        uniGrid.GridName = GridName;
        uniGrid.LoadGridDefinition();

        bool hasData = !DataHelper.DataSourceIsEmpty(result);

        // Load data to unigrid
        if (hasData || forceReload)
        {
            uniGrid.DataSource = result;
            if (!RequestHelper.IsPostBack())
            {
                uniGrid.Pager.DefaultPageSize = ItemsPerPage;
            }
            uniGrid.ReloadData();
        }

        // Display "No data" message
        if (!hasData)
        {
            lblStatus.Text = ZeroRowsText ?? ResHelper.GetString(ResourcePrefix + ".nodata|general.nodata");
        }
        else
        {
            lblStatus.Text = "";
        }
        //this.btnRemoveSelected.Enabled = hasData;
        //this.btnRemoveAll.Enabled = hasData;
        btnRemoveSelected.Visible = hasData;
        //this.btnRemoveAll.Visible = hasData;

        HasData = hasData;
    }


    /// <summary>
    /// Displays multiple selection textbox
    /// </summary>
    private void DisplayMultipleTextBox()
    {
        plcTextBoxSelect.Visible = true;

        if (!AllowEmpty)
        {
            btnClear.Visible = false;
        }

        // Setup the textbox
        if (AllowEditTextBox)
        {
            txtSingleSelect.ReadOnly = false;
        }
        else
        {
            txtSingleSelect.Text = ValidationHelper.GetString(Value, "").Trim(new char[] { ';', ' ' });
        }
    }


    /// <summary>
    /// Displays selection button
    /// </summary>
    private void DisplayButton()
    {
        if (ButtonImage == null)
        {
            // Standard button
            plcButtonSelect.Visible = true;
        }
        else
        {
            // Link button
            plcImageSelect.Visible = true;
        }
    }


    /// <summary>
    /// Returns data set depending on specified properties.
    /// </summary>
    private DataSet GetResultSet(string id, int topN, int pageIndex, bool forceReload)
    {
        int totalRecords = 0;
        return GetResultSet(id, topN, pageIndex, forceReload, 0, 0, ref totalRecords);
    }


    /// <summary>
    /// Returns data set depending on specified properties.
    /// </summary>
    private DataSet GetResultSet(string id, int topN, int pageIndex, bool forceReload, int offset, int maxRecords, ref int totalRecords)
    {
        DataSet ds = null;

        // Init columns
        string columns = null;
        if (DisplayNameFormat == USER_DISPLAY_FORMAT)
        {
            // Ensure columns which are needed for USER_DISPLAY_FORMAT
            columns = "UserName;FullName;";
        }
        else if (DisplayNameFormat != null)
        {
            columns = DataHelper.GetNotEmpty(TextHelper.GetMacros(DisplayNameFormat), objectType.DisplayNameColumn).Replace(";", ", ");
        }
        else
        {
            columns = objectType.DisplayNameColumn;
        }

        // Add return column name
        columns = SqlHelperClass.MergeColumns(columns, ReturnColumnName);

        // Add additional columns
        columns = SqlHelperClass.MergeColumns(columns, AdditionalColumns);

        // Add enabled column name
        columns = SqlHelperClass.MergeColumns(columns, EnabledColumnName);

        // Return result set for single selectors
        string itemsWhere = null;

        if (SelectionMode != SelectionModeEnum.Multiple)
        {
            // Build where condition
            if (!String.IsNullOrEmpty(id))
            {
                itemsWhere = ReturnColumnName + " = '" + id.Replace("'", "''") + "'";
            }
        }
        // Return result set for multiple selection
        else
        {
            if (pageIndex == 0)
            {
                pageIndex = uniGrid.Pager.CurrentPage - 1;
            }
            topN = ItemsPerPage * (pageIndex + 1 + uniGrid.Pager.CurrentPagesGroupSize);

            // Get where condition for selected items
            string[] items = hiddenField.Value.Split(ValuesSeparator.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            bool isString = !objectType.IDColumn.Equals(ReturnColumnName, StringComparison.InvariantCultureIgnoreCase);

            itemsWhere = SqlHelperClass.GetWhereCondition(ReturnColumnName, items, isString, isString);
        }

        // Modify WHERE condition
        string where = SqlHelperClass.AddWhereCondition(WhereCondition, itemsWhere);

        // Order by
        string orderBy = OrderBy;
        if (String.IsNullOrEmpty(orderBy))
        {
            orderBy = objectType.DisplayNameColumn;
        }

        // Get the result set
        ds = objectType.GetModifiedFrom(DateTimeHelper.ZERO_TIME, columns, where, orderBy, topN, offset, maxRecords, ref totalRecords, true, null);

        return ds;
    }


    /// <summary>
    /// Registers scripts.
    /// </summary>
    private void RegisterScripts()
    {
        string script =
            @"function US_InitDropDown(control) {
                if (control != null)
                {   
                    control.originalValue = control.value;
                }
              }

              function US_ItemChanged(control, clientId, restoredValue) {
                if (control != null) {
                    var editButton = document.getElementById(clientId + '_btnDropEdit');
                    if (editButton != null)
                    {
                        setTimeout('BTN_Enable(""' + clientId + '_btnDropEdit"");', 0);
                    }

                    var disableButton = false;
                    var result = true;
                    var restoreSelection = false;

                    if ((control.value == '-1') || (control.value == '0')) {
                        disableButton = true;
                    }
                    else if (control.value == '-2') {
                        setTimeout('US_SelectionDialog_' + clientId + '();', 1);
                        disableButton = true;
                        restoreSelection = true;
                        result = false;
                    }
                    else if (control.value == '-3') {
                        setTimeout('US_NewItem_' + clientId + '();', 1);
                        disableButton = true;
                        restoreSelection = true;
                        result = false;
                    }

                    if (restoreSelection)
                    {
                        if (control.originalValue != null)
                        {
                            control.value = control.originalValue;
                        }
                    }
                    else
                    {
                        control.originalValue = control.value;
                    }
                    if (disableButton && (editButton != null))
                    {
                        setTimeout('BTN_Disable(""' + clientId + '_btnDropEdit"");', 0);
                    }
                    return result;
                }
                return true;
            }

            function US_SetItems(items, names, hiddenFieldID, txtClientID, lblClientID, hidValue, fireOnChanged) {
                if (txtClientID != '') {
                    if (document.getElementById(txtClientID) != null) {
                        document.getElementById(txtClientID).value = decodeURIComponent(names);
                    }
                }
                if (lblClientID != '') {
                    if (document.getElementById(hiddenFieldID) != null) {
                        document.getElementById(hiddenFieldID).value = items;
                    }
                    if (document.getElementById(txtClientID) != null) {
                        document.getElementById(txtClientID).value = decodeURIComponent(names);
                    }
                    if (document.getElementById(lblClientID) != null) {
                        document.getElementById(lblClientID).innerHTML = '';
                    }
                    if (document.getElementById(hidValue) != null) {
                        document.getElementById(hidValue).value = items;
                    }
                }

                if (fireOnChanged) {
                    
                }

                return false;
            }

            function US_ProcessItem(clientId, valuesSeparator, chkbox, changeChecked) {   
                var itemsElem = document.getElementById(clientId + '_hiddenSelected');
                var items = itemsElem.value; 
                var item = chkbox.id.substr(36);
                if (changeChecked)
                {
                    chkbox.checked = !chkbox.checked;
                }
                if (chkbox.checked)
                {
                    if (items == '')
                    {
                        items = valuesSeparator + item + valuesSeparator;
                    }
                    else if (items.toLowerCase().indexOf(valuesSeparator + item.toLowerCase() + valuesSeparator) < 0)
                    {
                        items += item + valuesSeparator;
                    }
                }
                else
                {
                    var re = new RegExp(valuesSeparator + item + valuesSeparator, 'i');
                    items = items.replace(re, valuesSeparator);    
                }
                itemsElem.value = items;
            }
            
            function US_SelectAllItems(clientId, valuesSeparator, checkbox, checkboxClass)
            {
                var checkboxes = document.getElementsByTagName('input');
                for(var j = 0; j < checkboxes.length; j++)
                {
                    var chkbox = checkboxes[j];
                    if (chkbox.className == checkboxClass)
                    {
                        if(checkbox.checked) { chkbox.checked = true; }
                        else { chkbox.checked = false; }

                        US_ProcessItem(clientId, valuesSeparator, chkbox);
                    }
                }
            }";

        // General (shared) script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "UniSelector", ScriptHelper.GetScript(script));


        script = @"function US_SelectionDialogReady_" + ClientID + @"(rvalue, context) 
            { 
                modalDialog(context, '" + DialogWindowName + @"', " + DialogWindowWidth.ToString() + @", " + DialogWindowHeight.ToString() + @"); 
                return false;
            }";

        // Open dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "UniSelectorReady" + ClientID, ScriptHelper.GetScript(script));
    }

    #endregion


    #region "Event handlers"

    /// <summary>
    /// Drop-down list event handler.
    /// </summary>
    void drpSingleSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Only raise selected index changed when other than (more items...) is selected
        if (drpSingleSelect.SelectedValue != "-2")
        {
            RaiseSelectionChanged();
        }
    }


    /// <summary>
    /// Unigrid external databound handler
    /// </summary>
    object uniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {

        switch (sourceName.ToLower(CultureHelper.EnglishCulture))
        {
            case "yesno":
                return UniGridFunctions.ColoredSpanYesNo(parameter);

            case "select":
                {
                    // Get item ID
                    DataRowView drv = (parameter as DataRowView);
                    string itemID = drv[ReturnColumnName].ToString();

                    // Keep the check status if checked
                    bool isChecked = mPageChanged && (hiddenSelected.Value.IndexOf(ValuesSeparator + itemID + ValuesSeparator, StringComparison.InvariantCultureIgnoreCase) >= 0);

                    return "<input id=\"chk" + checkBoxClass + "_" + itemID + "\" type=\"checkbox\" onclick=\"US_ProcessItem('" + ClientID + "', '" + ValuesSeparator + "', this);\" class=\"chk" + checkBoxClass + "\"" + (isChecked ? "checked=\"checked\"" : "") + " />";
                }

            case "itemname":
                {
                    DataRowView drv = (parameter as DataRowView);

                    // Get item ID
                    string itemID = drv[ReturnColumnName].ToString();

                    // Get item name
                    string itemName = GetItemName(drv.Row);

                    return "<div class=\"SelectableItem\" onclick=\"US_ProcessItem('" + ClientID + "', '" + ValuesSeparator + "', document.getElementById('chk" + checkBoxClass + "_" + ScriptHelper.GetString(itemID).Trim('\'') + "'), true); return false;\">" + HTMLHelper.HTMLEncode(TextHelper.LimitLength(itemName, 100)) + "</div>";
                }
        }

        return null;
    }


    /// <summary>
    /// Return item display name based on DisplayNameFormat
    /// </summary>
    /// <param name="dr">Source data row</param>    
    private string GetItemName(DataRow dr)
    {
        string itemName = null;

        // Special formatted user name
        if (DisplayNameFormat == USER_DISPLAY_FORMAT)
        {
            string userName = ValidationHelper.GetString(DataHelper.GetDataRowValue(dr, "UserName"), String.Empty);
            string fullName = ValidationHelper.GetString(DataHelper.GetDataRowValue(dr, "FullName"), String.Empty);

            itemName = Functions.GetFormattedUserName(userName, fullName, this.IsLiveSite);
        }
        else if (DisplayNameFormat == null)
        {
            itemName = ValidationHelper.GetString(DataHelper.GetDataRowValue(dr, objectType.DisplayNameColumn), String.Empty);
        }
        else
        {
            itemName = th.MergeText(DisplayNameFormat, dr);
        }

        // Add items to unigrid
        if (String.IsNullOrEmpty(itemName))
        {
            itemName = EmptyReplacement;
        }

        return itemName;
    }


    /// <summary>
    /// Unigrid page index changed handler.
    /// </summary>
    protected void uniGrid_OnPageChanged(object sender, EventArgs e)
    {
        int currentOffset = (uniGrid.Pager.CurrentPage - 1) * (uniGrid.Pager.CurrentPageSize);
        int totalRecord = 0;
        result = GetResultSet(null, 0, 0, true, currentOffset, uniGrid.Pager.CurrentPageSize, ref totalRecord);

        mPageChanged = true;

        // Load data to unigrid
        if (!DataHelper.DataSourceIsEmpty(result))
        {
            uniGrid.PagerForceNumberOfResults = totalRecord;
            uniGrid.DataSource = result;
            uniGrid.ReloadData();
        }
    }


    /// <summary>
    /// Button clear click.
    /// </summary>
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Value = null;
        Reload(true);
    }


    /// <summary>
    /// Button "Remove selected items" click handler.
    /// </summary>
    protected void btnRemoveSelected_Click(object sender, EventArgs e)
    {
        // Unselect selected items
        if (!String.IsNullOrEmpty(hiddenSelected.Value))
        {
            hiddenField.Value = DataHelper.GetNewItemsInList(hiddenSelected.Value, hiddenField.Value, ValuesSeparator[0]);

            Reload(true);

            RaiseSelectionChanged();
        }
    }


    /// <summary>
    /// Button "Remove all items" click handler.
    /// </summary>
    protected void btnRemoveAll_Click(object sender, EventArgs e)
    {
        // Unselect selected items
        if (!String.IsNullOrEmpty(hiddenField.Value))
        {
            hiddenField.Value = "";

            Reload(true);

            RaiseSelectionChanged();
        }
    }


    /// <summary>
    /// Overriden set value to collect parameters
    /// </summary>
    /// <param name="propertyName">Property name</param>
    /// <param name="value">Value</param>
    public override void SetValue(string propertyName, object value)
    {
        base.SetValue(propertyName, value);

        // Set parameters for dialog
        parameters[propertyName] = value;
    }


    /// <summary>
    /// Overriden to get the parameters
    /// </summary>
    /// <param name="propertyName">Property name</param>
    public override object GetValue(string propertyName)
    {
        if (parameters.Contains(propertyName))
        {
            return parameters[propertyName];
        }

        return base.GetValue(propertyName);
    }

    #endregion


    #region "IPostBackEventHandler Members"

    /// <summary>
    /// Hanling of the postback event
    /// </summary>
    /// <param name="eventArgument">Event argument (selected value)</param>
    public void RaisePostBackEvent(string eventArgument)
    {
        switch (eventArgument)
        {
            case "selectitems":
                // Raise items selected event
                RaiseOnItemsSelected();
                break;

            case "reload":
                // Reload the data
                Reload(true);

                RaiseSelectionChanged();
                break;

            case "selectionchanged":
                // Raise selection changed event
                RaiseSelectionChanged();
                break;
        }
    }

    #endregion


    #region "ICallbackEventHandler Members"

    string ICallbackEventHandler.GetCallbackResult()
    {
        // Prepare the parameters for dialog
        SetDialogParameters((string)Value);

        return "";
    }


    void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
    {
        // Adopt new value from callback
        if ((eventArgument != null) && eventArgument.StartsWith("$|"))
        {
            Value = eventArgument.Substring(2);
        }
    }

    #endregion
}
