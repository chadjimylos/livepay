<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tree.aspx.cs" Inherits="CMSModules_ImportExport_SiteManager_NewSite_DefineSiteStructure_tree"
    EnableEventValidation="false" Theme="Default" %>

<%@ Register Src="~/CMSModules/Content/Controls/ContentTree.ascx" TagName="ContentTree"
    TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" enableviewstate="false">
    <title>Content - Tree</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>
</head>
<body class="TreeBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="pnlBody" CssClass="ContentTree">
        <asp:Panel runat="server" ID="pnlBorder" CssClass="TreeBorder">
            &nbsp;</asp:Panel>
        <cms:ContentTree ID="treeContent" runat="server" MaxTreeNodes="100000" AllowMarks="false" />
    </asp:Panel>
    <asp:HiddenField runat="server" ID="siteName" />

    <script type="text/javascript" src="tree.js"></script>

    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    <input type="hidden" id="hdnAction" name="hdnAction" />
    <input type="hidden" id="hdnParam1" name="hdnParam1" />
    <input type="hidden" id="hdnParam2" name="hdnParam2" />
    </form>
</body>
</html>
