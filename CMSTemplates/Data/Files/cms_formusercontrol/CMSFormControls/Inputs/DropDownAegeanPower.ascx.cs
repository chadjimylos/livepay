using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;

public partial class CMSFormControls_DropDownAegeanPower : FormEngineUserControl
{

    protected void page_load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            drpJob.ClearSelection();
            drpJob.Items.Clear();
            drpJob.Items.Add(new ListItem(ResHelper.LocalizeString("{$=Διευθύνων Σύμβουλος|en-us=Consultant Manager$}"), "1"));
            drpJob.Items.Add(new ListItem(ResHelper.LocalizeString("{$=Γενικός Διευθυντής|en-us=General Manager$}"), "2"));
            drpJob.Items.Add(new ListItem(ResHelper.LocalizeString("{$=Διευθυντής Προμηθειών|en-us=Director of Purchasing$}"), "3"));
            drpJob.Items.Add(new ListItem(ResHelper.LocalizeString("{$=Διευθυντής Οικονομικών|en-us=Finance Director$}"), "4"));
            drpJob.Items.Add(new ListItem(ResHelper.LocalizeString("{$=Διευθυντής Διοικητικών Λειτουργιών|en-us=Director of Administrative Operations$}"), "5"));
            drpJob.Items.Add(new ListItem(ResHelper.LocalizeString("{$=Άλλη θέση|en-us=Other$}"), "0"));

        }
    }
    protected void Index_Changed(object sender, EventArgs e)
    {
        if (DataHelper.IsZero(drpJob.SelectedValue.ToString()))
        {
            this.txtNewJob.Visible = true;
            this.txtNewJob.Text = ResHelper.LocalizeString("{$=Παρακαλώ προσδιορίστε|en-us=Please specify$}");
        }
        else
        {
            this.txtNewJob.Visible = false;
        }
    }    
}
