using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;

public partial class CMSModules_Membership_FormControls_Users_MultipleCategoriesSelector : FormEngineUserControl
{
    public override object Value
    {
        get
        {
            // Return string of categories ID
            return this.categorySelector.GetSelectedCategories();
        }
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.Form.OnAfterDataLoad += new BasicForm.OnAfterDataLoadEventHandler(Form_OnAfterDataLoad);
        this.Form.OnAfterSave += new BasicForm.OnAfterSaveEventHandler(Form_OnAfterSave);
    }


    void Form_OnAfterSave()
    {
        this.categorySelector.Save();
    }


    void Form_OnAfterDataLoad()
    {
        // Set document ID
        int documentID = ValidationHelper.GetInteger(DataHelper.GetDataRowValue(this.Form.DataRow, "DocumentID"), 0);
        if (documentID > 0)
        {
            this.categorySelector.DocumentID = documentID;
        }
        // Set user ID
        if (CMSContext.CurrentUser != null)
        {
            this.categorySelector.UserID = CMSContext.CurrentUser.UserID;
        }
    }
}
