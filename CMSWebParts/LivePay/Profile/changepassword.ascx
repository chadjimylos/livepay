<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/LivePay/Profile/changepassword.ascx.cs"
    Inherits="CMSWebParts_LivePay_Profile_ChangePassword" %>
    <style>
        .change-password
        {
            padding-left:20px;
        }
        .change-password .row
        {
            padding-top:15px;
        }
        .change-password .row #result
        {
            float:left;
            padding-left:10px;
            margin-top:5px;
            margin-left:5px;
            line-height:20px;
            width: 164px;
        }        
        .change-password .label
        {
            float:left;
            color:#094595;
            font-size:12px;
            font-weight:bold;
            width:111px;
            padding-right:10px;
            text-align:right;
        }      
        .change-password .textbox
        {
            float:left;
            margin-top:4px;
        }      
        .change-password .textbox input
        {
            border:1px solid #3b6db4;
        }          
        .change-password-bottom
        {
            width:262px;
            text-align:right;
            padding-top:10px;            
        }     
        .change-password-bottom input
        {
            background:transparent url(/App_Themes/LivePay/ChangeUserDetails/bgButton.gif) no-repeat;
            width:121px;
            height:23px;
            border:none;
            color:#fff;
            font-weight:bold;
            cursor:pointer;            
        }
        .change-password .tooshort
        {
            background-color:Gray;
            background:gray url(/App_Themes/LivePay/ChangeUserDetails/bgValidPassword.gif) no-repeat 5px 10px;
            padding-left:10px;
        }
        .change-password .bad
        {
            background-color:Yellow;
        }
        .change-password .good
        {
            background-color:Lime;
        }
        .change-password .strong
        {
            background-color:Black;
        }
        .ErrorLabel{color:Red;padding-left:30px;font-weight:bold}
    </style>
    <script type="text/javascript">
        var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1
        var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;


        var RegFormCaptchaCloudMess = ''
        var RegFormFnameCloudMess = ''
        var RegFormLnameCloudMess = ''
        var RegFormAnsCloudMess = ''
        var RegFormConfirmCloudMess = ''
        var RegFormPassCloudMess = ''
        var RegFormEmailCloudMess = ''
        var RegFormTermsCloudMess = ''


        jQuery(document).ready(function () {
            var idPassword = $('input[type=password][id$=txtNewPassword]:first')
            idPassword.keyup(function () { DivStrenght(passwordStrength(idPassword.val())) })

        });
        function DivStrenght(txt) {
            if (txt == 'Too short') {
                $('#result').css('background-color', '#ffcfcf');
                $('#result-icon').attr("class", "CUDStatus-icon-tooshort");
                $('#result-text').html('<%= ResHelper.LocalizeString("{$=���������� �������|en-us=Too short$}") %>');
            } else if (txt == 'Bad') {
                $('#result').css('background-color', '#ffd2b5');
                $('#result-icon').attr("class", "CUDStatus-icon-bad");
                $('#result-text').html('<%= ResHelper.LocalizeString("{$=��������|en-us=Bad$}") %>');
            } else if (txt == 'Good') {
                $('#result').css('background-color', '#fefdd2');
                $('#result-icon').attr("class", "CUDStatus-icon-good");
                $('#result-text').html('<%= ResHelper.LocalizeString("{$=�������|en-us=Good - ����$}") %>');

            } else if (txt == 'Strong') {
                $('#result').css('background-color', '#caedb8');
                $('#result-icon').attr("class", "CUDStatus-icon-strong");
                $('#result-text').html('<%= ResHelper.LocalizeString("{$=���� �������|en-us=Strong$}") %>');
            }
        }

        function ChanePassForm(oSrc, args) {
            var bIsValid = true
            var MyVal = args.Value
            if (MyVal.length == 0) {
                bIsValid = false
            }

            if (IsNumeric(MyVal) == false && oSrc.getAttribute('IsNumber') == 'yes') {
                bIsValid = false
            }

            var MaxLenght = MyVal.length
            if (oSrc.getAttribute('CheckLength') == 'yes' && MaxLenght != oSrc.getAttribute('LengthLimit')) {

                bIsValid = false
            }

            if (oSrc.getAttribute('IsPass') == 'yes') {
                bIsValid = IsPass(MyVal)
            }


            if (oSrc.getAttribute('IsConfirm') == 'yes') {
                var PassValue = document.getElementById('<%=txtNewPassword.ClientID %>').value
                if (PassValue != MyVal) {
                    bIsValid = false
                }
            }


            if (oSrc.getAttribute('IsPass') == 'yes') {
                if (bIsValid == false) {
                    RegFormCloud('#PassWordCloud', RegFormPassCloudMess, true, '')
                } else {
                    RegFormCloud('#PassWordCloud', '', false, '')
                }
            }

            if (oSrc.getAttribute('IsConfirm') == 'yes') {
                if (bIsValid == false) {
                    RegFormCloud('#ConfirmCloud', RegFormConfirmCloudMess, true, '')
                } else {
                    RegFormCloud('#ConfirmCloud', '', false, '')
                }
            }
            args.IsValid = bIsValid;
        }


        function IsPass(sPass) {
            if (sPass.length > 6) {
                var iSymbol = 0
                var iNumber = 0
                var iAlphas = 0
                var uAlphas = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                var lAlphas = "abcdefghijklmnopqrstuvwxyz";
                var Symbols = '`~!@#$%^&*?�()-_=+{}[]\|/,.<>;:'
                var Nums = "0123456789"
                for (i = 0; i < sPass.length; i++) {
                    Char = sPass.charAt(i);
                    if (Symbols.indexOf(Char) != -1) {
                        iSymbol++;
                    }
                    else if (Nums.indexOf(Char) != -1) {
                        iNumber++;
                    }
                    else if (lAlphas.indexOf(Char) != -1 || uAlphas.indexOf(Char) != -1) {
                        iAlphas++;
                    }
                }

                if (iAlphas >= 1 && iNumber >= 1 && iSymbol >= 1) {
                    return true
                } else {
                    return false
                }

            } else {
                return false
            }
        }

        function IsNumeric(sText) {
            var ValidChars = "0123456789";
            var IsNumber = true;
            var Char;
            for (i = 0; i < sText.length && IsNumber == true; i++) {
                Char = sText.charAt(i);
                if (ValidChars.indexOf(Char) == -1) {
                    IsNumber = false;
                }
            }
            return IsNumber;
        }

        function RegFormCloud(CloudID, CloudMess, show, YposCustom) {
            var YPos = -25;
            if (is_chrome) {
                $(CloudID).css('position', 'absolute')
                YPos = -27
                if (YposCustom != '' && YposCustom.length > 0) {
                    YPos = YposCustom;
                }
            }

            if (is_firefox) {
                YPos = -10
            }
            
            $(CloudID).poshytip('hide');
            
            $(CloudID).poshytip({
                className: 'tip-livepay',
                content: CloudMess,
                showOn: 'none',
                alignTo: 'target',
                alignX: 'right',
                offsetX: 165,
                offsetY: YPos, hideAniDuration: false
            });

            if (show) {
                $(CloudID).poshytip('show');
            }
        }
    </script>

<asp:Panel ID="pnlWebPart" runat="server" DefaultButton="btnOk">
            <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false" Visible="false" />
            <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false" Visible="false" />
                 <div class="change-password">
                    <div class="row">
                        <div class="label"><asp:Label ID="lblOldPassword" AssociatedControlID="txtOldPassword" runat="server" /></div>
                        <div class="textbox"><asp:TextBox style="" ID="txtOldPassword" runat="server" TextMode="Password" /></div>
                        <div class="Clear"></div>
                    </div>
                    <div class="row">
                        <div class="label"><asp:Label ID="lblNewPassword" AssociatedControlID="txtNewPassword" runat="server" /></div>
                        <div class="textbox"><a id="PassWordCloud" href="#"></a><asp:TextBox style="" ID="txtNewPassword" runat="server" TextMode="Password" /><font  style="color:#094595;font-weight:bold">*</font></div>
                        <div id="result"><div id="result-icon" class="CUDStatus icon-tooshort">&nbsp;</div> <span id="result-text"></span></div>
                        <div class="Clear"></div>
                         <div class="HideIt">
                            <asp:CustomValidator ID="UnRegUserPassword" ClientValidationFunction="ChanePassForm" ValidateEmptyText="true" runat="server" IsPass="yes" ControlToValidate="txtNewPassword" text="*" ValidationGroup="ChangePass"/>
                         </div>
                    </div> 
                    <div class="row" style="padding-top:7px">
                        <div class="label"><asp:Label ID="lblConfirmPassword" AssociatedControlID="txtConfirmPassword" runat="server" /></div>
                        <div class="textbox"><a id="ConfirmCloud" href="#"></a><asp:TextBox style="" ID="txtConfirmPassword" runat="server" TextMode="Password" /><font  style="color:#094595;font-weight:bold">*</font></div>
                        <div class="Clear"></div>
                        <div class="HideIt">
                    <asp:CustomValidator ID="UnRegUserConfirmPassword" ClientValidationFunction="ChanePassForm" ValidateEmptyText="true" runat="server" IsConfirm="yes" ControlToValidate="txtConfirmPassword" text="*" ValidationGroup="ChangePass"/>
                </div>
                    </div>
                </div>
                 <div class="change-password-bottom" ><cms:CMSButton style="font-family:Tahoma;font-size:11px" ValidationGroup="ChangePass" ID="btnOk" runat="server" OnClick="btnOk_Click" /></div>
                 <asp:Literal ID="lit_JsScript" runat="server" ></asp:Literal>
</asp:Panel>

<script language="javascript" type="text/javascript">

//    jQuery(document).ready(function () {
//        var AllSelect = document.getElementsByTagName('input');

//        for (i = 0; i < AllSelect.length; i++) {
//            if (AllSelect[i].id.indexOf('_UserPhone') != -1) {
//                jQuery(AllSelect[i]).removeAttr('maxlength')
//                jQuery(AllSelect[i]).attr('maxlength', '10')
//             
//                
//            }
//        }

//       
//    }
//    )
    
</script>