using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.FormControls;

public partial class CMSFormControls_Classes_SelectTransformation : FormEngineUserControl
{
    #region "Variables"

    private bool mDisplayClearButton = true;    

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Returns ClientID of the textbox with transformation.
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.uniSelector.TextBoxSelect.ClientID;
        }
    }


    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.uniSelector.Value;
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }
            this.uniSelector.Value = value;            
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to display Clear button.
    /// </summary>
    public bool DisplayClearButton
    {
        get
        {
            return this.mDisplayClearButton;
        }
        set
        {
            this.mDisplayClearButton = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.AllowEmpty = value;
            }
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.ButtonClear.Visible = false;        
        this.uniSelector.AllowEmpty = this.DisplayClearButton;
        this.uniSelector.SetValue("FilterMode", "transformation");

        // Check if user can edit the transformation
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        bool deskAuthorized = currentUser.IsAuthorizedPerUIElement("CMS.Desk", "Content");
        bool contentAuthorized = currentUser.IsAuthorizedPerUIElement("CMS.Content", new string[] { "Design", "Design.WebPartProperties", "WebPartProperties.EditTransformations" }, CMSContext.CurrentSiteName);

        if (deskAuthorized && contentAuthorized)
        {
            this.uniSelector.EditItemPageUrl = "~/CMSSiteManager/Development/DocumentTypes/DocumentType_Edit_Transformation_Edit.aspx?name=##ITEMID##&editonlycode=true";
        }
    }


    /// <summary>
    /// Return true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        // If macro or special value, do not validate
        string value = this.uniSelector.TextBoxSelect.Text.Trim();
        if (!ContextResolver.ContainsMacro(value) && (value != ""))
        {
            // Check if culture exists
            TransformationInfo ti = TransformationInfoProvider.GetTransformation(value);
            if (ti == null)
            {
                this.ValidationError = ResHelper.GetString("formcontrols_selecttransformation.notexist").Replace("%%code%%", value);
                return false;
            }
            else
            {
                return true;
            }
        }
        return true;
    }
}
