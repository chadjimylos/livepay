CREATE TABLE [Events_Attendee] (
		[AttendeeID]               int NOT NULL IDENTITY(1, 1),
		[AttendeeEmail]            nvarchar(250) NOT NULL,
		[AttendeeFirstName]        nvarchar(100) NULL,
		[AttendeeLastName]         nvarchar(100) NULL,
		[AttendeePhone]            nvarchar(50) NULL,
		[AttendeeEventNodeID]      int NOT NULL,
		[AttendeeGUID]             uniqueidentifier NOT NULL,
		[AttendeeLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Events_Attendee]
	ADD
	CONSTRAINT [PK_Events_Attendee]
	PRIMARY KEY
	NONCLUSTERED
	([AttendeeID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Events_Attendee]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Events_Attendee_AttendeeEventNodeID_CMS_Tree]') IS NULL
BEGIN
		ALTER TABLE [Events_Attendee]
			ADD CONSTRAINT [FK_Events_Attendee_AttendeeEventNodeID_CMS_Tree]
			FOREIGN KEY ([AttendeeEventNodeID]) REFERENCES [CMS_Tree] ([NodeID])
END
CREATE CLUSTERED INDEX [IX_Events_Attendee_AttendeeEmail_AttendeeFirstName_AttendeeLastName]
	ON [Events_Attendee] ([AttendeeEmail], [AttendeeFirstName], [AttendeeLastName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Events_Attendee_AttendeeEventNodeID]
	ON [Events_Attendee] ([AttendeeEventNodeID])
	ON [PRIMARY]
