<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebPartDocumentationPage.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_WebParts_WebPartDocumentationPage" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master"
    Title="Web part - Documentation" Theme="Default" %>

<asp:Content ID="cntContent" ContentPlaceHolderID="plcContent" runat="Server">
    <div class="PageContent">
        <!-- Teaser + description -->
        <table class="DocumentationWebPartsDescription">
            <tr>
                <td style="width: 50%; border-right: solid 1px #DDDDDD; vertical-align: middle; margin-left: auto;
                    margin-right: auto; text-align: center;">
                    <asp:Image runat="server" ID="imgTeaser" />
                </td>
                <td style="width: 50%; vertical-align: top;">
                    <asp:Literal runat="server" ID="ltrDescription" />
                </td>
            </tr>
        </table>
        <!-- web part properties -->
        <div class="DocumentationWebPartsProperties">
            <asp:Literal runat="server" ID="ltrProperties" />
        </div>
        <!-- Documentation -->
        <br />
        <br />
        <div style="border: solid 1px #dddddd;width:100%;">
            <asp:Literal runat="server" ID="ltlContent" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntFooter" ContentPlaceHolderID="plcFooter" runat="server">
    <div class="FloatRight">
        <cms:LocalizedButton ID="btnClose" runat="server" CssClass="SubmitButton" EnableViewState="False"
            OnClientClick="window.close(); return false;" ResourceString="WebPartDocumentDialog.Close" />
    </div>
</asp:Content>
