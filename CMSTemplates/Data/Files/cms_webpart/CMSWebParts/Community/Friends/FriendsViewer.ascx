<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Community/Friends/FriendsViewer.ascx.cs"
    Inherits="CMSWebParts_Community_Friends_FriendsViewer" %>
<%@ Register TagPrefix="cms" Namespace="CMS.Community" Assembly="CMS.Community" %>
<cms:BasicRepeater ID="repFriends" runat="server" />
<cms:FriendsDataSource ID="srcFriends" runat="server" />
<div class="Pager">
    <cms:UniPager ID="pagerElem" runat="server" />
</div>
