using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_FormControls_PaymentSelector : CMS.FormControls.FormEngineUserControl
{
    private bool mAddNoneRecord = false;
    private int mShippingOptionId = 0;
    private StatusEnum mDisplayItems = StatusEnum.Both;


    #region "Public properties"

    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.PaymentID;
        }
        set
        {
            this.PaymentID = ValidationHelper.GetInteger(value, 0);
        }
    }


    /// <summary>
    /// Gets or sets the Payment ID.
    /// </summary>
    public int PaymentID
    {
        get
        {
            return ValidationHelper.GetInteger(uniSelector.Value, 0);
        }
        set
        {
            this.uniSelector.Value = value;
        }
    }


    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Returns ClientID of the dropdownlist.
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect.ClientID;
        }
    }


    /// <summary>
    /// Indicates which payment methods should be displayed (Enabled/Disabled/Both). Default values is 'Both'.
    /// </summary>
    public StatusEnum DisplayItems
    {
        get
        {
            return mDisplayItems;
        }
        set
        {
            mDisplayItems = value;
        }
    }


    /// <summary>
    /// Add none record to the dropdownlist
    /// </summary>
    public bool AddNoneRecord
    {
        get
        {
            return mAddNoneRecord;
        }
        set
        {
            mAddNoneRecord = value;
        }
    }


    /// <summary>
    /// Shipping option ID.
    /// </summary>
    public int ShippingOptionID
    {
        get
        {
            return mShippingOptionId;
        }
        set
        {
            mShippingOptionId = value;
        }
    }


    /// <summary>
    /// Returns inner DropDownList control.
    /// </summary>
    public DropDownList DropDownSingleSelect
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect;
        }
    }


    /// <summary>
    /// Returns inner UniSelector control.
    /// </summary>
    public UniSelector UniSelector
    {
        get
        {
            return this.uniSelector;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        this.uniSelector.EnabledColumnName = "PaymentOptionEnabled";
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.AllowEmpty = this.AddNoneRecord;

        string where = null;

        if (this.ShippingOptionID > 0)
        {
            where = "PaymentOptionID IN (SELECT PaymentOptionID FROM COM_PaymentShipping WHERE ShippingOptionID = " + this.ShippingOptionID + ") AND "
                + "PaymentOptionSiteID = (SELECT ShippingOptionSiteID FROM COM_ShippingOption WHERE ShippingOptionID = " + this.ShippingOptionID + ")";
        }
        else
        {
            where = "PaymentOptionSiteID = " + CMSContext.CurrentSiteID;
        }
        if (this.DisplayItems == StatusEnum.Disabled)
        {
            where += " AND PaymentOptionEnabled = 0";
        }
        else if (this.DisplayItems == StatusEnum.Enabled)
        {
            where += " AND PaymentOptionEnabled = 1";
        }

        this.uniSelector.WhereCondition = where;
    }
}
