<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_Edit_Documents.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Products_Product_Edit_Documents" Theme="Default"
    Title="Product Edit - Documents" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<%@ Register Src="~/CMSModules/Ecommerce/Controls/UI/ProductDocuments.ascx" TagName="ProductDocuments"
    TagPrefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:ProductDocuments ID="productDocuments" runat="server" />
</asp:Content>
