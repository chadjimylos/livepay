<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User_list.aspx.cs" Inherits="CMSSiteManager_Administration_Users_User_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Users - User List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<%@ Register Src="~/CMSSiteManager/Administration/Users/UserFilter.ascx" TagName="UserFilter"
    TagPrefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:UserFilter ID="userFilterElem" runat="server" />
    <br />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <cms:UniGrid ID="gridElem" runat="server" GridName="User_List.xml" OrderBy="UserName"
        Columns="UserID, UserName, FullName, Email, UserNickName, UserCreated, UserEnabled"
        IsLiveSite="false" />

    <div style="display:none;">
        <asp:Label runat="server" ID="lbl_selectedUser"/>
        <asp:Button runat="server" ID="btn_CheckCards"/>
    </div>

    <script type="text/javascript">
        function CheckUserResponse() {
            if (confirm('� ������� ���� ������������� ������. ����� �������� ��� ������ �� ����������?'))
            {
                document.getElementById("<%= btn_CheckCards.ClientID %>").click();
            }
            else
            {
                return false;
            }
        }
    </script>

</asp:Content>


