using System;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.FormControls;
using CMS.UIControls;
using CMS.LicenseProvider;

public partial class CMSSiteManager_Administration_Users_User_Edit : CMSUsersPage
{
    #region "Protected variables"

    protected int userId = 0;
    protected int siteId = 0;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        userId = QueryHelper.GetInteger("userid", 0);
        siteId = QueryHelper.GetInteger("siteid", 0);

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }

        string users = ResHelper.GetString("general.users");
        string currentUser = Functions.GetFormattedUserName(UserInfoProvider.GetUserNameById(userId));

        //initializes PageTitle
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = users;
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Administration/Users/User_List.aspx?siteid=" + GetSiteID(Request.QueryString["siteid"]);
        pageTitleTabs[0, 2] = "_parent";
        pageTitleTabs[1, 0] = currentUser;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        CurrentMaster.Title.Breadcrumbs = pageTitleTabs;

        CurrentMaster.Title.HelpTopicName = "general_tab8";
        CurrentMaster.Title.HelpName = "helpTopic";

        // Register script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ShowContent", ScriptHelper.GetScript("function ShowContent(contentLocation) { parent.frames['content'].location.href= contentLocation; }"));
    }

    #endregion


    #region "Protected methods"

    /// <summary>
    /// Initializes user edit menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string generalString = ResHelper.GetString("general.general");
        string passwordString = ResHelper.GetString("Administration-User_Edit.Password");
        string rolesString = ResHelper.GetString("general.roles");
        string sitesString = ResHelper.GetString("general.sites");
        string customFieldsString = ResHelper.GetString("Administration-User_Edit.CustomFields");
        string departmentsString = ResHelper.GetString("Administration-User_Edit.Departments");
        string notificationsString = ResHelper.GetString("Administration-User_Edit.Notifications");
        string categoriesString = ResHelper.GetString("Administration-User_Edit.Categories");
        string friendsString = ResHelper.GetString("friends.friends");
        string subscriptionsString = ResHelper.GetString("Administration-User_Edit.Subscriptions");
        string languagesString = ResHelper.GetString("general.languages");

        // Check custom fields of user
        DataForm df = new DataForm();
        df.ClassName = "cms.user";
        UserInfo ui = UserInfoProvider.GetUserInfo(userId);
        if (ui != null)
        {
            df.ItemID = ui.UserID;
        }

        int customFieldsTab = (df.BasicForm.FormInformation.GetFormElements(true, false, true).Count <= 0 ? 0 : 1);

        // Check custom fields of user settings if needed
        if (customFieldsTab == 0)
        {
            df = new DataForm();
            df.ClassName = "cms.usersettings";
            if ((ui != null) && (ui.UserSettings != null))
            {
                df.ItemID = ui.UserSettings.UserSettingsID;
            }

            customFieldsTab = (df.BasicForm.FormInformation.GetFormElements(true, false, true).Count <= 0 ? 0 : 1);
        }

        // Display notifications tab ?
        bool showNotificationsTab = LicenseHelper.IsFeautureAvailableInUI(FeatureEnum.Notifications, ModuleEntry.NOTIFICATIONS);
        int notificationsTab = Convert.ToInt32(showNotificationsTab);

        // Display languages tab ?
        bool showLanguagesTab = LicenseKeyInfoProvider.IsFeatureAvailable(FeatureEnum.Multilingual);
        int languagesTab = Convert.ToInt32(showLanguagesTab);

        // Is E-commerce on site? => show department tab
        bool ecommerceOnSite = false;
        if (CMSContext.CurrentSite != null)
        {
            // Check if E-commerce module is installed
            ecommerceOnSite = ModuleEntry.IsModuleLoaded(ModuleEntry.ECOMMERCE);

            if (ecommerceOnSite)
            {
                // Check if E-commerce is enabled for current site
                ResourceInfo ri = ResourceInfoProvider.GetResourceInfo("CMS.Ecommerce");
                if (ri != null)
                {
                    ecommerceOnSite = ResourceInfoProvider.IsResourceOnSite(ri.ResourceId, CMSContext.CurrentSite.SiteID);
                }
            }
        }
        int departmentTab = Convert.ToInt32(ecommerceOnSite);
        int generalTabsCount = (CMSContext.CurrentUser.IsGlobalAdministrator) ? 8 : 7;

        string[,] tabs = new string[generalTabsCount + departmentTab + customFieldsTab + notificationsTab + languagesTab + 1, 4];

        tabs[0, 0] = generalString;
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab8');";
        tabs[0, 2] = "User_Edit_General.aspx?userid=" + userId + "&siteid=" + siteId;
        tabs[1, 0] = passwordString;
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'password_tab');";
        tabs[1, 2] = "User_Edit_Password.aspx?userid=" + userId + "&siteid=" + siteId;
        tabs[2, 0] = ResHelper.GetString("user.edit.settings");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'usersettings_tab');";
        tabs[2, 2] = "User_Edit_Settings.aspx?userid=" + userId + "&siteid=" + siteId;

        int lastTabIndex = 2;

        if (customFieldsTab > 0)
        {
            lastTabIndex++;
            tabs[lastTabIndex, 0] = customFieldsString;
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'CustomFields_tab');";
            tabs[lastTabIndex, 2] = "User_Edit_CustomFields.aspx?userid=" + userId + "&siteid=" + siteId;
        }

        if (CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            int sitesCount = SiteInfoProvider.GetSitesCount();
            if (sitesCount > 0)
            {
                lastTabIndex++;
                tabs[lastTabIndex, 0] = sitesString;
                tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'sites_tab');";
                tabs[lastTabIndex, 2] = "User_Edit_Sites.aspx?userid=" + userId + "&siteid=" + siteId;
            }

            lastTabIndex++;
            tabs[lastTabIndex, 0] = rolesString;
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'roles_tab');";
            tabs[lastTabIndex, 2] = "User_Edit_Roles.aspx?userid=" + userId + "&siteid=" + siteId;
        }
        else
        {
            lastTabIndex++;
            tabs[lastTabIndex, 0] = rolesString;
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'roles_tab');";
            tabs[lastTabIndex, 2] = "User_Edit_Roles.aspx?userid=" + userId + "&siteid=" + siteId;
        }

        // Is e-commerce on site? => show department tab OR feature available
        if (ecommerceOnSite)
        {
            lastTabIndex++;
            tabs[lastTabIndex, 0] = departmentsString;
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'departments_tab');";
            tabs[lastTabIndex, 2] = ResolveUrl("~/CMSModules/Ecommerce/Administration/Users/User_Edit_Departments.aspx") + "?userid=" + userId + "&siteid=" + siteId; ;
        }

        if (showNotificationsTab)
        {
            lastTabIndex++;
            tabs[lastTabIndex, 0] = notificationsString;
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'notifications_tab');";
            tabs[lastTabIndex, 2] = ResolveUrl("~/CMSModules/Notifications/Administration/Users/User_Edit_Notifications.aspx") + "?userid=" + userId + "&siteid=" + siteId;
        }

        lastTabIndex++;
        tabs[lastTabIndex, 0] = categoriesString;
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'categories_tab');";
        tabs[lastTabIndex, 2] = "User_Edit_Categories.aspx?userid=" + userId + "&siteid=" + siteId;

        bool showFriendsTab = LicenseHelper.IsFeautureAvailableInUI(FeatureEnum.Friends, ModuleEntry.COMMUNITY);
        if (showFriendsTab)
        {
            lastTabIndex++;
            tabs[lastTabIndex, 0] = friendsString;
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'friends_tab');";
            tabs[lastTabIndex, 2] = ResolveUrl("~/CMSModules/Friends/Administration/Users/User_Edit_Friends_Frameset.aspx?userid=" + userId + "&siteid=" + siteId);
        }

        lastTabIndex++;
        tabs[lastTabIndex, 0] = subscriptionsString;
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'user_subscriptions_tab');";
        tabs[lastTabIndex, 2] = "User_Edit_Subscriptions.aspx?userid=" + userId + "&siteid=" + siteId;

        if (showLanguagesTab)
        {
            lastTabIndex++;
            tabs[lastTabIndex, 0] = languagesString;
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'user_languages_tab');";
            tabs[lastTabIndex, 2] = "User_Edit_Languages.aspx?userid=" + userId + "&siteid=" + siteId;
        }

        // Object relationships
        //if (TypeInfo.AllowObjectRelationships)
        //{
        //    lastTabIndex++;
        //    tabs[lastTabIndex, 0] = ResHelper.GetString("General.Relationships");
        //    tabs[lastTabIndex, 2] = ResolveUrl("~/CMSModules/AdminControls/Pages/ObjectRelationships.aspx?objectid=" + userId + "&objecttype=cms.user&siteid=" + siteId);
        //}

        CurrentMaster.Tabs.UrlTarget = "content";
        CurrentMaster.Tabs.Tabs = tabs;
    }

    #endregion
}
