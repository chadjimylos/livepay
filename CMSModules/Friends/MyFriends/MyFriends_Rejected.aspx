<%@ Page Language="C#" Theme="default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    AutoEventWireup="true" CodeFile="MyFriends_Rejected.aspx.cs" Inherits="CMSModules_Friends_MyFriends_MyFriends_Rejected" %>

<%@ Register Src="~/CMSModules/Friends/Controls/FriendsRejectedList.ascx" TagName="FriendsListRejected"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:FriendsListRejected ID="FriendsListRejected" runat="server" Visible="true" IsLiveSite="false" />
    <cms:LocalizedLabel ID="lblError" runat="server" ResourceString="friends.norejectedfriends"
        EnableViewState="false" />
</asp:Content>
