CREATE TABLE [Reporting_ReportCategory] (
		[CategoryID]               int NOT NULL IDENTITY(1, 1),
		[CategoryDisplayName]      nvarchar(200) NOT NULL,
		[CategoryCodeName]         nvarchar(200) NOT NULL,
		[CategoryGUID]             uniqueidentifier NOT NULL,
		[CategoryLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Reporting_ReportCategory]
	ADD
	CONSTRAINT [PK_Reporting_ReportCategory]
	PRIMARY KEY
	NONCLUSTERED
	([CategoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_Reporting_ReportCategory_CategoryDisplayName]
	ON [Reporting_ReportCategory] ([CategoryDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
