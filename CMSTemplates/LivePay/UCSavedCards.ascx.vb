﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports System.Data.SqlClient

Partial Class CMSTemplates_LivePay_UCSavedCards
    Inherits CMSUserControl
#Region "Variables"

    ''' <summary>
    ''' My property
    ''' </summary>
    Private mMyProperty As String = Nothing

#End Region

#Region "Properties"


    ''' <summary>
    ''' Gets or sets value of MyProperty
    ''' </summary>
    Public Property MyProperty() As String
        Get
            Return mMyProperty
        End Get
        Set(ByVal value As String)
            mMyProperty = value
        End Set
    End Property

#End Region

#Region "Methods"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        btnDeleteCard.ImageUrl = ResHelper.LocalizeString("{$=/app_themes/LivePay/SavedCards/btnDeleteCard.png|en-us=/app_themes/LivePay/SavedCards/btnDeleteCard_en-us.png$}")
        GridViewPayments.Columns(0).HeaderText = ResHelper.LocalizeString("{$=Φιλική Ονομασία Κάρτας|en-us=Friendly Name$}")
        GridViewPayments.Columns(1).HeaderText = ResHelper.LocalizeString("{$=Τύπος|en-us=Type$}")
        GridViewPayments.Columns(2).HeaderText = ResHelper.LocalizeString("{$=Αριθμός Κάρτας|en-us=Card Number$}")
        GridViewPayments.Columns(3).HeaderText = ResHelper.LocalizeString("{$=Ημερομηνία|en-us=Date$}")
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        Bind()
        btnDeleteCard.Attributes.Add("onclick", "return DeleteCards()")
       

        'Dim dt As DataTable = DBConnection.GetAllTheCards
        'For i As Integer = 0 To dt.Rows.Count - 1
        '    'Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()
        '    'Dim objConnection As SqlConnection = New SqlConnection(strConnection)
        '    'Dim query As String = "update LivePay_UserCards set CardNumber = '" & Left(dt.Rows(i)("CardNumber").ToString, 4) & CryptoFunctions.EncryptString(Right(dt.Rows(i)("CardNumber").ToString, 12)) & "' where CardNumber = '" & dt.Rows(i)("CardNumber").ToString & "'"
        '    'objConnection.Open()
        '    'Dim objCommand As SqlCommand = New SqlCommand(query, objConnection)
        '    'objCommand.CommandType = CommandType.Text
        '    'objCommand.ExecuteScalar()
        '    'objConnection.Close()

        '    'Response.Write(dt.Rows(i)("CardNumber").ToString & "   -   " & Left(dt.Rows(i)("CardNumber").ToString, 4) & CryptoFunctions.EncryptString(Right(dt.Rows(i)("CardNumber").ToString, 12)) & "<br>")
        'Next
    End Sub

    Private Sub Bind()
        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = DBConnection.GetUserCards(UserID)
        Session("GetSvdCardsDataTable") = dt
        GridViewPayments.DataSource = dt
        GridViewPayments.DataBind()
        If dt.Rows.Count = 0 Then
            btnDeleteCard.Visible = False
        Else
            btnDeleteCard.Visible = True
        End If
        'If dt.Rows.Count = 0 Then
        '    divCards.Visible = False
        'End If
    End Sub

    Protected Sub btnDeleteCardClick(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDeleteCard.Click
        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim SelectedCards As String = Replace(Replace(StripHTMLFunctions.StripTags(HiddenCbxList.Text), "_", String.Empty), "0,", String.Empty)
        DBConnection.DeleteUserCards(UserID, SelectedCards)
        HiddenCbxList.Text = 0
        Bind()
    End Sub
#End Region

    Protected Sub GridViewPayments_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewPayments.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cbxCard As CheckBox = DirectCast(e.Row.FindControl("cbxCard"), CheckBox)
            Dim lblID As Label = DirectCast(e.Row.FindControl("lblID"), Label)
            Dim lbldtm As Label = DirectCast(e.Row.FindControl("lbldtm"), Label)
            If Not IsNothing(cbxCard) Then
                cbxCard.Attributes.Add("onclick", String.Concat("SendChbxValue(this,'", StripHTMLFunctions.StripTags(lblID.Text), "')"))
                Dim dtm As Date = StripHTMLFunctions.StripTags(lbldtm.Text)
                lbldtm.Text = dtm.ToString("dd/MM/yyyy")
            End If
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GridViewPayments.Columns(0).HeaderText = ResHelper.LocalizeString("{$=Φιλική Ονομασία Κάρτας|en-us=Friendly Name$}")
        GridViewPayments.Columns(1).HeaderText = ResHelper.LocalizeString("{$=Τύπος|en-us=Type$}")
        GridViewPayments.Columns(2).HeaderText = ResHelper.LocalizeString("{$=Αριθμός Κάρτας|en-us=Card Number$}")
        GridViewPayments.Columns(3).HeaderText = ResHelper.LocalizeString("{$=Ημερομηνία|en-us=Date$}")
    End Sub
End Class
