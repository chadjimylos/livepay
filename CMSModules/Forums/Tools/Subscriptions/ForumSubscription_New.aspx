<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForumSubscription_New.aspx.cs"
    Inherits="CMSModules_Forums_Tools_Subscriptions_ForumSubscription_New" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <br />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblSubscriptionEmail" EnableViewState="false"
                    ResourceString="general.email" DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtSubscriptionEmail" runat="server" CssClass="TextBoxField" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvSubscriptionEmail" runat="server" ErrorMessage=""
                    ControlToValidate="txtSubscriptionEmail"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <br />
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="ContentButton" />
            </td>
        </tr>
    </table>
</asp:Content>
