using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.LicenseProvider;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_AddItems : CMSEcommerceModalPage
{
    protected int orderId = 0;
    protected ShoppingCartInfo mShoppingCartObj = null;
    private string selectedSite = null;

    /// <summary>
    /// Shopping cart object with order data
    /// </summary>
    protected ShoppingCartInfo ShoppingCartObj
    {
        get
        {
            if (mShoppingCartObj == null)
            {
                string cartSessionName = ValidationHelper.GetString(Request.QueryString["cart"], "");
                if (cartSessionName != "")
                {
                    mShoppingCartObj = SessionHelper.GetValue(cartSessionName) as ShoppingCartInfo;
                }
            }
            return mShoppingCartObj;
        }
    }


    /// <summary>
    /// Shopping cart items selector SKUID.
    /// </summary>
    private int SKUID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["SKUID"], 0);
        }
        set
        {
            ViewState["SKUID"] = value;
        }
    }


    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        lblProductName.Text = ResHelper.GetString("Order_Edit_AddItems.ProductName");
        lblProductCode.Text = ResHelper.GetString("Order_Edit_AddItems.ProductCode");
        lblDepartment.Text = ResHelper.GetString("Order_Edit_AddItems.Department");
        lblSites.Text = ResHelper.GetString("Order_Edit_AddItems.Sites");
        btnAdd.Text = ResHelper.GetString("Order_Edit_AddItems.Add");
        btnShow.Text = ResHelper.GetString("general.show");

        if (!RequestHelper.IsPostBack())
        {
            // Display products
            plcProducts.Visible = true;

            // Hide shopping cart items selector
            plcSelector.Visible = false;
        }

        bool isAdmin = (CMSContext.CurrentUser != null) && (CMSContext.CurrentUser.IsGlobalAdministrator);

        if (!isAdmin)
        {
            this.plcSites.Visible = false;
            this.siteSelector.StopProcessing = false;
            selectedSite = CMSContext.CurrentSiteName;
        }
        else
        {
            // Set site selector        
            siteSelector.AllowAll = false;
            siteSelector.UseCodeNameForSelection = true;
            siteSelector.UniSelector.SpecialFields = new string[2, 2] { { ResHelper.GetString("general.selectall"), "" }, { ResHelper.GetString("Product_List.Sites.NotAssigned"), "0" } };

            if (!RequestHelper.IsPostBack())
            {
                selectedSite = CMSContext.CurrentSiteName;
                siteSelector.Value = selectedSite;
            }
            else
            {
                selectedSite = ValidationHelper.GetString(siteSelector.Value, String.Empty);
            }
        }

        GridViewProducts.Columns[0].HeaderText = "<span style=\"padding-left:10px;\">" + ResHelper.GetString("Order_Edit_AddItems.GridProductName") + "</span>";
        GridViewProducts.Columns[1].HeaderText = ResHelper.GetString("Order_Edit_AddItems.GridProductCode");
        GridViewProducts.Columns[2].HeaderText = ResHelper.GetString("Order_Edit_AddItems.GridUnitPrice");
        GridViewProducts.Columns[3].HeaderText = ResHelper.GetString("Order_Edit_AddItems.GridQuantity");
        GridViewProducts.Columns[4].Visible = false;
        InitializeGridView();

        PageTitleAddItems.TitleText = ResHelper.GetString("Order_Edit_AddItems.Title");
        PageTitleAddItems.TitleImage = GetImageUrl("Objects/Ecommerce_OrderItem/new.png");

        // Initialize shopping cart item selector
        cartItemSelector.SKUID = this.SKUID;
        cartItemSelector.LocalShoppingCartObj = this.ShoppingCartObj;
        cartItemSelector.OnAddToShoppingCart += new EventHandler(cartItemSelector_OnAddToShoppingCart);
    }


    /// <summary>
    /// On GridViewProduct databound event.
    /// </summary>
    protected void GridViewProducts_DataBound(object sender, EventArgs e)
    {
        Label id;
        TextBox txtUnits;

        if (GridViewProducts.Rows.Count > 0)
        {
            btnAdd.Enabled = true;

            for (int i = 0; i < GridViewProducts.Rows.Count; i++)
            {
                //copy id from 5th column to invisible label in last column
                id = new Label();
                id.Text = GridViewProducts.Rows[i].Cells[4].Text;
                id.ID = id.Text;
                id.Attributes.Add("style", "display: none;");
                GridViewProducts.Rows[i].Cells[5].Controls.Add(id);

                txtUnits = (TextBox)GridViewProducts.Rows[i].Cells[3].Controls[1];
                txtUnits.ID = "txtTaxValue" + id.Text;
                txtUnits.TextChanged += new EventHandler(txtUnits_Changed);
                txtUnits.MaxLength = 9;
            }
        }
    }


    protected void txtUnits_Changed(object sender, EventArgs e)
    {
    }


    /// <summary>
    /// On BtnShow click event.
    /// </summary>
    protected void BtnShow_Click(object sender, EventArgs e)
    {
    }


    /// <summary>
    /// GridView initialization.
    /// </summary>
    protected void InitializeGridView()
    {
        GeneralConnection conn = ConnectionHelper.GetConnection();

        string productNameFilter = txtProductName.Text.Trim().Replace("'", "''");
        string productCodeFilter = txtProductCode.Text.Trim().Replace("'", "''");

        // to display ONLY product - not product options
        string where = "(SKUEnabled = 1) AND (SKUOptionCategoryID IS NULL)";

        if (productNameFilter != "")
        {
            where += " AND (SKUName LIKE '%" + productNameFilter + "%')";
        }
        if (productCodeFilter != "")
        {
            where += " AND (SKUNumber LIKE '%" + productCodeFilter + "%')";
        }
        if (departmentElem.DepartmentID > 0)
        {
            where += " AND (SKUDepartmentID = " + departmentElem.DepartmentID + ")";
        }

        // Sites
        if (!String.IsNullOrEmpty(selectedSite))
        {
            // Not assigned products
            if (selectedSite == "0")
            {
                where += " AND (SKUID NOT IN (SELECT NodeSKUID FROM View_CMS_Tree_Joined WHERE NodeSKUID IS NOT NULL))";
            }
            // Products assigned to the specified site
            else
            {
                where += " AND (SKUID IN (SELECT NodeSKUID FROM View_CMS_Tree_Joined WHERE SiteName = '" + selectedSite.Replace("'", "''") + "' AND NodeSKUID IS NOT NULL))";
            }
        }

        DataSet dsSKU = SKUInfoProvider.GetSKUs(where, "SKUName");

        GridViewProducts.Columns[4].Visible = true;
        GridViewProducts.DataSource = dsSKU.Tables[0];
        GridViewProducts.DataBind();
        GridViewProducts.Columns[4].Visible = false;
        GridViewProducts.GridLines = GridLines.Horizontal;
    }


    protected void btnAddOneUnit_Click(object sender, EventArgs e)
    {
        int skuId = ValidationHelper.GetInteger(((LinkButton)sender).CommandArgument, 0);

        // If product has some product options 
        // -> abort inserting products to the shopping cart
        if (!DataHelper.DataSourceIsEmpty(OptionCategoryInfoProvider.GetSKUOptionCategories(skuId, "CategoryEnabled = 1")))
        {
            lblError.Visible = true;
            lblError.Text = string.Format(ResHelper.GetString("Order_Edit_AddItems.ProductOptionsRequired"), "");

            // Hide products
            plcProducts.Visible = false;

            // Show shopping cart item selector
            plcSelector.Visible = true;

            // Save SKUID to the viewstate
            this.SKUID = skuId;

            cartItemSelector.SKUID = this.SKUID;
            cartItemSelector.ReloadData();
        }
        // If product has no product options
        else
        {
            // Add product and close dialog window
            ScriptHelper.RegisterClientScriptBlock(this.Page, typeof(string), "addproduct", ScriptHelper.GetScript("AddProducts(" + skuId + ", 1);"));
        }
    }

    void cartItemSelector_OnAddToShoppingCart(object sender, EventArgs e)
    {
        // Add product and close dialog window
        ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "addScript", ScriptHelper.GetScript("AddProducts(" + cartItemSelector.SKUID + "," + cartItemSelector.Quantity + ",'" + cartItemSelector.ProductOptions + "');"));
    }


    /// <summary>
    /// On BtnAdd click event.
    /// </summary>
    protected void BtnAdd_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        Label lblSkuId;
        TextBox txtUnits;
        int units = 0;
        int skuId = 0;
        string allUnits = null;
        string allSkuId = null;

        for (int i = 0; i < GridViewProducts.Rows.Count; i++)
        {
            lblSkuId = (Label)GridViewProducts.Rows[i].Cells[5].Controls[0];
            skuId = ValidationHelper.GetInteger(lblSkuId.Text, 0);
            if (skuId > 0)
            {
                txtUnits = (TextBox)GridViewProducts.Rows[i].Cells[3].Controls[1];
                txtUnits.ID = "txtTaxValue" + skuId.ToString();
                units = ValidationHelper.GetInteger(txtUnits.Text, 0);
                if (units > 0)
                {
                    // If product has some product options 
                    // -> abort inserting products to the shopping cart
                    if (!DataHelper.DataSourceIsEmpty(OptionCategoryInfoProvider.GetSKUOptionCategories(skuId, "CategoryEnabled = 1")))
                    {
                        string skuName = ((LinkButton)GridViewProducts.Rows[i].Cells[0].Controls[1]).Text;
                        lblError.Visible = true;
                        lblError.Text = string.Format(ResHelper.GetString("Order_Edit_AddItems.ProductOptionsRequired"), skuName);
                        return;
                    }

                    // Create strings with SKU IDs and units separated by ';'
                    allSkuId += skuId.ToString() + ";";
                    allUnits += units.ToString() + ";";
                }
            }
        }

        // Close this modal window and refresh parent values in window
        CloseWindow(allSkuId, allUnits);

    }


    /// <summary>
    /// Generates script that closes the window and refreshes the parent page.
    /// </summary>
    /// <param name="skuIds">String with SKU IDs separated by ';'.</param>
    /// <param name="units">String with SKU units separated by ';'.</param>
    private void CloseWindow(string skuIds, string units)
    {
        ScriptHelper.RegisterClientScriptBlock(this.Page, typeof(string), "addproductClose", ScriptHelper.GetScript("AddProducts('" + skuIds + "','" + units + "');"));
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (plcSelector.Visible)
        {
            // Get product name and price
            SKUInfo skuObj = SKUInfoProvider.GetSKUInfo(this.SKUID);
            string skuName = "";
            if (skuObj != null)
            {
                lblPriceValue.Text = EcommerceFunctions.GetFormatedPrice(skuObj.SKUPrice, skuObj.SKUDepartmentID, this.ShoppingCartObj);
                skuName = ResHelper.LocalizeString(skuObj.SKUName); ;
            }

            // Show info text            
            lblTitle.Text = ResHelper.GetString("Order_Edit_AddItems.ProductOptions");
            lbPrice.Text = ResHelper.GetString("Order_Edit_AddItems.UnitPrice");
            //lblName.Text = ResHelper.GetString("Order_Edit_AddItems.SkuName");

            // Initializes page title control		
            string[,] tabs = new string[2, 3];
            tabs[0, 0] = ResHelper.GetString("Order_Edit_AddItems.Products");
            tabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Orders/Order_Edit_AddItems.aspx?cart=" + ValidationHelper.GetString(Request.QueryString["cart"], "");
            tabs[0, 2] = "";
            tabs[1, 0] = skuName;
            tabs[1, 1] = "";
            tabs[1, 2] = "";
            PageTitleAddItems.Breadcrumbs = tabs;
        }
    }
}
