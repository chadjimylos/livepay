﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.ExtendedControls;
using CMS.DataEngine;
using CMS.EmailEngine;
using CMS.SiteProvider;
using CMS.EventLog;
using CMS.URLRewritingEngine;
using CMS.MembershipProvider;
using CMS.PortalEngine;

public partial class CMSWebParts_AegeanPower_PasswdRetrieve: CMSAbstractWebPart
{
    #region "Private properties"


    #endregion
    
    #region "Public properties"

    /// <summary>
    /// Gets or sets the sender e-mail (from)
    /// </summary>
    public string SendEmailFrom
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("SendEmailFrom"), SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSSendPasswordEmailsFrom"));
        }
        set
        {
            this.SetValue("SendEmailFrom", value);
        }
    }

    /// <summary>
    /// PasswdRetrieval Text
    /// </summary>
    public string PasswdRetrievalText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("PasswdRetrievalText"), "");
        }
        set
        {
            this.SetValue("PasswdRetrievalText", value);
        }
    }

    /// <summary>
    /// PasswdRetrieval Button
    /// </summary>
    public string PasswdRetrievalButton
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("PasswdRetrievalButton"), "");
        }
        set
        {
            this.SetValue("PasswdRetrievalButton", value);
        }
    }
    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        
        if (this.StopProcessing)
        {
            this.rqValue.Visible = false;
            // Do not process
        }
        else
        {
            if (!CMSContext.CurrentUser.IsAuthenticated())
            {
                lblPasswdRetrieval.Text = PasswdRetrievalText;
                Panel1.Visible = true;
                rqValue.ErrorMessage = ResHelper.GetString("LogonForm.rqValue");
                btnPasswdRetrieval.Text = ResHelper.LocalizeString("{$=Αποστολή|en-us=Send$}");
                //btnPasswdRetrieval.Text = PasswdRetrievalButton;

                btnPasswdRetrieval.Click += new EventHandler(btnPasswdRetrieval_Click);
                rqValue.ErrorMessage = "Εισάγετε το email σας";
                if (!RequestHelper.IsPostBack())
                {
                }
            }
            else
            {
                Panel1.Visible = false;
            }
        }
    }


    /// <summary>
    /// Retrieve the user password
    /// </summary>
    /// 

    void btnPasswdRetrieval_Click(object sender, EventArgs e)
    {
        //string value = txtPasswordRetrieval.Text.Trim();
        
        //if (value != String.Empty)
        //{
        //    //---- GetUserByEmail
        //    if (UserInfoProvider.GetUserInfo(value) != null)
        //    {
        //        UserInfo usr = UserInfoProvider.GetUserInfo(value);
        //        txtPasswordRetrieval.Style.Add("display", "none");
        //        lblPasswdRetrieval.Style.Add("display", "none");         
        //        lblResult.Visible = false;
        //        btnPasswdRetrieval.Visible = false;
        //        EmailDiv.Visible = false;
        //        lblResult.Text = UserInfoProvider.ForgottenEmailRequest(value, CMSContext.CurrentSiteName, "LOGONFORM", this.SendEmailFrom, CMSContext.CurrentResolver);
        //        lblResult.Text = ResHelper.LocalizeString("{$=Ο νέος σας κωδικός έχει σταλεί στον λογαριασμό σας|en-us=ο νέος σας κωδικός έχει σταλεί στον λογαριασμό σας$}");
        //        lblResult.Style.Add("color", "green");
        //        lblResult.Visible = true;
        //    }
        //    else
        //    {
        //        lblResult.Visible = true;
        //        lblResult.Text = "<font color='red'>" + ResHelper.LocalizeString("{$=Δεν βρέθηκε καταχωρημένος  χρήστης με αυτό  το email|en-us=Δεν βρέθηκε καταχωρημένος  χρήστης με αυτό  το email$}") + "</font>";
        //    }          
        //}

        string value = txtPasswordRetrieval.Text.Trim();

        if (value != "")
        {
            string userParam = txtPasswordRetrieval.Text.Trim().Replace("'", "''");

            DataSet ds = UserInfoProvider.GetUsers("UserName=N'" + userParam + "' OR Email=N'" + userParam + "'", null);

            // User exists, send email with password
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                string siteName = CMSContext.CurrentSiteName;
                // Get user info
                UserInfo user = new UserInfo(ds.Tables[0].Rows[0], null, null);

                // Quit if user do not have an email
                if (string.IsNullOrEmpty(user.Email))
                {
                    //lblResult.Text = ResHelper.GetString("LogonForm.NoEmail");
                    //this.pnlPasswdRetrieval.Attributes.Add("style", "display:block;");
                    lblResult.Visible = true;
                    lblResult.Text = "<font color='red'>" + ResHelper.LocalizeString("{$=Δεν βρέθηκε καταχωρημένος  χρήστης με αυτό  το email|en-us=Δεν βρέθηκε καταχωρημένος  χρήστης με αυτό  το email$}") + "</font>";
                    return;
                }

                // Send password
                EmailTemplateInfo emailTemplate = EmailTemplateProvider.GetEmailTemplate("AegeanPowerforgottenPassword", siteName);
                if (emailTemplate != null)
                {
                    EmailMessage message = new EmailMessage();
                    message.EmailFormat = EmailFormatEnum.Default;
                    message.From = this.SendEmailFrom;
                    message.Subject = ResHelper.GetString("LogonForm.ForgottenPasswordSubject");
                    message.Recipients = user.Email;
                    message.Body = emailTemplate.TemplateText;

                    // Get the password
                    string password = ValidationHelper.GetString(user.GetValue("UserPassword"), "");
                    switch (user.PasswordFormat.ToLower())
                    {
                        // SHA1 hash of the password, generate new
                        case "sha1":
                            password = UserInfoProvider.GenerateNewPassword();
                            break;
                    }

                    // Macros
                    string[,] macros = new string[3, 2];
                    macros[0, 0] = "LogonUrl";
                    macros[0, 1] = HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
                    macros[1, 0] = "UserName";
                    macros[1, 1] = user.UserName;
                    macros[2, 0] = "Password";
                    macros[2, 1] = password;

                    // Create macro resolver
                    MacroResolver resolver = new MacroResolver();
                    resolver.SourceParameters = macros;

                    lblResult.Text = ResHelper.LocalizeString("{$=Ο νέος σας κωδικός έχει σταλεί στον λογαριασμό σας|en-us=ο νέος σας κωδικός έχει σταλεί στον λογαριασμό σας$}");

                    //ο νέος σας κωδικός έχει σταλεί στον λογαριασμό σας
                    lblResult.Style.Add("color", "green");
                    lblResult.Visible = true;

                    try
                    {
                        // Attach template metafiles to e-mail
                        MetaFileInfoProvider.ResolveMetaFileImages(message, emailTemplate.TemplateID, EmailObjectType.EMAILTEMPLATE, MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE);
                        // Send email
                        EmailSender.SendEmailWithTemplateText(siteName, message, emailTemplate, resolver, true);

                        // Set new password after email was send successfully
                        UserInfoProvider.SetPassword(user.UserName, password);
                        //lblResult.Text = ResHelper.GetString("LogonForm.PasswordSent") + " " + HTMLHelper.HTMLEncode(user.Email) + ".";
                        lblResult.Text = ResHelper.LocalizeString("{$=Ο νέος σας κωδικός έχει σταλεί στον λογαριασμό σας|en-us=O νέος σας κωδικός έχει σταλεί στον λογαριασμό σας$}");
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider eventLog = new EventLogProvider();
                        eventLog.LogEvent("Password retrieval", "LOGONFORM", ex);
                        lblResult.Text = "Failed to send the password: " + ex.Message;
                    }
                }
            }
            // Wrong username or email, inform user
            else
            {
                lblResult.Visible = true;
                lblResult.Text = "<font color='red'>" + ResHelper.LocalizeString("{$=Δεν βρέθηκε καταχωρημένος  χρήστης με αυτό  το email|en-us=Δεν βρέθηκε καταχωρημένος  χρήστης με αυτό  το email$}") + "</font>";
                //lblResult.Text = ResHelper.GetString("LogonForm.NoUser");
                //this.pnlPasswdRetrieval.Attributes.Add("style", "display:block;");
            }
            lblResult.Visible = true;
        }

    }
}
