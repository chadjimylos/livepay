<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    CodeFile="MyFriends_ToApprove.aspx.cs" Theme="default" Inherits="CMSModules_Friends_MyFriends_MyFriends_ToApprove" %>

<%@ Register Src="~/CMSModules/Friends/Controls/FriendsToApprovalList.ascx" TagName="FriendsListToApprove"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:FriendsListToApprove ID="FriendsListToApprove" runat="server" Visible="true"
        IsLiveSite="false" />
    <cms:LocalizedLabel ID="lblError" runat="server" ResourceString="friends.nowaitingfriends" EnableViewState="false" />
</asp:Content>
