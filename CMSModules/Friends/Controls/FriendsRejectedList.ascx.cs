using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.CMSHelper;
using CMS.Community;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Friends_Controls_FriendsRejectedList : CMSAdminListControl
{
    #region "Private variables"

    private int mUserId = 0;
    private bool mDisplayFilter = true;
    protected string dialogsUrl = "~/CMSModules/Friends/Dialogs/";
    private bool mUseEncapsulation = true;
    private TimeZoneInfo usedTimeZone = null;
    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets and sets User ID.
    /// </summary>
    public int UserID
    {
        get
        {
            return mUserId;
        }
        set
        {
            mUserId = value;
        }
    }


    /// <summary>
    /// Gets and sets whether to show filter.
    /// </summary>
    public bool DisplayFilter
    {
        get
        {
            return mDisplayFilter;
        }
        set
        {
            mDisplayFilter = value;
            searchElem.Visible = value;
        }
    }


    /// <summary>
    /// Determines whether use showing/hiding list of rejected friends
    /// </summary>
    public bool UseEncapsulation
    {
        get
        {
            return mUseEncapsulation;
        }
        set
        {
            mUseEncapsulation = value;
        }
    }


    /// <summary>
    /// Indicates if the show/hide link should be displayed.
    /// </summary>
    public bool ShowLink
    {
        get
        {
            return plcLink.Visible;
        }
        set
        {
            plcLink.Visible = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            Visible = false;
            gridElem.StopProcessing = true;
            // Do not load data
        }
        else
        {
            // Check permissions
            RaiseOnCheckPermissions("Read", this);

            if (StopProcessing)
            {
                return;
            }

            if (IsLiveSite)
            {
                dialogsUrl = "~/CMSModules/Friends/CMSPages/";
            }
            Visible = true;

            btnRemoveSelected.Attributes.Add("onclick", "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("friends.ConfirmRemove")) + ");");

            // Register the dialog script
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY,
                                                   ScriptHelper.DialogScript);

            // Register the refreshing script
            string refreshScript = ScriptHelper.GetScript("function refreshList(){" + Page.ClientScript.GetPostBackEventReference(hdnRefresh, string.Empty) + "}");
            ScriptHelper.RegisterClientScriptBlock(Page, typeof(string), "friendsListRefresh", refreshScript);

            // Register approval script
            StringBuilder approveScript = new StringBuilder();
            approveScript.AppendLine("function Approve(item, gridId)");
            approveScript.AppendLine("{");
            approveScript.AppendLine("   var items = '';");
            approveScript.AppendLine("   if(item == null)");
            approveScript.AppendLine("   {");
            approveScript.AppendLine("       items = document.getElementById(gridId).value;");
            approveScript.AppendLine("   }");
            approveScript.AppendLine("   else");
            approveScript.AppendLine("   {");
            approveScript.AppendLine("       items=item;");
            approveScript.AppendLine("   }");
            approveScript.AppendLine("   if(items != '' && items != '|')");
            approveScript.AppendLine("   {");
            approveScript.AppendLine("      modalDialog('" + CMSContext.ResolveDialogUrl(dialogsUrl + "Friends_Approve.aspx") + "?userid=" + UserID + "&ids='+items, 'rejectDialog', 410, 270);");
            approveScript.AppendLine("   }");
            approveScript.AppendLine("   ClearSelection_" + gridElem.ClientID + "();");
            approveScript.AppendLine("   return false;");
            approveScript.AppendLine("}");
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "approveScript", ScriptHelper.GetScript(approveScript.ToString()));

            // Add action to button
            btnApproveSelected.OnClientClick = "return Approve(null,'" + gridElem.ClientID + "_hidSelection');";

            if (!RequestHelper.IsPostBack())
            {
                pnlInner.Visible = !ShowLink;
            }
            btnShowHide.ResourceString = !pnlInner.Visible ? "friends.showrejected" : "friends.hiderejected";

            if (!UseEncapsulation)
            {
                plcLink.Visible = false;
                pnlInner.Visible = true;
            }

            // Setup grid
            gridElem.OnAction += uniGrid_OnAction;
            gridElem.OnDataReload += new UniGrid.OnDataReloadEventHandler(gridElem_OnDataReload);
            gridElem.OnExternalDataBound += gridElem_OnExternalDataBound;
            gridElem.OrderBy = "UserName";
            gridElem.IsLiveSite = IsLiveSite;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        if (!DisplayFilter)
        {
            Visible = HasData();
        }
        base.OnPreRender(e);

        // Reset grid selection after multiple action
        if (RequestHelper.IsPostBack())
        {
            string invokerName = Page.Request.Params.Get("__EVENTTARGET");
            Control invokeControl = !string.IsNullOrEmpty(invokerName) ? Page.FindControl(invokerName) : null;
            if (invokeControl == hdnRefresh)
            {
                gridElem.ResetSelection();
            }
        }
    }

    #endregion


    #region "Button handling"

    protected void btnShowHide_Click(object sender, EventArgs e)
    {
        pnlInner.Visible = !pnlInner.Visible;
        btnShowHide.ResourceString = !pnlInner.Visible ? "friends.showrejected" : "friends.hiderejected";
    }


    protected void btnRemoveSelected_Click(object sender, EventArgs e)
    {
        // If there user selected some items
        if (gridElem.SelectedItems.Count > 0)
        {
            RaiseOnCheckPermissions(PERMISSION_MANAGE, this);
            // Create where condition
            string where = "FriendID IN (";
            foreach (object friendId in gridElem.SelectedItems)
            {
                where += ValidationHelper.GetInteger(friendId, 0) + ",";
            }
            where = where.TrimEnd(',') + ")";
            // Get all needed friendships
            DataSet friendships = FriendInfoProvider.GetFriends(where, null);
            if (!DataHelper.DataSourceIsEmpty(friendships))
            {
                // Delete all these friendships
                foreach (DataRow friendship in friendships.Tables[0].Rows)
                {
                    FriendInfoProvider.DeleteFriendInfo(new FriendInfo(friendship));
                }
            }
            gridElem.ResetSelection();
            // Reload grid
            gridElem.ReloadData();
        }
    }

    #endregion


    #region "UniGrid actions"

    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName)
        {
            case "approve":
                // Get action button
                ImageButton approveBtn = (ImageButton)sender;
                // Get full row view
                DataRowView drv = GetDataRowView((DataControlFieldCell)approveBtn.Parent);
                // Add custom reject action
                approveBtn.Attributes["onclick"] = "return Approve('" + drv["FriendID"] + "',null);";
                return approveBtn;

            case "friendrejectedwhen":
                if (currentUserInfo == null)
                {
                    currentUserInfo = CMSContext.CurrentUser;
                }
                if (currentSiteInfo == null)
                {
                    currentSiteInfo = CMSContext.CurrentSite;
                }
                DateTime currentDateTime = ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME);
                if (IsLiveSite)
                {
                    return CMSContext.ConvertDateTime(currentDateTime, this);
                }
                else
                {
                    return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(currentDateTime, currentUserInfo, currentSiteInfo, out usedTimeZone);
                }

            case "formattedusername":
                return HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(Convert.ToString(parameter), this.IsLiveSite));

        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        RaiseOnCheckPermissions(PERMISSION_MANAGE, this);
        switch (actionName)
        {
            case "remove":
                FriendInfoProvider.DeleteFriendInfo(ValidationHelper.GetInteger(actionArgument, 0));
                gridElem.ReloadData();
                break;
        }
    }


    protected DataSet gridElem_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        // Load the data
        DataSet friendships = FriendInfoProvider.GetFullRejectedFriends(mUserId, searchElem.WhereCondition, gridElem.SortDirect, currentTopN, "FriendID,UserName,UserNickname,FullName,FriendComment,FriendRejectedWhen");
        // Hide filter when not needed
        if (searchElem.FilterIsSet)
        {
            DisplayFilter = true;
        }
        else if (!DataHelper.DataSourceIsEmpty(friendships))
        {
            int rowsCount = friendships.Tables[0].Rows.Count;
            DisplayFilter = (gridElem.FilterLimit <= 0) || (rowsCount > gridElem.FilterLimit);
        }
        else
        {
            DisplayFilter = false;
        }
        totalRecords = DataHelper.GetItemsCount(friendships);
        return friendships;
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Indicates if there are some data
    /// </summary>
    public bool HasData()
    {
        return (!DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource));
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }

    #endregion
}
