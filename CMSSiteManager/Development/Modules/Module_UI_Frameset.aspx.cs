using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CMS.ExtendedControls;
using CMS.UIControls;
using CMS.CMSHelper;
using CMS.GlobalHelper;

public partial class CMSSiteManager_Development_Modules_Module_UI_Frameset : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CultureHelper.IsUICultureRTL())
        {
            ControlsHelper.ReverseFrames(this.uiFrameset);
        }

        int moduleId = QueryHelper.GetInteger("moduleid", 0);
        treeFrame.Attributes["src"] = "Module_UI_Tree.aspx?moduleID=" + moduleId;
        contentFrame.Attributes["src"] = "Module_UI_New.aspx?moduleID=" + moduleId + "&saved=" + QueryHelper.GetInteger("saved", 0);
    }
}
