﻿Imports System.Data
Imports CMS.SettingsProvider
Imports CMS.SiteProvider
Imports CMS.DataEngine

Imports System
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls

Partial Class CMSTemplates_LivePay_xmlHttpMerchants
    Inherits System.Web.UI.Page


    Protected ReadOnly Property MerchantKeyWord() As String
        Get
            Dim sMerchantKeyWord As String = String.Empty
            If Not String.IsNullOrEmpty(Request("MerchantKeyWord")) Then
                sMerchantKeyWord = Request("MerchantKeyWord")
            End If
            Return sMerchantKeyWord
        End Get
    End Property

    Protected ReadOnly Property FormPage() As String
        Get
            Dim sFormPage As String = String.Empty
            If Not String.IsNullOrEmpty(Request("FormPage")) Then
                sFormPage = Request("FormPage")
            End If
            Return sFormPage
        End Get
    End Property



    Protected ReadOnly Property RecordsNo() As Integer
        Get
            Dim iRecordsNo As Integer = 5
            If Not String.IsNullOrEmpty(Request("RecordsNo")) Then
                iRecordsNo = Request("RecordsNo")
            End If
            Return iRecordsNo
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetItemsCustomTable("LivePay.Merchant", "MerchantID", "CompanyName")
        Me.repSearch.TransformationName = "LivePay.Merchant.Preview"
    End Sub

    Private Sub GetItemsCustomTable(ByVal CustomTable As String, ByVal ColumeValue As String, ByVal ColumeText As String)
      
        Dim key As String = Replace(MerchantKeyWord, "'", String.Empty)
        key = Replace(key, """", String.Empty)
        If key <> String.Empty Then

            ' Get data class using custom table name
            'Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass(CustomTable)
            'If customTableClassInfo Is Nothing Then
            'Throw New Exception("Given custom table does not exist.")
            'End If
            ' Initialize custom table item provider with current user info and general connection
            'Dim ctiProvider As New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())
            'Dim where As String = String.Concat("CompanyName", " like '", key, "%'")
            ' Get custom table items
            'Dim jsname As Guid = New Guid

            'Dim dsItems As DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, where, ColumeText, RecordsNo)
            ' Check if DataSet is not empty
            ' If Not DataHelper.DataSourceIsEmpty(dsItems) Then
            If FormPage <> String.Empty Then
                'Response.Write(dsItems.Tables(0).Rows.Count & "#]")
                Response.Write(Me.repSearch.Items.Count.ToString & "#]")
            End If

            'If dsItems.Tables(0).Rows.Count > 0 Then

            '    For i As Integer = 0 To dsItems.Tables(0).Rows.Count - 1
            '        If FormPage = String.Empty Then
            '            'Response.Write(String.Concat("<div class=""QuickSrcResTitle""><a onclick=""SetSuggestValue('", dsItems.Tables(0).Rows(i)("CompanyName").ToString(), "','", dsItems.Tables(0).Rows(i)("LivePayID").ToString(), "')"" class=""Chand"">» ", dsItems.Tables(0).Rows(i)("CompanyName").ToString(), "</a></div><div class=""SuggestBorderLine""></div>"))
            '        Else
            '            'Response.Write(String.Concat("<div class=""QuickSrcResTitle""><a href=""RegisteredUser.aspx?MerchantID=", dsItems.Tables(0).Rows(i)("MerchantID").ToString(), """ class=""Chand"">» ", dsItems.Tables(0).Rows(i)("CompanyName").ToString(), "</a></div><div class=""SuggestBorderLine""></div>"))
            '        End If

            '    Next
            'Else
            'End If
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim key As String = Replace(MerchantKeyWord, "'", String.Empty)
        key = Replace(key, """", String.Empty)
        key = Replace(key, "%", String.Empty)
        If key <> String.Empty Then

            Me.repSearch.WhereCondition = " (DiscreetTitle COLLATE Greek_CI_AI like N'%" & key & "%' or CompanyName COLLATE Greek_CI_AI like N'%" & key & "%') "
            Me.repSearch.TopN = RecordsNo
            If FormPage <> String.Empty Then
                Me.repSearch.TransformationName = "LivePay.Merchant.SearchMerchants"
            Else
                Me.repSearch.TransformationName = "LivePay.Merchant.SearchMerchantsForm"
            End If
        End If
    End Sub

End Class
