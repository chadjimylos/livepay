using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Send : CMSNewsletterPage
{
    protected int newsletterId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        newsletterId = QueryHelper.GetInteger("newsletterid", 0);
        if (newsletterId == 0)
        {
            btnSend.Enabled = false;
            return;
        }

        radSendNow.Text = ResHelper.GetString("Newsletter_Issue_Send.SendNow");
        radSendToSpecified.Text = ResHelper.GetString("Newsletter_Issue_Send.SendToSpecified");
        lblEmail.Text = ResHelper.GetString("Newsletter_Issue_Send.Email");
        btnSend.Text = ResHelper.GetString("Newsletter_Issue_Send.Send");
        
        if (!RequestHelper.IsPostBack())
        {
            radSendNow.Checked = true;
        }
    }


    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "authorissues"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "authorissues");
        }

        // Generate new issue
        try
        {
            int issueId = EmailQueueManager.GenerateDynamicIssue(newsletterId);
            if (issueId > 0)
            {
                if (radSendToSpecified.Checked)
                {
                    //Send to specified receivers
                    IssueProvider.SendIssue(issueId, txtSendToSpecified.Text);
                }
                else
                {
                    // Send to all subscribers
                    IssueProvider.SendIssue(issueId);
                }
            }

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("Newsletter_Send.SuccessfullySent");
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Newsletter_Send.ErrorSent") + ex.Message;
        }
    }


    protected void radGroupSend_CheckedChanged(object sender, EventArgs e)
    {
        if (radSendNow.Checked)
        {
            txtSendToSpecified.Enabled = false;
        }
        else if (radSendToSpecified.Checked)
        {
            txtSendToSpecified.Enabled = true;
        }
    }
}
