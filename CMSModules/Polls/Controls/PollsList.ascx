<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollsList.ascx.cs" Inherits="CMSModules_Polls_Controls_PollsList" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<asp:Panel ID="pnlBody" runat="server">
    <cms:UniGrid runat="server" ID="UniGrid" GridName="~/CMSModules/Polls/Controls/PollsList.xml"
        OrderBy="PollDisplayName" />
</asp:Panel>
