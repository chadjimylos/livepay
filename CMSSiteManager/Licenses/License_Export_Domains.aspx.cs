﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using System.Collections;
using CMS.CMSImportExport;



public partial class CMSSiteManager_Licenses_License_Export_Domains : SiteManagerPage
{
    #region "Page events"

    /// <summary>
    /// Page load event.
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Arguments</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Setup page title text and image
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("license.export");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_LicenseKey/export24.png");

        this.CurrentMaster.Title.HelpTopicName = "license_export";
        this.CurrentMaster.Title.HelpName = "helpTopic";
        
        // Setup breadcrums
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Licenses_License_New.Licenses");
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Licenses/License_List.aspx";
        pageTitleTabs[1, 0] = ResHelper.GetString("license.export");
        pageTitleTabs[1, 1] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;

        // Resource strings
        rfvFileName.ErrorMessage = ResHelper.GetString("license.export.filenameempty");

        // Default file name
        if (!RequestHelper.IsPostBack())
        {
            lblInfo.ResourceString = "license.export.info";
            lblInfo.Visible = true;
            txtFileName.Text = "list_of_domains_" + DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("HHmm") + ".txt";
        }
    }


    /// <summary>
    /// Button Ok click.
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Arguments</param>
    protected void btnOk_Click(object sender, EventArgs e)
    {
        // Validate file name
        string fileName = txtFileName.Text.Trim();
        string errorMessage = new Validator().NotEmpty(fileName, ResHelper.GetString("lincense.export.filenameempty")).IsFileName(fileName, ResHelper.GetString("license.export.notvalidfilename")).Result;
        if (!string.IsNullOrEmpty(errorMessage))
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
        else
        {
            try
            {
                // Create writers
                string path = Server.MapPath(ImportExportHelper.EXPORT_RELATIVE_PATH + fileName);
                using (FileStream file = new FileStream(path, FileMode.Create))
                {
                    using (StreamWriter sw = new StreamWriter(file))
                    {
                        // Array list for duplicity checking
                        ArrayList allSites = new ArrayList();

                        // Get all sites
                        DataSet sites = SiteInfoProvider.GetSites(null, null, "SiteID,SiteDomainName");
                        if (!DataHelper.DataSourceIsEmpty(sites))
                        {
                            foreach (DataRow dr in sites.Tables[0].Rows)
                            {
                                // Get domain
                                string domain = ValidationHelper.GetString(dr["SiteDomainName"], "");
                                if (!string.IsNullOrEmpty(domain))
                                {
                                    domain = GetDomain(domain);
                                    // Add to file
                                    if (!allSites.Contains(domain))
                                    {
                                        sw.WriteLine(domain);
                                        allSites.Add(domain);
                                    }

                                    // Add all domain aliases
                                    DataSet aliases = SiteDomainAliasInfoProvider.GetDomainAliases("SiteID=" + ValidationHelper.GetString(dr["SiteID"], ""), null, "SiteDomainAliasName");
                                    if (!DataHelper.DataSourceIsEmpty(aliases))
                                    {
                                        foreach (DataRow drAlias in aliases.Tables[0].Rows)
                                        {
                                            // Get domain
                                            domain = ValidationHelper.GetString(drAlias["SiteDomainAliasName"], "");
                                            if (!string.IsNullOrEmpty(domain))
                                            {
                                                domain = GetDomain(domain);
                                                // Add to file
                                                if (!allSites.Contains(domain))
                                                {
                                                    sw.WriteLine(domain);
                                                    allSites.Add(domain);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Output
                string location = UrlHelper.ResolveUrl(ImportExportHelper.EXPORT_RELATIVE_PATH + fileName);
                string link = "<a href=\"" + location + "\" target=\"_blank\">" + ResHelper.GetString("license.export.download") + "</a>" ;

                lblInfo.Text = string.Format(ResHelper.GetString("license.export.exported"), location) + "<br /><br />" + link;
                lblInfo.Visible = true;
                plcTextBox.Visible = false;
            }
            catch(Exception ex)
            {                
                lblError.Visible = true;
                lblError.Text = ex.Message;
            }
        } 
        
    }

    #endregion


    #region "Private methods" 

    /// <summary>
    /// Returns domain without www, protocol, port and appliaction path.
    /// </summary>
    /// <param name="domain"></param>
    /// <returns></returns>
    private string GetDomain(string domain)
    {
        // Trim to domain
        domain = UrlHelper.RemovePortFromURL(domain);
        domain = UrlHelper.RemoveProtocol(domain);
        domain = UrlHelper.RemoveWWW(domain);

        // Virtual directory
        int slash = domain.IndexOf('/') ;
        if(slash > -1)
        {
            domain = domain.Substring(0,slash);
        }

        return domain;
    }

    #endregion
}
