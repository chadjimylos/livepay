<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Analytics_ManageData.aspx.cs"
    Inherits="CMSModules_WebAnalytics_Tools_Analytics_ManageData" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master"
    Title="Analytics - Delete data" Theme="Default" %>

<asp:Content ID="cntBody" ContentPlaceHolderID="plcContent" runat="server">
    <div class="PageContent">
        <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
        <div style="height: 120px; width: 100%; overflow: auto;">
            <table>
                <tr>
                    <td>
                        <cms:LocalizedLabel ID="lblFrom" runat="server" EnableViewState="false" ResourceString="AnalyticsManageData.FromDate" />
                    </td>
                    <td>
                        <cms:DateTimePicker ID="pickerFrom" runat="server" EditTime="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <cms:LocalizedLabel ID="lblTo" runat="server" EnableViewState="false" ResourceString="AnalyticsManageData.ToDate" />
                    </td>
                    <td>
                        <cms:DateTimePicker ID="pickerTo" runat="server" EditTime="false" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:Literal ID="ltlScript" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:LocalizedButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" CssClass="SubmitButton"
            EnableViewState="false" ResourceString="general.delete" />
        <cms:LocalizedButton ID="btnCancel" runat="server" OnClientClick="window.close(); return false;"
            CssClass="SubmitButton" EnableViewState="false" ResourceString="general.cancel" />
    </div>
</asp:Content>
