using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Security.Principal;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.EmailEngine;
using CMS.UIControls;
using CMS.ExtendedControls;

public partial class CMSModules_EmailQueue_SentEmails : SiteManagerPage
{
    #region "Variables"

    protected int siteId = 0;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = ResHelper.GetString("emailqueue.archive.title");
        this.btnFilter.Text = ResHelper.GetString("General.Show");
        siteId = QueryHelper.GetInteger("siteid", -1);

        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        InitializeActionMenu();

        // Load drop-down lists
        if (!RequestHelper.IsPostBack())
        {
            if (drpPriority.Items.Count <= 0)
            {
                drpPriority.Items.Add(new ListItem(ResHelper.GetString("general.all"), "-1"));
                drpPriority.Items.Add(new ListItem(ResHelper.GetString("emailpriority.low"), Convert.ToString((int)EmailPriorityEnum.Low)));
                drpPriority.Items.Add(new ListItem(ResHelper.GetString("emailpriority.normal"), Convert.ToString((int)EmailPriorityEnum.Normal)));
                drpPriority.Items.Add(new ListItem(ResHelper.GetString("emailpriority.high"), Convert.ToString((int)EmailPriorityEnum.High)));

                InitFilterDropDown(drpFrom);
                InitFilterDropDown(drpTo);
                InitFilterDropDown(drpSubject);
                InitFilterDropDown(drpBody);
            }

            btnShowFilter.ResourceString = "emailqueue.displayfilter";
            imgShowFilter.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/SortDown.png");
        }

        gridElem.WhereCondition = GetWhereCondition();

        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        // Register script for modal dialog
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "displayModal", ScriptHelper.GetScript(
            "function DisplayRecipients(emailId) { \n" +
            "    if ( emailId != 0 ) { \n" +
            "       modalDialog('MassEmails_Recipients.aspx?emailid=' + emailId, 'emailrecipients', 920, 700); \n" +
            "    } } \n "));

        btnShowFilter.Click += new EventHandler(btnShowFilter_Click);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Enable/disable actions according to unigrid content
        InitializeActionMenu();
        CurrentMaster.HeaderActions.ReloadData();

        if (!DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource))
        {
            // Store e-mails' IDs to session for e-mail detail viewer
            SetEmailIds();
        }
    }

    #endregion


    #region "Button events"

    /// <summary>
    /// Show/Hide filter button click.
    /// </summary>
    void btnShowFilter_Click(object sender, EventArgs e)
    {
        // If filter is displayed then hide it
        if (plcFilter.Visible)
        {
            plcFilter.Visible = false;
            btnShowFilter.ResourceString = "emailqueue.displayfilter";
            imgShowFilter.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/SortDown.png");
        }
        else
        {
            plcFilter.Visible = true;
            btnShowFilter.ResourceString = "emailqueue.hidefilter";
            imgShowFilter.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/SortUp.png");
        }
    }


    /// <summary>
    /// Filter button clicked.
    /// </summary>
    protected void btnFilter_Clicked(object sender, EventArgs e)
    {
        gridElem.WhereCondition = GetWhereCondition();
        gridElem.ReloadData();
    }

    #endregion


    #region "Unigrid events"

    /// <summary>
    /// Handles Unigrid's OnExternalDataBound event.
    /// </summary>
    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "subject":
                return HTMLHelper.HTMLEncode(parameter.ToString());

            case "priority":
                switch ((EmailPriorityEnum)parameter)
                {
                    case EmailPriorityEnum.Low:
                        return ResHelper.GetString("emailpriority.low");
                    case EmailPriorityEnum.Normal:
                        return ResHelper.GetString("emailpriority.normal");
                    case EmailPriorityEnum.High:
                        return ResHelper.GetString("emailpriority.high");
                }
                break;

            case "emailto":
                DataRowView dr = (DataRowView)parameter;
                if (ValidationHelper.GetBoolean(dr["EmailIsMass"], false))
                {
                    int id = ValidationHelper.GetInteger(dr["EmailID"], 0);
                    return "<a href=\"#\" onclick=\"javascript: DisplayRecipients(" + id + "); return false; \">"
                    + ResHelper.GetString("emailqueue.queue.massdetails") + "</a>";
                }
                else
                {
                    return HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["EmailTo"], String.Empty));
                }
            case "edit":

                string caption = ResHelper.GetString("Unigrid.EventLog.Actions.Display");
                string imageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/View.png");
                string url = ResolveUrl("~/CMSModules/EmailQueue/EmailQueue_Details.aspx");
                string whereCondition = HttpUtility.UrlPathEncode(this.gridElem.WhereCondition).Replace("&", "%26").Replace("#", "%23").Replace("'", "\'");

                ImageButton viewBtn = (ImageButton)sender;

                url = UrlHelper.AddParameterToUrl(url, "orderby", this.gridElem.SortDirect);
                url = UrlHelper.AddParameterToUrl(url, "where", whereCondition);
                url = UrlHelper.AddParameterToUrl(url, "hash", QueryHelper.GetHash(url, false));
                url = UrlHelper.AddParameterToUrl(url, "emailid", Convert.ToString(viewBtn.CommandArgument));

                string action = "modalDialog(" + ScriptHelper.GetString(url) + ", 'emaildetails', 920, 700);";
                viewBtn.OnClientClick = action;

                break;
        }

        return null;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that threw event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "delete":
                EmailInfoProvider.DeleteEmailInfo(ValidationHelper.GetInteger(actionArgument, 0));
                break;

            case "resend":
                // Resend email info object from queue
                ThreadSender ts = new ThreadSender();
                ts.EmailId = ValidationHelper.GetInteger(actionArgument, -1);
                ts.RunAsync(WindowsIdentity.GetCurrent());
                break;
        }
    }

    #endregion


    #region "Header actions events"

    protected void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        bool reloaded = false;
        switch (e.CommandName.ToLower())
        {
            case "deleteall":
                EmailInfoProvider.DeleteAll(siteId, EmailStatusEnum.Archived, false);
                gridElem.ReloadData();
                reloaded = true;
                break;

            case "deleteselected":
                // Get list of selected e-mails
                ArrayList list = gridElem.SelectedItems;
                if (list.Count > 0)
                {
                    foreach (string emailId in list)
                    {
                        // Delete specific e-mail
                        EmailInfoProvider.DeleteEmailInfo(ValidationHelper.GetInteger(emailId, 0));
                    }
                    gridElem.ResetSelection();
                    gridElem.ReloadData();
                    reloaded = true;
                }
                break;

            case "refresh":
                gridElem.ReloadData();
                break;
        }
        // Reload on first page if no data found after perfoming action
        if (reloaded && DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource))
        {
            gridElem.Pager.UniPager.CurrentPage = 1;
            gridElem.ReloadData();
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes action menu in master page.
    /// </summary>
    private void InitializeActionMenu()
    {
        bool enabled = !DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource);

        // Resend all failed
        string[,] actions = new string[5, 10];
        actions[0, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[0, 1] = ResHelper.GetString("emailqueue.queue.delete");
        if (enabled)
        {
            actions[0, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EmailQueue.DeleteAllConfirmation")) + ")) return false;";
        }
        actions[0, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/deleteall.png");
        actions[0, 6] = "deleteall";
        actions[0, 9] = enabled.ToString();

        // Resend all
        actions[1, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[1, 1] = ResHelper.GetString("emailqueue.queue.deleteselected");
        if (enabled)
        {
            actions[1, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EmailQueue.DeleteSelectedConfirmation")) + ")) return false;";
        }
        actions[1, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/deleteselected.png");
        actions[1, 6] = "deleteselected";
        actions[1, 9] = enabled.ToString();

        // Refresh
        actions[2, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[2, 1] = ResHelper.GetString("general.refresh");
        actions[2, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/refresh.png");
        actions[2, 6] = "refresh";

        CurrentMaster.HeaderActions.Actions = actions;
        CurrentMaster.HeaderActions.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);
    }


    /// <summary>
    /// Sets session with all emails' IDs.
    /// </summary>
    protected void SetEmailIds()
    {
        ArrayList idArray = null;
        if (!DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource))
        {
            idArray = new ArrayList();
            // Get data source and sort it
            DataTable dt = DataHelper.GetDataTable(gridElem.GridView.DataSource);

            foreach (DataRow dr in dt.DefaultView.Table.Rows)
            {
                // Add all event ids to 
                idArray.Add(dr["EmailID"]);
            }
        }
        // Set session with array of IDs
        SessionHelper.SetValue("emailIds", idArray);
    }

    #endregion


    #region "Filter methods"

    /// <summary>
    /// Initializes standard filter dropdown
    /// </summary>
    /// <param name="drp">Dropdown to init</param>
    private void InitFilterDropDown(DropDownList drp)
    {
        if ((drp != null) && (drp.Items.Count <= 0))
        {
            drp.Items.Add(new ListItem("LIKE", "LIKE"));
            drp.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
            drp.Items.Add(new ListItem("=", "="));
            drp.Items.Add(new ListItem("<>", "<>"));
        }
    }


    /// <summary>
    /// Appends WHERE condition for specified column at the end of current/existing WHERE condition.
    /// </summary>
    /// <param name="whereCond">Current WHERE condition</param>
    /// <param name="column">Column name</param>
    /// <param name="value">Value to be searched (if empty no condition is created)</param>
    /// <param name="op">Operator to condition</param>
    private void BuildWhereCond(ref string whereCond, string column, string value, string op)
    {
        // Avoid SQL injenction
        string tempVal = value.Trim().Replace("'", "''");

        // No condition
        if (tempVal == "")
        {
            return;
        }

        // Support for exact phrase search
        if (op.Contains("LIKE"))
        {
            tempVal = "%" + tempVal + "%";
        }

        string where = "(" + column + " " + op + " N'" + tempVal + "')";
        if (!String.IsNullOrEmpty(whereCond))
        {
            where = " AND " + where;
        }

        // Get final where condition
        whereCond += where;
    }


    /// <summary>
    /// Return WHERE condition.
    /// </summary>
    protected string GetWhereCondition()
    {
        string where = "";

        BuildWhereCond(ref where, "EmailFrom", txtFrom.Text, drpFrom.SelectedValue);
        BuildWhereCond(ref where, "EmailSubject", txtSubject.Text, drpSubject.SelectedValue);
        BuildWhereCond(ref where, "EmailBody", txtBody.Text, drpBody.SelectedValue);

        // EmailTo condition
        if (!String.IsNullOrEmpty(txtTo.Text.Trim()))
        {
            if (!String.IsNullOrEmpty(where))
            {
                where += " AND ";
            }
            string toText = txtTo.Text.Trim().Replace("'", "''");
            string op = drpTo.SelectedValue;
            if (op.Contains("LIKE"))
            {
                toText = "%" + toText + "%";
            }
            toText = " N'" + toText + "'";
            string combineOp = " OR ";
            if ((op == "<>") || op.Contains("NOT"))
            {
                combineOp = " AND ";
            }
            where += "(EmailTo " + op + toText + combineOp +
                      "EmailCc " + op + toText + combineOp +
                      "EmailBcc " + op + toText + ")";
        }

        // Condition for priority
        int priority = ValidationHelper.GetInteger(drpPriority.SelectedValue, -1);
        if (priority >= 0)
        {
            if (!String.IsNullOrEmpty(where))
            {
                where += " AND ";
            }
            where += "EmailPriority=" + drpPriority.SelectedValue;
        }


        // Condition for site (and sent/archived e-mails)
        if (!String.IsNullOrEmpty(where))
        {
            where += " AND ";
        }

        where += "(EmailStatus = " + Convert.ToString((int)EmailStatusEnum.Archived) + ")";

        if (siteId == 0)      // Global
        {
            where += " AND (EmailSiteID IS NULL OR  EmailSiteID = 0)";
        }
        else if (siteId > 0)
        {
            where += " AND (EmailSiteID = " + siteId.ToString() + ")";
        }

        return where;
    }

    #endregion
}
