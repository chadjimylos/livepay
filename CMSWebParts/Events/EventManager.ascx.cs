using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.TreeEngine;
using CMS.EventManager;
using CMS.EventLog;
using CMS.EmailEngine;
using CMS.SettingsProvider;
using CMS.SiteProvider;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;
using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSWebParts_Events_EventManager : CMSAbstractWebPart
{
    protected DateTime eventDate = DataHelper.DATETIME_NOT_SELECTED;
    protected DateTime openFrom = DataHelper.DATETIME_NOT_SELECTED;
    protected DateTime openTo = DataHelper.DATETIME_NOT_SELECTED;
    protected int capacity = 0;
    protected bool allowRegistrationOverCapacity = false;
    protected bool errorOccurs = false;
    protected TreeNode mEventNode = null;

    protected TreeNode EventNode
    {
        get
        {
        	 return mEventNode; 
        }
        set
        {
        	 mEventNode = value; 
        }
    }


    #region "Public properties"

    /// <summary>
    ///  Gets or sets the value that indicates whether first and last user name are required
    /// </summary>
    public bool RequireName
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("RequireName"), true);
        }
        set
        {
            this.SetValue("RequireName", value);
        }
    }


    /// <summary>
    ///  Gets or sets the value that indicates whether phone number is required for registration
    /// </summary>
    public bool RequirePhone
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("RequirePhone"), true);
        }
        set
        {
            this.SetValue("RequirePhone", value);
        }
    }


    /// <summary>
    ///  Gets or sets the value that indicates whether link to *.ics file will be available after registration
    /// </summary>
    public bool AllowExportToOutlook
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("AllowExportToOutlook"), true);
        }
        set
        {
            this.SetValue("AllowExportToOutlook", value);
        }
    }


    /// <summary>
    ///  Gets or sets the value that indicates whether public users are allowed to register
    /// </summary>
    public bool AllowAnonymousRegistration
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("AllowAnonymousRegistration"), true);
        }
        set
        {
            this.SetValue("AllowAnonymousRegistration", value);
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing || CMSContext.CurrentDocument == null || CMSContext.CurrentDocument.NodeClassName.ToLower() != "cms.bookingevent")
        {
            // Do nothing
            this.Visible = false;
        }
        else
        {
            // Get current event document
            EventNode = CMSContext.CurrentDocument;

            // Get event date, open from/to, capacity and possibility to register over capacity information
            eventDate = ValidationHelper.GetDateTime(DataHelper.GetDataRowValue(EventNode.DataRow, "EventDate"), DataHelper.DATETIME_NOT_SELECTED);
            openFrom = ValidationHelper.GetDateTime(DataHelper.GetDataRowValue(EventNode.DataRow, "EventOpenFrom"), DataHelper.DATETIME_NOT_SELECTED);
            openTo = ValidationHelper.GetDateTime(DataHelper.GetDataRowValue(EventNode.DataRow, "EventOpenTo"), DataHelper.DATETIME_NOT_SELECTED);
            capacity = ValidationHelper.GetInteger(DataHelper.GetDataRowValue(EventNode.DataRow, "EventCapacity"), 0);
            allowRegistrationOverCapacity = ValidationHelper.GetBoolean(DataHelper.GetDataRowValue(EventNode.DataRow, "EventAllowRegistrationOverCapacity"), false);

            // Display registration section
            DisplayRegistration();

            // Display link to iCalendar file which adds this event to users Outlook
            if (this.AllowExportToOutlook)
            {
                TimeZoneInfo tzi = null;
                CMSContext.GetDateTimeForControl(this, DateTime.Now, out tzi);
                string timeZoneId = "";
                if (tzi != null)
                {
                    timeZoneId = "&timezoneid=" + tzi.TimeZoneID;
                }

                lnkOutlook.NavigateUrl = "~/CMSModules/EventManager/CMSPages/AddToOutlook.aspx?eventid=" + EventNode.NodeID.ToString() + timeZoneId;
                lnkOutlook.Target = "_blank";
                lnkOutlook.Text = ResHelper.GetString("eventmanager.exporttooutlook");
                lnkOutlook.Visible = true;
            }
        }
    }


    /// <summary>
    /// Displays registration section depending on situation
    /// </summary>
    protected void DisplayRegistration()
    {
        lblRegTitle.Text = ResHelper.GetString("eventmanager.registrationtitle");

        CurrentUserInfo userInfo = CMSContext.CurrentUser;
        // Display registration form to anonymous user only if this is allowed
        if ((this.AllowAnonymousRegistration || (userInfo != null && userInfo.IsAuthenticated())) && EventNode != null)
        {
            DateTime now = DateTime.Now;
            // Display registration form if opened
            if ((openFrom == DataHelper.DATETIME_NOT_SELECTED || openFrom < now) && (openTo == DataHelper.DATETIME_NOT_SELECTED || now <= openTo) && (now <= eventDate))
            {
                int actualCount = EventAttendeeInfoProvider.GetEventAttendeesCount(EventNode.OriginalNodeID);
                // Display registration form if capacity is not full
                if (actualCount == 0 || capacity == 0 || actualCount < capacity || allowRegistrationOverCapacity)
                {
                    // Preset fields with info of authenticated user
                    if (userInfo != null && userInfo.IsAuthenticated() && !RequestHelper.IsPostBack())
                    {
                        txtFirstName.Text = userInfo.FirstName;
                        txtLastName.Text = userInfo.LastName;
                        txtEmail.Text = userInfo.Email;
                    }

                    // Hide non-required fields
                    if (!this.RequireName)
                    {
                        plcName.Visible = false;
                    }
                    if (!this.RequirePhone)
                    {
                        plcPhone.Visible = false;
                    }
                }
                else
                {
                    pnlReg.Visible = false;
                    lblError.Text = ResHelper.GetString("eventmanager.fullcapacity");
                    lblError.Visible = true;
                    errorOccurs = true;
                }
            }
            else
            {
                pnlReg.Visible = false;
                lblError.Text = ResHelper.GetString("eventmanager.notopened");
                lblError.Visible = true;
                errorOccurs = true;
            }
        }
        else
        {
            pnlReg.Visible = false;
            lblError.Text = ResHelper.GetString("eventmanager.notauthenticated");
            lblError.Visible = true;
            errorOccurs = true;
        }
    }


    /// <summary>
    /// On btnRegister click.
    /// </summary>
    protected void btnRegister_Click(object sender, EventArgs e)
    {
        // Check banned ip
        if (!BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.AllNonComplete))
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("General.BannedIP");
            return;
        }

        // Exit if problem occurs
        if (errorOccurs)
        {
            return;
        }

        string result = "";
        Validator val = new Validator();
        // Check name fields if required
        if (this.RequireName)
        {
            result = val.NotEmpty(txtFirstName.Text, ResHelper.GetString("eventmanager.firstnamerequired"))
                .NotEmpty(txtLastName.Text, ResHelper.GetString("eventmanager.lastnamerequired")).Result;
        }
        // Check e-mail field
        if (result == "")
        {
            result = val.IsEmail(txtEmail.Text.Trim(), ResHelper.GetString("eventmanager.emailrequired")).Result;
        }
        // Check phone field if required
        if (this.RequirePhone && result == "")
        {
            result = val.NotEmpty(txtPhone.Text, ResHelper.GetString("eventmanager.phonerequired")).Result;
        }

        if (result == "")
        {
            DateTime now = DateTime.Now;
            // Allow registration if opened
            if ((openFrom == DataHelper.DATETIME_NOT_SELECTED || openFrom < now) && (openTo == DataHelper.DATETIME_NOT_SELECTED || now <= openTo) && (now <= eventDate))
            {
                if (EventNode != null)
                {
                    if (!EventAttendeeInfoProvider.IsRegisteredForEvent(EventNode.NodeID, txtEmail.Text.Trim()))
                    {
                        // Add new attendant to the event
                        EventAttendeeInfo eai = AddAttendantToEvent();

                        if (eai != null)
                        {
                            // Send invitation e-mail
                            TimeZoneInfo tzi = null;
                            CMSContext.GetDateTimeForControl(this, DateTime.Now, out tzi);
                            EventProvider.SendInvitation(CMSContext.CurrentSiteName, EventNode.DataRow, eai.DataClass.DataRow, tzi);

                            lblRegInfo.Text = ResHelper.GetString("eventmanager.registrationsucceeded");
                            lblRegInfo.Visible = true;
                            // Hide registration form
                            pnlReg.Visible = false;
                        }
                    }
                    else
                    {
                        // User is already registered
                        lblError.Text = ResHelper.GetString("eventmanager.attendeeregistered");
                        lblError.Visible = true;
                    }
                }
                else
                {
                    // Event does not exist
                    lblError.Text = ResHelper.GetString("eventmanager.eventnotexist");
                    lblError.Visible = true;
                    // Hide registration form
                    pnlReg.Visible = false;
                }
            }
            else
            {
                // Event registration is not opened
                lblError.Text = ResHelper.GetString("eventmanager.notopened");
                lblError.Visible = true;
                // Hide registration form
                pnlReg.Visible = false;
            }
        }
        else
        {
            // Display error message
            lblError.Text = result;
            lblError.Visible = true;
        }
    }


    /// <summary>
    /// Add new attendant to the event.
    /// </summary>
    /// <returns>Newly created attendee</returns>
    private EventAttendeeInfo AddAttendantToEvent()
    {
        EventAttendeeInfo attendeeInfo = null;

        if (EventNode != null)
        {
            attendeeInfo = new EventAttendeeInfo();

            attendeeInfo.AttendeeEventNodeID = EventNode.OriginalNodeID;
            attendeeInfo.AttendeeEmail = txtEmail.Text.Trim();
            if (this.RequireName)
            {
                attendeeInfo.AttendeeFirstName = txtFirstName.Text;
                attendeeInfo.AttendeeLastName = txtLastName.Text;
            }
            if (this.RequirePhone)
            {
                attendeeInfo.AttendeePhone = txtPhone.Text;
            }

            // Add new attendant to the event
            EventAttendeeInfoProvider.SetEventAttendeeInfo(attendeeInfo);
        }

        return attendeeInfo;
    }
}
