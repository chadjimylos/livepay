Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI

Imports CMS.CMSHelper
Imports CMS.GlobalHelper
Imports CMS.PortalControls

Imports CMS.SiteProvider
Imports CMS.SettingsProvider
Imports CMS.DataEngine

Partial Public Class CMSWebParts_AegeanPower_NewsLetter
    Inherits CMSAbstractWebPart
#Region "Properties"

    ''' <summary>
    ''' Gets or sets value of NewsLetterText
    ''' </summary>
    Public Property NewsLetterText() As String
        Get
            Return ValidationHelper.GetString(Me.GetValue("NewsLetterText"), "")
        End Get
        Set(ByVal value As String)
            Me.SetValue("NewsLetterText", value)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets value of NewsLetterMsg
    ''' </summary>
    Public Property NewsLetterMsg() As String
        Get
            Return ValidationHelper.GetString(Me.GetValue("NewsLetterMsg"), "")
        End Get
        Set(ByVal value As String)
            Me.SetValue("NewsLetterMsg", value)
        End Set
    End Property

#End Region


#Region "Methods"

    ''' <summary>
    ''' Content loaded event handler
    ''' </summary>
    Public Overloads Overrides Sub OnContentLoaded()
        MyBase.OnContentLoaded()
        SetupControl()
    End Sub


    ''' <summary>
    ''' Initializes the control properties
    ''' </summary>
    Protected Sub SetupControl()
        If Me.StopProcessing Then
            ' Do nothing
        Else
            ' Set WebPart properties
            lbNewsLetterText.Text = Me.NewsLetterText
            lbNewsLetterMsg.Text = Me.NewsLetterMsg
        End If
    End Sub


    ''' <summary>
    ''' Reload data
    ''' </summary>
    Public Overloads Overrides Sub ReloadData()
        MyBase.ReloadData()

        ' Reload data in all controls if needed
        ' this.MyControl.ReloadData();
    End Sub

#End Region

    Protected Sub Submit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles submit.Click
        Page.Validate()

        For Each v As IValidator In Me.Page.Validators

            If TypeOf v Is RegularExpressionValidator Then

                If Not v.IsValid Then
                    Dim validator As RegularExpressionValidator = CType(v, RegularExpressionValidator)
                    Dim control As TextBox = CType(Me.FindControl(validator.ControlToValidate), TextBox)
                    control.Text = Server.HtmlEncode(control.Text)
                End If
            End If
        Next

        'Me.RegisterAlert(Page.IsValid & " - " & Me.HasProducts())
        For Each v As IValidator In Me.Page.Validators
            If Not v.IsValid Then
                If TypeOf v Is RequiredFieldValidator Then
                    Dim validator As RequiredFieldValidator = CType(v, RequiredFieldValidator)
                    If validator.ValidationGroup = "OnlineHelpForm" Then
                        v.IsValid = True
                    End If
                    'v.IsValid = True
                End If
            End If
        Next

        If Page.IsValid Then
            If SaveNewsLetter() Then
                DivNewsletterContent.Visible = False
                DivNewsletterContentSubmit.Visible = True
                ltlScript.Text = ("<script>$('#NewsLetter').animate({ left: 0 }, 1000);</script>")
            End If

        Else
            lbNewsLetterText.Visible = False
        End If
    End Sub

    Private Function SaveNewsLetter() As Boolean
        Dim saved As Boolean = False
            Try
                ' Get data class using custom table name
                Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass("AegeanPower.NewsLetter")
                If customTableClassInfo Is Nothing Then
                    Throw New Exception("Given custom table does not exist.")
                End If

                ' Initialize custom table item provider with current user info and general connection
                Dim ctiProvider As CustomTableItemProvider = New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())

                ' Create new custom table item for given class of custom table
                Dim item As CustomTableItem = New CustomTableItem(customTableClassInfo.ClassName, ctiProvider)

                '' Set value of a custom table item field
                item.SetValue("Email", tbEmail.Text)
                item.Insert()

                saved = True
            Catch ex As Exception
                'ltrError.Text = ex.Message
                Throw
            End Try

            Return saved
    End Function
End Class

