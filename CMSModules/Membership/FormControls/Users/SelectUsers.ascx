<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectUsers.ascx.cs"
    Inherits="CMSModules_Membership_FormControls_Users_SelectUsers" %>

<%@ Register Src="~/CMSModules/Membership/FormControls/Users/selectuser.ascx" TagName="SelectUser" TagPrefix="cms" %>

<cms:SelectUser ID="selectUser" runat="server" SelectionMode="MultipleTextBox" />
