SET IDENTITY_INSERT [CMS_UICulture] ON
INSERT INTO [CMS_UICulture] ([UICultureID], [UICultureCode], [UICultureLastModified], [UICultureName], [UICultureGUID]) VALUES (23, N'cs-cz', '20091121 16:56:01', N'Czech', '0f367ea3-255c-4a76-897d-73659c789bb0')
INSERT INTO [CMS_UICulture] ([UICultureID], [UICultureCode], [UICultureLastModified], [UICultureName], [UICultureGUID]) VALUES (11, N'en-us', '20080820 13:52:32', N'English', '356a7bb3-5b11-4d83-bbc4-964aafd6d60a')
INSERT INTO [CMS_UICulture] ([UICultureID], [UICultureCode], [UICultureLastModified], [UICultureName], [UICultureGUID]) VALUES (34, N'sk-sk', '20080304 11:26:26', N'Slovak', 'c7b1d6df-7e4e-44fa-9749-06854244bb89')
SET IDENTITY_INSERT [CMS_UICulture] OFF
