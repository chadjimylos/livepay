using System;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Staging;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;

public partial class CMSModules_Content_CMSDesk_Properties_Categories : CMSPropertiesPage
{
    protected string mSave = null;
    protected bool hasModifyPermission = true;
    protected TreeProvider tree = null;
    protected TreeNode node = null;


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.Categories"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.Categories");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the scripts
        ScriptHelper.RegisterProgress(this.Page);
        
        UIContext.PropertyTab = PropertyTabEnum.Categories;

        // UI settings
        lblCategoryInfo.Text = ResHelper.GetString("Categories.DocumentAssignedTo");
        categoriesElem.HideOKButton = true;
        categoriesElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
        categoriesElem.OnAfterSave += categoriesElem_OnAfterSave;
        imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        imgSave.DisabledImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/savedisabled.png");
        mSave = ResHelper.GetString("general.save");

        int nodeId = QueryHelper.GetInteger("nodeid", 0);
        if (nodeId > 0)
        {
            tree = new TreeProvider(CMSContext.CurrentUser);
            node = tree.SelectSingleNode(nodeId, CMSContext.PreferredCultureCode);
            if (node != null)
            {
                // Check read permissions
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                {
                    RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
                }
                // Check modify permissions
                else if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                {
                    hasModifyPermission = false;
                    pnlUserCatgerories.Enabled = false;

                    // disable 'save button'
                    lnkSave.Enabled = false;
                    lnkSave.CssClass = "MenuItemEditDisabled";
                    imgSave.Enabled = false; 

                    lblCategoryInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                    pnlContent.Visible = true;
                }
                // Display all global categories in administration UI
                categoriesElem.UserID = CMSContext.CurrentUser.UserID;
                categoriesElem.DocumentID = node.DocumentID;
            }
        }
    }


    void categoriesElem_OnAfterSave()
    {
        if (hasModifyPermission)
        {
            // Log the synchronization
            DocumentHelper.LogSynchronization(node, TaskTypeEnum.UpdateDocument, tree);
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (categoriesElem.Editing)
        {
            pnlMenu.Visible = false;
        }
        else
        {
            pnlMenu.Visible = true;
            if (hasModifyPermission)
            {
                // Shortcut script
                ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
                            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>" +
                            ScriptHelper.GetScript("function SaveDocument() { " + ClientScript.GetPostBackEventReference(lnkSave, null) + " } \n"
                        ));
            }
        }
    }
    

    protected void lnkSave_Click(object sender, EventArgs e)
    {
        if (hasModifyPermission)
        {
            categoriesElem.Save();
        }
    }
}
