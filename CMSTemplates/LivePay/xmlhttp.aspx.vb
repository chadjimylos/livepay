﻿Imports System.Data

Partial Class CMSTemplates_LivePay_xmlhttp
    Inherits System.Web.UI.Page

    Protected ReadOnly Property MerchantID() As Integer
        Get
            Dim iMerchantID As Integer = 1
            If Not String.IsNullOrEmpty(Request("MerchantID")) Then
                iMerchantID = Request("MerchantID")
            End If
            Return iMerchantID
        End Get
    End Property

    Protected ReadOnly Property UserCardID() As Integer
        Get
            Dim iUserCardID As Integer = 1
            If Not String.IsNullOrEmpty(Request("UserCardID")) Then
                iUserCardID = Request("UserCardID")
            End If
            Return iUserCardID
        End Get
    End Property

    Protected ReadOnly Property CardNo() As String
        Get
            Dim _CardNo As String = String.Empty
            If Not String.IsNullOrEmpty(Request("CardNo")) Then
                _CardNo = Request("CardNo")
            End If
            Return _CardNo
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UserNewCard As String = Me.CardNo
        Dim EncryptedCard As String = Left(UserNewCard, 4) & CryptoFunctions.EncryptString(Right(UserNewCard, 12))
        If UserNewCard = String.Empty Then
            Dim DtCard As DataTable = DBConnection.GetUserCard(CMSContext.CurrentUser.UserID, Me.UserCardID)
            If DtCard.Rows.Count > 0 Then
                EncryptedCard = DtCard.Rows(0)("CardNumber").ToString
            End If
        End If

        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = DBConnection.GetMerchantTransaction(UserID, Me.MerchantID, Me.UserCardID, EncryptedCard, 2, True)
        Dim dtCards As DataTable = DBConnection.GetMerchantCardType(Me.UserCardID, UserNewCard)

        Dim CardLimitID As String = String.Empty
        Dim CardsJson As StringBuilder = New StringBuilder()
        If dtCards.Rows.Count > 0 Then
            If UserNewCard <> String.Empty Then '---- New Card
                CheckCardBinRage(dtCards, CardLimitID, CardsJson, UserNewCard)
            Else
                Dim DecryptedCard As String = Left(EncryptedCard, 4) & CryptoFunctions.DecryptString(Right(EncryptedCard, EncryptedCard.Length - 4))
                CheckCardBinRage(dtCards, CardLimitID, CardsJson, DecryptedCard)
            End If
        End If
        If dt.Rows.Count > 0 Then

            If UserNewCard <> String.Empty Then
                Response.Write(String.Concat("!", dt.Rows(0)("TransCountPerMerchantPerCard").ToString, "@>", "!", dt.Rows(0)("TransCountPerMerchant").ToString, "@>", "!", dt.Rows(0)("TransSumAmountPerCardPerMerchant").ToString, "@>", "!", String.Format("'{{""cards"" :{0}}}'", String.Concat("[", CardsJson.ToString(), "]")), "@>!", IsValidCard(dt.Rows(0)("CardNo").ToString), "@>!", dt.Rows(0)("CardNoIsSaved").ToString, "@"))
            Else
                If CardLimitID.Length > 0 Then
                    Response.Write(String.Concat("!", dt.Rows(0)("TransCountPerMerchantPerCard").ToString, "@>", "!", dt.Rows(0)("TransCountPerMerchant").ToString, "@>", "!", dt.Rows(0)("TransSumAmountPerCardPerMerchant").ToString, "@>", "!", String.Format("'{{""cards"" :{0}}}'", String.Concat("[", CardsJson.ToString(), "]")), "@>!True@>!", dt.Rows(0)("CardNoIsSaved").ToString, "@"))
                Else
                    Response.Write(String.Concat("!", dt.Rows(0)("TransCountPerMerchantPerCard").ToString, "@>", "!", dt.Rows(0)("TransCountPerMerchant").ToString, "@>", "!", dt.Rows(0)("TransSumAmountPerCardPerMerchant").ToString, "@>", "!@>!True@>!", dt.Rows(0)("CardNoIsSaved").ToString, "@"))
                End If

            End If

        End If
    End Sub

    Public Function IsValidCard(ByVal number As String) As Boolean


        If IsNumeric(Left(number, 4)) Then


            Dim DicreptedCard As String = Left(number, 4) & CryptoFunctions.DecryptString(Right(number, (number.ToString.Length - 4)))
            number = DicreptedCard

            Dim Digits() As Integer = Array.ConvertAll(number.ToCharArray(), New Converter(Of Char, Integer)(AddressOf CharToDigit))
            Array.Reverse(Digits)

            Dim Sum As Integer = 0
            Dim Flag As Boolean

            For Each digit As Integer In Digits
                If Flag Then
                    digit *= 2
                    If digit > 9 Then digit -= 9
                End If

                Sum += digit
                Flag = Not Flag
            Next

            Return (Sum Mod 10 = 0)
        Else
            Return False
        End If
    End Function

    Private Function CharToDigit(ByVal ch As Char) As Integer
        Return Convert.ToInt32(ch.ToString())
    End Function

    Private Sub CheckCardBinRage(dtCards As DataTable, ByRef CardLimitID As String, ByRef CardsJson As StringBuilder, UserCard As String)
        Dim MaxBinds As Integer = 0
        For i As Integer = 0 To dtCards.Rows.Count - 1
            Dim Limits As String = dtCards.Rows(i)("Limit").ToString
            If Limits <> String.Empty Then
                Dim LimitLength As Integer = Split(Limits, ",").Length - 1
                For iCount As Integer = 0 To LimitLength
                    Dim CardLimitNo As String = Split(Limits, ",")(iCount)
                    If Not String.IsNullOrEmpty(CardLimitNo) AndAlso UserCard.StartsWith(CardLimitNo) Then
                        If CardLimitNo.Trim.Length > MaxBinds Then
                            MaxBinds = CardLimitNo.Trim.Length
                        End If

                        CardLimitID &= dtCards.Rows(i)("itemid").ToString & "|"
                        If CardsJson.Length > 0 Then CardsJson.Append(", ")

                        CardsJson.AppendFormat("{{""id"":{0}, ""binds"":{1}}}", dtCards.Rows(i)("itemid").ToString, CardLimitNo.Trim.Length)

                    End If
                Next
            End If
        Next
    End Sub

End Class
