<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Polls_General.aspx.cs" Inherits="CMSModules_Polls_Tools_Polls_General"
    Title="Poll properties" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Theme="Default" %>

<%@ Register Src="~/CMSModules/Polls/Controls/PollProperties.ascx" TagName="PollProperties"
    TagPrefix="cms" %>
    
<asp:Content ID="cntBody" ContentPlaceHolderID="plcContent" runat="server">
    <cms:PollProperties ID="PollProperties" runat="server" Visible="true" /> 
</asp:Content>
