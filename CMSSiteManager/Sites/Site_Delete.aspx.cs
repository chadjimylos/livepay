using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Security.Principal;
using System.IO;
using System.Runtime.Serialization;
using System.Threading;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.WorkflowEngine;
using CMS.UIControls;
using CMS.CMSImportExport;
using CMS.SettingsProvider;
using CMS.FileManager;
using CMS.DirectoryUtilities;
using CMS.EventLog;

using TreeNode = CMS.TreeEngine.TreeNode;
using ProcessStatus = CMS.CMSImportExport.ProcessStatus;

public partial class CMSSiteManager_Sites_Site_Delete : SiteManagerPage, ICallbackEventHandler
{
    #region "Variables"

    private static Hashtable mManagers = new Hashtable();

    // Site ID
    private int siteId = 0;

    // Site name
    private string siteName = "";

    // Site display name
    private string siteDisplayName = "";

    SiteInfo si = null;

    #endregion


    #region "Public properties"
    
    /// <summary>
    /// Deletion manager
    /// </summary>
    public SiteDeletionManager DeletionManager
    {
        get
        {
            string key = "delManagers_" + ProcessGUID;
            if (mManagers[key] == null)
            {
                // Restart of the application
                if (ApplicationInstanceGUID != CMSContext.ApplicationInstanceGUID)
                {
                    LogStatusEnum progressLog = DeletionInfo.GetProgressState();
                    if (progressLog != LogStatusEnum.Finish)
                    {
                        DeletionInfo.LogDeletionState(LogStatusEnum.UnexpectedFinish, ResHelper.GetAPIString("Site_Delete.Applicationrestarted", "<strong>Application has been restarted and the logging of the site delete process has been terminated. Please make sure that the site is deleted. If it is not, please repeate the deletion process.</strong><br />"));
                    }
                }

                SiteDeletionManager dm = new SiteDeletionManager(DeletionInfo);
                mManagers[key] = dm;
            }
            return (SiteDeletionManager)mManagers[key];
        }
        set
        {
            string key = "delManagers_" + ProcessGUID;
            mManagers[key] = value;
        }
    }


    /// <summary>
    /// Application instance GUID
    /// </summary>
    public Guid ApplicationInstanceGUID
    {
        get
        {
            if (ViewState["ApplicationInstanceGUID"] == null)
            {
                ViewState["ApplicationInstanceGUID"] = CMSContext.ApplicationInstanceGUID;
            }

            return ValidationHelper.GetGuid(ViewState["ApplicationInstanceGUID"], Guid.Empty);
        }
    }


    /// <summary>
    /// Import process GUID
    /// </summary>
    public Guid ProcessGUID
    {
        get
        {
            if (ViewState["ProcessGUID"] == null)
            {
                ViewState["ProcessGUID"] = Guid.NewGuid();
            }

            return ValidationHelper.GetGuid(ViewState["ProcessGUID"], Guid.Empty);
        }
    }


    /// <summary>
    /// Persistent settings key
    /// </summary>
    public string PersistentSettingsKey
    {
        get
        {
            return "SiteDeletion_" + ProcessGUID + "_Settings";
        }
    }


    /// <summary>
    /// Deletion info
    /// </summary>
    public DeletionInfo DeletionInfo
    {
        get
        {
            DeletionInfo delInfo = (DeletionInfo)PersistentStorageHelper.GetValue(PersistentSettingsKey);
            if (delInfo == null)
            {
                throw new Exception("[SiteDelete.DeletionInfo]: Deletion info has been lost.");
            }
            return delInfo;
        }
        set
        {
            PersistentStorageHelper.SetValue(PersistentSettingsKey, value);
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register script for pendingCallbacks repair
        ScriptHelper.FixPendingCallbacks(this.Page);

        if (!IsCallback)
        {
            if (!RequestHelper.IsPostBack())
            {
                // Initialize deletion info
                DeletionInfo = new DeletionInfo();
                DeletionInfo.PersistentSettingsKey = PersistentSettingsKey;
            }

            DeletionManager.DeletionInfo = DeletionInfo;

            // Register the script to perform get flags for showing buttons retrieval callback
            ScriptHelper.RegisterClientScriptBlock(this, GetType(), "GetState", ScriptHelper.GetScript("function GetState(cancel){ return " + Page.ClientScript.GetCallbackEventReference(this, "cancel", "SetStateMssg", null) + " } \n"));

            // Setup page title text and image
            CurrentMaster.Title.TitleText = ResHelper.GetString("Site_Edit.DeleteSite");
            CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Sites/deletesite.png");

            //initialize PageTitle
            string[,] pageTitleTabs = new string[2, 3];
            pageTitleTabs[0, 0] = ResHelper.GetString("general.sites");
            pageTitleTabs[0, 1] = "~/CMSSiteManager/Sites/site_list.aspx";
            pageTitleTabs[0, 2] = "cmsdesktop";
            pageTitleTabs[1, 0] = ResHelper.GetString("Site_Edit.DeleteSite");
            pageTitleTabs[1, 1] = "";
            CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
            CurrentMaster.Title.HelpTopicName = "site_deletion";
            CurrentMaster.Title.HelpName = "helpTopic";

            // Get site ID
            siteId = ValidationHelper.GetInteger(Request.QueryString["siteId"], 0);

            si = SiteInfoProvider.GetSiteInfo(siteId);
            if (si != null)
            {
                siteName = si.SiteName;
                siteDisplayName = HTMLHelper.HTMLEncode(si.DisplayName);

                ucHeader.Header = string.Format(ResHelper.GetString("Site_Delete.Header"), siteDisplayName);
                ucHeaderConfirm.Header = ResHelper.GetString("Site_Delete.HeaderConfirm");

                // Initialize web root path
                DeletionInfo.WebRootFullPath = HttpContext.Current.Server.MapPath("~/");

                DeletionInfo.DeletionLog = string.Format("I" + SiteDeletionManager.SEPARATOR + DeletionManager.DeletionInfo.GetAPIString("Site_Delete.DeletingSite", "Initializing deletion of the site") + SiteDeletionManager.SEPARATOR + SiteDeletionManager.SEPARATOR, siteName);

                lblConfirmation.Text = string.Format(ResHelper.GetString("Site_Edit.Confirmation"), siteDisplayName);
                btnYes.Text = ResHelper.GetString("General.Yes");
                btnNo.Text = ResHelper.GetString("General.No");
                btnOk.Text = ResHelper.GetString("General.OK");
                lblLog.Text = string.Format(ResHelper.GetString("Site_Delete.DeletingSite"), siteDisplayName);
            }

            btnYes.Click += btnYes_Click;
            btnNo.Click += btnNo_Click;
            btnOk.Click += btnOK_Click;

            // Javascript functions
            string script =
                        "function SetStateMssg(rValue, context) \n" +
                        "{\n" +
                        "   var values = rValue.split('<#>');\n" +
                        "   if((values[0]=='E') || (values[0]=='F') || values=='')\n" +
                        "   {\n" +
                        "       StopStateTimer();\n" +
                        "       BTN_Enable('" + btnOk.ClientID + "');\n" +
                        "   }\n" +
                        "   if(values[0]=='E')\n" +
                        "   {\n" +
                        "       document.getElementById('" + lblError.ClientID + "').innerHTML = values[2];\n" +
                        "   }\n" +
                        "   else if(values[0]=='I')\n" +
                        "   {\n" +
                        "       document.getElementById('" + lblLog.ClientID + "').innerHTML = values[1];\n" +
                        "   }\n" +
                        "   else if((values=='') || (values[0]=='F'))\n" +
                        "   {\n" +
                        "       document.getElementById('" + lblLog.ClientID + "').innerHTML = values[1];\n" +
                        "   }\n" +
                        "   document.getElementById('" + lblWarning.ClientID + "').innerHTML = values[3];\n" +
                        "}\n";

            // Register the script to perform get flags for showing buttons retrieval callback
            ScriptHelper.RegisterClientScriptBlock(this, GetType(), "GetDeletionState", ScriptHelper.GetScript(script));
        }
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        UrlHelper.Redirect("~/CMSSiteManager/Sites/site_list.aspx");
    }


    protected void btnNo_Click(object sender, EventArgs e)
    {
        UrlHelper.Redirect("~/CMSSiteManager/Sites/site_list.aspx");
    }


    void btnYes_Click(object sender, EventArgs e)
    {
        pnlConfirmation.Visible = false;
        pnlDeleteSite.Visible = true;

        // Start the timer for the callbacks
        ltlScript.Text = ScriptHelper.GetScript("StartStateTimer();");

        // Initilaize web root path
        AttachmentHelper.WebRootFullPath = HttpContext.Current.Server.MapPath("~/");

        // Deletion info initialization
        DeletionInfo.DeleteAttachments = chkDeleteDocumentAttachments.Checked;
        DeletionInfo.DeleteMediaFiles = chkDeleteMediaFiles.Checked;
        DeletionInfo.DeleteMetaFiles = chkDeleteMetaFiles.Checked;
        DeletionInfo.SiteName = siteName;
        DeletionInfo.SiteDisplayName = siteDisplayName;
        DeletionManager.DeletionInfo = DeletionInfo;

        AsyncWorker worker = new AsyncWorker();
        worker.RunAsync(DeletionManager.DeleteSite, WindowsIdentity.GetCurrent());
    }


    /// <summary>
    /// Callback event handler
    /// </summary>
    /// <param name="argument">Callback argument</param>
    public void RaiseCallbackEvent(string argument)
    {
        hdnLog.Value = DeletionManager.DeletionInfo.DeletionLog;
    }


    /// <summary>
    /// Callback result retrieving handler
    /// </summary>
    public string GetCallbackResult()
    {
        return hdnLog.Value;
    }
}


/// <summary>
/// Deletion info
/// </summary>
[Serializable]
public class DeletionInfo : ISerializable
{
    #region "Variables"

    // Deletion log
    private string mDeletionLog = "I" + SiteDeletionManager.SEPARATOR + "" + SiteDeletionManager.SEPARATOR + "" + SiteDeletionManager.SEPARATOR;
    private string mWebRootFullPath = null;
    private bool mDeleteMetaFiles = true;
    private bool mDeleteMediaFiles = true;
    private bool mDeleteAttachments = true;
    private string mSiteName = null;
    private string mSiteDisplayName = null;

    /// <summary>
    /// Persistent key to store the settings
    /// </summary>
    protected string mPersistentSettingsKey = null;

    /// <summary>
    /// Current UI culture
    /// </summary>
    private System.Globalization.CultureInfo mUICulture = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// UI culture
    /// </summary>
    public System.Globalization.CultureInfo UICulture
    {
        get
        {
            if (mUICulture == null)
            {
                mUICulture = Thread.CurrentThread.CurrentUICulture;
            }
            return mUICulture;
        }
    }


    /// <summary>
    /// Keep information about deletion progress
    /// </summary>
    public string DeletionLog
    {
        get
        {
            return mDeletionLog;
        }
        set
        {
            mDeletionLog = value;
        }
    }


    /// <summary>
    /// Web root full path
    /// </summary>
    public string WebRootFullPath
    {
        get
        {
            return mWebRootFullPath;
        }
        set
        {
            mWebRootFullPath = value;
        }
    }


    /// <summary>
    /// Persistent key to store the settings
    /// </summary>
    public string PersistentSettingsKey
    {
        get
        {
            return mPersistentSettingsKey;
        }
        set
        {
            mPersistentSettingsKey = value;
        }
    }


    /// <summary>
    /// Indicates if meta files should be deleted
    /// </summary>
    public bool DeleteMetaFiles
    {
        get
        {
            return mDeleteMetaFiles;
        }
        set
        {
            mDeleteMetaFiles = value;
        }
    }


    /// <summary>
    /// Indicates if media files should be deleted
    /// </summary>
    public bool DeleteMediaFiles
    {
        get
        {
            return mDeleteMediaFiles;
        }
        set
        {
            mDeleteMediaFiles = value;
        }
    }


    /// <summary>
    /// Indicates if attachments should be deleted
    /// </summary>
    public bool DeleteAttachments
    {
        get
        {
            return mDeleteAttachments;
        }
        set
        {
            mDeleteAttachments = value;
        }
    }


    /// <summary>
    /// Site name
    /// </summary>
    public string SiteName
    {
        get
        {
            return mSiteName;
        }
        set
        {
            mSiteName = value;
        }
    }


    /// <summary>
    /// Site display name
    /// </summary>
    public string SiteDisplayName
    {
        get
        {
            return mSiteDisplayName;
        }
        set
        {
            mSiteDisplayName = value;
        }
    }

    #endregion


    #region "ISerializable Members"

    /// <summary>
    /// Serialization function
    /// </summary>
    /// <param name="info">serialization info</param>
    /// <param name="ctxt">streaming context</param>
    virtual public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        info.AddValue("DeletionLog", mDeletionLog);
        info.AddValue("WebRootFullPath", mWebRootFullPath);
        info.AddValue("DeleteMetaFiles", mDeleteMetaFiles);
        info.AddValue("DeleteMediaFiles", mDeleteMediaFiles);
        info.AddValue("DeleteAttachments", mDeleteAttachments);
        info.AddValue("PersistentSettingsKey", mPersistentSettingsKey);
        info.AddValue("SiteName", mSiteName);
        info.AddValue("SiteDisplayName", mSiteDisplayName);
    }


    /// <summary>
    /// Constructor for deserialization
    /// </summary>
    /// <param name="info">serialization inf</param>
    /// <param name="ctxt">streaming context</param>
    public DeletionInfo(SerializationInfo info, StreamingContext ctxt)
    {
        mDeletionLog = info.GetString("DeletionLog");
        mWebRootFullPath = info.GetString("WebRootFullPath");
        mDeleteMetaFiles = info.GetBoolean("DeleteMetaFiles");
        mDeleteMediaFiles = info.GetBoolean("DeleteMediaFiles");
        mDeleteAttachments = info.GetBoolean("DeleteAttachments");
        mPersistentSettingsKey = info.GetString("PersistentSettingsKey");
        mSiteName = info.GetString("SiteName");
        mSiteDisplayName = info.GetString("SiteDisplayName");
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Constructor
    /// </summary>
    public DeletionInfo()
    {
    }


    /// <summary>
    /// Saves the settings object
    /// </summary>
    public void SavePersistent()
    {
        if (mPersistentSettingsKey != null)
        {
            try
            {
                PersistentStorageHelper.SetValue(mPersistentSettingsKey, this);
            }
            catch
            {
            }
        }
    }


    /// <summary>
    /// Get progress state
    /// </summary>
    public LogStatusEnum GetProgressState()
    {
        string[] status = mDeletionLog.Split(new string[] { SiteDeletionManager.SEPARATOR }, StringSplitOptions.None);

        switch (status[0].ToLower())
        {
            case "e":
                return LogStatusEnum.Error;
            case "f":
                return LogStatusEnum.Finish;
            default:
                return LogStatusEnum.Info;
        }
    }


    /// <summary>
    /// Log deletion state
    /// </summary>
    /// <param name="type">Type of the message</param>
    /// <param name="message">Message to be logged</param>
    public void LogDeletionState(LogStatusEnum type, string message)
    {
        string[] status = DeletionLog.Split(new string[] { SiteDeletionManager.SEPARATOR }, StringSplitOptions.None);

        // Wrong format of the internal status
        if (status.Length != 4)
        {
            DeletionLog = "F" + SiteDeletionManager.SEPARATOR + "" + SiteDeletionManager.SEPARATOR + "" + SiteDeletionManager.SEPARATOR;
        }

        switch (type)
        {
            case LogStatusEnum.Info:
                status[0] = "I";
                status[1] = message + "<br />" + status[1];
                break;

            case LogStatusEnum.Error:
                status[0] = "E";
                status[2] = message + "<br />";
                break;

            case LogStatusEnum.Warning:
                status[3] += "<strong>" + GetAPIString("Global.Warning", "WARNING:&nbsp;") + "</strong>" + message + "<br />";
                break;

            case LogStatusEnum.Finish:
            case LogStatusEnum.UnexpectedFinish:
                status[0] = "F";
                status[1] = "<strong>" + message + "</strong><br />" + status[1];
                break;
        }

        DeletionLog = status[0] + SiteDeletionManager.SEPARATOR + status[1] + SiteDeletionManager.SEPARATOR + status[2] + SiteDeletionManager.SEPARATOR + status[3];
        SavePersistent();
    }


    /// <summary>
    /// Get resource string in correct culture
    /// </summary>
    /// <param name="key">Resource string key</param>
    /// <param name="defaultValue">Default value</param>
    public string GetAPIString(string key, string defaultValue)
    {
        return ResHelper.GetString(key, this.UICulture.Name, defaultValue);
    }

    #endregion
}


/// <summary>
/// Site deletion manager
/// </summary>
public class SiteDeletionManager
{
    #region "Constants"

    /// <summary>
    /// Log separator
    /// </summary>
    public const string SEPARATOR = "<#>";

    #endregion


    #region "Variables"

    private DeletionInfo mDeletionInfo = null;
    private ProcessStatus mStatus = ProcessStatus.Restarted;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Deletion info
    /// </summary>
    public DeletionInfo DeletionInfo
    {
        get
        {
            return mDeletionInfo;
        }
        set
        {
            mDeletionInfo = value;
        }
    }


    /// <summary>
    /// Process status
    /// </summary>
    public ProcessStatus Status
    {
        get
        {
            return mStatus;
        }
        set
        {
            mStatus = value;
        }
    }

    #endregion


    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="deletionInfo">Deletion info</param>
    public SiteDeletionManager(DeletionInfo deletionInfo)
    {
        DeletionInfo = deletionInfo;
    }


    /// <summary>
    /// Update the delete log
    /// </summary>
    /// <param name="node">Node that is about to be deleted</param>
    private void UpdateDeleteLog(TreeNode node)
    {
        DeletionInfo.LogDeletionState(LogStatusEnum.Info, string.Format(DeletionInfo.GetAPIString("Site_Delete.DeletingDocument","Deleting document '{0}' ({1})"), node.NodeAliasPath, node.DocumentCulture));
    }


    /// <summary>
    /// Process folder
    /// </summary>
    /// <param name="codeName">Code name</param>
    /// <param name="delete">Indicates if folder should be deleted</param>
    /// <param name="path">Folder path</param>
    /// <param name="webRootFullPath">Full path</param>
    private void ProcessFolder(string codeName, bool delete, string path, string webRootFullPath)
    {
        if (delete)
        {
            DeletionInfo.LogDeletionState(LogStatusEnum.Info, DeletionInfo.GetAPIString("Site_Delete." + codeName, "Deleting files folder"));
            try
            {
                if (Directory.Exists(path))
                {
                    WebSyncHelperClass.CreateTask(WebFarmTaskTypeEnum.DeleteFolder, "deletefolder", path.Substring(webRootFullPath.Length), null);
                    DirectoryHelper.DeleteDirectory(path, true);
                }
            }
            catch
            {
                DeletionInfo.LogDeletionState(LogStatusEnum.Warning, string.Format(DeletionInfo.GetAPIString("Site_Delete." + codeName + "Warning", "Error deleting folder '{0}'. Please delete the folder manually."), path));
            }
        }
    }


    /// <summary>
    /// Import execution
    /// </summary>
    public void DeleteSite(object parameter)
    {
        mStatus = ProcessStatus.Running;

        string currentDomain = UrlHelper.GetCurrentDomain();

        // Remove current domain temporarily from this request because of licensing
        RequestStockHelper.Remove("CurrentDomain", true);

        // Execute import
        // Save settings
        bool oldDeleteMetaFiles = AttachmentManager.DeletePhysicalFiles;
        bool oldDeleteMediaFiles = ModuleCommands.MediaLibraryGetDeletePhysicalFiles();
        bool oldDeleteAttachments = AttachmentManager.DeletePhysicalFiles;

        // Set settings
        MetaFileInfoProvider.DeletePhysicalFiles = false;
        ModuleCommands.MediaLibrarySetDeletePhysicalFiles(false);
        AttachmentManager.DeletePhysicalFiles = false;

        // Disable smart search reindexation
        bool searchIndexerEnabled = SearchTaskInfoProvider.EnableIndexer;
        SearchTaskInfoProvider.EnableIndexer = false;

        try
        {
            // Delete records in CMS_Tree and CMS_Document
            TreeProvider tree = new TreeProvider();
            tree.LogSynchronization = false;
            tree.LogEvents = false;
            tree.EnableNotifications = false;
            tree.TouchCacheDependencies = false;
            tree.AllowAsyncActions = false;
            tree.LogWebFarmTasks = false;

            // Delete site tree
            DocumentHelper.DeleteSiteTree(DeletionInfo.SiteName, tree, UpdateDeleteLog, DeletionInfo.WebRootFullPath);
            DeletionInfo.LogDeletionState(LogStatusEnum.Info, DeletionInfo.GetAPIString("Site_Delete.DeletingRest", "Deleting other site objects"));

            // Delete other dependences and the site
            SiteInfoProvider.DeleteSite(DeletionInfo.SiteName);

            // Clear all hashtables
            CMSObjectHelper.ClearHashtables();

            // Delete folders (folder info - {codeName, true if should be deleted, path})
            ArrayList folders = new ArrayList();
            folders.Add(new object[] { "DeletingAttachments", DeletionInfo.DeleteAttachments, AttachmentManager.GetFilesFolderPath(DeletionInfo.SiteName) });
            folders.Add(new object[] { "DeletingMediaFiles", DeletionInfo.DeleteMediaFiles, ModuleCommands.MediaLibraryGetMediaRootFolderPath(DeletionInfo.SiteName) });
            folders.Add(new object[] { "DeletingMetaFiles", DeletionInfo.DeleteMetaFiles, MetaFileInfoProvider.GetFilesFolderPath(DeletionInfo.SiteName) });

            foreach (object[] folderInfo in folders)
            {
                ProcessFolder((string)folderInfo[0], (bool)folderInfo[1], (string)folderInfo[2], DeletionInfo.WebRootFullPath);
            }

            DeletionInfo.LogDeletionState(LogStatusEnum.Finish, string.Format(DeletionInfo.GetAPIString("Site_Delete.DeletionFinished", "Site '{0}' has been successfully deleted.<br />"), DeletionInfo.SiteDisplayName));
        }
        catch (Exception ex)
        {
            DeletionInfo.LogDeletionState(LogStatusEnum.Error, string.Format(DeletionInfo.GetAPIString("Site_Delete.DeletionError", "Error deleting site: {0}"), EventLogProvider.GetExceptionLogMessage(ex)));
            // Log the exception
            mStatus = ProcessStatus.Error;
        }
        finally
        {
            // Restore current domain info for possible further use
            RequestStockHelper.Add("CurrentDomain", currentDomain);

            // Restore settings
            MetaFileInfoProvider.DeletePhysicalFiles = oldDeleteMetaFiles;
            ModuleCommands.MediaLibrarySetDeletePhysicalFiles(oldDeleteMediaFiles);
            AttachmentManager.DeletePhysicalFiles = oldDeleteAttachments;

            // Restore search indexer settings
            SearchTaskInfoProvider.EnableIndexer = searchIndexerEnabled;

            // Process search tasks
            SearchTaskInfoProvider.ProcessTasks(true);
        }

        mStatus = ProcessStatus.Finished;
    }
}