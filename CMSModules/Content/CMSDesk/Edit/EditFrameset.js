﻿var IsCMSDesk = true;
var toolbarSize = 76;

function ShowToolbar() {
    frmSet = document.getElementById('editFrameset');
    if (frmSet != null) {
        frmSet.rows = '43, ' + toolbarSize + ', *';
    }
}

function PassiveRefresh(nodeId, parentNodeId) {
    frames['editheader'].location.replace(frames['editheader'].location);
    if (nodeId != null && parentNodeId != null) {
        parent.parent.frames['contenttree'].RefreshNode(parentNodeId, nodeId);
    }
}

function RefreshNode(nodeId, selectNodeId) {
    // Update language selector
    parent.parent.frames['contentmenu'].SelectNode(nodeId);
    // Update tree
    parent.parent.frames['contenttree'].RefreshNode(nodeId, selectNodeId);
    return false;
}

function RefreshTree(expandNodeId, selectNodeId) {
    // Update tree
    parent.parent.frames['contenttree'].RefreshTree(expandNodeId, selectNodeId);
}

function SelectNode(nodeId) {
    parent.parent.frames['contenttree'].SelectNode(nodeId, null);
}

function CreateAnother() {
    window.location.replace(window.location.href);
}

function NewDocument(parentNodeId, className) {
    if (parentNodeId != 0) {
        parent.parent.frames['contentmenu'].NewDocument(parentNodeId, className);
        parent.parent.frames['contenttree'].RefreshNode(parentNodeId, parentNodeId);
    }
}

function NotAllowed(baseUrl, action) {
    parent.location.replace(baseUrl + '/CMSModules/PortalEngine/UI/Content/NotAllowed.aspx?action=' + action);
}

function DeleteDocument(nodeId) {
    if (nodeId != 0) {
        parent.parent.frames['contentmenu'].DeleteDocument(nodeId);
        parent.parent.frames['contenttree'].RefreshNode(nodeId, nodeId);
    }
}

function EditDocument(nodeId) {
    if (nodeId != 0) {
        parent.parent.frames['contentmenu'].EditDocument(nodeId);
        parent.parent.frames['contenttree'].RefreshNode(nodeId, nodeId);
    }
}

function FileCreated(nodeId, parentNodeId, closeWindow) {
    if (wopener != null) {
        wopener.FileCreated(nodeId, parentNodeId, closeWindow);
    }
}

function CheckChanges() {
    if (window.frames['editview'].CheckChanges) {
        return window.frames['editview'].CheckChanges();
    }
    else {
        return true;
    }
}

function SetTabsContext(mode) {
    parent.frames['contenteditheader'].SetTabsContext(mode);
}

var wopener = window.dialogArguments;
if (wopener == null) {
    wopener = opener;
}