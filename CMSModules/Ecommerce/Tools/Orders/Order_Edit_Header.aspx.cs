using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.FormControls;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_Header : CMSEcommercePage
{
    protected int orderId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        // get order id from querystring
        orderId = ValidationHelper.GetInteger(Request.QueryString["orderid"], 0);

        // initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Order_Edit.Orders");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Orders/Order_List.aspx";
        int customerId = ValidationHelper.GetInteger(Request.QueryString["customerId"], 0);
        if (customerId > 0)
        {
            pageTitleTabs[0, 1] += "?customerId=" + customerId.ToString();
            pageTitleTabs[0, 2] = "CustomerContent";
        }
        else
        {
            pageTitleTabs[0, 2] = "ecommerceContent";
        }
        pageTitleTabs[1, 0] = orderId.ToString();
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.HelpTopicName = "general_tab11";
        this.CurrentMaster.Title.HelpName = "helpTopic";
     
        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu(orderId);
        }
    }


    /// <summary>
    /// Initializes newsletter menu
    /// </summary>
    protected void InitalizeMenu(int orderId)
    {
        // custom fields
        DataForm df = new DataForm();
        df.ClassName = "ecommerce.order";
        df.ItemID = orderId;

        int showCustomFields = (df.BasicForm.FormInformation.GetFormElements(true, false).Count <= 0 ? 0 : 1);
        int lastTabIndex = 0;

        string[,] tabs = new string[6 + showCustomFields, 4];
        tabs[lastTabIndex, 0] = ResHelper.GetString("general.general");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab11');";
        tabs[lastTabIndex, 2] = "Order_Edit_General.aspx?orderid=" + orderId;
        lastTabIndex++;

        if (showCustomFields != 0)
        {
            tabs[lastTabIndex, 0] = ResHelper.GetString("Order_Edit_Header.CustomFields");
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'custom_fields_tab');";
            tabs[lastTabIndex, 2] = "Order_Edit_CustomFields.aspx?orderid=" + orderId;
            lastTabIndex++;
        }

        tabs[lastTabIndex, 0] = ResHelper.GetString("Order_Edit_Header.Shipping");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'shipping_tab');";
        tabs[lastTabIndex, 2] = "Order_Edit_Shipping.aspx?orderId=" + orderId;
        lastTabIndex++;

        tabs[lastTabIndex, 0] = ResHelper.GetString("Order_Edit_Header.Billing");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'billing_tab');";
        tabs[lastTabIndex, 2] = "Order_Edit_Billing.aspx?orderId=" + orderId;
        lastTabIndex++;

        tabs[lastTabIndex, 0] = ResHelper.GetString("Order_Edit_Header.Items");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'items_tab');";
        tabs[lastTabIndex, 2] = "Order_Edit_OrderItems.aspx?orderId=" + orderId;
        lastTabIndex++;

        tabs[lastTabIndex, 0] = ResHelper.GetString("Order_Edit_Header.Invoice");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'invoice_tab');";
        tabs[lastTabIndex, 2] = "Order_Edit_Invoice.aspx?orderId=" + orderId;
        lastTabIndex++;

        tabs[lastTabIndex, 0] = ResHelper.GetString("Order_Edit_Header.History");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'history_tab');";
        tabs[lastTabIndex, 2] = "Order_Edit_History.aspx?orderId=" + orderId;
        lastTabIndex++;

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "orderContent";
    }
}

