﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.Controls
Partial Class CMSTemplates_AegeanPower_UCNoAccessPopUp
    Inherits CMSUserControl


    Protected ReadOnly Property Anauthorise() As String
        Get
            Dim sAnauthorise As String = String.Empty
            If Not String.IsNullOrEmpty(Request("Anauthorized")) Then
                sAnauthorise = Request("Anauthorized")
            End If
            Return sAnauthorise
        End Get
    End Property


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If

        If Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        CheckNoAccessPages()
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
        CheckNoAccessPages()
    End Sub


    Private Sub CheckNoAccessPages()
        Dim Str As String = " $(document).ready(function () {"

        Str = String.Concat(Str, " $(""#contact-click"").mouseover(function () { ")
        Str = String.Concat(Str, "var DivPos = document.getElementById('Contact').offsetLeft; ")
        Str = String.Concat(Str, " if (DivPos == ""-213"") { ")
        Str = String.Concat(Str, " $(""#Contact"").animate({ left: 0 }, 1000); ")
        Str = String.Concat(Str, " $(""#NewsLetter"").hide(); ")
        Str = String.Concat(Str, " } ")
        Str = String.Concat(Str, " else { ")
        Str = String.Concat(Str, " $(""#Contact"").animate({ left: -213 }, 1000); ")
        Str = String.Concat(Str, " $(""#NewsLetter"").show(1000); ")
        Str = String.Concat(Str, " } ")
        Str = String.Concat(Str, " }); ")




        If Me.Anauthorise = "1" Then
            If CMSContext.CurrentUser.IsPublic() Then
                Str = String.Concat(Str, " $(""a[id$=HiddenPopUpNoAccess]"").click()  ")
            End If

        End If
        Str = String.Concat(Str, " } ); ")
        JsScript(Str)

    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

End Class
