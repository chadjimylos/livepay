<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckedOut.aspx.cs" Inherits="CMSDesk_MyDesk_CheckedOut_CheckedOut"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="MyDesk - Checked Out Documents" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <cms:UniGrid ID="ugCheckedDocs" runat="server" GridName="checkedout_List.xml" OrderBy="DocumentName"
        IsLiveSite="false" />

    <script type="text/javascript">
        //<![CDATA[
        // Select item action
        function SelectItem(nodeId, culture) {
            if (nodeId != 0) {
                parent.parent.location.href = "../../default.aspx?section=content&action=edit&nodeid=" + nodeId + "&culture=" + culture;
            }
        }
        //]]>
    </script>

</asp:Content>
