<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Alico.ascx.cs" Inherits="CMSFormControls_Alico" %>
<script type="text/javascript">
    function checkAlicoCode(sender, args) {
        var codeID = args.Value;
        var bIsValid = false;

        var RegExp_codeID = new RegExp(/^\d{18}$/);

        if (RegExp_codeID.test(codeID))
            bIsValid = true;


        if (bIsValid) {
            factor = 3;
            sum = 0;

            for (index = codeID.length - 1; index > 0; --index) {
                sum = sum + codeID.substring(index - 1, index) * factor;
                factor = 4 - factor;
            }
            cc = ((1000 - sum) % 10);

            if (cc == codeID.substring(17, 18)) {
                bIsValid = true;
            } else { bIsValid = false; }
            
        }

            
        var IsCutomValid = sender.getAttribute('IsCustomValidator');
        var CustomErrorMess = sender.getAttribute('CustomValidatorErrorMessage');
        if (IsCutomValid != null) {
            if (IsCutomValid.indexOf('CustomCloud_') != -1) {
                if (!bIsValid) {
                    Tab1_ValidationCloud('#' + IsCutomValid, CustomErrorMess, true, '')
                } else {
                    Tab1_ValidationCloud('#' + IsCutomValid, '', false, '')
                }
            }

        }
        args.IsValid = bIsValid;
    }
</script>

<div runat="server" id="CustomCloud_div"></div>

<asp:TextBox runat="server" ID="AlicoCode" cssclass="RegUsersTab1CustomTxt" width="260px" MaxLength="18" />

<div class="HideIt">
    <asp:CustomValidator ID="cv_AlicoCode" 
                        ClientValidationFunction="checkAlicoCode" runat="server"   
                        ControlToValidate="AlicoCode" ValidationGroup="Tab1_PayForm" />
</div>