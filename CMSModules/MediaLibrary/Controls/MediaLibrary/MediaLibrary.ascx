<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MediaLibrary.ascx.cs"
    Inherits="CMSModules_MediaLibrary_Controls_MediaLibrary_MediaLibrary" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/MediaLibraryTree.ascx"
    TagName="LibraryTree" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/FolderActions/FolderActions.ascx"
    TagName="FolderActions" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/FolderActions/EditFolder.ascx"
    TagName="FolderEdit" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/FolderActions/DeleteFolder.ascx"
    TagName="FolderDelete" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/Dialogs/MediaView.ascx" TagName="MediaView"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/LinkMediaSelector/Menu.ascx"
    TagName="DialogMenu" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/MediaFileEdit.ascx"
    TagName="FileEdit" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/MediaFileSingleImport.ascx"
    TagName="SingleImport" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/MediaFileMultipleImport.ascx"
    TagName="MultipleImport" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/FolderActions/CopyMoveFolder.ascx"
    TagName="CopyMove" TagPrefix="cms" %>

<script language="javascript" type="text/javascript">
    //<![CDATA[

    var editDialogOpened = 'true';
    var pendingEditExecuted = 0;

    // Function called from COPY dialog
    function RefreshPage(newPath, action, filename) {
        if (newPath != null) {
            if (action == null) {
                action = 'copymovefolder';
            }

            // Special threatment when called by ImageEditor
            if (action == 'edit') {
                pendingEditExecuted = 0;
                if (editDialogOpened == 'false') {
                    setTimeout('RaiseEdit(\'' + filename + '\')', 300);
                    return;
                }
            }

            SetAction(action, newPath);
            RaiseHiddenPostBack();
        }
    }

    function UploaderOnClick(fileName) {
        if ((fileName != null) && (fileName != '')) {
            $j('#hdnFileOrigName').attr('value', fileName);
        }
    }

    function RefreshLibrary(newPath) {
        newPath = newPath.replace(/\\/g, "\\\\").replace(/'/g, "\\'").replace(/"/g, "\\\"").replace(/</g, "\\<").replace(/>/g, "\\>");
        setTimeout("RefreshLibraryOrig('" + newPath + "')", 300);
    }

    function RefreshLibraryOrig(newPath) {
        SetAction('copymovefinished', newPath);
        RaiseHiddenPostBack();
    }

    // Confirm mass delete
    function MassConfirm(dropdown, msg) {
        var drop = document.getElementById(dropdown);
        if (drop != null) {
            if (drop.value == "delete") {
                return confirm(msg);
            }
            return true;
        }
        return true;
    }

    function SetLibParentAction(argument) {
        // Raise select action
        SetAction('morefolderselect', argument);
        RaiseHiddenPostBack();
    }

    function EditDialogStateUpdate(isOpened) {
        editDialogOpened = isOpened;
    }

    function imageEdit_Refresh(guid) {
        pendingEditExecuted = 0;
        if (editDialogOpened == 'false') {
            setTimeout('RaiseEdit(\'' + guid + '\')', 300);
        }
    }

    function RaiseEdit(guid) {
        if ((editDialogOpened == 'false') && (pendingEditExecuted == 0)) {
            pendingEditExecuted = 1;
            SetAction('edit', guid);
            RaiseHiddenPostBack();
        }
    }
    //]]>
</script>

<div class="Hidden HiddenButton">
    <asp:UpdatePanel ID="pnlUpdateHidden" runat="server">
        <ContentTemplate>
            <asp:Literal ID="ltlScript" runat="server" EnableViewState="false"></asp:Literal>
            <asp:HiddenField ID="hdnAction" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnArgument" runat="server"></asp:HiddenField>
            <cms:CMSButton ID="hdnButton" runat="server" OnClick="hdnButton_Click" CssClass="HiddenButton"
                EnableViewState="false" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<cms:FileSystemDataSource ID="fileSystemDataSource" runat="server" />
<div class="DialogMainBlock">
    <asp:UpdatePanel ID="pnlUpdateMenu" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <cms:DialogMenu ID="menuElem" runat="server" AllowFullscreen="true" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="DialogContent">
        <asp:Panel ID="pnlLeftContent" runat="server" CssClass="DialogLeftBlock">
            <asp:PlaceHolder ID="plcFolderActions" runat="server">
                <div class="MediaLibraryFolderActions">
                    <asp:UpdatePanel ID="pnlUpdateSelectors" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <cms:FolderActions ID="folderActions" runat="server" DisplayRename="false" DisplayNew="false"
                                Layout="Horizontal" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:PlaceHolder>
            <asp:Panel ID="pnlTreeArea" runat="server" class="DialogTreeArea">
                <div class="DialogTree">
                    <asp:UpdatePanel ID="pnlUpdateTree" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlTreeAreaSep" CssClass="DialogMediaLibraryTreeAreaSep">
                                <cms:LocalizedLabel runat="server" ID="lblNoLibraries" ResourceString="dialogs.libraries.nolibraries"
                                    EnableViewState="false" Visible="false" CssClass="InfoLabel" />
                            </asp:Panel>
                            <cms:LibraryTree ID="folderTree" runat="server" GenerateIDs="true" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>
            <div class="DialogResizerH">
                <div class="DialogResizerArrowH">
                    &nbsp;</div>
            </div>
        </asp:Panel>
        <div class="DialogTreeAreaSeparator">
        </div>
        <asp:Panel ID="pnlRightContent" runat="server" CssClass="DialogRightBlock">
            <asp:UpdatePanel ID="pnlUpdateContent" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <div id="divDialogView" class="DialogViewContent" runat="server">
                        <asp:UpdatePanel ID="pnlUpdateView" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <cms:CMSUpdatePanel ID="pnlUpdateViewInfo" runat="server" UpdateMode="Conditional"
                                    ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:Panel ID="pnlInfo" runat="server" CssClass="DialogErrorArea" Visible="false">
                                            <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"></asp:Label>
                                            <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"></asp:Label>
                                        </asp:Panel>
                                    </ContentTemplate>
                                </cms:CMSUpdatePanel>
                                <cms:MediaView ID="mediaView" runat="server" DisplayMode="Simple" />
                                <cms:CMSUpdatePanel ID="pnlUpdateViewHidden" runat="server" UpdateMode="Conditional"
                                    ChildrenAsTriggers="false">
                                    <ContentTemplate>
                                        <asp:HiddenField ID="hdnLastSearchedValue" runat="server" />
                                        <asp:HiddenField ID="hdnLastSelectedPath" runat="server" />
                                    </ContentTemplate>
                                </cms:CMSUpdatePanel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:Literal ID="ltlColorizeScript" runat="server"></asp:Literal>
                    </div>
                    <div id="divDialogResizer" class="DialogResizerVLine" runat="server" enableviewstate="false">
                        <div class="DialogResizerV">
                            <div class="DialogResizerArrowV">
                                &nbsp;</div>
                        </div>
                    </div>
                    <div id="divDialogProperties" class="DialogProperties MediaProperties" runat="server">
                        <asp:UpdatePanel ID="pnlUpdateProperties" runat="server" UpdateMode="Conditional"
                            ChildrenAsTriggers="false">
                            <ContentTemplate>
                                <asp:PlaceHolder ID="plcFileEdit" runat="server">
                                    <cms:FileEdit ID="fileEdit" runat="server" />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="plcEditFolder" runat="server">
                                    <cms:FolderEdit ID="folderEdit" runat="server" />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="plcImportFile" runat="server">
                                    <cms:SingleImport ID="fileImport" runat="server" />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="plcMultipleImport" runat="server">
                                    <cms:MultipleImport ID="multipleImport" runat="server" />
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="plcCopyMove" runat="server">
                                    <cms:CopyMove ID="folderCopyMove" runat="server" />
                                </asp:PlaceHolder>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
</div>
