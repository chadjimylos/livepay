<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ContentEditFrameset.aspx.cs"
    Inherits="CMSModules_Content_CMSDesk_ContentEditFrameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Content - Edit</title>

    <script type="text/javascript">
    //<![CDATA[
        function CheckChanges()
        {
            if ( window.frames['contenteditview'].CheckChanges )
            {
                return window.frames['contenteditview'].CheckChanges();
            }
            else
            {   
                return true;
            }
        }

        function RefreshTree(expandNodeId, selectNodeId) {
            // Update tree
            parent.frames['contenttree'].RefreshNode(expandNodeId, selectNodeId);
        }
        
        var IsCMSDesk = true;
    //]]>
    </script>
    
    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
</head>
    <frameset border="0" rows="35,*,0,0">
		<frame name="contenteditheader" src="Edit/edittabs.aspx<%=Request.Url.Query%>" scrolling="no" noresize="noresize" frameborder="0" />
		<frame name="contenteditview" src="<%=viewpage%>" scrolling="auto" frameborder="0" />
		<noframes>
			<p id="p1">
				This HTML frameset displays multiple Web pages. To view this frameset, use a 
				Web browser that supports HTML 4.0 and later.
			</p>
		</noframes>
	</frameset>
</html>
