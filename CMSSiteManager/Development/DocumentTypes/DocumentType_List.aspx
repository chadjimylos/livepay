<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentType_List.aspx.cs"
    Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Document Types - Document Type List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblError" ForeColor="red" EnableViewState="false" Visible="false" />
    <cms:UniGrid runat="server" ID="uniGrid" GridName="DocumentType_List.xml" OrderBy="ClassDisplayName"
        IsLiveSite="false" />
</asp:Content>
