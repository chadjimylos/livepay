using System;
using System.Xml;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text;
using System.Web;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.FormEngine;
using CMS.FormControls;
using CMS.CMSHelper;
using CMS.ExtendedControls;
using CMS.WorkflowEngine;
using CMS.DataEngine;
using CMS.PortalEngine;
using CMS.UIControls;


public partial class CMSModules_AdminControls_Controls_Class_FieldEditor : CMSUserControl
{
    #region "Variables"

    private FormInfo fi = new FormInfo();
    private FormFieldInfo ffi = null;
    private FormCategoryInfo fci = null;
    private string[] columnNames = null;

    private bool mShowFieldVisibility = false;
    private bool mDevelopmentMode = false;
    private string mClassName = "";
    private string mImageDirectoryPath = null;
    private bool mDisplaySourceFieldSelection = true;
    private int mWebPartId = 0;
    private FieldEditorModeEnum mMode;
    private FieldEditorControlsEnum mDisplayedControls = FieldEditorControlsEnum.ModeSelected;
    private bool mEnableSimplifiedMode = false;
    private bool mEnableSystemFields = false;
    private bool mEnableMacrosForDefaultValue = false;

    #endregion


    #region "Custom events"

    /// <summary>
    /// Event raised when OK button is clicked and before xml definition is changed
    /// </summary>
    public event OnBeforeDefinitionUpdateEventHandler OnBeforeDefinitionUpdate;

    /// <summary>
    /// Event raised when OK button is clicked and after xml definition is changed
    /// </summary>
    public event OnAfterDefinitionUpdateEventHandler OnAfterDefinitionUpdate;

    /// <summary>
    /// Event raised when new field is created and form defifition is saved
    /// </summary>
    public event OnFieldCreated OnFieldCreated;


    /// <summary>
    /// Raises OnFieldCreated event
    /// </summary>
    /// <param name="newField">Newly created field</param>
    protected void RaiseOnFieldCreated(FormFieldInfo newField)
    {
        if (OnFieldCreated != null)
        {
            OnFieldCreated(newField);
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Indicates if system fields from tables CMS_Tree and CMS_Documetn are offered to the user.
    /// </summary>
    public bool EnableSystemFields
    {
        get
        {
            return mEnableSystemFields;
        }
        set
        {
            mEnableSystemFields = value;
        }
    }


    /// <summary>
    /// Indicates if field visibility selector should be displayed.
    /// </summary>
    public bool ShowFieldVisibility
    {
        get
        {
            return mShowFieldVisibility;
        }
        set
        {
            mShowFieldVisibility = value;
        }
    }


    /// <summary>
    /// Indicates if field editor works in development mode.
    /// </summary>
    public bool DevelopmentMode
    {
        get
        {
            return mDevelopmentMode;
        }
        set
        {
            mDevelopmentMode = value;
        }
    }


    /// <summary>
    /// Class name
    /// </summary>
    public string ClassName
    {
        get
        {
            return mClassName;
        }
        set
        {
            mClassName = value;
        }
    }


    /// <summary>
    /// Directory path for images
    /// </summary>
    public string ImageDirectoryPath
    {
        get
        {
            return mImageDirectoryPath;
        }
        set
        {
            if (!value.EndsWith("/"))
            {
                mImageDirectoryPath = value + "/";
            }
            else
            {
                mImageDirectoryPath = value;
            }
        }
    }


    /// <summary>
    /// Indicates if display source field selection
    /// </summary>
    public bool DisplaySourceFieldSelection
    {
        get
        {
            return mDisplaySourceFieldSelection;
        }

        set
        {
            mDisplaySourceFieldSelection = value;
        }
    }


    /// <summary>
    /// Webpart Id.
    /// </summary>
    public int WebPartId
    {
        get
        {
            return mWebPartId;
        }

        set
        {
            mWebPartId = value;
        }
    }


    /// <summary>
    /// Mode.
    /// </summary>
    public FieldEditorModeEnum Mode
    {
        get
        {
            return mMode;
        }
        set
        {
            mMode = value;
        }
    }


    /// <summary>
    /// Type of custom controls that can be selected from the control list in FieldEditor.
    /// </summary>
    public FieldEditorControlsEnum DisplayedControls
    {
        get
        {
            return mDisplayedControls;
        }
        set
        {
            mDisplayedControls = value;
        }
    }


    /// <summary>
    /// Indicates if simplified mode is enabled.
    /// </summary>
    public bool EnableSimplifiedMode
    {
        get
        {
            return mEnableSimplifiedMode;
        }

        set
        {
            mEnableSimplifiedMode = value;
        }
    }


    /// <summary>
    /// Form XML definition
    /// </summary>
    public string FormDefinition
    {
        get
        {
            object obj = ViewState["FormDefinition"];
            return (obj == null) ? "" : (string)obj;
        }

        set
        {
            ViewState["FormDefinition"] = value;
        }
    }


    /// <summary>
    /// Enable or disable the option to use macros as default value
    /// </summary>
    public bool EnableMacrosForDefaultValue
    {
        get
        {
            return mEnableMacrosForDefaultValue;
        }
        set
        {
            mEnableMacrosForDefaultValue = value;
        }
    }

    #endregion


    #region "Private properties"

    /// <summary>
    /// Returns True if system fields are enabled and one of them is selected
    /// </summary>
    private bool IsSystemFieldSelected
    {
        get
        {
            return (EnableSystemFields && plcGroup.Visible);
        }
    }


    /// <summary>
    /// Selected mode.
    /// </summary>
    private FieldEditorSelectedModeEnum SelectedMode
    {
        get
        {
            FieldEditorSelectedModeEnum mode;

            if (ViewState["SelectedMode"] == null)
            {
                mode = GetDefaultSelectedMode();
            }
            else
            {
                switch (Convert.ToInt32(ViewState["SelectedMode"]))
                {
                    case 0:
                        mode = FieldEditorSelectedModeEnum.Simplified;
                        break;

                    case 1:
                        mode = FieldEditorSelectedModeEnum.Advanced;
                        break;

                    default:
                        mode = GetDefaultSelectedMode();
                        break;
                }
            }

            return mode;
        }
        set
        {
            switch (value)
            {
                case FieldEditorSelectedModeEnum.Simplified:
                    ViewState["SelectedMode"] = 0;
                    break;

                case FieldEditorSelectedModeEnum.Advanced:
                    ViewState["SelectedMode"] = 1;
                    break;
            }
        }
    }


    /// <summary>
    /// Indicates whether new item is edited.
    /// </summary>
    private bool IsNewItemEdited
    {
        get
        {
            object obj = ViewState["IsNewItemEdited"];
            return (obj == null) ? false : (bool)obj;
        }
        set
        {
            ViewState["IsNewItemEdited"] = value;
        }
    }


    /// <summary>
    /// Selected item name.
    /// </summary>
    private string SelectedItemName
    {
        get
        {
            object obj = ViewState["SelectedItemName"];
            return (obj == null) ? "" : (string)obj;
        }
        set
        {
            ViewState["SelectedItemName"] = value;
        }
    }


    /// <summary>
    /// Selected item type.
    /// </summary>
    private FieldEditorSelectedItemEnum SelectedItemType
    {
        get
        {
            object obj = ViewState["SelectedItemType"];
            return (obj == null) ? 0 : (FieldEditorSelectedItemEnum)obj;
        }
        set
        {
            ViewState["SelectedItemType"] = value;
        }
    }


    /// <summary>
    /// Is field primary.
    /// </summary>
    private bool IsFieldPrimary
    {
        get
        {
            object obj = ViewState["IsFieldPrimary"];
            return (obj == null) ? false : (bool)obj;
        }
        set
        {
            ViewState["IsFieldPrimary"] = value;
        }
    }


    /// <summary>
    /// Field data type.
    /// </summary>
    private string FieldDataType
    {
        get
        {
            object obj = ViewState["FieldDataType"];
            return (obj == null) ? "" : (string)obj;
        }
        set
        {
            ViewState["FieldDataType"] = (value != null) ? value.ToLower() : value;
        }
    }


    /// <summary>
    /// Indicates if document type is edited
    /// </summary>
    private bool IsDocumentType
    {
        get
        {
            object obj = ViewState["IsDocumentType"];
            if ((obj == null) && (!string.IsNullOrEmpty(ClassName)))
            {
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(mClassName);
                ViewState["IsDocumentType"] = ((dci != null) && dci.ClassIsDocumentType);
            }
            return ValidationHelper.GetBoolean(ViewState["IsDocumentType"], false);
        }
    }

    #endregion


    #region "Global definitions"

    // Constants
    private const string newCategPreffix = "#categ##new#";
    private const string newFieldPreffix = "#field##new#";
    private const string categPreffix = "#categ#";
    private const string fieldPreffix = "#field#";
    private const int preffixLength = 7; // Length of categPreffix = length of fieldPreffix = 7
    private const string dtPickerFolder = "~/CMSAdminControls/Calendar";
    const string controlPrefix = "#uc#";
    private const string SEMICOLON = "##";

    // Basic controls for both modes (simplified and advanced)
    //--------------------------------------------------------

    // Attribute name
    Label lblAttributeName = new Label();
    TextBox txtAttributeName = new TextBox();

    // Allow empty 
    Label lblAttributeAllowEmpty = new Label();
    CheckBox chkAllowEmpty = new CheckBox();

    // Default value types
    Label lblDefaultValue = new Label();
    TextBox txtDefaultValue = new TextBox();
    FormEngineUserControl txtLargeDefaultValue = new FormEngineUserControl();
    CheckBox chkDefaultValue = new CheckBox();
    DateTimePicker datetimeDefaultValue = new DateTimePicker();

    // Field is system ?
    Label lblAttributeIsSystem = new Label();
    CheckBox chkAttributeIsSystem = new CheckBox();
    PlaceHolder plcAttributeIsSystem = new PlaceHolder();

    // Field caption
    Label lblFieldCaption = new Label();
    TextBox txtFieldCaption = new TextBox();

    // Field type
    Label lblFieldType = new Label();
    DropDownList lstFieldType = new DropDownList();

    // Field type options
    PlaceHolder plcTimeOptions = new PlaceHolder();
    Label lblTimeZoneType = new Label();
    Label lblTimeZone = new Label();
    FormEngineUserControl timeZone = new FormEngineUserControl();
    FormEngineUserControl timeZoneType = new FormEngineUserControl();


    // Additional controls for both modes (simplified and advanced)
    //--------------------------------------------------------

    // Panel with upload file settings
    Panel pnlUploadFileSettings = new Panel();
    Label lblUploadFile = new Label();
    IFieldEditorControl elemAutoResize = null;

    // -------------------------------------

    // Panel with text area settings
    Panel pnlTextAreaSettings = new Panel();

    // Text area columns
    Label lblTextAreaCols = new Label();
    TextBox txtTextAreaCols = new TextBox();

    // Text area rows
    Label lblTextAreaRows = new Label();
    TextBox txtTextAreaRows = new TextBox();

    // Text area size
    Label lblTextAreaSize = new Label();
    TextBox txtTextAreaSize = new TextBox();

    //----------------------------------------

    // Panel with html area settings
    Panel pnlHTMLAreaSettings = new Panel();

    // Html area width
    Label lblHTMLAreaWidth = new Label();
    TextBox txtHTMLAreaWidth = new TextBox();

    // Html area height
    Label lblHTMLAreaHeight = new Label();
    TextBox txtHTMLAreaHeight = new TextBox();

    // Html area size
    Label lblHTMLAreaSize = new Label();
    TextBox txtHTMLAreaSize = new TextBox();

    // Html area css stylesheet
    Label lblHTMLAreaCss = new Label();
    CMSFormControls_SelectCssStylesheet usHTMLAreaCss = null;

    // Html area toolbar set
    Label lblHTMLAreaToolbarSet = new Label();
    TextBox txtHTMLAreaToolbarSet = new TextBox();

    // Html area toolbar set
    Label lblHTMLAreaStartingPath = new Label();
    TextBox txtHTMLAreaStartingPath = new TextBox();

    //----------------------------------------

    // Panel with drop down list settings
    Panel pnlDropDownListSettings = new Panel();

    // Placeholder containing dropdownlist with direction values (horizontal/vertical)
    PlaceHolder plcRadioButtonsDirection = new PlaceHolder();

    Label lblRadioButtonsDirection = new Label();
    DropDownList drpRadioButtonsDirection = new DropDownList();

    // Drop down list options radiobutton
    RadioButton radDropDownListOptions = new RadioButton();

    // Drop down list sql radiobutton 
    RadioButton radDropDownListSQL = new RadioButton();

    // Drop down list options textbox    
    TextBox txtDropDownListOptions = new TextBox();

    //----------------------------------------

    // Panel with calendar settings
    Panel pnlCalendarSettings = new Panel();

    // Edit time checkbox
    CheckBox chkCalendarSettingsEditTime = new CheckBox();

    // Show now link
    CheckBox chkCalendarSettingsShowNowLink = new CheckBox();


    #region "BBEditor settings"
    //----------------------------------------

    // Panel with BBEditor settings
    Panel pnlBBEditorSettings = new Panel();

    // Show Quote checkbox
    CheckBox chkBBEditorSettingsShowQuote = new CheckBox();

    // Show Bold checkbox
    CheckBox chkBBEditorSettingsShowBold = new CheckBox();

    // Show Italic checkbox
    CheckBox chkBBEditorSettingsShowItalic = new CheckBox();

    // Show Underline checkbox
    CheckBox chkBBEditorSettingsShowUnderline = new CheckBox();

    // Show Strike checkbox
    CheckBox chkBBEditorSettingsShowStrike = new CheckBox();

    // Show Color checkbox
    CheckBox chkBBEditorSettingsShowColor = new CheckBox();

    // Show Code checkbox
    CheckBox chkBBEditorSettingsShowCode = new CheckBox();

    // Show UsePromptDialog checkbox
    CheckBox chkBBEditorSettingsUsePromptDialog = new CheckBox();

    // BBEditor Text area columns
    Label lblBBEditorCols = new Label();
    TextBox txtBBEditorCols = new TextBox();

    // BBEditor Text area rows
    Label lblBBEditorRows = new Label();
    TextBox txtBBEditorRows = new TextBox();

    // BBEditor Text area size
    Label lblBBEditorSize = new Label();
    TextBox txtBBEditorSize = new TextBox();

    #endregion


    //----------------------------------------

    // Panel with document attachments settings
    Panel pnlDocumentAttachmentsSettings = new Panel();
    IFieldEditorControl ucDocAttachSettings = null;

    //----------------------------------------

    // Panel with multiple choice settings
    Panel pnlMultipleChoice = new Panel();

    // Multiple choice options radiobutton
    RadioButton radMultipleChoiceOptions = new RadioButton();

    Label lblMultipleChoiceDirection = new Label();
    DropDownList drpMultipleChoiceDirection = new DropDownList();

    // Multiple choice sql radiobutton 
    RadioButton radMultipleChoiceSQL = new RadioButton();

    // Multiple choice options textbox    
    TextBox txtMultipleChoiceOptions = new TextBox();

    //----------------------------------------

    // Panel with list box settings
    Panel pnlListBox = new Panel();

    // List box "Allow multiple choices" label and checkbox
    Label lblListBoxMultipleChoices = new Label();
    CheckBox chkListBoxMultipleChoices = new CheckBox();

    // List box options radiobutton
    RadioButton radListBoxOptions = new RadioButton();

    // List box sql radiobutton
    RadioButton radListBoxSQL = new RadioButton();

    // List box options textbox
    TextBox txtListBoxOptions = new TextBox();

    //----------------------------------------

    // Panel with image selection settings
    Panel pnlImageSelection = new Panel();

    Label lblImageWidth = new Label();
    Label lblImageHeight = new Label();
    Label lblImageMaxSideSize = new Label();

    TextBox txtImageWidth = new TextBox();
    TextBox txtImageHeight = new TextBox();
    TextBox txtImageMaxSideSize = new TextBox();

    //----------------------------------------

    // Panel public field option
    PlaceHolder pnlPublicField = new PlaceHolder();

    // Public field checkbox
    Label lblPublicField = new Label();

    // Public field checkbox
    CheckBox chkPublicField = new CheckBox();

    //----------------------------------------

    // Panel with all editable controls
    Panel pnlAttributeEdit = new Panel();

    #endregion


    protected void Page_PreRender(object sender, EventArgs e)
    {
        InitializeLinks();
        SetCssClasses();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (mImageDirectoryPath == null)
        {
            mImageDirectoryPath = "CMSModules/CMS_Class/";
        }

        if (EnableSystemFields)
        {
            btnNewSysAttribute.Visible = true;
            btnNewSysAttribute.ImageUrl = GetImageUrl(ImageDirectoryPath + "NewSystem.png");
            btnNewSysAttribute.ToolTip = ResHelper.GetString("TemplateDesigner.NewSysAttribute");
        }
        else
        {
            btnNewSysAttribute.Visible = false;
        }

        // Set images url
        btnNewCategory.ImageUrl = GetImageUrl(ImageDirectoryPath + "NewCategory.png");
        btnNewAttribute.ImageUrl = GetImageUrl(ImageDirectoryPath + "New.png");
        btnDeleteItem.ImageUrl = GetImageUrl(ImageDirectoryPath + "Delete.png");
        btnUpAttribute.ImageUrl = GetImageUrl(ImageDirectoryPath + "Up.png");
        btnDownAttribute.ImageUrl = GetImageUrl(ImageDirectoryPath + "Down.png");

        lblSectionDatabase.Text = ResHelper.GetString("TemplateDesigner.Section.Database");
        lblSectionField.Text = ResHelper.GetString("TemplateDesigner.Section.Field");
        lblSectionValidation.Text = ResHelper.GetString("TemplateDesigner.Section.Validation");
        lblSectionDesign.Text = ResHelper.GetString("TemplateDesigner.Section.Design");
        lblSectionCategory.Text = ResHelper.GetString("TemplateDesigner.Section.Category");
        lblTimeZoneSection.Text = ResHelper.GetString("TemplateDesigner.Section.TimeZone");

        btnDeleteItem.ToolTip = ResHelper.GetString("TemplateDesigner.DeleteItem");
        btnNewCategory.ToolTip = ResHelper.GetString("TemplateDesigner.NewCategory");
        btnNewAttribute.ToolTip = ResHelper.GetString("TemplateDesigner.NewAttribute");
        btnDownAttribute.ToolTip = ResHelper.GetString("TemplateDesigner.DownAttribute");
        btnUpAttribute.ToolTip = ResHelper.GetString("TemplateDesigner.UpAttribute");

        lblGroup.Text = ResHelper.GetString("TemplateDesigner.Group");
        lblGuid.Text = ResHelper.GetString("TemplateDesigner.Guid");
        lblCategoryName.Text = ResHelper.GetString("TemplateDesigner.CategoryName");
        lblAttributeSize.Text = ResHelper.GetString("TemplateDesigner.AttributeSize");
        lblAttributeType.Text = ResHelper.GetString("TemplateDesigner.AttributeType");
        lblFieldDescription.Text = ResHelper.GetString("TemplateDesigner.FieldDescription");
        lblSpellCheck.Text = ResHelper.GetString("TemplateDesigner.SpellCheck");
        btnOk.Text = ResHelper.GetString("general.ok");
        chkDisplayInForm.Text = ResHelper.GetString("TemplateDesigner.DisplayInForm");
        ltlConfirmText.Text = "<input type=\"hidden\" id=\"confirmdelete\" value=\"" + ResHelper.GetString("TemplateDesigner.ConfirmDelete") + "\"/>";
        btnDeleteItem.Attributes.Add("onclick", "javascript:return confirmDelete();");
        //this.lblUserControlSettingsName.Text = ResHelper.GetString("TemplateDesigner.UserControlSettingsName");

        // Field advanced settings
        lblAdvacedFieldSettings.Text = ResHelper.GetString("TemplateDesigner.AdvacedFieldSettings");
        lnkAdvacedFieldSettings.Text = ResHelper.GetString("TemplateDesigner.ConfigureSettings");
        lnkAdvacedFieldSettings.OnClientClick = "DisplayHideConfiguration();return false;";
        if (!UrlHelper.IsPostback())
        {
            pnlAdvancedFieldSettings.Attributes.Add("style", "display:none");
        }
        lblAutoResize.Text = ResHelper.GetString("dialogs.config.autoresize");

        StringBuilder script = new StringBuilder();
        script.Append("var displayConfig = false; function DisplayHideConfiguration(){");
        script.Append("var config = document.getElementById('" + pnlAdvancedFieldSettings.ClientID + "');");
        script.Append("if (config != null){");
        script.Append("displayConfig = !displayConfig;");
        script.Append("if (displayConfig){config.style.display = 'block';}else{config.style.display = 'none';}");
        script.Append("}}");
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "DisplayHideConfiguration", ScriptHelper.GetScript(script.ToString()));

        lblRegExpr.Text = ResHelper.GetString("TemplateDesigner.RegularExpression");
        lblMinValue.Text = ResHelper.GetString("TemplateDesigner.MinValue");
        lblMaxValue.Text = ResHelper.GetString("TemplateDesigner.MaxValue");
        lblMinLength.Text = ResHelper.GetString("TemplateDesigner.MinLength");
        lblMaxLength.Text = ResHelper.GetString("TemplateDesigner.MaxLength");
        lblFrom.Text = ResHelper.GetString("TemplateDesigner.From");
        lblTo.Text = ResHelper.GetString("TemplateDesigner.To");
        lblFileExtensions.Text = ResHelper.GetString("TemplateDesigner.Extensions");
        lblErrorMessage.Text = ResHelper.GetString("TemplateDesigner.ErrorMessage");

        lblCaptionStyle.Text = ResHelper.GetString("TemplateDesigner.CaptionStyle");
        lblInputStyle.Text = ResHelper.GetString("TemplateDesigner.InputStyle");
        lblControlCssClass.Text = ResHelper.GetString("TemplateDesigner.ControlCssClass");
        lblSourceField.Text = ResHelper.GetString("TemplateDesigner.SourceField");
        lblSourceAliasField.Text = ResHelper.GetString("TemplateDesigner.SourceAliasField");

        lblBBEnableImage.Text = ResHelper.GetString("TemplateDesigner.EnableImage");
        lblBBEnableLink.Text = ResHelper.GetString("TemplateDesigner.EnableLink");

        dateTo.SupportFolder = dtPickerFolder;
        dateFrom.SupportFolder = dtPickerFolder;

        lblChangeVisibility.Text = ResHelper.GetString("TemplateDesigner.ChangeVisibility");
        lblVisibility.Text = ResHelper.GetString("TemplateDesigner.Visibility");
        lblVisibilityControl.Text = ResHelper.GetString("TemplateDesigner.VisibilityControl");

        lblInfo.Visible = false;
        lblError.Visible = true;
        lblError.Text = "&nbsp;";
        ltlScript.Text = "";

        btnOk.Enabled = true;
        btnUpAttribute.Enabled = true;
        btnDownAttribute.Enabled = true;
        btnDeleteItem.Enabled = true;
        btnNewAttribute.Enabled = true;
        btnNewCategory.Enabled = true;

        // Set universal controls
        SetUniversalControls();

        // Set field types
        if ((EnableSystemFields) && (drpGroup.Items.Count == 0))
        {
            drpGroup.Items.Add(new ListItem(ResHelper.GetString("TemplateDesigner.DocumentAttributes"), "cms_document"));
            drpGroup.Items.Add(new ListItem(ResHelper.GetString("TemplateDesigner.NodeAttributes"), "cms_tree"));
        }

        if (!UrlHelper.IsPostback())
        {
            Reload(null);
        }

        // Mark categories in the listbox
        MarkCategories();

        // Display or hide tabs
        InitializeLinks();
    }


    /// <summary>
    /// Reload field editor
    /// </summary>
    /// <param name="selectedValue">Selected field in field list</param>
    public void Reload(string selectedValue)
    {
        bool isModeSelected = false;
        bool isItemSelected = false;

        switch (mMode)
        {
            case FieldEditorModeEnum.General:
            case FieldEditorModeEnum.AlternativeFormDefinition:
                if (!string.IsNullOrEmpty(FormDefinition))
                {
                    isModeSelected = true;
                }
                else
                {
                    // Clear item list
                    lstAttributes.Items.Clear();
                }
                break;

            case FieldEditorModeEnum.ClassFormDefinition:
            case FieldEditorModeEnum.BizFormDefinition:
            case FieldEditorModeEnum.SystemTable:
            case FieldEditorModeEnum.CustomTable:
                if (!string.IsNullOrEmpty(mClassName))
                {
                    isModeSelected = true;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "Class name is not set.";
                }
                break;

            case FieldEditorModeEnum.WebPartProperties:
                if ((mWebPartId > 0))
                {
                    isModeSelected = true;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "WebPartID is not set.";
                }
                break;

            default:
                lblError.Visible = true;
                lblError.Text = "No field editor mode is set.";
                break;
        }

        if (isModeSelected)
        {
            LoadFormDefinition();
            LoadAttributesList(selectedValue);

            if (SelectedItemType == FieldEditorSelectedItemEnum.Field)
            {
                isItemSelected = true;

                InitializeLinks();

                DisplaySelectedTabContent();
                SetUniversalControls();
                ffi = fi.GetFormField(SelectedItemName);
                LoadSelectedField();
            }
            else if (SelectedItemType == FieldEditorSelectedItemEnum.Category)
            {
                isItemSelected = true;
                LoadSelectedCategory();
            }
        }

        if (!isItemSelected)
        {
            HideAllPanels();
            btnOk.Enabled = false;
            btnUpAttribute.Enabled = false;
            btnDownAttribute.Enabled = false;
            btnDeleteItem.Enabled = false;
            btnNewAttribute.Enabled = true; // Only new items can be added
            btnNewCategory.Enabled = true; // Only new items can be added
        }

        DisplayOrHideSourceFieldSelection();
        DisplayOrHideActions();

        // Show or hide field visibility selector
        plcVisibility.Visible = ShowFieldVisibility;
    }


    /// <summary>
    /// Load xml definition of the form
    /// </summary>
    private void LoadFormDefinition()
    {
        bool isError = false;

        switch (mMode)
        {
            case FieldEditorModeEnum.General:
            case FieldEditorModeEnum.AlternativeFormDefinition:
                // Definition is loaded from external xml
                break;

            case FieldEditorModeEnum.WebPartProperties:

                // Load xml definition from webpartinfo
                WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(mWebPartId);
                if (wpi != null)
                {
                    FormDefinition = wpi.WebPartProperties;
                }
                else
                {
                    isError = true;
                }
                break;

            case FieldEditorModeEnum.ClassFormDefinition:
            case FieldEditorModeEnum.BizFormDefinition:
            case FieldEditorModeEnum.SystemTable:
            case FieldEditorModeEnum.CustomTable:

                // Load xml definition from Classinfo
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(mClassName);
                if (dci != null)
                {
                    FormDefinition = dci.ClassFormDefinition;
                }
                else
                {
                    isError = true;
                }
                break;
        }

        if (isError)
        {
            lblError.Visible = true;
            lblError.Text = "[ FieldEditor.LoadFormDefinition() ]: " + ResHelper.GetString("FieldEditor.XmlDefinitionNotLoaded");
        }

        fi.LoadXmlDefinition(FormDefinition);
    }


    /// <summary>
    /// Fill attribute list
    /// </summary>
    /// <param name="selectedValue">Selected value in attribute list, null if first item is selected</param>
    private void LoadAttributesList(string selectedValue)
    {
        FormFieldInfo formField = null;
        FormCategoryInfo formCategory = null;
        ListItem li = null;

        // Reload list only if new item is not edited
        if (!IsNewItemEdited)
        {
            // Clear item list
            lstAttributes.Items.Clear();

            // Get all list items (fields and categories)        
            ArrayList itemList = fi.GetFormElements(true, true);

            if (itemList != null)
            {
                foreach (object item in itemList)
                {
                    string itemDisplayName = null;
                    string itemCodeName = null;
                    if (item is FormFieldInfo)
                    {
                        formField = ((FormFieldInfo)(item));
                        itemDisplayName = formField.Name;
                        itemCodeName = fieldPreffix + formField.Name;
                        li = new ListItem(itemDisplayName, itemCodeName);
                    }
                    else if (item is FormCategoryInfo)
                    {
                        formCategory = ((FormCategoryInfo)(item));
                        itemDisplayName = formCategory.CategoryCaption;
                        itemCodeName = categPreffix + formCategory.CategoryName;
                        li = new ListItem(itemDisplayName, itemCodeName);
                        // Highlight categories in the list 
                        li.Attributes.Add("class", "FieldEditorCategoryItem");
                    }
                    if (li != null)
                    {
                        lstAttributes.Items.Add(li);
                    }
                }
            }

            // Set selected item
            if (lstAttributes.Items.Count > 0)
            {
                if (!string.IsNullOrEmpty(selectedValue))
                {
                    lstAttributes.SelectedValue = selectedValue;
                }
                else
                {
                    // Select first item of the list       
                    lstAttributes.SelectedIndex = 0;
                }
            }

            // Default values - list is empty
            SelectedItemName = null;
            SelectedItemType = 0;

            // Save selected item info
            if (lstAttributes.SelectedValue != null)
            {
                if (lstAttributes.SelectedValue.StartsWith(fieldPreffix))
                {
                    SelectedItemName = lstAttributes.SelectedValue.Substring(preffixLength);
                    SelectedItemType = FieldEditorSelectedItemEnum.Field;
                }
                else if (lstAttributes.SelectedValue.StartsWith(categPreffix))
                {
                    SelectedItemName = lstAttributes.SelectedValue.Substring(preffixLength);
                    SelectedItemType = FieldEditorSelectedItemEnum.Category;
                }
            }
        }
    }


    /// <summary>
    /// Marks catories in list with different color.
    /// </summary>
    private void MarkCategories()
    {
        foreach (ListItem item in lstAttributes.Items)
        {
            // Mark category item with different color
            if (item.Value.StartsWith(categPreffix))
            {
                item.Attributes.Add("class", "FieldEditorCategoryItem");
            }
        }
    }


    /// <summary>
    /// Set all values of the category edit form to defaults.
    /// </summary>
    private void LoadDefaultCategoryEditForm()
    {
        pnlAttributeEdit.Visible = false;
        pnlAdvancedAttributeEdit.Visible = false;
        pnlSimpleAttributeEdit.Visible = false;
        pnlCategoryEdit.Visible = true;
        txtCategoryName.Text = string.Empty;
    }


    /// <summary>
    /// Set all values of form to defaults.
    /// </summary>
    /// <param name="system">True - empty form for node or document attribute should be loaded, False - standard form should be loaded</param>
    private void LoadDefaultAttributeEditForm(bool system)
    {
        pnlAttributeEdit.Visible = true;
        pnlCategoryEdit.Visible = false;

        LoadAttributeTypes();
        if (lstAttributeType.Items.Count > 0)
        {
            lstAttributeType.SelectedIndex = 0;
        }
        LoadFieldTypes(false);
        ShowValidationOptions(false);

        txtAttributeSize.Text = "";
        chkAllowEmpty.Checked = (Mode == FieldEditorModeEnum.SystemTable);
        chkAttributeIsSystem.Checked = DevelopmentMode && !IsDocumentType;
        plcAttributeIsSystem.Visible = DevelopmentMode;
        chkDisplayInForm.Checked = true;
        chkPublicField.Checked = true;
        txtDescription.Text = "";
        txtFieldCaption.Text = "";

        txtSimpleTextBoxMaxLength.Text = "";
        elemAutoResize.ClearControl();
        autoResizeElem.ClearControl();
        if (ucDocAttachSettings != null)
        {
            ucDocAttachSettings.ClearControl();
        }
        elemAdvancedDialogConfig.ClearControl();

        // Set advanced options values        
        txtTextAreaCols.Text = "";
        txtTextAreaRows.Text = "";
        txtTextAreaSize.Text = "";
        txtHTMLAreaHeight.Text = "";
        txtHTMLAreaWidth.Text = "";
        txtHTMLAreaToolbarSet.Text = "";
        txtHTMLAreaStartingPath.Text = "";
        radDropDownListOptions.Checked = true;

        // Fill dropdown list with repeat direction
        if (drpRadioButtonsDirection.Items.Count == 0)
        {
            drpRadioButtonsDirection.Items.Add(new ListItem(ResHelper.GetString("FieldEditor.RadioButtons.RepeatDirection.Vertical"), "vertical"));
            drpRadioButtonsDirection.Items.Add(new ListItem(ResHelper.GetString("FieldEditor.RadioButtons.RepeatDirection.Horizontal"), "horizontal"));
        }
        if (drpRadioButtonsDirection.Items.Count > 0)
        {
            drpRadioButtonsDirection.SelectedIndex = 0;
        }

        // Fill dropdown list with repeat direction
        if (drpMultipleChoiceDirection.Items.Count == 0)
        {
            drpMultipleChoiceDirection.Items.Add(new ListItem(ResHelper.GetString("FieldEditor.RadioButtons.RepeatDirection.Vertical"), "vertical"));
            drpMultipleChoiceDirection.Items.Add(new ListItem(ResHelper.GetString("FieldEditor.RadioButtons.RepeatDirection.Horizontal"), "horizontal"));
            drpMultipleChoiceDirection.SelectedIndex = 0;
        }
        if (drpMultipleChoiceDirection.Items.Count > 0)
        {
            drpMultipleChoiceDirection.SelectedIndex = 0;
        }

        // BBEditor
        txtBBEditorCols.Text = "";
        txtBBEditorRows.Text = "";
        chkAdvancedBBEditorShowBold.Checked = true;
        chkAdvancedBBEditorShowCode.Checked = true;
        chkAdvancedBBEditorShowColor.Checked = true;
        chkAdvancedBBEditorShowItalic.Checked = true;
        chkAdvancedBBEditorShowQuote.Checked = true;
        chkAdvancedBBEditorShowStrike.Checked = true;
        chkAdvancedBBEditorShowUnderline.Checked = true;
        chkAdvancedBBEditorUsePromptDialog.Checked = true;
        radBBUrlSimple.Checked = true;
        radBBImageSimple.Checked = true;

        radDropDownListSQL.Checked = false;
        radMultipleChoiceOptions.Checked = true;
        radMultipleChoiceSQL.Checked = false;
        radListBoxOptions.Checked = true;
        radListBoxSQL.Checked = false;
        txtDropDownListOptions.Text = "";
        txtMultipleChoiceOptions.Text = "";
        txtListBoxOptions.Text = "";
        chkCalendarSettingsEditTime.Checked = true;
        chkCalendarSettingsShowNowLink.Checked = true;
        txtImageHeight.Text = "";
        txtImageWidth.Text = "";
        txtImageMaxSideSize.Text = "";

        // Validation section
        txtRegExpr.Text = "";
        txtMinValue.Text = "";
        txtMaxValue.Text = "";
        txtMinLength.Text = "";
        txtMaxLength.Text = "";
        dateFrom.SelectedDateTime = DateTimePicker.NOT_SELECTED;
        dateTo.SelectedDateTime = DateTimePicker.NOT_SELECTED;
        fileExt.LoadConfiguration(null);//.AllowedExtensions = "";
        txtErrorMessage.Text = "";

        // Design section
        txtCaptionStyle.Text = "";
        txtInputStyle.Text = "";
        txtControlCssClass.Text = "";

        // Default value controls
        txtDefaultValue.Text = "";
        txtLargeDefaultValue.Value = "";
        chkDefaultValue.Checked = false;
        datetimeDefaultValue.SelectedDateTime = DateTimePicker.NOT_SELECTED;

        EnableFieldEditing();
        ShowOrHideFieldDetails();
        ShowAdvancedOptions();

        ShowDefaultValue();
        SetDefaultValue();

        ShowAttributeTypeOptions();
        SetAttributeTypeOptions(false);

        SetAttributeName("", system, IsNewItemEdited);
        if (system)
        {
            LoadSystemField();
        }
        EnableOrDisableAttributeSize();
    }


    /// <summary>
    /// Fill form with selected category data.
    /// </summary>    
    private void LoadSelectedCategory()
    {
        pnlAttributeEdit.Visible = false;
        pnlAdvancedAttributeEdit.Visible = false;
        pnlSimpleAttributeEdit.Visible = false;
        pnlCategoryEdit.Visible = true;

        fci = fi.GetFormCategory(SelectedItemName);
        if (fci != null)
        {
            txtCategoryName.Text = fci.CategoryCaption;
        }
        else
        {
            LoadDefaultCategoryEditForm();
        }
    }


    /// <summary>
    /// Fill form with selected attribute data.
    /// </summary>    
    private void LoadSelectedField()
    {
        pnlAttributeEdit.Visible = true;
        pnlCategoryEdit.Visible = false;

        if (DevelopmentMode)
        {
            lblGuidValue.Text = ffi.Guid.ToString();
        }

        // Fill form
        if (ffi != null)
        {
            IsFieldPrimary = ffi.PrimaryKey;
            FieldDataType = FormHelper.GetFormFieldDataTypeString(ffi.DataType);

            SetAttributeName(ffi.Name, ffi.External, IsNewItemEdited);
            txtAttributeSize.Text = (ffi.Size == 0) ? "" : Convert.ToString(ffi.Size);

            // Load attribute types
            LoadAttributeTypes();
            if (ffi.DataType != FormFieldDataTypeEnum.Unknown)
            {
                try
                {
                    if (((Mode == FieldEditorModeEnum.BizFormDefinition) || (Mode == FieldEditorModeEnum.SystemTable))
                        && (ffi.FieldType == FormFieldControlTypeEnum.UploadControl))
                    {
                        // In "bizform" and "systemtable" modes 'file' data type is representing as nvarchar(500) to allow to save "<guid>.<extenstion>"
                        lstAttributeType.SelectedValue = FormFieldDataTypeCode.FILE;
                    }
                    else
                    {
                        lstAttributeType.SelectedValue = FormHelper.GetFormFieldDataTypeString(ffi.DataType);
                    }
                }
                catch
                {
                    // If not such value exist in the list
                }
            }

            // Load field types
            LoadFieldTypes(ffi.PrimaryKey);
            if (ffi.FieldType != FormFieldControlTypeEnum.Unknown)
            {
                try
                {
                    // Select user control
                    if (ffi.FieldType == FormFieldControlTypeEnum.CustomUserControl)
                    {
                        lstFieldType.SelectedValue = (controlPrefix + Convert.ToString(ffi.Settings["controlname"]).ToLower());
                    }
                    // Select default control
                    else
                    {
                        if (SelectedMode == FieldEditorSelectedModeEnum.Simplified)
                        {
                            if (ffi.FieldType == FormFieldControlTypeEnum.TextBoxControl)
                            {
                                if ((ffi.DataType == FormFieldDataTypeEnum.Integer))
                                {
                                    ffi.FieldType = FormFieldControlTypeEnum.IntegerNumberTextBox;
                                }
                                else if (ffi.DataType == FormFieldDataTypeEnum.LongInteger)
                                {
                                    ffi.FieldType = FormFieldControlTypeEnum.LongNumberTextBox;
                                }
                                else if (ffi.DataType == FormFieldDataTypeEnum.Decimal)
                                {
                                    ffi.FieldType = FormFieldControlTypeEnum.DecimalNumberTextBox;
                                }
                            }
                            else if (ffi.FieldType == FormFieldControlTypeEnum.LabelControl)
                            {   // Add "Label" option to dropdown
                                lstFieldType.Items.Add(new ListItem(ResHelper.GetString("TemplateDesigner.FieldControlTypes.Label"), FormFieldControlTypeCode.LABEL));
                            }
                        }

                        lstFieldType.SelectedValue = FormHelper.GetFormFieldControlTypeString(ffi.FieldType);
                    }
                }
                catch
                {
                    // If not such value exist in the list
                }
            }

            chkAllowEmpty.Checked = ffi.AllowEmpty;
            chkAttributeIsSystem.Checked = (ffi.System || ffi.PrimaryKey);
            plcAttributeIsSystem.Visible = DevelopmentMode;
            chkDisplayInForm.Checked = ffi.Visible;
            txtDescription.Text = ffi.Description;
            chkSpellCheck.Checked = ffi.SpellCheck;
            txtFieldCaption.Text = ffi.Caption;

            // Field visibility
            if (ShowFieldVisibility)
            {
                chkChangeVisibility.Checked = ffi.AllowUserToChangeVisibility;
                ctrlVisibility.Value = ffi.Visibility;

                // Load controls for user visibility
                //this.drpVisibilityControl.DataSource = FormHelper.GetFormUserControlsForDataType(this.lstAttributeType.SelectedValue, false, FieldEditorControlsEnum.Visibility, false);
                drpVisibilityControl.DataSource = FormUserControlInfoProvider.GetFormUserControls("UserControlCodeName, UserControlDisplayName, UserControlShowInBizForms, UserControlShowInDocumentTypes, UserControlShowInSystemTables, UserControlShowInWebParts, UserControlShowInReports, UserControlShowInCustomTables, UserControlForVisibility, UserControlForText, UserControlForLongText, UserControlForInteger, UserControlForDecimal, UserControlForDateTime, UserControlForBoolean, UserControlForFile, UserControlForGuid", "UserControlForVisibility = 1", "UserControlDisplayName");
                drpVisibilityControl.DataBind();

                try
                {
                    ListItem item = drpVisibilityControl.Items.FindByValue(ffi.VisibilityControl);
                    drpVisibilityControl.SelectedValue = ffi.VisibilityControl;
                }
                catch
                {
                }
            }

            if ((Mode == FieldEditorModeEnum.BizFormDefinition) ||
                DisplayedControls == FieldEditorControlsEnum.Bizforms)
            {
                chkPublicField.Checked = ffi.PublicField;
            }

            // Show validation options
            ShowValidationOptions(ffi.PrimaryKey);

            // Set advanced options values            
            txtTextAreaCols.Text = "";
            txtTextAreaRows.Text = "";
            txtTextAreaSize.Text = "";
            txtHTMLAreaHeight.Text = "";
            txtHTMLAreaWidth.Text = "";
            txtHTMLAreaToolbarSet.Text = "";
            txtHTMLAreaStartingPath.Text = "";
            radDropDownListOptions.Checked = true;

            // BBEditor
            txtBBEditorCols.Text = "";
            txtBBEditorRows.Text = "";
            chkAdvancedBBEditorShowBold.Checked = true;
            chkAdvancedBBEditorShowCode.Checked = true;
            chkAdvancedBBEditorShowColor.Checked = true;
            chkAdvancedBBEditorShowItalic.Checked = true;
            chkAdvancedBBEditorShowQuote.Checked = true;
            chkAdvancedBBEditorShowStrike.Checked = true;
            chkAdvancedBBEditorShowUnderline.Checked = true;
            chkAdvancedBBEditorUsePromptDialog.Checked = true;
            radBBImageSimple.Checked = true;
            radBBUrlSimple.Checked = true;

            // Fill dropdown list with repeat direction
            if (drpRadioButtonsDirection.Items.Count == 0)
            {
                drpRadioButtonsDirection.Items.Add(new ListItem(ResHelper.GetString("FieldEditor.RadioButtons.RepeatDirection.Vertical"), "vertical"));
                drpRadioButtonsDirection.Items.Add(new ListItem(ResHelper.GetString("FieldEditor.RadioButtons.RepeatDirection.Horizontal"), "horizontal"));
                drpRadioButtonsDirection.SelectedIndex = 0;
            }

            // Fill dropdown list with repeat direction
            if (drpMultipleChoiceDirection.Items.Count == 0)
            {
                drpMultipleChoiceDirection.Items.Add(new ListItem(ResHelper.GetString("FieldEditor.RadioButtons.RepeatDirection.Vertical"), "vertical"));
                drpMultipleChoiceDirection.Items.Add(new ListItem(ResHelper.GetString("FieldEditor.RadioButtons.RepeatDirection.Horizontal"), "horizontal"));
                drpMultipleChoiceDirection.SelectedIndex = 0;
            }

            radDropDownListSQL.Checked = false;
            radMultipleChoiceOptions.Checked = true;
            radMultipleChoiceSQL.Checked = false;
            radListBoxOptions.Checked = true;
            radListBoxSQL.Checked = false;
            txtDropDownListOptions.Text = "";
            txtMultipleChoiceOptions.Text = "";
            txtListBoxOptions.Text = "";
            chkCalendarSettingsEditTime.Checked = true;
            chkCalendarSettingsShowNowLink.Checked = true;
            txtImageWidth.Text = "";
            txtImageHeight.Text = "";
            txtImageMaxSideSize.Text = "";


            // Validation section
            txtRegExpr.Text = ffi.RegularExpression;
            if (ffi.DataType == FormFieldDataTypeEnum.LongInteger)
            {
                txtMinValue.Text = (ffi.MinLongNumericValue > -1) ? ffi.MinLongNumericValue.ToString() : "";
                txtMaxValue.Text = (ffi.MaxLongNumericValue > -1) ? ffi.MaxLongNumericValue.ToString() : "";
            }
            else
            {
                txtMinValue.Text = (ffi.MinNumericValue > -1) ? ffi.MinNumericValue.ToString() : "";
                txtMaxValue.Text = (ffi.MaxNumericValue > -1) ? ffi.MaxNumericValue.ToString() : "";
            }
            txtMinLength.Text = (ffi.MinStringLength > -1) ? Convert.ToString(ffi.MinStringLength) : "";
            txtMaxLength.Text = (ffi.MaxStringLength > -1) ? Convert.ToString(ffi.MaxStringLength) : "";
            dateFrom.SelectedDateTime = ffi.MinDateTimeValue;
            dateTo.SelectedDateTime = ffi.MaxDateTimeValue;
            txtErrorMessage.Text = ffi.ValidationErrorMessage;

            // Design section
            txtCaptionStyle.Text = ffi.CaptionStyle;
            txtInputStyle.Text = ffi.InputControlStyle;
            txtControlCssClass.Text = ffi.ControlCssClass;

            // Primary key not allowed to change
            // System field not allowed to change unless development mode
            if ((ffi.PrimaryKey) || (ffi.System && !DevelopmentMode))
            {
                DisableFieldEditing(ffi.External);
            }
            else
            {
                EnableFieldEditing();
                EnableDisableSections();
            }


            ShowAdvancedOptions();

            // Fill advanced options        
            switch (ffi.FieldType)
            {
                case FormFieldControlTypeEnum.TextBoxControl:
                    // --------------------------------------------------
                    if (SelectedMode == FieldEditorSelectedModeEnum.Simplified)
                    {
                        txtSimpleTextBoxMaxLength.Text = (ffi.Size == 0) ? "" : Convert.ToString(ffi.Size);
                    }
                    break;

                case FormFieldControlTypeEnum.TextAreaControl:
                    // --------------------------------------------------

                    txtTextAreaCols.Text = Convert.ToString(ffi.Settings["cols"]);
                    txtTextAreaRows.Text = Convert.ToString(ffi.Settings["rows"]);
                    txtTextAreaSize.Text = Convert.ToString(ffi.Settings["size"]);

                    break;

                case FormFieldControlTypeEnum.BBEditorControl:
                    // --------------------------------------------------                    
                    txtBBEditorCols.Text = Convert.ToString(ffi.Settings["cols"]);
                    txtBBEditorRows.Text = Convert.ToString(ffi.Settings["rows"]);
                    txtBBEditorSize.Text = Convert.ToString(ffi.Settings["size"]);

                    chkBBEditorSettingsUsePromptDialog.Checked = ValidationHelper.GetBoolean(ffi.Settings["usepromptdialog"], true);
                    chkBBEditorSettingsShowBold.Checked = ValidationHelper.GetBoolean(ffi.Settings["showbold"], true);
                    chkBBEditorSettingsShowCode.Checked = ValidationHelper.GetBoolean(ffi.Settings["showcode"], true);
                    chkBBEditorSettingsShowColor.Checked = ValidationHelper.GetBoolean(ffi.Settings["showcolor"], true);
                    chkBBEditorSettingsShowItalic.Checked = ValidationHelper.GetBoolean(ffi.Settings["showitalic"], true);
                    chkBBEditorSettingsShowQuote.Checked = ValidationHelper.GetBoolean(ffi.Settings["showquote"], true);
                    chkBBEditorSettingsShowStrike.Checked = ValidationHelper.GetBoolean(ffi.Settings["showstrike"], true);
                    chkBBEditorSettingsShowUnderline.Checked = ValidationHelper.GetBoolean(ffi.Settings["showunderline"], true);
                    radBBImageAdvanced.Checked = ValidationHelper.GetBoolean(ffi.Settings["showadvancedimage"], false);
                    radBBUrlAdvanced.Checked = ValidationHelper.GetBoolean(ffi.Settings["showadvancedurl"], false);
                    radBBImageSimple.Checked = ValidationHelper.GetBoolean(ffi.Settings["showimage"], true);
                    radBBUrlSimple.Checked = ValidationHelper.GetBoolean(ffi.Settings["showurl"], true);
                    radBBImageNo.Checked = !(radBBImageAdvanced.Checked || radBBImageSimple.Checked);
                    radBBUrlNo.Checked = !(radBBUrlAdvanced.Checked || radBBUrlSimple.Checked);
                    elemAdvancedDialogConfig.LoadConfiguration(ffi.Settings);
                    break;

                case FormFieldControlTypeEnum.HtmlAreaControl:
                    // --------------------------------------------------

                    txtHTMLAreaWidth.Text = Convert.ToString(ffi.Settings["width"]);
                    txtHTMLAreaHeight.Text = Convert.ToString(ffi.Settings["height"]);
                    txtHTMLAreaSize.Text = Convert.ToString(ffi.Settings["size"]);

                    usHTMLAreaCss.Value = Convert.ToString(ffi.Settings["cssstylesheet"]);
                    usHTMLAreaCss.Reload(true);

                    txtHTMLAreaToolbarSet.Text = Convert.ToString(ffi.Settings["toolbarset"]);
                    txtHTMLAreaStartingPath.Text = Convert.ToString(ffi.Settings["startingpath"]);
                    elemAdvancedDialogConfig.LoadConfiguration(ffi.Settings);
                    break;

                case FormFieldControlTypeEnum.DropDownListControl:
                case FormFieldControlTypeEnum.RadioButtonsControl:
                    // --------------------------------------------------

                    if (ffi.Settings.ContainsKey("query"))
                    {
                        radDropDownListSQL.Checked = true;
                        radDropDownListOptions.Checked = false;
                        txtDropDownListOptions.Text = HttpUtility.HtmlDecode(Convert.ToString(ffi.Settings["query"]));
                    }

                    else if (ffi.Settings.ContainsKey("options"))
                    {
                        XmlDocument xmlDoc = new XmlDocument();

                        xmlDoc.LoadXml("<options>" + Convert.ToString(ffi.Settings["options"]) + "</options>");

                        if (xmlDoc != null)
                        {
                            foreach (XmlNode optionItem in xmlDoc.SelectNodes("options/item"))
                            {
                                string dataField = optionItem.Attributes["value"].Value.Replace(";", "\\;");
                                string textField = optionItem.Attributes["text"].Value.Replace(";", "\\;");

                                // Display value in correct format due to current culture
                                if (ffi.DataType == FormFieldDataTypeEnum.Decimal)
                                {
                                    dataField = FormHelper.GetDoubleValueInCurrentCulture(dataField);
                                }
                                else if (ffi.DataType == FormFieldDataTypeEnum.DateTime)
                                {
                                    if (!string.IsNullOrEmpty(dataField))
                                    {
                                        dataField = FormHelper.GetDateTimeValueInCurrentCulture(dataField).ToString();
                                    }
                                }

                                txtDropDownListOptions.Text += dataField + ";" + textField + "\n";
                            }
                        }
                    }

                    // Set repeat direction for radio button list control
                    if (ffi.FieldType == FormFieldControlTypeEnum.RadioButtonsControl)
                    {
                        try
                        {
                            drpRadioButtonsDirection.SelectedValue = Convert.ToString(ffi.Settings["repeatdirection"]);
                        }
                        catch
                        {
                        }
                    }
                    break;


                case FormFieldControlTypeEnum.MultipleChoiceControl:
                    // --------------------------------------------------

                    if (ffi.Settings.ContainsKey("query"))
                    {
                        radMultipleChoiceSQL.Checked = true;
                        radMultipleChoiceOptions.Checked = false;
                        txtMultipleChoiceOptions.Text = HttpUtility.HtmlDecode(Convert.ToString(ffi.Settings["query"]));
                    }

                    else if (ffi.Settings.ContainsKey("options"))
                    {
                        XmlDocument xmlDoc = new XmlDocument();

                        xmlDoc.LoadXml("<options>" + Convert.ToString(ffi.Settings["options"] + "</options>"));

                        if (xmlDoc != null)
                        {
                            foreach (XmlNode optionItem in xmlDoc.SelectNodes("options/item"))
                            {
                                txtMultipleChoiceOptions.Text += optionItem.Attributes["value"].Value.Replace(";", "\\;") + ";" + optionItem.Attributes["text"].Value.Replace(";", "\\;") + "\n";
                            }
                        }
                    }

                    // Set repeat direction for checkbox list control
                    try
                    {
                        drpMultipleChoiceDirection.SelectedValue = Convert.ToString(ffi.Settings["repeatdirection"]);
                    }
                    catch
                    {
                    }

                    break;

                case FormFieldControlTypeEnum.ListBoxControl:
                    // --------------------------------------------------

                    if (ffi.Settings.ContainsKey("query"))
                    {
                        radListBoxSQL.Checked = true;
                        radListBoxOptions.Checked = false;
                        txtListBoxOptions.Text = HttpUtility.HtmlDecode(Convert.ToString(ffi.Settings["query"]));
                    }

                    else if (ffi.Settings.ContainsKey("options"))
                    {
                        XmlDocument xmlDoc = new XmlDocument();

                        xmlDoc.LoadXml("<options>" + Convert.ToString(ffi.Settings["options"] + "</options>"));

                        if (xmlDoc != null)
                        {
                            foreach (XmlNode optionItem in xmlDoc.SelectNodes("options/item"))
                            {
                                txtListBoxOptions.Text += optionItem.Attributes["value"].Value.Replace(";", "\\;") + ";" + optionItem.Attributes["text"].Value.Replace(";", "\\;") + "\n";
                            }
                        }
                    }

                    // Set "allow multiple choices" for list listbox control
                    chkListBoxMultipleChoices.Checked = ValidationHelper.GetBoolean(ffi.Settings["allowmultiplechoices"], true);

                    break;

                case FormFieldControlTypeEnum.CalendarControl:
                    // --------------------------------------------------

                    if (Convert.ToString(ffi.Settings["editTime"]).ToLower() == "true")
                    {
                        chkCalendarSettingsEditTime.Checked = true;
                    }
                    else
                    {
                        chkCalendarSettingsEditTime.Checked = false;
                    }

                    chkCalendarSettingsShowNowLink.Checked = ValidationHelper.GetBoolean(ffi.Settings["displayNow"], true);
                    break;

                case FormFieldControlTypeEnum.ImageSelectionControl:
                case FormFieldControlTypeEnum.MediaSelectionControl:
                case FormFieldControlTypeEnum.FileSelectionControl:
                    // --------------------------------------------------

                    if (ffi.FieldType == FormFieldControlTypeEnum.ImageSelectionControl)
                    {
                        txtImageWidth.Text = Convert.ToString(ffi.Settings["width"]);
                        txtImageHeight.Text = Convert.ToString(ffi.Settings["height"]);
                        txtImageMaxSideSize.Text = Convert.ToString(ffi.Settings["maxsidesize"]);
                    }
                    autoResizeElem.LoadConfiguration(ffi.Settings);
                    break;

                // Image auto resize on upload
                case FormFieldControlTypeEnum.UploadControl:
                case FormFieldControlTypeEnum.DirectUploadControl:
                    elemAutoResize.LoadConfiguration(ffi.Settings);
                    if (string.IsNullOrEmpty(ffi.FileExtensions))
                    {
                        fileExt.LoadConfiguration(ffi.Settings);
                    }
                    else
                    {
                        fileExt.AllowedExtensions = ffi.FileExtensions;
                    }

                    if (ucDocAttachSettings != null)
                    {
                        ucDocAttachSettings.LoadConfiguration(ffi.Settings);
                    }
                    break;

                // Document attachments
                case FormFieldControlTypeEnum.DocumentAttachmentsControl:
                    if (ucDocAttachSettings != null)
                    {
                        ucDocAttachSettings.LoadConfiguration(ffi.Settings);
                    }
                    // Disable allow empty value selection
                    chkAllowEmpty.Checked = true;
                    chkAllowEmpty.Enabled = false;
                    break;
            }

            EnableOrDisableAttributeSize();

            ShowOrHideFieldDetails();

            ShowDefaultValue();
            SetDefaultValue();

            ShowAttributeTypeOptions();
            SetAttributeTypeOptions(true);
        }
        else
        {
            LoadDefaultAttributeEditForm(false);
        }
    }


    /// <summary>
    /// Enables or disables sections of the unigrid according to the selected mode.
    /// </summary>
    protected void EnableDisableSections()
    {
        bool enabled = (mMode != FieldEditorModeEnum.AlternativeFormDefinition);

        //lblAttributeName.Enabled = false;
        //txtAttributeName.Enabled = enabled;
        EnableDisableAttributeName(enabled);

        //lblAttributeType.Enabled = enabled;
        lstAttributeType.Enabled = enabled;

        //lblAttributeSize.Enabled = enabled;
        txtAttributeSize.Enabled = enabled;
    }


    /// <summary>
    /// Displays or hides actions according to the selected mode
    /// </summary>
    protected void DisplayOrHideActions()
    {
        // Hide actions only when alternative form definition is edited
        if (mMode == FieldEditorModeEnum.AlternativeFormDefinition)
        {
            plcActions.Visible = false;
        }
    }


    /// <summary>
    /// Displays or hides source field selection, according to mode.
    /// </summary>
    protected void DisplayOrHideSourceFieldSelection()
    {
        pnlSourceField.Visible = false;

        // Display or hide source field selection
        if (mMode == FieldEditorModeEnum.ClassFormDefinition)
        {
            // Fill source field drop down list
            if (DisplaySourceFieldSelection)
            {
                pnlSourceField.Visible = true;

                // Add implicit list item
                drpSourceField.Items.Clear();
                drpSourceField.Items.Add(new ListItem(ResHelper.GetString("TemplateDesigner.ImplicitSourceField"), ""));

                drpSourceAliasField.Items.Clear();
                drpSourceAliasField.Items.Add(new ListItem(ResHelper.GetString("TemplateDesigner.DefaultSourceField"), ""));

                drpSourceAliasField.Items.Add(new ListItem("NodeID", "NodeID"));
                drpSourceAliasField.Items.Add(new ListItem("DocumentID", "DocumentID"));

                string[] columnNames = fi.GetColumnNames();
                if (columnNames != null)
                {
                    // Add attribute list item to the list of attributes
                    foreach (string name in columnNames)
                    {
                        FormFieldInfo ffiColumn = fi.GetFormField(name);
                        // Don't add primary key field
                        if (!ffiColumn.PrimaryKey)
                        {
                            drpSourceField.Items.Add(new ListItem(name, name));
                        }
                        drpSourceAliasField.Items.Add(new ListItem(name, name));
                    }
                }

                // Set selected value
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(ClassName);
                if (dci != null)
                {
                    try
                    {
                        drpSourceField.SelectedValue = dci.ClassNodeNameSource;
                        drpSourceAliasField.SelectedValue = dci.ClassNodeAliasSource;
                    }
                    catch
                    {
                    }
                }
            }
        }
    }


    /// <summary>
    /// When attribute up button is clicked
    /// </summary>
    protected void btnUpAttribute_Click(Object sender, System.Web.UI.ImageClickEventArgs e)
    {
        // Raise on before definition update event
        if (OnBeforeDefinitionUpdate != null)
        {
            OnBeforeDefinitionUpdate();
        }


        if (Mode == FieldEditorModeEnum.BizFormDefinition)
        {
            // Check 'EditForm' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "EditForm"))
            {
                RedirectToAccessDenied("cms.form", "EditForm");
            }
        }

        LoadFormDefinition();

        // First item of the attribute list cannot be moved higher
        if (string.IsNullOrEmpty(lstAttributes.SelectedValue) || (lstAttributes.SelectedIndex == 0))
        {
            return;
        }
        // 'new (not saved)' attribute cannot be moved
        else if ((SelectedItemName == newCategPreffix) || (SelectedItemName == newFieldPreffix))
        {
            ShowMessage(ResHelper.GetString("TemplateDesigner.AlertNewAttributeCannotBeMoved"));
            return;
        }

        if (SelectedItemType == FieldEditorSelectedItemEnum.Field)
        {
            // Move attribute up in attribute list                        
            fi.MoveFormFieldUp(SelectedItemName);
        }
        else if (SelectedItemType == FieldEditorSelectedItemEnum.Category)
        {
            // Move category up in attribute list                        
            fi.MoveFormCategoryUp(SelectedItemName);
        }

        // Update form definition
        FormDefinition = fi.GetXmlDefinition();

        switch (mMode)
        {
            case FieldEditorModeEnum.WebPartProperties:
                // Save xml string to CMS_WebPart table
                WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(mWebPartId);
                if (wpi != null)
                {
                    wpi.WebPartProperties = FormDefinition;
                    WebPartInfoProvider.SetWebPartInfo(wpi);
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "[FieldEditor.btnUpAttribute_Click()]: " + ResHelper.GetString("FieldEditor.WebpartNotFound");
                }
                break;

            case FieldEditorModeEnum.ClassFormDefinition:
            case FieldEditorModeEnum.BizFormDefinition:
            case FieldEditorModeEnum.SystemTable:
            case FieldEditorModeEnum.CustomTable:
                // Save xml string to CMS_Class table
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(mClassName);
                if (dci != null)
                {
                    dci.ClassFormDefinition = FormDefinition;

                    // Do not log synchronization for BizForm
                    if (mMode == FieldEditorModeEnum.BizFormDefinition)
                    {
                        dci.DisableLogging();
                    }

                    DataClassInfoProvider.SetDataClass(dci);
                    dci.EnableLogging();
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "[FieldEditor.btnUpAttribute_Click()]: " + ResHelper.GetString("FieldEditor.ClassNotFound");
                }
                break;
        }

        // Reload attibute list
        LoadAttributesList(lstAttributes.SelectedValue);

        if (OnAfterDefinitionUpdate != null)
        {
            OnAfterDefinitionUpdate();
        }
    }


    /// <summary>
    /// When attribute down button is clicked
    /// </summary>
    protected void btnDownAttribute_Click(Object sender, System.Web.UI.ImageClickEventArgs e)
    {
        // Raise on before definition update event
        if (OnBeforeDefinitionUpdate != null)
        {
            OnBeforeDefinitionUpdate();
        }

        if (Mode == FieldEditorModeEnum.BizFormDefinition)
        {
            // Check 'EditForm' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "EditForm"))
            {
                RedirectToAccessDenied("cms.form", "EditForm");
            }
        }

        LoadFormDefinition();

        // Last item of the attribute list cannot be moved lower
        if (string.IsNullOrEmpty(lstAttributes.SelectedValue) || lstAttributes.SelectedIndex >= lstAttributes.Items.Count - 1)
        {
            return;
        }
        // 'new and not saved' attribute cannot be moved
        else if ((SelectedItemName == newCategPreffix) || (SelectedItemName == newFieldPreffix))
        {
            ShowMessage(ResHelper.GetString("TemplateDesigner.AlertNewAttributeCannotBeMoved"));
            return;
        }

        if (SelectedItemType == FieldEditorSelectedItemEnum.Field)
        {
            // Move attribute down in attribute list                        
            fi.MoveFormFieldDown(SelectedItemName);
        }
        else if (SelectedItemType == FieldEditorSelectedItemEnum.Category)
        {
            // Move category down in attribute list                        
            fi.MoveFormCategoryDown(SelectedItemName);
        }

        // Update form definition
        FormDefinition = fi.GetXmlDefinition();

        switch (mMode)
        {
            case FieldEditorModeEnum.WebPartProperties:
                // Save xml string to CMS_WebPart table
                WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(mWebPartId);
                if (wpi != null)
                {
                    wpi.WebPartProperties = FormDefinition;
                    WebPartInfoProvider.SetWebPartInfo(wpi);
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "[FieldEditor.btnDownAttribute_Click()]: " + ResHelper.GetString("FieldEditor.WebpartNotFound");
                }
                break;

            case FieldEditorModeEnum.ClassFormDefinition:
            case FieldEditorModeEnum.BizFormDefinition:
            case FieldEditorModeEnum.SystemTable:
            case FieldEditorModeEnum.CustomTable:
                // Save xml string to CMS_Class table
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(mClassName);
                if (dci != null)
                {
                    dci.ClassFormDefinition = FormDefinition;

                    // Do not log synchronization for BizForm
                    if (mMode == FieldEditorModeEnum.BizFormDefinition)
                    {
                        dci.DisableLogging();
                    }

                    DataClassInfoProvider.SetDataClass(dci);
                    dci.EnableLogging();
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "[FieldEditor.btnDownAttribute_Click()]: " + ResHelper.GetString("FieldEditor.ClassNotFound");
                }
                break;
        }

        // Reload attibute list
        LoadAttributesList(lstAttributes.SelectedValue);

        // Raise on after definition update event
        if (OnAfterDefinitionUpdate != null)
        {
            OnAfterDefinitionUpdate();
        }
    }


    /// <summary>
    /// When chkDisplayInForm checkbox checked changed
    /// </summary>
    protected void chkDisplayInForm_CheckedChanged(Object sender, EventArgs e)
    {
        ShowOrHideFieldDetails();
    }


    /// <summary>
    /// Selected attribute changed event handler.
    /// </summary>
    protected void lstAttributes_SelectedIndexChanged(Object sender, EventArgs e)
    {
        bool isNewCreated = false;

        // Check if new attribute is edited -> select it and avoid selecting another attribute
        foreach (ListItem item in lstAttributes.Items)
        {
            switch (item.Value)
            {
                case newCategPreffix:
                    isNewCreated = true;
                    lstAttributes.SelectedValue = newCategPreffix;
                    break;

                case newFieldPreffix:
                    isNewCreated = true;
                    lstAttributes.SelectedValue = newFieldPreffix;
                    break;
            }

            if (isNewCreated)
            {
                ShowMessage(ResHelper.GetString("TemplateDesigner.AlertSaveNewItemOrDeleteItFirst"));
                if (IsSystemFieldSelected)
                {
                    DisableFieldEditing(true);
                }
                else
                {
                    EnableFieldEditing();
                }
                return;
            }
        }

        // Reload data
        Reload(lstAttributes.SelectedValue);
    }


    /// <summary>
    /// Show or hide details according to chkDisplayInForm checkbox is checked or not.
    /// </summary>   
    private void ShowOrHideFieldDetails()
    {
        if ((chkDisplayInForm.Checked) || (SelectedMode == FieldEditorSelectedModeEnum.Simplified))
        {
            ShowFieldDetails();
        }
        else
        {
            HideFieldDetails();
        }
    }


    /// <summary>
    /// Show field details
    /// </summary>
    private void ShowFieldDetails()
    {
        pnlDisplayInForm.Enabled = true;
        txtFieldCaption.ReadOnly = false;
        txtDescription.ReadOnly = false;
        /*   if (!(this.lstFieldType.Attributes["disabled"] == null))
           {
               this.lstFieldType.Attributes.Remove("disabled");
           }*/
    }


    /// <summary>
    /// Hide field details
    /// </summary>
    private void HideFieldDetails()
    {
        pnlDisplayInForm.Enabled = false;
        txtFieldCaption.ReadOnly = true;
        txtDescription.ReadOnly = true;

        //  this.lstFieldType.Attributes.Add("disabled", "true");
    }


    /// <summary>
    /// Disables field editing controls.
    /// </summary>
    /// <param name="enableName">Indicates if field name should be enabled.</param>
    private void DisableFieldEditing(bool enableName)
    {
        txtDefaultValue.Enabled = false;
        txtLargeDefaultValue.Enabled = false;
        chkDefaultValue.Enabled = false;
        datetimeDefaultValue.Enabled = false;
        lstAttributeType.Enabled = false;
        chkAllowEmpty.Enabled = false;
        //this.txtAttributeName.Enabled = false;
        EnableDisableAttributeName(enableName);

        chkAttributeIsSystem.Enabled = false;
        txtAttributeSize.Enabled = false;
    }


    /// <summary>
    /// Enables field editing controls, except field name
    /// </summary>
    private void EnableFieldEditing()
    {
        txtDefaultValue.Enabled = true;
        txtLargeDefaultValue.Enabled = true;
        chkDefaultValue.Enabled = true;
        datetimeDefaultValue.Enabled = true;
        lstAttributeType.Enabled = true;
        chkAllowEmpty.Enabled = (Mode != FieldEditorModeEnum.SystemTable);

        if (SelectedMode == FieldEditorSelectedModeEnum.Simplified)
        {
            //this.txtAttributeName.Enabled = IsNewItemEdited;
            EnableDisableAttributeName(IsNewItemEdited);
        }
        else
        {
            //this.txtAttributeName.Enabled = true;
            EnableDisableAttributeName(true);
        }
        chkAttributeIsSystem.Enabled = true;
    }


    /// <summary>
    /// Save/update button clicked
    /// </summary>
    protected void btnOK_Click(Object sender, EventArgs e)
    {
        // Raise on after definition update event
        if (OnBeforeDefinitionUpdate != null)
        {
            OnBeforeDefinitionUpdate();
        }

        if (Mode == FieldEditorModeEnum.BizFormDefinition)
        {
            // Check 'EditForm' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "EditForm"))
            {
                RedirectToAccessDenied("cms.form", "EditForm");
            }
        }

        string errorMessage = "";

        if (SelectedItemType == FieldEditorSelectedItemEnum.Field)
        {
            // Validate basic options
            errorMessage += ValidateForm();

            // Validate advanced options
            errorMessage += ValidateAdvancedOptions();
        }

        // Check occurred errors
        if (!string.IsNullOrEmpty(errorMessage.Trim()))
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
        else
        {
            //try
            //{
            lblError.Text = string.Empty;
            // Save selected field
            SaveSelectedField();

            ClearHashtables();
            //}
            //catch (Exception ex)
            //{
            //    this.lblError.Visible = true;
            //    lblError.Text = "[ FieldEditor.btnOK_Click() ]: " + ex.Message;
            //}
        }

        // Raise on after definition update event
        if (OnAfterDefinitionUpdate != null)
        {
            OnAfterDefinitionUpdate();
        }
    }


    /// <summary>
    /// Save selected field.
    /// </summary>
    private void SaveSelectedField()
    {
        // FormFieldInfo structure with data from updated form
        FormFieldInfo ffiUpdated = null;
        // FormCategoryInfo structure with data from updated form
        FormCategoryInfo fciUpdated = null;
        // Determines whether it is a new attribute (or attribute to update)
        bool isNewItem = false;
        string errorMessage = null;
        DataClassInfo dci = null;
        WebPartInfo wpi = null;

        // Variables for changes in DB tables
        string tableName = null;
        string oldColumnName = null;
        string newColumnName = null;
        string newColumnSize = null;
        string newColumnType = null;
        string newColumnDefaultValue = null;  // No default value
        bool newColumnAllowNull = true;

        switch (mMode)
        {
            case FieldEditorModeEnum.WebPartProperties:
                // Fill WebPartInfo structure with data from database
                wpi = WebPartInfoProvider.GetWebPartInfo(mWebPartId);
                break;

            case FieldEditorModeEnum.ClassFormDefinition:
            case FieldEditorModeEnum.BizFormDefinition:
            case FieldEditorModeEnum.SystemTable:
            case FieldEditorModeEnum.CustomTable:
                // Fill ClassInfo structure with data from database
                dci = DataClassInfoProvider.GetDataClass(mClassName);
                if (dci != null)
                {
                    // Set table name 
                    tableName = dci.ClassTableName;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "Table name is not set.";
                    return;
                }
                break;
        }

        // Load current xml form definition
        LoadFormDefinition();

        if (SelectedItemType == FieldEditorSelectedItemEnum.Field)
        {
            // Fill FormFieldInfo structure with original data
            ffi = fi.GetFormField(SelectedItemName);

            // Fill FormFieldInfo structure with updated form data
            ffiUpdated = FillFormFieldInfoStructure(ffi);

            // Determine whether it is a new attribute or not
            isNewItem = (ffi == null);


            // Check if the attribute name already exists
            //-----------------------------------------        
            if (isNewItem || (ffi.Name.ToLower() != ffiUpdated.Name.ToLower()))
            {
                columnNames = fi.GetColumnNames();

                if (columnNames != null)
                {
                    foreach (string colName in columnNames)
                    {
                        // If name already exists
                        if (ffiUpdated.Name.ToLower() == colName.ToLower())
                        {
                            lblError.Visible = true;
                            lblError.Text = ResHelper.GetString("TemplateDesigner.ErrorExistingColumnName");
                            return;
                        }
                    }
                }

                if (!IsSystemFieldSelected && (Mode != FieldEditorModeEnum.General) && (Mode != FieldEditorModeEnum.WebPartProperties))
                {
                    // Check whether current column already exists in system tables
                    if (DocumentHelper.ColumnExistsInSystemTable(ffiUpdated.Name))
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("TemplateDesigner.ErrorExistingColumnInSysTable");
                        return;
                    }
                }
            }

            // ------------------------------
            // New node                
            // ------------------------------   
            if (isNewItem)
            {
                ffiUpdated.PrimaryKey = false;
                // ffiUpdated.System = this.DevelopmentMode;

                newColumnName = ffiUpdated.Name;
                newColumnAllowNull = ffiUpdated.AllowEmpty;

                // Set implicit default value
                if (!(newColumnAllowNull) && (string.IsNullOrEmpty(ffiUpdated.DefaultValue)))
                {
                    switch (ffiUpdated.DataType)
                    {
                        case FormFieldDataTypeEnum.Integer:
                        case FormFieldDataTypeEnum.LongInteger:
                        case FormFieldDataTypeEnum.Decimal:
                        case FormFieldDataTypeEnum.Boolean:

                            newColumnDefaultValue = "0";

                            break;


                        case FormFieldDataTypeEnum.Text:
                        case FormFieldDataTypeEnum.LongText:
                        case FormFieldDataTypeEnum.DocumentAttachments:

                            newColumnDefaultValue = "";
                            break;


                        case FormFieldDataTypeEnum.DateTime:
                            newColumnDefaultValue = DateTime.Now.ToString();
                            break;

                        case FormFieldDataTypeEnum.File:
                        case FormFieldDataTypeEnum.GUID:
                            // 32 digits, empty Guid
                            newColumnDefaultValue = Guid.Empty.ToString();
                            break;
                    }
                }

                // Check if default value is in required format
                else if (!string.IsNullOrEmpty(ffiUpdated.DefaultValue))
                {
                    // If default value is macro, don't try to ensure the type
                    if (!ffiUpdated.IsMacro)
                    {
                        switch (ffiUpdated.DataType)
                        {
                            case FormFieldDataTypeEnum.Integer:
                                try
                                {
                                    int i = Int32.Parse(ffiUpdated.DefaultValue);
                                    newColumnDefaultValue = i.ToString();
                                }
                                catch
                                {
                                    newColumnDefaultValue = "0";
                                    errorMessage = ResHelper.GetString("TemplateDesigner.ErrorDefaultValueInteger");
                                }
                                break;


                            case FormFieldDataTypeEnum.LongInteger:
                                try
                                {
                                    long longInt = long.Parse(ffiUpdated.DefaultValue);
                                    newColumnDefaultValue = longInt.ToString();
                                }
                                catch
                                {
                                    newColumnDefaultValue = "0";
                                    errorMessage = ResHelper.GetString("TemplateDesigner.ErrorDefaultValueLongInteger");
                                }
                                break;

                            case FormFieldDataTypeEnum.Decimal:
                                if (ValidationHelper.IsDouble(ffiUpdated.DefaultValue))
                                {
                                    newColumnDefaultValue = FormHelper.GetDoubleValueInDBCulture(ffiUpdated.DefaultValue);
                                }
                                else
                                {
                                    newColumnDefaultValue = "0";
                                    errorMessage = ResHelper.GetString("TemplateDesigner.ErrorDefaultValueDouble");
                                }
                                break;

                            case FormFieldDataTypeEnum.DateTime:
                                if ((ffiUpdated.DefaultValue.ToLower() == DateTimePicker.DATE_TODAY.ToLower()) || (ffiUpdated.DefaultValue.ToLower() == DateTimePicker.TIME_NOW.ToLower()))
                                {
                                    newColumnDefaultValue = ffiUpdated.DefaultValue;
                                }
                                else
                                {
                                    try
                                    {
                                        DateTime dat = DateTime.Parse(ffiUpdated.DefaultValue);
                                        newColumnDefaultValue = dat.ToString();
                                    }
                                    catch
                                    {
                                        newColumnDefaultValue = DateTime.Now.ToString();
                                        errorMessage = ResHelper.GetString("TemplateDesigner.ErrorDefaultValueDateTime");
                                    }
                                }
                                break;

                            case FormFieldDataTypeEnum.File:
                            case FormFieldDataTypeEnum.GUID:
                                try
                                {
                                    Guid g = new Guid(ffiUpdated.DefaultValue);
                                    newColumnDefaultValue = g.ToString();
                                }
                                catch
                                {
                                    newColumnDefaultValue = Guid.Empty.ToString();
                                    errorMessage = ResHelper.GetString("TemplateDesigner.ErrorDefaultValueGuid");
                                }
                                break;

                            case FormFieldDataTypeEnum.LongText:
                            case FormFieldDataTypeEnum.Text:
                            case FormFieldDataTypeEnum.Boolean:

                                newColumnDefaultValue = ffiUpdated.DefaultValue;
                                break;
                        }
                    }
                }

                // Set column type and size
                LoadColumnTypeAndSize(ffiUpdated.DataType, ffiUpdated.Size, ref newColumnType, ref newColumnSize);

                if (string.IsNullOrEmpty(errorMessage))
                {
                    switch (mMode)
                    {
                        case FieldEditorModeEnum.ClassFormDefinition:
                        case FieldEditorModeEnum.BizFormDefinition:
                        case FieldEditorModeEnum.SystemTable:
                        case FieldEditorModeEnum.CustomTable:


                            // Add new column to specified table  
                            try
                            {
                                string newDBDefaultValue = null;

                                // Check if it is not a macro
                                if (ffiUpdated.IsMacro)
                                {
                                    newDBDefaultValue = newColumnDefaultValue;
                                }
                                else
                                {
                                    switch (ffiUpdated.DataType)
                                    {
                                        case FormFieldDataTypeEnum.Decimal:
                                            newDBDefaultValue = FormHelper.GetDoubleValueInDBCulture(newColumnDefaultValue);
                                            break;

                                        case FormFieldDataTypeEnum.DateTime:
                                            newDBDefaultValue = FormHelper.GetDateTimeValueInDBCulture(newColumnDefaultValue);
                                            break;
                                        default:
                                            newDBDefaultValue = newColumnDefaultValue;
                                            break;
                                    }
                                }

                                if (!ffiUpdated.External)
                                {
                                    TableManager.AddTableColumn(tableName, newColumnName, newColumnType, newColumnAllowNull, newDBDefaultValue);
                                }
                            }
                            catch (Exception ex)
                            {
                                lblError.Visible = true;
                                lblError.Text = ex.Message;
                                return;
                            }

                            break;
                    }
                }
                // Some error has occurred
                else
                {
                    lblError.Visible = true;
                    lblError.Text = errorMessage;
                    return;
                }
            }
            // ------------------------------
            // Existing node
            // ------------------------------
            else
            {
                // Get info whether it is a primary key or system fild
                ffiUpdated.PrimaryKey = ffi.PrimaryKey;
                //ffiUpdated.System = ffi.System;

                // If attribute is a primary key
                if (ffi.PrimaryKey)
                {
                    // Check if the attribute type is integer number
                    if (ffiUpdated.DataType != FormFieldDataTypeEnum.Integer)
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorPKNotInteger") + " ";
                    }

                    // Check if allow empty is disabled
                    if (ffiUpdated.AllowEmpty)
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorPKAllowsNulls") + " ";
                    }

                    // Check that the field type is label
                    if (ffiUpdated.FieldType != FormFieldControlTypeEnum.LabelControl)
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorPKisNotLabel") + " ";
                    }

                    // Some error has occurred
                    if (!string.IsNullOrEmpty(errorMessage))
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("TemplateDesigner.ErrorPKThisIsPK") + " " + errorMessage;
                        return;
                    }
                }

                // If table column update is needed
                if (((ffi.PrimaryKey) && (ffi.Name != ffiUpdated.Name)) ||
                     ((!ffi.PrimaryKey) &&
                        ((ffi.Name != ffiUpdated.Name) ||
                         (ffi.DataType != ffiUpdated.DataType) ||
                         (ffi.AllowEmpty != ffiUpdated.AllowEmpty) ||
                         (ffi.Size != ffiUpdated.Size) ||
                         ((ffi.DefaultValue != ffiUpdated.DefaultValue) || (ffiUpdated.DataType == FormFieldDataTypeEnum.Decimal)))
                     )
                   )
                {
                    // Set variables needed for changes in DB                
                    oldColumnName = ffi.Name;
                    newColumnName = ffiUpdated.Name;
                    newColumnAllowNull = ffiUpdated.AllowEmpty;

                    // Set implicit default value
                    if (!(newColumnAllowNull) && (string.IsNullOrEmpty(ffiUpdated.DefaultValue)))
                    {
                        switch (ffiUpdated.DataType)
                        {
                            case FormFieldDataTypeEnum.Integer:
                            case FormFieldDataTypeEnum.LongInteger:
                            case FormFieldDataTypeEnum.Decimal:
                            case FormFieldDataTypeEnum.Boolean:

                                newColumnDefaultValue = "0";

                                break;


                            case FormFieldDataTypeEnum.Text:
                            case FormFieldDataTypeEnum.LongText:

                                newColumnDefaultValue = "";
                                break;

                            case FormFieldDataTypeEnum.DateTime:

                                newColumnDefaultValue = DateTime.Now.ToString();
                                break;

                            case FormFieldDataTypeEnum.File:
                            case FormFieldDataTypeEnum.GUID:
                                // 32 digits, empty Guid
                                newColumnDefaultValue = Guid.Empty.ToString();
                                break;
                        }
                    }

                    // Check if default value is in required format
                    else if (!string.IsNullOrEmpty(ffiUpdated.DefaultValue))
                    {
                        // If default value is macro, don't try to ensure the type
                        if (!ffiUpdated.IsMacro)
                        {
                            switch (ffiUpdated.DataType)
                            {
                                case FormFieldDataTypeEnum.Integer:
                                    try
                                    {
                                        int i = Int32.Parse(ffiUpdated.DefaultValue);
                                        newColumnDefaultValue = i.ToString();
                                    }
                                    catch
                                    {
                                        newColumnDefaultValue = "0";
                                        errorMessage = ResHelper.GetString("TemplateDesigner.ErrorDefaultValueInteger");
                                    }
                                    break;


                                case FormFieldDataTypeEnum.LongInteger:
                                    try
                                    {
                                        long longInt = long.Parse(ffiUpdated.DefaultValue);
                                        newColumnDefaultValue = longInt.ToString();
                                    }
                                    catch
                                    {
                                        newColumnDefaultValue = "0";
                                        errorMessage = ResHelper.GetString("TemplateDesigner.ErrorDefaultValueLongInteger");
                                    }
                                    break;

                                case FormFieldDataTypeEnum.Decimal:
                                    if (ValidationHelper.IsDouble(ffiUpdated.DefaultValue))
                                    {
                                        newColumnDefaultValue = FormHelper.GetDoubleValueInDBCulture(ffiUpdated.DefaultValue);
                                    }
                                    else
                                    {
                                        newColumnDefaultValue = "0";
                                        errorMessage = ResHelper.GetString("TemplateDesigner.ErrorDefaultValueDouble");
                                    }
                                    break;

                                case FormFieldDataTypeEnum.DateTime:
                                    if ((ffiUpdated.DefaultValue.ToLower() == DateTimePicker.DATE_TODAY.ToLower()) || (ffiUpdated.DefaultValue.ToLower() == DateTimePicker.TIME_NOW.ToLower()))
                                    {
                                        newColumnDefaultValue = ffiUpdated.DefaultValue;
                                    }
                                    else
                                    {
                                        try
                                        {
                                            DateTime dat = DateTime.Parse(ffiUpdated.DefaultValue);
                                            newColumnDefaultValue = dat.ToString();
                                        }
                                        catch
                                        {
                                            newColumnDefaultValue = DateTime.Now.ToString();
                                            errorMessage = ResHelper.GetString("TemplateDesigner.ErrorDefaultValueDateTime");
                                        }
                                    }
                                    break;

                                case FormFieldDataTypeEnum.File:
                                case FormFieldDataTypeEnum.GUID:
                                    try
                                    {
                                        Guid g = new Guid(ffiUpdated.DefaultValue);
                                        newColumnDefaultValue = g.ToString();
                                    }
                                    catch
                                    {
                                        newColumnDefaultValue = Guid.Empty.ToString();
                                        errorMessage = ResHelper.GetString("TemplateDesigner.ErrorDefaultValueGuid");
                                    }
                                    break;

                                case FormFieldDataTypeEnum.LongText:
                                case FormFieldDataTypeEnum.Text:
                                case FormFieldDataTypeEnum.Boolean:

                                    newColumnDefaultValue = ffiUpdated.DefaultValue;
                                    break;
                            }
                        }
                    }

                    // Set column type and size
                    LoadColumnTypeAndSize(ffiUpdated.DataType, ffiUpdated.Size, ref newColumnType, ref newColumnSize);

                    if (string.IsNullOrEmpty(errorMessage))
                    {
                        switch (mMode)
                        {
                            case FieldEditorModeEnum.ClassFormDefinition:
                            case FieldEditorModeEnum.BizFormDefinition:
                            case FieldEditorModeEnum.SystemTable:
                            case FieldEditorModeEnum.CustomTable:

                                try
                                {
                                    string newDBDefaultValue = null;

                                    // Check if it is not a macro
                                    if (ffiUpdated.IsMacro)
                                    {
                                        newDBDefaultValue = newColumnDefaultValue;
                                    }
                                    else
                                    {
                                        switch (ffiUpdated.DataType)
                                        {
                                            case FormFieldDataTypeEnum.Decimal:
                                                newDBDefaultValue = FormHelper.GetDoubleValueInDBCulture(newColumnDefaultValue);
                                                break;

                                            case FormFieldDataTypeEnum.DateTime:
                                                newDBDefaultValue = FormHelper.GetDateTimeValueInDBCulture(newColumnDefaultValue);
                                                break;
                                            default:
                                                newDBDefaultValue = newColumnDefaultValue;
                                                break;
                                        }
                                    }

                                    if (ffiUpdated.External)
                                    {
                                        if (ffi.External)
                                        {
                                            // No changes in DB
                                        }
                                        else
                                        {
                                            // Drop old column from table
                                            TableManager.DropTableColumn(tableName, ffi.Name);
                                        }
                                    }
                                    else
                                    {
                                        if (ffi.External)
                                        {
                                            // Add table column
                                            TableManager.AddTableColumn(tableName, newColumnName, newColumnType, newColumnAllowNull, newDBDefaultValue);
                                        }
                                        else
                                        {
                                            // Change table column
                                            TableManager.AlterTableColumn(tableName, oldColumnName, newColumnName, newColumnType, newColumnAllowNull, newDBDefaultValue);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    // User friendly message for not null setting of column
                                    if (ffi.AllowEmpty && !newColumnAllowNull)
                                    {
                                        lblError.Visible = true;
                                        lblError.Text = ResHelper.GetString("FieldEditor.ColumnNotAcceptNull");
                                        lblError.ToolTip = ex.Message;
                                    }
                                    else
                                    {
                                        lblError.Visible = true;
                                        lblError.Text = ex.Message;
                                    }
                                    return;
                                }

                                break;
                        }
                    }
                    else// Some error has occurred
                    {
                        lblError.Visible = true;
                        lblError.Text = errorMessage;
                        return;
                    }
                } // End update needed
            } // End existing node

            // Update xml schema and xml form definition
            //------------------------------------------

            if ((newColumnDefaultValue != null) && (ffiUpdated.DefaultValue != newColumnDefaultValue))
            {
                // ffiUpdated.DefaultValue = newColumnDefaultValue;
            }

            if (isNewItem)
            {
                // Insert new field
                InsertFormItem(ffiUpdated);
            }
            else
            {
                // Update current field 
                fi.UpdateFormField(ffi.Name, ffiUpdated);
            }
        }
        else if (SelectedItemType == FieldEditorSelectedItemEnum.Category)
        {
            // Fill FormCategoryInfo structure with original data
            fci = fi.GetFormCategory(SelectedItemName);
            // Determine whether it is a new attribute or not
            isNewItem = (fci == null);

            // Fill FormCategoryInfo structure with updated form data
            fciUpdated = new FormCategoryInfo();
            fciUpdated.CategoryCaption = txtCategoryName.Text.Trim().Replace("'", "");

            // Check if the category caption is empty
            if (string.IsNullOrEmpty(fciUpdated.CategoryCaption))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("TemplateDesigner.ErrorCategoryNameEmpty");
                return;
            }

            if (isNewItem)
            {
                // Use category caption for name attribut
                fciUpdated.CategoryName = fciUpdated.CategoryCaption;
            }
            else
            {
                fciUpdated.CategoryName = SelectedItemName;
            }

            if (isNewItem)
            {
                // Get form category names
                string[] categoryNames = fi.GetCategoryNames();

                if (categoryNames != null)
                {
                    // Check if the category name is unique
                    foreach (string name in categoryNames)
                    {
                        // If name already exists return error
                        if (fciUpdated.CategoryName == name)
                        {
                            lblError.Visible = true;
                            lblError.Text = ResHelper.GetString("TemplateDesigner.ErrorExistingCategoryName");
                            return;
                        }
                    }
                }

                // Insert new category
                InsertFormItem(fciUpdated);
            }
            else
            {
                // Update current 
                fi.UpdateFormCategory(fci.CategoryName, fciUpdated);
            }
        }

        // Make changes in database
        //------------------------------------------
        if (SelectedItemType != 0)
        {
            // Get updated definition
            FormDefinition = fi.GetXmlDefinition();

            string error = null;
            switch (mMode)
            {
                case FieldEditorModeEnum.WebPartProperties:
                    if (wpi != null)
                    {
                        // Update xml definition
                        wpi.WebPartProperties = FormDefinition;

                        try
                        {
                            WebPartInfoProvider.SetWebPartInfo(wpi);
                        }
                        catch (Exception ex)
                        {
                            error = ex.Message;
                        }
                    }
                    else
                    {
                        error = ResHelper.GetString("FieldEditor.WebpartNotFound");
                    }
                    break;

                case FieldEditorModeEnum.ClassFormDefinition:
                case FieldEditorModeEnum.BizFormDefinition:
                case FieldEditorModeEnum.SystemTable:
                case FieldEditorModeEnum.CustomTable:
                    if (dci != null)
                    {
                        // Update xml definition
                        dci.ClassFormDefinition = FormDefinition;

                        // Update xml schema
                        dci.ClassXmlSchema = TableManager.GetXmlSchema(dci.ClassTableName);

                        // When updating existing field
                        if (ffi != null)
                        {
                            // Update ClassNodeNameSource field
                            if (dci.ClassNodeNameSource == ffi.Name)
                            {
                                dci.ClassNodeNameSource = ffiUpdated.Name;
                            }
                        }

                        bool fieldType = (SelectedItemType == FieldEditorSelectedItemEnum.Field);

                        // Update changes in DB
                        try
                        {
                            // Do not log synchronization for BizForm
                            bool bizForm = (mMode == FieldEditorModeEnum.BizFormDefinition);
                            if (bizForm || fieldType)
                            {
                                dci.DisableLogging();
                            }

                            DataClassInfoProvider.SetDataClass(dci);

                            // Restore logging
                            if (bizForm || fieldType)
                            {
                                dci.EnableLogging();
                            }
                        }
                        catch (Exception ex)
                        {
                            error = ex.Message;
                        }

                        if (fieldType)
                        {
                            // Generate default view
                            if (mMode == FieldEditorModeEnum.BizFormDefinition)
                            {
                                SqlGenerator.GenerateDefaultView(dci, CMSContext.CurrentSiteName);
                            }
                            else
                            {
                                SqlGenerator.GenerateDefaultView(dci, null);
                            }

                            // Regenerate queries                            
                            SqlGenerator.GenerateDefaultQueries(dci, true, true);
                        }

                        // Updates custom views
                        if (mMode == FieldEditorModeEnum.SystemTable)
                        {
                            try
                            {
                                TableManager.RefreshCustomViews(dci.ClassTableName);
                            }
                            catch (Exception ex)
                            {
                                error = ex.Message;
                            }
                        }
                    }
                    else
                    {
                        error = ResHelper.GetString("FieldEditor.ClassNotFound");
                    }
                    break;
            }

            if (!string.IsNullOrEmpty(error))
            {
                lblError.Visible = true;
                lblError.Text = "[FieldEditor.SaveSelectedField()]: " + error;
            }
            else
            {
                IsNewItemEdited = false;
                if (SelectedItemType == FieldEditorSelectedItemEnum.Category)
                {
                    Reload(categPreffix + fciUpdated.CategoryName);
                }
                else if (SelectedItemType == FieldEditorSelectedItemEnum.Field)
                {
                    Reload(fieldPreffix + ffiUpdated.Name);
                }

                lblError.Visible = false;
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("general.changessaved");
            }
        }

        // All done and new item, fire OnFieldCreated  event
        if (isNewItem && (ffiUpdated != null))
        {
            RaiseOnFieldCreated(ffiUpdated);
        }
    }


    /// <summary>
    /// When attribute type selected index is changed
    /// </summary>
    protected void lstAttributeType_SelectedIndexChanged(Object sender, EventArgs e)
    {
        EnableOrDisableAttributeSize();
        LoadFieldTypes(IsFieldPrimary);
        ShowAdvancedOptions();
        ShowValidationOptions(IsFieldPrimary);
        ShowDefaultValue();
        ShowAttributeTypeOptions();
        SetAttributeTypeOptions(false);
    }


    /// <summary>
    /// Enable or disable size attribute
    /// </summary>
    private void EnableOrDisableAttributeSize()
    {
        if (lstAttributeType.SelectedValue == "text")
        {
            if ((Mode == FieldEditorModeEnum.AlternativeFormDefinition) || IsSystemFieldSelected)
            {
                txtAttributeSize.Enabled = false;
            }
            else
            {
                txtAttributeSize.Enabled = true;
            }
            txtAttributeSize.MaxLength = 9;
        }
        else
        {
            txtAttributeSize.Enabled = false;
            txtAttributeSize.Text = "";
        }

        if (lstAttributeType.SelectedValue == "docattachments")
        {
            chkAllowEmpty.Checked = true;
        }
        chkAllowEmpty.Enabled = ((lstAttributeType.SelectedValue != "docattachments") && (Mode != FieldEditorModeEnum.SystemTable));
    }


    /// <summary>
    /// New category button clicked
    /// </summary>
    protected void btnNewCategory_Click(Object sender, System.Web.UI.ImageClickEventArgs e)
    {
        if (Mode == FieldEditorModeEnum.BizFormDefinition)
        {
            // Check 'EditForm' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "EditForm"))
            {
                RedirectToAccessDenied("cms.form", "EditForm");
            }
        }

        if (IsNewItemEdited)
        {
            // Display error - Only one new item can be edited
            ShowMessage(ResHelper.GetString("TemplateDesigner.ErrorCannotCreateAnotherNewItem"));
        }
        else
        {
            // Create #categ##new# item in list
            ListItem newListItem = new ListItem(ResHelper.GetString("TemplateDesigner.NewCategory"), newCategPreffix);

            if ((lstAttributes.Items.Count > 0) && (lstAttributes.SelectedIndex >= 0))
            {
                // Add behind the selected item 
                lstAttributes.Items.Insert(lstAttributes.SelectedIndex + 1, newListItem);
            }
            else
            {
                // Add at the end of the item collection
                lstAttributes.Items.Add(newListItem);
            }

            // Select new item 
            lstAttributes.SelectedIndex = lstAttributes.Items.IndexOf(newListItem);

            SelectedItemType = FieldEditorSelectedItemEnum.Category;
            SelectedItemName = newCategPreffix;

            LoadDefaultCategoryEditForm();

            IsNewItemEdited = true;
        }
    }


    /// <summary>
    /// New system attribute button clicked
    /// </summary>
    protected void btnNewSysAttribute_Click(Object sender, System.Web.UI.ImageClickEventArgs e)
    {
        NewAttribute(true);
    }


    /// <summary>
    /// New attribute button clicked
    /// </summary>
    protected void btnNewAttribute_Click(Object sender, System.Web.UI.ImageClickEventArgs e)
    {
        NewAttribute(false);
    }


    private void NewAttribute(bool system)
    {
        if (Mode == FieldEditorModeEnum.BizFormDefinition)
        {
            // Check 'EditForm' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "EditForm"))
            {
                RedirectToAccessDenied("cms.form", "EditForm");
            }
        }

        IsFieldPrimary = false;

        if (IsNewItemEdited)
        {
            // Only one new item can be edited
            ShowMessage(ResHelper.GetString("TemplateDesigner.ErrorCannotCreateAnotherNewItem"));
        }
        else
        {
            // Enable editing
            txtSimpleColumnName.Enabled = true;
            txtSimpleColumnName.ReadOnly = false;
            lstSimpleFieldType.Enabled = true;

            // Create #new# attribute in attribute list
            ListItem newListItem = new ListItem(ResHelper.GetString("TemplateDesigner.NewAttribute"), newFieldPreffix);

            if ((lstAttributes.Items.Count > 0) && (lstAttributes.SelectedIndex >= 0))
            {
                // Add behind the selected item 
                lstAttributes.Items.Insert(lstAttributes.SelectedIndex + 1, newListItem);
            }
            else
            {
                // Add at the end of the item collection
                lstAttributes.Items.Add(newListItem);
            }

            // Select new item 
            lstAttributes.SelectedIndex = lstAttributes.Items.IndexOf(newListItem);

            // Get type of previously selected item
            FieldEditorSelectedItemEnum oldItemType = SelectedItemType;

            // Initialize currently selected item type and name
            SelectedItemType = FieldEditorSelectedItemEnum.Field;
            SelectedItemName = newFieldPreffix;

            IsNewItemEdited = true;
            SetUniversalControls();

            bool newItemBlank = ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSClearFieldEditor"], true);
            if (newItemBlank || (oldItemType != FieldEditorSelectedItemEnum.Field))
            {
                LoadDefaultAttributeEditForm(system);
            }
            else
            {
                txtAdvancedAttributeName.Text = "";
                txtSimpleColumnName.Text = "";
            }
        }
    }


    /// <summary>
    /// Delete attribute button clicked.
    /// </summary>
    protected void btnDeleteItem_Click(Object sender, System.Web.UI.ImageClickEventArgs e)
    {
        if (Mode == FieldEditorModeEnum.BizFormDefinition)
        {
            // Check 'EditForm' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "EditForm"))
            {
                RedirectToAccessDenied("cms.form", "EditForm");
            }
        }

        // Raise on before definition update event
        if (OnBeforeDefinitionUpdate != null)
        {
            OnBeforeDefinitionUpdate();
        }

        FormFieldInfo ffiSelected = null;
        FormCategoryInfo fciSelected = null;
        DataClassInfo dci = null;
        WebPartInfo wpi = null;
        string errorMessage = null;
        string newSelectedValue = null;
        string deletedItemPreffix = null;

        // Load current xml form definition
        LoadFormDefinition();

        if ((!string.IsNullOrEmpty(SelectedItemName)) && (!IsNewItemEdited))
        {
            if (SelectedItemType == FieldEditorSelectedItemEnum.Field)
            {
                ffiSelected = fi.GetFormField(SelectedItemName);
                deletedItemPreffix = fieldPreffix;

                if (ffiSelected != null)
                {
                    if ((this.Mode == FieldEditorModeEnum.ClassFormDefinition) && !ffiSelected.System)
                    {
                        if (fi.GetFields(true, true, false).Length < 3)
                        {
                            lblError.Visible = true;
                            lblError.Text = ResHelper.GetString("TemplateDesigner.ErrorCannotDeleteAllCustomFields");
                            return;
                        }
                    }

                    // Do not allow deleting of the primary key
                    if (ffiSelected.PrimaryKey)
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("TemplateDesigner.ErrorCannotDeletePK");
                        return;
                    }

                    // Do not allow deleting of the system field
                    if (ffiSelected.System && !ffiSelected.External && !DevelopmentMode)
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("TemplateDesigner.ErrorCannotDeleteSystemField");
                        return;
                    }

                    // Remove specifield field from xml form definition
                    fi.RemoveFormField(SelectedItemName);

                    // Get updated definition
                    FormDefinition = fi.GetXmlDefinition();

                    switch (mMode)
                    {
                        case FieldEditorModeEnum.WebPartProperties:
                            // Web part properties
                            {
                                wpi = WebPartInfoProvider.GetWebPartInfo(mWebPartId);
                                if (wpi != null)
                                {
                                    wpi.WebPartProperties = FormDefinition;
                                    try
                                    {
                                        WebPartInfoProvider.SetWebPartInfo(wpi);
                                    }
                                    catch (Exception ex)
                                    {
                                        errorMessage = ex.Message;
                                    }
                                }
                                else
                                {
                                    errorMessage = ResHelper.GetString("FieldEditor.WebpartNotFound");
                                }
                            }
                            break;

                        case FieldEditorModeEnum.ClassFormDefinition:
                        case FieldEditorModeEnum.BizFormDefinition:
                        case FieldEditorModeEnum.SystemTable:
                        case FieldEditorModeEnum.CustomTable:
                            {
                                // Standard classes
                                dci = DataClassInfoProvider.GetDataClass(mClassName);
                                if (dci != null)
                                {
                                    // If document type is edited AND field that should be removed is FILE
                                    if ((mMode == FieldEditorModeEnum.ClassFormDefinition) && (!string.IsNullOrEmpty(ClassName)) &&
                                        (ffiSelected.DataType == FormFieldDataTypeEnum.File))
                                    {
                                        DocumentHelper.DeleteDocumentAttachments(ClassName, ffiSelected.Name, null);
                                    }

                                    // If bizform is edited AND field that should be removed is FILE
                                    if ((mMode == FieldEditorModeEnum.BizFormDefinition) && (!string.IsNullOrEmpty(ClassName)) &&
                                        (ffiSelected.FieldType == FormFieldControlTypeEnum.UploadControl))
                                    {
                                        BizFormInfoProvider.DeleteBizFormFiles(ClassName, ffiSelected.Name, CMSContext.CurrentSiteID);
                                    }

                                    // Update xml definition
                                    dci.ClassFormDefinition = FormDefinition;

                                    try
                                    {
                                        if (!ffiSelected.External)
                                        {
                                            // Remove corresponding column from table
                                            TableManager.DropTableColumn(dci.ClassTableName, SelectedItemName);

                                            // Update xml schema
                                            dci.ClassXmlSchema = TableManager.GetXmlSchema(dci.ClassTableName);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        errorMessage = ex.Message;
                                    }

                                    // Deleted field is used as ClassNodeNameSource -> remove node name source
                                    if (dci.ClassNodeNameSource == SelectedItemName)
                                    {
                                        dci.ClassNodeNameSource = "";
                                    }

                                    // Update changes in database
                                    try
                                    {
                                        // Do not log synchronization for BizForm
                                        if (mMode == FieldEditorModeEnum.BizFormDefinition)
                                        {
                                            dci.DisableLogging();
                                        }

                                        DataClassInfoProvider.SetDataClass(dci);
                                        dci.EnableLogging();
                                    }
                                    catch (Exception ex)
                                    {
                                        errorMessage = ex.Message;
                                    }

                                    // Refresh views and quries only if changes to DB were made
                                    if (!ffiSelected.External)
                                    {
                                        // Generate default view
                                        if (mMode == FieldEditorModeEnum.BizFormDefinition)
                                        {
                                            SqlGenerator.GenerateDefaultView(dci, CMSContext.CurrentSiteName);
                                        }
                                        else
                                        {
                                            SqlGenerator.GenerateDefaultView(dci, null);
                                        }

                                        // Regenerate queries                            
                                        SqlGenerator.GenerateDefaultQueries(dci, true, true);

                                        // Updates custom views
                                        if (mMode == FieldEditorModeEnum.SystemTable)
                                        {
                                            TableManager.RefreshCustomViews(dci.ClassTableName);
                                        }
                                    }

                                    // Clear hashtables
                                    ClearHashtables();
                                }
                                else
                                {
                                    errorMessage = ResHelper.GetString("FieldEditor.ClassNotFound");
                                }
                            }
                            break;
                    }
                }
            }
            else if (SelectedItemType == FieldEditorSelectedItemEnum.Category)
            {
                fciSelected = fi.GetFormCategory(SelectedItemName);

                deletedItemPreffix = categPreffix;

                // Remove specifield category from xml form definition
                fi.RemoveFormCategory(SelectedItemName);

                // Get updated form definition
                FormDefinition = fi.GetXmlDefinition();

                switch (mMode)
                {
                    case FieldEditorModeEnum.WebPartProperties:
                        wpi = WebPartInfoProvider.GetWebPartInfo(mWebPartId);
                        if (wpi != null)
                        {
                            wpi.WebPartProperties = FormDefinition;
                            try
                            {
                                WebPartInfoProvider.SetWebPartInfo(wpi);
                            }
                            catch (Exception ex)
                            {
                                errorMessage = ex.Message;
                            }
                        }
                        else
                        {
                            errorMessage = ResHelper.GetString("FieldEditor.WebpartNotFound");
                        }
                        break;

                    case FieldEditorModeEnum.ClassFormDefinition:
                    case FieldEditorModeEnum.BizFormDefinition:
                    case FieldEditorModeEnum.SystemTable:
                    case FieldEditorModeEnum.CustomTable:
                        dci = DataClassInfoProvider.GetDataClass(mClassName);

                        if (dci != null)
                        {
                            // Update xml definition
                            dci.ClassFormDefinition = FormDefinition;

                            // Update changes in database
                            try
                            {
                                // Do not log synchronization for BizForm
                                if (mMode == FieldEditorModeEnum.BizFormDefinition)
                                {
                                    dci.DisableLogging();
                                }

                                DataClassInfoProvider.SetDataClass(dci);
                                dci.EnableLogging();
                            }
                            catch (Exception ex)
                            {
                                errorMessage = ex.Message;
                            }
                        }
                        else
                        {
                            errorMessage = ResHelper.GetString("FieldEditor.ClassNotFound");
                        }
                        break;
                }
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                lblError.Visible = true;
                lblError.Text = "[ FieldEditor.btnDeleteItem_Click() ]: " + errorMessage;
            }
        }
        else
        {
            // "delete" new item from the list
            IsNewItemEdited = false;
        }

        // Set new selected value
        ListItem deletedItem = lstAttributes.Items.FindByValue(deletedItemPreffix + SelectedItemName);
        int deletedItemIndex = lstAttributes.Items.IndexOf(deletedItem);

        if ((deletedItemIndex > 0) && (lstAttributes.Items[deletedItemIndex - 1] != null))
        {
            newSelectedValue = lstAttributes.Items[deletedItemIndex - 1].Value;
        }

        // Reload data
        Reload(newSelectedValue);

        // Raise on after definition update event
        if (OnAfterDefinitionUpdate != null)
        {
            OnAfterDefinitionUpdate();
        }
    }


    /// <summary>
    /// Fill field types list
    /// </summary>
    /// <param name="isPrimary">Determines whether the attribute is primary key</param>
    private void LoadFieldTypes(bool isPrimary)
    {
        bool isSimplifiedMode = (SelectedMode == FieldEditorSelectedModeEnum.Simplified) ? true : false;
        FieldEditorControlsEnum controls = FieldEditorControlsEnum.None;

        // Get displayed controls
        if (mDisplayedControls == FieldEditorControlsEnum.ModeSelected)
        {
            switch (mMode)
            {
                case FieldEditorModeEnum.BizFormDefinition:
                    controls = FieldEditorControlsEnum.Bizforms;
                    break;

                case FieldEditorModeEnum.ClassFormDefinition:
                    controls = FieldEditorControlsEnum.DocumentTypes;
                    break;

                case FieldEditorModeEnum.SystemTable:
                    controls = FieldEditorControlsEnum.SystemTables;
                    break;

                case FieldEditorModeEnum.CustomTable:
                    controls = FieldEditorControlsEnum.CustomTables;
                    break;

                case FieldEditorModeEnum.WebPartProperties:
                    controls = FieldEditorControlsEnum.Controls;
                    break;

                case FieldEditorModeEnum.General:
                    controls = FieldEditorControlsEnum.All;
                    break;
            }
        }
        else
        {
            controls = DisplayedControls;
        }

        // Clear list
        lstFieldType.Items.Clear();
        lstFieldType.DataSource = FormHelper.GetFieldControlTypesWithUserControls(lstAttributeType.SelectedValue, controls, isSimplifiedMode, isPrimary);
        lstFieldType.DataBind();
    }


    /// <summary>
    /// Show javasript's alert message
    /// </summary>
    /// <param name="message">Message to show</param>
    private void ShowMessage(string message)
    {
        ltlScript.Text = ScriptHelper.GetScript("alert(" + ScriptHelper.GetString(message) + ");");
    }


    /// <summary>
    /// Called when field type selected index changed
    /// </summary>
    protected void lstFieldType_SelectedIndexChanged(object sender, EventArgs e)
    {
        EnableOrDisableAttributeSize();
        ShowAdvancedOptions();

        if (SelectedMode == FieldEditorSelectedModeEnum.Simplified)
        {
            ShowDefaultValue();
        }
        ShowValidationOptions(IsFieldPrimary);
    }


    /// <summary>
    /// Called when source field selected index changed
    /// </summary>
    protected void drpSourceField_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (mMode == FieldEditorModeEnum.ClassFormDefinition)
        {
            string errorMessage = null;

            DataClassInfo dci = DataClassInfoProvider.GetDataClass(ClassName);
            if (dci != null)
            {
                // Set document name source field
                if (string.IsNullOrEmpty(drpSourceField.SelectedValue))
                {
                    // Use extra field
                    dci.ClassNodeNameSource = "";
                }
                else
                {
                    // Use specified name
                    dci.ClassNodeNameSource = drpSourceField.SelectedValue;
                }
                // Set document alias source field
                if (string.IsNullOrEmpty(drpSourceAliasField.SelectedValue))
                {
                    // Use extra field
                    dci.ClassNodeAliasSource = "";
                }
                else
                {
                    // Use specified name
                    dci.ClassNodeAliasSource = drpSourceAliasField.SelectedValue;
                }

                try
                {
                    // Do not log synchronization for BizForm
                    if (mMode == FieldEditorModeEnum.BizFormDefinition)
                    {
                        dci.DisableLogging();
                    }

                    DataClassInfoProvider.SetDataClass(dci);
                    dci.EnableLogging();

                    lblError.Visible = false;
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("TemplateDesigner.SourceFieldSaved");
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }
            }
            else
            {
                errorMessage = ResHelper.GetString("FieldEditor.ClassNotFound");
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                lblError.Visible = true;
                lblError.Text = "[ FieldEditor.drpSourceField_SelectedIndexChanged() ]: " + errorMessage;
            }
        }
    }


    /// <summary>
    /// Show advanced options according to selected field type
    /// </summary>
    private void ShowAdvancedOptions()
    {
        pnlCalendarSettings.Visible = false;
        pnlDropDownListSettings.Visible = false;
        pnlHTMLAreaSettings.Visible = false;
        pnlTextAreaSettings.Visible = false;
        pnlBBEditorSettings.Visible = false;
        pnlDocumentAttachmentsSettings.Visible = false;
        pnlMultipleChoice.Visible = false;
        pnlListBox.Visible = false;
        pnlSimpleTextBox.Visible = false;
        pnlImageSelection.Visible = false;
        plcRadioButtonsDirection.Visible = false;
        pnlUploadFileSettings.Visible = false;
        plcAdvacedFieldSettings.Visible = false;
        pnlAutoResize.Visible = false;

        string selectedFieldType = lstFieldType.SelectedValue.ToLower();
        string selectedControlName = "";

        // Display textbox with max length size when in simplified mode and when default field type for user controls is TEXT
        if ((SelectedMode == FieldEditorSelectedModeEnum.Simplified) && (selectedFieldType.StartsWith(controlPrefix)))
        {
            selectedControlName = selectedFieldType.Substring(controlPrefix.Length);
            string[] controlDefaultDataType = GetUserControlDefaultDataType(selectedControlName);

            if (controlDefaultDataType[0] == FormFieldDataTypeCode.TEXT)
            {
                pnlSimpleTextBox.Visible = true;
                if (IsNewItemEdited)
                {
                    txtSimpleTextBoxMaxLength.Text = ValidationHelper.GetString(controlDefaultDataType[1], "");
                }
                else
                {
                    txtSimpleTextBoxMaxLength.Text = (ffi.Size == 0) ? "" : Convert.ToString(ffi.Size);
                }
            }
        }
        else
        {
            switch (selectedFieldType)
            {
                case FormFieldControlTypeCode.TEXTAREA:
                    pnlTextAreaSettings.Visible = true;
                    break;

                case FormFieldControlTypeCode.BBEDITOR:
                    pnlBBEditorSettings.Visible = true;
                    plcAdvacedFieldSettings.Visible = true;
                    elemAdvancedDialogConfig.DisplayEmailTabSetting = false;
                    break;

                case FormFieldControlTypeCode.HTMLAREA:
                    plcAdvacedFieldSettings.Visible = true;
                    pnlHTMLAreaSettings.Visible = true;
                    elemAdvancedDialogConfig.DisplayEmailTabSetting = true;
                    break;

                case FormFieldControlTypeCode.DROPDOWNLIST:
                    pnlDropDownListSettings.Visible = true;
                    plcRadioButtonsDirection.Visible = false;
                    break;

                case FormFieldControlTypeCode.RADIOBUTTONS:
                    pnlDropDownListSettings.Visible = true;
                    plcRadioButtonsDirection.Visible = true;
                    break;

                case FormFieldControlTypeCode.CALENDAR:
                    pnlCalendarSettings.Visible = true;
                    break;

                case FormFieldControlTypeCode.MULTIPLECHOICE:
                    pnlMultipleChoice.Visible = true;
                    break;

                case FormFieldControlTypeCode.LISTBOX:
                    pnlListBox.Visible = true;
                    break;

                case FormFieldControlTypeCode.IMAGEPATH:
                case FormFieldControlTypeCode.MEDIAPATH:
                case FormFieldControlTypeCode.FILEPATH:
                    pnlAutoResize.Visible = true;
                    pnlImageSelection.Visible = (selectedFieldType == FormFieldControlTypeCode.IMAGEPATH);
                    autoResizeElem.ClearControl();
                    break;

                // Only for simplified mode
                case FormFieldControlTypeCode.TEXTBOX:
                    if (SelectedMode == FieldEditorSelectedModeEnum.Simplified)
                    {
                        pnlSimpleTextBox.Visible = true;
                        txtSimpleTextBoxMaxLength.Text = "";
                    }
                    break;

                case FormFieldControlTypeCode.UPLOAD:
                    pnlUploadFileSettings.Visible = true;
                    break;

                case FormFieldControlTypeCode.DIRECTUPLOAD:
                    pnlDocumentAttachmentsSettings.Visible = true;
                    ucAdvancedDocAttachments.AdvancedMode = false;
                    break;

                case FormFieldControlTypeCode.DOCUMENT_ATTACHMENTS:
                    pnlDocumentAttachmentsSettings.Visible = true;
                    break;
            }
        }
    }


    /// <summary>
    /// Show validation options according to selected attribute type
    /// </summary>
    /// <param name="isPrimary">Indicates whether it is a primary key</param> 
    private void ShowValidationOptions(bool isPrimary)
    {
        pnlSectionValidation.Visible = false;
        pnlTextValidation.Visible = false;
        pnlDateTimeValidation.Visible = false;
        pnlNumberValidation.Visible = false;
        pnlFileValidation.Visible = false;
        pnlErrorMessageValidation.Visible = false;
        plcSpellCheck.Visible = false;

        if ((lstFieldType.SelectedValue != "label") && (!isPrimary))
        {
            switch (lstAttributeType.SelectedValue.ToLower())
            {
                case "text":
                case "longtext":
                    pnlSectionValidation.Visible = true;
                    pnlTextValidation.Visible = true;
                    pnlErrorMessageValidation.Visible = true;
                    if (Mode == FieldEditorModeEnum.ClassFormDefinition)
                    {
                        plcSpellCheck.Visible = true;
                    }
                    break;

                case "datetime":
                    pnlSectionValidation.Visible = true;
                    pnlDateTimeValidation.Visible = true;
                    pnlErrorMessageValidation.Visible = true;
                    break;

                case "integer":
                case "longinteger":
                case "double":
                    pnlSectionValidation.Visible = true;
                    pnlNumberValidation.Visible = true;
                    pnlErrorMessageValidation.Visible = true;
                    break;

                case "file":
                    if (lstFieldType.SelectedValue != FormFieldControlTypeCode.DIRECTUPLOAD)
                    {
                        pnlSectionValidation.Visible = true;
                        pnlFileValidation.Visible = true;
                    }
                    break;
            }
        }



        pnlMinMaxLengthValidation.Visible = true;
        switch (lstFieldType.SelectedValue.ToLower())
        {
            // Hide Min max length for selection controls
            case FormFieldControlTypeCode.DROPDOWNLIST:
            case FormFieldControlTypeCode.RADIOBUTTONS:
            case FormFieldControlTypeCode.MULTIPLECHOICE:
            case FormFieldControlTypeCode.LISTBOX:
                pnlMinMaxLengthValidation.Visible = false;
                break;

            // Hide the whole validation section 
            case FormFieldControlTypeCode.IMAGEPATH:
            case FormFieldControlTypeCode.FILEPATH:
            case FormFieldControlTypeCode.MEDIAPATH:
                pnlSectionValidation.Visible = false;
                break;

            default:
                // For custom controls, hide all from validation section ecept error message
                if ((lstFieldType.SelectedValue != null) && (lstFieldType.SelectedValue.StartsWith(controlPrefix)))
                {
                    pnlMinMaxLengthValidation.Visible = false;
                    pnlNumberValidation.Visible = false;
                    pnlTextValidation.Visible = false;
                }
                break;
        }



        // Show/Hide "Control CSS class" input textbox for controls
        //plcControlCssClass.Visible = ((lstFieldType.SelectedValue != null) && (lstFieldType.SelectedValue.StartsWith(controlPrefix)));
        //if (plcControlCssClass.Visible)
        //{            
        //    this.lblControlCssClass.Text = ResHelper.GetString("TemplateDesigner.ControlStyle");
        //}
    }


    /// <summary>
    /// Validates advanced options and returns validation errorr message
    /// </summary>
    private string ValidateAdvancedOptions()
    {
        string errorMessage = null;

        switch (lstFieldType.SelectedValue.ToLower())
        {
            case "textarea":
                // Check cols and rows values
                if ((!string.IsNullOrEmpty(txtTextAreaCols.Text.Trim())) && !(ValidationHelper.IsInteger(txtTextAreaCols.Text)))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorTextAreaColsNotNumber") + " ";
                }
                if ((!string.IsNullOrEmpty(txtTextAreaRows.Text.Trim())) && !(ValidationHelper.IsInteger(txtTextAreaRows.Text)))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorTextAreaRowsNotNumber") + " ";
                }
                if (!string.IsNullOrEmpty(txtTextAreaSize.Text.Trim()))
                {
                    if (!(ValidationHelper.IsInteger(txtTextAreaSize.Text)))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorTextAreaSizeNotNumber") + " ";
                    }
                    else if (Int32.Parse(txtTextAreaSize.Text) <= 0)
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorTextAreaSizeLessThanZero") + " ";
                    }
                }
                break;

            case "htmlarea":
                // Check width and height values
                if ((!string.IsNullOrEmpty(txtHTMLAreaHeight.Text.Trim())) && !(ValidationHelper.IsInteger(txtHTMLAreaHeight.Text)))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorHTMLAreaHeightNotNumber") + " ";
                }
                if ((!string.IsNullOrEmpty(txtHTMLAreaWidth.Text.Trim())) && !(ValidationHelper.IsInteger(txtHTMLAreaWidth.Text)))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorHTMLAreaWidthNotNumber") + " ";
                }
                if (!string.IsNullOrEmpty(txtHTMLAreaSize.Text.Trim()))
                {
                    if (!(ValidationHelper.IsInteger(txtHTMLAreaSize.Text)))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorHTMLAreaSizeNotNumber") + " ";
                    }
                    else if (Int32.Parse(txtHTMLAreaSize.Text) <= 0)
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorHTMLAreaSizeLessThanZero") + " ";
                    }
                }
                errorMessage += elemAdvancedDialogConfig.Validate();
                break;

            case "dropdownlist":
            case "radiobuttons":
                // Check options or sql query
                if (radDropDownListSQL.Checked && (string.IsNullOrEmpty(txtDropDownListOptions.Text.Trim())))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorDropDownListQueryEmpty") + " ";
                }
                if (radDropDownListOptions.Checked)
                {
                    if (string.IsNullOrEmpty(txtDropDownListOptions.Text.Trim()))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorDropDownListOptionsEmpty") + " ";
                    }
                    else
                    {
                        int lineIndex = 0;
                        string[] lines = txtDropDownListOptions.Text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
                        for (lineIndex = 0; lineIndex <= lines.GetUpperBound(0); lineIndex++)
                        {
                            if ((lines[lineIndex] != null) && (lines[lineIndex].Trim() != ""))
                            {
                                // Get line items
                                string[] items = lines[lineIndex].Replace("\\;", SEMICOLON).Split(';');

                                if (items.Length != 2)
                                {
                                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorDropDownListOptionsInvalidFormat") + " ";
                                }
                                else
                                {
                                    switch (lstAttributeType.SelectedValue)
                                    {
                                        case FormFieldDataTypeCode.DOUBLE:
                                            if (!ValidationHelper.IsDouble(items[0]) && !(chkAllowEmpty.Checked && string.IsNullOrEmpty(items[0])))
                                            {
                                                errorMessage += string.Format(ResHelper.GetString("TemplateDesigner.ErrorDropDownListOptionsInvalidDoubleFormat"), lineIndex + 1) + " ";
                                            }
                                            break;

                                        case FormFieldDataTypeCode.INTEGER:
                                            if (!ValidationHelper.IsInteger(items[0]) && !(chkAllowEmpty.Checked && string.IsNullOrEmpty(items[0])))
                                            {
                                                errorMessage += string.Format(ResHelper.GetString("TemplateDesigner.ErrorDropDownListOptionsInvalidIntFormat"), lineIndex + 1) + " ";
                                            }
                                            break;

                                        case FormFieldDataTypeCode.LONGINTEGER:
                                            if (!ValidationHelper.IsLong(items[0]) && !(chkAllowEmpty.Checked && string.IsNullOrEmpty(items[0])))
                                            {
                                                errorMessage += string.Format(ResHelper.GetString("TemplateDesigner.ErrorDropDownListOptionsInvalidLongIntFormat"), lineIndex + 1) + " ";
                                            }
                                            break;

                                        case FormFieldDataTypeCode.DATETIME:
                                            if ((ValidationHelper.GetDateTime(items[0], DateTimeHelper.ZERO_TIME) == DateTimeHelper.ZERO_TIME) && !(chkAllowEmpty.Checked && string.IsNullOrEmpty(items[0])))
                                            {
                                                errorMessage += string.Format(ResHelper.GetString("TemplateDesigner.ErrorDropDownListOptionsInvalidDateTimeFormat"), lineIndex + 1) + " ";
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
                pnlDropDownListSettings.Visible = true;
                //    plcRadioButtonsDirection.Visible = (this.lstFieldType.SelectedValue.ToLower() == "radiobuttons");                

                break;

            case "multiplechoice":
                // Check options or sql query
                if (radMultipleChoiceSQL.Checked && (string.IsNullOrEmpty(txtMultipleChoiceOptions.Text.Trim())))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMultipleChoiceQueryEmpty") + " ";
                }
                if (radMultipleChoiceOptions.Checked)
                {
                    if (string.IsNullOrEmpty(txtMultipleChoiceOptions.Text.Trim()))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMultipleChoiceOptionsEmpty") + " ";
                    }
                    else
                    {
                        int lineIndex = 0;
                        string[] lines = txtMultipleChoiceOptions.Text.Split('\n');
                        for (lineIndex = 0; lineIndex <= lines.GetUpperBound(0); lineIndex++)
                        {
                            if ((lines[lineIndex] != null) && (lines[lineIndex].Trim() != "") && (lines[lineIndex].Replace("\\;", SEMICOLON).Split(';').Length != 2))
                            {
                                errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMultipleChoiceOptionsInvalidFormat") + " ";
                            }
                        }
                    }
                }
                pnlMultipleChoice.Visible = true;
                break;

            case "listbox":
                // Check options or sql query
                if (radListBoxSQL.Checked && (string.IsNullOrEmpty(txtListBoxOptions.Text.Trim())))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorListBoxQueryEmpty") + " ";
                }
                if (radListBoxOptions.Checked)
                {
                    if (string.IsNullOrEmpty(txtListBoxOptions.Text.Trim()))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorListBoxOptionsEmpty") + " ";
                    }
                    else
                    {
                        int lineIndex = 0;
                        string[] lines = txtListBoxOptions.Text.Split('\n');
                        for (lineIndex = 0; lineIndex <= lines.GetUpperBound(0); lineIndex++)
                        {
                            if ((lines[lineIndex] != null) && (lines[lineIndex].Trim() != "") && (lines[lineIndex].Replace("\\;", SEMICOLON).Split(';').Length != 2))
                            {
                                errorMessage += ResHelper.GetString("TemplateDesigner.ErrorListBoxOptionsInvalidFormat") + " ";
                            }
                        }
                    }
                }
                pnlListBox.Visible = true;
                break;

            case "calendar":
                // Do nothing
                break;

            // Auto resize on upload
            case "upload":
                errorMessage += elemAutoResize.Validate() + " ";
                break;

            // File/Image/Media selection
            case FormFieldControlTypeCode.IMAGEPATH:
            case FormFieldControlTypeCode.FILEPATH:
            case FormFieldControlTypeCode.MEDIAPATH:
                errorMessage += autoResizeElem.Validate() + " ";
                break;

            // Document attachments
            case "documentattachments":
                if (ucDocAttachSettings != null)
                {
                    errorMessage += ucDocAttachSettings.Validate() + " ";
                }
                break;
        }

        return errorMessage;
    }


    /// <summary>
    /// Validates form and returns validation errorr message
    /// </summary>
    private string ValidateForm()
    {
        string errorMessage = null;
        const string INVALIDCHARACTERS = @".,;'`:/\*|?""&%$!-+=()[]{} ";
        //txtAttributeName.Text = txtAttributeName.Text.Trim();
        string attributeName = GetAttributeName();

        bool MinAndMaxLengthInCorrectFormat = true;
        bool MinAndMaxValueInCorrectFormat = true;


        // Validation of the controls shared by both modes (simplified and advanced)
        //--------------------------------------------------------------------------

        // Check if attribute name isn't empty
        if (string.IsNullOrEmpty(attributeName))
        {
            errorMessage += ResHelper.GetString("TemplateDesigner.ErrorEmptyAttributeName") + " ";
        }

        // Check if attribute name starts with a letter or '_' (if it is an identificator)
        if (!ValidationHelper.IsIdentificator(attributeName))
        {
            errorMessage += ResHelper.GetString("TemplateDesigner.ErrorAttributeNameDoesNotStartWithLetter") + " ";
        }

        // Check attribute name for invalid characters
        for (int i = 0; i <= INVALIDCHARACTERS.Length - 1; i++)
        {
            if (attributeName.Contains(Convert.ToString(INVALIDCHARACTERS[i])))
            {
                errorMessage += ResHelper.GetString("TemplateDesigner.ErrorInvalidCharacter") + INVALIDCHARACTERS + ". ";
            }
        }

        // Check if field caption is entered
        if (chkDisplayInForm.Checked)
        {
            if (string.IsNullOrEmpty(txtFieldCaption.Text.Trim()))
            {
                errorMessage += ResHelper.GetString("TemplateDesigner.ErrorEmptyFieldCaption") + " ";
            }
        }

        // Validation of the advanced mode controls
        //-----------------------------------------
        if (SelectedMode == FieldEditorSelectedModeEnum.Advanced)
        {
            // Check attribute size value
            if (lstAttributeType.SelectedValue.ToLower() == "text")
            {
                // Attribute size is empty -> error
                if (string.IsNullOrEmpty(txtAttributeSize.Text))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorEmptyAttributeSize") + " ";
                }
                else
                {
                    // Attribute size is invalid -> error
                    if ((lstAttributeType.SelectedValue.ToLower() == "text") && (ValidationHelper.GetInteger(txtAttributeSize.Text, 0) <= 0))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorInvalidAttributeSize") + " ";
                    }

                    if ((txtDefaultValue.Text.Length > ValidationHelper.GetInteger(txtAttributeSize.Text.Trim(), 0)) ||
                        (txtLargeDefaultValue.Value.ToString().Length > ValidationHelper.GetInteger(txtAttributeSize.Text.Trim(), 0)))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorDefaultValueSize") + " ";
                    }

                    // Attribute size is invalid -> error
                    if ((lstFieldType.SelectedValue.ToLower() == "htmlarea") && (ValidationHelper.GetInteger(txtAttributeSize.Text, 0) <= 0))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.HtmlArea.ErrorInvalidAttributeSize") + " ";
                    }
                }
            }

            // Check min length value
            if (!string.IsNullOrEmpty(txtMinLength.Text.Trim()))
            {
                if ((!ValidationHelper.IsInteger(txtMinLength.Text)) ||
                     ((ValidationHelper.IsInteger(txtMinLength.Text)) && (Convert.ToInt32(txtMinLength.Text.Trim()) < 0)))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMinLengthNotInteger") + " ";
                    MinAndMaxLengthInCorrectFormat = false;
                }
            }

            // Check max length value
            if (!string.IsNullOrEmpty(txtMaxLength.Text.Trim()))
            {
                if ((!ValidationHelper.IsInteger(txtMaxLength.Text)) ||
                     ((ValidationHelper.IsInteger(txtMaxLength.Text)) && (Convert.ToInt32(txtMaxLength.Text.Trim()) < 0)))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMaxLengthNotInteger") + " ";
                    MinAndMaxLengthInCorrectFormat = false;
                }
            }

            // Min and max length are specified and in correct format -> check if min length is less than max length
            if ((!string.IsNullOrEmpty(txtMinLength.Text.Trim())) && (!string.IsNullOrEmpty(txtMaxLength.Text.Trim())) && (MinAndMaxLengthInCorrectFormat))
            {
                if ((Convert.ToInt32(txtMinLength.Text.Trim())) > (Convert.ToInt32(txtMaxLength.Text.Trim())))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMinLengthGreater") + " ";
                }
            }


            if (lstAttributeType.SelectedValue == FormFieldDataTypeCode.DOUBLE)
            {
                string strMinValue = txtMinValue.Text.Trim();
                string strMaxValue = txtMaxValue.Text.Trim();

                double minValue = 0.0;
                double maxValue = 0.0;

                // Check min value
                if (!string.IsNullOrEmpty(strMinValue))
                {
                    minValue = ValidationHelper.GetDouble(strMinValue, Double.NaN);

                    if (Double.IsNaN(minValue))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMinValueNotDouble") + " ";
                        MinAndMaxValueInCorrectFormat = false;
                    }
                }

                // Check max value
                if (!string.IsNullOrEmpty(strMaxValue))
                {
                    maxValue = ValidationHelper.GetDouble(strMaxValue, Double.NaN);

                    if (Double.IsNaN(maxValue))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMaxValueNotDouble") + " ";
                        MinAndMaxValueInCorrectFormat = false;
                    }
                }

                // Min and max value are specified and in correct format -> check if min value is less than max value
                if ((!string.IsNullOrEmpty(strMinValue)) && (!string.IsNullOrEmpty(strMaxValue)) && (MinAndMaxValueInCorrectFormat))
                {
                    if (minValue > maxValue)
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMinValueGreater") + " ";
                    }
                }
            }
            else if (lstAttributeType.SelectedValue == FormFieldDataTypeCode.INTEGER)
            {
                // Check min value
                if (!string.IsNullOrEmpty(txtMinValue.Text.Trim()))
                {
                    if ((!ValidationHelper.IsInteger(txtMinValue.Text)) ||
                         ((ValidationHelper.IsInteger(txtMinValue.Text)) && (Convert.ToInt32(txtMinValue.Text.Trim()) < 0)))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMinValueNotInteger") + " ";
                        MinAndMaxValueInCorrectFormat = false;
                    }
                }

                // Check max value
                if (!string.IsNullOrEmpty(txtMaxValue.Text.Trim()))
                {
                    if ((!ValidationHelper.IsInteger(txtMaxValue.Text)) ||
                         ((ValidationHelper.IsInteger(txtMaxValue.Text)) && (Convert.ToInt32(txtMaxValue.Text.Trim()) < 0)))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMaxValueNotInteger") + " ";
                        MinAndMaxValueInCorrectFormat = false;
                    }
                }

                // Min and max value are specified and in correct format -> check if min value is less than max value
                if ((!string.IsNullOrEmpty(txtMinValue.Text.Trim())) && (!string.IsNullOrEmpty(txtMaxValue.Text.Trim())) && (MinAndMaxValueInCorrectFormat))
                {
                    if ((Convert.ToInt32(txtMinValue.Text.Trim())) > (Convert.ToInt32(txtMaxValue.Text.Trim())))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMinValueGreater") + " ";
                    }
                }
            }
            else if (lstAttributeType.SelectedValue == FormFieldDataTypeCode.LONGINTEGER)
            {
                // Check min value
                if (!string.IsNullOrEmpty(txtMinValue.Text.Trim()))
                {
                    if ((!ValidationHelper.IsLong(txtMinValue.Text)) ||
                         ((ValidationHelper.IsLong(txtMinValue.Text)) && (Convert.ToInt64(txtMinValue.Text.Trim()) < 0)))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMinValueNotLongInteger") + " ";
                        MinAndMaxValueInCorrectFormat = false;
                    }
                }

                // Check max value
                if (!string.IsNullOrEmpty(txtMaxValue.Text.Trim()))
                {
                    if ((!ValidationHelper.IsLong(txtMaxValue.Text)) ||
                         ((ValidationHelper.IsLong(txtMaxValue.Text)) && (Convert.ToInt64(txtMaxValue.Text.Trim()) < 0)))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMaxValueNotLongInteger") + " ";
                        MinAndMaxValueInCorrectFormat = false;
                    }
                }

                // Min and max value are specified and in correct format -> check if min value is less than max value
                if ((!string.IsNullOrEmpty(txtMinValue.Text.Trim())) && (!string.IsNullOrEmpty(txtMaxValue.Text.Trim())) && (MinAndMaxValueInCorrectFormat))
                {
                    if ((Convert.ToInt64(txtMinValue.Text.Trim())) > (Convert.ToInt64(txtMaxValue.Text.Trim())))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorMinValueGreater") + " ";
                    }
                }
            }
        }
        // Validation of the simplified mode controls
        else
        {
            // Check attribute size value
            if ((lstFieldType.SelectedValue.ToLower() == FormFieldControlTypeCode.TEXTBOX) || txtSimpleTextBoxMaxLength.Visible)
            {
                // Textbox length size is empty -> error
                if (string.IsNullOrEmpty(txtSimpleTextBoxMaxLength.Text))
                {
                    errorMessage += ResHelper.GetString("TemplateDesigner.ErrorEmptyTextBoxMaxLength") + " ";
                }
                else
                {
                    // Attribute size is invalid -> error
                    try
                    {
                        uint i = UInt32.Parse(txtSimpleTextBoxMaxLength.Text.Trim());
                    }
                    catch
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorInvalidTextBoxMaxLength") + " ";
                    }

                    if (txtSimpleDefault.Text.Length > ValidationHelper.GetInteger(txtSimpleTextBoxMaxLength.Text.Trim(), 0))
                    {
                        errorMessage += ResHelper.GetString("TemplateDesigner.ErrorDefaultValueSize") + " ";
                    }
                }
            }
        }

        return errorMessage;
    }


    /// <summary>
    /// Returns FormFieldInfo structure with form data
    /// </summary>   
    /// <param name="ffiOriginal">Original field info.</param>
    private FormFieldInfo FillFormFieldInfoStructure(FormFieldInfo ffiOriginal)
    {
        // Field info with updated information
        FormFieldInfo ffi = new FormFieldInfo();

        string selectedType = lstFieldType.SelectedValue.ToLower();
        string attributeType = lstAttributeType.SelectedValue.ToLower();

        // Ensure field GUID
        if (ffiOriginal != null)
        {
            ffi.Guid = ffiOriginal.Guid;
        }
        if (ffi.Guid == Guid.Empty)
        {
            ffi.Guid = Guid.NewGuid();
        }

        // Part of database section
        ffi.Name = GetAttributeName();
        ffi.Caption = txtFieldCaption.Text.Trim();
        ffi.AllowEmpty = chkAllowEmpty.Checked;
        ffi.System = chkAttributeIsSystem.Checked;
        ffi.DefaultValue = GetDefaultValue();
        ffi.IsMacro = EnableMacrosForDefaultValue && ValidationHelper.IsMacro(ffi.DefaultValue);

        // Save user visibility
        ffi.AllowUserToChangeVisibility = chkChangeVisibility.Checked;
        ffi.Visibility = ctrlVisibility.Value;
        ffi.VisibilityControl = drpVisibilityControl.SelectedValue;

        // Attribute type options
        if (attributeType == FormFieldDataTypeCode.DATETIME)
        {
            // Set time zone information            
            ffi.Settings["timezonetype"] = timeZoneType.Value;
            if (timeZoneType.Value.ToString().ToLower() == "custom")
            {
                ffi.Settings["timezone"] = timeZone.Value;
            }
            else
            {
                ffi.Settings["timezone"] = "";
            }
        }

        if ((Mode == FieldEditorModeEnum.BizFormDefinition) ||
            DisplayedControls == FieldEditorControlsEnum.Bizforms)
        {
            ffi.PublicField = chkPublicField.Checked;
        }
        else
        {
            ffi.PublicField = false;
        }

        string controlName = selectedType.Substring(controlPrefix.Length);

        // User control is selected
        if (selectedType.StartsWith(controlPrefix))
        {
            ffi.Settings.Add("controlname", controlName);
            ffi.FieldType = FormFieldControlTypeEnum.CustomUserControl;
        }
        // One of the default controls is selected
        else
        {
            ffi.FieldType = FormHelper.GetFormFieldControlType(selectedType);
        }

        // Determine if it external column
        ffi.External = IsSystemFieldSelected;

        // Update advanced options
        switch (selectedType)
        {
            case FormFieldControlTypeCode.TEXTAREA:
                ffi.Settings.Add("cols", txtTextAreaCols.Text.Trim());
                ffi.Settings.Add("rows", txtTextAreaRows.Text.Trim());
                ffi.Settings.Add("size", txtTextAreaSize.Text.Trim());
                break;

            case FormFieldControlTypeCode.BBEDITOR:
                elemAdvancedDialogConfig.UpdateConfiguration(ffi.Settings);
                ffi.Settings.Add("showbold", chkBBEditorSettingsShowBold.Checked);
                ffi.Settings.Add("showcode", chkBBEditorSettingsShowCode.Checked);
                ffi.Settings.Add("showcolor", chkBBEditorSettingsShowColor.Checked);
                ffi.Settings.Add("showitalic", chkBBEditorSettingsShowItalic.Checked);
                ffi.Settings.Add("showquote", chkBBEditorSettingsShowQuote.Checked);
                ffi.Settings.Add("showstrike", chkBBEditorSettingsShowStrike.Checked);
                ffi.Settings.Add("showunderline", chkBBEditorSettingsShowUnderline.Checked);
                ffi.Settings.Add("usepromptdialog", chkBBEditorSettingsUsePromptDialog.Checked);
                ffi.Settings.Add("showimage", radBBImageSimple.Checked);
                ffi.Settings.Add("showurl", radBBUrlSimple.Checked);
                ffi.Settings.Add("showadvancedimage", radBBImageAdvanced.Checked);
                ffi.Settings.Add("showadvancedurl", radBBUrlAdvanced.Checked);
                ffi.Settings.Add("cols", txtBBEditorCols.Text.Trim());
                ffi.Settings.Add("rows", txtBBEditorRows.Text.Trim());
                ffi.Settings.Add("size", txtBBEditorSize.Text.Trim());
                break;

            case FormFieldControlTypeCode.HTMLAREA:
                elemAdvancedDialogConfig.UpdateConfiguration(ffi.Settings);
                ffi.Settings.Add("width", txtHTMLAreaWidth.Text.Trim());
                ffi.Settings.Add("height", txtHTMLAreaHeight.Text.Trim());
                ffi.Settings.Add("size", txtHTMLAreaSize.Text.Trim());
                ffi.Settings.Add("cssstylesheet", usHTMLAreaCss.Value);
                ffi.Settings.Add("toolbarset", txtHTMLAreaToolbarSet.Text.Trim());
                ffi.Settings.Add("startingpath", txtHTMLAreaStartingPath.Text.Trim());
                break;

            case FormFieldControlTypeCode.DROPDOWNLIST:
            case FormFieldControlTypeCode.RADIOBUTTONS:

                // Query text 
                if (radDropDownListSQL.Checked)
                {
                    ffi.Settings.Add("query", txtDropDownListOptions.Text);
                }
                // Dropdownlist options
                else
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    XmlNode optionItemNode;

                    // Add options node to xml document
                    XmlNode optionsNode = xmlDoc.CreateNode(XmlNodeType.Element, "options", "");
                    xmlDoc.AppendChild(optionsNode);

                    // Split input options text according to the new line character
                    string[] optionItemLines = txtDropDownListOptions.Text.Split('\n');

                    // Process each option line
                    foreach (string line in optionItemLines)
                    {
                        // Replace escaped semicolon character with constant
                        string tempLine = line.Replace("\\;", SEMICOLON);
                        if (tempLine.IndexOf(";") >= 0)
                        {
                            optionItemNode = xmlDoc.CreateNode(XmlNodeType.Element, "item", "");
                            if (optionItemNode != null)
                            {
                                // Value attribute
                                XmlAttribute valueAttribute = xmlDoc.CreateAttribute("value");
                                string dataValue = tempLine.Substring(0, tempLine.IndexOf(";")).Replace(SEMICOLON, ";").Trim();

                                // Ensure correct value for storation into database
                                if (lstAttributeType.SelectedValue == FormFieldDataTypeCode.DOUBLE)
                                {
                                    dataValue = FormHelper.GetDoubleValueInDBCulture(dataValue);
                                }
                                else if (lstAttributeType.SelectedValue == FormFieldDataTypeCode.DATETIME)
                                {
                                    dataValue = FormHelper.GetDateTimeValueInDBCulture(dataValue);
                                }

                                valueAttribute.Value = dataValue;

                                // Text attribute
                                XmlAttribute textAttribute = xmlDoc.CreateAttribute("text");
                                textAttribute.Value = tempLine.Substring(tempLine.IndexOf(";") + 1).Replace(SEMICOLON, ";").Trim();

                                // Add attributes to option item node
                                optionItemNode.Attributes.Append(valueAttribute);
                                optionItemNode.Attributes.Append(textAttribute);

                                // Add option item node to options node
                                optionsNode.AppendChild(optionItemNode);
                            }
                        }
                    }
                    // Add OptionsNode's inner xml to hashtable 
                    ffi.Settings.Add("options", optionsNode.InnerXml);
                }

                if (selectedType == FormFieldControlTypeCode.RADIOBUTTONS)
                {
                    ffi.Settings.Add("repeatdirection", drpRadioButtonsDirection.SelectedValue);
                }
                break;

            case FormFieldControlTypeCode.MULTIPLECHOICE:

                // Query text 
                if (radMultipleChoiceSQL.Checked)
                {
                    ffi.Settings.Add("query", txtMultipleChoiceOptions.Text);
                }
                // Dropdownlist options
                else
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    XmlNode optionItemNode;

                    // Add options node to xml document
                    XmlNode optionsNode = xmlDoc.CreateNode(XmlNodeType.Element, "options", "");
                    xmlDoc.AppendChild(optionsNode);

                    // Split input options text according to the new line character
                    string[] optionItemLines = txtMultipleChoiceOptions.Text.Split('\n');

                    // Process each option line
                    foreach (string line in optionItemLines)
                    {
                        // Replace escaped semicolon character with constant
                        string tempLine = line.Replace("\\;", SEMICOLON);
                        if (tempLine.IndexOf(";") >= 0)
                        {
                            optionItemNode = xmlDoc.CreateNode(XmlNodeType.Element, "item", "");
                            if (optionItemNode != null)
                            {
                                // Value attribute
                                XmlAttribute valueAttribute = xmlDoc.CreateAttribute("value");
                                valueAttribute.Value = tempLine.Substring(0, tempLine.IndexOf(";")).Replace(SEMICOLON, ";").Trim();

                                // Text attribute
                                XmlAttribute textAttribute = xmlDoc.CreateAttribute("text");
                                textAttribute.Value = tempLine.Substring(tempLine.IndexOf(";") + 1).Replace(SEMICOLON, ";").Trim();

                                // Add attributes to option item node
                                optionItemNode.Attributes.Append(valueAttribute);
                                optionItemNode.Attributes.Append(textAttribute);

                                // Add option item node to options node
                                optionsNode.AppendChild(optionItemNode);
                            }
                        }
                    }
                    // Add OptionsNode's inner xml to hashtable 
                    ffi.Settings.Add("options", optionsNode.InnerXml);
                }

                // Save repeat direction for checkbox list
                ffi.Settings.Add("repeatdirection", drpMultipleChoiceDirection.SelectedValue);

                break;

            case FormFieldControlTypeCode.LISTBOX:

                // Query text 
                if (radListBoxSQL.Checked)
                {
                    ffi.Settings.Add("query", txtListBoxOptions.Text);
                }
                // Dropdownlist options
                else
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    XmlNode optionItemNode;

                    // Add options node to xml document
                    XmlNode optionsNode = xmlDoc.CreateNode(XmlNodeType.Element, "options", "");
                    xmlDoc.AppendChild(optionsNode);

                    // Split input options text according to the new line character
                    string[] optionItemLines = txtListBoxOptions.Text.Split('\n');

                    // Process each option line
                    foreach (string line in optionItemLines)
                    {
                        // Replace escaped semicolon character with constant
                        string tempLine = line.Replace("\\;", SEMICOLON);
                        if (tempLine.IndexOf(";") >= 0)
                        {
                            optionItemNode = xmlDoc.CreateNode(XmlNodeType.Element, "item", "");
                            if (optionItemNode != null)
                            {
                                // Value attribute
                                XmlAttribute valueAttribute = xmlDoc.CreateAttribute("value");
                                valueAttribute.Value = tempLine.Substring(0, tempLine.IndexOf(";")).Replace(SEMICOLON, ";").Trim();

                                // Text attribute
                                XmlAttribute textAttribute = xmlDoc.CreateAttribute("text");
                                textAttribute.Value = tempLine.Substring(tempLine.IndexOf(";") + 1).Replace(SEMICOLON, ";").Trim();

                                // Add attributes to option item node
                                optionItemNode.Attributes.Append(valueAttribute);
                                optionItemNode.Attributes.Append(textAttribute);

                                // Add option item node to options node
                                optionsNode.AppendChild(optionItemNode);
                            }
                        }
                    }
                    // Add OptionsNode's inner xml to hashtable 
                    ffi.Settings.Add("options", optionsNode.InnerXml);
                }

                // Save "allow multiple choices" for listbox
                ffi.Settings.Add("allowmultiplechoices", chkListBoxMultipleChoices.Checked);

                break;

            case FormFieldControlTypeCode.CALENDAR:

                ffi.Settings.Add("editTime", chkCalendarSettingsEditTime.Checked.ToString().ToLower());
                ffi.Settings.Add("displayNow", chkCalendarSettingsShowNowLink.Checked.ToString().ToLower());
                break;

            case FormFieldControlTypeCode.IMAGEPATH:
            case FormFieldControlTypeCode.MEDIAPATH:
            case FormFieldControlTypeCode.FILEPATH:

                autoResizeElem.UpdateConfiguration(ffi.Settings);

                if (selectedType == FormFieldControlTypeCode.IMAGEPATH)
                {
                    ffi.Settings.Add("width", txtImageWidth.Text.Trim());
                    ffi.Settings.Add("height", txtImageHeight.Text.Trim());
                    ffi.Settings.Add("maxsidesize", txtImageMaxSideSize.Text.Trim());
                }
                break;

            case FormFieldControlTypeCode.UPLOAD:
                elemAutoResize.UpdateConfiguration(ffi.Settings);
                ffi.FileExtensions = fileExt.AllowedExtensions;
                fileExt.UpdateConfiguration(ffi.Settings);
                break;

            case FormFieldControlTypeCode.DIRECTUPLOAD:
                if (ucDocAttachSettings != null)
                {
                    ucDocAttachSettings.UpdateConfiguration(ffi.Settings);
                }
                break;

            case FormFieldControlTypeCode.DOCUMENT_ATTACHMENTS:
                if (ucDocAttachSettings != null)
                {
                    ucDocAttachSettings.UpdateConfiguration(ffi.Settings);
                }
                break;
        }

        switch (SelectedMode)
        {
            case FieldEditorSelectedModeEnum.Advanced:

                if (((Mode == FieldEditorModeEnum.BizFormDefinition) || (Mode == FieldEditorModeEnum.SystemTable))
                    && (lstAttributeType.SelectedValue == FormFieldDataTypeCode.FILE))
                {
                    // Allow to save <guid>.<extension>
                    ffi.DataType = FormFieldDataTypeEnum.Text;
                    ffi.Size = 500;
                }
                else if (lstAttributeType.SelectedValue == FormFieldDataTypeCode.DOCUMENT_ATTACHMENTS)
                {
                    ffi.DataType = FormFieldDataTypeEnum.DocumentAttachments;
                    ffi.Size = 200;
                }
                else
                {
                    // Allow to save just <guid>
                    ffi.DataType = FormHelper.GetFormFieldDataType(lstAttributeType.SelectedValue);
                    //ffi.Size = (this.txtAttributeSize.Enabled) ? Convert.ToInt32(this.txtAttributeSize.Text.Trim()) : 0;
                    ffi.Size = ValidationHelper.GetInteger(txtAttributeSize.Text.Trim(), 0);
                }

                // Part of database section                                
                ffi.Visible = chkDisplayInForm.Checked;
                ffi.Description = txtDescription.Text.Trim();
                ffi.SpellCheck = chkSpellCheck.Checked;

                // Validation section
                ffi.RegularExpression = txtRegExpr.Text.Trim();
                ffi.MinStringLength = (string.IsNullOrEmpty(txtMinLength.Text.Trim())) ? -1 : Convert.ToInt32(txtMinLength.Text.Trim());
                ffi.MaxStringLength = (string.IsNullOrEmpty(txtMaxLength.Text.Trim())) ? -1 : Convert.ToInt32(txtMaxLength.Text.Trim());

                if (ffi.DataType == FormFieldDataTypeEnum.LongInteger)
                {
                    ffi.MinLongNumericValue = (string.IsNullOrEmpty(txtMinValue.Text.Trim())) ? -1 : Convert.ToInt64(txtMinValue.Text.Trim());
                    ffi.MaxLongNumericValue = (string.IsNullOrEmpty(txtMaxValue.Text.Trim())) ? -1 : Convert.ToInt64(txtMaxValue.Text.Trim());
                }
                else
                {
                    ffi.MinNumericValue = (string.IsNullOrEmpty(txtMinValue.Text.Trim())) ? -1 : Convert.ToDouble(txtMinValue.Text.Trim());
                    ffi.MaxNumericValue = (string.IsNullOrEmpty(txtMaxValue.Text.Trim())) ? -1 : Convert.ToDouble(txtMaxValue.Text.Trim());
                }
                ffi.MinDateTimeValue = dateFrom.SelectedDateTime;
                ffi.MaxDateTimeValue = dateTo.SelectedDateTime;
                ffi.ValidationErrorMessage = txtErrorMessage.Text.Trim();

                // Design section
                ffi.CaptionStyle = txtCaptionStyle.Text.Trim();
                ffi.InputControlStyle = txtInputStyle.Text.Trim();
                ffi.ControlCssClass = txtControlCssClass.Text.Trim();

                break;

            case FieldEditorSelectedModeEnum.Simplified:

                ffi.Visible = true;

                // New field
                if (IsNewItemEdited)
                {
                    // Set field properties.
                    switch (selectedType)
                    {
                        case FormFieldControlTypeCode.TEXTBOX:
                            ffi.Size = Convert.ToInt32(txtSimpleTextBoxMaxLength.Text);
                            ffi.DataType = FormFieldDataTypeEnum.Text;
                            break;

                        case FormFieldControlTypeCode.DOCUMENT_ATTACHMENTS:
                            ffi.Size = 0;
                            ffi.DataType = FormFieldDataTypeEnum.Text;
                            break;

                        case FormFieldControlTypeCode.TEXTAREA:
                        case FormFieldControlTypeCode.BBEDITOR:
                        case FormFieldControlTypeCode.HTMLAREA:
                            ffi.Size = 0;
                            ffi.DataType = FormFieldDataTypeEnum.LongText;
                            break;

                        case FormFieldControlTypeCode.INTEGER_NUMBER_TEXTBOX:
                            ffi.Size = 0;
                            ffi.DataType = FormFieldDataTypeEnum.Integer;
                            break;

                        case FormFieldControlTypeCode.LONG_NUMBER_TEXTBOX:
                            ffi.Size = 0;
                            ffi.DataType = FormFieldDataTypeEnum.LongInteger;
                            break;

                        case FormFieldControlTypeCode.DECIMAL_NUMBER_TEXTBOX:
                            ffi.Size = 0;
                            ffi.DataType = FormFieldDataTypeEnum.Decimal;
                            break;

                        case FormFieldControlTypeCode.CHECKBOX:
                            ffi.Size = 0;
                            ffi.DataType = FormFieldDataTypeEnum.Boolean;
                            break;

                        case FormFieldControlTypeCode.CALENDAR:
                            ffi.Size = 0;
                            ffi.DataType = FormFieldDataTypeEnum.DateTime;
                            break;

                        case FormFieldControlTypeCode.UPLOAD:
                        case FormFieldControlTypeCode.DIRECTUPLOAD:
                            if ((Mode == FieldEditorModeEnum.BizFormDefinition) || (Mode == FieldEditorModeEnum.SystemTable))
                            {
                                // Allow to save <guid>.<extension>
                                ffi.Size = 500;
                                ffi.DataType = FormFieldDataTypeEnum.Text;
                            }
                            else
                            {
                                // Allow to save just <guid>
                                ffi.Size = 0;
                                ffi.DataType = FormFieldDataTypeEnum.File;
                            }
                            break;

                        case FormFieldControlTypeCode.MULTIPLECHOICE:
                        case FormFieldControlTypeCode.LISTBOX:
                        case FormFieldControlTypeCode.DROPDOWNLIST:
                        case FormFieldControlTypeCode.RADIOBUTTONS:
                        case FormFieldControlTypeCode.FILEPATH:
                        case FormFieldControlTypeCode.IMAGEPATH:
                        case FormFieldControlTypeCode.MEDIAPATH:
                            ffi.Size = 500;
                            ffi.DataType = FormFieldDataTypeEnum.Text;
                            break;

                        default:
                            if (selectedType.StartsWith(controlPrefix))
                            {
                                // New field -> use default datatype and size
                                if (ffiOriginal == null)
                                {
                                    // Get control data type
                                    string[] controlDefaultDataType = GetUserControlDefaultDataType(controlName);
                                    ffi.DataType = FormHelper.GetFormFieldDataType(controlDefaultDataType[0]);

                                    if (ffi.DataType == FormFieldDataTypeEnum.Text)
                                    {
                                        ffi.Size = Convert.ToInt32(txtSimpleTextBoxMaxLength.Text);
                                    }
                                    else
                                    {
                                        ffi.Size = 0;
                                    }
                                }
                            }
                            break;
                    }
                }
                // Existing field -> set advanced settings to original settings
                else
                {
                    if (selectedType == FormFieldControlTypeCode.TEXTBOX)
                    {
                        ffi.Size = Convert.ToInt32(txtSimpleTextBoxMaxLength.Text);
                    }
                    else if (selectedType.StartsWith(controlPrefix))
                    {
                        // Get control data type
                        string[] controlDefaultDataType = GetUserControlDefaultDataType(controlName);
                        ffi.DataType = FormHelper.GetFormFieldDataType(controlDefaultDataType[0]);

                        if (ffi.DataType == FormFieldDataTypeEnum.Text)
                        {
                            ffi.Size = Convert.ToInt32(txtSimpleTextBoxMaxLength.Text);
                        }
                        else
                        {
                            ffi.Size = 0;
                        }
                    }
                    else
                    {
                        if (ffiOriginal != null)
                        {
                            ffi.Size = ffiOriginal.Size;
                        }
                    }

                    if (ffiOriginal != null)
                    {
                        ffi.DataType = ffiOriginal.DataType;

                        // Part of database section                    
                        ffi.Description = ffiOriginal.Description;

                        // Validation section
                        ffi.RegularExpression = ffiOriginal.RegularExpression;
                        ffi.MinStringLength = ffiOriginal.MinStringLength;
                        ffi.MaxStringLength = ffiOriginal.MaxStringLength;
                        ffi.MinNumericValue = ffiOriginal.MinNumericValue;
                        ffi.MaxNumericValue = ffiOriginal.MaxNumericValue;
                        ffi.MinLongNumericValue = ffiOriginal.MinLongNumericValue;
                        ffi.MaxLongNumericValue = ffiOriginal.MaxLongNumericValue;
                        ffi.MinDateTimeValue = ffiOriginal.MinDateTimeValue;
                        ffi.MaxDateTimeValue = ffiOriginal.MaxDateTimeValue;
                        ffi.FileExtensions = ffiOriginal.FileExtensions;
                        ffi.ValidationErrorMessage = ffiOriginal.ValidationErrorMessage;

                        // Design section
                        ffi.CaptionStyle = ffiOriginal.CaptionStyle;
                        ffi.InputControlStyle = ffiOriginal.InputControlStyle;
                        ffi.ControlCssClass = ffiOriginal.ControlCssClass;
                    }
                }
                break;
        }

        return ffi;
    }


    /// <summary>
    /// Fill attribute types list
    /// </summary>
    private void LoadAttributeTypes()
    {
        if (lstAttributeType.Items.Count > 0)
        {
            return;
        }

        lstAttributeType.DataSource = FormHelper.GetFieldTypes(IsDocumentType);
        lstAttributeType.DataBind();

        // Remove file type when editing webpart properties 
        // Remove file type when editing custom tables - not implemented yet 
        if ((Mode == FieldEditorModeEnum.WebPartProperties) || (Mode == FieldEditorModeEnum.CustomTable) || (Mode == FieldEditorModeEnum.SystemTable))
        {
            ListItem item = lstAttributeType.Items.FindByValue(FormFieldDataTypeCode.FILE);
            if (item != null)
            {
                lstAttributeType.Items.Remove(item);
            }
        }
    }


    /// <summary>
    /// Show default value control according attribute type
    /// </summary>
    protected void ShowDefaultValue()
    {
        chkDefaultValue.Visible = false;
        txtDefaultValue.Visible = false;
        txtLargeDefaultValue.Visible = false;
        datetimeDefaultValue.Visible = false;
        string fieldType = null;

        switch (SelectedMode)
        {
            case FieldEditorSelectedModeEnum.Simplified:

                fieldType = lstFieldType.SelectedValue.ToLower();

                switch (fieldType)
                {
                    // CALENDAR
                    case FormFieldControlTypeCode.CALENDAR:
                        datetimeDefaultValue.Visible = true;
                        break;

                    // CHECKBOX
                    case FormFieldControlTypeCode.CHECKBOX:
                        chkDefaultValue.Visible = true;
                        break;

                    // LARGETEXTAREA
                    case FormFieldControlTypeCode.TEXTAREA:
                    case FormFieldControlTypeCode.BBEDITOR:
                    case FormFieldControlTypeCode.HTMLAREA:
                        txtLargeDefaultValue.Visible = true;
                        break;

                    default:

                        // USER CONTROL                       
                        if (fieldType.StartsWith(controlPrefix))
                        {
                            string controlName = fieldType.Substring(controlPrefix.Length);
                            string[] defaultControlDataType = GetUserControlDefaultDataType(controlName);

                            // Display default value according to default datatype
                            string controlDataType = IsNewItemEdited ? defaultControlDataType[0] : FieldDataType;

                            switch (controlDataType)
                            {
                                case FormFieldDataTypeCode.DATETIME:
                                    datetimeDefaultValue.Visible = true;
                                    break;

                                case FormFieldDataTypeCode.BOOLEAN:
                                    chkDefaultValue.Visible = true;
                                    break;

                                default:
                                    txtDefaultValue.Visible = true;
                                    break;
                            }
                        }
                        // OTHER CONTROLS
                        else
                        {
                            txtDefaultValue.Visible = true;
                        }
                        break;
                }
                break;

            case FieldEditorSelectedModeEnum.Advanced:

                switch (lstAttributeType.SelectedValue.ToLower())
                {
                    case FormFieldDataTypeCode.DATETIME:
                        datetimeDefaultValue.Visible = true;
                        break;

                    case FormFieldDataTypeCode.BOOLEAN:
                        chkDefaultValue.Visible = true;
                        break;

                    case FormFieldDataTypeCode.LONGTEXT:
                        txtLargeDefaultValue.Visible = true;
                        break;

                    default:
                        txtDefaultValue.Visible = true;
                        break;
                }
                break;
        }
    }


    /// <summary>
    /// Shows additional attribute type options like timezone selection for datetime etc.
    /// </summary>
    private void ShowAttributeTypeOptions()
    {
        string type = lstAttributeType.SelectedValue.ToLower();
        plcTimeOptions.Visible = (type == FormFieldDataTypeCode.DATETIME);
    }


    /// <summary>
    /// Sets attribute type options
    /// </summary>
    private void SetAttributeTypeOptions(bool useOriginal)
    {
        string enable = "false";

        if (useOriginal && (ffi != null) && (ffi.DataType == FormFieldDataTypeEnum.DateTime))
        {
            if (ffi.Settings.Contains("timezone"))
            {
                timeZone.Value = ffi.Settings["timezone"];
            }
            else
            {
                timeZone.Value = null;
            }
            if (ffi.Settings.Contains("timezonetype"))
            {
                timeZoneType.Value = ffi.Settings["timezonetype"].ToString().ToLower();
                enable = ffi.Settings["timezonetype"].ToString().ToLower() == "custom" ? "true" : "false";
            }
        }
    }


    /// <summary>
    /// Set default value according to attribute type
    /// </summary>
    protected void SetDefaultValue()
    {
        string fieldType = lstFieldType.SelectedValue.ToLower();

        txtDefaultValue.Text = "";
        txtLargeDefaultValue.Value = "";
        chkDefaultValue.Checked = false;
        datetimeDefaultValue.SelectedDateTime = DateTimePicker.NOT_SELECTED;

        if (ffi != null)
        {
            switch (SelectedMode)
            {
                // Simplified mode
                case FieldEditorSelectedModeEnum.Simplified:

                    // Calendar or UserControl with datetime datatype
                    if ((fieldType == FormFieldControlTypeCode.CALENDAR) ||
                        ((fieldType.StartsWith(controlPrefix)) && (FieldDataType == FormFieldDataTypeCode.DATETIME)))
                    {
                        if (string.IsNullOrEmpty(ffi.DefaultValue))
                        {
                            datetimeDefaultValue.DateTimeTextBox.Text = "";
                        }
                        else if (ffi.DefaultValue.ToLower() == DateTimePicker.DATE_TODAY.ToLower())
                        {
                            datetimeDefaultValue.DateTimeTextBox.Text = DateTimePicker.DATE_TODAY;
                        }
                        else if (ffi.DefaultValue.ToLower() == DateTimePicker.TIME_NOW.ToLower())
                        {
                            datetimeDefaultValue.DateTimeTextBox.Text = DateTimePicker.TIME_NOW;
                        }
                        else
                        {
                            datetimeDefaultValue.SelectedDateTime = FormHelper.GetDateTimeValueInCurrentCulture(ffi.DefaultValue);
                        }

                        //if (ffi.DefaultValue == "")
                        //{
                        //    datetimeDefaultValue.DateTimeTextBox.Text = "";
                        //}
                        //else
                        //{
                        //    datetimeDefaultValue.SelectedDateTime = Convert.ToDateTime(ffi.DefaultValue);
                        //}
                    }
                    // DecimalNumberTextBox, Textbox with decimal filed or UserControl with double datatype
                    else if (ffi.DataType == FormFieldDataTypeEnum.Decimal)
                    {
                        txtDefaultValue.Text = FormHelper.GetDoubleValueInCurrentCulture(ffi.DefaultValue);
                    }
                    else if (ffi.DataType == FormFieldDataTypeEnum.DateTime)
                    {
                        txtDefaultValue.Text = FormHelper.GetDateTimeValueInCurrentCulture(ffi.DefaultValue).ToString();
                    }
                    // Checkbox or UserControl with boolean datatype
                    else if ((fieldType == FormFieldControlTypeCode.CHECKBOX) ||
                        ((fieldType.StartsWith(controlPrefix)) && (FieldDataType == FormFieldDataTypeCode.BOOLEAN)))
                    {
                        chkDefaultValue.Checked = ValidationHelper.GetBoolean(ffi.DefaultValue, false);
                    }
                    // TextArea, HTMLArea or UserControl with longtext datatype
                    else if ((fieldType == FormFieldControlTypeCode.HTMLAREA) || (fieldType == FormFieldControlTypeCode.TEXTAREA) || (fieldType == FormFieldControlTypeCode.BBEDITOR) ||
                   ((fieldType.StartsWith(controlPrefix)) && (FieldDataType == FormFieldDataTypeCode.LONGTEXT)))
                    {
                        txtLargeDefaultValue.Value = ffi.DefaultValue;
                    }
                    else
                    {
                        txtDefaultValue.Text = ffi.DefaultValue;
                    }
                    break;

                // Advanced mode
                case FieldEditorSelectedModeEnum.Advanced:

                    switch (lstAttributeType.SelectedValue.ToLower())
                    {
                        case FormFieldDataTypeCode.DATETIME:
                            if (string.IsNullOrEmpty(ffi.DefaultValue))
                            {
                                datetimeDefaultValue.DateTimeTextBox.Text = "";
                            }
                            else if (ffi.DefaultValue.ToLower() == DateTimePicker.DATE_TODAY.ToLower())
                            {
                                datetimeDefaultValue.DateTimeTextBox.Text = DateTimePicker.DATE_TODAY;
                            }
                            else if (ffi.DefaultValue.ToLower() == DateTimePicker.TIME_NOW.ToLower())
                            {
                                datetimeDefaultValue.DateTimeTextBox.Text = DateTimePicker.TIME_NOW;
                            }
                            else
                            {
                                datetimeDefaultValue.SelectedDateTime = FormHelper.GetDateTimeValueInCurrentCulture(ffi.DefaultValue);
                            }
                            break;

                        case FormFieldDataTypeCode.BOOLEAN:
                            chkDefaultValue.Checked = ValidationHelper.GetBoolean(ffi.DefaultValue, false);
                            break;

                        case FormFieldDataTypeCode.LONGTEXT:
                            txtLargeDefaultValue.Value = ffi.DefaultValue;
                            break;

                        case FormFieldDataTypeCode.DOUBLE:
                            txtDefaultValue.Text = FormHelper.GetDoubleValueInCurrentCulture(ffi.DefaultValue);
                            break;

                        default:
                            txtDefaultValue.Text = ffi.DefaultValue;
                            break;
                    }
                    break;
            }
        }
        else
        {
            txtDefaultValue.Text = "";
            txtLargeDefaultValue.Value = "";
            chkDefaultValue.Checked = false;
            datetimeDefaultValue.SelectedDateTime = DateTimePicker.NOT_SELECTED;
        }
    }


    /// <summary>
    /// Returns default value according to attribute type
    /// </summary>
    protected string GetDefaultValue()
    {
        string defaultValue = null;
        //System.Globalization.CultureInfo ciEnUs = new System.Globalization.CultureInfo("en-us"); // Culture info for datetime conversion to string

        switch (SelectedMode)
        {
            // Simplified mode
            case FieldEditorSelectedModeEnum.Simplified:

                string controlDataType = "";
                string fieldType = lstFieldType.SelectedValue.ToLower();

                /*   if (fieldType.StartsWith(controlPrefix))
                   {
                       string controlName = fieldType.Substring(controlPrefix.Length);
                       string[] defaultControlDataType = GetUserControlDefaultDataType(controlName);
                    
                       // Display default value according to default datatype
                       if (IsNewItemEdited)
                       {
                           controlDataType = defaultControlDataType[0];
                       }
                       // Display default value according to saved datatype
                       else
                       {
                           controlDataType = FieldDataType;
                       }
                   }*/

                // Calendar or UserControl with boolean datatype
                if ((fieldType == FormFieldControlTypeCode.CALENDAR) ||
                    ((fieldType.StartsWith(controlPrefix)) && (controlDataType == FormFieldDataTypeCode.DATETIME)))
                {
                    string datetimevalue = datetimeDefaultValue.DateTimeTextBox.Text.Trim();
                    if (datetimevalue.ToLower() == DateTimePicker.DATE_TODAY.ToLower())
                    {
                        defaultValue = DateTimePicker.DATE_TODAY;
                    }
                    else if (datetimevalue.ToLower() == DateTimePicker.TIME_NOW.ToLower())
                    {
                        defaultValue = DateTimePicker.TIME_NOW;
                    }
                    else if (datetimeDefaultValue.SelectedDateTime == DateTimePicker.NOT_SELECTED)
                    {
                        defaultValue = "";
                    }
                    else
                    {
                        defaultValue = datetimeDefaultValue.SelectedDateTime.ToString();
                    }
                }
                // Checkbox or UserControl with boolean datatype
                else if ((fieldType == FormFieldControlTypeCode.CHECKBOX) ||
                    ((fieldType.StartsWith(controlPrefix)) && (controlDataType == FormFieldDataTypeCode.BOOLEAN)))
                {
                    defaultValue = Convert.ToString(chkDefaultValue.Checked).ToLower();
                }
                else if ((fieldType == FormFieldControlTypeCode.HTMLAREA) || (fieldType == FormFieldControlTypeCode.TEXTAREA) || (fieldType == FormFieldControlTypeCode.BBEDITOR) ||
                ((fieldType.StartsWith(controlPrefix)) && (controlDataType == FormFieldDataTypeCode.LONGTEXT)))
                {
                    defaultValue = txtLargeDefaultValue.Value.ToString().Trim();
                }
                else if (fieldType == FormFieldControlTypeCode.DOCUMENT_ATTACHMENTS)
                {
                    string defValue = txtDefaultValue.Text.Trim();
                    defaultValue = (string.IsNullOrEmpty(defValue)) ? null : defValue;
                }
                else
                {
                    defaultValue = txtDefaultValue.Text.Trim();
                }

                break;

            // Advanced mode
            case FieldEditorSelectedModeEnum.Advanced:

                switch (lstAttributeType.SelectedValue.ToLower())
                {
                    case FormFieldDataTypeCode.DATETIME:
                        string datetimevalue = datetimeDefaultValue.DateTimeTextBox.Text.Trim();
                        if (datetimevalue.ToLower() == DateTimePicker.DATE_TODAY.ToLower())
                        {
                            defaultValue = DateTimePicker.DATE_TODAY;
                        }
                        else if (datetimevalue.ToLower() == DateTimePicker.TIME_NOW.ToLower())
                        {
                            defaultValue = DateTimePicker.TIME_NOW;
                        }
                        else if (datetimeDefaultValue.SelectedDateTime == DateTimePicker.NOT_SELECTED)
                        {
                            defaultValue = "";
                        }
                        else
                        {
                            defaultValue = datetimeDefaultValue.SelectedDateTime.ToString();
                        }
                        break;

                    case FormFieldDataTypeCode.BOOLEAN:
                        defaultValue = Convert.ToString(chkDefaultValue.Checked).ToLower();
                        break;

                    case FormFieldDataTypeCode.LONGTEXT:
                        defaultValue = txtLargeDefaultValue.Value.ToString().Trim();
                        break;

                    case FormFieldControlTypeCode.DOCUMENT_ATTACHMENTS:
                        string defValue = txtDefaultValue.Text.Trim();
                        defaultValue = (string.IsNullOrEmpty(defValue)) ? null : defValue;
                        break;

                    default:
                        defaultValue = txtDefaultValue.Text.Trim();
                        break;
                }
                break;
        }

        return defaultValue;
    }


    /// <summary>
    /// Displays selected tab content.
    /// </summary>
    protected void DisplaySelectedTabContent()
    {
        switch (SelectedMode)
        {
            case FieldEditorSelectedModeEnum.Simplified:
                pnlAdvancedAttributeEdit.Visible = false;
                pnlSimpleAttributeEdit.Visible = true;
                break;

            case FieldEditorSelectedModeEnum.Advanced:
                pnlAdvancedAttributeEdit.Visible = true;
                pnlSimpleAttributeEdit.Visible = false;
                break;
        }
    }


    /// <summary>
    /// Initialize links for mode switching.
    /// </summary>
    protected void InitializeLinks()
    {
        if (EnableSimplifiedMode)
        {
            if (SelectedItemType == FieldEditorSelectedItemEnum.Field)
            {
                btnAdvanced.Visible = (SelectedMode == FieldEditorSelectedModeEnum.Simplified);
                btnSimplified.Visible = (SelectedMode != FieldEditorSelectedModeEnum.Simplified);
            }
            else
            {
                btnAdvanced.Visible = false;
                btnSimplified.Visible = false;
            }
        }
    }


    /// <summary>
    /// Called by javascript postback when tab is clicked 
    /// </summary>
    protected void btnSimplified_Command(object sender, CommandEventArgs e)
    {
        SelectedMode = FieldEditorSelectedModeEnum.Simplified;
        Reload(lstAttributes.SelectedValue);
    }


    /// <summary>
    /// Called by javascript postback when tab is clicked
    /// </summary>
    protected void btnAdvanced_Command(object sender, CommandEventArgs e)
    {
        SelectedMode = FieldEditorSelectedModeEnum.Advanced;
        Reload(lstAttributes.SelectedValue);
    }


    /// <summary>
    /// Returns default selected mode.
    /// </summary>
    private FieldEditorSelectedModeEnum GetDefaultSelectedMode()
    {
        if (EnableSimplifiedMode)
        {
            return FieldEditorSelectedModeEnum.Simplified;
        }
        else
        {
            return FieldEditorSelectedModeEnum.Advanced;
        }
    }


    /// <summary>
    /// Sets universal controls to controls of the selected mode.
    /// </summary>
    private void SetUniversalControls()
    {
        if (SelectedItemType == FieldEditorSelectedItemEnum.Field)
        {
            switch (SelectedMode)
            {
                case FieldEditorSelectedModeEnum.Simplified:

                    #region "Set universal controls to simplified mode controls"

                    // Basic controls 
                    //--------------------------------------------------------

                    // Attribute name
                    lblAttributeName = lblSimpleColumnName;
                    txtAttributeName = txtSimpleColumnName;

                    // Allow empty 
                    lblAttributeAllowEmpty = lblSimpleAllowEmpty;
                    chkAllowEmpty = chkSimpleAllowEmpty;

                    // Is system
                    lblAttributeIsSystem = lblSimpleIsSystem;
                    chkAttributeIsSystem = chkSimpleIsSystem;
                    plcAttributeIsSystem = plcSimpleIsSystem;

                    // Default value types
                    lblDefaultValue = lblSimpleDefaultValue;
                    txtDefaultValue = txtSimpleDefault;
                    txtLargeDefaultValue = txtSimpleLargeDefault;
                    chkDefaultValue = chkSimpleDefault;
                    datetimeDefaultValue = datetimeSimpleDefault;

                    // Field caption
                    lblFieldCaption = lblSimpleFieldCaption;
                    txtFieldCaption = txtSimpleFieldCaption;

                    // Field type
                    lblFieldType = lblSimpleFieldType;
                    lstFieldType = lstSimpleFieldType;

                    // Attribute type options
                    plcTimeOptions = plcSimpleTimeOptions;
                    lblTimeZone = lblSimpleTimeZone;
                    lblTimeZoneType = lblSimpleTimeZoneType;
                    timeZone = SimpleTimeZone;
                    timeZoneType = SimpletimeZoneType;


                    // Additional controls 
                    //--------------------------------------------------------

                    // Panel with upload file settings
                    pnlUploadFileSettings = pnlSimpleUploadFile;
                    lblUploadFile = lblSimpleUploadFile;
                    elemAutoResize = autoResizeSimple;

                    //----------------------------------------

                    // Panel with text area settings
                    pnlTextAreaSettings = pnlSimpleTextAreaSettings;

                    // Text area columns
                    lblTextAreaCols = lblSimpleTextAreaCols;
                    txtTextAreaCols = txtSimpleTextAreaCols;

                    // Text area rows
                    lblTextAreaRows = lblSimpleTextAreaRows;
                    txtTextAreaRows = txtSimpleTextAreaRows;

                    // Text area size
                    lblTextAreaSize = lblSimpleTextAreaSize;
                    txtTextAreaSize = txtSimpleTextAreaSize;

                    //----------------------------------------

                    // Panel with BBEditor settings
                    pnlBBEditorSettings = pnlSimpleBBEditorSettings;

                    // BBEditor Text area columns
                    lblBBEditorCols = lblSimpleBBEditorCols;
                    txtBBEditorCols = txtSimpleBBEditorCols;

                    // BBEditor Text area rows
                    lblBBEditorRows = lblSimpleBBEditorRows;
                    txtBBEditorRows = txtSimpleBBEditorRows;

                    // BBEditor Text area size
                    lblBBEditorSize = lblSimpleBBEditorSize;
                    txtBBEditorSize = txtSimpleBBEditorSize;

                    //----------------------------------------

                    // Panel with html area settings
                    pnlHTMLAreaSettings = pnlSimpleHTMLAreaSettings;

                    // Html area width
                    lblHTMLAreaWidth = lblSimpleHTMLAreaWidth;
                    txtHTMLAreaWidth = txtSimpleHTMLAreaWidth;

                    // Html area height
                    lblHTMLAreaHeight = lblSimpleHTMLAreaHeight;
                    txtHTMLAreaHeight = txtSimpleHTMLAreaHeight;

                    // Html area size
                    lblHTMLAreaSize = lblSimpleHTMLAreaSize;
                    txtHTMLAreaSize = txtSimpleHTMLAreaSize;

                    // Html area css stylesheet
                    lblHTMLAreaCss = lblSimpleCssStyleSheet;
                    usHTMLAreaCss = ctrlSimpleStyleSheet;
                    ctrlSimpleStyleSheet.StopProcessing = false;

                    // Html area toolbarset
                    lblHTMLAreaToolbarSet = lblSimpleToolbarSet;
                    txtHTMLAreaToolbarSet = txtSimpleToolbarSet;

                    // Html area starting path
                    lblHTMLAreaStartingPath = lblSimpleStartingPath;
                    txtHTMLAreaStartingPath = txtSimpleStartingPath;

                    //----------------------------------------

                    // Panel with drop down list settings
                    pnlDropDownListSettings = pnlSimpleDropDownListSettings;

                    plcRadioButtonsDirection = plcSimpleRadioButtonsDirection;

                    lblRadioButtonsDirection = lblSimpleRadioButtonsDirection;
                    drpRadioButtonsDirection = drpSimpleRadioButtonsDirection;

                    // Drop down list options radiobutton
                    radDropDownListOptions = radSimpleDropDownListOptions;

                    // Drop down list sql radiobutton 
                    radDropDownListSQL = radSimpleDropDownListSQL;

                    // Drop down list options textbox    
                    txtDropDownListOptions = txtSimpleDropDownListOptions;

                    //----------------------------------------

                    // Panel with calendar settings
                    pnlCalendarSettings = pnlSimpleCalendarSettings;

                    // Edit time checkbox
                    chkCalendarSettingsEditTime = chkSimpleCalendarSettingsEditTime;

                    // Show now link
                    chkCalendarSettingsShowNowLink = chkSimpleCalendarSettingsShowNowLink;

                    //----------------------------------------

                    // Panel with multiple choice settings
                    pnlMultipleChoice = pnlSimpleMultipleChoice;

                    lblMultipleChoiceDirection = lblSimpleMultipleChoiceDirection;
                    drpMultipleChoiceDirection = drpSimpleMultipleChoiceDirection;

                    // Multiple choice options radiobutton
                    radMultipleChoiceOptions = radSimpleMultipleChoiceOptions;

                    // Multiple choice sql radiobutton 
                    radMultipleChoiceSQL = radSimpleMultipleChoiceSQL;

                    // Multiple choice options textbox    
                    txtMultipleChoiceOptions = txtSimpleMultipleChoiceOptions;

                    //----------------------------------------

                    // Panel with list box settings
                    pnlListBox = pnlSimpleListBox;

                    lblListBoxMultipleChoices = lblSimpleListBoxMultipleChoices;
                    chkListBoxMultipleChoices = chkSimpleListBoxMultipleChoices;

                    // List box options radiobutton
                    radListBoxOptions = radSimpleListBoxOptions;

                    // List box sql radiobutton 
                    radListBoxSQL = radSimpleListBoxSQL;

                    // List box options textbox    
                    txtListBoxOptions = txtSimpleListBoxOptions;

                    //----------------------------------------

                    // Panel with image selection settings
                    pnlImageSelection = pnlSimpleImageSelection;

                    lblImageWidth = lblSimpleImageWidth;
                    lblImageHeight = lblSimpleImageHeight;
                    lblImageMaxSideSize = lblSimpleImageMaxSideSize;
                    txtImageWidth = txtSimpleImageWidth;
                    txtImageHeight = txtSimpleImageHeight;
                    txtImageMaxSideSize = txtSimpleImageMaxSideSize;

                    //----------------------------------------

                    // Panel with public field option
                    pnlPublicField = pnlSimplePublicField;
                    lblPublicField = lblSimplePublicField;
                    chkPublicField = chkSimplePublicField;

                    //----------------------------------------
                    // Panel with all editable controls
                    pnlAttributeEdit = pnlSimpleAttributeEdit;

                    #endregion


                    // Enable or disable some controls in simplified mode while editing existing field
                    if (SelectedItemType == FieldEditorSelectedItemEnum.Field)
                    {
                        if (IsNewItemEdited)
                        {
                            // Editing new item
                            txtSimpleColumnName.Enabled = true;
                            txtSimpleColumnName.ReadOnly = false;
                            lstSimpleFieldType.Enabled = true;
                        }
                        else
                        {
                            // Editing existing item
                            txtSimpleColumnName.Enabled = false;
                            txtSimpleColumnName.ReadOnly = true;
                            lstSimpleFieldType.Enabled = false;
                        }
                    }

                    lblAttributeName.Text = ResHelper.GetString("TemplateDesigner.ColumName");
                    lblDefaultValue.Text = ResHelper.GetString("TemplateDesigner.ColumnDefaultValue");
                    lblSimpleTextBoxMaxLength.Text = ResHelper.GetString("TemplateDesigner.TextBoxLength");

                    break;

                case FieldEditorSelectedModeEnum.Advanced:

                    #region "Set universal controls to advanced mode controls"

                    // Basic controls 
                    //--------------------------------------------------------

                    // Attribute name
                    lblAttributeName = lblAdvancedAttributeName;
                    txtAttributeName = txtAdvancedAttributeName;

                    // Allow empty 
                    lblAttributeAllowEmpty = lblAdvancedAllowEmpty;
                    chkAllowEmpty = chkAdvancedAllowEmpty;

                    // Is system
                    lblAttributeIsSystem = lblAdvancedIsSystem;
                    chkAttributeIsSystem = chkAdvancedIsSystem;
                    plcAttributeIsSystem = plcAdvancedIsSystem;

                    // Default value types
                    lblDefaultValue = lblAdvancedDefaultValue;
                    txtDefaultValue = txtAdvancedDefaultValue;
                    txtLargeDefaultValue = txtAdvancedLargeDefaultValue;
                    chkDefaultValue = chkAdvancedDefaultValue;
                    datetimeDefaultValue = datetimeAdvancedDefatulValue;

                    // Field caption
                    lblFieldCaption = lblAdvancedFieldCaption;
                    txtFieldCaption = txtAdvancedFieldCaption;

                    // Field type
                    lblFieldType = lblAdvancedFieldType;
                    lstFieldType = lstAdvancedFieldType;

                    // Attribute type options
                    plcTimeOptions = plcAdvancedTimeOptions;
                    lblTimeZone = lblAdvancedTimeZone;
                    lblTimeZoneType = lblAdvancedTimeZoneType;
                    timeZone = advancedTimeZone;
                    timeZoneType = advancedtimeZoneType;


                    // Additional controls 
                    //--------------------------------------------------------

                    // Panel with upload file settings
                    pnlUploadFileSettings = pnlAdvacnedUploadFile;
                    lblUploadFile = lblAdvancedUploadFile;
                    elemAutoResize = autoResizeAdvanced;

                    //----------------------------------------

                    // Panel with text area settings
                    pnlTextAreaSettings = pnlAdvancedTextAreaSettings;

                    // Text area columns
                    lblTextAreaCols = lblAdvancedTextAreaCols;
                    txtTextAreaCols = txtAdvancedTextAreaCols;

                    // Text area rows
                    lblTextAreaRows = lblAdvancedTextAreaRows;
                    txtTextAreaRows = txtAdvancedTextAreaRows;

                    // Text area size
                    lblTextAreaSize = lblAdvancedTextAreaSize;
                    txtTextAreaSize = txtAdvancedTextAreaSize;

                    //----------------------------------------

                    // Panel BBEditor settings
                    pnlBBEditorSettings = pnlAdvancedBBEditorSettings;

                    // BBEditor Text area columns
                    lblBBEditorCols = lblAdvancedBBEditorCols;
                    txtBBEditorCols = txtAdvancedBBEditorCols;

                    // BBEditor Text area rows
                    lblBBEditorRows = lblAdvancedBBEditorRows;
                    txtBBEditorRows = txtAdvancedBBEditorRows;

                    // BBEditor Text area size
                    lblBBEditorSize = lblAdvancedBBEditorSize;
                    txtBBEditorSize = txtAdvancedBBEditorSize;

                    // BBEditor checkboxes
                    chkBBEditorSettingsShowBold = chkAdvancedBBEditorShowBold;
                    chkBBEditorSettingsShowCode = chkAdvancedBBEditorShowCode;
                    chkBBEditorSettingsShowColor = chkAdvancedBBEditorShowColor;
                    chkBBEditorSettingsShowItalic = chkAdvancedBBEditorShowItalic;
                    chkBBEditorSettingsShowQuote = chkAdvancedBBEditorShowQuote;
                    chkBBEditorSettingsShowStrike = chkAdvancedBBEditorShowStrike;
                    chkBBEditorSettingsShowUnderline = chkAdvancedBBEditorShowUnderline;
                    chkBBEditorSettingsUsePromptDialog = chkAdvancedBBEditorUsePromptDialog;

                    //----------------------------------------

                    // Panel document attachments settings
                    pnlDocumentAttachmentsSettings = pnlAdvancedDocAttachmentsSettings;
                    ucDocAttachSettings = ucAdvancedDocAttachments;

                    //----------------------------------------

                    // Panel with html area settings
                    pnlHTMLAreaSettings = pnlAdvancedHTMLAreaSettings;

                    // Html area width
                    lblHTMLAreaWidth = lblAdvancedHTMLAreaWidth;
                    txtHTMLAreaWidth = txtAdvancedHTMLAreaWidth;

                    // Html area height
                    lblHTMLAreaHeight = lblAdvancedHTMLAreaHeight;
                    txtHTMLAreaHeight = txtAdvancedHTMLAreaHeight;

                    // Html area size
                    lblHTMLAreaSize = lblAdvancedHTMLAreaSize;
                    txtHTMLAreaSize = txtAdvancedHTMLAreaSize;


                    // Html area css stylesheet
                    lblHTMLAreaCss = lblAdvancedCssStyleSheet;
                    usHTMLAreaCss = ctrlAdvancedStyleSheet;
                    ctrlAdvancedStyleSheet.StopProcessing = false;

                    // Html area toolbarset
                    lblHTMLAreaToolbarSet = lblAdvancedToolbarSet;
                    txtHTMLAreaToolbarSet = txtAdvancedToolbarSet;

                    // Html area starting path
                    lblHTMLAreaStartingPath = lblAdvancedStartingPath;
                    txtHTMLAreaStartingPath = txtAdvancedStartingPath;

                    //----------------------------------------

                    // Panel with drop down list settings
                    pnlDropDownListSettings = pnlAdvancedDropDownListSettings;

                    plcRadioButtonsDirection = plcAdvancedRadioButtonsDirection;

                    lblRadioButtonsDirection = lblAdvancedRadioButtonsDirection;
                    drpRadioButtonsDirection = drpAdvancedRadioButtonsDirection;

                    // Drop down list options radiobutton
                    radDropDownListOptions = radAdvancedDropDownListOptions;

                    // Drop down list sql radiobutton 
                    radDropDownListSQL = radAdvancedDropDownListSQL;

                    // Drop down list options textbox    
                    txtDropDownListOptions = txtAdvancedDropDownListOptions;

                    //----------------------------------------

                    // Panel with calendar settings
                    pnlCalendarSettings = pnlAdvancedCalendarSettings;

                    // Edit time checkbox
                    chkCalendarSettingsEditTime = chkAdvancedCalendarSettingsEditTime;

                    // Show now link
                    chkCalendarSettingsShowNowLink = chkAdvancedCalendarSettingsShowNowLink;

                    //----------------------------------------

                    // Panel with multiple choice settings
                    pnlMultipleChoice = pnlAdvancedMultipleChoice;

                    lblMultipleChoiceDirection = lblAdvancedMultipleChoiceDirection;
                    drpMultipleChoiceDirection = drpAdvancedMultipleChoiceDirection;

                    // Multiple choice options radiobutton
                    radMultipleChoiceOptions = radAdvancedMultipleChoiceOptions;

                    // Multiple choice sql radiobutton 
                    radMultipleChoiceSQL = radAdvancedMultipleChoiceSQL;

                    // Multiple choice options textbox    
                    txtMultipleChoiceOptions = txtAdvancedMultipleChoiceOptions;

                    //----------------------------------------

                    // Panel with list box settings
                    pnlListBox = pnlAdvancedListBox;

                    lblListBoxMultipleChoices = lblAdvancedListBoxMultipleChoices;
                    chkListBoxMultipleChoices = chkAdvancedListBoxMultipleChoices;

                    // List box options radiobutton
                    radListBoxOptions = radAdvancedListBoxOptions;

                    // List box sql radiobutton 
                    radListBoxSQL = radAdvancedListBoxSQL;

                    // List box options textbox    
                    txtListBoxOptions = txtAdvancedListBoxOptions;

                    //----------------------------------------

                    // Panel with image selection settings
                    pnlImageSelection = pnlAdvancedImageSelection;

                    lblImageWidth = lblAdvancedImageWidth;
                    lblImageHeight = lblAdvancedImageHeight;
                    lblImageMaxSideSize = lblAdvancedImageMaxSideSize;
                    txtImageWidth = txtAdvancedImageWidth;
                    txtImageHeight = txtAdvancedImageHeight;
                    txtImageMaxSideSize = txtAdvancedImageMaxSideSize;

                    //----------------------------------------

                    // Panel with public field option
                    pnlPublicField = pnlAdvancedPublicField;
                    lblPublicField = lblAdvancedPublicField;
                    chkPublicField = chkAdvancedPublicField;

                    //----------------------------------------

                    // Panel with all editable controls
                    pnlAttributeEdit = pnlAdvancedAttributeEdit;

                    #endregion

                    lblAttributeName.Text = ResHelper.GetString("TemplateDesigner.AttributeName");
                    lblDefaultValue.Text = ResHelper.GetString("TemplateDesigner.AttributeDefaultValue");

                    break;
            }

            if (usHTMLAreaCss != null)
            {
                usHTMLAreaCss.CurrentDropDown.CssClass = "SmallDropDown";
                usHTMLAreaCss.CurrentSelector.IsLiveSite = false;
                usHTMLAreaCss.CurrentSelector.SpecialFields = new string[1, 2] { { String.Empty, String.Empty } };
            }

            // Public field option initialization    
            if ((Mode == FieldEditorModeEnum.BizFormDefinition) ||
                (DisplayedControls == FieldEditorControlsEnum.Bizforms))
            {
                pnlPublicField.Visible = true;
                lblPublicField.Text = ResHelper.GetString("TemplateDesigner.PublicField");
            }
            else
            {
                pnlPublicField.Visible = false;
            }

            // Universal control initialization        
            lblFieldCaption.Text = ResHelper.GetString("TemplateDesigner.FieldCaption");
            lblFieldType.Text = ResHelper.GetString("TemplateDesigner.FieldType");
            lblAttributeAllowEmpty.Text = ResHelper.GetString("TemplateDesigner.AttributeAllowEmpty");
            lblAttributeIsSystem.Text = ResHelper.GetString("TemplateDesigner.AttributeIsSystem");
            lblTextAreaCols.Text = ResHelper.GetString("TemplateDesigner.TextAreaCols");
            lblTextAreaRows.Text = ResHelper.GetString("TemplateDesigner.TextAreaRows");
            lblTextAreaSize.Text = ResHelper.GetString("TemplateDesigner.TextAreaSize");
            lblHTMLAreaHeight.Text = ResHelper.GetString("TemplateDesigner.HTMLAreaHeight");
            lblHTMLAreaWidth.Text = ResHelper.GetString("TemplateDesigner.HTMLAreaWidth");
            lblHTMLAreaSize.Text = ResHelper.GetString("TemplateDesigner.HTMLAreaSize");
            lblHTMLAreaCss.Text = ResHelper.GetString("TemplateDesigner.HTMLAreaCss");
            lblHTMLAreaToolbarSet.Text = ResHelper.GetString("TemplateDesigner.HTMLAreaToolbarSet");
            lblHTMLAreaStartingPath.Text = ResHelper.GetString("TemplateDesigner.HTMLAreaStartingPath");
            radDropDownListOptions.Text = ResHelper.GetString("TemplateDesigner.DropDownListOptions");
            lblRadioButtonsDirection.Text = ResHelper.GetString("TemplateDesigner.lblRadioButtonsDirection");
            lblMultipleChoiceDirection.Text = lblRadioButtonsDirection.Text;
            lblListBoxMultipleChoices.Text = ResHelper.GetString("TemplateDesigner.lblListBoxMultipleChoices");
            radDropDownListSQL.Text = ResHelper.GetString("TemplateDesigner.DropDownListSQL");
            radMultipleChoiceOptions.Text = ResHelper.GetString("TemplateDesigner.MultipleChoiceOptions");
            radMultipleChoiceSQL.Text = ResHelper.GetString("TemplateDesigner.MultipleChoiceSQL");
            radListBoxOptions.Text = ResHelper.GetString("TemplateDesigner.ListBoxOptions");
            radListBoxSQL.Text = ResHelper.GetString("TemplateDesigner.ListBoxSQL");
            chkCalendarSettingsEditTime.Text = ResHelper.GetString("TemplateDesigner.CalendarSettingsEditTime");
            chkCalendarSettingsShowNowLink.Text = ResHelper.GetString("TemplateDesigner.CalendarSettingsShowNowLink");
            lblImageHeight.Text = ResHelper.GetString("dialogs.resize.height");
            lblImageWidth.Text = ResHelper.GetString("dialogs.resize.width");
            lblImageMaxSideSize.Text = ResHelper.GetString("dialogs.resize.maxsidesize");
            datetimeDefaultValue.SupportFolder = dtPickerFolder;
            lblTimeZone.Text = ResHelper.GetString("TemplateDesigner.TimeZone");
            lblTimeZoneType.Text = ResHelper.GetString("TemplateDesigner.TimeZoneType");
            lblUploadFile.Text = ResHelper.GetString("dialogs.config.autoresize");


            // BBEditor
            lblBBEditorCols.Text = ResHelper.GetString("TemplateDesigner.BBEditorCols");
            lblBBEditorRows.Text = ResHelper.GetString("TemplateDesigner.BBEditorRows");
            lblBBEditorSize.Text = ResHelper.GetString("TemplateDesigner.BBEditorSize");
            chkBBEditorSettingsShowBold.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowBold");
            chkBBEditorSettingsShowCode.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowCode");
            chkBBEditorSettingsShowColor.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowColor");
            chkBBEditorSettingsShowItalic.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowItalic");
            chkBBEditorSettingsShowQuote.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowQuote");
            chkBBEditorSettingsShowStrike.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowStrike");
            chkBBEditorSettingsShowUnderline.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowUnderline");
            chkBBEditorSettingsUsePromptDialog.Text = ResHelper.GetString("TemplateDesigner.BBEditorUsePromptDialog");
            radBBImageNo.Text = ResHelper.GetString("general.no");
            radBBUrlNo.Text = ResHelper.GetString("general.no");
            radBBImageSimple.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowSimpleImage");
            radBBUrlSimple.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowSimpleUrl");
            radBBImageAdvanced.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowUrl");
            radBBUrlAdvanced.Text = ResHelper.GetString("TemplateDesigner.BBEditorShowImage");
        }
    }


    /// <summary>
    /// Sets Field editor content and footer css class according to selected mode.
    /// </summary>
    protected void SetCssClasses()
    {
        if (EnableSimplifiedMode)
        {
            switch (SelectedMode)
            {
                case FieldEditorSelectedModeEnum.Simplified:
                    pnlFieldEditorContentCss.CssClass = "FieldEditorContentSimplifiedMode";
                    break;


                case FieldEditorSelectedModeEnum.Advanced:
                    pnlFieldEditorContentCss.CssClass = "FieldEditorContentAdvancedMode";
                    break;

                default:
                    pnlFieldEditorContentCss.CssClass = "FieldEditorContentAdvancedMode";
                    break;
            }
        }
        else
        {
            // Default FieldEditorContent has no additonal css styles
            pnlFieldEditorContentCss.CssClass = "";
        }
    }


    /// <summary>
    /// Hides all editing panels.
    /// </summary>
    protected void HideAllPanels()
    {
        pnlCategoryEdit.Visible = false;
        pnlAdvancedAttributeEdit.Visible = false;
        pnlSimpleAttributeEdit.Visible = false;
    }


    /// <summary>
    /// Adds new form item (field or category) to the form definition.
    /// </summary>
    /// <param name="formItem">Form item to add</param>
    protected void InsertFormItem(object formItem)
    {
        // Set new item preffix
        string newItemPreffix = (formItem is FormFieldInfo) ? newFieldPreffix : newCategPreffix;

        ListItem newItem = lstAttributes.Items.FindByValue(newItemPreffix);
        int newItemIndex = lstAttributes.Items.IndexOf(newItem);

        if ((newItemIndex > 0) && (lstAttributes.Items[newItemIndex - 1] != null))
        {
            string predecessorValue = lstAttributes.Items[newItemIndex - 1].Value;
            string predecessorName = null;
            FormItemTypeEnum predecessorType = 0;

            // Set predecessor item type
            if (predecessorValue.StartsWith(categPreffix))
            {
                predecessorType = FormItemTypeEnum.Category;
            }
            else if (predecessorValue.StartsWith(fieldPreffix))
            {
                predecessorType = FormItemTypeEnum.Field;
            }

            // Set predecessor name
            predecessorName = predecessorValue.Substring(preffixLength);

            // Add new item at the specified position
            fi.AddFormItem(formItem, predecessorName, predecessorType);
        }
        else
        {
            if (formItem is FormFieldInfo)
            {
                // Add new field at the end of the collection
                fi.AddFormField((FormFieldInfo)formItem);
            }
            else if (formItem is FormCategoryInfo)
            {
                // Add new category at the end of the collection
                fi.AddFormCategory((FormCategoryInfo)formItem);
            }
        }
    }


    /// <summary>
    /// Returns default datatype of the specified user control.
    /// </summary>
    /// <param name="controlName">User control name.</param>
    protected string[] GetUserControlDefaultDataType(string controlName)
    {
        string[] defaultDataType = new string[2];

        FormUserControlInfo control = FormUserControlInfoProvider.GetFormUserControlInfo(controlName);
        if (control != null)
        {
            defaultDataType[0] = control.UserControlDefaultDataType.ToLower();
            defaultDataType[1] = Convert.ToString(control.UserControlDefaultDataTypeSize);
        }
        else
        {
            defaultDataType[0] = FormFieldDataTypeCode.TEXT;
            defaultDataType[1] = "500";
        }

        return defaultDataType;
    }


    /// <summary>
    /// Clears hashtables
    /// </summary>
    private void ClearHashtables()
    {
        // Clear the object type hashtable
        ProviderStringDictionary.ReloadDictionaries(ClassName, true);

        // Clear the classes hashtable
        ProviderStringDictionary.ReloadDictionaries("cms.class", true);

        // Clear classname strucure info
        ClassStructureInfo.Remove(ClassName);

        // Get lower case class name
        string lowerClassName = ValidationHelper.GetString(ClassName, String.Empty).ToLower();

        // Invalidate all user objects if current class in cms.user
        if (lowerClassName == UserInfo.TYPEINFO.ObjectClassName.ToLower())
        {
            UserInfo.TYPEINFO.InvalidateAllObjects();
        }

        // Invalidate all user objects if current class in cms.usersettings
        if (lowerClassName == UserSettingsInfo.TYPEINFO.ObjectClassName.ToLower())
        {
            UserInfo.TYPEINFO.InvalidateAllObjects();
        }
    }

    #region " Support for system fields "

    private void SetAttributeName(string attributeName, bool showSystemFields, bool enableSystemFields)
    {
        if (DevelopmentMode)
        {
            plcGuid.Visible = true;
        }

        if (EnableSystemFields && showSystemFields)
        {
            plcGroup.Visible = true;
            drpSystemFields.Visible = true;
            txtAttributeName.Visible = false;

            drpGroup.Enabled = enableSystemFields;
            drpSystemFields.Enabled = enableSystemFields;

            // Get system table name
            string tableName = null;
            if ((string.IsNullOrEmpty(attributeName)) || (attributeName.ToLower().StartsWith("document")))
            {
                // Fields from table CMS_Document
                tableName = "cms_document";
            }
            else
            {
                // Fields from table CMS_Node
                tableName = "cms_tree";
            }
            drpGroup.SelectedValue = tableName;

            // Select system attribute name in dropdownlist
            SetSystemAttributeName(tableName, attributeName);

            DisableFieldEditing(true);
        }
        else
        {
            // Show textbox only     
            plcGroup.Visible = false;
            drpSystemFields.Visible = false;
            txtAttributeName.Visible = true;
            txtAttributeName.Text = attributeName;
            EnableFieldEditing();
        }
    }


    private void SetSystemAttributeName(string tableName, string attributeName)
    {
        // Load system fields from specified table
        drpSystemFields.DataSource = TableManager.GetColumnInformation(tableName);
        drpSystemFields.DataBind();
        try
        {
            drpSystemFields.SelectedValue = attributeName;
        }
        catch
        {
            if (drpSystemFields.Items.Count > 0)
            {
                drpSystemFields.SelectedIndex = 0;
            }
        }
    }


    private void EnableDisableAttributeName(bool enable)
    {
        drpGroup.Enabled = enable && IsNewItemEdited;
        drpSystemFields.Enabled = enable && IsNewItemEdited;
        txtAttributeName.Enabled = enable;
    }


    protected void drpGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetSystemAttributeName(drpGroup.SelectedValue, "");
        LoadSystemField();
    }


    protected void drpSystemFields_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSystemField();
    }


    /// <summary>
    /// Loads system field either from database colum data or from field XML definition.
    /// </summary>
    private void LoadSystemField()
    {
        string tableName = drpGroup.SelectedValue;
        string columnName = drpSystemFields.SelectedValue;
        if (SelectedItemName.ToLower() != columnName.ToLower())
        {
            // Get field info from database column
            ffi = FormHelper.GetFormFieldInfo(tableName, columnName);
        }
        else
        {
            // Get field info from XML definition
            LoadFormDefinition();
            ffi = fi.GetFormField(SelectedItemName);
        }
        LoadSelectedField();
    }


    /// <summary>
    /// Returns attribute name.
    /// </summary>    
    protected string GetAttributeName()
    {
        if (drpSystemFields.Visible)
        {
            return drpSystemFields.SelectedValue;
        }
        else
        {
            return txtAttributeName.Text.Trim();
        }
    }


    /// <summary>
    /// Loads the new column type and size
    /// </summary>
    /// <param name="dataType">Data type</param>
    /// <param name="newSize">New size</param>
    /// <param name="newColumnType">New column type</param>
    /// <param name="newColumnSize">New column size</param>
    protected void LoadColumnTypeAndSize(FormFieldDataTypeEnum dataType, int newSize, ref string newColumnType, ref string newColumnSize)
    {
        switch (dataType)
        {
            case FormFieldDataTypeEnum.Integer:
                newColumnType = SqlHelperClass.DATATYPE_INTEGER;
                break;

            case FormFieldDataTypeEnum.LongInteger:
                newColumnType = SqlHelperClass.DATATYPE_LONG;
                break;

            case FormFieldDataTypeEnum.Decimal:
                newColumnType = SqlHelperClass.DATATYPE_FLOAT;
                break;

            case FormFieldDataTypeEnum.Boolean:
                newColumnType = SqlHelperClass.DATATYPE_BOOLEAN;
                break;

            case FormFieldDataTypeEnum.DateTime:
                newColumnType = SqlHelperClass.DATATYPE_DATETIME;
                break;

            case FormFieldDataTypeEnum.LongText:
                newColumnType = SqlHelperClass.DATATYPE_LONGTEXT;
                break;

            case FormFieldDataTypeEnum.File:
            case FormFieldDataTypeEnum.GUID:
                newColumnType = SqlHelperClass.DATATYPE_GUID;
                break;

            default:
                newColumnSize = Convert.ToString(newSize);
                newColumnType = SqlHelperClass.DATATYPE_TEXT + "(" + newColumnSize + ")";
                break;
        }
    }

    #endregion
}
