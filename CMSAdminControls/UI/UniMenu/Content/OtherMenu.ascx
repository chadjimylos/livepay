﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OtherMenu.ascx.cs" Inherits="CMSAdminControls_UI_UniMenu_Content_OtherMenu" %>
<%@ Register Src="~/CMSAdminControls/UI/UniMenu/UniMenuButtons.ascx" TagName="UniMenuButtons"
    TagPrefix="cms" %>
<cms:UniMenuButtons ID="buttons" runat="server" EnableViewState="false" AllowSelection="false" />
