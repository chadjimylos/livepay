using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.EcommerceProvider;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_StoreSettings_StoreSettings_CheckoutProcess : CMSEcommerceConfigurationPage
{
    private SiteInfo currentSite = null;

    protected void Page_Load(object sender, EventArgs e)
    {        
        // Get current site information
        currentSite = CMSContext.CurrentSite;
        if (currentSite != null)
        {
            ucCheckoutProcess.OnCheckoutProcessDefinitionUpdate += new OnCheckoutProcessDefinitionUpdateEventHandler(ucCheckoutProcess_OnCheckoutProcessDefinitionUpdate);                    

            // Load data
            if (!RequestHelper.IsPostBack())
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(currentSite.SiteName);
                if (si != null)
                {
                    ucCheckoutProcess.Value = si.SiteCheckoutProcess;
                    ucCheckoutProcess.EnableDefaultCheckoutProcessTypes = true;
                }
            }
        }
    }


    void ucCheckoutProcess_OnCheckoutProcessDefinitionUpdate(string action)
    {
        // Check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            CMSPage.RedirectToCMSDeskAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }


        //if (!ucCheckoutProcess.IsValid())
        //{
        //    // Show error message
        //    lblError.Visible = true;
        //    lblError.Text = ucCheckoutProcess.ValidationError;
        //}

        switch (action.ToLower())
        {
            case "update":
            case "delete":
            case "moveup":
            case "movedown":
                if (currentSite != null)
                {
                    // Update checkout process xml definition in database
                    SiteInfo si = SiteInfoProvider.GetSiteInfo(currentSite.SiteID);
                    if (si != null)
                    {
                        si.SiteCheckoutProcess = ucCheckoutProcess.Value.ToString();
                        SiteInfoProvider.SetSiteInfo(si);
                        if (action.ToLower() == "update")
                        {
                            ucCheckoutProcess.Information = ResHelper.GetString("General.ChangesSaved");
                        }
                    }
                }
                break;

            case "defaultprocess":
                if (currentSite != null)
                {
                    // Set default checkout process
                    SiteInfo si = SiteInfoProvider.GetSiteInfo(currentSite.SiteID);
                    if (si != null)
                    {
                        ucCheckoutProcess.Value = ShoppingCart.DEFAULT_CHECKOUT_PROCESS;
                        ucCheckoutProcess.ReloadData();
                        si.SiteCheckoutProcess = ucCheckoutProcess.Value.ToString();
                        SiteInfoProvider.SetSiteInfo(si);
                        ucCheckoutProcess.Information = ResHelper.GetString("General.ChangesSaved");
                    }                    
                }                
                break;
        }
    }
}
