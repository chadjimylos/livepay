using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Newsletter;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.FileManager;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Templates_NewsletterTemplate_Edit : CMSNewsletterPage
{
    protected int templateid = 0;
    private string mCMSDeskPath = null;
    protected int siteId = 0;
    EmailTemplate emailTemplateObj = null;
    protected string mSave = null;


    /// <summary>
    /// Gets the CMS Desk virtual path
    /// </summary>
    private string CmsDeskPath
    {
        get
        {
            if (mCMSDeskPath == null)
            {
                // Get CMSDesk path from settings
                mCMSDeskPath = SettingsKeyProvider.GetStringValue("CMSDeskVirtualPath");
                if (string.IsNullOrEmpty(mCMSDeskPath))
                {
                    mCMSDeskPath = "~/cmsdesk";
                }
                if (mCMSDeskPath.EndsWith("/"))
                {
                    mCMSDeskPath.TrimEnd('/');
                }
            }

            return this.ResolveUrl(mCMSDeskPath);
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get site ID from query string
        siteId = QueryHelper.GetInteger("siteid", 0);

        rfvTemplateDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvTemplateName.ErrorMessage = ResHelper.GetString("NewsletterTemplate_Edit.ErrorEmptyName");

        // Control initializations
        string setResourceStringsScript = "";
        setResourceStringsScript += "var emptyNameMsg = '" + ResHelper.GetString("NewsletterTemplate_Edit.EmptyNameMsg") + "'; \n";
        setResourceStringsScript += "var emptyWidthMsg = '" + ResHelper.GetString("NewsletterTemplate_Edit.EmptyWidthMsg") + "'; \n";
        setResourceStringsScript += "var emptyHeightMsg = '" + ResHelper.GetString("NewsletterTemplate_Edit.EmptyHeightMsg") + "'; \n";
        ltlScript.Text = ScriptHelper.GetScript(setResourceStringsScript);

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>" +
            ScriptHelper.GetScript("function SaveDocument() { " + this.ClientScript.GetPostBackEventReference(this.lnkSave, null) + " } \n"
        ));

        this.imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        mSave = ResHelper.GetString("general.save");

        AttachmentTitle.TitleText = ResHelper.GetString("general.attachments");

        string currentEmailTemplate = ResHelper.GetString("NewsletterTemplate_Edit.NewItemCaption");

        // Get emailTemplate ID from querystring
        templateid = QueryHelper.GetInteger("templateid", 0);
        if (templateid > 0)
        {
            emailTemplateObj = EmailTemplateProvider.GetEmailTemplate(templateid);
            currentEmailTemplate = emailTemplateObj.TemplateDisplayName;

            macroSelectorElm.Resolver = MacroSelector.NewsletterResolver;
            macroSelectorElm.JavaScripFunction = "InsertHTML";

            AttachmentList.ObjectID = emailTemplateObj.TemplateID;
            AttachmentList.SiteID = siteId;
            AttachmentList.ObjectType = NewsletterObjectType.NEWSLETTERTEMPLATE;
            AttachmentList.Category = MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE;
            AttachmentList.AllowPasteAttachments = true;
            AttachmentList.AllowEdit = CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "managetemplates");

            // Display editable region section only for e-mail templates of type "Issue template"
            if (emailTemplateObj.TemplateType == "I")
            {
                pnlEditableRegion.Visible = true;
                btnInsertEditableRegion.Visible = true;
                lblInsertEditableRegion.Visible = true;
            }
            else
            {
                plcSubject.Visible = true;
            }

            // Fill editing form
            if (!RequestHelper.IsPostBack())
            {
                LoadData(emailTemplateObj);

                // Show that the emailTemplate was created or updated successfully
                if (Request.QueryString["saved"] == "1")
                {
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
            }
        }

        // Initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("NewsletterTemplate_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Newsletters/Tools/Templates/NewsletterTemplate_List.aspx?siteid=" + siteId.ToString();
        pageTitleTabs[0, 2] = "newslettersContent";
        pageTitleTabs[1, 0] = currentEmailTemplate;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        PageTitle.Breadcrumbs = pageTitleTabs;

        if (!RequestHelper.IsPostBack())
        {
            FillFieldsList();

            // Initialize HTML editor
            InitHTMLEditor();
        }

        // Init CSS styles every time during page load
        if (emailTemplateObj != null)
        {
            htmlTemplateBody.EditorAreaCSS = EmailTemplateProvider.GetStylesheetUrl(emailTemplateObj.TemplateName) + "&timestamp=" + DateTime.Now.Millisecond;
        }
    }


    /// <summary>
    /// Load data of editing emailTemplate.
    /// </summary>
    /// <param name="emailTemplateObj">EmailTemplate object.</param>
    protected void LoadData(EmailTemplate emailTemplateObj)
    {
        htmlTemplateBody.ResolvedValue = emailTemplateObj.TemplateBody;
        txtTemplateName.Text = emailTemplateObj.TemplateName;
        txtTemplateHeader.Value = emailTemplateObj.TemplateHeader;
        txtTemplateFooter.Value = emailTemplateObj.TemplateFooter;
        txtTemplateDisplayName.Text = emailTemplateObj.TemplateDisplayName;
        txtTemplateStyleSheetText.Text = emailTemplateObj.TemplateStylesheetText;

        // Display temaplate subject only for 'subscription' and 'unsubscription' template types
        if (emailTemplateObj.TemplateType != "I")
        {
            txtTemplateSubject.Text = emailTemplateObj.TemplateSubject;
        }
    }


    /// <summary>
    /// Fills list of available fields
    /// </summary>
    protected void FillFieldsList()
    {
        ListItem newItem = new ListItem(ResHelper.GetString("general.email"), IssueHelper.MacroEmail);
        lstInsertField.Items.Add(newItem);
        newItem = new ListItem(ResHelper.GetString("NewsletterTemplate.MacroFirstName"), IssueHelper.MacroFirstName);
        lstInsertField.Items.Add(newItem);
        newItem = new ListItem(ResHelper.GetString("NewsletterTemplate.MacroLastName"), IssueHelper.MacroLastName);
        lstInsertField.Items.Add(newItem);
        newItem = new ListItem(ResHelper.GetString("NewsletterTemplate.MacroUnsubscribeLink"), IssueHelper.MacroUnsubscribeLink);
        lstInsertField.Items.Add(newItem);
    }


    /// <summary>
    /// Initializes HTML editor's settings
    /// </summary>
    protected void InitHTMLEditor()
    {
        htmlTemplateBody.AutoDetectLanguage = false;
        htmlTemplateBody.DefaultLanguage = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        htmlTemplateBody.ToolbarSet = "Newsletter"; 
        htmlTemplateBody.MediaDialogConfig.UseFullURL = true;
        htmlTemplateBody.LinkDialogConfig.UseFullURL = true;
        htmlTemplateBody.QuickInsertConfig.UseFullURL = true;
    }


    /// <summary>
    /// Saves data to database.
    /// </summary>
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        // Check 'Manage templates' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "managetemplates"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "managetemplates");
        }

        string errorMessage = null;
        // Check template code name
        if (!ValidationHelper.IsCodeName(txtTemplateName.Text))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }
        else
        {
            // Check code and display name for emptiness
            errorMessage = new Validator().NotEmpty(txtTemplateDisplayName.Text, ResHelper.GetString("general.requiresdisplayname")).NotEmpty(txtTemplateName.Text, ResHelper.GetString("NewsletterTemplate_Edit.ErrorEmptyName")).Result;
        }

        if (string.IsNullOrEmpty(errorMessage))
        {
            // TemplateName must to be unique
            EmailTemplate emailTemplateObj = EmailTemplateProvider.GetEmailTemplate(txtTemplateName.Text.Trim(), CMSContext.CurrentSite.SiteID);

            // If templateName value is unique														
            if ((emailTemplateObj == null) || (emailTemplateObj.TemplateID == templateid))
            {
                // If templateName value is unique -> determine whether it is update or insert 
                if ((emailTemplateObj == null))
                {
                    // Get EmailTemplate object by primary key
                    emailTemplateObj = EmailTemplateProvider.GetEmailTemplate(templateid);
                    if (emailTemplateObj == null)
                    {
                        // Create new item -> insert
                        emailTemplateObj = new EmailTemplate();
                    }
                }

                // Check if template doesn't contains more editable regions with same name
                Hashtable eRegions = new Hashtable();
                bool isValid = true;
                bool isValidRegionName = true;

                int textStart = 0;
                int editRegStart = htmlTemplateBody.ResolvedValue.Trim().IndexOf("$$", textStart);
                int editRegEnd = 0;
                string region = null;
                string[] parts = null;
                string name = null;

                while (editRegStart >= 0)
                {
                    // End of region
                    editRegStart += 2;
                    textStart = editRegStart;
                    if (editRegStart < htmlTemplateBody.ResolvedValue.Trim().Length - 1)
                    {
                        editRegEnd = htmlTemplateBody.ResolvedValue.Trim().IndexOf("$$", editRegStart);
                        if (editRegEnd >= 0)
                        {
                            region = htmlTemplateBody.ResolvedValue.Trim().Substring(editRegStart, editRegEnd - editRegStart);
                            parts = (region + ":" + ":").Split(':');

                            textStart = editRegEnd + 2;
                            try
                            {
                                name = parts[0];
                                if ((!string.IsNullOrEmpty(name)) && (name.Trim() != ""))
                                {
                                    if (eRegions[name.ToLower()] != null)
                                    {
                                        isValid = false;
                                        break;
                                    }

                                    if (!ValidationHelper.IsCodeName(name))
                                    {
                                        isValidRegionName = false;
                                        break;
                                    }
                                    eRegions[name.ToLower()] = 1;
                                }
                            }
                            catch
                            {
                            }
                        }
                    }

                    editRegStart = htmlTemplateBody.ResolvedValue.Trim().IndexOf("$$", textStart);
                }

                if (isValid)
                {
                    if (isValidRegionName)
                    {
                        // Set template object
                        emailTemplateObj.TemplateBody = htmlTemplateBody.ResolvedValue.Trim();
                        emailTemplateObj.TemplateName = txtTemplateName.Text.Trim();
                        emailTemplateObj.TemplateHeader = ValidationHelper.GetString(txtTemplateHeader.Value, "").Trim();
                        emailTemplateObj.TemplateFooter = ValidationHelper.GetString(txtTemplateFooter.Value, "").Trim();
                        emailTemplateObj.TemplateDisplayName = txtTemplateDisplayName.Text.Trim();
                        emailTemplateObj.TemplateStylesheetText = txtTemplateStyleSheetText.Text.Trim();

                        // Set temaplte subject only for 'subscription' and 'unsubscription' template types
                        if (plcSubject.Visible)
                        {
                            emailTemplateObj.TemplateSubject = txtTemplateSubject.Text.Trim();
                        }

                        EmailTemplateProvider.SetEmailTemplate(emailTemplateObj);

                        UrlHelper.Redirect("NewsletterTemplate_Edit.aspx?templateid=" + Convert.ToString(emailTemplateObj.TemplateID) + "&saved=1");
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("NewsletterTemplate_Edit.EditableRegionNameError");
                    }
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("NewsletterTemplate_Edit.EditableRegionError");
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("NewsletterTemplate_Edit.TemplateNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}