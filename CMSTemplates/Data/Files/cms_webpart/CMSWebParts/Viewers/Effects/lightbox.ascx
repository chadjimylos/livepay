<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Viewers/Effects/lightbox.ascx.cs"
    Inherits="CMSWebParts_Viewers_Effects_lightbox" %>
<%@ Register TagPrefix="cms" Namespace="CMS.Controls" Assembly="CMS.Controls" %>
<asp:Literal ID="ltlScript" runat="server" />
<cms:LightboxExtender ID="extLightbox" runat="server" />
<cms:CMSRepeater ID="repItems" runat="server" EnableViewState="true" />
<asp:Literal ID="ltlInitResponse" runat="server" />
