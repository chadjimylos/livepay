using System;

using CMS.GlobalHelper;
using CMS.FormEngine;
using CMS.UIControls;

public partial class CMSModules_CustomTables_AlternativeForms_AlternativeForms_Header : CMSCustomTablesPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int classId = QueryHelper.GetInteger("classid", 0);
        int altFormId = QueryHelper.GetInteger("altformid", 0);
        int saved = QueryHelper.GetInteger("saved", 0);

        AlternativeFormInfo afi = AlternativeFormInfoProvider.GetAlternativeFormInfo(altFormId);

        // Init breadcrumbs
        string[,] breadcrumbs = new string[2, 3];

        // Edit item breadcrumbs
        breadcrumbs[0, 0] = ResHelper.GetString("altforms.listlink");
        breadcrumbs[0, 1] = "~/CMSModules/Customtables/AlternativeForms/AlternativeForms_List.aspx?classid=" + classId;
        breadcrumbs[0, 2] = "content";
        breadcrumbs[1, 0] = afi != null ? afi.FormDisplayName : String.Empty;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        CurrentMaster.Title.Breadcrumbs = breadcrumbs;

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu(classId, altFormId, saved);
        }
    }


    /// <summary>
    /// Initializes edit menu
    /// </summary>
    protected void InitalizeMenu(int classId, int altFormId, int saved)
    {
        string urlParams = "?altformid=" + altFormId;

        string[,] tabs = new string[3, 4];
        tabs[0, 0] = ResHelper.GetString("general.general");
        tabs[0, 1] = "";
        tabs[0, 2] = "AlternativeForms_Edit_General.aspx" + urlParams + "&saved=" + saved;
        tabs[1, 0] = ResHelper.GetString("general.fields");
        tabs[1, 1] = "";
        tabs[1, 2] = "AlternativeForms_Fields.aspx" + urlParams;
        tabs[2, 0] = ResHelper.GetString("general.layout");
        tabs[2, 1] = "";
        tabs[2, 2] = "AlternativeForms_Layout.aspx" + urlParams;

        CurrentMaster.Tabs.Tabs = tabs;
        CurrentMaster.Tabs.UrlTarget = "altFormsContent";
    }
}
