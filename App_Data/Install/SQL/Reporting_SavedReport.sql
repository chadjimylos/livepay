CREATE TABLE [Reporting_SavedReport] (
		[SavedReportID]                  int NOT NULL IDENTITY(1, 1),
		[SavedReportReportID]            int NOT NULL,
		[SavedReportGUID]                uniqueidentifier NOT NULL,
		[SavedReportTitle]               nvarchar(200) NULL,
		[SavedReportDate]                datetime NOT NULL,
		[SavedReportHTML]                nvarchar(max) NOT NULL,
		[SavedReportParameters]          nvarchar(max) NOT NULL,
		[SavedReportCreatedByUserID]     int NULL,
		[SavedReportLastModified]        datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Reporting_SavedReport]
	ADD
	CONSTRAINT [PK_Reporting_SavedReport]
	PRIMARY KEY
	NONCLUSTERED
	([SavedReportID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Reporting_SavedReport]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Reporting_SavedReport_SavedReportCreatedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Reporting_SavedReport]
			ADD CONSTRAINT [FK_Reporting_SavedReport_SavedReportCreatedByUserID_CMS_User]
			FOREIGN KEY ([SavedReportCreatedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Reporting_SavedReport]') IS NOT NULL
	AND OBJECT_ID(N'[Reporting_Report]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Reporting_SavedReport_SavedReportReportID_Reporting_Report]') IS NULL
BEGIN
		ALTER TABLE [Reporting_SavedReport]
			ADD CONSTRAINT [FK_Reporting_SavedReport_SavedReportReportID_Reporting_Report]
			FOREIGN KEY ([SavedReportReportID]) REFERENCES [Reporting_Report] ([ReportID])
END
CREATE INDEX [IX_Reporting_SavedReport_SavedReportCreatedByUserID]
	ON [Reporting_SavedReport] ([SavedReportCreatedByUserID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_Reporting_SavedReport_SavedReportReportID_SavedReportDate]
	ON [Reporting_SavedReport] ([SavedReportReportID], [SavedReportDate] DESC)
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
