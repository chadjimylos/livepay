﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.FileManager;

public partial class CMSModules_MediaLibrary_Controls_MediaLibrary_FolderActions_SelectFolderFooter : CMSUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.Visible = false;
        }
        else
        {
            // Setup controls
            SetupControls();
        }
    }


    #region "Private methods"

    /// <summary>
    /// Initializes all the nested controls
    /// </summary>
    private void SetupControls()
    {
        ScriptHelper.RegisterJQuery(Page);

        // Register for events
        this.btnInsert.Click += new EventHandler(btnInsert_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);

        switch (QueryHelper.GetString("action", "").ToLower().Trim())
        {
            case "copy":
                this.btnInsert.ResourceString = "general.copy";
                break;

            case "move":
                this.btnInsert.ResourceString = "general.move";
                break;

            default:
                this.btnInsert.ResourceString = "general.select";
                break;
        }
    }

    #endregion


    #region "Event handlers"

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.ltlScript.Text = ScriptHelper.GetScript("window.parent.close();");
    }


    protected void btnInsert_Click(object sender, EventArgs e)
    {
        this.ltlScript.Text = ScriptHelper.GetScript("if ((window.parent.frames['selectFolderContent'])&&(window.parent.frames['selectFolderContent'].RaiseAction)) {window.parent.frames['selectFolderContent'].RaiseAction();}");
    }

    #endregion
}
