using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.FormControls;
using CMS.PortalEngine;
using CMS.DataEngine;
using CMS.TreeEngine;

using TreeNode = CMS.TreeEngine.TreeNode;
using CMS.SiteProvider;

public partial class CMSModules_Content_FormControls_Tags_TagSelector : FormEngineUserControl
{
    #region "Variables"

    private bool mEnabled = true;

    #endregion

    #region "Properties"

    /// <summary>
    /// Enable/disable control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return this.mEnabled;
        }
        set
        {
            this.mEnabled = value;
            btnSelect.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return TagHelper.GetTagsForSave(this.txtTags.Text.Trim());
        }
        set
        {
            this.txtTags.Text = ValidationHelper.GetString(value, "");
        }
    }


    /// <summary>
    /// Tag Group ID.
    /// </summary>
    public int GroupId
    {
        get
        {
            int mGroupId = ValidationHelper.GetInteger(this.GetValue("GroupID"), 0);
            if ((mGroupId == 0) && (this.Form != null))
            {
                string path = "";

                // When inserting new document
                if (this.Form.ParentObject != null)
                {
                    // Get path and groupID of the parent node
                    path = ((TreeNode)this.Form.ParentObject).NodeAliasPath;
                    mGroupId = ((TreeNode)this.Form.ParentObject).DocumentTagGroupID;
                    // If nothing found try get inherited value
                    if (mGroupId == 0)
                    {
                        mGroupId = ValidationHelper.GetInteger(((TreeNode)this.Form.ParentObject).GetInheritedValue("DocumentTagGroupID", true), 0);
                    }
                }
                // When editing existing document
                else if (this.Form.EditedObject != null)
                {
                    // Get path and groupID of the parent node
                    path = ((TreeNode)this.Form.EditedObject).NodeAliasPath;
                    mGroupId = ((TreeNode)this.Form.EditedObject).DocumentTagGroupID;
                    // If nothing found try get inherited value
                    if (mGroupId == 0)
                    {
                        mGroupId = ValidationHelper.GetInteger(((TreeNode)this.Form.EditedObject).GetInheritedValue("DocumentTagGroupID", true), 0);
                    }
                }
            }

            return mGroupId;
        }
        set
        {
            this.SetValue("GroupID", value);
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Ensure Script Manager is the first control on the page
        ScriptManager sMgr = ScriptManager.GetCurrent(Page);
        if (sMgr != null)
        {
            sMgr.Services.Add(new ServiceReference("~/CMSModules/Content/FormControls/Tags/TagSelectorService.asmx"));
        }

        // Register the dialog script
        ScriptHelper.RegisterDialogScript(this.Page);

        // Register tag script 
        ScriptHelper.RegisterStartupScript(this, typeof(string), "tagScript", ScriptHelper.GetScript(tagScript()));
        
        // Create script for valid inserting into textbox
        this.ltlScript.Text = ScriptHelper.GetScript("function itemSelected(source, eventArgs) {\n" +
        "    var txtBox = $get('" + this.txtTags.ClientID + "');\n" +
        "    txtBox.value = eventArgs.get_text().replace(/\'\"/,'\"').replace(/\"\'/,'\"');\n" +
        "}\n");

        this.btnSelect.Text = ResHelper.GetString("general.select");
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        // Enable/Disable control
        this.txtTags.Enabled = this.Enabled;
        this.btnSelect.Enabled = this.Enabled;
        if (this.Enabled)
        {
            this.autoComplete.ContextKey = GroupId.ToString();
            this.btnSelect.OnClientClick = "tagSelect('" + this.txtTags.ClientID + "','" + GroupId.ToString() + "'); return false;";
        }
    }


    /// <summary>
    /// Returns tag JS script.
    /// </summary>
    private string tagScript()
    {
        string baseUrl = "~/CMSFormControls/Selectors/TagSelector.aspx";
        if (this.IsLiveSite)
        {
            baseUrl = "~/CMSFormControls/LiveSelectors/TagSelector.aspx";
        }

        StringBuilder builder = new StringBuilder();
        // Build script with modal dialog opener and set textbox functions
        builder.Append("function tagSelect(id,group){\n");
        builder.Append("    var textbox = document.getElementById(id);\n");
        builder.Append("    if (textbox != null){\n");
        builder.Append("        var tags = encodeURIComponent(textbox.value.replace(/\\|/,\"-\").replace(/%/,\"-\")).replace(/&/,\"%26\");\n");
        builder.Append("        modalDialog('" + ResolveUrl(baseUrl) + "?textbox='+ id +'&group='+ group +'&tags=' + tags, 'Tag Selector', 570, 670);\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("function setTagsToTextBox(textBoxId,tagString){\n");
        builder.Append("    if (textBoxId != '') {\n");
        builder.Append("        var textbox = document.getElementById(textBoxId);\n");
        builder.Append("        if (textbox != null){\n");
        builder.Append("            textbox.value = decodeURI(tagString);\n");
        builder.Append("        }\n");
        builder.Append("    }\n");
        builder.Append("}\n");

        return builder.ToString();
    }
}
