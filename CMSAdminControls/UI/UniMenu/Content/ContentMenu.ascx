<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContentMenu.ascx.cs" Inherits="CMSAdminControls_UI_UniMenu_Content_ContentMenu" %>
<%@ Register Src="~/CMSAdminControls/UI/UniMenu/UniMenuButtons.ascx" TagName="UniMenuButtons"
    TagPrefix="cms" %>
<cms:UniMenuButtons ID="buttonsBig" runat="server" EnableViewState="false" />
<asp:Panel ID="pnlSmallButtons" runat="server" EnableViewState="false" CssClass="ActionButtons">
    <cms:UniMenuButtons ID="buttonsSmall" runat="server" EnableViewState="false" MaximumItems="2" />
</asp:Panel>
