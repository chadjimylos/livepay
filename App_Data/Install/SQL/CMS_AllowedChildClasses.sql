CREATE TABLE [CMS_AllowedChildClasses] (
		[ParentClassID]     int NOT NULL,
		[ChildClassID]      int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_AllowedChildClasses]
	ADD
	CONSTRAINT [PK_CMS_AllowedChildClasses]
	PRIMARY KEY
	([ParentClassID], [ChildClassID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_AllowedChildClasses]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_AllowedChildClasses_ChildClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_AllowedChildClasses]
			ADD CONSTRAINT [FK_CMS_AllowedChildClasses_ChildClassID_CMS_Class]
			FOREIGN KEY ([ChildClassID]) REFERENCES [CMS_Class] ([ClassID])
END
IF OBJECT_ID(N'[CMS_AllowedChildClasses]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_AllowedChildClasses_ParentClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_AllowedChildClasses]
			ADD CONSTRAINT [FK_CMS_AllowedChildClasses_ParentClassID_CMS_Class]
			FOREIGN KEY ([ParentClassID]) REFERENCES [CMS_Class] ([ClassID])
END
