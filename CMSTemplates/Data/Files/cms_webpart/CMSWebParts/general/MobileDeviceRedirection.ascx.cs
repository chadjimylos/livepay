﻿using System.Web;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.PortalEngine;

public partial class CMSWebParts_General_MobileDeviceRedirection : CMSAbstractWebPart
{
    #region Webpart properties

    /// <summary>
    /// URL to which should be mobile device redirected
    /// </summary>
    public string RedirectionURL
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("RedirectionURL"), "");
        }
        set
        {
            this.SetValue("RedirectionURL", value);
        }
    }

    #endregion


    #region Webpart methods

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (!this.StopProcessing)
        {
            // Check if visitor use mobile device
            if (BrowserHelper.IsMobileDevice())
            {
                // Trim blank characters
                string rawURL = this.RedirectionURL.Trim();

                // Check if some address is specified
                if ((rawURL.Length > 0) &&
                    (CMSContext.ViewMode == ViewModeEnum.LiveSite))
                {
                    string newURL = UrlHelper.ResolveUrl(rawURL);

                    // If current URL is same as set, no redirection is done
                    if ((UrlHelper.CurrentURL != newURL) &&
                        (UrlHelper.GetAbsoluteUrl(UrlHelper.CurrentURL) != newURL))
                    {
                        UrlHelper.ResponseRedirect(newURL);
                    }
                }
            }
        }
    }

    #endregion
}
