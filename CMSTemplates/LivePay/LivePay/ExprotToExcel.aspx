﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExprotToExcel.aspx.vb" Inherits="CMSTemplates_LivePay_ExprotToExcel" %>
<link type="text/css" rel="stylesheet" href="/CMSPages/GetCSS.aspx?stylesheetname=LivePay" /> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divRep1" runat="server" visible="false" >
        <asp:GridView  ID="GridViewPayments" runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Names="arial" HeaderStyle-Font-Size="14px" HeaderStyle-Font-Bold="true" RowStyle-Font-Names="arial" RowStyle-Font-Size="12px" >
            <Columns>
                <asp:BoundField DataField="transactiondDte"  />
                <asp:BoundField DataField="merchantId" />
                 <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"   ItemStyle-CssClass="MerchSrcItemSimple" >
                    <ItemTemplate>
                        <asp:Label ID="lblCardNumber" runat="server" Text=' <%#Eval("CardNumber") %> ' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple" ItemStyle-HorizontalAlign="Right"  ItemStyle-CssClass="MerchSrcItemSimple" >
                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("transactioAamount") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="transactionStatus" />
            </Columns>
        </asp:GridView>
    </div>

    <div id="divRep2" runat="server" visible="false" >
        <asp:GridView  ID="GridViewMerchantsPayments" runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Names="arial" HeaderStyle-Font-Size="14px" HeaderStyle-Font-Bold="true" RowStyle-Font-Names="arial" RowStyle-Font-Size="12px" >
            <Columns>
                <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderLeft MerchSrcHeader" ItemStyle-CssClass="MerchSrcItemLeft"  >
                    <ItemTemplate>
                        <asp:label ID="lblBoxNo" runat="server" Text='<%#Eval("BatchID") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:label ID="lblCloseDtm" runat="server" Text='<%#Eval("BatchDate").toString().split(" ")(0) & "<br />" & Eval("BatchDate").toString().split(" ")(1) %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:label ID="lblBoxStatus" runat="server" Text='<%#IIF(Eval("BatchClosed")=true,"Κλειστό","Ανοιχτό") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  >
                    <ItemTemplate>
                        <asp:label ID="lblTransCode" runat="server" Text='<%#Eval("TransactionID") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple "  ItemStyle-CssClass="MerchSrcItemSimple " >
                    <ItemTemplate>
                        <asp:label ID="lblTransDtm" runat="server" Text='<%#Eval("transactiondDte").toString().split(" ")(0) & "<br />" & Eval("transactiondDte").toString().split(" ")(1) %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple W60"  ItemStyle-CssClass="MerchSrcItemSimple W60" >
                    <ItemTemplate>
                        <asp:label ID="lblCustName" runat="server" Text='<%#Eval("CustomerName") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple" ItemStyle-HorizontalAlign="Right"  ItemStyle-CssClass="MerchSrcItemSimple" >
                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("transactioAamount") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:label ID="lblTransType" runat="server" Text='<%#Eval("transactionType") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblTransStatus" runat="server" Text='<%#GetTransactionStatusDescr(Eval("TransactionStatus")) %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
