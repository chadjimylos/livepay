using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_TrialVersion :  CMSDeskPage
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.RequireSite = false;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["appexpires"] != null)
        {
            // Application expires
            int appExpiration = QueryHelper.GetInteger("appexpires", 0);
            if (appExpiration <= 0)
            {
                ltlText.Text = ResHelper.GetString("Trial.AppExpired");
            }
            else
            {
                ltlText.Text = string.Format(ResHelper.GetString("Trial.AppExpiresIn"), appExpiration);
            }
        }
        else
        {
            // Trial version expiration date
            int expiration = QueryHelper.GetInteger("expirationdate", 0);
            if (expiration <= 0)
            {
                ltlText.Text = ResHelper.GetString("Trial.Expired");
            }
            else
            {
                ltlText.Text = string.Format(ResHelper.GetString("Trial.ExpiresIn"), expiration);
            }
        }
    }
}
