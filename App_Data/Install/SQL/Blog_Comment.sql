CREATE TABLE [Blog_Comment] (
		[CommentID]                   int NOT NULL IDENTITY(1, 1),
		[CommentUserName]             nvarchar(200) NOT NULL,
		[CommentUserID]               int NULL,
		[CommentUrl]                  nvarchar(450) NULL,
		[CommentText]                 nvarchar(max) NOT NULL,
		[CommentApprovedByUserID]     int NULL,
		[CommentPostDocumentID]       int NOT NULL,
		[CommentDate]                 datetime NOT NULL,
		[CommentIsSpam]               bit NULL,
		[CommentApproved]             bit NULL,
		[CommentIsTrackBack]          bit NULL,
		[CommentEmail]                nvarchar(250) NULL
)
ON [PRIMARY]
ALTER TABLE [Blog_Comment]
	ADD
	CONSTRAINT [PK_Blog_Comment]
	PRIMARY KEY
	NONCLUSTERED
	([CommentID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Blog_Comment]
	ADD
	CONSTRAINT [DEFAULT_Blog_Comment_CommentApproved]
	DEFAULT ((0)) FOR [CommentApproved]
ALTER TABLE [Blog_Comment]
	ADD
	CONSTRAINT [DEFAULT_Blog_Comment_CommentIsSpam]
	DEFAULT ((0)) FOR [CommentIsSpam]
ALTER TABLE [Blog_Comment]
	ADD
	CONSTRAINT [DEFAULT_Blog_Comment_CommentIsTrackBack]
	DEFAULT ((0)) FOR [CommentIsTrackBack]
IF OBJECT_ID(N'[Blog_Comment]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Blog_Comment_CommentApprovedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Blog_Comment]
			ADD CONSTRAINT [FK_Blog_Comment_CommentApprovedByUserID_CMS_User]
			FOREIGN KEY ([CommentApprovedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Blog_Comment]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Document]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Blog_Comment_CommentPostDocumentID_CMS_Document]') IS NULL
BEGIN
		ALTER TABLE [Blog_Comment]
			ADD CONSTRAINT [FK_Blog_Comment_CommentPostDocumentID_CMS_Document]
			FOREIGN KEY ([CommentPostDocumentID]) REFERENCES [CMS_Document] ([DocumentID])
END
IF OBJECT_ID(N'[Blog_Comment]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Blog_Comment_CommentUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Blog_Comment]
			ADD CONSTRAINT [FK_Blog_Comment_CommentUserID_CMS_User]
			FOREIGN KEY ([CommentUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_Blog_Comment_CommentApprovedByUserID]
	ON [Blog_Comment] ([CommentApprovedByUserID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_Blog_Comment_CommentDate]
	ON [Blog_Comment] ([CommentDate] DESC)
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Blog_Comment_CommentIsSpam_CommentIsApproved_CommentIsTrackback]
	ON [Blog_Comment] ([CommentIsSpam], [CommentApproved], [CommentIsTrackBack])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Blog_Comment_CommentPostDocumentID]
	ON [Blog_Comment] ([CommentPostDocumentID])
	ON [PRIMARY]
CREATE INDEX [IX_Blog_Comment_CommentUserID]
	ON [Blog_Comment] ([CommentUserID])
	ON [PRIMARY]
