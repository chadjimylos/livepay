<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="CMSAdminControls_TabsFrameset_default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title></title>
</head>
<frameset border="0" cols="6, *">
    <frame name="TabPageBorder" src="border.aspx" frameborder="0" scrolling="no" noresize="noresize" />
    <frame name="TabPageContent" src="<%=contentUrl %>" scrolling="no" frameborder="0" />
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
</frameset>
</html>
