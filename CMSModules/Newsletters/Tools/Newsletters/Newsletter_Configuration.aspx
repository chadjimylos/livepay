<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletter_Configuration.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_Configuration"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Tools - Newsletter configuration" %>

<%@ Register Src="~/CMSModules/Newsletters/FormControls/NewsletterTemplateSelector.ascx"
    TagName="NewsletterTemplateSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/Selectors/ScheduleInterval.ascx" TagName="ScheduleInterval"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewsletterDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterDisplayName" runat="server" CssClass="TextBoxField"
                    MaxLength="250" />
                <asp:RequiredFieldValidator ID="rfvNewsletterDisplayName" runat="server" ControlToValidate="txtNewsletterDisplayName"
                    Display="dynamic" EnableViewState="false">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewsletterName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterName" runat="server" CssClass="TextBoxField" MaxLength="250" />
                <asp:RequiredFieldValidator ID="rfvNewsletterName" runat="server" ControlToValidate="txtNewsletterName"
                    Display="dynamic" EnableViewState="false">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewsletterSenderName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterSenderName" runat="server" CssClass="TextBoxField"
                    MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvNewsletterSenderName" runat="server" ErrorMessage=""
                    ControlToValidate="txtNewsletterSenderName" EnableViewState="false">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewsletterSenderEmail" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterSenderEmail" runat="server" CssClass="TextBoxField"
                    MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvNewsletterSenderEmail" runat="server" ErrorMessage=""
                    ControlToValidate="txtNewsletterSenderEmail" EnableViewState="false">
                </asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewsletterBaseUrl" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterBaseUrl" runat="server" CssClass="TextBoxField" MaxLength="500" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewsletterUnsubscribeUrl" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNewsletterUnsubscribeUrl" runat="server" CssClass="TextBoxField"
                    MaxLength="1000" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblSubscriptionTemplate" EnableViewState="false" />
            </td>
            <td>
                <cms:NewsletterTemplateSelector ID="subscriptionTemplate" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblUnsubscriptionTemplate" EnableViewState="false" />
            </td>
            <td>
                <cms:NewsletterTemplateSelector ID="unsubscriptionTemplate" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblUseEmailQueue" runat="server" ResourceString="newsletter.useemailqueue"
                    DisplayColon="true" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox ID="chkUseEmailQueue" runat="server" />
            </td>
        </tr>
        <tr style="height: 20px;">
        </tr>
        <tr>
            <td colspan="2" style="font-weight: bold">
                <asp:Label ID="lblTemplateBased" runat="server" EnableViewState="false" />
                <asp:Label ID="lblDynamic" runat="server" EnableViewState="false" />
            </td>
        </tr>
        <tr style="height: 15px">
            <td colspan="2">
            </td>
        </tr>
        <asp:Panel ID="pnlDynamic" runat="server">
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblSubject" runat="server" EnableViewState="false" ResourceString="general.subject"
                        DisplayColon="true" />
                </td>
                <td>
                    <asp:RadioButton ID="radPageTitle" runat="server" GroupName="Subject" AutoPostBack="True"
                        OnCheckedChanged="radPageTitle_CheckedChanged" TextAlign="Right" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:RadioButton ID="radFollowing" runat="server" GroupName="Subject" AutoPostBack="True"
                        OnCheckedChanged="radFollowing_CheckedChanged" TextAlign="Right" />
                    <br />
                    <asp:TextBox ID="txtSubject" runat="server" MaxLength="500" CssClass="TextBoxField" Style="margin-top: 5px" />
                </td>
            </tr>
        </asp:Panel>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblIssueTemplate" EnableViewState="false" />
            </td>
            <td>
                <cms:NewsletterTemplateSelector ID="issueTemplate" runat="server" />
            </td>
        </tr>
        <asp:PlaceHolder ID="plcUrl" runat="server">
        <tr style="height: 5px;">
        </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblNewsletterDynamicURL" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtNewsletterDynamicURL" runat="server" CssClass="TextBoxField"
                        MaxLength="500" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcInterval" runat="server">
            <tr>
                <td class="FieldLabel" style="vertical-align: top;">
                    <asp:Label runat="server" ID="lblSchedule" EnableViewState="false" />
                </td>
                <td>
                    <asp:CheckBox ID="chkSchedule" runat="server" Checked="true" OnCheckedChanged="chkSchedule_CheckedChanged"
                        AutoPostBack="true" /><br />
                    <cms:ScheduleInterval ID="ScheduleInterval1" runat="server" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
