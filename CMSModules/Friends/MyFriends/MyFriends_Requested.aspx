<%@ Page Language="C#" AutoEventWireup="true" Theme="default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    CodeFile="MyFriends_Requested.aspx.cs" Inherits="CMSModules_Friends_MyFriends_MyFriends_Requested" %>

<%@ Register Src="~/CMSModules/Friends/Controls/FriendsRequestedList.ascx" TagName="FriendsListRequested"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:FriendsListRequested ID="FriendsListRequested" runat="server" Visible="true"
        IsLiveSite="false" />
    <cms:LocalizedLabel ID="lblError" runat="server" ResourceString="friends.norequestedfriends" EnableViewState="false" />
</asp:Content>
