using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.FileManager;
using CMS.Ecommerce;
using CMS.DataEngine;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_InvoiceTemplate_InvoiceTemplate_Edit : CMSEcommerceConfigurationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptHelper.RegisterClientScriptBlock(this, typeof (string), "MacroInsert", ScriptHelper.GetScript("function InsertMacro(macro) { var oEditor = FCKeditorAPI.GetInstance('" + this.htmlInvoiceTemplate.ClientID + "') ; if ( oEditor != null){ if (oEditor.EditMode == FCK_EDITMODE_WYSIWYG ) {oEditor.InsertHtml(macro) ;}  else { alert('You must be on WYSIWYG mode!') }; } } "));

        // Pagetitle
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("InvoiceTemplate.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Ecommerce/invoice.png");
        this.CurrentMaster.Title.HelpTopicName = "invoice";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // New item link
        string[,] actions = new string[1, 9];
        actions[0, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[0, 1] = ResHelper.GetString("general.save");
        actions[0, 2] = null;
        actions[0, 3] = null;
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        actions[0, 6] = "lnksave_click";
        actions[0, 7] = null;
        actions[0, 8] = "true";

        this.CurrentMaster.HeaderActions.LinkCssClass = "ContentSaveLinkButton";
        this.CurrentMaster.HeaderActions.Actions = actions;
        this.CurrentMaster.HeaderActions.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);

        AttachmentTitle.TitleText = ResHelper.GetString("general.attachments");

        // Init attachment storage
        if (CMSContext.CurrentSite != null)
        {
            AttachmentList.ObjectID = CMSContext.CurrentSite.SiteID;
        }

        macroSelectorElm.Resolver = MacroSelector.ShoppingCartResolver;

        AttachmentList.ObjectType = SiteObjectType.SITE;
        AttachmentList.Category = MetaFileInfoProvider.OBJECT_CATEGORY_INVOICE;
        AttachmentList.AllowEdit = CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify");

        DisplayHelperTable();

        if (!RequestHelper.IsPostBack())
        {
            if (CMSContext.CurrentSite != null)
            {
                SiteInfo site = SiteInfoProvider.GetSiteInfo(CMSContext.CurrentSite.SiteID);                

                htmlInvoiceTemplate.ResolvedValue = site.SiteInvoiceTemplate;
            }

            InitHTMLEditor();
        }
    }


    /// <summary>
    /// Save button action
    /// </summary>
    protected void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "lnksave_click":

                // check 'ConfigurationModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
                {
                    RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
                }

                // Check if template doesn't contains more editable regions with same name
                Hashtable eRegions = new Hashtable();

                int count = 0;
                int textStart = 0;
                int editRegStart = htmlInvoiceTemplate.ResolvedValue.Trim().IndexOf("$$", textStart);

                while (editRegStart >= 0)
                {
                    count++;

                    // End of region
                    editRegStart += 2;
                    textStart = editRegStart;
                    if (editRegStart < htmlInvoiceTemplate.ResolvedValue.Trim().Length - 1)
                    {
                        int editRegEnd = htmlInvoiceTemplate.ResolvedValue.Trim().IndexOf("$$", editRegStart);
                        if (editRegEnd >= 0)
                        {
                            string region = htmlInvoiceTemplate.ResolvedValue.Trim().Substring(editRegStart, editRegEnd - editRegStart);
                            string[] parts = (region + ":" + ":").Split(':');

                            textStart = editRegEnd + 2;
                            try
                            {
                                string name = parts[0];
                                if (name.Trim() != "")
                                {
                                    if (eRegions[name.ToLower()] != null)
                                    {
                                        break;
                                    }

                                    if (!ValidationHelper.IsCodeName(name))
                                    {
                                        break;
                                    }
                                    eRegions[name.ToLower()] = 1;
                                }
                            }
                            catch
                            {
                            }
                        }
                    }

                    editRegStart = htmlInvoiceTemplate.ResolvedValue.Trim().IndexOf("$$", textStart);
                }

                if (CMSContext.CurrentSite != null && IsValid)
                {
                    SiteInfo site = CMSContext.CurrentSite.Clone();

                    site.SiteInvoiceTemplate = htmlInvoiceTemplate.ResolvedValue.Trim();
                    SiteInfoProvider.SetSiteInfo(site);
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
                break;
        }
    }


    /// <summary>
    /// Initializes HTML editor's settings
    /// </summary>
    protected void InitHTMLEditor()
    {
        htmlInvoiceTemplate.AutoDetectLanguage = false;
        htmlInvoiceTemplate.DefaultLanguage = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        htmlInvoiceTemplate.MediaDialogConfig.UseFullURL = true;
        htmlInvoiceTemplate.LinkDialogConfig.UseFullURL = true;
        htmlInvoiceTemplate.QuickInsertConfig.UseFullURL = true;
    }


    protected void FillHelperTable(Table tblHelp, string[,] tableColumns)
    {
        for (int i = 0; i < tableColumns.GetUpperBound(0) + 1; i++)
        {
            TableRow tRow = new TableRow();
            TableCell leftCell = new TableCell();
            leftCell.Wrap = false;

            TableCell rightCell = new TableCell();

            Label lblExample = new Label();
            Label lblExplanation = new Label();

            // init labels
            lblExample.Text = tableColumns[i, 0];
            lblExplanation.Text = tableColumns[i, 1];

            // add labels to the cells
            leftCell.Controls.Add(lblExample);
            rightCell.Controls.Add(lblExplanation);

            leftCell.Width = new Unit(250);

            // add cells to the row
            tRow.Cells.Add(leftCell);
            tRow.Cells.Add(rightCell);

            // add row to the table
            tblHelp.Rows.Add(tRow);
        }
    }


    /// <summary>
    /// Displays helper table with makro examples.
    /// </summary>
    protected void DisplayHelperTable()
    {
        // 0 - left column (example), 1 - right column (explanation)
        string[,] tableColumns = new string[14, 2];

        int i = 0;

        //transformation expression examples
        tableColumns[i, 0] = "##BILLINGADDRESS##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_BILLINGADDRESS");

        tableColumns[i, 0] = "##COMPANYADDRESS##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.COMPANYADDRESS");

        tableColumns[i, 0] = "##SHIPPINGADDRESS##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.ShipppingAddress");

        tableColumns[i, 0] = "##PAYMENTOPTION##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_PAYMENTOPTION");

        tableColumns[i, 0] = "##SHIPPINGOPTION##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_ShippingOption");

        tableColumns[i, 0] = "##INVOICENUMBER##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_INVOICENUMBER");

        tableColumns[i, 0] = "##ORDERDATE##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_ORDERDATE");

        tableColumns[i, 0] = "##ORDERNOTE##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_ORDERNOTE");

        tableColumns[i, 0] = "##PRODUCTLIST##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_PRODUCTLIST");

        tableColumns[i, 0] = "##TOTALSHIPPING##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_TOTALSHIPPING");

        tableColumns[i, 0] = "##TOTALPRICE##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_TOTALPRICE");

        tableColumns[i, 0] = "##TAXRECAPITULATION##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_TAXRECAPITULATION");

        tableColumns[i, 0] = "##TAXREGISTRATIONID##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_TAXREGISTRATIONID");

        tableColumns[i, 0] = "##ORGANIZATIONID##";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_ORGANIZATIONID");

        FillHelperTable(tblHelp, tableColumns);

        // Generic macros
        tableColumns = new string[17, 2];

        i = 0;

        tableColumns[i, 0] = "{%ShoppingCart.ShoppingCartID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_ShoppingCart");

        tableColumns[i, 0] = "{%Order.OrderID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_ColumnName");

        tableColumns[i, 0] = "{%OrderStatus.StatusID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_OrderStatus");

        tableColumns[i, 0] = "{%BillingAddress.AddressID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_BillingAddressColumn");

        tableColumns[i, 0] = "{%BillingAddress.Country.CountryID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_BillingAddressCountry");

        tableColumns[i, 0] = "{%BillingAddress.State.StateID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_BillingAddressState");

        tableColumns[i, 0] = "{%ShippingAddress.AddressID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_ShippingAddress");

        tableColumns[i, 0] = "{%ShippingAddress.Country.CountryID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_ShippingAddressCountry");

        tableColumns[i, 0] = "{%ShippingAddress.State.StateID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_ShippingAddressState");

        tableColumns[i, 0] = "{%CompanyAddress.AddressID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_CompanyAddress");

        tableColumns[i, 0] = "{%CompanyAddress.Country.CountryID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_CompanyAddressCountry");

        tableColumns[i, 0] = "{%CompanyAddress.State.StateID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_CompanyAddressState");

        tableColumns[i, 0] = "{%ShippingOption.ShippingOptionID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_ShippingOptionColumn");

        tableColumns[i, 0] = "{%PaymentOption.PaymentOptionID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_PaymentOptionColumn");

        tableColumns[i, 0] = "{%Currency.CurrencyID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_Currency");

        tableColumns[i, 0] = "{%Customer.CustomerID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_Customer");

        tableColumns[i, 0] = "{%DiscountCoupon.DiscountCouponID%}";
        tableColumns[i++, 1] = ResHelper.GetString("Order_Edit_Invoice.Help_DiscountCoupon");

        FillHelperTable(tblMore, tableColumns);

        lblHelpHeader.Text = ResHelper.GetString("Order_Edit_Invoice.Help_Header");
        lnkMoreMacros.Text = ResHelper.GetString("Order_Edit_Invoice.MoreMacros");
        lblMoreInfo.Text = ResHelper.GetString("Order_Edit_Invoice.MoreInfo");
    }
}
