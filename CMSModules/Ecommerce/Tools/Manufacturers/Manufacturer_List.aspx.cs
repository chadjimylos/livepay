using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Manufacturers_Manufacturer_List : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.HeaderActions.HelpTopicName = "manufacturer_list";
        this.CurrentMaster.HeaderActions.HelpName = "helpTopic";

        // New item link
        string[,] actions = new string[1, 7];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Manufacturer_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("Manufacturer_Edit.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_Manufacturer/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;

        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    protected object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "manuenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Manufacturer_Edit.aspx?manufacturerId=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // check 'EcommerceModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
            }

            int manufacturerId = ValidationHelper.GetInteger(actionArgument, 0);
            // Check dependences
            if (ManufacturerInfoProvider.CheckDependencies(manufacturerId))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                return;
            }

            // delete ManufacturerInfo object from database
            ManufacturerInfoProvider.DeleteManufacturerInfo(manufacturerId);
        }
    }
}
