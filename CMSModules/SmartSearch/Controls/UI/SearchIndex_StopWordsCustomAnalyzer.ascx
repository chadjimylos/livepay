﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchIndex_StopWordsCustomAnalyzer.ascx.cs"
    Inherits="CMSModules_SmartSearch_Controls_UI_SearchIndex_StopWordsCustomAnalyzer" %>
<tr runat="server" id="stopWordsRow">
    <td class="FieldLabel">
        <cms:LocalizedLabel runat="server" ID="lblStopWords" EnableViewState="false" ResourceString="srch.index.stopwords"
            DisplayColon="true" />
    </td>
    <td>
        <asp:DropDownList ID="drpStopWords" runat="server" CssClass="DropDownField">
        </asp:DropDownList>
    </td>
</tr>
<tr runat="server" id="customAnalyzerName">
    <td class="FieldLabel">
        <cms:LocalizedLabel runat="server" ID="lblCustomAnalyzer" EnableViewState="false"
            ResourceString="srch.index.customanalyzer" DisplayColon="true" />
    </td>
    <td>
        <asp:TextBox ID="txtCustomAnalyzer" runat="server" CssClass="TextBoxField" MaxLength="200" />
    </td>
</tr>
