<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Category_List.aspx.cs" Inherits="CMSSiteManager_Development_Categories_Category_List"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="Default" Title="Categories List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <cms:UniGrid ID="gridCategories" runat="server" IsLiveSite="false" Columns="CategoryID, CategoryDisplayName, CategoryEnabled, CategoryCount" />
</asp:Content>
