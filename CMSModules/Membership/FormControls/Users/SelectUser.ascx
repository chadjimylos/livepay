<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectUser.ascx.cs" Inherits="CMSModules_Membership_FormControls_Users_SelectUser" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="usUsers" runat="server" ObjectType="cms.user" SelectionMode="SingleTextBox" AllowEditTextBox="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
