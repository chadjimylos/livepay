﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;

public partial class CMSModules_MediaLibrary_Controls_MediaLibrary_ImageEditor : CMSModalPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (QueryHelper.GetBoolean("refresh", false))
        {
            string guid = QueryHelper.GetString("MediaFileGUID", "");
            string siteName = QueryHelper.GetString("siteName", "");
            bool isPreview = QueryHelper.GetBoolean("isPreview", false);

            this.CurrentMaster.Body.Attributes["onload"] = "if(typeof(wopener.EditDialogStateUpdate) == 'function') { wopener.EditDialogStateUpdate('true'); }";
            this.CurrentMaster.Body.Attributes["onbeforeunload"] = "if(typeof(wopener.EditDialogStateUpdate) == 'function') { wopener.EditDialogStateUpdate('false'); }";
            this.CurrentMaster.Body.Attributes["onunload"] = "if(typeof(wopener.imageEdit_Refresh) == 'function') { wopener.imageEdit_Refresh('" + guid + "|" + siteName + "|" + isPreview + "'); }";                          
        }

        this.CurrentMaster.Title.TitleText = ResHelper.GetString("general.editimage");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Design/Controls/ImageEditor/Title.png");

        this.AddNoCacheTag();
    }
}