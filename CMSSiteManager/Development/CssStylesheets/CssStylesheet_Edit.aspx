<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CssStylesheet_Edit.aspx.cs" Inherits="CMSSiteManager_Development_CssStylesheets_CssStylesheet_Edit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Css stylesheets - edit</title>
</head>
		<frameset border="0" rows="102, *" id="rowsFrameset">
    		<frame name="Menu" src="CssStylesheet_Header.aspx?cssstylesheetid=<%=Request.QueryString["cssstylesheetid"]%> " scrolling="no" frameborder="0" noresize="noresize" />		
		    <frame name="Content" src="CssStylesheet_General.aspx?cssstylesheetid=<%=Request.QueryString["cssstylesheetid"]%>&saved=<%=Request.QueryString["saved"]%> " frameborder="0"  noresize="noresize" />
    	<noframes>
			<body>
			 <p id="p1">
				This HTML frameset displays multiple Web pages. To view this frameset, use a 
				Web browser that supports HTML 4.0 and later.
			 </p>
			</body>
		</noframes>
</frameset>
</html>
