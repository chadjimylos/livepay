using System;
using System.Data;
using System.Collections;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.CMSImportExport;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.PortalEngine;
using CMS.UIControls;
using CMS.LicenseProvider;

public partial class CMSModules_ImportExport_Controls_ImportGridView : CMSUserControl
{
    #region "Variables"

    // Indicates if object selection is enabled
    protected bool selectionEnabled = true;

    protected string codeNameColumnName = "";
    protected string displayNameColumnName = "";

    protected string preposition = "chck_";
    protected string taskPreposition = "etchck_";

    private SiteImportSettings mSettings = null;
    private ArrayList mSelectedValues = null;
    private ArrayList mSelectedTasks = null;

    protected ArrayList mExistingObjects = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Import settings
    /// </summary>
    public SiteImportSettings Settings
    {
        get
        {
            return mSettings;
        }
        set
        {
            mSettings = value;
        }
    }


    /// <summary>
    /// Selected values
    /// </summary>
    public ArrayList SelectedValues
    {
        get
        {
            return mSelectedValues;
        }
    }


    /// <summary>
    /// Selected tasks
    /// </summary>
    public ArrayList SelectedTasks
    {
        get
        {
            return mSelectedTasks;
        }
    }


    /// <summary>
    /// Existing objects in the database
    /// </summary>
    public ArrayList ExistingObjects
    {
        get
        {
            if (mExistingObjects == null)
            {
                DataSet ds = ImportProvider.GetExistingObjects(Settings, ObjectType, SiteObject);
                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    // Get info object
                    IInfoObject infoObj = CMSObjectHelper.GetObject(ObjectType);
                    if (infoObj == null)
                    {
                        throw new Exception("[ImportGridView]: Object type '" + ObjectType + "' not found.");
                    }

                    // For each object get codename
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        string codeName = ValidationHelper.GetString(DataHelper.GetDataRowValue(row, infoObj.CodeNameColumn), null);

                        if (codeName != null)
                        {
                            // Initialize array list
                            if (mExistingObjects == null)
                            {
                                mExistingObjects = new ArrayList();
                            }

                            mExistingObjects.Add(codeName.ToLower());
                        }
                    }
                }
            }
            return mExistingObjects;
        }
    }


    /// <summary>
    /// Current object type
    /// </summary>
    public string ObjectType
    {
        get
        {
            return ValidationHelper.GetString(ViewState["ObjectType"], null);
        }
        set
        {
            ViewState["ObjectType"] = value;
        }
    }


    /// <summary>
    /// True if site object
    /// </summary>
    public bool SiteObject
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["SiteObject"], false);
        }
        set
        {
            ViewState["SiteObject"] = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (RequestHelper.IsPostBack())
        {
            // Initialize selected values
            foreach (string key in Request.Form.AllKeys)
            {
                if (key == null)
                {
                    continue;
                }

                if (key.StartsWith(preposition))
                {
                    if (mSelectedValues == null)
                    {
                        mSelectedValues = new ArrayList();
                    }

                    mSelectedValues.Add(key.Remove(0, preposition.Length).ToLower());
                }

                if (key.StartsWith(taskPreposition))
                {
                    if (mSelectedTasks == null)
                    {
                        mSelectedTasks = new ArrayList();
                    }

                    int taskId = ValidationHelper.GetInteger(key.Remove(0, taskPreposition.Length), 0);
                    if (taskId > 0)
                    {
                        mSelectedTasks.Add(taskId);
                    }
                }
            }
        }
    }


    /// <summary>
    /// Bind the data
    /// </summary>
    public void Bind()
    {
        if (!string.IsNullOrEmpty(ObjectType))
        {
            pnlGrid.Visible = true;
            selectionEnabled = ((ObjectType != LicenseObjectType.LICENSEKEY) || !Settings.IsOlderVersion);

            lblTasks.Text = ResHelper.GetString("Export.Tasks");

            if (selectionEnabled)
            {
                // Initilaize strings
                btnAll.Text = ResHelper.GetString("ImportExport.All");
                btnNone.Text = ResHelper.GetString("export.none");
                btnAllTasks.Text = ResHelper.GetString("ImportExport.All");
                btnNoneTasks.Text = ResHelper.GetString("export.none");
                btnDefault.Text = ResHelper.GetString("General.Default");

                // Add selection links functionality
                btnAll.OnClientClick = "igvChangeCheckboxes('" + ValidationHelper.GetIdentificator(ObjectType) + "', 1, " + (SiteObject ? "1" : "0") + ");return false;";
                btnNone.OnClientClick = "igvChangeCheckboxes('" + ValidationHelper.GetIdentificator(ObjectType) + "', 0, " + (SiteObject ? "1" : "0") + ");return false;";
                btnDefault.OnClientClick = "igvChangeCheckboxes('" + ValidationHelper.GetIdentificator(ObjectType) + "', -1, " + (SiteObject ? "1" : "0") + ");return false;";
                btnAllTasks.OnClientClick = "igvChangeCheckboxes('export_task', 1, " + (SiteObject ? "1" : "0") + ");return false;";
                btnNoneTasks.OnClientClick = "igvChangeCheckboxes('export_task', 0, " + (SiteObject ? "1" : "0") + ");return false;";
            }

            pnlLinks.Visible = selectionEnabled;
            pnlTaskLinks.Visible = selectionEnabled;

            // Get object info
            IInfoObject info = CMSObjectHelper.GetObject(ObjectType);
            if (info != null)
            {
                plcGrid.Visible = true;
                codeNameColumnName = info.CodeNameColumn;
                displayNameColumnName = info.DisplayNameColumn;

                // Get data source
                DataSet ds = ImportProvider.LoadObjects(Settings, ObjectType, SiteObject);
                DataTable table = null;
                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    // Get the table
                    string tableName = CMSObjectHelper.GetTableName(info);
                    table = ds.Tables[tableName];

                    // Sort data
                    if (!DataHelper.DataSourceIsEmpty(table))
                    {
                        string orderBy = GetOrderByExpression(info);
                        table.DefaultView.Sort = orderBy;
                    }
                }

                // Prepare checkbox column
                TemplateField checkBoxField = (TemplateField)gvObjects.Columns[0];
                checkBoxField.HeaderText = ResHelper.GetString("General.Import");

                // Prepare name field
                TemplateField nameField = (TemplateField)gvObjects.Columns[1];
                nameField.HeaderText = ResHelper.GetString("general.displayname");

                if (!DataHelper.DataSourceIsEmpty(table))
                {
                    plcObjects.Visible = true;
                    lblNoData.Visible = false;
                    gvObjects.DataSource = table;
                    gvObjects.DataBind();
                }
                else
                {
                    plcObjects.Visible = false;
                    lblNoData.Visible = true;
                    lblNoData.Text = String.Format(ResHelper.GetString("ImportGridView.NoData"), ResHelper.GetString("objecttype." + ObjectType.Replace(".", "_").Replace("#", "_")));
                }

                // Task fields
                TemplateField taskCheckBoxField = (TemplateField)gvTasks.Columns[0];
                taskCheckBoxField.HeaderText = ResHelper.GetString("General.Process");

                BoundField titleField = (BoundField)gvTasks.Columns[1];
                titleField.HeaderText = ResHelper.GetString("Export.TaskTitle");

                BoundField typeField = (BoundField)gvTasks.Columns[2];
                typeField.HeaderText = ResHelper.GetString("general.type");

                BoundField timeField = (BoundField)gvTasks.Columns[3];
                timeField.HeaderText = ResHelper.GetString("Export.TaskTime");

                // Load tasks
                if (!DataHelper.DataSourceIsEmpty(ds) && !DataHelper.DataSourceIsEmpty(ds.Tables["Export_Task"]) && (ValidationHelper.GetBoolean(Settings.GetSettings(ImportExportHelper.SETTINGS_TASKS), true)))
                {
                    plcTasks.Visible = true;
                    gvTasks.DataSource = ds.Tables["Export_Task"];
                    gvTasks.DataBind();
                }
                else
                {
                    plcTasks.Visible = false;
                }
            }
            else
            {
                plcGrid.Visible = false;
            }

            // Disable license selection
            bool enable = !((ObjectType == LicenseObjectType.LICENSEKEY) && Settings.IsOlderVersion);
            gvObjects.Enabled = enable;
            gvTasks.Enabled = enable;
            pnlLinks.Enabled = enable;
            pnlTaskLinks.Enabled = enable;
            lblInfo.Text = enable ? ResHelper.GetString("ImportGridView.Info") : ResHelper.GetString("ImportGridView.Disabled");
        }
        else
        {
            pnlGrid.Visible = false;
            gvObjects.DataSource = null;
            gvObjects.DataBind();
        }
    }


    // Get orderby expression
    private static string GetOrderByExpression(IInfoObject info)
    {
        switch (info.ObjectType)
        {
            case PortalObjectType.PAGETEMPLATE:
                return "PageTemplateIsReusable DESC," + info.DisplayNameColumn;
            default:
                {
                    if (info.DisplayNameColumn != TypeInfo.COLUMN_NAME_UNKNOWN)
                    {
                        return info.DisplayNameColumn;
                    }
                    else
                    {
                        return info.CodeNameColumn;
                    }
                }
        }
    }


    /// <summary>
    /// Ensure objects preselection and genearate appropriate code snippet
    /// </summary>
    /// <param name="codeName">Code name</param>
    protected string IsChecked(object codeName)
    {
        string value = "";
        string name = ValidationHelper.GetString(codeName, "");
        if (Settings.IsSelected(ObjectType, name, SiteObject))
        {
            value += " checked=\"checked\" ";
        }

        if (!selectionEnabled)
        {
            value += " disabled=\"disabled\" ";
        }

        return value;
    }


    /// <summary>
    /// Ensure tasks preselection
    /// </summary>
    /// <param name="taskId">Task ID</param>
    protected string IsTaskChecked(object taskId)
    {
        string value = "";
        int id = ValidationHelper.GetInteger(taskId, 0);
        if (Settings.IsTaskSelected(ObjectType, id, SiteObject))
        {
            value += " checked=\"checked\" ";
        }
        if (!selectionEnabled)
        {
            value += " disabled=\"disabled\" ";
        }

        return value;
    }


    /// <summary>
    /// Check if object is in conflict in database and genearate appropriate code snippet
    /// </summary>
    /// <param name="codeName">Code name</param>
    protected string IsInConflict(object codeName)
    {
        string name = ValidationHelper.GetString(codeName, "").ToLower();
        if ((ExistingObjects != null) && (ExistingObjects.Contains(name)))
        {
            return "*";
        }
        return "";
    }


    protected string GetName(object codeNameObj, object displayNameObj)
    {
        string codeName = ValidationHelper.GetString(codeNameObj, "");
        string displayName = ValidationHelper.GetString(displayNameObj, "");

        if (string.IsNullOrEmpty(displayName))
        {
            return codeName;
        }
        return ResHelper.GetString(displayName);
    }
}
