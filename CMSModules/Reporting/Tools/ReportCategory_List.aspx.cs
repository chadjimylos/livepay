using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Reporting;
using CMS.CMSHelper;
using CMS.LicenseProvider;
using CMS.UIControls;

public partial class CMSModules_Reporting_Tools_ReportCategory_List : CMSReportingPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        this.InitializeMasterPage();
    }


    /// <summary>
    /// Initializes Master Page.
    /// </summary>
    protected void InitializeMasterPage()
    {
        // Initialize help and title
        this.Title = "Report category list";
        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.Title.HelpTopicName = "report_category_list";

        // Set actions
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("ReportCategory_List.NewItemCaption");
        actions[0, 3] = ResolveUrl("ReportCategory_New.aspx");
        actions[0, 5] = GetImageUrl("Objects/Reporting_ReportCategory/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        // Set title label
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("ReportCategory_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Reporting_ReportCategory/object.png");
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Category_Frameset.aspx?CategoryID=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // Check 'Read' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
            {
                RedirectToAccessDenied("cms.reporting", "Modify");
            }

            // delete ReportCategoryInfo object from database
            ReportCategoryInfoProvider.DeleteReportCategoryInfo(Convert.ToInt32(actionArgument));
        }
    }
}
