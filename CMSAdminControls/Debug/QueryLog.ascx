<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QueryLog.ascx.cs" Inherits="CMSAdminControls_Debug_QueryLog" %>
<%@ Import Namespace="CMS.GlobalHelper" %>
<div style="<%=mLogStyle%>">
    <asp:Literal runat="server" ID="ltlInfo" EnableViewState="false" />
    <asp:GridView runat="server" ID="gridQueries" EnableViewState="false" GridLines="Both"
        AutoGenerateColumns="false" Width="100%" CellPadding="3" ShowFooter="true" BorderStyle="Solid"
        BorderColor="#cccccc" BackColor="#ffffff">
        <HeaderStyle HorizontalAlign="Left" BackColor="#e8e8e8" />
        <AlternatingRowStyle BackColor="#f4f4f4" />
        <FooterStyle BackColor="#e8e8e8" />
        <Columns>
            <asp:TemplateField>
                <HeaderStyle Wrap="false" BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1" />
                <FooterStyle BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1" />
                <ItemStyle BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1" HorizontalAlign="Center" />
                <ItemTemplate>
                    <strong>
                        <%# GetIndex(Eval("QueryResultsSize"), Eval("QueryText"))%>
                    </strong>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1" />
                <FooterStyle BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1" />
                <ItemStyle BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1" />
                <ItemTemplate>
                    <asp:PlaceHolder runat="server" ID="PlaceHolder1" Visible='<%# ValidationHelper.GetString(Eval("ConnectionOp"), "") != "" %>'>
                        <strong><%# TextHelper.EnsureLineEndings(HttpUtility.HtmlEncode(ValidationHelper.GetString(Eval("ConnectionOp"), "")), "<br />")%></strong>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="plcQueryName" Visible='<%# ValidationHelper.GetString(Eval("QueryName"), "") != "" %>'>
                        <div style="margin-bottom: 2px; padding: 1px; border-bottom: solid 1px #dddddd;">
                            <strong>(<%# HttpUtility.HtmlEncode(ValidationHelper.GetString(Eval("QueryName"), "").ToLower())%>)</strong>
                        </div>
                    </asp:PlaceHolder>
                    <%# TextHelper.EnsureLineEndings(HttpUtility.HtmlEncode(ValidationHelper.GetString(Eval("QueryText"), "")), "")%>
                    <asp:PlaceHolder runat="server" ID="plcParameters" Visible='<%# ValidationHelper.GetString(Eval("QueryParameters"), "") != "" %>'>
                        <div style="margin-top: 2px; padding: 1px; border-top: solid 1px #dddddd;">
                            <%# TextHelper.EnsureLineEndings(HttpUtility.HtmlEncode(ValidationHelper.GetString(Eval("QueryParameters"), "")), "<br />")%>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="plcResults" Visible='<%# ValidationHelper.GetString(Eval("QueryResults"), "") != "" %>'>
                        <div style="margin-top: 2px; border-top: solid 1px #dddddd;">
                            <%# TextHelper.EnsureLineEndings(HttpUtility.HtmlEncode(ValidationHelper.GetString(Eval("QueryResults"), "")), ", ")%>
                        </div>
                    </asp:PlaceHolder>
                </ItemTemplate>
                <FooterTemplate>
                    <strong>
                        <%# String.Format(ResHelper.GetString("QueryLog.Total"), index, CMS.SettingsProvider.SqlHelperClass.GetSizeString(this.TotalSize)) %>
                    </strong>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle Wrap="false" HorizontalAlign="Center" BorderColor="#cccccc" BorderStyle="Solid"
                    BorderWidth="1" />
                <FooterStyle BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1" />
                <ItemStyle BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1" />
                <ItemTemplate>
                    <%# TextHelper.EnsureLineEndings(ValidationHelper.GetString(Eval("Context"), ""), "<br />")%>
                </ItemTemplate>
                <FooterTemplate>
                    <strong>
                        <%# cmsVersion %></strong>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle Wrap="false" Width="70px" HorizontalAlign="Center" BorderColor="#cccccc"
                    BorderStyle="Solid" BorderWidth="1" />
                <ItemStyle Wrap="false" HorizontalAlign="Right" BorderColor="#cccccc" BorderStyle="Solid"
                    BorderWidth="1" />
                <FooterStyle Wrap="false" HorizontalAlign="Right" BorderColor="#cccccc" BorderStyle="Solid"
                    BorderWidth="1" />
                <ItemTemplate>
                    <%# GetDuration(Eval("QueryStartTime"), Eval("QueryEndTime")) %>
                </ItemTemplate>
                <FooterTemplate>
                    <strong>
                        <%# this.TotalDuration.ToString("F3") %>
                    </strong>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
