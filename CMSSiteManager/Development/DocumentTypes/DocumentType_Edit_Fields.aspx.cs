using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_Fields : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int classId = 0;
        string className = null;
        bool isCoupledClass = false;
        DataClassInfo dci = null;        

        classId = ValidationHelper.GetInteger(Request.QueryString["documenttypeid"], 0);
        dci = DataClassInfoProvider.GetDataClass(classId);
        FieldEditor.Visible = false;

        if (dci != null)
        {
            className = dci.ClassName;
            isCoupledClass = dci.ClassIsCoupledClass;

            if (dci.ClassIsCoupledClass)
            {
                FieldEditor.Visible = true;
                FieldEditor.ClassName = className;                
                FieldEditor.Mode = FieldEditorModeEnum.ClassFormDefinition;
            }
            else
            {                
                lblError.Text = ResHelper.GetString("EditTemplateFields.ErrorIsNotCoupled");
            }
        }                
    }
}
