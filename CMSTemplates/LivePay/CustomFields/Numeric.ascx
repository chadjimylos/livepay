<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Numeric.ascx.cs" Inherits="CMSFormControls_Numeric" %>
<script type="text/javascript">
    function checkNumeric(sender, args) {
        var numeric = args.Value;
        var bIsValid = false;

        var RegExp_Numeric = new RegExp(/^\d+$/);

        if (RegExp_Numeric.test(numeric))
            bIsValid = true;

        var IsCutomValid = sender.getAttribute('IsCustomValidator');
        var CustomErrorMess = sender.getAttribute('CustomValidatorErrorMessage');
        if (IsCutomValid != null) {
            if (IsCutomValid.indexOf('CustomCloud_') != -1) {
                if (!bIsValid) {
                    Tab1_ValidationCloud('#' + IsCutomValid, CustomErrorMess, true, '')
                } else {
                    Tab1_ValidationCloud('#' + IsCutomValid, '', false, '')
                }
            }

        }
        args.IsValid = bIsValid;
    }
</script>

<div runat="server" id="CustomCloud_div"></div>

<asp:TextBox runat="server" ID="Numeric" cssclass="RegUsersTab1CustomTxt" width="260px" />

<div class="HideIt">
    
    <asp:CustomValidator ID="cv_Numeric" 
                        ClientValidationFunction="checkNumeric" runat="server"   
                        ControlToValidate="Numeric" ValidationGroup="Tab1_PayForm" />
</div>