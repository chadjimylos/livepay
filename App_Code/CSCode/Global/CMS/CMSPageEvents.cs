using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Controls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;

/// <summary>
/// Summary description for CMSPageEvents
/// </summary>
public static class CMSPageEvents
{
    /// <summary>
    /// Fires before page PreInit
    /// </summary>
    /// <returns>Returns true if default action should be performed</returns>
    public static bool BeforePagePreInit(object sender, EventArgs e)
    {
        // Add your custom actions

        return true;
    }


    /// <summary>
    /// Fires after page PreInit
    /// </summary>
    public static void AfterPagePreInit(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before page Init
    /// </summary>
    /// <returns>Returns true if default action should be performed</returns>
    public static bool BeforePageInit(object sender, EventArgs e)
    {
        // Add your custom actions

        return true;
    }


    /// <summary>
    /// Fires after page Init
    /// </summary>
    public static void AfterPageInit(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before page Load
    /// </summary>
    /// <returns>Returns true if default action should be performed</returns>
    public static bool BeforePageLoad(object sender, EventArgs e)
    {
        // Add your custom actions
        return true;
    }


    /// <summary>
    /// Fires after page Load
    /// </summary>
    public static void AfterPageLoad(object sender, EventArgs e)
    {
        // Add your custom actions

        /*
         
        CMSPage page = (CMSPage)sender;
        switch (page.RelativePath.ToLower())
        {
            case "/cmsmodules/bizforms/tools/bizform_edit_header.aspx":
                BasicTabControl tabControl = (BasicTabControl)page["TabControl"];
                // Add tabs
                string[,] tabs = BasicTabControl.GetTabsArray(1);

                tabs[0, 0] = "Google";
                tabs[0, 2] = "http://www.google.com";

                tabControl.AddTabs(tabs);

                break;
        }

        */
    }


    /// <summary>
    /// Fires before page PreRender
    /// </summary>
    /// <returns>Returns true if default action should be performed</returns>
    public static bool BeforePagePreRender(object sender, EventArgs e)
    {
        // Add your custom actions

        return true;
    }


    /// <summary>
    /// Fires after page PreRender
    /// </summary>
    public static void AfterPagePreRender(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before page Render
    /// </summary>
    /// <returns>Returns true if default action should be performed</returns>
    public static bool BeforePageRender(object sender, HtmlTextWriter writer)
    {
        // Add your custom actions

        return true;
    }


    /// <summary>
    /// Fires after page Render
    /// </summary>
    public static void AfterPageRender(object sender, HtmlTextWriter writer)
    {
        // Add your custom actions
    }
}
