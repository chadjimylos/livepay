CREATE TABLE [CMS_ResourceSite] (
		[ResourceID]     int NOT NULL,
		[SiteID]         int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_ResourceSite]
	ADD
	CONSTRAINT [PK_CMS_ResourceSite]
	PRIMARY KEY
	([ResourceID], [SiteID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_ResourceSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Resource]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_ResourceSite_ResourceID_CMS_Resource]') IS NULL
BEGIN
		ALTER TABLE [CMS_ResourceSite]
			ADD CONSTRAINT [FK_CMS_ResourceSite_ResourceID_CMS_Resource]
			FOREIGN KEY ([ResourceID]) REFERENCES [CMS_Resource] ([ResourceID])
END
IF OBJECT_ID(N'[CMS_ResourceSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_ResourceSite_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_ResourceSite]
			ADD CONSTRAINT [FK_CMS_ResourceSite_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
