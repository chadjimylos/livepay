<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CssStylesheet_General.aspx.cs"
    Inherits="CMSSiteManager_Development_CssStylesheets_CssStylesheet_General" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Css stylesheets - new" %>

<asp:Content ID="cntBeforeBody" runat="server" ContentPlaceHolderID="plcBeforeContent">
    <asp:Panel ID="pnlCheckOutInfo" runat="server" CssClass="PageContentLine">
        <asp:Label runat="server" EnableViewState="false" ID="lblCheckOutInfo" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Panel ID="pnlContainer" runat="server">
        <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" Visible="false" EnableViewState="false" />
        <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" ForeColor="red" />
        <table>
            <tbody>
                <tr>
                    <td>
                        <asp:Label ID="lblCssStylesheetDisplayName" runat="server" EnableViewState="False" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtCssStylesheetDisplayName" runat="server" CssClass="TextBoxField"
                            MaxLength="200"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" EnableViewState="false"
                            Display="dynamic" ControlToValidate="txtCssStylesheetDisplayName"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblCssStylesheetName" runat="server" EnableViewState="False" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtCssStylesheetName" runat="server" CssClass="TextBoxField" MaxLength="200" /><br />
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" EnableViewState="false" Display="dynamic"
                            ControlToValidate="txtCssStylesheetName"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblCssStylesheetText" runat="server" />
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <cms:ExtendedTextArea EnableViewState="false" ID="txtCssStylesheetText" runat="server"
                                        CssClass="TextAreaField" Width="620px" TextMode="MultiLine" Height="385px" RegularExpression="\s*/\*\s*#\s*([a-zA-Z_0-9-/\+\*.=~\!@\$%\^&\(\[\]\);:<>\?\s]*)\s*#\s*\*/"
                                        EnableSections="true" Wrap="false" /><br />
                                    <asp:RequiredFieldValidator ID="rfvText" runat="server" Display="Dynamic" ControlToValidate="txtCssStylesheetText" />
                                </td>
                                <td>
                                    <cms:ExtendedListBox ID="extListBox" runat="server" Width="200px" TextBoxControl="txtCssStylesheetText"
                                        SectionSeparator="/" Height="390px">
                                    </cms:ExtendedListBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
</asp:Content>