using System;

using CMS.WorkflowEngine;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Workflows_Workflow_General : SiteManagerPage
{
    #region "Private variables"

    protected int workflowId = 0;

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "Workflow Edit - General";

        rfvCodeName.ErrorMessage = ResHelper.GetString("Development-Workflow_New.RequiresCodeName");
        RequiredFieldValidatorDisplayName.ErrorMessage = ResHelper.GetString("Development-Workflow_New.RequiresDisplayName");

        if (QueryHelper.GetInteger("saved", 0) == 1)
        {
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }

        // Get ID of workflow from querystring
        workflowId = QueryHelper.GetInteger("workflowid", 0);

        if ((workflowId != 0) && !RequestHelper.IsPostBack())
        {
            WorkflowInfo wi = WorkflowInfoProvider.GetWorkflowInfo(workflowId);
            if (wi != null)
            {
                txtCodeName.Text = wi.WorkflowName;
                TextBoxWorkflowDisplayName.Text = wi.WorkflowDisplayName;
            }
        }
    }


    /// <summary>
    /// Saves data of edited workflow from TextBoxes into DB.
    /// </summary>
    protected void ButtonOK_Click(object sender, EventArgs e)
    {
        // finds whether required fields are not empty
        string result = new Validator().NotEmpty(TextBoxWorkflowDisplayName.Text, ResHelper.GetString("Development-Workflow_New.RequiresDisplayName"))
            .NotEmpty(txtCodeName.Text, ResHelper.GetString("Development-Workflow_New.RequiresCodeName"))
            .IsCodeName(txtCodeName.Text, ResHelper.GetString("general.invalidcodename"))
            .Result;

        if (result == "")
        {
            if (workflowId > 0)
            {
                WorkflowInfo wi = WorkflowInfoProvider.GetWorkflowInfo(workflowId);
                if (wi != null)
                {
                    // Codename must be unique
                    WorkflowInfo wiCodename = WorkflowInfoProvider.GetWorkflowInfo(txtCodeName.Text);
                    if ((wiCodename == null) || (wiCodename.WorkflowID == wi.WorkflowID))
                    {
                        wi.WorkflowDisplayName = TextBoxWorkflowDisplayName.Text;
                        wi.WorkflowName = txtCodeName.Text;
                        WorkflowInfoProvider.SetWorkflowInfo(wi);

                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("Development-Workflow_New.WorkflowExists");
                    }
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Development-Workflow_General.WorkflowDoesNotExists");
                }
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }

    #endregion
}
