CREATE TABLE [CMS_SearchIndex] (
		[IndexID]                     int NOT NULL IDENTITY(1, 1),
		[IndexName]                   nvarchar(200) NOT NULL,
		[IndexDisplayName]            nvarchar(200) NOT NULL,
		[IndexAnalyzerType]           nvarchar(200) NULL,
		[IndexIsCommunityGroup]       bit NOT NULL,
		[IndexSettings]               nvarchar(max) NULL,
		[IndexGUID]                   uniqueidentifier NOT NULL,
		[IndexLastModified]           datetime NOT NULL,
		[IndexLastRebuildTime]        datetime NULL,
		[IndexType]                   nvarchar(200) NOT NULL,
		[IndexStopWordsFile]          nvarchar(200) NULL,
		[IndexCustomAnalyzerName]     nvarchar(200) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_SearchIndex]
	ADD
	CONSTRAINT [PK_CMS_SearchIndex]
	PRIMARY KEY
	NONCLUSTERED
	([IndexID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_SearchIndex]
	ADD
	CONSTRAINT [DEFAULT_CMS_SearchIndex_IndexType]
	DEFAULT ('') FOR [IndexType]
CREATE CLUSTERED INDEX [IX_CMS_SearchIndex_IndexDisplayName]
	ON [CMS_SearchIndex] ([IndexDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
