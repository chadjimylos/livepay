<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SystemTable_Edit_Query_List.aspx.cs"
    Inherits="CMSSiteManager_Development_SystemTables_SystemTable_Edit_Query_List"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="System tables - Queries"
    Theme="Default" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/ClassQueries.ascx" TagName="ClassQueries" TagPrefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:ClassQueries ID="classEditQuery" runat="server" />
</asp:Content>
