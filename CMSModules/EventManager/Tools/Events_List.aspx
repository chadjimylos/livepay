<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Events_List.aspx.cs" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Events list" Inherits="CMSModules_EventManager_Tools_Events_List" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <cms:UniGrid runat="server" ID="gridElem" GridName="Events_List.xml" OrderBy="EventDate DESC"
        IsLiveSite="false" />

    <script type="text/javascript">
    //<![CDATA[
        function EditEvent(eventId)
        {
            location.replace('Events_Edit.aspx?eventId=' + eventId);
        }
    //]]>
    </script>

</asp:Content>
