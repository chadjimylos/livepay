using System;
using System.Collections;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.TreeEngine;
using CMS.CMSHelper;


public partial class CMSModules_Content_Controls_Dialogs_Properties_DocCopyMoveProperties : ItemProperties
{
    #region "Enums"

    protected enum Action
    {
        Move = 0,
        Copy = 1,
        Link = 2,
        LinkDoc = 3
    }

    #endregion


    #region "Private properties"

    /// <summary>
    /// Returns current dialog action
    /// </summary>
    private Action CurrentAction
    {
        get
        {
            try
            {
                return (Action)Enum.Parse(typeof(Action), Config.CustomFormatCode, true);
            }
            catch
            {
                return Action.Move;
            }
        }
    }


    /// <summary>
    /// Target node alias path.
    /// </summary>
    private int AliasPath
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["AliasPath"], 0);
        }
        set
        {
            ViewState["AliasPath"] = value;
        }
    }


    /// <summary>
    /// Target node ID.
    /// </summary>
    private int TargetNodeID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["TargetNodeID"], 0);
        }
        set
        {
            ViewState["TargetNodeID"] = value;
        }
    }


    /// <summary>
    /// Preview URL.
    /// </summary>
    private string PreviewURL
    {
        get
        {
            return ValidationHelper.GetString(ViewState["PreviewURL"], string.Empty);
        }
        set
        {
            ViewState["PreviewURL"] = value;
        }
    }


    /// <summary>
    /// Preview extension.
    /// </summary>
    private string PreviewExt
    {
        get
        {
            return ValidationHelper.GetString(ViewState["PreviewExt"], string.Empty);
        }
        set
        {
            ViewState["PreviewExt"] = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        innerFrame.Attributes.Add("src", GetFrameUrl(null, null, null, null, null, null, QueryHelper.GetBoolean("multiple", false), CurrentAction));

        string nodeIdsString = QueryHelper.GetString("sourcenodeids", string.Empty);

        // Load node IDs to session for the first time
        if (!UrlHelper.IsPostback() && !String.IsNullOrEmpty(nodeIdsString))
        {
            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
            SessionHelper.SetValue("CopyMoveDocAliasPaths", tree.SelectNodes(CMSContext.CurrentSiteName, TreeProvider.ALL_DOCUMENTS, TreeProvider.ALL_CULTURES, true, null, "NodeID IN (" + nodeIdsString.Trim('|').Replace("|", ",") + ")", null, TreeProvider.ALL_LEVELS, false, 0, TreeProvider.SELECTNODES_REQUIRED_COLUMNS + ",NodeParentID, DocumentName, NodeAliasPath, NodeLinkedNodeID"));
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Returns URL for the inner IFrame.
    /// </summary>
    private string GetFrameUrl()
    {
        string url = ResolveUrl("~/CMSModules/Content/Controls/Dialogs/Properties/DocCopyMoveProperites.aspx");
        url = UrlHelper.AddParameterToUrl(url, "hash", QueryHelper.GetHash(url));
        return url;
    }


    /// <summary>
    /// Returns URL for the inner IFrame.
    /// </summary>
    private string GetFrameUrl(object nodeIds, object parentAlias, object whereCondition, object targetId, object ext, object aliasPath, object multiple, object action)
    {
        string frameUrl = GetFrameUrl();
        int target = ValidationHelper.GetInteger(targetId, 0);
        frameUrl = UrlHelper.RemoveParameterFromUrl(frameUrl, "hash");
        frameUrl += "?sourcenodeids=" + nodeIds + "&parentAlias=" + parentAlias + "&where=" + whereCondition + "&multiple=" + multiple + "&targetid=" + targetId +
           "&previewext=" + ext + "&aliasPath=" + aliasPath + "&output=" + CurrentAction;
        if ((action != null) && (target > 0))
        {
            frameUrl = UrlHelper.AddParameterToUrl(frameUrl, "action", action.ToString());
        }
        frameUrl = UrlHelper.AddParameterToUrl(frameUrl, "hash", QueryHelper.GetHash(frameUrl));
        return frameUrl;
    }

    #endregion


    #region "Overriden methods"

    public override void LoadSelectedItems(MediaItem item, Hashtable properties)
    {
        if (properties == null)
        {
            properties = new Hashtable();
        }

        string url = item.Url;

        if (item.MediaType == MediaTypeEnum.Flash)
        {
            url = UrlHelper.UpdateParameterInUrl(url, "ext", "." + item.Extension.TrimStart('.'));
        }

        properties[DialogParameters.DOC_NODEALIASPATH] = item.AliasPath;
        properties[DialogParameters.DOC_TARGETNODEID] = item.NodeID;
        properties[DialogParameters.URL_EXT] = item.Extension;
        properties[DialogParameters.URL_URL] = url;

        TargetNodeID = ValidationHelper.GetInteger(item.NodeID, 0);
        PreviewURL = url;
        PreviewExt = item.Extension;

        LoadProperties(properties);
    }


    /// <summary>
    /// Loads the properites into control
    /// </summary>
    /// <param name="properties">Collection with properties</param>
    public override void LoadItemProperties(Hashtable properties)
    {
        LoadProperties(properties);
    }


    /// <summary>
    /// Loads the properites into control
    /// </summary>
    /// <param name="properties">Collection with properties</param>
    public override void LoadProperties(Hashtable properties)
    {
        if (properties != null)
        {
            innerFrame.Attributes.Add("src", GetFrameUrl(
                QueryHelper.GetString("sourcenodeids", string.Empty),
                QueryHelper.GetString("parentalias", string.Empty),
                QueryHelper.GetString("where", string.Empty),
                properties[DialogParameters.DOC_TARGETNODEID],
                properties[DialogParameters.URL_EXT],
                properties[DialogParameters.DOC_NODEALIASPATH], QueryHelper.GetBoolean("multiple", false), null)
            );
        }
    }


    /// <summary>
    /// Returns all parameters of the selected item as name – value collection.
    /// </summary>
    public override Hashtable GetItemProperties()
    {
        string url = GetFrameUrl(QueryHelper.GetString("sourcenodeids", string.Empty), QueryHelper.GetString("parentalias", string.Empty), QueryHelper.GetString("where", string.Empty), TargetNodeID, PreviewExt, AliasPath, QueryHelper.GetBoolean("multiple", false), true);
        innerFrame.Attributes.Add("src", url);
        return null;
    }


    /// <summary>
    /// Clears the properties form.
    /// </summary>
    /// <param name="hideProperties"></param>
    public override void ClearProperties(bool hideProperties)
    {
        // Do nothing, inner frame handles it.
    }

    #endregion
}
