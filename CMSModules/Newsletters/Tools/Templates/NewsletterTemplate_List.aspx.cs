using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Generic;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Newsletter;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Templates_NewsletterTemplate_List : CMSNewsletterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.HeaderActions.HelpTopicName = "templates_tab";
        this.CurrentMaster.HeaderActions.HelpName = "helpTopic";

        // Add subscriber link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("NewsletterTemplate_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("NewsletterTemplate_New.aspx?siteid=" + CMSContext.CurrentSite.SiteID);
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Newsletter_EmailTemplate/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.GridView.DataBound += new EventHandler(GridView_DataBound);
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
        UniGrid.WhereCondition = "(TemplateSiteID = " + CMSContext.CurrentSiteID + ")";
    }



    /// <summary>
    /// GridView databound event handler.
    /// </summary>
    void GridView_DataBound(object sender, EventArgs e)
    {
        // Initialize template type column
        for (int i = 0; i < UniGrid.GridView.Rows.Count; i++)
        {
            if (UniGrid.GridView.Rows[i].Cells[2].Text == "U")
            {
                UniGrid.GridView.Rows[i].Cells[2].Text = ResHelper.GetString("NewsletterTemplate_List.Unsubscription");
            }
            else if (UniGrid.GridView.Rows[i].Cells[2].Text == "S")
            {
                UniGrid.GridView.Rows[i].Cells[2].Text = ResHelper.GetString("NewsletterTemplate_List.Subscription");
            }
            else
            {
                UniGrid.GridView.Rows[i].Cells[2].Text = ResHelper.GetString("NewsletterTemplate_List.Issue");
            }
        }
    }


    /// <summary>
    /// Increment counter at the end of string.
    /// </summary>
    /// <param name="s">String</param>
    /// <param name="lpar">Left parathenses</param>
    /// <param name="rpar">Right parathenses</param>
    string Increment(string s, string lpar, string rpar)
    {
        int i = 1;
        s = s.Trim();
        if ((rpar == String.Empty) || s.EndsWith(rpar))
        {
            int leftpar = s.LastIndexOf(lpar);
            if (lpar == rpar)
            {
                leftpar = s.LastIndexOf(lpar, leftpar - 1);
            }

            if (leftpar >= 0)
            {
                i = ValidationHelper.GetSafeInteger(s.Substring(leftpar + lpar.Length, s.Length - leftpar - lpar.Length - rpar.Length), 0);
                // Remove parathenses only if parathenses found
                if (i > 0)
                {
                    s = s.Remove(leftpar);
                }
                i++;
            }
        }

        s += lpar + i + rpar;
        return s;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            // Edit the template
            case "edit":
                UrlHelper.Redirect("NewsletterTemplate_Edit.aspx?templateid=" + Convert.ToString(actionArgument) + "&siteid=" + CMSContext.CurrentSite.SiteID);
                break;
            // Delete the template
            case "delete":
                // Check 'Manage templates' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "managetemplates"))
                {
                    RedirectToCMSDeskAccessDenied("cms.newsletter", "managetemplates");
                }

                string templateId = actionArgument.ToString();

                DataSet newsByEmailtempl = NewsletterProvider.GetNewsleters("NewsletterTemplateID = " + templateId, null);
                DataSet newsBySubscrTempl = NewsletterProvider.GetNewsleters("NewsletterSubscriptionTemplateID = " + templateId, null);
                DataSet newsByUnsubscrTempl = NewsletterProvider.GetNewsleters("NewsletterUnsubscriptionTemplateID = " + templateId, null);
                if ((DataHelper.DataSourceIsEmpty(newsByEmailtempl)) && (DataHelper.DataSourceIsEmpty(newsBySubscrTempl)) && (DataHelper.DataSourceIsEmpty(newsByUnsubscrTempl)))
                {
                    DataSet newsletterIssues = IssueProvider.GetIssues("IssueTemplateID = " + templateId, null);
                    if (DataHelper.DataSourceIsEmpty(newsletterIssues))
                    {
                        // Delete EmailTemplate object from database
                        EmailTemplateProvider.DeleteEmailTemplate(Convert.ToInt32(actionArgument));
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("NewsletterTemplate_List.TemplateInUseByNewsletterIssue");
                    }
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("NewsletterTemplate_List.TemplateInUseByNewsletter");
                }
                break;
            // Clone the template
            case "clone":
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "managetemplates"))
                {
                    RedirectToCMSDeskAccessDenied("cms.newsletter", "managetemplates");
                }

                int tmpId = ValidationHelper.GetInteger(actionArgument, 0);
                EmailTemplate oldet = EmailTemplateProvider.GetEmailTemplate(tmpId);
                if (oldet != null)
                {
                    EmailTemplate et = new EmailTemplate();
                    et.TemplateBody = oldet.TemplateBody;
                    et.TemplateDisplayName = oldet.TemplateDisplayName;
                    et.TemplateFooter = oldet.TemplateFooter;
                    et.TemplateHeader = oldet.TemplateHeader;
                    et.TemplateID = 0;
                    et.TemplateName = oldet.TemplateName;
                    et.TemplateSiteID = oldet.TemplateSiteID;
                    et.TemplateStylesheetText = oldet.TemplateStylesheetText;
                    et.TemplateType = oldet.TemplateType;

                    string templateName = et.TemplateName;
                    string templateDisplayName = et.TemplateDisplayName;

                    while (EmailTemplateProvider.GetEmailTemplate(templateName, et.TemplateSiteID) != null)
                    {
                        templateName = Increment(templateName, "_", "");
                        templateDisplayName = Increment(templateDisplayName, "(", ")");
                    }

                    et.TemplateName = templateName;
                    et.TemplateDisplayName = templateDisplayName;

                    // Get new ID
                    EmailTemplateProvider.SetEmailTemplate(et);

                    List<Guid> convTable = new List<Guid>();

                    try
                    {
                        MetaFileInfoProvider.CopyMetaFiles(tmpId, et.TemplateID, NewsletterObjectType.NEWSLETTERTEMPLATE, MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE, convTable);
                    }
                    catch (Exception e)
                    {
                        lblError.Visible = true;
                        lblError.Text = e.Message;
                        EmailTemplateProvider.DeleteEmailTemplate(et);
                        return;
                    }

                    for (int i = 0; i < convTable.Count; i += 2)
                    {
                        et.TemplateBody = et.TemplateBody.Replace(convTable[i].ToString(), convTable[i + 1].ToString());
                    }

                    EmailTemplateProvider.SetEmailTemplate(et);
                }
                break;
        }
    }
}

