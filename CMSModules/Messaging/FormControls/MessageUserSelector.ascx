<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MessageUserSelector.ascx.cs"
    Inherits="CMSModules_Messaging_FormControls_MessageUserSelector" %>
<asp:TextBox ID="txtUser" runat="server" EnableViewState="false" CssClass="ToField"
    ReadOnly="true" /><cms:CMSButton ID="btnSelect" runat="server" CssClass="ContentButton"
        EnableViewState="false" />
<asp:HiddenField ID="hiddenField" runat="server" />