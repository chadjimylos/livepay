CREATE TABLE [CMS_AlternativeForm] (
		[FormID]                 int NOT NULL IDENTITY(1, 1),
		[FormDisplayName]        nvarchar(250) NOT NULL,
		[FormName]               nvarchar(250) NOT NULL,
		[FormClassID]            int NOT NULL,
		[FormDefinition]         nvarchar(max) NULL,
		[FormLayout]             nvarchar(max) NULL,
		[FormGUID]               uniqueidentifier NOT NULL,
		[FormLastModified]       datetime NOT NULL,
		[FormCoupledClassID]     int NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_AlternativeForm]
	ADD
	CONSTRAINT [PK_CMS_AlternativeForm]
	PRIMARY KEY
	([FormID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_AlternativeForm]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_AlternativeForm_FormClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_AlternativeForm]
			ADD CONSTRAINT [FK_CMS_AlternativeForm_FormClassID_CMS_Class]
			FOREIGN KEY ([FormClassID]) REFERENCES [CMS_Class] ([ClassID])
END
IF OBJECT_ID(N'[CMS_AlternativeForm]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_AlternativeForm_FormCoupledClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_AlternativeForm]
			ADD CONSTRAINT [FK_CMS_AlternativeForm_FormCoupledClassID_CMS_Class]
			FOREIGN KEY ([FormCoupledClassID]) REFERENCES [CMS_Class] ([ClassID])
END
CREATE INDEX [IX_CMS_AlternativeForm_FormClassID_FormName]
	ON [CMS_AlternativeForm] ([FormClassID], [FormName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_AlternativeForm_FormCoupledClassID]
	ON [CMS_AlternativeForm] ([FormCoupledClassID])
	ON [PRIMARY]
