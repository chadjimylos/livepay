﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FullMediaLibrarySelector.ascx.cs" Inherits="CMSModules_MediaLibrary_FormControls_FullMediaLibrarySelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server" RenderMode="InLine">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" ObjectType="Media.Library" SelectionMode="SingleDropDownList"  AllowEditTextBox="false" />        
    </ContentTemplate>
</cms:CMSUpdatePanel>