CREATE PROCEDURE [Proc_CMS_WebpartCategory_UpdateChildPaths] 
	@OldCategoryPath nvarchar(450), 
    @NewCategoryPath nvarchar(450)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- Update category paths
UPDATE CMS_WebPartCategory SET 
    CategoryPath = @NewCategoryPath + right(CategoryPath, len(CategoryPath) - len(@OldCategoryPath)) 
WHERE
    left(CategoryPath, len(@OldCategoryPath) + 1) = @OldCategoryPath + '/' ; 
END
