<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_Edit_VolumeDiscount_Edit.aspx.cs"
    Inherits="CMSModules_Ecommerce_Content_Product_Product_Edit_VolumeDiscount_Edit"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master"
    Title="Product edit - volume discount edit" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <div class="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <table style="vertical-align: top">
            <tr>
                <td class="FieldLabel" style="height: 32px">
                    <asp:Label runat="server" ID="lblVolumeDiscountMinCount" EnableViewState="false" />
                </td>
                <td style="height: 32px">
                    <asp:TextBox ID="txtVolumeDiscountMinCount" runat="server" CssClass="TextBoxField"
                        MaxLength="9" />&nbsp;&nbsp;
                    <div>
                        <asp:RequiredFieldValidator ID="rfvMinCount" runat="server" ControlToValidate="txtVolumeDiscountMinCount"
                            Display="Dynamic" EnableViewState="false" /><asp:RangeValidator ID="rvMinCount" runat="server"
                                ControlToValidate="txtVolumeDiscountMinCount" MinimumValue="1" Type="Integer"
                                Display="Dynamic" MaximumValue="999999999" EnableViewState="false"></asp:RangeValidator>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblVolumeDiscountValue" EnableViewState="false" />
                </td>
                <td>
                    <asp:RadioButton ID="radDiscountRelative" runat="server" GroupName="group1" />
                    <asp:RadioButton ID="radDiscountAbsolute" runat="server" GroupName="group1" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                </td>
                <td>
                    <asp:TextBox ID="txtVolumeDiscountValue" runat="server" CssClass="TextBoxField" MaxLength="10" />&nbsp;&nbsp;
                    <div>
                        <asp:RequiredFieldValidator ID="rfvDiscountValue" runat="server" ControlToValidate="txtVolumeDiscountValue"
                            Display="Dynamic" EnableViewState="false" /><asp:RangeValidator ID="rvDiscountValue"
                                runat="server" ControlToValidate="txtVolumeDiscountValue" Type="Double" Display="Dynamic"
                                MaximumValue="9999999999" MinimumValue="0" EnableViewState="false"></asp:RangeValidator>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
            CssClass="SubmitButton" />
    </div>
</asp:Content>
