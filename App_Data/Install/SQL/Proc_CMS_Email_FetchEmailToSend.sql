CREATE PROCEDURE [Proc_CMS_Email_FetchEmailToSend] 
	@FetchFailed bit,
	@FetchNew bit,
	@FirstEmailID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @emails TABLE (
		EmailID int
	);
	BEGIN TRANSACTION
	IF @FetchFailed = 1 AND @FetchNew = 1
		BEGIN
			INSERT INTO @emails SELECT TOP 10 EmailID FROM CMS_Email WHERE (EmailID > @FirstEmailID AND EmailStatus = 1) ORDER BY EmailPriority DESC, EmailID;
		END
	ELSE IF @FetchNew = 1
		BEGIN
			INSERT INTO @emails SELECT TOP 10 EmailID FROM CMS_Email WHERE (EmailID > @FirstEmailID AND EmailStatus = 1 AND EmailLastSendResult IS NULL) ORDER BY EmailPriority DESC, EmailID;
		END
	ELSE
		BEGIN
			INSERT INTO @emails SELECT TOP 10 EmailID FROM CMS_Email WHERE (EmailID > @FirstEmailID AND EmailStatus = 1 AND EmailLastSendResult IS NOT NULL) ORDER BY EmailPriority DESC, EmailID;
		END
	
	/* Status: 0 - created; 1 - waiting; 2 - sending; 3 - archived */
	UPDATE CMS_Email SET EmailStatus = 2 WHERE EmailID IN (SELECT EmailID FROM @emails);
	COMMIT TRANSACTION
	SELECT * FROM CMS_Email WHERE EmailID IN (SELECT EmailID FROM @emails);
END
