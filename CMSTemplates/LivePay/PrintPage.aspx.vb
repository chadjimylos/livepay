﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports ExportToPDF
Imports LivePay_ESBBridge
Imports System.Threading
Imports System.Globalization
Imports CMS.UIControls
Imports System.Reflection

Partial Class CMSTemplates_LivePay_PrintPage
    Inherits System.Web.UI.Page

#Region " Properties "


    Protected ReadOnly Property Lang() As String
        Get
            Dim sLang As String = "el-gr"
            If Not String.IsNullOrEmpty(Request("lang")) Then
                sLang = Request("lang")
            End If
            Return sLang
        End Get
    End Property

    Protected ReadOnly Property ExportType() As String
        Get
            Dim sCode As String = String.Empty
            If Not String.IsNullOrEmpty(Request("ExportType")) Then
                sCode = Request("ExportType")
            End If
            Return sCode
        End Get
    End Property

    Protected ReadOnly Property FromPopUpDetails() As String
        Get
            Dim sFromPopUpDetails As String = String.Empty
            If Not String.IsNullOrEmpty(Request("FromPopUpDetails")) Then
                sFromPopUpDetails = Request("FromPopUpDetails")
            End If
            Return sFromPopUpDetails
        End Get
    End Property


    Protected ReadOnly Property PageType() As String
        Get
            Dim sPageType As String = String.Empty
            If Not String.IsNullOrEmpty(Request("PageType")) Then
                sPageType = Request("PageType").ToLower
            End If
            Return sPageType
        End Get
    End Property

    Public ExportToPdf As Boolean = False

    Private FileName As String = String.Empty
    Private TimeoutLivePayBridge As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutLivePayBridge"))
#End Region

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        If Me.ExportToPdf Then
            Using stringWriter As StringWriter = New StringWriter()
                Using htmlWriter As HtmlTextWriter = New HtmlTextWriter(stringWriter)
                    MyBase.Render(htmlWriter)
                    Dim html As String = stringWriter.ToString()
                    Me.ExportHtmlAsPdf(html)
                End Using
            End Using
        Else
            MyBase.Render(writer)
        End If
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Me.Lang)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(Me.Lang)
        MyBase.InitializeCulture()
        tdTransDateTime.InnerHtml = ResHelper.LocalizeString("{$=Ημερομηνία & Ώρα|en-us=Date & Time $}")

        If Me.ExportType = "SrcHisPay" Then
            FileName = "Istoriko_Pliromon"
            divRep1.Visible = True
            GridViewPayments.Columns(0).HeaderText = ResHelper.LocalizeString("{$=Ημερομηνία Συναλλαγής|en-us=Transaction Date$}")
            GridViewPayments.Columns(1).HeaderText = ResHelper.LocalizeString("{$=Πληρωμή προς|en-us=Payment to$}")
            GridViewPayments.Columns(2).HeaderText = ResHelper.LocalizeString("{$=Κάρτα|en-us=Card$}")
            GridViewPayments.Columns(3).HeaderText = ResHelper.LocalizeString("{$=Ποσό(€)|en-us=Amount(€)$}")
            GridViewPayments.Columns(4).HeaderText = ResHelper.LocalizeString("{$=Αποτέλεσμα|en-us=Result$}")
            GridViewPayments.DataSource = Session("GetSrcHisPayDataTable") ', DataTable)
            GridViewPayments.DataBind()

        End If

        If Me.ExportType = "MerchantsSrcHisPay" Then
            FileName = "Istoriko_Pliromon_Emporou"
            divRep2.Visible = True
            GridViewMerchantsPayments.Columns(0).HeaderText = ResHelper.LocalizeString("{$=Αριθμός<br>Πακέτου|en-us=Batch<br>Number$}")
            GridViewMerchantsPayments.Columns(1).HeaderText = ResHelper.LocalizeString("{$=Ημ/νια<br>Κλεισιμάτος<br>Πακέτου|en-us=Batch<br>Submission<br>Date$}")
            GridViewMerchantsPayments.Columns(2).HeaderText = ResHelper.LocalizeString("{$=Κατάσταση<br>Πακέτου|en-us=Batch<br>Status$}")
            GridViewMerchantsPayments.Columns(3).HeaderText = ResHelper.LocalizeString("{$=Κωδικός<br>Συναλλαγής|en-us=Transaction<br>Code$}")
            GridViewMerchantsPayments.Columns(4).HeaderText = ResHelper.LocalizeString("{$=Ημ/νια<br>Συναλλαγής|en-us=Transaction<br>Date$}")
            GridViewMerchantsPayments.Columns(5).HeaderText = ResHelper.LocalizeString("{$=Ονομ/επώνυμο<br>Πελάτη|en-us=Customer<br>FullName$}")
            GridViewMerchantsPayments.Columns(6).HeaderText = ResHelper.LocalizeString("{$=Ποσό(€)|en-us=Amount(€)$}")
            GridViewMerchantsPayments.Columns(7).HeaderText = ResHelper.LocalizeString("{$=Τύπος<br>Συν/γής|en-us=Transaction<br>Type$}")
            GridViewMerchantsPayments.Columns(8).HeaderText = ResHelper.LocalizeString("{$=Κατάσταση<br/>Συναλλαγής|en-us=Transaction<br/>Status$}")
            GridViewMerchantsPayments.DataSource = Session("Merchants_GetSrcHisPayDataTable")
            GridViewMerchantsPayments.DataBind()

        End If

        If Me.ExportType = "MerchantTransDetails" Then

            FileName = "PliforiesSinalagis"
            DivMerchantTransDetails.Visible = True
            Dim TransID As String = Session("TransDetails_TransIDForExport")
            Dim TransSystem As LivePay_ESBBridge.SystemEnum = DirectCast(Convert.ToInt32(Session("TransDetails_SystemForExport")), LivePay_ESBBridge.SystemEnum)
            SetTransDetailsData(TransID, TransSystem)

        End If

        'Response.Write(Me.ExportType)
        'Response.End()
        If Me.ExportType = "TransReceipt" AndAlso Session("TransReceipt_SystemForExport").ToString() <> String.Empty Then

            FileName = "PliforiesSinalagis"
            DivTransReceipt.Visible = True
            Dim TransID As String = Session("TransReceipt_TransIDForExport")

            'Dim TransSystem As LivePay_ESBBridge.SystemEnum = DirectCast(Convert.ToInt32(Session("TransReceipt_SystemForExport")), LivePay_ESBBridge.SystemEnum)
            Dim TransSystem As LivePay_ESBBridge.SystemEnum = DirectCast(Session("TransReceipt_SystemForExport"), LivePay_ESBBridge.SystemEnum)
            SetTransReceiptData(TransID, TransSystem)
        End If

        If ((Session("AcknowledgementTransaction_TransIDForExport") IsNot Nothing) AndAlso (Session("AcknowledgementTransaction_TransIDForExport").ToString <> String.Empty)) Then
            td_AcknowledgementTransaction.InnerHtml = "<br/>" + Session("AcknowledgementTransaction_TransIDForExport").ToString + "<br/>"
        End If

    End Sub

#Region " GridViewMerchantsPayments "

    Function GetTransactionStatusDescr(ByVal status As LivePay_ESBBridge.TxnResultEnum) As String
        'Failed  1, Reversal 2, Successful 0, Successful_Uploaded 4, Voided 3, Voided_Uploaded 5
        Select Case status
            Case 0 : Return "Επιτυχημένη"
            Case 1 : Return "Αποτυχημένη"
            Case 2 : Return "Reversal"
            Case 3 : Return "Ακυρωμένη"
            Case 4 : Return "Successful_Uploaded"
            Case 5 : Return "Voided_Uploaded"
        End Select
        Return status
    End Function

    Sub GridViewMerchantsPayments_DataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridViewMerchantsPayments.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim amount As String = CType(e.Row.FindControl("lblAmount"), Label).Text
            amount = Replace(amount, ".", ",")
            If amount.Split(",").Length > 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
            ElseIf amount.Split(",").Length = 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & ",00"
            Else
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount
            End If
        End If
    End Sub

#End Region

    Protected Sub GridViewPayments_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewPayments.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim amount As String = CType(e.Row.FindControl("lblAmount"), Label).Text
            amount = Replace(amount, ".", ",")
            If amount.Split(",").Length > 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
            ElseIf amount.Split(",").Length = 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & ",00"
            Else
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount
            End If
        End If
    End Sub

#Region " TransReceipt "

    Private Sub SetTransReceiptData(ByVal TransID As String, ByVal TransSystem As LivePay_ESBBridge.SystemEnum)
        If TransID <> String.Empty Then

            Dim MerchantName As String = String.Empty
            Dim w As New LivePay_ESBBridge.Bridge
            w.Timeout = TimeoutLivePayBridge
            Dim TransResponse As TransactionDetailsResponse = w.GetTransactionDetails(TransID, TransSystem)
            Dim TransInfo As TransactionDetailsInfo = TransResponse.TransactionDetails

            If Not IsNothing(TransInfo) Then


                'Response.Write(TransInfo.transactionId)
                'Response.End()

                If TransSystem = LivePay_ESBBridge.SystemEnum.Public Then
                    tdDtm.InnerHtml = TransInfo.transactionDate.ToString("d", CultureInfo.CurrentCulture)
                    'tdPayCode.InnerHtml = TransInfo.info1
                    tdPayCode.InnerHtml = "---"
                    JsScript("$('[id$=tdPayCode]').parent().hide();")
                    tdTransDateTime.InnerHtml = ResHelper.LocalizeString("{$=Ημερομηνία|en-us=Date$}")
                Else
                    tdDtm.InnerHtml = TransInfo.transactionDate
                    tdPayCode.InnerHtml = TransInfo.transactionId
                    tdTransDateTime.InnerHtml = ResHelper.LocalizeString("{$=Ημερομηνία & Ώρα|en-us=Date & Time$}")

                    'Get stixia timologisis
                    Dim Invoice As LivePay.Invoice
                    Invoice = LivePay.Invoice.GetInvoice(TransID)

                    If Not Invoice Is Nothing Then
                        ph_Receipt_Invoice.Visible = True
                        tdRecCompanyName.InnerText = Invoice.CompanyName
                        tdRecProfession.InnerText = Invoice.Profession
                        tdRecAddress.InnerText = Invoice.Address
                        tdRecPostalCode.InnerText = Invoice.PostalCode
                        tdRecCity.InnerText = Invoice.City
                        tdRecVATNumber.InnerText = Invoice.VATNumber
                        tdRecTaxOffice.InnerText = Invoice.TaxOffice
                    Else
                        ph_Receipt_Invoice.Visible = False
                    End If
                End If

                'tdPayCode.InnerHtml = TransInfo.transactionId
                tdCustCode.InnerHtml = TransInfo.customerId
                If TransInfo.installments > 0 Then
                    plcInstallments.Visible = True
                    tdInstallments_Receipt.InnerHtml = TransInfo.installments
                End If

                If TransInfo.commission <> 0 Then
                    plc_Commission.Visible = True
                    tdCommission_Receipt.InnerHtml = TransInfo.commission.ToString("#0.00").PadLeft(3).Replace(".", ",")
                End If

                Dim amount As String = Replace(TransInfo.transactionAmount, ".", ",")
                If amount.Split(",").Length > 1 Then
                    tdPrice_Receipt.InnerHtml = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
                ElseIf amount.Split(",").Length = 1 Then
                    tdPrice_Receipt.InnerHtml = amount & ",00"
                Else
                    tdPrice_Receipt.InnerHtml = amount
                End If

                Dim CardNo As String = Replace(TransInfo.cardNumber, " ", String.Empty)
                tdCardNo_Receipt.InnerHtml = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))


                Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(TransInfo.merchantId, CultureInfo.CurrentCulture.Name)
                Dim LivePay_MerchantID As Integer = 0
                If dtMerc.Rows.Count > 0 Then
                    LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
                End If

                Dim dt As DataTable = DBConnection.GetMerchantByID(LivePay_MerchantID, CultureInfo.CurrentCulture.Name)
                If dt.Rows.Count > 0 Then
                    MerchantName = dt.Rows(0)("DiscreetTitle").ToString
                    Me.imgLogo.ImageUrl = "/getattachment/" & dt.Rows(0)("Logo").ToString & "/logo.aspx"
                    If dt.Rows(0)("logo").ToString.Length = 0 Then
                        Me.imgLogo.Visible = False
                    End If
                End If
                TdCompanyTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=Πληροφορίες συναλλαγής - ", MerchantName, "|en-us=Transaction Information - ", MerchantName, "$}")) ' MerchantName
                tdCompPay.InnerHtml = MerchantName
                CreateCustomFields_TransReceipt(TransInfo.merchantId, TransInfo)
            End If
        End If
    End Sub

    Private Sub CreateCustomFields_TransReceipt(ByVal MerchantID As String, ByVal TransInfo As TransactionDetailsInfo)
        Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(MerchantID, CultureInfo.CurrentCulture.Name)
        Dim LivePay_MerchantID As Integer = 0
        If dtMerc.Rows.Count > 0 Then
            LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
        End If

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(LivePay_MerchantID)
        Dim dt As DataTable = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim CustomTR As New HtmlTableRow
            Dim CustomTDTitle As New HtmlTableCell
            Dim CustomTDValue As New HtmlTableCell
            CustomTDTitle.Attributes.Add("class", "PopTransRecTitle")
            CustomTDValue.Attributes.Add("class", "PopTransDetValue")

            CustomTDTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-Us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))

            Select Case i
                Case 0
                    CustomTDValue.InnerHtml = TransInfo.info1
                Case 1
                    CustomTDValue.InnerHtml = TransInfo.info2
                Case 2
                    CustomTDValue.InnerHtml = TransInfo.info3
                Case 3
                    CustomTDValue.InnerHtml = TransInfo.info4
                Case 4
                    CustomTDValue.InnerHtml = TransInfo.info5
                Case 5
                    CustomTDValue.InnerHtml = TransInfo.info6
                Case 6
                    CustomTDValue.InnerHtml = TransInfo.info7
                Case 7
                    CustomTDValue.InnerHtml = TransInfo.info8
                Case 8
                    CustomTDValue.InnerHtml = TransInfo.info9
                Case 9
                    CustomTDValue.InnerHtml = TransInfo.info10
            End Select

            CustomTR.Controls.Add(CustomTDTitle)
            CustomTR.Controls.Add(CustomTDValue)
            tblCustomFields_Receipt.Controls.Add(CustomTR)
        Next
    End Sub

#End Region

#Region " TransDetails "

    Private Sub SetTransDetailsData(ByVal TransID As String, ByVal TransSystem As LivePay_ESBBridge.SystemEnum)

        If TransID <> String.Empty Then
            Dim w As New LivePay_ESBBridge.Bridge
            w.Timeout = TimeoutLivePayBridge
            Dim TransResponse As TransactionDetailsResponse = w.GetTransactionDetails(TransID, TransSystem)
            Dim TransInfo As TransactionDetailsInfo = TransResponse.TransactionDetails
            If Not IsNothing(TransInfo) Then

                If Me.PageType = "cancel" Then
                    '---- Hide Batch Fields
                    FieldBatchNo.Visible = False
                    FieldBatchCloseDtm.Visible = False
                    FieldBatchStatus.Visible = False
                    FieldCancelDTM.Visible = True
                    tdCancelDTM.InnerHtml = Now.Date.ToShortDateString
                    '---- Hide Batch Fields
                End If

                'Get stixia timologisis
                Dim Invoice As LivePay.Invoice
                Invoice = LivePay.Invoice.GetInvoice(TransID)

                If Not Invoice Is Nothing Then
                    ph_Invoice.Visible = True
                    tdCompanyName.InnerText = Invoice.CompanyName
                    tdProfession.InnerText = Invoice.Profession
                    tdAddress.InnerText = Invoice.Address
                    tdPostalCode.InnerText = Invoice.PostalCode
                    tdCity.InnerText = Invoice.City
                    tdVATNumber.InnerText = Invoice.VATNumber
                    tdTaxOffice.InnerText = Invoice.TaxOffice
                Else
                    ph_Invoice.Visible = False
                End If

                Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(TransInfo.merchantId, CultureInfo.CurrentCulture.Name)
                Dim CompanyName As String = String.Empty
                If dtMerc.Rows.Count > 0 Then
                    CompanyName = dtMerc.Rows(0)("DiscreetTitle").ToString
                End If
                PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Πληροφορίες Συναλλαγής|en-us=Transaction Information$}") & " - " & CompanyName
                tdBoxNo.InnerHtml = TransInfo.batchId
                tdBoxCloseDtm.InnerHtml = TransInfo.batchDate
                tdBoxStatus.InnerHtml = IIf(TransInfo.batchClosed, ResHelper.LocalizeString("{$=Κλειστό|en-us=Closed$}"), ResHelper.LocalizeString("{$=Ανοιχτό|en-us=Open$}"))

                If TransSystem = LivePay_ESBBridge.SystemEnum.Public Then
                    tdTransDtm.InnerHtml = TransInfo.transactionDate.ToString("d", CultureInfo.CurrentCulture)
                    tdTransCode.InnerHtml = TransInfo.info1
                Else
                    tdTransDtm.InnerHtml = TransInfo.transactionDate
                    tdTransCode.InnerHtml = TransInfo.transactionId
                End If


                tdCustFullName.InnerHtml = TransInfo.customerName

                If Me.FromPopUpDetails = "1" Then
                    AuthorisationCodeTR.Visible = True
                    tdAuthorisationCode.InnerHtml = TransInfo.appcode
                Else
                    AuthorisationCodeTR.Visible = False
                End If

                Dim amount As String = Replace(TransInfo.transactionAmount, ".", ",")
                If amount.Split(",").Length > 1 Then
                    tdPrice.InnerHtml = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
                ElseIf amount.Split(",").Length = 1 Then
                    tdPrice.InnerHtml = amount & ",00"
                Else
                    tdPrice.InnerHtml = amount
                End If


                TRPrice.Style.Add("display", "")
                tdTransStatus.InnerHtml = TransInfo.transactionStatus.ToString
                tdCardType.InnerHtml = IIf(TransInfo.cardType = 0, "Visa", "Master")
                Dim CardNo As String = Replace(TransInfo.cardNumber, " ", String.Empty)
                tdCardNo.InnerHtml = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                tdEmail.InnerHtml = TransInfo.customerEmail
                tdContactPhone.InnerHtml = TransInfo.customerTelephone
                tdNotes.InnerHtml = TransInfo.customerComments
                CreateCustomFields_TransDetails(TransInfo.merchantId, TransInfo)
            End If
        End If
    End Sub

    Private Sub CreateCustomFields_TransDetails(ByVal MerchantID As String, ByVal TransInfo As TransactionDetailsInfo)
        Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(MerchantID, CultureInfo.CurrentCulture.Name)
        Dim LivePay_MerchantID As Integer = 0
        If dtMerc.Rows.Count > 0 Then
            LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
        End If

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(LivePay_MerchantID)
        Dim dt As DataTable = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim CustomTR As New HtmlTableRow
            Dim CustomTDTitle As New HtmlTableCell
            Dim CustomTDValue As New HtmlTableCell
            CustomTDTitle.Attributes.Add("class", "PopTransDetTitle2")
            'With CustomTDTitle.Style
            '    .Add("width", "90px")
            '    .Add("text-align", "right")
            '    .Add("color", "#43474a")
            '    .Add("font-size", "12px")
            '    .Add("font-family", "tahoma")
            '    .Add("line-height", "25px")
            'End With

            'padding-left:15px;font-weight:bold;
            CustomTDValue.Attributes.Add("class", "PopTransDetValue")
            'With CustomTDValue.Style
            '    .Add("width", "334px")
            '    .Add("color", "#43474a")
            '    .Add("font-size", "12px")
            '    .Add("font-family", "tahoma")
            '    .Add("line-height", "25px")
            '    .Add("font-weight", "bold")
            '    .Add("padding-left", "15px")
            'End With

            CustomTDTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldGr").ToString, "$}")) 'NameFieldGEn Na to allakso
            Select Case i
                Case 0
                    CustomTDValue.InnerHtml = TransInfo.info1
                Case 1
                    CustomTDValue.InnerHtml = TransInfo.info2
                Case 2
                    CustomTDValue.InnerHtml = TransInfo.info3
                Case 3
                    CustomTDValue.InnerHtml = TransInfo.info4
                Case 4
                    CustomTDValue.InnerHtml = TransInfo.info5
                Case 5
                    CustomTDValue.InnerHtml = TransInfo.info6
                Case 6
                    CustomTDValue.InnerHtml = TransInfo.info7
                Case 7
                    CustomTDValue.InnerHtml = TransInfo.info8
                Case 8
                    CustomTDValue.InnerHtml = TransInfo.info9
                Case 9
                    CustomTDValue.InnerHtml = TransInfo.info10
            End Select

            CustomTR.Controls.Add(CustomTDTitle)
            CustomTR.Controls.Add(CustomTDValue)
            tblCustomFields.Controls.Add(CustomTR)
        Next
    End Sub

#End Region


    Private Sub ExportHtmlAsPdf(ByVal html As String)
        Dim b() As Byte = Me.GetPdfBytes(html)
        With Response
            .Clear()
            .ContentType = "application/octet-stream"
            .AddHeader("content-disposition", _
              String.Format("attachment; filename={0}_{1}.pdf", FileName & " -", DateTime.Now.ToString("ddMMyyyyHHmmss")))
            .OutputStream.Write(b, 0, b.Length)
            .End()
        End With
    End Sub

    Private Function GetPdfBytes(ByVal html As String) As Byte()
        Dim ExpToPDF As New ExportToPDF

        Return Global.ExportToPDF.CreateFromHtmlString(html)
    End Function

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

End Class
