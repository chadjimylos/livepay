using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.PortalControls;
using CMS.UIControls;

public partial class CMSModules_Widgets_LiveDialogs_WidgetProperties_Buttons : LivePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // set button text
        btnOk.Text = ResHelper.GetString("general.ok");
        btnApply.Text = ResHelper.GetString("general.apply");
        btnCancel.Text = ResHelper.GetString("general.cancel");

        this.btnCancel.OnClientClick = "Close(); return false;";
        this.btnApply.OnClientClick = "Apply(); return false;";
        this.btnOk.OnClientClick = "Save(); return false;";

        RegisterDialogCSSLink();
    }
}
