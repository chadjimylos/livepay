using System;
using System.Web;
using System.IO;
using System.Web.UI.WebControls;
using System.Security.Principal;
using System.Collections;
using System.Data;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.TreeEngine;
using CMS.CMSImportExport;
using CMS.CMSHelper;
using CMS.EventLog;
using CMS.UIControls;
using CMS.PortalEngine;
using CMS.ExtendedControls;

public partial class CMSModules_ImportExport_SiteManager_ExportObject : CMSModalSiteManagerPage
{
    #region "Variables"

    protected string codeName = null;
    protected string exportObjectDisplayName = null;
    protected string targetUrl = null;
    protected bool allowDependent = false;
    protected bool siteObject = false;
    protected int siteId = 0;
    protected int objectId = 0;
    protected string objectType = string.Empty;

    protected IInfoObject infoObj = null;
    protected IInfoObject exportObj = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Export process GUID
    /// </summary>
    public Guid ProcessGUID
    {
        get
        {
            if (ViewState["ProcessGUID"] == null)
            {
                ViewState["ProcessGUID"] = Guid.NewGuid();
            }

            return ValidationHelper.GetGuid(ViewState["ProcessGUID"], Guid.Empty);
        }
    }


    /// <summary>
    /// Persistent settings key
    /// </summary>
    public string PersistentSettingsKey
    {
        get
        {
            return "ExportObject_" + ProcessGUID + "_Settings";
        }
    }


    /// <summary>
    /// Export settings stored in viewstate
    /// </summary>
    public SiteExportSettings ExportSettings
    {
        get
        {
            SiteExportSettings settings = (SiteExportSettings)PersistentStorageHelper.GetValue(PersistentSettingsKey);
            if (settings == null)
            {
                throw new Exception("[ExportObject.ExportSettings]: Export settings has been lost.");
            }
            return settings;
        }
        set
        {
            PersistentStorageHelper.SetValue(PersistentSettingsKey, value);
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register script for pendingCallbacks repair
        ScriptHelper.FixPendingCallbacks(this.Page);

        // Async control events binding
        ucAsyncControl.OnFinished += ucAsyncControl_OnFinished;
        ucAsyncControl.OnError += ucAsyncControl_OnError;

        if (!IsCallback)
        {
            try
            {
                // Delete temporary files
                ExportProvider.DeleteTemporaryFiles();
            }
            catch (Exception ex)
            {
                DisplayError(ex);
            }

            this.CurrentMaster.Title.TitleText = ResHelper.GetString("ExportObject.Title");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_ImportExport/exportobject.png");

            // Display BETA warning
            lblBeta.Visible = CMSContext.IsBetaVersion();
            lblBeta.Text = string.Format(ResHelper.GetString("export.BETAwarning"), CMSContext.SYSTEM_VERSION);

            // Get data from parameters
            siteId = ValidationHelper.GetInteger(Request.QueryString["siteId"], 0);
            objectId = ValidationHelper.GetInteger(Request.QueryString["objectId"], 0);
            objectType = ValidationHelper.GetString(Request.QueryString["objectType"], "");

            // Get the object
            infoObj = CMSObjectHelper.GetObject(objectType);
            if (infoObj == null)
            {
                plcExportDetails.Visible = false;
                lblIntro.Text = ResHelper.GetString("ExportObject.ObjectTypeNotFound");
                lblIntro.CssClass = "ErrorLabel";
                return;
            }

            // Get exported object
            exportObj = infoObj.GetObject(objectId);
            if (exportObj == null)
            {
                plcExportDetails.Visible = false;
                lblIntro.Text = ResHelper.GetString("ExportObject.ObjectNotFound");
                lblIntro.CssClass = "ErrorLabel";
                return;
            }

            // Store display name
            exportObjectDisplayName = exportObj.ObjectDisplayName;
            codeName = exportObj.ObjectCodeName;
            lblIntro.Text = string.Format(ResHelper.GetString("ExportObject.Intro"), exportObjectDisplayName);
            btnOk.Click += btnOk_Click;

            if (!RequestHelper.IsPostBack())
            {
                lblIntro.Visible = true;
                lblFileName.Visible = true;
                btnOk.Text = ResHelper.GetString("General.export");
                btnCancel.Text = ResHelper.GetString("General.Close");

                txtFileName.Text = objectType.Replace(".", "_") + "_" + codeName.Replace(".", "_") + "_" + DateTime.Now.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("HHmm") + ".zip";
            }

            targetUrl = ResolveUrl(ImportExportHelper.EXPORT_RELATIVE_PATH.TrimEnd('/') + "/" + txtFileName.Text);
        }
    }


    private void DisplayError(Exception ex)
    {
        pnlProgress.Visible = false;
        btnOk.Enabled = false;
        pnlDetails.Visible = false;
        pnlContent.Visible = true;

        string displayName = null;
        if (exportObj != null)
        {
            displayName = exportObj.ObjectDisplayName;
        }
        lblResult.Text = string.Format(ResHelper.GetString("ExportObject.Error"), displayName, ex.Message);
        lblResult.ToolTip = EventLogProvider.GetExceptionLogMessage(ex);
        lblResult.CssClass = "ErrorLabel";

        EventLogProvider ev = new EventLogProvider();
        ev.LogEvent("Export", "ExportObject", ex);
    }


    void btnOk_Click(object sender, EventArgs e)
    {
        siteObject = (exportObj.ObjectSiteID > 0);

        // Prepare the settings
        ExportSettings = new SiteExportSettings(CMSContext.CurrentUser);

        ExportSettings.WebsitePath = Server.MapPath("~/");
        ExportSettings.TargetPath = Server.MapPath(ImportExportHelper.EXPORT_RELATIVE_PATH);

        string result = ImportExportHelper.ValidateExportFileName(ExportSettings, txtFileName.Text);

        // Filename is valid
        if (!string.IsNullOrEmpty(result))
        {
            lblError.Text = result;
            // Display error
        }
        else
        {
            txtFileName.Text = txtFileName.Text.Trim();

            // Add extension
            if (Path.GetExtension(txtFileName.Text).ToLower() != ".zip")
            {
                txtFileName.Text = txtFileName.Text.TrimEnd('.') + ".zip";
            }

            // Set the filename
            ExportSettings.TargetFileName = txtFileName.Text;
            lblProgress.Text = string.Format(ResHelper.GetString("ExportObject.ExportProgress"), exportObjectDisplayName);

            pnlContent.Visible = false;
            pnlDetails.Visible = false;
            btnOk.Enabled = false;
            pnlProgress.Visible = true;

            try
            {
                // Export the data
                ltlScript.Text = ScriptHelper.GetScript("StartTimer();");
                ucAsyncControl.RunAsync(ExportSingleObject, WindowsIdentity.GetCurrent());
            }
            catch (Exception ex)
            {
                DisplayError(ex);
            }
        }
    }


    private void GetInheritedWebParts(IInfoObject infoObj, ArrayList webparts)
    {
        int parentWebpartId = ValidationHelper.GetInteger(infoObj.GetValue("WebPartParentID"), 0);
        if (parentWebpartId != 0)
        {
            WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(parentWebpartId);
            if (wpi != null)
            {
                webparts.Add(wpi.WebPartName);
                GetInheritedWebParts(wpi, webparts);
            }
        }
    }


    private void GetSavedReports(IInfoObject infoObj, ArrayList reports)
    {
        DataSet dsSavedReports = ModuleCommands.ReportingGetSavedReports("SavedReportReportID = " + infoObj.ObjectID, null, 0, "SavedReportGUID");
        if (!DataHelper.DataSourceIsEmpty(dsSavedReports))
        {
            foreach (DataRow dr in dsSavedReports.Tables[0].Rows)
            {
                reports.Add(((Guid)dr["SavedReportGUID"]).ToString());
            }
        }
    }


    private void GetDependantWebPart(IInfoObject infoObj, ArrayList webparts)
    {
        int webpartId = ValidationHelper.GetInteger(infoObj.GetValue("WidgetWebPartID"), 0);
        if (webpartId != 0)
        {
            WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webpartId);
            if (wpi != null)
            {
                webparts.Add(wpi.WebPartName);
                GetInheritedWebParts(wpi, webparts);
            }
        }
    }


    // Export object
    private void ExportSingleObject(object parameter)
    {
        // Set export to single selected object
        ExportSettings.DefaultProcessObjectType = ProcessObjectEnum.None;

        ExportSettings.LoadDefaultSelection();

        ArrayList selectedObjects = new ArrayList();

        // Select dependant objects
        switch (objectType.ToLower())
        {
            case PortalObjectType.WIDGET:
                ArrayList selectedWebParts = new ArrayList();
                GetDependantWebPart(exportObj, selectedWebParts);
                // Add web parts
                if (selectedWebParts.Count != 0)
                {
                    ExportSettings.SetObjectsProcessType(ProcessObjectEnum.Selected, PortalObjectType.WEBPART, false);
                    ExportSettings.SetSelectedObjects(selectedWebParts, PortalObjectType.WEBPART, false);
                }
                break;

            case PredefinedObjectType.REPORT:
                ArrayList selectedReports = new ArrayList();
                GetSavedReports(exportObj, selectedReports);
                // Add saved reports
                if (selectedReports.Count != 0)
                {
                    ExportSettings.SetObjectsProcessType(ProcessObjectEnum.Selected, PredefinedObjectType.REPORTSAVEDREPORT, false);
                    ExportSettings.SetSelectedObjects(selectedReports, PredefinedObjectType.REPORTSAVEDREPORT, false);
                }
                break;

            case PortalObjectType.WEBPART:
                // Web part
                GetInheritedWebParts(exportObj, selectedObjects);
                break;

            case PortalObjectType.PAGETEMPLATE:
                // Page template
                ExportSettings.SetObjectsProcessType(ProcessObjectEnum.Selected, PortalObjectType.PAGETEMPLATESCOPE, false);
                break;
        }

        // Add current object
        selectedObjects.Add(codeName);

        ExportSettings.SetObjectsProcessType(ProcessObjectEnum.Selected, objectType, siteObject);
        ExportSettings.SetSelectedObjects(selectedObjects, objectType, siteObject);

        // Single object export
        ExportSettings.SetInfo(ImportExportHelper.INFO_SINGLE_OBJECT, true);

        // Export of site object, include siteinfo
        if (siteId > 0)
        {
            ExportSettings.SiteId = siteId;
            ExportSettings.SetObjectsProcessType(ProcessObjectEnum.All, SiteObjectType.SITE, true);
            ExportSettings.SetObjectsProcessType(ProcessObjectEnum.None, TreeObjectType.DOCUMENT, true);
        }

        ExportSettings.SetSettings(ImportExportHelper.SETTINGS_GLOBAL_FOLDERS, false);
        ExportSettings.SetSettings(ImportExportHelper.SETTINGS_SITE_FOLDERS, false);
        ExportSettings.SetSettings(ImportExportHelper.SETTINGS_TASKS, false);
        ExportSettings.SetSettings(ImportExportHelper.SETTINGS_COPY_ASPX_TEMPLATES_FOLDER, false);

        // Export object
        ExportProvider.ExportObjectsData(ExportSettings);
    }


    void ucAsyncControl_OnError(object sender, EventArgs e)
    {
        ltlScript.Text += ScriptHelper.GetScript("StopTimer();");
        Exception ex = ((CMSAdminControls_AsyncControl)sender).Worker.LastException;

        DisplayError(ex);
    }


    void ucAsyncControl_OnFinished(object sender, EventArgs e)
    {
        ltlScript.Text += ScriptHelper.GetScript("StopTimer();");
        pnlProgress.Visible = false;
        btnOk.Visible = false;
        pnlContent.Visible = true;
        lblResult.Text = string.Format(ResHelper.GetString("ExportObject.lblResult"), exportObjectDisplayName, targetUrl);
        lblResult.CssClass = "ContentLabel";
        lnkDownload.NavigateUrl = targetUrl;
        lnkDownload.Text = ResHelper.GetString("ExportObject.Download");
        lnkDownload.Visible = true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        lblResult.Visible = (lblResult.Text != "");
        base.OnPreRender(e);
    }
}
