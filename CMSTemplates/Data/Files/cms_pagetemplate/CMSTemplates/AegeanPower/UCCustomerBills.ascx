﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCCustomerBills.ascx.vb" Inherits="CMSTemplates_AegeanPower_UCCustomerBills" %>
<style type="text/css">
.BillsGvShortLnks_Bold{font-family:Tahoma;font-size:11px;color:white;line-height:19px;text-decoration:none;font-weight:bold;}
.BillsGvShortLnks{font-family:Tahoma;font-size:11px;color:white;line-height:19px;text-decoration:none}
.BillsProvNumLbl{border:0px;background-color:transparent;width:80px;font-size:11px;color:#004990;text-align:center}

.BillsGv_Title{padding-left:5px;text-align:left;font-size:11px;color:#004990;font-weight:normal;background-color:#e3f2f7;height:20px;border-right:1px solid white}
.BillsGv_Title_Last{padding-left:5px;text-align:left;font-size:11px;color:#004990;font-weight:normal;background-color:#e3f2f7;height:20px;}

.BillsGv_RowA{background-color:#f7f9f9;height:33px;}
.BillsGv_RowA td{border-right:1px solid white;padding-left:5px;text-align:left;font-size:11px;color:#004990;font-weight:normal;}
.BillsGv_RowA_Last{height:33px;}
.BillsGv_RowA_Last td{border-right:0px;padding-left:5px;text-align:left;font-size:11px;color:#004990;font-weight:normal;}
.BillsGv_RowB{background-color:#ffffff;height:33px;}
.BillsGv_RowB td{border-right:1px solid white;padding-left:5px;text-align:left;font-size:11px;color:#004990;font-weight:normal;}


.BillsGv_RowA .BillsGv_RowA_Last{border-right:0px;}

.BillGv_Price{color:#7ac142;font-weight:bold;font-size:11px;font-family:Tahoma}
.BillGv_ExpDate{color:#2c2c2c}
.BillViewTypeAllDiv{float:left;padding-left:10px;width:83px}
.BillViewTypeAllDivSel{float:left;padding-left:10px;width:73px}

.BillViewTypeAllPointer{float:left;padding-top:8px;width:38px}
.BillViewTypeLastDiv{float:left;padding-right:5px;width:108px}
.BillViewTypeLastDivSel{float:left;padding-right:5px;width:118px}
.BillViewTypeLastPointer{float:left;padding-top:8px;width:46px}


.Bill_GvPagerPointLeft{padding-top:7px;padding-right:3px;padding-left:5px}
.Bill_GvPagerPointRight{padding-top:7px;padding-right:5px;padding-left:3px}
.Width438{width:438px}
.Width458{width:448px}
</style>

<script language="javascript" type="text/javascript">
    function SetViewType(val) {
        document.getElementById('<%=hiddenViewType.ClientID %>').value = val
    }

    function SetYearData(obj) {
        document.getElementById('<%=hiddenYear.ClientID %>').value = obj.innerHTML
    }

</script>
<div style="width:486px;padding-left:12px;">
    <div style="padding-left:2px;font-family:Tahoma;font-weight:bold;font-size:11px;color:#004990;padding-bottom:9px">Λογαριασμoί Ηλεκτρικού Ρεύματος Aegean Power</div>
    <div >
        <div style="background-color:#004990;width:100%;">
            <div class="BillViewTypeAllDiv" id="BillTopGvLnkAllBill" runat="server" ><asp:LinkButton OnClientClick="SetViewType(0)" CssClass="BillsGvShortLnks_Bold" ID="LnkViewAllBills" runat="server"><%=ResHelper.LocalizeString("{$=Πλήρης λίστα|en-us=Πλήρης λίστα$}") %></asp:LinkButton></div>
            <div class="BillViewTypeAllPointer"><img src="/App_Themes/AegeanPower/Images/Bills/GreenPointer.png" /></div>
            <div class="BillViewTypeLastDiv" id="BillTopGvLnkLastBill" runat="server" ><asp:LinkButton OnClientClick="SetViewType(1)" CssClass="BillsGvShortLnks" ID="LnkViewLastBill" runat="server"><%=ResHelper.LocalizeString("{$=Τρέχων Λογαριασμός|en-us=Τρέχων Λογαριασμός$}") %></asp:LinkButton></div>
            <div class="BillViewTypeLastPointer"><img src="/App_Themes/AegeanPower/Images/Bills/GreenPointer.png" /></div>
            
                <div class="Fleft BillsGvShortLnks" ><%=ResHelper.LocalizeString("{$=Αριθμός Παροχής:|en-us=Αριθμός Παροχής:$}") %></div>
                <div class="Fleft" style="padding-top:4px;padding-left:3px">
                    <div  style="background-image:url('/App_Themes/AegeanPower/Images/Bills/ProviderNoTxtBG.png');background-repeat:no-repeat;width:85px;">
                        <div ID="LlbProviderNo" runat="server" class="BillsProvNumLbl"></div>
                    </div>
                    <div id="ProviderRptMain" style="position:absolute;width:100px;background-color:White;height:100px">
                        <asp:Repeater ID="rptProviders" runat="server">
                            <ItemTemplate>
                                <div><asp:LinkButton ID="lnkRptProvider" runat="server" Text='<%#Eval("Provider_Number") %>'></asp:LinkButton></div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                 </div>
                 <div class="Fleft" style="padding-top:4px"><img class="Chant" src="/App_Themes/AegeanPower/Images/Bills/GreenDDLPointer.png" /></div>
                <div class="Clear"></div>
            
        </div>
        <div style="padding-top:1px">
            <asp:GridView ID="GvBills" runat="server" AutoGenerateColumns="false" GridLines="None" >
            <AlternatingRowStyle CssClass="BillsGv_RowB" />
            <RowStyle  CssClass="BillsGv_RowA" />
                <Columns>
                    <asp:TemplateField HeaderText="Ημερομηνία Έκδοσης" HeaderStyle-Width="160px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblIssueDate" runat="server" Text='<%#Eval("IssDate") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ποσό"  HeaderStyle-Width="115px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblAmmount" CssClass="BillGv_Price" runat="server" Text='<%#Eval("Amount") & " &euro;" %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ημ/νία Λήξης" HeaderStyle-Width="120px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblExpDate" CssClass="BillGv_ExpDate" runat="server" Text='<%#Eval("ExpDate") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Επιλογές" ItemStyle-CssClass="BillsGv_RowA_Last" HeaderStyle-Width="67px" HeaderStyle-CssClass="BillsGv_Title_Last">
                        <ItemTemplate>
                           <div>
                              <div class="Fleft"  style="padding-left:10px" ><img src="/App_Themes/AegeanPower/Images/Bills/CardIcon.png" /></div>
                              <div class="Fleft" style="padding-left:10px"><img src="/App_Themes/AegeanPower/Images/Bills/PDFIcon.png" /></div>
                              <div class="Clear"></div>
                           </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </div>
        <div style="background-color:#004990;width:100%;" id="PagerMain" runat="server" visible="false">
            <div id="PagerLeft" runat="server"  visible="false">
                <div class="Fleft Bill_GvPagerPointLeft" ><img src="/App_Themes/AegeanPower/Images/Bills/GreenPointerLeft.png" id="PagerLeftPointer" runat="server" /></div>
                <div class="Fleft Width438"><asp:LinkButton CssClass="BillsGvShortLnks" OnClientClick="SetYearData(this)" ID="PagerlnkLastYear" runat="server" ></asp:LinkButton></div>
            </div>
            <div class="Fleft Width458" id="PagerCenter" runat="server" visible="false">&nbsp;</div>
            <div id="PagerRight" runat="server" visible="false">
                <div class="Fleft"><asp:LinkButton CssClass="BillsGvShortLnks" OnClientClick="SetYearData(this)" ID="PagerLnkNextYear" runat="server" ></asp:LinkButton></div>
                <div class="Fleft Bill_GvPagerPointRight" ><img src="/App_Themes/AegeanPower/Images/Bills/GreenPointer.png" /></div>
            </div> 
            
            <div class="Clear"></div>
        </div>
    </div>
</div>

<asp:TextBox id="hiddenViewType" runat="server" Text="0" ></asp:TextBox>
<asp:TextBox id="hiddenYear" runat="server"  ></asp:TextBox>
<asp:TextBox id="hiddenProviderNumber" runat="server"  ></asp:TextBox>