CREATE TABLE [COM_InternalStatus] (
		[InternalStatusID]               int NOT NULL IDENTITY(1, 1),
		[InternalStatusName]             nvarchar(200) NOT NULL,
		[InternalStatusDisplayName]      nvarchar(200) NOT NULL,
		[InternalStatusEnabled]          bit NOT NULL,
		[InternalStatusGUID]             uniqueidentifier NOT NULL,
		[InternalStatusLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_InternalStatus]
	ADD
	CONSTRAINT [PK_COM_InternalStatus]
	PRIMARY KEY
	NONCLUSTERED
	([InternalStatusID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_InternalStatus_InternalStatusDisplayName_InternalStatusEnabled]
	ON [COM_InternalStatus] ([InternalStatusDisplayName], [InternalStatusEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
