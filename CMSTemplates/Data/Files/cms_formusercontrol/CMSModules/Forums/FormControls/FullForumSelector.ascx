﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FullForumSelector.ascx.cs" Inherits="CMSModules_Forums_FormControls_FullForumSelector" %>
<%@ Register src="~/CMSModules/Forums/FormControls/ForumSelector.ascx" tagname="ForumSelector" tagprefix="cms" %>

<cms:ForumSelector ID="forumSelector" runat="server" DisplayAllForumsOption="true" />