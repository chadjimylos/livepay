<%@ Page Language="C#" AutoEventWireup="true" CodeFile="leftmenu.aspx.cs" Inherits="CMSDesk_MyDesk_leftmenu"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/Tree.master" %>

<%@ Register Src="~/CMSAdminControls/UI/UIProfiles/UIMenu.ascx" TagName="UIMenu"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcTree">
    <cms:UIMenu runat="server" ID="menuElem" ModuleName="CMS.MyDesk" JavaScriptHandler="LoadItem"
        ModuleAvailabilityForSiteRequired="false" />
</asp:Content>