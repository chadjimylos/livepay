CREATE TABLE [CMS_UserCulture] (
		[UserID]        int NOT NULL,
		[CultureID]     int NOT NULL,
		[SiteID]        int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_UserCulture]
	ADD
	CONSTRAINT [PK_CMS_UserCulture]
	PRIMARY KEY
	([UserID], [CultureID], [SiteID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_UserCulture]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Culture]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserCulture_CultureID_CMS_Culture]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserCulture]
			ADD CONSTRAINT [FK_CMS_UserCulture_CultureID_CMS_Culture]
			FOREIGN KEY ([CultureID]) REFERENCES [CMS_Culture] ([CultureID])
END
IF OBJECT_ID(N'[CMS_UserCulture]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserCulture_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserCulture]
			ADD CONSTRAINT [FK_CMS_UserCulture_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[CMS_UserCulture]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserCulture_UserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserCulture]
			ADD CONSTRAINT [FK_CMS_UserCulture_UserID_CMS_User]
			FOREIGN KEY ([UserID]) REFERENCES [CMS_User] ([UserID])
END
