<%@ Control Language="C#" AutoEventWireup="true" CodeFile="USphone.ascx.cs" Inherits="CMSFormControls_Inputs_USphone" %>
(<asp:TextBox runat="server" ID="txt1st" MaxLength="3" Width="25" />)&nbsp;<asp:TextBox
    runat="server" ID="txt2nd" MaxLength="3" Width="25" />-<asp:TextBox runat="server"
        ID="txt3rd" MaxLength="4" Width="30" /><cms:LocalizedLabel ID="lbl2nd" AssociatedControlID="txt2nd"
            EnableViewState="false" runat="server" ResourceString="USPhone.2nd" /><cms:LocalizedLabel ID="lbl3rd"
                AssociatedControlID="txt3rd" EnableViewState="false" ResourceString="USPhone.3rd" runat="server" />

<div class="HideIt">
    <asp:CustomValidator ID="cv_txt1st" IsNumber="yes" 
                        ClientValidationFunction="validateControls" ValidateEmptyText="true" runat="server"   
                        ControlToValidate="txt1st" text="*" ValidationGroup="Tab1_PayForm" IsCustomValidator="CustomCloud_0"/>
</div>