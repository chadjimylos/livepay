using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Web.Security;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.EmailEngine;
using CMS.EventLog;
using CMS.DataEngine;
using CMS.WebAnalytics;
using CMS.LicenseProvider;
using CMS.PortalEngine;
using CMS.SettingsProvider;
using CMS.MembershipProvider;
using CMS.URLRewritingEngine;


public partial class CMSWebParts_Membership_FacebookConnect_FacebookUsersRequiredData : CMSAbstractWebPart
{
    #region "Constants"

    protected const string SESSION_NAME_USERDATA = "facebookid";

    #endregion


    #region "Private variables"

    private string facebookUserId = null;
    private string mDefaultTargetUrl = String.Empty;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether email to user should be sent.
    /// </summary>
    public bool SendWelcomeEmail
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("SendWelcomeEmail"), true);
        }
        set
        {
            this.SetValue("SendWelcomeEmail", value);
        }
    }


    /// <summary>
    /// Gets or sets registration approval page URL.
    /// </summary>
    public string ApprovalPage
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ApprovalPage"), "");
        }
        set
        {
            this.SetValue("ApprovalPage", value);
        }
    }


    /// <summary>
    /// Gets or sets the sender email (from).
    /// </summary>
    public string FromAddress
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("FromAddress"), SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSNoreplyEmailAddress"));
        }
        set
        {
            this.SetValue("FromAddress", value);
        }
    }


    /// <summary>
    /// Gets or sets the recepient email (to).
    /// </summary>
    public string ToAddress
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ToAddress"), SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSAdminEmailAddress"));
        }
        set
        {
            this.SetValue("ToAddress", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether after successful registration is 
    /// notification email sent to the administrator.
    /// </summary>
    public bool NotifyAdministrator
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("NotifyAdministrator"), false);
        }
        set
        {
            this.SetValue("NotifyAdministrator", value);
        }
    }


    /// <summary>
    /// Gets or sets the message which is displayed after successful registration.
    /// </summary>
    public string DisplayMessage
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("DisplayMessage"), "");
        }
        set
        {
            this.SetValue("DisplayMessage", value);
        }
    }


    /// <summary>
    /// Gets or sets the value which enables abitity of new user to set password.
    /// </summary>
    public bool AllowFormsAuthentication
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("AllowFormsAuthentication"), false);
        }
        set
        {
            SetValue("AllowFormsAuthentication", value);
            plcPasswordNew.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which enables abitity join liveid with existing account.
    /// </summary>
    public bool AllowExistingUser
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("AllowExistingUser"), true);
        }
        set
        {
            SetValue("AllowExistingUser", value);
            plcPasswordNew.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets the default target url (redirection when the user is logged in).
    /// </summary>
    public string DefaultTargetUrl
    {
        get
        {
            return ValidationHelper.GetString(GetValue("DefaultTargetUrl"), mDefaultTargetUrl);
        }
        set
        {
            SetValue("DefaultTargetUrl", value);
            mDefaultTargetUrl = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines the behaviour for no Facebook users.
    /// </summary>
    public bool HideForNoFacebookID
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("HideForNoFacebookUserID"), true);
        }
        set
        {
            SetValue("HideForNoFacebookUserID", value);
            this.Visible = !value;
        }
    }

    #endregion


    #region "Conversion properties"

    /// <summary>
    /// Gets or sets the conversion track name used after successful registration.
    /// </summary>
    public string TrackConversionName
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("TrackConversionName"), "");
        }
        set
        {
            if ((value != null) && (value.Length > 400))
            {
                value = value.Substring(0, 400);
            }
            this.SetValue("TrackConversionName", value);
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (!this.StopProcessing)
        {
            string currentSiteName = CMSContext.CurrentSiteName;

            // Get Facebook Connect settings
            string apiKey = SettingsKeyProvider.GetStringValue(currentSiteName + ".CMSFacebookConnectApiKey");
            string secret = SettingsKeyProvider.GetStringValue(currentSiteName + ".CMSFacebookApplicationSecret");

            if (SettingsKeyProvider.GetBoolValue(currentSiteName + ".CMSEnableFacebookConnect") &&
                !String.IsNullOrEmpty(apiKey) && !String.IsNullOrEmpty(secret))
            {

                // Hide webpart if user is authenticated
                if (CMSContext.CurrentUser.IsAuthenticated())
                {
                    this.Visible = false;
                    return;
                }

                plcPasswordNew.Visible = this.AllowFormsAuthentication;
                pnlExistingUser.Visible = this.AllowExistingUser;

                facebookUserId = ValidationHelper.GetString(Session[SESSION_NAME_USERDATA], null);

                // There is no Facebook user ID stored in session - hide all
                if (String.IsNullOrEmpty(facebookUserId) && HideForNoFacebookID)
                {
                    this.Visible = false;
                }
            }
            else
            {
                // Error label is displayed in Design mode when Facebook Connect is disabled
                if (CMSContext.ViewMode == ViewModeEnum.Design)
                {
                    string parameter = null;
                    if (CMSContext.CurrentUser.IsGlobalAdministrator)
                    {
                        parameter = "<a href=\"" + UrlHelper.GetAbsoluteUrl("~/CMSSiteManager/default.aspx?section=settings") + "\" target=\"_top\">SiteManager -> Settings -> Facebook Connect</a>";
                    }
                    else
                    {
                        parameter = "SiteManager -> Settings -> Facebook Connect";
                    }
                    lblError.Text = String.Format(ResHelper.GetString("mem.facebook.disabled"), parameter);
                    plcError.Visible = true;
                    plcContent.Visible = false;
                }
                else
                {
                    this.Visible = false;
                }
            }
        }
        else
        {
            this.Visible = false;
        }
    }


    /// <summary>
    /// Handles btnOkExist click, joins existing user with liveid token.
    /// </summary>
    protected void btnOkExist_Click(object sender, EventArgs e)
    {
        // Live user must be retrieved from session
        if (!String.IsNullOrEmpty(facebookUserId))
        {
            if (!String.IsNullOrEmpty(txtUserName.Text))
            {
                // Try to authenticate user
                UserInfo ui = UserInfoProvider.AuthenticateUser(txtUserName.Text, txtPassword.Text, CMSContext.CurrentSiteName);

                // Check banned IPs
                BannedIPInfoProvider.CheckIPandRedirect(CMSContext.CurrentSiteName, BanControlEnum.Login);

                if (ui != null)
                {
                    // Add Facebook Connect user ID token to user
                    ui.UserSettings.UserFacebookID = facebookUserId;
                    UserInfoProvider.SetUserInfo(ui);

                    // Set authentication cookie and redirect to page
                    SetAuthCookieAndRedirect(ui);
                }
                else // Invalid credentials
                {
                    lblError.Text = ResHelper.GetString("Login_FailureText");
                    plcError.Visible = true;
                }
            }
            else // User did not fill the form
            {
                lblError.Text = ResHelper.GetString("mem.facebook.fillloginform");
                plcError.Visible = true;
            }
        }
    }


    /// <summary>
    /// Handles btnOkNew click, creates new user and joins it with liveid token
    /// </summary>
    protected void btnOkNew_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(facebookUserId))
        {
            string currentSiteName = CMSContext.CurrentSiteName;

            // Check banned IPs
            BannedIPInfoProvider.CheckIPandRedirect(currentSiteName, BanControlEnum.Registration);

            // Validate entered values
            string errorMessage = new Validator().IsRegularExp(txtUserNameNew.Text, "^([a-zA-Z0-9_\\-\\.@]+)$", ResHelper.GetString("mem.facebook.fillcorrectusername"))
                .IsEmail(txtEmail.Text, ResHelper.GetString("mem.facebook.fillvalidemail")).Result;

            string password = txtPasswordNew.Text.Trim();

            // If password is enabled to set, check it
            if (plcPasswordNew.Visible && (String.IsNullOrEmpty(errorMessage)))
            {
                if (String.IsNullOrEmpty(password))
                {
                    errorMessage = ResHelper.GetString("mem.facebook.specifyyourpass");
                }
                else if (password != txtConfirmPassword.Text.Trim())
                {
                    errorMessage = ResHelper.GetString("webparts_membership_registrationform.passwordonotmatch");
                }
            }

            // Check whether email is unique if it is required
            if ((String.IsNullOrEmpty(errorMessage)) && !UserInfoProvider.IsEmailUnique(txtEmail.Text.Trim(), currentSiteName, 0))
            {
                errorMessage = ResHelper.GetString("UserInfo.EmailAlreadyExist");
            }

            // Check reserved names
            if ((String.IsNullOrEmpty(errorMessage)) && UserInfoProvider.NameIsReserved(currentSiteName, txtUserNameNew.Text.Trim()))
            {
                errorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.UserNameReserved").Replace("%%name%%", HTMLHelper.HTMLEncode(txtUserNameNew.Text.Trim()));
            }

            if (String.IsNullOrEmpty(errorMessage))
            {
                UserInfo ui = UserInfoProvider.AuthenticateFacebookConnectUser(facebookUserId, currentSiteName, true, true);

                if (ui != null)
                {
                    // Set additional information
                    ui.UserName = ui.UserNickName = txtUserNameNew.Text.Trim();
                    ui.Email = txtEmail.Text;

                    // Set password
                    if (plcPasswordNew.Visible)
                    {
                        UserInfoProvider.SetPassword(ui, password);

                        // If user can choose password then is not considered external(external user can't login in common way)
                        ui.IsExternal = false;
                    }

                    #region "License limitations"

                    // Check limitations for Global administrator
                    if (ui.IsGlobalAdministrator)
                    {
                        if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.GlobalAdmininistrators, VersionActionEnum.Insert, false))
                        {
                            lblError.Visible = true;
                            lblError.Text = ResHelper.GetString("License.MaxItemsReachedGlobal");
                            return;
                        }
                    }

                    // Check limitations for editors
                    if (ui.IsEditor)
                    {
                        if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Editors, VersionActionEnum.Insert, false))
                        {
                            lblError.Visible = true;
                            lblError.Text = ResHelper.GetString("License.MaxItemsReachedEditor");
                            return;
                        }
                    }

                    // Check limitations for site members
                    if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.SiteMembers, VersionActionEnum.Insert, false))
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("License.MaxItemsReachedSiteMember");
                        return;
                    }

                    #endregion

                    ui.UserSettings.UserRegistrationInfo.IPAddress = HttpContext.Current.Request.UserHostAddress;
                    ui.UserSettings.UserRegistrationInfo.Agent = HttpContext.Current.Request.UserAgent;

                    // Check whether confirmation is required
                    bool requiresConfirmation = SettingsKeyProvider.GetBoolValue(currentSiteName + ".CMSRegistrationEmailConfirmation");
                    bool requiresAdminApprove = SettingsKeyProvider.GetBoolValue(currentSiteName + ".CMSRegistrationAdministratorApproval");
                    if (!requiresConfirmation)
                    {
                        // If confirmation is not required check whether administration approval is reqiures
                        if (requiresAdminApprove)
                        {
                            ui.Enabled = false;
                            ui.UserSettings.UserWaitingForApproval = true;
                        }
                    }
                    else
                    {
                        // EnableUserAfterRegistration is overrided by requiresConfirmation - user needs to be confirmed before enable
                        ui.Enabled = false;
                    }

                    UserInfoProvider.SetUserInfo(ui);

                    // Remove live user object from session, won't be needed
                    Session.Remove(SESSION_NAME_USERDATA);

                    #region "Welcome Emails (confirmation, waiting for approval)"

                    bool error = false;
                    EventLogProvider ev = new EventLogProvider();
                    EmailTemplateInfo template = null;

                    // Prepare macro replacements
                    string[,] replacements = new string[3, 2];
                    replacements[0, 0] = "confirmaddress";
                    replacements[0, 1] = (this.ApprovalPage != String.Empty) ? UrlHelper.GetAbsoluteUrl(this.ApprovalPage) + "?userguid=" + ui.UserGUID : UrlHelper.GetAbsoluteUrl("~/CMSPages/Dialogs/UserRegistration.aspx") + "?userguid=" + ui.UserGUID;
                    replacements[1, 0] = "username";
                    replacements[1, 1] = ui.UserName;
                    replacements[2, 0] = "password";
                    replacements[2, 1] = password;

                    // Set resolver
                    ContextResolver resolver = CMSContext.CurrentResolver;
                    resolver.SourceParameters = replacements;

                    // Email message
                    EmailMessage emailMessage = new EmailMessage();
                    emailMessage.EmailFormat = EmailFormatEnum.Default;
                    emailMessage.Recipients = ui.Email;

                    // Send welcome message with username and password, with confirmation link, user must confirm registration
                    if (requiresConfirmation)
                    {
                        template = EmailTemplateProvider.GetEmailTemplate("RegistrationConfirmation", CMSContext.CurrentSiteName);
                        emailMessage.Subject = ResHelper.GetString("RegistrationForm.RegistrationConfirmationEmailSubject");
                    }
                    // Send welcome message with username and password, with information that user must be approved by administrator
                    else if (this.SendWelcomeEmail)
                    {
                        if (requiresAdminApprove)
                        {
                            template = EmailTemplateProvider.GetEmailTemplate("Membership.RegistrationWaitingForApproval", CMSContext.CurrentSiteName);
                            emailMessage.Subject = ResHelper.GetString("RegistrationForm.RegistrationWaitingForApprovalSubject");
                        }
                        // Send welcome message with username and password, user can logon directly
                        else
                        {
                            template = EmailTemplateProvider.GetEmailTemplate("Membership.Registration", CMSContext.CurrentSiteName);
                            emailMessage.Subject = ResHelper.GetString("RegistrationForm.RegistrationSubject");
                        }
                    }

                    if (template != null)
                    {
                        emailMessage.From = EmailHelper.GetSender(template, SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSNoreplyEmailAddress"));
                        // Enable macro encoding for body
                        resolver.EncodeResolvedValues = true;
                        emailMessage.Body = resolver.ResolveMacros(template.TemplateText);
                        // Disable macro encoding for plaintext body and subject
                        resolver.EncodeResolvedValues = false;
                        emailMessage.PlainTextBody = resolver.ResolveMacros(template.TemplatePlainText);
                        emailMessage.Subject = resolver.ResolveMacros(EmailHelper.GetSubject(template, emailMessage.Subject));

                        emailMessage.CcRecipients = template.TemplateCc;
                        emailMessage.BccRecipients = template.TemplateBcc;

                        try
                        {
                            MetaFileInfoProvider.ResolveMetaFileImages(emailMessage, template.TemplateID, EmailObjectType.EMAILTEMPLATE, MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE);
                            // Send the e-mail immediately
                            EmailSender.SendEmail(CMSContext.CurrentSiteName, emailMessage, true);
                        }
                        catch (Exception ex)
                        {
                            ev.LogEvent("E", "RegistrationForm - SendEmail", ex);
                            error = true;
                        }
                    }

                    // If there was some error, user must be deleted
                    if (error)
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("RegistrationForm.UserWasNotCreated");

                        // Email was not send, user can't be approved - delete it
                        UserInfoProvider.DeleteUser(ui);
                        return;
                    }

                    #endregion

                    #region "Administrator notification email"

                    // Notify administrator if enabled and email confirmation is not required
                    if (!requiresConfirmation && this.NotifyAdministrator && (!String.IsNullOrEmpty(this.FromAddress)) && (!String.IsNullOrEmpty(this.ToAddress)))
                    {
                        EmailTemplateInfo mEmailTemplate = null;

                        if (requiresAdminApprove)
                        {
                            mEmailTemplate = EmailTemplateProvider.GetEmailTemplate("Registration.Approve", CMSContext.CurrentSiteName);
                        }
                        else
                        {
                            mEmailTemplate = EmailTemplateProvider.GetEmailTemplate("Registration.New", CMSContext.CurrentSiteName);
                        }

                        if (mEmailTemplate == null)
                        {
                            ev.LogEvent("E", DateTime.Now, "RegistrationForm", "GetEmailTemplate", HTTPHelper.GetAbsoluteUri());
                        }
                        //email template ok
                        else
                        {
                            replacements = new string[4, 2];
                            replacements[0, 0] = "firstname";
                            replacements[0, 1] = ui.FirstName;
                            replacements[1, 0] = "lastname";
                            replacements[1, 1] = ui.LastName;
                            replacements[2, 0] = "email";
                            replacements[2, 1] = ui.Email;
                            replacements[3, 0] = "username";
                            replacements[3, 1] = ui.UserName;

                            // Set resolver
                            resolver = CMSContext.CurrentResolver;
                            resolver.SourceParameters = replacements;
                            // Enable macro encoding for body
                            resolver.EncodeResolvedValues = true;

                            EmailMessage message = new EmailMessage();
                            message.EmailFormat = EmailFormatEnum.Default;
                            message.From = EmailHelper.GetSender(mEmailTemplate, this.FromAddress);
                            message.Recipients = this.ToAddress;
                            message.Body = resolver.ResolveMacros(mEmailTemplate.TemplateText);
                            // Disable macro encoding for plaintext body and subject
                            resolver.EncodeResolvedValues = false;
                            message.Subject = resolver.ResolveMacros(EmailHelper.GetSubject(mEmailTemplate, ResHelper.GetString("RegistrationForm.EmailSubject")));
                            message.PlainTextBody = resolver.ResolveMacros(mEmailTemplate.TemplatePlainText);

                            message.CcRecipients = mEmailTemplate.TemplateCc;
                            message.BccRecipients = mEmailTemplate.TemplateBcc;

                            try
                            {
                                // Attach template meta-files to e-mail
                                MetaFileInfoProvider.ResolveMetaFileImages(message, mEmailTemplate.TemplateID, EmailObjectType.EMAILTEMPLATE, MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE);
                                EmailSender.SendEmail(CMSContext.CurrentSiteName, message);
                            }
                            catch
                            {
                                ev.LogEvent("E", DateTime.Now, "Membership", "RegistrationEmail", CMSContext.CurrentSite.SiteID);
                            }
                        }
                    }

                    #endregion

                    #region "Web analytics"

                    // Track successful registration conversion
                    if (!String.IsNullOrEmpty(this.TrackConversionName))
                    {
                        if (AnalyticsHelper.AnalyticsEnabled(currentSiteName) && AnalyticsHelper.TrackConversionsEnabled(currentSiteName) && !AnalyticsHelper.IsIPExcluded(currentSiteName, HttpContext.Current.Request.UserHostAddress))
                        {
                            string objectName = new ContextResolver().ResolveMacros(this.TrackConversionName);
                            HitLogProvider.LogHit(HitLogProvider.CONVERSIONS, currentSiteName, CMSContext.PreferredCultureCode, objectName, 0);
                        }
                    }

                    // Log registered user if confirmation is not required
                    if (!requiresConfirmation)
                    {
                        AnalyticsHelper.LogRegisteredUser(CMSContext.CurrentSiteName, ui);
                    }

                    #endregion

                    // Set authentication cookie and redirect to page
                    SetAuthCookieAndRedirect(ui);

                    if (!String.IsNullOrEmpty(this.DisplayMessage))
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = this.DisplayMessage;
                        plcForm.Visible = false;
                    }
                    else
                    {
                        UrlHelper.Redirect(ResolveUrl("~/Default.aspx"));
                    }
                }
            }
            else
            {
                lblError.Text = errorMessage;
                plcError.Visible = true;
            }
        }
    }


    /// <summary>
    /// Helper method, set authentication cookie and redirect to return URL or default page.
    /// </summary>
    /// <param name="ui">User info</param>
    /// <param name="user">Windows live user</param>
    private void SetAuthCookieAndRedirect(UserInfo ui)
    {
        // Create autentification cookie
        if (ui.Enabled)
        {
            BannedIPInfoProvider.CheckIPandRedirect(CMSContext.CurrentSiteName, BanControlEnum.Login);

            UserInfoProvider.SetAuthCookieWithUserData(ui.UserName, true, Session.Timeout, new string[] { "facebooklogin" });
            UserInfoProvider.SetPreferredCultures(ui);

            string returnUrl = QueryHelper.GetString("returnurl", null);

            // Redirect to ReturnURL
            if (!String.IsNullOrEmpty(returnUrl))
            {
                UrlHelper.Redirect(ResolveUrl(HttpUtility.UrlDecode(returnUrl)));
            }
            // Redirect to default page    
            else if (!String.IsNullOrEmpty(this.DefaultTargetUrl))
            {
                UrlHelper.Redirect(ResolveUrl(this.DefaultTargetUrl));
            }
            // Otherwise refresh current page
            else
            {
                UrlHelper.Redirect(URLRewriter.CurrentURL);
            }
        }
    }

    #endregion
}
