CREATE TABLE [Reporting_SavedGraph] (
		[SavedGraphID]                int NOT NULL IDENTITY(1, 1),
		[SavedGraphSavedReportID]     int NOT NULL,
		[SavedGraphGUID]              uniqueidentifier NOT NULL,
		[SavedGraphBinary]            varbinary(max) NOT NULL,
		[SavedGraphMimeType]          nvarchar(100) NOT NULL,
		[SavedGraphLastModified]      datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Reporting_SavedGraph]
	ADD
	CONSTRAINT [PK_Reporting_SavedGraph]
	PRIMARY KEY
	([SavedGraphID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Reporting_SavedGraph]') IS NOT NULL
	AND OBJECT_ID(N'[Reporting_SavedReport]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Reporting_SavedGraph_SavedGraphSavedReportID_Reporting_SavedReport]') IS NULL
BEGIN
		ALTER TABLE [Reporting_SavedGraph]
			ADD CONSTRAINT [FK_Reporting_SavedGraph_SavedGraphSavedReportID_Reporting_SavedReport]
			FOREIGN KEY ([SavedGraphSavedReportID]) REFERENCES [Reporting_SavedReport] ([SavedReportID])
END
CREATE INDEX [IX_Reporting_SavedGraph_SavedGraphGUID]
	ON [Reporting_SavedGraph] ([SavedGraphGUID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Reporting_SavedGraph_SavedGraphSavedReportID]
	ON [Reporting_SavedGraph] ([SavedGraphSavedReportID])
	ON [PRIMARY]
