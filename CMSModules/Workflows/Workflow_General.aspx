<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Workflow_General.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSSiteManager_Development_Workflows_Workflow_General"
    Theme="Default" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblDisplayName" runat="server" EnableViewState="false" ResourceString="general.displayname"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="TextBoxWorkflowDisplayName" runat="server" CssClass="TextBoxField"
                    MaxLength="450" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDisplayName" runat="server"
                    EnableViewState="false" ControlToValidate="TextBoxWorkflowDisplayName" Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblCodeName" runat="server" EnableViewState="false" ResourceString="general.codename"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="450" />
                <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" EnableViewState="false"
                    ControlToValidate="txtCodeName" Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:LocalizedButton ID="btnOk" runat="server" OnClick="ButtonOK_Click" CssClass="SubmitButton"
                    ResourceString="general.ok" EnableViewState="false" />
            </td>
        </tr>
    </table>
</asp:Content>
