<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/MediaLibrary/MediaGalleryFileList.ascx.cs"
    Inherits="CMSWebParts_MediaLibrary_MediaGalleryFileList" %>
<%@ Register Src="~/CMSAdminControls/UI/System/PermissionMessage.ascx" TagName="PermissionMessage"
    TagPrefix="cms" %>
<cms:PermissionMessage ID="messageElem" runat="server" EnableViewState="false" DisplayMessage="false" />
<cms:BasicRepeater ID="repItems" runat="server" />