﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using CMS.UIControls;

public partial class CMSAdminControls_Debug_Debug : CMSUserControl
{
    protected override void Render(HtmlTextWriter writer)
    {
        // Do not render if nothing is displayed
        if (this.logCache.Visible ||
            this.logSQL.Visible ||
            this.logSec.Visible ||
            this.logState.Visible)
        {
            base.Render(writer);
        }
    }
}
