CREATE TABLE [Staging_Server] (
		[ServerID]                  int NOT NULL IDENTITY(1, 1),
		[ServerName]                nvarchar(100) NOT NULL,
		[ServerDisplayName]         nvarchar(440) NOT NULL,
		[ServerSiteID]              int NOT NULL,
		[ServerURL]                 nvarchar(450) NOT NULL,
		[ServerEnabled]             bit NOT NULL,
		[ServerAuthentication]      nvarchar(20) NOT NULL,
		[ServerUsername]            nvarchar(100) NULL,
		[ServerPassword]            nvarchar(100) NULL,
		[ServerX509ClientKeyID]     nvarchar(200) NULL,
		[ServerX509ServerKeyID]     nvarchar(200) NULL,
		[ServerGUID]                uniqueidentifier NOT NULL,
		[ServerLastModified]        datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Staging_Server]
	ADD
	CONSTRAINT [PK_Staging_Server]
	PRIMARY KEY
	NONCLUSTERED
	([ServerID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Staging_Server]
	ADD
	CONSTRAINT [DEFAULT_staging_server_ServerDisplayName]
	DEFAULT ('') FOR [ServerDisplayName]
IF OBJECT_ID(N'[Staging_Server]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Staging_Server_ServerSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Staging_Server]
			ADD CONSTRAINT [FK_Staging_Server_ServerSiteID_CMS_Site]
			FOREIGN KEY ([ServerSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE INDEX [IX_Staging_Server_ServerEnabled]
	ON [Staging_Server] ([ServerEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_Staging_Server_ServerSiteID_ServerDisplayName]
	ON [Staging_Server] ([ServerSiteID], [ServerDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
