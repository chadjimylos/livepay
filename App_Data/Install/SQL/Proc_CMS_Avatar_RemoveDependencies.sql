-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_CMS_Avatar_RemoveDependencies]
	@AvatarID int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE [CMS_UserSettings] SET UserAvatarID=NULL WHERE UserAvatarID=@AvatarID;
	UPDATE [Community_Group] SET GroupAvatarID=NULL WHERE GroupAvatarID=@AvatarID;
END
