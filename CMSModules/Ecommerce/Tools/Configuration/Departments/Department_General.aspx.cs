using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_Departments_Department_General : CMSEcommerceConfigurationPage
{
    protected int departmentId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // field validator error messages initialization
        rfvDisplayName.ErrorMessage = ResHelper.GetString("Department_Edit.errorEmptyDisplayName");
        rfvCodeName.ErrorMessage = ResHelper.GetString("Department_Edit.errorEmptyCodeName");

        // control initializations				
        lblDepartmentName.Text = ResHelper.GetString("Department_Edit.DepartmentNameLabel");
        lblDepartmentDisplayName.Text = ResHelper.GetString("Department_Edit.DepartmentDisplayNameLabel");

        btnOk.Text = ResHelper.GetString("General.OK");

        // show that the department was created or updated successfully
        if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
        {
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }

        // get department id from querystring		
        departmentId = ValidationHelper.GetInteger(Request.QueryString["departmentId"], 0);
        if (departmentId > 0)
        {
            DepartmentInfo departmentObj = DepartmentInfoProvider.GetDepartmentInfo(departmentId);
            if (departmentObj != null)
            {
                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(departmentObj);
                }
            }
        }
    }


    /// <summary>
    /// Load data of editing department.
    /// </summary>
    /// <param name="departmentObj">Department object.</param>
    protected void LoadData(DepartmentInfo departmentObj)
    {
        txtDepartmentName.Text = departmentObj.DepartmentName;
        txtDepartmentDisplayName.Text = departmentObj.DepartmentDisplayName;
        
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        // check input values from textboxes and other contrlos - new Validator()
        string errorMessage = new Validator()
            .NotEmpty(txtDepartmentDisplayName.Text, ResHelper.GetString("Department_Edit.errorEmptyDisplayName"))
            .NotEmpty(txtDepartmentName.Text, ResHelper.GetString("Department_Edit.errorEmptyCodeName"))
            .IsCodeName(txtDepartmentName.Text, ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat")).Result;

        if (errorMessage == "")
        {
            // Check unique name
            DepartmentInfo departmentObj = DepartmentInfoProvider.GetDepartmentInfo(txtDepartmentName.Text.Trim());
            if ((departmentObj == null) || (departmentObj.DepartmentID == departmentId))
            {
                // Get the object
                if (departmentObj == null)
                {
                    departmentObj = DepartmentInfoProvider.GetDepartmentInfo(departmentId);
                    if (departmentObj == null)
                    {
                        departmentObj = new DepartmentInfo();
                    }
                }

                departmentObj.DepartmentID = departmentId;
                departmentObj.DepartmentName = txtDepartmentName.Text.Trim();
                departmentObj.DepartmentDisplayName = txtDepartmentDisplayName.Text.Trim();
                
                // Save the object
                DepartmentInfoProvider.SetDepartmentInfo(departmentObj);

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Department_Edit.DepartmentNameExists");
                lblInfo.Visible = false;
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
