using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.Controls;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_ProductOptions_OptionCategory_Edit_Header : CMSEcommercePage
{
    protected int categoryId = 0;

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        int showTitle = QueryHelper.GetInteger("showtitle", 0);
        if (showTitle > 0) 
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("OptionCategory_Edit.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_OptionCategory/object.png");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool hideBreadcrumbs = ValidationHelper.GetBoolean(Request.QueryString["hidebreadcrumbs"], false);
        // Get option category ID from querystring
        categoryId = ValidationHelper.GetInteger(Request.QueryString["categoryID"], 0);

        // Get localized option category name
        string categName = "";
        OptionCategoryInfo categObj = OptionCategoryInfoProvider.GetOptionCategoryInfo(categoryId);
        if (categObj != null)
        {
            categName = ResHelper.LocalizeString(categObj.CategoryDisplayName);
        }

        if (!hideBreadcrumbs)
        {
            // Initializes page title control
            string[,] pageTitleTabs = new string[2, 3];
            pageTitleTabs[0, 0] = ResHelper.GetString("OptionCategory_Edit.OptionCategories");
            pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/ProductOptions/OptionCategory_List.aspx";
            pageTitleTabs[0, 2] = "ecommerceContent";
            pageTitleTabs[1, 0] = categName;
            pageTitleTabs[1, 1] = "";
            pageTitleTabs[1, 2] = "";
            this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        }

        this.CurrentMaster.Title.HelpTopicName = "edit_option_category___general";
        this.CurrentMaster.Title.HelpName = "helpTopic";


        if (!RequestHelper.IsPostBack())
        {
            InitializeTabMenu();
        }
    }


    /// <summary>
    /// Initializes tab menu control.
    /// </summary>
    private void InitializeTabMenu()
    {
        this.CurrentMaster.Tabs.Tabs = BasicTabControl.GetTabsArray(2);
        this.CurrentMaster.Tabs.Tabs[0, 0] = ResHelper.GetString("general.general");
        this.CurrentMaster.Tabs.Tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'edit_option_category___general');";
        this.CurrentMaster.Tabs.Tabs[0, 2] = "OptionCategory_Edit_General.aspx?categoryId=" + categoryId.ToString();
        this.CurrentMaster.Tabs.Tabs[1, 0] = ResHelper.GetString("OptionCategory_Edit.Options");
        this.CurrentMaster.Tabs.Tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'edit_option_category___options');";
        this.CurrentMaster.Tabs.Tabs[1, 2] = "../../Tools/ProductOptions/OptionCategory_Edit_Options.aspx?categoryId=" + categoryId.ToString();
        this.CurrentMaster.Tabs.UrlTarget = "OptionCategoryEdit";
    }
}
