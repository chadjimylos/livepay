using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.FormControls;
using CMS.EventLog;

public partial class CMSSiteManager_Administration_EventLog_EventFilter : CMSUserControl
{
    #region "Variables"

    private int mSiteId = 0;
    private bool isAdvancedMode = false;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the site ID for which the events should be filtered.
    /// </summary>
    public int SiteID
    {
        get
        {
            return this.mSiteId;
        }
        set
        {
            this.mSiteId = value;
        }
    }


    /// <summary>
    /// Gets the where condition created using filtered parameters.
    /// </summary>
    public string WhereCondition
    {
        get
        {
            return GenerateWhereCondition();
        }
    }

    #endregion


    #region "Page methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        InitializeForm();

        // Show correct filter panel
        EnsureFilterMode();
        plcAdvancedSearch.Visible = isAdvancedMode;
    }

    #endregion


    #region "UI methods"

    /// <summary>
    /// Initializes standard filter dropdown
    /// </summary>
    /// <param name="drp">Dropdown to init</param>
    private void InitFilterDropDown(DropDownList drp)
    {
        if ((drp != null) && (drp.Items.Count <= 0))
        {
            drp.Items.Add(new ListItem("LIKE", "LIKE"));
            drp.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
            drp.Items.Add(new ListItem("=", "="));
            drp.Items.Add(new ListItem("<>", "<>"));
        }
    }


    /// <summary>
    /// Shows/hides all elements for advanced or simple mode.
    /// </summary>
    /// <param name="showAdvanced"></param>
    private void ShowFilterElements(bool showAdvanced)
    {
        plcAdvancedSearch.Visible = showAdvanced;
        pnlAdvanced.Visible = showAdvanced;
        pnlSimple.Visible = !showAdvanced;
    }


    /// <summary>
    /// Initializes the layout of the form.
    /// </summary>
    private void InitializeForm()
    {
        // General UI
        this.lnkShowAdvancedFilter.Text = ResHelper.GetString("user.filter.showadvanced");
        this.imgShowAdvancedFilter.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/SortDown.png");
        this.lnkShowSimpleFilter.Text = ResHelper.GetString("user.filter.showsimple");
        this.imgShowSimpleFilter.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/SortUp.png");
        plcAdvancedSearch.Visible = isAdvancedMode;
        pnlAdvanced.Visible = isAdvancedMode;
        pnlSimple.Visible = !isAdvancedMode;


        // Initialize advanced filter dropdownlists
        if (!RequestHelper.IsPostBack())
        {
            InitFilterDropDown(drpSource);
            InitFilterDropDown(drpEventCode);
            InitFilterDropDown(drpUserName);
            InitFilterDropDown(drpIPAddress);
            InitFilterDropDown(drpDocumentName);
            InitFilterDropDown(drpMachineName);
            InitFilterDropDown(drpEventURL);
            InitFilterDropDown(drpURLReferrer);
            InitFilterDropDown(drpDescription);
            InitFilterDropDown(drpUserAgent);

            if (drpType.Items.Count <= 0)
            {
                drpType.Items.Add(new ListItem(ResHelper.GetString("general.all"), ""));
                foreach (string eventType in new string[] { "e", "i", "w" })
                {
                    drpType.Items.Add(new ListItem(EventLogHelper.GetEventTypeText(eventType), eventType));
                }
            }



            if (drpRoles.Items.Count <= 0)
            {
                drpRoles.Items.Add(new ListItem(ResHelper.GetString("general.all"), ""));
                foreach (DataRow role in CMS.SiteProvider.RoleInfoProvider.GetAllRoles(this.SiteID).Tables[0].Rows)
                {
                    drpRoles.Items.Add(new ListItem(role[1].ToString(), role[0].ToString()));
                }
            }
        }
    }


    /// <summary>
    /// Ensures correct filter mode flag if filter mode was just changed.
    /// </summary>
    private void EnsureFilterMode()
    {
        if (UrlHelper.IsPostback())
        {
            // Get current event target
            string uniqieId = ValidationHelper.GetString(Request.Params["__EVENTTARGET"], String.Empty);
            uniqieId = uniqieId.Replace("$", "_");

            // If postback was fired by mode switch, update isAdvancedMode variable
            if (uniqieId == lnkShowAdvancedFilter.ClientID)
            {
                isAdvancedMode = true;
            }
            else if (uniqieId == lnkShowSimpleFilter.ClientID)
            {
                isAdvancedMode = false;
            }
            else
            {
                isAdvancedMode = ValidationHelper.GetBoolean(ViewState["IsAdvancedMode"], false);
            }
        }
    }


    /// <summary>
    /// Sets the advanced mode
    /// </summary>
    protected void lnkShowAdvancedFilter_Click(object sender, EventArgs e)
    {
        isAdvancedMode = true;
        ViewState["IsAdvancedMode"] = isAdvancedMode;
        ShowFilterElements(isAdvancedMode);
    }


    /// <summary>
    /// Sets the simple mode
    /// </summary>
    protected void lnkShowSimpleFilter_Click(object sender, EventArgs e)
    {
        isAdvancedMode = false;
        ViewState["IsAdvancedMode"] = isAdvancedMode;
        ShowFilterElements(isAdvancedMode);
    }

    #endregion


    #region "Search methods - where condition"

    /// <summary>
    /// Generates complete filter where condition
    /// </summary>    
    private string GenerateWhereCondition()
    {
        // Get mode from viewstate
        EnsureFilterMode();

        string whereCond = "";

        // Create WHERE condition for basic filter
        BuildWhereCond(ref whereCond, "EventType", drpType.SelectedValue, "=");
        BuildWhereCond(ref whereCond, "Source", txtSource.Text, drpSource.SelectedValue);
        BuildWhereCond(ref whereCond, "EventCode", txtEventCode.Text, drpEventCode.SelectedValue);

        // Make sure that fromTime precedes toTime
        DateTime fromTime = dtmTimeFrom.SelectedDateTime;
        DateTime toTime = dtmTimeTo.SelectedDateTime;
        if ((fromTime != DataHelper.DATETIME_NOT_SELECTED) && (toTime != DataHelper.DATETIME_NOT_SELECTED))
        {
            if (fromTime > toTime)
            {
                DateTime tmp = fromTime;
                fromTime = toTime;
                toTime = tmp;
            }
        }

        // Apply fromTime limit
        if (fromTime != DataHelper.DATETIME_NOT_SELECTED)
        {
            if (!String.IsNullOrEmpty(whereCond))
            {
                whereCond += " AND ";
            }
            whereCond += "EventTime >= '" + fromTime.ToString("yyyy-MM-dd HH:mm:ss") + "'";
        }


        // Apply toTime limit
        if (toTime != DataHelper.DATETIME_NOT_SELECTED)
        {
            if (!String.IsNullOrEmpty(whereCond))
            {
                whereCond += " AND ";
            }
            whereCond += "EventTime <= '" + toTime.ToString("yyyy-MM-dd HH:mm:ss") + "'";
        }

        // Create WHERE condition for advanced filter (id needed)
        if (isAdvancedMode)
        {
            BuildWhereCond(ref whereCond, "UserName", txtUserName.Text, drpUserName.SelectedValue);
            BuildWhereCond(ref whereCond, "IPAddress", txtIPAddress.Text, drpIPAddress.SelectedValue);
            BuildWhereCond(ref whereCond, "DocumentName", txtDocumentName.Text, drpDocumentName.SelectedValue);
            BuildWhereCond(ref whereCond, "EventMachineName", txtMachineName.Text, drpMachineName.SelectedValue);
            BuildWhereCond(ref whereCond, "EventUrl", txtEventURL.Text, drpEventURL.SelectedValue);
            BuildWhereCond(ref whereCond, "EventUrlReferrer", txtURLReferrer.Text, drpURLReferrer.SelectedValue);
            BuildWhereCond(ref whereCond, "EventDescription", txtDescription.Text, drpDescription.SelectedValue);
            BuildWhereCond(ref whereCond, "EventUserAgent", txtUserAgent.Text, drpUserAgent.SelectedValue);
        }

        // Append site condition if siteid given
        if (!String.IsNullOrEmpty(whereCond) && (this.SiteID >= 0))
        {
            whereCond += " AND ";
        }

        if (this.SiteID > 0)
        {
            whereCond += " (SiteID=" + this.SiteID.ToString() + ")";
        }
        else if (this.SiteID == 0)
        {
            whereCond += " (SiteID IS NULL)";
        }

        if (drpRoles.SelectedValue != "") {
            whereCond += "UserID IN (SELECT UserID FROM CMS_UserRole WHERE RoleID = " + drpRoles.SelectedValue + ")";
        }

        return whereCond;
    }


    /// <summary>
    /// Appends WHERE condition for specified column at the end of current/existing WHERE condition.
    /// </summary>
    /// <param name="whereCond">Current WHERE condition</param>
    /// <param name="column">Column name</param>
    /// <param name="value">Value to be searched (if empty no condition is created)</param>
    /// <param name="op">Operator to condition</param>
    private void BuildWhereCond(ref string whereCond, string column, string value, string op)
    {
        // Avoid SQL injenction
        string tempVal = value.Trim().Replace("'", "''");

        // No condition
        if (tempVal == "")
        {
            return;
        }

        // Support for exact phrase search
        if (op.Contains("LIKE"))
        {
            tempVal = "%" + tempVal + "%";
        }

        string where = "(" + column + " " + op + " N'" + tempVal + "')";
        if (!String.IsNullOrEmpty(whereCond))
        {
            where = " AND " + where;
        }

        // Get final where condition
        whereCond += where;
    }

    #endregion
}