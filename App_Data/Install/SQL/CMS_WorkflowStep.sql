CREATE TABLE [CMS_WorkflowStep] (
		[StepID]               int NOT NULL IDENTITY(1, 1),
		[StepDisplayName]      nvarchar(450) NOT NULL,
		[StepName]             nvarchar(440) NULL,
		[StepOrder]            int NOT NULL,
		[StepWorkflowID]       int NOT NULL,
		[StepGUID]             uniqueidentifier NOT NULL,
		[StepLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WorkflowStep]
	ADD
	CONSTRAINT [PK_CMS_WorkflowStep]
	PRIMARY KEY
	NONCLUSTERED
	([StepID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_WorkflowStep]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Workflow]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WorkflowStep_StepWorkflowID]') IS NULL
BEGIN
		ALTER TABLE [CMS_WorkflowStep]
			ADD CONSTRAINT [FK_CMS_WorkflowStep_StepWorkflowID]
			FOREIGN KEY ([StepWorkflowID]) REFERENCES [CMS_Workflow] ([WorkflowID])
END
CREATE CLUSTERED INDEX [IX_CMS_WorkflowStep_StepID_StepName]
	ON [CMS_WorkflowStep] ([StepID], [StepName])
	ON [PRIMARY]
CREATE UNIQUE INDEX [IX_CMS_WorkflowStep_StepWorkflowID_StepName]
	ON [CMS_WorkflowStep] ([StepWorkflowID], [StepName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WorkflowStep_StepWorkflowID_StepOrder]
	ON [CMS_WorkflowStep] ([StepWorkflowID], [StepOrder])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
