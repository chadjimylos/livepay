using System;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.Community;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Friends_Controls_FriendsToApprovalList : CMSAdminListControl
{
    #region "Private variables"

    private int mUserId = 0;
    private bool mDisplayFilter = true;
    protected string dialogsUrl = "~/CMSModules/Friends/Dialogs/";
    private TimeZoneInfo usedTimeZone = null;
    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets and sets User ID.
    /// </summary>
    public int UserID
    {
        get
        {
            return mUserId;
        }
        set
        {
            mUserId = value;
        }
    }


    /// <summary>
    /// Gets and sets whether to show filter.
    /// </summary>
    public bool DisplayFilter
    {
        get
        {
            return mDisplayFilter;
        }
        set
        {
            mDisplayFilter = value;
            searchElem.Visible = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            Visible = false;
            // Do not load data
            gridElem.StopProcessing = true;
        }
        else
        {
            RaiseOnCheckPermissions(PERMISSION_READ, this);
            Visible = true;

            if (IsLiveSite)
            {
                dialogsUrl = "~/CMSModules/Friends/CMSPages/";
            }

            // Register the dialog script
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

            // Register the refreshing script
            string refreshScript = ScriptHelper.GetScript("function refreshList(){" + Page.ClientScript.GetPostBackEventReference(hdnRefresh, string.Empty) + "}");
            ScriptHelper.RegisterClientScriptBlock(Page, typeof(string), "friendsListRefresh", refreshScript);

            gridElem.OnDataReload += new UniGrid.OnDataReloadEventHandler(gridElem_OnDataReload);
            gridElem.OnExternalDataBound += gridElem_OnExternalDataBound;
            gridElem.OrderBy = "UserName";
            gridElem.IsLiveSite = IsLiveSite;

            // Register the dialog script
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY,
                                                   ScriptHelper.DialogScript);

            // Register reject script
            StringBuilder rejectScript = new StringBuilder();
            rejectScript.AppendLine("function Reject(item, gridId)");
            rejectScript.AppendLine("{");
            rejectScript.AppendLine("   var items = '';");
            rejectScript.AppendLine("   if(item == null)");
            rejectScript.AppendLine("   {");
            rejectScript.AppendLine("       items = document.getElementById(gridId).value;");
            rejectScript.AppendLine("   }");
            rejectScript.AppendLine("   else");
            rejectScript.AppendLine("   {");
            rejectScript.AppendLine("       items=item;");
            rejectScript.AppendLine("   }");
            rejectScript.AppendLine("   if(items != '' && items != '|')");
            rejectScript.AppendLine("   {");
            rejectScript.AppendLine("       modalDialog('" + CMSContext.ResolveDialogUrl(dialogsUrl + "Friends_Reject.aspx") + "?userid=" + UserID + "&ids='+items, 'rejectDialog', 410, 270);");
            rejectScript.AppendLine("   }");
            rejectScript.AppendLine("   ClearSelection_" + gridElem.ClientID + "();");
            rejectScript.AppendLine("   return false;");
            rejectScript.AppendLine("}");
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "rejectScript", ScriptHelper.GetScript(rejectScript.ToString()));

            // Register approve script
            StringBuilder approveScript = new StringBuilder();
            approveScript.AppendLine("function Approve(item, gridId)");
            approveScript.AppendLine("{");
            approveScript.AppendLine("   var items = '';");
            approveScript.AppendLine("   if(item == null)");
            approveScript.AppendLine("   {");
            approveScript.AppendLine("       items = document.getElementById(gridId).value;");
            approveScript.AppendLine("   }");
            approveScript.AppendLine("   else");
            approveScript.AppendLine("   {");
            approveScript.AppendLine("       items=item;");
            approveScript.AppendLine("   }");
            approveScript.AppendLine("   if(items != '' && items != '|')");
            approveScript.AppendLine("   {");
            approveScript.AppendLine("      modalDialog('" + CMSContext.ResolveDialogUrl(dialogsUrl + "Friends_Approve.aspx") + "?userid=" + UserID + "&ids='+items, 'rejectDialog', 410, 270);");
            approveScript.AppendLine("   }");
            approveScript.AppendLine("   ClearSelection_" + gridElem.ClientID + "();");
            approveScript.AppendLine("   return false;");
            approveScript.AppendLine("}");
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "approveScript", ScriptHelper.GetScript(approveScript.ToString()));

            // Add actions to buttons
            btnApproveSelected.OnClientClick = "return Approve(null,'" + gridElem.ClientID + "_hidSelection');";
            btnRejectSelected.OnClientClick = "return Reject(null,'" + gridElem.ClientID + "_hidSelection');";

        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        if (!DisplayFilter)
        {
            Visible = HasData();
        }
        base.OnPreRender(e);

        // Reset grid selection after multiple action
        if (RequestHelper.IsPostBack())
        {
            string invokerName = Page.Request.Params.Get("__EVENTTARGET");
            Control invokeControl = !string.IsNullOrEmpty(invokerName) ? Page.FindControl(invokerName) : null;
            if (invokeControl == hdnRefresh)
            {
                gridElem.ResetSelection();
            }
        }
    }

    #endregion


    #region "Grid events"

    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView drv = null;
        switch (sourceName)
        {
            case "reject":
                // Get action button
                ImageButton rejectBtn = (ImageButton)sender;
                // Get full row view
                drv = GetDataRowView((DataControlFieldCell)rejectBtn.Parent);
                // Add custom reject action
                rejectBtn.Attributes["onclick"] = "return Reject('" + drv["FriendID"] + "',null);";
                return rejectBtn;

            case "approve":
                // Get action button
                ImageButton approveBtn = (ImageButton)sender;
                // Get full row view
                drv = GetDataRowView((DataControlFieldCell)approveBtn.Parent);
                // Add custom reject action
                approveBtn.Attributes["onclick"] = "return Approve('" + drv["FriendID"] + "',null);";
                return approveBtn;

            case "friendrequestedwhen":
                if (currentUserInfo == null)
                {
                    currentUserInfo = CMSContext.CurrentUser;
                }
                if (currentSiteInfo == null)
                {
                    currentSiteInfo = CMSContext.CurrentSite;
                }
                DateTime currentDateTime = ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME);
                if (IsLiveSite)
                {
                    return CMSContext.ConvertDateTime(currentDateTime, this);
                }
                else
                {
                    return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(currentDateTime, currentUserInfo, currentSiteInfo, out usedTimeZone);
                }

            case "formattedusername":
                return HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(Convert.ToString(parameter), this.IsLiveSite));

        }
        return parameter;
    }


    protected DataSet gridElem_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        // Load the data
        DataSet friendships = FriendInfoProvider.GetFullFriendsForApproval(UserID, searchElem.WhereCondition, gridElem.SortDirect, currentTopN, "FriendID,UserName,UserNickname,FullName,FriendComment,FriendRequestedWhen");
        // Hide filter when not needed
        if (searchElem.FilterIsSet)
        {
            DisplayFilter = true;
        }
        else if (!DataHelper.DataSourceIsEmpty(friendships))
        {
            int rowsCount = friendships.Tables[0].Rows.Count;
            DisplayFilter = (gridElem.FilterLimit <= 0) || (rowsCount > gridElem.FilterLimit);
        }
        else
        {
            DisplayFilter = false;
        }
        totalRecords = DataHelper.GetItemsCount(friendships);
        return friendships;
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Indicates if there are some data
    /// </summary>
    public bool HasData()
    {
        return (!DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource));
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }

    #endregion
}
