CREATE TABLE [COM_DiscountLevel] (
		[DiscountLevelID]               int NOT NULL IDENTITY(1, 1),
		[DiscountLevelDisplayName]      nvarchar(200) NOT NULL,
		[DiscountLevelName]             nvarchar(100) NOT NULL,
		[DiscountLevelValue]            float NOT NULL,
		[DiscountLevelEnabled]          bit NOT NULL,
		[DiscountLevelValidFrom]        datetime NULL,
		[DiscountLevelValidTo]          datetime NULL,
		[DiscountLevelGUID]             uniqueidentifier NOT NULL,
		[DiscountLevelLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_DiscountLevel]
	ADD
	CONSTRAINT [PK_COM_DiscountLevel]
	PRIMARY KEY
	NONCLUSTERED
	([DiscountLevelID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_DiscountLevel_DiscountLevelDisplayName_DiscountLevelEnabled]
	ON [COM_DiscountLevel] ([DiscountLevelDisplayName], [DiscountLevelEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
