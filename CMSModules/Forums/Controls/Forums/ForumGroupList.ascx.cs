using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.CMSHelper;
using CMS.Forums;
using CMS.UIControls;

public partial class CMSModules_Forums_Controls_Forums_ForumGroupList : CMSAdminListControl
{
    #region "Variables"

    protected bool showApprove = true;
    private string mWhereCondition = String.Empty;
    private int mCommunityGroupId = 0;
    private string mTitleElemUrl = String.Empty;
    private bool process = true;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets ID of current group.
    /// </summary>
    public int CommunityGroupId
    {
        get
        {
            return this.mCommunityGroupId;
        }
        set
        {
            this.mCommunityGroupId = value;
        }
    }


    /// <summary>
    /// Additional WHERE condition to filter data.
    /// </summary>
    public string WhereCondition
    {
        get
        {
            return this.mWhereCondition;
        }
        set
        {
            this.mWhereCondition = value;
        }
    }


    /// <summary>
    /// URL of the approve title image
    /// </summary>
    public string TitleElemUrl
    {
        get
        {
            return this.mTitleElemUrl;
        }
        set
        {
            this.mTitleElemUrl = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        process = true;
        // If control is not visible don't process anything       
        if (!this.Visible || this.StopProcessing)
        {
            process = false;
            EnableViewState = false;
        }

        // Group where condition part
        string groupWhere = "GroupSiteID = " + CMSContext.CurrentSite.SiteID;

        if (this.CommunityGroupId > 0)
        {
            groupWhere += " AND GroupGroupID= " + this.CommunityGroupId;
        }

        // Add where condition from property
        if (this.WhereCondition != String.Empty)
        {
            groupWhere += " AND " + this.WhereCondition;
        }

        string forumsIds = null;
        bool hasGroupRights = false;

        if (this.CommunityGroupId > 0)
        {
            if (CMSContext.CurrentUser.IsGroupAdministrator(CommunityGroupId) ||
                CMSContext.CurrentUser.IsAuthorizedPerResource("cms.groups", "Manage"))
            {
                hasGroupRights = true;
            }
        }
        // Get forums moderated by current user
        else if (!CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            // Get forumId where the user is moderator and forum satisfy group where condition
            string whereCond = "UserID =" + CMSContext.CurrentUser.UserID + " AND ForumID IN ( SELECT ForumID from Forums_Forum WHERE " +
                    "ForumGroupId IN (SELECT GroupID FROM Forums_ForumGroup WHERE " + groupWhere + "))";


            // Get forums where user is moderator
            DataSet ds = ForumModeratorInfoProvider.GetGroupForumsModerators(whereCond, null);

            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                forumsIds = "";

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    forumsIds += ValidationHelper.GetString(dr["ForumId"], "") + ",";
                }

                // Remove ending ,
                forumsIds = forumsIds.TrimEnd(',');
            }
        }

        // Check 'Read' permission
        if (RaiseOnCheckPermissions(CMSAdminControl.PERMISSION_READ, this))
        {
            if ((this.StopProcessing) && ((forumsIds != null)))
            {
                pnlGroups.Visible = false;
            }
        }
        else
        {
            // Check the 'Read' permission for default resource
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.forums", CMSAdminControl.PERMISSION_READ))
            {
                AccessDenied("cms.forums", CMSAdminControl.PERMISSION_READ);
            }
        }

        // Hide approvals
        if ((!CMSContext.CurrentUser.IsGlobalAdministrator) && (forumsIds == null) && (hasGroupRights == false))
        {
            gridApprove.GridView.Enabled = false;
            pnlApprove.Visible = false;
            showApprove = false;
        }

        titleElem.TitleText = ResHelper.GetString("Forums.WaitingApprove");
        titleElem.TitleImage = (!String.IsNullOrEmpty(this.TitleElemUrl)) ? this.TitleElemUrl : GetImageUrl("CMSModules/CMS_Forums/waitingforapproval.png");

        gridApprove.OnAction += new OnActionEventHandler(gridApprove_OnAction);
        gridApprove.GridView.AllowSorting = false;
        gridApprove.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridApprove_OnExternalDataBound);
        gridApprove.IsLiveSite = this.IsLiveSite;
        gridApprove.HideControlForZeroRows = false;
        gridApprove.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        // Get post for GlobalAdministrator and moderators
        if (CMSContext.CurrentUser.IsGlobalAdministrator || hasGroupRights)
        {
            gridApprove.WhereCondition = "(PostApproved IS NULL OR PostApproved = 0) AND (PostForumID IN (SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (SELECT GroupID FROM [Forums_ForumGroup] WHERE " + groupWhere + ")))";
        }
        else if (forumsIds != null)
        {
            gridApprove.WhereCondition = "((PostApproved IS NULL) OR (PostApproved = 0)) AND (PostForumID IN  (SELECT ForumID FROM [Forums_Forum] WHERE (ForumID IN (" +
               forumsIds + ")) AND(ForumGroupID IN(SELECT GroupID FROM [Forums_ForumGroup] WHERE " + groupWhere + "))))";
        }


        gridGroups.WhereCondition = groupWhere;
        gridGroups.IsLiveSite = this.IsLiveSite;
        gridGroups.OnAction += new OnActionEventHandler(gridGroups_OnAction);
        gridGroups.GridView.AllowSorting = false;
        gridGroups.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Reloads the grid data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        this.gridGroups.ReloadData();
        this.gridApprove.ReloadData();
    }

    protected override void OnPreRender(EventArgs e)
    {
        if (!this.IsLiveSite && process)
        {
            ReloadData();
        }

        if (DataHelper.DataSourceIsEmpty(this.gridApprove.GridView.DataSource))
        {
            pnlApprove.Visible = false;
        }

        base.OnPreRender(e);
    }


    #region "UniGrid events"

    /// <summary>
    /// OnExterna databound
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="sourceName">Source name</param>
    /// <param name="parameter">Parameter</param>
    object gridApprove_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "forum":
                ForumInfo fi = ForumInfoProvider.GetForumInfo(ValidationHelper.GetInteger(parameter, 0));
                if (fi != null)
                {
                    return HTMLHelper.HTMLEncode(fi.ForumDisplayName);
                }
                break;

            case "content":
                DataRowView dr = parameter as DataRowView;
                if (dr != null)
                {
                    string toReturn = "";
                    toReturn = "<strong>" + HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["PostUserName"], "")) + ":</strong> ";
                    toReturn += HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["PostSubject"], "")) + "<br />";
                    toReturn += HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["PostText"], ""));
                    return toReturn;
                }
                break;
        }

        return "";
    }


    /// <summary>
    /// Approve, reject or delete post
    /// </summary>
    protected void gridApprove_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "deletepost":
                ForumPostInfoProvider.DeleteForumPostInfo(ValidationHelper.GetInteger(actionArgument, 0));
                break;

            case "approve":
                ForumPostInfo fpi = ForumPostInfoProvider.GetForumPostInfo(ValidationHelper.GetInteger(actionArgument, 0));
                if (fpi != null)
                {
                    fpi.PostApprovedByUserID = CMSContext.CurrentUser.UserID;
                    fpi.PostApproved = true;
                    ForumPostInfoProvider.SetForumPostInfo(fpi);
                }
                break;
        }

        this.RaiseOnAction(actionName, actionArgument);
    }


    /// <summary>
    /// Forums unigird on action event.
    /// </summary>
    protected void gridGroups_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "delete":
            case "up":
            case "down":
                if (!CheckPermissions("cms.forums", CMSAdminControl.PERMISSION_MODIFY))
                {
                    return;
                }
                break;
        }

        switch (actionName.ToLower())
        {
            case "delete":
                ForumGroupInfoProvider.DeleteForumGroupInfo(ValidationHelper.GetInteger(actionArgument, 0));
                break;

            case "up":
                ForumGroupInfoProvider.MoveGroupUp(ValidationHelper.GetInteger(actionArgument, 0));
                break;

            case "down":
                ForumGroupInfoProvider.MoveGroupDown(ValidationHelper.GetInteger(actionArgument, 0));
                break;
        }

        this.RaiseOnAction(actionName, actionArgument);
    }

    #endregion
}
