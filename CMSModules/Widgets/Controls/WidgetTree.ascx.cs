﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.FormControls;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.ExtendedControls;

public partial class CMSModules_Widgets_Controls_WidgetTree : CMSAdminControl
{
    #region "Variables"

    private bool mSelectWidgets = true;
    private bool mShowRecentlyUsed = false;
    private bool mSelectEditorWidgets = false;
    private bool mSelectUserWidgets = false;
    private bool mSelectGroupWidgets = false;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets whether widgets are shown in tree or not.
    /// </summary>
    public bool SelectWidgets
    {
        get
        {
            return mSelectWidgets;
        }
        set
        {
            mSelectWidgets = value;
        }
    }


    /// <summary>
    /// Gets or sets whether recently used link is shown or not.
    /// </summary>
    public bool ShowRecentlyUsed
    {
        get
        {
            return mShowRecentlyUsed;
        }
        set
        {
            mShowRecentlyUsed = value;
        }
    }


    /// <summary>
    /// Gets or sets selected item.
    /// </summary>
    public string SelectedItem
    {
        get
        {
            return treeElem.SelectedItem;
        }
        set
        {
            treeElem.SelectedItem = value;
        }
    }


    /// <summary>
    /// Enables or disables use of postback in tree. If disabled JavaScript is used
    /// </summary>
    public bool UsePostBack
    {
        get
        {
            return treeElem.UsePostBack;
        }
        set
        {
            treeElem.UsePostBack = value;
        }
    }


    /// <summary>
    /// Gets or sets select path.
    /// </summary>
    public string SelectPath
    {
        get
        {
            return treeElem.SelectPath;
        }
        set
        {
            treeElem.SelectPath = value;
            treeElem.ExpandPath = value;
        }
    }


    /// <summary>
    /// Indicates if the control should perform the operations
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return base.StopProcessing;
        }
        set
        {
            base.StopProcessing = value;
            treeElem.StopProcessing = value;
        }
    }


    /// <summary>
    /// Indicates if control is used on live site
    /// </summary>
    public override bool IsLiveSite
    {
        get
        {
            return base.IsLiveSite;
        }
        set
        {
            base.IsLiveSite = value;
            treeElem.IsLiveSite = value;
        }
    }


    /// <summary>
    /// Determines whether the flat selector will display widgets available for editors
    /// </summary>
    public bool SelectEditorWidgets
    {
        get
        {
            return mSelectEditorWidgets;
        }
        set
        {
            mSelectEditorWidgets = value;
        }
    }


    /// <summary>
    /// Determines whether the flat selector will only display widgets available for authenticated users
    /// </summary>
    public bool SelectUserWidgets
    {
        get
        {
            return mSelectUserWidgets;
        }
        set
        {
            mSelectUserWidgets = value;
        }
    }


    /// <summary>
    /// Determines whether the flat selector will only display widgets available for group admin
    /// </summary>
    public bool SelectGroupWidgets
    {
        get
        {
            return mSelectGroupWidgets;
        }
        set
        {
            mSelectGroupWidgets = value;
        }
    }

    #endregion


    #region "Custom events"

    /// <summary>
    /// On selected item event handler.
    /// </summary>    
    public delegate void ItemSelectedEventHandler(string selectedValue);

    /// <summary>
    /// On selected item event handler.
    /// </summary>
    public event ItemSelectedEventHandler OnItemSelected;

    #endregion


    #region "Page and other events"

    /// <summary>
    /// Page_Load event.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            return;
        }

        // Create and set category provider
        UniTreeProvider categoryProvider = new UniTreeProvider();
        categoryProvider.DisplayNameColumn = "DisplayName";
        categoryProvider.IDColumn = "ObjectID";
        categoryProvider.LevelColumn = "ObjectLevel";
        categoryProvider.OrderColumn = "ObjectType DESC, WidgetCategoryOrder";
        categoryProvider.ParentIDColumn = "ParentID";
        categoryProvider.PathColumn = "ObjectPath";
        categoryProvider.ValueColumn = "ObjectID";
        categoryProvider.ChildCountColumn = "CompleteChildCount";
        categoryProvider.QueryName = "cms.widgetcategory.selectallview";
        categoryProvider.ObjectTypeColumn = "ObjectType";
        categoryProvider.ImageColumn = "WidgetCategoryImagePath";

        // Build where condition       
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        string securityWhere = String.Empty;


        // Security where condition
        if (!SelectGroupWidgets && !currentUser.IsGlobalAdministrator)
        {
            securityWhere = "(WidgetSecurity = 0)"; // Allowed for all        
            if (currentUser.IsAuthenticated())
            {
                securityWhere += " OR (WidgetSecurity = 1)"; // Authenticated
                securityWhere += " OR ((WidgetSecurity = 2) AND (ObjectID IN ( SELECT WidgetID FROM CMS_WidgetRole WHERE RoleID IN (SELECT RoleID FROM CMS_UserRole WHERE UserID = " + currentUser.UserID + "))))";  // Authorized roles
            }
        }

        //  Categories AND widgets
        if (SelectWidgets)
        {
            categoryProvider.WhereCondition = securityWhere;
            treeElem.OnGetImage += new CMSAdminControls_UI_Trees_UniTree.GetImageEventHandler(treeElem_OnGetImage);
        }
        else // Only Categories
        {
            categoryProvider.WhereCondition = "ObjectType = 'widgetcategory'";
            categoryProvider.ChildCountColumn = "WidgetCategoryChildCount";
            categoryProvider.ObjectTypeColumn = "";
            treeElem.DefaultImagePath = GetImageUrl("Objects/CMS_WidgetCategory/list.png");

            // Create WHERE condition which filters categories without widgets
            string where = "";
            where = @"0 <>(SELECT TOP 1 ObjectID FROM 
            (SELECT ObjectPath as WidgetPath, ObjectType, WidgetForUser,WidgetForGroup,WidgetForEditor, WidgetSecurity, ObjectID 
            FROM View_CMS_WidgetCategoryWidget_Joined) AS SUB 
            WHERE SUB.ObjectType = 'widget' AND SUB.WidgetPath LIKE ObjectPath + '%'";

            if (SelectEditorWidgets)
            {
                where += " AND SUB.WidgetForEditor = 1 ";
            }
            else if (SelectUserWidgets)
            {
                where += " AND SUB.WidgetForUser = 1 ";
            }
            else if (SelectGroupWidgets)
            {
                where += " AND SUB.WidgetForGroup = 1";
            }

            if (!string.IsNullOrEmpty(securityWhere))
            {
                where += " AND ( " + securityWhere + ")";
            }

            categoryProvider.WhereCondition = SqlHelperClass.AddWhereCondition(categoryProvider.WhereCondition , where + ")");
        }

        // Set up tree 
        treeElem.ProviderObject = categoryProvider;

        if (SelectWidgets)
        {
            string script = "onclick=\"SelectNode(##NODEID##,'##OBJECTTYPE##', ##PARENTNODEID##);\"";
            treeElem.NodeTemplate = String.Format("<span id=\"##OBJECTTYPE##_##NODEID##\" {0} name=\"treeNode\" class=\"ContentTreeItem\">##ICON## <span class=\"Name\">##NODENAME##</span></span>", script);
            treeElem.SelectedNodeTemplate = String.Format("<span id=\"##OBJECTTYPE##_##NODEID##\" {0} name=\"treeNode\" class=\"ContentTreeItem ContentTreeSelectedItem\">##ICON## <span class=\"Name\">##NODENAME##</span></span>", script);
        }
        else
        {
            treeElem.NodeTemplate = "<span onclick=\"SelectNode(##NODEID##, this);\" class=\"ContentTreeItem\">##ICON## <span class=\"Name\">##NODENAME##</span></span>";
            treeElem.DefaultItemTemplate = "<span onclick=\"SelectNode('recentlyused', this);\" class=\"ContentTreeItem\">##ICON##<span class=\"Name\">##NODENAME##</span></span>";
            treeElem.SelectedDefaultItemTemplate = "<span onclick=\"SelectNode('recentlyused', this);\" class=\"ContentTreeItem ContentTreeSelectedItem\">##ICON##<span class=\"Name\">##NODENAME##</span></span>";
            treeElem.SelectedNodeTemplate = "<span onclick=\"SelectNode(##NODEID##, this);\" class=\"ContentTreeItem ContentTreeSelectedItem\">##ICON## <span class=\"Name\">##NODENAME##</span></span>";

            // Register jquery
            ScriptHelper.RegisterJQuery(this.Page);

            string js = "var selectedItem = $j('.ContentTreeSelectedItem');" +
                "function SelectNode(nodeid, sender){" +
                "selectedItem.removeClass('ContentTreeSelectedItem'); " +
                "selectedItem.addClass('ContentTreeItem');" +
                "selectedItem = $j(sender);" +
                "selectedItem.removeClass('ContentTreeItem'); " +
                "selectedItem.addClass('ContentTreeSelectedItem'); " +
                "document.getElementById('" + this.treeElem.SelectedItemFieldId + "').value = nodeid;" +
                treeElem.GetOnSelectedItemBackEventReference() +
                "}";

            ScriptHelper.RegisterStartupScript(this.Page, typeof(string), "SelectTreeNode", ScriptHelper.GetScript(js));
        }

        // Add last recently used
        if (ShowRecentlyUsed)
        {
            treeElem.AddDefaultItem(ResHelper.GetString("widgets.recentlyused"), "recentlyused", ResolveUrl(GetImageUrl("CMSModules/CMS_Widgets/recentlyused.png")));
        }

        // Setup event handler
        treeElem.OnItemSelected += new CMSAdminControls_UI_Trees_UniTree.ItemSelectedEventHandler(treeElem_OnItemSelected);
    }


    /// <summary>
    /// Page PreRender.
    /// </summary>
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            return;
        }

        // Load data
        if (!RequestHelper.IsPostBack())
        {
            treeElem.ReloadData();
        }
    }


    /// <summary>
    ///  On selected item event.
    /// </summary>
    /// <param name="selectedValue">Selected value.</param>
    protected void treeElem_OnItemSelected(string selectedValue)
    {
        if (OnItemSelected != null)
        {
            OnItemSelected(selectedValue);
        }
    }


    /// <summary>
    /// On get image event.
    /// </summary>
    /// <param name="node">Current node.</param>
    protected string treeElem_OnGetImage(UniTreeNode node)
    {
        if ((node != null) && (node.ItemData != null))
        {
            string objectType = string.Empty;

            DataRow dr = (DataRow)node.ItemData;
            if (dr != null)
            {
                objectType = ValidationHelper.GetString(dr["ObjectType"], "").ToLower();
            }

            // Return image path
            switch (objectType)
            {
                case "widget":
                    return ResolveUrl(GetImageUrl("Objects/CMS_Widget/list.png"));

                case "widgetcategory":
                    return ResolveUrl(GetImageUrl("Objects/CMS_WidgetCategory/list.png"));
            }
        }

        return String.Empty;
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Reloads the tree data
    /// </summary>
    public override void ReloadData()
    {
        treeElem.ReloadData();
        base.ReloadData();
    }

    #endregion
}
