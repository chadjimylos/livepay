<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Password.ascx.cs" Inherits="CMSModules_Membership_FormControls_Passwords_Password" %>
<div>
    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="TextBoxField" MaxLength="100"></asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvPassword" ValidationGroup="ConfirmRegForm" runat="server"
        ControlToValidate="txtPassword" Display="Dynamic"></asp:RequiredFieldValidator>
</div>
