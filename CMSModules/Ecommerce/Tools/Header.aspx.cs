using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Tools_Header : CMSEcommercePage
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        CurrentMaster.Tabs.SelectedTab = QueryHelper.GetInteger("selectedtab", 0);
    }


    protected void Page_Init(object sender, EventArgs e)
    {
        if ((CurrentMaster.Tabs.SelectedTab == 8) || (CurrentMaster.Tabs.SelectedTab == 9))
        {
            CurrentMaster.PanelLeft.CssClass = "FullTabsLeft";
            CurrentMaster.PanelRight.CssClass = "FullTabsRight";
        }
        else
        {
            CurrentMaster.PanelLeft.CssClass = "FullTabsLeft";
            CurrentMaster.PanelRight.CssClass = "FullTabsRight";
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentMaster.Title.TitleText = ResHelper.GetString("Ecomerce.HeaderCaption");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Ecommerce/module.png");

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }

        CurrentMaster.SetRTL();
    }


    /// <summary>
    /// Initializes menu
    /// </summary>
    protected void InitalizeMenu()
    {
        CurrentMaster.Tabs.Tabs = new string[10, 4];
        CurrentMaster.Tabs.Tabs[0, 0] = ResHelper.GetString("Ecommerce.Orders");
        CurrentMaster.Tabs.Tabs[0, 2] = "Orders/Order_List.aspx";
        CurrentMaster.Tabs.Tabs[1, 0] = ResHelper.GetString("Ecommerce.Customers");
        CurrentMaster.Tabs.Tabs[1, 2] = "Customers/Customer_List.aspx";
        CurrentMaster.Tabs.Tabs[2, 0] = ResHelper.GetString("Ecommerce.Products");
        CurrentMaster.Tabs.Tabs[2, 2] = "Products/Product_List.aspx";
        CurrentMaster.Tabs.Tabs[3, 0] = ResHelper.GetString("Ecommerce.ProductOptions");
        CurrentMaster.Tabs.Tabs[3, 2] = "ProductOptions/OptionCategory_List.aspx";
        CurrentMaster.Tabs.Tabs[4, 0] = ResHelper.GetString("Ecommerce.Manufacturers");
        CurrentMaster.Tabs.Tabs[4, 2] = "Manufacturers/Manufacturer_List.aspx";
        CurrentMaster.Tabs.Tabs[5, 0] = ResHelper.GetString("Ecommerce.Suppliers");
        CurrentMaster.Tabs.Tabs[5, 2] = "Suppliers/supplier_list.aspx";
        CurrentMaster.Tabs.Tabs[6, 0] = ResHelper.GetString("Ecommerce.Discounts");
        CurrentMaster.Tabs.Tabs[6, 2] = "DiscountCoupon/Discount_List.aspx";
        CurrentMaster.Tabs.Tabs[7, 0] = ResHelper.GetString("Ecommerce.DiscountLevels");
        CurrentMaster.Tabs.Tabs[7, 2] = "DiscountLevel/DiscountLevel_List.aspx";

        if (ModuleEntry.IsModuleLoaded(ModuleEntry.REPORTING))
        {
            CurrentMaster.Tabs.Tabs[8, 0] = ResHelper.GetString("Ecommerce.Reports");
            CurrentMaster.Tabs.Tabs[8, 2] = "Reports/default.aspx";
        }

        // Check 'EcommerceConfiguration' permission
        if (CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationRead"))
        {
            CurrentMaster.Tabs.Tabs[9, 0] = ResHelper.GetString("Ecommerce.Configuration");
            CurrentMaster.Tabs.Tabs[9, 2] = "Configuration/Configuration_Frameset.aspx";
        }

        CurrentMaster.Tabs.UrlTarget = "ecommerceContent";
    }
}
