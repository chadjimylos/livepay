﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCReceipt.ascx.vb" Inherits="CMSTemplates_LivePay_PopUp_UCReceipt" %>

<style type="text/css" >
@media print
{
    .MasterMain { display:none  }
}
</style>

<script>
    function ExportDetails(url) {
        document.getElementById('<%=exportpopFrame.ClientID %>').src = url;
        return false
    }
</script>
<div style="display:none">
<iframe id="exportpopFrame" runat="server" EnableViewState="false" ></iframe>
</div>
<table cellpadding="0" cellspacing="0" border="0" class="PopUpMainTable" style="width:620px">
    <tr>
	<td class="PopTopLeft">&nbsp;</td>
        <td class="PopTopCenter">&nbsp;</td>
        <td class="PopTopClose"><img src="/App_Themes/LivePay/PopUp/Close.png"  alt="Close" title="Close" style="cursor:pointer;" onclick="$.unblockUI()"/></td>
	<td class="PopTopRight">&nbsp;</td>  
    </tr>
     <tr>
	<td class="PopCenterLeft">&nbsp;</td>
	<td colspan="2" class="PopCenter">
        <table cellpadding="0" cellspacing="0">
            <tr><td class="PopTopTitleSmall" id="TdCompanyTitle" runat="server" EnableViewState="false"></td>
            <td class="PopTopLogo">
            <asp:Image runat="server" ID="imgLogo" EnableViewState="false" />
            </tr>
            <tr><td class="PopTopTitleUnder"  colspan="2">&nbsp;</td></tr>
            <tr><td class="PopContent" colspan="2">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="PopTransRecTitle" runat="server" id="tdPopTransDateTime"></td>
                        <td class="PopTransDetValue" id="tdDtm" runat="server" EnableViewState="false"></td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Πληρωμή προς |en-us=Payment to$}") %></td>
                        <td class="PopTransDetValue" id="tdCompPay" runat="server" EnableViewState="false"></td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Κωδικός Πληρωμής |en-us=Payment Code$}") %></td>
                        <td class="PopTransDetValue" id="tdPayCode" runat="server" EnableViewState="false"></td>
                    </tr>
                    <tr style="display:none">
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Κωδικός Πελάτη |en-us=Customer Code $}") %></td>
                        <td class="PopTransDetValue" id="tdCustCode" runat="server" EnableViewState="false"></td>
                    </tr>
                    <tr>
                        <td colspan="2" width="100%">
                            <table cellpadding="0" cellspacing="0" id="tblCustomFields" runat="server" EnableViewState="false">
                            </table>
                        </td>
                    </tr>
                     <tr>
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Τύπος συναλλαγής|en-us=Transaction Type $}") %></td>
                        <td class="PopTransDetValue" id="tdTransType" runat="server" EnableViewState="false"></td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle"><%= ResHelper.LocalizeString("{$=Ποσό |en-us=Amount $}")%> (&euro;)</td>
                        <td class="PopTransDetValue" id="tdPrice" runat="server" EnableViewState="false"></td>
                    </tr>

                    <tr>
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Κάρτα |en-us=Card $}") %></td>
                        <td class="PopTransDetValue" id="tdCardNo" runat="server" EnableViewState="false"></td>
                    </tr>
                    <asp:PlaceHolder runat="server" Visible="false" ID="plc_Installments" EnableViewState="false">
                        <tr>
                            <td class="PopTransRecTitle"><%= ResHelper.LocalizeString("{$=Αριθμός δόσεων |en-us=Αριθμός δόσεων $}")%></td>
                            <td class="PopTransDetValue" id="tdInstallments" runat="server" EnableViewState="false"></td>
                        </tr>
                    </asp:PlaceHolder>
                    <tr id="ErrosCell" runat="server" visible="false" EnableViewState="false">
                        <td class="PopTransRecTitle"><%= ResHelper.LocalizeString("{$=Αιτιολογία |en-us=Reason $}")%></td>
                        <td class="PopTransDetValue" id="tdErrorMessage" runat="server" EnableViewState="false"></td>
                    </tr>




                    <asp:PlaceHolder runat="server" ID="ph_Invoice" Visible="false" EnableViewState="false">
                       <tr>
                            <td class="PopTopTitleSmall" colspan="2" ><%=ResHelper.LocalizeString("{$=Στοιχεία Τιμολόγησεις |en-us=Invoice Details $}")%></td>
                        </tr>
                       <tr>
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Επωνυμία Επιχείρησης|en-us=Company Name $}")%></td>
                            <td class="PopTransDetValue" id="tdCompanyName" runat="server"></td>
                        </tr>
                        <tr>
                            <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Επάγγελμα |en-us=Profession $}")%></td>
                            <td class="PopTransDetValue" id="tdProfession" runat="server"></td>
                        </tr>
                        <tr>
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Διεύθυνση |en-us=Address $}") %></td>
                            <td class="PopTransDetValue" id="tdAddress" runat="server"></td>
                        </tr>
                        <tr>
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Τ.Κ.|en-us=Postal Code $}")%></td>
                            <td class="PopTransDetValue" id="tdPostalCode" runat="server"></td>
                        </tr>
                        <tr>
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Πόλη |en-us=City $}") %></td>
                            <td class="PopTransDetValue" id="tdCity" runat="server"></td>
                        </tr>
                        <tr>
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=ΑΦΜ |en-us=VAT Number$}") %></td>
                            <td class="PopTransDetValue" id="tdVATNumber" runat="server"></td>
                        </tr>
                        <tr>
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=ΔΟΥ |en-us=Tax office $}")%></td>
                            <td class="PopTransDetValue" id="tdTaxOffice" runat="server"></td>
                        </tr>
                    </asp:PlaceHolder>


                 
                    <tr>
                        <td colspan="2" class="PopTransDetButtons">
                        <asp:LinkButton runat="server" ID="btnSaveToPDF" EnableViewState="false" ><img class="Chand" border="0" src='<%=ResHelper.LocalizeString("{$=/app_themes/LivePay/popup/btnpdf.png|en-us=/app_themes/LivePay/popup/btnpdf_en-us.png$}") %>'  /></asp:LinkButton>
                        &nbsp;
                        <img border="0" id="btnPrint" runat="server" class="Chand" EnableViewState="false" />
                        </td>
                    </tr>
                   
                </table>
            </td></tr>
        </table>
    </td>
	<td class="PopCenterRight">&nbsp;</td>
    </tr>
    <tr>
	<td class="PopBotLeft">&nbsp;</td>
	<td colspan="2" class="PopBotCenter"></td>
	<td class="PopBotRight">&nbsp;</td>
    </tr>
</table>