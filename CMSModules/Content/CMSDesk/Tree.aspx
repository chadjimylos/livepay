<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Tree.aspx.cs" Inherits="CMSModules_Content_CMSDesk_Tree"
    EnableEventValidation="false" Theme="Default" %>

<%@ Register Src="../Controls/TreeContextMenu.ascx" TagName="TreeContextMenu" TagPrefix="cms" %>
<%@ Register Src="../Controls/ContentTree.ascx" TagName="ContentTree" TagPrefix="cms" %>
<%@ Register src="~/CMSAdminControls/UI/Trees/TreeBorder.ascx" tagname="TreeBorder" tagprefix="cms" %>
<%@ Register TagPrefix="cms" Namespace="CMS.ExtendedControls.DragAndDrop" Assembly="CMS.ExtendedControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Content - Tree</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }
    </style>
</head>
<body class="TreeBody <%=mBodyClass%>" onunload="Maximize();" onload="InitFocus();"
    oncontextmenu="return false;">

    <script type="text/javascript" src="tree.js"></script>

    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="manScript" EnableViewState="false" ScriptMode="Release" />
    <asp:Panel runat="server" ID="pnlBody" CssClass="ContentTree" EnableViewState="false">
        <cms:TreeBorder ID="borderElem" runat="server" MinSize="10,*" FramesetName="colsFrameset" />
        <div class="TreeArea" onclick="HideAllContextMenus();" style="background-color: #f9fcfd;">
            <cms:ContextMenu ID="menuNode" runat="server" MenuID="nodeMenu" VerticalPosition="Bottom"
                HorizontalPosition="Left" OffsetX="4" OffsetY="-2" ActiveItemCssClass="TreeContextActiveNode">
                <cms:TreeContextMenu ID="contextMenu" runat="server" />
            </cms:ContextMenu>
            <div class="TreeAreaTree">
                <cms:ContentTree ID="treeContent" runat="server" AllowDragAndDrop="true" />
            </div>
        </div>
    </asp:Panel>
    <script type="text/javascript">
        //<![CDATA[
        var currentNode = document.getElementById('treeSelectedNode');
        //]]>
    </script>
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    <input type="hidden" id="hdnAction" name="hdnAction" />
    <input type="hidden" id="hdnParam1" name="hdnParam1" />
    <input type="hidden" id="hdnParam2" name="hdnParam2" />
    </form>
</body>
</html>
