<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User_Edit_Departments.aspx.cs"
    Inherits="CMSModules_Ecommerce_Administration_Users_User_Edit_Departments" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="User Edit - Departments" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>


<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:PlaceHolder ID="plcTable" runat="server"><strong>
        <cms:LocalizedLabel runat="server" ID="lblDepartmentInfo" DisplayColon="true" ResourceString="com.departments.userdepartments"
            EnableViewState="false" CssClass="InfoLabel" /></strong>
        <cms:UniSelector ID="uniSelector" runat="server" IsLiveSite="false" OrderBy="DepartmentName"
            ObjectType="ecommerce.department" SelectionMode="Multiple" ResourcePrefix="departmentselector" />
    </asp:PlaceHolder>
</asp:Content>
