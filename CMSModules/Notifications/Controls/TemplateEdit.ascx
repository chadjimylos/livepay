<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TemplateEdit.ascx.cs"
    Inherits="CMSModules_Notifications_Controls_TemplateEdit" %>
<cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<cms:LocalizedLabel runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
    Visible="false" />
<table>
    <tr>
        <td>
            <cms:LocalizedLabel runat="server" ID="lblDisplayName" CssClass="ContentLabel" DisplayColon="true"
                EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtDisplayName" CssClass="TextBoxField" MaxLength="250"
                EnableViewState="false" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDisplayName" ID="valDisplayName"
                EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            <cms:LocalizedLabel runat="server" ID="lblCodeName" CssClass="ContentLabel" DisplayColon="true"
                EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox runat="server" ID="txtCodeName" CssClass="TextBoxField" MaxLength="250"
                EnableViewState="false" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtCodeName" ID="valCodeName"
                EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <cms:LocalizedButton runat="server" ID="btnOk" Style="text-align: center;" CssClass="SubmitButton"
                OnClick="btnOK_Click" EnableViewState="false" />
        </td>
    </tr>
</table>
