<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditFolder.ascx.cs" Inherits="CMSModules_MediaLibrary_Controls_MediaLibrary_FolderActions_EditFolder" %>
<asp:Panel ID="pnlFolderEdit" runat="server" EnableViewState="false">
    <ajaxToolkit:TabContainer ID="pnlTabs" runat="server" CssClass="Dialog_Tabs">
        <ajaxToolkit:TabPanel ID="tabGeneral" runat="server">
            <ContentTemplate>
                <asp:Panel ID="pnlContent" runat="server" DefaultButton="btnOk">
                    <table width="100%">
                        <tr>
                            <td colspan="2" class="FolderEditInfoArea">
                                <cms:LocalizedLabel ID="lblInfo" runat="server" DisplayColon="false" Visible="false"
                                    CssClass="InfoLabel" EnableViewState="false"></cms:LocalizedLabel>
                                <cms:LocalizedLabel ID="lblError" runat="server" CssClass="ErrorLabel" DisplayColon="false"
                                    EnableViewState="false" Visible="false"></cms:LocalizedLabel>
                            </td>
                        </tr>
                        <tr>
                            <td style="white-space: nowrap;" class="FolderEditLabelArea">
                                <cms:LocalizedLabel ID="lblFolderName" runat="server" CssClass="FieldLabel" DisplayColon="true"
                                    ResourceString="media.folder.foldername" EnableViewState="false"></cms:LocalizedLabel>
                            </td>
                            <td style="width: 100%">
                                <asp:TextBox ID="txtFolderName" runat="server" CssClass="TextBoxField" MaxLength="50"
                                    EnableViewState="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:RequiredFieldValidator ID="rfvFolderName" runat="server" Display="Dynamic" ControlToValidate="txtFolderName"
                                    ValidationGroup="btnOk" EnableViewState="false"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td colspan="2">
                                <div id="divButtons" runat="server" enableviewstate="false" style="padding-top: 8px;">
                                    <cms:CMSButton ID="btnOk" runat="server" ValidationGroup="btnOk" OnClick="btnOk_Click"
                                        CssClass="SubmitButton" EnableViewState="false" />
                                    <asp:PlaceHolder ID="plcCancelArea" runat="server">
                                        <cms:CMSButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" CssClass="SubmitButton"
                                            EnableViewState="false" />
                                    </asp:PlaceHolder>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
</asp:Panel>
