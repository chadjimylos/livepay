<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MemberList.ascx.cs" Inherits="CMSModules_Groups_Controls_Members_MemberList" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Groups/Controls/Members/MemberFilter.ascx" TagName="MemberFilter"
    TagPrefix="cms" %>
<cms:MemberFilter runat="server" ID="filterMembers" />
<div style="padding-top: 10px">
</div>
<cms:UniGrid runat="server" ID="gridElem" OrderBy="UserName" GridName="~/CMSModules/Groups/Controls/Members/Member_List.xml" />
