using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.CMSHelper;

public partial class CMSModules_Ecommerce_FormControls_ShippingSelector : CMS.FormControls.FormEngineUserControl
{
    #region "Properties

    private bool mAddNoneRecord = false;
    private bool mAutoPostBack = false;
    private StatusEnum mDisplayItems = StatusEnum.Both;
    private ShoppingCartInfo mCart = null;
    private int mSiteID = 0;

    #endregion


    #region "Events"

    /// <summary>
    /// Event raised on dropdownlist selected item changed event
    /// </summary>
    public event EventHandler ShippingChange;

    #endregion


    #region "Public properties"

    // Indicates the site the shipping options should be loaded from, if not set current site is used
    public int SiteID
    {
        get
        {
            if (mSiteID == 0)
            {
                mSiteID = CMSContext.CurrentSiteID;
            }
            return mSiteID;
        }
        set
        {
            mSiteID = value;
        }
    }


    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Indicates which shipping options should be displayed (Enabled/Disabled/Both). Default values is 'Both'.
    /// </summary>
    public StatusEnum DisplayItems
    {
        get
        {
            return mDisplayItems;
        }
        set
        {
            mDisplayItems = value;
        }
    }


    /// <summary>
    /// Add none record to the dropdownlist
    /// </summary>
    public bool AddNoneRecord
    {
        get
        {
            return mAddNoneRecord;
        }
        set
        {
            mAddNoneRecord = value;
        }
    }


    /// <summary>
    /// Determines whether the AutoPostBack property of dropdownlist should be enabled.
    /// </summary>
    public bool AutoPostBack
    {
        get
        {
            return mAutoPostBack;
        }
        set
        {
            mAutoPostBack = value;
        }
    }


    /// <summary>
    /// Shipping ID
    /// </summary>
    public int ShippingID
    {
        get
        {
        	 return ValidationHelper.GetInteger(uniSelector.Value, 0); 
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }
            uniSelector.Value = value; 
        }
    }


    /// <summary>
    /// Shoping Cart
    /// </summary>
    public ShoppingCartInfo ShopingCart
    {
        get
        {
        	 return mCart; 
        }
        set
        {
        	 mCart = value; 
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.ShippingID;
        }
        set
        {
            this.ShippingID = ValidationHelper.GetInteger(value, 0);
        }
    }


    /// <summary>
    /// Returns ClientID of the dropdownlist.
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect.ClientID;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        this.uniSelector.EnabledColumnName = "ShippingOptionEnabled";
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.AllowEmpty = this.AddNoneRecord;
        this.uniSelector.OnSelectionChanged += new EventHandler(uniSelector_OnSelectionChanged);

        string where = null;
        if (mCart == null)
        {
            where = "ShippingOptionSiteID = " + this.SiteID;
        }
        else
        {
            where = "ShippingOptionSiteID = " + this.ShopingCart.ShoppingCartSiteID;
        }
        if (this.DisplayItems == StatusEnum.Disabled)
        {
            where += " AND ShippingOptionEnabled = 0";
        }
        else if (this.DisplayItems == StatusEnum.Enabled)
        {
            where += " AND ShippingOptionEnabled = 1";
        }

        this.uniSelector.WhereCondition = where;
    }


    protected void uniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        RaiseShippingChange(sender, e);
    }


    /// <summary>
    /// Raises OnShippingChange event
    /// </summary>
    protected void RaiseShippingChange(object sender, EventArgs e)
    {
        if (ShippingChange != null)
        {
            ShippingChange(sender, e);
        }
    }
}
