<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/LivePay/Logon/logonform.ascx.cs"
    Inherits="CMSWebParts_LivePay_Logon_logonform" %>

    <style>
        .DialogPosition{
	        margin-top:10px;
        }
        .LogonDialog{

        }
        .logon-form
        {
            font-size:11px;
            color:#fff;
            margin-top:10px;
        }
        .logon-form a
        {
            font-size:11px;
            color:#fff;
        }
        .logon-form-header{
	        background:transparent url(/App_Themes/LivePay/LoginForm/bgHeader.gif) no-repeat;
	        width:174px;
	        height:29px;
        }
        .logon-form-header .label
        {
            font-size:12px;
            font-weight:bold;
            padding:8px 10px 0px 10px;
        }
        .logon-form-body
        {
            background:transparent url(/App_Themes/LivePay/LoginForm/bgBody.gif) repeat-y;
	        width:174px;
	        padding-left:7px;
        }
        .logon-form-username
        {
            padding-top:10px;
        }
        .logon-form-username .label
        {
            float:left;
            width:52px;
            line-height:21px;
            font-weight:bold;
        }
        .logon-form-username .fullname
        {
            font-weight:bold;
        }
        .logon-form-password
        {
            padding-top:5px;
            left:0px;width:100%;
        }
        .logon-form-password .label
        {
            float:left;
            width:52px;
            line-height:21px;
            font-weight:bold;
        }
        .logon-form-links
        {
            padding-top:7px;
        }
        .logon-form-time
        {
            padding-top:7px;
            padding-bottom:7px;
        }
        .logon-form-time .label
        {
            float:left;
        }
        .logon-form-time .time
        {
            float:left;
            padding-left:5px;
        }
        .logon-form-rememberMe
        {
        }
        .logon-form-submit input
        {
            float:right;
            margin-right:14px;
            margin-top:7px;
            margin-bottom:7px;
            border:none;
            font-size:11px;
            color:#fff;
            font-weight:bold;
            background:transparent url(/App_Themes/LivePay/LoginForm/bgButtonSmall.png) no-repeat;
            width:91px;
            height:23px;
            cursor:pointer;
        }
        .logon-form-logout
        {
            float:left;
            padding-left:27px;
            padding-top:10px;
        }
        .logon-form-logout input
        {
            background:transparent url(/App_Themes/LivePay/LoginForm/bgButton.gif) no-repeat;
            width:105px;
            height:23px;
            border:none;
            color:#fff;
            font-weight:bold;
            cursor:pointer;
        }
        .logon-form-bottom{
	        background:transparent url(/App_Themes/LivePay/LoginForm/bgBottom.gif) no-repeat;
	        width:174px;
	        height:6px;
        }
        .logoffImgDiv{
            clear:both;width:150px;margin-top:18px;margin-top:expression(0);text-align:center;
        }
        
        .Logon-fomr-Error-Div{margin-top:3px;color: #d41f00;background-color:#fddcdc;border:2px solid #d41f00; width:150px;min-height:33px;font-size:11px;background-image:url('/App_Themes/LivePay/LoginForm/bgLoginError.png');background-repeat:no-repeat;background-position:center left}
    </style>

    <script language=javascript>
        if (document.location.href.toLowerCase().indexOf('/cms/') == -1) {

            var lTotalSecondsToCountDown = 1200;
            var lCurrentSeconds = 0;

            function lCountDownToAutoRefresh() {
                var btnSignOut = $("input[id$=btnSignOut]")//document.getElementById('ctl00_ctl00_SiteContent_AppCntBody_btnRefreshData');
                if (lCurrentSeconds == lTotalSecondsToCountDown) {
                    btnSignOut.click();
                    lTotalSecondsToCountDown = 60;
                    lCurrentSeconds = 0;
                }
                else {
                    var i = document.getElementById('AutoRefreshCountDown');
                    var interv = lTotalSecondsToCountDown - lCurrentSeconds;
                    if (i) {
                        i.innerHTML = parseInt(interv / 60) + ':' + ((interv % 60).toString().length < 2 ? '0' : '') + (interv % 60)
                        if ((interv / 60) < 17) document.getElementById('logoffImg').src = '/App_Themes/LivePay/LoginForm/logoff4.jpg';
                        if ((interv / 60) < 13) document.getElementById('logoffImg').src = '/App_Themes/LivePay/LoginForm/logoff3.jpg';
                        if ((interv / 60) < 9) document.getElementById('logoffImg').src = '/App_Themes/LivePay/LoginForm/logoff2.jpg';
                        if ((interv / 60) < 5) document.getElementById('logoffImg').src = '/App_Themes/LivePay/LoginForm/logoff1.jpg';
                    }
                    lCurrentSeconds++;
                }

                setTimeout("lCountDownToAutoRefresh()", 1000);
            }

            window.onload = lCountDownToAutoRefresh;
        }
    </script>

<asp:Panel ID="Panel_Logout" runat="server" CssClass="LogoutPageBackground" Visible="false">
    <div class="logon-form">
        <div class="logon-form-header">
            <div class="label"><cms:LocalizedLabel ID="lblHeaderTitleLogout" runat="server" EnableViewState="false" /></div>
        </div>
        <div class="logon-form-body">
            <div class="logon-form-username">
                <div class="fullname"><asp:Label ID="lblFullName" runat="server" CssClass="CurrentUserLabel" EnableViewState="false" /></div>
            </div>
            <div class="logon-form-links">
                <asp:PlaceHolder ID="phOldLinks" runat="server" Visible="false">
                    <div class="label"><asp:LinkButton ID="lnkShort1" runat="server" EnableViewState="false" /></div>
                    <div class="label"><asp:LinkButton ID="lnkShort2" runat="server" EnableViewState="false" /></div>
                    <div class="label"><asp:LinkButton ID="lnkShort3" runat="server" EnableViewState="false" /></div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phUserLinks" runat="server" visible="false">
                    
                    <div class="label"><a href="/ipiresies/istoriko-pliromon.aspx"><%=ResHelper.LocalizeString("{$=�������� ��������|en-us=Payment History$}")%></a></div>
                    <%--<div class="label"><a href="/PaymentHistoryUnavailable.aspx"><%=ResHelper.LocalizeString("{$=�������� ��������|en-us=Payment History$}")%></a></div>--%>
                    <div class="label"><a href="/ChangeUserDetails.aspx"><%=ResHelper.LocalizeString("{$=�������� ������|en-us=User Information$}")%></a></div>
                    <div class="label"><a href="/SavedCards.aspx"><%=ResHelper.LocalizeString("{$=������������� ������|en-us=Saved Cards$}")%></a></div>
                    <div class="label"><a href="/SavedTransactions.aspx"><%=ResHelper.LocalizeString("{$=���������� ����������|en-us=Favourite Payments$}")%></a></div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phMerchantLinks" runat="server" Visible="false">
                    <div class="label"><a href="/Searchhistpayments.aspx"><%=ResHelper.LocalizeString("{$=�������� ��������|en-us=Payment History$}")%></a></div>
                    <div class="label"><a href="/merchants_srchistpayments.aspx"><%=ResHelper.LocalizeString("{$=�������� ����������|en-us=Payment History$}")%></a></div>
                    <div class="label"><a href="/ChangeUserDetails.aspx"><%=ResHelper.LocalizeString("{$=�������� ������|en-us=User Information$}")%></a></div>
                    <div class="label"><a href="/SavedCards.aspx"><%=ResHelper.LocalizeString("{$=������������� ������|en-us=Saved Cards$}")%></a></div>
                    <div class="label"><a href="/SavedTransactions.aspx"><%=ResHelper.LocalizeString("{$=���������� ����������|en-us=Favourite Payments$}")%></a></div>
                </asp:PlaceHolder>
            </div>
            <div class="logon-form-time">
                <div class="label"><%=ResHelper.LocalizeString("{$=���������� ��|en-us=Disconnection in$}")%></div>
                <div class="time" id="AutoRefreshCountDown"></div>
                <div class="logoffImgDiv"><img id="logoffImg" src="/App_Themes/LivePay/LoginForm/logoff5.jpg" /></div>
            </div>
            <div class="logon-form-logout">
                <cms:CMSButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click" CssClass="signoutButton" Text="����������" EnableViewState="false" />
            </div>
        <div class="Clear"></div>
        </div>
        <div class="logon-form-bottom"></div>
    </div>
</asp:Panel>

<asp:Panel ID="Panel1" runat="server" CssClass="LogonPageBackground">
    <div class="logon-form">
        <div class="logon-form-header">
            <div class="label"><cms:LocalizedLabel ID="lblHeaderTitle" runat="server" EnableViewState="false" /></div>
        </div>
        <div class="logon-form-body">
                <asp:Login ID="Login1" runat="server" DestinationPageUrl="~/Default.aspx">
                    <LayoutTemplate>
                        <asp:Panel runat="server" ID="pnlLogin" DefaultButton="LoginButton" Width="167">
                        <div class="logon-form-username" >
                            <div class="label" style="width:62px">
                                <cms:LocalizedLabel ID="lblUserName" runat="server" AssociatedControlID="UserName" EnableViewState="false" />
                            </div>
                            <div class="textbox" >
                                <asp:TextBox ID="UserName" runat="server" MaxLength="100"  style="border:1px solid #3b6db4;width:90px;width:expression(88)" CssClass="LogonTextBox" AutoComplete="Off" />
                                <asp:RequiredFieldValidator ID="rfvUserNameRequired" runat="server" ControlToValidate="UserName"
                                    ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1"
                                    EnableViewState="false">*</asp:RequiredFieldValidator>                           
                            </div>
                        </div>                        
                        <div class="logon-form-password">
                            <div class="label" style="width:62px">
                                <cms:LocalizedLabel ID="lblPassword" runat="server" AssociatedControlID="Password" EnableViewState="false" />
                            </div>
                            <div class="textbox" >
                                <asp:TextBox ID="Password" runat="server" TextMode="Password" style="border:1px solid #3b6db4;width:90px;width:expression(88)" MaxLength="110"  CssClass="LogonTextBox"  AutoComplete="Off"/>                            
                            </div>
                        </div>                                              
                        
                        <cms:LocalizedLiteral ID="FailureText" runat="server" EnableViewState="False" />
                        
                                         
                        <div class="logon-form-submit">
                            <cms:LocalizedButton ID="LoginButton" runat="server" CommandName="Login" ValidationGroup="Login1" EnableViewState="false" />
                        </div>

                        </asp:Panel>
                    </LayoutTemplate>
                </asp:Login>
        <div>
                <asp:LinkButton ID="lnkPasswdRetrieval" runat="server" EnableViewState="false" />
        </div>
        <div><asp:LinkButton ID="lnkMember" runat="server" EnableViewState="false" /></div>

        </div><!-- Class End Body -->
        <div class="logon-form-bottom"></div>

    </div>
</asp:Panel>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
<asp:HiddenField runat="server" ID="hdnPasswDisplayed" />