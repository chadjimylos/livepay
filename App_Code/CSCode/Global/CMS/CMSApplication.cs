using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;

/// <summary>
/// Custom application events
/// </summary>
public static class CMSApplication
{
    /// <summary>
    /// Fires before the application start event
    /// </summary>
    public static void BeforeAplicationStart(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires after the application start event
    /// </summary>
    public static void AfterApplicationStart(object sender, EventArgs e)
    {
        // Add your custom actions
     }


    /// <summary>
    /// Fires before the application end event
    /// </summary>
    public static void BeforeAplicationEnd(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires after the application end event
    /// </summary>
    public static void AfterApplicationEnd(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before the application error event
    /// </summary>
    public static void BeforeAplicationError(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires after the application error event
    /// </summary>
    public static void AfterApplicationError(object sender, EventArgs e)
    {
        // Add your custom actions
    }
}
