CREATE TABLE [CMS_WebPartCategory] (
		[CategoryID]                    int NOT NULL IDENTITY(1, 1),
		[CategoryDisplayName]           nvarchar(100) NOT NULL,
		[CategoryParentID]              int NULL,
		[CategoryName]                  nvarchar(100) NOT NULL,
		[CategoryGUID]                  uniqueidentifier NOT NULL,
		[CategoryLastModified]          datetime NOT NULL,
		[CategoryImagePath]             nvarchar(450) NULL,
		[CategoryPath]                  nvarchar(450) NOT NULL,
		[CategoryOrder]                 int NULL,
		[CategoryLevel]                 int NULL,
		[CategoryChildCount]            int NULL,
		[CategoryWebPartChildCount]     int NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WebPartCategory]
	ADD
	CONSTRAINT [PK_CMS_WebPartCategory]
	PRIMARY KEY
	NONCLUSTERED
	([CategoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_WebPartCategory]
	ADD
	CONSTRAINT [DEFAULT_CMS_WebPartCategory_CategoryPath]
	DEFAULT ('') FOR [CategoryPath]
IF OBJECT_ID(N'[CMS_WebPartCategory]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WebPartCategory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WebPartCategory_CategoryParentID_CMS_WebPartCategory]') IS NULL
BEGIN
		ALTER TABLE [CMS_WebPartCategory]
			ADD CONSTRAINT [FK_CMS_WebPartCategory_CategoryParentID_CMS_WebPartCategory]
			FOREIGN KEY ([CategoryParentID]) REFERENCES [CMS_WebPartCategory] ([CategoryID])
END
CREATE INDEX [IX_CMS_WebPartCategory_CategoryParentID_CategoryOrder]
	ON [CMS_WebPartCategory] ([CategoryParentID], [CategoryOrder])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE UNIQUE CLUSTERED INDEX [IX_CMS_WebPartCategory_CategoryPath]
	ON [CMS_WebPartCategory] ([CategoryPath])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
