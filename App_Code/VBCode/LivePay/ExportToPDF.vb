﻿Imports System.IO
Imports Microsoft.VisualBasic
Imports Winnovative.WnvHtmlConvert

Public Class ExportToPDF
    Private Const PDFConverterLicenseKey As String = "9d7E1cTVxdXB28XVxsTbxMfbzMzMzA=="

    Public Shared Function Create(ByVal url As String) As Byte()
        Dim pdfConv As PdfConverter = New PdfConverter()
        pdfConv.LicenseKey = PDFConverterLicenseKey
        pdfConv.PdfDocumentOptions.BottomMargin = 10
        pdfConv.PdfDocumentOptions.TopMargin = 10
        pdfConv.PdfDocumentOptions.LeftMargin = 10
        pdfConv.PdfDocumentOptions.RightMargin = 10
        Return pdfConv.GetPdfFromUrlBytes(url)
    End Function

    Public Shared Function CreateFromHtmlFile(ByVal filePath As String) As Byte()
        Dim pdfConv As PdfConverter = New PdfConverter()
        pdfConv.LicenseKey = PDFConverterLicenseKey
        pdfConv.PdfDocumentOptions.BottomMargin = 10
        pdfConv.PdfDocumentOptions.TopMargin = 15
        pdfConv.PdfDocumentOptions.LeftMargin = 10
        pdfConv.PdfDocumentOptions.RightMargin = 10
        pdfConv.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4
        Return pdfConv.GetPdfBytesFromHtmlFile(filePath)
    End Function

    Public Shared Function CreateFromHtmlString(ByVal html As String) As Byte()
        Dim pdfConv As PdfConverter = New PdfConverter()
        pdfConv.LicenseKey = PDFConverterLicenseKey
        pdfConv.PdfDocumentOptions.BottomMargin = 10
        pdfConv.PdfDocumentOptions.TopMargin = 15
        pdfConv.PdfDocumentOptions.LeftMargin = 10
        pdfConv.PdfDocumentOptions.RightMargin = 10
        pdfConv.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4
        Return pdfConv.GetPdfBytesFromHtmlString(html, GetBaseUrl())
    End Function

    Public Shared Function CreateFromHtmlString(ByVal html As Stream) As Byte()
        Dim pdfConv As PdfConverter = New PdfConverter()
        pdfConv.LicenseKey = PDFConverterLicenseKey
        pdfConv.PdfDocumentOptions.BottomMargin = 10
        pdfConv.PdfDocumentOptions.TopMargin = 15
        pdfConv.PdfDocumentOptions.LeftMargin = 10
        pdfConv.PdfDocumentOptions.RightMargin = 10
        pdfConv.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4
        Return pdfConv.GetPdfBytesFromHtmlStream(html, System.Text.Encoding.UTF8)
    End Function

    Public Shared Function GetBaseUrl() As String
        Dim Url As String = HttpContext.Current.Request.Url.ToString()
        If Not String.IsNullOrEmpty(HttpContext.Current.Request.Url.Query) Then Url = Url.Replace(HttpContext.Current.Request.Url.Query, String.Empty)
        Dim CurrentPage() As String = Url.Split("/"c)
        Dim Root As String = Url.Replace(CurrentPage(CurrentPage.Length - 1), String.Empty)
        Return Root
    End Function

End Class

