using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_General : CMSEcommercePage
{
    protected int orderId = 0;
    protected int originalStatusId = 0;
    protected int originalCompanyAddressId = 0;
    protected int customerId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "RefreshPageScript", ScriptHelper.GetScript("function RefreshPage() { window.location.replace(window.location.href); }"));
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AddressChange", ScriptHelper.GetScript("   function AddressChange(AddressId) { if (AddressId > 0) { document.getElementById('" + hdnAddress.ClientID + "').value = AddressId; " + ClientScript.GetPostBackEventReference(btnChange, "") + " } } "));

        this.addressElem.DropDownSingleSelect.AutoPostBack = true;
        this.addressElem.DropDownSingleSelect.SelectedIndexChanged += new EventHandler(DropDownSingleSelect_SelectedIndexChanged);

        // controls initialization
        lblOrderId.Text = ResHelper.GetString("order_edit.orderidlabel");
        lblOrderDate.Text = ResHelper.GetString("order_edit.orderdatelabel");
        lblInvoiceNumber.Text = ResHelper.GetString("order_edit.invoicenumberlabel");
        lblStatus.Text = ResHelper.GetString("order_edit.orderstatuslabel");
        lblCustomer.Text = ResHelper.GetString("order_edit.customerlabel");
        lblNotes.Text = ResHelper.GetString("order_edit.ordernotelabel");
        btnEditCustomer.Text = ResHelper.GetString("general.edit");
        btnOk.Text = ResHelper.GetString("general.ok");
        lblCompanyAddress.Text = ResHelper.GetString("order_edit.lblCompanyAddress");
        btnNewAddress.Text = ResHelper.GetString("general.new");
        btnEditAddress.Text = ResHelper.GetString("general.edit");

        // get order ID from url
        orderId = ValidationHelper.GetInteger(Request.QueryString["orderid"], 0);
        // get order info from database and fill the form
        OrderInfo oi = OrderInfoProvider.GetOrderInfo(orderId);

        if (oi != null)
        {
            originalCompanyAddressId = oi.OrderCompanyAddressID;
            originalStatusId = oi.OrderStatusID;
            customerId = oi.OrderCustomerID;

            this.addressElem.CustomerID = customerId;

            // Initialize javascript to button clicks
            btnEditCustomer.OnClientClick = "EditCustomer(" + customerId + "); return false;";
            btnNewAddress.OnClientClick = "AddAddress('" + customerId + "'); return false;";
            if (this.addressElem.AddressID > 0)
            {
                btnEditAddress.OnClientClick = "EditAddress('" + customerId + "','" + this.addressElem.AddressID + "'); return false;";
            }

            if (!RequestHelper.IsPostBack())
            {
                // initialize form
                InitializeForm(oi);

                // show that the Order was updated successfully
                if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                {
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
            }
        }
    }


    protected void DropDownSingleSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnAddress.Value = this.addressElem.AddressID.ToString();
    }


    /// <summary>
    /// Initialize the form with order values.
    /// </summary>
    protected void InitializeForm(OrderInfo orderInfo)
    {
        lblOrderIdValue.Text = Convert.ToString(orderInfo.OrderID);
        orderDate.DateTimeTextBox.Text = Convert.ToString(orderInfo.OrderDate);
        lblInvoiceNumberValue.Text = HTMLHelper.HTMLEncode(Convert.ToString(orderInfo.OrderInvoiceNumber));
        txtNotes.Text = orderInfo.OrderNote;

        CustomerInfo ci = CustomerInfoProvider.GetCustomerInfo(customerId);
        if (ci != null)
        {
            lblCustomerName.Text = HTMLHelper.HTMLEncode(ci.CustomerFirstName + " " + ci.CustomerLastName);
        }

        this.statusElem.OrderStatusID = originalStatusId;
        this.addressElem.AddressID = originalCompanyAddressId;
    }


    /// <summary>
    /// On btnOK button click.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Event argument.</param>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        string errorMessage = ValidateForm();

        if (errorMessage == "")
        {
            OrderInfo oi = OrderInfoProvider.GetOrderInfo(orderId);
            if (oi != null)
            {
                oi.OrderDate = orderDate.SelectedDateTime;
                oi.OrderNote = txtNotes.Text;
                oi.OrderStatusID = this.statusElem.OrderStatusID;
                oi.OrderCompanyAddressID = this.addressElem.AddressID;

                // update orderinfo
                OrderInfoProvider.SetOrderInfo(oi);

                UrlHelper.Redirect("Order_Edit_General.aspx?orderid=" + Convert.ToString(oi.OrderID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                errorMessage = ResHelper.GetString("order_edit.ordernotexist");
            }
        }

        // Show error message
        if (errorMessage != "")
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    /// <summary>
    /// Validates form input fields.
    /// </summary>
    protected string ValidateForm()
    {
        // Validate order date for emptyness
        string errorMessage = new Validator().NotEmpty(orderDate.DateTimeTextBox.Text, ResHelper.GetString("order_edit.dateerr")).Result;

        if (errorMessage == "")
        {
            // Validate order date for wrong format
            if (ValidationHelper.GetDateTime(orderDate.SelectedDateTime, DataHelper.DATETIME_NOT_SELECTED) == DataHelper.DATETIME_NOT_SELECTED)
            {
                errorMessage = ResHelper.GetString("order_edit.datewrongformat");
            }
            if ((originalCompanyAddressId > 0) && (errorMessage == ""))
            {
                if (this.addressElem.AddressID == 0)
                {
                    errorMessage = ResHelper.GetString("order_edit.emptycompanyaddress");
                }
            }
        }

        return errorMessage;
    }
}
