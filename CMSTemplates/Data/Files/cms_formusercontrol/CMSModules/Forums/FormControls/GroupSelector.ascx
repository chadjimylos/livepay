<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupSelector.ascx.cs" Inherits="CMSModules_Forums_FormControls_GroupSelector" %>

<%@ Register Src="~/CMSModules/Forums/FormControls/ForumGroupSelector.ascx" TagName="ForumGroupSelector" TagPrefix="cms" %>

<cms:ForumGroupSelector id="selectGroup" runat="server" SelectionMode="MultipleTextBox" />
