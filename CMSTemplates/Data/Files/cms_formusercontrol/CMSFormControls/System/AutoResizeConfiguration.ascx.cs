using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.FormControls;
using CMS.SettingsProvider;

public partial class CMSFormControls_System_AutoResizeConfiguration : FormEngineUserControl
{
    private XmlData mValue = new XmlData("AutoResize");

    #region "Public properties"

    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return this.elemAutoResize.Enabled;
        }
        set
        {
            this.elemAutoResize.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            this.elemAutoResize.UpdateConfiguration(mValue);            
            return mValue.GetData();
        }
        set
        {
            mValue.LoadData(Convert.ToString(value));            
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Return true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        this.ValidationError = this.elemAutoResize.Validate();
        return (this.ValidationError == "");
    }


    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.elemAutoResize.LoadConfiguration(mValue);
    }

    #endregion
}
