﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Messaging/ContactList.ascx.cs"
    Inherits="CMSWebParts_Messaging_ContactList" %>
<%@ Register Src="~/CMSModules/Messaging/Controls/ContactList.ascx" TagName="ContactList"
    TagPrefix="cms" %>
<cms:ContactList ID="lstContacts" runat="server" />
