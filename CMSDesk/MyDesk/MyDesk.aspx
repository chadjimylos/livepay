<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyDesk.aspx.cs" Theme="default" Inherits="CMSDesk_MyDesk_MyDesk"
 MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="My Desk Description" %>

<%@ Register Src="~/CMSAdminControls/UI/UIProfiles/UIGuide.ascx" TagName="UIGuide" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <uc1:UIGuide ID="guide" runat="server" EnableViewState="false" ModuleName="CMS.MyDesk"
        ModuleAvailabilityForSiteRequired="false" />
</asp:Content>
