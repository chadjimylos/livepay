using System;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Cultures_Culture_Edit_Header : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string currentCulture = "";
        int cultureId = QueryHelper.GetInteger("cultureid", 0);

        CultureInfo ci = CultureInfoProvider.GetCultureInfo(cultureId);
        if (ci != null)
        {
            currentCulture = ci.CultureName;
        }

        // Initialize page title
        string cultures = ResHelper.GetString("general.cultures");
        string title = ResHelper.GetString("culture.edit.properties");

        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = cultures;
        breadcrumbs[0, 1] = "~/CMSSiteManager/Development/Cultures/Culture_List.aspx";
        breadcrumbs[0, 2] = "_parent";
        breadcrumbs[1, 0] = currentCulture;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        this.CurrentMaster.Title.TitleText = title;
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Culture/object.png");
        this.CurrentMaster.Title.HelpTopicName = "general_tabnew";
        this.CurrentMaster.Title.HelpName = "helpTopic";


        // Initialize menu
        string generalString = ResHelper.GetString("general.general");
        string sitesString = ResHelper.GetString("general.sites");

        string[,] tabs = new string[8, 4];
        tabs = new string[2, 4];
        tabs[0, 0] = generalString;
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tabnew');";
        tabs[0, 2] = "Culture_Edit_General.aspx?cultureID=" + cultureId;
        tabs[1, 0] = sitesString;
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'sites_tab4');";
        tabs[1, 2] = "Culture_Edit_Sites.aspx?cultureID=" + cultureId;
        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "content";
    }
}
