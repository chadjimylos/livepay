using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;
using System.Data.SqlClient;
using System.Collections.Generic;

public partial class CMSFormControls_Alico : LivePayFormEngineUserControl
{
    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.AlicoCode.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (IsEmpty())
            {
                return "keno";
            }
            return this.AlicoCode.Text;
        }
        set
        {
            this.AlicoCode.Text = (string)value;
        }
    }


    /// <summary>
    /// Clears current value
    /// </summary>
    //public void Clear()
    //{
    //    IDTransaction.Text = "";
    //}


    /// <summary>
    /// Returns true if the number is empty
    /// </summary>
    public bool IsEmpty()
    {
        return (DataHelper.IsEmpty(this.AlicoCode.Text));
    }


    /// <summary>
    /// Returns true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        if (IsEmpty() && this.Mandatory)
        {
            return false;
        }

        Validator val = new Validator();
        //� ������� �������� � ������ ����������� ��� 18 ����� �� ��� ������ �� 18o ����� ����� �� check digit.
        AlicoCode.Text = Right("000000000000000000" + AlicoCode.Text, 18);
        string result = val.IsRegularExp(AlicoCode.Text, @"\d{18}", "error").Result;

        if (result != "")
        {
            return false;
        }

        //12� - 17� ���� �������� ��� ���������� ������� (��� ����� ������)
        Int16 dd = Convert.ToInt16(Mid(AlicoCode.Text, 11, 2));
        Int16 MM = Convert.ToInt16(Mid(AlicoCode.Text, 13, 2));
        Int16 yy = Convert.ToInt16(Mid(AlicoCode.Text, 15, 2));
        DateTime d = new DateTime();
        Int32 dateNow =  Convert.ToInt32(DateTime.Now.ToString("yyMMdd"));
        DateTime expDate = d.AddDays(dd).AddMonths(MM - 1).AddYears(yy - 1);
        //��� �� ������ �� ������� ����� � ������� �������������� ���� ��� 60 ������ ��� ��� ���������� �������.
        Int32 MaxDate = Convert.ToInt32(expDate.AddDays(60).ToString("yyMMdd"));
        
        if (MaxDate < dateNow)
            return false;

        int factor = 3;
        int sum = 0;
        for (int index = AlicoCode.Text.Length - 1; index > 0; --index)
        {
            sum = sum + Convert.ToInt32(AlicoCode.Text.Substring(index - 1, 1)) * factor;
            factor = 4 - factor;
        }
        int cc = ((1000 - sum) % 10);

        
        if (cc == Convert.ToInt16(AlicoCode.Text.Substring(17, 1)))
        {
            return true;
        }
        else { return false; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomCloud_div.InnerHtml = string.Concat("<a id='CustomCloud_", this.RowCustomField, "' href='#'></a>");

        cv_AlicoCode.Attributes.Add("ControlValidated", AlicoCode.ClientID);
        cv_AlicoCode.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", this.RowCustomField));
        cv_AlicoCode.Attributes.Add("CustomValidatorErrorMessage", this.ValidationError);        
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }
}

