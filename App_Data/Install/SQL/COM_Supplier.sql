CREATE TABLE [COM_Supplier] (
		[SupplierID]               int NOT NULL IDENTITY(1, 1),
		[SupplierDisplayName]      nvarchar(50) NOT NULL,
		[SupplierPhone]            nvarchar(50) NOT NULL,
		[SupplierEmail]            nvarchar(200) NOT NULL,
		[SupplierFax]              nvarchar(50) NOT NULL,
		[SupplierEnabled]          bit NOT NULL,
		[SupplierGUID]             uniqueidentifier NOT NULL,
		[SupplierLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_Supplier]
	ADD
	CONSTRAINT [PK_COM_Supplier]
	PRIMARY KEY
	NONCLUSTERED
	([SupplierID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_Supplier_SupplierDisplayName_SupplierEnabled]
	ON [COM_Supplier] ([SupplierDisplayName], [SupplierEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
