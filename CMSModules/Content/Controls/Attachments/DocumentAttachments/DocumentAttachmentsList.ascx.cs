using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;

using CMS.CMSHelper;
using CMS.FileManager;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.FormControls;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_Content_Controls_Attachments_DocumentAttachments_DocumentAttachmentsList : AttachmentsControl
{
    #region "Variables"

    private string mInnerDivClass = "NewAttachment";
    private bool dataReloaded = false;
    private int? mFilterLimit = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Minimal count of entries for display filter.
    /// </summary>
    public int FilterLimit
    {
        get
        {
            if (this.mFilterLimit == null)
            {
                this.mFilterLimit = ValidationHelper.GetInteger(SettingsHelper.AppSettings["CMSDefaultListingFilterLimit"], 25);
            }
            return (int)this.mFilterLimit;
        }
        set
        {
            this.mFilterLimit = value;
        }
    }


    /// <summary>
    /// CSS class of the new attachment link
    /// </summary>
    public string InnerDivClass
    {
        get
        {
            return mInnerDivClass;
        }
        set
        {
            mInnerDivClass = value;
            if (lblDisabled != null)
            {
                lblDisabled.CssClass = value + "Disabled";
            }
        }
    }


    /// <summary>
    /// Info label
    /// </summary>
    public Label InfoLabel
    {
        get
        {
            return lblInfo;
        }
    }


    /// <summary>
    /// Indicates whether grouped attachments should be displayed
    /// </summary>
    public override Guid GroupGUID
    {
        get
        {
            return base.GroupGUID;
        }
        set
        {
            base.GroupGUID = value;
            if ((dsAttachments != null) && (newAttachmentElem != null))
            {
                dsAttachments.AttachmentGroupGUID = value;
                newAttachmentElem.AttachmentGroupGUID = value;
            }
        }
    }


    /// <summary>
    /// Indicates if user is allowed to change the order of the attachments
    /// </summary>
    public override bool AllowChangeOrder
    {
        get
        {
            return base.AllowChangeOrder;
        }
        set
        {
            base.AllowChangeOrder = value;
        }
    }


    /// <summary>
    /// Indicates if paging is allowed
    /// </summary>
    public override bool AllowPaging
    {
        get
        {
            return base.AllowPaging;
        }
        set
        {
            base.AllowPaging = value;
            if (gridAttachments != null)
            {
                gridAttachments.GridView.AllowPaging = value;
            }
        }
    }


    /// <summary>
    /// Defines size of the page for paging
    /// </summary>
    public override string PageSize
    {
        get
        {
            return base.PageSize;
        }
        set
        {
            base.PageSize = value;
            if (gridAttachments != null)
            {
                gridAttachments.PageSize = value;
            }
        }
    }


    /// <summary>
    /// Width of the attachment
    /// </summary>
    public override int ResizeToWidth
    {
        get
        {
            return base.ResizeToWidth;
        }
        set
        {
            base.ResizeToWidth = value;
            if (newAttachmentElem != null)
            {
                newAttachmentElem.ResizeToWidth = value;
            }
        }
    }


    /// <summary>
    /// Height of the attachment
    /// </summary>
    public override int ResizeToHeight
    {
        get
        {
            return base.ResizeToHeight;
        }
        set
        {
            base.ResizeToHeight = value;
            if (newAttachmentElem != null)
            {
                newAttachmentElem.ResizeToHeight = value;
            }
        }
    }


    /// <summary>
    /// Maximal side size of the attachment
    /// </summary>
    public override int ResizeToMaxSideSize
    {
        get
        {
            return base.ResizeToMaxSideSize;
        }
        set
        {
            base.ResizeToMaxSideSize = value;
            if (newAttachmentElem != null)
            {
                newAttachmentElem.ResizeToMaxSideSize = value;
            }
        }
    }


    /// <summary>
    /// List of allowed extensions
    /// </summary>
    public override string AllowedExtensions
    {
        get
        {
            return base.AllowedExtensions;
        }
        set
        {
            base.AllowedExtensions = value;
            if (newAttachmentElem != null)
            {
                newAttachmentElem.AllowedExtensions = value;
            }
        }
    }


    /// <summary>
    /// Specifies the document for which the attachments should be displayed
    /// </summary>
    public override int DocumentID
    {
        get
        {
            return base.DocumentID;
        }
        set
        {
            base.DocumentID = value;
            if (newAttachmentElem != null)
            {
                newAttachmentElem.DocumentID = value;
            }
        }
    }


    /// <summary>
    /// Specifies the version of the document (optional)
    /// </summary>
    public override int VersionHistoryID
    {
        get
        {
            int versionHistoryId = base.VersionHistoryID;
            if ((versionHistoryId == 0) && (Node != null))
            {
                versionHistoryId = Node.DocumentCheckedOutVersionHistoryID;
            }
            return versionHistoryId;
        }
        set
        {
            base.VersionHistoryID = value;
            if ((dsAttachments != null) && UsesWorkflow)
            {
                dsAttachments.DocumentVersionHistoryID = value;
            }
        }
    }


    /// <summary>
    /// Defines the form GUID; indicates that the temporary attachment will be handled
    /// </summary>
    public override Guid FormGUID
    {
        get
        {
            return base.FormGUID;
        }
        set
        {
            base.FormGUID = value;
            if ((dsAttachments != null) && (newAttachmentElem != null))
            {
                dsAttachments.AttachmentFormGUID = value;
                newAttachmentElem.FormGUID = value;
            }
        }
    }


    /// <summary>
    /// If true, control does not process the data
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return base.StopProcessing;
        }
        set
        {
            base.StopProcessing = value;
            if ((dsAttachments != null) && (newAttachmentElem != null))
            {
                dsAttachments.StopProcessing = value;
                newAttachmentElem.StopProcessing = value;
            }
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (dsAttachments == null)
        {
            StopProcessing = true;
        }
        Visible = !StopProcessing;

        if (StopProcessing)
        {
            if (dsAttachments != null)
            {
                dsAttachments.StopProcessing = true;
            }
            if (newAttachmentElem != null)
            {
                newAttachmentElem.StopProcessing = true;
            }
            // Do nothing
        }
        else
        {
            // Register script for tooltips
            if (ShowTooltip)
            {
                ScriptHelper.RegisterTooltip(Page);
            }

            // Ensure info message
            if ((Request["__EVENTTARGET"] == hdnPostback.ClientID) || Request["__EVENTTARGET"] == hdnFullPostback.ClientID)
            {
                string action = (string)Request["__EVENTARGUMENT"];

                switch (action)
                {
                    case "insert":
                        lblInfo.Text = ResHelper.GetString("attach.inserted");
                        break;

                    case "update":
                        lblInfo.Text = ResHelper.GetString("attach.updated");
                        break;

                    case "delete":
                        lblInfo.Text = ResHelper.GetString("attach.deleted");
                        break;

                    default:
                        if (action != "")
                        {
                            UpdateEditParameters(action);
                        }
                        break;
                }
            }

            #region "Scripts"

            // Refresh script
            string refreshScript = "function RefreshUpdatePanel_" + ClientID + @"(hiddenFieldID, action) {
                        var hiddenField = document.getElementById(hiddenFieldID);
                        if (hiddenField) {
                            __doPostBack(hiddenFieldID, action);
                        }
                    }

                    function FullRefresh(hiddenFieldID, action) {
                        if(PassiveRefresh != null)
                        {
                            PassiveRefresh();
                        }

                        var hiddenField = document.getElementById(hiddenFieldID);
                        if (hiddenField) {
                            __doPostBack(hiddenFieldID, action);
                        }
                    }

                    function FullPageRefresh_" + ClientID + @"(guid) {
                        if(PassiveRefresh != null)
                        {
                            PassiveRefresh();
                        }

                        var hiddenField = document.getElementById('" + hdnFullPostback.ClientID + "');" +
                            @"if (hiddenField) {
                            __doPostBack('" + hdnFullPostback.ClientID + "', 'refresh|' + guid);" +
                            @"}
                    }";

            // Initialize refresh script for update panel
            string initRefreshScript =
                "function InitRefresh_" + ClientID + "(msg, fullRefresh, action)\n" +
                "{\n" +
                "   if((msg != null) && (msg != \"\")){ alert(msg); action='error'; }\n" +
                "   if(fullRefresh){ FullRefresh('" + hdnFullPostback.ClientID + "', action); }\n" +
                "   else { RefreshUpdatePanel_" + ClientID + "('" + hdnPostback.ClientID + "', action); }\n" +
                "}\n";

            // Initialize deletion confirmation
            string deleteConfirmation = "function DeleteConfirmation(){var conf = confirm('" +
                                        ResHelper.GetString("attach.deleteconfirmation") + "'); return conf;}\n";

            #endregion

            ScriptHelper.RegisterClientScriptBlock(this, GetType(), "AttachmentScripts_" + Guid.NewGuid().ToString(), ScriptHelper.GetScript(refreshScript + initRefreshScript + deleteConfirmation));

            // Register dialog script
            ScriptHelper.RegisterDialogScript(Page);

            // Register jQuery script for thumbnails updating
            ScriptHelper.RegisterJQuery(Page);

            // Initialize button for adding attachments
            newAttachmentElem.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/upload_new.png"));
            newAttachmentElem.ImageWidth = 200;
            newAttachmentElem.ImageHeight = 16;
            newAttachmentElem.DocumentID = DocumentID;
            newAttachmentElem.NodeParentNodeID = NodeParentNodeID;
            newAttachmentElem.NodeClassName = NodeClassName;
            newAttachmentElem.ResizeToWidth = ResizeToWidth;
            newAttachmentElem.ResizeToHeight = ResizeToHeight;
            newAttachmentElem.ResizeToMaxSideSize = ResizeToMaxSideSize;
            newAttachmentElem.AttachmentGroupGUID = GroupGUID;
            newAttachmentElem.FormGUID = FormGUID;
            newAttachmentElem.AllowedExtensions = AllowedExtensions;
            newAttachmentElem.ParentElemID = ClientID;
            newAttachmentElem.ForceLoad = true;
            newAttachmentElem.InnerDivHtml = ResHelper.GetString("attach.newattachment");
            newAttachmentElem.InnerDivClass = InnerDivClass;
            newAttachmentElem.InnerLoadingDivHtml = ResHelper.GetString("attach.loading");
            newAttachmentElem.InnerLoadingDivClass = InnerLoadingDivClass;
            newAttachmentElem.IsLiveSite = IsLiveSite;
            newAttachmentElem.CheckPermissions = CheckPermissions;
            lblDisabled.CssClass = InnerDivClass + "Disabled";

            imgDisabled.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/upload_newdisabled.png"));

            // Grid initialization
            gridAttachments.OnExternalDataBound += GridDocsOnExternalDataBound;
            gridAttachments.OnAction += GridAttachmentsOnAction;
            gridAttachments.OnPageChanged += new EventHandler<EventArgs>(gridAttachments_OnPageChanged);
            gridAttachments.OnBeforeDataReload += new OnBeforeDataReload(gridAttachments_OnBeforeDataReload);

            pnlGrid.Attributes.Add("style", "margin-top: 5px;");
        }
    }


    protected void gridAttachments_OnBeforeDataReload()
    {
        gridAttachments.IsLiveSite = IsLiveSite;
        // Ensure right row order
        gridAttachments.OrderBy = "AttachmentOrder, AttachmentName, AttachmentID";
        // Bind UniGrid to DataSource
        gridAttachments.PageSize = PageSize;
        gridAttachments.GridView.AllowPaging = AllowPaging;
        if (!AllowPaging)
        {
            gridAttachments.PageSize = "0";
        }
        gridAttachments.GridView.AllowSorting = false;
    }


    /// <summary>
    /// Updates parameters used by Edit button when displaying image editor.
    /// </summary>
    /// <param name="action"></param>
    private void UpdateEditParameters(string action)
    {
        if (ShowTooltip)
        {
            // Try to get attachment Guid
            Guid attGuid = ValidationHelper.GetGuid(action, Guid.Empty);
            if (attGuid != null)
            {
                // Get attachment
                AttachmentInfo attInfo = AttachmentManager.GetAttachmentInfo(attGuid, SiteName);
                if (attInfo != null)
                {
                    // Get attachment URL
                    string attachmentUrl = null;
                    if (Node != null)
                    {
                        attachmentUrl = ResolveUrl(TreePathUtils.GetAttachmentUrl(attGuid, UrlHelper.GetSafeFileName(attInfo.AttachmentName, CMSContext.CurrentSiteName), VersionHistoryID, "getattachment"));
                    }
                    else
                    {
                        attachmentUrl = ResolveUrl(DocumentHelper.GetAttachmentUrl(attGuid, VersionHistoryID));
                    }
                    attachmentUrl = UrlHelper.UpdateParameterInUrl(attachmentUrl, "chset", Guid.NewGuid().ToString());

                    // Ensure correct URL
                    if (SiteName != CMSContext.CurrentSiteName)
                    {
                        attachmentUrl = UrlHelper.AddParameterToUrl(attachmentUrl, "sitename", SiteName);
                    }

                    // Ensure max width
                    int attachmentWidth = attInfo.AttachmentImageWidth;
                    if (attachmentWidth > 300)
                    {
                        attachmentWidth = 300;
                    }

                    // Generate new tooltip command
                    string newToolTip = "Tip('<div style=\\'width:" + attachmentWidth + "px; text-align:center;\\'><img src=\\'" +
                        UrlHelper.AddParameterToUrl(attachmentUrl, "width", "300") + "\\' alt=\\'" + attInfo.AttachmentName + "\\' /></div>')";

                    // Get update script
                    string updateScript = "$j(\"#" + attGuid + "\").attr('onmouseover', '').unbind('mouseover').mouseover(function(){ " + newToolTip + " });";

                    // Execute update
                    ScriptHelper.RegisterStartupScript(Page, typeof(Page), "AttachmentUpdateEdit", ScriptHelper.GetScript(updateScript));
                }
            }
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            // Load data
            ReloadData();

            lblError.Visible = (lblError.Text != "");
            lblInfo.Visible = (lblInfo.Text != "");

            // Ensure uplaoder button
            plcUploader.Visible = Enabled;
            plcUploaderDisabled.Visible = !Enabled;

            // Hide actions
            gridAttachments.GridView.Columns[0].Visible = !HideActions;
            gridAttachments.GridView.Columns[1].Visible = !HideActions;
            newAttachmentElem.Visible = !HideActions && Enabled;
            plcUploaderDisabled.Visible = !HideActions && !Enabled;

            if (!RequestHelper.IsPostBack())
            {
                // Hide filter
                if ((FilterLimit > 0) && (gridAttachments.RowsCount <= FilterLimit))
                {
                    pnlFilter.Visible = false;
                }
                else
                {
                    pnlFilter.Visible = true;
                }
            }

            // Ensure correct layout
            bool gridHasData = !DataHelper.DataSourceIsEmpty(gridAttachments.DataSource);
            Visible = gridHasData || !HideActions || pnlFilter.Visible;
            pnlGrid.Visible = gridHasData || pnlFilter.Visible;

            lblNoData.Visible = (!gridHasData && pnlFilter.Visible);

            // Dialog for editing image
            string editImageScript =
                "function EditImage_" + ClientID + "(attachmentGUID, formGUID, versionHistoryID, parentId, hash) { " +
                    "var form = ''; \n" +
                    "if(formGUID != ''){ form = '&formguid=' + formGUID + '&parentid=' + parentId;}" +
                    ((Node != null) ? "else{form = '&siteid=' + " + Node.NodeSiteID + ";}" : "") +
                    "modalDialog('" + ResolveUrl((IsLiveSite ? "~/CMSFormControls/LiveSelectors/ImageEditor.aspx" : "~/CMSModules/Content/CMSDesk/Edit/ImageEditor.aspx") + "?attachmentGUID=' + attachmentGUID + '&versionHistoryID=' + versionHistoryID + form + '&clientid=" + ClientID + "&hash=' + hash") + ", 'imageEditorDialog', 905, 670); " +
                    "return false; }\n";

            // Register script for image editing
            ScriptHelper.RegisterClientScriptBlock(this, GetType(), "AttachmentEditScripts_" + ClientID, ScriptHelper.GetScript(editImageScript));
        }
    }

    #endregion


    #region "Public methods"

    /// <summary>
    /// Indicates if the control contains some data
    /// </summary>
    public override bool HasData()
    {
        if (dsAttachments != null)
        {
            return !DataHelper.DataSourceIsEmpty(dsAttachments.DataSource);
        }
        else
        {
            return false;
        }
    }

    #endregion


    #region "Private & protected methods"

    private string GetWhereCondition()
    {
        string where = null;
        if (!string.IsNullOrEmpty(txtFilter.Text))
        {
            where = "AttachmentName LIKE '%" + txtFilter.Text.Replace("'", "''") + "%'";
        }
        return where;
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        if (!dataReloaded)
        {
            dataReloaded = true;
            if (Node != null)
            {
                dsAttachments.Path = Node.NodeAliasPath;
                dsAttachments.CultureCode = CMSContext.PreferredCultureCode;

                SiteInfo si = SiteInfoProvider.GetSiteInfo(Node.NodeSiteID);
                SiteName = si.SiteName;
                dsAttachments.SiteName = SiteName;
            }

            // Versioned attachments
            bool attachmentHistory = (Node != null) && UsesWorkflow && (VersionHistoryID > 0);

            if (UsesWorkflow)
            {
                dsAttachments.DocumentVersionHistoryID = VersionHistoryID;
            }
            dsAttachments.AttachmentGroupGUID = GroupGUID;
            dsAttachments.AttachmentFormGUID = FormGUID;
            dsAttachments.OrderBy = "AttachmentOrder, AttachmentName, " + (attachmentHistory ? "AttachmentHistoryID" : "AttachmentID");
            dsAttachments.SelectedColumns = (attachmentHistory ? "AttachmentHistoryID" : "AttachmentID") + ", AttachmentGUID, AttachmentImageWidth, AttachmentExtension, AttachmentName, AttachmentSize, AttachmentOrder";
            dsAttachments.TopN = gridAttachments.GridView.PageSize * (gridAttachments.GridView.PageIndex + 1 + gridAttachments.GridView.PagerSettings.PageButtonCount);
            dsAttachments.WhereCondition = GetWhereCondition();

            // Ensure right column name (for attachments under workflow)
            if (!DataHelper.DataSourceIsEmpty(dsAttachments.DataSource))
            {
                DataSet ds = (DataSet)dsAttachments.DataSource;
                if (ds != null)
                {
                    DataTable dt = (ds).Tables[0];
                    if (!dt.Columns.Contains("AttachmentFormGUID"))
                    {
                        dt.Columns.Add("AttachmentFormGUID");
                    }
                }
            }

            gridAttachments.DataSource = dsAttachments.DataSource;
            gridAttachments.ReloadData();
        }
    }


    void gridAttachments_OnPageChanged(object sender, EventArgs e)
    {
        ReloadData();
    }


    /// <summary>
    /// UniGrid action buttons event handler.
    /// </summary>
    protected void GridAttachmentsOnAction(string actionName, object actionArgument)
    {
        if (Enabled && !HideActions)
        {
            // Check the permissions
            #region "Check permissions"

            if (CheckPermissions)
            {
                if (FormGUID != Guid.Empty)
                {
                    if (!RaiseOnCheckPermissions("Create", this))
                    {
                        if (!CMSContext.CurrentUser.IsAuthorizedToCreateNewDocument(NodeParentNodeID, NodeClassName))
                        {
                            lblError.Text = ResHelper.GetString("attach.actiondenied");
                            return;
                        }
                    }
                }
                else
                {
                    if (!RaiseOnCheckPermissions("Modify", this))
                    {
                        if (CMSContext.CurrentUser.IsAuthorizedPerDocument(Node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                        {
                            lblError.Text = ResHelper.GetString("attach.actiondenied");
                            return;
                        }
                    }
                }
            }

            #endregion

            int attachmentId = 0;
            Guid attachmentGuid = Guid.Empty;

            // Get action argument (Guid or int)
            if (ValidationHelper.IsGuid(actionArgument))
            {
                attachmentGuid = ValidationHelper.GetGuid(actionArgument, Guid.Empty);
            }
            if (ValidationHelper.IsInteger(actionArgument))
            {
                attachmentId = ValidationHelper.GetInteger(actionArgument, 0);
            }

            // Process proper action
            switch (actionName.ToLower())
            {
                case "moveup":
                    if (attachmentGuid != Guid.Empty)
                    {
                        // Move attachment up
                        if (FormGUID == Guid.Empty)
                        {
                            // Ensure automatic check-in/ check-out
                            VersionManager vm = null;

                            // Check out the document
                            if (AutoCheck && UsesWorkflow)
                            {
                                vm = new VersionManager(TreeProvider);
                                vm.CheckOut(Node);
                            }

                            DocumentHelper.MoveAttachmentUp(attachmentGuid, Node);

                            // Ensure refresh
                            if (UsesWorkflow && AutoCheck)
                            {
                                ScriptHelper.RegisterStartupScript(Page, typeof(Page), "moveUpRefresh", ScriptHelper.GetScript("InitRefresh_" + ClientID + "('', true, 'moveup');"));
                            }

                            // Check in the document
                            if (AutoCheck && UsesWorkflow)
                            {
                                if (vm != null)
                                {
                                    vm.CheckIn(Node, null, null);
                                    VersionHistoryID = Node.DocumentCheckedOutVersionHistoryID;
                                }
                            }

                            // Log synchronization task if not under workflow
                            if (!UsesWorkflow)
                            {
                                DocumentHelper.LogSynchronization(Node, CMS.Staging.TaskTypeEnum.UpdateDocument, TreeProvider);
                            }
                        }
                        else
                        {
                            AttachmentManager.MoveAttachmentUp(attachmentGuid, 0);
                        }
                    }
                    break;

                case "movedown":
                    if (attachmentGuid != Guid.Empty)
                    {
                        // Move attachment down
                        if (FormGUID == Guid.Empty)
                        {
                            // Ensure automatic check-in/ check-out
                            VersionManager vm = null;

                            // Check out the document
                            if (AutoCheck && UsesWorkflow)
                            {
                                vm = new VersionManager(TreeProvider);
                                vm.CheckOut(Node);
                            }

                            DocumentHelper.MoveAttachmentDown(attachmentGuid, Node);

                            // Ensure refresh
                            if (UsesWorkflow && AutoCheck)
                            {
                                ScriptHelper.RegisterStartupScript(Page, typeof(Page), "moveDownRefresh", ScriptHelper.GetScript("InitRefresh_" + ClientID + "('', true, 'movedown');"));
                            }

                            // Check in the document
                            if (AutoCheck && UsesWorkflow)
                            {
                                if (vm != null)
                                {
                                    vm.CheckIn(Node, null, null);
                                    VersionHistoryID = Node.DocumentCheckedOutVersionHistoryID;
                                }
                            }

                            // Log synchronization task if not under workflow
                            if (!UsesWorkflow)
                            {
                                DocumentHelper.LogSynchronization(Node, CMS.Staging.TaskTypeEnum.UpdateDocument, TreeProvider);
                            }
                        }
                        else
                        {
                            AttachmentManager.MoveAttachmentDown(attachmentGuid, 0);
                        }
                    }
                    break;

                case "delete":
                    if (attachmentGuid != Guid.Empty)
                    {
                        // Delete attachment
                        if (FormGUID == Guid.Empty)
                        {
                            // Ensure automatic check-in/ check-out
                            VersionManager vm = null;

                            // Check out the document
                            if (AutoCheck && UsesWorkflow)
                            {
                                vm = new VersionManager(TreeProvider);
                                vm.CheckOut(Node);
                            }

                            DocumentHelper.DeleteAttachment(Node, attachmentGuid, TreeProvider);

                            // Ensure full page refresh
                            if (UsesWorkflow && AutoCheck)
                            {
                                ScriptHelper.RegisterStartupScript(Page, typeof(Page), "deleteRefresh", ScriptHelper.GetScript("InitRefresh_" + ClientID + "('', true, 'delete');"));
                            }

                            // Check in the document
                            if (AutoCheck && UsesWorkflow)
                            {
                                if (vm != null)
                                {
                                    vm.CheckIn(Node, null, null);
                                    VersionHistoryID = Node.DocumentCheckedOutVersionHistoryID;
                                }
                            }

                            // Log synchronization task if not under workflow
                            if (!UsesWorkflow)
                            {
                                DocumentHelper.LogSynchronization(Node, CMS.Staging.TaskTypeEnum.UpdateDocument, TreeProvider);
                            }
                        }
                        else
                        {
                            AttachmentManager.DeleteTemporaryAttachment(attachmentGuid, CMSContext.CurrentSiteName);
                        }

                        lblInfo.Text = ResHelper.GetString("attach.deleted");
                    }
                    break;
            }
        }
    }


    /// <summary>
    /// UniGrid external data bound.
    /// </summary>
    protected object GridDocsOnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "update":
                PlaceHolder plcUpd = new PlaceHolder();
                plcUpd.ID = "plcUdateAction";

                // Add disabled image
                ImageButton imgUpdate = new ImageButton();
                imgUpdate.ID = "imgUpdate";
                imgUpdate.PreRender += imgUpdate_PreRender;
                plcUpd.Controls.Add(imgUpdate);

                // Add update control
                // Dynamically load uploader control
                DirectFileUploader dfuElem = Page.LoadControl("~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploader.ascx") as DirectFileUploader;

                // Set uploader's properties
                if (dfuElem != null)
                {
                    dfuElem.SourceType = CMS.ExtendedControls.MediaSourceEnum.DocumentAttachments;
                    dfuElem.ID = "dfuElem" + DocumentID;
                    dfuElem.ControlGroup = "update";
                    dfuElem.AttachmentGUID = GetAttachmentGuid(sender as DataControlFieldCell);
                    dfuElem.PreRender += dfuElem_PreRender;
                    plcUpd.Controls.Add(dfuElem);
                }
                return plcUpd;

            case "edit":
                // Get file extension
                string extension = ValidationHelper.GetString(((DataRowView)((GridViewRow)parameter).DataItem).Row["AttachmentExtension"], string.Empty).ToLower();
                Guid guid = ValidationHelper.GetGuid(((DataRowView)((GridViewRow)parameter).DataItem).Row["AttachmentGUID"], Guid.Empty);

                ImageButton img = (ImageButton)sender;
                img.AlternateText = extension + "|" + guid;
                img.PreRender += img_PreRender;
                break;

            case "delete":
                ImageButton imgDelete = (ImageButton)sender;
                // Turn off validation
                imgDelete.CausesValidation = false;
                imgDelete.PreRender += imgDelete_PreRender;
                // Explicitly initialize confirmation
                imgDelete.OnClientClick = "if(DeleteConfirmation() == false){return false;}";
                break;

            case "moveup":
                ImageButton imgUp = (ImageButton)sender;
                // Turn off validation
                imgUp.CausesValidation = false;
                imgUp.PreRender += imgUp_PreRender;
                break;

            case "movedown":
                ImageButton imgDown = (ImageButton)sender;
                // Turn off validation
                imgDown.CausesValidation = false;
                imgDown.PreRender += imgDown_PreRender;
                break;

            case "attachmentname":
                {
                    DataControlFieldCell cell = sender as DataControlFieldCell;

                    // Get attachment GUID
                    Guid attachmentGuid = GetAttachmentGuid(cell);

                    // Get attachment extension
                    string attachmentExt = GetAttachmentExtension(cell);
                    bool isImage = ImageHelper.IsImage(attachmentExt);
                    string iconUrl = GetFileIconUrl(attachmentExt, "List");

                    // Get link for attachment
                    string attachmentUrl = null;
                    string attName = parameter.ToString();
                    int documentId = DocumentID;

                    if (Node != null)
                    {
                        if (IsLiveSite && (documentId > 0))
                        {
                            attachmentUrl = ResolveUrl(TreePathUtils.GetAttachmentUrl(attachmentGuid, UrlHelper.GetSafeFileName(attName, CMSContext.CurrentSiteName), 0, "getattachment"));
                        }
                        else
                        {
                            attachmentUrl = ResolveUrl(TreePathUtils.GetAttachmentUrl(attachmentGuid, UrlHelper.GetSafeFileName(attName, CMSContext.CurrentSiteName), VersionHistoryID, "getattachment"));
                        }
                    }
                    else
                    {
                        attachmentUrl = ResolveUrl(DocumentHelper.GetAttachmentUrl(attachmentGuid, VersionHistoryID));
                    }
                    attachmentUrl = UrlHelper.UpdateParameterInUrl(attachmentUrl, "chset", Guid.NewGuid().ToString());

                    // Ensure correct URL
                    if (SiteName != CMSContext.CurrentSiteName)
                    {
                        attachmentUrl = UrlHelper.AddParameterToUrl(attachmentUrl, "sitename", SiteName);
                    }

                    // Add latest version requirement for live site
                    if (IsLiveSite && (documentId > 0))
                    {
                        // Add requirement for latest version of files for current document
                        string newparams = "latestfordocid=" + documentId;
                        newparams += "&hash=" + ValidationHelper.GetHashString("d" + documentId);

                        attachmentUrl += "&" + newparams;
                    }

                    // Optionally trim attachment name
                    string attachmentName = TextHelper.LimitLength(attName, ATTACHMENT_NAME_LIMIT, "...");

                    // Tooltip
                    string tooltip = null;
                    if (ShowTooltip)
                    {
                        if (isImage)
                        {
                            int attachmentWidth = GetAttachmentWidth(cell);
                            if (attachmentWidth > 300)
                            {
                                attachmentWidth = 300;
                            }
                            tooltip = "onmouseout=\"UnTip()\" onmouseover=\"Tip('<div style=\\'width:" + attachmentWidth + "px;text-align:center;\\'><img src=\\'" + UrlHelper.AddParameterToUrl(attachmentUrl, "width", "300") + "\\' alt=\\'" + attachmentName + "\\' /></div>')\"";
                        }
                        else
                        {
                            tooltip = "onmouseout=\"UnTip()\" onmouseover=\"Tip('" + TextHelper.EnsureMaximumLineLength(attName, 90) + "', WIDTH, 0)\"";
                        }
                    }

                    // Icon
                    string imageTag = "<img class=\"Icon\" src=\"" + iconUrl + "\" alt=\"" + attachmentName + "\" />";
                    if (isImage)
                    {
                        return "<a href=\"#\" onclick=\"javascript: window.open('" + attachmentUrl + "'); return false;\"><span id=\"" + attachmentGuid + "\" " + tooltip + ">" + imageTag + attachmentName + "</span></a>";
                    }
                    else
                    {
                        return "<a href=\"" + attachmentUrl + "\">" + imageTag + "</a><a href=\"" + attachmentUrl + "\"><span id=\"" + attachmentGuid + "\" " + tooltip + ">" + attachmentName + "</span></a>";
                    }
                }

            case "attachmentsize":
                long size = ValidationHelper.GetLong(parameter, 0);
                return DataHelper.GetSizeString(size);
        }

        return parameter;
    }


    #region "Grid actions events"

    protected void dfuElem_PreRender(object sender, EventArgs e)
    {
        DirectFileUploader dfuElem = (DirectFileUploader)sender;
        if (Enabled)
        {
            dfuElem.ForceLoad = true;
            dfuElem.FormGUID = FormGUID;
            dfuElem.AttachmentGroupGUID = GroupGUID;
            dfuElem.DocumentID = DocumentID;
            dfuElem.NodeParentNodeID = NodeParentNodeID;
            dfuElem.NodeClassName = NodeClassName;
            dfuElem.ResizeToWidth = ResizeToWidth;
            dfuElem.ResizeToHeight = ResizeToHeight;
            dfuElem.ResizeToMaxSideSize = ResizeToMaxSideSize;
            dfuElem.AllowedExtensions = AllowedExtensions;
            dfuElem.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/upload.png"));
            dfuElem.ImageHeight = 16;
            dfuElem.ImageWidth = 16;
            dfuElem.InsertMode = false;
            dfuElem.ParentElemID = ClientID;
            dfuElem.CheckPermissions = CheckPermissions;

        }
        else
        {
            dfuElem.Visible = false;
        }
    }


    protected void imgUpdate_PreRender(object sender, EventArgs e)
    {
        ImageButton imgUpdate = (ImageButton)sender;
        if (!Enabled)
        {
            imgUpdate.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/uploaddisabled.png"));
            imgUpdate.Style.Add("cursor", "default");
            imgUpdate.Enabled = false;
        }
        else
        {
            imgUpdate.Visible = false;
        }
    }


    protected void img_PreRender(object sender, EventArgs e)
    {
        ImageButton img = (ImageButton)sender;

        if (CMSContext.CurrentUser.IsAuthenticated())
        {
            string[] args = img.AlternateText.Split('|');
            // If the file is not an image don't allow image editing
            if (!ImageHelper.IsSupportedByImageEditor(args[0]) || !Enabled)
            {
                // Disable edit icon in case that attachment is not an image
                img.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/editdisabled.png");
                img.Enabled = false;
                img.Style.Add("cursor", "default");
            }
            else
            {
                string strForm = (FormGUID == Guid.Empty) ? "" : FormGUID.ToString();

                // Create security hash
                string form = null;
                if (!String.IsNullOrEmpty(strForm))
                {
                    form = "&formguid=" + strForm + "&parentid=" + NodeParentNodeID;
                }
                else if (Node != null)
                {
                    form += "&siteid=" + Node.NodeSiteID;
                }
                string parameters = "?attachmentGUID=" + args[1] + "&versionHistoryID=" + VersionHistoryID + form + "&clientid=" + ClientID;
                string validationHash = QueryHelper.GetHash(parameters);
                img.Attributes.Add("onclick", "EditImage_" + ClientID + "('" + args[1] + "', '" + strForm + "', '" + VersionHistoryID + "', " + NodeParentNodeID + ", '" + validationHash + "');return false;");
            }

            img.AlternateText = ResHelper.GetString("general.edit");
        }
        else
        {
            img.Visible = false;
        }
    }


    protected void imgDown_PreRender(object sender, EventArgs e)
    {
        ImageButton imgDown = (ImageButton)sender;
        if (!Enabled || !AllowChangeOrder)
        {
            // Disable move down icon in case that editing is not allowed
            imgDown.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/downdisabled.png");
            imgDown.Enabled = false;
            imgDown.Style.Add("cursor", "default");
        }
    }


    void imgUp_PreRender(object sender, EventArgs e)
    {
        ImageButton imgUp = (ImageButton)sender;
        if (!Enabled || !AllowChangeOrder)
        {
            // Disable move up icon in case that editing is not allowed
            imgUp.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/updisabled.png");
            imgUp.Enabled = false;
            imgUp.Style.Add("cursor", "default");
        }
    }


    protected void imgDelete_PreRender(object sender, EventArgs e)
    {
        ImageButton imgDelete = (ImageButton)sender;
        if (!Enabled)
        {
            // Disable delete icon in case that editing is not allowed
            imgDelete.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/deletedisabled.png");
            imgDelete.Enabled = false;
            imgDelete.Style.Add("cursor", "default");
        }
    }

    #endregion


    /// <summary>
    /// Gets GUID value from DataControlFieldCell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>GUID of attachment</returns>
    protected Guid GetAttachmentGuid(DataControlFieldCell dcf)
    {
        // Get GUID of attachment
        return ValidationHelper.GetGuid(GetAttachmentProperty(dcf, "AttachmentGUID"), Guid.Empty);
    }


    /// <summary>
    /// Gets width value from DataControlFieldCell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Width of attachment</returns>
    protected int GetAttachmentWidth(DataControlFieldCell dcf)
    {
        // Get width of attachment
        return ValidationHelper.GetInteger(GetAttachmentProperty(dcf, "AttachmentImageWidth"), 0);
    }


    /// <summary>
    /// Gets extension value from DataControlFieldCell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Extension of attachment</returns>
    protected string GetAttachmentExtension(DataControlFieldCell dcf)
    {
        // Get extension of attachment
        return ValidationHelper.GetString(GetAttachmentProperty(dcf, "AttachmentExtension"), "");
    }


    protected object GetAttachmentProperty(DataControlFieldCell dcf, string property)
    {
        // Get datarowview
        DataRowView drv = ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
        if (drv != null)
        {
            // Get GUID of attachment
            return drv.Row[property];
        }
        return null;
    }

    #endregion
}
