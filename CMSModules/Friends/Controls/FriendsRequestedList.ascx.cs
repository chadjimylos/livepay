using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.Community;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Friends_Controls_FriendsRequestedList : CMSAdminListControl
{
    #region "Private variables"

    private int mUserId = 0;
    private bool mDisplayFilter = true;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets and sets User ID.
    /// </summary>
    public int UserID
    {
        get
        {
            return mUserId;
        }
        set
        {
            mUserId = value;
        }
    }


    /// <summary>
    /// Gets and sets whether to show filter.
    /// </summary>
    public bool DisplayFilter
    {
        get
        {
            return mDisplayFilter;
        }
        set
        {
            mDisplayFilter = value;
            searchElem.Visible = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            Visible = false;
            // Do not load data
            gridElem.StopProcessing = true;
        }
        else
        {
            RaiseOnCheckPermissions(PERMISSION_READ, this);
            Visible = true;

            // Register the dialog script
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
            btnRemoveSelected.Attributes.Add("onclick", "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("friends.ConfirmRemove")) + ");");

            // Register the refreshing script
            string refreshScript = ScriptHelper.GetScript("function refreshList(){" + Page.ClientScript.GetPostBackEventReference(hdnRefresh, string.Empty) + "}");
            ScriptHelper.RegisterClientScriptBlock(Page, typeof(string), "friendsListRefresh", refreshScript);

            gridElem.OnAction += uniGrid_OnAction;
            gridElem.OnExternalDataBound += gridElem_OnExternalDataBound;
            gridElem.OnDataReload += new UniGrid.OnDataReloadEventHandler(gridElem_OnDataReload);
            gridElem.OrderBy = "UserName";
            gridElem.IsLiveSite = IsLiveSite;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        if (!DisplayFilter)
        {
            Visible = HasData();
        }
        base.OnPreRender(e);

        // Reset grid selection after multiple action
        if (RequestHelper.IsPostBack())
        {
            string invokerName = Page.Request.Params.Get("__EVENTTARGET");
            Control invokeControl = !string.IsNullOrEmpty(invokerName) ? Page.FindControl(invokerName) : null;
            if (invokeControl == hdnRefresh)
            {
                gridElem.ResetSelection();
            }
        }
    }

    #endregion


    #region "Grid events"

    protected DataSet gridElem_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        // Load the data
        DataSet friendships = FriendInfoProvider.GetRequestedFriends(UserID, searchElem.WhereCondition, gridElem.SortDirect, currentTopN, "FriendID,UserName,UserNickname,FriendStatus,FullName,FriendComment");
        // Hide filter when not needed
        if (searchElem.FilterIsSet)
        {
            DisplayFilter = true;
        }
        else if (!DataHelper.DataSourceIsEmpty(friendships))
        {
            int rowsCount = friendships.Tables[0].Rows.Count;
            DisplayFilter = (gridElem.FilterLimit <= 0) || (rowsCount > gridElem.FilterLimit);
        }
        else
        {
            DisplayFilter = false;
        }
        totalRecords = DataHelper.GetItemsCount(friendships);
        return friendships;
    }


    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "remove":
                bool rejected = ValidationHelper.GetString(((DataRowView)((GridViewRow)parameter).DataItem).Row["FriendStatus"], string.Empty) == "1";
                // Disable checkbox
                GridViewRow row = (GridViewRow)parameter;

                Control control = CMS.ExtendedControls.ControlsHelper.GetChildControl(row, typeof(CheckBox)); ;
                if (control != null)
                {
                    CheckBox checkBox = (CheckBox)control;
                    checkBox.Enabled = !rejected;
                }
                // Disable button
                if (rejected)
                {
                    ImageButton button = ((ImageButton)sender);
                    button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/deletedisabled.png");
                    button.Enabled = false;
                }
                break;

            case "status":
                // Set status (rejected/waiting)
                FriendshipStatusEnum status =
                    (FriendshipStatusEnum)Enum.Parse(typeof(FriendshipStatusEnum), parameter.ToString());
                switch (status)
                {
                    case FriendshipStatusEnum.Waiting:
                        parameter = "<span class=\"Waiting\">" + ResHelper.GetString("friends.waiting") + "</span>";
                        break;

                    case FriendshipStatusEnum.Rejected:
                        parameter = "<span class=\"Rejected\">" + ResHelper.GetString("general.rejected") + "</span>";
                        break;
                }
                break;

            case "formattedusername":
                return HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(Convert.ToString(parameter), this.IsLiveSite));

        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        RaiseOnCheckPermissions(PERMISSION_MANAGE, this);

        FriendInfo fi = FriendInfoProvider.GetFriendInfo(ValidationHelper.GetInteger(actionArgument, 0));
        if (fi != null)
        {
            switch (actionName)
            {
                case "remove":
                    if (fi.FriendStatus != FriendshipStatusEnum.Rejected)
                    {
                        FriendInfoProvider.DeleteFriendInfo(fi);
                    }
                    gridElem.ReloadData();
                    break;
            }
        }
    }

    #endregion


    #region "Button handling"

    protected void btnRemoveSelected_Click(object sender, EventArgs e)
    {
        // If there user selected some items
        if (gridElem.SelectedItems.Count > 0)
        {
            RaiseOnCheckPermissions(PERMISSION_MANAGE, this);
            // Create where condition
            string where = "FriendID IN (";
            foreach (object friendId in gridElem.SelectedItems)
            {
                where += ValidationHelper.GetInteger(friendId, 0) + ",";
            }
            where = where.TrimEnd(',') + ")";
            // Get all needed friendships
            DataSet friendships = FriendInfoProvider.GetFriends(where, null);
            if (!DataHelper.DataSourceIsEmpty(friendships))
            {
                // Delete all these friendships
                foreach (DataRow friendship in friendships.Tables[0].Rows)
                {
                    FriendInfo fi = new FriendInfo(friendship);
                    if (fi.FriendStatus != FriendshipStatusEnum.Rejected)
                    {
                        FriendInfoProvider.DeleteFriendInfo(fi);
                    }
                }
            }
            gridElem.ResetSelection();
            // Reload grid
            gridElem.ReloadData();
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Indicates if there are some data
    /// </summary>
    public bool HasData()
    {
        return (!DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource));
    }

    #endregion
}
