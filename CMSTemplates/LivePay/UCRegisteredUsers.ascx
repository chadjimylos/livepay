﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCRegisteredUsers.ascx.vb" Inherits="CMSTemplates_LivePay_UCRegisteredUsers" %>
<%@ Register Src="~/CMSTemplates/livepay/PopUp/UCReceipt.ascx" TagName="Receipt" TagPrefix="uc"  %>
<script language="javascript" type="text/javascript" >

    function CheckForEvent(e) {
        var keynum = 0;
        if (window.event) { keynum = e.keyCode; }  // IE (sucks)
        else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

      
        if (keynum == 13) { // enter
            var InvoiceChecked = document.getElementById('<%=Tab2_ChlIWantInvoice.clientID %>').checked
            if (InvoiceChecked) {
                AllCardsValidation('2');
            } else {
                
                $('#Tab2_btnNextCheckNewCard').click()
            }
            return false;
        }
    }

    function findCardID(json, MerchantCards) {
        var MaxBinds = -1;
        var ItemID = -1;
        for (var i in json.cards) {

            if (MerchantCards.indexOf('[' + json.cards[i].id + ']') > -1) {
                if (json.cards[i].binds > MaxBinds) {
                    MaxBinds = json.cards[i].binds;
                    ItemID = json.cards[i].id;
                }
            }
        }
        return ItemID;
    }

    function exists(json, MerchantCards) {
        return (findCardID(json, MerchantCards) > -1);
    }


    Array.prototype.findValue = function (MerchantCards) {
        var index = -1;
        for (var i = 0; i < this.length; i++) {
            if (MerchantCards.indexOf('[' + this[i] + ']') > -1) {
                index = this[i];
                break;
            }
        }
        return index;
    }

    Array.prototype.exists = function (MerchantCards) {
        return (this.findValue(MerchantCards) > -1);
    }


    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    var CardBelongToOther_User = 0;

    var IsDirectPage = '<%= CMSContext.CurrentUser.IsPublic() %>'
   
    //---- For UnReg Users
    var Tab2_PhoneErrorMessage = ''
    var Tab2_EmailErrorMessage = ''
    //---- For UnReg Users

    var Tab1_PriceErrorMessage = ''
    var Tab1_PriceErrorMessageLimit = ''
    //--Start---For New Card---
    var FullNameErrorMessage = ''
    var NewCardCVV2ErrorMessage = ''
    var NewCardFriendlyErrorMessage = ''
    var SavedCardCVV2ErrorMessage = ''
    var SaveCardTopErrorMessage = ''
    var CardTypeTopErrorMessage = 'wwwww'
    var EmptySelectedCardErrorMessage = ''
    var EmptyNewCardErrorMessage = ''
    var CardBelongToOtherUserErrorMessage = ''

    var Tab2_ChlIWantInvoice = '<%=Tab2_ChlIWantInvoice.ClientID %>'
    var Inv_CompanyNameErrorMessage = ''
    var Inv_OccupationErrorMessage = ''
    var Inv_AddressErrorMessage = ''
    var Inv_TKErrorMessage = ''
    var Inv_CityErrorMessage = ''
    var Inv_AFMErrorMessage = ''
    var Inv_DOYErrorMessage = ''
    var CompleteErrorMessage = ''

    var Tab2_NotValidCardErrorMessage = ''

    var TermsOfUse = ''

    var MerchantMaxTransErrorMessage = ''
    var UsertMaxTransSessionErrorMessage = ''


    //--- CardMessages 
    var Tab2_CardNoSupportFromMerchant = '' //'Η κάρτα δεν υποστηρίζετε από την εταιρία'
    var Tab2_CardMaxLimitPerDay = ''        //'Η κάρτα έχει ξεπεράσει τις μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για την εταιρία'
    var Tab2_CardMaxAmountLimitPerDay = ''  //'Το αθροιστικό ποσό της καρτας ξεπερνά το Μέγιστο επιτρεπόμενο ποσό συναλλαγών ανά Ημέρα για την εταιρία'
    var Tab2_MerchantLimitPerDay = ''       //'Η εταιρία έχει ξεπεράσει<br>τις μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα'



    var NewCard_Tab2_txtCardNo = '<%=Tab2_txtCardNo.ClientID %>'
    var NewCard_NotValidCard = '<%=NewCard_HiddenNotValidCard.ClientID %>'
    var NewCard_NotValidMerchant = '<%=NewCard_HiddenNotValidMerchant.ClientID %>'
    var Tab2_txtFriendName = '<%=Tab2_txtFriendName.ClientID %>'
    //--End---For New Card---

    //--Start--For Saved Cards
    var Tab2_ddlSavedCard = '<%=Tab2_ddlSavedCard.ClientID %>'
    var NotValidCard = '<%=HiddenNotValidCard.ClientID %>'
    var NotValidMerchant = '<%=HiddenNotValidMerchant.ClientID %>'
    //--End--For Saved Cards


    



    var chkCardForSave = '<%=Tab2_ChkSaveDetails.ClientID %>'
    var HiddenChoice = '<%=HiddenChoice.ClientID %>'
    var CompanyName = '';
    var MerchantValidCards = '';
    var _MerchantID = '<%=HiddenMerchantID.ClientID %>'
    var MinimumTransactionAmount = '<%=HiddenMinimumTransactionAmount.ClientID %>';
    var MaximumTransactionAmount = '<%=HiddenMaximumTransactionAmount.ClientID %>';
    var MaximumTransactionsPerDayCard = ''; //-- ok
    var MaximumTransactionsPerDayGlobally = ''; //-- ok
    var MaximumSumTransactionsPerDayInCard = ''; //-- ok
    var MaxTransactionsPerSessionPerCard = 0;

    if (mobileDivice.isMobile()) {
        mobileInit();
    }

    function AllCardsValidation(Type) {
        if (document.getElementById(HiddenChoice).value == 2) {
            
            NewCard_CheckValidCard(true, false, Type)
        } else {
            if (mobileDivice.isMobile()) {
                CheckValidSaveCardMobile(Type);
            } else {
                CheckValidCard(Type);
            }
        }

        if (mobileDivice.isMobile()) {
            mobileInit();
        }

    }

    function ddlInstallments(cardTypeIndex) {
        //if (cardTypeIndex != 0) {
            var _pnlInstallments = $('[id$=pnlInstallments]');
            _pnlInstallments.hide();
            $("select[id$=DDL_Installments] > option").not('option:[value=-1]').not('option:[value=0]').remove();

            //$('[id$=DDL_Installments]').append($("<option value='" + 0 + "'>" + 0 + "</option>"));
            var merchant_min = _installment.merchant.min;
            var merchant_max = _installment.merchant.max;

            var _cardtype = _installment.cardtype.find(function (item, index) {
                return (item.index == cardTypeIndex);
            });


            if (_cardtype != null) {
                var cardtype_min = _cardtype.min;
                var cardtype_max = _cardtype.max;
                var _pnlshow = false;

                if (cardtype_min != "" || cardtype_max != "") {

                    for (i = merchant_min; i <= merchant_max; i++) {
                        if (i >= cardtype_min && i <= cardtype_max) {
                            //$('[id$=DDL_Installments]').append(new Option(i, i));
                            $('[id$=DDL_Installments]').append($("<option value='" + i + "'>" + i + "</option>"));

                            _pnlshow = true;
                        }
                    }
                } else {
                    $('[id$=hiddenInstallments]').val(-1);
                }

                if (_pnlshow)
                    _pnlInstallments.show();
            } else {
                $('[id$=hiddenInstallments]').val(-1);
            }

            $('[id$=DDL_Installments]').val($('[id$=hiddenInstallments]').val());
            //}
            PosBottom();
    }

    function OnChange_Installments(obj) {
        var element = $(obj);
        $('[id$=hiddenInstallments]').val(element.val())
    }

    function NewCard_CheckValidCard(IsFromValidation, ForChoice,FinalValidation) {
       
        
        //--- For xml
        var xmlhttp;
        if (window.ActiveXObject)
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        else
            xmlhttp = new XMLHttpRequest();

        var TransCountPerMerchantPerCard = 0; //--MaximumTransactionsPerDayCard  ---μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για τον συγκεκριμένο έμπορο
        var TransCountPerMerchant = 0;  //--MaximumTransactionsPerDayGlobally -- Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα gia ton emporo
        var TransSumAmountPerCardPerMerchant = 0; //---MaximumSumTransactionsPerDayInCard -- Το ποσό να μην ξεπερνά το Μέγιστο επιτρεπόμενο ποσό συναλλαγών ανά Ημέρα ανά κάρτα
        var TheCardIsNotValid = true;
        var GetNewCardType = 0;
        //--- For xml
        var CompanyNameValue = CompanyName.value
        var MerchantValidCardsValue = MerchantValidCards.value
        var CardObj = document.getElementById(NewCard_Tab2_txtCardNo);
        var CardObjValue = CardObj.value;
        var ShowCloud = false;

        //alert(CardObjValue)

        if (document.getElementById(HiddenChoice).value == 2) {    //--- Selected Choice "New Card"
            //ChangeRadioChoice('<%= Tab2_RbNewCard.ClientID %>', '<%= Tab2_RbSavedCard.ClientID%>');

            //$('[id$=Tab2_RbNewCard]').click()
            $('[id$=Tab2_RbNewCard]').attr('checked', 'checked');
            $('[id$=Tab2_RbSavedCard]').removeAttr('checked');
            CloseAllClouds();
            $('[id$=pnlNewCard]').show();
            PosBottom();
            //NewCard_CheckValidCard(false, true)
            
            //----Get Limits Per Card and Merchant
            document.getElementById('Tab2_btnNextCheckNewCard').disabled = true
            document.getElementById('Tab2_NextBtnInvoiceCheckNewCard').disabled = true
            PleaseWait("SavedCardsPlswaitNewCard", true);

            $('[id$=hiddenUserCardID]').val(0)
          
                xmlhttp.open("GET", "/CMSTemplates/LivePay/xmlhttp.aspx?MerchantID=" + MerchantID.value + "&UserCardID=0&CardNo=" + CardObjValue, true);
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4) {
                        var AllLimits = xmlhttp.responseText;

                        TransCountPerMerchantPerCard = GetLimit(AllLimits, 0)
                        TransCountPerMerchant = GetLimit(AllLimits, 1)
                        TransSumAmountPerCardPerMerchant = GetLimit(AllLimits, 2)
                        TheCardIsNotValid = GetLimit(AllLimits, 4)
                        CardBelongToOther_User = GetLimit(AllLimits, 5)
                        document.getElementById('<%=CardBelongToOtherUser.ClientID %>').value = GetLimit(AllLimits, 5)

                        //NotValidCardErrorMessage
                        //GetNewCardType = '[' + GetLimit(AllLimits, 3) + ']'
                        //$('[id$=hiddenInstallments]').val();


                        var strCards = GetLimit(AllLimits, 3);
                        var jsonCards = '{}';
                        if (strCards.length > 0)
                            jsonCards = jQuery.parseJSON(eval(strCards)); //jsonCards = JSON.parse(eval(strCards));

                        ddlInstallments(findCardID(jsonCards, MerchantValidCardsValue))


                        if (IsFromValidation) {
                            if (CardObjValue.length == 0) {
                                if (ForChoice != true) {

                                    ShowCloud = true;
                                    CloudContent = $('[id$=lbl_EmptySelectedCardErrorMessage]').html()//EmptySelectedCardErrorMessage
                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                NewCardErrorsType('ok', 'ok');
                            }
                        }

                        if (CardObjValue.length > 0) {
                            if (CardObjValue.length != 16) {
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = EmptyNewCardErrorMessage
                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }

                                NewCardErrorsType('ok', 'ok');
                            }


                            if (CardBelongToOther_User != '0' && document.getElementById(chkCardForSave).checked == true) { //--- IF the Card Belong to other User

                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = CardBelongToOtherUserErrorMessage

                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                document.getElementById('<%=NewCard_HiddenCardBelongToOtherUser.ClientID %>').value = ''
                            } else {
                                document.getElementById('<%=NewCard_HiddenCardBelongToOtherUser.ClientID %>').value = 'ok'
                            }




                            if (TheCardIsNotValid == 'False' && ShowCloud == false) {            //------Check if the Card Is Valid number
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = Tab2_NotValidCardErrorMessage

                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                NewCardErrorsType('', 'ok');
                            }


                            //if (MerchantValidCardsValue.indexOf('[' + GetNewCardType + ']') == -1 && ShowCloud == false) {            //------Check if the Card Is Valid for the Merchant
                            //if ((!ArrCards.exists(MerchantValidCardsValue)) && ShowCloud == false) {            //------Check if the Card Is Valid for the Merchant
                            if ((!exists(jsonCards, MerchantValidCardsValue)) && ShowCloud == false) {            //------Check if the Card Is Valid for the Merchant
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = Tab2_CardNoSupportFromMerchant

                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                NewCardErrorsType('', 'ok');
                            }

                            if (parseInt(TransCountPerMerchantPerCard) >= parseInt(MaximumTransactionsPerDayCard.value) && ShowCloud == false) { //------Check if μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για την εταιρία
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = Tab2_CardMaxLimitPerDay

                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                NewCardErrorsType('', 'ok');
                            }

                            if (parseFloat(TransSumAmountPerCardPerMerchant) >= parseFloat(MaximumSumTransactionsPerDayInCard.value) && ShowCloud == false) { //------Check if Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα του εμπορου
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = Tab2_CardMaxAmountLimitPerDay

                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                NewCardErrorsType('', 'ok');
                            }

                            if (parseInt(TransCountPerMerchant) >= parseInt(MaximumTransactionsPerDayGlobally.value) && ShowCloud == false) { //------Check if Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα του εμπορου
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = Tab2_MerchantLimitPerDay

                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, false, true, true, '')
                                }
                                NewCardErrorsType('', 'ok');
                            }
                        }
                        PleaseWait("SavedCardsPlswaitNewCard", false)
                        document.getElementById('Tab2_btnNextCheckNewCard').disabled = false
                        document.getElementById('Tab2_NextBtnInvoiceCheckNewCard').disabled = false


                        if (FinalValidation == '1' && ShowCloud == false) {
                            document.getElementById('<%=Tab2_btnNext.ClientID%>').click();
                        } else {
                            if (ShowCloud == false) {
                                $('#SavedCardMsgNewCard_Mob').hide()
                            }

                        }

                        if (FinalValidation == '2' && ShowCloud == false) {
                            document.getElementById('<%=Tab2_btnNext.ClientID%>').click();
                            //document.getElementById('<%=Tab2_NextBtnInvoice.ClientID%>').click();
                        }


                    }
                    //PleaseWait("SavedCardsPlswaitNewCard", false
                    //                    if (DetectIphoneOrIpod() || DetectAndroid()) {
                    //                        resizefooter()
                    //                    }
                }
                xmlhttp.send(null);
                //PleaseWait("SavedCardsPlswaitNewCard", false)
                //--END--Get Limits Per Card and Merchant
        }
        
        if (ShowCloud) {
           
        } else {
            
            document.getElementById(NewCard_NotValidMerchant).value = 'ok';
            document.getElementById(NewCard_NotValidCard).value = 'ok';
            $('#SavedCardMsgNewCard').poshytip('hide');
        }
    }

    function NewCardErrorsType(Card,Merch) {
        document.getElementById(NewCard_NotValidCard).value = Card;
        document.getElementById(NewCard_NotValidMerchant).value = Merch;
    }

    function CheckValidSaveCardMobile(FinalValidation) {
    
        //alert('CheckValidSaveCardMobile -> ' + FinalValidation);

        var TransCountPerMerchantPerCard = 0; //--MaximumTransactionsPerDayCard  ---μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για τον συγκεκριμένο έμπορο
        var TransCountPerMerchant = 0;  //--MaximumTransactionsPerDayGlobally -- Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα gia ton emporo
        var TransSumAmountPerCardPerMerchant = 0; //---MaximumSumTransactionsPerDayInCard -- Το ποσό να μην ξεπερνά το Μέγιστο επιτρεπόμενο ποσό συναλλαγών ανά Ημέρα ανά κάρτα 

        var CompanyNameValue = CompanyName.value
        var MerchantValidCardsValue = MerchantValidCards.value

        //alert('MerchantID -> ' + MerchantID.value)
        //alert('hid_MobileSelectedSaveCardID -> ' + $('[id$=hid_MobileSelectedSaveCardID]').text())
        $.ajax({
            url: "/CMSTemplates/LivePay/xmlhttp.aspx"
	                , data: "MerchantID=" + MerchantID.value + "&UserCardID=" + $('[id$=hid_MobileSelectedSaveCardID]').text()
	                , type: "GET"
	                , contentType: "text/plain"
	                , dataType: "text"
	                , success: function (data) {
	                    //alert(data);
	                    //if (data == 'true') {
	                    var AllLimits = data;
	                    TransCountPerMerchantPerCard = GetLimit(AllLimits, 0)
	                    TransCountPerMerchant = GetLimit(AllLimits, 1)
	                    TransSumAmountPerCardPerMerchant = GetLimit(AllLimits, 2)

	                    	                    var strCards = GetLimit(AllLimits, 3);
	                    	                    var jsonCards = '{}';
	                    	                    if (strCards.length > 0)
	                    	                        jsonCards = jQuery.parseJSON(eval(strCards));


	                    	                    //alert(jsonCards)
	                    	                    //alert(MerchantValidCardsValue)
	                    	                    ddlInstallments(findCardID(jsonCards, MerchantValidCardsValue))


	                    //	                    if (parseInt(TransCountPerMerchantPerCard) > parseInt(MaximumTransactionsPerDayCard.value)) { //------Check if μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για την εταιρία
	                    //	                        ShowCloud = true;
	                    //	                        CloudContent = Tab2_CardMaxLimitPerDay
	                    //	                        CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
	                    //	                    }
	                    //	                    if (parseFloat(TransSumAmountPerCardPerMerchant) > parseFloat(MaximumSumTransactionsPerDayInCard.value) && ShowCloud == false) { //------Check if Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα του εμπορου
	                    //	                        ShowCloud = true;
	                    //	                        CloudContent = Tab2_CardMaxAmountLimitPerDay
	                    //	                        CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
	                    //	                    }

	                    //	                    if (parseInt(TransCountPerMerchant) > parseInt(MaximumTransactionsPerDayGlobally.value) && ShowCloud == false) { //------Check if Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα του εμπορου
	                    //	                        ShowCloud = true;
	                    //	                        CloudContent = Tab2_MerchantLimitPerDay
	                    //	                        CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, false, false, false)
	                    //	                    }

	                    //PleaseWait("SavedCardsPlswait", false)
	                    //if (FinalValidation == '1') {
	                    
	                    document.getElementById('<%=Tab2_btnNext.ClientID%>').click();
	                    //}

	                    //if (FinalValidation == '2') {
	                    //document.getElementById('<%=Tab2_NextBtnInvoice.ClientID%>').click();
	                    //}
	                    //}
	                }
	                , error: function (er) { $.unblockUI(); }
        });
    }
    
    function CheckValidCard(FinalValidation) {
        //--- For xml
        var xmlhttp;
        if (window.ActiveXObject)
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        else
            xmlhttp = new XMLHttpRequest();

        var TransCountPerMerchantPerCard = 0; //--MaximumTransactionsPerDayCard  ---μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για τον συγκεκριμένο έμπορο
        var TransCountPerMerchant = 0;  //--MaximumTransactionsPerDayGlobally -- Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα gia ton emporo
        var TransSumAmountPerCardPerMerchant = 0; //---MaximumSumTransactionsPerDayInCard -- Το ποσό να μην ξεπερνά το Μέγιστο επιτρεπόμενο ποσό συναλλαγών ανά Ημέρα ανά κάρτα 
      
        //--- For xml
        var CompanyNameValue = CompanyName.value
        var MerchantValidCardsValue = MerchantValidCards.value
        var DDL = document.getElementById(Tab2_ddlSavedCard);
        var DDLIndex = DDL.selectedIndex;
        var DDLText = DDL.options[DDLIndex].text;
        var DDLValue = DDL.value;
        var CardTypeFormat = '[' + GetSelectedCardType(DDLValue,'_',1) + ']';
        var ShowCloud = false;

        if (FinalValidation == '1' || FinalValidation == '2') {
            if (DDLValue == '') {
                ShowCloud = true;
                CloudContent = SaveCardTopErrorMessage
                CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
            }
        }
        if (document.getElementById(HiddenChoice).value == 1 && DDLValue != '') {

            CloseAllClouds();
            $('[id$=Tab2_RbNewCard]').removeAttr('checked');
            $('[id$=Tab2_RbSavedCard]').attr('checked', 'checked'); 
            $('[id$=pnlNewCard]').hide();
            PosBottom();

            //if (MerchantValidCardsValue.indexOf(CardTypeFormat) == -1) {            //------Check if the Card Is Valid for the Merchant
            //    ShowCloud = true;
            //    CloudContent = Tab2_CardNoSupportFromMerchant
            //    CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
            //} else {                                                               //------Check if the Card Is Valid for the Merchant
                //----Get Limits Per Card and Merchant

                $('[id$=hiddenUserCardID]').val(GetSelectedCardType(DDLValue, '_', 0))

                xmlhttp.open("GET", "/CMSTemplates/LivePay/xmlhttp.aspx?MerchantID=" + MerchantID.value + "&UserCardID=" + GetSelectedCardType(DDLValue, '_', 0), true);
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4) {
                        var AllLimits = xmlhttp.responseText;
                        TransCountPerMerchantPerCard = GetLimit(AllLimits, 0)
                        TransCountPerMerchant = GetLimit(AllLimits, 1)
                        TransSumAmountPerCardPerMerchant = GetLimit(AllLimits, 2)

                        //alert('GetLimit(AllLimits, 3) -> ' + GetLimit(AllLimits, 3))

                        //ddlInstallments(GetLimit(AllLimits, 3));

                        var strCards = GetLimit(AllLimits, 3);

                        var jsonCards = '{}';
                        if (strCards.length > 0)
                            jsonCards = jQuery.parseJSON(eval(strCards)); //jsonCards = JSON.parse(eval(strCards));


                        ddlInstallments(findCardID(jsonCards, MerchantValidCardsValue))

                        if ((!exists(jsonCards, MerchantValidCardsValue)) && ShowCloud == false) {            //------Check if the Card Is Valid for the Merchant
                            ShowCloud = true;
                            CloudContent = Tab2_CardNoSupportFromMerchant
                            CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
                        }
                        if (parseInt(TransCountPerMerchantPerCard) >= parseInt(MaximumTransactionsPerDayCard.value)) { //------Check if μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για την εταιρία
                            ShowCloud = true;
                            CloudContent = Tab2_CardMaxLimitPerDay
                            CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
                        }

                        if (parseFloat(TransSumAmountPerCardPerMerchant) >= parseFloat(MaximumSumTransactionsPerDayInCard.value) && ShowCloud == false) { //------Check if Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα του εμπορου
                            ShowCloud = true;
                            CloudContent = Tab2_CardMaxAmountLimitPerDay
                            CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
                        }

                        if (parseInt(TransCountPerMerchant) >= parseInt(MaximumTransactionsPerDayGlobally.value) && ShowCloud == false) { //------Check if Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα του εμπορου
                            ShowCloud = true;
                            CloudContent = Tab2_MerchantLimitPerDay
                            CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, false, false, false)
                        }

                        PleaseWait("SavedCardsPlswait", false)
                        if (FinalValidation == '1' || FinalValidation == '2') {
                            document.getElementById('<%=Tab2_btnNext.ClientID%>').click();
                        }

                        //if (FinalValidation == '2') {
                            //document.getElementById('<%=Tab2_NextBtnInvoice.ClientID%>').click();
                        //}
                    }
//                    if (DetectIphoneOrIpod() || DetectAndroid()) {
//                        resizefooter()
//                    }
                }
                xmlhttp.send(null);
                PleaseWait("SavedCardsPlswait", true)
                //--END--Get Limits Per Card and Merchant
            //}
        }

        if (ShowCloud) {
        } else {
            document.getElementById(NotValidCard).value = 'ok';
            document.getElementById(NotValidMerchant).value = 'ok';  
            $('#SavedCardMsg').poshytip('hide');
        }
    }


    function SetCustomFieldsValues(objTxt, objSpan, Records) {

        for (i = 0; i <= Records; i++) {
            $('#' + objSpan + i).html($("input[id*='formUserControl_" + i + "']").val());
        }

        //$('#' + objSpan + Records).html($("input[id*='formUserControl_" + Records + "']").val());
//        var CustomFieldsContainer = document.getElementById('<%=CustomFields.ClientID %>')
//        var AllSelect = CustomFieldsContainer.getElementsByTagName('input');
//        var ICount = 0;
//        for (i = 0; i < AllSelect.length; i++) {
//            if (AllSelect[i].id.indexOf(objTxt) != -1) {
//                document.getElementById(objSpan + ICount).innerHTML = AllSelect[i].value;
//                ICount = ICount + 1;
//            }
//        }
    }


    function ShowReceipt() {
        $.blockUI({ css: { border: '0px', backgroundColor: 'transparent', cursor: 'default' }, message: $('#ReceiptPopUp') });
    }

    var index_PayComplete = 0;
    function CheckToComplete() {
        if (index_PayComplete == 1) { return false; }
        if (IsLogedIn) {
            var IsAccepted = document.getElementById('<%=Tab3_chkTermsAccept.ClientID%>').checked
            if (IsAccepted) {
                index_PayComplete = 1;
                return true
            } else {
                return false
            }
        } else { index_PayComplete = 1; return true; }
    }


    function OpenTheCustomCloudMobile(str, ObjStr, Object, ObjOpener, Show) {
        var Cloud = document.getElementById(Object)
        var CloudContent = document.getElementById(ObjStr)

        
            var CloudContent = document.getElementById(ObjStr)
            if (Show) {
                var offset = ObjOpener
                ObjOpener.style.position = 'absolute'
                $('#MobileInfo_').show()
                $('#MobileInfoContent_').html($('#TopMerchant_CloudContent').html())
                $('#MobileInfoContent_').css("max-width", "500px")
                $('#MainWidth_').css("max-width","600px")
//               
                $('#MobileRptInfoTop_').css("max-width", "576px")
                var divWidth = $('#MobileRptInfoTop_').width()
                $('#MobileRptInfoBottom_').width(divWidth + 'px')
                $('#RightBottom_').css("padding-left", divWidth + "px")
                var _Left = ObjOpener.offsetLeft + 120
                $('#MobileInfo_').css("left", _Left + "px")
                //DetectIphoneOrIpod DetectAndroid
                var _Top = ObjOpener.offsetTop
                $('#MobileInfo_').css("top", _Top + 'px')
                ObjOpener.style.position = 'relative'
            
        } else {
          
        }

    }



    var DefaultOffsetTop = 0
    function OpenCustomCloud(str, ObjStr, Object, ObjOpener, Show) {
        var IsNotMobile = CheckIfIsnotMobile()
        if (IsNotMobile) {

            var Cloud = document.getElementById(Object)
            var CloudContent = document.getElementById(ObjStr)
            if (Show) {
                var LinkOpener = document.getElementById(ObjOpener)
                CloudContent.innerHTML = str
                Cloud.style.display = ''
                if (document.all) {
                    //                Cloud.style.top = (ObjOpener.offsetTop - Cloud.offsetHeight + 15) + 'px'
                    //                Cloud.style.left = (ObjOpener.offsetLeft + ObjOpener.offsetWidth + 15) + 'px'
                } else {
                    //Cloud.style.top = (ObjOpener.offsetTop - Cloud.offsetHeight) + 'px'
                    Cloud.style.left = (ObjOpener.offsetLeft + ObjOpener.offsetWidth) + 'px'
                }
            } else {
                Cloud.style.display = 'none'
            }
        } else {
           
            
            var CloudContent = document.getElementById(ObjStr)
            if (Show) {
                var offset = document.getElementById(ObjOpener)
                
               $('#MobileInfo').show()
                //+ LinkOpener.offsetWidth
                $('#MobileInfoContent').html(str)
                var MainWidth = $('#MobileInfoContent').width() + 92
                $('#MainWidth').css("width", MainWidth + "px")
                $('#MobileRptInfoTop').width(MainWidth - 24)
                var divWidth = $('#MobileRptInfoTop').width()
                
                $('#MobileRptInfoBottom').width(divWidth + 'px')
                $('#RightBottom').css("padding-left",divWidth + "px")
                 var _Left = offset.offsetLeft - divWidth - 44
                 $('#MobileInfo').css("left", _Left + "px")
                
                var _Top = offset.offsetTop - 24
                $('#MobileInfo').css("top", _Top + 'px')
               
            }
        }
    }

    function Installments() {
        var CardType
    }

    function ShowTerms() {
        var popup = $('div#TextTerms');
        $.blockUI(
        {
            message: popup
            , css:
            {
                top: '10%',
                left: ($(window).width() - 500) / 2 + 'px',
                border: 0,
                width: '500px',
                textAlign: 'left',
                cursor: 'default'
            }
            , overlayCSS:
            {
                backgroundColor: '#0d1d40'
            }
        });
    }
    function closePopUp() {
        $.unblockUI();
    }

    
    $(document).ready(function () {
     
        if (mobileDivice.isMobile()) {
           
            SetZoom_()
        }
        
    });


   
    

//    function rdCard() {
//        $('[id$=Tab2_RbSavedCard]').click(function () {
//            $('[id$=pnlNewCard]').hide();
//        });
//        $('[id$=Tab2_RbNewCard]').click(function () {
//            $('[id$=pnlNewCard]').show();
//        });
//    }

   


</script>
<script src="/CMSScripts/LivePay/RegisteredUsers.js?v=3" type="text/javascript"></script>

<style type="text/css">
div.growlUI { background: url(check48.png) no-repeat 10px 10px;width:200px; }
div.growlUI h1, div.growlUI h2 {font-size:12px;color: white;text-align: left;width:200px}
.test
{
    display:none;
}


</style>
<asp:Literal ID="NewStyle" runat="server" ></asp:Literal>


<asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
<ContentTemplate>


<div style="display:none;position:absolute;color:White;" id="MobileInfo_">
    <div>
        <div style="float:left;"><img src="/App_Themes/LivePay/Info/LeftCorner.png" /></div>
        <div style="float:left;background-image:url('/App_Themes/LivePay/Info/Top.png');background-repeat:repeat-x;height:14px;width:auto" id="MobileRptInfoTop_">
            <div style="background-color:#0c4593;margin-top:8px;margin-left:-12px;margin-right:-12px;" id="MainWidth_">
                <div id="MobileInfoContent_" style="float:left;padding-left:10px;min-height:62px;padding-top:10px;width:auto">
            
                </div>
                <div  style="float:left;width:80px;text-align:right;">
                    <img src="/App_Themes/LivePay/Info/Close.png" onclick="$('#MobileInfo_').hide()" />
                </div>
                <div style="clear:both"></div>
            </div>
            <div style="margin-left:-12px;float:left;margin-top:-1px"><img src="/App_Themes/LivePay/Info/BottomLeftCorner.png" /></div>
            <div style="float:left;background-image:url('/App_Themes/LivePay/Info/Bottom.png');background-repeat:repeat-x;height:14px;margin-top:-1px;" id="MobileRptInfoBottom_">&nbsp;</div> 
            <div style="float:left;margin-top:-1px;position:absolute" id="RightBottom_"><img src="/App_Themes/LivePay/Info/RightBottomCorner.png" /></div>
            <div style="clear:both"></div>
        </div>
        <div style="float:left;" ><img src="/App_Themes/LivePay/Info/RightCorner.png" /></div>
        <div style="clear:both"></div>
    </div>
    <div>
    </div>
</div>

<div style="display:none;position:absolute;color:White;font-size:11px;" id="AppMobileInfo_">
    <div>
        <div style="float:left;"><img src="/App_Themes/LivePay/Info/LeftCorner.png" /></div>
        <div style="float:left;background-image:url('/App_Themes/LivePay/Info/Top.png');background-repeat:repeat-x;height:14px;width:auto" id="AppMobileRptInfoTop_">
            <div style="background-color:#0c4593;margin-top:8px;margin-left:-12px;margin-right:-12px;" id="AppMainWidth_">
                <div id="AppMobileInfoContent_" style="float:left;padding-left:10px;width:auto">
            
                </div>
                <div  style="float:right;padding-right:10px;text-align:right;">
                    <img src="/App_Themes/LivePay/Mobile/AppiPhone/Close.png" onclick="$('#AppMobileInfo_').hide()" />
                </div>
                <div style="clear:both"></div>
            </div>
            <div style="margin-left:-12px;float:left;margin-top:-1px"><img src="/App_Themes/LivePay/Info/BottomLeftCorner.png" /></div>
            <div style="float:left;background-image:url('/App_Themes/LivePay/Info/Bottom.png');background-repeat:repeat-x;height:14px;margin-top:-1px;" id="AppMobileRptInfoBottom_">&nbsp;</div> 
            <div style="float:left;margin-top:-1px;position:absolute" id="AppRightBottom_"><img src="/App_Themes/LivePay/Info/RightBottomCorner.png" /></div>
            <div style="clear:both"></div>
        </div>
        <div style="float:left;" ><img src="/App_Themes/LivePay/Info/RightCorner.png" /></div>
        <div style="clear:both"></div>
    </div>
    <div>
    </div>
</div>

<div style="display:none;position:absolute;color:White;" id="MobileInfo">
    <div>
        <div style="float:left;"><img src="/App_Themes/LivePay/Info/LeftCorner.png" /></div>
        <div style="float:left;background-image:url('/App_Themes/LivePay/Info/Top.png');background-repeat:repeat-x;height:14px;width:auto" id="MobileRptInfoTop">
            <div style="background-color:#0c4593;margin-top:8px;margin-left:-12px;margin-right:-12px;" id="MainWidth">
                <div id="MobileInfoContent" style="float:left;padding-left:10px;min-height:62px;padding-top:10px;width:auto">
            
                </div>
                <div  style="float:left;width:80px;text-align:right;">
                    <img src="/App_Themes/LivePay/Info/Close.png" onclick="$('#MobileInfo').hide()" />
                </div>
                <div style="clear:both"></div>
            </div>
            <div style="margin-left:-12px;float:left;margin-top:-1px"><img src="/App_Themes/LivePay/Info/BottomLeftCorner.png" /></div>
            <div style="float:left;background-image:url('/App_Themes/LivePay/Info/Bottom.png');background-repeat:repeat-x;height:14px;margin-top:-1px;" id="MobileRptInfoBottom">&nbsp;</div> 
            <div style="float:left;margin-top:-1px;position:absolute" id="RightBottom"><img src="/App_Themes/LivePay/Info/RightBottomCorner.png" /></div>
            <div style="clear:both"></div>
        </div>
        <div style="float:left;" ><img src="/App_Themes/LivePay/Info/RightCorner.png" /></div>
        <div style="clear:both"></div>
    </div>
    <div>
    </div>
</div>

<div style="display:none;position:absolute;color:White;font-size:11px !important;" id="AppMobileInfo">
    <div>
        <div style="float:left;"><img src="/App_Themes/LivePay/Info/LeftCorner.png" /></div>
        <div style="float:left;background-image:url('/App_Themes/LivePay/Info/Top.png');background-repeat:repeat-x;height:14px;width:auto" id="AppMobileRptInfoTop">
            <div style="background-color:#0c4593;margin-top:8px;margin-left:-12px;margin-right:-12px;" id="AppMainWidth">
                <div id="AppMobileInfoContent" style="float:left;padding-left:10px;width:auto">
            
                </div>
                <div  style="float:right;padding-right:10px;text-align:right;">
                    <img src="/App_Themes/LivePay/Mobile/AppiPhone/Close.png" onclick="$('#AppMobileInfo').hide()" />
                </div>
                <div style="clear:both"></div>
            </div>
            <div style="margin-left:-12px;float:left;margin-top:-1px"><img src="/App_Themes/LivePay/Info/BottomLeftCorner.png" /></div>
            <div style="float:left;background-image:url('/App_Themes/LivePay/Info/Bottom.png');background-repeat:repeat-x;height:14px;margin-top:-1px;" id="AppMobileRptInfoBottom">&nbsp;</div> 
            <div style="float:left;margin-top:-1px;position:absolute" id="AppRightBottom"><img src="/App_Themes/LivePay/Info/RightBottomCorner.png" /></div>
            <div style="clear:both"></div>
        </div>
        <div style="float:left;" ><img src="/App_Themes/LivePay/Info/RightCorner.png" /></div>
        <div style="clear:both"></div>
    </div>
    <div>
    </div>
</div>

<div id="ReceiptPopUp" style="display:none">
        <uc:Receipt ID="Receipt" runat="server" />
</div>
<div style="Padding-left:20px;padding-Bottom:10px;Color:#B04424;">
	<asp:Label ID="ErrorText" runat="server" EnableViewState="false" />
</div>
 <div class="RegUserTopMain" style="height:10px">
         <img src="/App_Themes/LivePay/RegisteredUsers/RegUserTopBG.png" />
     </div> 
 <div class="RegUsersMainPage">
    <asp:Label runat="server" ID="hid_MobileSelectedSaveCardID" style="display:none" />
    <asp:Label ID="lbl_EmptySelectedCardErrorMessage" runat="server" style="display:none" />
    <asp:Label ID="lbl_NewCardCVV2ErrorMessage" runat="server" style="display:none" />
    <asp:Label ID="lbl_FullNameErrorMessage" runat="server" style="display:none" />
    <asp:Label ID="lbl_CardTypeTopErrorMessage" runat="server" style="display:none" />
    <asp:Label ID="lbl_DDLInstallmentsErrorMessage" runat="server" style="display:none" />
	
	<asp:Label ID="lbl_ErrorMaximumTransactionsPerDayGlobally" runat="server" style="display:none" />
	<asp:Label ID="lbl_ErrorTransCountPerMerchantPerCard" runat="server" style="display:none" />
	<asp:Label ID="lbl_ErrorTransSumAmountPerCardPerMerchant" runat="server" style="display:none" />
    
	<asp:Label ID="hdn_CVV" runat="server" style="display:none" />
    <div id="Tab1" style="display:;">

        <div class="PL10">
            <div class="Tab1MainBG">
                <div id="DivTabsTab1">
                    <div class="RegTopTab1btn"><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 1: Στοιχεία Πληρωμής|en-us=Step 1: Payment Information$}")%></div>
                    <div class="RegTopTab2_3btn" ><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 2: Στοιχεία Χρέωσης|en-us=Step 2: Card Information$}")%></div>
                    <div class="RegTopTab2_3btn"><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 3: Επιβεβαίωση|en-us=Step 3: Payment Confirmation$}")%></div>
                    <div class="Clear"></div>
                </div>
                <div id="DivFormTab1" class="PL10PT30">
                    <div id="CustomFields" runat="server">
                    </div>
                    <div class="RegUsersTab1CustomTitle"><%= ResHelper.LocalizeString("{$=Ποσό|en-us=Amount$}")%> (&euro;)</div>
                    <div class="RegUsersTab2_TxtDiv RegUsersTab2_TxtDiv_text">
                    <a id="Tab1_PriceMsg" href="#"></a>
                    <asp:TextBox cssclass="RegUsersTab1CustomTxt" onkeyup="FixMoney(this,false,true)" onblur="FixMoney(this,true,true)" width="260px" id="Tab1_txtPrice" runat="server" ></asp:TextBox>
                    <div class="HideIt"><asp:CustomValidator ID="Tab1_ReqFldVal_Price" IsNumber="yes" ClientValidationFunction="validateControls" ValidateEmptyText="true" runat="server"   ControlToValidate="Tab1_txtPrice" text="*" ValidationGroup="Tab1_PayForm"/>
                    </div> 
                    </div> 
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="Tab1_PriceMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
                    <div class="RegUsersTab1CustomTitle" runat="server" id="divNotes"><%=ResHelper.LocalizeString("{$=Σημειώσεις|en-us=Comments$}") %></div>
                    <div class="RegUsersTab2_TxtDiv RegUsersTab2_textarea" id="div_txtNones" runat="server"><asp:TextBox cssclass="RegUsersTab1CustomTxt" width="260px" Rows="4" TextMode="MultiLine" id="Tab1_txtNotes" runat="server" ></asp:TextBox></div>
                    <div class="Clear"></div>
                    <div class="REgUsersBottomMust">
                        <%= ResHelper.LocalizeString("{$=Τα πεδία με τον αστερίσκο (*) είναι υποχρεωτικά|en-us=Fields marked as (*) are obligatory$}")%>
                    </div>
                    <div style="float:left;width:140px;text-align:right;">&nbsp;</div>
                    <div class="RegUsersTab2_TxtDiv RegUsersTab2_TxtDiv_btn"><asp:ImageButton ID="Tab1_BtnNext" ValidationGroup="Tab1_PayForm" runat="server" /></div>
                    <div class="Clear"></div>
                   
                </div>
            </div>
            <div class="RegUserBottomMain"><img src="/App_Themes/LivePay/RegisteredUsers/Tab1WhiteBottom.png" /></div>
        </div>
    </div>
    <div id="Tab2" style="display:none">
        <div class="PL10">
            <div class="Tab2MainBG">
                <div id="DivTabsTab2">
                    <div class="RegTopTab1btn" ><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 1: Στοιχεία Πληρωμής|en-us=Step 1: Payment Information$}")%></div>
                    <div class="RegTopTab2_3btn" ><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 2: Στοιχεία Χρέωσης|en-us=Step 2: Card Information$}")%></div>
                    <div class="RegTopTab2_3btn" ><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 3: Επιβεβαίωση|en-us=Step 3: Payment Confirmation$}")%></div>
                    <div class="Clear"></div>
                </div>
                <div id="DivFormTab2" class="PL10PT30">
                    <div id="ForRegUsersTopMobile" visible="false" runat="server">
                        <div class="btnSaveCardMobile">
                            <asp:LinkButton runat="server" ID="lnkNewCard" OnClientClick="$('#Tab1').hide();ChangeRadioChoice('<%= Tab2_RbNewCard.ClientID%>','<%= Tab2_RbSavedCard.ClientID%>');" OnClick="LinkButtonMobileNewCard_Click">Εισαγωγή νέας κάρτας</asp:LinkButton>
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="pnlCardMobileCVV" Visible="false">
                        <div class="RegUserTitleB">CVV2</div>
                        <div class="RegUsersTab2_TxtDiv_text_small" ><a id="SaveCardMobileCVVMsg" href="#"></a><asp:TextBox runat="server" ID="txtSaveCardMobileCVV" Width="108" MaxLength="3" autocomplete="off" TextMode="Password" /></div>
                    
                        <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                        <div class="Clear"></div>
                        <div id="SaveCardMobileCVVMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>


                        <div class="HideIt"><asp:CustomValidator ID="cvSaveMobileCardCVV" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" IsSaveCardMobileCVV="yes" IsSavedMobileCards="yes" IsNumber="yes" LengthLimit="3" CheckLength="yes" ErrorMessage="CVV2" runat="server" ControlToValidate="txtSaveCardMobileCVV" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </asp:Panel>
                    <div id="ForRegUsersTop" runat="server">
                        <div class="Tab2RAdChoice1">
                            <asp:RadioButton Checked="true" cssclass="RegUserRadioA" ID="Tab2_RbSavedCard" runat="server" />
                        </div>
                        <div class="RegUserMainSavedCards">
                         <a id="SavedCardMsg" href="#"></a><a id="SavedCardMsgSec" href="#"></a>
                         <div id="SavedCardsPlswait" class="PleaseWait"><img src="/App_Themes/LivePay/LoadingProgressBar.gif" /></div>
                         <asp:DropDownList ID="Tab2_ddlSavedCard" runat="server" cssclass="REgUserDDL" width="230px" ></asp:DropDownList>
                         <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_SavedCard" ErrorMessage="CVV2" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" isDDLSavedCards="yes" IsSavedCards="yes" ControlToValidate="Tab2_ddlSavedCard" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                         <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_iddenNotValidCard" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsCardHidden="yes" IsSavedCards="yes" ControlToValidate="HiddenNotValidCard" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                         <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_iddenNotValidMerchant" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsCardHidden="yes" IsSavedCards="yes" ControlToValidate="HiddenNotValidMerchant" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                     
                        </div>
                        <div class="RegUserCVVTopTitle">CVV2</div> 
                        <div class="RegUserCVVTitleB"><a id="SavedCardCVV2Msg" href="#"></a><asp:TextBox autocomplete="off" cssclass="RegUsersTab1CustomTxt" width="110px" TextMode="Password" id="Tab2_txtCVV2Top" MaxLength="3" runat="server" ></asp:TextBox></div> 
                        <div class="Tab2CVV2Div" onmouseover="ShowClound('CVV2Cloud',this)" onmouseout="HideClound('CVV2Cloud')"><img  src="/App_Themes/LivePay/InfoBtn.png" />
                            <div style="display:none;" class="RegUsersMainClouds" id="CVV2Cloud"><img src="/App_Themes/LivePay/RegisteredUsers/Tab1CloudCustCode.png" /></div>
                            <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_CVV2Top" ErrorMessage="CVV2" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsNumber="yes" LengthLimit="3" CheckLength="yes" IsSavedCards="yes" ControlToValidate="Tab2_txtCVV2Top" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        </div>
                        <div class="Clear"></div> 
                        <div style="padding-top:4px"><div style="height:2px;background-color:#f1f1f1;width:650;font-size:1px;">&nbsp;</div></div>
                    </div>
                    <div class="RegUsersTab2_RB2Div" id="Tab2_RB2Div">
                        <div id="Tab2_RbNewCardDivCont" runat="server"><asp:RadioButton cssclass="RegUserRadioA" ID="Tab2_RbNewCard" runat="server" /></div>
                    </div>
                    
                    <asp:Panel runat="server" ID="pnlNewCard">
                    <div class="RegUserTitleA"><%= ResHelper.LocalizeString("{$=Τύπος Κάρτας|en-us=Card Type$}")%></div>
                    <div class="RegUsersTab2_TxtDivSec ddlMobSpace"><a id="CardTypeMsg" href="#"></a><asp:DropDownList cssclass="REgUserDDL" width="264px" id="Tab2_ddlCardType" runat="server" ></asp:DropDownList>
                         <div class="HideIt"><asp:CustomValidator SetFocusOnError="true" id="Tab2_ReqFldVal_CardType"  runat="server" ValidationGroup="Tab2_PayForm" ValidateEmptyText="true" IsCardType="yes" ControlToValidate = "Tab2_ddlCardType" ClientValidationFunction="validateControlsTabB" ></asp:CustomValidator></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="CardTypeMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>

                    <div class="RegUserTitleA"><%=ResHelper.LocalizeString("{$=Αριθμός Κάρτας|en-us=Card Number$}") %></div>
                    <div class="RegUsersTab2_TxtDivSec RegUsersTab2_TxtDiv_text">
                    <a id="SavedCardMsgNewCard" href="#"></a><div id="SavedCardsPlswaitNewCard" class="PleaseWait"><img src="/App_Themes/LivePay/LoadingProgressBar.gif" /></div>
                    <asp:TextBox cssclass="RegUsersTab1CustomTxt" autocomplete="off" width="260px" id="Tab2_txtCardNo" onkeydown="return CheckForEvent(event)"  onkeyup="ClearCharcters(this,false)" onblur="ClearCharcters(this,true)" runat="server" MaxLength="16" ></asp:TextBox>
                         <div class="HideIt"><%--<asp:CustomValidator ID="Tab2_ReqFldVal_NewCardBelognToOtherUser" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" isCVVChecked="yes" runat="server"  ControlToValidate="NewCard_HiddenCardBelongToOtherUser"  text="*" ValidationGroup="Tab2_PayForm"/>--%></div> 
                        <div class="HideIt"><%--<asp:CustomValidator ID="Tab2_ReqFldVal_NewCardNotValCard" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" isCVVChecked="yes" runat="server"  ControlToValidate="NewCard_HiddenNotValidCard"  text="*" ValidationGroup="Tab2_PayForm"/>--%></div> 
                        <div class="HideIt"><%--<asp:CustomValidator ID="Tab2_ReqFldVal_NewCardNotValMerchant" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" isCVVChecked="yes" runat="server" isTest="yes" ControlToValidate="NewCard_HiddenNotValidMerchant" text="*" ValidationGroup="Tab2_PayForm"/>--%></div> 
                        <div class="HideIt"><%--<asp:CustomValidator ID="Tab2_ReqFldVal_CardNo" runat="server" ValidationGroup="Tab2_PayForm" ValidateEmptyText="true" IsNewCardNo="yes" LengthLimit="16" CheckLength="yes" IsNumber="yes" ClientValidationFunction="validateControlsTabB"  ControlToValidate="Tab2_txtCardNo" text="*" />--%></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="SavedCardMsgNewCard_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
                    <div class="RegUserTitleB"><%= ResHelper.LocalizeString("{$=Ονοματεπώνυμο Κατόχου|en-us=Card Holder Name$}")%></div> 
                    <div class="RegUsersTab2_TxtDivSec RegUsersTab2_TxtDiv_text"><a id="NewCardFullNameMsg" href="#"></a><asp:TextBox onkeydown="return CheckForEvent(event)" autocomplete="off" cssclass="RegUsersTab1CustomTxt" MaxLength="150" width="260px" id="Tab2_txtFullNameOwner" runat="server" ></asp:TextBox>
                        <div class="HideIt"><asp:CustomValidator SetFocusOnError="true" ID="Tab2_ReqFldVal_FullNameOwner" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsFullName="yes" ControlToValidate="Tab2_txtFullNameOwner" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="NewCardFullNameMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
                    <div class="RegUserTitleB"><%= IIf(BrowserHelper.IsMobileDevice(), ResHelper.LocalizeString("{$=Ημερομηνία Λήξης|en-us=Card Expiration Date$}"), ResHelper.LocalizeString("{$=Ημερομηνία<Br />Λήξης|en-us=Card <br />Expiration Date$}"))%></div>
                    <div class="RegUsersTab2_TxtDiv ddlMobSpace"><asp:DropDownList cssclass="REgUserDDL" width="112px" id="Tab2_DdlCardMonth" runat="server" ></asp:DropDownList>
                        <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_CardMonth" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server"  ControlToValidate="Tab2_DdlCardMonth" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="RegUsersTab2_TxtDiv ddlMobSpace"><asp:DropDownList cssclass="REgUserDDL" width="112px" id="Tab2_DdlCardYear" runat="server" ></asp:DropDownList>
                        <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_CardYear" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" ControlToValidate="Tab2_DdlCardYear" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div class="RegUserTitleB">CVV2</div>
                    <div class="RegUserNewCVV2DIV RegUsersTab2_TxtDiv_text_small"><a id="NewCardCVVMsg" href="#"></a><asp:TextBox MaxLength="3" TextMode="Password" onkeydown="return CheckForEvent(event)" autocomplete="off" cssclass="RegUsersTab1CustomTxt" width="108px" id="Tab2_txtCVV2" runat="server" ></asp:TextBox>
                        <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_CVV2" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" IsNewCardCVV="yes" IsNumber="yes" LengthLimit="3" CheckLength="yes" ErrorMessage="CVV2" runat="server" ControlToValidate="Tab2_txtCVV2" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        
                    </div>
                    <div class="Tab2CVV2Div" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="NewCardCVVMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
                    <div id="ForRegUsersChoiceToSave" runat="server">
                        <div class="RegUsersSeprSaveCard">&nbsp;</div>
                        <div class="FLEFT"><asp:CheckBox ID="Tab2_ChkSaveDetails" cssclass="RegUSerControlA" runat="server" Text="Αποθήκευση Στοιχείων Κάρτας για Μελλοντική Χρήση" /></div>
                        <div class="Clear"></div>
                        <div style="min-height:34px">
                            <div id="friendlyNameMainDiv" style="display:none">
                                <div class="RegUserTitleB"><%= ResHelper.LocalizeString("{$=Φιλική Ονομασία|en-us=Friendly Name$}")%></div>
                                <div class="RegUsersTab2_TxtDivSec" style="padding-top:10px"><a id="NewCardFriendly" href="#"></a><asp:TextBox onkeydown="return CheckForEvent(event)" autocomplete="off" cssclass="RegUsersTab1CustomTxt" width="260px" id="Tab2_txtFriendName" runat="server" MaxLength="30" ></asp:TextBox>
                                    <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_FriendName" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsFriendlyName="yes" ControlToValidate="Tab2_txtFriendName" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                                </div>
                                <div class="RegUsersMainDivCloud" style="padding-top:12px;color:#094595;font-weight:bold">*</div>
                                 <div class="Clear"></div>
                            </div>
                        </div>
                    </div>
                    <asp:Panel runat="server" ID="pnlPhoneMail" Visible="false">
                    <div id="ForUnRegUsers_PhoneMail">
                        <div style="padding-top:4px"><div style="height:2px;background-color:#f1f1f1;width:650;font-size:1px;">&nbsp;</div></div>
                        <div style="padding-top:5px;padding-bottom:20px">
                        <%= ResHelper.LocalizeString("{$=Σε περίπτωση που θέλετε να παραλάβετε το αποδεικτικό της συναλλαγής, συμπληρώστε το email σας|en-us=Please fill in your e-mail address, if you wish to receive a transaction receipt$}")%>
                    </div>
                        <div class="RegUsersTab2_PhoneDiv" id="Tab2_PhoneDiv">
                            <div class="RegUserTitleA"><%= ResHelper.LocalizeString("{$=Τηλέφωνο|en-us=Phone Number$}")%></div>
                            <div class="RegUsersTab2_TxtDivSec RegUsersTab2_TxtDiv_text"><a id="UnRegUserPhoneMsg" href="#"></a><asp:TextBox onkeydown="return CheckForEvent(event)" onkeyup="ClearCharcters(this,false)" onblur="ClearCharcters(this,false)" autocomplete="off" MaxLength="15"  cssclass="RegUsersTab1CustomTxt" width="260px" id="Tab2_txtPhone" runat="server" ></asp:TextBox>
                                <div class="HideIt"><asp:CustomValidator ID="UnRegUserPhoneValid" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsPhone="yes" IsNumber="yes" LengthLimit="15" CheckLength="yes" IsUnRegField="yes" ControlToValidate="Tab2_txtPhone" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                            </div>
                            <div class="RegUsersMainDivCloud" style="padding-top:12px;color:#094595;font-weight:bold" >*</div>
                        <div class="Clear"></div>
                        <div id="UnRegUserPhoneMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:"></div>
                        </div>
                        <div class="RegUserTitleA"><%= ResHelper.LocalizeString("{$=E-mail|en-us=E-mail$}")%></div>
                        <div class="RegUsersTab2_TxtDivSec RegUsersTab2_TxtDiv_text"><a id="UnRegUserEmailMsg" href="#"></a><asp:TextBox autocomplete="off" onkeydown="return CheckForEvent(event)" cssclass="RegUsersTab1CustomTxt" MaxLength="150" width="260px" id="Tab2_txtEmail" runat="server" ></asp:TextBox>
                        <div class="HideIt"><asp:CustomValidator SetFocusOnError="true" ID="UnRegUserEmailValid" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsUnRegField="yes" IsEmail="yes" ControlToValidate="Tab2_txtEmail" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        </div>
                        <div class="Clear"></div>
                        <div id="UnRegUserEmailMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:"></div>
                    </div>
                    </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlInstallments" style="display:none">
                    <div style="padding-top:4px"><div style="height:2px;background-color:#f1f1f1;width:650;font-size:1px;">&nbsp;</div></div>
                        <div class="RegUserTitleB"><%= ResHelper.LocalizeString("{$=Αριθμός δόσεων |en-us=Installments $}")%></div>
                        <div class="RegUserNewCVV2DIV">
                            <a id="CloudInstallmentsMsg" href="#"></a>
                            <asp:DropDownList runat="server" ID="DDL_Installments" onchange="OnChange_Installments(this);" >
                                <asp:ListItem Value="-1" Selected="True" meta:resourcekey="SelectedInstallments" />
                                <asp:ListItem Value="0" meta:resourcekey="DDL_Installments" />
                            </asp:DropDownList>
                        </div>
                        <div class="HideIt"><asp:CustomValidator SetFocusOnError="true" ID="cvInstallments" ClientValidationFunction="validateControlsTabB" Installments="yes" ValidateEmptyText="true" runat="server" ControlToValidate="DDL_Installments" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        <div class="Tab2CVV2Div" style="color:#094595;font-weight:bold">*</div>
                        <div class="Clear"></div>
                        <div id="CloudInstallmentsMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:"></div>
                        <div class="Clear"></div>
                    </asp:Panel>
                     
                   <div style="padding-top:4px"><div style="height:2px;background-color:#f1f1f1;width:650;font-size:1px;">&nbsp;</div></div>
                    <div class="RegUsersTab2_BtnNextDiv" id="Tab2_BtnNextDiv">
                        <div class="RegUsersFirstPaddingButton">&nbsp;</div>
                        <div class="FLEFT"><asp:ImageButton ID="Tab2_btnReturn_Mobile" runat="server" style="display:none;" EnableViewState="false" /></div>
                        <div class="FLEFT"><asp:ImageButton ID="Tab2_btnReturn" OnClientClick="ShowHideTabs('','none','none');return false"  runat="server"  /></div>
                        <div style="float:left;padding-left:10px">
                        <img id="Tab2_btnNextCheckNewCard" style="cursor:pointer" src='<%= ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/BtnNext.png|en-us=/App_Themes/LivePay/RegisteredUsers/BtnNext_en-us.png$}")%>' onclick="AllCardsValidation('1')" /> 
                        <asp:ImageButton style="display:none" ID="Tab2_btnNext" ValidationGroup="Tab2_PayForm" runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/BtnNext.png" />
                        </div>
                        <div class="Clear"></div>
                        
                    </div>
                    <div class="REgUsersBottomMust">
                        <%= ResHelper.LocalizeString("{$=Τα πεδία με τον αστερίσκο (*) είναι υποχρεωτικά|en-us=Fields marked as (*) are obligatory$}")%>
                    </div>
                    <div>
                        <asp:CheckBox ID="Tab2_ChlIWantInvoice" Visible="false" onclick="ShowHideInvoice(this)" cssclass="RegUSerControlA" runat="server"  />
                    </div>
                </div>       
                <div id="InvoiceDiv" style="padding-left:10px;background-color:White;padding-bottom:5px;display:none;">
                    <div class="RegUserTitleA"><%= ResHelper.LocalizeString("{$=Επωνυμία Επιχείρησης|en-us=Enter the Company Name$}")%></div>
                    <div class="RegUsersTab2_TxtDivSec RegUsersTab2_TxtDiv_text"><a id="Inv_CompanyNameMsg" href="#"></a><asp:TextBox onkeydown="return CheckForEvent(event)" autocomplete="off" CssClass="RegUsersTab1CustomTxt" MaxLength="200" width="260px" id="Tab2Inv_CompanyName" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:CustomValidator SetFocusOnError="true" ID="Tab2_CustVal_InvCompanyName" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_CompanyName="yes" IsInvoice="yes" ControlToValidate="Tab2Inv_CompanyName" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div> 
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="Inv_CompanyNameMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
                    <div class="RegUserTitleB"><%= ResHelper.LocalizeString("{$=Επάγγελμα|en-us=Profession$}")%></div>
                    <div class="RegUsersTab2_TxtDivSec RegUsersTab2_TxtDiv_text"><a id="Inv_OccupationMsg" href="#"></a><asp:TextBox onkeydown="return CheckForEvent(event)" autocomplete="off" CssClass="RegUsersTab1CustomTxt" MaxLength="150" width="260px" id="Tab2Inv_Occupation" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvOccupation" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_Occupation="yes" IsInvoice="yes" ControlToValidate="Tab2Inv_Occupation" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="Inv_OccupationMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
                    <div class="RegUserTitleB"><%= ResHelper.LocalizeString("{$=Διεύθυνση|en-us=Address$}")%></div>
                    <div class="RegUsersTab2_TxtDivSec RegUsersTab2_TxtDiv_text"><a id="Inv_AddressMsg" href="#"></a><asp:TextBox onkeydown="return CheckForEvent(event)" autocomplete="off" CssClass="RegUsersTab1CustomTxt" MaxLength="200" width="260px" id="Tab2Inv_Address" runat="server" ></asp:TextBox>
                      <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvAddress" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_Address="yes" IsInvoice="yes" ControlToValidate="Tab2Inv_Address" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="Inv_AddressMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
                    <div class="RegUserTitleB"><%= ResHelper.LocalizeString("{$=Πόλη|en-us=City$}")%></div>
                    <div class="RegUsersTab2_TxtDivSec RegUsersTab2_TxtDiv_text"><a id="Inv_CityMsg" href="#"></a><asp:TextBox onkeydown="return CheckForEvent(event)"  autocomplete="off" CssClass="RegUsersTab1CustomTxt" width="122px" MaxLength="100" id="Tab2Inv_City" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvCity" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_City="yes"  IsInvoice="yes" ControlToValidate="Tab2Inv_City" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="Inv_CityMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
                    <div class="RegUserTitleB"><%= ResHelper.LocalizeString("{$=Τ.Κ.|en-us=Postal Code$}")%></div>
                    <div class="RegUsersTab2_TxtDivSec RegUsersTab2_TxtDiv_text" ><a id="Inv_TKMsg" href="#"></a><asp:TextBox onkeydown="return CheckForEvent(event)" autocomplete="off" MaxLength="5" CssClass="RegUsersTab1CustomTxt" width="60px" id="Tab2Inv_TK" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvTK" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_TK="yes" IsInvoice="yes" IsNumber="yes" LengthLimit="5" CheckLength="yes" ControlToValidate="Tab2Inv_TK" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="Inv_TKMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>

                    <div class="RegUserTitleB"><%= ResHelper.LocalizeString("{$=ΑΦΜ|en-us=VAT Number$}")%></div>
                    <div class="RegUsersTab2_TxtDivSec RegUsersTab2_TxtDiv_text"><a id="Inv_AFMMsg" href="#"></a><asp:TextBox onkeydown="return CheckForEvent(event)" autocomplete="off" MaxLength="9" CssClass="RegUsersTab1CustomTxt" width="200px" id="Tab2Inv_AFM" runat="server"  ></asp:TextBox>
                        <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvAFM" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_AFM="yes" IsInvoice="yes" IsNumber="yes" LengthLimit="9" CheckLength="yes" ControlToValidate="Tab2Inv_AFM" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="Inv_AFMMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
                     <div class="RegUserTitleB"><%= ResHelper.LocalizeString("{$=ΔΟΥ|en-us=Tax office$}")%></div>
                    <div class="RegUsersTab2_TxtDivSec"><a id="Inv_DOYMsg" href="#"></a><asp:DropDownList cssclass="REgUserDDL" ID="ddlDoy" runat="server" ></asp:DropDownList>
                    <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvDOY" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_DOYMsg="yes" IsInvoice="yes" ControlToValidate="ddlDoy" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    
                    </div>
                    <div class="RegUsersMainDivCloud" style="color:#094595;font-weight:bold">*</div>
                    <div class="Clear"></div>
                    <div id="Inv_DOYMsg_Mob" class="msg_error_mobile" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
                     <div class="RegUsersTab2_BtnNextDiv">
                        <div class="RegUsersFirstPaddingButton">&nbsp;</div>
                        <div class="FLEFT">
                         
                        <asp:ImageButton ID="ImageButton2" OnClientClick="ShowHideTabs('','none','none');return false"  runat="server"  />
                        </div>
                        <div style="float:left;padding-left:10px">
                        <img id="Tab2_NextBtnInvoiceCheckNewCard" style="cursor:pointer" src='<%= ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/BtnNext.png|en-us=/App_Themes/LivePay/RegisteredUsers/BtnNext_en-us.png$}")%>' onclick="AllCardsValidation('2')" />
                        <asp:ImageButton ID="Tab2_NextBtnInvoice" style="display:none" ValidationGroup="Tab2_PayForm" runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/BtnNext.png" />
                        </div>
                        <div class="Clear"></div>
                    </div>
                </div>
                <div class="tab_bottom"><img src="/App_Themes/LivePay/RegisteredUsers/Tab1WhiteBottom.png" /></div>
            </div>
        </div>
    </div>

    
    <div id="Tab3" style="display:none;">
        <div class="PL10">
            <div class="Tab3MainBG">
                <div id="DivTabsTab3">
                    <div class="RegTopTab1btn"><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 1: Στοιχεία Πληρωμής|en-us=Step 1: Payment Information$}")%></div>
                    <div class="RegTopTab2_3btn"><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 2: Στοιχεία Χρέωσης|en-us=Step 2: Card Information$}")%></div>
                    <div class="RegTopTab2_3btn"><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 3: Επιβεβαίωση|en-us=Step 3: Payment Confirmation$}")%></div>
                    <div class="Clear"></div>
                </div>
                <div id="DivFormTab3" style="padding-left:10px;padding-top:20px">
                    <div class="RegUserTab3Info"><%= ResHelper.LocalizeString("{$=Διαβάστε προσεχτικά τα παρακάτω και πατήστε το κουμπί <b>Ολοκλήρωση</b> προκειμένου να πραγματοποιήσετε την Πληρωμή|en-us=Please read the terms and press the "" button to complete your Payment $}")%></div>
                    <div class="RegUserTab3Title"><%= ResHelper.LocalizeString("{$=Πληρωμή προς|en-us=Payment to$}")%></div>
                    <div class="RegUserTab3MerchantTitle"><asp:Label ID="Tab3_lblPayTo" runat="server" CssClass="RegUSerTab3Control" ></asp:Label></div>
                    <div class="Clear"></div>
                    <div id="Tab3_CustomFields" runat="server">
                    </div>
                    <div class="RegUserTab3TitleB"><%= ResHelper.LocalizeString("{$=Ποσό|en-us=Amount$}")%> (&euro;)</div>
                    <div class="RegUsersTab2_TxtDiv"><asp:Label ID="Tab3_lblPrice" runat="server" CssClass="RegUSerTab3Control" ></asp:Label></div>
                    <div class="Clear"></div>

                    <asp:PlaceHolder runat="server" ID="Tab3_Commission" Visible="false">
                        <div class="RegUserTab3TitleB"><%= ResHelper.LocalizeString("{$=Προμήθεια (&euro;)|en-us=Commission (&euro;)$}")%></div>
                        <div class="RegUsersTab2_TxtDiv"><asp:Label ID="Text_Commission" runat="server" CssClass="RegUSerTab3Control" ></asp:Label></div>
                        <div class="Clear"></div>
                    </asp:PlaceHolder>

                    <asp:PlaceHolder runat="server" ID="Tab3_Installments" Visible="false" EnableViewState="false">
                        <div class="RegUserTab3TitleB"><%= ResHelper.LocalizeString("{$=Αριθμός δόσεων |en-us=Number of installments$}")%></div>
                        <div class="RegUsersTab2_TxtDiv"><asp:Label ID="Text_Installments" runat="server" CssClass="RegUSerTab3Control" ></asp:Label></div>
                        <div class="Clear"></div>
                    </asp:PlaceHolder>

                    <div class="RegUserTab3TitleB"><%= ResHelper.LocalizeString("{$=Κάρτα|en-us=Card$}")%></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:Label ID="Tab3_lblCard" runat="server" CssClass="RegUSerTab3Control" ></asp:Label></div>



                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;">&nbsp;</div>
                    <div style="float:left;padding-left:11px;" id="Div_Terms" runat="server"><asp:CheckBox ID="Tab3_chkTermsAccept" cssclass="RegUSerControlA" runat="server" /><%= ResHelper.LocalizeString("{$=Αποδέχομαι τους|en-us=I accept the$}")%> <a href="javascript:void(0);" onclick="ShowTerms();" class="RegUSerControlA"><%= ResHelper.LocalizeString("{$=όρους συναλλαγής|en-us=Payment Terms of Use$}")%></a></div>
                    <div class="Clear"></div>
                    <div class="terms-textdiv" id="TextTerms" style="display:none;height:300px;overflow:auto;">

                    <asp:Label runat="server" ID="Tab3_lblTerms" CssClass="terms-text" />
                        
                    <div style="position:absolute; top:0px; left:94%;"><a href="#" onclick="closePopUp();" id="close"><img src="/App_Themes/LivePay/close.gif" alt="Close" border="0" /></a></div>
                    </div>
                    <div class="Clear"></div>
                     <div class="tab3_btn" style="padding-top:12px;">
                        <div class="RegUsersFirstPaddingButtonTab3">&nbsp;</div>
                        <div class="FLEFT"><asp:ImageButton ID="Tab3_btnReturn"  OnClientClick="ShowHideTabs('none','','none');AllCardsValidation('');return false" runat="server" /></div>
                        <div style="float:left;padding-left:10px"><asp:ImageButton ID="Tab3_btnComplete" OnClientClick="return CheckToComplete();" runat="server" /></div>
                        <div class="Clear"></div>
                    </div>
                </div>
                 <div class="tab_bottom"><img src="/App_Themes/LivePay/RegisteredUsers/Tab1WhiteBottom.png" /></div>
            </div>
        </div>
    </div>
     <div class="RegUserBotMain" style="height:10px">
         <img src="/App_Themes/LivePay/RegisteredUsers/RegUserBotBG.png" />
     </div> 
 </div>

    <asp:TextBox ID="HiddenChoice" runat="server" Text="1" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenCompanyName" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenMerchantValidCards" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenMerchantID" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenMinimumTransactionAmount" runat="server" style="display:none"></asp:TextBox> 
    <asp:TextBox ID="HiddenMaximumTransactionAmount" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenMaximumTransactionsPerDayCard" runat="server" style="display:none"></asp:TextBox> 
    <asp:TextBox ID="HiddenMaximumTransactionsPerDayGlobally" runat="server" style="display:none"></asp:TextBox> 
    <asp:TextBox ID="HiddenMaximumSumTransactionsPerDayInCard" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenMaxTransactionsPerSessionPerCard" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenNotValidCard" runat="server" Text="ok"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenNotValidMerchant" runat="server" Text="ok"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="NewCard_HiddenCardBelongToOtherUser" runat="server" Text="ok"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="NewCard_HiddenNotValidCard" runat="server" Text="ok"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="NewCard_HiddenNotValidMerchant" runat="server" Text="ok"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="CardBelongToOtherUser" runat="server"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="hiddenInstallments" runat="server" text="-1" style="display:none"></asp:TextBox>
    <asp:TextBox ID="hiddenUserCardID" runat="server" text="0" style="display:none"></asp:TextBox>
    
<asp:Literal ID="lit_JsScript" runat="server" ></asp:Literal>
</ContentTemplate>

<Triggers>
    <asp:AsyncPostBackTrigger ControlID="Tab1_BtnNext" />
    <asp:AsyncPostBackTrigger ControlID="Tab2_BtnNext" />
    <asp:AsyncPostBackTrigger ControlID="Tab3_btnComplete" />
</Triggers>
</asp:UpdatePanel>
  <script language="javascript" type="text/javascript" >
     
      FixChromeHeight()
      function FixChromeHeight() {
          var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
          if (is_chrome) {
//              document.getElementById('Tab2_PhoneDiv').className = 'RegUsersTab2_PhoneDivChrome'
//              document.getElementById('Tab2_BtnNextDiv').className = 'RegUsersTab2_BtnNextDivChrome'
              document.getElementById('Tab2_RB2Div').className = 'RegUsersTab2_RB2DivChrome'
          }

      }
       
      CompanyName = document.getElementById('<%=HiddenCompanyName.ClientID %>')
      MerchantValidCards = document.getElementById('<%=HiddenMerchantValidCards.ClientID %>')
      MaximumTransactionsPerDayCard = document.getElementById('<%=HiddenMaximumTransactionsPerDayCard.ClientID %>')
      MaximumTransactionsPerDayGlobally = document.getElementById('<%=HiddenMaximumTransactionsPerDayGlobally.ClientID %>')
      MaximumSumTransactionsPerDayInCard = document.getElementById('<%=HiddenMaximumSumTransactionsPerDayInCard.ClientID %>')
      MaxTransactionsPerSessionPerCard = document.getElementById('<%=HiddenMaxTransactionsPerSessionPerCard.ClientID %>')
      MerchantID = document.getElementById('<%=HiddenMerchantID.ClientID %>')

      if (mobileDivice.isMobile()) {
          SetZoom_()
      }
      
  </script>