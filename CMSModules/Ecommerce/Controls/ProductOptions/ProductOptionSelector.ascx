<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductOptionSelector.ascx.cs" Inherits="CMSModules_Ecommerce_Controls_ProductOptions_ProductOptionSelector" %>
<asp:Panel ID="pnlContainer" runat="server" CssClass="ProductOptionSelectorContainer">
    <cms:LocalizedLabel ID="lblCategName" runat="server" CssClass="OptionCategoryName" EnableViewState="false" />
    <asp:Panel ID="pnlSelector" runat="server" CssClass="ProductOptionSelector" />    
    <asp:Label ID="lblCategDescription" runat="server" CssClass="OptionCategoryDescription" EnableViewState="false" />
</asp:Panel>