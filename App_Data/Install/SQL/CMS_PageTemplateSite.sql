CREATE TABLE [CMS_PageTemplateSite] (
		[PageTemplateID]     int NOT NULL,
		[SiteID]             int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_PageTemplateSite]
	ADD
	CONSTRAINT [PK_CMS_PageTemplateSite]
	PRIMARY KEY
	([PageTemplateID], [SiteID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_PageTemplateSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_PageTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplateSite_PageTemplateID_CMS_PageTemplate]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplateSite]
			ADD CONSTRAINT [FK_CMS_PageTemplateSite_PageTemplateID_CMS_PageTemplate]
			FOREIGN KEY ([PageTemplateID]) REFERENCES [CMS_PageTemplate] ([PageTemplateID])
END
IF OBJECT_ID(N'[CMS_PageTemplateSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplateSite_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplateSite]
			ADD CONSTRAINT [FK_CMS_PageTemplateSite_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
