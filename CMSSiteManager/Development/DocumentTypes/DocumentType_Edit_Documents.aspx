<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="DocumentType_Edit_Documents.aspx.cs" Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_Documents"
    Title="Document Type Edit - Documents" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<%@ Register Src="~/CMSFormControls/Filters/DocumentFilter.ascx" TagName="DocumentFilter"
    TagPrefix="cms" %>
<asp:Content ID="Content" ContentPlaceHolderID="plcContent" runat="server">
    <cms:DocumentFilter ID="filterDocuments" runat="server" LoadSites="true" />
    <br />
    <br />
    <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cms:LocalizedLabel ID="lblTitle" runat="server" ResourceString="document_type_edit.documents.used"
                DisplayColon="true" EnableViewState="false" />
            <br />
            <br />
            <cms:UniGrid ID="ucDocuments" runat="server" GridName="DocumentType_Documents.xml"
                HideControlForZeroRows="false" IsLiveSite="false" />
        </ContentTemplate>
    </cms:CMSUpdatePanel>
</asp:Content>
