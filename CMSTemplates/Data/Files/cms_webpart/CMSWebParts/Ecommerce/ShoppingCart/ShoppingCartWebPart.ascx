<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Ecommerce/ShoppingCart/ShoppingCartWebPart.ascx.cs"
    Inherits="CMSWebParts_Ecommerce_ShoppingCart_ShoppingCartWebPart" %>
<%@ Register Src="~/CMSModules/Ecommerce/Controls/ShoppingCart/ShoppingCart.ascx" TagName="ShoppingCart" TagPrefix="cms" %>
<cms:ShoppingCart ID="cartElem" runat="server" />
