﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CustomFieldsMerchants.ascx.vb"
    Inherits="CMSWebParts_LivePay_Filter_CustomFieldsMerchants" %>

<div class="SrHistPayBG ">
    <div class="SrHistPayTitle">
        <%= ResHelper.LocalizeString("{$=Επεξεργασία Custom πεδίων Εμπόρου|en-us=Merchand Custom Fields Edit$}")%></div>
    <div style="padding: 20px 10px 20px 10px">
            <div style="float:left; line-height:22px;padding-right:5px;"><%= ResHelper.LocalizeString("{$=CompanyName|en-us=CompanyName$}")%>:</div>
            <div style="float:left; padding-right:10px;"><asp:TextBox ID="txtMerchantsName" runat="server"></asp:TextBox></div>
            <div><asp:ImageButton ID="btnFilter" OnClientClick="return CheckStoreForm()" runat="server"  /></div>
    </div>
    <div class="GridMainGrid">
        <div class="SrcHisPayGridBGTop650">
            <div class="SrcHisPayGridTopTitle">
                <%=ResHelper.LocalizeString("{$=Custom πεδία|en-us=Custom πεδία$}") %></div>
            <div class="SrcHisPayGridDiv">
                <asp:GridView ID="GridViewPayments" EmptyDataTemplate-CssClass="SrcHisPayPagerEmpty"
                    PagerStyle-CssClass="SrcHisPayPager650" GridLines="None" runat="server" PageSize="10"
                    AutoGenerateColumns="false" AllowPaging="true" Width="650px">
                    <HeaderStyle BackColor="#ffffff" ForeColor="#58595b" Height="35px" Font-Size="10px" />
                    <AlternatingRowStyle Height="25px" BackColor="#ffffff" ForeColor="#58595b" Font-Size="11px"
                        Font-Names="tahoma" />
                    <RowStyle Height="25px" BackColor="#f7f7f7" ForeColor="#58595b" Font-Size="11px"
                        Font-Names="tahoma" />
                    <Columns>
                        <asp:TemplateField HeaderText="Επωνυμία Εμπόρου" HeaderStyle-CssClass="MerchSrcHeaderLeft MerchSrcHeader"
                            ItemStyle-CssClass="MerchSrcItemLeft">
                            <ItemTemplate>
                                <asp:Label ID="lblBoxNo" runat="server" Text='<%#Eval("CompanyName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Merchant" HeaderStyle-CssClass="MerchSrcHeaderSimple"
                            ItemStyle-CssClass="MerchSrcItemSimple">
                            <ItemTemplate>
                                <asp:Label ID="lblBoxNo" runat="server" Text='<%#Eval("Merchant") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NameFieldGR" HeaderStyle-CssClass="MerchSrcHeaderSimple"
                            ItemStyle-CssClass="MerchSrcItemSimple">
                            <ItemTemplate>
                                <asp:Label ID="lblBoxNo" runat="server" Text='<%#Eval("NameFieldGR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NameFieldEN" HeaderStyle-CssClass="MerchSrcHeaderSimple"
                            ItemStyle-CssClass="MerchSrcItemSimple">
                            <ItemTemplate>
                                <asp:Label ID="lblBoxNo" runat="server" Text='<%#Eval("NameFieldEN") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MaxLength" HeaderStyle-CssClass="MerchSrcHeaderSimple"
                            ItemStyle-CssClass="MerchSrcItemSimple">
                            <ItemTemplate>
                                <asp:Label ID="lblBoxNo" runat="server" Text='<%#Eval("MaxLength") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mandatory" HeaderStyle-CssClass="MerchSrcHeaderSimple"
                            ItemStyle-CssClass="MerchSrcItemSimple">
                            <ItemTemplate>
                                <asp:Label ID="lblBoxNo" runat="server" Text='<%#Eval("Mandatory") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescriptionGR" HeaderStyle-CssClass="MerchSrcHeaderSimple"
                            ItemStyle-CssClass="MerchSrcItemSimple">
                            <ItemTemplate>
                                <asp:Label ID="lblBoxNo" runat="server" Text='<%#Eval("DescriptionGR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DescriptionEN" HeaderStyle-CssClass="MerchSrcHeaderSimple"
                            ItemStyle-CssClass="MerchSrcItemSimple">
                            <ItemTemplate>
                                <asp:Label ID="lblBoxNo" runat="server" Text='<%#Eval("DescriptionEN") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="SrcHisPayGridHeaderLast" ItemStyle-CssClass="SrcHisPayGridItemLast">
                            <ItemTemplate>
                                   <a href="http://livepay.realize.gr/CMSModules/CustomTables/Tools/CustomTable_Data_EditItem.aspx?customtableid=2402&itemId=<%# Eval("itemid")%>"
                                    target="_blank">Link</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <div class="SrcHisPayPagerEmpty650">
                            <div style="padding-top: 5px; padding-left: 10px">
                                <%=ResHelper.LocalizeString("{$=NoRecords|en-us=NoRecords$}") %></div>
                        </div>
                    </EmptyDataTemplate>
                    <PagerTemplate>
                        <div class="SrcHisPayPagerSpace">
                            <div>
                                <div class="GridPagerNoLinkDivLeft5">
                                    <asp:LinkButton ID="lnkPrev" CssClass="PagerText" OnClick="ChangePageByLinkNumber_Before"
                                        runat="server"><%= ResHelper.LocalizeString("{$=Προηγούμενη|en-us=Προηγούμενη$}")%></asp:LinkButton>&nbsp;</div>
                                <div class="GridPagerNoLinkDivCenter5" id="lblPageIndex" runat="server">
                                    &nbsp;</div>
                                <div class="GridPagerNoLinkDivRight">
                                    &nbsp;<asp:LinkButton ID="lnkNext" CssClass="PagerText" OnClick="ChangePageByLinkNumber_Next"
                                        runat="server"><%=ResHelper.LocalizeString("{$=Επόμενη|en-us=Επόμενη$}") %></asp:LinkButton></div>
                            </div>
                        </div>
                    </PagerTemplate>
                </asp:GridView>
            </div>
            <asp:LinkButton ID="HiddenPageBtn" Style="display: none" runat="server" Text="0"></asp:LinkButton>
            <asp:TextBox ID="HiddenPage" Style="display: none" runat="server" Text="1"></asp:TextBox>
            <asp:TextBox ID="HiddenSearchType" Style="display: none" runat="server" Text="0"></asp:TextBox>
            <div id="BottomDiv" style="display: none" runat="server" class="SrcHisPayGridBGBottom">
            </div>
        </div>
    </div>
    <div>
        <img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
</div>
</div>
<script language="javascript" type="text/javascript">

    function GoToPage(no) {
        document.getElementById('<%=HiddenPage.ClientID %>').value = no;
        document.getElementById('<%=HiddenPageBtn.ClientID %>').innerHTML = no;
        var GetID = '<%=HiddenPageBtn.ClientID %>'
        GetID = GetID.replace(/[_]/gi, "$")
        __doPostBack(GetID, '')
    }

    CheckAddtributes()
    function CheckAddtributes() {
        var GetSrcType = document.getElementById('<%=HiddenSearchType.ClientID %>').value
        var GetForm = document.getElementById('DivFullSrc');
        if (GetSrcType == '1') {
            GetForm.style.display = ''
        }
    }


    
</script>
