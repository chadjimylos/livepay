CREATE PROCEDURE Proc_CMS_ACL_ClearACLs
	@NodeID int, @NewACLID int
AS
BEGIN
	update CMS_Tree SET NodeACLID = @NewACLID where NodeID=@NodeID
	delete from CMS_ACLItem where ACLID in (select CMS_ACL.ACLID from CMS_ACL left join CMS_Tree on cms_tree.nodeid = cms_acl.aclownernodeid where (cms_tree.nodeid is null) or (cms_tree.nodeid is not null and cms_tree.nodeaclid <> cms_acl.aclid))
	delete from cms_acl where aclid in (select cms_acl.aclid from cms_acl left join cms_tree on cms_tree.nodeid = cms_acl.aclownernodeid where (cms_tree.nodeid is null) or (cms_tree.nodeid is not null and cms_tree.nodeaclid <> cms_acl.aclid))
END
