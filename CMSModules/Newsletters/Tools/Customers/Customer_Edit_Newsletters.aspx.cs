using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Newsletter;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.LicenseProvider;

public partial class CMSModules_Newsletters_Tools_Customers_Customer_Edit_Newsletters : CMSEcommercePage
{
    private int siteId = 0;
    private string currentValues = string.Empty;
    private string email = null;
    private string firstName = null;
    private string lastName = null;

    /// <summary>
    /// On page load.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Event arguments.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check the license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), "") != "")
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Newsletters);
        }

        // Check site availability
        if (!ResourceSiteInfoProvider.IsResourceOnSite("CMS.Newsletter", CMSContext.CurrentSiteName))
        {
            RedirectToResourceNotAvailableOnSite("CMS.Newsletter");
        }

        CurrentUserInfo user = CMSContext.CurrentUser;

        // Check 'NewsletterRead' permission
        if (!user.IsAuthorizedPerResource("CMS.Newsletter", "Read"))
        {
            RedirectToCMSDeskAccessDenied("CMS.Newsletter", "Read");
        }

        lblTitle.Text = ResHelper.GetString("Customer_Edit_Newsletters.Title");

        siteId = CMSContext.CurrentSiteID;

        // Load customer data
        IInfoObject customerObj = ModuleCommands.ECommerceGetCustomerInfo(QueryHelper.GetInteger("customerId", 0));
        if (customerObj != null)
        {
            email = Convert.ToString(customerObj.GetValue("CustomerEmail"));
            firstName = Convert.ToString(customerObj.GetValue("CustomerFirstName"));
            lastName = Convert.ToString(customerObj.GetValue("CustomerLastName"));
        }

        usNewsletters.WhereCondition = "NewsletterSiteID = " + siteId;
        usNewsletters.ButtonRemoveSelected.CssClass = "XLongButton";
        usNewsletters.ButtonAddItems.CssClass = "XLongButton";
        usNewsletters.OnSelectionChanged += new EventHandler(usNewsletters_OnSelectionChanged);

        Subscriber sb = SubscriberProvider.GetSubscriberByEmail(email, siteId);
        if (sb != null)
        {
            // Get selected newsletters
            DataSet ds = SubscriberNewsletterInfoProvider.GetSubscriberNewsletters(sb.SubscriberID, null, -1, "NewsletterID");
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "NewsletterID"));
            }

            // Load selected newsletters
            if (!IsPostBack)
            {
                usNewsletters.Value = currentValues;
            }
        }
    }


    private void usNewsletters_OnSelectionChanged(object sender, EventArgs e)
    {
        // Check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            CMSPage.RedirectToCMSDeskAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        if (email != null)
        {
            // Check wheather subcriber already exist
            Subscriber sb = SubscriberProvider.GetSubscriberByEmail(email, siteId);
            if (sb == null)
            {
                // Create new subscriber
                sb = new Subscriber();
                sb.SubscriberEmail = email;
                sb.SubscriberFirstName = firstName;
                sb.SubscriberLastName = lastName;
                sb.SubscriberSiteID = siteId;
                sb.SubscriberGUID = Guid.NewGuid();
                SubscriberProvider.SetSubscriber(sb);
            }

            // Remove old items
            string newValues = ValidationHelper.GetString(usNewsletters.Value, null);
            string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
            if (!String.IsNullOrEmpty(items))
            {
                string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (newItems != null)
                {
                    foreach (string item in newItems)
                    {
                        int newsletterId = ValidationHelper.GetInteger(item, 0);

                        // If subscriber is subscribed, unsubscribe him
                        if (SubscriberProvider.IsSubscribed(sb.SubscriberID, newsletterId))
                        {
                            try
                            {
                                SubscriberProvider.Unsubscribe(sb.SubscriberGUID, newsletterId);
                            }
                            catch { };
                        }
                    }
                }
            }

            // Add new items
            items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
            if (!String.IsNullOrEmpty(items))
            {
                string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                if (newItems != null)
                {
                    foreach (string item in newItems)
                    {
                        int newsletterId = ValidationHelper.GetInteger(item, 0);

                        // If subscriber is not subscribed, subscribe him
                        if (!SubscriberProvider.IsSubscribed(sb.SubscriberID, newsletterId))
                        {
                            try
                            {
                                SubscriberProvider.Subscribe(sb.SubscriberID, newsletterId, DateTime.Now);
                            }
                            catch { };
                        }
                    }
                }
            }

            // Display information about successful (un)subscription
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }
    }
}
