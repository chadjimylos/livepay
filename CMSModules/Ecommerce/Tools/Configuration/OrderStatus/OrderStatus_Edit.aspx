<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrderStatus_Edit.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_OrderStatus_OrderStatus_Edit"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="E-commerce Configuration - Order status properties" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblStatusDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtStatusDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvStatusDisplayName" runat="server" ErrorMessage=""
                    ControlToValidate="txtStatusDisplayName" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblStatusName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtStatusName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvStatusName" runat="server" ErrorMessage="" ControlToValidate="txtStatusName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblStatusColor" EnableViewState="false" />
            </td>
            <td>
                <cms:ColorPicker ID="clrPicker" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblStatusEmail" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox ID="chkStatusEmail" runat="server" CssClass="CheckBoxMovedLeft" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblStatusEnabled" EnableViewState="false"
                    ResourceString="general.enabled" DisplayColon="true" />
            </td>
            <td>
                <asp:CheckBox ID="chkStatusEnabled" runat="server" CssClass="CheckBoxMovedLeft" Checked="true"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
