<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AutoResizeConfiguration.ascx.cs" Inherits="CMSAdminControls_AutoResizeConfiguration" %>
<table width="297">
    <tr>
        <td colspan="2">            
            <cms:LocalizedDropDownList ID="drpSettings" runat="server" UseResourceStrings="true" CssClass="DropDownField">
                <asp:ListItem  Text="dialogs.resize.donotresize" Value="noresize" />
                <asp:ListItem  Text="dialogs.resize.usesitesettings" Value="" />
                <asp:ListItem Text="dialogs.resize.usecustomsettings" Value="custom" />
            </cms:LocalizedDropDownList>
        </td>
    </tr>
    <tr>
        <td style="width:142px;"><cms:LocalizedLabel ID="lblWidth" runat="server" EnableViewState="false" ResourceString="dialogs.resize.width" /></td>
        <td><asp:TextBox ID="txtWidth" runat="server" CssClass="SmallTextBox" /></td>
    </tr>
    <tr>
        <td style="width:142px;"><cms:LocalizedLabel ID="lblHeight" runat="server" EnableViewState="false" ResourceString="dialogs.resize.height" /></td>
        <td><asp:TextBox ID="txtHeight" runat="server" CssClass="SmallTextBox" /></td>
    </tr>
    <tr>
        <td style="width:142px;white-space:nowrap;"><cms:LocalizedLabel ID="lblMax" runat="server" EnableViewState="false" ResourceString="dialogs.resize.maxsidesize" /></td>
        <td><asp:TextBox ID="txtMax" runat="server" CssClass="SmallTextBox" /></td>
    </tr>    
</table>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />