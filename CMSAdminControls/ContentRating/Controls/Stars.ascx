<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Stars.ascx.cs" Inherits="CMSAdminControls_ContentRating_Controls_Stars" %>
<ajaxToolkit:Rating ID="elemRating" runat="server"
    StarCssClass="ratingStar"
    WaitingStarCssClass="savedRatingStar"
    FilledStarCssClass="filledRatingStar"
    EmptyStarCssClass="emptyRatingStar"
    />
