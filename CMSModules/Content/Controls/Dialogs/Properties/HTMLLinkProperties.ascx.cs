using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;

public partial class CMSModules_Content_Controls_Dialogs_Properties_HTMLLinkProperties : ItemProperties
{

    /// <summary>
    /// Gets or sets the value which determines whether to show or hide general tab.
    /// </summary>
    public bool ShowGeneralTab
    {
        get
        {
            return this.tabGeneral.Visible;
        }
        set
        {
            this.tabGeneral.Visible = value;
            this.tabGeneral.HeaderText = (value ? ResHelper.GetString("general.general") : "");
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether the control is displayed on the Web tab.
    /// </summary>
    public bool IsWeb
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["IsWeb"], false);
        }
        set
        {
            ViewState["IsWeb"] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.StopProcessing)
        {
            SetupControls();
        }
        else
        {
            this.pnlEmpty.Visible = true;
            this.pnlTabs.Visible = false;
            this.lblEmpty.Text = this.NoSelectionText;
        }
    }


    private void SetupControls()
    {
        // Check AjaxToolkitVersion version
        bool isLatestFramework = true;
        if (AJAXHelper.AjaxToolkitVersion < 3)
        {
            isLatestFramework = false;
        }

        if (this.IsWeb && !this.ShowGeneralTab && isLatestFramework)
        {
            this.pnlTabs.ActiveTabIndex = 1;
        }
        else
        {
            this.pnlTabs.ActiveTabIndex = 0;
        }

        if (!UrlHelper.IsPostback() && this.IsWeb)         
        {
            this.pnlEmpty.Visible = false;
            this.pnlTabs.CssClass = "Dialog_Tabs";
        }

        // Script for hiding target frame
        ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "targetSelectedScript", ScriptHelper.GetScript(
            "function targetSelected() { " + "\n" +
            "    var txt = document.getElementById('" + this.txtTargetFrame.ClientID + "');" + "\n" +
            "    var lbl = document.getElementById('" + this.lblTargetFrame.ClientID + "');" + "\n" +
            "    var drp = document.getElementById('" + this.drpTarget.ClientID + "');" + "\n" +
            "    if ((drp != null) && (txt != null) && (lbl != null)) {" + "\n" +
            "        if (drp.value != \"frame\") {" + "\n" +
            "            txt.style.display = 'none';" + "\n" +
            "            lbl.style.display = 'none'; " + "\n" +
            "        } else {" + "\n" +
            "            txt.style.display = 'inline';" + "\n" +
            "            lbl.style.display = 'block';" + "\n" +
            "        }" + "\n" +
            "    }" + "\n" +
            "}"));

        this.btnHidden.Click += new EventHandler(btnHidden_Click);
        this.tabGeneral.HeaderText = (this.ShowGeneralTab ? ResHelper.GetString("general.general") : "");
        this.tabAdvanced.HeaderText = ResHelper.GetString("dialogs.tab.advanced");
        this.tabTarget.HeaderText = ResHelper.GetString("dialogs.tab.target");

        this.lblEmpty.Text = this.NoSelectionText;

        if (ValidationHelper.GetBoolean(SessionHelper.GetValue("HideLinkText"), false))
        {
            this.urlSelectElem.LinkTextEnabled = false;
        }

        string postBackRef = ControlsHelper.GetPostBackEventReference(this.btnHidden, "");
        string postBackKeyDownRef = "var keynum;if(window.event){keynum = event.keyCode;}else if(event.which){keynum = event.which;}if(keynum == 13){" + postBackRef + "; return false;}";

        this.urlSelectElem.TextBoxUrl.Attributes["onchange"] = postBackRef;
        this.urlSelectElem.TextBoxUrl.Attributes["onkeydown"] = postBackKeyDownRef;
        this.urlSelectElem.TextBoxLinkText.Attributes["onchange"] = postBackRef;
        this.urlSelectElem.TextBoxLinkText.Attributes["onkeydown"] = postBackKeyDownRef;
        this.urlSelectElem.DropDownProtocol.Attributes["onchange"] = postBackRef;
        this.drpTarget.Attributes["onchange"] = "targetSelected();" + postBackRef;
        this.txtTargetFrame.Attributes["onchange"] = postBackRef;
        this.txtTargetFrame.Attributes["onkeydown"] = postBackKeyDownRef;
        this.txtAdvId.Attributes["onchange"] = postBackRef;
        this.txtAdvId.Attributes["onkeydown"] = postBackKeyDownRef;
        this.txtAdvName.Attributes["onchange"] = postBackRef;
        this.txtAdvName.Attributes["onkeydown"] = postBackKeyDownRef;
        this.txtAdvTooltip.Attributes["onchange"] = postBackRef;
        this.txtAdvTooltip.Attributes["onkeydown"] = postBackKeyDownRef;
        this.txtAdvStyleSheet.Attributes["onchange"] = postBackRef;
        this.txtAdvStyleSheet.Attributes["onkeydown"] = postBackKeyDownRef;
        this.txtAdvStyle.Attributes["onchange"] = postBackRef;
        this.txtAdvStyle.Attributes["onkeydown"] = postBackKeyDownRef;

        if (this.drpTarget.SelectedValue != "frame")
        {
            this.txtTargetFrame.Style.Add("display", "none");
            this.lblTargetFrame.Style.Add("display", "none");
        }
        else
        {
            this.txtTargetFrame.Style.Add("display", "inline");
            this.lblTargetFrame.Style.Add("display", "block");
        }
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.StopProcessing)
        {
            // Load target dropdown with values
            this.drpTarget.Items.Add(new ListItem(ResHelper.GetString("dialogs.target.notset"), "notset"));
            this.drpTarget.Items.Add(new ListItem(ResHelper.GetString("dialogs.target.frame"), "frame"));
            this.drpTarget.Items.Add(new ListItem(ResHelper.GetString("dialogs.target.blank"), "_blank"));
            this.drpTarget.Items.Add(new ListItem(ResHelper.GetString("dialogs.target.self"), "_self"));
            this.drpTarget.Items.Add(new ListItem(ResHelper.GetString("dialogs.target.parent"), "_parent"));
            this.drpTarget.Items.Add(new ListItem(ResHelper.GetString("dialogs.target.top"), "_top"));
        }
    }


    protected void btnHidden_Click(object sender, EventArgs e)
    {
        SaveSession();
    }


    /// <summary>
    /// Save current properties into session
    /// </summary>
    private void SaveSession()
    {
        Hashtable savedProperties = SessionHelper.GetValue("DialogSelectedParameters") as Hashtable;
        if (savedProperties == null)
        {
            savedProperties = new Hashtable();
        }
        Hashtable properties = GetItemProperties();
        foreach (DictionaryEntry entry in properties)
        {
            savedProperties[entry.Key] = entry.Value;
        }
        SessionHelper.SetValue("DialogSelectedParameters", savedProperties);
    }


    #region "Overriden methods"

    /// <summary>
    /// Loads given link parameters.
    /// </summary>
    /// <param name="item"></param>
    public override void LoadSelectedItems(MediaItem item, Hashtable properties)
    {

        // Display the properties
        this.pnlEmpty.Visible = false;
        this.pnlTabs.CssClass = "Dialog_Tabs";
        this.tabGeneral.HeaderText = (this.ShowGeneralTab ? ResHelper.GetString("general.general") : "");

        if (properties != null)
        {
            LoadProperties(properties);
            string shortUrl = UrlHelper.RemoveProtocol(item.Url);
            string protocol = item.Url.Substring(0, item.Url.Length - shortUrl.Length);
            if ((protocol == "http://") || (protocol == "https://") ||
                (protocol == "ftp://") || (protocol == "news://"))
            {
                this.urlSelectElem.LinkProtocol = protocol;
                this.urlSelectElem.LinkURL = shortUrl;
            }
            else
            {
                this.urlSelectElem.LinkProtocol = "other";
                this.urlSelectElem.LinkURL = item.Url;
            }
            this.urlSelectElem.LinkText = item.Name;

            if (item.MediaType == MediaTypeEnum.Flash)
            {
                this.urlSelectElem.LinkURL = UrlHelper.UpdateParameterInUrl(this.urlSelectElem.LinkURL, "ext", "." + item.Extension.TrimStart('.'));
            }
        }
        SaveSession();
    }


    /// <summary>
    /// Loads the properites into control
    /// </summary>
    /// <param name="properties"></param>
    public override void LoadItemProperties(Hashtable properties)
    {
        // Hide the link text
        this.urlSelectElem.LinkTextEnabled = false;
        SessionHelper.SetValue("HideLinkText", true);

        if (properties != null)
        {
            // Display the properties
            this.pnlEmpty.Visible = false;
            this.pnlTabs.CssClass = "Dialog_Tabs";
            // Load properties
            LoadProperties(properties);
        }
        SaveSession();
    }

    /// <summary>
    /// Loads the properites
    /// </summary>
    /// <param name="properties">Properties collection.</param>
    public override void LoadProperties(Hashtable properties)
    {
        if (properties != null)
        {
            #region "General tab"

            string linkText = ValidationHelper.GetString(properties[DialogParameters.LINK_TEXT], "");
            string linkProtocol = ValidationHelper.GetString(properties[DialogParameters.LINK_PROTOCOL], "other");
            string linkUrl = ValidationHelper.GetString(properties[DialogParameters.LINK_URL], "");

            this.urlSelectElem.LinkText = HttpUtility.HtmlDecode(linkText);
            this.urlSelectElem.LinkURL = linkUrl;
            this.urlSelectElem.LinkProtocol = linkProtocol;

            #endregion

            #region "Target tab"

            string linkTarget = ValidationHelper.GetString(properties[DialogParameters.LINK_TARGET], "");
            if (linkTarget == "")
            {
                linkTarget = "notset";
            }

            this.drpTarget.ClearSelection();
            ListItem liTarget = this.drpTarget.Items.FindByValue(linkTarget);
            if (liTarget != null)
            {
                liTarget.Selected = true;
            }
            else if (linkTarget != "notset")
            {
                // Select specific frame
                this.drpTarget.SelectedIndex = 1;
                this.txtTargetFrame.Text = HttpUtility.HtmlDecode(linkTarget);
            }

            #endregion

            #region "Advanced tab"

            string linkId = ValidationHelper.GetString(properties[DialogParameters.LINK_ID], "");
            string linkName = ValidationHelper.GetString(properties[DialogParameters.LINK_NAME], "");
            string linkTooltip = ValidationHelper.GetString(properties[DialogParameters.LINK_TOOLTIP], "");
            string linkClass = ValidationHelper.GetString(properties[DialogParameters.LINK_CLASS], "");
            string linkStyle = ValidationHelper.GetString(properties[DialogParameters.LINK_STYLE], "");

            this.txtAdvId.Text = HttpUtility.HtmlDecode(linkId);
            this.txtAdvName.Text = HttpUtility.HtmlDecode(linkName);
            this.txtAdvTooltip.Text = HttpUtility.HtmlDecode(linkTooltip);
            this.txtAdvStyleSheet.Text = HttpUtility.HtmlDecode(linkClass);
            this.txtAdvStyle.Text = HttpUtility.HtmlDecode(linkStyle);

            #endregion

            #region "General items"

            this.EditorClientID = ValidationHelper.GetString(properties[DialogParameters.EDITOR_CLIENTID], "");

            #endregion
        }
    }

    /// <summary>
    /// Returns all parameters of the selected item as name – value collection.
    /// </summary>
    public override Hashtable GetItemProperties()
    {
        Hashtable retval = new Hashtable();

        #region "General tab"

        retval[DialogParameters.LINK_TEXT] = HTMLHelper.HTMLEncode(this.urlSelectElem.LinkText.Replace("%", "%25"));
        retval[DialogParameters.LINK_URL] = UrlHelper.ResolveUrl(this.urlSelectElem.LinkURL.Trim().Replace("%", "%25"));
        retval[DialogParameters.LINK_PROTOCOL] = this.urlSelectElem.LinkProtocol;

        #endregion

        #region "Target tab"

        if (this.drpTarget.SelectedIndex > 0)
        {
            if (this.drpTarget.SelectedIndex == 1)
            {
                retval[DialogParameters.LINK_TARGET] = HTMLHelper.HTMLEncode(this.txtTargetFrame.Text.Trim().Replace("%", "%25"));
            }
            else
            {
                retval[DialogParameters.LINK_TARGET] = this.drpTarget.SelectedValue;
            }
        }
        else
        {
            retval[DialogParameters.LINK_TARGET] = "";
        }

        #endregion

        #region "Advanced tab"

        retval[DialogParameters.LINK_ID] = HTMLHelper.HTMLEncode(this.txtAdvId.Text.Trim().Replace("%", "%25"));
        retval[DialogParameters.LINK_NAME] = HTMLHelper.HTMLEncode(this.txtAdvName.Text.Trim().Replace("%", "%25"));
        retval[DialogParameters.LINK_TOOLTIP] = HTMLHelper.HTMLEncode(this.txtAdvTooltip.Text.Trim().Replace("%", "%25"));
        retval[DialogParameters.LINK_CLASS] = HTMLHelper.HTMLEncode(this.txtAdvStyleSheet.Text.Trim().Replace("%", "%25"));
        string style = this.txtAdvStyle.Text.Replace(Environment.NewLine, "").Trim();
        style = style.TrimEnd(';') + ";";
        retval[DialogParameters.LINK_STYLE] = (style != ";" ? HTMLHelper.HTMLEncode(style.Replace("%", "%25")) : "");

        #endregion

        #region "General items"

        retval[DialogParameters.EDITOR_CLIENTID] = (String.IsNullOrEmpty(EditorClientID) ? "" : EditorClientID.Replace("%", "%25"));
        
        #endregion

        return retval;
    }


    /// <summary>
    /// Clears the properties form.
    /// </summary>
    public override void ClearProperties(bool hideProperties)
    {

        // Hide the properties
        this.pnlEmpty.Visible = hideProperties;
        this.pnlTabs.CssClass = (hideProperties ? "DialogElementHidden" : "Dialog_Tabs");

        this.pnlTabs.ActiveTabIndex = 0;

        this.urlSelectElem.LinkTextEnabled = true;
        this.urlSelectElem.LinkProtocol = "other";
        this.urlSelectElem.LinkText = "";
        this.urlSelectElem.LinkURL = "";

        this.txtTargetFrame.Text = "";
        this.drpTarget.SelectedIndex = 0;

        this.txtAdvId.Text = "";
        this.txtAdvName.Text = "";
        this.txtAdvStyle.Text = "";
        this.txtAdvStyleSheet.Text = "";
        this.txtAdvTooltip.Text = "";

    }

    #endregion
}
