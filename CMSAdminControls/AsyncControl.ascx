<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AsyncControl.ascx.cs" Inherits="CMSAdminControls_AsyncControl" %>
<cms:CMSButton ID="btnFinished" runat="server" OnClick="btnFinished_Click" />
<cms:CMSButton ID="btnError" runat="server" OnClick="btnError_Click" />
<cms:CMSButton ID="btnCancel" runat="server" OnClick="btnCancel_Click" />
<div id="AsyncLog">
</div>
<asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
