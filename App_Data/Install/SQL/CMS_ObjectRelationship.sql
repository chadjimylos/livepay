CREATE TABLE [CMS_ObjectRelationship] (
		[RelationshipLeftObjectID]        int NOT NULL,
		[RelationshipLeftObjectType]      nvarchar(100) NOT NULL,
		[RelationshipNameID]              int NOT NULL,
		[RelationshipRightObjectID]       int NOT NULL,
		[RelationshipRightObjectType]     nvarchar(100) NOT NULL,
		[RelationshipCustomData]          nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_ObjectRelationship]
	ADD
	CONSTRAINT [PK_CMS_ObjectRelationship]
	PRIMARY KEY
	([RelationshipLeftObjectID], [RelationshipLeftObjectType], [RelationshipNameID], [RelationshipRightObjectID], [RelationshipRightObjectType])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_ObjectRelationship]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_RelationshipName]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_ObjectRelationship_RelationshipNameID_CMS_RelationshipName]') IS NULL
BEGIN
		ALTER TABLE [CMS_ObjectRelationship]
			ADD CONSTRAINT [FK_CMS_ObjectRelationship_RelationshipNameID_CMS_RelationshipName]
			FOREIGN KEY ([RelationshipNameID]) REFERENCES [CMS_RelationshipName] ([RelationshipNameID])
END
CREATE INDEX [IX_CMS_ObjectRelationship_RelationshipLeftObjectType_RelationshipLeftObjectID]
	ON [CMS_ObjectRelationship] ([RelationshipLeftObjectType], [RelationshipLeftObjectID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_ObjectRelationship_RelationshipRightObjectType_RelationshipRightObjectID]
	ON [CMS_ObjectRelationship] ([RelationshipRightObjectType], [RelationshipRightObjectID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
