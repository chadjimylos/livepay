﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.Controls

Partial Class CMSTemplates_AegeanPower_UCCustomerBills
    Inherits CMSUserControl

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If

        If Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub


    Public Overloads Sub ReloadData()
        SetAttributes()
        Bind()
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()

    End Sub


    Private Sub SetAttributes()
        hiddenYear.Text = Now.Year
        hiddenProviderNumber.Text = "0003242334342"
        LlbProviderNo.InnerText = "0003242334342"
    End Sub



    Private Sub Bind()
        Dim ProviderNo As String = hiddenProviderNumber.Text
        Dim DataYear As Integer = hiddenYear.Text
        Dim ViewType As Integer = hiddenViewType.Text
        Dim dt As DataTable = AegeanPower_DBConnection.GetCustomerBills(ProviderNo, DataYear, ViewType, 1)
        GvBills.DataSource = dt
        GvBills.DataBind()



        If ViewType = 0 Then
            LnkViewAllBills.CssClass = "BillsGvShortLnks_Bold"
            LnkViewLastBill.CssClass = "BillsGvShortLnks"

            BillTopGvLnkAllBill.Attributes.Add("class", "BillViewTypeAllDiv")
            BillTopGvLnkLastBill.Attributes.Add("class", "BillViewTypeLastDiv")
        Else
            BillTopGvLnkAllBill.Attributes.Add("class", "BillViewTypeAllDivSel")
            BillTopGvLnkLastBill.Attributes.Add("class", "BillViewTypeLastDivSel")
            LnkViewAllBills.CssClass = "BillsGvShortLnks"
            LnkViewLastBill.CssClass = "BillsGvShortLnks_Bold"
        End If
        
        PagerMain.Visible = False
        PagerLeft.Visible = False
        PagerCenter.Visible = False
        PagerRight.Visible = False
        PagerlnkLastYear.Text = String.Empty
        PagerLnkNextYear.Text = String.Empty
        If dt.Rows.Count > 0 AndAlso ViewType = 0 Then
            Dim LastYear As Integer = dt.Rows(0)("LastYear").ToString
            Dim NextYear As Integer = dt.Rows(0)("NextYear").ToString
            If LastYear <> 0 Then
                PagerMain.Visible = True
                PagerLeft.Visible = True
                PagerlnkLastYear.Text = LastYear
            Else
                PagerCenter.Visible = True
            End If
            If NextYear <> 0 Then
                PagerMain.Visible = True
                PagerRight.Visible = True
                PagerLnkNextYear.Text = NextYear
            End If
            'PagerlnkLastYear
        End If
    End Sub

    Protected Sub GvBills_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvBills.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblIssueDate As Label = DirectCast(e.Row.FindControl("lblIssueDate"), Label)
            If Not IsNothing(lblIssueDate) Then
                Dim dtm As Date = lblIssueDate.Text
                lblIssueDate.Text = String.Concat("<b>", dtm.ToString("MMMM"), "</b>", " ", Year(dtm))
            End If
        End If
    End Sub

    Protected Sub PagerlnkLastYear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PagerlnkLastYear.Click
        Bind()
    End Sub

    Protected Sub PagerLnkNextYear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PagerLnkNextYear.Click
        Bind()
    End Sub

    Protected Sub LnkViewAllBills_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkViewAllBills.Click
        Bind()
    End Sub

    Protected Sub LnkViewLastBill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkViewLastBill.Click
        Bind()
    End Sub
End Class
