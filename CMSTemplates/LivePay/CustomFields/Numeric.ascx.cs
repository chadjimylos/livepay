using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;
using System.Data.SqlClient;

public partial class CMSFormControls_Numeric : LivePayFormEngineUserControl
{
    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.Numeric.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (IsEmpty())
            {
                return "keno";
            }
            return this.Numeric.Text;
        }
        set
        {
            this.Numeric.Text = (string)value;
        }
    }


    /// <summary>
    /// Clears current value
    /// </summary>
    //public void Clear()
    //{
    //    Numeric.Text = "";
    //}


    /// <summary>
    /// Returns true if the number is empty
    /// </summary>
    public bool IsEmpty()
    {
        return (DataHelper.IsEmpty(this.Numeric.Text));
    }


    /// <summary>
    /// Returns true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        if (IsEmpty() && this.Mandatory)
        {
            return false;
        }

        Validator val = new Validator();
        string result = val.IsRegularExp(Numeric.Text, @"\d{" + this.MaxLength +"}", "error").Result;

        if (result != "")
        {
            return false;
        }

        return true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Numeric.MaxLength = this.MaxLength;

        CustomCloud_div.InnerHtml = string.Concat("<a id='CustomCloud_", this.RowCustomField, "' href='#'></a>");

        cv_Numeric.Attributes.Add("ControlValidated", Numeric.ClientID);
        cv_Numeric.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", this.RowCustomField));
        cv_Numeric.Attributes.Add("CustomValidatorErrorMessage", this.ValidationError);
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }
}

