﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RegistrationParticulars.ascx.vb" Inherits="CMSWebParts_AegeanPower_RegistrationParticulars" %>

<style type="text/css">
.AdminUploadTitle{float:left;width:150px;font-weight:bold;font-size:12px}
.AdminUploadCtlr{float:left;padding-right:10px}
.Fleft{float:left;}
.Clear{clear:both}
</style>


<div>
    <div class="AdminUploadTitle">Αρχείο Μετρητών</div>
    <div class="AdminUploadCtlr"><input type="file" id="UpLoadCounterExcel" runat="server"  /></div>
    <div class="Fleft"><asp:Button Width="213px" runat="server" ID="BtnUploadCounter" Text="Upload Αρχείο Μετρητών" /></div>
    <div class="Clear"></div>
    <div id="divActiveVat" runat="server" visible="true" style="padding:10px;width:530px">
        <div style="padding:5px;font-size:14px"><b>New Active Vat Numbers</b></div>
        <div style="font-weight:bold;border-top:1px solid gray;border-right:1px solid gray;border-left:1px solid gray">
            <div class="Fleft" style="padding:3px;width:160px;border-right:1px solid gray;">
                Email
            </div>
            <div class="Fleft" style="padding:3px;width:140px;border-right:1px solid gray;">
               FullName
            </div>
            <div class="Fleft" style="padding:3px;width:80px;border-right:1px solid gray;">
                AFM
            </div>
            
            <div class="Fleft" style="padding:3px;width:100px">
              ProviderNo
            </div>
            <div class="Clear"></div>

        </div>
        <div style="border-top:1px solid gray;"><asp:Repeater runat="server" ID="rptActiveAFM" >
        <ItemTemplate>
            <div class="Fleft" style="padding:3px;border-left:1px solid gray;border-bottom:1px solid gray;width:160px">
            <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
            </div>
            <div class="Fleft" style="padding:3px;border-left:1px solid gray;border-bottom:1px solid gray;;width:140px">
            <asp:Label ID="lblUser" runat="server" Text='<%#Eval("FullName") %>'></asp:Label>
            </div>
            <div class="Fleft" style="padding:3px;border-left:1px solid gray;border-bottom:1px solid gray;width:80px">
            <asp:Label ID="lblAFM" runat="server" Text='<%#Eval("AFM") %>'></asp:Label>
            </div>
            <div class="Fleft" style="padding:3px;border-left:1px solid gray;border-bottom:1px solid gray;width:100px;border-right:1px solid gray;">
            <asp:Label ID="lblProviderNo" runat="server" Text='<%#Eval("ProviderNo") %>'></asp:Label>
            </div>
            
            <div class="Clear"></div>
        </ItemTemplate>
         
        </asp:Repeater></div>
    </div>
    <div id="DivNewVatToSendEmail" runat="server" visible="true" style="padding:10px;width:530px">
        <div style="padding:5px;font-size:14px"><b>Στάλθηκε email στα παρακάτω ΑΦΜ</b></div>
        <div style="font-weight:bold;border-top:1px solid gray;border-right:1px solid gray;border-left:1px solid gray">
            <div class="Fleft" style="padding:3px;width:160px;border-right:1px solid gray;">
                Email
            </div>
            <div class="Fleft" style="padding:3px;width:140px;border-right:1px solid gray;">
               FullName
            </div>
            <div class="Fleft" style="padding:3px;width:80px;border-right:1px solid gray;">
                AFM
            </div>
            
            <div class="Fleft" style="padding:3px;width:100px">
              ProviderNo
            </div>
            <div class="Clear"></div>

        </div>
        <div style="border-top:1px solid gray;"><asp:Repeater runat="server" ID="RptVATMails" >
        <ItemTemplate>
            <div class="Fleft" style="padding:3px;border-left:1px solid gray;border-bottom:1px solid gray;width:160px">
            <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
            </div>
            <div class="Fleft" style="padding:3px;border-left:1px solid gray;border-bottom:1px solid gray;;width:140px">
            <asp:Label ID="lblUser" runat="server" Text='<%#Eval("FullName") %>'></asp:Label>
            </div>
            <div class="Fleft" style="padding:3px;border-left:1px solid gray;border-bottom:1px solid gray;width:80px">
            <asp:Label ID="lblAFM" runat="server" Text='<%#Eval("AFM") %>'></asp:Label>
            </div>
            <div class="Fleft" style="padding:3px;border-left:1px solid gray;border-bottom:1px solid gray;width:100px;border-right:1px solid gray;">
            <asp:Label ID="lblProviderNo" runat="server" Text='<%#Eval("ProviderNo") %>'></asp:Label>
            </div>
            
            <div class="Clear"></div>
        </ItemTemplate>
         
        </asp:Repeater></div>
    </div>
    <div style="border-top:1px solid gray;width:550px;padding-bottom:10px">&nbsp;</div>
    <div class="AdminUploadTitle">Αρχείο Λογαριασμών</div>
    <div class="AdminUploadCtlr"><input type="file" id="UploadAcountExcel" runat="server"  /></div>
    <div class="Fleft"><asp:Button Width="213px" runat="server" ID="BtnUploadAcount" Text="Upload Αρχείο Λογαριασμών"  /></div>
    <div class="Clear"></div>
</div>
<asp:Label ID="Label1" runat="server" ></asp:Label>


<div style="float:left; width:700px;display:none">
<div style="float:left;width:300px;">
<asp:FileUpload ID="BrowseFile" runat="server" />
<asp:Button runat="server" ID="btnUpload" Text="Upload" />
<input type="button" value="Upload" disabled="disabled" style="display:none" id="hiddenupload" />
</div>
<div style="float:left;width:50px; color:Red; margin:5px 0px 0px 10px;">
<asp:Label ID="lblError" runat="server" ></asp:Label>
</div>
</div>

<%--<div style="float:left; width:700px;">
<div style="float:left;width:300px;">
<asp:FileUpload ID="BrowseSecondFile" runat="server" />
<asp:Button runat="server" ID="btnSecondUpload" Text="Upload" />
<input type="button" value="Upload" disabled="disabled" style="display:none" id="hiddenSecondupload" />
</div>
<div style="float:left;width:50px; color:Red; margin:5px 0px 0px 10px;">
<asp:Label ID="lblSecondError" runat="server" ></asp:Label>
</div>
</div>--%>