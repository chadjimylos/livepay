using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.EmailEngine;
using CMS.EventLog;
using CMS.DataEngine;
using CMS.WebAnalytics;
using CMS.LicenseProvider;
using CMS.PortalEngine;
using CMS.SettingsProvider;

public partial class CMSWebParts_AegeanPower_registrationform: CMSAbstractWebPart
{
    #region "Text properties"

    /// <summary>
    /// Gets or sets the Skin ID
    /// </summary>
    public override string SkinID
    {
        get
        {
            return base.SkinID;
        }
        set
        {
            base.SkinID = value;
            SetSkinID(value);
        }
    }


    /// <summary>
    /// Gets or sets the first name text
    /// </summary>
    public string FirstNameText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("FirstNameText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.FirstName$}"));
        }
        set
        {
            this.SetValue("FirstNameText", value);
            lblFirstName.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the last name text
    /// </summary>
    public string LastNameText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("LastNameText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.LastName$}"));
        }
        set
        {
            this.SetValue("LastNameText", value);
            lblLastName.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the e-mail text
    /// </summary>
    public string EmailText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("EmailText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Email$}"));
        }
        set
        {
            this.SetValue("EmailText", value);
            lblEmail.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the password text
    /// </summary>
    public string PasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("PasswordText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Password$}"));
        }
        set
        {
            this.SetValue("PasswordText", value);
            lblPassword.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the confirmation password text
    /// </summary>
    public string ConfirmPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ConfirmPasswordText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.ConfirmPassword$}"));
        }
        set
        {
            this.SetValue("ConfirmPasswordText", value);
            lblConfirmPassword.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the button text
    /// </summary>
    public string ButtonText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ButtonText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Button$}"));
        }
        set
        {
            this.SetValue("ButtonText", value);
            btnOk.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the captcha label text
    /// </summary>
    public string CaptchaText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("CaptchaText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Captcha$}"));
        }
        set
        {
            this.SetValue("CaptchaText", value);
            lblCaptcha.Text = value;
        }
    }

    /// <summary>
    /// Gets or sets the phone label text
    /// </summary>
    //public string PhoneText
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("PhoneText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Phone$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("PhoneText", value);
    //        lblPhone.Text = value;
    //    }
    //}

    /// <summary>
    /// Gets or sets the terms label text
    /// </summary>
    //public string TermsText
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("TermsText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Terms$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("TermsText", value);
    //        lblTerms.Text = value;
    //    }
    //}

    /// <summary>
    /// Gets or sets the terms label text
    /// </summary>
    //public string QuestionText
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("QuestionText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.QuestionText$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("QuestionText", value);
    //        lblQuestion.Text = value;
    //    }
    //}

    /// <summary>
    /// Gets or sets the answer label text
    /// </summary>
    //public string AnswerText
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("AnswerText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.AnswerText$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("AnswerText", value);
    //        lblAnswer.Text = value;
    //    }
    //}

    /// <summary>
    /// Gets or sets the terms area text
    /// </summary>
    //public string TermsAreaText
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("TermsAreaText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.TermsArea$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("TermsAreaText", value);
    //        txtTerms.Text = value;
    //    }
    //}

    /// <summary>
    /// Gets or sets the Accept Terms text
    /// </summary>
    //public string AcceptTermsText
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("AcceptTermsText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.AcceptTermsText$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("AcceptTermsText", value);
    //        chbAcceptTerms.Text = value;
    //    }
    //}

    /// <summary>
    /// Gets or sets the Accept Send News text
    /// </summary>
    //public string AcceptSendNewsText
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("AcceptSendNewsText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.AcceptSendNewsText$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("AcceptSendNewsText", value);
    //        chbAcceptSendNews.Text = value;
    //    }
    //}

    /// <summary>
    /// Gets or sets registration approval page URL
    /// </summary>
    public string ApprovalPage
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ApprovalPage"), "");
        }
        set
        {
            this.SetValue("ApprovalPage", value);
        }
    }

    #endregion


    #region "Registration properties"

    /// <summary>
    /// Gets or sets the value that indicates whether email to user should be sent.
    /// </summary>
    public bool SendWelcomeEmail
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("SendWelcomeEmail"), true);
        }
        set
        {
            this.SetValue("SendWelcomeEmail", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether user is enabled after registration
    /// </summary>
    public bool EnableUserAfterRegistration
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("EnableUserAfterRegistration"), true);
        }
        set
        {
            this.SetValue("EnableUserAfterRegistration", value);
        }
    }


    /// <summary>
    /// Gets or sets the sender email (from)
    /// </summary>
    public string FromAddress
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("FromAddress"), SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSNoreplyEmailAddress"));
        }
        set
        {
            this.SetValue("FromAddress", value);
        }
    }


    /// <summary>
    /// Gets or sets the recepient email (to)
    /// </summary>
    public string ToAddress
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ToAddress"), SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSAdminEmailAddress"));
        }
        set
        {
            this.SetValue("ToAddress", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether after successful registration is 
    /// notification email sent to the administrator 
    /// </summary>
    public bool NotifyAdministrator
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("NotifyAdministrator"), false);
        }
        set
        {
            this.SetValue("NotifyAdministrator", value);
        }
    }


    /// <summary>
    /// Gets or sets the roles where is user assigned after successful registration
    /// </summary>
    public string AssignRoles
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("AssignToRoles"), "");
        }
        set
        {
            this.SetValue("AssignToRoles", value);
        }
    }


    /// <summary>
    /// Gets or sets the sites where is user assigned after successful registration
    /// </summary>
    public string AssignToSites
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("AssignToSites"), "");
        }
        set
        {
            this.SetValue("AssignToSites", value);
        }
    }


    /// <summary>
    /// Gets or sets the message which is displayed after successful registration
    /// </summary>
    public string DisplayMessage
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("DisplayMessage"), "");
        }
        set
        {
            this.SetValue("DisplayMessage", value);
        }
    }


    /// <summary>
    /// Gets or set the url where is user redirected after successful registration
    /// </summary>
    public string RedirectToURL
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("RedirectToURL"), "");
        }
        set
        {
            this.SetValue("RedirectToURL", value);
        }
    }


    /// <summary>
    /// Gets or sets value that indicates whether the captcha image should be displayed
    /// </summary>
    public bool DisplayCaptcha
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("DisplayCaptcha"), false);
        }
        set
        {
            this.SetValue("DisplayCaptcha", value);
            plcCaptcha.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets the default starting alias path for newly registered user
    /// </summary>
    public string StartingAliasPath
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("StartingAliasPath"), "");
        }
        set
        {
            this.SetValue("StartingAliasPath", value);
        }
    }


    /// <summary>
    /// Gets or sets the password minimal length
    /// </summary>
    public int PasswordMinLength
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("PasswordMinLength"), 0);
        }
        set
        {
            this.SetValue("PasswordMinLength", 0);
        }
    }

    #endregion


    #region "Conversion properties"

    /// <summary>
    /// Gets or sets the conversion track name used after successful registration.
    /// </summary>
    public string TrackConversionName
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("TrackConversionName"), "");
        }
        set
        {
            if (value.Length > 400)
            {
                value = value.Substring(0, 400);
            }
            this.SetValue("TrackConversionName", value);
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
            rfvFirstName.Enabled = false;
            rfvEmail.Enabled = false;
            rfvPassword.Enabled = false;
            rfvConfirmPassword.Enabled = false;
            rfvLastName.Enabled = false;
        }
        else
        {
            // Set texts
            lblFirstName.Text = this.FirstNameText;
            lblLastName.Text = this.LastNameText;
            lblEmail.Text = this.EmailText;
            lblPassword.Text = this.PasswordText;
            lblConfirmPassword.Text = this.ConfirmPasswordText;
            btnOk.Text = this.ButtonText;
            lblCaptcha.Text = this.CaptchaText;
            //lblPhone.Text = this.PhoneText;
            //lblTerms.Text = this.TermsText;
            //lblQuestion.Text = this.QuestionText;
            //lblAnswer.Text = this.AnswerText;
            //txtTerms.Text = this.TermsAreaText;
            //chbAcceptTerms.Text = this.AcceptTermsText;
            //chbAcceptSendNews.Text = this.AcceptSendNewsText;

            ////Set DropDownList Question
            //string systemTableClassName = "livepay.SecurityQuestion";

            //// Get data class using system table name
            //DataClassInfo systemTableClassInfo = DataClassInfoProvider.GetDataClass(systemTableClassName);
            //if (systemTableClassInfo == null)
            //{
            //    throw new Exception("Given custom table does not exist.");
            //}
            //// Initialize custom table item provider with current user info and general connection
            //CustomTableItemProvider ctiProvider = new CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection());

            //// Get custom table items
            //DataSet dsItems = ctiProvider.GetItems(systemTableClassInfo.ClassName, null, null);

            //ddlQuestion.Items.Clear();
            //ddlQuestion.Items.Add(new ListItem("---------", "0"));

            ////Lang
            //string lang = CMSContext.CurrentDocument.DocumentCulture.Split('-')[0].ToString();

            //// Check if DataSet is not empty
            //if (!DataHelper.DataSourceIsEmpty(dsItems))
            //{
            //    for (int i = 0; i <= dsItems.Tables[0].Rows.Count - 1; i++) {
            //        ddlQuestion.Items.Add(new ListItem(dsItems.Tables[0].Rows[i]["SecurityQuestionText" + lang].ToString(), dsItems.Tables[0].Rows[i]["itemID"].ToString()));
            //        //ddlQuestion.Items.Add(new ListItem("SecurityQuestionText" + lang, "SecurityQuestionText" + lang));
            //    }
            //}

            // Set required field validators texts
            rfvFirstName.ErrorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.rfvFirstName");
            rfvLastName.ErrorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.rfvLastName");
            rfvEmail.ErrorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.rfvEmail");
            rfvPassword.ErrorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.rfvPassword");
            rfvConfirmPassword.ErrorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.rfvConfirmPassword");
            revEmail.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.revEmail");

            // Set SkinID
            if (!this.StandAlone && (this.PageCycle < PageCycleEnum.Initialized))
            {
                SetSkinID(this.SkinID);
            }

            plcCaptcha.Visible = this.DisplayCaptcha;
        }
    }


    /// <summary>
    /// Sets SkinID
    /// </summary>
    void SetSkinID(string skinId)
    {
        if (skinId != "")
        {
            lblFirstName.SkinID = skinId;
            lblLastName.SkinID = skinId;
            lblEmail.SkinID = skinId;
            lblPassword.SkinID = skinId;
            lblConfirmPassword.SkinID = skinId;
            txtFirstName.SkinID = skinId;
            txtLastName.SkinID = skinId;
            txtEmail.SkinID = skinId;
            txtPassword.SkinID = skinId;
            txtConfirmPassword.SkinID = skinId;
            //txtPhone.SkinID = skinId;
            //txtAnswer.SkinID = skinId;
            btnOk.SkinID = skinId;
        }
    }


    /// <summary>
    /// OK click handler (Proceed registration)
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if ((this.PageManager.ViewMode == ViewModeEnum.Design) || (this.HideOnCurrentPage) || (!this.IsVisible))
        {
            // Do not process
        }
        else
        {
            #region "Banned IPs"

            // Ban IP addresses which are blocked for registration
            if (!BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.Registration))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("banip.ipisbannedregistration");
                return;
            }

            #endregion


            #region "Check Email & password"

            // Check whether user with same email does not exist 
            UserInfo ui = UserInfoProvider.GetUserInfo(txtEmail.Text);
            if (ui != null)
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.UserAlreadyExists").Replace("%%name%%", HTMLHelper.HTMLEncode(txtEmail.Text));
                return;
            }

            // Check whether password is same
            if (txtPassword.Text != txtConfirmPassword.Text)
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.PassworDoNotMatch");
                return;
            }

            if ((this.PasswordMinLength > 0) && (txtPassword.Text.Length < this.PasswordMinLength))
            {
                lblError.Visible = true;
                lblError.Text = String.Format(ResHelper.GetString("Webparts_Membership_RegistrationForm.PasswordMinLength"), this.PasswordMinLength.ToString());
                return;
            }

            if (!ValidationHelper.IsEmail(txtEmail.Text.ToLower()))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.EmailIsNotValid");
                return;
            }

            #endregion


            #region "Captcha"

            //// OLD - Check if captcha is required
            //if (this.DisplayCaptcha)
            //{
            //    // OLD - Verifiy captcha text
            //    if (!scCaptcha.IsValid())
            //    {
            //        // OLD - Display error message if catcha text is not valid
            //        lblError.Visible = true;
            //        lblError.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.captchaError");
            //        return;
            //    }
            //    else
            //    {
            //        // OLD - Generate new code and clear captcha textbox if cpatcha code is valid
            //        scCaptcha.GenerateNewCode();
            //    }
            //}

            //  Check if captcha is required
            if (this.DisplayCaptcha)
            {
                // Verifiy captcha text

                scCaptcha.ValidateCaptcha(txtCaptcha.Text.ToUpper());
                if (!scCaptcha.UserValidated)
                {
                    // Display error message if catcha text is not valid
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.captchaError");
                    return;
                }
                else
                {
                    // Generate new code and clear captcha textbox if cpatcha code is valid
                    //scCaptcha.GenerateNewCode();
                }
            }




            #endregion


            #region "User properties"

            ui = new UserInfo();
            ui.PreferredCultureCode = "";
            ui.Email = txtEmail.Text.Trim();
            ui.FirstName = txtFirstName.Text.Trim();
            ui.FullName = txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim();
            ui.LastName = txtLastName.Text.Trim();
            ui.MiddleName = "";
            ui.UserName = txtEmail.Text.Trim();
            //ui.SetValue("UserPhone", txtPhone.Text.Trim());
            //ui.SetValue("UserQuestions", ddlQuestion.SelectedValue.ToString());
            //ui.SetValue("UserAnswers", txtAnswer.Text);
            //ui.SetValue("UserTerms", chbAcceptSendNews.Text);            

            ui.Enabled = this.EnableUserAfterRegistration;
            ui.IsEditor = false;
            ui.IsGlobalAdministrator = false;
            ui.UserURLReferrer = CMSContext.CurrentUser.URLReferrer;
            ui.UserCampaign = CMSContext.CurrentUser.Campaign;

            ui.UserSettings.UserRegistrationInfo.IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            ui.UserSettings.UserRegistrationInfo.Agent = HttpContext.Current.Request.UserAgent;

            // Check whether confirmation is required
            bool requiresConfirmation = SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSRegistrationEmailConfirmation");
            bool requiresAdminApprove = true;

            if (!requiresConfirmation)
            {
                // If confirmation is not required check whether administration approval is reqiures
                if ((requiresAdminApprove = SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSRegistrationAdministratorApproval")))
                {
                    ui.Enabled = false;
                    ui.UserSettings.UserWaitingForApproval = true;
                }
            }
            else
            {
                // EnableUserAfterRegistration is overrided by requiresConfirmation - user needs to be confirmed before enable
                ui.Enabled = false;
            }

            // Set user's starting alias path
            if (!String.IsNullOrEmpty(this.StartingAliasPath))
            {
                ui.UserStartingAliasPath = this.StartingAliasPath;
            }

            #endregion


            #region "Reserved names"

            // Check for reserved user names like administrator, sysadmin, ...
            if (UserInfoProvider.NameIsReserved(CMSContext.CurrentSiteName, ui.UserName))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.UserNameReserved").Replace("%%name%%", HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(ui.UserName, true)));
                return;
            }

            if (UserInfoProvider.NameIsReserved(CMSContext.CurrentSiteName, ui.UserNickName))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.UserNameReserved").Replace("%%name%%", HTMLHelper.HTMLEncode(ui.UserNickName));
                return;
            }

            #endregion


            #region "License limitations"

            // Check limitations for Global administrator
            if (ui.IsGlobalAdministrator)
            {
                if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.GlobalAdmininistrators, VersionActionEnum.Insert, false))
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("License.MaxItemsReachedGlobal");
                    return;
                }
            }

            // Check limitations for editors
            if (ui.IsEditor)
            {
                if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Editors, VersionActionEnum.Insert, false))
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("License.MaxItemsReachedEditor");
                    return;
                }
            }

            // Check limitations for site members
            if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.SiteMembers, VersionActionEnum.Insert, false))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("License.MaxItemsReachedSiteMember");
                return;
            }

            #endregion


            // Check whether email is unique if it is required
            string checkSites = (String.IsNullOrEmpty(this.AssignToSites)) ? CMSContext.CurrentSiteName : this.AssignToSites;
            if (!UserInfoProvider.IsEmailUnique(txtEmail.Text.Trim(), checkSites, 0))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("UserInfo.EmailAlreadyExist");
                return;
            }

            // Store to DB
            UserInfoProvider.SetUserInfo(ui);

            // Set password
            UserInfoProvider.SetPassword(ui.UserName, txtPassword.Text);


            #region "Welcome Emails (confirmation, waiting for approval)"

            bool error = false;
            EventLogProvider ev = new EventLogProvider();
            EmailTemplateInfo template = null;

            string emailSubject = null;
            // Send welcome message with username and password, with confirmation link, user must confirm registration
            if (requiresConfirmation)
            {
                //template = EmailTemplateProvider.GetEmailTemplate("RegistrationConfirmation", CMSContext.CurrentSiteName);
                //emailSubject = EmailHelper.GetSubject(template, ResHelper.GetString("RegistrationForm.RegistrationConfirmationEmailSubject"));

                
                MailContext RegMail = new MailContext();
                RegMail.Username = txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim(); ;
                RegMail.RegistrationLink = UrlHelper.GetAbsoluteUrl("~/���������/UserRegistration/Account�ctivation.aspx") + "?userguid=" + ui.UserGUID;
                RegMail.UsernameGreeklish = "username greeklish";
                RegMail.SendEmail("USERREG", this.ToAddress, txtEmail.Text.Trim());
               

            }
            // Send welcome message with username and password, with information that user must be approved by administrator
            else if (this.SendWelcomeEmail)
            {
                if (requiresAdminApprove)
                {
                    template = EmailTemplateProvider.GetEmailTemplate("Membership.RegistrationWaitingForApproval", CMSContext.CurrentSiteName);
                    emailSubject = EmailHelper.GetSubject(template, ResHelper.GetString("RegistrationForm.RegistrationWaitingForApprovalSubject"));
                }
                // Send welcome message with username and password, user can logon directly
                else
                {
                    template = EmailTemplateProvider.GetEmailTemplate("Membership.Registration", CMSContext.CurrentSiteName);
                    emailSubject = EmailHelper.GetSubject(template, ResHelper.GetString("RegistrationForm.RegistrationSubject"));
                }
            }

            if (template != null)
            {
                // Prepare macro replacements
                string[,] replacements = new string[3, 2];
                replacements[0, 0] = "confirmaddress";
                replacements[0, 1] = (this.ApprovalPage != String.Empty) ? UrlHelper.GetAbsoluteUrl(this.ApprovalPage) + "?userguid=" + ui.UserGUID : UrlHelper.GetAbsoluteUrl("~/CMSPages/Dialogs/UserRegistration.aspx") + "?userguid=" + ui.UserGUID;
                replacements[1, 0] = "username";
                replacements[1, 1] = ui.UserName;
                replacements[2, 0] = "password";
                replacements[2, 1] = txtPassword.Text;

                // Set resolver
                ContextResolver resolver = CMSContext.CurrentResolver;
                resolver.SourceParameters = replacements;
                resolver.EncodeResolvedValues = true;

                // Email message
                EmailMessage email = new EmailMessage();
                email.EmailFormat = EmailFormatEnum.Default;
                email.Recipients = ui.Email;
                
                email.From = EmailHelper.GetSender(template, SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSNoreplyEmailAddress"));
                email.Body = resolver.ResolveMacros(template.TemplateText);
                
                resolver.EncodeResolvedValues = false;
                email.PlainTextBody = resolver.ResolveMacros(template.TemplatePlainText);
                email.Subject = resolver.ResolveMacros(emailSubject);
                               
                email.CcRecipients = template.TemplateCc;
                email.BccRecipients = template.TemplateBcc;

                try
                {
                    MetaFileInfoProvider.ResolveMetaFileImages(email, template.TemplateID, EmailObjectType.EMAILTEMPLATE, MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE);
                    // Send the e-mail immediately
                    EmailSender.SendEmail(CMSContext.CurrentSiteName, email, true);
                }
                catch (Exception ex)
                {
                    ev.LogEvent("E", "RegistrationForm - SendEmail", ex);
                    error = true;
                }
            }

            // If there was some error, user must be deleted
            if (error)
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("RegistrationForm.UserWasNotCreated");

                // Email was not send, user can't be approved - delete it
                UserInfoProvider.DeleteUser(ui);
                return;
            }

            #endregion


            #region "Administrator notification email"

            // Notify administrator if enabled and e-mail confirmation is not required
            if (!requiresConfirmation && this.NotifyAdministrator && (this.FromAddress != String.Empty) && (this.ToAddress != String.Empty))
            {
                EmailTemplateInfo mEmailTemplate = null;

                if (requiresAdminApprove)
                {
                    mEmailTemplate = EmailTemplateProvider.GetEmailTemplate("Registration.Approve", CMSContext.CurrentSiteName);
                }
                else
                {
                    mEmailTemplate = EmailTemplateProvider.GetEmailTemplate("Registration.New", CMSContext.CurrentSiteName);
                }

                if (mEmailTemplate == null)
                {
                    // Log missing e-mail template
                    ev.LogEvent("E", DateTime.Now, "RegistrationForm", "GetEmailTemplate", HTTPHelper.GetAbsoluteUri());
                }
                else
                {
                    string[,] replacements = new string[4, 2];
                    replacements[0, 0] = "firstname";
                    replacements[0, 1] = ui.FirstName;
                    replacements[1, 0] = "lastname";
                    replacements[1, 1] = ui.LastName;
                    replacements[2, 0] = "email";
                    replacements[2, 1] = ui.Email;
                    replacements[3, 0] = "username";
                    replacements[3, 1] = ui.UserName;

                    ContextResolver resolver = CMSContext.CurrentResolver;
                    resolver.SourceParameters = replacements;
                    resolver.EncodeResolvedValues = true;

                    EmailMessage message = new EmailMessage();

                    message.EmailFormat = EmailFormatEnum.Default;
                    message.From = EmailHelper.GetSender(mEmailTemplate, this.FromAddress);
                    message.Recipients = this.ToAddress;
                    message.Body = resolver.ResolveMacros(mEmailTemplate.TemplateText);

                    resolver.EncodeResolvedValues = false;
                    message.PlainTextBody = resolver.ResolveMacros(mEmailTemplate.TemplatePlainText);
                    message.Subject = resolver.ResolveMacros(EmailHelper.GetSubject(mEmailTemplate, ResHelper.GetString("RegistrationForm.EmailSubject")));

                    message.CcRecipients = mEmailTemplate.TemplateCc;
                    message.BccRecipients = mEmailTemplate.TemplateBcc;

                    try
                    {
                        // Attach template meta-files to e-mail
                        MetaFileInfoProvider.ResolveMetaFileImages(message, mEmailTemplate.TemplateID, EmailObjectType.EMAILTEMPLATE, MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE);
                        EmailSender.SendEmail(CMSContext.CurrentSiteName, message);
                    }
                    catch
                    {
                        ev.LogEvent("E", DateTime.Now, "Membership", "RegistrationEmail", CMSContext.CurrentSite.SiteID);
                    }
                }
            }

            #endregion


            #region "Web analytics"

            // Track successful registration conversion
            if (this.TrackConversionName != String.Empty)
            {
                string siteName = CMSContext.CurrentSiteName;

                if (AnalyticsHelper.AnalyticsEnabled(siteName) && AnalyticsHelper.TrackConversionsEnabled(siteName) && !AnalyticsHelper.IsIPExcluded(siteName, HttpContext.Current.Request.UserHostAddress))
                {
                    string objectName = new ContextResolver().ResolveMacros(this.TrackConversionName);
                    HitLogProvider.LogHit(HitLogProvider.CONVERSIONS, siteName, CMSContext.PreferredCultureCode, objectName, 0);
                }
            }

            // Log registered user if confirmation is not required
            if (!requiresConfirmation)
            {
                AnalyticsHelper.LogRegisteredUser(CMSContext.CurrentSiteName, ui);
            }

            #endregion


            #region "Roles & authentication"

            string[] roleList = this.AssignRoles.Split(';');           
            string[] siteList;

            // If AssignToSites field set
            if (!String.IsNullOrEmpty(this.AssignToSites))
            {
                siteList = this.AssignToSites.Split(';');
            }            
            else // If not set user current site 
            {
                siteList = new string[] { CMSContext.CurrentSiteName };
            }

            foreach (string siteName in siteList)
            {               
                // Add new user to the current site
                UserInfoProvider.AddUserToSite(ui.UserName, siteName);
                foreach (string roleName in roleList)
                {
                    if (!String.IsNullOrEmpty(roleName))
                    {
                        // Add user to desired roles
                        if (RoleInfoProvider.RoleExists(roleName, siteName))
                        {
                            UserInfoProvider.AddUserToRole(ui.UserName, roleName, siteName);
                        }
                    }
                }
            }

            if (this.DisplayMessage.Trim() != String.Empty)
            {
                pnlForm.Visible = false;
                lblText.Visible = true;
                lblText.Text = this.DisplayMessage;
            }
            else
            {
                if (ui.Enabled)
                {
                    FormsAuthentication.SetAuthCookie(ui.UserName, true);
                    CMSContext.SetCurrentUser(new CurrentUserInfo(ui, true));
                    UserInfoProvider.SetPreferredCultures(ui);
                }

                if (this.RedirectToURL != String.Empty)
                {
                    UrlHelper.Redirect(this.RedirectToURL);
                }

                else if (QueryHelper.GetString("ReturnURL", "") != String.Empty)
                {
                    // redirect, encode param for sure
                    UrlHelper.Redirect(QueryHelper.GetText("ReturnURL", ""));
                }
            }

            #endregion

            lblError.Visible = false;
        }
    }

    #endregion
}
