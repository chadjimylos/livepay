<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvoiceTemplate_Edit.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_InvoiceTemplate_InvoiceTemplate_Edit"
    Theme="Default" ValidateRequest="false" EnableEventValidation="false" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="E-commerce Configuration - Invoice template" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>

<%@ Register Src="~/CMSAdminControls/MetaFiles/FileList.ascx" TagName="FileList" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/Selectors/MacroSelector.ascx" TagName="MacroSelector" TagPrefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:ScriptManager ID="manScript" runat="server" />
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
        Visible="false"></asp:Label>
    <cms:CMSHtmlEditor ID="htmlInvoiceTemplate" runat="server" Width="770px" Height="400px" />
    <br />
    <cms:MacroSelector ID="macroSelectorElm" runat="server" JavaScripFunction="InsertMacro" />
    <br />
    <a href="#" onclick="document.getElementById('moreMacros').style.display = (document.getElementById('moreMacros').style.display == 'none')? 'block' : 'none';">
        <asp:Label ID="lnkMoreMacros" runat="server" /><br />
        <br />
    </a>
    <div id="moreMacros" style="display: none;">
        <strong>
            <asp:Label ID="lblMoreInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" /></strong>
        <asp:Table ID="tblMore" runat="server" EnableViewState="false" GridLines="Horizontal"
            CellPadding="3" CellSpacing="0" Width="100%">
        </asp:Table>
        <br />
        <strong>
            <asp:Label ID="lblHelpHeader" runat="server" CssClass="InfoLabel" EnableViewState="false" /></strong>
        <asp:Table ID="tblHelp" runat="server" EnableViewState="false" GridLines="Horizontal"
            CellPadding="3" CellSpacing="0" Width="100%">
        </asp:Table>
    </div>
    <br />
    <br />
    <cms:PageTitle ID="AttachmentTitle" runat="server" TitleCssClass="SubTitleHeader"
        EnableViewState="false" />
    <br />
    <cms:FileList ID="AttachmentList" runat="server" />
</asp:Content>
