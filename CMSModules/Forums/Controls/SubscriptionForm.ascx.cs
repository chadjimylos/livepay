using System;
using System.Web;

using CMS.Forums;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;

public partial class CMSModules_Forums_Controls_SubscriptionForm : ForumViewer
{
    protected void Page_Load(object sender, EventArgs e)
    {
        #region "Resources"

        // Set string values
        rfvEmail.ErrorMessage = ResHelper.GetString("Forums_WebInterface_ForumNewPost.emailErrorMsg");

        // Regular expression to validate email (e-mail is not required)
        rfvEmail.ValidationExpression = @"^([\w0-9_-]+(\.[\w0-9_-]+)*@[\w0-9_-]+(\.[\w0-9_-]+)+)*$";
        rfvEmailRequired.ErrorMessage = ResHelper.GetString("Forums_WebInterface_ForumNewPost.emailRequireErrorMsg");

        btnOk.Text = ResHelper.GetString("General.Ok");
        btnCancel.Text = ResHelper.GetString("General.Cancel");

        #endregion

        // Pre-fill user email
        if (!RequestHelper.IsPostBack())
        {
            txtEmail.Text = CMSContext.CurrentUser.Email;
        }
    }


    /// <summary>
    /// OK click handler
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check banned IP
        if (!BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.AllNonComplete))
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("General.BannedIP");
            return;
        }

        // Check input fields
        string result = new Validator().NotEmpty(txtEmail.Text, rfvEmailRequired.ErrorMessage).IsRegularExp(txtEmail.Text, @"^([\w0-9_-]+(\.[\w0-9_-]+)*@[\w0-9_-]+(\.[\w0-9_-]+)+)*$", rfvEmail.ErrorMessage).Result;

        if (result == "")
        {
            // For selected forum and only if subscribtion is enabled
            if ((ForumContext.CurrentForum != null) && ((ForumContext.CurrentState == ForumStateEnum.SubscribeToPost) || (ForumContext.CurrentState == ForumStateEnum.NewSubscription)))
            {
                // Check permissions
                if (!this.IsAvailable(ForumContext.CurrentForum.DataClass.DataRow, ForumActionType.SubscribeToForum))
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("ForumNewPost.PermissionDenied");
                    return;
                }

                
                // Create new subscribtion
                ForumSubscriptionInfo fsi = new ForumSubscriptionInfo();
                fsi.SubscriptionForumID = ForumContext.CurrentForum.ForumID;
                fsi.SubscriptionEmail = HTMLHelper.HTMLEncode(txtEmail.Text.Trim());
                fsi.SubscriptionGUID = Guid.NewGuid();

                if (ForumContext.CurrentSubscribeThread != null)
                {
                    fsi.SubscriptionPostID = ForumContext.CurrentSubscribeThread.PostId;
                }

                if (CMSContext.CurrentUser != null)
                {
                    fsi.SubscriptionUserID = CMSContext.CurrentUser.UserID;
                }

                // Check whethger user is not subscribed
                if (ForumSubscriptionInfoProvider.IsSubscribed(txtEmail.Text.Trim(), fsi.SubscriptionForumID, fsi.SubscriptionPostID))
                {
                    lblError.Text = ResHelper.GetString("ForumSubscibe.SubscriptionExists");
                    lblError.Visible = true;
                    return;
                }
                
                ForumSubscriptionInfoProvider.SetForumSubscriptionInfo(fsi);
            }
        }

        UrlHelper.Redirect(ClearURL());
    }


    /// <summary>
    /// Cancel click nahdler
    /// </summary>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        UrlHelper.Redirect(ClearURL());
    }
}
