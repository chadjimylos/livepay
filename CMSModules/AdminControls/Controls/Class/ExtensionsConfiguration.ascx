<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExtensionsConfiguration.ascx.cs"
    Inherits="CMSModules_AdminControls_Controls_Class_ExtensionsConfiguration" %>
<table>
    <tr>
        <td>
            <cms:LocalizedCheckBox ID="chkInehrit" runat="server" CssClass="CheckBoxMovedLeft"
                ResourceString="attach.inheritfromsettings" Checked="true" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="txtAllowedExtensions" runat="server" CssClass="TextBoxField" /><br />
            <em>
                <cms:LocalizedLabel ID="lblExtExample" runat="server" ResourceString="attach.extensionexample" /></em>
        </td>
    </tr>
</table>
