CREATE TABLE [Forums_ForumSubscription] (
		[SubscriptionID]               int NOT NULL IDENTITY(1, 1),
		[SubscriptionUserID]           int NULL,
		[SubscriptionEmail]            nvarchar(100) NULL,
		[SubscriptionForumID]          int NOT NULL,
		[SubscriptionPostID]           int NULL,
		[SubscriptionGUID]             uniqueidentifier NOT NULL,
		[SubscriptionLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Forums_ForumSubscription]
	ADD
	CONSTRAINT [PK_Forums_ForumSubscription]
	PRIMARY KEY
	NONCLUSTERED
	([SubscriptionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Forums_ForumSubscription]') IS NOT NULL
	AND OBJECT_ID(N'[Forums_Forum]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumSubscription_SubscriptionForumID_Forums_Forum]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumSubscription]
			ADD CONSTRAINT [FK_Forums_ForumSubscription_SubscriptionForumID_Forums_Forum]
			FOREIGN KEY ([SubscriptionForumID]) REFERENCES [Forums_Forum] ([ForumID])
END
IF OBJECT_ID(N'[Forums_ForumSubscription]') IS NOT NULL
	AND OBJECT_ID(N'[Forums_ForumPost]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumSubscription_SubscriptionPostID_Forums_ForumPost]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumSubscription]
			ADD CONSTRAINT [FK_Forums_ForumSubscription_SubscriptionPostID_Forums_ForumPost]
			FOREIGN KEY ([SubscriptionPostID]) REFERENCES [Forums_ForumPost] ([PostId])
END
IF OBJECT_ID(N'[Forums_ForumSubscription]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumSubscription_SubscriptionUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumSubscription]
			ADD CONSTRAINT [FK_Forums_ForumSubscription_SubscriptionUserID_CMS_User]
			FOREIGN KEY ([SubscriptionUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE CLUSTERED INDEX [IX_Forums_ForumSubscription_SubscriptionForumID_SubscriptionEmail]
	ON [Forums_ForumSubscription] ([SubscriptionEmail], [SubscriptionForumID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Forums_ForumSubscription_SubscriptionPostID]
	ON [Forums_ForumSubscription] ([SubscriptionPostID])
	ON [PRIMARY]
CREATE INDEX [IX_Forums_ForumSubscription_SubscriptionUserID]
	ON [Forums_ForumSubscription] ([SubscriptionUserID])
	ON [PRIMARY]
