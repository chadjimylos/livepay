using System;
using System.Data;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;

using CMS.Reporting;
using CMS.ExtendedControls;
using CMS.CMSHelper;
using CMS.FormEngine;
using CMS.FormControls;
using CMS.GlobalHelper;

public partial class CMSModules_Reporting_Controls_DisplayReport : AbstractReportControl
{
    #region "Variables"

    private string mReportName = "";
    private string mReportHTML = "";
    private bool mDisplayFilter = true;          // DisplayFilter property
    private bool mDisplayFilterResult = true;    // internal display filter based on DisplayFilter and visible elements
    private bool mSaveMode = false;
    private bool mLoadFormParameters = true;
    private ReportInfo mReportInfo = null;
    private int mSavedReportId = 0;
    ArrayList mReportControls = null;
    private bool wasInit = false;
    private bool wasPreRender = false;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Report name
    /// </summary>
    public ReportInfo ReportInfo
    {
        get
        {
            if (mReportInfo == null)
            {
                mReportInfo = ReportInfoProvider.GetReportInfo(this.ReportName);
            }
            return mReportInfo;
        }
        set
        {
            mReportInfo = value;
        }
    }


    /// <summary>
    /// Content panel
    /// </summary>
    public Panel ReportPanel
    {
        get
        {
            return pnlContent;
        }
    }


    /// <summary>
    /// BasicForm object
    /// </summary>
    public BasicForm ParametersForm
    {
        get
        {
            return formElem;
        }
    }


    /// <summary>
    /// Report name
    /// </summary>
    public string ReportName
    {
        get
        {
            return mReportName;
        }
        set
        {
            mReportName = value;
        }
    }


    /// <summary>
    /// ReportHTML
    /// </summary>
    public string ReportHTML
    {
        get
        {
            return mReportHTML;
        }
        set
        {
            mReportHTML = value;
        }
    }


    /// <summary>
    /// Display filter
    /// </summary>
    public bool DisplayFilter
    {
        get
        {
            return mDisplayFilter;
        }
        set
        {
            mDisplayFilter = value;
        }
    }


    /// <summary>
    /// Body CSS class
    /// </summary>
    public string BodyCssClass
    {
        get
        {
            return this.pnlContent.CssClass;
        }
        set
        {
            this.pnlContent.CssClass = value;
        }
    }


    /// <summary>
    /// Form CSS class
    /// </summary>
    public string FormCssClass
    {
        get
        {
            return this.formElem.CssClass;
        }
        set
        {
            this.formElem.CssClass = value;
        }
    }


    /// <summary>
    /// Assigned if parameters will be loaded automaticcaly
    /// </summary>
    public bool LoadFormParameters
    {
        get
        {
            return mLoadFormParameters;
        }
        set
        {
            mLoadFormParameters = value;
        }
    }


    /// <summary>
    /// Load always default values
    /// </summary>
    public bool ForceLoadDefaultValues
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["ForceLoadDefaultValues"], false);
        }
        set
        {
            ViewState["ForceLoadDefaultValues"] = value;
        }
    }


    /// <summary>
    /// Ingnore was init
    /// </summary>
    public bool IngnoreWasInit
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["IngnoreWasInit"], false);
        }
        set
        {
            ViewState["IngnoreWasInit"] = value;
        }
    }


    /// <summary>
    /// Child report controls
    /// </summary>
    public ArrayList ReportControls
    {
        get
        {
            if (mReportControls == null)
            {
                mReportControls = new ArrayList();

                // Collect the controls
                foreach (Control c in pnlContent.Controls)
                {
                    if (c is AbstractReportControl)
                    {
                        mReportControls.Add(c);
                    }
                }
            }

            return mReportControls;
        }
    }

    #endregion


    /// <summary>
    /// Load parameters from data form
    /// </summary>
    protected void LoadParameters()
    {
        if (this.ReportInfo != null)
        {
            // Set the form
            FormInfo fi = new FormInfo();
            fi.LoadXmlDefinition(this.ReportInfo.ReportParameters);
            // Get datarow with required columns
            DataRow dr = fi.GetDataRow();
            if ((!RequestHelper.IsPostBack()) || (ForceLoadDefaultValues))
            {
                fi.LoadDefaultValues(dr);
            }
            else
            {
                if ((!ForceLoadDefaultValues) && (formElem.DataRow != null))
                {
                    dr = formElem.DataRow;
                }
            }

            // show filter - based on DisplayFilter and number of visible elements
            mDisplayFilterResult = this.DisplayFilter && (fi.GetFormElements(true, false).Count > 0);

            formElem.DataRow = dr;
            formElem.SubmitButton.Visible = mDisplayFilterResult;

            if (RenderCssClasses)
            {
                formElem.SubmitButton.CssClass = "SubmitButton";
            }

            formElem.FormInformation = fi;

            formElem.SubmitButton.Text = ResHelper.GetString("Report_View.btnUpdate");
            formElem.SiteName = CMSContext.CurrentSiteName;
            formElem.ShowPrivateFields = true;
            formElem.Mode = FormModeEnum.Insert;
            formElem.Visible = mDisplayFilterResult;
        }
    }


    /// <summary>
    /// OnInit
    /// </summary>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // Load parameters from data form
        LoadParameters();

        // Set wasInit value
        wasInit = true;

        // Load data
        if (!RequestHelper.IsPostBack())
        {
            ReloadData(false);
        }

        // Get handler to OnAfter save
        formElem.OnAfterSave += new BasicForm.OnAfterSaveEventHandler(formElem_OnAfterSave);
    }


    /// <summary>
    /// OnPreRender override
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        wasPreRender = true;

        if (RequestHelper.IsPostBack())
        {
            try
            {                
                formElem.SaveData(null);
            }
            catch
            {
            }
        }

        // Disable CSS class for filter if filter isn't visible
        if ((this.mDisplayFilterResult == false) || (this.formElem == null) ||
            (this.ReportInfo == null) || (this.ReportInfo.ReportParameters == "") ||
            (this.ReportInfo.ReportParameters.ToLower() == "<form></form>"))
        {
            formElem.CssClass = "";
        }

        base.OnPreRender(e);
    }


    /// <summary>
    /// OnAfterSave handler
    /// </summary>
    void formElem_OnAfterSave()
    {
        // Only wan't to react to SaveData()
        if (wasPreRender)
        {
            bool tmpForceLoad = this.ForceLoadDefaultValues;
            this.ForceLoadDefaultValues = false;
            ReloadData(false);
            this.ForceLoadDefaultValues = tmpForceLoad;
        }
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData(bool forceLoad)
    {
        this.pnlContent.Controls.Clear();
        this.mReportControls = null;

        // Repurt info must exists
        if (this.ReportInfo != null)
        {
            // Check permissions for report
            if (this.ReportInfo.ReportAccess != ReportAccessEnum.All)
            {
                if (!CMSContext.CurrentUser.IsAuthenticated())
                {
                    this.Visible = false;
                    return;
                }
            }

            // If ReloadData is called before Init, load parameters
            if (!wasInit || IngnoreWasInit)
            {
                LoadParameters();
            }

            // Set parameters only if it is allowed
            if (mLoadFormParameters)
            {
                this.ReportParameters = formElem.DataRow;
            }

            // Clear resolver because it can contains old parameters values
            ClearResolver();

            // Build report HTML
            mReportHTML = this.ReportInfo.ReportLayout;
            mReportHTML = ResolveMacros(mReportHTML);
            mReportHTML = HTMLHelper.ResolveUrls(mReportHTML, null);

            // Add the content
            this.pnlContent.Controls.Clear();
            this.pnlContent.Controls.Add(new LiteralControl(mReportHTML));

            ControlsHelper.ResolveDynamicControls(this.pnlContent);

            // Init the child controls
            foreach (AbstractReportControl ctrl in this.ReportControls)
            {
                ctrl.RenderCssClasses = this.RenderCssClasses;
                ctrl.ReportParameters = this.ReportParameters;
                ctrl.ContextParameters = this.ContextParameters;
                ctrl.AllParameters = this.AllParameters;
                ctrl.SavedReportGuid = Guid.Empty;

                // In save mode must be defined new Guid for graph image and saved reeport id
                if (mSaveMode)
                {
                    ctrl.SavedReportGuid = Guid.NewGuid();
                    ctrl.SavedReportID = mSavedReportId;
                }
                ctrl.ReloadData(forceLoad);
            }

            // Display/hide the filtering form
            formElem.Visible = this.mDisplayFilterResult;
        }
    }


    /// <summary>
    /// Saves the report
    /// </summary>
    /// <returns>Returns the saved report ID or 0 if some error was occurred or don't have permissions to this report</returns>
    public int SaveReport()
    {
        // Report info must exists
        if (this.ReportInfo != null)
        {
            // Check permissions for report
            if (this.ReportInfo.ReportAccess != ReportAccessEnum.All)
            {
                if (!CMSContext.CurrentUser.IsAuthenticated())
                {
                    this.Visible = false;
                    return 0;
                }
            }
            try
            {
                // Save saved report info object
                SavedReportInfo sri = new SavedReportInfo();
                sri.SavedReportDate = DateTime.Now;
                sri.SavedReportGUID = Guid.NewGuid();
                sri.SavedReportHTML = "";
                sri.SavedReportReportID = this.ReportInfo.ReportID;
                sri.SavedReportCreatedByUserID = CMSContext.CurrentUser.UserID;
                sri.SavedReportParameters = this.ReportInfo.ReportParameters;

                string name = this.ReportInfo.ReportDisplayName;
                string timeStamp = " - " + DateTime.Now.ToString();
                if (name.Length + timeStamp.Length > 200)
                {
                    name = name.Substring(0, name.Length - (name.Length + timeStamp.Length - 200));
                }
                sri.SavedReportTitle = name + timeStamp;
                SavedReportInfoProvider.SetSavedReportInfo(sri);

                // Render control to get HTML code
                mSavedReportId = sri.SavedReportID;
                mSaveMode = true;

                formElem.SaveData("");

                ReloadData(true);

                // Render panel with controls and save it to string
                StringBuilder sb = new StringBuilder();
                Html32TextWriter writer2 = new Html32TextWriter(new StringWriter(sb));
                this.pnlContent.RenderControl(writer2);

                // Save HTML to the saved report
                sri.SavedReportHTML = ResolveMacros(sb.ToString());
                sri.SavedReportHTML = HTMLHelper.UnResolveUrls(sri.SavedReportHTML, ResolveUrl("~/"));
                SavedReportInfoProvider.SetSavedReportInfo(sri);

                return sri.SavedReportID;
            }
            catch
            {
            }
        }
        return 0;
    }


}
