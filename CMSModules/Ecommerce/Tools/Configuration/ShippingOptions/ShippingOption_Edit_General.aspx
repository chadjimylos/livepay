<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShippingOption_Edit_General.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_ShippingOptions_ShippingOption_Edit_General"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" %>

<%@ Register Src="~/CMSModules/Ecommerce/Controls/UI/PriceSelector.ascx" TagName="PriceSelector"
    TagPrefix="cms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <div class="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <table style="vertical-align: top">
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblShippingOptionDisplayName" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtShippingOptionDisplayName" runat="server" CssClass="TextBoxField"
                        MaxLength="200" EnableViewState="false" />
                    <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtShippingOptionDisplayName"
                        Display="Dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblShippingOptionName" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtShippingOptionName" runat="server" CssClass="TextBoxField" MaxLength="200"
                        EnableViewState="false" />
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtShippingOptionName"
                        Display="Dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblShippingOptionCharge" EnableViewState="false" />
                </td>
                <td>
                    <cms:PriceSelector ID="txtShippingOptionCharge" runat="server" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel runat="server" ID="lblShippingOptionEnabled" EnableViewState="false"
                        ResourceString="general.enabled" DisplayColon="true" />
                </td>
                <td>
                    <asp:CheckBox ID="chkShippingOptionEnabled" runat="server" CssClass="CheckBoxMovedLeft"
                        Checked="True" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                        CssClass="SubmitButton" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
