<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Report_List.aspx.cs" Inherits="CMSModules_Reporting_Tools_Report_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Report_List.xml" OrderBy="ReportDisplayName"
        Columns="ReportID, ReportDisplayName" IsLiveSite="false" />
</asp:Content>
