using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;

using CMS.GlobalHelper;
using CMS.Newsletter;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Preview : CMSUserControl
{
    // Maximal number of subscribers for preview
    private const int MAX_PREVIEW_SUBSCRIBERS = 20;


    protected void Page_Load(object sender, EventArgs e)
    {
        InitGUI();

        // Get issue ID from query string
        int newsletterIssueId = QueryHelper.GetInteger("issueid", 0);

        // Get Issue object
        Issue issue = IssueProvider.GetIssue(newsletterIssueId);
        if (issue == null)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Newsletter_Issue_New_Preview.NoIssue");

            return;
        }

        // Get all subscribers' IDs of the newsletter
        DataSet subscribers = SubscriberProvider.GetAllSubscribersOfNewsletter(issue.IssueNewsletterID);
        string script = "";
        if (!DataHelper.DataSourceIsEmpty(subscribers))
        {
            // Generate javascript based on subscribers

            // Limit max subscribers count to MAX_PREVIEW_SUBSCRIBERS
            int maxCount = subscribers.Tables[0].Rows.Count;
            if (maxCount > MAX_PREVIEW_SUBSCRIBERS)
            {
                maxCount = MAX_PREVIEW_SUBSCRIBERS;
            }

            script += "newsletterIssueId =" + newsletterIssueId.ToString() + "; \n";
            script += "var guid = new Array(" + maxCount + "); \n ";
            script += "var email = new Array(" + maxCount + "); \n ";
            script += "var subject = new Array(" + maxCount + "); \n ";
            script += "var subscribers = new Array(guid, email); \n ";

            // Ensure correct subject culture
            string culture = CultureHelper.GetDefaultCulture(CMSContext.CurrentSiteName);

            // Get newsletter object
            Newsletter news = NewsletterProvider.GetNewsletter(issue.IssueNewsletterID);
            // Initialize data for subject resolving
            object[] data = new object[3];
            data[0] = issue;
            if (news != null)
            {
                data[1] = news;
            }

            // Get subject base
            string subjectBase = ResHelper.GetString("general.subject") + ResHelper.Colon;

            int subscriberId = 0;
            Subscriber subscriber = null;
            Subscriber sb = null;
            SortedDictionary<int, Subscriber> subMembers = null;

            for (int i = 0; i < maxCount; i++)
            {
                subscriberId = ValidationHelper.GetInteger(DataHelper.GetDataRowValue(subscribers.Tables[0].Rows[i], "SubscriberID"), 0);

                // Get subscriber
                subscriber = SubscriberProvider.GetSubscriber(subscriberId);
                if (subscriber != null)
                {
                    // Insert subscriber GUID
                    script += "guid[" + i.ToString() + "] = '" + subscriber.SubscriberGUID.ToString() + "'; \n ";

                    // Get subscriber's member (different for user or role subscribers)
                    subMembers = SubscriberProvider.GetSubscribers(subscriber, 1, 0, null);
                    foreach (KeyValuePair<int, Subscriber> item in subMembers)
                    {
                        // Get 1st subscriber's member
                        sb = item.Value;
                        if (sb != null)
                        {
                            // Create information line
                            string infoLine = ScriptHelper.GetString(sb.SubscriberEmail, false);
                            if (sb.SubscriberType == SiteObjectType.USER)
                            {
                                infoLine += " (" + SiteObjectType.USER + ")";
                            }
                            else if (sb.SubscriberType == SiteObjectType.ROLE)
                            {
                                infoLine += " (" + SiteObjectType.ROLE + ")";
                            }
                            script += "email[" + i.ToString() + "] = '" + infoLine + "'; \n ";

                            // Initialize data for subject resolving
                            data[2] = sb;
                            string subject = issue.IssueSubject;
                            // Resolve dynamic field macros ({%FirstName%}, {%LastName%}, {%Email%})
                            IssueHelper ih = new IssueHelper();
                            if (ih.LoadDynamicFields(sb, news, issue.IssueID, true, CMSContext.CurrentSiteName, null, null, null))
                            {
                                subject = ih.ResolveDynamicFieldMacros(subject);
                            }

                            // Create resolved subject
                            string subj = HTMLHelper.HTMLEncode(subjectBase + " " + IssueHelper.ResolveMacros(data, subject, culture));
                            script += "subject[" + i.ToString() + "] = " + ScriptHelper.GetString(subj) + "; \n ";
                        }
                    }
                }
            }
        }
        else
        {
            // No subscribers? => hide 'prev' and 'next' link buttons
            pnlLinkButtons.Visible = false;

            // Generate void javascript 
            script += "newsletterIssueId =" + newsletterIssueId.ToString() + "; \n ";
            script += "var guid = new Array(1); \n ";
            script += "var email = new Array(1); \n ";
            script += "var subscribers = new Array(guid, email); \n ";
            script += "guid[1] = 0; \n ";
            script += "email[1] = 0; \n ";
        }
        ltlScript.Text = ScriptHelper.GetScript(script);

        if (!RequestHelper.IsPostBack())
        {
            ltlPageLoadScript.Text = ScriptHelper.GetScript("pageLoad();");
        }
    }


    /// <summary>
    /// GUI initialization.
    /// </summary>
    protected void InitGUI()
    {
        lblSubscriber.Text = ResHelper.GetString("Newsletter_Issue_New_Preview.Subscriber") + "&nbsp;&nbsp;&nbsp;&nbsp;";

        lnkNext.Text = ResHelper.GetString("general.next") + " >";
        lblNext.Text = ResHelper.GetString("general.next") + " >";
        lnkPrevious.Text = "< " + ResHelper.GetString("general.back");
        lblPrevious.Text = "< " + ResHelper.GetString("general.back");
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        string elemString = "var lblEmail = '" + lblEmail.ClientID + "';\n" +
            "var lblPrev = '" + lblPrevious.ClientID + "';\n" +
            "var lnkPrev = '" + lnkPrevious.ClientID + "';\n" +
            "var lblNext = '" + lblNext.ClientID + "';\n" +
            "var lnkNext = '" + lnkNext.ClientID + "';\n" +
            "var lblSubj = '" + lblSubject.ClientID + "';\n";

        // Register client IDs of the elements
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "PreviewElems", ScriptHelper.GetScript(elemString));
    }
}
