CREATE TABLE [CMS_WebTemplate] (
		[WebTemplateID]               int NOT NULL IDENTITY(1, 1),
		[WebTemplateDisplayName]      nvarchar(200) NOT NULL,
		[WebTemplateFileName]         nvarchar(100) NOT NULL,
		[WebTemplateDescription]      nvarchar(max) NOT NULL,
		[WebTemplateGUID]             uniqueidentifier NOT NULL,
		[WebTemplateLastModified]     datetime NOT NULL,
		[WebTemplateName]             nvarchar(100) NOT NULL,
		[WebTemplateOrder]            int NOT NULL,
		[WebTemplateLicenses]         nvarchar(200) NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WebTemplate]
	ADD
	CONSTRAINT [PK_CMS_WebTemplate]
	PRIMARY KEY
	NONCLUSTERED
	([WebTemplateID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_WebTemplate_WebTemplateOrder]
	ON [CMS_WebTemplate] ([WebTemplateOrder])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
