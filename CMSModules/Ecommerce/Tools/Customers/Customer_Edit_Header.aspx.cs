using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.FormControls;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Tools_Customers_Customer_Edit_Header : CMSEcommercePage
{
    protected int customerId = 0;


    protected void Page_PreInit(object sender, EventArgs e)
    {
        // Change master page for dialog
        int hideBreadcrumbs = QueryHelper.GetInteger("hidebreadcrumbs", 0);
        if(hideBreadcrumbs > 0)
        {
            this.MasterPageFile = "~/CMSMasterPages/UI/Dialogs/TabsHeader.master";
        }

        int showTitle = QueryHelper.GetInteger("showtitle", 0);
        if (showTitle > 0)
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("Customer_Edit.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_Customer/object.png");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        string currentCustomer = "";
        customerId = ValidationHelper.GetInteger(Request.QueryString["customerid"], 0);
        if (customerId > 0)
        {
            CustomerInfo customerInfoObj = CustomerInfoProvider.GetCustomerInfo(customerId);

            if (customerInfoObj != null)
            {
                if (ValidationHelper.GetString(customerInfoObj.CustomerCompany, "") != "")
                {
                    currentCustomer = customerInfoObj.CustomerCompany;
                }
                else
                {
                    currentCustomer = customerInfoObj.CustomerLastName + " " + customerInfoObj.CustomerFirstName;
                }
            }
        }

        int hideBreadcrumbs = ValidationHelper.GetInteger(Request.QueryString["hidebreadcrumbs"], 0);
        
        if (hideBreadcrumbs == 0)
        {
            InitializeBreadcrumbs(currentCustomer);
        }

        // custom fields
        DataForm df = new DataForm();
        df.ClassName = "ecommerce.customer";
        df.ItemID = customerId;

        int showCustomFields = (df.BasicForm.FormInformation.GetFormElements(true, false).Count <= 0 ? 0 : 1);;

        InitializeMasterPage(showCustomFields, customerId);
    }


    /// <summary>
    /// Initializes master page elements
    /// </summary>
    private void InitializeMasterPage(int showCustomFields, int customerId)
    {
        // Set the HELP element
        this.CurrentMaster.Title.HelpTopicName = "general_tab14";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Set the tabs
        string[,] tabs = new string[6 + showCustomFields, 4];
        
        int lastTabIndex = 0;

        tabs[lastTabIndex, 0] = ResHelper.GetString("general.general");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab14');";
        tabs[lastTabIndex, 2] = "Customer_Edit_General.aspx?customerId=" + customerId.ToString();
        lastTabIndex++;

        if (showCustomFields != 0)
        {
            tabs[lastTabIndex, 0] = ResHelper.GetString("Customer_Edit_Header.CustomFields");
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'custom_fields_tab2');";
            tabs[lastTabIndex, 2] = "Customer_Edit_CustomFields.aspx?customerId=" + customerId.ToString();
            lastTabIndex++;
        }

        tabs[lastTabIndex, 0] = ResHelper.GetString("Customer_Edit_Header.Addresses");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'address_tab');";
        tabs[lastTabIndex, 2] = "Customer_Edit_Address_List.aspx?customerId=" + customerId.ToString();
        lastTabIndex++;

        tabs[lastTabIndex, 0] = ResHelper.GetString("Customer_Edit_Header.Orders");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'orders_tab');";
        tabs[lastTabIndex, 2] = "../Orders/Order_List.aspx?customerId=" + customerId.ToString();
        lastTabIndex++;

        tabs[lastTabIndex, 0] = ResHelper.GetString("Customer_Edit_Header.Credit");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'credit_tab');";
        tabs[lastTabIndex, 2] = "Customer_Edit_Credit_List.aspx?customerId=" + customerId.ToString();
        lastTabIndex++;

        if (ModuleEntry.IsModuleLoaded(ModuleEntry.NEWSLETTER))
        {
            tabs[lastTabIndex, 0] = ResHelper.GetString("Customer_Edit_Header.Newsletters");
            tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'newsletter_tab');";
            tabs[lastTabIndex, 2] = "../../../Newsletters/Tools/Customers/Customer_Edit_Newsletters.aspx?customerId=" + customerId.ToString();
            lastTabIndex++;
        }

        tabs[lastTabIndex, 0] = ResHelper.GetString("Customer_Edit_Header.Login");
        tabs[lastTabIndex, 1] = ""; // "SetHelpTopic('helpTopic', 'login_tab');";
        tabs[lastTabIndex, 2] = "Customer_Edit_Login.aspx?customerId=" + customerId.ToString();
        lastTabIndex++;

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "CustomerContent";
    }


    /// <summary>
    /// Initializes the breadcrumb mastre page element
    /// </summary>
    private void InitializeBreadcrumbs(string currentCustomer)
    {
        // initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Customer_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Customers/Customer_List.aspx";
        pageTitleTabs[0, 2] = "ecommerceContent";
        pageTitleTabs[1, 0] = currentCustomer;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }
}
