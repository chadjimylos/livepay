set identity_insert [cms_class] on
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Culture', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="CultureID" fieldcaption="CultureID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="a7cdc074-6681-4037-9c7c-168f92de6ec0" /><field column="CultureName" fieldcaption="CultureName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5523daf0-292c-462e-9b45-a8aff0e555d6" /><field column="CultureCode" fieldcaption="CultureCode" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d9cfc140-95f9-4fc4-b123-0b4b5bdaf347" /><field column="CultureShortName" fieldcaption="CultureShortName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c5669759-7b7d-47ae-8aeb-4e1e33490105" /><field column="CultureGUID" fieldcaption="CultureGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="17a5237f-e3ca-4052-848d-cab4b5423494" /><field column="CultureLastModified" fieldcaption="CultureLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="29623bff-09c2-4789-86c2-dd326b85ef20" /></form>', '20091103 08:51:43', 1, N'cms.culture', 0, 0, 0, '21ea0bc0-a9b7-4888-8255-9bd5255e2ef0', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Culture', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Culture">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CultureID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="CultureName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CultureCode">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CultureShortName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CultureGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="CultureLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Culture" />
      <xs:field xpath="CultureID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Site', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="8f1d2a8a-6f44-41ef-899b-d0dd37b82cab" /><field column="SiteName" fieldcaption="SiteName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4f69f0f6-e786-435f-83f1-9f771fa2f726" /><field column="SiteDisplayName" fieldcaption="SiteDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="24ef1161-4c27-4bea-8b37-36acc59cff34" /><field column="SiteDescription" fieldcaption="SiteDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6c445911-e93e-4449-bb1f-9cc4048e788f" /><field column="SiteStatus" fieldcaption="SiteStatus" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4683b075-8db9-4411-bf14-563e795cdd95" /><field column="SiteDomainName" fieldcaption="SiteDomainName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4a553b11-0d6a-44c5-8e2c-7bef0ce6587d" /><field column="SiteDefaultStylesheetID" fieldcaption="SiteDefaultStylesheetID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3f4474c6-9673-4bbc-8782-d4d24a38826b" /><field column="SiteDefaultVisitorCulture" fieldcaption="SiteDefaultVisitorCulture" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ddfbfe8d-3b87-4785-b147-24a28f107937" /><field column="SiteDefaultEditorStylesheet" fieldcaption="SiteDefaultEditorStylesheet" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0c91ee48-7b00-4ece-ab38-39e20fd64788" /><field column="SiteStoreAllowAnonymousCustomers" fieldcaption="SiteStoreAllowAnonymousCustomers" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1e4b0c07-819c-4f7e-8d87-a9fa4c4b08dc" /><field column="SiteStoreShippingFreeLimit" fieldcaption="SiteStoreShippingFreeLimit" visible="true" columntype="double" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0fbbcac8-0d4d-4c3d-bc38-cdc32568658a" /><field column="SiteInvoiceTemplate" fieldcaption="SiteInvoiceTemplate" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e322bb09-8b88-457e-885c-bdf525826fdf" /><field column="SiteDefaultCountryID" fieldcaption="SiteDefaultCountryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5bfa9aaa-01b9-4fff-8199-0692ed6ce985" /><field column="SiteShowTaxRegistrationID" fieldcaption="SiteShowTaxRegistrationID" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3d40ed12-4eca-4caf-b889-021791f55df6" /><field column="SiteShowOrganizationID" fieldcaption="SiteShowOrganizationID" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ed4b3a03-eff6-4d5a-a76d-9c8734db1404" /><field column="SiteSendStoreEmailsFrom" fieldcaption="SiteSendStoreEmailsFrom" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="64349175-c1ea-43d2-b62a-16268db96597" /><field column="SiteSendOrderNotificationTo" fieldcaption="SiteSendOrderNotificationTo" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c9bd4b53-0cf1-407f-9aef-bc424a1a0d7b" /><field column="SiteUseExtraCompanyAddress" fieldcaption="SiteUseExtraCompanyAddress" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43ef6884-091a-4af8-aef7-021693c5303a" /><field column="SiteSendOrderNotification" fieldcaption="SiteSendOrderNotification" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="860ed31d-7972-403b-b0af-85ed74157b76" /><field column="SiteSendPaymentNotification" fieldcaption="SiteSendPaymentNotification" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cb794bcb-d211-4300-b8ed-2df974b971ca" /><field column="SiteCheckoutProcess" fieldcaption="SiteCheckoutProcess" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="77bbf7a8-e3f4-4b12-9a2d-5d5a77179886" /><field column="SiteGUID" fieldcaption="SiteGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="de9b4fc5-3c93-4751-9b10-061edd14a3d8" /><field column="SiteLastModified" fieldcaption="SiteLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="90802d29-9156-46b9-891a-4612618d8ea5" /><field column="SiteRequireOrgTaxRegIDs" fieldcaption="SiteRequireOrgTaxRegIDs" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c2ee5d67-b0c3-4b0c-b357-8e21a8759d09" /></form>', '20100426 15:52:02', 52, N'cms.site', 0, 0, 0, '8f2f80f1-13cb-4050-bc10-14a45b09f4e0', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Site', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Site">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SiteID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SiteName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteStatus">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="20" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteDomainName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteDefaultStylesheetID" type="xs:int" minOccurs="0" />
              <xs:element name="SiteDefaultVisitorCulture" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteDefaultEditorStylesheet" type="xs:int" minOccurs="0" />
              <xs:element name="SiteStoreAllowAnonymousCustomers" type="xs:boolean" minOccurs="0" />
              <xs:element name="SiteStoreShippingFreeLimit" type="xs:double" minOccurs="0" />
              <xs:element name="SiteInvoiceTemplate" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteDefaultCountryID" type="xs:int" minOccurs="0" />
              <xs:element name="SiteShowTaxRegistrationID" type="xs:boolean" minOccurs="0" />
              <xs:element name="SiteShowOrganizationID" type="xs:boolean" minOccurs="0" />
              <xs:element name="SiteSendStoreEmailsFrom" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteSendOrderNotificationTo" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteUseExtraCompanyAddress" type="xs:boolean" minOccurs="0" />
              <xs:element name="SiteSendOrderNotification" type="xs:boolean" minOccurs="0" />
              <xs:element name="SiteSendPaymentNotification" type="xs:boolean" minOccurs="0" />
              <xs:element name="SiteCheckoutProcess" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="SiteLastModified" type="xs:dateTime" minOccurs="0" />
              <xs:element name="SiteRequireOrgTaxRegIDs" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Site" />
      <xs:field xpath="SiteID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Role', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="RoleID" fieldcaption="RoleID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="0c8cc1de-1c82-4596-b6d0-b2c60bcad3b7" /><field column="RoleDisplayName" fieldcaption="RoleDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="c5f83fe3-f362-431f-80be-2d40122e469e" /><field column="RoleName" fieldcaption="RoleName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="918120c1-6d6b-42f5-be30-37f91af11666" /><field column="RoleDescription" fieldcaption="RoleDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c465483d-4158-42e0-bb25-0c274769c74a" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7238f49e-49eb-46a7-9dc4-811c3ca7d524" /><field column="RoleGUID" fieldcaption="RoleGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ad0f25d2-15b7-4eae-86bd-fc64538c45f9" /><field column="RoleLastModified" fieldcaption="RoleLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="11b1bf06-0088-48a8-87d6-cffafff290ca" /><field column="RoleGroupID" fieldcaption="RoleGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bf9ad143-276c-4860-8005-adf080f77aac" /><field column="RoleIsGroupAdministrator" fieldcaption="RoleIsGroupAdministrator" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e4477aee-81e5-475f-90e2-7bb416b9f412" /><field column="RoleIsDomain" fieldcaption="RoleIsDomain" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="aa3f60b2-8781-44d4-8705-d264d514883c" /></form>', '20100218 08:26:55', 56, N'cms.Role', 0, 0, 0, '1dba5a45-954e-442c-8a00-41927c501f2b', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Role', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Role">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="RoleID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="RoleDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="RoleName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="RoleDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteID" type="xs:int" />
              <xs:element name="RoleGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="RoleLastModified" type="xs:dateTime" />
              <xs:element name="RoleGroupID" type="xs:int" minOccurs="0" />
              <xs:element name="RoleIsGroupAdministrator" type="xs:boolean" minOccurs="0" />
              <xs:element name="RoleIsDomain" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Role" />
      <xs:field xpath="RoleID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'UserNickName', NULL, N'User', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="UserID" fieldcaption="UserID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="153b1cec-1580-43ae-adf8-ca0e4879d168" /><field column="UserName" fieldcaption="UserName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="aa4122b7-db14-48a5-885e-07a242f84702" /><field column="FirstName" fieldcaption="FirstName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="60af25ea-ad95-49ba-a446-2ea754cd10be" /><field column="MiddleName" fieldcaption="MiddleName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="bc86d726-a42d-4401-b92a-227e8e280293" /><field column="LastName" fieldcaption="LastName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="67305fcf-928a-45ea-8e29-735f17da0972" /><field column="FullName" fieldcaption="FullName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="5c3003d6-de76-4e96-aca4-cf1efa86b3fa" /><field column="Email" fieldcaption="Email" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="fcef0fb3-6145-48ed-8f41-e28b71a665a8" /><field column="UserPassword" fieldcaption="UserPassword" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="adaad374-af5a-4e1b-afc7-72b4fb798d19" /><field column="PreferredCultureCode" fieldcaption="PreferredCultureCode" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="10" publicfield="false" spellcheck="true" guid="03d85a82-c928-4967-b793-f33bedb426be" /><field column="PreferredUICultureCode" fieldcaption="PreferredUICultureCode" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="10" publicfield="false" spellcheck="true" guid="d0a047c0-ea96-4422-af97-fff6c35a80de" /><field column="UserEnabled" fieldcaption="UserEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d5d58e7d-6b71-4a51-9226-d7ce166e8580" /><field column="UserIsEditor" fieldcaption="UserIsEditor" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9804fc8b-7b25-4c23-b0f4-f6c57b7ae77f" /><field column="UserIsGlobalAdministrator" fieldcaption="UserIsGlobalAdministrator" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4de4df49-2e8d-4bb5-9fb9-a3da2a9040ba" /><field column="UserIsExternal" fieldcaption="UserIsExternal" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="def5feec-077b-4c26-8c4a-005a4a8d82d8" /><field column="UserPasswordFormat" fieldcaption="UserPasswordFormat" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="10" publicfield="false" spellcheck="true" guid="d3f178a2-6df1-4651-bec5-864be986332a" /><field column="UserCreated" fieldcaption="UserCreated" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d75ebe3e-3d7d-44ab-af34-0ee947c3a3eb" /><field column="LastLogon" fieldcaption="LastLogon" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8bb80186-0c9b-45cb-91d1-cbec14b8fdc7" /><field column="UserStartingAliasPath" fieldcaption="UserStartingAliasPath" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="e2152255-6ad0-4b88-8497-2b505c64d23c" /><field column="UserGUID" fieldcaption="UserGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="89fae932-ca98-420b-92a0-0fc146856ce3" /><field column="UserLastModified" fieldcaption="UserLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e1f9c8fd-8c5d-415c-ad0e-935034d751de" /><field column="UserLastLogonInfo" fieldcaption="UserLastLogonInfo" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c17d233f-cd61-42d7-9c98-f96267a12764" /><field column="UserIsHidden" fieldcaption="UserIsHidden" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="82febdf9-9057-4cea-bbba-de40b8cf653e" /><field column="UserVisibility" fieldcaption="UserVisibility" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="150ce55f-7617-4fff-9d31-6f3792291b67" /><field column="UserIsDomain" fieldcaption="UserIsDomain" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6ca5c321-81dd-4fbf-8ccc-9df2675839f9" /><field column="UserHasAllowedCultures" fieldcaption="UserHasAllowedCultures" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e7db3339-6c1c-413d-8fa3-fa85d99b8c80" /><field column="UserSiteManagerDisabled" fieldcaption="Site manager disabled" visible="false" defaultvalue="false" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="175707eb-d3ee-42b1-bd2b-7c2f67a70449" visibility="none" ismacro="false" /></form>', '20100419 17:18:59', 59, N'cms.user', 0, 0, 0, '2e02c378-0f3d-45de-9b2d-b8cf2bd87b55', N'', NULL, 0, N'UserActivationDate', N'', N'', N'UserDescription', 0, 0, N'CMS_User', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_User">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="UserID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="UserName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FirstName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="MiddleName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LastName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FullName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="Email" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserPassword">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PreferredCultureCode" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="10" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PreferredUICultureCode" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="10" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserEnabled" type="xs:boolean" />
              <xs:element name="UserIsEditor" type="xs:boolean" />
              <xs:element name="UserIsGlobalAdministrator" type="xs:boolean" />
              <xs:element name="UserIsExternal" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserPasswordFormat" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="10" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserCreated" type="xs:dateTime" minOccurs="0" />
              <xs:element name="LastLogon" type="xs:dateTime" minOccurs="0" />
              <xs:element name="UserStartingAliasPath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="UserLastModified" type="xs:dateTime" />
              <xs:element name="UserLastLogonInfo" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserIsHidden" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserVisibility" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserIsDomain" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserHasAllowedCultures" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserSiteManagerDisabled" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_User" />
      <xs:field xpath="UserID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'<search><item tokenized="False" name="UserID" content="False" searchable="True" id="2fc94fa5-93f2-482a-aaff-18c5e0896091" /><item tokenized="True" name="UserName" content="True" searchable="False" id="a6c9d421-ff1f-4e99-94c3-dc7dbe5d649c" /><item tokenized="True" name="FirstName" content="True" searchable="False" id="2472021c-9cba-4fe3-be89-a798105a26c9" /><item tokenized="True" name="MiddleName" content="True" searchable="False" id="8e698aa5-78f5-48e5-a07b-2dcd90cb542f" /><item tokenized="True" name="LastName" content="True" searchable="False" id="25abd2c2-7bcb-41b0-9f42-2247ecc060a1" /><item tokenized="True" name="FullName" content="True" searchable="False" id="64bf2f46-82a6-4fec-a5f7-fa7ecc00cbef" /><item tokenized="False" name="Email" content="False" searchable="True" id="f4ff2f3c-937a-47d3-a841-5ed9bcdbc1b6" /><item tokenized="False" name="UserPassword" content="False" searchable="False" id="36694adb-52cd-4130-aa0b-e24fbda45bec" /><item tokenized="False" name="PreferredCultureCode" content="False" searchable="False" id="159a2baa-e00b-443d-988e-48dd03fd6ee5" /><item tokenized="False" name="PreferredUICultureCode" content="False" searchable="False" id="cf12e448-77cc-4ae0-a595-0841a6939e58" /><item tokenized="False" name="UserEnabled" content="False" searchable="False" id="622bd09b-e611-4650-a9fd-3bac58e92d83" /><item tokenized="False" name="UserIsEditor" content="False" searchable="False" id="93c1e98a-1378-412a-862e-292b98e360a2" /><item tokenized="False" name="UserIsGlobalAdministrator" content="False" searchable="False" id="ca08d99c-ec92-4ca7-93be-1d8e397909f3" /><item tokenized="False" name="UserIsExternal" content="False" searchable="False" id="8a3fb95b-0012-4276-b0f8-79ce519373f2" /><item tokenized="False" name="UserPasswordFormat" content="False" searchable="False" id="027324a9-c81c-45e8-99f2-1ae9e09abf65" /><item tokenized="False" name="UserCreated" content="False" searchable="True" id="1a8d6875-1a70-4450-8cb4-62e9c3586a31" /><item tokenized="False" name="LastLogon" content="False" searchable="True" id="ee60d640-d81d-435d-ab04-31802d06b4ed" /><item tokenized="False" name="UserStartingAliasPath" content="False" searchable="False" id="f2faf52a-d7c8-4579-9796-63b8c858d6e9" /><item tokenized="False" name="UserGUID" content="False" searchable="False" id="6eebd1d4-bd26-40fa-8da6-f8056ebef090" /><item tokenized="False" name="UserLastModified" content="False" searchable="False" id="66c55a8b-eae7-41ba-8bfd-93ee503d9048" /><item tokenized="False" name="UserLastLogonInfo" content="False" searchable="False" id="c496260c-5c5e-42af-b621-f52c35d4a2b6" /><item tokenized="False" name="UserIsHidden" content="False" searchable="False" id="1bc5d037-03b7-41bf-b5f8-cd0cf322b3c2" /><item tokenized="False" name="UserVisibility" content="False" searchable="False" id="7f43802a-de69-404f-93e1-0f7f3e24caf4" /><item tokenized="False" name="UserIsDomain" content="False" searchable="False" id="1ab1e2bb-e34c-4eb1-abf3-6568854e0f10" /><item tokenized="False" name="UserHasAllowedCultures" content="False" searchable="False" id="343df60e-3364-4d37-9bf7-6b301b66e255" /><item tokenized="False" name="UserSettingsID" content="False" searchable="False" id="81a3f75a-4cdb-42c7-ae61-0501ead0b19b" /><item tokenized="True" name="UserNickName" content="True" searchable="False" id="70f6a70c-5e9a-4792-8091-1939d94d11ca" /><item tokenized="False" name="UserPicture" content="False" searchable="False" id="9a4c0bdd-0274-4ba0-b1c6-2437a9e91a6a" /><item tokenized="True" name="UserSignature" content="True" searchable="False" id="f21d842f-4eb3-48da-bcec-deb32f97ca54" /><item tokenized="False" name="UserURLReferrer" content="False" searchable="False" id="9411f487-4791-4dcb-af62-ecb7c2fdb792" /><item tokenized="False" name="UserCampaign" content="False" searchable="True" id="46120db8-1a49-4189-a2a2-e817572183a7" /><item tokenized="False" name="UserMessagingNotificationEmail" content="False" searchable="False" id="61f550c2-fc75-4061-82c4-e9a2fde3b1da" /><item tokenized="False" name="UserCustomData" content="False" searchable="False" id="4be56ac2-878c-4d6f-a4b2-295aa99fc5ba" /><item tokenized="False" name="UserRegistrationInfo" content="False" searchable="False" id="3cd730ae-c979-4142-9759-7b9a766b3457" /><item tokenized="False" name="UserPreferences" content="False" searchable="False" id="f0b06d46-d88e-451a-996d-6532afc69962" /><item tokenized="False" name="UserActivationDate" content="False" searchable="False" id="087d6b3e-9e20-4e40-97d4-a4caf3297de9" /><item tokenized="False" name="UserActivatedByUserID" content="False" searchable="False" id="12a3ef63-e156-4731-93a8-758842200aa3" /><item tokenized="False" name="UserTimeZoneID" content="False" searchable="False" id="828a8eeb-5977-49dd-b2a9-6b548bc10c7d" /><item tokenized="False" name="UserAvatarID" content="False" searchable="False" id="920a5076-7d8f-4fec-9de3-b3931f169a7c" /><item tokenized="False" name="UserBadgeID" content="False" searchable="False" id="dbbd3541-5368-4528-85a8-c23f38695825" /><item tokenized="False" name="UserShowSplashScreen" content="False" searchable="False" id="fdf5d17b-3b34-4fd0-a463-20eb2df9c730" /><item tokenized="False" name="UserActivityPoints" content="False" searchable="False" id="dda1ce4f-914b-4855-97ee-cef2d9e516f2" /><item tokenized="False" name="UserForumPosts" content="False" searchable="False" id="00fa034d-5162-491e-a69f-22884c9e7d0b" /><item tokenized="False" name="UserBlogComments" content="False" searchable="False" id="7125387a-6d07-4129-872f-84b933772305" /><item tokenized="False" name="UserGender" content="False" searchable="False" id="3f6c1236-2a27-4a53-bc8c-8218f265dc7f" /><item tokenized="False" name="UserDateOfBirth" content="False" searchable="False" id="3ece2c21-cd50-410b-afaf-7e50f0adbefb" /><item tokenized="False" name="UserMessageBoardPosts" content="False" searchable="False" id="81d15c3d-8258-4551-bbf6-b76df6564246" /><item tokenized="False" name="UserSettingsUserGUID" content="False" searchable="False" id="b8006d53-cffe-4c10-9659-c80124164d53" /><item tokenized="False" name="UserSettingsUserID" content="False" searchable="False" id="27b7eca3-5b6e-41ff-a969-0d0e430a2bb8" /><item tokenized="False" name="WindowsLiveID" content="False" searchable="False" id="c44e427c-8c84-4eac-8027-bb420bb56cc5" /><item tokenized="False" name="UserBlogPosts" content="False" searchable="False" id="f080777a-3ed0-4768-826f-7a1b560ea374" /><item tokenized="False" name="UserWaitingForApproval" content="False" searchable="False" id="99c81b09-12f3-4db6-b376-bcf4dfe4957a" /><item tokenized="False" name="UserDialogsConfiguration" content="False" searchable="False" id="816fde4d-83e9-44fa-9632-b82a97f0b5df" /><item tokenized="True" name="UserDescription" content="True" searchable="False" id="aa8e03f1-f2b4-48d0-adf3-ee380783da77" /><item tokenized="False" name="UserUsedWebParts" content="False" searchable="False" id="0f0df891-c127-45d5-b947-ca8ee23a2c92" /><item tokenized="False" name="UserUsedWidgets" content="False" searchable="False" id="8a58b06c-5d8d-43e5-88f2-5fa85b6a06ae" /></search>', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'UserRole', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="UserID" fieldcaption="UserID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="f3837b59-c480-4b5b-a2f5-f2086fa4b9ef" /><field column="RoleID" fieldcaption="RoleID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c2b55895-db19-430a-850a-3c421c97d6b0" /></form>', '20081229 09:07:40', 109, N'cms.userrole', 0, 0, 0, '7664a5c5-128c-4546-a7f9-c6d3e694c28a', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_UserRole', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_UserRole">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="UserID" type="xs:int" />
              <xs:element name="RoleID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_UserRole" />
      <xs:field xpath="UserID" />
      <xs:field xpath="RoleID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Email template', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="EmailTemplateID" fieldcaption="EmailTemplateID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="ada65d2f-9f6f-4cac-b7b4-380a13ba6a53" /><field column="EmailTemplateName" fieldcaption="EmailTemplateName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="db77f0cf-12cf-43e6-be0a-28c1c97fb026" /><field column="EmailTemplateDisplayName" fieldcaption="EmailTemplateDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4c80ea4a-4e2f-4f4e-b524-9ec6acb2290b" /><field column="EmailTemplateText" fieldcaption="EmailTemplateText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7cb8a717-2b0c-4b92-9e38-2530185dcafb" /><field column="EmailTemplateSiteID" fieldcaption="EmailTemplateSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="152ad89f-51ae-42f3-a7cd-b28794ebbb73" /><field column="EmailTemplateGUID" fieldcaption="EmailTemplateGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="42373ea6-c944-44c2-9a3f-95c5bdc78012" /><field column="EmailTemplateLastModified" fieldcaption="EmailTemplateLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="513a454c-2061-4083-88bb-12b92bdf2a5e" /><field column="EmailTemplatePlainText" fieldcaption="EmailTemplatePlainText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6c25debb-1332-41eb-a77c-a69f93b3111c" /><field column="EmailTemplateSubject" fieldcaption="EmailTemplateSubject" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="31f6996b-5f0e-4350-bfb7-4857e4b89bb0" /><field column="EmailTemplateFrom" fieldcaption="EmailTemplateFrom" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="515d919e-b186-4bc4-9064-c086fd34715c" /><field column="EmailTemplateCc" fieldcaption="EmailTemplateCc" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43b52be1-a0d2-4af5-9ae4-5e80ca589396" /><field column="EmailTemplateBcc" fieldcaption="EmailTemplateBcc" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f0fafbef-3dd5-40d8-923c-091f692e7664" /></form>', '20091023 08:23:57', 129, N'cms.emailtemplate', 0, 0, 0, 'f54a32bf-6218-46cc-802c-89efad7a5740', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_EmailTemplate', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_EmailTemplate">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="EmailTemplateID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="EmailTemplateName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailTemplateDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailTemplateText">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailTemplateSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="EmailTemplateGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="EmailTemplateLastModified" type="xs:dateTime" />
              <xs:element name="EmailTemplatePlainText" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailTemplateSubject" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailTemplateFrom" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailTemplateCc" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailTemplateBcc" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_EmailTemplate" />
      <xs:field xpath="EmailTemplateID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Permission', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="PermissionID" fieldcaption="PermissionID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="f61417a5-d60b-4c3e-a8f4-6b5d08407b2e" /><field column="PermissionDisplayName" fieldcaption="PermissionDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="216fca18-3ef5-4346-a067-df2600e48af2" /><field column="PermissionName" fieldcaption="PermissionName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="81099da7-b5b3-4e0c-af3d-9dc48b3b3238" /><field column="ClassID" fieldcaption="ClassID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0c800bfe-2dc6-4053-85cb-5d7fcce37ced" /><field column="ResourceID" fieldcaption="ResourceID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8b3c89b8-6134-4450-b1db-e20cb98a157b" /><field column="PermissionGUID" fieldcaption="PermissionGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="72fec8a3-7ead-451f-8356-f34dc56df28d" /><field column="PermissionLastModified" fieldcaption="PermissionLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="01413912-03a0-436b-a91f-21cc1d3fe749" /><field column="PermissionDescription" fieldcaption="PermissionDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2720ebba-d118-42f8-b4e3-e3b864deddfb" /><field column="PermissionDisplayInMatrix" fieldcaption="PermissionDisplayInMatrix" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ad45cd7d-f305-42d0-ac09-3aab213c2371" /></form>', '20100128 16:35:34', 131, N'cms.permission', 0, 0, 0, '83a574c4-dffd-45f1-bd21-c78f18dcaa72', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Permission', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Permission">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="PermissionID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="PermissionDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PermissionName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassID" type="xs:int" minOccurs="0" />
              <xs:element name="ResourceID" type="xs:int" minOccurs="0" />
              <xs:element name="PermissionGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="PermissionLastModified" type="xs:dateTime" />
              <xs:element name="PermissionDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PermissionDisplayInMatrix" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Permission" />
      <xs:field xpath="PermissionID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Resource', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ResourceID" fieldcaption="ResourceID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="870bc6d4-f89a-4116-80b3-45a32604b394" ismacro="false" /><field column="ResourceDisplayName" fieldcaption="ResourceDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="e4714769-8c09-4488-82ee-69f0c7aeccac" ismacro="false" /><field column="ResourceName" fieldcaption="ResourceName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="69930d99-46c8-4be2-b19c-5a9edb05fb79" ismacro="false" /><field column="ResourceDescription" fieldcaption="ResourceDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="27828b12-d1fb-4cb1-866d-a15f683bb674" ismacro="false" /><field column="ShowInDevelopment" fieldcaption="ShowInDevelopment" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b8fdc63e-35c2-400f-af7f-9b04b39c4b2f" ismacro="false" /><field column="ResourceURL" fieldcaption="ResourceURL" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="1000" publicfield="false" spellcheck="true" guid="5585f8e7-88f5-4b0e-88ec-3c695822530c" ismacro="false" /><field column="ResourceGUID" fieldcaption="ResourceGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2389fed5-9c07-45e7-88db-4e56ec0c2889" ismacro="false" /><field column="ResourceLastModified" fieldcaption="ResourceLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a43d2cef-9fdc-43a8-8cd8-37b161ef4582" ismacro="false" /></form>', '20091124 19:02:33', 134, N'cms.resource', 0, 0, 0, '93746c62-21e4-4fda-bcbd-61c5fcee9945', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Resource', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Resource">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ResourceID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ResourceDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ResourceName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ResourceDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ShowInDevelopment" type="xs:boolean" minOccurs="0" />
              <xs:element name="ResourceURL" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1000" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ResourceGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ResourceLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Resource" />
      <xs:field xpath="ResourceID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Event log', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="EventID" fieldcaption="EventID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="26c35bd1-8066-4da8-a725-07ad0b2b2c6b" /><field column="EventType" fieldcaption="EventType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9faaf8cf-4252-49bf-bda1-9e196842868f" /><field column="EventTime" fieldcaption="EventTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3e07f490-7533-444d-8f56-f524582462dc" /><field column="Source" fieldcaption="Source" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2e2efa2a-56af-4977-b7c3-ad308f96aa41" /><field column="EventCode" fieldcaption="EventCode" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0c924b46-c45d-4b5b-b0e0-61e8ae0689bb" /><field column="UserID" fieldcaption="UserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c093cdd5-eb7f-40ca-a750-f99fcee80d3b" /><field column="UserName" fieldcaption="UserName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="216b36b3-7748-4782-89bd-1a2098ceda4a" columnsize="250" visibility="none" ismacro="false" /><field column="IPAddress" fieldcaption="IPAddress" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ce09dd69-20b2-4d97-bbf3-7d19cd61904e" /><field column="NodeID" fieldcaption="NodeID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d597f6eb-77be-4a4a-994d-b6b868ab6e4a" /><field column="DocumentName" fieldcaption="DocumentName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="52e8e3d1-efb4-4779-b906-a0e8bfb9c2b6" /><field column="EventDescription" fieldcaption="EventDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d5804fb1-6a0c-4cb0-988f-531f2c7c5ba2" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c5b5f876-db4c-47bc-8b45-d6c83d3c9da9" /><field column="EventUrl" fieldcaption="EventUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="76a68240-9806-48d0-9c4b-b17eed6f0fba" /><field column="EventMachineName" fieldcaption="EventMachineName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="aa1bad36-c476-43f0-8713-c61bdfcecfe5" /><field column="EventUserAgent" fieldcaption="EvetUserAgent" visible="true" columntype="longtext" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="38bf0ded-0615-4469-beb6-b8770631b371" visibility="none" ismacro="false" /><field column="EventUrlReferrer" fieldcaption="EventUrlReferrer" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="2000" publicfield="false" spellcheck="true" guid="95f527b8-6fb0-4f4a-9afd-480f6a42841d" visibility="none" ismacro="false" /></form>', '20100513 18:12:33', 140, N'CMS.EventLog', 0, 0, 0, 'e497827b-e411-4975-9277-b73235b21f87', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_EventLog', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_EventLog">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="EventID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="EventType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="5" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EventTime" type="xs:dateTime" />
              <xs:element name="Source">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EventCode">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserID" type="xs:int" minOccurs="0" />
              <xs:element name="UserName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IPAddress" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NodeID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EventDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteID" type="xs:int" minOccurs="0" />
              <xs:element name="EventUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2000" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EventMachineName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EventUserAgent" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EventUrlReferrer" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2000" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_EventLog" />
      <xs:field xpath="EventID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Tree', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="NodeID" fieldcaption="NodeID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="0613f642-fa08-4ccf-91db-f5ca551d3638" /><field column="NodeAliasPath" fieldcaption="NodeAliasPath" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="42542959-12de-4bf6-bed2-d07f3c2abddc" /><field column="NodeName" fieldcaption="NodeName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e238f7de-90c6-4519-8ae1-a414730ef028" /><field column="NodeAlias" fieldcaption="NodeAlias" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d89ab77f-84e6-43b9-9b9b-6937212b1fcb" /><field column="NodeClassID" fieldcaption="NodeClassID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0efc576e-e707-46a1-9743-d98d3e1e180e" /><field column="NodeParentID" fieldcaption="NodeParentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b307bb89-57ad-4f8e-be4d-ac3ef6a86433" /><field column="NodeLevel" fieldcaption="NodeLevel" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2869d3a1-0970-4f75-8dae-7e788fab732d" /><field column="NodeACLID" fieldcaption="NodeACLID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f9173dcf-54dd-4175-9d6c-b0112033b967" /><field column="NodeSiteID" fieldcaption="NodeSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e5ea2dd1-ce02-4e0f-8514-1ebb22f17a2b" /><field column="NodeGUID" fieldcaption="NodeGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="adbb0aee-8534-4305-93ba-edb98a4fa59f" /><field column="NodeOrder" fieldcaption="NodeOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c0154ae9-30e7-4232-8ff9-bc8c8c131ab9" /><field column="IsSecuredNode" fieldcaption="IsSecuredNode" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7932cb3d-ffb3-4a67-b667-531a4ed31dcc" /><field column="NodeCacheMinutes" fieldcaption="NodeCacheMinutes" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bc0e11d5-ba1e-40bd-bc21-ed617bf04545" /><field column="NodeSKUID" fieldcaption="NodeSKUID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4f7cb7bc-d9f2-49dc-83ef-1ea8b4ceb77e" /><field column="NodeDocType" fieldcaption="NodeDocType" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="39b03150-e45e-4f60-8b51-8ba9b9e998ac" /><field column="NodeHeadTags" fieldcaption="NodeHeadTags" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3b56e3cf-f960-4cf5-a617-5fb0bd97e40e" /><field column="NodeBodyElementAttributes" fieldcaption="NodeBodyElementAttributes" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="046d912a-4c8d-4462-9534-b8e1cbff23cd" /><field column="NodeInheritPageLevels" fieldcaption="NodeInheritPageLevels" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a6b88ee8-1fa6-4d46-84fc-f178238f9f5f" /><field column="NodeChildNodesCount" fieldcaption="NodeChildNodesCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a1633aa4-61f3-4620-97e6-fed9c80cbb03" /><field column="RequiresSSL" fieldcaption="RequiresSSL" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="84423e46-399f-4416-94ce-45e68dbe0d4f" /><field column="NodeLinkedNodeID" fieldcaption="NodeLinkedNodeID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9f6f0915-b193-441b-9629-5913925a31e7" /><field column="NodeOwner" fieldcaption="NodeOwner" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="320b49a6-e758-4400-b007-35f6ea15b801" /><field column="NodeCustomData" fieldcaption="NodeCustomData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c5f806aa-0624-491c-8f3c-b943d106fdc2" /><field column="NodeGroupID" fieldcaption="NodeGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6d3b21ac-2686-4766-bad1-80b133ca9ed6" /></form>', '20100513 13:47:28', 143, N'cms.tree', 0, 0, 0, '6d418504-6c8b-44f5-853b-10759216a050', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Tree', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Tree">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="NodeID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="NodeAliasPath">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NodeName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NodeAlias">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NodeClassID" type="xs:int" />
              <xs:element name="NodeParentID" type="xs:int" />
              <xs:element name="NodeLevel" type="xs:int" />
              <xs:element name="NodeACLID" type="xs:int" minOccurs="0" />
              <xs:element name="NodeSiteID" type="xs:int" />
              <xs:element name="NodeGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="NodeOrder" type="xs:int" minOccurs="0" />
              <xs:element name="IsSecuredNode" type="xs:boolean" minOccurs="0" />
              <xs:element name="NodeCacheMinutes" type="xs:int" minOccurs="0" />
              <xs:element name="NodeSKUID" type="xs:int" minOccurs="0" />
              <xs:element name="NodeDocType" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NodeHeadTags" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NodeBodyElementAttributes" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NodeInheritPageLevels" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NodeChildNodesCount" type="xs:int" minOccurs="0" />
              <xs:element name="RequiresSSL" type="xs:int" minOccurs="0" />
              <xs:element name="NodeLinkedNodeID" type="xs:int" minOccurs="0" />
              <xs:element name="NodeOwner" type="xs:int" minOccurs="0" />
              <xs:element name="NodeCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NodeGroupID" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Tree" />
      <xs:field xpath="NodeID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Document', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="DocumentID" fieldcaption="DocumentID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="04c53ea8-89c6-45fe-b9f8-11c869742937" visibility="none" ismacro="false" /><field column="DocumentName" fieldcaption="DocumentName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="1e0f27f5-f59a-4fa1-871f-5c2d946453ca" /><field column="DocumentNamePath" fieldcaption="DocumentNamePath" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="1500" publicfield="false" spellcheck="true" guid="4afd853c-e3da-46dd-87c1-aa931e249b99" /><field column="DocumentModifiedWhen" fieldcaption="DocumentModifiedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d86fd91f-9650-459d-a6a9-f101ec936cdf" /><field column="DocumentModifiedByUserID" fieldcaption="DocumentModifiedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3db3f1c9-02b4-4d37-abd5-1298a9068ac1" /><field column="DocumentForeignKeyValue" fieldcaption="DocumentForeignKeyValue" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1ab4dfd9-6e8c-4b4d-9526-528890bc9c47" /><field column="DocumentCreatedByUserID" fieldcaption="DocumentCreatedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="89690d5f-2c54-4788-8926-b38692719e0e" /><field column="DocumentCreatedWhen" fieldcaption="DocumentCreatedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="82f0e9c1-9dfa-42a1-9b4a-a0090bbfad73" /><field column="DocumentCheckedOutByUserID" fieldcaption="DocumentCheckedOutByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d561f437-cf47-4681-a94e-085d9632b926" /><field column="DocumentCheckedOutWhen" fieldcaption="DocumentCheckedOutWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="65160c58-f425-4370-baf6-b47dc987611d" /><field column="DocumentCheckedOutVersionHistoryID" fieldcaption="DocumentCheckedOutVersionHistoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="31bf940c-d22c-4bf8-a430-776b6d4488c7" /><field column="DocumentPublishedVersionHistoryID" fieldcaption="DocumentPublishedVersionHistoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8229ebd2-c4d2-43bd-82e9-ae9af6146c97" /><field column="DocumentWorkflowStepID" fieldcaption="DocumentWorkflowStepID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b7238cf1-94ac-4c59-87f4-f472f78245b2" /><field column="DocumentPublishFrom" fieldcaption="DocumentPublishFrom" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5998e7fe-a503-4c5a-8711-9a4cbe77d8a3" /><field column="DocumentPublishTo" fieldcaption="DocumentPublishTo" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="75b22166-d757-485f-901a-6636cabe930e" /><field column="DocumentUrlPath" fieldcaption="DocumentUrlPath" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="ce4e33fb-c401-409f-bd58-d43bf642b1af" /><field column="DocumentCulture" fieldcaption="DocumentCulture" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="10" publicfield="false" spellcheck="true" guid="e123ee40-049c-48b6-9dd8-a51b1e7da6b1" /><field column="DocumentNodeID" fieldcaption="DocumentNodeID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3bc79b36-ba8f-4fea-85a4-c72fdbc315d2" /><field column="DocumentPageTitle" fieldcaption="DocumentPageTitle" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a2df3057-5b8d-481b-9247-88b970c57a0b" /><field column="DocumentPageKeyWords" fieldcaption="DocumentPageKeyWords" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e6a709b2-d460-4f13-9abf-c0f100e97033" /><field column="DocumentPageDescription" fieldcaption="DocumentPageDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4af8a60e-4d17-42c6-a830-1f6655aa807e" /><field column="DocumentShowInSiteMap" fieldcaption="DocumentShowInSiteMap" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b82973af-c753-43c4-b98a-1e33382f381f" /><field column="DocumentMenuItemHideInNavigation" fieldcaption="DocumentMenuItemHideInNavigation" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2551533d-3329-445f-a008-b2912c36b7d5" /><field column="DocumentMenuCaption" fieldcaption="DocumentMenuCaption" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="b057698c-757f-478f-8e69-4218eff45127" /><field column="DocumentMenuStyle" fieldcaption="DocumentMenuStyle" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="35cafdb7-af8b-4243-bdce-babe46387a61" /><field column="DocumentMenuItemImage" fieldcaption="DocumentMenuItemImage" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="722c4b74-aa68-4ab9-8a34-7cbc711ee0be" /><field column="DocumentMenuItemLeftImage" fieldcaption="DocumentMenuItemLeftImage" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="06f35e92-e38e-4af4-a7fd-db3a7e625310" /><field column="DocumentMenuItemRightImage" fieldcaption="DocumentMenuItemRightImage" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="8bf6ec0a-3fcc-4024-a9e4-ce022a762b02" /><field column="DocumentPageTemplateID" fieldcaption="DocumentPageTemplateID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0d26da2b-4ac7-489b-a91b-88034794f569" /><field column="DocumentMenuJavascript" fieldcaption="DocumentMenuJavascript" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="538ab879-552c-4d63-b4e0-f41307286544" /><field column="DocumentMenuRedirectUrl" fieldcaption="DocumentMenuRedirectUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="6df14253-cc1c-4ea7-8cd7-6c8375c3c44c" /><field column="DocumentUseNamePathForUrlPath" fieldcaption="DocumentUseNamePathForUrlPath" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3091667f-22b4-4a4e-96ba-709e31f17a8f" /><field column="DocumentStylesheetID" fieldcaption="DocumentStylesheetID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1d74c412-a60d-4ff9-9966-549f3f9483fd" /><field column="DocumentContent" fieldcaption="DocumentContent" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7cc90d0b-19c9-49fa-a7ce-14104dd32586" /><field column="DocumentMenuClass" fieldcaption="DocumentMenuClass" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="d47569fa-a1a5-476a-bb3f-25751812a790" /><field column="DocumentMenuStyleOver" fieldcaption="DocumentMenuStyleOver" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="ea47e74e-8ca9-4e31-99a1-650081889f1a" /><field column="DocumentMenuClassOver" fieldcaption="DocumentMenuClassOver" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="a374c26a-6273-47b2-a4a6-d75efbaf6372" /><field column="DocumentMenuItemImageOver" fieldcaption="DocumentMenuItemImageOver" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="7237fca8-7f53-477a-b075-516bd38d592e" /><field column="DocumentMenuItemLeftImageOver" fieldcaption="DocumentMenuItemLeftImageOver" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="1d2f3721-8d4d-4a08-89af-be5a14d1fe9e" /><field column="DocumentMenuItemRightImageOver" fieldcaption="DocumentMenuItemRightImageOver" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="e4b0b6ce-a5f7-4b66-bf93-6acfdf437593" /><field column="DocumentMenuStyleHighlighted" fieldcaption="DocumentMenuStyleHighlighted" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="e53e6cda-97c2-4fe4-9d04-8a9e639c1ee8" /><field column="DocumentMenuClassHighlighted" fieldcaption="DocumentMenuClassHighlighted" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="ddaaa43a-c322-4d5e-9cd5-d7139f679883" /><field column="DocumentMenuItemImageHighlighted" fieldcaption="DocumentMenuItemImageHighlighted" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="dd5a4cb8-0fc7-4759-b062-6088b67b04ee" /><field column="DocumentMenuItemLeftImageHighlighted" fieldcaption="DocumentMenuItemLeftImageHighlighted" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="240a6893-095d-44f0-aaa6-e32443d64f34" /><field column="DocumentMenuItemRightImageHighlighted" fieldcaption="DocumentMenuItemRightImageHighlighted" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="3eaff1dd-1492-42f8-8035-1214d1dc0a6f" /><field column="DocumentMenuItemInactive" fieldcaption="DocumentMenuItemInactive" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e0b5fe75-ad44-4221-a175-54706d85f2ae" /><field column="DocumentCustomData" fieldcaption="DocumentCustomData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8c76cc14-a486-44f9-a1f2-1947192ca6a2" /><field column="DocumentExtensions" fieldcaption="DocumentExtensions" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="1c1cbb47-7832-4158-9d56-fbb34d6ae87a" /><field column="DocumentCampaign" fieldcaption="DocumentCampaign" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="948c554d-da2a-4e1d-b7ba-9236ab980725" /><field column="DocumentTags" fieldcaption="DocumentTags" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6a33a0ff-ffd9-4615-a1c8-5e1bba780964" /><field column="DocumentTagGroupID" fieldcaption="DocumentTagGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="82d94a1c-a0e9-4496-bad0-6192217212bf" /><field column="DocumentWildcardRule" fieldcaption="DocumentWildcardRule" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="440" publicfield="false" spellcheck="true" guid="c827cdfb-bd56-4fe3-a781-5b6b12a3e79f" visibility="none" ismacro="false" /><field column="DocumentWebParts" fieldcaption="DocumentWebParts" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="770989d4-ef83-48ac-b6a7-9800cedeb67a" /><field column="DocumentGroupWebParts" visible="false" columntype="longtext" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="596af945-57d7-432f-a12a-7b16bf385504" visibility="none" /><field column="DocumentRatingValue" fieldcaption="DocumentRatingValue" visible="true" columntype="double" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="11320c2e-1c67-49e2-bd7a-7273e53f42db" /><field column="DocumentRatings" fieldcaption="DocumentRatings" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="abd2bd0d-8911-4e29-abcb-896d7e8cb8bc" /><field column="DocumentPriority" fieldcaption="DocumentPriority" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="97bfb6a2-ab54-416e-9578-a38844a4b48e" /><field column="DocumentType" fieldcaption="DocumentType" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="50" publicfield="false" spellcheck="true" guid="bb21f9de-f10c-43e9-b200-cc4c25c97acf" /><field column="DocumentLastPublished" fieldcaption="DocumentLastPublished" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="834785f6-f532-435f-b76f-c20dfd9399f6" /><field column="DocumentUseCustomExtensions" fieldcaption="DocumentUseCustomExtensions" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="aeae798e-898e-428c-8468-eff4126e0299" /></form>', '20100504 16:08:41', 144, N'cms.document', 0, 0, 0, '4d58a766-f13b-48fe-950b-63dbc2aeca69', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Document', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Document">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="DocumentID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="DocumentName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentNamePath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1500" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentModifiedWhen" type="xs:dateTime" minOccurs="0" />
              <xs:element name="DocumentModifiedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentForeignKeyValue" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentCreatedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentCreatedWhen" type="xs:dateTime" minOccurs="0" />
              <xs:element name="DocumentCheckedOutByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentCheckedOutWhen" type="xs:dateTime" minOccurs="0" />
              <xs:element name="DocumentCheckedOutVersionHistoryID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentPublishedVersionHistoryID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentWorkflowStepID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentPublishFrom" type="xs:dateTime" minOccurs="0" />
              <xs:element name="DocumentPublishTo" type="xs:dateTime" minOccurs="0" />
              <xs:element name="DocumentUrlPath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentCulture">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="10" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentNodeID" type="xs:int" />
              <xs:element name="DocumentPageTitle" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentPageKeyWords" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentPageDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentShowInSiteMap" type="xs:boolean" />
              <xs:element name="DocumentMenuItemHideInNavigation" type="xs:boolean" />
              <xs:element name="DocumentMenuCaption" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuStyle" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuItemImage" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuItemLeftImage" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuItemRightImage" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentPageTemplateID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentMenuJavascript" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuRedirectUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentUseNamePathForUrlPath" type="xs:boolean" minOccurs="0" />
              <xs:element name="DocumentStylesheetID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentContent" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuClass" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuStyleOver" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuClassOver" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuItemImageOver" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuItemLeftImageOver" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuItemRightImageOver" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuStyleHighlighted" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuClassHighlighted" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuItemImageHighlighted" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuItemLeftImageHighlighted" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuItemRightImageHighlighted" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentMenuItemInactive" type="xs:boolean" minOccurs="0" />
              <xs:element name="DocumentCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentExtensions" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentCampaign" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentTags" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentTagGroupID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentWildcardRule" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="440" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentWebParts" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentRatingValue" type="xs:double" minOccurs="0" />
              <xs:element name="DocumentRatings" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentPriority" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentType" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DocumentLastPublished" type="xs:dateTime" minOccurs="0" />
              <xs:element name="DocumentUseCustomExtensions" type="xs:boolean" minOccurs="0" />
              <xs:element name="DocumentGroupWebParts" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Document" />
      <xs:field xpath="DocumentID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'<search><item tokenized="False" name="SKUID" content="False" searchable="True" id="2a9f5150-ddcf-4d25-a442-17d57bbeb80b" /><item tokenized="False" name="SKUNumber" content="False" searchable="True" id="f16332ad-16d7-4f22-a3cb-ef40e453e82c" /><item tokenized="False" name="SKUName" content="True" searchable="False" id="d235e8b1-d5ac-4d7e-b167-8237316df6a7" /><item tokenized="False" name="SKUDescription" content="True" searchable="False" id="6f14749e-404c-4ecb-81ea-762f8bad1570" /><item tokenized="False" name="SKUPrice" content="False" searchable="True" id="05d67677-caea-4580-b2a8-a27a91d5b02d" /><item tokenized="False" name="SKUEnabled" content="False" searchable="True" id="56e3b2a9-6b47-4169-bd06-896e758d7318" /><item tokenized="False" name="SKUDepartmentID" content="False" searchable="True" id="839d519a-878a-4e60-9706-223837906db0" /><item tokenized="False" name="SKUManufacturerID" content="False" searchable="True" id="78eef96c-a7f5-42da-96b8-46764a264718" /><item tokenized="False" name="SKUInternalStatusID" content="False" searchable="True" id="ac6b3c73-358a-4f99-8498-ac676617c755" /><item tokenized="False" name="SKUPublicStatusID" content="False" searchable="True" id="148cc6af-3217-4692-bb7f-d550f090ddd9" /><item tokenized="False" name="SKUSupplierID" content="False" searchable="True" id="ed9570f1-0122-4957-b50d-908990ed7853" /><item tokenized="False" name="SKUAvailableInDays" content="False" searchable="True" id="7339087a-a215-4100-af36-9e1f68087338" /><item tokenized="False" name="SKUGUID" content="False" searchable="False" id="8c41eaf6-41a2-4062-8134-55d6085db248" /><item tokenized="False" name="SKUImagePath" content="True" searchable="False" id="f39280c4-eda9-4cca-b11f-a9824fc35544" /><item tokenized="False" name="SKUWeight" content="False" searchable="True" id="d8dbc82f-0b7f-4534-90f5-129f530ee1da" /><item tokenized="False" name="SKUWidth" content="False" searchable="True" id="dd275787-a881-4e5e-8423-4cf2e3bf3d3e" /><item tokenized="False" name="SKUDepth" content="False" searchable="True" id="b45c8cce-f164-4bc3-b380-a20389da5255" /><item tokenized="False" name="SKUHeight" content="False" searchable="True" id="3749f970-7f77-43fd-9bca-654746eecb7b" /><item tokenized="False" name="SKUAvailableItems" content="False" searchable="True" id="db94a211-1ba4-4b00-8fc5-48ee9ae93c78" /><item tokenized="False" name="SKUSellOnlyAvailable" content="False" searchable="True" id="235bbef3-585f-4100-ac67-425fc36e2a10" /><item tokenized="False" name="SKUCustomData" content="False" searchable="True" id="4c2c24a3-a0da-4fe0-ba16-323d9f1b0657" /><item tokenized="False" name="SKUOptionCategoryID" content="False" searchable="True" id="09fba92f-31e1-44b2-a0bc-e9a418c69c06" /><item tokenized="False" name="SKUOrder" content="False" searchable="True" id="f0cff458-393c-4707-ba4d-7f316b180dcd" /><item tokenized="False" name="SKULastModified" content="False" searchable="True" id="214b2b49-a205-4d43-8bdf-bb814705c665" /><item tokenized="False" name="SKUCreated" content="False" searchable="True" id="d1555f72-cb12-40c8-aacb-0dc9bb3b3121" /><item tokenized="False" name="DocumentID" content="False" searchable="True" id="f857affd-4a68-48a8-9be9-fd9b558e7da3" /><item tokenized="True" name="DocumentName" content="True" searchable="False" id="b88ce66a-ad99-4855-9cef-959b8f2eb006" /><item tokenized="True" name="DocumentNamePath" content="True" searchable="False" id="110fd353-db7b-4ada-87a1-5cbcc08d42c3" /><item tokenized="False" name="DocumentModifiedWhen" content="False" searchable="True" id="0adb9d8a-facd-4642-b194-9e8847c0e5e1" /><item tokenized="False" name="DocumentModifiedByUserID" content="False" searchable="True" id="5a0e0207-c04d-41e4-8369-b0ada904c027" /><item tokenized="False" name="DocumentForeignKeyValue" content="False" searchable="True" id="a42b639d-0339-456a-ae8e-436197b82066" /><item tokenized="False" name="DocumentCreatedByUserID" content="False" searchable="True" id="69b85b4b-762d-4f0d-a019-10c2d9d986ff" /><item tokenized="False" name="DocumentCreatedWhen" content="False" searchable="True" id="61b3e802-c8e6-4cca-8939-1d716130300b" /><item tokenized="False" name="DocumentCheckedOutByUserID" content="False" searchable="True" id="f9ef4a5d-1b5a-47a4-b206-e8426f3042a2" /><item tokenized="False" name="DocumentCheckedOutWhen" content="False" searchable="True" id="392bb2b1-d01b-46ae-a162-5de191c9bd7f" /><item tokenized="False" name="DocumentCheckedOutVersionHistoryID" content="False" searchable="True" id="3ab4b335-4919-4b85-80c4-6e6e0585f097" /><item tokenized="False" name="DocumentPublishedVersionHistoryID" content="False" searchable="True" id="76c52859-7fa6-4368-aba6-da33581062a4" /><item tokenized="False" name="DocumentWorkflowStepID" content="False" searchable="True" id="6aeefa9e-998e-4967-9b06-ee9bbfd73845" /><item tokenized="False" name="DocumentPublishFrom" content="False" searchable="True" id="3f5edefa-4033-4367-b592-88719c19121c" /><item tokenized="False" name="DocumentPublishTo" content="False" searchable="True" id="7a041640-9425-4dd6-9fdf-626c2bd08e05" /><item tokenized="True" name="DocumentUrlPath" content="True" searchable="False" id="e2f5e668-b495-47cc-b4c5-cb03544b29a9" /><item tokenized="True" name="DocumentCulture" content="False" searchable="True" id="2ce2dc0b-a388-491a-aba3-20b00d9628ac" /><item tokenized="False" name="DocumentNodeID" content="False" searchable="True" id="e870e14a-1ad4-4387-8f51-f2619af34ba9" /><item tokenized="True" name="DocumentPageTitle" content="True" searchable="False" id="78717d03-c9c0-4390-b02e-f368cd33a23c" /><item tokenized="True" name="DocumentPageKeyWords" content="True" searchable="False" id="7283501b-61d4-4fa8-833d-60f0a3911f6f" /><item tokenized="True" name="DocumentPageDescription" content="True" searchable="False" id="1047e80c-d088-422f-a613-f801cc3f841d" /><item tokenized="False" name="DocumentShowInSiteMap" content="False" searchable="True" id="16158e99-a957-48d1-82be-1cadce0fb5ff" /><item tokenized="False" name="DocumentMenuItemHideInNavigation" content="False" searchable="True" id="c5f4c74e-f620-41af-9717-0def610d3dbc" /><item tokenized="True" name="DocumentMenuCaption" content="True" searchable="False" id="b38215a9-7899-43d4-a5d0-a1a549e0e32f" /><item tokenized="False" name="DocumentMenuStyle" content="False" searchable="False" id="d010b1ac-eee2-41a6-8bfe-3041a5f5a0a3" /><item tokenized="False" name="DocumentMenuItemImage" content="False" searchable="False" id="2cf862a5-1dc3-4ae4-86b2-7ff02028761e" /><item tokenized="False" name="DocumentMenuItemLeftImage" content="False" searchable="False" id="967a760f-9fb5-45c8-bd06-7f859d5c7f36" /><item tokenized="False" name="DocumentMenuItemRightImage" content="False" searchable="False" id="878d219c-cb12-4242-a4c2-95f805fc75de" /><item tokenized="False" name="DocumentPageTemplateID" content="False" searchable="False" id="4bcd7be0-92cc-48d6-8615-d142a29393c8" /><item tokenized="False" name="DocumentMenuJavascript" content="False" searchable="False" id="6784cb70-33a1-4770-ae77-4b4915b401ab" /><item tokenized="False" name="DocumentMenuRedirectUrl" content="False" searchable="False" id="e3c0ffd7-1c03-4dc6-b6ed-63c0e1aea3af" /><item tokenized="False" name="DocumentUseNamePathForUrlPath" content="False" searchable="False" id="77812fed-fe42-4c84-9821-81992ef70251" /><item tokenized="False" name="DocumentStylesheetID" content="False" searchable="False" id="259acf2b-4aee-4878-b67c-39df586486fc" /><item tokenized="True" name="DocumentContent" content="True" searchable="False" id="08311f5c-29ca-4887-87f8-dfa1e212fd2c" /><item tokenized="False" name="DocumentMenuClass" content="False" searchable="False" id="083cea00-c550-4ca6-baa5-bfdc112bb113" /><item tokenized="False" name="DocumentMenuStyleOver" content="False" searchable="False" id="170674b3-f6dd-4791-842c-3a26861f22cf" /><item tokenized="False" name="DocumentMenuClassOver" content="False" searchable="False" id="ed42b800-9d21-4c04-bed2-55446aef51f8" /><item tokenized="False" name="DocumentMenuItemImageOver" content="False" searchable="False" id="e4041740-52b2-437e-b227-e7864e314013" /><item tokenized="False" name="DocumentMenuItemLeftImageOver" content="False" searchable="False" id="8bb09fba-17b6-417f-afda-bf30a70e948e" /><item tokenized="False" name="DocumentMenuItemRightImageOver" content="False" searchable="False" id="6545c139-6282-4bc0-86cb-8842a0cffc97" /><item tokenized="False" name="DocumentMenuStyleHighlighted" content="False" searchable="False" id="c417febd-de7b-4898-bc0e-506925ef7829" /><item tokenized="False" name="DocumentMenuClassHighlighted" content="False" searchable="False" id="94d57af6-7147-4142-a3be-b1f0196b549b" /><item tokenized="False" name="DocumentMenuItemImageHighlighted" content="False" searchable="False" id="0c5ead0b-d6dd-4f05-abae-c0421a95ce4f" /><item tokenized="False" name="DocumentMenuItemLeftImageHighlighted" content="False" searchable="False" id="c3c9217b-c1fe-4ecf-b69f-114cdcf8db44" /><item tokenized="False" name="DocumentMenuItemRightImageHighlighted" content="False" searchable="False" id="0f8cb686-4c3f-44c3-a28e-e2c8b4171147" /><item tokenized="False" name="DocumentMenuItemInactive" content="False" searchable="False" id="9c66c57e-e177-483b-9a60-3cd828b6a259" /><item tokenized="False" name="DocumentCustomData" content="False" searchable="False" id="8dc8097e-8dee-435b-98ef-98ec08608450" /><item tokenized="False" name="DocumentExtensions" content="False" searchable="False" id="c1f49ff3-1de1-4776-89cb-713baec62293" /><item tokenized="False" name="DocumentCampaign" content="False" searchable="False" id="67247266-2ca7-4838-a0bf-f1abd7eb7f46" /><item tokenized="True" name="DocumentTags" content="True" searchable="False" id="3bae0ce4-ff3a-4a26-816f-e6cf29191f75" /><item tokenized="False" name="DocumentTagGroupID" content="False" searchable="True" id="663f96d4-ee29-4abd-ae87-970f0ef7a10e" /><item tokenized="False" name="DocumentWildcardRule" content="False" searchable="False" id="f901e969-f244-4f93-bc58-3b268a3f3f59" /><item tokenized="True" name="DocumentWebParts" content="True" searchable="False" id="f241fc18-aac4-483c-bb6d-7618e38994f0" /><item tokenized="False" name="DocumentRatingValue" content="False" searchable="True" id="946294da-62ba-4697-9047-4af93fb925a3" /><item tokenized="False" name="DocumentRatings" content="False" searchable="True" id="c9d4bf12-409d-4390-8953-1bb79a9f8917" /><item tokenized="False" name="DocumentPriority" content="False" searchable="True" id="01ddcf7f-ebfd-4d9f-b333-8b6f7c88cca1" /><item tokenized="False" name="DocumentType" content="False" searchable="True" id="bce6cbbe-b19c-40ba-af45-9958e13c1548" /><item tokenized="False" name="DocumentLastPublished" content="False" searchable="True" id="b2d9ee55-180d-482e-844b-deddaee941e5" /><item tokenized="False" name="NodeID" content="False" searchable="True" id="ec1cac98-297d-45ab-953f-6b04cbfd9f30" /><item tokenized="False" name="NodeAliasPath" content="False" searchable="True" id="dd70067d-c5ee-4b19-8cf9-ffe93efcdfb5" /><item tokenized="True" name="NodeName" content="True" searchable="False" id="5c2393d5-c789-4394-963f-391566b84437" /><item tokenized="True" name="NodeAlias" content="True" searchable="False" id="22c4a7a7-9407-4287-96aa-a22cf2ee688d" /><item tokenized="False" name="NodeClassID" content="False" searchable="True" id="7ed5e5aa-3abb-4581-b85f-cd60becb5f40" /><item tokenized="False" name="NodeParentID" content="False" searchable="True" id="37ff7a6b-888e-447b-88a3-8de0e16683a2" /><item tokenized="False" name="NodeLevel" content="False" searchable="True" id="b32c80a0-4ce7-45f1-ad40-1d2ca8da0928" /><item tokenized="False" name="NodeACLID" content="False" searchable="True" id="5980e7a1-0f82-4592-8d73-a4dd53c3a41c" /><item tokenized="False" name="NodeSiteID" content="False" searchable="True" id="f106305e-271e-433d-bd66-58b6e1311aae" /><item tokenized="False" name="NodeGUID" content="False" searchable="True" id="ecc43138-c51f-4418-9e3f-6673e7214baf" /><item tokenized="False" name="NodeOrder" content="False" searchable="True" id="b47b7d44-72b8-4572-b065-5cb312791efc" /><item tokenized="False" name="IsSecuredNode" content="False" searchable="True" id="65839488-027c-4be8-9cfd-f3e0cf04f1b7" /><item tokenized="False" name="NodeCacheMinutes" content="False" searchable="True" id="8c10ee30-b045-4ad0-8408-7c214b9581fd" /><item tokenized="False" name="NodeSKUID" content="False" searchable="True" id="903e78b9-ec76-4244-bb71-977636a4af5d" /><item tokenized="False" name="NodeDocType" content="False" searchable="False" id="ade723ed-e3ad-4589-9d2a-ac358ab0c284" /><item tokenized="True" name="NodeHeadTags" content="True" searchable="False" id="41451671-38d3-4891-8f80-596144e440fa" /><item tokenized="False" name="NodeBodyElementAttributes" content="False" searchable="False" id="86db0549-5255-428e-b2ab-18ab698054fa" /><item tokenized="False" name="NodeInheritPageLevels" content="False" searchable="False" id="8d0423a8-de70-473d-a891-79c06cf55aa4" /><item tokenized="False" name="NodeChildNodesCount" content="False" searchable="True" id="12941a54-dfca-4ca8-9817-5ae262257cde" /><item tokenized="False" name="RequiresSSL" content="False" searchable="True" id="3945de87-09d9-427a-9e5e-c6cdf5d50e31" /><item tokenized="False" name="NodeLinkedNodeID" content="False" searchable="True" id="014b13fe-73ec-4f23-b725-8d7e88e390c1" /><item tokenized="False" name="NodeOwner" content="False" searchable="True" id="3f34ad81-e521-4dbf-ac4e-a26dfd589ea5" /><item tokenized="True" name="NodeCustomData" content="True" searchable="False" id="be6985b5-ce54-410f-89e5-0f09b876eb53" /><item tokenized="False" name="NodeGroupID" content="False" searchable="True" id="ed4e14b3-3fc0-47c9-acd5-b781758667f8" /></search>', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Class', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ClassID" fieldcaption="ClassID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="0baf1038-97e3-4e65-880c-35da63fee40b" /><field column="ClassDisplayName" fieldcaption="ClassDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ff01ffa6-bc9a-4760-860e-90e2316256ca" /><field column="ClassName" fieldcaption="ClassName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7797c91f-73b9-4100-b490-34207538c356" /><field column="ClassUsesVersioning" fieldcaption="ClassUsesVersioning" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ba0272bd-4933-470d-880f-c3f7c6b98eb7" /><field column="ClassIsDocumentType" fieldcaption="ClassIsDocumentType" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5525a6c9-933d-4bc5-b659-070ecbd8bb9e" /><field column="ClassIsCoupledClass" fieldcaption="ClassIsCoupledClass" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6b5e4c5e-9d7c-44f4-a5d8-b22334cb9f95" /><field column="ClassXmlSchema" fieldcaption="ClassXmlSchema" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3db53773-1efb-444e-9a4f-7366788b5564" /><field column="ClassFormDefinition" fieldcaption="ClassFormDefinition" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8d9fbca4-3b93-4053-bae2-0cb2fa107e2b" /><field column="ClassEditingPageUrl" fieldcaption="ClassEditingPageUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d839ce92-981f-4ff3-86c7-f9018fa540a4" /><field column="ClassListPageUrl" fieldcaption="ClassListPageUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e07d265c-31fd-406f-9541-16cac0262eaf" /><field column="ClassNodeNameSource" fieldcaption="ClassNodeNameSource" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f629c749-07df-4771-86b1-c6cc01a5dd05" /><field column="ClassTableName" fieldcaption="ClassTableName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d760ba06-04f3-4e95-9b0b-28d4ce696946" /><field column="ClassViewPageUrl" fieldcaption="ClassViewPageUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="79a6670e-2b88-4bdb-9091-5a7a4f458a18" /><field column="ClassPreviewPageUrl" fieldcaption="ClassPreviewPageUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6857dea5-dbef-4dd1-88d5-6ba7f22b06b9" /><field column="ClassFormLayout" fieldcaption="ClassFormLayout" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="eb01e0b3-1b93-4d91-93f8-54d8f6f91b15" /><field column="ClassNewPageUrl" fieldcaption="ClassNewPageUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="066a9293-b55f-47fc-9c7c-e7f7e8c27644" /><field column="ClassShowAsSystemTable" fieldcaption="ClassShowAsSystemTable" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d87cfae9-2047-4017-a690-136fd115ace2" /><field column="ClassUsePublishFromTo" fieldcaption="ClassUsePublishFromTo" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4d010adc-2aef-457b-a562-bb5bb0321769" /><field column="ClassShowTemplateSelection" fieldcaption="ClassShowTemplateSelection" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="84b93b79-ce58-44de-8893-31e7eca71bc8" /><field column="ClassSKUMappings" fieldcaption="ClassSKUMappings" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="23b8b405-f437-437f-9c02-7dce6982c0b1" /><field column="ClassIsMenuItemType" fieldcaption="ClassIsMenuItemType" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3287da27-313b-49a1-ac44-ec688c65e0ed" /><field column="ClassNodeAliasSource" fieldcaption="ClassNodeAliasSource" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="63944d9a-287a-46f5-8386-1976159b07da" /><field column="ClassDefaultPageTemplateID" fieldcaption="ClassDefaultPageTemplateID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b1532089-6b7c-4367-9e09-3e7c5911af56" /><field column="ClassSKUDefaultDepartmentID" fieldcaption="ClassSKUDefaultDepartmentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5b700356-2925-4c96-83c3-c13f961ae53f" /><field column="ClassLastModified" fieldcaption="ClassLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="77b800c3-26ee-48bb-9608-1054f69aa4b8" /><field column="ClassGUID" fieldcaption="ClassGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3d1a72e7-721a-40e2-97e6-d5fdf19507f2" /><field column="ClassCreateSKU" fieldcaption="ClassCreateSKU" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1398a9ae-7b15-4f75-9eda-554897bdae77" /><field column="ClassIsProduct" fieldcaption="ClassIsProduct" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3f8608a0-bb65-435a-afc7-3731390f52d7" /><field column="ClassLoadGeneration" visible="false" columntype="integer" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="27012dbf-cb1d-48f5-b5a6-8ea49a1cb3ae" /><field column="ClassSearchTitleColumn" fieldcaption="ClassSearchTitleColumn" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="cac829b9-641f-48dd-bcac-5c6bbb1bb6a4" visibility="none" /><field column="ClassSearchContentColumn" fieldcaption="ClassSearchContentColumn" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="633d3e3a-f961-4959-89d2-b0a9ba6a6f76" visibility="none" /><field column="ClassSearchImageColumn" fieldcaption="ClassSearchImageColumn" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="c54398e5-ee11-4188-8b4b-5133bca4e00c" visibility="none" /><field column="ClassSearchCreationDateColumn" fieldcaption="ClassSearchCreationDateColumn" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="ee76b6d7-4341-4138-b54b-75f86f063233" visibility="none" /><field column="ClassSearchSettings" fieldcaption="ClassSearchSettings" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a2cb82b7-0186-400f-99c2-795dfbb5d871" visibility="none" /></form>', '20100308 14:15:47', 145, N'cms.class', 0, 0, 0, 'd7e91104-201b-4b11-9550-e93ad9a4d81f', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Class', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Class">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ClassID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ClassDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassUsesVersioning" type="xs:boolean" />
              <xs:element name="ClassIsDocumentType" type="xs:boolean" />
              <xs:element name="ClassIsCoupledClass" type="xs:boolean" />
              <xs:element name="ClassXmlSchema">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassFormDefinition">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassEditingPageUrl">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassListPageUrl">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassNodeNameSource">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassTableName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassViewPageUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassPreviewPageUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassFormLayout" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassNewPageUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassShowAsSystemTable" type="xs:boolean" />
              <xs:element name="ClassUsePublishFromTo" type="xs:boolean" minOccurs="0" />
              <xs:element name="ClassShowTemplateSelection" type="xs:boolean" minOccurs="0" />
              <xs:element name="ClassSKUMappings" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassIsMenuItemType" type="xs:boolean" minOccurs="0" />
              <xs:element name="ClassNodeAliasSource" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassDefaultPageTemplateID" type="xs:int" minOccurs="0" />
              <xs:element name="ClassSKUDefaultDepartmentID" type="xs:int" minOccurs="0" />
              <xs:element name="ClassLastModified" type="xs:dateTime" />
              <xs:element name="ClassGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ClassCreateSKU" type="xs:boolean" minOccurs="0" />
              <xs:element name="ClassIsProduct" type="xs:boolean" minOccurs="0" />
              <xs:element name="ClassIsCustomTable" type="xs:boolean" />
              <xs:element name="ClassShowColumns" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1000" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassLoadGeneration" type="xs:int" />
              <xs:element name="ClassSearchTitleColumn" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassSearchContentColumn" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassSearchImageColumn" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassSearchCreationDateColumn" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ClassSearchSettings" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Class" />
      <xs:field xpath="ClassID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'PageTemplate', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="PageTemplateID" fieldcaption="PageTemplateID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="1e965561-7a54-4269-bbd3-4784c10e33b8" /><field column="PageTemplateDisplayName" fieldcaption="PageTemplateDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="62991232-e9a8-494d-b31e-6be68f6610b0" /><field column="PageTemplateCodeName" fieldcaption="PageTemplateCodeName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a0e4fcc6-0885-4d0c-a311-679a0b2bd0bb" /><field column="PageTemplateDescription" fieldcaption="PageTemplateDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0a71b845-085d-4b4b-a736-242db5a8d7bc" /><field column="PageTemplateFile" fieldcaption="PageTemplateFile" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5abb7834-f39e-44e2-a6b2-3ae019817544" /><field column="PageTemplateIsPortal" fieldcaption="PageTemplateIsPortal" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="59ed01c6-4b29-439e-8519-593f886be9bb" /><field column="PageTemplateCategoryID" fieldcaption="PageTemplateCategoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="60d03ec1-54ec-4531-a401-d8cf50ffd962" /><field column="PageTemplateLayoutID" fieldcaption="PageTemplateLayoutID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="07361311-11ba-43e0-aff0-5c7bc261967b" /><field column="PageTemplateWebParts" fieldcaption="PageTemplateWebParts" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9ac0e7d2-2166-440d-b7db-2c93b6671d46" /><field column="PageTemplateIsReusable" fieldcaption="PageTemplateIsReusable" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="69c10c7c-0ec3-48ba-9237-78f4fb1e36d9" /><field column="PageTemplateShowAsMasterTemplate" fieldcaption="PageTemplateShowAsMasterTemplate" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c38d560c-db21-4758-b025-04ddfb1d4c7e" /><field column="PageTemplateInheritPageLevels" fieldcaption="PageTemplateInheritPageLevels" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="db56a561-db7e-4fc7-899e-82fe7a079ad5" /><field column="PageTemplateLayout" fieldcaption="PageTemplateLayout" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fef00776-f430-4164-9d1a-cf4bba1f3ecd" /><field column="PageTemplateLayoutCheckedOutFileName" fieldcaption="PageTemplateLayoutCheckedOutFileName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="82fbfd7b-2147-43b7-828a-514fa99404d5" /><field column="PageTemplateLayoutCheckedOutByUserID" fieldcaption="PageTemplateLayoutCheckedOutByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6936e715-d20a-4f0e-9659-c98737a045b7" /><field column="PageTemplateLayoutCheckedOutMachineName" fieldcaption="PageTemplateLayoutCheckedOutMachineName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="31b1701f-8745-4100-91eb-5e7935ef9512" /><field column="PageTemplateVersionGUID" fieldcaption="PageTemplateVersionGUID" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b42bc101-e5a5-4293-9e1e-ecdbd7159c44" /><field column="PageTemplateHeader" fieldcaption="PageTemplateHeader" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="44e13d8a-452b-4e4b-b535-5573a07876c9" /><field column="PageTemplateGUID" fieldcaption="PageTemplateGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="46bb92aa-a912-46b3-a858-b35b05e4fd9b" /><field column="PageTemplateLastModified" fieldcaption="PageTemplateLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="343c268d-28f9-428f-88fb-38f1f8d0ed39" /><field column="PageTemplateSiteID" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="352a6057-db2b-463f-82b6-fe29c94e3dde" /><field column="PageTemplateForAllPages" visible="false" defaultvalue="true" columntype="boolean" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="69097c75-42f7-43ba-b841-df725d5b2035" visibility="none" ismacro="false" /></form>', '20100303 14:34:17', 157, N'cms.pagetemplate', 0, 0, 0, '8bb71cc8-1fcb-4073-b127-0e3574ecc207', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_PageTemplate', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_PageTemplate">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="PageTemplateID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="PageTemplateDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateCodeName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateFile">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateIsPortal" type="xs:boolean" minOccurs="0" />
              <xs:element name="PageTemplateCategoryID" type="xs:int" minOccurs="0" />
              <xs:element name="PageTemplateLayoutID" type="xs:int" minOccurs="0" />
              <xs:element name="PageTemplateWebParts" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateIsReusable" type="xs:boolean" minOccurs="0" />
              <xs:element name="PageTemplateShowAsMasterTemplate" type="xs:boolean" minOccurs="0" />
              <xs:element name="PageTemplateInheritPageLevels" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateLayout" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateLayoutCheckedOutFileName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateLayoutCheckedOutByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="PageTemplateLayoutCheckedOutMachineName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateVersionGUID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateHeader" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="PageTemplateLastModified" type="xs:dateTime" />
              <xs:element name="PageTemplateSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="PageTemplateForAllPages" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_PageTemplate" />
      <xs:field xpath="PageTemplateID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Query', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="QueryID" fieldcaption="QueryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="5c3db3dc-6a01-4d2a-9b74-24b79704a82b" /><field column="QueryName" fieldcaption="QueryName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ad1056d3-34b8-41b3-b31d-bdf116f010ec" /><field column="QueryTypeID" fieldcaption="QueryTypeID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b96bf312-8b7c-42d2-b0b2-a395e10ccfd4" /><field column="QueryText" fieldcaption="QueryText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bcfc8502-008d-4d21-aac1-63810e7d563c" /><field column="QueryRequiresTransaction" fieldcaption="QueryRequiresTransaction" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5cc03d8e-017e-4a5c-bb1e-5f7c42290ab7" /><field column="ClassID" fieldcaption="ClassID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="34bfc28c-0e6a-497b-a761-eb00dd435ae3" /><field column="QueryIsLocked" fieldcaption="QueryIsLocked" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="154d5cd0-615c-4f4f-b158-434357d93a8f" /><field column="QueryLastModified" fieldcaption="QueryLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8763542e-3500-4ae8-8fc7-07cd5a116f06" /><field column="QueryGUID" fieldcaption="QueryGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6f3b0835-98e0-49f8-a972-74e39d7d91d8" /><field column="QueryLoadGeneration" visible="false" columntype="integer" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3505f469-2896-4606-903e-b7bdc7766a89" /><field column="QueryIsCustom" fieldcaption="QueryIsCustom" visible="true" defaultvalue="false" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="be70ac52-0624-49e5-ae56-d9f95f3ddf56" /></form>', '20090102 14:02:46', 161, N'cms.query', 0, 0, 0, '821c115d-0b5b-4d8a-b5f9-7d2e0f97e0bd', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_Query', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Query">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="QueryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="QueryName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="QueryTypeID" type="xs:int" />
              <xs:element name="QueryText">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="QueryRequiresTransaction" type="xs:boolean" />
              <xs:element name="ClassID" type="xs:int" />
              <xs:element name="QueryIsLocked" type="xs:boolean" />
              <xs:element name="QueryLastModified" type="xs:dateTime" />
              <xs:element name="QueryGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="QueryLoadGeneration" type="xs:int" />
              <xs:element name="QueryIsCustom" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Query" />
      <xs:field xpath="QueryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Transformation', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TransformationID" fieldcaption="TransformationID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="7752c3fc-e7d8-4e90-83ae-9b2601bb56d1" /><field column="TransformationName" fieldcaption="TransformationName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="44b9c824-dddc-41b1-b0aa-3c5b68ae7198" /><field column="TransformationCode" fieldcaption="TransformationCode" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="972bd3c6-7b24-4299-a9df-80f6922f3768" /><field column="TransformationType" fieldcaption="TransformationType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="30f88c23-bd10-438d-9f8f-f37bd120421b" /><field column="TransformationClassID" fieldcaption="TransformationClassID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ea9dfc80-cacc-43b9-acd3-eaa90b5dccea" /><field column="TransformationCheckedOutByUserID" fieldcaption="TransformationCheckedOutByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f8e41597-f0e1-4b83-81c9-ae6345367bb4" /><field column="TransformationCheckedOutMachineName" fieldcaption="TransformationCheckedOutMachineName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3bcc00c6-6753-49f1-82dd-a36e6a5e1920" /><field column="TransformationCheckedOutFilename" fieldcaption="TransformationCheckedOutFilename" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="575cebea-f64b-4e54-8230-7e4866387100" /><field column="TransformationVersionGUID" fieldcaption="TransformationVersionGUID" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="45689f6f-0ba9-4d34-858c-75a1ff74c2f3" /><field column="TransformationGUID" fieldcaption="TransformationGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cd6460eb-38a9-4103-9735-83cb401660ab" /><field column="TransformationLastModified" fieldcaption="TransformationLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a7a37222-a815-48f2-888d-a15bbb51b536" /></form>', '20081229 09:07:39', 162, N'cms.transformation', 0, 0, 0, '719c71f8-4dcd-4ab5-8d4e-84e6a60fe7be', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_Transformation', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Transformation">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TransformationID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="TransformationName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TransformationCode">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TransformationType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TransformationClassID" type="xs:int" />
              <xs:element name="TransformationCheckedOutByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="TransformationCheckedOutMachineName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TransformationCheckedOutFilename" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TransformationVersionGUID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TransformationGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="TransformationLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Transformation" />
      <xs:field xpath="TransformationID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Workflow', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="WorkflowID" fieldcaption="WorkflowID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="bf3f5149-ebe0-4c41-8cde-79a0e63bdb66" /><field column="WorkflowDisplayName" fieldcaption="WorkflowDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1493eba2-b913-4a2f-af71-9df4ebcb6f51" /><field column="WorkflowName" fieldcaption="WorkflowName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c0821ace-fa5b-4e08-b9eb-32783abd66c9" /><field column="WorkflowGUID" fieldcaption="WorkflowGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a7be7df7-232f-4cf4-8446-09df882727bc" /><field column="WorkflowLastModified" fieldcaption="WorkflowLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="83b95ad1-000c-4e16-9177-71b438a75d3c" /></form>', '20100304 13:13:00', 171, N'cms.workflow', 0, 0, 0, 'a80047fb-e386-48ea-b433-bcdd92d131e4', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Workflow', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Workflow">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="WorkflowID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="WorkflowDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WorkflowName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WorkflowGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="WorkflowLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Workflow" />
      <xs:field xpath="WorkflowID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Workflow step', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="StepID" fieldcaption="StepID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="dc2613e4-432b-40b2-b6ae-2b0e883309a2" /><field column="StepDisplayName" fieldcaption="StepDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="78ff440b-cf76-4421-b641-c3732ea35f61" /><field column="StepName" fieldcaption="StepName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="db5c64f9-63a6-42d7-84ad-3bb20f78d72b" /><field column="StepOrder" fieldcaption="StepOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f12564cc-48d4-45c2-ad20-e6e3379a5710" /><field column="StepWorkflowID" fieldcaption="StepWorkflowID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a13c24d9-9013-42aa-966d-02121b44c37c" /><field column="StepGUID" fieldcaption="StepGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ce724361-4974-4f98-bba8-123c2b66bdb2" /><field column="StepLastModified" fieldcaption="StepLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f31371a3-cf3c-40c5-a0bb-b1ecdd98921f" /></form>', '20100506 09:47:14', 172, N'cms.workflowstep', 0, 0, 0, '6fc9d49b-83c2-4a7e-9a33-037883a76a26', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_WorkflowStep', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WorkflowStep">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="StepID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="StepDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StepName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StepOrder" type="xs:int" />
              <xs:element name="StepWorkflowID" type="xs:int" />
              <xs:element name="StepGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="StepLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WorkflowStep" />
      <xs:field xpath="StepID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Workflow scope', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ScopeID" fieldcaption="ScopeID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="80348be1-c666-4a97-9d48-1d46bb49d061" /><field column="ScopeStartingPath" fieldcaption="ScopeStartingPath" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="e1bfc63e-7081-4b80-acc6-993bcc17becb" /><field column="ScopeWorkflowID" fieldcaption="ScopeWorkflowID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d53bd2fe-2023-4098-9527-fd6d13fc8cff" /><field column="ScopeClassID" fieldcaption="ScopeClassID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="86559d2f-0ac6-4714-adb6-e1b2ce4cae90" /><field column="ScopeSiteID" fieldcaption="ScopeSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="314be952-3e36-4868-b10b-b1026cb021cc" /><field column="ScopeGUID" fieldcaption="ScopeGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2b198dac-7fd9-46b5-bfff-e8c087bbbec2" /><field column="ScopeLastModified" fieldcaption="ScopeLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="42da4cc7-0319-486d-84ab-0cc0aecab0ad" /><field column="ScopeCultureID" fieldcaption="ScopeCultureID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3fd1415f-d6c5-4305-8c67-5c2cc778768a" /></form>', '20090415 18:09:55', 175, N'cms.workflowscope', 0, 0, 0, '8cba8304-c75f-45ce-8f39-7f363cf5892a', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_WorkflowScope', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WorkflowScope">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ScopeID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ScopeStartingPath">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ScopeWorkflowID" type="xs:int" />
              <xs:element name="ScopeClassID" type="xs:int" minOccurs="0" />
              <xs:element name="ScopeSiteID" type="xs:int" />
              <xs:element name="ScopeGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ScopeLastModified" type="xs:dateTime" />
              <xs:element name="ScopeCultureID" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WorkflowScope" />
      <xs:field xpath="ScopeID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Version history', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="VersionHistoryID" fieldcaption="VersionHistoryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="032b5d2d-3e97-44da-a573-109f8a26c440" /><field column="NodeSiteID" fieldcaption="NodeSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="29aee6c7-b6a2-42d8-8180-d9947a54e404" /><field column="DocumentID" fieldcaption="DocumentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="90d894a9-003b-440a-9b99-96d3727a0fed" /><field column="DocumentNamePath" fieldcaption="DocumentNamePath" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d4324f6f-cb8c-4d4e-8f7d-aebc5b0e355c" /><field column="NodeXML" fieldcaption="NodeXML" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e2f43580-d08f-41ca-8ed5-2f13eba66a8a" /><field column="ModifiedByUserID" fieldcaption="ModifiedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a343763c-16aa-4384-bec9-2cea568b40a7" /><field column="ModifiedWhen" fieldcaption="ModifiedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f42040c2-ecb6-444d-962b-37bf6f5353f8" /><field column="VersionNumber" fieldcaption="VersionNumber" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="00fa9c9f-a418-4c97-be43-281dd1ad2d12" /><field column="VersionComment" fieldcaption="VersionComment" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d8ebc7c7-7d2a-4a52-8fff-168c1ac1922e" /><field column="ToBePublished" fieldcaption="ToBePublished" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="49ac547e-9ac5-4122-afc4-446dfdea5005" /><field column="PublishFrom" fieldcaption="PublishFrom" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6e4ae6e1-7e3d-43ce-9087-37737207429d" /><field column="PublishTo" fieldcaption="PublishTo" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2c7714af-1a7e-4e94-9c1e-164579ae7921" /><field column="WasPublishedFrom" fieldcaption="WasPublishedFrom" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cf5302b2-d7c4-4597-a79a-f383b16615d5" /><field column="WasPublishedTo" fieldcaption="WasPublishedTo" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0cfbd232-721f-47b6-b9ce-64d7dda4c8b6" /></form>', '20100303 10:23:25', 214, N'cms.versionhistory', 0, 0, 0, '41fd4469-5173-4b22-b89d-5fb5d2e1c5fb', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_VersionHistory', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_VersionHistory">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="VersionHistoryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="NodeSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentID" type="xs:int" minOccurs="0" />
              <xs:element name="DocumentNamePath">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NodeXML">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ModifiedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="ModifiedWhen" type="xs:dateTime" />
              <xs:element name="VersionNumber" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="VersionComment" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ToBePublished" type="xs:boolean" />
              <xs:element name="PublishFrom" type="xs:dateTime" minOccurs="0" />
              <xs:element name="PublishTo" type="xs:dateTime" minOccurs="0" />
              <xs:element name="WasPublishedFrom" type="xs:dateTime" minOccurs="0" />
              <xs:element name="WasPublishedTo" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_VersionHistory" />
      <xs:field xpath="VersionHistoryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'ACL item', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ACLItemID" fieldcaption="ACLItemID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="fc46d68f-94de-4f63-bc5d-bee495e2afe9" /><field column="ACLID" fieldcaption="ACLID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="368d7b03-c6a7-4684-8aa5-5e03aab24de9" /><field column="UserID" fieldcaption="UserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3a51b2be-1ba4-42fd-b15d-70371c2cf9fb" /><field column="RoleID" fieldcaption="RoleID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9b98e19f-e935-438a-9269-47abb95830f5" /><field column="Allowed" fieldcaption="Allowed" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ca5e7c3c-65f6-4e90-9b99-5bae5f769b16" /><field column="Denied" fieldcaption="Denied" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="87100fd6-a32f-4097-b857-696e8d2ddc1b" /><field column="LastModified" fieldcaption="LastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8247705e-9a5a-4aed-a914-61c662306a2b" /><field column="LastModifiedByUserID" fieldcaption="LastModifiedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2b3c3129-984b-4528-a639-f1703401961f" /></form>', '20091110 13:10:24', 225, N'cms.aclitem', 0, 0, 0, '83fdf79a-9d56-474f-b229-115fccabf042', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_ACLItem', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_ACLItem">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ACLItemID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ACLID" type="xs:int" />
              <xs:element name="UserID" type="xs:int" minOccurs="0" />
              <xs:element name="RoleID" type="xs:int" minOccurs="0" />
              <xs:element name="Allowed" type="xs:int" />
              <xs:element name="Denied" type="xs:int" />
              <xs:element name="LastModified" type="xs:dateTime" />
              <xs:element name="LastModifiedByUserID" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_ACLItem" />
      <xs:field xpath="ACLItemID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
set identity_insert [cms_class] off
