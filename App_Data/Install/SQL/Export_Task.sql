CREATE TABLE [Export_Task] (
		[TaskID]             int NOT NULL IDENTITY(1, 1),
		[TaskSiteID]         int NULL,
		[TaskTitle]          nvarchar(450) NOT NULL,
		[TaskData]           nvarchar(max) NOT NULL,
		[TaskTime]           datetime NOT NULL,
		[TaskType]           nvarchar(50) NOT NULL,
		[TaskObjectType]     nvarchar(100) NULL,
		[TaskObjectID]       int NULL
)
ON [PRIMARY]
ALTER TABLE [Export_Task]
	ADD
	CONSTRAINT [PK_Export_Task]
	PRIMARY KEY
	([TaskID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Export_Task]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Export_Task_TaskSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Export_Task]
			ADD CONSTRAINT [FK_Export_Task_TaskSiteID_CMS_Site]
			FOREIGN KEY ([TaskSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE INDEX [IX_Export_Task_TaskSiteID_TaskObjectType]
	ON [Export_Task] ([TaskSiteID], [TaskObjectType])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
