using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Newsletter;
using CMS.Scheduler;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_New : CMSNewsletterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set title
        this.CurrentMaster.Title.HelpTopicName = "new_newsletter";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        rfvNewsletterDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvNewsletterName.ErrorMessage = ResHelper.GetString("Newsletter_Edit.ErrorEmptyName");
        rfvNewsletterSenderName.ErrorMessage = ResHelper.GetString("Newsletter_Edit.ErrorEmptySenderName");
        rfvNewsletterSenderEmail.ErrorMessage = ResHelper.GetString("Newsletter_Edit.ErrorEmptySenderEmail");

        lblNewsletterName.Text = ResHelper.GetString("Newsletter_Edit.NewsletterNameLabel");
        lblNewsletterDisplayName.Text = ResHelper.GetString("Newsletter_Edit.NewsletterDisplayNameLabel");
        lblNewsletterSenderName.Text = ResHelper.GetString("Newsletter_Edit.NewsletterSenderNameLabel");
        lblNewsletterDynamicURL.Text = ResHelper.GetString("Newsletter_Edit.NewsletterDynamicURLLabel");
        lblNewsletterSenderEmail.Text = ResHelper.GetString("Newsletter_Edit.NewsletterSenderEmailLabel");
        lblSubscriptionTemplate.Text = ResHelper.GetString("Newsletter_Edit.SubscriptionTemplate");
        lblUnsubscriptionTemplate.Text = ResHelper.GetString("Newsletter_Edit.UnsubscriptionTemplate");
        lblIssueTemplate.Text = ResHelper.GetString("Newsletter_Edit.NewsletterTemplate");
        lblNewsletterDynamicURL.Text = ResHelper.GetString("Newsletter_Edit.SourcePageURL");
        lblSchedule.Text = ResHelper.GetString("Newsletter_Edit.Schedule");
        radDynamic.Text = ResHelper.GetString("Newsletter_Edit.Dynamic");
        radTemplateBased.Text = ResHelper.GetString("Newsletter_Edit.TemplateBased");
        btnOk.Text = ResHelper.GetString("General.OK");

        string currentNewsletter = ResHelper.GetString("Newsletter_Edit.NewItemCaption");

        // initializes page title control		
        string[,] tabs = new string[2, 3];
        tabs[0, 0] = ResHelper.GetString("Newsletter_Edit.ItemListLink");
        tabs[0, 1] = "~/CMSModules/Newsletters/Tools/Newsletters/Newsletter_List.aspx";
        tabs[0, 2] = "newslettersContent";
        tabs[1, 0] = currentNewsletter;
        tabs[1, 1] = "";
        tabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = tabs;

        if (!RequestHelper.IsPostBack())
        {
            radDynamic.Checked = false;
            radTemplateBased.Checked = true;
            SetTypeToTemplateBased();
        }

        int siteId = CMSContext.CurrentSiteID;
        subscriptionTemplate.WhereCondition = "TemplateType='S' AND TemplateSiteID=" + siteId;
        unsubscriptionTemplate.WhereCondition = "TemplateType='U' AND TemplateSiteID=" + siteId;
        issueTemplate.WhereCondition = "TemplateType='I' AND TemplateSiteID=" + siteId;
    }


    protected void SetTypeToTemplateBased()
    {
        issueTemplate.Enabled = true;
        txtNewsletterDynamicURL.Enabled = false;
        lblSchedule.Visible = false;
        plcInterval.Visible = false;
    }


    protected void SetTypeToDynamic()
    {
        issueTemplate.Enabled = false;
        txtNewsletterDynamicURL.Enabled = true;
        lblSchedule.Visible = true;
        plcInterval.Visible = true;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check 'configure' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "configure"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "configure");
        }

        string errorMessage = new Validator().NotEmpty(txtNewsletterDisplayName.Text, ResHelper.GetString("general.requiresdisplayname")).NotEmpty(txtNewsletterName.Text, ResHelper.GetString("Newsletter_Edit.ErrorEmptyName")).NotEmpty(txtNewsletterSenderName.Text, ResHelper.GetString("Newsletter_Edit.ErrorEmptySenderName")).NotEmpty(txtNewsletterSenderEmail.Text, ResHelper.GetString("Newsletter_Edit.ErrorEmptySenderEmail")).IsEmail(txtNewsletterSenderEmail.Text.Trim(), ResHelper.GetString("Newsletter_Edit.ErrorEmailFormat")).IsCodeName(txtNewsletterName.Text, ResHelper.GetString("general.invalidcodename")).Result;

        if (errorMessage == "")
        {
            if (String.IsNullOrEmpty((string)subscriptionTemplate.Value))
            {
                lblSubscriptionError.Visible = true;
                return;
            }

            if (String.IsNullOrEmpty((string)unsubscriptionTemplate.Value))
            {
                lblUnsubscriptionError.Visible = true;
                return;
            }

            if (String.IsNullOrEmpty((string)issueTemplate.Value))
            {
                lblIssueError.Visible = true;
                return;
            }

            if ((radDynamic.Checked) && (!ScheduleInterval1.CheckOneDayMinimum()))
            {
                //if problem occurred while setting schedule interval
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Newsletter_Edit.NoDaySelected");
                return;
            }
            // newsletterName must to be unique
            Newsletter newsletterObj = NewsletterProvider.GetNewsletter(txtNewsletterName.Text.Trim(), CMSContext.CurrentSite.SiteID);

            // if newsletterName value is unique														
            if (newsletterObj == null)
            {
                // create new item -> insert
                newsletterObj = new Newsletter();

                newsletterObj.NewsletterDisplayName = txtNewsletterDisplayName.Text.Trim();
                newsletterObj.NewsletterName = txtNewsletterName.Text.Trim();

                int selectedValue = ValidationHelper.GetInteger(subscriptionTemplate.Value, 0);
                if (selectedValue != 0)
                {
                    newsletterObj.NewsletterSubscriptionTemplateID = selectedValue;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Newsletter_Edit.NoSubscriptionTemplateSelected");
                    return;
                }

                selectedValue =  ValidationHelper.GetInteger(unsubscriptionTemplate.Value, 0);
                if (selectedValue != 0)
                {
                    newsletterObj.NewsletterUnsubscriptionTemplateID = selectedValue;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Newsletter_Edit.NoUnsubscriptionTemplateSelected");
                    return;
                }
                newsletterObj.NewsletterSenderName = txtNewsletterSenderName.Text.Trim();
                newsletterObj.NewsletterSenderEmail = txtNewsletterSenderEmail.Text.Trim();
                if (radDynamic.Checked)
                {
                    newsletterObj.NewsletterDynamicURL = txtNewsletterDynamicURL.Text.Trim();
                    newsletterObj.NewsletterType = "D";
                }
                else
                {
                    selectedValue = ValidationHelper.GetInteger(issueTemplate.Value, 0);
                    if (selectedValue != 0)
                    {
                        newsletterObj.NewsletterTemplateID = selectedValue;
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("Newsletter_Edit.NoEmailTemplateSelected");
                        return;
                    }
                    newsletterObj.NewsletterType = "T";
                }
                newsletterObj.NewsletterSiteID = CMS.CMSHelper.CMSContext.CurrentSite.SiteID;
                newsletterObj.NewsletterGUID = Guid.NewGuid();

                if (NewsletterProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Newsletters, VersionActionEnum.Insert))
                {
                    //create new newsletter
                    NewsletterProvider.SetNewsletter(newsletterObj);

                    if (radDynamic.Checked)
                    {
                        if (chkSchedule.Checked)
                        {
                            //Create new task to schedule the dynamic newsletter
                            TaskInfo task = new TaskInfo();
                            task.TaskAssemblyName = "CMS.Newsletter";
                            task.TaskClass = "CMS.Newsletter.DynamicNewsletterSender";
                            task.TaskDisplayName = ResHelper.GetString("DynamicNewsletter.TaskName") + newsletterObj.NewsletterDisplayName;
                            task.TaskEnabled = true;
                            task.TaskInterval = ScheduleInterval1.ScheduleInterval;
                            task.TaskLastResult = "";
                            task.TaskName = "DynamicNewsletter." + ValidationHelper.GetCodeName(newsletterObj.NewsletterName, "_");
                            task.TaskSiteID = CMSContext.CurrentSite.SiteID;
                            task.TaskNextRunTime = SchedulingHelper.GetNextTime(task.TaskInterval, new DateTime(), new DateTime());
                            task.TaskData = newsletterObj.NewsletterID.ToString();

                            TaskInfoProvider.SetTaskInfo(task);

                            newsletterObj.NewsletterDynamicScheduledTaskID = task.TaskID;
                            NewsletterProvider.SetNewsletter(newsletterObj);
                        }
                    }
                    UrlHelper.Redirect("Newsletter_Frameset.aspx?newsletterid=" + ValidationHelper.GetString(newsletterObj.NewsletterID, null) + "&saved=1");
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("LicenseVersionCheck.Newsletter");
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Newsletter_Edit.NewsletterNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    protected void radTemplateBased_CheckedChanged(object sender, EventArgs e)
    {
        if (radDynamic.Checked)
        {
            SetTypeToDynamic();
        }
        else
        {
            SetTypeToTemplateBased();
        }
    }


    protected void radDynamic_CheckedChanged(object sender, EventArgs e)
    {
        if (radDynamic.Checked)
        {
            SetTypeToDynamic();
        }
        else
        {
            SetTypeToTemplateBased();
        }
    }


    protected void chkSchedule_CheckedChanged(object sender, EventArgs e)
    {
        if (chkSchedule.Checked)
        {
            ScheduleInterval1.Visible = true;
        }
        else
        {
            ScheduleInterval1.Visible = false;
        }
    }
}
