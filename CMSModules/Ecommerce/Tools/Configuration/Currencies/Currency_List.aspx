<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Currency_List.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Configuration_Currencies_Currency_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Currency - list" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="gridElem" GridName="Currency_List.xml" OrderBy="CurrencyDisplayName"
        IsLiveSite="false" Columns="CurrencyID,CurrencyDisplayName,CurrencyCode,CurrencyEnabled,CurrencyIsMain" />
</asp:Content>
