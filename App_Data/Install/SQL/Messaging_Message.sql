CREATE TABLE [Messaging_Message] (
		[MessageID]                    int NOT NULL IDENTITY(1, 1),
		[MessageSenderUserID]          int NULL,
		[MessageSenderNickName]        nvarchar(200) NULL,
		[MessageRecipientUserID]       int NULL,
		[MessageRecipientNickName]     nvarchar(200) NULL,
		[MessageSent]                  datetime NOT NULL,
		[MessageSubject]               nvarchar(200) NULL,
		[MessageBody]                  nvarchar(max) NOT NULL,
		[MessageRead]                  datetime NULL,
		[MessageSenderDeleted]         bit NULL,
		[MessageRecipientDeleted]      bit NULL,
		[MessageGUID]                  uniqueidentifier NOT NULL,
		[MessageLastModified]          datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Messaging_Message]
	ADD
	CONSTRAINT [PK_Messaging_Message]
	PRIMARY KEY
	NONCLUSTERED
	([MessageID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Messaging_Message]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Messaging_Message_MessageRecipientUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Messaging_Message]
			ADD CONSTRAINT [FK_Messaging_Message_MessageRecipientUserID_CMS_User]
			FOREIGN KEY ([MessageRecipientUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Messaging_Message]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Messaging_Message_MessageSenderUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Messaging_Message]
			ADD CONSTRAINT [FK_Messaging_Message_MessageSenderUserID_CMS_User]
			FOREIGN KEY ([MessageSenderUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE CLUSTERED INDEX [IX_Messaging_Message_MessageRecipientUserID_MessageSent_MessageRecipientDeleted]
	ON [Messaging_Message] ([MessageRecipientUserID], [MessageSent] DESC, [MessageRecipientDeleted])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Messaging_Message_MessageSenderUserID_MessageSent_MessageSenderDeleted]
	ON [Messaging_Message] ([MessageSenderUserID], [MessageSent], [MessageSenderDeleted])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
