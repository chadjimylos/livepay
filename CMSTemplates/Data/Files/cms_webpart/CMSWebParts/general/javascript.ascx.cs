using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.Controls;
using CMS.CMSHelper;
using CMS.GlobalHelper;

public partial class CMSWebParts_General_javascript : CMSAbstractWebPart
{
    private string mInlineScript = "";
    private string mLinkedFile = "";

    #region "Public properties"

    /// <summary>
    /// Gets or sets the inline JavaScript code
    /// </summary>
    public string InlineScript
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("InlineScript"), mInlineScript);
        }
        set
        {
            this.SetValue("InlineScript", value);
            this.mInlineScript = value;
        }
    }


    /// <summary>
    /// Gets or sets the linked file url
    /// </summary>
    public string LinkedFile
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("LinkedFile"), mLinkedFile);
        }
        set
        {
            this.SetValue("LinkedFile", value);
            this.mLinkedFile = value;
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data for partial caching
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            // Set up the properties
            this.mInlineScript = this.InlineScript;
            this.mLinkedFile = this.LinkedFile;

            // Render the inline script
            if (InlineScript.Trim() != "")
            {
                ltlInlineScript.Text = ScriptHelper.GetScript(InlineScript);
            }

            // Create linked js file
            if (LinkedFile.Trim() != "")
            {
                string linkedFile = string.Format("<script type=\"text/javascript\" src=\"{0}\" ></script>", ResolveUrl(LinkedFile));
                this.Page.Header.Controls.Add(new LiteralControl(linkedFile));
            }
        }
    }
}
