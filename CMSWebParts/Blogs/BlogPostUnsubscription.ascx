<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebparts/Blogs/BlogPostUnsubscription.ascx.cs"
    Inherits="CMSWebParts_Blogs_BlogPostUnsubscription" %>

<cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<cms:LocalizedLabel runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
    Visible="false" />