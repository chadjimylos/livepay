<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Versions.aspx.cs" Inherits="CMSModules_Content_CMSDesk_Properties_Versions"
    Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Properties - Versions</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
        .DestroyHistory
        {
        	vertical-align: bottom;
        	text-align: right;
        }
        .RTL .DestroyHistory
        {
        	text-align: left;
        }
    </style>

    <script type="text/javascript">
        //<![CDATA[
        // Displays the given version
        function ViewVersion(versionHistoryId) {
            // Display the dialog
            window.open('ViewVersion.aspx?versionHistoryId=' + versionHistoryId);
        }

        function RefreshTree(expandNodeId, selectNodeId) {
            // Update tree
            parent.RefreshTree(expandNodeId, selectNodeId);
        }
        //]]>
    </script>

</head>
<body class="VerticalTabsBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
    <asp:Panel runat="server" ID="pnlBody" CssClass="VerticalTabsPageBody">
        <asp:Panel ID="pnlMenu" runat="server" CssClass="ContentEditMenu">
            <table width="100%">
                <tr>
                    <td>
                        <div style="height: 24px; padding: 5px;">
                        </div>
                    </td>
                    <td class="TextRight">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
            <asp:Label runat="server" ID="lblError" ForeColor="red" EnableViewState="false" />
            <asp:Label runat="server" ID="lblApprove" Visible="false" EnableViewState="false" />
            <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false" />
            <asp:Panel runat="server" ID="pnlVersions">
                <div style="width: 100%">
                    <table width="100%">
                        <asp:PlaceHolder ID="plcForm" runat="server">
                            <tr>
                                <td colspan="2">
                                    <asp:Label ID="lblCheckInfo" runat="server" Font-Bold="true" EnableViewState="false" />
                                    <asp:Panel runat="server" ID="pnlForm">
                                        <table>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <cms:LocalizedLabel ID="lblVersion" runat="server" ResourceString="VersionsProperties.Version" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtVersion" runat="server" CssClass="TextBoxField" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <cms:LocalizedLabel ID="lblComment" runat="server" ResourceString="VersionsProperties.Comment"
                                                        EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" CssClass="TextAreaField" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                    <cms:LocalizedButton ID="btnCheckout" runat="server" CssClass="SubmitButton" Visible="false"
                                                        OnClick="btnCheckout_Click" ResourceString="VersionsProperties.btnCheckout" EnableViewState="false" />
                                                    <cms:LocalizedButton ID="btnCheckin" runat="server" CssClass="SubmitButton" Visible="false"
                                                        OnClick="btnCheckin_Click" ResourceString="VersionsProperties.btnCheckin" EnableViewState="false" />
                                                    <cms:LocalizedButton ID="btnUndoCheckout" runat="server" Visible="false" OnClick="btnUndoCheckout_Click"
                                                        ResourceString="VersionsProperties.btnUndoCheckout" CssClass="LongSubmitButton"
                                                        EnableViewState="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="plcVersions" runat="server">
                            <tr>
                                <td>
                                    <cms:LocalizedLabel ID="lblHistory" runat="server" Font-Bold="true" ResourceString="VersionsProperties.History"
                                        EnableViewState="false" />
                                </td>
                                <td class="DestroyHistory">
                                    <cms:LocalizedButton ID="btnDestroy" runat="server" CssClass="LongButton" OnClick="btnDestroy_Click"
                                        OnClientClick="return confirm(varConfirmDestroy);" ResourceString="VersionsProperties.Destroy" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <cms:UniGrid ID="gridHistory" runat="server" GridName="versionhistory.xml" OrderBy="VersionHistoryID DESC"
                                        IsLiveSite="false" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <tr>
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
