using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.LicenseProvider;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_BannedIP_Tools_BannedIP_List : CMSBannedIPsPage
{
    int siteId = 0;
    int selectedSiteId = 0;    


    /// <summary>
    /// Page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    { 
        siteId = QueryHelper.GetInteger("siteId", 0);

        if (siteId > 0)
        {
            // Hide site selector if accessing page from CMSDesk
            plcSites.Visible = false;
        }
        else
        {
            // Show contentplaceholder where site selector can be shown
            this.CurrentMaster.DisplayControlsPanel = true;

            // Set site selector
            siteSelector.DropDownSingleSelect.AutoPostBack = true;
            siteSelector.AllowAll = true;
            siteSelector.OnlyRunningSites = false;
            siteSelector.UniSelector.SpecialFields = new string[1, 2] { { ResHelper.GetString("general.global"), "0" } };
            siteSelector.UniSelector.OnSelectionChanged += new EventHandler(UniSelector_OnSelectionChanged);


            if (!RequestHelper.IsPostBack())
            {
                // Sites
                this.lblSite.Text = ResHelper.GetString("general.site") + ResHelper.Colon;

                siteSelector.AllowSetSpecialFields = true;

                // Preselect site from query or global = 0 value 
                selectedSiteId = QueryHelper.GetInteger("selectedsiteid", 0);
                siteSelector.Value = selectedSiteId;
            }
            else
            {
                selectedSiteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
            }
        }

        UniGrid.OrderBy = "IPAddress";
        UniGrid.WhereCondition = GenerateWhereCondition();

        this.CurrentMaster.Title.TitleText = ResHelper.GetString("banip.listHeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_BannedIP/object.png");
        this.CurrentMaster.Title.HelpTopicName = "banip_list";

        string[,] actions = new string[1, 6];

        // New item link
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("banip.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = "javascript: AddNewItem();";
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/CMS_BannedIP/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Page pre render
    /// </summary>
    protected void Page_PreRender(object sender, EventArgs e)
    {
        // If page is viewed from CMSDesk hide Sitename column
        if (siteId > 0)
        {
            UniGrid.GridView.Columns[5].Visible = false;
        }

        // Display information about disabled Banned IP module
        int currentSiteID = siteId > 0 ? siteId : ValidationHelper.GetInteger(siteSelector.Value, 0);
        string siteName = string.Empty;
        SiteInfo si = SiteInfoProvider.GetSiteInfo(currentSiteID);
        if (si != null)
        {
            siteName = si.SiteName;
        }

        string lblDisabledMessage = string.Empty;
        if (currentSiteID > 0)
        {
            lblDisabledMessage = ResHelper.GetString("banip.moduleDisabled");
        }
        else
        {
            lblDisabledMessage = ResHelper.GetString("banip.moduleDisabledglobal");
        }

        if (!BannedIPInfoProvider.IsBannedIPEnabled(siteName))
        {
            lblDisabled.Text = lblDisabledMessage;
        }
        else
        {
            lblDisabled.Text = string.Empty;           
        }
        
        pnlUpdateDisabled.Update();
        
        // Register correct script for new item
        ScriptHelper.RegisterClientScriptBlock(this.Page, typeof(string), "AddNewItem", ScriptHelper.GetScript(
            "function AddNewItem() { this.window.location = '" + ResolveUrl("BannedIP_Edit.aspx" + "?selectedsiteid=" + selectedSiteId + "&siteId=" + siteId) + "'} "));

    }


    /// <summary>
    /// Generates where condition
    /// </summary>    
    private string GenerateWhereCondition()
    {
        if (siteId > 0)
        {
            return "IPAddressSiteID = " + siteId;
        }
        else if (selectedSiteId > 0)
        {
            return "IPAddressSiteID = " + selectedSiteId;
        }
        else if (siteSelector.Value.ToString() == "0")
        {
            return "IPAddressSiteID IS NULL";
        }

        // (all)
        return String.Empty;
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        this.pnlUpdate.Update();
    }


    /// <summary>
    /// Handles unigrid external fields binding
    /// </summary>
    protected object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "bantype":
                BanControlEnum banControl = BannedIPInfoProvider.GetBanControlEnum((string)parameter);
                return ResHelper.GetString("banip.bantype" + banControl.ToString());
            case "sitename":
                int siteId = ValidationHelper.GetInteger(parameter, 0);
                SiteInfo site = SiteInfoProvider.GetSiteInfo(siteId);
                if (site != null)
                {
                    return site.SiteName;
                }
                break;

            case "allowed":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
            case "enabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }

        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("BannedIP_Edit.aspx?itemid=" + Convert.ToString(actionArgument) + "&selectedsiteid=" + selectedSiteId + "&siteId=" + siteId);
        }
        else if (actionName == "delete")
        {
            // check 'Modify' permission
            CheckPermissions("Modify");

            // delete BannedIPInfo object from database
            BannedIPInfoProvider.DeleteBannedIPInfo(Convert.ToInt32(actionArgument));
        }
    }


    /// <summary>
    /// Check specifies permission
    /// </summary>
    /// <param name="permissionName">name of permission</param>
    private void CheckPermissions(string permissionName)
    {
        //Check permissions
        if (siteId == 0)
        {
            // Must be global admin for manage all bannedip
            CheckGlobalAdministrator();
        }
        else
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);

            // check 'Read' permission for specified site
            if ((si == null) || (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.bannedip", permissionName, si.SiteName)))
            {
                RedirectToAccessDenied("cms.bannedip", permissionName);
            }
        }
    }
}
