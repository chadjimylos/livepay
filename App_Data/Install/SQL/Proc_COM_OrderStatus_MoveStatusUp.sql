CREATE PROCEDURE [Proc_COM_OrderStatus_MoveStatusUp]
	@StatusID int
AS
BEGIN
    /* Move the previous step(s) down */
	UPDATE COM_OrderStatus SET StatusOrder = StatusOrder + 1 WHERE StatusOrder = (SELECT StatusOrder FROM COM_OrderStatus WHERE StatusID = @StatusID) - 1 
	/* Move the current step up */
	UPDATE COM_OrderStatus SET StatusOrder = StatusOrder - 1 WHERE StatusID = @StatusID AND StatusOrder > 1
END
