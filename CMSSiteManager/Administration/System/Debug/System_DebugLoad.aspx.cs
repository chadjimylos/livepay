using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Threading;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.URLRewritingEngine;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_System_Debug_System_DebugLoad : CMSDebugPage
{
    #region "Variables"

    private static bool mCancel = true;
    
    private static int mSuccessRequests = 0;
    private static int mErrors = 0;
    
    private static int mCurrentThreads = 0;

    protected static string mLastError = null;

    #endregion


    #region "Request loader"

    /// <summary>
    /// Request loader class
    /// </summary>
    protected class RequestLoader
    {
        public string[] URLs = null;
        public DateTime RunUntil = DateTime.MaxValue;
        public int WaitInterval = 0;


        /// <summary>
        /// Returns true if the loader is canceled (exceeds the execution time or is forcibly canceled)
        /// </summary>
        protected bool IsCanceled()
        {
            return mCancel || (DateTime.Now > RunUntil);
        }


        /// <summary>
        /// Runs the load to the URLs
        /// </summary>
        public void Run()
        {
            mCurrentThreads++;

            // Prepare the client
            System.Net.WebClient client = new System.Net.WebClient();

            while (!IsCanceled())
            {
                // Run the list of URLs
                foreach (string url in URLs)
                {
                    if (IsCanceled())
                    {
                        break;
                    }

                    // Wait if some interval specified
                    if (WaitInterval > 0)
                    {
                        Thread.Sleep(WaitInterval);
                    }

                    try
                    {
                        // Get the page
                        client.DownloadData(url);

                        mSuccessRequests++;
                    }
                    catch (Exception ex)
                    {
                        mLastError = ex.Message;
                        mErrors++;
                    }
                }
            }

            // Dispose the client
            client.Dispose();

            mCurrentThreads--;
        }
    }

    #endregion


    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.lblInfo.Text = String.Format(ResHelper.GetString("DebugLoad.Info"), mCurrentThreads, mSuccessRequests, mErrors);
        this.lblError.Text = mLastError;

        this.btnStart.Text = ResHelper.GetString("DebugLoad.Generate");
        this.btnStop.Text = ResHelper.GetString("DebugLoad.Stop");

        this.btnStop.Enabled = (mCurrentThreads > 0);
    }


    protected void btnStop_Click(object sender, EventArgs e)
    {
        mCancel = true;
        while (mCurrentThreads > 0)
        {
            Thread.Sleep(100);
        }

        this.btnStart.Enabled = true;
        this.btnStop.Enabled = false;

        mSuccessRequests = 0;
        mErrors = 0;
    }


    protected void btnStart_Click(object sender, EventArgs e)
    {
        mCancel = false;

        if (!String.IsNullOrEmpty(txtURLs.Text))
        {
            // Prepare the parameters
            string[] urls = txtURLs.Text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < urls.Length; i++)
            {
                urls[i] = UrlHelper.GetAbsoluteUrl(urls[i]);
            }

            int newThreads = ValidationHelper.GetInteger(txtThreads.Text, 0);
            if (newThreads > 0)
            {
                int duration = ValidationHelper.GetInteger(txtDuration.Text, 0);
                int interval = ValidationHelper.GetInteger(txtInterval.Text, 0);

                DateTime runUntil = DateTime.Now.AddSeconds(duration);

                // Run specified number of threads
                for (int i = 0; i < newThreads; i++)
                {
                    // Prepare the loader object
                    RequestLoader loader = new RequestLoader();
                    loader.URLs = urls;
                    loader.WaitInterval = interval;
                    if (duration > 0)
                    {
                        loader.RunUntil = runUntil;
                    }

                    // Start new thread
                    CMSThread newThread = new CMSThread(loader.Run);
                    newThread.Start();
                }

                this.btnStop.Enabled = true;
            }
        }
    }


    
}
