(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// stage content:
(lib.banner_913x394 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// bravio6
	this.instance = new lib.Symbol6("synched",0);
	this.instance.setTransform(599.3,67.8,1.497,1.497,0,0,0,47.5,53.3);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(337).to({startPosition:0,_off:false},0).to({regX:47.4,regY:53.5,scaleX:0.56,scaleY:0.56,x:598.9,y:76.2,alpha:1},7).wait(17));

	// bravio5
	this.instance_1 = new lib.Symbol5("synched",0);
	this.instance_1.setTransform(530.5,75,1.831,1.831,0,0,0,32.5,32.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(331).to({startPosition:0,_off:false},0).to({scaleX:1,scaleY:1,x:541,alpha:1},6).wait(24));

	// bravio4
	this.instance_2 = new lib.Symbol4("synched",0);
	this.instance_2.setTransform(469.5,75,1.831,1.831,0,0,0,32.5,32.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(323).to({startPosition:0,_off:false},0).to({scaleX:1,scaleY:1,x:476,alpha:1},8).wait(30));

	// bravio3
	this.instance_3 = new lib.Symbol3("synched",0);
	this.instance_3.setTransform(411.5,76,1.831,1.831,0,0,0,32.5,32.5);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(316).to({startPosition:0,_off:false},0).to({scaleX:1,scaleY:1,x:411,y:75,alpha:1},7).wait(38));

	// bravio2
	this.instance_4 = new lib.Symbol2("synched",0);
	this.instance_4.setTransform(352,81,1.831,1.831,0,0,0,32.5,32.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(308).to({startPosition:0,_off:false},0).to({scaleX:1,scaleY:1,x:343,y:75,alpha:1},8).wait(45));

	// bravio1
	this.instance_5 = new lib.Symbol1("synched",0);
	this.instance_5.setTransform(272.5,75,1.831,1.831,0,0,0,32.5,32.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(301).to({startPosition:0,_off:false},0).to({scaleX:1,scaleY:1,alpha:1},7).wait(53));

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_95 = new cjs.Graphics().p("EgqLAJEIAAjxMB19AAAIAADxg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(95).to({graphics:mask_graphics_95,x:485,y:58}).wait(266));

	// oroi
	this.instance_6 = new lib.oroi();
	this.instance_6.setTransform(1249.4,102.1,1,1,0,0,0,273.4,8.3);
	this.instance_6._off = true;

	this.instance_6.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(95).to({_off:false},0).to({x:-63.1,y:100.1},194).to({_off:true},1).wait(71));

	// text
	this.instance_7 = new lib.text();
	this.instance_7.setTransform(521.7,61.3,1,1,0,0,0,274.6,22.3);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(83).to({_off:false},0).to({alpha:1},12).wait(194).to({alpha:0},12).to({_off:true},1).wait(59));

	// teleies
	this.instance_8 = new lib.teleies();
	this.instance_8.setTransform(274.3,105.3,1,1,0,0,0,279,1);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(55).to({_off:false},0).to({alpha:1},6).wait(10).to({x:-130.5,y:104.9},12).wait(278));

	// tonos shine
	this.instance_9 = new lib.tonosshine();
	this.instance_9.setTransform(-29.8,78.3,1,1,0,0,0,-178.7,16.3);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(83).to({_off:false},0).wait(278));

	// tonos
	this.instance_10 = new lib.tonos();
	this.instance_10.setTransform(529.6,35.3,2.344,2.344,0,0,0,11.6,23.6);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(47).to({_off:false},0).to({regX:11.5,scaleX:1,scaleY:1,x:529.3,alpha:1},7).wait(17).to({x:124.3,y:34.9},12).wait(278));

	// 10
	this.instance_11 = new lib.Tween7("synched",0);
	this.instance_11.setTransform(470,71.4);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(24).to({startPosition:0,_off:false},0).wait(47).to({startPosition:0},0).to({x:65,y:71},12).wait(278));

	// 10
	/* Layers with classic tweens must contain only a single symbol instance. */

	// bg_grey
	this.instance_12 = new lib.bg_gold_970x200();
	this.instance_12.setTransform(-5.9,-31.4,0.955,2.21);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_12}]}).wait(361));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5.9,-31.4,926,442);


// symbols:
(lib.Path = function() {
	this.initialize(img.Path);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,558,2);


(lib.award = function() {
	this.initialize(img.award);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,58,67);


(lib.bg_gold_970x200 = function() {
	this.initialize(img.bg_gold_970x200);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,970,200);


(lib.bg_white_970x200 = function() {
	this.initialize(img.bg_white_970x200);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,970,200);


(lib.bravio_1 = function() {
	this.initialize(img.bravio_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,65,65);


(lib.bravio_2 = function() {
	this.initialize(img.bravio_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,65,65);


(lib.bravio_3 = function() {
	this.initialize(img.bravio_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,65,65);


(lib.bravio_4 = function() {
	this.initialize(img.bravio_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,65,65);


(lib.bravio_5 = function() {
	this.initialize(img.bravio_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,65,65);


(lib.pb2005 = function() {
	this.initialize(img.pb2005);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,69,69);


(lib.Tween7 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#80775A","#AFA68A","#80775A"],[0,0.525,1],63.3,-50.2,-27.8,25.3).s().p("AieGaQhXgUgWgtQgFgLgBgJQgFgDgEgOIgMAAIAIgGIgDgEIAAgFIAEgKQgHgXgBhOQALhCAVg4IgEgEIAFgEQAGgEAWgwIgFgDIAGgDQAGgCArhQQAthNBOhZQAHgIAQgOIAlgkIgBAAIAAgKIAJgHIgHgHIAMAAQAEAAAjghIAYgQIgGgLIASgNIAFAAIAJAHIAOgCIACABQAZANAOANIACABIAAAHIgEAEQA3ArAoA/QA5BTgRChIgBABIgCAGIABACIAAAEQgSBchMBpQhABRg/AvQgvAlgiAOQg7AihEAAIgHgBIgKADgAiAF0QAiAAAqgQIAAgEQABgJARgCIAGgBIgBAHQA0gZBCgyQBwh0AIgkIABgDIAEgBIABgEQADgIAFgEIABgGIADgDIgHgHIAMAAQACgBABgFIgCgMIAGAAQADgCAEgQIgBgQIADAAIABgMIAFAAIADgNIgEAAIACgKIAHgGIgBgHIABgJIADgHIgDgMIACAAIABgCIACgGQADgagBgNIgDgHIgFAAIABgMIAEAAQgBgggEgGIgNAAIAIgHIgJAAIALgKIAAgBQguh0hYg5QhDABh1CBIgkArQiYDMgPCHIgCARQgFAxARAyIAAAAIAkAlIgIAJQAIAMAiAMIADAAIACgBIAJAAIAAADIAGAAQAFAAACADQACACAAAFIAHABIANgEIAAACIADgBg");
	this.shape.setTransform(11.9,1.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#80775A","#AFA68A","#80775A"],[0,0.525,1],68.9,-54.9,-22.2,20.7).s().p("AhNFpIABgDIARAAIgBADgAAdE3IAAAFIgNACQABgFAMgCgABGlhIAAgCIAGgFIACADIAAACIgDACg");
	this.shape_1.setTransform(5.4,4.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#80775A","#AFA68A","#80775A"],[0,0.525,1],82.5,-68,-8.5,7.5).s().p("AidHyIgJgBIgIgTQgEgCgDgEQgEgIAEgQIACgEIADgCIAEgnIALACIAAgCIACgSIADgCIgBgGIAvisIAGgJIAAgLIAHgVIABgBIgBgBIASgmIgBgLIADgCIABgKIAVgyIADgVIASgkIAAgMIAFgLIgCgEIAPgaIgDgCIAriFIgHgLIANADIAUg7IgCgDIABgFIAVgvIgBgIIAAgCIADgEIgDgBIikDkQgbAcgMAIQgGAFgEgBIgGgBQgIgCgGgXIAAgCIAAgBQADgJALgIIgGgLIACgGIACgBQAkgZAegiIABgFIACACQAVgWAWgdIACgGIAyg+IgDgFIAGgEIADgFIAfglIgCgCIADgGQAhgqAeg3IAHgXIASgKIAKABIAIAJIALgGIADABQAOAKAAAIIgBACIgHAQIAFAGIgDAHIgMAPIgLgCIgBgCIgFABIgBARIgBAAIgBAFIgCAAQgLAKg0CKIACADIgIAQIhjEhIgJAJIgBAQIgDAIIgBAAIAAABIgCAKIgBAAIAAABIgnB0IACADIgCAGIgGAHQgiCCgKA+IADAAQAeAMgHAaIgCAFIgCABIAEANIgCAJIgQAJIgCgCQgJgEgEgHQgMAUgHAAg");
	this.shape_2.setTransform(-22.9,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#80775A","#AFA68A","#80775A"],[0,0.525,1],79.1,-65.5,-11.9,10).s().p("AgHAIIABgEQAJgHAAgHIACACIADAAQgIATgFAAg");
	this.shape_3.setTransform(-31.5,-16.9);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-41.4,-49.8,82.9,99.8);


(lib.Tween6 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#80775A","#AFA68A","#80775A"],[0,0.525,1],63.3,-50.2,-27.8,25.3).s().p("AieGaQhXgUgWgtQgFgLgBgJQgFgDgEgOIgMAAIAIgGIgDgEIAAgFIAEgKQgHgXgBhOQALhCAVg4IgEgEIAFgEQAGgEAWgwIgFgDIAGgDQAGgCArhQQAthNBOhZQAHgIAQgOIAlgkIgBAAIAAgKIAJgHIgHgHIAMAAQAEAAAjghIAYgQIgGgLIASgNIAFAAIAJAHIAOgCIACABQAZANAOANIACABIAAAHIgEAEQA3ArAoA/QA5BTgRChIgBABIgCAGIABACIAAAEQgSBchMBpQhABRg/AvQgvAlgiAOQg7AihEAAIgHgBIgKADgAiAF0QAiAAAqgQIAAgEQABgJARgCIAGgBIgBAHQA0gZBCgyQBwh0AIgkIABgDIAEgBIABgEQADgIAFgEIABgGIADgDIgHgHIAMAAQACgBABgFIgCgMIAGAAQADgCAEgQIgBgQIADAAIABgMIAFAAIADgNIgEAAIACgKIAHgGIgBgHIABgJIADgHIgDgMIACAAIABgCIACgGQADgagBgNIgDgHIgFAAIABgMIAEAAQgBgggEgGIgNAAIAIgHIgJAAIALgKIAAgBQguh0hYg5QhDABh1CBIgkArQiYDMgPCHIgCARQgFAxARAyIAAAAIAkAlIgIAJQAIAMAiAMIADAAIACgBIAJAAIAAADIAGAAQAFAAACADQACACAAAFIAHABIANgEIAAACIADgBg");
	this.shape.setTransform(11.9,1.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#80775A","#AFA68A","#80775A"],[0,0.525,1],68.9,-54.9,-22.2,20.7).s().p("AhNFpIABgDIARAAIgBADgAAdE3IAAAFIgNACQABgFAMgCgABGlhIAAgCIAGgFIACADIAAACIgDACg");
	this.shape_1.setTransform(5.4,4.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.lf(["#80775A","#AFA68A","#80775A"],[0,0.525,1],82.5,-68,-8.5,7.5).s().p("AidHyIgJgBIgIgTQgEgCgDgEQgEgIAEgQIACgEIADgCIAEgnIALACIAAgCIACgSIADgCIgBgGIAvisIAGgJIAAgLIAHgVIABgBIgBgBIASgmIgBgLIADgCIABgKIAVgyIADgVIASgkIAAgMIAFgLIgCgEIAPgaIgDgCIAriFIgHgLIANADIAUg7IgCgDIABgFIAVgvIgBgIIAAgCIADgEIgDgBIikDkQgbAcgMAIQgGAFgEgBIgGgBQgIgCgGgXIAAgCIAAgBQADgJALgIIgGgLIACgGIACgBQAkgZAegiIABgFIACACQAVgWAWgdIACgGIAyg+IgDgFIAGgEIADgFIAfglIgCgCIADgGQAhgqAeg3IAHgXIASgKIAKABIAIAJIALgGIADABQAOAKAAAIIgBACIgHAQIAFAGIgDAHIgMAPIgLgCIgBgCIgFABIgBARIgBAAIgBAFIgCAAQgLAKg0CKIACADIgIAQIhjEhIgJAJIgBAQIgDAIIgBAAIAAABIgCAKIgBAAIAAABIgnB0IACADIgCAGIgGAHQgiCCgKA+IADAAQAeAMgHAaIgCAFIgCABIAEANIgCAJIgQAJIgCgCQgJgEgEgHQgMAUgHAAg");
	this.shape_2.setTransform(-22.9,0);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.lf(["#80775A","#AFA68A","#80775A"],[0,0.525,1],79.1,-65.5,-11.9,10).s().p("AgHAIIABgEQAJgHAAgHIACACIADAAQgIATgFAAg");
	this.shape_3.setTransform(-31.5,-16.9);

	this.addChild(this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-41.4,-49.8,82.9,99.8);


(lib.Tween1 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","#FFFFFF","rgba(255,255,255,0)"],[0,0.49,1],-39.9,-0.4,40,-0.4).s().p("AmOD0IAAnoIMeAAIAAHog");

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-39.9,-24.4,80,49);


(lib.tonos = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#80775A","#AFA68A","#80775A"],[0,0.525,1],14.3,-15,-76.7,60.6).s().p("AhsDqIgFgJIACgIIAEgCIAAAAQgGgKAIggIABgDIANgBIAAgJIAAAAQAEgOAQgHIAAgMIAGgEIACgFQAMgVAIgHIgBgCIAGgRIAJABIAAgBIgEgNIAAgCQAEgJAMAAIgEgLIAJgcIALACIABADIAMgnIgMgDIAPg4IgPAAIgHgCIgBgBQgggvAIgXQAehWAxALIAPADIAFAJIAUABIALACQAcARgUBOIAAACIgGADIAAABIgBAVIgCAAIgDAJIgYASQgDAIACADQAAABAAAAQABABAAAAQABAAAAABQABAAABAAIAFABIgKAeIgCABIABACIgGARIgFgBIABACIgCACQg/CHhGBUIgCACIgDgBQgEAAgDAKIgBACIgJAEgAAQgxIAEgMIgBAAg");
	this.shape.setTransform(11.5,23.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.lf(["#80775A","#AFA68A","#80775A"],[0,0.525,1],10.4,-8.7,-80.7,66.8).s().p("AALA5IAFgPIAEABIgFAPgAAOAhIAGgSIAFABIgHASgAgYghIADgXIAFAAIACAFIgGATg");
	this.shape_1.setTransform(12.3,13.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,23,47.2);


(lib.text = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#726C50").s().p("AgLALQgFgEAAgHQAAgGAFgEQAFgFAGAAQAHAAAFAFQAFAEAAAGQAAAHgFAEQgFAFgHAAQgGAAgFgFg");
	this.shape.setTransform(547.5,37.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#726C50").s().p("AADBEIAHgIIADgGIACgFIABgEIgBgEIgDgEIgEgDIgSgGQgIgDgIgGQgIgIgFgIQgFgIAAgOQAAgMAEgKQAFgKAHgIQAIgHALgEQAJgDANAAQAOAAASAHIAAAXQgOgIgMAAQgGAAgJACQgEACgFAEQgFAEgCAGQgDAGAAAIQAAAKAGAJQAHAFALAEQAMADAHAFQAIAEADAHQADAIgCAIQgCAHgGAJIgKAOg");
	this.shape_1.setTransform(539.4,36.2);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#726C50").s().p("AgXAyQgKgEgGgIQgGgHgDgKQgEgJAAgMQAAgLAEgKQACgIAHgJQAHgHAIgEQAKgEAKAAQAIAAAGACQAJADAHAFIAAgIIAbAAIAABnIgcAAIAAgNQgGAJgJACQgKAEgEAAQgKAAgJgEgAgQgXQgIAJAAAOQAAAOAIAJQAIAJALAAQAFAAAHgEQAEgCAGgIIAAgpIgLgHIgMgCQgLAAgHAJg");
	this.shape_2.setTransform(527.4,34.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#726C50").s().p("AgWBNIAAhnIAZAAIAABngAgSgoIASgkIAXAAIgbAkg");
	this.shape_3.setTransform(519.4,31.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#726C50").s().p("AgdAxQgJgEgJgHQgIgIgDgJQgEgLAAgKQAAgKAEgKQAEgKAHgHQAJgHAJgEQAJgEANAAIBGAAIAAAWIgcAAQAJAFAEAJQACAJAAAHQAAAKgEALQgEAKgIAHQgJAIgJADQgLAEgKAAQgMAAgMgEgAgagWQgIAJAAANQAAAOAHAJQAJAJANAAQAMAAAHgJQAIgKAAgNQAAgMgIgKQgJgIgKAAQgOAAgHAIg");
	this.shape_4.setTransform(508.8,34.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#726C50").s().p("AgSAyQgIgCgIgGQgHgGgDgJQgEgJAAgMIAAg6IAcAAIAAA5QAAAOAGAFQAGAHAIAAQAJAAAGgHQAGgGAAgOIAAg4IAcAAIAAA4QAAAYgOAMQgMANgXAAQgIAAgKgDg");
	this.shape_5.setTransform(495.6,34.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#726C50").s().p("AgWAyQgKgEgIgIQgIgHgEgKQgEgKAAgLQAAgKAEgLQAEgJAIgIQAIgHAKgEQALgEALAAQAMAAALAEQAKAEAIAHQAIAIAEAJQAEALAAAKQAAALgEAKQgEAKgIAIQgIAHgKAEQgLAEgMAAQgLAAgLgEgAgUgWQgIAJAAANQAAAOAIAJQAIAJAMAAQANAAAIgJQAIgJAAgOQAAgGgCgGQgCgGgEgEQgEgEgGgDQgGgCgFAAQgLAAgJAJg");
	this.shape_6.setTransform(483.1,34.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#726C50").s().p("AgMA0IAAhnIAZAAIAABng");
	this.shape_7.setTransform(473.8,34.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#726C50").s().p("Ag2BPIAAhmQAAgPAFgKQAGgMAHgFQAJgGAKgEQAKgDAIAAQAJAAAJADQAKAEAHAFQAIAIAFAKQAGAKAAAPQAAANgGAKQgGAKgHAFQgIAIgJACQgJAEgJAAQgHAAgHgEQgIgCgFgHIAAA/gAgTgvQgHAJAAAOQAAAJACAGQACAFAEAEQADACAFADIAKACQALAAAIgHQAHgIAAgQQAAgIgCgGQgCgGgEgEQgDgDgFgDQgFgCgFAAQgLAAgIAJg");
	this.shape_8.setTransform(464.7,36.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#726C50").s().p("AgOA0IgQgFQgIgEgDgGQgFgGAAgIQAAgEACgEQABgEAEgDIAJgHQAFgBAIgBQgMgCgGgGQgGgHgBgHQAAgIAEgGQAFgFAHgEQAKgEAGgBIAPgCIALABIASAFIAKAFIAAAVQgGgEgGgCIgKgEIgJgBIgNAAIgFACIgEADQgCACAAAEQAAAEABACQACACADACIAFABIAaAAIAAAPIgYAAIgHACQgFACgDADQgCACAAAFIABAGIAEAEIAHADIAFABIAIgBIAKgCQAFAAAHgEQALgEAEgDIAAAVIgLAHIgLAEIgLACIgMABg");
	this.shape_9.setTransform(452.4,34.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#726C50").s().p("AAUA0IAAhRIgnAAIAABRIgbAAIAAhRIgOAAIAAgWIB5AAIAAAWIgOAAIAABRg");
	this.shape_10.setTransform(440.1,34.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#726C50").s().p("AAVBPIAAhxQAAgMgEgGQgFgGgJAAQgFAAgGAEQgIAGgEAGIAABFIgcAAIAAhnIAcAAIAAANQAOgPASAAQAQAAAKAKQALALgBAUIAAB0g");
	this.shape_11.setTransform(420.9,36.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#726C50").s().p("AgcAxQgKgEgIgHQgIgHgEgKQgEgLAAgKQAAgKAEgKQAFgMAHgFQAHgHAKgEQAKgEANAAIBFAAIAAAWIgcAAQAJAFADAJQADAJAAAHQAAAMgEAJQgEAKgIAHQgIAHgKAEQgLAEgLAAQgLAAgLgEgAgagWQgIAJAAANQAAAOAHAJQAJAJAMAAQANAAAHgJQAIgJAAgOQAAgMgIgKQgIgIgMAAQgNAAgHAIg");
	this.shape_12.setTransform(407.8,34.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#726C50").s().p("AgMA0IAAhnIAZAAIAABng");
	this.shape_13.setTransform(397.9,34.1);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#726C50").s().p("Ag2BPIAAhmQAAgOAFgLQAFgLAJgGQAJgHAJgDQAKgDAIAAQAJAAAJADQAKAEAHAFQAIAIAGAKQAFAKAAAPQAAANgFAKQgHAKgHAFQgIAIgJACQgJAEgJAAQgGAAgIgEQgHgCgGgHIAAA/gAgTgvQgHAJAAAOQgBAIADAHQACAFAEAEQAEADAFACIAJACQALAAAHgHQAJgIgBgQQAAgHgCgHQgCgGgEgEQgDgDgGgDQgDgCgGAAQgLAAgIAJg");
	this.shape_14.setTransform(388.8,36.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#726C50").s().p("AgWBNIAAhnIAZAAIAABngAgTgoIATgkIAYAAIgcAkg");
	this.shape_15.setTransform(380,31.6);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#726C50").s().p("AgOA0QgKgCgGgDQgHgEgEgGQgFgGAAgIIABgIIAGgHQADgEAGgDQAGgBAGgBQgLgCgHgGQgFgHgBgHQAAgIAEgGQAEgFAIgEQAKgEAGgBIAPgCIALABIASAFIAKAFIAAAVQgGgEgGgCIgKgEIgIgBIgOAAIgEACIgGADQgBADAAADQAAAEABACQABACAEACIAFABIAZAAIAAAPIgXAAIgHACQgFACgDADQgDACAAAFIACAGIAEAEIAHADIAFABIAIgBIAKgCQAFAAAHgEQALgEAEgDIAAAVIgMAHIgKAEIgLACIgMABg");
	this.shape_16.setTransform(370.7,34.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#726C50").s().p("AAbA0IgbgiIgaAiIgiAAIAsg0IgsgzIAiAAIAaAhIAZghIAiAAIgrAzIAtA0g");
	this.shape_17.setTransform(359,34.1);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#726C50").s().p("AgXAyQgKgFgFgHQgGgHgEgKQgEgJAAgMQAAgLAEgKQACgIAHgJQAIgIAIgDQAJgEAKAAQAJAAAGACQAIADAHAFIAAgIIAbAAIAABnIgcAAIAAgNQgGAJgJACQgJAEgFAAQgKAAgJgEgAgQgXQgHAJAAAOQAAAOAHAJQAIAJALAAQAGAAAGgEQAGgDAEgHIAAgpIgLgHQgHgCgFAAQgLAAgHAJg");
	this.shape_18.setTransform(346.3,34.2);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#726C50").s().p("AgMA0IAAhnIAZAAIAABng");
	this.shape_19.setTransform(337.5,34.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#726C50").s().p("AgVBMQgKgEgIgHQgIgHgFgKQgEgJAAgLQAAgLADgIQAEgJAGgFQAFgGAKgFQAJgDAJgCIgQgHQgHgDgEgEQgEgDgCgEIgBgIQAAgIAEgFQAFgHAFgCQAGgDAJgCIAPgCIANABIAKADIASAIIAAAVIgKgHIgKgFIgIgCIgJAAIgGABIgGACIgEAEQgCACAAADQAAAHAJAFQAGAFAPAGQAKAFAHAEQAIAGAEAFQAGAGADAIQADAJAAAKQAAAMgEAIQgFAKgHAHQgJAHgJAEQgLADgMABQgLgBgKgDgAgKAAQgFABgEAFQgEADgDAGQgCAFAAAIQAAAIACAFQADAFAEAEQAEAEAFACIAKACQAEAAAHgCQAGgCADgEQAEgEACgFQADgGAAgHQAAgGgDgHQgCgGgEgDQgDgFgGgBQgFAAgGAAQgFAAgFAAg");
	this.shape_20.setTransform(328.3,31.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#726C50").s().p("AAVBPIAAhxQAAgMgEgGQgFgGgJAAQgFAAgHAEQgHAFgEAHIAABFIgbAAIAAhnIAbAAIAAANIAAAAQAOgPASAAQARAAAJAKQALALAAAUIAAB0g");
	this.shape_21.setTransform(309.2,36.7);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#726C50").s().p("AgDAsQgLgJAAgSIAAgvIgiAAIAAgWIBhAAIAAAWIglAAIAAAtQAAAJAFAEQAFAEAGAAQAFAAAEgCQAEAAAEgDIAAAVIgKAEIgMABQgRAAgJgJg");
	this.shape_22.setTransform(297.8,34.2);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#726C50").s().p("AgcAxQgKgEgIgHQgIgIgEgJQgEgKAAgLQAAgKAEgKQAEgKAHgHQAIgHAKgEQAJgEAOAAIBFAAIAAAWIgcAAQAJAFADAJQADAIAAAIQAAAKgEALQgFAKgHAHQgIAHgKAEQgLAEgKAAQgMAAgLgEgAgagWQgIAKAAAMQAAANAIAKQAHAJANAAQAMAAAIgJQAIgJAAgOQAAgNgIgJQgJgIgLAAQgMAAgIAIg");
	this.shape_23.setTransform(285.8,34.2);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#726C50").s().p("AgWAyQgKgEgIgIQgIgHgEgKQgEgKAAgLQAAgKAEgLQAEgJAIgIQAJgIAJgDQALgEALAAQAMAAALAEQAKAEAIAHQAIAIAEAJQAEALAAAKQAAALgEAKQgEAKgIAIQgIAHgKAEQgLAEgMAAQgKAAgMgEgAgUgWQgIAJAAANQAAAOAIAJQAIAJAMAAQANAAAIgJQAIgJAAgOQAAgGgCgGQgCgGgEgEQgEgEgGgDQgGgCgFAAQgLAAgJAJg");
	this.shape_24.setTransform(265.8,34.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#726C50").s().p("AgEA1IgxhpIAdAAIAYA7IAZg7IAdAAIgvBpg");
	this.shape_25.setTransform(254,34.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#726C50").s().p("AgWBKQgKgEgIgIQgIgHgEgKQgEgKAAgMQAAgLAEgLQAEgHAIgIQAJgIAJgDQALgEALAAQAMAAALAEQAKAEAIAHQAIAIAEAHQAEALAAALQAAAMgEAKQgEAKgIAIQgIAHgKAEQgLAEgMAAQgKAAgMgEgAgUAAQgIAJAAAOQAAAPAIAJQAIAJAMAAQANAAAIgJQAIgJAAgPQAAgHgCgGQgCgGgEgEQgEgCgGgDQgGgCgFAAQgLAAgJAHgAgFgpIASgkIAYAAIgeAkg");
	this.shape_26.setTransform(242.2,31.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#726C50").s().p("AgDAsQgKgJAAgSIAAgvIgjAAIAAgWIBhAAIAAAWIglAAIAAAtQAAAJAFAEQAFAEAGAAQAGAAADgCQAEAAAEgDIAAAVIgKAEIgMABQgRAAgJgJg");
	this.shape_27.setTransform(230.5,34.2);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#726C50").s().p("AgFA1IgwhpIAdAAIAYA7IAag7IAcAAIguBpg");
	this.shape_28.setTransform(213.6,34.2);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#726C50").s().p("AgWAyQgKgEgIgIQgIgHgEgKQgEgKAAgLQAAgKAEgLQAEgJAIgIQAIgHAKgEQALgEALAAQALAAAMAEQAJADAJAIQAIAIAEAJQAEALAAAKQAAAMgEAJQgEAKgIAIQgIAHgKAEQgLAEgMAAQgLAAgLgEgAgUgWQgIAJAAANQAAAOAIAJQAIAJAMAAQANAAAIgJQAIgIAAgPQAAgHgCgFQgCgGgEgEQgEgEgGgDQgGgCgFAAQgMAAgIAJg");
	this.shape_29.setTransform(201.8,34.1);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#726C50").s().p("AgDAsQgKgJAAgSIAAgvIgjAAIAAgWIBhAAIAAAWIglAAIAAAtQAAAJAFAEQAFAEAGAAQAGAAADgCQAEAAAEgDIAAAVIgKAEIgMABQgRAAgJgJg");
	this.shape_30.setTransform(190.1,34.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#726C50").s().p("AgOA0IgQgFQgIgEgEgGQgEgGAAgIIACgIIAFgHIAJgHQAFgBAIgBQgMgCgGgGQgHgHAAgHQAAgJAFgFQAFgGAHgDQAIgEAIgBIAPgCIALABIAJACIAIADIAKAFIAAAVQgGgEgGgCIgKgEIgJgBIgNAAIgEACIgFADQgCACAAAEQAAAEACACQAAACAEACIAFABIAaAAIAAAPIgYAAIgHACQgEACgEADQgDACABAFQgBAEACACIAFAEIAGADIAFABIAIgBIAKgCIANgEIAOgHIAAAVIgLAHIgLAEIgKACIgNABg");
	this.shape_31.setTransform(173.1,34.1);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#726C50").s().p("AgxBOIAAibIAcAAIAAA2QAAARAFAGQAGAJAKAAQAJAAAHgIQAGgHAAgOIAAg5IAcAAIAABnIgaAAIAAgOIAAAAQgEAJgGADQgGADgIABQgNAAgIgOIAAAAIAABAg");
	this.shape_32.setTransform(161.2,36.8);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#726C50").s().p("AgSAyQgIgCgIgGQgFgFgFgKQgEgJAAgMIAAg6IAcAAIAAA5QAAAOAGAFQAGAHAIAAQAJAAAGgHQAGgGAAgOIAAg4IAcAAIAAA4QAAAYgOAMQgMANgXAAQgIAAgKgDg");
	this.shape_33.setTransform(148.2,34.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#726C50").s().p("AgWAyQgKgEgIgIQgIgHgEgKQgEgKAAgLQAAgKAEgLQAEgJAIgIQAIgHAKgEQALgEALAAQAMAAALAEQAKAEAIAHQAIAIAEAJQAEALAAAKQAAALgEAKQgEAKgIAIQgIAHgKAEQgLAEgMAAQgLAAgLgEgAgUgWQgIAJAAANQAAAOAIAJQAIAJAMAAQANAAAIgJQAIgJAAgOQAAgGgCgGQgCgGgEgEQgEgEgGgDQgGgCgFAAQgLAAgJAJg");
	this.shape_34.setTransform(135.7,34.1);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#726C50").s().p("AgEA1IgxhpIAeAAIAYA7IAZg7IAcAAIguBpg");
	this.shape_35.setTransform(123.9,34.2);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#726C50").s().p("AgWBNIAAhnIAZAAIAABngAgSgoIASgkIAXAAIgbAkg");
	this.shape_36.setTransform(116.6,31.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#726C50").s().p("AgVBMQgLgEgIgHQgHgHgFgKQgEgJAAgLQAAgLADgIQAEgJAGgFQAGgGAJgFQAKgDAIgCIgQgHQgHgDgEgEQgDgCgDgFIgBgIQAAgIAEgFQAEgGAGgDQAGgDAJgCIAPgCIANABIAKADIASAIIAAAVIgLgHIgJgFIgIgCIgJAAIgGABIgGACIgFAEQgBACAAADQAAAHAIAFQAHAFAPAGQAJAFAIAEQAHAFAFAGQAGAGADAIQADAIAAALQAAALgEAJQgFAKgHAHQgKAIgJADQgKADgMABQgLgBgKgDgAgKAAQgGABgDAFQgEAEgDAFQgCAHAAAGQAAAHACAGQACAEAFAFQAEAEAFACIAKACQAEAAAHgCQAFgCAEgEIAHgJQACgFAAgIQAAgIgDgFQgCgGgEgDQgEgFgFgBQgFAAgGAAQgFAAgFAAg");
	this.shape_37.setTransform(106.4,31.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#726C50").s().p("AgXAyQgKgEgGgIQgHgIgCgJQgEgJAAgMQAAgKADgLQAEgLAGgGQAHgIAIgDQAKgEAKAAQAIAAAGACQAHADAJAFIAAgIIAbAAIAABnIgcAAIAAgNQgGAJgJACQgKAEgEAAQgKAAgJgEgAgQgXQgIAJAAAOQAAAOAIAJQAHAJAMAAQAFAAAHgEQAFgDAFgHIAAgpQgGgFgGgCIgLgCQgLAAgHAJg");
	this.shape_38.setTransform(86.8,34.2);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#726C50").s().p("AgMA0IAAhnIAZAAIAABng");
	this.shape_39.setTransform(78,34.1);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#726C50").s().p("AgEA1IgxhpIAdAAIAZA7IAYg7IAdAAIguBpg");
	this.shape_40.setTransform(69.9,34.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#726C50").s().p("AgWBKQgKgEgIgIQgHgGgFgLQgEgLAAgLQAAgLAEgLQAEgHAIgIQAIgHAKgEQALgEALAAQALAAAMAEQAJAEAJAHQAIAIAEAHQAEALAAALQAAANgEAJQgEAKgIAIQgIAHgKAEQgLAEgMAAQgLAAgLgEgAgUAAQgIAJAAAOQAAAQAIAIQAIAJAMAAQANAAAIgJQAIgIAAgQQAAgIgCgFQgDgGgEgEQgDgDgGgCQgGgCgFAAQgMAAgIAHgAgFgpIASgkIAYAAIgeAkg");
	this.shape_41.setTransform(58.1,31.7);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#726C50").s().p("Ag2BPIAAhmQAAgPAFgKQAFgKAIgHQAJgHAKgDQAKgDAIAAQAIAAAKADQAKAEAHAFQAIAIAFAKQAGAKAAAPQAAANgGAKQgGAKgHAFQgIAIgJACQgKAEgIAAQgHAAgHgEQgIgCgFgHIAAA/gAgTgvQgHAIAAAPQAAAJACAGQACAFAEAEQADACAFADIAKACQALAAAIgHQAHgIAAgQQAAgFgCgJQgCgGgEgEQgDgDgGgDQgEgCgFAAQgLAAgIAJg");
	this.shape_42.setTransform(45.2,36.7);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#726C50").s().p("AAbA0IgbgiIgbAiIghAAIArg0IgrgzIAiAAIAaAhIAYghIAiAAIgqAzIAtA0g");
	this.shape_43.setTransform(32.3,34.1);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#726C50").s().p("AgVBKQgKgFgHgLQgHgLgEgOQgEgQAAgRQAAgQAEgQQAEgNAHgNQAHgJAKgGQAKgGALAAQALAAAKAGQALAGAHAJQAGALAFAPQAEAQAAAQQAAARgEAQQgFAQgGAJQgHALgLAFQgKAGgLAAQgLAAgKgGgAgQgoQgGAPAAAZQAAAaAGAOQAGAOAKAAQALAAAGgOQAGgOAAgaQAAgZgGgPQgGgOgLAAQgKAAgGAOg");
	this.shape_44.setTransform(13.7,31.6);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#726C50").s().p("AgNBOIAAibIAbAAIAACbg");
	this.shape_45.setTransform(1.5,31.5);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#726C50").s().p("AgLALQgFgFAAgGQAAgFAFgFQAFgFAGAAQAHAAAFAFQAFAFAAAFQAAAGgFAFQgFAFgHAAQgGAAgFgFg");
	this.shape_46.setTransform(518,14.4);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#726C50").s().p("AgWBLQgKgCgGgEQgGgDgFgHQgDgFAAgHQAAgVAbgGQgMgGAAgIQAAgFAFgDQAFgEAIgBIAAAAQgNgFgHgJQgGgHAAgNQAAgRANgJQANgKAXAAIAxAAIAAATIgSAAQAFAHABACIACALQAAAKgGAIQgGAIgKADQgFADgHACIgQADQgEABAAADQAAAFAIACIAVAEQASADAHAHQAIAJAAALQAAAQgOAJQgNAJgYAAQgLAAgLgDgAgUAjQgHADAAAIQAAAOAbAAQALAAAIgEQAHgEAAgGQAAgDgDgEIgHgFIgJgCIgJgBQgMABgGADgAgOg1QgGAGAAAHQAAAJAGAEQAFAGAJAAQAHAAAGgGQAFgFAAgIQAAgIgFgFQgGgFgHAAQgJAAgFAFg");
	this.shape_47.setTransform(509.5,13.3);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#726C50").s().p("AAVA1IAAg9QAAgMgFgGQgFgGgIAAQgFAAgGAEQgGAEgHAIIAABFIgbAAIAAhnIAbAAIAAAOIABgBQAOgPASAAQAPAAALAKQALALgBAUIAABAg");
	this.shape_48.setTransform(497.8,10.5);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#726C50").s().p("AgNBLIAAhmIAaAAIAABmgAgKgxQgEgEAAgHQAAgHAEgEQAEgDAGAAQAGAAAEADQAFAFAAAGQAAAHgFAEQgEAEgGABQgFgBgFgEg");
	this.shape_49.setTransform(488.8,8.3);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#726C50").s().p("AAUBOIgug2IAqgxIAiAAIgsAxIAwA2gAg2BOIAAibIAcAAIAACbg");
	this.shape_50.setTransform(480.9,8);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#726C50").s().p("AAUA1IAAg9QABgMgFgGQgFgGgIAAQgFAAgGAEQgHAEgGAIIAABFIgbAAIAAhnIAbAAIAAAOIABgBQAOgPASAAQAPAAALAKQAKALAAAUIAABAg");
	this.shape_51.setTransform(467.9,10.5);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#726C50").s().p("AAIArQgMALgRAAQgNAAgIgIQgHgHAAgNQAAgGADgGQACgEAGgEIAMgGIAQgFIANgFIAFgBIAAgFQAAgJgFgDQgDgEgKAAQgJAAgJAEQgIADgJAKIAAgYQAQgOAZAAQATAAAKAJQAKAIAAATIAAAtQAAAGAFAAQADAAAHgEIAAAQIgNAGQgFACgHAAQgPAAgCgLgAgNAKQgJAGAAAHQAAAGAEAEQAEADAGAAQAIAAAIgHIAAgcQgJADgMAGg");
	this.shape_52.setTransform(456.2,10.6);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#726C50").s().p("Ag8BOIAAibIA1AAQANAAAKADQAKADAHAGQAGAEAFAIQAEAHAAAJQAAAXgXAJIAAABIAAAAQASAEAIAIQAKALgBAPQAAAKgDAIQgEAIgHAFQgHAGgLADQgKAEgRAAgAgcA1IAbAAQAPAAAHgGQAHgGAAgJQAAgLgIgFQgIgGgOAAIgaAAgAgcgMIASAAQAZAAAAgVQAAgJgFgFQgHgGgKAAIgVAAg");
	this.shape_53.setTransform(443.9,8);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#726C50").s().p("AgTAxQgLgEgGgHQgHgHgDgLQgEgKAAgKQAAgLAEgLQAEgKAHgHQAHgHAJgEQAJgDALAAQAWAAAPAOQANAPAAAbIAAABIhKAAQACAOAJAIQAJAHANAAQATAAAUgNIAAAUQgLAIgKACQgNAEgMAAQgLAAgLgFgAgOgbQgGAFgCANIAuAAQgBgLgGgGQgGgHgKAAQgJAAgGAGg");
	this.shape_54.setTransform(423.8,10.7);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#726C50").s().p("AgOA5QgJgKAAgUIAAgsIgRAAIAAgFIAogsIADAAIAAAcIAjAAIAAAVIgjAAIAAArQAAALAEAEQAFAEAGAAQAKAAANgGIAAAWIgPAEIgOACQgSAAgIgKg");
	this.shape_55.setTransform(413.5,9.4);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#726C50").s().p("AAIArQgMALgRAAQgNAAgIgIQgHgHAAgNQAAgGADgGQACgEAGgEQAGgFAGgBIAdgKIAEgBIABgCIAAgDQAAgJgFgDQgEgEgJAAQgJAAgJAEQgJAEgJAJIABgYQAQgOAZAAQASAAALAJQAKAIAAATIAAAtQAAAGAEAAQAEAAAHgEIAAAQQgKAFgDABQgFACgHAAQgPAAgCgLgAgNAKQgJAGAAAHQAAAGAEAEQAEADAGAAQAIAAAHgHIAAgcQgIADgMAGg");
	this.shape_56.setTransform(404,10.6);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#726C50").s().p("AgEA1IgxhpIAdAAIAYA7IAZg7IAdAAIguBpg");
	this.shape_57.setTransform(392.9,10.7);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#726C50").s().p("AgMBLIAAhmIAZAAIAABmgAgJgxQgFgEAAgHQAAgGAFgFQAEgDAFAAQAGAAAEADQAFAFAAAGQAAAHgFAEQgEAEgGABQgFgBgEgEg");
	this.shape_58.setTransform(384.9,8.3);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#726C50").s().p("AgoA1IAAhnIAcAAIAAAaIAAAAQAJgPAEgGQAIgHAJAAQAFAAAGADIAMAHIgKAYIgJgGQgFgCgEAAQgLAAgFAIQgHAIgCANIAAAyg");
	this.shape_59.setTransform(378.1,10.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#726C50").s().p("Ag2BOIAAibIA2AAQAbAAAOANQAOAMAAAVQAAAWgPAKQgOAMgdAAIgTAAIAABBgAgWgKIAWAAQAJAAAHgFQAHgFAAgLQAAgIgGgHQgGgGgLAAIgWAAg");
	this.shape_60.setTransform(366.5,8);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#726C50").s().p("AgWAyQgKgEgIgIQgIgHgEgKQgEgKAAgLQAAgKAEgLQAEgJAIgIQAJgIAJgDQAKgEAMAAQANAAAKAEQAKAEAIAHQAIAIAEAJQAEALAAAKQAAALgEAKQgEAKgIAIQgIAHgKAEQgLAEgMAAQgKAAgMgEgAgUgWQgIAJAAANQAAAOAIAJQAIAJAMAAQANAAAIgJQAIgJAAgOQAAgGgCgGQgCgGgEgEQgEgEgGgDQgGgCgFAAQgLAAgJAJg");
	this.shape_61.setTransform(346.4,10.6);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#726C50").s().p("AgDAsQgKgJAAgSIAAgvIgjAAIAAgWIBhAAIAAAWIglAAIAAAtQAAAIAFAFQAEADAHAAIAJgBQAEAAAEgDIAAAVIgKAEIgMABQgRAAgJgJg");
	this.shape_62.setTransform(334.7,10.7);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#726C50").s().p("AgcAxQgJgDgJgIQgIgIgEgJQgEgLAAgKQAAgKAEgKQAEgKAHgHQAJgIAJgDQAJgEAOAAIBFAAIAAAWIgcAAQAJAFADAJQADAIAAAIQAAALgEAKQgEAJgIAIQgIAHgKAEQgLAEgKAAQgMAAgLgEgAgagWQgIAKAAAMQAAAOAIAJQAHAIANAAQANAAAHgIQAIgJAAgOQAAgNgIgJQgJgIgLAAQgMAAgIAIg");
	this.shape_63.setTransform(322.8,10.7);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#726C50").s().p("AgXAxQgKgDgFgIQgGgHgEgKQgEgJAAgMQAAgMAEgJQACgJAHgIQAHgHAIgEQAIgEAMAAQAIAAAGADQAJACAHAFIAAgIIAbAAIAABmIgcAAIAAgMQgGAJgJACQgKAEgEAAQgKAAgJgFgAgQgXQgIAJAAAOQAAAOAIAJQAIAJALAAQAFAAAHgDQAEgDAGgIIAAgpIgLgHIgMgCQgLAAgHAJg");
	this.shape_64.setTransform(302.6,10.7);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#726C50").s().p("AgDAsQgKgJAAgSIAAgvIgjAAIAAgWIBhAAIAAAWIglAAIAAAtQAAAIAFAFQAEADAHAAIAJgBQAEAAAEgDIAAAVIgKAEIgMABQgRAAgJgJg");
	this.shape_65.setTransform(291.3,10.7);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#726C50").s().p("AgdAxQgIgDgJgIQgIgIgEgJQgEgLAAgKQAAgKAEgKQAEgKAHgHQAJgIAJgDQAJgEAOAAIBFAAIAAAWIgcAAQAJAFADAJQADAIAAAIQAAALgEAKQgEAJgIAIQgIAHgKAEQgLAEgKAAQgMAAgMgEgAgagWQgIAKAAAMQAAAOAIAJQAHAIANAAQANAAAHgIQAIgJAAgOQAAgNgIgJQgJgIgLAAQgMAAgIAIg");
	this.shape_66.setTransform(279.4,10.7);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#726C50").s().p("AgMA0IAAhnIAZAAIAABng");
	this.shape_67.setTransform(269.6,10.6);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#726C50").s().p("Ag2BPIAAhmQAAgPAFgKQAFgKAIgHQAHgFAMgFQAKgDAIAAQAIAAAKADQAKAEAHAGQAHAFAGALQAGALAAAPQAAANgGAKQgGAKgHAFQgIAIgJACQgKAEgIAAQgHAAgHgEQgIgCgFgHIAAA/gAAAg4QgLAAgIAJQgHAIAAAPQAAAJACAGQACAFAEAEQADACAFADIAKACQALAAAIgHQAHgIAAgQQAAgGgCgIQgCgGgEgEQgDgDgFgDQgFgCgFAAIAAAAg");
	this.shape_68.setTransform(260.4,13.2);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#726C50").s().p("AgXBJQgJgDgHgIQgHgIgCgJQgEgJAAgNQAAgNAEgJQACgHAHgIQAHgIAIgDQAIgEAMAAQAIAAAGADQAHACAJAFIAAgIIAbAAIAABmIgcAAIAAgMQgGAJgJACQgKAEgEAAQgKAAgJgFgAgQAAQgIAIAAAPQAAAPAIAJQAIAJALAAQAFAAAHgDQAFgEAFgHIAAgqIgLgGIgMgCQgLAAgHAIgAgDgpIASgkIAYAAIgeAkg");
	this.shape_69.setTransform(247,8.3);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#726C50").s().p("AgOA0IgQgFQgHgEgEgGQgFgGAAgIQAAgEACgEQABgFAEgCIAJgHQAGgBAGgBQgMgCgGgGQgFgHgBgIQAAgHAEgGQAEgFAIgEQAKgEAGgBIAPgCIALABIAJACIAJADIAKAFIAAAVIgMgGIgKgEIgJgBIgNAAIgEACIgFADQgCACAAAEQAAAEABACIAFAEIAFABIAaAAIAAAPIgYAAIgHACQgFACgCACQgDADAAAFIABAGIAEAEIAHADIANAAIAKgCIAMgEIAPgHIAAAVIgMAHIgKAEIgLACIgMABg");
	this.shape_70.setTransform(229,10.6);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#726C50").s().p("AgxBOIAAibIAbAAIAAA3QABAOAFAIQAGAJAKAAQAKgBAGgHQAGgHAAgOIAAg5IAcAAIAABmIgaAAIAAgNIgBAAQgCAHgHAFQgGAEgIAAQgNAAgIgOIgBAAIAABAg");
	this.shape_71.setTransform(217,13.3);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#726C50").s().p("AgSAyQgKgDgGgFQgGgGgEgJQgEgIAAgNIAAg6IAcAAIAAA5QAAANAGAGQAGAHAIAAQAJAAAGgHQAGgGAAgOIAAg4IAcAAIAAA4QAAAYgOAMQgNANgWAAQgIAAgKgDg");
	this.shape_72.setTransform(204.1,10.7);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#726C50").s().p("AgWAyQgKgEgIgIQgIgGgEgLQgEgLAAgKQAAgKAEgLQAEgJAIgIQAIgHAKgEQAJgEANAAQANAAAKAEQAJAEAJAHQAIAIAEAJQAEALAAAKQAAAMgEAJQgEAKgIAIQgIAHgKAEQgLAEgMAAQgLAAgLgEgAgUgWQgIAIAAAOQAAAPAIAIQAIAJAMAAQANAAAIgJQAIgIAAgPQAAgHgCgFQgDgGgEgEQgDgFgGgCQgGgCgFAAQgMAAgIAJg");
	this.shape_73.setTransform(191.6,10.6);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#726C50").s().p("AgEA1IgxhpIAdAAIAYA7IAZg7IAdAAIgvBpg");
	this.shape_74.setTransform(179.7,10.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#726C50").s().p("Ag2BPIAAhmQAAgPAFgKQAFgKAIgHQAHgFAMgFQAKgDAIAAQAJAAAJADQAKAEAHAGQAHAGAGAKQAGALAAAPQAAANgGAKQgGAKgHAFQgIAIgJACQgJAEgJAAQgHAAgHgEQgIgCgFgHIAAA/gAAAg4QgLAAgIAJQgHAJAAAOQAAAJACAGQACAFAEAEQADACAFADIAKACQALAAAIgHQAHgIAAgQQAAgIgCgGQgCgGgEgEQgDgDgFgDQgFgCgFAAIAAAAg");
	this.shape_75.setTransform(168,13.2);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#726C50").s().p("AgWBNIAAhnIAZAAIAABngAgSgoIASgkIAXAAIgbAkg");
	this.shape_76.setTransform(159.2,8.1);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#726C50").s().p("AgXAxQgKgDgFgIQgGgHgEgKQgEgJAAgMQAAgMAEgJQACgJAHgIQAIgIAIgDQAHgEAMAAQAIAAAGADQAJACAHAFIAAgIIAbAAIAABmIgcAAIAAgMQgGAJgJACQgJAEgFAAQgKAAgJgFgAgQgXQgHAJAAAOQAAAOAHAJQAIAJALAAQAFAAAHgDQAGgEAEgHIAAgpIgLgHIgMgCQgLAAgHAJg");
	this.shape_77.setTransform(148.8,10.7);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#726C50").s().p("AATA0IAAhRIglAAIAABRIgcAAIAAhRIgPAAIAAgWIB7AAIAAAWIgPAAIAABRg");
	this.shape_78.setTransform(136.1,10.6);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#726C50").s().p("AgXAxQgKgDgFgIQgGgHgEgKQgEgJAAgMQAAgMAEgJQACgJAHgIQAIgIAHgDQAIgEAMAAQAIAAAGADQAJACAHAFIAAgIIAbAAIAABmIgcAAIAAgMQgGAJgJACQgKAEgEAAQgKAAgJgFgAgQgXQgIAJAAAOQAAANAIAKQAIAJALAAQAFAAAHgDQAGgEAEgHIAAgpIgLgHIgMgCQgLAAgHAJg");
	this.shape_79.setTransform(116.4,10.7);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#726C50").s().p("AgMA0IAAhnIAZAAIAABng");
	this.shape_80.setTransform(107.5,10.6);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#726C50").s().p("AgFA1IgwhpIAdAAIAYA7IAag7IAcAAIguBpg");
	this.shape_81.setTransform(99.4,10.7);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#726C50").s().p("AgWBKQgKgEgIgIQgIgHgEgKQgEgKAAgMQAAgLAEgLQAEgHAIgIQAIgHAKgEQAKgEAMAAQANAAAKAEQAKAEAIAHQAIAIAEAHQAEALAAALQAAAMgEAKQgEAKgIAIQgIAHgKAEQgLAEgMAAQgLAAgLgEgAgUAAQgIAJAAAOQAAAPAIAJQAIAJAMAAQANAAAIgJQAIgJAAgPQAAgHgCgGQgCgGgEgEQgEgCgGgDQgGgCgFAAQgLAAgJAHgAgFgpIASgkIAYAAIgeAkg");
	this.shape_82.setTransform(87.6,8.2);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#726C50").s().p("Ag2BPIAAhmQAAgPAFgKQAGgKAIgHQAHgGALgEQAKgDAIAAQAJAAAJADQAJADAIAHQAHAGAGAKQAGAJAAARQAAAOgGAJQgGAKgHAFQgJAIgIACQgJAEgJAAQgHAAgHgEQgIgCgFgHIAAA/gAAAg4QgLAAgIAJQgHAJAAAOQAAAJACAGQACAFAEAEQAEADAFACQAGACADAAQALAAAIgHQAHgIAAgQQAAgIgCgGQgCgGgEgEQgDgDgFgDQgFgCgFAAIAAAAg");
	this.shape_83.setTransform(74.7,13.2);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#726C50").s().p("AAbA0IgbgiIgbAiIghAAIArg0IgrgzIAiAAIAaAhIAZghIAhAAIgqAzIAtA0g");
	this.shape_84.setTransform(61.8,10.6);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#726C50").s().p("AgUBKQgKgFgIgLQgGgJgFgQQgEgQAAgRQAAgQAEgQQAFgQAGgKQAIgLAKgFQAJgFALAAQANAAAJAFQAJAFAIALQAHANAEANQAEAQAAAQQAAARgEAQQgEANgHAMQgHALgKAFQgKAGgMAAQgKAAgKgGgAgQgoQgGAPAAAZQAAAaAGAOQAGAOAKABQALgBAHgOQAGgOAAgaQAAgZgGgPQgHgOgLAAQgKAAgGAOg");
	this.shape_85.setTransform(43.2,8.1);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#726C50").s().p("AgNBOIAAibIAbAAIAACbg");
	this.shape_86.setTransform(31,8);

	this.addChild(this.shape_86,this.shape_85,this.shape_84,this.shape_83,this.shape_82,this.shape_81,this.shape_80,this.shape_79,this.shape_78,this.shape_77,this.shape_76,this.shape_75,this.shape_74,this.shape_73,this.shape_72,this.shape_71,this.shape_70,this.shape_69,this.shape_68,this.shape_67,this.shape_66,this.shape_65,this.shape_64,this.shape_63,this.shape_62,this.shape_61,this.shape_60,this.shape_59,this.shape_58,this.shape_57,this.shape_56,this.shape_55,this.shape_54,this.shape_53,this.shape_52,this.shape_51,this.shape_50,this.shape_49,this.shape_48,this.shape_47,this.shape_46,this.shape_45,this.shape_44,this.shape_43,this.shape_42,this.shape_41,this.shape_40,this.shape_39,this.shape_38,this.shape_37,this.shape_36,this.shape_35,this.shape_34,this.shape_33,this.shape_32,this.shape_31,this.shape_30,this.shape_29,this.shape_28,this.shape_27,this.shape_26,this.shape_25,this.shape_24,this.shape_23,this.shape_22,this.shape_21,this.shape_20,this.shape_19,this.shape_18,this.shape_17,this.shape_16,this.shape_15,this.shape_14,this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,549.3,44.6);


(lib.teleies = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Path();
	this.instance.setTransform(0,-4.8);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-4.8,558,2);


(lib.Symbol6 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AFMAPIALAAIAAASIgLAIgAj/AXQgFgFABgKIAKAAQABAGABABQACACADAAQADAAACgCQACgCAAgFIgBgIQgBAAgEgBIgIgDQgFgDgDgDQgBgDAAgIQAAgJAFgGQAEgDAIAAQAGAAAFADQAFAFAAALIgLAAQAAgGgBgCQgCgCgDAAQgBAAAAAAQgBAAAAAAQgBABAAAAQgBAAgBABQgBADAAAEIABAFQABACAEACIAIADQAFACACAFQADACAAAHQAAALgFAFQgFAEgIAAQgIAAgFgEgAE3AaIgLgaIgEAJIAAARIgLAAIAAhBIALAAIAAAfIAOgfIALAAIgOAcIAQAlgAEHAaIgQglIAAAlIgLAAIAAhBIAKAAIAQAnIAAgnIAKAAIAABBgADZAaIgDgNIgQAAIgCANIgMAAIARhBIAKAAIAQBBgADUAEIgGgaIgGAaIAMAAgACPAaIAAhBIARAAQAJAAAEAEQAFAFAAAKQAAAHgEAEIgDACIADADQAEADAAAIQAAALgFAEQgEAFgJgBgACaARIAFAAQAIAAAAgKQAAgJgIAAIgFAAgACagLIAFAAQAIAAgBgJQABgKgIABIgFAAgABTAaIAAhBIAcAAIAAAKIgRAAIAAATIAPAAIAAAJIgPAAIAAASIARAAIAAAJgAA0AaIAAg3IgMAAIAAgKIAhAAIAAAKIgKAAIAAA3gAAcAaIgDgNIgPAAIgDANIgJAAIAOhBIAKAAIARBBgAAXAEIgFgaIgHAaIAMAAgAgbAaIgPhBIALAAIAJAxIAIgxIAMAAIgOBBgAg+AaIAAhBIALAAIAABBgAhTAaIgJgaIgGAAIAAAaIgKAAIAAhBIAQAAQASAAAAAUQAAANgIAFIAKAbgAhigJIAGAAQAHAAAAgKQAAgKgHAAIgGAAgAiZAaIAAhBIARAAQARAAAAAUQAAATgRAAIgGAAIAAAagAiOgIIAGAAQAGABAAgMQAAgLgGABIgGAAgAjNAaIAAg3IgLAAIAAgKIAhAAIAAAKIgMAAIAAA3gAkoAaIAAhBIAcAAIAAAKIgSAAIAAATIAQAAIAAAJIgQAAIAAASIASAAIAAAJgAlWAaIAAhBIAQAAQAKAAAEAEQAEAFAAAKQAAAHgEAEIgDACIADADQAEADAAAIQAAALgEAEQgEAFgKgBgAlLARIAEAAQAJAAgBgKQABgJgJAAIgEAAgAlLgLIAEAAQAJAAgBgJQABgKgJABIgEAAg");
	this.shape.setTransform(47.5,46.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AA0AcQgFgFAAgXQAAgWAFgFQAEgGAJAAQAHAAAFAEQAEAFABAKIgLAAQAAgFgBgCQgCgCgDAAQgEAAgBACQgCADAAASQAAASACAEQABACAEAAQAEAAABgCIACgHIAKAAQgBAKgEAEQgFAFgHAAQgIAAgFgGgAhzAcQgFgGAAgWQAAgWAFgFQAFgGAIAAQAJAAADAFQAFAEABAKIgLAAQgBgJgGAAQgDAAgCACQgCADAAASQAAASACAEQACACADAAQAHAAAAgIIAAgLIgHAAIAAgHIASAAIAAARQAAAJgFAFQgFAFgIAAQgHAAgGgGgABcAhIAAhBIAdAAIAAAJIgSAAIAAATIAPAAIAAAIIgPAAIAAATIASAAIAAAKgAAKAhIAAhBIAcAAIAAAJIgRAAIAAATIAPAAIAAAIIgPAAIAAATIARAAIAAAKgAgbAhIAAhBIAbAAIAAAJIgQAAIAAATIAPAAIAAAIIgPAAIAAATIAQAAIAAAKgAgwAhIgJgcIgGAAIAAAcIgKAAIAAhBIARAAQARAAAAAUQAAAMgIADIAKAegAg/gCIAHAAQAGAAAAgKQAAgLgGAAIgHAAg");
	this.shape_1.setTransform(47.5,55.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#006389").s().p("AnLCHIgOgNIAAjzIAOgNIOXAAIAOANIAADzIgOANg");
	this.shape_2.setTransform(47.5,50.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#006389").s().p("ABaBCQgKgJAAgSIAAgFIAcAAIAAAKQABAKAIAAQAFAAACgDQADgEgBgWQABgTgCgDQgDgDgFAAQgIAAgBAJIAAABIgbAAIAAhTIBIAAIAAAZIguAAIAAAhQAGgGACgBQAGgDAFAAQAOAAAIAJQAHAJAAAdIgBAZQgDAMgHAHQgKAKgRAAQgPAAgMgKgAg7BCQgLgJAAgSIAAhNQAAgSALgJQAKgKAQAAQARAAAKAKQAJAJAAASIAABNQAAASgJAJQgKAKgRAAQgQAAgKgKgAgpgoIAABRQAAAKAIAAQAJAAAAgKIAAhRQAAgKgJAAQgIAAAAAKgAAmBKIAAhyIgXAQIAAggIAXgRIAcAAIAACTgAiYBKIAAgYIAmhBQAGgKAAgOQAAgLgIAAQgKAAAAAKIAAAKIgcAAIAAgIQAAgSAKgJQALgKARAAQAQAAAJAJQALAJAAATQAAASgIANIgiA4IAqAAIAAAZg");
	this.shape_3.setTransform(47.8,77.2);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#252524").s().p("AgtAhQgGgHABgaQgBgaAGgGQAEgGAKAAQAIAAAEAGQAGAGAAAaQAAAagGAHQgFAFgHABQgJgBgFgFgAgmgaQgDADAAAXQAAAXADAEQADADAEAAQADAAADgDQADgEAAgXQAAgXgDgDQgEgEgCAAQgEAAgDAEgAiUAhQgEgFgBgIIAAg5IAJAAIAAA5QAAAKAJAAQAJAAgBgKIAAg5IAKAAIAAA5QAAAIgFAFQgGAGgHAAQgHAAgGgGgAC7AmIgNgjIgHANIAAAWIgJAAIAAhLIAJAAIAAAlIATglIAJAAIgPAgIASArgACFAmIgUgzIAAAzIgJAAIAAhLIAIAAIAUAyIAAgyIAJAAIAABLgABRAmIgEgSIgRAAIgDASIgKAAIAShLIAJAAIARBLgABMALIgHghIgIAhIAPAAgAABAmIAAhLIAQAAQAJAAAFAFQAFAFAAALQAAAIgFAFIgEADIAEABQAFAFAAAJQAAANgFAFQgFAEgJABgAAKAeIAGAAQALgBAAgOQAAgNgLAAIgGAAgAAKgEIAGAAQAKAAAAgMQAAgNgKAAIgGAAgAhJAmIgKgiIgJAAIAAAiIgJAAIAAhLIARAAQASAAAAAWQABAPgKADIAMAjgAhcgCIAIAAQAKAAgBgNQABgOgKAAIgIAAgAjEAmIAAhLIAdAAIAAAIIgUAAIAAAaIARAAIAAAGIgRAAIAAAbIAUAAIAAAIg");
	this.shape_4.setTransform(47.3,95.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#252524").s().p("ACVAmQgPgOAAgXQAAgVAOgOQAPgPAXAAQAHAAAQAEQAIADABgGIACAAIADAgIgDAAQgCgNgJgHQgKgIgMABQgRAAgLANQgIAMgBASQAAAUALAMQAJANASAAQATAAAQgTIADACQgQAWgYAAQgWAAgPgMgADmAwIAAgDQAJAAAEgDQACgDAAgIIAAg9QAAgJgCgCQgEgDgJAAIAAgDIBVAAIABAWIgEAAQgBgQgSAAIgaAAQgEAAgBACQgBACAAAGIAAAbIAdAAQAIAAACgDQADgDABgIIADAAIAAAfIgDAAQgCgNgMAAIgdAAIAAAeQAAAIABABQABADAFAAIAdAAQASAAAGgTIADABIgHAYgAB4AwIhChMIAAA1QAAAMACADQACAEAJABIAAADIgiAAIAAgDQAKgBADgEQABgDAAgMIAAg5QAAgEgFgEQgEgDgGgBIAAgDIAZAAIA7BDIAAgsQAAgMgCgDQgCgFgKAAIAAgDIAiAAIAAADQgJAAgDAFQgCADAAAMIAABIgAgNAwIAAgDIABAAQAJAAAAgGQAAgEgJgTIgqAAIgFAMIgDALQAAAGAKAAIABAAIAAADIghAAIAAgDQAGgBADgCQADgCADgHIAnhRIAEAAIAlBRQAFAMAKAAIAAADgAgOAKIgTgmIgSAmIAlAAgAhdAwIhChMIAAA1QAAAMABADQADAEAJABIAAADIghAAIAAgDQAJgBADgEQABgDAAgMIAAg5QAAgEgEgEQgFgDgGgBIAAgDIAZAAIA7BDIAAgsQAAgMgCgDQgDgFgJAAIAAgDIAiAAIAAADQgJAAgDAFQgCADAAAMIAABIgAjrAwIAAgDQAIAAAEgDQADgDAAgIIAAg9QAAgJgDgCQgEgDgIAAIAAgDIAtAAIAAADQgJAAgDADQgDACAAAJIAAA9QAAAIADADQADADAJAAIAAADgAlDAwIAAgDQAJAAADgDQADgDAAgIIAAg9QAAgJgDgCQgDgDgJAAIAAgDIBRAAIACAXIgDAAQgCgJgDgEQgEgEgHAAIgcAAQgEAAgBACIgBAIIAAAbIAWAAQANAAABgOIACAAIAAAgIgCAAQgBgIgCgDQgDgDgIAAIgWAAIAAAfQAAAIADADQAEADAJAAIAAADg");
	this.shape_5.setTransform(47,26.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#252524").s().p("AiUAsQgSgRAAgbQAAgZASgSQAQgRAbAAQAcAAARARQARASAAAZQABAbgTARQgRARgbAAQgaAAgRgRgAiIglQgJAOAAAXQAAAWAIAPQALARAVAAQAXAAAKgRQAJgPAAgWQAAgXgKgOQgLgRgVAAQgUAAgLARgAjSA7IgchJIgbBJIgEAAIgkhaQgEgMgDgEQgEgHgIAAIAAgDIAuAAIAAADQgIABgCABQgCACAAADIAcBKIAAAAIATgyIgDgKQgFgNgCgDQgEgEgJgBIAAgDIAwAAIAAADQgOABAAAGIADAHIAZBDIABAAIAYhDIABgHQAAgFgLgCIAAgDIAgAAIAAADQgHABgDAFIgFANIgjBegADFA6IAAgDQALAAAEgDQADgEAAgKIAAhLQAAgKgDgEQgEgDgLAAIAAgDIA8AAQAdAAARAMQAVAPAAAeQAAAfgYAPQgTAMgZAAgADrgwQgBACAAAPIAABGQAAAIABACQADADAJAAQAbAAAOgNQAQgOAAgZQAAgXgPgOQgPgOgYAAQgNAAgCADgABYA6IAAgDQAKgBADgDQAEgDAAgKIAAhMQAAgJgEgDQgDgDgKgBIAAgDIA2AAIAAADQgLAAgEADQgEAEAAAKIAABJQABAIABADQACAEAGAAIAZAAQANAAAIgGQAHgFAGgNIAEABIgKAegAAxA6Igsg0IgOAAIAAAgQAAAKAEAEQADADAJAAIAAADIg1AAIAAgDQAKAAAFgDQAEgEgBgKIAAhLQABgKgEgEQgFgDgKAAIAAgDIA2AAQAVAAALAEQAPAGAAASQAAAagfAEIAlAsQAGAHAJADIAAADgAgHgxQgCABAAAGIAAAqIAJAAQAiAAAAgaQAAgZggAAQgHAAgCACg");
	this.shape_6.setTransform(47,14.2);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f().s("#9A9A9A").ss(0.6,0,0,4).p("AlwoTIgcAbIAAPxIAcAbILiAAIAbgbIAAvxIgbgbg");
	this.shape_7.setTransform(47,53.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AlwIUIgcgbIAAvxIAcgbILiAAIAbAbIAAPxIgbAbg");
	this.shape_8.setTransform(47,53.3);

	this.addChild(this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,94.9,106.6);


(lib.Symbol5 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.bravio_5();
	this.instance.setTransform(-3.9,0,1.015,1.015);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-3.9,0,66,66);


(lib.Symbol4 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.bravio_4();
	this.instance.setTransform(1.5,4.5,0.877,0.877);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(1.5,4.5,57,57);


(lib.Symbol3 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.bravio_3();
	this.instance.setTransform(2.5,5,0.877,0.877);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(2.5,5,57,57);


(lib.Symbol2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.bravio_2();
	this.instance.setTransform(0,0,1.015,1.015);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,66,66);


(lib.Symbol1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.bravio_1();
	this.instance.setTransform(0,0,1.015,1.015);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,66,66);


(lib.oroi = function() {
	this.initialize();

	// Layer 1
	this.text = new cjs.Text("Βάσει των διακρίσεων ως “Best Private Bank in Greece” από τα περιοδικά Euromoney, World Finance, PWM και The Banker.", "11px Arial", "#252524");
	this.text.lineHeight = 17;

	this.addChild(this.text);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,617.1,16.4);


(lib.tonosshine = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AhsDpIgFgIIACgIIAEgCIgBgBQgFgIAIghIABgEIANgBIgBgIIABgBQAEgNAQgIIgBgMIAHgDIACgGQANgWAIgFIgCgCIAGgSIAJACIAAgCIgEgNIAAgBQAEgJALAAIgEgLIAKgcIALACIABACIAMgmIgMgDIAPg5IgPAAIgGgBIgCgCQggguAIgXQAdhXAyAMIAOADIAGAJIAUAAIALADQAcASgUBNIAAACIgGADIAAABIgCAVIgBAAIgDAIIgZATQgCAIACADIAEACIAFACIgKAdIgDABIACACIgGASIgFgBIAAABIAAADQhACIhGBSIgCADIgDgBQgFgBgDALIAAACIgJAEgAAPgxIAFgMIgBAAg");
	mask.setTransform(-24.2,-27.2);

	// Shine
	this.instance = new lib.Tween1("synched",0);
	this.instance.setTransform(-155.9,-29.4,1.081,1.081);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:53.2,y:-29.8},47).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-199.1,-55.9,86.5,53);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;