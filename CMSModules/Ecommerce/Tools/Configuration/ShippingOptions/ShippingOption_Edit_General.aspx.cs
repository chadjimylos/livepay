using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_ShippingOptions_ShippingOption_Edit_General : CMSEcommerceConfigurationPage
{
	protected int shippingOptionID = 0;
	
	protected void Page_Load(object sender, EventArgs e)
	{
        // Required field validator error messages initialization
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvName.ErrorMessage = ResHelper.GetString("COM_ShippingOption_Edit.NameError");
        txtShippingOptionCharge.EmptyErrorMessage = ResHelper.GetString("COM_ShippingOption_Edit.ChargeError");
        txtShippingOptionCharge.ValidationErrorMessage = ResHelper.GetString("COM_ShippingOption_Edit.ChargePositive");
				
		// control initializations				
		//lblShippingOptionSiteID.Text = ResHelper.GetString("COM_ShippingOption_Edit.ShippingOptionSiteIDLabel");
		lblShippingOptionDisplayName.Text = ResHelper.GetString("COM_ShippingOption_Edit.ShippingOptionDisplayNameLabel");
		lblShippingOptionCharge.Text = ResHelper.GetString("COM_ShippingOption_Edit.ShippingOptionChargeLabel");
		lblShippingOptionName.Text = ResHelper.GetString("COM_ShippingOption_Edit.ShippingOptionNameLabel");

		btnOk.Text = ResHelper.GetString("General.OK");	

		string currentShippingOption = ResHelper.GetString("COM_ShippingOption_Edit.NewItemCaption");
		// get shippingOption id from querystring		
        shippingOptionID = ValidationHelper.GetInteger(Request.QueryString["ShippingOptionID"], 0);
        
        //edit shiping option
        if (shippingOptionID > 0)
        {
            ShippingOptionInfo shippingOptionObj = ShippingOptionInfoProvider.GetShippingOptionInfo(shippingOptionID);
            if (shippingOptionObj != null)
            {
                currentShippingOption = shippingOptionObj.ShippingOptionDisplayName;

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(shippingOptionObj);
                    // edit item
                    // show that the shippingOption was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
        }

	
	}


	/// <summary>
	/// Load data of editing shippingOption.
	/// </summary>
	/// <param name="shippingOptionObj">ShippingOption object.</param>
	protected void LoadData(ShippingOptionInfo shippingOptionObj)
	{
	
		//txtShippingOptionSiteID.Text = Convert.ToString(shippingOptionObj.ShippingOptionSiteID);
		txtShippingOptionDisplayName.Text = shippingOptionObj.ShippingOptionDisplayName;
		txtShippingOptionCharge.Value = shippingOptionObj.ShippingOptionCharge;
		txtShippingOptionName.Text = shippingOptionObj.ShippingOptionName;
		chkShippingOptionEnabled.Checked = shippingOptionObj.ShippingOptionEnabled;
	 }


	/// <summary>
	/// Sets data to database.
	/// </summary>
	protected void btnOK_Click(object sender, EventArgs e)
	{
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        string errorMessage = new Validator()
            .NotEmpty(txtShippingOptionDisplayName.Text, rfvDisplayName.ErrorMessage)
            .NotEmpty(txtShippingOptionName.Text, rfvName.ErrorMessage).Result;

        if (!ValidationHelper.IsCodeName(txtShippingOptionName.Text))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

        if (errorMessage == "")
        {
            errorMessage = txtShippingOptionCharge.ValidatePrice(false);
        }

		if (errorMessage == "")
		{
            // Check unique name
			ShippingOptionInfo shippingOptionObj = ShippingOptionInfoProvider.GetShippingOptionInfo(txtShippingOptionName.Text.Trim(), CMSContext.CurrentSiteName);

			if ((shippingOptionObj == null) || (shippingOptionObj.ShippingOptionID == shippingOptionID))			
			{
                // Get object
				if ((shippingOptionObj == null))
				{
					shippingOptionObj = ShippingOptionInfoProvider.GetShippingOptionInfo(shippingOptionID);
					if (shippingOptionObj == null)
					{
						shippingOptionObj = new ShippingOptionInfo();                        
					}			        									
				}

                if (CMSContext.CurrentSite != null)
                {
                    shippingOptionObj.ShippingOptionSiteID = CMSContext.CurrentSite.SiteID;
                }
				shippingOptionObj.ShippingOptionDisplayName = txtShippingOptionDisplayName.Text.Trim();
                shippingOptionObj.ShippingOptionCharge = txtShippingOptionCharge.Value;
				shippingOptionObj.ShippingOptionName = txtShippingOptionName.Text.Trim();
				shippingOptionObj.ShippingOptionEnabled = chkShippingOptionEnabled.Checked;
                shippingOptionObj.ShippingOptionSiteID = CMSContext.CurrentSite.SiteID;
				
				ShippingOptionInfoProvider.SetShippingOptionInfo(shippingOptionObj);

				UrlHelper.Redirect("ShippingOption_Edit_General.aspx?ShippingOptionID=" + Convert.ToString(shippingOptionObj.ShippingOptionID) + "&saved=1");
			}
			else
			{
				lblError.Visible = true;
				lblError.Text = ResHelper.GetString("ShippingOption_Edit.ShippingOptionNameExists");
			}
		}
		else
		{
			lblError.Visible = true;
			lblError.Text = errorMessage;
		}
	}
}
