﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCTransDetails.ascx.vb" Inherits="CMSTemplates_LivePay_PopUp_UCTransDetails" %>

<script>
    function ExportDetails(url) {
        document.getElementById('<%=exportpopFrame.ClientID %>').src = url;
        return false
    }
</script>

<div style="display:none">
<iframe id="exportpopFrame" runat="server" ></iframe>
</div>
<table cellpadding="0" cellspacing="0" border="0" class="PopUpMainTable">
    <tr>
	<td class="PopTopLeft">&nbsp;</td>
        <td class="PopTopCenter">&nbsp;</td>
        <td class="PopTopClose"><img src="/App_Themes/LivePay/PopUp/Close.png"  alt="Close" title="Close" style="cursor:pointer;" onclick="$.unblockUI()"/></td>
	<td class="PopTopRight">&nbsp;</td>  
    </tr>
    <tr>
	<td class="PopCenterLeft">&nbsp;</td>
	<td colspan="2" class="PopCenter">
        <table cellpadding="0" cellspacing="0">
            <tr><td class="PopTopTitle" id="PageTitle" runat="server"></td></tr>
            <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>
            <div id="MainForm" runat="server">
            <tr id="ReportRow" runat="server" style="display:none"><td>
               <table cellpadding="0" cellspacing="0">
                    <tr><td class="PopTopSep">&nbsp;</td></tr>
                    <tr><td class="PopTopAlertBG">
                    <div class="PopTopAlertText"><%=ResHelper.LocalizeString("{$=Έχετε επιλέξει να αντιλογήσετε την παρακάτω συναλλαγή |en-us=You wish to refund the transaction below $}") %></div>
                    </td></tr>
               </table>
            </td></tr>
            <tr id="CancelRow" runat="server" style="display:none"><td>
               <table cellpadding="0" cellspacing="0">
                <tr><td class="PopTopSep">&nbsp;</td></tr>
                <tr><td class="PopTopCancelBG">
                    <div class="PopTopCancelText"><%=ResHelper.LocalizeString("{$=Έχετε επιλέξει να ακυρώσετε την παρακάτω συναλλαγή |en-us=You wish to refund the transaction below $}") %></div>
                </td></tr>
               </table>
            </td></tr>

            <tr><td class="PopContent">
                <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center" >
                    <tr>
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Κωδικός Συναλλαγής |en-us=Transaction Code $}") %></td>
                        <td class="PopTransDetValue" id="tdTransCode" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ημερομηνία Συναλλαγής |en-us=Transaction Date$}")%></td>
                        <td class="PopTransDetValue" id="tdTransDtm" runat="server"></td>
                    </tr>
                     <tr id="FieldCancelDTM" runat="server" visible="false">
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ημερομηνία Αντιλογισμού|en-us=Refund Date$}")%></td>
                        <td class="PopTransDetValue" id="tdCancelDTM" runat="server"></td>
                    </tr>
                    <tr id="TRPrice" runat="server">
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ποσό |en-us=Amount $}")%></td>
                        <td class="PopTransDetValue"  ID="tdPrice" runat="server" ></td>
                    </tr>
                    <tr id="TRPriceTxt" runat="server">
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Ποσό |en-us=Amount $}") %></td>
                        <td class="PopTransDetValue"  ID="TdPriceTxt" runat="server" ><asp:TextBox onkeyup="FixMoney(this,false,false)" onblur="FixMoney(this,true,false)" CssClass="PopTransREp" ID="txtPrice" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Τύπος Συναλλαγής |en-us=Transaction Type $}")%></td>
                        <td class="PopTransDetValue" id="tdTransType" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle"><%= ResHelper.LocalizeString("{$=Κατάσταση Συναλλαγής |en-us=Transaction Status $}")%></td>
                        <td class="PopTransDetValue"  id="tdTransStatus" runat="server"></td>
                    </tr>
                    <tr id="AuthorisationCodeTR" runat="server" visible="false">
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Authorisation Code|en-us=Authorisation Code$}")%></td>
                        <td class="PopTransDetValue" id="tdAuthorisationCode" runat="server"></td>
                    </tr>
                    <tr id="CustomFieldsRow" runat="server" visible="false">
                        <td colspan="2" width="100%">
                            <table cellpadding="0" cellspacing="0" id="tblCustomFields" runat="server">
                            </table>
                        </td>
                    </tr>
                </table>
            </td></tr>
            <tr><td class="PopTopTitle"><%=ResHelper.LocalizeString("{$=Στοιχεία Πακέτου |en-us=Batch Details $}") %></td></tr>
            <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>
            <tr><td class="PopContent">
                <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center" >
                   <tr id="FieldBatchNo" runat="server">
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Αριθμός Πακέτου |en-us=Batch Number $}") %></td>
                        <td class="PopTransDetValue" id="tdBoxNo" runat="server"></td>
                    </tr>
                    <tr id="FieldBatchCloseDtm" runat="server">
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ημερομηνία Κλεισίματος Πακέτου |en-us=Batch Submission Date $}")%></td>
                        <td class="PopTransDetValue" id="tdBoxCloseDtm" runat="server"></td>
                    </tr>
                    <tr id="FieldBatchStatus" runat="server">
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Κατάσταση Πακέτου |en-us=Batch Status $}") %></td>
                        <td class="PopTransDetValue" id="tdBoxStatus" runat="server"></td>
                    </tr>
                </table>
            </td></tr>



            <asp:PlaceHolder runat="server" ID="ph_Invoice" Visible="false">
                <tr><td class="PopTopTitle"><%=ResHelper.LocalizeString("{$=Στοιχεία Τιμολόγησεις |en-us=Invoice Details $}") %></td></tr>
                <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>
                <tr><td class="PopContent">
                    <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center" >
                       <tr id="TrCompanyName" runat="server">
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Επωνυμία Επιχείρησης|en-us=Company Name $}")%></td>
                            <td class="PopTransDetValue" id="tdCompanyName" runat="server"></td>
                        </tr>
                        <tr id="TrProfession" runat="server">
                            <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Επάγγελμα |en-us=Profession $}")%></td>
                            <td class="PopTransDetValue" id="tdProfession" runat="server"></td>
                        </tr>
                        <tr id="TrAddress" runat="server">
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Διεύθυνση |en-us=Address $}") %></td>
                            <td class="PopTransDetValue" id="tdAddress" runat="server"></td>
                        </tr>
                        <tr id="TrPostalCode" runat="server">
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Τ.Κ.|en-us=Postal Code $}")%></td>
                            <td class="PopTransDetValue" id="tdPostalCode" runat="server"></td>
                        </tr>
                        <tr id="TrCity" runat="server">
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Πόλη |en-us=City $}") %></td>
                            <td class="PopTransDetValue" id="tdCity" runat="server"></td>
                        </tr>
                        <tr id="TrVATNumber" runat="server">
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=ΑΦΜ |en-us=VAT Number$}") %></td>
                            <td class="PopTransDetValue" id="tdVATNumber" runat="server"></td>
                        </tr>
                        <tr id="TrTaxOffice" runat="server">
                            <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=ΔΟΥ |en-us=Tax office $}")%></td>
                            <td class="PopTransDetValue" id="tdTaxOffice" runat="server"></td>
                        </tr>
                    </table>
                </td></tr>
            </asp:PlaceHolder>



            <tr><td class="PopTopTitle"><%=ResHelper.LocalizeString("{$=Στοιχεία Πελάτη |en-us=Customer Information $}") %></td></tr>
            <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>
            <tr><td>
                <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center" >
                    <tr>
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Ονοματεπώνυμο Πελάτη |en-us=Customer Full Name$}") %></td>
                        <td class="PopTransDetValue" id="tdCustFullName" runat="server"></td>
                    </tr>
                    
                    <tr>
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Τύπος Κάρτας |en-us=Card Type $}")%></td>
                        <td class="PopTransDetValue" id="tdCardType" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Αριθμός Κάρτας |en-us=Card Number$}")%></td>
                        <td class="PopTransDetValue" id="tdCardNo" runat="server"></td>
                    </tr>
                     <tr>
                        <td class="PopTransDetTitle" >E-mail</td>
                        <td class="PopTransDetValue" id="tdEmail" runat="server"></td>
                    </tr>
                     <tr>
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Τηλέφωνο Επικοινωνίας |en-us=Phone Number$}") %></td>
                        <td class="PopTransDetValue" id="tdContactPhone" runat="server"></td>
                    </tr>
                     <tr>
                        <td class="PopTransDetTitle"><%=ResHelper.LocalizeString("{$=Σημειώσεις |en-us=Comments $}") %></td>
                        <td class="PopTransDetNotes" id="tdNotes" runat="server">-</td>
                    </tr>
                      <tr id="ErrosCell" runat="server" visible="false">
                        <td class="PopTransDetTitle"><%= ResHelper.LocalizeString("{$=Αιτιολογία |en-us=Reason $}")%></td>
                        <td class="PopTransDetValue" id="tdErrorMessage" runat="server"></td>
                    </tr>
                    <tr id="DetailsBtnRow" runat="server" style="display:none;">
                        <td class="PopTransDetTitle" style="padding-top:10px"><asp:LinkButton runat="server" ID="btnSaveToPDF" runat="server" ><img class="Chand" border="0" src='<%=ResHelper.LocalizeString("{$=/app_themes/LivePay/popup/btnpdf.png|en-us=/app_themes/LivePay/popup/btnpdf_en-us.png$}") %>'  /></asp:LinkButton></td>
                        <td class="PopTransDetValue" style="padding-top:10px"><img border="0"  id="btnPrint" runat="server" class="Chand" /></td>
                        
                    </tr>
                     <tr id="CancelBtnRow" runat="server" style="display:none">
                        <td colspan="2" class="PopTransReportButtons"><img class="Chand" border="0" src='<%=ResHelper.LocalizeString("{$=/app_themes/LivePay/popup/btnreturn.png|en-us=/app_themes/LivePay/popup/btnreturn_en-us.png$}") %>' onclick="$.unblockUI()" />&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton runat="server" ID="btnCancel" runat="server" ><img border="0"  class="Chand" src='<%=ResHelper.LocalizeString("{$=/app_themes/LivePay/popup/btnTrans.png|en-us=/app_themes/LivePay/popup/btnTrans_en-us.png$}") %>' /></asp:LinkButton></td>
                    </tr>
                     <tr id="ReportBtnRow" runat="server" style="display:none">
                        <td colspan="2" class="PopTransReportButtons">
                        <img border="0" src='<%=ResHelper.LocalizeString("{$=/app_themes/LivePay/popup/btnreturn.png|en-us=/app_themes/LivePay/popup/btnreturn_en-us.png$}") %>' onclick="$.unblockUI()" class="Chand" />&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton runat="server" ID="btnRefund" runat="server" ><img border="0" class="Chand" src='<%=ResHelper.LocalizeString("{$=/app_themes/LivePay/popup/btncont.png|en-us=/app_themes/LivePay/popup/btncont_en-us.png$}") %>' /></asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="PopTransDEtBotDescr"><%=ResHelper.LocalizeString("{$=Για οποιαδήποτε πληροφορία καλέστε στο 801-111-1144 από σταθερό τηλέφωνο ή στο  210 - 9555000 από κινητό (24 ώρες το 24 ωρό) ή αποστείλατε e-mail στο ebanking@eurobank.gr |en-us=For more information, please call our support department at 210 - 9555019 or e-mail us at ebanking@eurobank.gr $}") %></td>
                    </tr>
                   
                </table>
            </td></tr>
            </div>
            <div id="ChangesDiv" visible="false" runat="server">
               <tr><td class="PopContent">
                <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center" id="ChangesTable" runat="server" >
                    <tr>
                        <td class="PopTransChangesTitle"><%=ResHelper.LocalizeString("{$=Αριθμός<br />Πακέτου|en-us=Transaction Code $}") %></td>
                        <td class="PopTransChangesTitle"><%=ResHelper.LocalizeString("{$=Ημ/νία<br>Κλεισίματος<br>Πακέτου|en-us=Submission Date $}") %></td>
                        <td class="PopTransChangesTitle"><%= ResHelper.LocalizeString("{$=Κωδικός<br>Συναλλαγής |en-us=Transaction<br>Status $}")%></td>
                        <td class="PopTransChangesTitle"><%=ResHelper.LocalizeString("{$=Ημ/νία<br>Συναλλαγής|en-us=Transaction Date$}") %></td>
                        <td class="PopTransChangesTitle"><%=ResHelper.LocalizeString("{$=Ποσό |en-us=Amount $}") %></td>
                        <td class="PopTransChangesTitle"><%=ResHelper.LocalizeString("{$=Τύπος<br>Συν/γης|en-us=Transaction<br>Type $}") %></td>
                    </tr>
                    <tr><td class="PopTopTitleUnder" colspan="3">&nbsp;</td></tr>
                </table> 
                </td> 
                </tr> 
            </div>
        </table>
    </td>
	<td class="PopCenterRight">&nbsp;</td>
    </tr>
    <tr>
	<td class="PopBotLeft">&nbsp;</td>
	<td colspan="2" class="PopBotCenter"></td>
	<td class="PopBotRight">&nbsp;</td>
    </tr>
</table>	


<table cellpadding="0" cellspacing="0" border="0" style="display:none" class="PopUpMainTable">
    <tr>
	<td class="PopTopLeft">&nbsp;</td>
        <td class="PopTopCenter">&nbsp;</td>
        <td class="PopTopClose"><img src="/App_Themes/LivePay/PopUp/Close.png"  alt="Close" title="Close" style="cursor:pointer;"/></td>
	<td class="PopTopRight">&nbsp;</td>  
    </tr>
    <tr>
	<td class="PopCenterLeft">&nbsp;</td>
	<td colspan="2" class="PopCenter">
        <table cellpadding="0" cellspacing="0">
            <tr><td class="PopTopTitle"><%=ResHelper.LocalizeString("{$=Πληροφορίες Συναλλαγής|en-us=Transaction Information $}") %></td></tr>
            <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>
            <tr><td></td></tr>
        </table>
    </td>
	<td class="PopCenterRight">&nbsp;</td>
    </tr>
    <tr>
	<td class="PopBotLeft">&nbsp;</td>
	<td colspan="2" class="PopBotCenter"></td>
	<td class="PopBotRight">&nbsp;</td>
    </tr>
</table>	
<asp:Label ID="lblDebug" runat="server" />
<script language="javascript" type="text/javascript">
    function SetIt() {
        var txtPrice = document.getElementById('<%=txtPrice.clientID %>')
        var txtBackPrice = document.getElementById(TestControl)
        var PriceLimit = '<%=btnRefund.CommandName%>'
        PriceLimit = PriceLimit.replace(",", ".")
        txtPrice.focus()
        txtPrice.blur()
        txtBackPrice.value = txtPrice.value.replace(".", ",")

        if (parseFloat(txtPrice.value) <= 0) {
            alert('<%=ResHelper.LocalizeString("{$=Το ποσό που συμπληρώσατε (' + parseFloat(txtPrice.value) + ') πρέπει να είναι μεγαλύτερο απο το 0 !|en-us=The amount you have entered (' + parseFloat(txtPrice.value) + ') should be greater than 0!$}") %>')
            return false
        } else {
            if (parseFloat(PriceLimit) >= parseFloat(txtPrice.value)) {
                return true
            } else {

                alert('<%=ResHelper.LocalizeString("{$=Το ποσό που συμπληρώσατε ( ' + parseFloat(txtPrice.value) + ' ) είναι μεγαλύτερο από το υπολειπόμενο ποσό ( ' + parseFloat(PriceLimit) + ' ) της συναλλαγής!|en-us=The amount you have entered ( ' + parseFloat(txtPrice.value) + ' ) exceeds the remaining payment amount ( ' + parseFloat(PriceLimit) + ' ) !$}") %>')
                return false
            }
        }

    } 
</script>