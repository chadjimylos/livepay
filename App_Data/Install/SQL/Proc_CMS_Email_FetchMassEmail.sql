-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_CMS_Email_FetchMassEmail]
	@FetchFailed bit,
	@FetchNew bit,
	@EmailID int,
	@FirstUserID int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @users TABLE (
		UserID int
	);
	BEGIN TRANSACTION
	IF @FetchFailed = 1 AND @FetchNew = 1
		BEGIN
			/* Get failed and waiting */
			INSERT INTO @users SELECT TOP 10 UserID FROM CMS_EmailUser WHERE (EmailID = @EmailID AND UserID > @FirstUserID AND Status = 1) ORDER BY UserID;
		END
	ELSE IF @FetchNew = 1
		BEGIN
			/* Get only waiting */
			INSERT INTO @users SELECT TOP 10 UserID FROM CMS_EmailUser WHERE (EmailID = @EmailID AND UserID > @FirstUserID AND Status = 1 AND LastSendResult IS NULL) ORDER BY UserID;
		END
	ELSE IF @FetchFailed = 1
		BEGIN
			/* Get only failed */
			INSERT INTO @users SELECT TOP 10 UserID FROM CMS_EmailUser WHERE (EmailID = @EmailID AND UserID > @FirstUserID AND Status = 1 AND LastSendResult IS NOT NULL) ORDER BY UserID;
		END
	ELSE
		BEGIN
			/* Get archived */
			INSERT INTO @users SELECT TOP 10 UserID FROM CMS_EmailUser WHERE (EmailID = @EmailID AND UserID > @FirstUserID AND Status = 3) ORDER BY UserID;
		END
	
	/* Status: 0 - created; 1 - waiting; 2 - sending; 3 - archived */
	UPDATE CMS_EmailUser SET Status = 2 WHERE (EmailID = @EmailID AND UserID IN (SELECT UserID FROM @users));
	COMMIT TRANSACTION
	SELECT UserID, Email FROM CMS_User WHERE UserID IN (SELECT UserID FROM @users);
END
