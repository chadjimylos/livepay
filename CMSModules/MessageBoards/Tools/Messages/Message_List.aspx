<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Message_List.aspx.cs" Inherits="CMSModules_MessageBoards_Tools_Messages_Message_List"
    Theme="Default" Title="Message boards - Message list" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" EnableEventValidation="false" %>

<%@ Register Src="~/CMSModules/MessageBoards/Controls/Messages/MessageList.ascx" TagName="MessageList" TagPrefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:ScriptManager ID="manScript" runat="server"  />
    <cms:MessageList ID="messageList" runat="server" />
</asp:Content>
