﻿$(document).ready(function () {
   
});

function BindManual(RunAgainScroller) {
    BuildDropDowns()
    $(document).click(function () {
        DocClick()
    });
    if (RunAgainScroller) {
        $('.scroll-pane').jScrollPane({ scrollbarWidth: 11, scrollbarMargin: 2 });
        var AllScrollers = $('.DropDownContent').get()
        $.each(AllScrollers, function (index, obj) {
           var MaxWidth = $(obj).attr("maxwidth")

           $(obj).find('.jScrollPaneContainer').css('height', '189px')
           $(obj).find('.jScrollPaneContainer').css('width', MaxWidth + 'px')
             //.jScrollPaneContainer
//            obj.style.height = '189px'
//            obj.style.width ='143px'
       })

//       AllScrollers = $('.Custom-DDL-Holder').get()
//       $.each(AllScrollers, function (index, obj) {
//           alert(obj.offsetWidth)
//       })
       
       
    }
}

function CreateDropDown(DropDownHolder,IsCustom) {
    var DropDownID = $('#' + DropDownHolder).find('.Custom-DLL').attr("id")
    var MainDiv = document.createElement('Div')
    MainDiv.className = 'DDLHolder'

    

      var TextWidth = 0;
      if (IsCustom > 0){
          TextWidth = IsCustom - 33
      }

    var First_Holder = document.createElement('Div')
    First_Holder.className = 'Custom-DDL-Main-Holder'

    if (IsCustom > 0) {
        First_Holder.style.width = IsCustom + 'px'
    }
        var Div_Text = document.createElement('Div')
        var OnKeyUpParam = "this,'Div_" + DropDownHolder + "','" + DropDownID + "',0," + IsCustom
            Div_Text.className = 'FloatLeft'
            if (TextWidth>0)
                Div_Text.innerHTML = '<input autocomplete="off" type="text" id="txt_' + DropDownHolder + '" value="Choose" style="width:' + TextWidth + 'px" onkeyup="GetData(' + OnKeyUpParam + ')"  class="Custom-DDL-Text" />'
            else
                Div_Text.innerHTML = '<input autocomplete="off" type="text" id="txt_' + DropDownHolder + '" value="Choose" onkeyup="GetData(' + OnKeyUpParam + ')"  class="Custom-DDL-Text" />'

        var Div_Image = document.createElement('Div')
        var OnClickParam = "'txt_" + DropDownHolder + "','Div_" + DropDownHolder + "','" + DropDownID + "',1," + IsCustom
            Div_Image.className = 'FloatLeft'
            Div_Image.innerHTML = '<img src="../../App_Themes/Posidonia2012/CustomDropDown/DDLPointer.png" onclick="GetData(' + OnClickParam + ')" />'
        var Div_Clear = document.createElement('Div')
            Div_Clear.className = 'Clear'
        
        First_Holder.appendChild(Div_Text)
        First_Holder.appendChild(Div_Image)
        First_Holder.appendChild(Div_Clear)

    var Second_Holder = document.createElement('Div')
        Second_Holder.className = 'Custom-DDL-Holder'
        Second_Holder.id = 'Main_Div_' + DropDownHolder
        //Second_Holder.style.width='280px'
        //width:280px
        var Data_Holder = document.createElement('Div')
        Data_Holder.className = 'scroll-pane Custom-DDL-DivIn'
        
        
        if (IsCustom > 0) {
            var FixedWidth = parseInt(IsCustom) + 6
            Data_Holder.style.width = FixedWidth + 'px'
            Second_Holder.style.width = IsCustom + 'px'
            //Second_Holder.style.width = FixedWidth + 'px'
        }

       
            Data_Holder.id = 'Div_' + DropDownHolder
   
    Second_Holder.appendChild(Data_Holder)

    MainDiv.appendChild(First_Holder)
    MainDiv.appendChild(Second_Holder)
    $('#' + DropDownHolder).append(MainDiv)

    Div_Clear = document.createElement('Div')
    Div_Clear.className = 'Clear'
    $('#' + DropDownHolder).append(Div_Clear)

    $(MainDiv).click(function (event) { SetClick(event) })
   
}

function BuildDropDowns() {
    var AllHolders = $('.DropDownContent').get()
    $.each(AllHolders, function (index, obj) {
        if ($(obj).attr("Builded")!='done'){
            if ($(obj).attr("MaxWidth")) {
                if ($(obj).attr("MaxWidth") > 0) {
                    CreateDropDown(obj.id, $(obj).attr("MaxWidth"))
                } else {
                    CreateDropDown(obj.id, 0)
                }
            } else {
                CreateDropDown(obj.id, 0)
            }
            $(obj).attr("Builded", "done")
        }
        $('.scroll-pane').jScrollPane({ scrollbarWidth: 11, scrollbarMargin: 2 });
    });
    
    $('.Custom-DDL-Holder').hide()
    DocClick()
    

}

function DocClick() {
   
    $('.Custom-DDL-Holder').hide()
    var AllHolders = $('.DropDownContent').get()
    $.each(AllHolders, function (index, obj) {
        var AutoComplete = $(obj).find('.Custom-DDL-Text')
        var DropDown = $(obj).find('.Custom-DLL')

        if (AutoComplete.val() == '' && DropDown.val() == '0') {
            AutoComplete.val('Choose')
        }
        if (AutoComplete.val() == '' && DropDown.val() != '0') {
            AutoComplete.val(DropDown.find("option:selected").text())
        }

        if (AutoComplete.val() != '' && DropDown.val() == '0') {
            if (AutoComplete.val() != 'Choose') {
                AutoComplete.val('Choose')
            }
        }

        if (AutoComplete.val() != '' && DropDown.val() != '0') {
            if (AutoComplete.val() != DropDown.find("option:selected").text()) {
                AutoComplete.val(DropDown.find("option:selected").text())
            }
        }
    });
    
}


function GetData(txtObj, divObj, ddlObj, all, IsCustom) {
    var TextWidth = 0;
    if (IsCustom > 0) {
        TextWidth = IsCustom - 47
    }

    if (all == 1)
        txtObj = document.getElementById(txtObj)
   
    var MainDivData = 'Main_' + divObj
    var Val_ = txtObj.value

    var Objdiv_ = document.getElementById(divObj)
    MainDivData = document.getElementById(MainDivData)
   
    if (all == 1) {
        if ($(MainDivData).is(":visible")) {
            $(MainDivData).hide()
        } else {
            $(MainDivData).show()
            txtObj.focus()
            txtObj.select();
        }
    } else {
        $(MainDivData).show()
    }

    if (Objdiv_.hasChildNodes()) {      //--------- Delete All Records
        while (Objdiv_.childNodes.length >= 1) {
            Objdiv_.removeChild(Objdiv_.firstChild);
        }
    }

    var MyDivSep = document.createElement('Div')
    MyDivSep.innerHTML = '<div style="font-size:1px;height:5px">&nbsp;</div>'
    Objdiv_.appendChild(MyDivSep)

   


   

    if (Val_ != '' && all == 0) {
        $('#' + ddlObj).find('option').each(function () {
            var CountryName = $(this).text().toLowerCase()
            if (CountryName.startsWith(Val_.toLowerCase())) {
                var MyDiv = document.createElement('Div')
                var Over = "this.className='Custom-DDL-Option-Over';$($(this).parent().get()[0] + ' Custom-DDL-LeftCorner').addClass('Custom-DDL-LeftCorner-Over')"//"this.style.backgroundImage='url(../../App_Themes/Posidonia2012/CustomDropDown/BgDDLOver.png)';this.style.backgroundColor='transparent';this.style.backgroundRepeat='no-repeat'"
                var Out = "this.className='Custom-DDL-Option'"//"this.style.backgroundImage='url()'"
                var OnClickVal = "'" + $(this).text() + "','" + $(this).val() + "','" + txtObj.id + "','" + Objdiv_.id + "','" + ddlObj + "','" + MainDivData.id + "'," + TextWidth; 
                if (TextWidth>0)
                    MyDiv.innerHTML = '<div class="Custom-DDL-Option-Div"><div class="Custom-DDL-LeftCorner"></div><a class="Custom-DDL-Option" style="width:' + TextWidth + 'px" onmouseover=' + Over + '; onmouseout=' + Out + '; onclick="SelectTheCountry(' + OnClickVal + ')">' + $(this).text() + '</a><div class="Custom-DDL-RightCorner"></div><div class="Clear"></div></div>'
                else
                    MyDiv.innerHTML = '<div class="Custom-DDL-Option-Div"><div class="Custom-DDL-LeftCorner"></div><a class="Custom-DDL-Option" onmouseover=' + Over + '; onmouseout=' + Out + '; onclick="SelectTheCountry(' + OnClickVal + ')">' + $(this).text() + '</a><div class="Custom-DDL-RightCorner"></div><div class="Clear"></div></div>'

                Objdiv_.appendChild(MyDiv)
            }
        });

    } else {
        $('#' + ddlObj).find('option').each(function () {
            var MyDiv = document.createElement('Div')
            var Over = "this.className='Custom-DDL-Option-Over'"//"this.style.backgroundImage='url(../../App_Themes/Posidonia2012/CustomDropDown/BgDDLOver.png)';this.style.backgroundColor='transparent';this.style.backgroundRepeat='no-repeat'"
            var Out = "this.className='Custom-DDL-Option'"//"this.style.backgroundImage='url()'"

            var OnClickVal = "'" + $(this).text() + "','" + $(this).val() + "','" + txtObj.id + "','" + Objdiv_.id + "','" + ddlObj + "','" + MainDivData.id + "'," + TextWidth;

            if (TextWidth > 0)
                MyDiv.innerHTML = '<div class="Custom-DDL-Option-Div"><div class="Custom-DDL-LeftCorner"></div><a class="Custom-DDL-Option" style="width:' + TextWidth + 'px" onmouseover=' + Over + '; onmouseout=' + Out + '; onclick="SelectTheCountry(' + OnClickVal + ')">' + $(this).text() + '</a><div class="Custom-DDL-RightCorner"></div><div class="Clear"></div></div>'
            else
                MyDiv.innerHTML = '<div class="Custom-DDL-Option-Div"><div class="Custom-DDL-LeftCorner"></div><a class="Custom-DDL-Option" onmouseover=' + Over + '; onmouseout=' + Out + '; onclick="SelectTheCountry(' + OnClickVal + ')">' + $(this).text() + '</a><div class="Custom-DDL-RightCorner"></div><div class="Clear"></div></div>'

            Objdiv_.appendChild(MyDiv)
        });
    }
    $('.scroll-pane').jScrollPane({ scrollbarWidth: 11, scrollbarMargin: 2 });
}

function SelectTheCountry(Country, CountryId, txtObj, Objdiv_, ddlObj, MainDivData, TextWidth) {
    
    var Objtxt = $('#' + txtObj)
  
    Objtxt.val(Country)
    Objdiv_ = document.getElementById(Objdiv_)
    MainDivData = document.getElementById(MainDivData)
    $('select[id$=' + ddlObj + ']').val(CountryId)
    if (Objdiv_.hasChildNodes()) {      //--------- Delete All Records
        while (Objdiv_.childNodes.length >= 1) {
            Objdiv_.removeChild(Objdiv_.firstChild);
        }
    }
    var MyDivSep = document.createElement('Div')
    MyDivSep.innerHTML = '<div style="font-size:1px;height:5px">&nbsp;</div>'
    Objdiv_.appendChild(MyDivSep)
    $('#' + ddlObj).find('option').each(function () {
        var MyDiv = document.createElement('Div')
        var Over = "this.className='Custom-DDL-Option-Over';"//"this.style.backgroundImage='url(../../App_Themes/Posidonia2012/CustomDropDown/BgDDLOver.png)';this.style.backgroundColor='transparent';this.style.backgroundRepeat='no-repeat'"
        var Out = "this.className='Custom-DDL-Option'"//"this.style.backgroundImage='url()'"
        var OnClickVal = "'" + $(this).text() + "','" + $(this).val() + "','" + txtObj.id + "','" + Objdiv_.id + "','" + MainDivData.id + "'," + TextWidth;

        if (TextWidth > 0)
            MyDiv.innerHTML = '<div class="Custom-DDL-Option-Div"><div class="Custom-DDL-LeftCorner"></div><a class="Custom-DDL-Option" style="width:' + TextWidth + 'px" onmouseover=' + Over + '; onmouseout=' + Out + '; onclick="SelectTheCountry(' + OnClickVal + ')">' + $(this).text() + '</a><div class="Custom-DDL-RightCorner"></div><div class="Clear"></div></div>'
        else
            MyDiv.innerHTML = '<div class="Custom-DDL-Option-Div"><div class="Custom-DDL-LeftCorner"></div><a class="Custom-DDL-Option"  onmouseover=' + Over + '; onmouseout=' + Out + '; onclick="SelectTheCountry(' + OnClickVal + ')">' + $(this).text() + '</a><div class="Custom-DDL-RightCorner"></div><div class="Clear"></div></div>'

        Objdiv_.appendChild(MyDiv)
    });
   
    $(MainDivData).hide()
}

function SetClick(e) {
    if (!e) e = window.event;
    if (e.stopPropagation) {
        e.stopPropagation();
    } else {
        e.cancelBubble = true;
    }
}
