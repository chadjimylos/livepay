using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Customers_Customer_Edit_Orders : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);

        InitializeMasterPage();
    }


    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    private void InitializeMasterPage()
    {
        // Set the master page title
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Order_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_Order/object.png");

        // Set the action element
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("Order_List.NewItemCaption");
        actions[0, 3] = "~/CMSModules/Ecomemrce/Tools/Customers/Order_New.aspx";
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_Order/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Order_Edit.aspx?orderID=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // check 'EcommerceModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
            }

            // delete OrderInfo object from database
            OrderInfoProvider.DeleteOrderInfo(ValidationHelper.GetInteger(actionArgument, 0));
        }
    }
}
