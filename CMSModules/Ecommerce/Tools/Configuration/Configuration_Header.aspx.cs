using System;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_Configuration_Header : CMSEcommerceConfigurationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string[,] tabs = new string[11, 4];
        tabs[0, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.StoreSettings") + "&nbsp;";       
        tabs[0, 2] = "StoreSettings/StoreSettings_Frameset.aspx";
        tabs[1, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.Departments") + "&nbsp;";
        tabs[1, 2] = "Departments/Department_List.aspx";
        tabs[2, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.ShippingOptions") + "&nbsp;";
        tabs[2, 2] = "ShippingOptions/ShippingOption_List.aspx";
        tabs[3, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.PaymentOptions") + "&nbsp;";
        tabs[3, 2] = "PaymentOptions/PaymentOption_List.aspx";
        tabs[4, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.TaxClasses") + "&nbsp;";
        tabs[4, 2] = "TaxClasses/TaxClass_List.aspx";
        tabs[5, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.Currencies") + "&nbsp;";
        tabs[5, 2] = "Currencies/Currency_List.aspx";
        tabs[6, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.ExchangeRates") + "&nbsp;";
        tabs[6, 2] = "ExchangeRates/ExchangeTable_List.aspx";
        tabs[7, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.OrderStatus") + "&nbsp;";
        tabs[7, 2] = "OrderStatus/OrderStatus_List.aspx";
        tabs[8, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.PublicStatus") + "&nbsp;";
        tabs[8, 2] = "PublicStatus/PublicStatus_List.aspx";
        tabs[9, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.InternalStatus") + "&nbsp;";
        tabs[9, 2] = "InternalStatus/InternalStatus_List.aspx";
        tabs[10, 0] = "&nbsp;" + ResHelper.GetString("EcommerceConfiguration.Invoice") + "&nbsp;";
        tabs[10, 2] = "InvoiceTemplate/InvoiceTemplate_Edit.aspx";
        
        CurrentMaster.Tabs.Tabs = tabs;
        CurrentMaster.Tabs.SelectedTab = 0;
        CurrentMaster.Tabs.UrlTarget = "configEdit";
        CurrentMaster.SetRTL();
    }
}
