using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSImportExport;
using CMS.SettingsProvider;
using CMS.WorkflowEngine;
using CMS.TreeEngine;

public partial class CMSModules_ImportExport_Controls_Import_cms_document : ImportExportControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.chkDocuments.Text = ResHelper.GetString("ImportDocuments.ImportDocuments");
        this.chkDocumentsHistory.Text = ResHelper.GetString("ImportDocuments.ImportDocumentsHistory") + "<br />";
        this.chkRelationships.Text = ResHelper.GetString("ImportDocuments.ImportRelationships") + "<br />";
        this.chkACLs.Text = ResHelper.GetString("ImportDocuments.ImportACLs") + "<br />";
        this.chkEventAttendees.Text = ResHelper.GetString("ImportDocuments.ImportEventAttendees") + "<br />";
        this.chkBlogComments.Text = ResHelper.GetString("ImportDocuments.ImportBlogComments") + "<br />";

        // Javascript
        string script = "var parent = document.getElementById('" + chkDocuments.ClientID + "'); \n" +
                        "var childIDs = ['" + chkDocumentsHistory.ClientID + "', '" + chkRelationships.ClientID + "', '" + chkBlogComments.ClientID + "', '" + chkEventAttendees.ClientID + "', '" + chkACLs.ClientID + "']; \n" +
                        "InitCheckboxes(); \n";

        ltlScript.Text = ScriptHelper.GetScript(script);

        this.chkDocuments.Attributes.Add("onclick", "CheckChange();");
    }


    /// <summary>
    /// Get settings
    /// </summary>
    public override void SaveSettings()
    {
        ProcessObjectEnum documents = (this.chkDocuments.Checked ? ProcessObjectEnum.All : ProcessObjectEnum.None);

        Settings.SetObjectsProcessType(documents, TreeObjectType.DOCUMENT, true);

        Settings.SetSettings(ImportExportHelper.SETTINGS_DOC_HISTORY, this.chkDocumentsHistory.Checked);
        Settings.SetSettings(ImportExportHelper.SETTINGS_DOC_RELATIONSHIPS, this.chkRelationships.Checked);
        Settings.SetSettings(ImportExportHelper.SETTINGS_DOC_ACLS, this.chkACLs.Checked);
        Settings.SetSettings(ImportExportHelper.SETTINGS_EVENT_ATTENDEES, this.chkEventAttendees.Checked);
        Settings.SetSettings(ImportExportHelper.SETTINGS_BLOG_COMMENTS, this.chkBlogComments.Checked);
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData()
    {
        this.chkDocuments.Checked = (Settings.GetObjectsProcessType(TreeObjectType.DOCUMENT, true) != ProcessObjectEnum.None);
        this.chkDocumentsHistory.Checked = ValidationHelper.GetBoolean(Settings.GetSettings(ImportExportHelper.SETTINGS_DOC_HISTORY), true);

        this.chkRelationships.Checked = ValidationHelper.GetBoolean(Settings.GetSettings(ImportExportHelper.SETTINGS_DOC_RELATIONSHIPS), true);
        this.chkACLs.Checked = ValidationHelper.GetBoolean(Settings.GetSettings(ImportExportHelper.SETTINGS_DOC_ACLS), true);
        this.chkEventAttendees.Checked = ValidationHelper.GetBoolean(Settings.GetSettings(ImportExportHelper.SETTINGS_EVENT_ATTENDEES), true);
        this.chkBlogComments.Checked = ValidationHelper.GetBoolean(Settings.GetSettings(ImportExportHelper.SETTINGS_BLOG_COMMENTS), true);

        // Visibility
        SiteImportSettings settings = (SiteImportSettings)this.Settings;
        
        this.chkACLs.Visible = settings.IsIncluded(TreeObjectType.ACL, false);
        this.chkDocumentsHistory.Visible = settings.IsIncluded(WorkflowObjectType.VERSIONHISTORY, false);
        this.chkRelationships.Visible = settings.IsIncluded(TreeObjectType.RELATIONSHIP, false);
        this.pnlDocumentData.Visible = this.chkDocumentsHistory.Visible || this.chkACLs.Visible || this.chkRelationships.Visible;

        this.chkBlogComments.Visible = settings.IsIncluded(PredefinedObjectType.BLOGCOMMENT, false);
        this.chkEventAttendees.Visible = settings.IsIncluded(PredefinedObjectType.EVENTATTENDEE, false);
        this.pnlModules.Visible = this.chkBlogComments.Visible || this.chkEventAttendees.Visible;
    }
}
