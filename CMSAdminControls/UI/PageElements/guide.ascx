<%@ Control Language="C#" AutoEventWireup="true" CodeFile="guide.ascx.cs" Inherits="CMSAdminControls_UI_PageElements_guide" %>

<script type="text/javascript">
    //<![CDATA[
    function ShowDesktopContent(contentUrl, nodeElem, nodeCodeName) {
        if ((leftMenuFrame != null) && (leftMenuFrame.SelectNode != null) && (nodeCodeName)) {
            leftMenuFrame.SelectNode(nodeCodeName);
        }
        else {
            // Handle marking selected menu items
            for (var i = 0; i < allElems.length; i++) {
                if (allElems[i].className == 'ContentTreeItem') {
                    if (allElems[i].firstChild.innerHTML == nodeElem) {
                        allElems[i].className = 'ContentTreeSelectedItem';
                    }
                }
                else {
                    if (allElems[i].className == 'ContentTreeSelectedItem') {
                        allElems[i].className = 'ContentTreeItem';
                    }
                }
            }
        }
        parent.frames['frameMain'].location.href = contentUrl; // Redirect to menu item       
    }
    //]]>    
</script>

<asp:Panel ID="plcGuide" runat="server" />
