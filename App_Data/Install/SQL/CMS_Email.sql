CREATE TABLE [CMS_Email] (
		[EmailID]                  int NOT NULL IDENTITY(1, 1),
		[EmailFrom]                nvarchar(250) NOT NULL,
		[EmailTo]                  nvarchar(max) NULL,
		[EmailCc]                  nvarchar(max) NULL,
		[EmailBcc]                 nvarchar(max) NULL,
		[EmailSubject]             nvarchar(450) NOT NULL,
		[EmailBody]                nvarchar(max) NULL,
		[EmailPlainTextBody]       nvarchar(max) NULL,
		[EmailFormat]              int NOT NULL,
		[EmailPriority]            int NOT NULL,
		[EmailSiteID]              int NULL,
		[EmailLastSendResult]      nvarchar(max) NULL,
		[EmailLastSendAttempt]     datetime NULL,
		[EmailGUID]                uniqueidentifier NOT NULL,
		[EmailLastModified]        datetime NOT NULL,
		[EmailStatus]              int NULL,
		[EmailIsMass]              bit NULL,
		[EmailSetName]             nvarchar(250) NULL,
		[EmailSetRelatedID]        int NULL,
		[EmailReplyTo]             nvarchar(250) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Email]
	ADD
	CONSTRAINT [PK_CMS_Email]
	PRIMARY KEY
	NONCLUSTERED
	([EmailID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_Email]
	ADD
	CONSTRAINT [DEFAULT_CMS_Email_EmailIsMass]
	DEFAULT ((1)) FOR [EmailIsMass]
ALTER TABLE [CMS_Email]
	ADD
	CONSTRAINT [DEFAULT_CMS_Email_EmailSubject]
	DEFAULT ('') FOR [EmailSubject]
CREATE CLUSTERED INDEX [IX_CMS_Email_EmailPriority_EmailStatus]
	ON [CMS_Email] ([EmailPriority] DESC, [EmailStatus])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
