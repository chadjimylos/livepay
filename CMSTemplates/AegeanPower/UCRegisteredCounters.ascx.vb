﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.Controls
Partial Class CMSTemplates_AegeanPower_UCRegisteredCounters
    Inherits CMSUserControl

    Protected ReadOnly Property NewVat() As String
        Get
            Dim sAnauthorise As String = String.Empty
            If Not String.IsNullOrEmpty(Request("NewVat")) Then
                sAnauthorise = Request("NewVat")
            End If
            Return sAnauthorise
        End Get
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If

        If Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub


    Public Overloads Sub ReloadData()
        Bind()
        If Me.NewVat = "1" Then
            Dim Str As String = " $(document).ready(function () {"
            Str = String.Concat(Str, " $(""a[id$=HiddenLink]"").click()  ")
            Str = String.Concat(Str, " } ); ")
            JsScript(Str)

        End If
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
        If Me.NewVat = "1" Then
            Dim Str As String = " $(document).ready(function () {"
            Str = String.Concat(Str, " $(""a[id$=HiddenLink]"").click()  ")
            Str = String.Concat(Str, " } ); ")
            JsScript(Str)
        End If
    End Sub

    Private Sub Bind()

        MainCounter.Visible = False
        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = AegeanPower_DBConnection.GetAfmByUser(UserID)
        GvAFM.DataSource = dt
        GvAFM.DataBind()
    End Sub


    Protected Sub GvAFM_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvAFM.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblStatus As Label = DirectCast(e.Row.FindControl("lblStatus"), Label)
            Dim imgCounters As ImageButton = DirectCast(e.Row.FindControl("imgCounters"), ImageButton)
            Dim GvAfmRow As HtmlGenericControl = DirectCast(e.Row.FindControl("GvAfmRow_GvRpt"), HtmlGenericControl)
            Dim GvAfmLastDiv_GvRpt As HtmlGenericControl = DirectCast(e.Row.FindControl("GvAfmLastDiv_GvRpt"), HtmlGenericControl)
         
            Dim lblAFM As Label = DirectCast(e.Row.FindControl("lblAFM"), Label)
            If Not IsNothing(lblStatus) Then

                If lblStatus.Text = "1" Then
                    lblStatus.Text = ResHelper.LocalizeString("{$=Ενεργό|en-us=Ενεργό$}")
                    imgCounters.Visible = True
                    imgCounters.Enabled = True
                    imgCounters.CommandArgument = lblAFM.Text
                    imgCounters.CommandName = GvAfmRow.ClientID & "[#@]" & GvAfmLastDiv_GvRpt.ClientID
                Else
                    imgCounters.Visible = False
                    lblStatus.Text = ResHelper.LocalizeString("{$=Ανενεργό|en-us=Ανενεργό$}")
                    lblStatus.Style.Add("color", "#8e8d8d")
                End If
            End If
        End If
    End Sub

    Protected Sub GvAFM_CounterBtn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim MyBtn As ImageButton = DirectCast(sender, ImageButton)
        Dim Jsstr As String = String.Concat(" SelectAfmRow('", Split(MyBtn.CommandName, "[#@]")(0), "','", Split(MyBtn.CommandName, "[#@]")(1), "'); ")
        JsScript(Jsstr)
        SelectedAFMRow.Text = MyBtn.CommandName
        SelectedAfm.Text = MyBtn.CommandArgument
        SelectedPage.Text = 1
        BindGvCounters(MyBtn.CommandArgument)
        MainCounter.Visible = True
    End Sub

    'DlCounter_SelectedPage
    Private Sub BindGvCounters(ByVal afm As String)
        MainCounter.Visible = True
        DivAfmTitle.InnerHtml = afm
        Dim iSelectedPage As Integer = SelectedPage.Text
        Dim dt As DataTable = AegeanPower_DBConnection.GetArchiveProvidersByAfm(afm, iSelectedPage)
        dlCounters.DataSource = dt
        dlCounters.DataBind()
        If dt.Rows.Count > 0 Then
            Dim iAllPages As Integer = dt.Rows(0)("AllPages").ToString
            Dim CurrentPage As Integer = dt.Rows(0)("CurrentPage").ToString
            If iAllPages > 1 Then
                '----- Prev 
                If CurrentPage > 1 Then
                    PagerPrev.Visible = True
                Else
                    PagerPrev.Visible = False
                End If

                Dim _SelectedPage As Integer = SelectedPage.Text
                _SelectedPage = _SelectedPage + 1
                PagerNext.Attributes.Add("onclick", String.Concat("SetDLPage('", _SelectedPage, "')"))
                PagerNext.Attributes.Add("alt", "Page " & _SelectedPage)
                '----- Next
                If CurrentPage < iAllPages Then
                    PagerNext.Visible = True
                Else
                    PagerNext.Visible = False
                End If
                _SelectedPage = SelectedPage.Text
                _SelectedPage = _SelectedPage - 1
                PagerPrev.Attributes.Add("onclick", String.Concat("SetDLPage('", _SelectedPage, "')"))
                PagerPrev.Attributes.Add("alt", "Page " & _SelectedPage)

                dlCountersPager.Visible = True
                For i As Integer = 1 To iAllPages
                    Dim NewDiv As New HtmlGenericControl("div")
                    Dim NewLink As New HtmlAnchor
                    NewLink.InnerHtml = i
                    NewLink.Attributes.Add("onclick", String.Concat("SetDLPage('", i, "')"))
                    If CurrentPage = i Then
                        NewLink.Attributes.Add("class", "RegCounterPagerNumbersSel")
                    Else
                        NewLink.Attributes.Add("class", "RegCounterPagerNumbers")
                    End If
                    PagerNumbers.Controls.Add(NewLink)
                Next
                Dim NewDivClear As New HtmlGenericControl("div")
                NewDivClear.Attributes.Add("class", "Clear")
                PagerNumbers.Controls.Add(NewDivClear)
            Else
                dlCountersPager.Visible = False
            End If
        Else
            dlCountersPager.Visible = False
        End If

    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub
    
    Protected Sub HiddenBtnNextPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HiddenBtnNextPage.Click
        Dim Jsstr As String = String.Concat(" SelectAfmRow('", Split(SelectedAFMRow.Text, "[#@]")(0), "','", Split(SelectedAFMRow.Text, "[#@]")(1), "'); ")
        JsScript(Jsstr)
        BindGvCounters(SelectedAfm.Text)
    End Sub

    Dim CounterRow As Integer = 0
    Protected Sub dlCounters_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlCounters.ItemDataBound
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim GvRow As HtmlGenericControl = DirectCast(e.Item.FindControl("GvRow"), HtmlGenericControl)
            CounterRow = CounterRow + 1
            If (CounterRow Mod 4) = 0 Then
                GvRow.Attributes.Add("class", "RegCounterGvRowLast")
            End If
        End If
        
    End Sub

    Protected Sub btnRebindGVAFM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRebindGVAFM.Click
        Bind()
    End Sub
End Class
