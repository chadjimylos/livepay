<%@ Page Language="C#" AutoEventWireup="true" CodeFile="System_DebugLoad.aspx.cs"
    Inherits="CMSSiteManager_Administration_System_Debug_System_DebugLoad" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Group list" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="CacheItemsGrid.ascx" TagName="CacheItemsGrid" TagPrefix="uc1" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">

    <script type="text/javascript">
        //<![CDATA[
        function alert() {
        }
        //]]>
    </script>

    <asp:ScriptManager runat="server" ID="manScript" ScriptMode="Release" />
    <asp:UpdatePanel runat="server" ID="pnlInfo">
        <ContentTemplate>
            <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />
            <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" />
            <asp:Timer runat="server" ID="timRefresh" Interval="1000" Enabled="true" />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
    <table>
        <tr>
            <td>
                <cms:LocalizedLabel runat="server" ID="lblThreads" EnableViewState="false" ResourceString="DebugLoad.Threads" />
            </td>
            <td>
                <asp:TextBox runat="server" CssClass="ShortTextBox" ID="txtThreads" Text="10" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:LocalizedLabel runat="server" ID="lblDuration" EnableViewState="false" ResourceString="DebugLoad.Duration" />
            </td>
            <td>
                <asp:TextBox runat="server" CssClass="ShortTextBox" ID="txtDuration" Text="60" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:LocalizedLabel runat="server" ID="lblInterval" EnableViewState="false" ResourceString="DebugLoad.Interval" />
            </td>
            <td>
                <asp:TextBox runat="server" CssClass="ShortTextBox" ID="txtInterval" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:LocalizedLabel runat="server" ID="lblURLs" EnableViewState="false" ResourceString="DebugLoad.URLs" />
            </td>
            <td>
                <asp:TextBox runat="server" CssClass="TextAreaField" ID="txtURLs" TextMode="MultiLine"
                    Text="~/Home.aspx" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnStart" CssClass="LongSubmitButton" OnClick="btnStart_Click" /><cms:CMSButton
                    runat="server" ID="btnStop" CssClass="LongSubmitButton" OnClick="btnStop_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
