﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DecryptedCard.aspx.vb" Inherits="CMSTemplates_LivePay_DecryptedCard" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        DecryptedCard : <asp:TextBox runat="server" ID="tbDecryptedCard" MaxLength="16" />
        <asp:RequiredFieldValidator runat="server" ID="rfvDecryptedCard" ValidationGroup="Card" ControlToValidate="tbDecryptedCard" ErrorMessage="vale carta" />
        <asp:Button runat="server" ID="btnSubmit" Text="Submit" ValidationGroup="Card" />

        <asp:GridView runat="server" ID="gvShowData" AutoGenerateColumns="true">     
        <Columns>
            <asp:TemplateField HeaderText="Decrypted CardNumber">
                <ItemTemplate>
                    <asp:Literal runat="server" ID="litCard" text='<%# Eval("CardNumber") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>       
        <EmptyDataTemplate>
            <div>den egine kapio update</div>
        </EmptyDataTemplate>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
