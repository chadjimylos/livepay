﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCTransReceipt.ascx.vb" Inherits="CMSTemplates_LivePay_UCTransReceipt" %>

<script src="/CMSScripts/jquery-1.5.2.js"></script>
<script language="javascript" type="text/javascript">
    function ExportDetails(url) {
        document.getElementById('<%=exportpopFrame.ClientID %>').src = url;
        return false
    }
</script>
<div style="display:none">
<iframe id="exportpopFrame" runat="server" ></iframe>
</div>
<div class="PT20">
     <div class="TransRecMainContent">
	<div class="TransRecTblTitle" ><%=ResHelper.LocalizeString("{$=Αποδεικτικό Συναλλαγής|en-us=Payment Receipt$}") %></div>
	<div class="TransRecTblTick" ><img id="imgResult" runat="server" src="/app_themes/LivePay/GreenTick.gif"></div>
	<div class="TransRecTblGreenTitle" id="MessageTitle" runat="server" ></div>
	<div class="Clear"></div>
	<div style="padding-top:20px;padding-left:20px" id="MainDivForm" runat="server">
	   <div class="TransRecLeftRow" runat="server" id="divDateAndTime"></div>
	   <div class="TransRecRightRow" id="tdDateTime" runat="server" ></div>
	   <div class="Clear"></div>
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Πληρωμή προς|en-us=Payment to$}") %></div>
	   <div class="TransRecRightRow" id="tdMerchant" runat="server" ></div>
	   <div class="Clear"></div>
	   <div class="TransRecLeftRow" runat="server" ID="txtTransCode"><%=ResHelper.LocalizeString("{$=Κωδικός Συναλλαγής|en-us=Transaction Code$}") %></div>
	   <div class="TransRecRightRow" id="tdTransCode" runat="server" ></div>
	   <div class="Clear"></div>
       <div style="display:none">
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Κωδικός Πελάτη|en-us=Customer Code$}") %></div>
	   <div class="TransRecRightRow" id="tdCustCode" runat="server" ></div>
	   <div class="Clear"></div>
       </div>
	   <div id="CustomFieldsPanel" runat="server">
       </div>
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Ποσό|en-us=Amount$}") %> (&euro;)</div>
	   <div class="TransRecRightRow" id="tdPrice" runat="server" ></div>
	   <div class="Clear"></div>
       <asp:PlaceHolder runat="server" ID="plc_Commission" Visible="false" EnableViewState="false">
	       <div class="TransRecLeftRow"><%= ResHelper.LocalizeString("{$=Προμήθεια (&euro;)|en-us=Commission (&euro;)$}")%></div>
	       <div class="TransRecRightRow" id="tdCommission" runat="server" ></div>
	       <div class="Clear"></div>
       </asp:PlaceHolder>
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Κάρτα|en-us=Card$}") %></div>
	   <div class="TransRecRightRow" id="tdCard" runat="server" ></div>
	   <div class="Clear"></div>
       
       <asp:Panel runat="server" ID="pnlInstallments" Visible="false">
	       <div class="TransRecLeftRow"><%= ResHelper.LocalizeString("{$=Αριθμός δόσεων |en-us=Number of installments$}")%></div>
	       <div class="TransRecRightRow" id="tdInstallments" runat="server" ></div>
	       <div class="Clear"></div>
       </asp:Panel>

        <div class="PT10">
            <div class="TransRecLeftRow" id="tdAcknowledgementTransaction" runat="server" style="width:100%;text-align:left;"></div>
        </div>

	   <div class="PT10">
		<div class="FLEFT"><img class="Chand"  onclick="ExportDetails('/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=TransReceipt&lang=<%=CMSContext.CurrentDocumentCulture.CultureCode %>')" src='<%=ResHelper.LocalizeString("{$=/app_themes/LivePay/btnSavePDF.gif|en-us=/app_themes/LivePay/btnSavePDF_en-us.png$}") %>' /></div>
		<div class="FLEFT PL10"><img class="Chand" onclick="window.open('/CMSTemplates/LivePay/PrintPage.aspx?ExportType=TransReceipt&lang=<%=CMSContext.CurrentDocumentCulture.CultureCode %>')" src='<%=ResHelper.LocalizeString("{$=/app_themes/LivePay/btnPrint.png|en-us=/app_themes/LivePay/btnPrint_en-us.png$}") %>' /></div>
        <div class="FLEFT PL10"><asp:ImageButton runat="server" ID="btnSave" /></div>
		<div class="Clear"></div>
	   </div>
	</div>
    <div style="line-height:22px;font-size:12px;color:#43474a;padding-top:20px;display:none;padding-left:10px" id="MainDivErrorContainer" runat="server">
    </div> 
      </div>
      <div><img src="/app_themes/LivePay/TransRecContentBGBot.png" /></div>
  </div>