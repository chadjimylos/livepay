using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.DataEngine;
using CMS.SettingsProvider;
using CMS.FormControls;

public partial class CMSFormControls_Inputs_LargeTextArea : FormEngineUserControl
{
    private bool mAllowMacros = true;


    #region "Methods"

    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.txtArea.Enabled = value;
            this.btnMore.Enabled = value;
        }
    }


    /// <summary>
    /// Width
    /// </summary>
    public Unit Width
    {
        get
        {
            return txtArea.Width;
        }
        set
        {
            txtArea.Width = value;
        }
    }


    /// <summary>
    /// Rows
    /// </summary>
    public int Rows
    {
        get
        {
            return txtArea.Rows;
        }
        set
        {
            txtArea.Rows = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.txtArea.Text;
        }
        set
        {
            this.txtArea.Text = (string)value;
        }
    }


    /// <summary>
    /// Gets ClientID of the text area
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return txtArea.ClientID;
        }
    }


    /// <summary>
    /// Allow macros in the editor
    /// </summary>
    public bool AllowMacros
    {
        get
        {
            if (this.Form != null)
            {
                return this.Form.AllowMacroEditing;
            }

            return mAllowMacros;
        }
        set
        {
            mAllowMacros = value;
        }
    }




    /// <summary>
    /// Text area control
    /// </summary>
    public TextBox TextArea
    {
        get
        {
            return this.txtArea;
        }
    }


    /// <summary>
    /// More button
    /// </summary>
    public Button MoreButton
    {
        get
        {
            return btnMore;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        string mJavaScript = "";
        
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        btnMore.OnClientClick += "ShowLargeTextArea('" + txtArea.ClientID + "', " + (this.AllowMacros ? "true" : "false") + "); return false;";

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ShowLargeTextArea", ScriptHelper.GetScript("function ShowLargeTextArea(txtClientID, allowMacros) { modalDialog('" + ResolveUrl("~/CMSFormControls/Selectors/LargeTextAreaDesigner.aspx") + "?areaid=' + txtClientID + (allowMacros ? '' : '&allowMacros=false'), 'ShowLargeTextArea', 1034, " + (660 - (this.AllowMacros ? 0 : 60)) + "); return false;}"));

        // Set this text area
        mJavaScript += "function SetAreaValue(areaId, areaText ){ document.getElementById(areaId).value = areaText; return false }\n";
        // send value from this text area
        mJavaScript += "function GetAreaValue(areaId)  { return document.getElementById(areaId).value; return false;  }\n";

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AreaGlobal", ScriptHelper.GetScript(mJavaScript));
    }


    /// <summary>
    /// Returns the arraylist of the field IDs (Client IDs of the inner controls) that should be spell checked
    /// </summary>
    public override ArrayList GetSpellCheckFields()
    {
        ArrayList result = new ArrayList();
        result.Add(this.txtArea.ClientID);

        return result;
    }
}
