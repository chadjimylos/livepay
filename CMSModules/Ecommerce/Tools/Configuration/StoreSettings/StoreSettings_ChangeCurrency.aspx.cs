using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.LicenseProvider;

public partial class CMSModules_Ecommerce_Tools_Configuration_StoreSettings_StoreSettings_ChangeCurrency : CMSEcommerceModalPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check permissions (only global admin can see this dialog)
        CurrentUserInfo ui = CMSContext.CurrentUser;

        // Check 'EcommerceConfiguration' permission
        if (!ui.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationRead"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationRead");
        }

        if ((ui == null) || !ui.IsGlobalAdministrator)
        {
            // Redirect to access denied
            RedirectToAccessDenied(ResHelper.GetString("StoreSettings_ChangeCurrency.AccessDenied"));
        }

        // Load the UI
        this.CurrentMaster.Page.Title = "Ecommerce - Change main currency";
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("StoreSettings_ChangeCurrency.ChangeCurrencyTitle");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_Currency/object.png");
        this.chkCredit.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.ChangeCredit");
        this.chkDocuments.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.ChangeDocuments");
        this.chkExchangeRates.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.ChangeExchangeRates");
        this.chkFlatDiscountsCoupons.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.ChangeDiscountCoupons");
        this.chkFlatTaxes.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.ChangeTaxes");
        this.chkFlatVolumeDiscounts.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.ChangeVolumeDiscounts");
        this.chkProductPrices.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.ChangeProductPrices");
        this.chkShipping.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.ChangeShipping");
        this.chkFreeShipping.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.ChangeFreeShipping");
        this.btnCancel.Text = ResHelper.GetString("General.Cancel");
        this.lblExchangeRate.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.ExchangeRate");
        this.lblRound.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.Round");
        this.btnOk.Text = ResHelper.GetString("General.OK");
        this.imgHelp.ImageUrl = GetImageUrl("General/HelpSmall.png");
        this.imgHelp.ToolTip = ResHelper.GetString("StoreSettings_ChangeCurrency.ExchangeRateHelp");
        this.imgRoundHelp.ImageUrl = GetImageUrl("General/HelpSmall.png");
        this.imgRoundHelp.ToolTip = ResHelper.GetString("StoreSettings_ChangeCurrency.ExchangeRateRoundHelp");
        this.btnOk.Attributes["onclick"] = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("StoreSettings_ChangeCurrency.Confirmation")) + ")";

        if (!UrlHelper.IsPostback())
        {
            if (QueryHelper.GetBoolean("saved", false))
            {
                lblInfo.Text = ResHelper.GetString("general.changessaved");
                lblInfo.Visible = true;
            }
        }
    }


    /// <summary>
    /// Changes the selected prices and other object fields
    /// </summary>
    protected void btnOk_Click(object sender, EventArgs e)
    {
        string err = new Validator().NotEmpty(txtEchangeRate.Text.Trim(), ResHelper.GetString("StoreSettings_ChangeCurrency.EmptyExchangeRate"))
                                    .NotEmpty(txtRound.Text.Trim(), ResHelper.GetString("StoreSettings_ChangeCurrency.InvalidRound"))
                                    .IsInteger(txtRound.Text.Trim(), ResHelper.GetString("StoreSettings_ChangeCurrency.InvalidRound"))
                                    .IsDouble(txtEchangeRate.Text.Trim(), ResHelper.GetString("StoreSettings_ChangeCurrency.InvalidExchangeRate"))
                                    .IsPositiveNumber(txtEchangeRate.Text.Trim(), ResHelper.GetString("StoreSettings_ChangeCurrency.NegativeExchangeRate")).Result;

        if (err != "")
        {
            lblError.Text = err;
            lblError.Visible = true;
            return;
        }

        // Get the new currency
        CurrencyInfo ci = CurrencyInfoProvider.GetCurrencyInfo(ValidationHelper.GetInteger(currencyElem.Value, 0));
        if (ci != null)
        {
            // Set new main currency
            CurrencyInfoProvider.RemoveMainCurrency();
            ci.CurrencyIsMain = true;
            CurrencyInfoProvider.SetCurrencyInfo(ci);

            double rate = ValidationHelper.GetDouble(txtEchangeRate.Text, 1);
            int round = ValidationHelper.GetInteger(txtRound.Text, 2);

            try
            {
                // Recalculates the values
                CurrencyInfoProvider.RecalculateValues(rate, round, chkExchangeRates.Checked, chkProductPrices.Checked, chkFlatTaxes.Checked, chkFlatDiscountsCoupons.Checked, chkFlatVolumeDiscounts.Checked, chkCredit.Checked, chkShipping.Checked, chkDocuments.Checked, chkFreeShipping.Checked);

                // Refresh the page
                UrlHelper.Redirect(UrlHelper.AddParameterToUrl(UrlHelper.CurrentURL, "saved", "1"));
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                lblError.Visible = true;
                return;
            }
        }
        else
        {
            // No currency selected
            lblError.Text = ResHelper.GetString("StoreSettings_ChangeCurrency.CurrencyNotSelected");
            lblError.Visible = true;
            return;
        }
    }
}
