CREATE VIEW [View_NewsletterSubscriberUserRole_Joined]
AS
SELECT     Newsletter_Subscriber.*,
                      CMS_User0.UserName, CMS_User0.UserID, CMS_User0.FirstName, CMS_User0.MiddleName, CMS_User0.LastName,
                      CMS_User0.FullName, CMS_User0.Email, CMS_User0.UserEnabled, CMS_Role.RoleID, CMS_Role.RoleName, CMS_Role.SiteID
FROM         Newsletter_Subscriber LEFT OUTER JOIN
                      View_CMS_User AS CMS_User0 ON Newsletter_Subscriber.SubscriberRelatedID = CMS_User0.UserID LEFT OUTER JOIN
                      CMS_Role ON Newsletter_Subscriber.SubscriberRelatedID = CMS_Role.RoleID
WHERE     (Newsletter_Subscriber.SubscriberType IS NULL) AND (Newsletter_Subscriber.SubscriberRelatedID IS NULL)
UNION ALL
SELECT     Newsletter_Subscriber_1.*,
                      CMS_User_1.UserName, CMS_User_1.UserID, CMS_User_1.FirstName, CMS_User_1.MiddleName, CMS_User_1.LastName, CMS_User_1.FullName,
                      CMS_User_1.Email, CMS_User_1.UserEnabled, CMS_Role_1.RoleID, CMS_Role_1.RoleName, CMS_Role_1.SiteID
FROM         Newsletter_Subscriber AS Newsletter_Subscriber_1 INNER JOIN
                      View_CMS_User AS CMS_User_1 ON Newsletter_Subscriber_1.SubscriberRelatedID = CMS_User_1.UserID LEFT OUTER JOIN
                      CMS_Role AS CMS_Role_1 ON Newsletter_Subscriber_1.SubscriberRelatedID = CMS_Role_1.RoleID
WHERE     (Newsletter_Subscriber_1.SubscriberType = 'cms.user')
UNION ALL
SELECT     Newsletter_Subscriber_2.*,
                      CMS_User_2.UserName, CMS_User_2.UserID, CMS_User_2.FirstName, CMS_User_2.MiddleName, CMS_User_2.LastName,
                      CMS_User_2.FullName, CMS_User_2.Email, CMS_User_2.UserEnabled, CMS_Role_2.RoleID, CMS_Role_2.RoleName, CMS_Role_2.SiteID
FROM         Newsletter_Subscriber AS Newsletter_Subscriber_2 INNER JOIN
                      CMS_Role AS CMS_Role_2 ON Newsletter_Subscriber_2.SubscriberRelatedID = CMS_Role_2.RoleID LEFT OUTER JOIN
                      View_CMS_User AS CMS_User_2 ON Newsletter_Subscriber_2.SubscriberRelatedID = CMS_User_2.UserID
WHERE     (Newsletter_Subscriber_2.SubscriberType = 'cms.role')
