using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.LicenseProvider;
using CMS.CMSHelper;
using CMS.MediaLibrary;
using CMS.DirectoryUtilities;

public partial class CMSModules_MediaLibrary_CMSPages_FileUpload : CMSLiveModalPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fileUpload.LibraryID = QueryHelper.GetInteger("libraryid", 0);
        fileUpload.FolderPath = HttpUtility.UrlDecode(QueryHelper.GetString("folderpath", ""));
        fileUpload.IsLiveSite = true;
        fileUpload.OnNotAllowed += new CMSAdminControl.NotAllowedEventHandler(fileUpload_OnNotAllowed);
    }

    void fileUpload_OnNotAllowed(string permissionType, CMSAdminControl sender)
    {
        if (sender != null)
        {
            sender.StopProcessing = true;
        }

        fileUpload.StopProcessing = true;
        fileUpload.Visible = false;
        messageElem.DisplayMessage = true;
        messageElem.ErrorMessage = MediaLibraryHelper.GetAccessDeniedMessage("filecreate");
    }
}