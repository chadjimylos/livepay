using System;

using CMS.GlobalHelper;
using CMS.WebFarmSync;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.LicenseProvider;
using CMS.SettingsProvider;
using CMS.EventLog;

public partial class CMSSiteManager_Administration_WebFarm_WebFarm_Server_Edit : SiteManagerPage
{
    int serverid;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set the page title
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("webfarm.edit.newserver");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_WebFarmServer/new.png");
        this.CurrentMaster.Title.HelpTopicName = "newedit_server";

        // Initialize breadcrumbs 		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("WebFarmServers_Edit.WebFarmServers");
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Administration/WebFarm/WebFarm_Server_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        pageTitleTabs[1, 0] = ResHelper.GetString("WebFarmServers_Edit.New");

        btnOk.Text = ResHelper.GetString("general.ok");
        rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");
        rfvURL.ErrorMessage = ResHelper.GetString("WebFarmServers_Edit.UrlEmpty");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");

        // Get server ID
        serverid = QueryHelper.GetInteger("serverid", 0);

        if (serverid > 0)
        {
            WebFarmServerInfo wi = WebFarmServerInfoProvider.GetWebFarmServerInfo(serverid);

            if (wi == null)
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("WebFarmServers_Edit.InvalidServerID");
                plcEditForm.Visible = false;
            }
            else
            {
                this.CurrentMaster.Title.TitleText = ResHelper.GetString("WebFarmServers_Edit.HeaderCaption");
                this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_WebFarmServer/object.png");
                pageTitleTabs[1, 0] = HTMLHelper.HTMLEncode(wi.ServerDisplayName);

                if (!RequestHelper.IsPostBack())
                {
                    txtCodeName.Text = wi.ServerName;
                    txtDisplayName.Text = wi.ServerDisplayName;
                    txtURL.Text = wi.ServerURL;
                    chkEnabled.Checked = wi.ServerEnabled;
                }
            }
        }

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;

        if (ValidationHelper.GetString(Request.QueryString["saved"], "") != "")
        {
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            lblInfo.Visible = true;
        }
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        string result = new Validator().NotEmpty(rfvDisplayName, rfvDisplayName.ErrorMessage).NotEmpty(rfvCodeName, rfvCodeName.ErrorMessage).NotEmpty(rfvURL, rfvURL.ErrorMessage)
            .IsCodeName(txtCodeName.Text, ResHelper.GetString("general.invalidcodename"))
            .Result;

        // Get the object
        WebFarmServerInfo wi = WebFarmServerInfoProvider.GetWebFarmServerInfo(serverid) ?? new WebFarmServerInfo();

        // Check license web farm server limit
        if (String.IsNullOrEmpty(result))
        {
            LicenseKeyInfo lki = LicenseHelper.CurrentLicenseInfo;
            if (lki == null)
            {
                return;
            }

            // Only if server is enabled
            if (chkEnabled.Checked)
            {
                // Enabling or new server as action insert
                VersionActionEnum action = ((wi.ServerID > 0) && wi.ServerEnabled) ? VersionActionEnum.Edit : VersionActionEnum.Insert;
                if (!lki.CheckServerCount(WebSyncHelperClass.ServerCount, action))
                {
                    result = ResHelper.GetString("licenselimitation.infopagemessage");
                }

                // Log the message
                if (!String.IsNullOrEmpty(result))
                {
                    EventLogProvider eventLog = new EventLogProvider();
                    string message = ResHelper.GetString("licenselimitation.serversexceeded");
                    eventLog.LogEvent("W", DateTime.Now, "WebFarms", LicenseHelper.LICENSE_LIMITATION_EVENTCODE, UrlHelper.CurrentURL, message);
                }
            }
        }


        if (result == "")
        {
            wi.ServerID = serverid;
            wi.ServerDisplayName = txtDisplayName.Text;
            wi.ServerName = txtCodeName.Text;
            wi.ServerURL = txtURL.Text;
            wi.ServerEnabled = chkEnabled.Checked;
            try
            {
                WebFarmServerInfoProvider.SetWebFarmServerInfo(wi);
                // Clear server list
                UrlHelper.Redirect("WebFarm_Server_Edit.aspx?serverid=" + wi.ServerID + "&saved=1");
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message.Replace("%%name%%", wi.ServerName);
                lblError.Visible = true;
                lblInfo.Visible = false;
            }
        }
        else
        {
            lblError.Text = result;
            lblError.Visible = true;
            lblInfo.Visible = false;
        }
    }
}
