﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CacheItemsGrid.ascx.cs"
    Inherits="CMSSiteManager_Administration_System_Debug_CacheItemsGrid" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/Controls/UniGridPager.ascx" TagName="UniGridPager"
    TagPrefix="cms" %>
<asp:PlaceHolder runat="server" ID="plcItems">
    <table border="1" cellspacing="0" cellpadding="3" class="UniGridGrid" rules="rows"
        style="border-collapse: collapse;">
        <tr class="UniGridHead">
            <th style="white-space: nowrap;">
                <asp:Label runat="server" ID="lblKey" EnableViewState="false" />
            </th>
            <asp:PlaceHolder runat="server" ID="plcData">
                <th style="width: 100%;">
                    <asp:Label runat="server" ID="lblData" EnableViewState="false" />
                </th>
            </asp:PlaceHolder>
        </tr>
        <asp:Literal ID="ltlCacheInfo" runat="server" EnableViewState="false" />
    </table>
    <cms:UniGridPager ID="pagerItems" ShowDirectPageControl="true" runat="server" />
    <br />
    <br />
</asp:PlaceHolder>
