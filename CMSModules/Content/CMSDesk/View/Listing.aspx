<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Listing.aspx.cs" Inherits="CMSModules_Content_CMSDesk_View_Listing"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Content - Listing" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Cultures/SiteCultureSelector.ascx" TagName="SiteCultureSelector"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <asp:Panel ID="pnlFilter" runat="server" DefaultButton="btnShow">
        <table>
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblName" runat="server" ResourceString="general.documentname"
                        DisplayColon="true" />
                </td>
                <td>
                    <asp:DropDownList ID="drpOperator" runat="server" CssClass="ContentDropdown" />
                    <asp:TextBox ID="txtName" runat="server" Width="265" MaxLength="100" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblType" runat="server" ResourceString="general.documenttype"
                        DisplayColon="true" />
                </td>
                <td>
                    <asp:DropDownList ID="drpOperator2" runat="server" CssClass="ContentDropdown" />
                    <asp:TextBox ID="txtType" runat="server" Width="265" MaxLength="100" />
                </td>
            </tr>
            <asp:PlaceHolder ID="plcLang" runat="server">
                <tr>
                    <td style="vertical-align: bottom;">
                        <cms:LocalizedLabel ID="lblLanguage" runat="server" ResourceString="general.language"
                            DisplayColon="true" />
                    </td>
                    <td>
                        <asp:DropDownList ID="drpLanguage" runat="server" CssClass="SelectorDropDown" Width="145px" />
                        <cms:SiteCultureSelector runat="server" ID="cultureElem" IsLiveSite="false" />
                    </td>
                </tr>
            </asp:PlaceHolder>
            <tr>
                <td>
                </td>
                <td>
                    <cms:LocalizedButton ID="btnShow" runat="server" ResourceString="general.show" CssClass="ContentButton"
                        OnClick="gridDocuments_OnShowButtonClick" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <cms:UniGrid ID="gridDocuments" runat="server" GridName="Listing.xml" EnableViewState="true"
        DelayedReload="true" IsLiveSite="false" />
    <asp:Panel ID="pnlFooter" runat="server" CssClass="MassAction">
        <asp:DropDownList ID="drpWhat" runat="server" CssClass="DropDownFieldSmall" />
        <asp:DropDownList ID="drpAction" runat="server" CssClass="DropDownFieldSmall" />
        <cms:LocalizedButton ID="btnOk" runat="server" ResourceString="general.ok" CssClass="SubmitButton"
            EnableViewState="false" />
        <br />
        <br />
    </asp:Panel>
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />

    <script type="text/javascript" src="Listing.js"></script>

</asp:Content>
