CREATE TABLE [CMS_SearchIndexSite] (
		[IndexID]         int NOT NULL,
		[IndexSiteID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_SearchIndexSite]
	ADD
	CONSTRAINT [PK_CMS_SearchIndexSite]
	PRIMARY KEY
	([IndexID], [IndexSiteID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_SearchIndexSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_SearchIndex]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_SearchIndexSite_IndexID_CMS_SearchIndex]') IS NULL
BEGIN
		ALTER TABLE [CMS_SearchIndexSite]
			ADD CONSTRAINT [FK_CMS_SearchIndexSite_IndexID_CMS_SearchIndex]
			FOREIGN KEY ([IndexID]) REFERENCES [CMS_SearchIndex] ([IndexID])
END
IF OBJECT_ID(N'[CMS_SearchIndexSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_SearchIndexSite_IndexSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_SearchIndexSite]
			ADD CONSTRAINT [FK_CMS_SearchIndexSite_IndexSiteID_CMS_Site]
			FOREIGN KEY ([IndexSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
