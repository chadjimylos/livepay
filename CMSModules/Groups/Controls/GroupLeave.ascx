<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupLeave.ascx.cs" Inherits="CMSModules_Groups_Controls_GroupLeave" %>
<table class="JoinTable">
    <tr>
        <td colspan="2" class="InfoArea">
            <asp:Label runat="server" ID="lblInfo" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="ButtonsArea">
            <cms:CMSButton runat="server" CssClass="ContentButton" ID="btnLeave" OnClick="btnLeave_Click" EnableViewState="false" />&nbsp;<cms:CMSButton
                runat="server" CssClass="ContentButton" ID="btnCancel" EnableViewState="false"/>
        </td>
    </tr>
</table>