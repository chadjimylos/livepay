<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectReportCategory.ascx.cs" Inherits="CMSModules_Reporting_FormControls_SelectReportCategory" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="usCategories" runat="server" ObjectType="reporting.reportcategory" SelectionMode="SingleDropDownList" 
         DisplayNameFormat="{%CategoryDisplayName%}" ReturnColumnName="CategoryId" AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>

