﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports LivePay_ESBBridge
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.FormEngine
Imports CMS.SettingsProvider
Imports CMS.DataEngine
Imports CMS.EmailEngine

Partial Class CMSTemplates_LivePay_MerchantRegistration
    Inherits CMSUserControl


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        btnOK.ImageUrl = ResHelper.LocalizeString(String.Concat("{$=/App_Themes/LivePay/Contact/btnSend.png|en-us=/App_Themes/LivePay/Contact/btnSend_en-us.png$}"))
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        ElseIf Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
        SetErrorMessages()
    End Sub

    Public Overloads Sub ReloadData()
        SetErrorMessages()
    End Sub

    Private Sub SetErrorMessages()
        Dim dt As DataTable = DBConnection.GetErrorMessages(3)
        Dim JsStringMessages As String = String.Empty
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim ErrorMessageStr As String = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("ErrorMessageGR").ToString, "|en-us=", dt.Rows(i)("ErrorMessageEN").ToString, "$}"))
            ErrorMessageStr = Replace(ErrorMessageStr, "'", "\'")
            JsStringMessages = String.Concat(JsStringMessages, " ", dt.Rows(i)("CodeName").ToString, " = '", ErrorMessageStr, "'; ")
        Next
        JsScript(JsStringMessages)
    End Sub
    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        If ValidationHelper.IsEmail(txtEmail.Text.ToLower()) = True And IsNumeric(ContactPhone.Text) = True And txtAFM.Text.Length = 9 And IsNumeric(txtAFM.Text) = True Then
            Dim bizFormName As String = "UserDetails"
            Dim siteName As String = CMSContext.CurrentSiteName
            Dim bfi As BizFormInfo = BizFormInfoProvider.GetBizFormInfo(bizFormName, siteName)
            If Not IsNothing(bfi) Then
                Dim dci As DataClassInfo = DataClassInfoProvider.GetDataClass(bfi.FormClassID)

                Dim CompanyName_Item As String = StripHTMLFunctions.StripTags(CompanyName.Text)
                Dim CommunicationsOfficer_Item As String = StripHTMLFunctions.StripTags(CommunicationsOfficer.Text)
                Dim Email_Item As String = StripHTMLFunctions.StripTags(txtEmail.Text)
                Dim Comments_Item As String = StripHTMLFunctions.StripTags(Comments.Text)

                If Not IsNothing(dci) Then
                    Dim genConn As GeneralConnection = ConnectionHelper.GetConnection()

                    Dim formRecord As DataClass = New DataClass(dci.ClassName, genConn)
                    formRecord.SetValue("FormInserted", DateTime.Now)
                    formRecord.SetValue("FormUpdated", DateTime.Now)
                    formRecord.SetValue("CompanyName", CompanyName_Item)
                    formRecord.SetValue("CommunicationsOfficer", CommunicationsOfficer_Item)
                    formRecord.SetValue("ContactPhone", ContactPhone.Text)
                    formRecord.SetValue("Email", Email_Item)
                    formRecord.SetValue("AFM", txtAFM.Text)
                    formRecord.SetValue("Comments", Comments_Item)
                    formRecord.Insert()

                    'Dim message As EmailMessage = New EmailMessage
                    'message.EmailFormat = EmailFormatEnum.Default
                    'message.From = "info@livepay.gr"
                    'message.Subject = "Merchant Details from Livepay.gr"
                    'message.Recipients = "egasteratou@eurobank-cards.gr"
                    'message.Body = "Επωνυμία Επιχείρησης: " & CompanyName.Text & "<br>Υπεύθυνος Επικοινωνίας: " & CommunicationsOfficer.Text & "<br>Τηλέφωνο Επικοινωνίας: " & ContactPhone.Text & "<br>Email: " & txtEmail.Text & "<br>AFM: " & txtAFM.Text & "<br>Σχόλια: " & Comments.Text
                    'EmailSender.SendEmail(message)

                    bfi.FormItems += 1
                    BizFormInfoProvider.SetBizFormInfo(bfi)

                    Dim Content As IDataClass = DataClassFactory.NewDataClass(dci.ClassName, formRecord.ID, genConn)

                    Dim sendFromEmail As String = bfi.FormSendFromEmail
                    Dim sendToEmail As String = bfi.FormSendToEmail
                    Dim sendEmailtoAdmin As Boolean = ((Not String.IsNullOrEmpty(sendFromEmail)) AndAlso (sendToEmail <> ""))
                    If sendEmailtoAdmin Then
                        If Not formRecord.IsEmpty Then
                            Dim row As DataRow = Content.DataRow
                            Dim bizform_ As New CMS.FormControls.BizForm()
                            bizform_.SendNotificationEmail(sendFromEmail, sendToEmail, row, bfi, Content)
                        End If
                    End If
                    Response.Redirect("~/Merchants_Thanks.aspx")
                End If
            End If
        End If
    End Sub
End Class
