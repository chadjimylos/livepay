using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.Community;
using CMS.GlobalHelper;
using CMS.PortalControls;

public partial class CMSWebParts_Community_Membership_MyInvitations : CMSAbstractWebPart
{
    #region "Private variables"

    private int mUserId = 0;
    protected string mDeleteImageUrl = string.Empty;
    protected string mAcceptImageUrl = string.Empty;
    protected string mDeleteToolTip = string.Empty;
    protected string mAcceptToolTip = string.Empty;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets URL of the delete button image
    /// </summary>
    public string DeleteImageUrl
    {
        get
        {
            return UrlHelper.ResolveUrl(DataHelper.GetNotEmpty(GetValue("DeleteImageUrl"), GetImageUrl("Design/Controls/UniGrid/Actions/delete.png")));
        }
        set
        {
            mDeleteImageUrl = value;
            SetValue("DeleteImageUrl", value);
        }
    }


    /// <summary>
    /// Gets or sets URL of the accept button image
    /// </summary>
    public string AcceptImageUrl
    {
        get
        {
            return UrlHelper.ResolveUrl(DataHelper.GetNotEmpty(GetValue("AcceptImageUrl"), GetImageUrl("Design/Controls/UniGrid/Actions/Approve.png")));
        }
        set
        {
            mDeleteImageUrl = value;
            SetValue("AcceptImageUrl", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether control should be hidden if no data found
    /// </summary>
    public bool HideControlForZeroRows
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("HideControlForZeroRows"), rptMyInvitations.HideControlForZeroRows);
        }
        set
        {
            SetValue("HideControlForZeroRows", value);
            rptMyInvitations.HideControlForZeroRows = value;
        }
    }


    /// <summary>
    /// Gets or sets the text which is displayed when there is no data
    /// </summary>
    public string ZeroRowsText
    {
        get
        {
            return ValidationHelper.GetString(GetValue("ZeroRowsText"), rptMyInvitations.ZeroRowsText);
        }
        set
        {
            SetValue("ZeroRowsText", value);
            rptMyInvitations.ZeroRowsText = value;
        }
    }


    public int UserID
    {
        get
        {
            mUserId = ValidationHelper.GetInteger(GetValue("UserID"), 0);
            return (mUserId == 0) ? CMSContext.CurrentUser.UserID : mUserId;
        }
        set
        {
            SetValue("UserID", value);
            mUserId = value;
        }
    }

    #endregion


    #region "Caption properties"


    public string InvitationIsNotValid
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("InvitationIsNotValid"), ResHelper.GetString("group.invitationisnotvalid"));
        }
    }


    public string GroupNoLongerExists
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("GroupNoLongerExists"), ResHelper.GetString("group.nolongerexists"));
        }
    }


    public string MemberJoined
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("MemberJoined"), ResHelper.GetString("groups.memberjoined"));
        }
    }


    public string MemberWaiting
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("MemberWaiting"), ResHelper.GetString("groups.memberjoinedwaiting"));
        }
    }


    public string UserIsAlreadyMember
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("UserIsAlreadyMember"), ResHelper.GetString("groups.userisalreadymember"));
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    protected void SetupControl()
    {
        if (StopProcessing)
        {
            Visible = false;
            // Do not load data
        }
        else
        {
            ltlMessage.Text = ScriptHelper.GetScript("var deleteMessage ='" + ResHelper.GetString("general.confirmdelete") + "';");
            ltlMessage.Text += ScriptHelper.GetScript("var acceptMessage ='" + ResHelper.GetString("groupinvitation.confirmaccept") + "';");
            rptMyInvitations.ZeroRowsText = ZeroRowsText;
            rptMyInvitations.HideControlForZeroRows = HideControlForZeroRows;
            mDeleteImageUrl = DeleteImageUrl;
            mAcceptImageUrl = AcceptImageUrl;
            mAcceptToolTip = ResHelper.GetString("general.accept");
            mDeleteToolTip = ResHelper.GetString("general.delete");
            BindData();
        }
    }


    /// <summary>
    /// Deletes invitation
    /// </summary>
    protected void btnDelete_OnCommand(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            int invitationId = ValidationHelper.GetInteger(e.CommandArgument, 0);
            InvitationInfoProvider.DeleteInvitationInfo(invitationId);
            BindData();
        }
    }


    /// <summary>
    /// Accepts invitation
    /// </summary>
    protected void btnAccept_OnCommand(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "accept")
        {
            int invitationId = ValidationHelper.GetInteger(e.CommandArgument, 0);
            InvitationInfo invitation = InvitationInfoProvider.GetInvitationInfo(invitationId);

            if ((invitation.InvitationValidTo == DateTimeHelper.ZERO_TIME) ||
                                (invitation.InvitationValidTo >= DateTime.Now))
            {
                GroupInfo group = GroupInfoProvider.GetGroupInfo(invitation.InvitationGroupID);
                if (group != null)
                {
                    if (!GroupMemberInfoProvider.IsMemberOfGroup(invitation.InvitedUserID, invitation.InvitationGroupID))
                    {
                        // Create group member info object
                        GroupMemberInfo groupMember = new GroupMemberInfo();
                        groupMember.MemberInvitedByUserID = invitation.InvitedByUserID;
                        groupMember.MemberUserID = UserID;
                        groupMember.MemberGroupID = invitation.InvitationGroupID;
                        groupMember.MemberJoined = DateTime.Now;

                        // Set proper status depending on grouo settings
                        switch (group.GroupApproveMembers)
                        {
                            case GroupApproveMembersEnum.ApprovedCanJoin:
                                groupMember.MemberStatus = GroupMemberStatus.WaitingForApproval;
                                lblInfo.Text = MemberWaiting.Replace("##GROUPNAME##", HTMLHelper.HTMLEncode(group.GroupDisplayName)) + "<br /><br />";
                                break;
                            default:
                                groupMember.MemberApprovedByUserID = invitation.InvitedByUserID;
                                groupMember.MemberApprovedWhen = DateTime.Now;
                                groupMember.MemberStatus = GroupMemberStatus.Approved;
                                lblInfo.Text = MemberJoined.Replace("##GROUPNAME##", HTMLHelper.HTMLEncode(group.GroupDisplayName)) + "<br /><br />";
                                break;
                        }
                        // Store info object to database
                        GroupMemberInfoProvider.SetGroupMemberInfo(groupMember);

                        // Delete all invitations to specified group for specified user
                        string whereCondition = "InvitationGroupID = " + invitation.InvitationGroupID + " AND (InvitedUserID=" + UserID + ")";
                        InvitationInfoProvider.DeleteInvitations(whereCondition);
                    }
                    else
                    {
                        lblInfo.Text = UserIsAlreadyMember.Replace("##GROUPNAME##", HTMLHelper.HTMLEncode(group.GroupDisplayName)) + "<br /><br />";
                        lblInfo.CssClass = "InvitationErrorLabel";
                        InvitationInfoProvider.DeleteInvitationInfo(invitation);
                    }
                }
                else
                {
                    lblInfo.Text = GroupNoLongerExists + "<br /><br />"; ;
                    lblInfo.CssClass = "InvitationErrorLabel";
                    InvitationInfoProvider.DeleteInvitationInfo(invitation);
                }
            }
            else
            {
                lblInfo.Text = InvitationIsNotValid + "<br /><br />"; ;
                lblInfo.CssClass = "InvitationErrorLabel";
                InvitationInfoProvider.DeleteInvitationInfo(invitation);
            }

            lblInfo.Visible = true;
            BindData();
        }
    }


    /// <summary>
    /// Binds data to repeater
    /// </summary>
    private void BindData()
    {
        if (UserID != 0)
        {
            DataSet invitations = InvitationInfoProvider.GetMyInvitations("InvitedUserID=" + UserID, "InvitationCreated");
            rptMyInvitations.DataSource = invitations;
            rptMyInvitations.DataBind();
            // Hide control if no data
            if (DataHelper.DataSourceIsEmpty(invitations) && (HideControlForZeroRows))
            {
                Visible = false;
                rptMyInvitations.Visible = false;
            }
        }
    }


    /// <summary>
    /// Resolve text
    /// </summary>
    /// <param name="value">Input value</param>
    public string ResolveText(object value)
    {
        return HTMLHelper.HTMLEncode(ValidationHelper.GetString(value, ""));
    }

    #endregion
}
