<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjectTree.ascx.cs" Inherits="CMSAdminControls_UI_Trees_ObjectTree" %>
<asp:Label runat="server" ID="lblError" ForeColor="Red" EnableViewState="false" />
<asp:TreeView ID="treeElem" runat="server" ShowLines="True" CssClass="ContentTree"
    EnableViewState="true" />
