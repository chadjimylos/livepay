<%@ Control Language="VB" AutoEventWireup="false" CodeFile="~/CMSWebParts/LivePay/GetImageRep.ascx.cs" Inherits="CMSWebParts_LivePay_GetImageRep" %>

<cms:CMSEditModeButtonAdd ID="btnAdd" runat="server" Visible="False" />


<table cellpadding="0" cellspacing="0" border="0" class="PopUpMainTable">
    <tr>
	<td class="PopTopLeft">&nbsp;</td>
        <td class="PopTopCenter">&nbsp;</td>
        <td class="PopTopClose"><img src="/App_Themes/LivePay/PopUp/Close.png"  alt="Close" title="Close" style="cursor:pointer;" onclick="$.unblockUI()"/></td>
	<td class="PopTopRight">&nbsp;</td>  
    </tr>
    <tr>
	<td class="PopCenterLeft">&nbsp;</td>
	<td colspan="2" class="PopCenter">
        <table cellpadding="0" cellspacing="0">
            <tr><td class="PopTopTitleSmall" id="TdCompanyTitle" runat="server">sss</td>
            <td class="PopTopLogo">
            <%--<cms:CMSRepeater runat="server" id="repImg" TransformationName="LivePay.Merchant.SubRepMerchant" SelectedItemTransformationName="LivePay.Merchant.SubRepMerchant"  ClassNames="LivePay.Merchant" Path="/%" />--%>
            <cms:CMSRepeater ID="repItems" runat="server" /></td>
            </tr>
            <tr><td class="PopTopTitleUnder"  colspan="2">&nbsp;</td></tr>
            <tr><td class="PopContent" colspan="2">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="PopTransRecTitle">���������� & ���</td>
                        <td class="PopTransDetValue" id="tdDtm" runat="server">������� 31 ������� 2010, 17:45</td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle">������� ����</td>
                        <td class="PopTransDetValue" id="tdCompPay" runat="server">Wind Hellas ���������������</td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle">������� ��������</td>
                        <td class="PopTransDetValue" id="tdPayCode" runat="server">xxxxx</td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle">������� ������</td>
                        <td class="PopTransDetValue" id="tdCustCode" runat="server">011558962</td>
                    </tr>
                    <tr>
                        <td colspan="2" width="100%">
                            <table cellpadding="0" cellspacing="0" id="tblCustomFields" runat="server">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle">����</td>
                        <td class="PopTransDetValue" id="tdPrice" runat="server">20,00</td>
                    </tr>

                    <tr>
                        <td class="PopTransRecTitle">�����</td>
                        <td class="PopTransDetValue" id="tdCardNo" runat="server">xxxx-xxxx-xxxx-1234</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="PopTransDetButtons"><asp:ImageButton ID="btnSaveToPDF" runat="server" ImageUrl="/app_themes/LivePay/popup/btnpdf.png" />&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="btnPrint" runat="server" ImageUrl="/app_themes/LivePay/popup/btnPrint.png" /></td>
                    </tr>
                   
                </table>
            </td></tr>
        </table>
    </td>
	<td class="PopCenterRight">&nbsp;</td>
    </tr>
    <tr>
	<td class="PopBotLeft">&nbsp;</td>
	<td colspan="2" class="PopBotCenter"></td>
	<td class="PopBotRight">&nbsp;</td>
    </tr>
</table>