using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Controls_ShoppingCart_OrderItemEdit : CMSEcommercePage
{
    #region "Variables"

    private ShoppingCartInfo mShoppingCartObj = null;
    private ShoppingCartItemInfo mShoppingCartItemObj = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Shopping cart object with order data
    /// </summary>
    private ShoppingCartInfo ShoppingCartObj
    {
        get
        {
            if (mShoppingCartObj == null)
            {
                string cartSessionName = QueryHelper.GetString("cart", String.Empty);
                if (cartSessionName != String.Empty)
                {
                    mShoppingCartObj = SessionHelper.GetValue(cartSessionName) as ShoppingCartInfo;                     
                }
            }
            return mShoppingCartObj;
        }
    }


    /// <summary>
    /// Shopping cart item data
    /// </summary>
    private ShoppingCartItemInfo ShoppingCartItemObj
    {
        get
        {
            if (mShoppingCartItemObj == null)
            {
                if (this.ShoppingCartObj != null)
                {                    
                    Guid cartItemGuid = QueryHelper.GetGuid("itemguid", Guid.Empty);
                    if (cartItemGuid != Guid.Empty)
                    {
                        mShoppingCartItemObj = this.ShoppingCartObj.GetShoppingCartItemInfo(cartItemGuid);
                    }
                }
            }
            return mShoppingCartItemObj;
        }
    }

    #endregion

    
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'EcommerceRead' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceRead"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceRead");
        }


        // Initialize controls
        lblSKUName.Text = ResHelper.GetString("OrderItemEdit.SKUName");
        lblSKUPrice.Text = ResHelper.GetString("OrderItemEdit.SKUPrice");
        lblSKUUnits.Text = ResHelper.GetString("OrderItemEdit.SKUUnits");
        btnOk.Text = ResHelper.GetString("General.OK");

        // Initialize validators
        rfvSKUName.ErrorMessage = ResHelper.GetString("OrderItemEdit.ErrorSKUName");
        rfvSKUUnits.ErrorMessage = ResHelper.GetString("OrderItemEdit.ErrorSKUUnits");
        txtSKUPrice.EmptyErrorMessage = ResHelper.GetString("OrderItemEdit.ErrorSKUNPrice");
        txtSKUPrice.ValidationErrorMessage = ResHelper.GetString("NewProduct.SKUPriceNotDouble");

        // Title
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_OrderItem/object.png");
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("OrderItemEdit.Title");

        // Load data
        if (!RequestHelper.IsPostBack())
        {
            LoadData();
        }

        RegisterEscScript();
        RegisterModalPageScripts();
    }


    /// <summary>
    /// Loads product data to the form fields
    /// </summary>
    private void LoadData()
    {
        if ((this.ShoppingCartItemObj != null) && (this.ShoppingCartItemObj.SKUObj != null))
        {
            // Load data
            txtSKUName.Text = this.ShoppingCartItemObj.SKUObj.SKUName;
            txtSKUPrice.Value = this.ShoppingCartItemObj.SKUObj.SKUPrice;
            txtSKUPrice.Value = ExchangeTableInfoProvider.ApplyExchangeRate(this.ShoppingCartItemObj.SKUObj.SKUPrice, this.ShoppingCartObj.ExchangeRate);
            txtSKUUnits.Text = this.ShoppingCartItemObj.CartItemUnits.ToString();

            // Disable units editing when it is a product option
            txtSKUUnits.ReadOnly = (this.ShoppingCartItemObj.CartItemParentGUID != Guid.Empty);
        }
    }


    protected void btnOk_Click(object sender, EventArgs e)
    {
        // Check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        string errorMessage = ValidateForm();

        if (errorMessage == "")
        {
            this.ShoppingCartItemObj.SKUObj.SKUName = txtSKUName.Text;
            this.ShoppingCartItemObj.SKUObj.SKUPrice = ExchangeTableInfoProvider.ApplyExchangeRate(txtSKUPrice.Value, 1 / this.ShoppingCartObj.ExchangeRate);
            this.ShoppingCartItemObj.CartItemUnits = ValidationHelper.GetInteger(txtSKUUnits.Text, 0);

            // Update units of the product options
            foreach (ShoppingCartItemInfo option in ShoppingCartItemObj.ProductOptions)
            {
                option.CartItemUnits = ShoppingCartItemObj.CartItemUnits;
            }

            // Evaluate shopping cart content
            ShoppingCartInfoProvider.EvaluateShoppingCartContent(this.ShoppingCartObj);

            // Close dialog window and refresh parent window
            string url = "~/CMSModules/Ecommerce/Tools/Orders/Order_Edit_OrderItems.aspx?orderid=" + this.ShoppingCartObj.OrderId + "&cartexist=1";
            ltlScript.Text = ScriptHelper.GetScript("CloseAndRefresh('" + ResolveUrl(url) + "')");
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    /// <summary>
    /// Validates form data.
    /// </summary>
    private string ValidateForm()
    {
        string error = new Validator().NotEmpty(txtSKUName.Text.Trim(), rfvSKUName.ErrorMessage).
                        NotEmpty(txtSKUUnits.Text.Trim(), rfvSKUUnits.ErrorMessage).Result;                        

        // Validate product price
        if (error == "")
        {
            error = txtSKUPrice.ValidatePrice(this.ShoppingCartItemObj.IsProductOption);
        }

        // Validate product units
        if (error == "")
        {
            if (ValidationHelper.GetInteger(txtSKUUnits.Text.Trim(), -1) < 0)
            {
                error = ResHelper.GetString("OrderItemEdit.SKUUnitsNotPositiveInteger");
            }
        }


        return error;
    }
}
