<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebPart_Edit_Layout_New.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_WebParts_Development_WebPart_Edit_Layout_New"
    Theme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Web part layout - New</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>
</head>
<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel ID="pnlBody" runat="server" CssClass="TabsPageBody">
        <asp:Panel runat="server" ID="pnlScroll" CssClass="TabsPageScrollArea">
            <asp:Panel runat="server" ID="pnlTab" CssClass="TabsPageContent">
                <asp:Panel ID="pblContent" runat="server" CssClass="PageContent">
                    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
                        Visible="false" />
                    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
                        Visible="false" />
                    <table style="vertical-align: top">
                        <tr>
                            <td class="FieldLabel">
                                <asp:Label runat="server" ID="lblDisplayName" EnableViewState="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <asp:Label runat="server" ID="lblCodeName" EnableViewState="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                                <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtCodeName" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <asp:Label runat="server" ID="lblDescription" EnableViewState="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="TextAreaField"
                                    MaxLength="450" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <asp:Label runat="server" ID="lblCode" EnableViewState="false" />
                            </td>
                            <td>
                                <cms:ExtendedTextArea ID="tbCode" runat="server" CssClass="TextAreaCode" TextMode="MultiLine"
                                    Width="600px" />
                                <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="tbCode"
                                    Display="Dynamic" EnableViewState="False"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <cms:CMSButton runat="server" ID="btnOk" EnableViewState="false" CssClass="SubmitButton"
                                    OnClick="btnOK_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
