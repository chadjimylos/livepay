Option Strict Off
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports Microsoft.VisualBasic
Imports CMS.DataEngine

Imports System.Collections
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.Controls

Imports CMS.CMSHelper
Imports CMS.FormControls
Imports CMS.SettingsProvider
Imports CMS.SiteProvider
Imports CMS.ExtendedControls
Partial Class NOK
    Inherits Web.UI.Page

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Ref As String = Request("ref")

        Lalibela_DBConnection.TransactionLogAdd(Ref, "NOTOK Occured")
        UpdateMerchantRefStatus(Ref)
        UpdateBizForm(Ref)
        'SendConfirmationEmail(orderID)
    End Sub

    Sub UpdateMerchantRefStatus(ByVal MerchantRef As String)

        Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()

        Dim objConnection As New SqlConnection(strConnection)
        objConnection.Open()

        Dim objCommand As New SqlCommand("Lalibela_UpdateMerchantRefStatus", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@MerchantRef", MerchantRef)
        objCommand.Parameters.AddWithValue("@Successful", 0)

        objCommand.ExecuteNonQuery()
        objConnection.Close()
    End Sub

    Sub UpdateBizForm(ByVal MerchantRef As String)
        Dim CreditCardData As String = "MerchantRef: " + MerchantRef
        Dim LalibelaDonateID As Integer = Session.Item("BizFormID")
        Dim ds As New DataSet()
        Response.Write(CreditCardData)
        Dim parameters As Object(,) = New Object(2, 2) {}
        parameters(0, 0) = "@LalibelaDonateID"
        parameters(0, 1) = LalibelaDonateID
        parameters(1, 0) = "@CreditCardData"
        parameters(1, 1) = CreditCardData
        parameters(2, 0) = "@PaidStatus"
        parameters(2, 1) = "False"
        ds = ConnectionHelper.GetConnection().ExecuteQuery("Lalibela.Transformation.UpdateDonateCreditCard", parameters)

    End Sub

End Class
