<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IDTransaction.ascx.cs" Inherits="CMSFormControls_IDTransaction" %>
<script type="text/javascript">
    function checkIDTransaction(sender, args) {
        var transID = args.Value;
        var bIsValid = false;

        var RegExp_TransID = new RegExp(/^\d{9}\s\d{11}\s\d{3,11}$/);
        
        if (RegExp_TransID.test(transID))
            bIsValid = true;

        //alert(bIsValid);

        if (bIsValid) {
            var mSplitResult = transID.split(' ');

            var taxID = mSplitResult[0];
            var transID = mSplitResult[1];
            var price = Right("00000000000" + mSplitResult[2], 11);

            taxID = taxID.split('').reverse().join('');

            var Num1 = 0;
            for (var iDigit = 1; iDigit <= 8; iDigit++) {
                Num1 += taxID.charAt(iDigit) << iDigit;
            }

            if (Num1 == 0) {
                bIsValid = false;

            } else if ((Num1 % 11) % 10 == taxID.charAt(0)) {
                bIsValid = true;
            }
            else {
                bIsValid = false;
            }
        }

        var IsCutomValid = sender.getAttribute('IsCustomValidator');
        var CustomErrorMess = sender.getAttribute('CustomValidatorErrorMessage');
        if (IsCutomValid != null) {
            if (IsCutomValid.indexOf('CustomCloud_') != -1) {
                if (!bIsValid) {
                    Tab1_ValidationCloud('#' + IsCutomValid, CustomErrorMess, true, '')
                } else {
                    Tab1_ValidationCloud('#' + IsCutomValid, '', false, '')
                }
            }

        }
        args.IsValid = bIsValid;
    }
</script>

<div runat="server" id="CustomCloud_div"></div>

<asp:TextBox runat="server" ID="IDTransaction" cssclass="RegUsersTab1CustomTxt" width="260px" MaxLength="33" />

<div class="HideIt">
    <asp:CustomValidator ID="cv_IDTransaction" 
                        ClientValidationFunction="checkIDTransaction" runat="server"   
                        ControlToValidate="IDTransaction" ValidationGroup="Tab1_PayForm" />
</div>