using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.PortalControls;
using CMS.PortalEngine;

public partial class CMSModules_PortalEngine_Controls_Layout_PlaceholderMenu : CMSAbstractPortalUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Use UI culture for strings
        string culture = CMSContext.CurrentUser.PreferredUICultureCode;

        if ((mPagePlaceholder != null) && (mPagePlaceholder.ViewMode == ViewModeEnum.DesignDisabled))
        {
            // Hide edit layout and edit template if design mode is disabled
            this.pnlLayout.Visible = false;
            this.pnlTemplate.Visible = false;
        }
        else
        {
            // Edit layout
            this.imgLayout.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/ContextMenu/Layout.png");
            this.lblLayout.Text = ResHelper.GetString("PlaceholderMenu.IconLayout", culture);
            this.imgLayout.AlternateText = this.lblLayout.Text;
            //this.lblLayout.ToolTip = ResHelper.GetString("WebPartMenu.Layout", culture);
            this.pnlLayout.Attributes.Add("onclick", "ContextEditLayout(GetContextMenuParameter('pagePlaceholderMenu'));");

            // Template properties
            this.imgTemplate.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/ContextMenu/Template.png");
            this.lblTemplate.Text = ResHelper.GetString("PlaceholderMenu.IconTemplate", culture);
            this.imgTemplate.AlternateText = this.lblTemplate.Text;
            //this.lblTemplate.ToolTip = ResHelper.GetString("WebPartMenu.Template", culture);
            this.pnlTemplate.Attributes.Add("onclick", "EditTemplate(GetContextMenuParameter('pagePlaceholderMenu'));");
        }

        this.imgClone.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/ContextMenu/Clonetemplate.png");
        this.lblClone.Text = ResHelper.GetString("PlaceholderMenu.IconClone", culture);
        this.imgClone.AlternateText = this.lblClone.Text;
        //this.lblClone.ToolTip = ResHelper.GetString("WebPartMenu.Clone", culture);
        this.pnlClone.Attributes.Add("onclick", "CloneTemplate(GetContextMenuParameter('pagePlaceholderMenu'));");

        this.imgRefresh.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/ContextMenu/Refresh.png");
        this.lblRefresh.Text = ResHelper.GetString("PlaceholderMenu.IconRefresh", culture);
        this.imgRefresh.AlternateText = this.lblRefresh.Text;
        //this.lblRefresh.ToolTip = ResHelper.GetString("WebPartMenu.Refresh", culture);
        this.pnlRefresh.Attributes.Add("onclick", "RefreshPage();");
    }
}
