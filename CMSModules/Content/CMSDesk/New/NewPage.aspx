<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewPage.aspx.cs" Inherits="CMSModules_Content_CMSDesk_New_NewPage"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" Title="Content - New page"
    EnableEventValidation="false" %>

<%@ Register Src="~/CMSModules/PortalEngine/Controls/Layout/PageTemplateSelector.ascx"
    TagName="PageTemplateSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/PortalEngine/Controls/Layout/LayoutFlatSelector.ascx"
    TagName="LayoutFlatSelector" TagPrefix="cms" %>
<%@ Register TagPrefix="cms" Namespace="CMS.UIControls" Assembly="CMS.UIControls" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">

    <script src="../../../../CMSScripts/shortcuts.js" type="text/javascript"></script>

    <script src="../../../../CMSAdminControls/SpellChecker/spell.js" type="text/javascript"></script>

    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    <div class="NewPageDialog">
        <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlContent" runat="server" CssClass="PageContentFrame">
                    <div class="PTSelection">
                        <div style="padding: 5px 5px 0px 5px;">
                            <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
                                Visible="false" />
                            <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" />
                        </div>
                        <table cellpadding="0" cellspacing="0" class="Table" border="0">
                            <tr class="HeaderRow">
                                <td class="LeftBorder">
                                </td>
                                <td style="vertical-align: top;" class="Header">
                                    <cms:LocalizedLabel ID="lblPageName" runat="server" ResourceString="NewPage.PageName" />
                                    <asp:TextBox ID="txtPageName" runat="server" CssClass="TextBoxField" MaxLength="100" /><br />
                                </td>
                                <td class="RightBorder">
                                </td>
                            </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" class="Table" border="0">
                            <tr class="Row">
                                <td style="vertical-align: top;" class="Content">
                                    <div class="RadioPanel">
                                        <cms:LocalizedRadioButton ID="radUseTemplate" runat="server" GroupName="NewPage"
                                            Checked="true" CssClass="LeftAlign" ResourceString="NewPage.UseTemplate" AutoPostBack="true" />
                                        <cms:LocalizedRadioButton ID="radInherit" CssClass="LeftAlign" runat="server" GroupName="NewPage"
                                            ResourceString="NewPage.Inherit" AutoPostBack="true" />
                                        <cms:LocalizedRadioButton ID="radCreateBlank" CssClass="LeftAlign" runat="server"
                                            GroupName="NewPage" ResourceString="NewPage.CreateBlank" AutoPostBack="true"
                                            Checked="false" />
                                    </div>
                                    <div style="margin-left: 0px; padding: 0px; border-left: 1px solid #aabfca; border-right: 1px solid #aabfca;">
                                        <asp:PlaceHolder ID="plcTemplateSelector" runat="server">
                                            <cms:PageTemplateSelector ID="templateSelector" runat="server" Mode="newpage" ShowEmptyCategories="false"
                                                IsLiveSite="false" IsNewPage="true" />
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder ID="plcInherited" runat="server">
                                            <div class="ItemSelector">
                                                <div class="InheritedTemplate">
                                                    <cms:LocalizedLabel ID="lblIngerited" runat="server" EnableViewState="false" />
                                                </div>
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder ID="plcLayout" runat="server">
                                            <asp:PlaceHolder ID="plcLayoutSelector" runat="server">
                                                <cms:LayoutFlatSelector ID="layoutSelector" runat="server" IsLiveSite="false" />
                                            </asp:PlaceHolder>
                                            <div class="CopyLayoutPanel">
                                                <cms:LocalizedCheckBox runat="server" ID="chkLayoutPageTemplate" Checked="true" ResourceString="NewPage.LayoutPageTemplate" />
                                            </div>
                                        </asp:PlaceHolder>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="Footer">
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </cms:CMSUpdatePanel>
    </div>
</asp:Content>
