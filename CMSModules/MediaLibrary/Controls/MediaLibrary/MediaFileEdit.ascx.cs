﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.MediaLibrary;
using CMS.CMSHelper;
using CMS.ExtendedControls;
using CMS.SiteProvider;
using CMS.FormControls;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_MediaLibrary_Controls_MediaLibrary_MediaFileEdit : CMSAdminControl
{
    #region "Event & delegates"

    /// <summary>
    /// Event fired after saved succeeded.
    /// </summary>
    public event OnActionEventHandler Action;

    #endregion


    #region "Private variables"

    private bool mHasCustomFields = false;

    private MediaFileInfo mFileInfo = null;
    private MediaLibraryInfo mLibraryInfo = null;
    private SiteInfo mLibrarySiteInfo = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Indicates whetherm the custom fields tab is displayed.
    /// </summary>
    private bool HasCustomFields
    {
        get
        {
            return this.mHasCustomFields;
        }
        set
        {
            this.mHasCustomFields = value;
        }
    }


    /// <summary>
    /// Indicates whether the current file has a preview.
    /// </summary>
    private bool HasPreview
    {
        get
        {
            return MediaLibraryHelper.HasPreview(this.LibrarySiteInfo.SiteName, this.MediaLibraryID, this.FileInfo.FilePath);
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Currently edited file info.
    /// </summary>
    public MediaFileInfo FileInfo
    {
        get
        {
            if ((this.mFileInfo == null) && (this.FileID > 0))
            {
                this.mFileInfo = MediaFileInfoProvider.GetMediaFileInfo(this.FileID);
            }
            return this.mFileInfo;
        }
        set
        {
            this.mFileInfo = value;
        }
    }


    /// <summary>
    /// File ID
    /// </summary>
    public int FileID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["FileID"], 0);
        }
        set
        {
            ViewState["FileID"] = value;
            this.FileInfo = null;
        }
    }


    /// <summary>
    /// Current file path
    /// </summary>
    public string FilePath
    {
        get
        {
            return ValidationHelper.GetString(ViewState["FilePath"], "");
        }
        set
        {
            ViewState["FilePath"] = value;
        }
    }


    /// <summary>
    /// Current folder path
    /// </summary>
    public string FolderPath
    {
        get
        {
            return ValidationHelper.GetString(ViewState["FolderPath"], "");
        }
        set
        {
            ViewState["FolderPath"] = value;
        }
    }


    /// <summary>
    /// Media library ID
    /// </summary>
    public int MediaLibraryID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["MediaLibraryID"], 0);
        }
        set
        {
            ViewState["MediaLibraryID"] = value;
        }
    }


    /// <summary>
    /// Gets library info object.
    /// </summary>
    public MediaLibraryInfo LibraryInfo
    {
        get
        {
            if ((this.mLibraryInfo == null) && (this.MediaLibraryID > 0))
            {
                this.LibraryInfo = MediaLibraryInfoProvider.GetMediaLibraryInfo(this.MediaLibraryID);
            }
            return this.mLibraryInfo;
        }
        set
        {
            this.mLibraryInfo = value;
        }
    }


    /// <summary>
    /// Info on the site related to the current library
    /// </summary>
    public SiteInfo LibrarySiteInfo
    {
        get
        {
            if (this.mLibrarySiteInfo == null)
            {
                this.mLibrarySiteInfo = SiteInfoProvider.GetSiteInfo(this.LibraryInfo.LibrarySiteID);
            }
            return this.mLibrarySiteInfo;
        }
        set
        {
            this.mLibrarySiteInfo = value;
        }
    }

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.formMediaFileCustomFields.StopProcessing = true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if ((this.FileInfo != null) && (this.LibraryInfo != null) && this.HasPreview)
        {
            this.plcPreview.Visible = true;

            string fileName = this.FileInfo.FileName + "." + this.FileInfo.FileExtension.TrimStart('.');
            string url = MediaFileInfoProvider.GetMediaFileUrl(this.FileInfo.FileGUID, fileName);
            url = UrlHelper.UpdateParameterInUrl(url, "preview", "1");
            this.lblPreviewPermaLink.Text = UrlHelper.ResolveUrl(url);

            if (MediaLibraryHelper.IsExternalLibrary(CMSContext.CurrentSiteName))
            {
                this.plcPrevDirPath.Visible = false;
            }
            else
            {
                this.plcPrevDirPath.Visible = true;
                this.lblPrevDirectLinkVal.Text = GetPrevDirectPath();
            }
        }
        else
        {
            this.lblNoPreview.Text = ResHelper.GetString("media.file.nothumb");

            this.plcNoPreview.Visible = true;
            this.plcPreview.Visible = false;
        }
        this.pnlUpdatePreviewDetails.Update();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.Visible = false;
        }
        else
        {
            ReloadControl(false);
        }

        this.fileUplPreview.StopProcessing = this.StopProcessing;
    }


    #region "Public methods"

    /// <summary>
    /// Reloads controls content.
    /// </summary>
    public void ReloadControl()
    {
        ReloadControl(true);
    }

    /// <summary>
    /// Reloads controls content.
    /// </summary>
    /// <param name="forceReload">Indicates whether the content should be reloaded as well.</param>
    public void ReloadControl(bool forceReload)
    {
        if (!this.StopProcessing)
        {
            this.Visible = true;
            SetupControls(forceReload);
        }
        else
        {
            this.Visible = false;
        }
    }


    /// <summary>
    /// Ensures required actions when the file was saved recently.
    /// </summary>
    /// <param name="file">Recently saved file info.</param>
    public void AfterSave()
    {
        SetupFile();
    }


    /// <summary>
    /// Set default values and clear textboxes.
    /// </summary>
    public void SetDefault()
    {
        this.txtEditDescription.Text = "";
        this.txtEditName.Text = "";
        this.txtEditTitle.Text = "";
    }


    /// <summary>
    /// Setup all labels and buttons text.
    /// </summary>
    public void SetupTexts()
    {
        // File form
        this.lblDirPath.Text = ResHelper.GetString("media.file.dirpath");
        this.lblPermaLink.Text = ResHelper.GetString("media.file.permalink");

        // Edit form
        this.lblEditTitle.Text = ResHelper.GetString("media.file.filetitle");
        this.lblEditDescription.Text = ResHelper.GetString("general.description") + ResHelper.Colon;
        this.btnEdit.Text = ResHelper.GetString("general.ok");
        this.rfvEditName.ErrorMessage = ResHelper.GetString("general.requiresvalue");
        this.rfvEditTitle.ErrorMessage = ResHelper.GetString("general.requiresvalue");

        this.lblCreatedBy.Text = ResHelper.GetString("media.file.createdby");
        this.lblCreatedWhen.Text = ResHelper.GetString("media.file.createdwhen");
        this.lblExtension.Text = ResHelper.GetString("media.file.extension");
        this.lblDimensions.Text = ResHelper.GetString("media.file.dimensions");
        this.lblModified.Text = ResHelper.GetString("media.file.modified");
        this.lblFileModified.Text = ResHelper.GetString("media.file.filemodified");
        this.lblSize.Text = ResHelper.GetString("media.file.size");
        this.lblFileSize.Text = ResHelper.GetString("media.file.filesize");
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes all the nested controls.
    /// </summary>
    /// <param name="forceReload">Indicates whether the content should be reloaded as well.</param>
    private void SetupControls(bool forceReload)
    {
        ShowProperTabs(forceReload);

        SetupTexts();
    }


    /// <summary>
    /// Setup general values.
    /// </summary>
    private void SetupFile()
    {
        // Get file and library info
        if ((this.FileInfo != null) && (this.LibraryInfo != null))
        {
            this.formMediaFileCustomFields.IsLiveSite = this.IsLiveSite;

            if (MediaLibraryHelper.IsExternalLibrary(CMSContext.CurrentSiteName))
            {
                this.plcDirPath.Visible = false;
            }
            else
            {
                this.lblDirPathValue.Text = TextHelper.EnsureMaximumLineLength(ResolveUrl(MediaFileInfoProvider.GetMediaFileUrl(this.LibrarySiteInfo.SiteName, this.LibraryInfo.LibraryFolder, this.FileInfo.FilePath)), 80);
            }
            this.lblPermaLinkValue.Text = TextHelper.EnsureMaximumLineLength(ResolveUrl(MediaFileInfoProvider.GetMediaFileUrl(this.FileInfo.FileGUID, this.FileInfo.FileName)), 80);
            if (ImageHelper.IsImage(this.FileInfo.FileExtension))
            {
                // Ensure max side size 200
                int[] maxsize = ImageHelper.EnsureImageDimensions(0, 0, 200, this.FileInfo.FileImageWidth, this.FileInfo.FileImageHeight);
                this.imagePreview.Width = maxsize[0];
                this.imagePreview.Height = maxsize[1];

                // If is Image show image properties
                this.imagePreview.URL = UrlHelper.AddParameterToUrl(MediaFileInfoProvider.GetMediaFileUrl(this.FileInfo.FileGUID, CMSContext.CurrentSiteName), "maxsidesize", "200");
                this.imagePreview.URL = UrlHelper.AddParameterToUrl(this.imagePreview.URL, "chset", Guid.NewGuid().ToString());
                this.plcImagePreview.Visible = true;

                this.pnlPrew.Visible = true;
            }
            else if (CMS.GlobalHelper.MediaHelper.IsFlash(this.FileInfo.FileExtension) || CMS.GlobalHelper.MediaHelper.IsAudio(this.FileInfo.FileExtension) ||
                CMS.GlobalHelper.MediaHelper.IsVideo(this.FileInfo.FileExtension))
            {
                if (CMS.GlobalHelper.MediaHelper.IsAudio(this.FileInfo.FileExtension))
                {
                    this.mediaPreview.Height = 45;
                }
                else
                {
                    this.mediaPreview.Height = 180;
                }
                this.mediaPreview.Width = 270;

                this.mediaPreview.AutoPlay = false;
                this.mediaPreview.AVControls = true;
                this.mediaPreview.Loop = false;
                this.mediaPreview.Menu = true;
                this.mediaPreview.Type = this.FileInfo.FileExtension;

                // If is Image show image properties
                this.mediaPreview.Url = MediaFileInfoProvider.GetMediaFileUrl(this.FileInfo.FileGUID, this.FileInfo.FileName);
                this.plcMediaPreview.Visible = true;

                this.pnlPrew.Visible = true;
            }
            else
            {
                this.pnlPrew.Visible = false;
            }
        }
        else
        {
            this.pnlPrew.Visible = false;
        }
    }


    /// <summary>
    /// Setup preview values.
    /// </summary>
    private void SetupPreview()
    {
        if ((this.FileInfo != null) && (this.LibraryInfo != null))
        {
            this.fileUplPreview.EnableUpdate = MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "filemodify");
            this.fileUplPreview.StopProcessing = false;
            this.fileUplPreview.IsLiveSite = this.IsLiveSite;
            this.fileUplPreview.LibraryFolderPath = this.FolderPath;
            this.fileUplPreview.LibraryID = this.LibraryInfo.LibraryID;
            this.fileUplPreview.MediaFileID = this.FileID;
            this.fileUplPreview.FileInfo = this.FileInfo;
            this.fileUplPreview.ReloadData();
        }
        else
        {
            this.plcPreview.Visible = false;
        }
    }


    /// <summary>
    /// Setup edit values.
    /// </summary>
    private void SetupEdit()
    {
        if (this.FileInfo != null)
        {
            // Fill edit form
            this.txtEditName.Text = this.FileInfo.FileName;
            this.txtEditDescription.Text = this.FileInfo.FileDescription;
            this.txtEditTitle.Text = this.FileInfo.FileTitle;

            // Created by
            string userName = null;
            UserInfo ui = UserInfoProvider.GetFullUserInfo(this.FileInfo.FileCreatedByUserID);
            if (ui != null)
            {
                if (ui.IsPublic())
                {
                    userName = ResHelper.GetString("general.na");
                }
                else
                {
                    userName = ui.FullName;
                }
            }
            else
            {
                userName = ResHelper.GetString("general.na");
            }
            this.lblCreatedByVal.Text = userName;

            // Created when
            DateTime dtCreated = ValidationHelper.GetDateTime(this.FileInfo.FileCreatedWhen, DateTimeHelper.ZERO_TIME);
            this.lblCreatedWhenVal.Text = CMSContext.ConvertDateTime(dtCreated, this).ToString();

            // Modified when
            DateTime dtModified = ValidationHelper.GetDateTime(this.FileInfo.FileModifiedWhen, DateTimeHelper.ZERO_TIME);
            this.lblModifiedVal.Text = CMSContext.ConvertDateTime(dtModified, this).ToString(); ;

            // Get system file info
            string filePath = MediaFileInfoProvider.GetMediaFilePath(FileInfo.FileLibraryID, FileInfo.FilePath);
            if (File.Exists(filePath))
            {
                System.IO.FileInfo sysFileInfo = new FileInfo(filePath);

                // File modified when
                DateTime dtFileModified = ValidationHelper.GetDateTime(sysFileInfo.LastWriteTime, DateTimeHelper.ZERO_TIME);
                // Display only if system time is 
                if ((dtFileModified - dtModified).TotalSeconds > 5)
                {
                    this.lblFileModifiedVal.Text = CMSContext.ConvertDateTime(dtFileModified, this).ToString();
                    
                    this.plcFileModified.Visible = true;
                    this.plcRefresh.Visible = true;
                }
                else
                {
                    this.plcFileModified.Visible = false;
                    this.plcRefresh.Visible = false;
                }

                // File size
                if (sysFileInfo.Length != FileInfo.FileSize)
                {
                    this.lblFileSizeVal.Text = DataHelper.GetSizeString(sysFileInfo.Length);
                    this.plcFileSize.Visible = true;
                    this.plcRefresh.Visible = true;
                }
                else
                {
                    this.plcFileSize.Visible = false;
                    this.plcRefresh.Visible = false;
                }
            }

            // Size
            this.lblSizeVal.Text = DataHelper.GetSizeString(FileInfo.FileSize);

            // Extension
            this.lblExtensionVal.Text = FileInfo.FileExtension.TrimStart('.').ToLower();

            // Dimensions
            if (ImageHelper.IsImage(FileInfo.FileExtension))
            {
                this.lblDimensionsVal.Text = FileInfo.FileImageWidth + " x " + FileInfo.FileImageHeight;
                this.plcDimensions.Visible = true;
            }
            else
            {
                this.plcDimensions.Visible = false;
            }
        }
        else
        {
            this.txtEditName.Text = "";
            this.txtEditDescription.Text = "";
            this.txtEditTitle.Text = "";
        }
    }


    /// <summary>
    /// Ensures that specified script is passed to the parent control.
    /// </summary>
    /// <param name="script">Script to pass.</param>
    private void EnsureParentScript(string script)
    {
        this.RaiseOnAction("setscript", script);
    }


    /// <summary>
    /// Raises action event.
    /// </summary>
    /// <param name="actionName">Name of tha action occuring.</param>
    /// <param name="actionArgument">Argument related to the action.</param>
    private void RaiseOnAction(string actionName, object actionArgument)
    {
        if (this.Action != null)
        {
            this.Action(actionName, actionArgument);
        }
    }


    /// <summary>
    /// Display or hides the tabs according to the ViewMode setting.
    /// </summary>
    /// <param name="forceReload">Indicates whether the content should be reloaded as well.</param> 
    private void ShowProperTabs(bool forceReload)
    {
        // We need to remove the header text for unused tabs, because of bug
        // in AjaxToolkit Tab control (when hiding the tab text is still visible)
        this.tabGeneral.HeaderText = ResHelper.GetString("general.file");
        this.tabPreview.HeaderText = ResHelper.GetString("general.thumbnail");
        this.tabEdit.HeaderText = ResHelper.GetString("general.edit");
        this.tabCustomFields.HeaderText = ResHelper.GetString("general.customfields");

        DisplayCustomFields(forceReload);

        if (forceReload)
        {
            SetupEdit();
        }
        SetupFile();
        SetupPreview();

        if (!UrlHelper.IsPostback())
        {
            this.pnlTabs.ActiveTabIndex = 0;
        }
    }


    /// <summary>
    /// Handles custom fields tab displaying.
    /// </summary>
    private void DisplayCustomFields()
    {
        DisplayCustomFields(false);
    }


    /// <summary>
    /// Handles custom fields tab displaying.
    /// </summary>
    /// <param name="forceReload">Indicates whether the content should be reloaded as well.</param> 
    private void DisplayCustomFields(bool forceReload)
    {
        // Initialize DataForm
        if ((this.FileID > 0) && this.Visible)
        {
            this.formMediaFileCustomFields.OnBeforeSave += new DataForm.OnBeforeSaveEventHandler(formMediaFileCustomFields_OnBeforeSave);
            this.formMediaFileCustomFields.OnAfterSave += new DataForm.OnAfterSaveEventHandler(formMediaFileCustomFields_OnAfterSave);
            this.formMediaFileCustomFields.OnValidationFailed += new DataForm.OnValidationFailedEventHandler(formMediaFileCustomFields_OnValidationFailed);

            this.formMediaFileCustomFields.IsLiveSite = this.IsLiveSite;
            this.formMediaFileCustomFields.StopProcessing = false;
            this.formMediaFileCustomFields.Info = this.FileInfo;
            this.formMediaFileCustomFields.ID = "formMediaFileCustomFields" + this.FileID;

            this.formMediaFileCustomFields.BasicForm.HideSystemFields = true;
            this.formMediaFileCustomFields.BasicForm.SubmitButton.CssClass = "SubmitButton";

            if (forceReload)
            {
                if (this.formMediaFileCustomFields.BasicForm == null)
                {
                    this.formMediaFileCustomFields.ReloadData();
                }
                this.formMediaFileCustomFields.BasicForm.ReloadData();
            }

            // Initialize customn fields tab if visible
            this.HasCustomFields = (this.formMediaFileCustomFields.BasicForm.FormInformation.GetFormElements(true, false, true).Count > 0);
            if (this.HasCustomFields)
            {
                if ((this.formMediaFileCustomFields.BasicForm != null) && this.formMediaFileCustomFields.BasicForm.SubmitButton.Visible)
                {
                    // Register the postback control
                    ScriptManager manager = ScriptManager.GetCurrent(this.Page);
                    if (manager != null)
                    {
                        manager.RegisterPostBackControl(this.formMediaFileCustomFields.BasicForm.SubmitButton);
                    }
                }

                this.tabCustomFields.Visible = true;
                this.plcMediaFileCustomFields.Visible = true;
            }
            else
            {
                this.formMediaFileCustomFields.StopProcessing = true;
                this.formMediaFileCustomFields.Enabled = false;
                this.formMediaFileCustomFields.Visible = false;
                this.tabCustomFields.Visible = false;
                this.tabCustomFields.HeaderText = "";
                this.plcMediaFileCustomFields.Visible = false;
            }
        }

        this.pnlUpdateCustomFields.Update();
    }


    /// <summary>
    /// Gets direct path for preview image of currently edited media file.
    /// </summary>
    private string GetPrevDirectPath()
    {
        string prevUrl = "";

        // Direct path
        string previewPath = null;
        string previewFolder = null;

        if (Path.GetDirectoryName(this.FileInfo.FilePath).EndsWith(MediaLibraryHelper.GetMediaFileHiddenFolder(CMSContext.CurrentSiteName)))
        {
            previewFolder = Path.GetDirectoryName(this.FileInfo.FilePath) + "\\" + MediaLibraryHelper.GetPreviewFileName(FileInfo.FileName, FileInfo.FileExtension, ".*", CMSContext.CurrentSiteName);
            previewPath = MediaLibraryInfoProvider.GetMediaLibraryFolderPath(FileInfo.FileLibraryID) + "\\" + previewFolder;
        }
        else
        {
            previewFolder = Path.GetDirectoryName(this.FileInfo.FilePath) + "\\" + MediaLibraryHelper.GetMediaFileHiddenFolder(CMSContext.CurrentSiteName) + "\\" + MediaLibraryHelper.GetPreviewFileName(FileInfo.FileName, FileInfo.FileExtension, ".*", CMSContext.CurrentSiteName);
            previewPath = MediaLibraryInfoProvider.GetMediaLibraryFolderPath(FileInfo.FileLibraryID) + "\\" + previewFolder;
        }
        if (Directory.Exists(Path.GetDirectoryName(previewPath)))
        {
            string[] files = Directory.GetFiles(Path.GetDirectoryName(previewPath), Path.GetFileName(previewPath));
            if (files.Length > 0)
            {
                previewFolder = Path.GetDirectoryName(previewFolder).Replace('\\', '/').TrimStart('/');
                string prevFileName = Path.GetFileName(files[0]);

                prevUrl = MediaFileInfoProvider.GetMediaFileUrl(CMSContext.CurrentSiteName, this.LibraryInfo.LibraryFolder, previewFolder + '/' + prevFileName);
                prevUrl = UrlHelper.ResolveUrl(prevUrl);
            }
        }

        return prevUrl;
    }

    #endregion


    #region "Edit tab"

    /// <summary>
    /// Edit file event handler.
    /// </summary>
    protected void btnEdit_Click(object sender, EventArgs e)
    {
        // Check 'File modify' permission
        if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "filemodify"))
        {
            this.lblErrorEdit.Text = MediaLibraryHelper.GetAccessDeniedMessage("filemodify");
            this.lblErrorEdit.Visible = true;

            SetupTexts();
            SetupEdit();

            // Update form
            pnlUpdateEditInfo.Update();
            pnlUpdateFileInfo.Update();
            return;
        }

        FileInfo fi = new FileInfo(MediaFileInfoProvider.GetMediaFilePath(CMSContext.CurrentSiteName, this.LibraryInfo.LibraryFolder, this.FilePath));
        if ((fi != null) && (this.LibraryInfo != null))
        {
            string path = MediaLibraryHelper.EnsurePath(this.FilePath);
            string fileName = UrlHelper.GetSafeFileName(this.txtEditName.Text.Trim(), CMSContext.CurrentSiteName);
            string origFileName = Path.GetFileNameWithoutExtension(fi.FullName);

            if (this.FileInfo != null)
            {
                if ((CMSContext.CurrentUser != null) && (!CMSContext.CurrentUser.IsPublic()))
                {
                    this.FileInfo.FileModifiedWhen = CMSContext.CurrentUser.DateTimeNow;
                    this.FileInfo.FileModifiedByUserID = CMSContext.CurrentUser.UserID;
                }
                else
                {
                    this.FileInfo.FileModifiedWhen = DateTime.Now;
                }
                // Check if filename is changed ad move file if necessary
                if (fileName != origFileName)
                {
                    try
                    {
                        // Check if file with new file name exists
                        string newFilePath = Path.GetDirectoryName(fi.FullName) + "\\" + fileName + fi.Extension;
                        if (!File.Exists(newFilePath))
                        {
                            string newPath = (string.IsNullOrEmpty(Path.GetDirectoryName(path)) ? "" : Path.GetDirectoryName(path) + "/") + fileName + this.FileInfo.FileExtension;
                            MediaFileInfoProvider.MoveMediaFile(CMSContext.CurrentSiteName, this.FileInfo.FileLibraryID, path, newPath, false);
                            this.FileInfo.FilePath = CMS.MediaLibrary.MediaLibraryHelper.EnsurePath(newPath);
                        }
                        else
                        {
                            this.lblErrorEdit.Text = ResHelper.GetString("general.fileexists");
                            this.lblErrorEdit.Visible = true;
                            this.pnlUpdateEditInfo.Update();
                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        this.lblErrorEdit.Text = ResHelper.GetString("media.rename.failed") + ": " + ex.Message;
                        this.lblErrorEdit.Visible = true;
                        this.pnlUpdateEditInfo.Update();
                        return;
                    }
                }
                // Set media file info
                this.FileInfo.FileName = fileName;
                this.FileInfo.FileTitle = this.txtEditTitle.Text.Trim();
                this.FileInfo.FileDescription = this.txtEditDescription.Text.Trim();

                // Save
                MediaFileInfoProvider.SetMediaFileInfo(this.FileInfo);
                this.FilePath = this.FileInfo.FilePath;

                // Inform user on success
                this.lblInfoEdit.Text = ResHelper.GetString("general.changessaved");
                this.lblInfoEdit.Visible = true;
                this.pnlUpdateEditInfo.Update();

                SetupEdit();
                this.pnlUpdateFileInfo.Update();

                SetupTexts();
                SetupFile();
                this.pnlUpdateGeneral.Update();

                SetupPreview();
                this.pnlUpdatePreviewDetails.Update();

                RaiseOnAction("rehighlightitem", Path.GetFileName(this.FileInfo.FilePath));
            }
        }
    }


    /// <summary>
    /// Edit file event handler.
    /// </summary>
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        // Check 'File modify' permission
        if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "filemodify"))
        {
            this.lblErrorEdit.Text = MediaLibraryHelper.GetAccessDeniedMessage("filemodify");
            this.lblErrorEdit.Visible = true;

            SetupFile();
            return;
        }

        FileInfo fi = new FileInfo(MediaFileInfoProvider.GetMediaFilePath(CMSContext.CurrentSiteName, this.LibraryInfo.LibraryFolder, this.FilePath));
        if ((fi != null) && (this.LibraryInfo != null))
        {
            if (this.FileInfo != null)
            {

                this.FileInfo.FileModifiedWhen = DateTime.Now;
                // Set media file info
                this.FileInfo.FileSize = fi.Length;
                if (ImageHelper.IsImage(FileInfo.FileExtension))
                {
                    ImageHelper ih = new ImageHelper();
                    ih.LoadImage(File.ReadAllBytes(fi.FullName));
                    this.FileInfo.FileImageWidth = ih.ImageWidth;
                    this.FileInfo.FileImageHeight = ih.ImageHeight;
                }
                this.FileInfo.FileTitle = this.txtEditTitle.Text.Trim();
                this.FileInfo.FileDescription = this.txtEditDescription.Text.Trim();

                // Save
                MediaFileInfoProvider.SetMediaFileInfo(this.FileInfo);

                // Inform user on success
                this.lblInfoEdit.Text = ResHelper.GetString("media.refresh.success");
                this.lblInfoEdit.Visible = true;

                this.pnlUpdateEditInfo.Update();

                SetupTexts();

                SetupFile();
                this.pnlUpdateGeneral.Update();

                SetupPreview();
                this.pnlUpdatePreviewDetails.Update();

                SetupEdit();
                this.pnlUpdateFileInfo.Update();

                RaiseOnAction("rehighlightitem", Path.GetFileName(this.FileInfo.FilePath));
            }
        }
    }


    /// <summary>
    /// BreadCrumbs in edit file form.
    /// </summary>
    protected void lnkEditList_Click(object sender, EventArgs e)
    {
        // Hide preview/edit form and show unigrid
        RaiseOnAction("showlist", null);
    }


    /// <summary>
    /// Stores new media file info into the DB.
    /// </summary>
    /// <param name="fi">Info on file to be stored</param>
    /// <param name="description">Description of new media file</param>
    /// <param name="name">Name of new media file</param>
    public MediaFileInfo SaveNewFile(FileInfo fi, string title, string description, string name, string filePath)
    {
        string path = MediaLibraryHelper.EnsurePath(filePath);
        string fileName = name;
        string origFileName = Path.GetFileNameWithoutExtension(fi.Name);

        string fullPath = fi.FullName;
        string extension = UrlHelper.GetSafeFileName(fi.Extension, CMSContext.CurrentSiteName);
        int fileSize = ValidationHelper.GetInteger(fi.Length, 0);

        // Check if filename is changed ad move file if necessary
        if (fileName + extension != fi.Name)
        {
            string oldPath = path;
            fullPath = MediaLibraryHelper.EnsureUniqueFileName(Path.GetDirectoryName(fullPath) + "\\" + fileName + extension);
            path = MediaLibraryHelper.EnsurePath(Path.GetDirectoryName(path) + "/" + Path.GetFileName(fullPath)).TrimStart('/');
            MediaFileInfoProvider.MoveMediaFile(CMSContext.CurrentSiteName, MediaLibraryID, oldPath, path, true);
            fileName = Path.GetFileNameWithoutExtension(fullPath);
        }

        // Create media file info
        MediaFileInfo fileInfo = new MediaFileInfo(fullPath, this.LibraryInfo.LibraryID, MediaLibraryHelper.EnsurePath(Path.GetDirectoryName(path)));

        fileInfo.FileTitle = title;
        fileInfo.FileDescription = description;

        // Save media file info
        MediaFileInfoProvider.ImportMediaFileInfo(fileInfo);

        // Save FileID in ViewState
        this.FileID = fileInfo.FileID;
        this.FilePath = fileInfo.FilePath;

        return fileInfo;
    }

    #endregion


    #region "Event handlers"

    private void formMediaFileCustomFields_OnValidationFailed()
    {
        this.pnlUpdateCustomFields.Update();
    }


    private void formMediaFileCustomFields_OnAfterSave()
    {
        this.lblInfo.Text = ResHelper.GetString("general.changessaved");
        this.lblInfo.Visible = true;

        this.pnlUpdateCustomFields.Update();

        SetupEdit();
        this.pnlUpdateEditInfo.Update();
    }


    private void formMediaFileCustomFields_OnBeforeSave()
    {
        // Check 'File modify' permission
        if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "filemodify"))
        {
            this.lblErrorCustom.Text = MediaLibraryHelper.GetAccessDeniedMessage("filemodify");
            this.lblErrorCustom.Visible = true;

            DisplayCustomFields(true);
            this.formMediaFileCustomFields.StopProcessing = true;

            // Update form
            SetupEdit();
        }
    }

    #endregion
}
