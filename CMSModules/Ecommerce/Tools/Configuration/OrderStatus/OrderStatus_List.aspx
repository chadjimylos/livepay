<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrderStatus_List.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_OrderStatus_OrderStatus_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="E-commerce Configuration - Order status" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="gridElem" GridName="OrderStatus_List.xml" OrderBy="StatusOrder"
        IsLiveSite="false" Columns="StatusID,StatusDisplayName,StatusEnabled,StatusColor,StatusSendNotification" />
</asp:Content>
