using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_ShippingOptions_ShippingOption_List : CMSEcommerceConfigurationPage
{
    int siteId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("COM_ShippingOption_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_ShippingOption/object.png");
        this.CurrentMaster.Title.HelpTopicName = "shipping_options_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        siteId = CMSContext.CurrentSiteID;

        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);
        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
        UniGrid.WhereCondition = "ShippingOptionSiteID = " + siteId;

        // Prepare the new shipping option header element
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("COM_ShippingOption_List.NewItemCaption");
        actions[0, 3] = "~/CMSModules/Ecommerce/Tools/Configuration/ShippingOptions/ShippingOption_New.aspx";
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_ShippingOption/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("ShippingOption_Edit_Frameset.aspx?ShippingOptionID=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // check 'ConfigurationModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
            }

            // Check dependencies
            if (ShippingOptionInfoProvider.CheckDependencies(ValidationHelper.GetInteger(actionArgument, 0)))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                return;
            }
            // delete ShippingOptionInfo object from database
            ShippingOptionInfoProvider.DeleteShippingOptionInfo(ValidationHelper.GetInteger(actionArgument, 0));
        }
    }


    object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "shippoptenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }

        return parameter;
    }
}
