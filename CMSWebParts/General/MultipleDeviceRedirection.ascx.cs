using System.Web;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.PortalEngine;

public partial class CMSWebParts_General_MultipleDeviceRedirection: CMSAbstractWebPart
{
    #region Webpart properties

    /// <summary>
    /// URL to which should be mobile device redirected
    /// </summary>
    public string RedirectionURL
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("RedirectionURL"), "");
        }
        set
        {
            this.SetValue("RedirectionURL", value);
        }
    }

    #endregion


    #region Webpart methods

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {

        if (!this.StopProcessing)
        {
            // retrieve the data from the custom table
            string customTableClassName = "customtable.mobile";
            // Get data class using custom table name
            CMS.SettingsProvider.DataClassInfo customTableClassInfo =
            CMS.SettingsProvider.DataClassInfoProvider.GetDataClass(customTableClassName);
            if (customTableClassInfo == null)
            {
                throw new System.Exception("Unable to locate the mobile table");
            }
            // Initialize custom table item provider with current user info and general connection
            CMS.SiteProvider.CustomTableItemProvider ctiProvider = new
            CMS.SiteProvider.CustomTableItemProvider(CMSContext.CurrentUser,
            CMS.DataEngine.ConnectionHelper.GetConnection());
            // Get custom table items
            System.Data.DataSet dsItems =
            ctiProvider.GetItems(customTableClassInfo.ClassName, null, null);
            // Check if DataSet is not empty
            if (!DataHelper.DataSourceIsEmpty(dsItems))
            {
                // Handle the retrieved data
                // Loop until we find the request value we are searching for otherwise exit
                foreach (System.Data.DataTable table in dsItems.Tables)
                {
                    foreach (System.Data.DataRow row in table.Rows)
                    {
                        string requestValue = row["PhoneIdentifier"].ToString();
                        if (Request.UserAgent.ToLower().Contains(requestValue.ToLower()))
                        {
                            string RedirectionURL = row["RedirectionPath"].ToString();
                            //Trim blank characters
                            string rawURL = RedirectionURL.Trim();
                            // Check if some address is specified
                            if ((rawURL.Length > 0) && (CMSContext.ViewMode ==
                            ViewModeEnum.LiveSite))
                            {
                                string newURL = UrlHelper.ResolveUrl(rawURL);
                                // If current URL is same as set, no redirection is done
                                if ((UrlHelper.CurrentURL != newURL) &&
                                (UrlHelper.GetAbsoluteUrl(UrlHelper.CurrentURL) != newURL))
                                {
                                    UrlHelper.ResponseRedirect(newURL);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    #endregion
}
