﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.SettingsProvider
Imports CMS.SiteProvider
Imports CMS.DataEngine

Partial Class CMSTemplates_LivePay_UCNewPay
    Inherits CMSUserControl

    Protected ReadOnly Property NodeID() As Integer
        Get
            Dim iNodeID As Integer = 0
            If Not String.IsNullOrEmpty(Request("NodeID")) Then
                iNodeID = Request("NodeID")
            End If
            Return iNodeID
        End Get
    End Property

    Public searchText As String

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        btnSearch.ImageUrl = ResHelper.LocalizeString("{$=/app_themes/LivePay/btnSearchOk.gif|en-us=/app_themes/LivePay/btnSearchOk_en-us.png$}")
        'btnSearch_big.ImageUrl = ResHelper.LocalizeString("{$=/app_themes/LivePay/btnbigsearch.png|en-us=/app_themes/LivePay/btnbigsearch.png$}")
        imgSearch_big.Src = ResHelper.LocalizeString("{$=/app_themes/LivePay/btnbigsearch.png|en-us=/app_themes/LivePay/btnbigsearch.png$}")
        txt_Nomos.Value = ResHelper.LocalizeString("{$=Επιλέξτε Νομό|en-us=Enter District$}")
        txt_Area.Value = ResHelper.LocalizeString("{$=Επιλέξτε Περιοχή|en-us=Enter Area$}")

        NomosText.Value = ResHelper.LocalizeString("{$=Επιλέξτε Νομό|en-us=Enter District$}")
        AreaText.Value = ResHelper.LocalizeString("{$=Επιλέξτε Περιοχή|en-us=Enter Area$}")

     
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        searchText = ResHelper.LocalizeString("{$=Πληκτρολογήστε Επωνυμία Επιχείρησης|en-us=Enter the Company Name$}")

      


        Me.txtSearch.Text = searchText
        Me.txtSearch.Attributes.Add("onfocus", "if(this.value=='" & searchText & "'){this.value='';}")
        Me.txtSearch.Attributes.Add("onBlur", "if (this.value == '') {this.value = '" & searchText & "';}")

        Me.txtSearch_big.Text = searchText
        Me.txtSearch_big.Attributes.Add("onfocus", "if(this.value=='" & searchText & "'){this.value='';}")
        Me.txtSearch_big.Attributes.Add("onBlur", "if (this.value == '') {this.value = '" & searchText & "';}")


        SimpleSearch.Style.Add("display", "")
        Dim ds_Nodes As New DataSet
        Dim customTableClassName As String = "Livepay.Settings"
        Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass(customTableClassName)
        If customTableClassInfo Is Nothing Then

        End If
        Dim ctiProvider As CustomTableItemProvider = New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())
        Dim dsItems As DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, String.Empty, String.Empty)
        If (DataHelper.DataSourceIsEmpty(dsItems) = False) Then
            For i As Integer = 0 To dsItems.Tables(0).Rows.Count - 1
                If dsItems.Tables(0).Rows(i)("code").ToString = "Geographic" Then
                    Dim AllNodes As Array = dsItems.Tables(0).Rows(i)("value").ToString.Split(",")
                    For iNode As Integer = 0 To AllNodes.Length - 1
                        If CMSContext.CurrentDocument.NodeID = AllNodes(iNode) Then
                            BigSearch.Style.Add("display", "")
                            SimpleSearch.Style.Add("display", "none")
                        End If
                    Next
                End If
            Next
        End If

        If Me.NodeID > 0 Then
            BigSearch.Style.Add("display", "")
            SimpleSearch.Style.Add("display", "none")
        End If
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString(), Script, True)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim key As String = Replace(txtSearch.Text, "'", String.Empty)
        key = Replace(key, """", String.Empty)
        If key <> String.Empty Then
            Response.Redirect(String.Concat("~/Search.aspx?searchtext=", key, ""))
        End If
    End Sub

    Protected Sub btnSearch_big_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch_big.Click
        If txtSearch_big.Text = ResHelper.LocalizeString("{$=Πληκτρολογήστε Επωνυμία Επιχείρησης|en-us=Enter the Company Name$}") Then
            txtSearch_big.Text = String.Empty
        End If

        Dim Nomos As String = ResHelper.LocalizeString("{$=Επιλέξτε Νομό|en-us=Enter District$}")
        Dim Area As String = ResHelper.LocalizeString("{$=Επιλέξτε Περιοχή|en-us=Enter Area$}")

        If txt_Area.Value = Area Then
            AreaVal.Value = String.Empty
            AreaText.Value = String.Empty
        End If

        If txt_Nomos.Value = Nomos Then
            NomosVal.Value = String.Empty
            NomosText.Value = String.Empty
        End If
        Dim key As String = Replace(StripHTMLFunctions.StripTags(txtSearch_big.Text), "'", String.Empty)
        key = Replace(key, """", String.Empty)
       
        Response.Redirect(String.Concat("~/Search.aspx?searchtext=", key, "&nodeid=" & IIf(String.IsNullOrEmpty(Request("nodeid")), CMSContext.CurrentDocument.NodeID, Request("nodeid"))) & "&area=" & AreaVal.Value & "&nomos=" & NomosVal.Value & "&areatxt=" & AreaText.Value & "&nomostxt=" & NomosText.Value)

    End Sub

End Class
