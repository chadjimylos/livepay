CREATE PROCEDURE [Proc_COM_TaxClasses_RemoveDependences]
    @TaxClassID int
AS
BEGIN
    DELETE FROM COM_TaxClassCountry WHERE [TaxClassID] = @TaxClassID;
    DELETE FROM COM_TaxClassState   WHERE [TaxClassID] = @TaxClassID;
    DELETE FROM [COM_SKUTaxClasses] WHERE [TaxClassID] = @TaxClassID;
    UPDATE [COM_Department] SET [DepartmentDefaultTaxClassID] = NULL WHERE [DepartmentDefaultTaxClassID] = @TaxClassID;
    DELETE FROM COM_DepartmentTaxClass WHERE TaxClassID = @TaxClassID;
END
