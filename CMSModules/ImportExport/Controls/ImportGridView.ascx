<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImportGridView.ascx.cs"
    Inherits="CMSModules_ImportExport_Controls_ImportGridView" %>

<script type="text/javascript">
//<![CDATA[
    // Check specified checkboxes
    function igvSelectCheckboxes(checkbox,classname)
    {
        var checkboxes = document.getElementsByTagName("input");
        for(var i = 0;i<checkboxes.length;i++)
        {
            if(checkboxes[i].className == classname)
            {
                if(checkbox.checked)
                {
                    checkboxes[i].checked=true;
                }
                else
                {
                    checkboxes[i].checked=false;
                }
            }
                
        }
        return false;
    }

    // Change checkboxes states
    function igvChangeCheckboxes(className, state, siteObject, clientId)
    {
        function igvModify(state)
        {
            var checkboxes = document.getElementsByTagName("input");
            for(var i = 0;i<checkboxes.length;i++)
            {
                if(checkboxes[i].className == className)
                {
                    checkboxes[i].checked = state;
                }
            }
        }
        
        // Selct all / Unselect all
        if(state!=-1)
        {
            var checked = (state==1);
            igvModify(checked);
        }
        // Select default
        else
        {
            igvModify(false);
            if((importDefaultObjectsValues!=null) && (importDefaultSiteObjectsValues!=null))
            {
                if(siteObject)
                {
                    var codeNames = importDefaultSiteObjectsValues[className];
                    if(codeNames!=null)
                    {
                        for(var i = 0;i<codeNames.length;i++)
                        {
                            var element = document.getElementById(codeNames[i]);
                            if(element!=null)
                            {
                                element.checked = true;
                            }
                        }
                    }
                }
                else
                {
                    var codeNames = importDefaultObjectsValues[className];
                    if(codeNames!=null)
                    {
                        for(var i = 0;i<codeNames.length;i++)
                        {
                            var element = document.getElementById(codeNames[i]);
                            if(element!=null)
                            {
                                element.checked = true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
//]]>
</script>

<asp:PlaceHolder ID="plcGrid" runat="server">
    <asp:Panel ID="pnlGrid" runat="server">
        <asp:PlaceHolder runat="server" ID="plcObjects">
            <asp:Label ID="lblInfo" runat="server" EnableViewState="false" /><br />
            <br />
            <asp:Panel ID="pnlLinks" runat="server">
                &nbsp;
                <asp:LinkButton ID="btnAll" runat="Server" />&nbsp;
                <asp:LinkButton ID="btnNone" runat="Server" />&nbsp;
                <asp:LinkButton ID="btnDefault" runat="Server" /><br />
                <br />
            </asp:Panel>
            <asp:Label ID="lblCategoryCaption" runat="Server" />
            <asp:GridView ID="gvObjects" runat="server" AutoGenerateColumns="False" GridLines="Horizontal"
                CssClass="UniGridGrid" CellPadding="1" Width="100%">
                <HeaderStyle CssClass="UniGridHead" Wrap="False" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle Width="50" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" Wrap="false" />
                        <ItemTemplate>
                            <div style="padding-right: 15px;">
                                <%# IsInConflict(Eval(codeNameColumnName)) %>
                                <input type="checkbox" class='<%# ValidationHelper.GetIdentificator(ObjectType) %>'
                                    id='<%# preposition + ValidationHelper.GetIdentificator(Eval(codeNameColumnName)).ToLower().Replace("'","\'") %>'
                                    name='<%# preposition + HttpUtility.HtmlEncode(ValidationHelper.GetString(Eval(codeNameColumnName), "")) %>'
                                    <%# IsChecked(Eval(codeNameColumnName)) %> />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle Wrap="false" />
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" ToolTip='<%# HttpUtility.HtmlEncode(ValidationHelper.GetString(Eval(codeNameColumnName), "")) %>' Text='<%# HttpUtility.HtmlEncode(GetName(Eval(codeNameColumnName), Eval(displayNameColumnName))) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="OddRow" />
                <AlternatingRowStyle CssClass="EvenRow" />
            </asp:GridView>
        </asp:PlaceHolder>
        <asp:Label ID="lblNoData" runat="Server" /><br />
        <asp:PlaceHolder runat="server" ID="plcTasks">
            <br />
            <asp:Label ID="lblTasks" runat="Server" Font-Bold="true" />
            <br />
            <br />
            <asp:Panel ID="pnlTaskLinks" runat="server">
                &nbsp;
                <asp:LinkButton ID="btnAllTasks" runat="Server" />&nbsp;
                <asp:LinkButton ID="btnNoneTasks" runat="Server" /><br />
                <br />
            </asp:Panel>
            <asp:GridView ID="gvTasks" runat="server" AutoGenerateColumns="False" GridLines="Horizontal"
                CssClass="UniGridGrid" CellPadding="1" Width="100%">
                <HeaderStyle CssClass="UniGridHead" Wrap="False" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle Width="50" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="right" />
                        <ItemTemplate>
                            <div style="padding-right: 15px;">
                                <input type="checkbox" class='export_task' id='<%# taskPreposition + Eval("TaskID") %>'
                                    name='<%# taskPreposition + Eval("TaskID") %>' <%# IsTaskChecked(Eval("TaskID")) %> />
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TaskTitle">
                        <ItemStyle Wrap="false" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TaskType">
                        <ItemStyle Wrap="false" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TaskTime">
                        <ItemStyle Wrap="false" />
                    </asp:BoundField>
                </Columns>
                <RowStyle CssClass="OddRow" />
                <AlternatingRowStyle CssClass="EvenRow" />
            </asp:GridView>
        </asp:PlaceHolder>
    </asp:Panel>
    <br />
    <br />
</asp:PlaceHolder>
