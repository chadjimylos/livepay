﻿// Clears the FCKEditor toolbar
function ClearFCKToolbar()
{
    if ((parent!=null) && (parent.ClearToolbar)) 
    { 
        parent.ClearToolbar(); 
    } 
}

// Selects the node within the tree
function SelectNode(nodeId)
{
    parent.SelectNode(nodeId);
}

function RefreshTree(expandNodeId, selectNodeId) {
    // Update tree
    parent.RefreshTree(expandNodeId, selectNodeId);
}

function RefreshNode(expandNodeId, selectNodeId) {
    RefreshTree(expandNodeId, selectNodeId);
}

// Passive refresh
function PassiveRefresh(nodeId, parentNodeId)
{
    parent.PassiveRefresh(nodeId, parentNodeId);
}

// Create another
function CreateAnother()
{
    parent.CreateAnother();
}

// Spell check
function SpellCheck(spellURL) {
    checkSpelling(spellURL);
}