using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.LicenseProvider;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_Address : CMSEcommerceModalPage
{
    protected int addressId = 0;
    protected int customerId = 0;
    protected int typeId = -1;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Order_Edit_Address.Title");

        rqvCity.ErrorMessage = ResHelper.GetString("Customer_Edit_Address_Edit.rqvCity");
        rqvLine.ErrorMessage = ResHelper.GetString("Customer_Edit_Address_Edit.rqvLine");
        rqvZipCode.ErrorMessage = ResHelper.GetString("Customer_Edit_Address_Edit.rqvZipCode");
        rqvPersonalName.ErrorMessage = ResHelper.GetString("Customer_Edit_Address_Edit.rqvPersonalName");

        // control initializations				
        lblAddressZip.Text = ResHelper.GetString("Customer_Edit_Address_Edit.AddressZipLabel");
        //lblAddressState.Text = ResHelper.GetString("Customer_Edit_Address_Edit.AddressStateIDLabel");
        lblAddressDeliveryPhone.Text = ResHelper.GetString("Customer_Edit_Address_Edit.AddressDeliveryPhoneLabel");
        lblAddressCountry.Text = ResHelper.GetString("Customer_Edit_Address_Edit.AddressCountryIDLabel");
        //lblAddressName.Text = ResHelper.GetString("Customer_Edit_Address_Edit.AddressNameLabel");
        //lblAddressLine3.Text = ResHelper.GetString("Customer_Edit_Address_Edit.AddressLine3Label");
        lblAddressLine1.Text = ResHelper.GetString("Customer_Edit_Address_Edit.AddressLine1Label");
        //lblAddressLine2.Text = ResHelper.GetString("Customer_Edit_Address_Edit.AddressLine2Label");
        lblAddressCity.Text = ResHelper.GetString("Customer_Edit_Address_Edit.AddressCityLabel");
        lblPersonalName.Text = ResHelper.GetString("Customer_Edit_Address_Edit.lblPersonalName");

        btnOk.Text = ResHelper.GetString("General.OK");
        btnCancel.Text = ResHelper.GetString("General.Cancel");

        string currentAddress = ResHelper.GetString("Customer_Edit_Address_Edit.NewItemCaption");

        // get address id from querystring		
        customerId = ValidationHelper.GetInteger(Request.QueryString["customerId"], 0);
        addressId = ValidationHelper.GetInteger(Request.QueryString["addressId"], 0);
        typeId = ValidationHelper.GetInteger(Request.QueryString["typeId"], -1);
        if (addressId > 0)
        {
            AddressInfo addressObj = AddressInfoProvider.GetAddressInfo(addressId);
            if (addressObj != null)
            {
                currentAddress = addressObj.AddressName;

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(addressObj);
                }
            }
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_Address/object.png");
        }

        else
        {
            if (!RequestHelper.IsPostBack())
            {
                // Init data due to customer settings
                InitData();
            }
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_Address/new.png");
        }
    }


    /// <summary>
    /// Load data of editing address.
    /// </summary>
    /// <param name="addressObj">Address object.</param>
    protected void LoadData(AddressInfo addressObj)
    {
        txtAddressZip.Text = addressObj.AddressZip;
        txtAddressDeliveryPhone.Text = addressObj.AddressPhone;
        txtPersonalName.Text = addressObj.AddressPersonalName;
        //txtAddressName.Text = addressObj.AddressName;
        txtAddressLine1.Text = addressObj.AddressLine1;
        chkAddressEnabled.Checked = addressObj.AddressEnabled;
        txtAddressLine2.Text = addressObj.AddressLine2;
        txtAddressCity.Text = addressObj.AddressCity;

        ucCountrySelector.CountryID = addressObj.AddressCountryID;
        ucCountrySelector.StateID = addressObj.AddressStateID;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        if (customerId != 0)
        {
            string errorMessage = new Validator().NotEmpty(txtAddressLine1.Text, "Customer_Edit_Address_Edit.rqvLine").NotEmpty(txtAddressCity.Text, "Customer_Edit_Address_Edit.rqvCity").NotEmpty(txtAddressZip.Text, "Customer_Edit_Address_Edit.rqvZipCode").NotEmpty(txtPersonalName.Text, "Customer_Edit_Address_Edit.rqvPersonalName").Result;

            if (errorMessage == "")
            {
                AddressInfo addressObj = AddressInfoProvider.GetAddressInfo(addressId);

                // if address doesn't already exist, create new one
                if (addressObj == null)
                {
                    addressObj = new AddressInfo();
                    addressObj.AddressIsShipping = false;
                    addressObj.AddressIsBilling = false;
                    addressObj.AddressIsCompany = false;
                }

                switch (typeId)
                {
                    // Shipping addres selection
                    case 0:
                        addressObj.AddressIsShipping = true;
                        break;
                    // Billing addres selection
                    case 1:
                        addressObj.AddressIsBilling = true;
                        break;
                    // Company addres selection
                    case 2:
                        addressObj.AddressIsCompany = true;
                        break;
                    default:
                        break;
                }
                addressObj.AddressZip = txtAddressZip.Text.Trim();
                addressObj.AddressPhone = txtAddressDeliveryPhone.Text.Trim();
                addressObj.AddressPersonalName = txtPersonalName.Text.Trim();
                addressObj.AddressLine1 = txtAddressLine1.Text.Trim();
                addressObj.AddressEnabled = chkAddressEnabled.Checked;
                addressObj.AddressLine2 = txtAddressLine2.Text.Trim();
                addressObj.AddressCity = txtAddressCity.Text.Trim();                

                addressObj.AddressCountryID = ucCountrySelector.CountryID;
                int stateId = ucCountrySelector.StateID;
                if (stateId != 0)
                {
                    addressObj.AddressStateID = stateId;
                }
                addressObj.AddressCustomerID = customerId;

                addressObj.AddressName = AddressInfoProvider.GetAddressName(addressObj);

                AddressInfoProvider.SetAddressInfo(addressObj);

                ltlScript.Text = ScriptHelper.GetScript("if(wopener.AddressChange!=null){wopener.AddressChange('" + addressObj.AddressID + "');}window.close();");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = errorMessage;
            }
        }
    }


    private void InitData()
    {
        CustomerInfo ci = CustomerInfoProvider.GetCustomerInfo(customerId);
        if (ci != null)
        {
            ucCountrySelector.CountryID = ci.CustomerCountryID;
            ucCountrySelector.StateID = ci.CustomerStateID;
            txtAddressDeliveryPhone.Text = ci.CustomerPhone;
            txtPersonalName.Text = ci.CustomerFirstName + " " + ci.CustomerLastName;
        }
    }
}
