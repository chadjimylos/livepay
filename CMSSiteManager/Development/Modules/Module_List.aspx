<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Module_List.aspx.cs" Inherits="CMSSiteManager_Development_Modules_Module_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Modules - Module List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:UniGrid ID="UniGridModules" runat="server" GridName="Module_List.xml" OrderBy="ResourceDisplayName"
        Columns="ResourceID, ResourceDisplayName" IsLiveSite="false" />
</asp:Content>
