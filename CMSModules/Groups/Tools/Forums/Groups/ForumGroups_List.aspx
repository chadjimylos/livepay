<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="ForumGroups_List.aspx.cs" Inherits="CMSModules_Groups_Tools_Forums_Groups_ForumGroups_List"
    Title="Forum Groups List" Theme="Default" %>
<%@ Register Src="~/CMSModules/Forums/Controls/Forums/ForumGroupList.ascx" TagName="ForumGroupsList" TagPrefix="cms" %>

<asp:Content ID="Content3" ContentPlaceHolderID="plcContent" runat="Server">
    <cms:ForumGroupsList ID="forumGroupsList" runat="server" />
</asp:Content>
