<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SKUSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_FormControls_SKUSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%SKUName%}"
            ObjectType="ecommerce.sku" ResourcePrefix="productselector" ReturnColumnName="SKUID"
            SelectionMode="SingleTextBox" AllowEditTextBox="false" AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
