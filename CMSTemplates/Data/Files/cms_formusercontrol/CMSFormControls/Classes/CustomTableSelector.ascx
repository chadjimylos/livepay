<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomTableSelector.ascx.cs"
    Inherits="CMSFormControls_Classes_CustomTableSelector" %>
<%@ Register Src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" TagName="UniSelector"
    TagPrefix="cms" %>
<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" AllowEditTextBox="true" DisplayNameFormat="{%ClassDisplayName%} ({%ClassName%})"
            ReturnColumnName="ClassName" ObjectType="cms.documenttype" ResourcePrefix="customtableselector"
            SelectionMode="SingleDropDownList" AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
