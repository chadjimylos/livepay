using System;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSModules_Content_CMSDesk_Properties_Advanced_EditableContent_Header : CMSModalPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initializes page title control
        CurrentMaster.Title.TitleText = ResHelper.GetString("EditableContent.Header");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Content/EditableContent/editablecontent.png");
    }
}
