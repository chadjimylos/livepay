using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.PortalControls;
using CMS.PortalEngine;
using CMS.CMSHelper;

public partial class CMSModules_PortalEngine_UI_WebParts_WebPartProperties_properties : CMSWebPartPropertiesPage
{
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Check permissions for web part properties UI
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        if (!currentUser.IsAuthorizedPerUIElement("CMS.Content", "WebPartProperties.General"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "WebPartProperties.General");
        }

        // Initialize the control
        webPartProperties.AliasPath = ValidationHelper.GetString(Request.QueryString["aliaspath"], "");
        webPartProperties.WebpartId = ValidationHelper.GetString(Request.QueryString["webpartid"], "");
        webPartProperties.ZoneId = ValidationHelper.GetString(Request.QueryString["zoneid"], "");
        webPartProperties.InstanceGUID = ValidationHelper.GetGuid(Request.QueryString["instanceguid"], Guid.Empty);
        webPartProperties.IsNewWebPart = QueryHelper.GetBoolean("isnew", false);
    }
}
