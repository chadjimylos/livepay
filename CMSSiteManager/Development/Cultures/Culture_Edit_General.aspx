<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Culture_Edit_General.aspx.cs"
    Inherits="CMSSiteManager_Development_Cultures_Culture_Edit_General" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Culture Edit - General" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCultureName" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCultureName" runat="server" CssClass="TextBoxField" MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvCultureName" runat="server" ControlToValidate="txtCultureName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCultureCode" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCultureCode" runat="server" CssClass="TextBoxField" MaxLength="10" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvCultureCode" runat="server" ControlToValidate="txtCultureCode"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCultureShortName" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCultureShortName" runat="server" CssClass="TextBoxField" MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvCultureShortName" runat="server" ControlToValidate="txtCultureShortName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton"
                    EnableViewState="false" />
            </td>
        </tr>
    </table>
</asp:Content>
