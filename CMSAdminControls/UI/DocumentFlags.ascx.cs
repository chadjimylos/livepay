using System;
using System.Data;
using System.Text;

using CMS.TreeEngine;
using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;

public partial class CMSAdminControls_UI_DocumentFlags : DocumentFlagsControl
{
    #region "Private variables"

    private int mNodeID = 0;
    private object mDataSource = null;
    private DataSet mSiteCultures = null;
    private string mDefaultSiteCulture = null;
    private int mRepeatColumns = 10;

    #endregion


    #region "Public propetries"

    /// <summary>
    /// Gets or sets Node ID
    /// </summary>
    public override int NodeID
    {
        get
        {
            return mNodeID;

        }
        set
        {
            mNodeID = value;
        }
    }


    /// <summary>
    /// Number of columns for the flags
    /// </summary>
    public override int RepeatColumns
    {
        get
        {
            return mRepeatColumns;

        }
        set
        {
            mRepeatColumns = value;
        }
    }


    /// <summary>
    /// Data source object
    /// </summary>
    public override object DataSource
    {
        get
        {
            return mDataSource;

        }
        set
        {
            mDataSource = value;
        }
    }


    /// <summary>
    /// DataSet containing all site cultures
    /// </summary>
    public override DataSet SiteCultures
    {
        get
        {
            return mSiteCultures;

        }
        set
        {
            mSiteCultures = value;
        }
    }


    /// <summary>
    /// Default culture of the site
    /// </summary>
    public string DefaultSiteCulture
    {
        get
        {
            if (mDefaultSiteCulture == null)
            {
                mDefaultSiteCulture = CultureHelper.GetDefaultCulture(CMSContext.CurrentSiteName);
            }
            return mDefaultSiteCulture;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            ReloadData();
        }
    }

    #endregion


    #region "Public methods"

    override public void ReloadData()
    {
        // Hide control for one culture version
        if ((DataSource == null) || DataHelper.DataSourceIsEmpty(SiteCultures) || (SiteCultures.Tables[0].Rows.Count <= 1))
        {
            Visible = false;
        }
        else
        {
            // Check the data source
            if (!(DataSource is GroupedDataSource))
            {
                throw new Exception("[DocumentFlags]: Only GroupedDataSource is supported as a data source.");
            }

            // Register tooltip script
            ScriptHelper.RegisterTooltip(Page);

            // Get appropriate table from the data source
            GroupedDataSource gDS = (GroupedDataSource)DataSource;
            DataTable table = gDS.GetGroupDataTable(NodeID);

            // Get document in the default site culture
            DateTime defaultLastModification = DateTimeHelper.ZERO_TIME;
            DateTime defaultLastPublished = DateTimeHelper.ZERO_TIME;
            bool defaultCultureExists = false;
            DataRow[] rows = null;
            if (table != null)
            {
                rows = table.Select("DocumentCulture='" + DefaultSiteCulture + "'");
                defaultCultureExists = (rows.Length > 0);
            }

            if (defaultCultureExists)
            {
                defaultLastModification = ValidationHelper.GetDateTime(rows[0]["DocumentModifiedWhen"], DateTimeHelper.ZERO_TIME);
                defaultLastPublished = ValidationHelper.GetDateTime(rows[0]["DocumentLastPublished"], DateTimeHelper.ZERO_TIME);
            }

            string strCulture = ResHelper.GetString("transman.language");
            string strStatus = ResHelper.GetString("transman.status");
            string strModified = ResHelper.GetString("transman.modified");
            string strVersion = ResHelper.GetString("transman.version");

            // Build the content
            StringBuilder sb = new StringBuilder();

            sb.Append("<table cellpadding=\"1\" cellspacing=\"0\" class=\"DocumentFlags\">\n<tr>\n");
            int colsInRow = 0;
            int cols = 0;
            int colsCount = SiteCultures.Tables[0].Rows.Count;
            if (colsCount < RepeatColumns)
            {
                RepeatColumns = colsCount;
            }

            foreach (DataRow dr in SiteCultures.Tables[0].Rows)
            {
                ++cols;
                ++colsInRow;
                DateTime lastModification = DateTimeHelper.ZERO_TIME;
                string versionNumber = null;
                TranslationStatusEnum status = TranslationStatusEnum.NotAvailable;
                string cultureName = ValidationHelper.GetString(DataHelper.GetDataRowValue(dr, "CultureName"), "-");
                string cultureCode = ValidationHelper.GetString(DataHelper.GetDataRowValue(dr, "CultureCode"), "-");

                // Get document for given culture
                if (table != null)
                {
                    rows = table.Select("DocumentCulture='" + cultureCode + "'");
                    // Document doesn't exist
                    if (rows.Length != 0)
                    {
                        versionNumber = ValidationHelper.GetString(DataHelper.GetDataRowValue(rows[0], "VersionNumber"), null);

                        // Check if document is outdated
                        if (versionNumber != null)
                        {
                            lastModification = ValidationHelper.GetDateTime(rows[0]["DocumentLastPublished"], DateTimeHelper.ZERO_TIME);
                            status = (lastModification < defaultLastPublished) ? TranslationStatusEnum.Outdated : TranslationStatusEnum.Translated;
                        }
                        else
                        {
                            lastModification = ValidationHelper.GetDateTime(rows[0]["DocumentModifiedWhen"], DateTimeHelper.ZERO_TIME);
                            status = (lastModification < defaultLastModification) ? TranslationStatusEnum.Outdated : TranslationStatusEnum.Translated;
                        }
                    }
                }

                sb.Append("<td class=\"" + GetStatusCSSClass(status) + "\">");
                sb.Append("<img onmouseout=\"UnTip()\" style=\"cursor:pointer;\" onclick=\"if(RedirectItem!=null){RedirectItem('" + NodeID + "','" + cultureCode + "');}\" onmouseover=\"Tip('");

                // Tooltip HTML code
                StringBuilder sbTooltip = new StringBuilder();

                // Check if RTL culture
                bool isRTL = CultureHelper.IsUICultureRTL();

                if (isRTL)
                {
                    sbTooltip.Append("<div class='RTL'>");
                }
                sbTooltip.Append("<div class='FlagTooltip'><div style='text-align:center;'><img class='Icon' src='");
                sbTooltip.Append(GetFlagIconUrl(cultureCode, "48x48"));
                sbTooltip.Append("' alt='");
                sbTooltip.Append(cultureName);
                sbTooltip.Append("' /></div><div class='Text'>");
                sbTooltip.Append(string.Format(strCulture, cultureName + "&nbsp;"));
                sbTooltip.Append("<br />");
                sbTooltip.Append(string.Format(strStatus, GetStatusString(status) + "&nbsp;"));
                if (versionNumber != null)
                {
                    sbTooltip.Append("<br />");
                    sbTooltip.Append(string.Format(strVersion, versionNumber + "&nbsp;"));
                }
                if (lastModification != DateTimeHelper.ZERO_TIME)
                {
                    sbTooltip.Append("<br />");
                    sbTooltip.Append(string.Format(strModified, lastModification + "&nbsp;"));
                }
                sbTooltip.Append("</div></div>");
                if (isRTL)
                {
                    sbTooltip.Append("</div>");
                }

                sb.Append(ScriptHelper.FormatTooltipString(sbTooltip.ToString(), false));
                sb.Append("')\" src=\"");
                sb.Append(GetFlagIconUrl(cultureCode, "16x16"));
                sb.Append("\" alt=\"");
                sb.Append(cultureName);
                sb.Append("\" /></td>\n");

                // Ensure repeat columns
                if (((colsInRow % RepeatColumns) == 0) && (cols != colsCount))
                {
                    sb.Append("</tr><tr>\n");
                    colsInRow = 0;
                }
            }

            // Ensure rest of the table cells
            if ((colsInRow != RepeatColumns) && (colsInRow != 0))
            {
                while (colsInRow < RepeatColumns)
                {
                    sb.Append("<td>&nbsp;</td>\n");
                    ++colsInRow;
                }
            }
            sb.Append("</tr>\n</table>\n");
            ltlFlags.Text = sb.ToString();
        }
    }

    #endregion


    #region "Private methods"

    private static string GetStatusCSSClass(TranslationStatusEnum status)
    {
        switch (status)
        {
            case TranslationStatusEnum.NotAvailable:
                return "NotAvailable";

            case TranslationStatusEnum.Outdated:
                return "Outdated";

            default:
                return "Translated";
        }
    }


    private static string GetStatusString(TranslationStatusEnum status)
    {
        switch (status)
        {
            case TranslationStatusEnum.NotAvailable:
                return ResHelper.GetString("transman.NotAvailable");

            case TranslationStatusEnum.Outdated:
                return ResHelper.GetString("transman.Outdated");

            default:
                return ResHelper.GetString("transman.Translated");
        }
    }

    #endregion
}
