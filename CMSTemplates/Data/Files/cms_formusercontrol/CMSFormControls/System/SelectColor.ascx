<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectColor.ascx.cs" Inherits="CMSFormControls_System_SelectColor" %>
<div class="ColorPickerFormControl">
    <cms:ColorPicker ID="clrPicker" runat="server" SupportFolder="~/CMSAdminControls/ColorPicker" />
</div>
