﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LinkMediaSelector.ascx.cs"
    Inherits="CMSModules_MediaLibrary_Controls_Dialogs_LinkMediaSelector" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/MediaLibraryTree.ascx"
    TagName="MediaLibraryTree" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/Dialogs/MediaView.ascx" TagName="MediaView"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/LinkMediaSelector/Menu.ascx"
    TagName="DialogMenu" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/Properties/HTMLMediaProperties.ascx"
    TagName="HTMLMediaProperties" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/Properties/URLProperties.ascx"
    TagName="URLProperties" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/Properties/HTMLLinkProperties.ascx"
    TagName="HTMLLinkProperties" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/Properties/BBMediaProperties.ascx"
    TagName="BBMediaProperties" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/Properties/BBLinkProperties.ascx"
    TagName="BBLinkProperties" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/Properties/NodeGuidProperties.ascx"
    TagName="NodeGuidProperties" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/Dialogs/AdvancedMediaLibrarySelector.ascx"
    TagName="LibrarySelector" TagPrefix="cms" %>

<script type="text/javascript" language="javascript">
    function insertItem() {
        SetAction('insertItem', '');
        RaiseHiddenPostBack();
    }
</script>

<div class="Hidden">
    <asp:UpdatePanel ID="pnlUpdateHidden" runat="server">
        <ContentTemplate>
            <asp:Literal ID="ltlScript" runat="server" EnableViewState="false"></asp:Literal>
            <asp:HiddenField ID="hdnAction" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hdnArgument" runat="server"></asp:HiddenField>
            <cms:CMSButton ID="hdnButton" runat="server" OnClick="hdnButton_Click" CssClass="HiddenButton"
                EnableViewState="false" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<div class="DialogMainBlock">
    <asp:UpdatePanel ID="pnlUpdateMenu" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <cms:DialogMenu ID="menuElem" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="DialogContent">
        <asp:Panel ID="pnlLeftContent" runat="server" CssClass="DialogLeftBlock">
            <div class="DialogMediaLibraryBlock">
                <asp:UpdatePanel ID="pnlUpdateSelectors" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <cms:LibrarySelector ID="librarySelector" runat="server" OnLibraryChanged="librarySelector_LibraryChanged" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:Panel runat="server" ID="pnlTreeAreaSep" CssClass="DialogMediaLibraryTreeAreaSep">
                <asp:UpdatePanel ID="pnlUpdateTreeSeparator" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <cms:LocalizedLabel runat="server" ID="lblNoLibraries" ResourceString="dialogs.libraries.nolibraries"
                            EnableViewState="false" Visible="false" CssClass="InfoLabel" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <asp:Panel ID="pnlTreeArea" runat="server" class="DialogTreeArea">
                <div class="DialogTree">
                    <asp:UpdatePanel ID="pnlUpdateTree" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                        <ContentTemplate>
                            <asp:Panel ID="pnlTree" runat="server">
                                <cms:MediaLibraryTree ID="folderTree" runat="server" GenerateIDs="true" />
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>
            <div class="DialogResizerH">
                <div class="DialogResizerArrowH">
                    &nbsp;</div>
            </div>
        </asp:Panel>
        <div class="DialogTreeAreaSeparator">
        </div>
        <asp:Panel ID="pnlRightContent" runat="server" CssClass="DialogRightBlock">
            <asp:UpdatePanel ID="pnlUpdateContent" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>
                    <div id="divDialogView" class="DialogViewContent" runat="server">
                        <asp:UpdatePanel ID="pnlUpdateView" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="pnlInfo" runat="server" CssClass="DialogErrorArea" Visible="false">
                                    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"></asp:Label>
                                    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"></asp:Label>
                                </asp:Panel>
                                <cms:MediaView ID="mediaView" runat="server" />
                                <asp:HiddenField ID="hdnLastSearchedValue" runat="server" />
                                <asp:HiddenField ID="hdnLastSelectedPath" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:Literal ID="ltlColorizeScript" runat="server"></asp:Literal>
                    </div>
                    <div id="divDialogResizer" class="DialogResizerVLine" runat="server" enableviewstate="false">
                        <div class="DialogResizerV">
                            <div class="DialogResizerArrowV">
                                &nbsp;</div>
                        </div>
                    </div>
                    <div id="divDialogProperties" class="DialogProperties" runat="server">
                        <asp:UpdatePanel ID="pnlUpdateProperties" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="pnlEmpty" runat="server" CssClass="DialogInfoArea">
                                    <cms:LocalizedLabel ID="lblEmpty" runat="server" CssClass="InfoLabel" EnableViewState="false"></cms:LocalizedLabel>
                                </asp:Panel>
                                <cms:HTMLMediaProperties ID="htmlMediaProp" runat="server" Visible="false" SourceType="MediaLibraries" />
                                <cms:HTMLLinkProperties ID="htmlLinkProp" runat="server" Visible="false" />
                                <cms:BBMediaProperties ID="bbMediaProp" runat="server" Visible="false" />
                                <cms:BBLinkProperties ID="bbLinkProp" runat="server" Visible="false" />
                                <cms:URLProperties ID="urlProp" runat="server" Visible="false" />
                                <cms:NodeGuidProperties ID="nodeGuidProp" runat="server" Visible="false" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
</div>
