using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_TaxClasses_TaxClass_New : CMSEcommerceConfigurationPage
{
    protected int taxclassid = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        rfvTaxClassDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvTaxClassName.ErrorMessage = ResHelper.GetString("general.requirescodename");

        // control initializations				
        lblTaxClassDisplayName.Text = ResHelper.GetString("TaxClass_Edit.TaxClassDisplayNameLabel");
        lblTaxClassName.Text = ResHelper.GetString("TaxClass_Edit.TaxClassNameLabel");
        lblTaxZero.Text = ResHelper.GetString("TaxClass_Edit.lblTaxZero");

        btnOk.Text = ResHelper.GetString("General.OK");

        string currentTaxClass = ResHelper.GetString("TaxClass_Edit.NewItemCaption");

        // initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("TaxClass_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/TaxClasses/TaxClass_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentTaxClass;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("TaxClass_Edit.HeaderCaption_New");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_TaxClass/new.png");
        this.CurrentMaster.Title.HelpTopicName = "new_classgeneral_tab";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        string errorMessage = new Validator()
            .NotEmpty(txtTaxClassDisplayName.Text.Trim(), ResHelper.GetString("general.requiresdisplayname"))
            .NotEmpty(txtTaxClassName.Text.Trim(), ResHelper.GetString("general.requirescodename")).Result;

        if (!ValidationHelper.IsCodeName(txtTaxClassName.Text.Trim()))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

        if (errorMessage == "")
        {
            TaxClassInfo taxClassObj = TaxClassInfoProvider.GetTaxClassInfo(txtTaxClassName.Text.Trim());

           														
            if (taxClassObj == null)
            {
                // if taxClassName value is unique
                taxClassObj = new TaxClassInfo();

                taxClassObj.TaxClassDisplayName = txtTaxClassDisplayName.Text.Trim();
                taxClassObj.TaxClassName = txtTaxClassName.Text.Trim();
                taxClassObj.TaxClassZeroIfIDSupplied = chkTaxZero.Checked;

                TaxClassInfoProvider.SetTaxClassInfo(taxClassObj);

                UrlHelper.Redirect("TaxClass_Frameset.aspx?taxclassid=" + Convert.ToString(taxClassObj.TaxClassID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("TaxClass_Edit.TaxClassNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
