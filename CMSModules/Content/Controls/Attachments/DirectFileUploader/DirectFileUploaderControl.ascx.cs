using System;
using System.Web;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.TreeEngine;
using CMS.FileManager;
using CMS.CMSHelper;
using CMS.WorkflowEngine;
using CMS.LicenseProvider;
using CMS.SettingsProvider;
using CMS.FormEngine;
using CMS.SiteProvider;

public partial class CMSModules_Content_Controls_Attachments_DirectFileUploader_DirectFileUploaderControl : DirectFileUploader
{
    #region "Variables"
    
    private bool? mIsSupportedBrowser = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Uploaded file
    /// </summary>
    public override HttpPostedFile PostedFile
    {
        get
        {
            return ucFileUpload.PostedFile;
        }
    }


    /// <summary>
    /// File upload user control.
    /// </summary>
    public override CMSFileUpload FileUploadControl
    {
        get
        {
            return ucFileUpload;
        }
    }


    /// <summary>
    /// Indicates if supported browser.
    /// </summary>
    public bool IsSupportedBrowser
    {
        get
        {
            if(mIsSupportedBrowser == null)
            {
                string browser = DataHelper.GetNotEmpty(CMSContext.GetBrowserClass(), "").ToLower();
                mIsSupportedBrowser = browser.Contains("ie") || browser.Contains("gecko");
            }
            return mIsSupportedBrowser.Value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            // Do nothing
            Visible = false;
        }
        else
        {
            ScriptHelper.RegisterScriptFile(Page, "~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploader.js");

            // Prepare disabled image
            // Prepare loading image
            imgLoading.ImageUrl = ResolveUrl(LoadingImageUrl);
            lblLoading.CssClass = InnerLoadingDivClass;
            lblLoading.Text = InnerLoadingDivHtml;
            pnlLoading.Style.Add("display", "none");
            bool isRTL = (IsLiveSite && CultureHelper.IsPreferredCultureRTL()) || (!IsLiveSite && CultureHelper.IsUICultureRTL());
            if (isRTL)
            {
                pnlLoading.Style.Add("text-align", "right");
                pnlStandard.Style.Add("text-align", "right");
            }

            // Hide uploader if browser is unsupported
            if (!IsSupportedBrowser)
            {
                plcUploader.Visible = false;
                plcStandard.Visible = true;
                imgStandard.ImageUrl = ResolveUrl(ImageUrl);
                lblStandard.Text = InnerDivHtml;
                lblStandard.CssClass = InnerDivClass;

                // Register dialog script
                ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
                string modalDialog = "modalDialog('" + CMSContext.ResolveDialogUrl(GetDialogUrl()) + "', 'imageEditorDialog', 300, 180);";
                imgStandard.Attributes.Add("onclick", modalDialog);
                lblStandard.Attributes.Add("onclick", modalDialog);
            }
            else
            {
                // Initialize uploader
                ltlCss.Text = CreateCss();
                HtmlGenericControl lblContainer = (HtmlGenericControl)FindControl("container");
                lblContainer.Attributes.Add("class", "container");
                ucFileUpload.Attributes.Add("class", "fileUpload");
                ucFileUpload.Attributes.Add("onchange", "document.getElementById('" + pnlLoading.ClientID + "').style.display='';document.getElementById('" + lblContainer.ClientID + "').style.display='none';" + Page.ClientScript.GetPostBackEventReference(btnHidden, null, false));
                
                // SI script
                string SIScript = "var uploaderInitilaized=false;function InitUploader(){if((window.SI) && (!uploaderInitilaized)){ uploaderInitilaized=true;SI.Files.stylizeById('" + ucFileUpload.ClientID + "');}}InitUploader();";
                ltlScript.Text += ScriptHelper.GetScript(SIScript);

                string rtl = (isRTL) ? "var rtl = true;" : "var rtl = false;";
                ltlInnerDiv.Text = ScriptHelper.GetScript("var innerDivHtml = '" + InnerDivHtml.Replace("'", "\\\'") + "';\nvar innerDivClass='" + InnerDivClass.Replace("'", "\\\'") + "';" + rtl);
                btnHidden.Attributes.Add("style", "display:none;");
            }
        }
    }


    protected void btnHidden_Click(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            switch (SourceType)
            {
                case MediaSourceEnum.Attachment:
                    HandleAttachmentUpload(true);
                    break;

                case MediaSourceEnum.DocumentAttachments:
                    HandleAttachmentUpload(false);
                    break;

                case MediaSourceEnum.Content:
                    HandleContentUpload();
                    break;

                default:
                    break;
            }
        }
    }

    #endregion


    #region "Attachments"

    /// <summary>
    /// Provides operations necessary to create and store new attachment.
    /// </summary>
    private void HandleAttachmentUpload(bool fieldAttachment)
    {
        TreeProvider tree = null;
        TreeNode node = null;

        // New attachment
        AttachmentInfo newAttachment = null;

        string message = string.Empty;
        bool fullRefresh = false;

        try
        {
            // Get the existing document
            if (DocumentID != 0)
            {
                // Get document
                tree = new TreeProvider(CMSContext.CurrentUser, Connection);
                node = DocumentHelper.GetDocument(DocumentID, tree);
                if (node == null)
                {
                    throw new Exception("Given document doesn't exist!");
                }
            }

            #region "Check permissions"
            
            if (CheckPermissions)
            {
                CheckNodePermissions(node);
            }

            #endregion

            // Check the allowed extensions
            CheckAllowedExtensions();

            // Standard attachments
            if (DocumentID != 0)
            {
                // Ensure automatic check-in/check-out
                bool autoCheck = !VersionManager.UseCheckInCheckOut(NodeSiteName);
                WorkflowManager workflowMan = new WorkflowManager(tree);
                VersionManager vm = null;

                // Check if the document uses workflow
                bool useWorkflow = (workflowMan.GetNodeWorkflow(node) != null);

                // Check out the document
                if (autoCheck && useWorkflow)
                {
                    // Get current step info
                    WorkflowStepInfo si = workflowMan.GetStepInfo(node);
                    if (si != null)
                    {
                        // Decide if full refresh is needed
                        string stepName = si.StepName.ToLower();
                        fullRefresh = (stepName == "published") || (stepName == "archived");
                    }

                    vm = new VersionManager(tree);
                    vm.CheckOut(node);
                }

                // Handle field attachment
                if (fieldAttachment)
                {
                    newAttachment = DocumentHelper.AddAttachment(node, AttachmentGUIDColumnName, Guid.Empty, Guid.Empty, ucFileUpload.PostedFile, tree, ResizeToWidth, ResizeToHeight, ResizeToMaxSideSize);
                    // Update attachment field
                    DocumentHelper.UpdateDocument(node, tree);
                }
                // Handle grouped and unsorted attachments
                else
                {
                    // Grouped attachment
                    if (AttachmentGroupGUID != Guid.Empty)
                    {
                        newAttachment = DocumentHelper.AddGroupedAttachment(node, AttachmentGUID, AttachmentGroupGUID, ucFileUpload.PostedFile, tree, ResizeToWidth, ResizeToHeight, ResizeToMaxSideSize);
                    }
                    // Unsorted attachment
                    else
                    {
                        newAttachment = DocumentHelper.AddUnsortedAttachment(node, AttachmentGUID, ucFileUpload.PostedFile, tree, ResizeToWidth, ResizeToHeight, ResizeToMaxSideSize);
                    }
                }

                // Check in the document
                if (autoCheck && useWorkflow)
                {
                    if (vm != null)
                    {
                        vm.CheckIn(node, null, null);
                    }
                }

                // Log synchronization task if not under workflow
                if (!useWorkflow)
                {
                    DocumentHelper.LogSynchronization(node, CMS.Staging.TaskTypeEnum.UpdateDocument, tree);
                }
            }

            // Temporary attachments
            if (FormGUID != Guid.Empty)
            {
                newAttachment = AttachmentManager.AddTemporaryAttachment(FormGUID, AttachmentGUIDColumnName, AttachmentGUID, AttachmentGroupGUID, ucFileUpload.PostedFile, CMSContext.CurrentSiteID, ResizeToWidth, ResizeToHeight, ResizeToMaxSideSize);
            }

            // Ensure properties update
            if ((newAttachment != null) && !InsertMode)
            {
                AttachmentGUID = newAttachment.AttachmentGUID;
            }

            if (newAttachment == null)
            {
                throw new Exception("The attachment hasn't been created since no DocumentID or FormGUID was supplied.");
            }
        }
        catch (Exception ex)
        {
            message = ex.Message;
        }
        finally
        {
            // Call aftersave javascript if exists
            if (!String.IsNullOrEmpty(AfterSaveJavascript))
            {
                if ((message == string.Empty) && (newAttachment != null))
                {
                    string url = null;
                    string saveName = UrlHelper.GetSafeFileName(newAttachment.AttachmentName, CMSContext.CurrentSiteName);
                    if (node != null)
                    {
                        SiteInfo si = SiteInfoProvider.GetSiteInfo(node.NodeSiteID);
                        if (si != null)
                        {
                            bool usePermanent = SettingsKeyProvider.GetBoolValue(si.SiteName + ".CMSUsePermanentURLs");
                            if (usePermanent)
                            {
                                url = ResolveUrl(AttachmentManager.GetAttachmentUrl(newAttachment.AttachmentGUID, saveName));
                            }
                            else
                            {
                                url = ResolveUrl(AttachmentManager.GetAttachmentUrl(saveName, node.NodeAliasPath));
                            }
                        }
                    }
                    else
                    {
                        url = ResolveUrl(AttachmentManager.GetAttachmentUrl(newAttachment.AttachmentGUID, saveName));
                    }
                    // Calling javascript function with parameters attachments url, name, width, height
                    string jsParams = "('" + url + "', '" + newAttachment.AttachmentName + "', '" + newAttachment.AttachmentImageWidth + "', '" + newAttachment.AttachmentImageHeight + "');";
                    string script = "if (window." + AfterSaveJavascript + " != null){window." + AfterSaveJavascript + jsParams + "}";
                    script += "else if ((window.parent != null) && (window.parent." + AfterSaveJavascript + " != null)){window.parent." + AfterSaveJavascript + jsParams + "}";
                    ltlScript.Text += ScriptHelper.GetScript(script);
                }
                else
                {
                    ltlScript.Text += ScriptHelper.GetAlertScript(message);
                }
            }

            // Create attachment info string
            string attachmentInfo = "";
            if ((newAttachment != null) && (newAttachment.AttachmentGUID != Guid.Empty) && (IncludeNewItemInfo))
            {
                attachmentInfo = newAttachment.AttachmentGUID.ToString();
            }

            // Ensure message text
            message = HTMLHelper.EnsureLineEnding(message, " ");

            // Call function to refresh parent window
            ltlScript.Text += ScriptHelper.GetScript("if ((window.parent != null) && (/parentelemid=" + ParentElemID + "/i.test(window.location.href)) && (window.parent.InitRefresh_" + ParentElemID + " != null)){window.parent.InitRefresh_" + ParentElemID + "(" + ScriptHelper.GetString(message.Trim()) + ", " + (fullRefresh ? "true" : "false") + ((attachmentInfo != "") ? ", '" + attachmentInfo + "'" : "") + (InsertMode ? ", 'insert'" : ", 'update'") + ");}");
        }
    }


    /// <summary>
    /// Check permissions
    /// </summary>
    /// <param name="node">Tree node</param>
    private void CheckNodePermissions(TreeNode node)
    {
        // For new document
        if (FormGUID != Guid.Empty)
        {
            if (NodeParentNodeID == 0)
            {
                throw new Exception(ResHelper.GetString("attach.document.parentmissing"));
            }

            if (!RaiseOnCheckPermissions("Create", this))
            {
                if (!CMSContext.CurrentUser.IsAuthorizedToCreateNewDocument(NodeParentNodeID, NodeClassName))
                {
                    throw new Exception(ResHelper.GetString("attach.actiondenied"));
                }
            }
        }
        // For existing document
        else if (DocumentID > 0)
        {
            if (!RaiseOnCheckPermissions("Modify", this))
            {
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                {
                    throw new Exception(ResHelper.GetString("attach.actiondenied"));
                }
            }
        }
    }

    #endregion


    #region "Content files"

    /// <summary>
    /// Provides operations necessary to create and store new content file.
    /// </summary>
    private void HandleContentUpload()
    {
        TreeNode node = null;
        TreeProvider tree = null;

        string message = string.Empty;
        bool newDocumentCreated = false;

        try
        {
            if (NodeParentNodeID == 0)
            {
                throw new Exception(ResHelper.GetString("dialogs.document.parentmissing"));
            }

            // Check license limitations
            if (!LicenseHelper.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Documents, VersionActionEnum.Insert))
            {
                throw new Exception(ResHelper.GetString("cmsdesk.documentslicenselimits"));
            }

            // Check if class exists
            DataClassInfo ci = DataClassInfoProvider.GetDataClass("CMS.File");
            if (ci == null)
            {
                throw new Exception(string.Format(ResHelper.GetString("dialogs.newfile.classnotfound"), "CMS.File"));
            }

            #region "Check permissions"

            // Get the node
            tree = new TreeProvider(CMSContext.CurrentUser);
            TreeNode parentNode = tree.SelectSingleNode(NodeParentNodeID, TreeProvider.ALL_CULTURES);
            if (parentNode != null)
            {
                if (!DataClassInfoProvider.IsChildClassAllowed(ValidationHelper.GetInteger(parentNode.GetValue("NodeClassID"), 0), ci.ClassID))
                {
                    throw new Exception(ResHelper.GetString("Content.ChildClassNotAllowed"));
                }
            }

            // Check user permissions
            if (!RaiseOnCheckPermissions("Create", this))
            {
                if (!CMSContext.CurrentUser.IsAuthorizedToCreateNewDocument(parentNode, "CMS.File"))
                {
                    throw new Exception(string.Format(ResHelper.GetString("dialogs.newfile.notallowed"), NodeClassName));
                }
            }

            #endregion

            // Check the allowed extensions
            CheckAllowedExtensions();

            // Create new document
            string fileName = Path.GetFileNameWithoutExtension(ucFileUpload.FileName);
            string ext = Path.GetExtension(ucFileUpload.FileName);

            // Make sure the file name respects maximum file name limit porvided by the database column (100 characters)
            if (fileName.Length >= 100)
            {
                fileName = TextHelper.LimitLength(fileName, 90, "");
                fileName = fileName + ext;
            }

            node = new TreeNode("CMS.File", tree);
            node.DocumentCulture = CMSContext.PreferredCultureCode;
            node.DocumentName = fileName;

            // Load default values
            FormHelper.LoadDefaultValues(node);

            node.SetValue("FileDescription", "");
            node.SetValue("FileName", fileName);
            node.SetValue("FileAttachment", Guid.Empty);

            // Set default template ID                
            node.DocumentPageTemplateID = ci.ClassDefaultPageTemplateID;

            // Insert the document
            DocumentHelper.InsertDocument(node, parentNode.NodeID, tree);

            newDocumentCreated = true;

            // Add the file
            DocumentHelper.AddAttachment(node, "FileAttachment", ucFileUpload.PostedFile, tree, ResizeToWidth, ResizeToHeight, ResizeToMaxSideSize);

            // Create default SKU if configured
            if (ModuleEntry.CheckModuleLicense(ModuleEntry.ECOMMERCE, UrlHelper.GetCurrentDomain(), FeatureEnum.Ecommerce, VersionActionEnum.Insert))
            {
                node.CreateDefaultSKU();
            }

            // Update the document
            DocumentHelper.UpdateDocument(node, tree);
        }
        catch (Exception ex)
        {
            // Delete the document if something failed
            if (newDocumentCreated && (node != null) && (node.DocumentID > 0))
            {
                DocumentHelper.DeleteDocument(node, tree, false, true, true);
            }

            message = ex.Message;
        }
        finally
        {
            // Create node info string
            string nodeInfo = "";
            if ((node != null) && (node.NodeID > 0) && (IncludeNewItemInfo))
            {
                nodeInfo = node.NodeID.ToString();
            }

            // Ensure message text
            message = HTMLHelper.EnsureLineEnding(message, " ");

            // Call function to refresh parent window                                                     
            ltlScript.Text += ScriptHelper.GetScript("if ((window.parent != null) && (/parentelemid=" + ParentElemID + "/i.test(window.location.href)) && (window.parent.InitRefresh_" + ParentElemID + " != null)){window.parent.InitRefresh_" + ParentElemID + "(" + ScriptHelper.GetString(message.Trim()) + ", false" + ((nodeInfo != "") ? ", '" + nodeInfo + "'" : "") + (InsertMode ? ", 'insert'" : ", 'update'") + ");}");
        }
    }

    #endregion
}
