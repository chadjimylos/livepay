<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PasswordConfirmator.ascx.cs"
    Inherits="CMSModules_Membership_FormControls_Passwords_PasswordConfirmator" %>
<div>
    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="LogonTextBox"
        MaxLength="100"></asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvPassword" ValidationGroup="ConfirmRegForm" runat="server"
        ControlToValidate="txtPassword" Display="Dynamic"></asp:RequiredFieldValidator>
    <div class="ConfirmationSeparator">
    </div>
    <cms:LocalizedLabel ID="lblConfirmPassword" runat="server" ResourceString="general.confirmpassword" AssociatedControlID="txtConfirmPassword" EnableViewState="false" Display="false" />
    <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" CssClass="LogonTextBox"
        MaxLength="100"></asp:TextBox>
    <asp:RequiredFieldValidator ID="rfvConfirmPassword" ValidationGroup="ConfirmRegForm" runat="server"
        ControlToValidate="txtConfirmPassword" Display="Dynamic"></asp:RequiredFieldValidator>
</div>
