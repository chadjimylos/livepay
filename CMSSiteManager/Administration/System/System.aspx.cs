using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Diagnostics;
using System.IO;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.URLRewritingEngine;
using CMS.UIControls;
using CMS.EventLog;

public partial class CMSSiteManager_Administration_System_System : SiteManagerPage
{
    #region "Variables"

    private static DateTime mLastRPS = DateTime.MinValue;

    private static long mLastPageRequests = 0;
    private static long mLastNonPageRequests = 0;
    private static long mLastSystemPageRequests = 0;
    private static long mLastPageNotFoundRequests = 0;
    private static long mLastGetFileRequests = 0;

    private static double mRPSPageRequests = -1;
    private static double mRPSNonPageRequests = -1;
    private static double mRPSSystemPageRequests = -1;
    private static double mRPSPageNotFoundRequests = -1;
    private static double mRPSGetFileRequests = -1;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Reevaluate RPS
        if (mLastRPS != DateTime.MinValue)
        {
            double seconds = DateTime.Now.Subtract(mLastRPS).TotalSeconds;
            if ((seconds < 3) && (seconds > 0))
            {
                mRPSSystemPageRequests = (RequestHelper.TotalSystemPageRequests - mLastSystemPageRequests) / seconds;
                mRPSPageRequests = (RequestHelper.TotalPageRequests - mLastPageRequests) / seconds;
                mRPSPageNotFoundRequests = (RequestHelper.TotalPageNotFoundRequests - mLastPageNotFoundRequests) / seconds;
                mRPSNonPageRequests = (RequestHelper.TotalNonPageRequests - mLastNonPageRequests) / seconds;
                mRPSGetFileRequests = (RequestHelper.TotalGetFileRequests - mLastGetFileRequests) / seconds;
            }
            else
            {
                mRPSGetFileRequests = -1;
                mRPSNonPageRequests = -1;
                mRPSPageNotFoundRequests = -1;
                mRPSPageRequests = -1;
                mRPSSystemPageRequests = -1;
            }
        }

        mLastRPS = DateTime.Now;

        // Update last values
        mLastGetFileRequests = RequestHelper.TotalGetFileRequests;
        mLastNonPageRequests = RequestHelper.TotalNonPageRequests;
        mLastPageNotFoundRequests = RequestHelper.TotalPageNotFoundRequests;
        mLastPageRequests = RequestHelper.TotalPageRequests;
        mLastSystemPageRequests = RequestHelper.TotalSystemPageRequests;
        
        // Do not count this page with async postback
        if (RequestHelper.IsAsyncPostback())
        {
            RequestHelper.TotalSystemPageRequests--;
        }

        lblMachineName.Text = ResHelper.GetString("Administration-System.MachineName");
        lblMachineNameValue.Text = HTTPHelper.MachineName;

        lblAspAccount.Text = ResHelper.GetString("Administration-System.Account");
        lblValueAspAccount.Text = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

        lblMemory.Text = ResHelper.GetString("Administration-System.MemoryStatistics");
        
        lblAlocatedMemory.Text = ResHelper.GetString("Administration-System.Memory");
        lblPeakMemory.Text = ResHelper.GetString("Administration-System.PeakMemory");
        lblVirtualMemory.Text = ResHelper.GetString("Administration-System.VirtualMemory");
        lblPhysicalMemory.Text = ResHelper.GetString("Administration-System.PhysicalMemory");
        lblIP.Text = ResHelper.GetString("Administration-System.IP");

        lblPageViews.Text = ResHelper.GetString("Administration-System.PageViews");
        lblPageViewsValues.Text = ResHelper.GetString("Administration-System.PageViewsValues");

        lblPages.Text = ResHelper.GetString("Administration-System.Pages");
        lblSystemPages.Text = ResHelper.GetString("Administration-System.SystemPages");
        lblNonPages.Text = ResHelper.GetString("Administration-System.NonPages");
        lblGetFilePages.Text = ResHelper.GetString("Administration-System.GetFilePages");
        lblPagesNotFound.Text = ResHelper.GetString("Administration-System.PagesNotFound");

        lblCache.Text = ResHelper.GetString("Administration-System.CacheStatistics");
        lblCacheExpired.Text = ResHelper.GetString("Administration-System.CacheExpired");
        lblCacheRemoved.Text = ResHelper.GetString("Administration-System.CacheRemoved");
        lblCacheUnderused.Text = ResHelper.GetString("Administration-System.CacheUnderused");
        lblCacheItems.Text = ResHelper.GetString("Administration-System.CacheItems");
        lblCacheDependency.Text = ResHelper.GetString("Administration-System.CacheDependency");

        lblGC.Text = ResHelper.GetString("Administration-System.GC");

        btnClear.Text = ResHelper.GetString("Administration-System.btnClear");
        btnClearCache.Text = ResHelper.GetString("Administration-System.btnClearCache");
        btnRestart.Text = ResHelper.GetString("Administration-System.btnRestart");
        btnRestartWebfarm.Text = ResHelper.GetString("Administration-System.btnRestartWebfarm");

        if (!WebSyncHelperClass.WebFarmEnabled)
        {
            this.btnRestartWebfarm.Visible = false;
        }

        LoadData();

        if (!RequestHelper.IsPostBack())
        {
            bool isRestarted = QueryHelper.GetBoolean("IsRestarted", false);
            bool isWebfarmRestarted = QueryHelper.GetBoolean("IsWebfarmRestarted", false);
            if (isRestarted)
            {
                lblInfo.Text = ResHelper.GetString("Administration-System.RestartSuccess");
            }
            else if (isWebfarmRestarted)
            {
                lblInfo.Text = ResHelper.GetString("Administration-System.WebframRestarted");
            }
        }

        lblRunTime.Text = ResHelper.GetString("Administration.System.RunTime");

        // Remove miliseconds from the end of the time string
        string timeSpan = (DateTime.Now - Global.ApplicationStart).ToString();
        int index = timeSpan.LastIndexOf('.');
        if (index >= 0)
        {
            timeSpan = timeSpan.Remove(index);
        }

        // Display application run time
        lblRunTimeValue.Text = timeSpan;

        lblIPValue.Text = Request.UserHostAddress;
    }


    /// <summary>
    /// Loads the data
    /// </summary>
    protected void LoadData()
    {
        lblValueAlocatedMemory.Text = DataHelper.GetSizeString(GC.GetTotalMemory(false));

        lblValueVirtualMemory.Text = "N/A";
        lblValuePhysicalMemory.Text = "N/A";
        lblValuePeakMemory.Text = "N/A";

        // Process memory
        try
        {
            ProcessInfo pr = ProcessModelInfo.GetCurrentProcessInfo();
            lblValuePeakMemory.Text = DataHelper.GetSizeString(pr.PeakMemoryUsed * 1024);

            lblValueVirtualMemory.Text = DataHelper.GetSizeString(SystemHelper.GetVirtualMemorySize());
            lblValuePhysicalMemory.Text = DataHelper.GetSizeString(SystemHelper.GetWorkingSetSize());
            lblValuePeakMemory.Text = DataHelper.GetSizeString(SystemHelper.GetPeakWorkingSetSize());
        }
        catch
        {
        }

        this.lblValuePages.Text = GetViewValues(RequestHelper.TotalPageRequests, 0, mRPSPageRequests);
        this.lblValuePagesNotFound.Text = GetViewValues(RequestHelper.TotalPageNotFoundRequests, 0, mRPSPageNotFoundRequests);
        this.lblValueSystemPages.Text = GetViewValues(RequestHelper.TotalSystemPageRequests, 0, mRPSSystemPageRequests); ;
        this.lblValueNonPages.Text = GetViewValues(RequestHelper.TotalNonPageRequests, 0, mRPSNonPageRequests);
        this.lblValueGetFilePages.Text = GetViewValues(RequestHelper.TotalGetFileRequests, 0, mRPSGetFileRequests);

        this.lblCacheItemsValue.Text = Cache.Count.ToString();
        this.lblCacheExpiredValue.Text = CacheHelper.Expired.ToString();
        this.lblCacheRemovedValue.Text = CacheHelper.Removed.ToString();
        this.lblCacheUnderusedValue.Text = CacheHelper.Underused.ToString();
        this.lblCacheDependencyValue.Text = CacheHelper.DependencyChanged.ToString();
        
        // GC collections
        try
        {
            this.plcGC.Controls.Clear();

            int generations = GC.MaxGeneration;
            for (int i = 0; i <= generations; i++)
            {
                int count = GC.CollectionCount(i);
                string genString = "<tr><td style=\"white-space: nowrap; width: 200px;\">" + ResHelper.GetString("GC.Generation") + " " + i.ToString() + ":</td><td>" + count.ToString() + "</td></tr>";

                this.plcGC.Controls.Add(new LiteralControl(genString));
            }
        }
        catch
        {
        }
    }


    /// <summary>
    /// Gets the values string for the page views
    /// </summary>
    private string GetViewValues(long total, long async, double rps)
    {
        return total.ToString() /*+ " / " + async.ToString() + " / "*/ + ((rps >= 1) ? " (" + Math.Floor(rps).ToString() + " RPS)" : "");
    }


    /// <summary>
    /// Clear unused memory
    /// </summary>
    protected void btnClear_Click(object sender, EventArgs e)
    {
        // Collect the memory
        GC.Collect();
        GC.WaitForPendingFinalizers();

        // Log event
        EventLogProvider eventLog = new EventLogProvider();
        eventLog.LogEvent("I", DateTime.Now, "System", "CLEARMEM", null, ResHelper.GetString("Administration-System.ClearSuccess"));
        
        lblInfo.Text = ResHelper.GetString("Administration-System.ClearSuccess");

        LoadData();
    }


    /// <summary>
    /// Restart application
    /// </summary>
    protected void btnRestart_Click(object sender, EventArgs e)
    {
        bool restarted = true;

        try
        {
            // Try to restart applicatin by unload app domain
            HttpRuntime.UnloadAppDomain();
        }
        catch
        {
            try
            {
                // Try to restart application by changing web.config file
                File.SetLastWriteTimeUtc(Request.PhysicalApplicationPath + "\\web.config", DateTime.UtcNow);
            }
            catch
            {
                restarted = false;
            }
        }

        if (restarted)
        {
            // Log event
            EventLogProvider eventLog = new EventLogProvider();
            eventLog.LogEvent("I", DateTime.Now, "System", "ENDAPP", null, ResHelper.GetString("Administration-System.RestartSuccess"));
            
            string url = UrlHelper.AddParameterToUrl(URLRewriter.CurrentURL, "IsRestarted", "1");
            UrlHelper.Redirect(url);
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("System.Restart.Failed");
        }
    }


    /// <summary>
    /// Restart webfarm
    /// </summary>
    protected void btnRestartWebfarm_Click(object sender, EventArgs e)
    {
        // Create webfarm task for application restart
        WebSyncHelperClass.CreateTask(WebFarmTaskTypeEnum.RestartApplication, "RestartApplication", "", null);

        // Restart current server
        HttpRuntime.UnloadAppDomain();

        // Log event
        EventLogProvider eventLog = new EventLogProvider();
        eventLog.LogEvent("I", DateTime.Now, "System", "RESTARTWEBFARM", null, ResHelper.GetString("Administration-System.WebframRestarted")); 

        string url = UrlHelper.AddParameterToUrl(URLRewriter.CurrentURL, "IsWebfarmRestarted", "1");
        UrlHelper.Redirect(url);
    }


    protected void btnClearCache_Click(object sender, EventArgs e)
    {
        // Clear the cache
        CacheHelper.ClearCache(null, true);
        Functions.ClearHashtables();

        // Collect the memory
        GC.Collect();
        GC.WaitForPendingFinalizers();

        // Log event
        EventLogProvider eventLog = new EventLogProvider();
        eventLog.LogEvent("I", DateTime.Now, "System", "CLEARCACHE", null, ResHelper.GetString("Administration-System.ClearCacheSuccess"));
        
        lblInfo.Text = ResHelper.GetString("Administration-System.ClearCacheSuccess");

        LoadData();
    }
}
