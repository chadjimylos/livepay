using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Cultures_Culture_List : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Administration-Culture_List.Title");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Culture/object.png");
        this.CurrentMaster.Title.HelpTopicName = "cultures_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Administration-Culture_List.NewCulture");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("Culture_New.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/CMS_Culture/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        UniGridCultures.OnAction += new OnActionEventHandler(UniGridCultures_OnAction);
        UniGridCultures.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that threw event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGridCultures_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Culture_Edit_Frameset.aspx?cultureID=" + actionArgument.ToString());
        }
        else if (actionName == "delete")
        {
            string cultureCode = CultureInfoProvider.GetCultureInfo(Convert.ToInt32(actionArgument)).CultureCode;
            DataSet ds = CultureInfoProvider.GetCultureSites(cultureCode);
            if (DataHelper.DataSourceIsEmpty(ds))
            {
                CultureInfoProvider.DeleteCultureInfo(cultureCode);
            }
            else
            {
                lblError.Visible = true;
                lblError.Text += ResHelper.GetString("culture_list.errorremoveculture") + "\n";
                return;
            }
        }
    }
}
