CREATE TABLE [CMS_VersionAttachment] (
		[VersionHistoryID]        int NOT NULL,
		[AttachmentHistoryID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_VersionAttachment]
	ADD
	CONSTRAINT [PK_CMS_VersionAttachment]
	PRIMARY KEY
	([VersionHistoryID], [AttachmentHistoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_VersionAttachment]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_AttachmentHistory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_VersionAttachment_AttachmentHistoryID_CMS_AttachmentHistory]') IS NULL
BEGIN
		ALTER TABLE [CMS_VersionAttachment]
			ADD CONSTRAINT [FK_CMS_VersionAttachment_AttachmentHistoryID_CMS_AttachmentHistory]
			FOREIGN KEY ([AttachmentHistoryID]) REFERENCES [CMS_AttachmentHistory] ([AttachmentHistoryID])
END
IF OBJECT_ID(N'[CMS_VersionAttachment]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_VersionHistory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_VersionAttachment_VersionHistoryID_CMS_VersionHistory]') IS NULL
BEGIN
		ALTER TABLE [CMS_VersionAttachment]
			ADD CONSTRAINT [FK_CMS_VersionAttachment_VersionHistoryID_CMS_VersionHistory]
			FOREIGN KEY ([VersionHistoryID]) REFERENCES [CMS_VersionHistory] ([VersionHistoryID])
END
