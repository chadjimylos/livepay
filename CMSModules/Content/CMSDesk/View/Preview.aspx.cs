using System;

using CMS.TreeEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.WorkflowEngine;
using CMS.PortalEngine;
using CMS.UIControls;
using CMS.SiteProvider;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_View_Preview : CMSContentPage
{
    protected string viewpage = "../blank.htm";
    protected string headersize = "43";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Switch the view mode
        CMSContext.ViewMode = ViewModeEnum.Preview;

        // Get the document
        int nodeId = 0;
        if (Request.QueryString["nodeid"] != null)
        {
            nodeId = ValidationHelper.GetInteger(Request.QueryString["nodeid"], 0);
        }

        // Get the document
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        TreeNode node = tree.SelectSingleNode(nodeId);

        // Redirect to the live URL
        if (node != null)
        {
            // If no workflow defined, hide the menu bar
            WorkflowManager wm = new WorkflowManager(tree);
            WorkflowInfo wi = wm.GetNodeWorkflow(node);
            if (wi == null)
            {
                headersize = "0";
            }
            else
            {
                // Get current step info
                WorkflowStepInfo si = wm.GetStepInfo(node);
                if (si != null)
                {
                    switch (si.StepName.ToLower())
                    {
                        case "published":
                        case "archived":
                            headersize = "0";
                            break;
                    }
                }
            }

            // Check the document availability
            string siteName = CMSContext.CurrentSiteName;
            if (!node.DocumentCulture.Equals(CMSContext.PreferredCultureCode, StringComparison.InvariantCultureIgnoreCase) && (!SiteInfoProvider.CombineWithDefaultCulture(siteName) || !node.DocumentCulture.Equals(CultureHelper.GetDefaultCulture(siteName), StringComparison.InvariantCultureIgnoreCase)))
            {
                viewpage = "~/CMSMessages/PageNotAvailable.aspx?reason=missingculture";
            }
            else
            {
                // Use permanent URL to get proper preview mode
                viewpage = TreePathUtils.GetPermanentDocUrl(node.NodeGUID, node.NodeAlias, CMSContext.CurrentSiteName, PageInfoProvider.PREFIX_CMS_GETDOC, ".aspx");
            }
        }
        else
        {
            viewpage = "~/CMSMessages/PageNotAvailable.aspx";
        }

        viewpage = ResolveUrl(viewpage);
    }
}
