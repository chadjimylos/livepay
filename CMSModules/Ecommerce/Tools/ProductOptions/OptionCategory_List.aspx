<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OptionCategory_List.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_ProductOptions_OptionCategory_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Option Categories - List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
    <cms:UniGrid runat="server" ID="OptionCategoryGrid" GridName="OptionCategory_List.xml"
        IsLiveSite="false" Columns="CategoryID, CategoryDisplayName, CategorySelectionType, CategoryEnabled" />
</asp:Content>
