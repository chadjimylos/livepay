CREATE VIEW [View_CMS_LayoutMetafile_Joined]
AS
SELECT LayoutID, LayoutCodeName, LayoutDisplayName, LayoutDescription, MetaFileGuid
FROM CMS_Layout 
LEFT OUTER JOIN (
 SELECT MetaFileGUID, MetaFileObjectID FROM CMS_MetaFile
 WHERE (MetaFileObjectType = 'cms.layout')
) AS LayoutFiles 
ON CMS_Layout.LayoutID = LayoutFiles.MetaFileObjectID
                      
