using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.ExtendedControls;
using CMS.UIControls;
using CMS.CMSHelper;
using CMS.PortalEngine;

public partial class CMSAdminControls_FCKeditor_editor_plugins_InsertRating_fck_InsertRating : CMSPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetBrowserClass();
        bodyElem.Attributes["class"] = mBodyClass;

        // Prepare dataset
        DataSet ds = new DataSet();
        ds.Tables.Add();
        ds.Tables[0].Columns.Add("UserControlDisplayName");
        ds.Tables[0].Columns.Add("UserControlCodeName");

        // Get file names of rating controls
        string[] fileList = Directory.GetFiles(Server.MapPath(AbstractRatingControl.GetRatingControlUrl("")), "*.ascx");
        string fileName = null;
        foreach (string file in fileList)
        {
            fileName = Path.GetFileNameWithoutExtension(file);
            // Initialize dataset
            ds.Tables[0].Rows.Add(ResHelper.GetString("contentrating." + fileName), fileName);
        }

        // Initialize grid
        gridForms.Columns[0].HeaderText = "<strong>" + ResHelper.GetString("SelectRatingDialog.FormName") + "</strong>";
        gridForms.DataSource = ds;
        gridForms.DataBind();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
        {
            // Register custom css if exists
            RegisterDialogCSSLink();
        }
    }
}
