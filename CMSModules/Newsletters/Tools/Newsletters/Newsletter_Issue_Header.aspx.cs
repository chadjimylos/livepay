using System;

using CMS.Controls;
using CMS.Newsletter;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_Header : CMSNewsletterPage
{
    protected int newsletterIssueId;
    protected Issue issue;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set the page title
        CurrentMaster.Title.TitleText = ResHelper.GetString("Newsletter_Issue_List.HeaderCaption");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Newsletter_Issue/object.png");
        CurrentMaster.Title.HelpTopicName = "edit_tab";
        CurrentMaster.Title.HelpName = "helpTopic";
        
        // Get newsletter issue ID from querystring
        newsletterIssueId = QueryHelper.GetInteger("issueid", 0);
        Issue issue = IssueProvider.GetIssue(newsletterIssueId);
        if (issue == null)
        {
            return;
        }
        
        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu(issue);
        }
    }


    /// <summary>
    /// Initializes newsletter menu
    /// </summary>
    /// <param name="issue">Issue object</param>
    protected void InitalizeMenu(Issue issue)
    {
        Newsletter news = NewsletterProvider.GetNewsletter(issue.IssueNewsletterID);
        if (news == null)
        {
        	return;
        }

        string[,] tabs = null;
        if (news.NewsletterType == "D")
        {
            tabs = BasicTabControl.GetTabsArray(1);
            tabs[0, 0] = ResHelper.GetString("Newsletter_Issue_Header.Send");
            tabs[0, 2] = "Newsletter_Issue_Send.aspx?issueid=" + newsletterIssueId;
        }
        else
        {
            tabs = BasicTabControl.GetTabsArray(3);
            if (CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "authorissues"))
            {
                tabs[0, 0] = ResHelper.GetString("General.Edit");
                tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'edit_tab');";
                tabs[0, 2] = "Newsletter_Issue_Edit.aspx?issueid=" + newsletterIssueId;
                tabs[1, 0] = ResHelper.GetString("Newsletter_Issue_Header.Send");
                tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'send_tab');";
                tabs[1, 2] = "Newsletter_Issue_Send.aspx?issueid=" + newsletterIssueId;
            }
                
            tabs[2, 0] = ResHelper.GetString("Newsletter_Issue_Header.Preview");
            tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'preview_tab');";
            tabs[2, 2] = "Newsletter_Issue_Preview.aspx?issueid=" + newsletterIssueId;
        }

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "newsletterIssueContent";
    }
}
