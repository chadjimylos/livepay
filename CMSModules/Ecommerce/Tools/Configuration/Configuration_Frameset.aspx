<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Configuration_Frameset.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_Configuration_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>E-commerce - Configuration</title>
</head>
<frameset border="0" cols="126, 6, *" frameborder="0" runat="server" id="colsFrameset">
		<frame name="configHeader" src="Configuration_Header.aspx" scrolling="no" noresize="noresize" frameborder="0" runat="server" id="configHeader" />		
   		<frame name="storesettingsBorder" src="../../../../CMSAdminControls/TabsFrameset/border.aspx " scrolling="no" frameborder="0" noresize="noresize" />		
		<frame name="configEdit" src="StoreSettings/StoreSettings_Frameset.aspx"  noresize="noresize" frameborder="0" runat="server" id="configEdit" scrolling="auto" />		
    	<noframes>
			<p id="p1">
				This HTML frameset displays multiple Web pages. To view this frameset, use a 
				Web browser that supports HTML 4.0 and later.
			</p>
		</noframes>
	</frameset>
</html>
