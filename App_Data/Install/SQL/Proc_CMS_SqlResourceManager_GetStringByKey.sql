CREATE PROCEDURE [Proc_CMS_SqlResourceManager_GetStringByKey]
	@stringKey nvarchar(200),
	@cultureCode nvarchar(50)
AS
SELECT [TranslationText] FROM CMS_ResourceTranslation 
WHERE TranslationUIcultureID = 
(SELECT [UICultureID] FROM [CMS_UICulture] WHERE UICultureCode = @cultureCode)  
AND
TranslationStringId = (SELECT TOP 1 [StringId] FROM [CMS_ResourceString] WHERE StringKey = @stringKey)
