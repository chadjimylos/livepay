using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing.Imaging;

using CMS.CaptchaImage;
using CMS.GlobalHelper;

public partial class CMSPages_Dialogs_CaptchaImage : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string capcha = QueryHelper.GetString("capcha", "");
        if (WindowHelper.GetItem("CaptchaImageText" + capcha) != null)
        {
            // Create a CAPTCHA image using the text stored in the Session object.
            CaptchaImage ci = new CaptchaImage(WindowHelper.GetItem("CaptchaImageText" + capcha).ToString(), 80, 20, null);

            // Change the response headers to output a JPEG image.
            this.Response.Clear();
            this.Response.ContentType = "image/jpeg";
            this.Response.Cache.SetCacheability(HttpCacheability.NoCache);

            // Write the image to the response stream in JPEG format.
            ci.Image.Save(this.Response.OutputStream, ImageFormat.Jpeg);

            // Dispose of the CAPTCHA image object.
            ci.Dispose();

            RequestHelper.EndResponse();
        }
    }
}
