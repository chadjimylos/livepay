using System;
using System.Web.UI;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.MembershipProvider;

public partial class CMSDesk_Header : CMSDeskPage
{
    #region "Variables"

    private int selectedTabIndex = 0;
    private string section = null;
    private bool exploreTreePermissionMissing = false;

    #endregion


    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        this["TabControl"] = BasicTabControlHeader;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Facebook Connect sign out
        if (CMSContext.CurrentUser.IsAuthenticated())
        {
            if (QueryHelper.GetInteger("logout", 0) > 0)
            {
                btnSignOut_Click(this, EventArgs.Empty);
                return;
            }
        }

        lblVersion.Text = "v" + CMSContext.FullSystemSuffixVersion;

        // Make 'Site manager' link visible for global administrators
        CurrentUserInfo ui = CMSContext.CurrentUser;
        if ((ui != null) && (ui.UserSettings != null))
        {
            lnkSiteManager.Visible = (ui.IsGlobalAdministrator && !ui.UserSiteManagerDisabled);
        }

        // Site selector settings
        siteSelector.DropDownSingleSelect.CssClass = "HeaderSiteDrop";
        siteSelector.UpdatePanel.RenderMode = UpdatePanelRenderMode.Inline;
        siteSelector.AllowAll = false;
        siteSelector.UniSelector.OnSelectionChanged += SiteSelector_OnSelectionChanged;
        siteSelector.DropDownSingleSelect.AutoPostBack = true;
        siteSelector.OnlyRunningSites = true;

        if (!RequestHelper.IsPostBack())
        {
            siteSelector.Value = CMSContext.CurrentSiteID;
        }

        // Show only assigned sites for not global admins
        if (!CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            siteSelector.UserId = CMSContext.CurrentUser.UserID;
        }

        section = QueryHelper.GetString("section", string.Empty).ToLower();

        pnlSignOut.BackImageUrl = GetImageUrl("Design/Buttons/SignOutButton.png");
        lblSignOut.Text = ResHelper.GetString("signoutbutton.signout");

        lblUser.Text = ResHelper.GetString("Header.User");
        lblUserInfo.Text = HTMLHelper.HTMLEncode(CMSContext.CurrentUser.FullName);


        lnkSiteManager.Text = ResHelper.GetString("Header.SiteManager");
        lnkSiteManager.NavigateUrl = "~/CMSSiteManager/default.aspx";
        lnkSiteManager.Target = "_parent";

        lnkSiteManagerLogo.NavigateUrl = "~/CMSDesk/default.aspx";
        lnkSiteManagerLogo.Target = "_parent";

        BasicTabControlHeader.OnTabCreated += tabElem_OnTabCreated;
        BasicTabControlHeader.UrlTarget = "cmsdesktop";

        // Init Facebook Connect and join logout script to sign out button
        string logoutScript = FacebookConnectHelper.FacebookConnectInitForSignOut(CMSContext.CurrentSiteName, ltlFBConnectScript);
        if (!String.IsNullOrEmpty(logoutScript))
        {
            // If Facebook Connect initialized include 'CheckChanges()' to logout script
            logoutScript = "if (CheckChanges()) { " + logoutScript + " } return false; ";
        }
        else
        {
            // If Facebook Connect not initialized just return 'CheckChanges()' script
            logoutScript = "return CheckChanges();";
        }
        lnkSignOut.OnClientClick = logoutScript;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        BasicTabControlHeader.SelectedTab = selectedTabIndex;

        // Call the script for tab which is selected
        if (!BasicTabControlHeader.TabsEmpty)
        {
            ltlScript.Text += ScriptHelper.GetScript(" redir('" + BasicTabControlHeader.Tabs[selectedTabIndex, 2] + Request.Url.Query + "','" + BasicTabControlHeader.UrlTarget + "'); " + BasicTabControlHeader.Tabs[selectedTabIndex, 1]);
        }
        // "Explore tree" permission is required
        else if (exploreTreePermissionMissing)
        {
            ltlScript.Text = ScriptHelper.GetScript(" redir('" + UrlHelper.ResolveUrl("~/CMSDesk/accessdenied.aspx") + "?resource=CMS.Content&permission=ExploreTree','" + BasicTabControlHeader.UrlTarget + "');");
        }
        // No tab is visible
        else
        {
            ltlScript.Text += ScriptHelper.GetScript(" redir('" + UrlHelper.ResolveUrl("~/CMSMessages/Information.aspx") + "?message=" + ResHelper.GetString("uiprofile.uinotavailable") + "','" + BasicTabControlHeader.UrlTarget + "'); ");
        }
    }


    protected string[] tabElem_OnTabCreated(UIElementInfo element, string[] parameters, int tabIndex)
    {
        // Ensure additional permissions to 'Content' tab
        if (element.ElementName.ToLower() == "content")
        {
            if (!IsUserAuthorizedPerContent())
            {
                exploreTreePermissionMissing = true;
                return null;
            }
        }

        if (element.ElementName.ToLower() == section)
        {
            selectedTabIndex = tabIndex;
        }
        return parameters;
    }


    protected void SiteSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        // Create url
        int siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
        SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);
        if (si != null)
        {
            string domain = si.DomainName.TrimEnd('/');
            string url = string.Empty;
            if (domain.Contains("/"))
            {
                // Application path is part of domain name
                url = UrlHelper.GetAbsoluteUrl("/cmsdesk", domain, null, null);
            }
            else
            {
                // Resolve application path
                url = UrlHelper.GetAbsoluteUrl("~/cmsdesk", domain, null, null);
            }

            ScriptHelper.RegisterStartupScript(Page, typeof(Page), "selectSite", ScriptHelper.GetScript("SiteRedirect('" + url + "');"));
        }
    }


    protected void btnSignOut_Click(object sender, EventArgs e)
    {
        // Usual sign out
        string signOutUrl = Request.ApplicationPath.TrimEnd('/') + "/default.aspx";
        // LiveID sign out URL is set if this LiveID session
        SignOut(ref signOutUrl);

        ltlScript.Text += ScriptHelper.GetScript("parent.location.replace('" + signOutUrl + "');");
    }
}
