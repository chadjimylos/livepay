<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Analytics_Days.aspx.cs" Inherits="CMSModules_WebAnalytics_Tools_Analytics_Days"
    Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>

<%@ Register Src="~/CMSAdminControls/UI/HTMLGraph.ascx" TagName="HTMLGraph" TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Analytics - Days</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
            overflow: hidden;
        }
    </style>

    <script type="text/javascript">
        //<![CDATA[
        function ucGraphDays_ShowDesktopContent(contentUrl, contentUrl2) {
            parent.frames['analreport'].location.href = contentUrl;
        }

        function ucGraphDays_OnSelectBar(sender) {
            parent.frames['analtree'].SetSelectedBar('hdnDaysSelectedBar', sender.id); ;
        }
        //]]>
    </script>

</head>
<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="pnlTitle" CssClass="PageHeader">
        <cms:PageTitle ID="PageTitle" runat="server" />
    </asp:Panel>
    <div style="padding: 5px 0px 0px 0px;" class="PageHeaderLine">
        <cms:HTMLGraph ID="ucGraphDays" runat="server" />
        <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    </div>
    </form>
</body>
</html>
