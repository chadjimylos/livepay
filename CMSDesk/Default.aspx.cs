using System;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSDesk_Default : CMSDeskPage
{
    protected string desktoppage;
    //protected string trialHeight;
    //protected string trialExpires;
    protected string trialPageURL;
    protected string headerPageURL;

    protected void Page_Load(object sender, EventArgs e)
    {

        // Check the license key for trial version
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), string.Empty) != string.Empty)
        {
            LicenseKeyInfo lki = LicenseKeyInfoProvider.GetLicenseKeyInfo(UrlHelper.GetCurrentDomain());
            if (lki != null)
            {
                // Check the number of users for free edition
                if (lki.Edition == ProductEditionEnum.Free)
                {
                    UserInfoProvider.LicenseController();
                }
            }
        }

        trialPageURL = ResolveUrl("~/CMSSiteManager/trialversion.aspx");
        headerPageURL = ResolveUrl("~/CMSDesk/Header.aspx");

        // Load particular action
        string section = QueryHelper.GetString("section", "content").ToLower();
        switch (section)
        {
            case "content":
                if (!IsUserAuthorizedPerContent())
                {
                    desktoppage = ResolveUrl("~/CMSDesk/MyDesk/default.aspx");
                }
                // Else will be solved by UI profiles
                break;

            case "mydesk":
                desktoppage = ResolveUrl("~/CMSDesk/MyDesk/default.aspx");
                break;

            case "tools":
                desktoppage = ResolveUrl("~/CMSDesk/Tools/default.aspx");
                break;

            case "administration":
                desktoppage = ResolveUrl("~/CMSDesk/Administration/default.aspx");
                break;
        }


        desktoppage += Request.Url.Query;
    }
}
