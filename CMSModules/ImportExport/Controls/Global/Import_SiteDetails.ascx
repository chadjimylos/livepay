<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Import_SiteDetails.ascx.cs"
    Inherits="CMSModules_ImportExport_Controls_Global_Import_SiteDetails" %>
<table>
    <tr>
        <td nowrap="nowrap">
            <asp:Label ID="lblSiteDisplayName" runat="server" />
        </td>
        <td>
            <asp:TextBox ID="txtSiteDisplayName" runat="server" />
            <asp:RequiredFieldValidator ID="rfvSiteDisplayName" runat="server" ControlToValidate="txtSiteDisplayName" />
        </td>
    </tr>
    <tr>
        <td nowrap="nowrap">
            <asp:Label ID="lblSiteName" runat="server" />
        </td>
        <td>
            <asp:TextBox ID="txtSiteName" runat="server" />
            <asp:RequiredFieldValidator ID="rfvSiteName" runat="server" ControlToValidate="txtSiteName" />
        </td>
    </tr>
</table>
