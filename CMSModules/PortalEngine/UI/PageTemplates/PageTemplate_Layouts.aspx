<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageTemplate_Layouts.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_Layouts" Theme="Default"
    ValidateRequest="false" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Page template - Layouts" %>
    <%@ Register src="~/CMSModules/PortalEngine/FormControls/PageLayouts/PageLayoutSelector.ascx" tagname="layoutSelector" tagprefix="cms" %>

<asp:content id="cntBody" runat="server" contentplaceholderid="plcContent">
    <asp:PlaceHolder ID="plcLayout" runat="server">
        <asp:Label runat="server" ID="lblAspxInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Panel ID="pnlCheckOutInfo" runat="server" CssClass="InfoLabel" Visible="false">
            <asp:Label runat="server" ID="lblCheckOutInfo" />
            <br />
        </asp:Panel>
        <asp:PlaceHolder ID="plcContent" runat="server">
            <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
                Visible="false" />
            <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
                Visible="false" />
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td style="height: 20px">
                        <asp:RadioButton runat="server" ID="radShared" GroupName="LayoutGroup" AutoPostBack="True"
                            OnCheckedChanged="radShared_CheckedChanged" />
                    </td>
                </tr>
                <tr>
                    <td>                        
                         <cms:LayoutSelector runat="server" ID="selectShared" IsLiveSite="false" OnChanged="selectShared_Changed" />   
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButton runat="server" ID="radCustom" GroupName="LayoutGroup" AutoPostBack="True"
                            OnCheckedChanged="radShared_CheckedChanged" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:PlaceHolder runat="server" ID="plcVirtualInfo" Visible="false">
                            <br />
                            <asp:Label runat="server" ID="lblVirtualInfo" CssClass="ErrorLabel" EnableViewState="false" />
                        </asp:PlaceHolder>
                        <cms:ExtendedTextArea ID="txtCustom" runat="server" CssClass="TextAreaCode" TextMode="MultiLine"
                            Width="98%" /><br />
                    </td>
                </tr>
            </table>
        </asp:PlaceHolder>
    </asp:PlaceHolder>
</asp:content>
