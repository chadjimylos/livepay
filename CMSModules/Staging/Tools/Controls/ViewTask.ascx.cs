using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Collections.Generic;
using System.Xml;

using CMS.GlobalHelper;
using CMS.Staging;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_Staging_Tools_Controls_ViewTask : CMSAdminEditControl
{
    #region "Variables"

    private int mTaskId = 0;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets the ID of the task
    /// </summary>
    public int TaskId
    {
        get
        {
            return mTaskId;
        }
        set
        {
            mTaskId = value;
        }
    }

    #endregion"


    protected void Page_Load(object sender, EventArgs e)
    {
        TaskInfo ti = TaskInfoProvider.GetTaskInfo(TaskId);
        if (ti != null)
        {
            DataSet ds = GetDataSet(ti.TaskData, TaskHelper.GetTaskTypeString(ti.TaskType), ti.TaskObjectType);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                // Prepare list of tables
                SortedDictionary<string, string> tables = new SortedDictionary<string, string>();
                foreach (DataTable dt in ds.Tables)
                {
                    if (!DataHelper.DataSourceIsEmpty(dt))
                    {
                        tables.Add(ResHelper.GetString("ObjectType." + dt.TableName), dt.TableName);
                    }
                }

                // Prepare the translations for the ID information
                //TranslationHelper th = new TranslationHelper(ds.Tables["ObjectTranslation"], null);

                // Generate the tables
                foreach (string tableName in tables.Values)
                {
                    DataTable dt = ds.Tables[tableName];
                    pnlContent.Controls.Add(new LiteralControl("<h3>" + ResHelper.GetString("ObjectType." + tableName) + "</h3>"));

                    if ((dt.Columns.Count >= 6) && !tableName.Equals("ObjectTranslation", StringComparison.InvariantCultureIgnoreCase))
                    {
                        // Write all rows
                        foreach (DataRow dr in dt.Rows)
                        {
                            StringBuilder sb = new StringBuilder();

                            sb.Append("<table class=\"UniGridGrid\" cellspacing=\"0\" cellpadding=\"3\" rules=\"rows\" border=\"1\" style=\"border-collapse:collapse;\" width=\"100%\">");

                            // Add header
                            sb.Append("<tr class=\"UniGridHead\"><th>" + ResHelper.GetString("General.FieldName") + "</th><th style=\"width: 100%;\">" + ResHelper.GetString("General.Value") + "</th></tr>");

                            // Add values
                            int i = 0;
                            foreach (DataColumn dc in dt.Columns)
                            {
                                object value = dr[dc.ColumnName];
                                if (!DataHelper.IsEmpty(value))
                                {
                                    ++i;
                                    string className = ((i % 2) == 0) ? "OddRow" : "EvenRow";
                                    sb.Append("<tr class=\"" + className + "\"><td>");
                                    sb.Append("<strong>" + dc.ColumnName + "</strong>");
                                    sb.Append("</td><td style=\"width: 100%;\">");

                                    string content = null;

                                    // Binary columns
                                    if ((dc.DataType == typeof(byte[])) && (value != DBNull.Value))
                                    {
                                        byte[] data = (byte[])dr[dc.ColumnName];
                                        content = "<" + ResHelper.GetString("General.BinaryData") + ", " + DataHelper.GetSizeString(data.Length) + ">";
                                    }
                                    else
                                    {
                                        content = ValidationHelper.GetString(value, "");
                                    }

                                    // Possible DataTime columns
                                    if ((dc.DataType == typeof(DateTime)) && (value != DBNull.Value))
                                    {
                                        DateTime dateTime = Convert.ToDateTime(content);
                                        System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(CMSContext.CurrentUser.PreferredUICultureCode);
                                        content = dateTime.ToString(cultureInfo);
                                    }

                                    bool standard = true;
                                    switch (dc.ColumnName.ToLower())
                                    {
                                        // Document content
                                        case "documentcontent":
                                            EditableItems items = new EditableItems();
                                            items.LoadContentXml(ValidationHelper.GetString(value, ""));

                                            // Add regions
                                            foreach (DictionaryEntry region in items.EditableRegions)
                                            {
                                                sb.Append("<span class=\"VersionEditableRegionTitle\">" + (string)region.Key + "</span>");

                                                string regionContent = HTMLHelper.ResolveUrls((string)region.Value, HttpContext.Current.Request.ApplicationPath);

                                                sb.Append("<span class=\"VersionEditableRegionText\">" + regionContent + "</span>");
                                            }

                                            // Add web parts
                                            foreach (DictionaryEntry part in items.EditableWebParts)
                                            {
                                                sb.Append("<span class=\"VersionEditableWebPartTitle\">" + (string)part.Key + "</span>");

                                                string regionContent = HTMLHelper.ResolveUrls((string)part.Value, HttpContext.Current.Request.ApplicationPath);
                                                sb.Append("<span class=\"VersionEditableWebPartText\">" + regionContent + "</span>");
                                            }

                                            standard = false;
                                            break;

                                        // XML columns
                                        case "pagetemplatewebparts":
                                        case "webpartproperties":
                                        case "reportparameters":
                                        case "classformdefinition":
                                        case "classxmlschema":
                                        case "classformlayout":
                                        case "userdialogsconfiguration":
                                        case "siteinvoicetemplate":
                                        case "userlastlogoninfo":
                                        case "formdefinition":
                                        case "formlayout":
                                        case "uservisibility":
                                            content = HTMLHelper.ReformatHTML(content);
                                            break;

                                        // File columns
                                        case "metafilename":
                                            {
                                                Guid metaFileGuid = ValidationHelper.GetGuid(dr["MetaFileGuid"], Guid.Empty);
                                                if (metaFileGuid != Guid.Empty)
                                                {
                                                    string metaFileName = ValidationHelper.GetString(dr["MetaFileName"], "");

                                                    content = "<a href=\"" + ResolveUrl(MetaFileInfoProvider.GetMetaFileUrl(metaFileGuid, metaFileName)) + "\" target=\"_blank\" >" + HTMLHelper.HTMLEncode(metaFileName) + "</a>";
                                                    sb.Append(content);

                                                    standard = false;
                                                }
                                            }
                                            break;
                                    }

                                    // Standard rendering
                                    if (standard)
                                    {
                                        content = HTMLHelper.HTMLEncode(content);
                                        content = TextHelper.EnsureLineEndings(content, "<br />");
                                        content = content.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
                                        sb.Append("<div style=\"max-height: 300px; overflow: auto;\">" + content + "</div>");
                                    }

                                    sb.Append("</td></tr>");
                                }
                            }

                            sb.Append("</table><br />\n");

                            pnlContent.Controls.Add(new LiteralControl(sb.ToString()));
                        }
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();

                        sb.Append("<table class=\"UniGridGrid\" cellspacing=\"0\" cellpadding=\"3\" rules=\"rows\" border=\"1\" style=\"border-collapse:collapse;\" width=\"100%\">");

                        // Add header
                        sb.Append("<tr class=\"UniGridHead\">");
                        int h = 1;
                        foreach (DataColumn column in dt.Columns)
                        {
                            string style = null;
                            if (h == dt.Columns.Count)
                            {
                                style = "style=\"width:100%;\"";
                            }
                            sb.Append("<th " + style + " >" + column.ColumnName + "</th>");
                            h++;
                        }
                        sb.Append("</tr>");

                        // Write all rows
                        int i = 0;
                        foreach (DataRow dr in dt.Rows)
                        {
                            ++i;
                            string className = ((i % 2) == 0) ? "OddRow" : "EvenRow";
                            sb.Append("<tr class=\"" + className + "\">");

                            // Add values
                            foreach (DataColumn dc in dt.Columns)
                            {
                                object value = dr[dc.ColumnName];
                                // Possible DataTime columns
                                if ((dc.DataType == typeof(DateTime)) && (value != DBNull.Value))
                                {
                                    DateTime dateTime = Convert.ToDateTime(value);
                                    System.Globalization.CultureInfo cultureInfo = new System.Globalization.CultureInfo(CMSContext.CurrentUser.PreferredUICultureCode);
                                    value = dateTime.ToString(cultureInfo);
                                }

                                string content = ValidationHelper.GetString(value, "");
                                content = HTMLHelper.HTMLEncode(content);

                                sb.Append("<td style=\"white-space:nowrap;\">" + content + "</td>");
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table><br />\n");

                        pnlContent.Controls.Add(new LiteralControl(sb.ToString()));
                    }
                }
            }
        }
    }


    /// <summary>
    /// Returns the dataset loaded from the given document data
    /// </summary>
    /// <param name="documentData">Document data to make the dataset from</param>
    /// <param name="taskType">Task type</param>
    /// <param name="taskObjectType">Task object type</param>
    protected virtual DataSet GetDataSet(string documentData, string taskType, string taskObjectType)
    {
        DataSet ds = CMSObjectHelper.GetStagingTaskDataSet(taskType, null, taskObjectType, null);

        XmlParserContext xmlContext = new XmlParserContext(null, null, null, XmlSpace.None);
        XmlReader reader = new XmlTextReader(documentData, XmlNodeType.Element, xmlContext);
        return DataHelper.ReadDataSetFromXml(ds, reader, null, null);
    }


    /// <summary>
    /// Gets the object created from the given DataRow
    /// </summary>
    /// <param name="objectRow">Object DataRow</param>
    /// <param name="objectType">Object type</param>
    protected IInfoObject GetObject(DataRow objectRow, string objectType)
    {
        IInfoObject infoObj = CMSObjectHelper.GetObject(objectRow, objectType);
        if (infoObj == null)
        {
            throw new Exception("Object type '" + objectType + "' not found.");
        }

        return infoObj;
    }
}
