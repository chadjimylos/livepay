CREATE TABLE [COM_Manufacturer] (
		[ManufacturerID]               int NOT NULL IDENTITY(1, 1),
		[ManufacturerDisplayName]      nvarchar(200) NOT NULL,
		[ManufactureHomepage]          nvarchar(400) NULL,
		[ManufacturerEnabled]          bit NOT NULL,
		[ManufacturerGUID]             uniqueidentifier NOT NULL,
		[ManufacturerLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_Manufacturer]
	ADD
	CONSTRAINT [PK_COM_Manufacturer]
	PRIMARY KEY
	NONCLUSTERED
	([ManufacturerID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_Manufacturer_ManufacturerDisplayName_ManufacturerEnabled]
	ON [COM_Manufacturer] ([ManufacturerDisplayName], [ManufacturerEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
