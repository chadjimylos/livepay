<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaxClass_State.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Configuration_TaxClasses_TaxClass_State"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <asp:Panel runat="server" ID="pnlMenu" CssClass="TabsEditMenu" EnableViewState="false">
        <table>
            <tr>
                <td>
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="MenuItemEdit" OnClick="btnSave_Click" EnableViewState="false">
                        <asp:Image ID="imgSave" runat="server" EnableViewState="false" />
                        <% =mSave %>
                    </asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblCountry" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <cms:LocalizedDropDownList ID="drpCountry" CssClass="DropDownField" runat="server"
                        AutoPostBack="true" OnSelectedIndexChanged="drpCountry_SelectedIndexChanged"
                        EnableViewState="true" />
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="gvStates" runat="server" OnDataBound="gvStates_DataBound" CellPadding="3"
            CssClass="UniGridGrid" AutoGenerateColumns="false" Width="50%" EnableViewState="false">
            <HeaderStyle HorizontalAlign="Left" CssClass="UniGridHead" Wrap="false" />
            <AlternatingRowStyle CssClass="OddRow" />
            <RowStyle CssClass="EvenRow" />
            <Columns>
                <asp:TemplateField>
                    <ItemStyle Wrap="false" Width="198" />
                    <HeaderStyle Wrap="false" />
                    <ItemTemplate>
                        <asp:Label ID="lblOrderStatus" runat="server" Text='<%# HTMLHelper.HTMLEncode(Eval("StateDisplayName").ToString()) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="StateCode">
                    <ItemStyle Wrap="false" />
                    <HeaderStyle Wrap="false" />
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:TextBox ID="txtTaxValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TaxValue") %>'
                            MaxLength="10" CssClass="ShortTextBox" OnTextChanged="txtTaxValue_Changed" EnableViewState="false"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:CheckBox ID="chkIsFlatValue" runat="server" Checked='<%# ConvertToBoolean(DataBinder.Eval(Container.DataItem, "IsFlatValue")) %>'
                            OnCheckedChanged="chkIsFlatValue_Changed" EnableViewState="false" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="StateID">
                    <ItemStyle />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
