<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Role_List.aspx.cs" 
    Inherits="CMSModules_Groups_Tools_Roles_Role_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Role list" %>

<%@ Register Src="~/CMSSiteManager/Administration/Roles/Controls/RoleList.ascx" TagName="RoleList" TagPrefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:RoleList Id="roleListElem" runat="server" />
</asp:Content>
