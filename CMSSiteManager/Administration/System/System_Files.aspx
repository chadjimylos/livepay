<%@ Page Language="C#" AutoEventWireup="true" CodeFile="System_Files.aspx.cs" 
    Inherits="CMSSiteManager_Administration_System_System_Files" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Administration - System - Files" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />
    <br />
    <cms:CMSButton  runat="server" ID="btnTest" CssClass="LongSubmitButton" OnClick="btnTest_Click" EnableViewState="false" />
    <br />
    <br />
    <asp:Label runat="server" ID="ltlInfo" EnableViewState="false" />
</asp:Content>
