-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Newsletter_Emails_CreateMails]
	@NewsletterIssueID int,
	@NewsletterID int,
	@SiteID int
AS
BEGIN
	BEGIN TRANSACTION
    
	/* Insert the binding */
	INSERT INTO Newsletter_Emails (EmailNewsletterIssueID, EmailSubscriberID, EmailSiteID, EmailGUID, EmailLastModified)
		SELECT @NewsletterIssueID, SubscriberID, @SiteID, NEWID(), GETDATE()
		FROM Newsletter_SubscriberNewsletter WHERE NewsletterID = @NewsletterID;
	COMMIT TRANSACTION;
END
