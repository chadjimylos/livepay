using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.FormControls;
using CMS.ExtendedControls;

public partial class CMSModules_Ecommerce_FormControls_PublicStatusSelector : FormEngineUserControl
{
    #region "Variables"

    private bool mUseStatusNameForSelection = true;
    private bool mAddAllItemsRecord = true;
    private bool mAddNoneRecord = false;

    private StatusEnum mDisplayItems = StatusEnum.Both;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Indicates which objects should be displayed (Enabled/Disabled/Both). Default values is 'Both'.
    /// </summary>
    public StatusEnum DisplayItems
    {
        get
        {
            return mDisplayItems;
        }
        set
        {
            mDisplayItems = value;
        }
    }


    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (this.mUseStatusNameForSelection)
            {
                return this.PublicStatusName;
            }
            else
            {
                return this.PublicStatusID;
            }
        }
        set
        {
            if (this.mUseStatusNameForSelection)
            {
                this.PublicStatusName = ValidationHelper.GetString(value, "");
            }
            else
            {
                this.PublicStatusID = ValidationHelper.GetInteger(value, 0);
            }
        }
    }


    /// <summary>
    /// Gets or sets the PublicStatus ID.
    /// </summary>
    public int PublicStatusID
    {
        get
        {
            if (this.mUseStatusNameForSelection)
            {
                string name = ValidationHelper.GetString(uniSelector.Value, "");
                PublicStatusInfo tgi = PublicStatusInfoProvider.GetPublicStatusInfo(name);
                if (tgi != null)
                {
                    return tgi.PublicStatusID;
                }
                return 0;
            }
            else
            {
                return ValidationHelper.GetInteger(uniSelector.Value, 0);
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.mUseStatusNameForSelection)
            {
                PublicStatusInfo tgi = PublicStatusInfoProvider.GetPublicStatusInfo(value);
                if (tgi != null)
                {
                    this.uniSelector.Value = tgi.PublicStatusID;
                }
            }
            else
            {
                this.uniSelector.Value = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets the PublicStatus code name.
    /// </summary>
    public string PublicStatusName
    {
        get
        {
            if (this.mUseStatusNameForSelection)
            {
                return ValidationHelper.GetString(this.uniSelector.Value, "");
            }
            else
            {
                int id = ValidationHelper.GetInteger(this.uniSelector.Value, 0);
                PublicStatusInfo tgi = PublicStatusInfoProvider.GetPublicStatusInfo(id);
                if (tgi != null)
                {
                    return tgi.PublicStatusName;
                }
                return "";
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.mUseStatusNameForSelection)
            {
                this.uniSelector.Value = value;
            }
            else
            {
                PublicStatusInfo tgi = PublicStatusInfoProvider.GetPublicStatusInfo(value);
                if (tgi != null)
                {
                    this.uniSelector.Value = tgi.PublicStatusName;
                }
            }
        }
    }


    /// <summary>
    ///  If true, selected value is PublicStatusName, if false, selected value is PublicStatusID
    /// </summary>
    public bool UseStatusNameForSelection
    {
        get
        {
            return mUseStatusNameForSelection;
        }
        set
        {
            mUseStatusNameForSelection = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add all item record to the dropdownlist.
    /// </summary>
    public bool AddAllItemsRecord
    {
        get
        {
            return mAddAllItemsRecord;
        }
        set
        {
            mAddAllItemsRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add none item record to the dropdownlist.
    /// </summary>
    public bool AddNoneRecord
    {
        get
        {
            return mAddNoneRecord;
        }
        set
        {
            mAddNoneRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Inner control
    /// </summary>
    public DropDownList InnerControl
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect;
        }
    }


    /// <summary>
    /// Returns ClientID of the dropdownlist.
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect.ClientID;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        this.uniSelector.EnabledColumnName = "PublicStatusEnabled";
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.AllowAll = this.AddAllItemsRecord;
        this.uniSelector.AllowEmpty = this.AddNoneRecord;
        this.uniSelector.ReturnColumnName = (this.UseStatusNameForSelection ? "PublicStatusName" : "PublicStatusID");

        if (this.DisplayItems == StatusEnum.Disabled)
        {
            this.uniSelector.WhereCondition = "PublicStatusEnabled = 0";
        }
        else if (this.DisplayItems == StatusEnum.Enabled)
        {
            this.uniSelector.WhereCondition = "PublicStatusEnabled = 1";
        }

        if (this.UseStatusNameForSelection)
        {
            this.uniSelector.AllRecordValue = "";
            this.uniSelector.NoneRecordValue = "";
        }
    }
}
