CREATE TABLE [CMS_UserSettings] (
		[UserSettingsID]                     int NOT NULL IDENTITY(1, 1),
		[UserNickName]                       nvarchar(200) NULL,
		[UserPicture]                        nvarchar(200) NULL,
		[UserSignature]                      nvarchar(max) NULL,
		[UserURLReferrer]                    nvarchar(450) NULL,
		[UserCampaign]                       nvarchar(200) NULL,
		[UserMessagingNotificationEmail]     nvarchar(200) NULL,
		[UserCustomData]                     nvarchar(max) NULL,
		[UserRegistrationInfo]               nvarchar(max) NULL,
		[UserPreferences]                    nvarchar(max) NULL,
		[UserActivationDate]                 datetime NULL,
		[UserActivatedByUserID]              int NULL,
		[UserTimeZoneID]                     int NULL,
		[UserAvatarID]                       int NULL,
		[UserBadgeID]                        int NULL,
		[UserShowSplashScreen]               bit NULL,
		[UserActivityPoints]                 int NULL,
		[UserForumPosts]                     int NULL,
		[UserBlogComments]                   int NULL,
		[UserGender]                         int NULL,
		[UserDateOfBirth]                    datetime NULL,
		[UserMessageBoardPosts]              int NULL,
		[UserSettingsUserGUID]               uniqueidentifier NOT NULL,
		[UserSettingsUserID]                 int NOT NULL,
		[WindowsLiveID]                      nvarchar(50) NULL,
		[UserBlogPosts]                      int NULL,
		[UserWaitingForApproval]             bit NULL,
		[UserDialogsConfiguration]           nvarchar(max) NULL,
		[UserDescription]                    nvarchar(max) NULL,
		[UserUsedWebParts]                   nvarchar(1000) NULL,
		[UserUsedWidgets]                    nvarchar(1000) NULL,
		[UserFacebookID]                     nvarchar(100) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_UserSettings]
	ADD
	CONSTRAINT [PK_CMS_UserSettings]
	PRIMARY KEY
	NONCLUSTERED
	([UserSettingsID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_UserSettings]
	ADD
	CONSTRAINT [DEFAULT_CMS_UserSettings_UserSettingsUserGUID]
	DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [UserSettingsUserGUID]
ALTER TABLE [CMS_UserSettings]
	ADD
	CONSTRAINT [DEFAULT_CMS_UserSettings_UserSettingsUserID]
	DEFAULT ((0)) FOR [UserSettingsUserID]
ALTER TABLE [CMS_UserSettings]
	ADD
	CONSTRAINT [DEFAULT_CMS_UserSettings_UserShowSplashScreen]
	DEFAULT ((0)) FOR [UserShowSplashScreen]
ALTER TABLE [CMS_UserSettings]
	ADD
	CONSTRAINT [DEFAULT_CMS_UserSettings_UserWaitingForApproval]
	DEFAULT ((0)) FOR [UserWaitingForApproval]
IF OBJECT_ID(N'[CMS_UserSettings]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserSettings_UserActivatedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserSettings]
			ADD CONSTRAINT [FK_CMS_UserSettings_UserActivatedByUserID_CMS_User]
			FOREIGN KEY ([UserActivatedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[CMS_UserSettings]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Avatar]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserSettings_UserAvatarID_CMS_Avatar]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserSettings]
			ADD CONSTRAINT [FK_CMS_UserSettings_UserAvatarID_CMS_Avatar]
			FOREIGN KEY ([UserAvatarID]) REFERENCES [CMS_Avatar] ([AvatarID])
END
IF OBJECT_ID(N'[CMS_UserSettings]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Badge]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserSettings_UserBadgeID_CMS_Badge]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserSettings]
			ADD CONSTRAINT [FK_CMS_UserSettings_UserBadgeID_CMS_Badge]
			FOREIGN KEY ([UserBadgeID]) REFERENCES [CMS_Badge] ([BadgeID])
END
IF OBJECT_ID(N'[CMS_UserSettings]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserSettings_UserSettingsUserGUID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserSettings]
			ADD CONSTRAINT [FK_CMS_UserSettings_UserSettingsUserGUID_CMS_User]
			FOREIGN KEY ([UserSettingsUserGUID]) REFERENCES [CMS_User] ([UserGUID])
END
IF OBJECT_ID(N'[CMS_UserSettings]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserSettings_UserSettingsUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserSettings]
			ADD CONSTRAINT [FK_CMS_UserSettings_UserSettingsUserID_CMS_User]
			FOREIGN KEY ([UserSettingsUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[CMS_UserSettings]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_TimeZone]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UserSettings_UserTimeZoneID_CMS_TimeZone]') IS NULL
BEGIN
		ALTER TABLE [CMS_UserSettings]
			ADD CONSTRAINT [FK_CMS_UserSettings_UserTimeZoneID_CMS_TimeZone]
			FOREIGN KEY ([UserTimeZoneID]) REFERENCES [CMS_TimeZone] ([TimeZoneID])
END
CREATE INDEX [IX_CMS_UserSettings_UserActivatedByUserID]
	ON [CMS_UserSettings] ([UserActivatedByUserID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_UserSettings_UserAvatarID]
	ON [CMS_UserSettings] ([UserAvatarID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_UserSettings_UserBadgeID]
	ON [CMS_UserSettings] ([UserBadgeID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_UserSettings_UserFacebookID]
	ON [CMS_UserSettings] ([UserFacebookID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_UserSettings_UserGender]
	ON [CMS_UserSettings] ([UserGender])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_UserSettings_UserNickName]
	ON [CMS_UserSettings] ([UserNickName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_UserSettings_UserSettingsUserGUID]
	ON [CMS_UserSettings] ([UserSettingsUserGUID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_UserSettings_UserSettingsUserID]
	ON [CMS_UserSettings] ([UserSettingsUserID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_UserSettings_UserTimeZoneID]
	ON [CMS_UserSettings] ([UserTimeZoneID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_UserSettings_UserWaitingForApproval]
	ON [CMS_UserSettings] ([UserWaitingForApproval])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_UserSettings_WindowsLiveID]
	ON [CMS_UserSettings] ([WindowsLiveID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
