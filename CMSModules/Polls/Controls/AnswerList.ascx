<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AnswerList.ascx.cs" Inherits="CMSModules_Polls_Controls_AnswerList" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<asp:Panel ID="pnlBody" runat="server">
    <cms:UniGrid runat="server" ID="uniGrid" GridName="~/CMSModules/Polls/Controls/AnswerList.xml"
        OrderBy="AnswerOrder ASC" />
</asp:Panel>
