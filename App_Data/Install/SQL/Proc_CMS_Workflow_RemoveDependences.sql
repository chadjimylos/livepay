CREATE PROCEDURE [Proc_CMS_Workflow_RemoveDependences]
	@WorkflowID int
AS
BEGIN
	-- Remove step roles
	DELETE FROM CMS_WorkflowStepRoles WHERE StepID IN (SELECT StepID FROM CMS_WorkflowStep WHERE StepWorkflowID = @WorkflowID); 
	-- Clear the documents steps
	UPDATE CMS_Document SET DocumentWorkflowStepID = NULL WHERE DocumentWorkflowStepID IN (SELECT StepID FROM CMS_WorkflowStep WHERE StepWorkflowID = @WorkflowID);
	-- Clear steps within workflow history
	UPDATE CMS_WorkflowHistory SET StepID = NULL WHERE StepID IN (SELECT StepID FROM CMS_WorkflowStep WHERE StepWorkflowID = @WorkflowID);
	-- Remove steps
	DELETE FROM CMS_WorkflowStep WHERE StepWorkflowID = @WorkflowID; 
    -- Remove scopes
	DELETE FROM CMS_WorkflowScope WHERE ScopeWorkflowID = @WorkflowID;
END
