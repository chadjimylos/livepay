using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_DiscountCoupon_Discount_Edit_Header : CMSEcommercePage
{
    protected int discountId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        string currentDiscountCoupon = "";
        discountId = ValidationHelper.GetInteger(Request.QueryString["discountid"], 0);
        if (discountId > 0)
        {
             DiscountCouponInfo discountCouponObj = DiscountCouponInfoProvider.GetDiscountCouponInfo(discountId);
             if (discountCouponObj != null)
             {
                 currentDiscountCoupon = ResHelper.LocalizeString(discountCouponObj.DiscountCouponDisplayName);
             }
        }
        
        // Page title
        this.CurrentMaster.Title.HelpTopicName = "new_coupongeneral_tab";
        this.CurrentMaster.Title.HelpName = "helpTopic";
        
        // Initializes page title breadcrumbs control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("DiscounCoupon_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/DiscountCoupon/Discount_List.aspx";
        pageTitleTabs[0, 2] = "ecommerceContent";
        pageTitleTabs[1, 0] = currentDiscountCoupon;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;

        // Tabs
        this.CurrentMaster.Tabs.Tabs = new string[2, 4];
        this.CurrentMaster.Tabs.Tabs[0, 0] = ResHelper.GetString("general.general");
        this.CurrentMaster.Tabs.Tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'new_coupongeneral_tab');";
        this.CurrentMaster.Tabs.Tabs[0, 2] = "Discount_Edit_General.aspx?discountid=" + discountId.ToString();
        this.CurrentMaster.Tabs.Tabs[1, 0] = ResHelper.GetString("Discount_Edit_Header.Products");
        this.CurrentMaster.Tabs.Tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'products');";
        this.CurrentMaster.Tabs.Tabs[1, 2] = "Discount_Edit_Product_List.aspx?discountid=" + discountId.ToString();

        this.CurrentMaster.Tabs.UrlTarget = "DiscountContent";
    }
}
