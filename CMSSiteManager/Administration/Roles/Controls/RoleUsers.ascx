<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RoleUsers.ascx.cs" 
    Inherits="CMSSiteManager_Administration_Roles_Controls_RoleUsers" %>

<%@ Register Src="~/CMSModules/Membership/FormControls/Users/selectuser.ascx" TagName="SelectUser" TagPrefix="cms" %>
    
<asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false" Visible="false" />
<asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false" Visible="false" />

<asp:Label ID="lblAvialable" runat="server" CssClass="BoldInfoLabel" />
<cms:SelectUser ID="usUsers" runat="server" SelectionMode="Multiple" />
