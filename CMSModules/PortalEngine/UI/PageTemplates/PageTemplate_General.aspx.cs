using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.FileManager;
using CMS.UIControls;
using CMS.ExtendedControls.Dialogs;

public partial class CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_General : CMSEditTemplatePage
{
    #region "Variables"

    protected string pageTemplateWebParts = "";
    protected bool pageTemplateIsReusable = false;
    private PageTemplateInfo pti = null;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get and check template ID
        pageTemplateId = QueryHelper.GetInteger("templateid", 0);
        if (pageTemplateId <= 0)
        {
            return;
        }

        // Hide teaser
        lblUploadFile.Visible = false;
        UploadFile.Visible = false;

        lblTemplateDisplayName.Text = ResHelper.GetString("Administration-PageTemplate_General.TemplateDisplayName");
        lblTemplateCodeName.Text = ResHelper.GetString("Administration-PageTemplate_General.TemplateCodeName");
        lblTemplateCategory.Text = ResHelper.GetString("Administration-PageTemplate_General.Category");
        lblTemplateDescription.Text = ResHelper.GetString("Administration-PageTemplate_General.TemplateDescription");
        lblTemplateType.Text = ResHelper.GetString("Administration-PageTemplate_General.Type");
        radPortal.Text = ResHelper.GetString("Administration-PageTemplate_General.PortalPage");
        radAspx.Text = ResHelper.GetString("Administration-PageTemplate_General.AspxPage");
        lblShowAsMasterTemplate.Text = ResHelper.GetString("Administration-PageTemplate_General.ShowAsMasterTemplate");
        lblInheritLevels.Text = ResHelper.GetString("Administration-PageTemplate_General.InheritLevels");
        lblUploadFile.Text = ResHelper.GetString("Administration-PageTemplate_General.lblUpload");

        rfvTemplateDisplayName.ErrorMessage = ResHelper.GetString("Administration-PageTemplate_General.ErrorEmptyTemplateDisplayName");
        rfvTemplateCodeName.ErrorMessage = ResHelper.GetString("Administration-PageTemplate_General.ErrorEmptyTemplateCodeName");

        // New item link
        string[,] actions = new string[1, 9];
        actions[0, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[0, 1] = ResHelper.GetString("general.save");
        actions[0, 2] = null;
        actions[0, 3] = null;
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        actions[0, 6] = "lnksave_click";
        actions[0, 7] = null;
        actions[0, 8] = "true";
        this.CurrentMaster.HeaderActions.LinkCssClass = "ContentSaveLinkButton";
        this.CurrentMaster.HeaderActions.Actions = actions;
        this.CurrentMaster.HeaderActions.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);

        if (!RequestHelper.IsPostBack())
        {
            LoadData();
            
            pti = PageTemplateInfoProvider.GetPageTemplateInfo(pageTemplateId);
            if (pti != null)
            {
                InheritLevels1.Value = pti.InheritPageLevels;
                InheritLevels1.Level = 10;                
            }
        }
        else
        {
            pti = PageTemplateInfoProvider.GetPageTemplateInfo(pageTemplateId);
            pageTemplateIsReusable = pti.IsReusable;
            pageTemplateWebParts = pti.WebParts;
        }
       

        // Show teaser if needed
        if ((pti != null) && (!pti.DisplayName.ToLower().StartsWith("ad-hoc")))
        {
            lblUploadFile.Visible = true;
            UploadFile.Visible = true;
            UploadFile.ObjectID = pageTemplateId;
            UploadFile.ObjectType = PortalObjectType.PAGETEMPLATE;
            UploadFile.Category = MetaFileInfoProvider.OBJECT_CATEGORY_THUMBNAIL;
        }

        FileSystemDialogConfiguration config = new FileSystemDialogConfiguration();
        config.DefaultPath = "CMSTemplates";
        config.AllowedExtensions = "aspx";
        config.ShowFolders = false;
        FileSystemSelector.DialogConfig = config;
        FileSystemSelector.AllowEmptyValue = true;
        FileSystemSelector.SelectedPathPrefix = "~/CMSTemplates/";
        FileSystemSelector.ValidationError = ResHelper.GetString("Administration-PageTemplate_General.ErrorEmptyTemplateFileName");

        // Script for dynamic hiding of inherited levels
        string script = @"
            function HideOrShowInheritLevels() {                
                var tr = document.getElementById('inheritLevels');                
                if (tr) {                     
                    var checkbox = document.getElementById('" + chkShowAsMasterTemplate.ClientID + @"') ;                               
                    if(checkbox != null) {
                        if(checkbox.checked == 1) {
                            tr.style.display = 'none';
                        }
                        else {
                            tr.style.display = '';
                        }
                    }
                }            
            }
            setTimeout(HideOrShowInheritLevels, 100)";

        // Register script to page
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "showOrHide", ScriptHelper.GetScript(script)) ;

        chkShowAsMasterTemplate.Attributes.Add("onclick", "HideOrShowInheritLevels();");    
    }


    /// <summary>
    /// Save button action
    /// </summary>
    protected void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "lnksave_click":

                // Template has to exist
                if (pti == null)
                {
                    return;
                }

                // Limit text length
                txtTemplateCodeName.Text = TextHelper.LimitLength(txtTemplateCodeName.Text.Trim(), 100, "");
                txtTemplateDisplayName.Text = TextHelper.LimitLength(txtTemplateDisplayName.Text.Trim(), 200, "");

                // finds whether required fields are not empty
                string result = String.Empty;

                result = new Validator().NotEmpty(txtTemplateDisplayName.Text, ResHelper.GetString("Administration-PageTemplate_General.ErrorEmptyTemplateDisplayName")).NotEmpty(txtTemplateCodeName.Text, ResHelper.GetString("Administration-PageTemplate_General.ErrorEmptyTemplateCodeName"))
                     .IsCodeName(txtTemplateCodeName.Text, ResHelper.GetString("general.invalidcodename"))
                     .Result;

                if ((result == String.Empty) && !radPortal.Checked)
                {
                    result = new Validator().NotEmpty(FileSystemSelector.Value.ToString().Trim(), ResHelper.GetString("Administration-PageTemplate_General.ErrorEmptyTemplateFileName")).Result;
                }

                // If name changed, check if new name is unique
                if ((result == String.Empty) && (String.Compare(pti.CodeName, txtTemplateCodeName.Text, true) != 0))
                {
                    if (PageTemplateInfoProvider.PageTemplateNameExists(txtTemplateCodeName.Text))
                    {
                        result = ResHelper.GetString("general.codenameexists");
                    }
                }

                if (result == "")
                {
                    //Update page template info                        

                    pti.DisplayName = txtTemplateDisplayName.Text;
                    pti.CodeName = txtTemplateCodeName.Text;
                    pti.Description = txtTemplateDescription.Text;
                    pti.CategoryID = Convert.ToInt32(categorySelector.Value);
                    if (radPortal.Checked)
                    {
                        pti.FileName = "";
                        pti.IsPortal = true;

                        // Save inherit levels
                        if (!chkShowAsMasterTemplate.Checked)
                        {
                            pti.InheritPageLevels = ValidationHelper.GetString(InheritLevels1.Value, "");
                        }
                        else
                        {
                            pti.InheritPageLevels = "/";
                        }

                        // Show hide inherit levels radio buttons
                        pti.ShowAsMasterTemplate = chkShowAsMasterTemplate.Checked;                        
                    }
                    else
                    {
                        pti.FileName = FileSystemSelector.Value.ToString();
                        pti.IsPortal = false;
                        pti.LayoutID = 0;
                        pti.ShowAsMasterTemplate = false;
                        pti.InheritPageLevels = "";
                    }

                    pti.IsReusable = pageTemplateIsReusable;
                    pti.WebParts = pageTemplateWebParts;                

                    try
                    {
                        PageTemplateInfoProvider.SetPageTemplateInfo(pti);

                        ltlScript.Text += ScriptHelper.GetScript(
                            "if ((parent.parent != null) && (parent.parent.frames['pt_tree'] != null)) parent.parent.frames['pt_tree'].location.href = 'PageTemplate_Tree.aspx?templateid=" + pageTemplateId + "'; \n" +
                            "if (parent.frames['pt_edit_menu'] != null) parent.frames['pt_edit_menu'].location.href = 'PageTemplate_Header.aspx?templateid=" + pageTemplateId + ((ValidationHelper.GetInteger(Request.QueryString["nobreadcrumbs"], 0) > 0) ? "&nobreadcrumbs=1" : "") + "'; \n" + 
                             "var txtDisplayName = document.getElementById('" + txtTemplateDisplayName.ClientID + "');" +
                             "var wopener = parent.wopener; if ((wopener != null) && (wopener.SetTemplateName)) wopener.SetTemplateName(txtDisplayName.value);\n");
                                      
                        lblInfo.Text = "The changes were saved.";
                    }
                    catch (Exception ex)
                    {
                        lblError.Text = ex.Message;
                        lblError.Visible = true;
                    }
                }
                else
                {
                    
                    rfvTemplateDisplayName.Visible = false;
                    rfvTemplateCodeName.Visible = false;

                    lblError.Visible = true;
                    lblError.Text = result;
                }
                break;
        }
    }


    /// <summary>
    /// Load data of edited module from DB into TextBoxes.
    /// </summary>
    protected void LoadData()
    {
        PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(pageTemplateId);
        if (pti == null)
        {
            return;
        }

        txtTemplateDisplayName.Text = pti.DisplayName;
        txtTemplateCodeName.Text = pti.CodeName;
        txtTemplateDescription.Text = pti.Description;

        // Select category
        categorySelector.Value = pti.CategoryID.ToString();

        if (pti.IsPortal)
        {
            radPortal.Checked = true;
            radAspx.Checked = false;

            SetPageTypeToPortal();
        }
        else
        {
            radPortal.Checked = false;
            radAspx.Checked = true;
            FileSystemSelector.Value = pti.FileName;
            SetPageTypeToAspx();
        }
        chkShowAsMasterTemplate.Checked = pti.ShowAsMasterTemplate;
        pageTemplateIsReusable = pti.IsReusable;
        pageTemplateWebParts = pti.WebParts;
    }


    /// <summary>
    /// Shows controls for layout editing and hides controls for template file editing
    /// </summary>
    protected void SetPageTypeToPortal()
    {
        FileSystemSelector.Visible = false;
        lblTemplateFileName.Visible = false;

        chkShowAsMasterTemplate.Visible = true;
        lblShowAsMasterTemplate.Visible = true;
        InheritLevels1.Visible = true;
        lblInheritLevels.Visible = true;
    }


    /// <summary>
    /// Shows controls for template file editing and hides controls for layout editing
    /// </summary>
    protected void SetPageTypeToAspx()
    {
        FileSystemSelector.Visible = true;
        lblTemplateFileName.Visible = true;

        chkShowAsMasterTemplate.Visible = false;
        lblShowAsMasterTemplate.Visible = false;
        InheritLevels1.Visible = false;
        lblInheritLevels.Visible = false;
    }


    /// <summary>
    /// Handles onCheckedChange event of radPortal radio button
    /// </summary>
    protected void radPortal_CheckedChange(object sender, EventArgs e)
    {
        if (radPortal.Checked == true)
        {
            SetPageTypeToPortal();
        }
        else
        {
            SetPageTypeToAspx();
        }
    }


    /// <summary>
    /// Handles onCheckedChange event of radAspx radio button
    /// </summary>
    protected void radAspx_CheckedChange(object sender, EventArgs e)
    {
        if (radAspx.Checked == false)
        {
            SetPageTypeToPortal();
        }
        else
        {
            SetPageTypeToAspx();
        }
    }
}
