using System;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_CustomTables_CustomTable_Edit_Header : CMSCustomTablesPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int customTableId = QueryHelper.GetInteger("customTableId", 0);

        if (customTableId > 0)
        {
            DataClassInfo classInfo = DataClassInfoProvider.GetDataClass(customTableId);

            // If class exists
            if (classInfo != null)
            {
                // Initializes PageTitle

                string[,] pageTitleTabs = new string[2, 3];
                pageTitleTabs[0, 0] = ResHelper.GetString("customtable.list.title");
                pageTitleTabs[0, 1] = "~/CMSModules/CustomTables/CustomTable_List.aspx";
                pageTitleTabs[0, 2] = "_parent";
                pageTitleTabs[1, 0] = classInfo.ClassDisplayName;
                pageTitleTabs[1, 1] = string.Empty;
                pageTitleTabs[1, 2] = string.Empty;

                CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
                CurrentMaster.Title.TitleText = ResHelper.GetString("customtable.edit.header");
                CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_CustomTable/object.png");
                CurrentMaster.Title.HelpTopicName = "CustomTable_Edit_General";
                CurrentMaster.Title.HelpName = "helpTopic";

                ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ShowContent", ScriptHelper.GetScript("function ShowContent(contentLocation) { parent.content.location.href = contentLocation; }"));

                if (!RequestHelper.IsPostBack())
                {
                    // Initialize menu
                    InitializeMenu(classInfo);
                }
            }
        }
    }


    /// <summary>
    /// Initializes CustomTable edit menu
    /// </summary>    
    protected void InitializeMenu(DataClassInfo classObj)
    {
        string[,] tabs = new string[10, 4];
        tabs[0, 0] = ResHelper.GetString("general.general");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'CustomTable_Edit_General');";
        tabs[0, 2] = "CustomTable_Edit_General.aspx?customtableid=" + classObj.ClassID;
        tabs[1, 0] = ResHelper.GetString("general.fields");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'CustomTable_Edit_Fields');";
        tabs[1, 2] = "CustomTable_Edit_Fields.aspx?customtableid=" + classObj.ClassID;
        tabs[2, 0] = ResHelper.GetString("general.form");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'CustomTable_Edit_Form');";
        tabs[2, 2] = "CustomTable_Edit_Form.aspx?customtableid=" + classObj.ClassID;
        tabs[3, 0] = ResHelper.GetString("general.Transformations");
        tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'CustomTable_Edit_Transformation_List');";
        tabs[3, 2] = "CustomTable_Edit_Transformation_List.aspx?customtableid=" + classObj.ClassID;
        tabs[4, 0] = ResHelper.GetString("general.Queries");
        tabs[4, 1] = ""; // "SetHelpTopic('helpTopic', 'CustomTable_Edit_Query_List');";
        tabs[4, 2] = "CustomTable_Edit_Query_List.aspx?customtableid=" + classObj.ClassID;
        tabs[6, 0] = ResHelper.GetString("general.sites");
        tabs[6, 1] = ""; // "SetHelpTopic('helpTopic', 'CustomTable_Edit_Sites');";
        tabs[6, 2] = "CustomTable_Edit_Sites.aspx?customtableid=" + classObj.ClassID;
        tabs[7, 0] = ResHelper.GetString("customtable.header.altforms");
        tabs[7, 1] = ""; // "SetHelpTopic('helpTopic', 'CustomTable_Edit_AlternativeForms');";
        tabs[7, 2] = "AlternativeForms/AlternativeForms_List.aspx?classid=" + classObj.ClassID;
        tabs[8, 0] = ResHelper.GetString("srch.fields");
        tabs[8, 1] = ""; // "SetHelpTopic('helpTopic', 'CustomTable_Edit_SearchFields');";
        tabs[8, 2] = "CustomTable_Edit_SearchFields.aspx?classid=" + classObj.ClassID;

        CurrentMaster.Tabs.Tabs = tabs;
        CurrentMaster.Tabs.UrlTarget = "content";
    }
}
