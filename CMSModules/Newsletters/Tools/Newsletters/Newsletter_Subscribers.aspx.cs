using System;
using System.Data;
using System.Data.SqlTypes;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Newsletter;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.DataEngine;
using CMS.LicenseProvider;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Subscribers : CMSNewsletterPage
{
    #region "Variables"

    protected int newsletterId = 0;
    protected string currentUsers = null;
    protected string currentRoles = null;
    protected string currentSubscribers = null;

    #endregion


    #region "Methods"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        this.CurrentMaster.ActionsViewstateEnabled = true;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        newsletterId = QueryHelper.GetInteger("newsletterid", 0);
        this.CurrentMaster.DisplayActionsPanel = true;

        chkSendConfirmation.Text = ResHelper.GetString("Newsletter_Subscribers.SendConfirmation");
        chkSendConfirmation.TextAlign = TextAlign.Right;

        if (!RequestHelper.IsPostBack())
        {
            chkSendConfirmation.Checked = false;
        }

        // Initialize unigrid
        UniGridSubscribers.WhereCondition = "SubscriberID IN (SELECT SubscriberID FROM Newsletter_SubscriberNewsletter WHERE NewsletterID = " + newsletterId.ToString() + ")";
        UniGridSubscribers.OnAction += new OnActionEventHandler(UniGridSubscribers_OnAction);
        UniGridSubscribers.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGridSubscribers_OnExternalDataBound);

        CurrentMaster.DisplayControlsPanel = true;

        SetupSelectors();
        ReloadData(false);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        lblInfo.Visible = UniGridSubscribers.IsEmpty;
    }


    /// <summary>
    /// Unigrid external databound event handler.
    /// </summary>
    protected object UniGridSubscribers_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        if (sourceName.ToLower() == "email")
        {
            DataRowView dr = (DataRowView)parameter;
            string email = ValidationHelper.GetString(DataHelper.GetDataRowValue(dr.Row, "SubscriberEmail"), "");
            if (!string.IsNullOrEmpty(email))
            {
                return email;
            }
            else
            {
                return ValidationHelper.GetString(DataHelper.GetDataRowValue(dr.Row, "Email"), "");
            }
        }

        return null;
    }


    /// <summary>
    /// Configures selectors.
    /// </summary>
    private void SetupSelectors()
    {
        // Setup role selector
        selectRole.CurrentSelector.SelectionMode = SelectionModeEnum.MultipleButton;
        selectRole.CurrentSelector.OnItemsSelected += new EventHandler(RolesSelector_OnItemsSelected);
        selectRole.CurrentSelector.ReturnColumnName = "RoleID";
        selectRole.CurrentSelector.ButtonImage = GetImageUrl("Objects/CMS_Role/add.png");
        selectRole.ImageDialog.CssClass = "NewItemImage";
        selectRole.LinkDialog.CssClass = "NewItemLink";
        selectRole.ShowSiteFilter = false;
        selectRole.CurrentSelector.ResourcePrefix = "addroles";
        selectRole.DialogButton.CssClass = "LongButton";
        selectRole.IsLiveSite = false;
        selectRole.UseCodeNameForSelection = false;

        // Setup user selector
        selectUser.UniSelector.SelectionMode = SelectionModeEnum.MultipleButton;
        selectUser.UniSelector.OnItemsSelected += new EventHandler(UserSelector_OnItemsSelected);
        selectUser.UniSelector.ReturnColumnName = "UserID";
        selectUser.UniSelector.ButtonImage = GetImageUrl("Objects/CMS_User/add.png");
        selectUser.ImageDialog.CssClass = "NewItemImage";
        selectUser.LinkDialog.CssClass = "NewItemLink";
        selectUser.ShowSiteFilter = false;
        selectUser.ResourcePrefix = "addusers";
        selectUser.DialogButton.CssClass = "LongButton";
        selectUser.IsLiveSite = false;

        // Setup subscriber selector
        selectSubscriber.UniSelector.SelectionMode = SelectionModeEnum.MultipleButton;
        selectSubscriber.UniSelector.OnItemsSelected += new EventHandler(SubscriberSelector_OnItemsSelected);
        selectSubscriber.UniSelector.ReturnColumnName = "SubscriberID";
        selectSubscriber.UniSelector.ButtonImage = GetImageUrl("Objects/Newsletter_Subscriber/add.png");
        selectSubscriber.ImageDialog.CssClass = "NewItemImage";
        selectSubscriber.LinkDialog.CssClass = "NewItemLink";
        selectSubscriber.ShowSiteFilter = false;
        selectSubscriber.DialogButton.CssClass = "LongButton";
        selectSubscriber.IsLiveSite = false;
    }


    /// <summary>
    /// Reloads data.
    /// </summary>
    /// <param name="forceReload">Force reload indicator</param>
    public void ReloadData(bool forceReload)
    {
        // Get all subsribed roles of newsletter
        GeneralConnection conn = ConnectionHelper.GetConnection();
        DataSet ds = conn.ExecuteQuery("newsletter.subscriber.selectview", null, UniGridSubscribers.WhereCondition, UniGridSubscribers.SortDirect, 0, "UserID, RoleID, SubscriberID, SubscriberType");

        // Filter only role subscribers
        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "SubscriberType = '" + SiteObjectType.ROLE + "'";
        DataTable dtFiltered = dv.ToTable();
        currentRoles = String.Join(";", SqlHelperClass.GetStringValues(dtFiltered, "RoleID"));

        // Filter only user subscribers
        dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "SubscriberType = '" + SiteObjectType.USER + "'";
        dtFiltered = dv.ToTable();
        currentUsers = String.Join(";", SqlHelperClass.GetStringValues(dtFiltered, "UserID"));

        // Filter regular subscribers
        dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "SubscriberType IS NULL";
        dtFiltered = dv.ToTable();
        currentSubscribers = String.Join(";", SqlHelperClass.GetStringValues(dtFiltered, "SubscriberID"));

        if (!RequestHelper.IsPostBack() || forceReload)
        {
            selectRole.Value = currentRoles;
            selectUser.Value = currentUsers;
            selectSubscriber.Value = currentSubscribers;
        }
    }


    /// <summary>
    /// Roles control items changed event.
    /// </summary>
    protected void RolesSelector_OnItemsSelected(object sender, EventArgs e)
    {
        // Check permissions
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "configure"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "configure");
        }

        int roleID = 0;
        string[] newItems = null;
        Subscriber sb = null;

        // Get new items from selector
        string newValues = ValidationHelper.GetString(selectRole.Value, null);

        // Get removed items
        string items = DataHelper.GetNewItemsInList(newValues, currentRoles, ';');
        if (!String.IsNullOrEmpty(items))
        {
            newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                foreach (string item in newItems)
                {
                    roleID = ValidationHelper.GetInteger(item, 0);

                    // Get subscriber
                    sb = SubscriberProvider.GetSubscriber(SiteObjectType.ROLE, roleID);

                    // If subscriber is subscribed, unsubscribe him
                    if ((sb != null) && (SubscriberProvider.IsSubscribed(sb.SubscriberID, newsletterId)))
                    {
                        try
                        {
                            SubscriberProvider.Unsubscribe(sb.SubscriberGUID, newsletterId, chkSendConfirmation.Checked);
                        }
                        catch { };
                    }
                }
            }
        }

        // Get added items
        items = DataHelper.GetNewItemsInList(currentRoles, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                foreach (string item in newItems)
                {
                    roleID = ValidationHelper.GetInteger(item, 0);

                    // Get subscriber
                    sb = SubscriberProvider.GetSubscriber(SiteObjectType.ROLE, roleID);
                    if (sb == null)
                    {
                        // Get role info and copy display name to new subscriber
                        RoleInfo ri = RoleInfoProvider.GetRoleInfo(roleID);
                        if (ri == null)
                        {
                            break;
                        }

                        // Check limited number of subscribers
                        if (!SubscriberProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Subscribers, VersionActionEnum.Insert))
                        {
                            lblError.Text = ResHelper.GetString("licenselimitations.subscribers.errormultiple");
                            lblError.Visible = true;
                            break;
                        }

                        // Create new subscriber of role type
                        sb = new Subscriber();
                        sb.SubscriberFirstName = ri.DisplayName;
                        // Full name consists of "role " and role display name
                        sb.SubscriberFullName = "Role '" + ri.DisplayName + "'";
                        sb.SubscriberSiteID = CMSContext.CurrentSiteID;
                        sb.SubscriberType = SiteObjectType.ROLE;
                        sb.SubscriberRelatedID = roleID;
                        SubscriberProvider.SetSubscriber(sb);
                    }

                    // If subscriber exists and is not subscribed, subscribe him
                    if ((sb != null) && (!SubscriberProvider.IsSubscribed(sb.SubscriberID, newsletterId)))
                    {
                        try
                        {
                            SubscriberProvider.Subscribe(sb.SubscriberID, newsletterId, DateTime.Now, chkSendConfirmation.Checked);
                        }
                        catch { };
                    }
                }
            }
        }

        UniGridSubscribers.ReloadData();
        pnlUpdate.Update();
    }


    /// <summary>
    /// User control items changed event.
    /// </summary>
    protected void UserSelector_OnItemsSelected(object sender, EventArgs e)
    {
        // Check permissions
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "configure"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "configure");
        }

        int userID = 0;
        string[] newItems = null;
        Subscriber sb = null;

        // Get new items from selector
        string newValues = ValidationHelper.GetString(selectUser.Value, null);

        // Get removed items
        string items = DataHelper.GetNewItemsInList(newValues, currentUsers, ';');
        if (!String.IsNullOrEmpty(items))
        {
            newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                foreach (string item in newItems)
                {
                    userID = ValidationHelper.GetInteger(item, 0);

                    // Get subscriber
                    sb = SubscriberProvider.GetSubscriber(SiteObjectType.USER, userID);

                    // If subscriber is subscribed, unsubscribe him
                    if ((sb != null) && (SubscriberProvider.IsSubscribed(sb.SubscriberID, newsletterId)))
                    {
                        try
                        {
                            SubscriberProvider.Unsubscribe(sb.SubscriberGUID, newsletterId, chkSendConfirmation.Checked);
                        }
                        catch { };
                    }
                }
            }
        }

        UserInfo ui = null;

        // Get added items
        items = DataHelper.GetNewItemsInList(currentUsers, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                foreach (string item in newItems)
                {
                    userID = ValidationHelper.GetInteger(item, 0);

                    // Get subscriber
                    sb = SubscriberProvider.GetSubscriber(SiteObjectType.USER, userID);
                    if (sb == null)
                    {
                        // Get user info
                        ui = UserInfoProvider.GetUserInfo(userID);
                        if (ui == null)
                        {
                            break;
                        }

                        // Check limited number of subscribers
                        if (!SubscriberProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Subscribers, VersionActionEnum.Insert))
                        {
                            lblError.Text = ResHelper.GetString("licenselimitations.subscribers.errormultiple");
                            lblError.Visible = true;
                            break;
                        }

                        // Create new subscriber of user type
                        sb = new Subscriber();
                        sb.SubscriberFirstName = ui.FullName;
                        sb.SubscriberFullName = "User '" + ui.FullName + "'";
                        sb.SubscriberSiteID = CMSContext.CurrentSiteID;
                        sb.SubscriberType = SiteObjectType.USER;
                        sb.SubscriberRelatedID = userID;
                        SubscriberProvider.SetSubscriber(sb);
                    }

                    // If subscriber exists and is not subscribed, subscribe him
                    if ((sb != null) && (!SubscriberProvider.IsSubscribed(sb.SubscriberID, newsletterId)))
                    {
                        try
                        {
                            SubscriberProvider.Subscribe(sb.SubscriberID, newsletterId, DateTime.Now, chkSendConfirmation.Checked);
                        }
                        catch { };
                    }
                }
            }
        }

        UniGridSubscribers.ReloadData();
        pnlUpdate.Update();
    }


    /// <summary>
    /// Subscriber control items changed event.
    /// </summary>
    protected void SubscriberSelector_OnItemsSelected(object sender, EventArgs e)
    {
        // Check permissions
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "configure"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "configure");
        }

        int subscriberID = 0;
        string[] newItems = null;
        Subscriber sb = null;

        // Get new items from selector
        string newValues = ValidationHelper.GetString(selectSubscriber.Value, null);

        // Get removed items
        string items = DataHelper.GetNewItemsInList(newValues, currentSubscribers, ';');
        if (!String.IsNullOrEmpty(items))
        {
            newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                foreach (string item in newItems)
                {
                    subscriberID = ValidationHelper.GetInteger(item, 0);

                    // Get subscriber
                    sb = SubscriberProvider.GetSubscriber(subscriberID);

                    // If subscriber is subscribed, unsubscribe him
                    if ((sb != null) && (SubscriberProvider.IsSubscribed(sb.SubscriberID, newsletterId)))
                    {
                        try
                        {
                            SubscriberProvider.Unsubscribe(sb.SubscriberGUID, newsletterId, chkSendConfirmation.Checked);
                        }
                        catch { };
                    }
                }
            }
        }

        // Get added items
        items = DataHelper.GetNewItemsInList(currentSubscribers, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    subscriberID = ValidationHelper.GetInteger(item, 0);

                    // Get subscriber
                    sb = SubscriberProvider.GetSubscriber(subscriberID);

                    // If subscriber exists and is not subscribed, subscribe him
                    if ((sb != null) && (!SubscriberProvider.IsSubscribed(sb.SubscriberID, newsletterId)))
                    {
                        try
                        {
                            SubscriberProvider.Subscribe(sb.SubscriberID, newsletterId, DateTime.Now, chkSendConfirmation.Checked);
                        }
                        catch { };
                    }
                }
            }
        }

        UniGridSubscribers.ReloadData();
        pnlUpdate.Update();
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that threw event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGridSubscribers_OnAction(string actionName, object actionArgument)
    {
        // Unsubscribe selected subscriber
        if (actionName == "remove")
        {
            // Check 'configure' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "configure"))
            {
                RedirectToCMSDeskAccessDenied("cms.newsletter", "configure");
            }

            Subscriber sub = SubscriberProvider.GetSubscriber(Convert.ToInt32(actionArgument));
            if (sub != null)
            {
                SubscriberProvider.Unsubscribe(sub.SubscriberGUID, newsletterId, chkSendConfirmation.Checked);
                ReloadData(true);
            }
        }
    }

    #endregion
}
