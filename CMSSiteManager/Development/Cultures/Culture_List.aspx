<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Culture_List.aspx.cs" Inherits="CMSSiteManager_Development_Cultures_Culture_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Cultures - Culture List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <cms:UniGrid ID="UniGridCultures" runat="server" GridName="Culture_List.xml" OrderBy="CultureName"
        Columns="CultureID, CultureName, CultureCode, CultureShortName" IsLiveSite="false" />
</asp:Content>
