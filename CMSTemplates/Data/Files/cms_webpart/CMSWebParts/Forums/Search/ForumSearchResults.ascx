<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Forums/Search/ForumSearchResults.ascx.cs"
    Inherits="CMSWebParts_Forums_Search_ForumSearchResults" %>
<%@ Register Src="~/CMSModules/Forums/Controls/ForumDivider.ascx" TagName="ForumFlatView" TagPrefix="cms" %>
<cms:ForumFlatView ID="forumElem" runat="server" />
<asp:Label ID="lblNoResults" runat="server" Visible="false" EnableViewState="false" />
