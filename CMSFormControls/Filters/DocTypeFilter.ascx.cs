using System;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.Controls;
using CMS.CMSHelper;
using CMS.SettingsProvider;

public partial class CMSFormControls_Filters_DocTypeFilter : CMSAbstractBaseFilterControl
{
    private CMSUserControl filteredControl = null;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // Load Class type DDL
        if (!UrlHelper.IsPostback())
        {
            drpClassType.Items.Clear();
            drpClassType.Items.Add(new ListItem(ResHelper.GetString("general.documenttype"), "doctype"));
            drpClassType.Items.Add(new ListItem(ResHelper.GetString("queryselection.classtype.customtables"), "customtables"));

            lblDocType.Text = ResHelper.GetString("queryselection.lbldoctypes");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();

            WhereCondition = GenerateWhereCondition();

        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        filteredControl = FilteredControl as CMSUserControl;

        // Initialize UniSelector with document types
        uniSelector.DisplayNameFormat = "{%ClassDisplayName%} ({%ClassName%})";
        uniSelector.SelectionMode = SelectionModeEnum.SingleDropDownList;

        if (drpClassType.SelectedValue == "doctype")
        {
            uniSelector.WhereCondition = "(ClassIsDocumentType = 1)";
        }
        else
        {
            uniSelector.WhereCondition = "(ClassIsCustomTable = 1) AND " +
                "(ClassID IN (SELECT ClassID FROM CMS_ClassSite WHERE SiteID = " +
                CMSContext.CurrentSiteID + "))";
        }

        uniSelector.ReturnColumnName = "ClassID";
        uniSelector.ObjectType = SettingsObjectType.DOCUMENTTYPE;
        uniSelector.ResourcePrefix = "allowedclasscontrol";
        uniSelector.AllowAll = false;
        uniSelector.AllowEmpty = false;
        uniSelector.DropDownSingleSelect.AutoPostBack = true;
        uniSelector.OnSelectionChanged += uniSelector_OnSelectionChanged;
        uniSelector.IsLiveSite = (filteredControl != null ? filteredControl.IsLiveSite : true);
        uniSelector.DialogWindowName = "DocumentTypeSelectionDialog";
    }


    /// <summary>
    /// Main filter action (document name changed). Raises the filter event.
    /// </summary>
    protected void uniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        // Set where condition
        WhereCondition = GenerateWhereCondition();

        // Raise OnFilterChange event
        RaiseOnFilterChanged();
    }


    /// <summary>
    /// Class type changed event.
    /// </summary>
    protected void drpClassType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (drpClassType.SelectedValue == "doctype")
        {
            lblDocType.Text = ResHelper.GetString("queryselection.lbldoctypes");
            uniSelector.WhereCondition = "(ClassIsDocumentType = 1)";
        }
        else
        {
            lblDocType.Text = ResHelper.GetString("queryselection.customtable");
            uniSelector.WhereCondition = "(ClassIsCustomTable = 1) AND " +
                "(ClassID IN (SELECT ClassID FROM CMS_ClassSite WHERE SiteID = " +
                CMSContext.CurrentSiteID + "))";
        }

        uniSelector.Reload(true);

        WhereCondition = GenerateWhereCondition();
        RaiseOnFilterChanged();
    }


    /// <summary>
    /// Generates where condition.
    /// </summary>
    private string GenerateWhereCondition()
    {
        if (!uniSelector.HasData)
        {
            return "0=1";
        }

        // Get the class ID
        int classId = ValidationHelper.GetInteger(uniSelector.Value, 0);
        if (classId > 0)
        {
            // Only results for specific class
            string mode = "";
            if (filteredControl != null)
            {
                mode = ValidationHelper.GetString(filteredControl.GetValue("FilterMode"), "");

                // Set the prefix for the item
                DataClassInfo ci = DataClassInfoProvider.GetDataClass(classId);
                filteredControl.SetValue("ItemPrefix", ci.ClassName + ".");
            }

            switch (mode.ToLower())
            {
                case "transformation":
                    return "(TransformationClassID = " + uniSelector.Value + ")";

                default:
                    return "(ClassID = " + uniSelector.Value + ")";
            }
        }
        else
        {
            // No results
            return "0=1";
        }
    }
}
