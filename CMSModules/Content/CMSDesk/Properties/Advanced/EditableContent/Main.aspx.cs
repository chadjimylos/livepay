using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;

using CMS.PortalEngine;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.FormEngine;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Properties_Advanced_EditableContent_Main : CMSModalPage
{
    #region "Private variables"

    private static int nodeId = 0;
    private static string keyName = null;
    private EditableContentType keyType;

    private static TreeProvider tree = null;
    private static TreeNode node = null;
    private string content = null;
    private static bool createNew = false;

    private Control invokeControl = null;

    private enum EditingForms { EditableImage = 0, HTMLEditor = 1, TextArea = 2, TextBox = 3 };
    private enum EditableContentType { webpart = 0, region = 1 };

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RequestHelper.IsPostBack())
        {
            // Initialize HTML editor & fill dropdown list
            InitHTMLEditor();
            InitEditorOptions();

            // Inform user about saving
            if (QueryHelper.GetBoolean("imagesaved", false))
            {
                lblInfo.Text = ResHelper.GetString("general.changessaved");
                lblInfo.Visible = true;
                drpEditControl.SelectedIndex = 1;
            }
        }

        ltlScript.Text +=
            ScriptHelper.GetScript("function LocalCheckIn(){" +
                                   Page.ClientScript.GetPostBackEventReference(btnCheckIn, null) + ";}");

        // Find postback invoker
        string invokerName = Page.Request.Params.Get("__EVENTTARGET");
        invokeControl = !string.IsNullOrEmpty(invokerName) ? Page.FindControl(invokerName) : null;

        // Set whether new item is to be created
        if (!RequestHelper.IsPostBack())
        {
            if (QueryHelper.GetBoolean("createNew", false))
            {
                createNew = true;
            }
            else
            {
                createNew = false;
            }
        }
        else
        {
            if (invokerName != null)
            {
                if (createNew && (invokeControl == drpEditControl))
                {
                    createNew = true;
                }
                else
                {
                    if (invokeControl == drpEditControl)
                    {
                        createNew = false;
                        lblInfo.Text = string.Empty;
                        lblInfo.Visible = false;
                    }
                }
            }
        }

        // Get query parameters
        if (!RequestHelper.IsPostBack())
        {
            keyName = QueryHelper.GetString("nodename", string.Empty);
        }

        nodeId = QueryHelper.GetInteger("nodeid", 0);

        // Set editable content type enum
        switch (QueryHelper.GetString("nodetype", "webpart"))
        {
            case "webpart":
                keyType = EditableContentType.webpart;
                break;

            case "region":
                keyType = EditableContentType.region;
                break;
        }

        // Get node
        tree = new TreeProvider(CMSContext.CurrentUser);
        node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, tree);

        // Initialize javascripts
        ltlScript.Text += ScriptHelper.GetScript("mainUrl = '" + ResolveUrl("~/CMSModules/Content/CMSDesk/Properties/Advanced/EditableContent/main.aspx") + "?nodeid=" + nodeId + "';");

        lblError.Visible = false;

        // Show editing controls
        if ((createNew || keyName != string.Empty))
        {
            pnlMenu.Visible = IsAuthorizedToModify() ? true : false;
            pnlEditableContent.Visible = true;
        }

        // Get content
        if ((node != null) && (keyName != string.Empty) || createNew)
        {
            if (!IsPostBack && !createNew)
            {
                txtName.Text = MultiKeyHashtable.GetFirstKey(keyName);
            }

            if (!createNew)
            {
                // Set content variable
                switch (keyType)
                {
                    case EditableContentType.webpart:
                        if (node != null)
                        {
                            if (node.DocumentContent.EditableWebParts.ContainsKey(keyName))
                            {
                                content =
                                    ValidationHelper.GetString(
                                        node.DocumentContent.EditableWebParts[keyName].ToString(), string.Empty);
                            }
                        }
                        break;

                    case EditableContentType.region:
                        if (node != null)
                        {
                            if (node.DocumentContent.EditableRegions.ContainsKey(keyName))
                            {
                                content =
                                    ValidationHelper.GetString(
                                        node.DocumentContent.EditableRegions[keyName].ToString(), string.Empty);
                            }
                        }
                        break;
                }

                // Disable HTML editor if content is typeof image
                if (content != null)
                {
                    if (content.StartsWith("<image>"))
                    {
                        ListItem li =
                            drpEditControl.Items.FindByValue(Convert.ToInt32(EditingForms.HTMLEditor).ToString());
                        if (li != null)
                        {
                            drpEditControl.Items.Remove(li);
                        }
                    }
                }
            }
        }

        // Hide all content controls
        txtAreaContent.Visible = txtContent.Visible = htmlContent.Visible = imageContent.Visible = false;

        // Set up editing forms
        switch (((EditingForms)Convert.ToInt32(drpEditControl.SelectedValue)))
        {
            case EditingForms.TextArea:
                txtAreaContent.Visible = true;
                break;

            case EditingForms.HTMLEditor:
                htmlContent.Visible = true;
                break;

            case EditingForms.EditableImage:
                imageContent.Visible = true;
                imageContent.ImageTitle = MultiKeyHashtable.GetFirstKey(keyName);
                break;

            case EditingForms.TextBox:
                txtContent.Visible = true;
                break;
        }
        lblContent.Visible = txtContent.Visible;
    }


    protected override void OnPreRender(EventArgs e)
    {
        // Load content to controls
        bool isMenuAction = false;
        foreach (Control control in menuElem.Controls)
        {
            if (control == invokeControl && control != menuElem.FindControl("btnSave"))
            {
                isMenuAction = true;
            }
        }

        // Load content to text fields
        if (!RequestHelper.IsPostBack() || invokeControl == drpEditControl || isMenuAction)
        {
            txtAreaContent.Text = content;
            htmlContent.ResolvedValue = content;
            txtContent.Text = content;
        }
        if (menuElem.AllowEdit && menuElem.AllowSave)
        {
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
                "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>"
            );
            pnlEditableContent.Enabled = true;
            htmlContent.Enabled = true;
            LoadImageData(ViewModeEnum.Edit);
        }
        else
        {
            pnlEditableContent.Enabled = false;
            htmlContent.Enabled = false;
            LoadImageData(ViewModeEnum.EditDisabled);
        }
        lblWorkflow.Text = menuElem.WorkflowInfo;
        lblWorkflow.Visible = !string.IsNullOrEmpty(lblWorkflow.Text);
        base.OnPreRender(e);
    }

    #endregion


    #region "Button handling"

    /// <summary>
    /// Checkes in node
    /// </summary>
    protected void btnCheckIn_Click(object sender, EventArgs e)
    {
        // Do validation
        if (!ValidateEntries())
        {
            return;
        }

        // Do save
        btnSave_Click(sender, e);

        // Check in
        if (node != null)
        {
            VersionManager verMan = new VersionManager(tree);

            // Check in the document        
            verMan.CheckIn(node, null, null);

            lblInfo.Text += " " + ResHelper.GetString("ContentEdit.WasCheckedIn");
        }
        // Reload menu
        menuElem.ReloadMenu();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        EditingForms editingForm = (EditingForms)Convert.ToInt32(drpEditControl.SelectedValue);
        // Get content to save
        switch (editingForm)
        {
            case EditingForms.TextArea:
                content = txtAreaContent.Text.Trim();
                break;

            case EditingForms.HTMLEditor:
                content = htmlContent.ResolvedValue;
                if (String.IsNullOrEmpty(content))
                {
                    content = "<br />";
                }
                break;

            case EditingForms.EditableImage:
                content = imageContent.GetContent();
                break;

            case EditingForms.TextBox:
                content = txtContent.Text.Trim();
                break;
        }

        if (!ValidateEntries())
        {
            return;
        }

        if (!QueryHelper.GetBoolean("imagesaved", false))
        {
            createNew = !node.DocumentContent.EditableWebParts.Contains(keyName) &&
                        !node.DocumentContent.EditableRegions.ContainsKey(keyName);
        }

        // Set PageInfo
        switch (keyType)
        {
            case EditableContentType.webpart:
                if (!createNew)
                {
                    // If editing -> remove old
                    node.DocumentContent.EditableWebParts.Remove(keyName);
                }

                if (!node.DocumentContent.EditableWebParts.ContainsKey(txtName.Text.Trim()))
                {
                    node.DocumentContent.EditableWebParts.Add(txtName.Text.Trim(), content);
                }
                else
                {
                    // Set error label
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("EditableContent.ItemExists");
                    return;
                }
                break;

            case EditableContentType.region:
                if (!createNew)
                {
                    // If editing -> remove old
                    node.DocumentContent.EditableRegions.Remove(keyName);
                }

                if (!node.DocumentContent.EditableRegions.ContainsKey(txtName.Text.Trim()))
                {
                    node.DocumentContent.EditableRegions.Add(txtName.Text.Trim(), content);
                }
                else
                {
                    // Set error label
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("EditableContent.ItemExists");
                    return;
                }
                break;
        }

        // Save node
        SaveNode(false);

        if (txtName.Text != string.Empty)
        {
            keyName = txtName.Text.Trim();
        }

        // Reload menu
        menuElem.ReloadMenu();

        // Inform user
        lblInfo.Text = ResHelper.GetString("general.changessaved");
        lblInfo.Visible = true;

        // Refresh tree
        if (createNew)
        {
            ltlScript.Text +=
                ScriptHelper.GetScript("parent.frames['tree'].location.replace('" +
                                       ResolveUrl("~/CMSModules/Content/CMSDesk/Properties/Advanced/EditableContent/tree.aspx") + "?nodeid=" +
                                       nodeId + "&selectednodename=" + txtName.Text.Trim() + "&selectednodetype=" + keyType +
                                       "');");
            if (editingForm == EditingForms.EditableImage)
            {
                ltlScript.Text +=
                    ScriptHelper.GetScript("SelectNodeAfterImageSave(" + ScriptHelper.GetString(keyName) + ", '" + keyType + "');");
            }
        }
        else
        {
            ltlScript.Text +=
                ScriptHelper.GetScript("RefreshNode(" + ScriptHelper.GetString(txtName.Text.Trim()) + ", '" + keyType + "', " + nodeId + ");");
        }
        createNew = false;
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Validates inputs
    /// </summary>
    /// <returns>True if valid</returns>
    private bool ValidateEntries()
    {
        // Validate
        string errorMessage = new Validator().NotEmpty(txtName.Text.Trim(), ResHelper.GetString("general.invalidcodename")).IsRegularExp(txtName.Text.Trim(), ValidationHelper.CodenameRegExp.ToString(), ResHelper.GetString("general.invalidcodename")).Result;
        if (errorMessage != string.Empty)
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
            return false;
        }
        return true;
    }


    /// <summary>
    /// Saves node, ensures workflow
    /// </summary>
    protected void SaveNode(bool forceCheckout)
    {
        // Get content
        content = node.DocumentContent.GetContentXml();
        // Save content
        if (node != null)
        {
            bool autoCheck = !VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName);
            VersionManager vm = null;
            WorkflowManager workflowMan = new WorkflowManager(tree);
            bool useWorkflow = workflowMan.GetNodeWorkflowScope(node) != null;

            // If not using check-in/check-out, check out automatically
            if (autoCheck & useWorkflow || forceCheckout)
            {
                if (node.GetValue("DocumentCheckedOutByUserID") == null)
                {
                    // Check out
                    vm = new VersionManager(tree);
                    vm.CheckOut(node);
                }
            }
            node.UpdateOriginalValues();

            node.SetValue("DocumentContent", content);
            DocumentHelper.UpdateDocument(node, tree);

            // Check in the document
            if ((autoCheck & useWorkflow) && vm != null)
            {
                vm.CheckIn(node, null, null);
            }
        }
    }


    /// <summary>
    /// Loads data to editable image
    /// </summary>
    protected void LoadImageData(ViewModeEnum mode)
    {
        // Set view mode
        PortalContext.ViewMode = mode;
        // Initialize editable image properties
        imageContent.DisplaySelectorTextBox = false;
        imageContent.SelectOnlyPublished = false;
        // Ensure loading image
        if (!string.IsNullOrEmpty(content))
        {
            if (content.StartsWith("<image>"))
            {
                // Initialize editable image
                imageContent.LoadContent(content);
            }
        }
    }


    /// <summary>
    /// Initializes HTML editor's settings
    /// </summary>
    protected void InitHTMLEditor()
    {
        htmlContent.AutoDetectLanguage = false;
        htmlContent.DefaultLanguage = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        // Set direction
        htmlContent.Config["ContentLangDirection"] = "ltr";
        if (CultureHelper.IsPreferredCultureRTL())
        {
            htmlContent.Config["ContentLangDirection"] = "rtl";
        }
        if (CMSContext.CurrentSite != null)
        {
            htmlContent.EditorAreaCSS = FormHelper.GetHtmlEditorAreaCss(CMSContext.CurrentSiteName);
        }
    }


    /// <summary>
    /// Initializes dropdown list with editing options
    /// </summary>
    protected void InitEditorOptions()
    {
        drpEditControl.Items.AddRange(new ListItem[] {
                new ListItem(ResHelper.GetString("EditableContent.HTMLEditor"), Convert.ToInt32(EditingForms.HTMLEditor).ToString()),
                new ListItem(ResHelper.GetString("EditableContent.EditableImage"), Convert.ToInt32(EditingForms.EditableImage).ToString()), 
                new ListItem(ResHelper.GetString("EditableContent.TextArea"), Convert.ToInt32(EditingForms.TextArea).ToString()) ,
                new ListItem(ResHelper.GetString("EditableContent.TextBox"),Convert.ToInt32(EditingForms.TextBox).ToString() )});
    }


    /// <summary>
    /// Checks whether current user is authorized to modify editable content.
    /// </summary>
    /// <returns>True if authorized.</returns>
    protected static bool IsAuthorizedToModify()
    {
        return (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Allowed);
    }

    #endregion
}
