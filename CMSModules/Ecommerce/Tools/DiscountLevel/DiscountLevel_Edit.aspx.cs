using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_DiscountLevel_DiscountLevel_Edit : CMSEcommercePage
{
	protected int discountLevelId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // control initializations				
        lblDiscountLevelValidFrom.Text = ResHelper.GetString("DiscountLevel_Edit.DiscountLevelValidFromLabel");
        lblDiscountLevelValue.Text = ResHelper.GetString("DiscountLevel_Edit.DiscountLevelValueLabel");
        lblDiscountLevelValidTo.Text = ResHelper.GetString("DiscountLevel_Edit.DiscountLevelValidToLabel");
        // Init Validators
        rfvDiscountLevelDisplayName.ErrorMessage = ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelDisplayName.ErrorMessage");
        rfvDiscountLevelName.ErrorMessage = ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelName.ErrorMessage");
        rfvDiscountLevelValue.ErrorMessage = ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelValue.ErrorMessage");

        btnOk.Text = ResHelper.GetString("General.OK");
        dtPickerDiscountLevelValidFrom.SupportFolder = "~/CMSAdminControls/Calendar";
        dtPickerDiscountLevelValidTo.SupportFolder = "~/CMSAdminControls/Calendar";

        string currentDiscountLevel = "";

        // get discountlLevel id from querystring		
        discountLevelId = ValidationHelper.GetInteger(Request.QueryString["discountLevelId"], 0);
        // edit existing
        if (discountLevelId > 0)
        {
            // check if there isnt already duplicate entry
            DiscountLevelInfo discountLevelObj = DiscountLevelInfoProvider.GetDiscountLevelInfo(discountLevelId);
            if (discountLevelObj != null)
            {
                currentDiscountLevel = discountLevelObj.DiscountLevelDisplayName;

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(discountLevelObj);

                    // show that the discountlLevel was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
        }
    }


	/// <summary>
	/// Load data of editing discountLevel.
	/// </summary>
	/// <param name="discountlLevelObj">DiscountLevel object.</param>
    protected void LoadData(DiscountLevelInfo discountLevelObj)
    {
        dtPickerDiscountLevelValidFrom.SelectedDateTime = discountLevelObj.DiscountLevelValidFrom;
        chkDiscountLevelEnabled.Checked = discountLevelObj.DiscountLevelEnabled;
        txtDiscountLevelName.Text = discountLevelObj.DiscountLevelName;
        txtDiscountLevelValue.Text = Convert.ToString(discountLevelObj.DiscountLevelValue);
        dtPickerDiscountLevelValidTo.SelectedDateTime = discountLevelObj.DiscountLevelValidTo;
        txtDiscountLevelDisplayName.Text = discountLevelObj.DiscountLevelDisplayName;
    }


	/// <summary>
	/// Sets data to database.
	/// </summary>
	protected void btnOK_Click(object sender, EventArgs e)
	{
        // check 'EcommerceRead' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        string errorMessage = new Validator().NotEmpty(txtDiscountLevelDisplayName.Text, ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelDisplayName.ErrorMessage"))
                                             .NotEmpty(txtDiscountLevelName.Text, ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelName.ErrorMessage"))
                                             .NotEmpty(txtDiscountLevelValue.Text, ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelValue.ErrorMessage")).Result;
        
        // Is discount level name in code name format?
        if (errorMessage == "")
        {
            if (!ValidationHelper.IsCodeName(txtDiscountLevelName.Text))
            {
                errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
            }
        }

        // Discount level value must be between 0-100
        if (errorMessage == "")
        {
            double value = ValidationHelper.GetDouble(txtDiscountLevelValue.Text.Trim(), -1);
            if ((value < 0) || (value > 100))
            {
                errorMessage = ResHelper.GetString("DiscountLevel_Edit.rvDiscountLevelValue.ErrorMessage");
            }
        }

        // from/to date validation
        if (errorMessage == "")
        {
            if ((dtPickerDiscountLevelValidFrom.SelectedDateTime != DateTime.MinValue) &&
            (dtPickerDiscountLevelValidTo.SelectedDateTime != DateTime.MinValue) &&
            (dtPickerDiscountLevelValidFrom.SelectedDateTime >= dtPickerDiscountLevelValidTo.SelectedDateTime))
            {
                errorMessage = ResHelper.GetString("General.DateOverlaps");
            }
        }

        if (errorMessage == "")
        {
            // Check unique name
            DiscountLevelInfo discountLevelObj = DiscountLevelInfoProvider.GetDiscountLevelInfo(txtDiscountLevelName.Text.Trim());
            if ((discountLevelObj == null) || (discountLevelObj.DiscountLevelID == discountLevelId))
            {
                // Get the object
                if (discountLevelObj == null)
                {
                    discountLevelObj = DiscountLevelInfoProvider.GetDiscountLevelInfo(discountLevelId);
                    if (discountLevelObj == null)
                    {
                        discountLevelObj = new DiscountLevelInfo();
                    }
                }

                // Set values
                discountLevelObj.DiscountLevelValidFrom = dtPickerDiscountLevelValidFrom.SelectedDateTime;
                discountLevelObj.DiscountLevelEnabled = chkDiscountLevelEnabled.Checked;
                discountLevelObj.DiscountLevelName = txtDiscountLevelName.Text.Trim();
                discountLevelObj.DiscountLevelValue = Convert.ToDouble(txtDiscountLevelValue.Text.Trim());
                discountLevelObj.DiscountLevelValidTo = dtPickerDiscountLevelValidTo.SelectedDateTime;
                discountLevelObj.DiscountLevelDisplayName = txtDiscountLevelDisplayName.Text.Trim();

                // Save to the database
                DiscountLevelInfoProvider.SetDiscountLevelInfo(discountLevelObj);

                UrlHelper.Redirect("DiscountLevel_Edit.aspx?discountLevelId=" + Convert.ToString(discountLevelObj.DiscountLevelID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("DiscountLevel_Edit.DiscountLevelNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
	}
}
