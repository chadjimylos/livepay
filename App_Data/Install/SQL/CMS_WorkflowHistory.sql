CREATE TABLE [CMS_WorkflowHistory] (
		[WorkflowHistoryID]     int NOT NULL IDENTITY(1, 1),
		[VersionHistoryID]      int NOT NULL,
		[StepID]                int NULL,
		[StepDisplayName]       nvarchar(450) NOT NULL,
		[ApprovedByUserID]      int NULL,
		[ApprovedWhen]          datetime NULL,
		[Comment]               nvarchar(max) NULL,
		[WasRejected]           bit NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WorkflowHistory]
	ADD
	CONSTRAINT [PK_CMS_WorkflowHistory]
	PRIMARY KEY
	([WorkflowHistoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_WorkflowHistory]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WorkflowHistory_ApprovedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_WorkflowHistory]
			ADD CONSTRAINT [FK_CMS_WorkflowHistory_ApprovedByUserID_CMS_User]
			FOREIGN KEY ([ApprovedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[CMS_WorkflowHistory]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WorkflowStep]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WorkflowHistory_StepID_CMS_WorkflowStep]') IS NULL
BEGIN
		ALTER TABLE [CMS_WorkflowHistory]
			ADD CONSTRAINT [FK_CMS_WorkflowHistory_StepID_CMS_WorkflowStep]
			FOREIGN KEY ([StepID]) REFERENCES [CMS_WorkflowStep] ([StepID])
END
IF OBJECT_ID(N'[CMS_WorkflowHistory]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_VersionHistory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WorkflowHistory_VersionHistoryID_CMS_VersionHistory]') IS NULL
BEGIN
		ALTER TABLE [CMS_WorkflowHistory]
			ADD CONSTRAINT [FK_CMS_WorkflowHistory_VersionHistoryID_CMS_VersionHistory]
			FOREIGN KEY ([VersionHistoryID]) REFERENCES [CMS_VersionHistory] ([VersionHistoryID])
END
CREATE INDEX [IX_CMS_WorkflowHistory_ApprovedByUserID]
	ON [CMS_WorkflowHistory] ([ApprovedByUserID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WorkflowHistory_ApprovedWhen]
	ON [CMS_WorkflowHistory] ([ApprovedWhen])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WorkflowHistory_StepID]
	ON [CMS_WorkflowHistory] ([StepID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WorkflowHistory_VersionHistoryID]
	ON [CMS_WorkflowHistory] ([VersionHistoryID])
	ON [PRIMARY]
