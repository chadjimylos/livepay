<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollView.ascx.cs" Inherits="CMSModules_Polls_Controls_PollView" %>

<asp:Panel runat="server" ID="pnlControl" CssClass="PollControl" DefaultButton="btnVote" EnableViewState="false">
    <cms:LocalizedLabel runat="server" CssClass="PollTitle" ID="lblTitle" EnableViewState="false" />
    <cms:LocalizedLabel runat="server" ID="lblQuestion" CssClass="PollQuestion" EnableViewState="false" />
    <asp:Panel runat="server" ID="pnlAnswer" CssClass="PollAnswers" />
    <asp:Panel runat="server" ID="pnlFooter" CssClass="PollFooter" EnableViewState="false">
        <cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="PollInfo" EnableViewState="false" />
        <cms:LocalizedButton runat="server" ID="btnVote" CssClass="PollVoteButton" OnClick="btnVote_OnClick" />
    </asp:Panel>
</asp:Panel>
