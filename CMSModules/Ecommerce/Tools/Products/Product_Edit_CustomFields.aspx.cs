using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.FormControls;
using CMS.UIControls;
using CMS.Ecommerce;

public partial class CMSModules_Ecommerce_Tools_Products_Product_Edit_CustomFields : CMSEcommercePage
{
    protected int productId = 0;


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // set edit mode
        productId = ValidationHelper.GetInteger(Request.QueryString["productId"], 0);
        if (productId > 0)
        {
            formProductCustomFields.Info = SKUInfoProvider.GetSKUInfo(productId);
            formProductCustomFields.OnBeforeSave +=new DataForm.OnBeforeSaveEventHandler(formProductCustomFields_OnBeforeSave);
            formProductCustomFields.OnAfterSave +=new DataForm.OnAfterSaveEventHandler(formProductCustomFields_OnAfterSave);
        }
        else
        {
            formProductCustomFields.Enabled = false;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (formProductCustomFields.BasicForm != null)
        {
            if (formProductCustomFields.BasicForm.FieldControls.Count <= 0)
            {
                formProductCustomFields.BasicForm.SubmitButton.Visible = false;
            }
            else
            {
                formProductCustomFields.BasicForm.SubmitButton.CssClass = "ContentButton";
            }
        }
    }


    void formProductCustomFields_OnBeforeSave()
    {
        // Check 'EcommerceModify' permission
        if ((CMSContext.CurrentSite != null) && (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify")))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }
    }


    void formProductCustomFields_OnAfterSave()
    {
        // Display 'changes saved' information
        this.lblInfo.Visible = true;
    }
}
