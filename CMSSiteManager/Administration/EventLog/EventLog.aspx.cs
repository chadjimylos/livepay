using System;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_EventLog_EventLog : CMSEventLogPage
{
    #region "Protectetd variables"

    protected int siteId = -1;
    protected EventLogProvider eventProvider = new EventLogProvider();

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        // Setup page title text and image
        CurrentMaster.Title.TitleText = ResHelper.GetString("EventLogList.Header");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_EventLog/object.png");

        CurrentMaster.Title.HelpTopicName = "list_of_events";
        CurrentMaster.Title.HelpName = "helpTopic";

        lblSite.Text = ResHelper.GetString("EventLogList.Label");

        gridEvents.OnDataReload += new UniGrid.OnDataReloadEventHandler(gridEvents_OnDataReload);
        gridEvents.GridView.RowDataBound += GridView_RowDataBound;
        gridEvents.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridEvents_OnExternalDataBound);

        if (!RequestHelper.IsPostBack())
        {
            gridEvents.OrderBy = "EventTime DESC, EventID DESC";
        }

        btnClearLog.Text = ResHelper.GetString("EventLogList.Clear");
        btnClearLog.Attributes.Add("onclick", "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("EventLogList.ClearAllConfirmation")) + ");");

        siteId = QueryHelper.GetInteger("siteid", 0);
        //if (siteId > 0)
        //{
        //    pnlSites.Visible = false;
        //}
        //else
        //{
            // Set site selector
            siteSelector.DropDownSingleSelect.AutoPostBack = true;
            siteSelector.OnlyRunningSites = false;
            siteSelector.AllowAll = true;
            siteSelector.UniSelector.SpecialFields = new string[1, 2] { { ResHelper.GetString("EventLogList.GlobalEvents"), "0" } };
            siteSelector.UniSelector.OnSelectionChanged += UniSelector_OnSelectionChanged;

            if (!RequestHelper.IsPostBack())
            {
                // Preselect (all events)
                siteId = -1;
            }
            else
            {
                siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
            }
        //}

        cntFilter.SiteID = siteId;
        gridEvents.WhereCondition = cntFilter.WhereCondition;
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        // If events grid not empty allow clear log button
        btnClearLog.Enabled = (gridEvents.GridView.Rows.Count != 0);
        if (btnClearLog.Enabled)
        {
            btnClearLog.Attributes.Add("onclick", "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("EventLogList.ClearConfirmation")) + ");");
        }
    }

    #endregion


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        // Update grid
        pnlUpdate.Update();
    }


    /// <summary>
    /// Deletes event logs from DB.
    /// </summary>
    protected void ClearLogButton_Click(object sender, EventArgs e)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.EventLog", "ClearLog"))
        {
            RedirectToAccessDenied("CMS.EventLog", "ClearLog");
        }

        GeneralConnection genConn = ConnectionHelper.GetConnection();
        EventLogProvider eventProvider = new EventLogProvider(genConn);
        UserInfo ui = CMSContext.CurrentUser;

        // Deletes event logs of specific site from DB
        eventProvider.ClearEventLog(ui.UserID, ui.UserName, Request.UserHostAddress, siteId);

        gridEvents.ReloadData();
    }


    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string color = null;
            string code = ValidationHelper.GetString(((DataRowView)(e.Row.DataItem)).Row["EventType"], string.Empty);
            switch (code.ToLower())
            {
                case "e":
                    if ((e.Row.RowIndex & 1) == 1)
                    {
                        color = "#EEC9C9";
                    }
                    else
                    {
                        color = "#FFDADA";
                    }
                    break;

                case "w":
                    if ((e.Row.RowIndex & 1) == 1)
                    {
                        color = "#EEEEC9";
                    }
                    else
                    {
                        color = "#FFFFDA";
                    }
                    break;
            }

            if (!string.IsNullOrEmpty(color))
            {
                e.Row.Style.Add("background-color", color);
            }
        }
    }


    protected DataSet gridEvents_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        string columns = "EventID,EventType,EventTime,Source,EventCode,UserName,IPAddress,DocumentName,SiteDisplayName,EventMachineName,ROW_NUMBER() OVER (ORDER BY " + currentOrder + ") AS RowNumber";
        return eventProvider.GetAllEvents(completeWhere, currentOrder, 0, columns, currentOffset, currentPageSize, ref totalRecords);
    }


    protected object gridEvents_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        if (sourceName == "eventtype")
        {
            string evetType = ValidationHelper.GetString(parameter, "");
            return "<div style=\"width:100%;text-align:center;cursor:help;\" title=\"" + HTMLHelper.HTMLEncode(EventLogHelper.GetEventTypeText(evetType)) + "\">" + evetType + " </div>";
        }
        else if (sourceName == "formattedusername")
        {
            return HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(Convert.ToString(parameter)));
        }
        else if (sourceName == "edit")
        {
            string caption = ResHelper.GetString("Unigrid.EventLog.Actions.Display");
            string imageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/View.png");
            string url = ResolveUrl("~/CMSSiteManager/Administration/EventLog/EventLog_Details.aspx");
            string whereCondition = HttpUtility.UrlPathEncode(this.gridEvents.WhereCondition).Replace("&", "%26").Replace("#", "%23").Replace("'", "\'");

            url = UrlHelper.AddParameterToUrl(url, "orderby", this.gridEvents.SortDirect);
            url = UrlHelper.AddParameterToUrl(url, "where", whereCondition);
            if (siteId > 0)
            {
                url = UrlHelper.AddParameterToUrl(url, "siteid", siteId.ToString());
            }
            url = UrlHelper.AddParameterToUrl(url, "hash", QueryHelper.GetHash(url, false));
            url = UrlHelper.AddParameterToUrl(url, "eventid", Convert.ToString(parameter)); 

            string action = "modalDialog(" + ScriptHelper.GetString(url) + ", 'eventdetails', 920, 700);";

            return "<input type=\"image\" style=\"border-width: 0px; margin: 0px 3px;\" alt=\"" + caption +
                "\" src=\"" + imageUrl + "\" class=\"UnigridActionButton\" title=\"" + caption + "\" onclick=\"" + action + "\">";
        }

        return parameter;
    }
}
