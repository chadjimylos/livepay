CREATE TABLE [CMS_RelationshipNameSite] (
		[RelationshipNameID]     int NOT NULL,
		[SiteID]                 int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_RelationshipNameSite]
	ADD
	CONSTRAINT [PK_CMS_RelationshipNameSite]
	PRIMARY KEY
	([RelationshipNameID], [SiteID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_RelationshipNameSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_RelationshipName]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_RelationshipNameSite_RelationshipNameID_CMS_RelationshipName]') IS NULL
BEGIN
		ALTER TABLE [CMS_RelationshipNameSite]
			ADD CONSTRAINT [FK_CMS_RelationshipNameSite_RelationshipNameID_CMS_RelationshipName]
			FOREIGN KEY ([RelationshipNameID]) REFERENCES [CMS_RelationshipName] ([RelationshipNameID])
END
IF OBJECT_ID(N'[CMS_RelationshipNameSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_RelationshipNameSite_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_RelationshipNameSite]
			ADD CONSTRAINT [FK_CMS_RelationshipNameSite_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
