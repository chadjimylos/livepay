<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HOL.ascx.cs" Inherits="CMSFormControls_HOL" %>
<script type="text/javascript">
    function checkHOLCode(sender, args) {
        var transID = args.Value;
        var bIsValid = false;

        var RegExp_TransID = new RegExp(/^(9|1){1}\d{12}$/);
        
        if (RegExp_TransID.test(transID))
            bIsValid = true;
            
        var IsCutomValid = sender.getAttribute('IsCustomValidator');
        var CustomErrorMess = sender.getAttribute('CustomValidatorErrorMessage');
        if (IsCutomValid != null) {
            if (IsCutomValid.indexOf('CustomCloud_') != -1) {
                if (!bIsValid) {
                    Tab1_ValidationCloud('#' + IsCutomValid, CustomErrorMess, true, '')
                } else {
                    Tab1_ValidationCloud('#' + IsCutomValid, '', false, '')
                }
            }

        }
        args.IsValid = bIsValid;
    }
</script>

<div runat="server" id="CustomCloud_div"></div>

<asp:TextBox runat="server" ID="HOLcode" cssclass="RegUsersTab1CustomTxt" width="260px" MaxLength="13" />

<div class="HideIt">
    <asp:CustomValidator ID="cv_HOLcode" 
                        ClientValidationFunction="checkHOLCode" runat="server"   
                        ControlToValidate="HOLcode" ValidationGroup="Tab1_PayForm" />
</div>