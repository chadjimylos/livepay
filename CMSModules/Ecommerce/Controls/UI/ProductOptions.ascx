<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductOptions.ascx.cs"
    Inherits="CMSModules_Ecommerce_Controls_UI_ProductOptions" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<asp:Panel ID="pnlBody" runat="server" CssClass="PageBody">
    <asp:Panel ID="pblContent" runat="server" CssClass="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <strong>
            <asp:Label runat="server" ID="lblAvailable" CssClass="InfoLabel" EnableViewState="false" /></strong>
        <cms:UniSelector ID="uniSelector" runat="server" IsLiveSite="false" ObjectType="ecommerce.optioncategory"
            SelectionMode="Multiple" ResourcePrefix="optioncategoryselect" />
    </asp:Panel>
</asp:Panel>
