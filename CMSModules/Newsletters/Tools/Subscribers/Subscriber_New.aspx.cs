using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Newsletters_Tools_Subscribers_Subscriber_New : CMSNewsletterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Title.HelpTopicName = "new_subscriber";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Controls' initialization
        lblSubscriberLastName.Text = ResHelper.GetString("Subscriber_Edit.SubscriberLastNameLabel");
        lblSubscriberFirstName.Text = ResHelper.GetString("Subscriber_Edit.SubscriberFirstNameLabel");
        btnOk.Text = ResHelper.GetString("General.OK");
        rfvSubscriberEmail.ErrorMessage = ResHelper.GetString("Subscriber_Edit.ErrorEmptyEmail");

        // Initialize page title control
        string[,] tabs = new string[2, 3];
        tabs[0, 0] = ResHelper.GetString("Subscriber_Edit.ItemListLink");
        tabs[0, 1] = "~/CMSModules/Newsletters/Tools/Subscribers/Subscriber_List.aspx";
        tabs[0, 2] = "newslettersContent";
        tabs[1, 0] = ResHelper.GetString("Subscriber_Edit.NewItemCaption");
        tabs[1, 1] = "";
        tabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = tabs;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check "Manage subscribers" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "managesubscribers"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "managesubscribers");
        }

        // Check entered values
        string errorMessage = new Validator().NotEmpty(txtSubscriberEmail.Text, ResHelper.GetString("Subscriber_Edit.ErrorEmptyEmail"))
            .IsEmail(txtSubscriberEmail.Text, ResHelper.GetString("NewsletterSubscription.ErrorInvalidEmail"))
            .Result;

        if (errorMessage == "")
        {
            if (!SubscriberProvider.EmailExists(txtSubscriberEmail.Text.Trim()))
            {
                // Create new subscriber info and set values
                Subscriber subscriberObj = new Subscriber();

                subscriberObj.SubscriberID = 0;
                subscriberObj.SubscriberLastName = txtSubscriberLastName.Text.Trim();
                subscriberObj.SubscriberEmail = txtSubscriberEmail.Text.Trim();
                subscriberObj.SubscriberFirstName = txtSubscriberFirstName.Text.Trim();
                subscriberObj.SubscriberFullName = subscriberObj.SubscriberFirstName + " " + subscriberObj.SubscriberLastName;
                subscriberObj.SubscriberSiteID = CMSContext.CurrentSite.SiteID;
                subscriberObj.SubscriberGUID = Guid.NewGuid();

                if (SubscriberProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Subscribers, VersionActionEnum.Insert))
                {
                    // Save subscriber to DB
                    SubscriberProvider.SetSubscriber(subscriberObj);
                    // Redirect to edit page
                    UrlHelper.Redirect("Subscriber_Frameset.aspx?subscriberid=" + Convert.ToString(subscriberObj.SubscriberID) + "&saved=1");
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("LicenseVersionCheck.Subscribers");
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Subscriber_Edit.EmailAlreadyExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}