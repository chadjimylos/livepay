using System;
using System.Web.UI.WebControls;

using CMS.CMSImportExport;
using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.CMSHelper;
using CMS.TreeEngine;

public partial class CMSModules_AbuseReport_AbuseReport_Edit : CMSAbuseReportPage
{
    #region "Private variables"

    private AbuseReportInfo mCurrentReport = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Gets ID of current report
    /// </summary>
    private static int ReportId
    {
        get
        {
            return QueryHelper.GetInteger("reportid", 0);
        }
    }


    /// <summary>
    /// Gets current report
    /// </summary>
    private AbuseReportInfo CurrentReport
    {
        get
        {
            if (mCurrentReport == null)
            {
                mCurrentReport = AbuseReportInfoProvider.GetAbuseReportInfo(ReportId);
            }
            return mCurrentReport;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set the page title
        CurrentMaster.Title.TitleText = ResHelper.GetString("abuse.properties");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_AbuseReport/object.png");
        CurrentMaster.Title.HelpTopicName = "abusereport";

        ReloadData();

        btnOk.Click += btnOK_Click;
        rfvText.ErrorMessage = ResHelper.GetString("abuse.textreqired");
    }

    #endregion


    #region "Other events"

    /// <summary>
    /// Button OK click event handler.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check permissions
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.AbuseReport", "Manage"))
        {
            RedirectToAccessDenied("CMS.AbuseReport", "Manage");
        }

        // Check that text area is not empty
        txtCommentValue.Text = txtCommentValue.Text.Trim();
        if (txtCommentValue.Text.Length > 1000)
        {
            txtCommentValue.Text = txtCommentValue.Text.Substring(0, 1000);
        }

        if (txtCommentValue.Text.Length > 0)
        {
            // Load new values
            CurrentReport.ReportComment = txtCommentValue.Text;
            CurrentReport.ReportStatus = (AbuseReportStatusEnum)ValidationHelper.GetInteger(drpStatus.SelectedValue, 0);

            // Save AbuseReport
            AbuseReportInfoProvider.SetAbuseReportInfo(CurrentReport);
            lblSaved.Visible = true;
        }
        else
        {
            lblError.Visible = true;
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Reloads all data.
    /// </summary>
    private void ReloadData()
    {
        // Initializes page breadcrumbs
        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("abuse.reports");
        breadcrumbs[0, 1] = "~/CMSModules/AbuseReport/AbuseReport_List.aspx";

        if (CurrentReport != null)
        {
            // Set breadcrumbs
            breadcrumbs[1, 0] = lblTitleValue.Text = HTMLHelper.HTMLEncode(CurrentReport.ReportTitle);

            // Load labels
            if (!RequestHelper.IsPostBack())
            {
                // Create query parameters
                string query = "?ObjectID=" + CurrentReport.ReportObjectID;

                // Set link value
                string url = CurrentReport.ReportURL;
                if (CurrentReport.ReportCulture != String.Empty)
                {
                    url = UrlHelper.AddParameterToUrl(url, "lang", CurrentReport.ReportCulture);
                }
                lnkUrlValue.Text = HTMLHelper.HTMLEncode(url);
                lnkUrlValue.NavigateUrl = url;
                lnkUrlValue.ToolTip = HTMLHelper.HTMLEncode(url);
                lnkUrlValue.Target = "_blank";

                // Set culture value
                System.Globalization.CultureInfo ci = CultureHelper.GetCultureInfo(CurrentReport.ReportCulture);
                lblCultureValue.Text = (ci != null) ? ci.DisplayName : "-";

                // Set site value
                SiteInfo si = SiteInfoProvider.GetSiteInfo(CurrentReport.ReportSiteID);
                lblSiteValue.Text = (si != null) ? si.DisplayName : "-";

                // Set labels
                if (!string.IsNullOrEmpty(CurrentReport.ReportObjectType))
                {
                    lblObjectTypeValue.Text = ResHelper.GetString("ObjectType." + ImportExportHelper.GetSafeObjectTypeName(CurrentReport.ReportObjectType));
                    query += "&ObjectType=" + CurrentReport.ReportObjectType;
                    if ((CurrentReport.ReportObjectID > 0) && (CurrentReport.ReportObjectType.ToLower() != TreeObjectType.DOCUMENT))
                    {
                        pnlLink.Visible = true;
                    }
                }
                else
                {
                    lblObjectTypeValue.Text = "-";
                }

                // Get object display name
                lblObjectNameValue.Text = "-";
                if ((CurrentReport.ReportObjectID > 0) && (!string.IsNullOrEmpty(CurrentReport.ReportObjectType)))
                {
                    IInfoObject info = CMSObjectHelper.GetObject(CurrentReport.ReportObjectType);
                    if ((info != null) && (CurrentReport.ReportObjectType.ToLower() != TreeObjectType.DOCUMENT.ToLower()))
                    {
                        IInfoObject obj = info.GetObject(CurrentReport.ReportObjectID);
                        if ((obj != null) && !string.IsNullOrEmpty(obj.ObjectDisplayName))
                        {
                            lblObjectNameValue.Text = HTMLHelper.HTMLEncode(obj.ObjectDisplayName);
                        }
                    }
                }

                // Set Reported by label
                lblReportedByValue.Text = "-";
                if (CurrentReport.ReportUserID != 0)
                {
                    UserInfo ui = UserInfoProvider.GetUserInfo(CurrentReport.ReportUserID);
                    lblReportedByValue.Text = (ui != null) ? HTMLHelper.HTMLEncode(ui.FullName) : ResHelper.GetString("general.NA");
                }

                // Set other parameters
                lblReportedWhenValue.Text = CurrentReport.ReportWhen.ToString();
                LoadStatus((int)CurrentReport.ReportStatus);
                txtCommentValue.Text = CurrentReport.ReportComment;

                if ((CurrentReport.ReportObjectID > 0) && (!string.IsNullOrEmpty(CurrentReport.ReportObjectType)) && AbuseReportInfoProvider.IsObjectTypeSupported(CurrentReport.ReportObjectType))
                {
                    lnkShowDetails.Visible = true;
                    lnkShowDetails.NavigateUrl = "~/CMSModules/AbuseReport/AbuseReport_ObjectDetails.aspx" + query;
                }
            }
        }

        CurrentMaster.Title.Breadcrumbs = breadcrumbs;
    }


    /// <summary>
    /// Loads status from enumeration to dropdown list.
    /// </summary>
    private void LoadStatus(int reportStatus)
    {
        drpStatus.Items.Add(new ListItem(ResHelper.GetString("general.new"), "0"));
        drpStatus.Items.Add(new ListItem(ResHelper.GetString("general.solved"), "1"));
        drpStatus.Items.Add(new ListItem(ResHelper.GetString("general.rejected"), "2"));
        drpStatus.SelectedValue = reportStatus.ToString();
    }

    #endregion
}
