using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Controls_MyDetails_MyCredit : CMSAdminControl
{
    private CurrencyInfo mDefaultCurrency = null;
    private int mCustomerId = 0;

    /// <summary>
    /// Site default currency object
    /// </summary>
    private CurrencyInfo DefaultCurrency
    {
        get
        {
            if (mDefaultCurrency == null)
            {
                mDefaultCurrency = CurrencyInfoProvider.GetCurrencyInfo(CMSContext.CurrentSite.SiteDefaultCurrencyID);
            }
            return mDefaultCurrency;
        }
    }


    /// <summary>
    /// Customer ID.
    /// </summary>
    public int CustomerId
    {
        get
        {
            return mCustomerId;
        }
        set
        {
            mCustomerId = value;
        }
    }


    /// <summary>
    /// If true, control does not process the data
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["StopProcessing"], false);
        }
        set
        {
            ViewState["StopProcessing"] = value;
        }
    }


    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.StopProcessing)
        {
            if (CMSContext.CurrentUser.IsAuthenticated())
            {
                gridCreditEvents.IsLiveSite = this.IsLiveSite;
                gridCreditEvents.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridCreditEvents_OnExternalDataBound);
                gridCreditEvents.OrderBy = "EventDate DESC, EventName ASC";
                gridCreditEvents.WhereCondition = "EventCustomerID = " + this.CustomerId;

                // Set total credit value
                lblCredit.Text = ResHelper.GetString("Ecommerce.MyCredit.TotalCredit");
                lblCreditValue.Text = DefaultCurrency != null ? String.Format(DefaultCurrency.CurrencyFormatString, CreditEventInfoProvider.GetCustomerTotalCredit(this.CustomerId)) : Convert.ToString(CreditEventInfoProvider.GetCustomerTotalCredit(this.CustomerId));
            }
            else
            {
                // Hide if user is not authenticated
                this.Visible = false;
            }
        }
    }

    protected object gridCreditEvents_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        // Show only date part from date-time value
        switch (sourceName.ToLower())
        {
            case "eventdate":
                DateTime date = ValidationHelper.GetDateTime(parameter, DataHelper.DATETIME_NOT_SELECTED);
                if (date != DataHelper.DATETIME_NOT_SELECTED)
                {
                    return date.ToShortDateString();
                }
                else
                {
                    return "";
                }

            case "eventcreditchange":
                return CurrencyInfoProvider.GetFormatedPrice(ValidationHelper.GetDouble(parameter, 0), DefaultCurrency);
        }

        return parameter;
    }


    /// <summary>
    /// Overriden SetValue - because of MyAccount webpart.
    /// </summary>
    /// <param name="propertyName">Name of the property to set</param>
    /// <param name="value">Value to set</param>
    public override void SetValue(string propertyName, object value)
    {
        base.SetValue(propertyName, value);

        switch (propertyName.ToLower())
        {
            case "customerid":
                this.CustomerId = ValidationHelper.GetInteger(value, 0);
                break;
        }
    }
}
