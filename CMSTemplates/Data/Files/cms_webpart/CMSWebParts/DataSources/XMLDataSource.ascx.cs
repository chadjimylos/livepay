using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI;

using CMS.Controls;
using CMS.PortalControls;
using CMS.CMSHelper;
using CMS.GlobalHelper;

public partial class CMSWebParts_DataSources_XMLDataSource : CMSAbstractWebPart
{
    #region "Properties"

    /// <summary>
    /// Gets or sets the XML URL
    /// </summary>
    public string XmlUrl
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("XmlUrl"), "");
        }
        set
        {
            this.SetValue("XmlUrl", value);
            srcXml.XmlUrl = value;
        }
    }


    /// <summary>
    /// Gets or sets WHERE condition.
    /// </summary>
    public string WhereCondition
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("WhereCondition"), "");
        }
        set
        {
            this.SetValue("WhereCondition", value);
            srcXml.WhereCondition = value;
        }
    }


    /// <summary>
    /// Gets or sets ORDER BY condition.
    /// </summary>
    public string OrderBy
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("OrderBy"), "");
        }
        set
        {
            this.SetValue("OrderBy", value);
            srcXml.OrderBy = value;
        }
    }


    /// <summary>
    /// Gets or sets top N selected documents.
    /// </summary>
    public int SelectTopN
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("SelectTopN"), 0);
        }
        set
        {
            this.SetValue("SelectTopN", value);
            srcXml.TopN = value;
        }
    }


    /// <summary>
    /// Gets or sets the source filter name
    /// </summary>
    public string FilterName
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("FilterName"), "");
        }
        set
        {
            this.SetValue("FilterName", value);
            srcXml.SourceFilterName = value;
        }
    }


    /// <summary>
    /// Gets or sets the cache item name
    /// </summary>
    public override string CacheItemName
    {
        get
        {
            return base.CacheItemName;
        }
        set
        {
            base.CacheItemName = value;
            this.srcXml.CacheItemName = value;
        }
    }


    /// <summary>
    /// Cache dependencies, each cache dependency on a new line
    /// </summary>
    public override string CacheDependencies
    {
        get
        {
            return ValidationHelper.GetString(base.CacheDependencies, this.srcXml.CacheDependencies);
        }
        set
        {
            base.CacheDependencies = value;
            this.srcXml.CacheDependencies = value;
        }
    }


    /// <summary>
    /// Gets or sets the cache minutes
    /// </summary>
    public override int CacheMinutes
    {
        get
        {
            return base.CacheMinutes;
        }
        set
        {
            base.CacheMinutes = value;
            this.srcXml.CacheMinutes = value;
        }
    }


    /// <summary>
    /// Gets or sets the name of dataset table which will be used as datasource
    /// </summary>
    public string TableName
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("TableName"), "");
        }
        set
        {
            this.SetValue("TableName", value);
            srcXml.TableName = value;
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler.
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes control properties.
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            this.srcXml.XmlUrl = this.XmlUrl;
            this.srcXml.WhereCondition = this.WhereCondition;
            this.srcXml.OrderBy = this.OrderBy;
            this.srcXml.TopN = this.SelectTopN;
            this.srcXml.FilterName = ValidationHelper.GetString(this.GetValue("WebPartControlID"), this.ClientID);
            this.srcXml.SourceFilterName = this.FilterName;
            this.srcXml.CacheItemName = this.CacheItemName;
            this.srcXml.CacheDependencies = this.CacheDependencies;
            this.srcXml.CacheMinutes = this.CacheMinutes;
            this.srcXml.TableName = this.TableName;
        }
    }


    /// <summary>
    /// Clears cache.
    /// </summary>
    public override void ClearCache()
    {
        this.srcXml.ClearCache();
    }

    #endregion
}
