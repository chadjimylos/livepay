using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Controls;
using CMS.TreeEngine;
using CMS.PortalEngine;

public partial class CMSWebParts_Maps_YahooMaps : CMSAbstractWebPart
{
    #region "Variables"

    private bool? mDetailMode = null;
    private bool loadScripts = true;
    private bool errorOccurred = false;
    private string outputHtml = "";
    private string mKey = "";
    private string mapName = null;
    private int marker = 0;

    #endregion


    #region "Document properties"

    /// <summary>
    /// Cache item name
    /// </summary>
    public override string CacheItemName
    {
        get
        {
            return base.CacheItemName;
        }
        set
        {
            base.CacheItemName = value;
            this.repItems.CacheItemName = value;
        }
    }


    /// <summary>
    /// Cache dependencies, each cache dependency on a new line
    /// </summary>
    public override string CacheDependencies
    {
        get
        {
            return ValidationHelper.GetString(base.CacheDependencies, this.repItems.CacheDependencies);
        }
        set
        {
            base.CacheDependencies = value;
            this.repItems.CacheDependencies = value;
        }
    }


    /// <summary>
    /// Cache minutes
    /// </summary>
    public override int CacheMinutes
    {
        get
        {
            return base.CacheMinutes;
        }
        set
        {
            base.CacheMinutes = value;
            this.repItems.CacheMinutes = value;
        }
    }


    /// <summary>
    /// Check permissions
    /// </summary>
    public bool CheckPermissions
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("CheckPermissions"), this.repItems.CheckPermissions);
        }
        set
        {
            this.SetValue("CheckPermissions", value);
        }
    }


    /// <summary>
    /// Class names
    /// </summary>
    public string ClassNames
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("Classnames"), this.repItems.ClassNames), this.repItems.ClassNames);
        }
        set
        {
            this.SetValue("ClassNames", value);
            this.repItems.ClassNames = value;
        }
    }


    /// <summary>
    /// Combine with default culture
    /// </summary>
    public bool CombineWithDefaultCulture
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("CombineWithDefaultCulture"), this.repItems.CombineWithDefaultCulture);
        }
        set
        {
            this.SetValue("CombineWithDefaultCulture", value);
            this.repItems.CombineWithDefaultCulture = value;
        }
    }


    /// <summary>
    /// Filter out duplicate documents
    /// </summary>
    public bool FilterOutDuplicates
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("FilterOutDuplicates"), this.repItems.FilterOutDuplicates);
        }
        set
        {
            this.SetValue("FilterOutDuplicates", value);
            this.repItems.FilterOutDuplicates = value;
        }
    }


    /// <summary>
    /// Culture code
    /// </summary>
    public string CultureCode
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("CultureCode"), this.repItems.CultureCode), this.repItems.CultureCode);
        }
        set
        {
            this.SetValue("CultureCode", value);
            this.repItems.CultureCode = value;
        }
    }


    /// <summary>
    /// Maximal relative level
    /// </summary>
    public int MaxRelativeLevel
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("MaxRelativeLevel"), this.repItems.MaxRelativeLevel);
        }
        set
        {
            this.SetValue("MaxRelativeLevel", value);
            this.repItems.MaxRelativeLevel = value;
        }
    }


    /// <summary>
    /// Order by clause
    /// </summary>
    public string OrderBy
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("OrderBy"), this.repItems.OrderBy), this.repItems.OrderBy);
        }
        set
        {
            this.SetValue("OrderBy", value);
            this.repItems.OrderBy = value;
        }
    }


    /// <summary>
    /// Nodes path 
    /// </summary>
    public string Path
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("Path"), null), null);
        }
        set
        {
            this.SetValue("Path", value);
            this.repItems.Path = value;
        }
    }


    /// <summary>
    /// Select only published nodes
    /// </summary>
    public bool SelectOnlyPublished
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("SelectOnlyPublished"), this.repItems.SelectOnlyPublished);
        }
        set
        {
            this.SetValue("SelectOnlyPublished", value);
            this.repItems.SelectOnlyPublished = value;
        }
    }


    /// <summary>
    /// Site name
    /// </summary>
    public string SiteName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("SiteName"), this.repItems.SiteName), this.repItems.SiteName);
        }
        set
        {
            this.SetValue("SiteName", value);
            this.repItems.SiteName = value;
        }
    }


    /// <summary>
    /// Where condition
    /// </summary>
    public string WhereCondition
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("WhereCondition"), this.repItems.WhereCondition);
        }
        set
        {
            this.SetValue("WhereCondition", value);
            this.repItems.WhereCondition = value;
        }
    }


    /// <summary>
    /// Select top N items
    /// </summary>
    public int SelectTopN
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("SelectTopN"), this.repItems.SelectTopN);
        }
        set
        {
            this.SetValue("SelectTopN", value);
            this.repItems.SelectTopN = value;
        }
    }

    #endregion


    #region "Relationships properties"

    /// <summary>
    /// Related node is on the left side
    /// </summary>
    public bool RelatedNodeIsOnTheLeftSide
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("RelatedNodeIsOnTheLeftSide"), this.repItems.RelatedNodeIsOnTheLeftSide);
        }
        set
        {
            this.SetValue("RelatedNodeIsOnTheLeftSide", value);
            this.repItems.RelatedNodeIsOnTheLeftSide = value;
        }
    }


    /// <summary>
    /// Relationship name
    /// </summary>
    public string RelationshipName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("RelationshipName"), this.repItems.RelationshipName), this.repItems.RelationshipName);
        }
        set
        {
            this.SetValue("RelationshipName", value);
            this.repItems.RelationshipName = value;
        }
    }


    /// <summary>
    /// Relationship with node GUID
    /// </summary>
    public Guid RelationshipWithNodeGUID
    {
        get
        {
            return ValidationHelper.GetGuid(this.GetValue("RelationshipWithNodeGuid"), this.repItems.RelationshipWithNodeGuid);
        }
        set
        {
            this.SetValue("RelationshipWithNodeGuid", value);
            this.repItems.RelationshipWithNodeGuid = value;
        }
    }

    #endregion


    #region "Transformation properties"

    /// <summary>
    /// Gets or sets the name of the transforamtion which is used for displaying the results
    /// </summary>
    public string TransformationName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("TransformationName"), this.repItems.TransformationName), this.repItems.TransformationName);
        }
        set
        {
            this.SetValue("TransformationName", value);
            this.repItems.TransformationName = value;
        }
    }


    /// <summary>
    /// Transformation name
    /// </summary>
    public string AlternatingTransformationName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("AlternatingTransformationName"), this.repItems.AlternatingTransformationName), this.repItems.AlternatingTransformationName);
        }
        set
        {
            this.SetValue("AlternatingTransformationName", value);
            this.repItems.AlternatingTransformationName = value;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the nested controls IDs. Use ';' like separator.
    /// </summary>
    public string NestedControlsID
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("NestedControlsID"), repItems.NestedControlsID);
        }
        set
        {
            this.SetValue("NestedControlsID", value);
            this.repItems.NestedControlsID = value;
        }
    }


    /// <summary>
    /// Hide control for zero rows
    /// </summary>
    public bool HideControlForZeroRows
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("HideControlForZeroRows"), this.repItems.HideControlForZeroRows);
        }
        set
        {
            this.SetValue("HideControlForZeroRows", value);
            this.repItems.HideControlForZeroRows = value;
        }
    }


    /// <summary>
    /// Zero rows text
    /// </summary>
    public string ZeroRowsText
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("ZeroRowsText"), this.repItems.ZeroRowsText), this.repItems.ZeroRowsText);
        }
        set
        {
            this.SetValue("ZeroRowsText", value);
            this.repItems.ZeroRowsText = value;
        }
    }


    /// <summary>
    /// Item separator
    /// </summary>
    public string ItemSeparator
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ItemSeparator"), this.repItems.ItemSeparator);
        }
        set
        {
            this.SetValue("ItemSeparator", value);
            this.repItems.ItemSeparator = value;
        }
    }

    #endregion


    #region "Map properties"

    /// <summary>
    /// Gets or sets the Yahoo AppID(s) separated by semicolon.
    /// </summary>
    public string YahooAppID
    {
        get
        {
            return ValidationHelper.GetString(GetValue("YahooAppID"), "");
        }
        set
        {
            SetValue("YahooAppID", value);
        }
    }


    /// <summary>
    /// Gets or sets the latitude of of the center of the map.
    /// </summary>
    public double Latitude
    {
        get
        {
            return ValidationHelper.GetDouble(this.GetValue("Latitude"), 38, "en-us");
        }
        set
        {
            this.SetValue("Latitude", value);
        }
    }


    /// <summary>
    /// Gets or sets the longitude of of the center of the map.
    /// </summary>
    public double Longitude
    {
        get
        {
            return ValidationHelper.GetDouble(this.GetValue("Longitude"), -95, "en-us");
        }
        set
        {
            this.SetValue("Longitude", value);
        }
    }


    /// <summary>
    /// Gets or sets the scale of the map.
    /// </summary>
    public int Scale
    {
        get
        {
            int value = ValidationHelper.GetInteger(this.GetValue("Scale"), 15);
            if (value < 0)
            {
                value = 7;
            }
            return value;
        }
        set
        {
            this.SetValue("Scale", value);
        }
    }


    /// <summary>
    /// Gets or sets the scale of the map when zoomed (after marker click event).
    /// </summary>
    public int ZoomScale
    {
        get
        {
            int value = ValidationHelper.GetInteger(this.GetValue("ZoomScale"), 10);
            if (value < 0)
            {
                value = 10;
            }
            return value;
        }
        set
        {
            this.SetValue("ZoomScale", value);
        }
    }


    /// <summary>
    /// Gets or sets the latitude of the center of the map.
    /// </summary>
    public String LatitudeField
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("LatitudeField"), "");
        }
        set
        {
            this.SetValue("LatitudeField", value);
        }
    }


    /// <summary>
    /// Gets or sets the longitude of the center of the map.
    /// </summary>
    public String LongitudeField
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("LongitudeField"), "");
        }
        set
        {
            this.SetValue("LongitudeField", value);
        }
    }


    /// <summary>
    /// Gets or sets the tool tip text field (filed for markers tool tip text).
    /// </summary>
    public String ToolTipField
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("ToolTipField"), "");
        }
        set
        {
            this.SetValue("ToolTipField", value);
        }
    }


    /// <summary>
    /// Gets or sets the width of the map.
    /// </summary>
    public int Width
    {
        get
        {
            int value = ValidationHelper.GetInteger(this.GetValue("Width"), 400);
            if (value < 0)
            {
                value = 400;
            }
            return value;
        }
        set
        {
            this.SetValue("Width", value);
        }
    }


    /// <summary>
    /// Gets or sets the height of the map.
    /// </summary>
    public int Height
    {
        get
        {
            int value = ValidationHelper.GetInteger(this.GetValue("Height"), 400);
            if (value < 0)
            {
                value = 400;
            }
            return value;
        }
        set
        {
            this.SetValue("Height", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether NavigationControl is displayed.
    /// </summary>
    public bool ShowNavigationControl
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("ShowNavigationControl"), true);
        }
        set
        {
            this.SetValue("ShowNavigationControl", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether ScaleControl is displayed.
    /// </summary>
    public bool ShowScaleControl
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("ShowScaleControl"), true);
        }
        set
        {
            this.SetValue("ShowScaleControl", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether MapTypeControl is displayed.
    /// </summary>
    public bool ShowMapTypeControl
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("ShowMapTypeControl"), true);
        }
        set
        {
            this.SetValue("ShowMapTypeControl", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether the user can drag the map with the mouse. 
    /// </summary>
    public bool EnableMapDragging
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("EnableMapDragging"), true);
        }
        set
        {
            this.SetValue("EnableMapDragging", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether the keyboard shortcuts are enabled.
    /// </summary>
    public bool EnableKeyboardShortcuts
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("EnableKeyboardShortcuts"), true);
        }
        set
        {
            this.SetValue("EnableKeyboardShortcuts", value);
        }
    }


    /// <summary>
    /// Gets or sets the initial map type.
    /// YAHOO_MAP_REG - Road map type.
    /// YAHOO_MAP_SAT - Satellite map type.
    /// YAHOO_MAP_HYB - Hybrid map type.
    /// </summary>
    public string MapType
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("MapType"), "YAHOO_MAP_REG");
        }
        set
        {
            this.SetValue("MapType", value);
        }
    }


    /// <summary>
    /// The Navigation control may appear in one of the following style options:
    /// 0 - Large Zoom & Pan displays the standard zoom slider control with a panning control.
    /// 1 - Small Zoom & Pan displays the small zoom slider control with a panning control.
    /// 2 - Small displays only + and - buttons.
    /// </summary>
    public int NavigationControlType
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("NavigationControlType"), 0);
        }
        set
        {
            this.SetValue("NavigationControlType", value);
        }
    }


    /// <summary>
    /// Detail mode
    /// </summary>
    private bool DetailMode
    {
        get
        {
            if (mDetailMode == null)
            {
                // If this Document type is one of the specified, then we are in detail mode
                mDetailMode = this.ClassNames.ToLower().Contains(CMSContext.CurrentPageInfo.ClassName.ToLower());
            }

            if (mDetailMode == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }


    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Due to new design mode (with preview) we need to move map down for the user to be able to drag and drop the control
        if (CMSContext.ViewMode == ViewModeEnum.Design)
        {
            ltlDesign.Text = "<div style=\"width: 10px; height: 30px\"></div>";
        }
        else
        {
            ltlDesign.Text = "";
        }

        // Get current domain name (for yahoo key identification)
        string currentDomainWithPort = UrlHelper.GetFullDomain().ToLower();

        // Get the yahoo map key based on current domainname
        if (!YahooAppID.Contains("="))
        {
            // Only one key without site specified
            mKey = YahooAppID;
        }
        else
        {
            // Handle key=value pair input (domain_name1=yahoo_key1;domain_name2=yahoo_key2)
            string[] keys = YahooAppID.Split(';');
            foreach (string key in keys)
            {
                int pos = key.IndexOf("=");
                if (pos > -1)
                {
                    // Get corrected domain name (without protocol and www)
                    string domainName = key.Substring(0, pos);
                    // If there is a match, use this key
                    if (domainName.ToLower() == currentDomainWithPort)
                    {
                        mKey = key.Substring(pos + 1);
                        break;
                    }
                }
            }
        }

        // If we haven't found yahoo maps key specified, chceck default domains and keys
        if (string.IsNullOrEmpty(mKey))
        {
            loadScripts = false;
            repItems.StopProcessing = true;
            // Write instructions to user
            ltlDesign.Text += "You haven't specified Yahoo AppID for your domain. Go to <a href=\"https://developer.apps.yahoo.com/wsregapp/\" target=\"_blank\">https://developer.apps.yahoo.com/wsregapp/</a> and sign up for the AppID.";
            return;
        }
    }

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            this.repItems.StopProcessing = true;
        }
        else
        {
            this.repItems.ControlContext = this.ControlContext;

            // Document properties
            this.repItems.CacheItemName = this.CacheItemName;
            this.repItems.CacheDependencies = this.CacheDependencies;
            this.repItems.CacheMinutes = this.CacheMinutes;
            this.repItems.CheckPermissions = this.CheckPermissions;
            this.repItems.ClassNames = this.ClassNames;
            this.repItems.CombineWithDefaultCulture = this.CombineWithDefaultCulture;
            this.repItems.CultureCode = this.CultureCode;
            this.repItems.MaxRelativeLevel = this.MaxRelativeLevel;
            this.repItems.OrderBy = this.OrderBy;
            this.repItems.SelectTopN = this.SelectTopN;
            this.repItems.SelectOnlyPublished = this.SelectOnlyPublished;
            this.repItems.FilterOutDuplicates = this.FilterOutDuplicates;

            this.repItems.Path = this.Path;

            this.repItems.SiteName = this.SiteName;
            this.repItems.WhereCondition = this.WhereCondition;

            // Relationships
            this.repItems.RelatedNodeIsOnTheLeftSide = this.RelatedNodeIsOnTheLeftSide;
            this.repItems.RelationshipName = this.RelationshipName;
            this.repItems.RelationshipWithNodeGuid = this.RelationshipWithNodeGUID;

            // Transformation properties
            this.repItems.TransformationName = this.TransformationName;
            this.repItems.AlternatingTransformationName = this.AlternatingTransformationName;

            // Public properties
            this.repItems.HideControlForZeroRows = this.HideControlForZeroRows;
            this.repItems.ZeroRowsText = this.ZeroRowsText;
            this.repItems.ItemSeparator = this.ItemSeparator;

            this.repItems.NestedControlsID = this.NestedControlsID;

            // Create unique div the map will be loaded in.
            mapName = this.ClientID + "_map";
        }
    }


    /// <summary>
    /// OnPreRender
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.Visible = this.repItems.Visible;

        if (errorOccurred)
        {
            StopRendering();
            return;
        }

        if (!this.StopProcessing && loadScripts)
        {
            // Register Yahoo API and Load scripts
            ScriptHelper.RegisterClientScriptBlock(this.Page, typeof(string), "yahooMapsScript", "<script src=\"http://api.maps.yahoo.com/ajaxymap?v=3.8&amp;appid=" + mKey + "\" type=\"text/javascript\"></script>");
            ScriptHelper.RegisterClientScriptBlock(this.Page, typeof(string), "yahooMapVariables", ScriptHelper.GetScript("var yahoo_content_repaired; \n"));
            ScriptHelper.RegisterClientScriptBlock(this.Page, typeof(string), "addLoad", ScriptHelper.GetScript(
                @"function addLoadEvent(func)
                {
                    var oldonload = window.onload;
                    if (typeof window.onload != 'function') {
                        window.onload = func;
                    } else {
                        window.onload = function() {
                            oldonload();
                            func();
                        }
                    }
                }"));

            // Add Yahoo map scrip declaration
            ltlBefore.Text = "<script type=\"text/javascript\">\n<!--" + "\n" + "var " + mapName + "; \n";

            // Create MapLoad function
            ltlBefore.Text += "function MapLoad_" + this.ClientID + "() {" + "\n" +

                      mapName + " = new YMap(document.getElementById(\"" + mapName + "\")); \n" +
                      mapName + ".setMapType(" + this.MapType + "); \n" +
                      mapName + ".drawZoomAndCenter(new YGeoPoint(" + this.Latitude.ToString().Replace(",", ".") + ", " + this.Longitude.ToString().Replace(",", ".") + "), " + this.Scale + ");" +

                      (this.EnableKeyboardShortcuts ? mapName + ".enableKeyControls();" : mapName + ".disableKeyControls();") + "\n" +
                      (this.EnableMapDragging ? mapName + ".enableDragMap();" : mapName + ".disableDragMap();") + "\n" +
                      (this.ShowMapTypeControl ? mapName + ".addTypeControl();" : "") + "\n" +
                      (this.ShowScaleControl ? mapName + ".addZoomScale();" : mapName + ".removeZoomScale();") + "\n" +
                      "\n";

            if (this.ShowNavigationControl)
            {
                switch (this.NavigationControlType)
                {
                    case 0:
                        ltlBefore.Text += mapName + ".addZoomLong(); \n" + mapName + ".addPanControl(); \n";
                        break;
                    case 1:
                        ltlBefore.Text += mapName + ".addZoomShort(); \n" + mapName + ".addPanControl(); \n";
                        break;
                    case 2:
                        ltlBefore.Text += mapName + ".addZoomShort(); \n";
                        break;
                }
            }
            else
            {
                ltlBefore.Text += mapName + ".removeZoomControl(); " + mapName + ".removePanControl(); \n";
            }

            ltlAfter.Text += "}\n-->\n</script><div style=\"position: relative; width:" + this.Width + "px; height: " + this.Height + "px\" id=\"" + mapName + "\"></div>" + "\n";
            ltlAfter.Text += ScriptHelper.GetScript("addLoadEvent(MapLoad_" + this.ClientID + ");");
        }
    }


    /// <summary>
    /// Item databound
    /// </summary>
    protected void repItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        mapName = this.ClientID + "_map";

        if ((this.LatitudeField.Trim() != "") && (this.LongitudeField.Trim() != ""))
        {
            try
            {
                DataRow dr = e.Item.DataItem as DataRow;

                if (dr == null)
                {
                    DataRowView dv = e.Item.DataItem as DataRowView;
                    if (dv != null)
                    {
                        dr = dv.Row;
                    }
                }

                if (dr != null)
                {
                    // Validate input
                    if (!dr.Table.Columns.Contains(this.LatitudeField))
                    {
                        this.lblError.Text = String.Format(ResHelper.GetString("maps.latitudefieldnotfound"), this.LatitudeField);
                        errorOccurred = true;
                        return;
                    }
                    else if (!dr.Table.Columns.Contains(this.LongitudeField))
                    {
                        this.lblError.Text = String.Format(ResHelper.GetString("maps.longitudefieldnotfound"), this.LongitudeField);
                        errorOccurred = true;
                        return;
                    }

                    // Get correct values from DataSet (replace decimal dash to dot)
                    string lLatitude = dr[this.LatitudeField].ToString().Replace(",", ".");
                    string lLongitude = dr[this.LongitudeField].ToString().Replace(",", ".");
                    string tooltip = "";
                    if (!String.IsNullOrEmpty(this.ToolTipField) && dr.Table.Columns.Contains(this.ToolTipField))
                    {
                        tooltip = ValidationHelper.GetString(dr[this.ToolTipField], "");
                    }

                    // Create unique marker
                    marker++;
                    string markerName = "marker_" + marker;
                    string pointName = "point_" + marker;
                    string markerCode =
                        "var " + pointName + " = new YGeoPoint(" + lLatitude + ", " + lLongitude + ");" + "\n" +
                        "var " + markerName + " = new YMarker(" + pointName + "); " + "\n" +
                        mapName + ".addOverlay(" + markerName + "); \n" +
                        markerName + ".addLabel('" + tooltip + "'); \n" +
                        markerName + ".addAutoExpand(yahoo_content_repaired); \n" +
                        "YEvent.Capture(" + markerName + ", EventsList.MouseClick, \n" +
                        "    function(){ \n" +
                        "        " + mapName + ".drawZoomAndCenter(" + pointName + ", " + this.ZoomScale + ");" +
                        "        " + markerName + ".openAutoExpand(); \n" +
                        "    } \n" +
                        "); \n" +
                        "YEvent.Capture(" + markerName + ", EventsList.MouseOver, \n" +
                        "    function(){ \n" +
                        "        " + markerName + ".closeAutoExpand(); \n" +
                        "    } \n" +
                        "); \n" +
                        "\n";

                    // Mark the content with macros ##TR## (start) and ##TRE## (end) for the input text to be corrected
                    e.Item.Controls.AddAt(0, new LiteralControl("yahoo_content_repaired=\"##TR##"));
                    e.Item.Controls.Add(new LiteralControl("##TRE##\";" + "\n" + markerCode + "\n"));

                    // Zoom on curent marker if we are in detailed mode
                    int docID = ValidationHelper.GetInteger(dr["DocumentID"], 0);
                    if (DetailMode && (docID == CMSContext.CurrentDocument.DocumentID))
                    {
                        e.Item.Controls.Add(new LiteralControl(mapName + ".drawZoomAndCenter(" + pointName + ", " + this.ZoomScale + ");" + markerName + ".openAutoExpand();" + "\n"));
                    }
                }
            }
            catch (Exception ex)
            {
                this.lblError.Visible = true;
                this.lblError.Text += ex.ToString();
            }
        }
    }


    /// <summary>
    /// Stops rendering of the map and displays error message.
    /// </summary>
    private void StopRendering()
    {
        this.lblError.Visible = true;
        this.ltlAfter.Visible = false;
        this.ltlBefore.Visible = false;
        this.repItems.Visible = false;
    }


    /// <summary>
    /// Repair input string (remove forbiden characters between macros '##TR##' and 6420'##TRE##')
    /// </summary>
    private string RepairInputString(string input)
    {
        string inputText = input;

        while ((inputText.IndexOf("##TR##") > 0) && (inputText.IndexOf("##TRE##") > 0))
        {
            int firstIndex = inputText.IndexOf("##TR##");
            int lastIndex = inputText.IndexOf("##TRE##") + 7;

            string sub = inputText.Substring(firstIndex, lastIndex - firstIndex);
            sub = sub.Replace("\"", "\\" + "\"").Replace("\n", " ").Replace("\r", "").Replace("##TR##", "").Replace("##TRE##", "").Trim();
            inputText = inputText.Remove(firstIndex, lastIndex - firstIndex);
            inputText = inputText.Insert(firstIndex, sub);
        }

        return inputText;
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        this.SetupControl();
        this.repItems.ReloadData(true);
    }


    /// <summary>
    /// Render override
    /// </summary>
    protected override void Render(HtmlTextWriter writer)
    {
        if (this.StopProcessing)
        {
            base.Render(writer);
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            Html32TextWriter mwriter = new Html32TextWriter(new StringWriter(sb));
            base.Render(mwriter);

            outputHtml = RepairInputString(sb.ToString());
            writer.Write(outputHtml);
        }
    }


    /// <summary>
    /// Clear cache.
    /// </summary>
    public override void ClearCache()
    {
        this.repItems.ClearCache();
    }
}
