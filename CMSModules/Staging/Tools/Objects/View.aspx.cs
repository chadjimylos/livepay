using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Staging_Tools_Objects_View : CMSStagingPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'Manage tasks' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.staging", "ManageObjectTasks"))
        {
            RedirectToAccessDenied("cms.staging", "ManageObjectTasks");
        }

        CurrentMaster.Title.TitleText = ResHelper.GetString("Task.ViewHeader");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Staging/tasks.png");

        int taskId = QueryHelper.GetInteger("taskid", 0);
        ucViewTask.TaskId = taskId;
    }
}