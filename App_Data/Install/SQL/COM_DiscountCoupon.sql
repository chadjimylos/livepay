CREATE TABLE [COM_DiscountCoupon] (
		[DiscountCouponID]               int NOT NULL IDENTITY(1, 1),
		[DiscountCouponDisplayName]      nvarchar(200) NOT NULL,
		[DiscountCouponIsExcluded]       bit NOT NULL,
		[DiscountCouponValidFrom]        datetime NULL,
		[DiscountCouponValidTo]          datetime NULL,
		[DiscountCouponValue]            float NULL,
		[DiscountCouponIsFlatValue]      bit NOT NULL,
		[DiscountCouponCode]             nvarchar(200) NOT NULL,
		[DiscountCouponGUID]             uniqueidentifier NOT NULL,
		[DiscountCouponLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_DiscountCoupon]
	ADD
	CONSTRAINT [PK_COM_DiscountCoupon]
	PRIMARY KEY
	NONCLUSTERED
	([DiscountCouponID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE UNIQUE INDEX [IX_COM_DiscountCoupon_DiscountCouponCode]
	ON [COM_DiscountCoupon] ([DiscountCouponCode])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_DiscountCoupon_DiscoutCouponDisplayName]
	ON [COM_DiscountCoupon] ([DiscountCouponDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
