<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteFilter.ascx.cs" Inherits="CMSFormControls_Filters_SiteFilter" %>
<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector" TagPrefix="cms" %>
<asp:Panel CssClass="Filter" runat="server" ID="pnlSearch">
    <cms:LocalizedLabel ID="lblSite" runat="server" EnableViewState="false" />&nbsp;
    <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" />
</asp:Panel>
