<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PostListing.ascx.cs" Inherits="CMSModules_Forums_Controls_Posts_PostListing" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<cms:UniGrid ID="gridPosts" runat="server" GridName="~/CMSModules/Forums/Controls/Posts/PostListing.xml"
    EnableViewState="true" DelayedReload="false" IsLiveSite="false" Columns="PostID, PostForumID, PostSubject, PostApproved, PostThreadPostsAbsolute, PostLevel, PostUserName, PostLastModified"
    OrderBy="PostTime" />
<br />
<asp:Panel ID="pnlFooter" runat="server" Style="clear: both;">
    <%--<asp:DropDownList ID="drpWhat" runat="server" CssClass="DropDownFieldSmall" />--%>
    <cms:LocalizedLabel ID="lblSelectedPosts" ResourceString="forums.listing.selectedposts"
        DisplayColon="true" runat="server" EnableViewState="false" />
    <asp:DropDownList ID="drpAction" runat="server" CssClass="DropDownFieldSmall" />
    <cms:LocalizedButton ID="btnOk" runat="server" ResourceString="general.ok" CssClass="SubmitButton"
        EnableViewState="false" />
    <br />
    <br />
</asp:Panel>
<asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />