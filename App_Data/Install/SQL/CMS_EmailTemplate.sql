CREATE TABLE [CMS_EmailTemplate] (
		[EmailTemplateID]               int NOT NULL IDENTITY(1, 1),
		[EmailTemplateName]             nvarchar(200) NOT NULL,
		[EmailTemplateDisplayName]      nvarchar(200) NOT NULL,
		[EmailTemplateText]             nvarchar(max) NOT NULL,
		[EmailTemplateSiteID]           int NULL,
		[EmailTemplateGUID]             uniqueidentifier NOT NULL,
		[EmailTemplateLastModified]     datetime NOT NULL,
		[EmailTemplatePlainText]        nvarchar(max) NULL,
		[EmailTemplateSubject]          nvarchar(250) NULL,
		[EmailTemplateFrom]             nvarchar(250) NULL,
		[EmailTemplateCc]               nvarchar(max) NULL,
		[EmailTemplateBcc]              nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_EmailTemplate]
	ADD
	CONSTRAINT [PK_CMS_EmailTemplate]
	PRIMARY KEY
	NONCLUSTERED
	([EmailTemplateID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_EmailTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Email_EmailTemplateSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_EmailTemplate]
			ADD CONSTRAINT [FK_CMS_Email_EmailTemplateSiteID_CMS_Site]
			FOREIGN KEY ([EmailTemplateSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_CMS_EmailTemplate_EmailTemplateDisplayName]
	ON [CMS_EmailTemplate] ([EmailTemplateDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_EmailTemplate_EmailTemplateName_EmailTemplateSiteID]
	ON [CMS_EmailTemplate] ([EmailTemplateName], [EmailTemplateSiteID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
