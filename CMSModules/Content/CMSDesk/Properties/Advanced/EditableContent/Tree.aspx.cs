using System;
using System.Collections;
using System.Web;

using CMS.PortalEngine;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.SettingsProvider;

using TreeElemNode = System.Web.UI.WebControls.TreeNode;
using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Properties_Advanced_EditableContent_Tree : CMSModalPage
{
    #region "Private variables"

    private int nodeId = 0;
    private string selectedNodeType = null;
    private string selectedNodeName = null;
    private CurrentUserInfo currentUser = null;
    private TreeNode node = null;
    private TreeProvider tree = null;

    private enum EditableContentType { webpart = 0, region = 1 };

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        currentUser = CMSContext.CurrentUser;
        this.CurrentMaster.FrameResizer.Visible = false;

        // Get query parameters
        selectedNodeType = QueryHelper.GetString("selectednodetype", "webpart");
        selectedNodeName = QueryHelper.GetString("selectednodename", string.Empty);
        hdnCurrentNodeType.Value = selectedNodeType;
        hdnCurrentNodeName.Value = selectedNodeName;
        nodeId = QueryHelper.GetInteger("nodeid", 0);

        // Images
        if (hdnCurrentNodeType.Value == "region")
        {
            imgNewItem.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditableContent/addeditableitemsmall.png");
        }
        else
        {
            imgNewItem.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditableContent/editablewebpart.png");
        }
        imgDeleteItem.ImageUrl = GetImageUrl("Objects/CMS_WebPart/delete.png");
        if (QueryHelper.GetString("selectednodename", null) != null)
        {
            lnkDeleteItem.CssClass = "NewItemLink";
            lnkDeleteItem.Attributes.Add("onclick", "return DeleteItem();");
        }

        // Resource strings
        lnkDeleteItem.Text = ResHelper.GetString("Development-WebPart_Tree.DeleteItem");
        lnkNewItem.Text = ResHelper.GetString("Development-WebPart_Tree.NewWebPart");
        lnkNewItem.Attributes.Add("onclick", "CreateNew(); return false;");

        // Initialize page

        string script =
        "treeUrl = '" + ResolveUrl("~/CMSModules/Content/CMSDesk/Properties/Advanced/EditableContent/tree.aspx") + "?nodeid=" + nodeId + "'; \n" +
            // Set proper image for 'new item' link and disable delete link for root item
        "function UpdateMenu(selectedNodeName, selectedItemType) { \n" +
            "var newItem = document.getElementById('" + imgNewItem.ClientID + "'); \n" +
            "var deleteItem = document.getElementById('" + lnkDeleteItem.ClientID + "'); \n" +

            "if ((selectedNodeName == \"\") || (selectedNodeName == null)) { \n" +
                "deleteItem.className = \"NewItemLinkDisabled\"; \n" +
                "deleteItem.onclick = null; \n" +
            "} else { \n" +
                "deleteItem.className = \"NewItemLink\"; \n" +
                "deleteItem.onclick = DeleteItem; \n" +
            "}\n" +

            "if (selectedItemType == 'webpart') { \n" +
                "newItem.src = \"" + GetImageUrl("CMSModules/CMS_Content/EditableContent/editablewebpart.png") + "\"; \n" +
            "} else { \n" +
                "newItem.src = \"" + GetImageUrl("CMSModules/CMS_Content/EditableContent/addeditableitemsmall.png") +"\"; \n" +
        "} } \n" +

        // Open 'Create new' menu on main panel
        "function CreateNew() { \n" +
                "parent.frames['main'].CreateNew(document.getElementById('" + hdnCurrentNodeType.ClientID + "').value) \n" +
        "} \n" +

        // Open confirmation dialog and delete item 
        "function DeleteItem() { \n" +
            " if (confirm(" + ScriptHelper.GetString(ResHelper.GetString("editablecontent.confirmdelete")) + ")) { \n" +
                "document.location.replace(treeUrl+\"&deleteItem=true&nodename=\"+document.getElementById('" + hdnCurrentNodeName.ClientID + "').value+\"&nodetype=\"+document.getElementById('" + hdnCurrentNodeType.ClientID + "').value); } \n" +
            "return false;\n" +
        "} \n" +

        // Select node action
        "function SelectNode(nodeName, nodeType, nodeElem) { \n" +
            "if (currentNode == null) { \n" +
                "currentNode = document.getElementById('treeSelectedNode'); \n" +
            "} \n" +
            "if ((currentNode != null) && (nodeElem != null)) { \n" +
                "currentNode.className = 'ContentTreeItem'; \n" +
            "} \n" +
            "parent.frames['main'].SelectNode(nodeName, nodeType); \n" +
            "document.getElementById('" + hdnCurrentNodeName.ClientID + "').value = nodeName; \n" +
            "document.getElementById('" + hdnCurrentNodeType.ClientID + "').value = nodeType; \n" +
            "if (nodeElem != null) { \n" +
                "currentNode = nodeElem; \n" +
                "if (currentNode != null) { \n" +
                    "currentNode.className = 'ContentTreeSelectedItem'; \n" +
                "} \n" +
            "} \n" +
            "UpdateMenu(nodeName, nodeType); \n" +
        "} \n";

        ltlScript.Text = ScriptHelper.GetScript(script);

        if (CultureHelper.IsUICultureRTL())
        {
            webpartsTree.LineImagesFolder = GetImageUrl("RTL/Design/Controls/Tree/", false, true);
            regionsTree.LineImagesFolder = GetImageUrl("RTL/Design/Controls/Tree/", false, true);
        }
        else
        {
            webpartsTree.LineImagesFolder = GetImageUrl("Design/Controls/Tree/", false, true);
            regionsTree.LineImagesFolder = GetImageUrl("Design/Controls/Tree/", false, true);
        }

        // Get node
        tree = new TreeProvider(currentUser);
        node = DocumentHelper.GetDocument(nodeId, currentUser.PreferredCultureCode, tree);
        if (node != null)
        {
            string webpartRootAttributes = "class=\"ContentTreeItem\"";
            string regionRootAttributes = "class=\"ContentTreeItem\"";

            if (string.IsNullOrEmpty(selectedNodeName))
            {
                switch (selectedNodeType)
                {
                    case "webpart":
                        webpartRootAttributes = "class=\"ContentTreeSelectedItem\" id=\"treeSelectedNode\"";
                        regionRootAttributes = "class=\"ContentTreeItem\" ";
                        break;

                    case "region":
                        webpartRootAttributes = "class=\"ContentTreeItem\" ";
                        regionRootAttributes = "class=\"ContentTreeSelectedItem\" id=\"treeSelectedNode\"";
                        break;
                    default:
                        webpartRootAttributes = "class=\"ContentTreeSelectedItem\" id=\"treeSelectedNode\"";
                        regionRootAttributes = "class=\"ContentTreeItem\" ";
                        break;
                }
            }
            else
            {
                webpartRootAttributes = "class=\"ContentTreeSelectedItem\" id=\"treeSelectedNode\"";
                regionRootAttributes = "class=\"ContentTreeItem\" ";
            }

            // Create tree menus
            TreeElemNode rootWebpartNode = new TreeElemNode();
            rootWebpartNode.Text = "<span " + webpartRootAttributes + " onclick=\"SelectNode('','webpart', this);\"><span class=\"Name\">" + ResHelper.GetString("EditableWebparts.Root") + "</span></span>";
            rootWebpartNode.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditableContent/editablewebparts.png");
            rootWebpartNode.Expanded = true;
            rootWebpartNode.NavigateUrl = "#";


            TreeElemNode rootRegionNode = new TreeElemNode();
            rootRegionNode.Text = "<span " + regionRootAttributes + " onclick=\"SelectNode('','region', this);\"><span class=\"Name\">" + ResHelper.GetString("EditableRegions.Root") + "</span></span>";
            rootRegionNode.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditableContent/editableregions.png");
            rootRegionNode.Expanded = true;
            rootRegionNode.NavigateUrl = "#";

            // Editable web parts
            webpartsTree.Nodes.Add(rootWebpartNode);
            if (node.DocumentContent.EditableWebParts.Count > 0)
            {
                foreach (DictionaryEntry d in node.DocumentContent.EditableWebParts)
                {
                    string key = d.Key.ToString();
                    string name = MultiKeyHashtable.GetFirstKey(key);
                    AddNode(rootWebpartNode, name, key, "webpart");
                }
            }

            // Editable regions
            regionsTree.Nodes.Add(rootRegionNode);
            if (node.DocumentContent.EditableRegions.Count > 0)
            {
                foreach (DictionaryEntry d in node.DocumentContent.EditableRegions)
                {
                    string key = d.Key.ToString();
                    string name = MultiKeyHashtable.GetFirstKey(key);
                    AddNode(rootRegionNode, name, key, "region");
                }
            }
        }

        // Delete item if requested from querystring
        string nodeType = QueryHelper.GetString("nodetype", null);
        string nodeName = QueryHelper.GetString("nodename", null);
        if (!RequestHelper.IsPostBack() && !String.IsNullOrEmpty(nodeType) && QueryHelper.GetBoolean("deleteItem", false))
        {
            DeleteItem(nodeType, nodeName);
        }
    }


    #region "Private methods"

    /// <summary>
    /// Adds node to parent node
    /// </summary>
    /// <param name="parentNode">Parent node</param>
    /// <param name="nodeName">Name of node</param>
    /// <param name="nodeKey">Node key</param>
    /// <param name="nodeType">Type of node</param>
    private void AddNode(TreeElemNode parentNode, string nodeName, string nodeKey, string nodeType)
    {
        TreeElemNode newNode = new TreeElemNode();
        string cssClass = "ContentTreeItem";
        string elemId = string.Empty;

        // Select proper node
        if (selectedNodeName == nodeName)
        {
            if (webpartsTree.Nodes.Count > 0)
            {
                webpartsTree.Nodes[0].Text = webpartsTree.Nodes[0].Text.Replace("ContentTreeSelectedItem", "ContentTreeItem").Replace("treeSelectedNode", string.Empty);
            }
            else if (regionsTree.Nodes.Count > 0)
            {
                regionsTree.Nodes[0].Text = regionsTree.Nodes[0].Text.Replace("ContentTreeSelectedItem", "ContentTreeItem").Replace("treeSelectedNode", string.Empty);
            }
            if (nodeType == selectedNodeType)
            {
                cssClass = "ContentTreeSelectedItem";
                elemId = "id=\"treeSelectedNode\"";
            }
        }
        newNode.Text = "<span class=\"" + cssClass + "\" " + elemId + " onclick=\"SelectNode('" + HttpUtility.UrlEncode(nodeKey) + "','" + HttpUtility.UrlEncode(nodeType) + "', this);\"><span class=\"Name\">" + HTMLHelper.HTMLEncode(nodeName) + "</span></span>";
        newNode.NavigateUrl = "#";
        parentNode.ChildNodes.Add(newNode);
        return;
    }


    /// <summary>
    /// Deletes item.
    /// </summary>
    protected void DeleteItem(string nodeType, string nodeName)
    {
        // Remove key from hashtable
        switch (nodeType)
        {
            case "webpart":
                node.DocumentContent.EditableWebParts.Remove(nodeName);
                break;

            case "region":
                node.DocumentContent.EditableRegions.Remove(nodeName);
                break;
        }

        // Save node
        SaveNode(true);

        // Refresh
        ltlScript.Text += ScriptHelper.GetScript("document.location.replace('" +
                                                 ResolveUrl(
                                                 "~/CMSModules/Content/CMSDesk/Properties/Advanced/EditableContent/tree.aspx") +
                                                 "?nodeid=" + nodeId + "&selectednodetype=" +
                                                 nodeType + "'); parent.frames['main'].SelectNode('','" + nodeType + "');");
    }


    /// <summary>
    /// Saves node, ensures workflow
    /// </summary>
    protected void SaveNode(bool forceCheckout)
    {
        // Get content
        string content = node.DocumentContent.GetContentXml();
        // Save content
        if (node != null)
        {
            bool autoCheck = !VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName);
            VersionManager vm = null;
            WorkflowManager workflowMan = new WorkflowManager(tree);
            bool useWorkflow = workflowMan.GetNodeWorkflowScope(node) != null;

            // If not using check-in/check-out, check out automatically
            if (autoCheck & useWorkflow || forceCheckout)
            {
                if (node.GetValue("DocumentCheckedOutByUserID") == null)
                {
                    // Check out
                    vm = new VersionManager(tree);
                    vm.CheckOut(node);
                }
            }
            node.UpdateOriginalValues();

            node.SetValue("DocumentContent", content);
            DocumentHelper.UpdateDocument(node, tree);

            // Check in the document
            if ((autoCheck & useWorkflow) && vm != null)
            {
                vm.CheckIn(node, null, null);
            }
        }
    }

    #endregion
}
