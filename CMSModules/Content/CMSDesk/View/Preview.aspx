<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Preview.aspx.cs" Inherits="CMSModules_Content_CMSDesk_View_Preview" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Content - Preview</title>

    <script type="text/javascript">
    //<![CDATA[
        var IsCMSDesk = true;
    //]]>
    </script>

</head>
<frameset border="0" rows="<%=headersize%>,*" border="0" id="previewframeset">
    <frame name="previewmenu" src="previewmenu.aspx<%=Request.Url.Query%>" scrolling="no"
        noresize="noresize" frameborder="0" />
    <frame name="previewview" src="<%=viewpage%>" scrolling="auto" noresize="noresize" frameborder="0" />
    <noframes>
        <p id="p1">
            This HTML frameset displays multiple Web pages. To view this frameset, use a Web
            browser that supports HTML 4.0 and later.
        </p>
    </noframes>
</frameset>
</html>
