using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

using CMS.ExtendedControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.WorkflowEngine;
using CMS.DataEngine;
using CMS.SettingsProvider;

using TreeNode = CMS.TreeEngine.TreeNode;
using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Content_CMSDesk_View_Listing : CMSContentPage, ICallbackEventHandler
{
    #region "Private and protected variables & enums"

    private CurrentUserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;
    private TimeZoneInfo usedTimeZone = null;
    private string where = null;
    private const string WHERE_KEY = "whereCondition";

    protected string checkedNodeIDs = string.Empty;
    protected string mDefaultSiteCulture = null;
    protected string currentSiteName = null;
    protected int nodeId = 0;
    protected bool dataLoaded = false;
    protected bool allSelected = false;
    protected string aliasPath = null;

    protected TreeProvider tree = null;
    protected TreeNode node = null;
    protected ArrayList mFlagsControls = null;
    protected DataSet mSiteCultures = null;
    protected DialogConfiguration mConfig = null;

    protected string urlParameter = string.Empty;
    protected Action callbackAction = Action.Move;

    protected enum Action
    {
        SelectAction = 0,
        Move = 1,
        Copy = 2,
        Link = 3,
        Delete = 4,
        Publish = 5,
        Archive = 6
    }

    protected enum What
    {
        SelectedDocuments = 0,
        AllDocuments = 1
    }

    protected enum Argument
    {
        Action = 0,
        AllSelected = 1,
        WhereCondition = 2,
        Items = 3
    }

    #endregion


    #region "Private properties"

    /// <summary>
    /// Default culture of the site
    /// </summary>
    private string DefaultSiteCulture
    {
        get
        {
            if (mDefaultSiteCulture == null)
            {
                mDefaultSiteCulture = CultureHelper.GetDefaultCulture(currentSiteName);
            }
            return mDefaultSiteCulture;
        }
    }


    /// <summary>
    /// Hashtable with document flags controls
    /// </summary>
    private ArrayList FlagsControls
    {
        get
        {
            if (mFlagsControls == null)
            {
                mFlagsControls = new ArrayList();
            }
            return mFlagsControls;
        }
    }


    /// <summary>
    /// Site cultures
    /// </summary>
    private DataSet SiteCultures
    {
        get
        {
            if (mSiteCultures == null)
            {
                mSiteCultures = CultureInfoProvider.GetSiteCultures(currentSiteName).Copy();
                if (!DataHelper.DataSourceIsEmpty(mSiteCultures))
                {
                    DataTable cultureTable = mSiteCultures.Tables[0];
                    DataRow[] defaultCultureRow = cultureTable.Select("CultureCode='" + DefaultSiteCulture + "'");

                    // Ensure default culture to be first
                    DataRow dr = cultureTable.NewRow();
                    if (defaultCultureRow.Length > 0)
                    {
                        dr.ItemArray = defaultCultureRow[0].ItemArray;
                        cultureTable.Rows.InsertAt(dr, 0);
                        cultureTable.Rows.Remove(defaultCultureRow[0]);
                    }
                }
            }
            return mSiteCultures;
        }
    }


    /// <summary>
    /// Gets the configuration for Copy and Move dialog.
    /// </summary>
    private DialogConfiguration Config
    {
        get
        {
            if (mConfig == null)
            {
                mConfig = new DialogConfiguration();
                mConfig.ContentSelectedSite = CMSContext.CurrentSiteName;
                mConfig.OutputFormat = OutputFormatEnum.Custom;
                mConfig.SelectableContent = SelectableContentEnum.AllContent;
                mConfig.HideAttachments = false;
            }
            return mConfig;
        }
    }

    #endregion


    #region "Page events"

    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        currentSiteName = CMSContext.CurrentSiteName;
        currentUserInfo = CMSContext.CurrentUser;

        // Current Node ID
        nodeId = QueryHelper.GetInteger("nodeid", 0);

        // Setup page title text and image
        CurrentMaster.Title.TitleText = ResHelper.GetString("Content.ListingTitle");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Content/Menu/Listing.png");

        CurrentMaster.Title.HelpName = "helpTopic";
        CurrentMaster.Title.HelpTopicName = "list_tab";

        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Listing.ParentDirectory");
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Content/Listing/parent.png");
        CurrentMaster.HeaderActions.Actions = actions;

        if (nodeId > 0)
        {
            tree = new TreeProvider(currentUserInfo);
            node = tree.SelectSingleNode(nodeId, TreeProvider.ALL_CULTURES);

            if (node != null)
            {
                if (currentUserInfo.IsAuthorizedPerDocument(node, NodePermissionsEnum.ExploreTree) != AuthorizationResultEnum.Allowed)
                {
                    RedirectToCMSDeskAccessDenied("CMS.Content", "exploretree");
                }

                aliasPath = node.NodeAliasPath;

                // Setup the link to the parent document
                if ((node.NodeClassName.ToLower() != "cms.root") && (currentUserInfo.UserStartingAliasPath.ToLower() != node.NodeAliasPath.ToLower()))
                {
                    CurrentMaster.HeaderActions.Actions[0, 3] = "javascript:SelectItem(" + node.NodeParentID + ");";
                }
                else
                {
                    CurrentMaster.HeaderActions.Visible = false;
                    CurrentMaster.PanelBody.FindControl("pnlActions").Visible = false;
                }
            }

            ScriptHelper.RegisterProgress(this);
            ScriptHelper.RegisterDialogScript(this);
            ScriptHelper.RegisterJQuery(this);

            InitDropdowLists();

            cultureElem.DropDownCultures.Width = 222;

            StringBuilder actionScript = new StringBuilder();
            actionScript.AppendLine("function PerformAction(gridId, dropId)");
            actionScript.AppendLine("{");
            actionScript.AppendLine("   var label = document.getElementById('" + lblInfo.ClientID + "');");
            actionScript.AppendLine("   var whatDrp = document.getElementById('" + drpWhat.ClientID + "');");
            actionScript.AppendLine("   var action = document.getElementById(dropId).value;");
            actionScript.AppendLine("   var gridElem = document.getElementById(gridId);");
            actionScript.AppendLine("   var allSelected = " + (int)What.SelectedDocuments + ";");
            actionScript.AppendLine("   if (action == '" + (int)Action.SelectAction + "')");
            actionScript.AppendLine("   {");
            actionScript.AppendLine("       label.innerHTML = '" + ResHelper.GetString("massaction.selectsomeaction") + "';");
            actionScript.AppendLine("       return false;");
            actionScript.AppendLine("   }");
            actionScript.AppendLine("   if(whatDrp.value == '" + (int)What.AllDocuments + "')");
            actionScript.AppendLine("   {");
            actionScript.AppendLine("       allSelected = " + (int)What.AllDocuments + ";");
            actionScript.AppendLine("   }");
            actionScript.AppendLine("   var items = gridElem.value;");
            actionScript.AppendLine("   if(!SelectionIsEmpty(gridElem) || whatDrp.value == '" + (int)What.AllDocuments + "')");
            actionScript.AppendLine("   {");
            actionScript.AppendLine("       var argument = '|' + allSelected + '|' + where + '|' + items;");
            actionScript.AppendLine("       switch(action)");
            actionScript.AppendLine("       {");
            actionScript.AppendLine("           case '" + (int)Action.Move + "':");
            actionScript.AppendLine("               argument = '" + Action.Move + "' + argument;");
            actionScript.AppendLine(ClientScript.GetCallbackEventReference(this, "argument", "OpenModal", string.Empty) + ";");
            actionScript.AppendLine("               break;");
            actionScript.AppendLine("           case '" + (int)Action.Copy + "':");
            actionScript.AppendLine("               argument = '" + Action.Copy + "' + argument;");
            actionScript.AppendLine(ClientScript.GetCallbackEventReference(this, "argument", "OpenModal", string.Empty) + ";");
            actionScript.AppendLine("               break;");
            actionScript.AppendLine("           case '" + (int)Action.Link + "':");
            actionScript.AppendLine("               argument = '" + Action.Link + "' + argument;");
            actionScript.AppendLine(ClientScript.GetCallbackEventReference(this, "argument", "OpenModal", string.Empty) + ";");
            actionScript.AppendLine("               break;");
            actionScript.AppendLine("           case '" + (int)Action.Delete + "':");
            actionScript.AppendLine("               argument = '" + Action.Delete + "' + argument;");
            actionScript.AppendLine(ClientScript.GetCallbackEventReference(this, "argument", "Redirect", string.Empty) + ";");
            actionScript.AppendLine("               break;");
            actionScript.AppendLine("           case '" + (int)Action.Publish + "':");
            actionScript.AppendLine("               argument = '" + Action.Publish + "' + argument;");
            actionScript.AppendLine(ClientScript.GetCallbackEventReference(this, "argument", "Redirect", string.Empty) + ";");
            actionScript.AppendLine("               break;");
            actionScript.AppendLine("           case '" + (int)Action.Archive + "':");
            actionScript.AppendLine("               argument = '" + Action.Archive + "' + argument;");
            actionScript.AppendLine(ClientScript.GetCallbackEventReference(this, "argument", "Redirect", string.Empty) + ";");
            actionScript.AppendLine("               break;");
            actionScript.AppendLine("           default:");
            actionScript.AppendLine("               return false;");
            actionScript.AppendLine("       }");
            actionScript.AppendLine("   }");
            actionScript.AppendLine("   else");
            actionScript.AppendLine("   {");
            actionScript.AppendLine("       label.innerHTML = '" + ResHelper.GetString("documents.selectdocuments") + "';");
            actionScript.AppendLine("   }");
            actionScript.AppendLine("   return false;");
            actionScript.AppendLine("}");
            actionScript.AppendLine("function SelectionIsEmpty(gridElem)");
            actionScript.AppendLine("{");
            actionScript.AppendLine("   var items = gridElem.value;");
            actionScript.AppendLine("   return !(items != '' && items != '|');");
            actionScript.AppendLine("}");
            actionScript.AppendLine("function OpenModal(arg, context)");
            actionScript.AppendLine("{");
            actionScript.AppendLine("   modalDialog(arg,'actionDialog','90%', '85%');");
            actionScript.AppendLine("}");
            actionScript.AppendLine("function Redirect(arg, context)");
            actionScript.AppendLine("{");
            actionScript.AppendLine("   document.location.replace(arg);");
            actionScript.AppendLine("}");
            actionScript.AppendLine("function ClearSelection()");
            actionScript.AppendLine("{");
            actionScript.AppendLine("   ClearSelection_" + gridDocuments.ClientID + "();");
            actionScript.AppendLine("}");

            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "actionScript", ScriptHelper.GetScript(actionScript.ToString()));

            // Add action to button
            btnOk.OnClientClick = "return PerformAction('" + gridDocuments.ClientID + "_hidSelection','" + drpAction.ClientID + "');";

            // Initialize dropdown lists
            if (!RequestHelper.IsPostBack())
            {
                drpAction.Items.Add(new ListItem(ResHelper.GetString("general." + Action.SelectAction), Convert.ToInt32(Action.SelectAction).ToString()));
                drpAction.Items.Add(new ListItem(ResHelper.GetString("general." + Action.Move), Convert.ToInt32(Action.Move).ToString()));
                drpAction.Items.Add(new ListItem(ResHelper.GetString("general." + Action.Copy), Convert.ToInt32(Action.Copy).ToString()));
                drpAction.Items.Add(new ListItem(ResHelper.GetString("general." + Action.Link), Convert.ToInt32(Action.Link).ToString()));
                drpAction.Items.Add(new ListItem(ResHelper.GetString("general." + Action.Delete), Convert.ToInt32(Action.Delete).ToString()));
                if (currentUserInfo.IsGlobalAdministrator)
                {
                    drpAction.Items.Add(new ListItem(ResHelper.GetString("general." + Action.Publish), Convert.ToInt32(Action.Publish).ToString()));
                    drpAction.Items.Add(new ListItem(ResHelper.GetString("general." + Action.Archive), Convert.ToInt32(Action.Archive).ToString()));
                }

                drpWhat.Items.Add(new ListItem(ResHelper.GetString("contentlisting." + What.SelectedDocuments), Convert.ToInt32(What.SelectedDocuments).ToString()));
                drpWhat.Items.Add(new ListItem(ResHelper.GetString("contentlisting." + What.AllDocuments), Convert.ToInt32(What.AllDocuments).ToString()));
            }

            // Setup the grid
            gridDocuments.OnExternalDataBound += gridDocuments_OnExternalDataBound;
            gridDocuments.OnShowButtonClick += gridDocuments_OnShowButtonClick;
            gridDocuments.OnBeforeDataReload += gridDocuments_OnBeforeDataReload;

            string refreshTree = "function RefreshTree(){if(parent!=null)if(parent.parent!=null)if(parent.parent.frames['contenttree']!=null)if(parent.parent.frames['contenttree'].RefreshNode!=null)parent.parent.frames['contenttree'].RefreshNode(" + nodeId + "," + nodeId + ");}";
            string refreshScript = ScriptHelper.GetScript(refreshTree + "function RefreshGrid(){ClearSelection();RefreshTree();" + ClientScript.GetPostBackEventReference(btnShow, "null") + "}");
            ScriptHelper.RegisterClientScriptBlock(Page, typeof(string), "refreshListing", refreshScript);
        }
    }


    /// <summary>
    /// OnPreRender.
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        if (!dataLoaded)
        {
            gridDocuments.ReloadData();
        }

        // Hide column with languages if only one culture is assigned to the site
        if (DataHelper.DataSourceIsEmpty(SiteCultures) || (SiteCultures.Tables[0].Rows.Count <= 1))
        {
            DataControlField languagesColumn = gridDocuments.GridView.Columns[7];
            languagesColumn.Visible = false;
            plcLang.Visible = false;
        }
        else
        {
            if (FlagsControls.Count != 0)
            {
                // Get all document node IDs
                string nodeIds = null;
                foreach (DocumentFlagsControl ucDocFlags in FlagsControls)
                {
                    nodeIds += ucDocFlags.NodeID + ",";
                }

                if (nodeIds != null)
                {
                    nodeIds = nodeIds.TrimEnd(',');
                }

                // Get all documents
                tree.SelectQueryName = "SelectVersions";
                DataSet dsDocs = tree.SelectNodes(currentSiteName, "/%", TreeProvider.ALL_CULTURES, false, null, "NodeID IN (" + nodeIds + ")", null, -1, false, 0, "NodeID, VersionNumber, DocumentCulture, DocumentModifiedWhen, DocumentLastPublished");
                GroupedDataSource gDSDocs = new GroupedDataSource(dsDocs, "NodeID");

                // Initialize the document flags controls
                foreach (DocumentFlagsControl ucDocFlags in FlagsControls)
                {
                    ucDocFlags.DataSource = gDSDocs;
                    ucDocFlags.ReloadData();
                }
            }
        }
        const string script = "var where = '';";
        ScriptHelper.RegisterStartupScript(Page, typeof(string), WHERE_KEY, ScriptHelper.GetScript(script));

        base.OnPreRender(e);
    }

    #endregion


    #region "Grid events"

    protected void gridDocuments_OnShowButtonClick(object sender, EventArgs e)
    {
        gridDocuments.WhereCondition = GetWhereCondition();
        string script = "var where = " + ScriptHelper.GetString(ValidationHelper.GetString(gridDocuments.WhereCondition, string.Empty)) + ";";
        ScriptHelper.RegisterStartupScript(Page, typeof(string), WHERE_KEY, ScriptHelper.GetScript(script));
    }


    protected void gridDocuments_OnBeforeDataReload()
    {
        dataLoaded = true;

        if (node != null)
        {
            // Get the site
            SiteInfo si = SiteInfoProvider.GetSiteInfo(node.NodeSiteID);
            if (si != null)
            {
                // Get the filter where condition
                string whereCondition = SqlHelperClass.AddWhereCondition("NodeParentID = " + node.NodeID + " AND NodeLevel = " + (node.NodeLevel + 1), GetWhereCondition());
                string columns = SqlHelperClass.MergeColumns(DocumentHelper.GETDOCUMENTS_REQUIRED_COLUMNS, "DocumentName, DocumentNamePath, NodeParentID, ClassDisplayName, DocumentModifiedWhen, Published, VersionNumber");

                // Get documents
                DataSet ds = DocumentHelper.GetDocuments(currentSiteName, TreeProvider.ALL_DOCUMENTS, TreeProvider.ALL_CULTURES, true, null, whereCondition, "NodeOrder ASC, NodeName ASC, NodeAlias ASC", -1, false, gridDocuments.TopN, columns, tree);
                if (DataHelper.DataSourceIsEmpty(ds))
                {
                    lblInfo.Text = ResHelper.GetString("content.nochilddocumentsfound");
                    lblInfo.Visible = true;
                    pnlFooter.Visible = false;

                    string cultureFilterValue = ValidationHelper.GetString(cultureElem.Value, null);
                    string cultureOperatorValue = ValidationHelper.GetString(drpLanguage.SelectedValue, null);
                    bool cultureFilterOff = (cultureFilterValue == "##ANY##") || (cultureFilterValue == string.Empty) && (cultureOperatorValue == "=");
                    bool typeFilterOff = string.IsNullOrEmpty(txtType.Text);
                    bool nameFilterOff = string.IsNullOrEmpty(txtName.Text);

                    // If filter is not set
                    if (typeFilterOff && nameFilterOff && cultureFilterOff && !RequestHelper.IsPostBack())
                    {
                        pnlFilter.Visible = false;
                    }
                }
                else
                {
                    pnlFooter.Visible = true;
                }


                gridDocuments.DataSource = ds;
            }
        }
    }


    /// <summary>
    /// External data binding handler.
    /// </summary>
    protected object gridDocuments_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        int currentNodeId = 0;

        sourceName = sourceName.ToLower();
        switch (sourceName)
        {
            case "published":
                bool published = ValidationHelper.GetBoolean(parameter, true);
                if (published)
                {
                    return "<span class=\"DocumentPublishedYes\">" + ResHelper.GetString("General.Yes") + "</span>";
                }
                else
                {
                    return "<span class=\"DocumentPublishedNo\">" + ResHelper.GetString("General.No") + "</span>";
                }

            case "versionnumber":
                if (parameter == DBNull.Value)
                {
                    parameter = "-";
                }
                return parameter;

            case "documentname":
                DataRowView data = (DataRowView)parameter;
                string name = ValidationHelper.GetString(data["DocumentName"], string.Empty);
                string culture = ValidationHelper.GetString(data["DocumentCulture"], string.Empty);
                string cultureString = null;
                currentNodeId = ValidationHelper.GetInteger(data["NodeID"], 0);
                int nodeParentId = ValidationHelper.GetInteger(data["NodeParentID"], 0);

                // Default culture
                if (culture.ToLower() != CMSContext.PreferredCultureCode.ToLower())
                {
                    cultureString = " (" + culture + ")";
                }

                string result = "<a href=\"javascript: SelectItem(" + currentNodeId + ", " + nodeParentId + ");\">" + HTMLHelper.HTMLEncode(TextHelper.LimitLength(name, 50)) + cultureString + "</a>";
                bool isLink = (data["NodeLinkedNodeID"] != DBNull.Value);
                if (isLink)
                {
                    result += " " + UIHelper.GetDocumentMarkImage(this, DocumentMarkEnum.Link);
                }

                return result;

            case "documentnametooltip":
                data = (DataRowView)parameter;
                return UniGridFunctions.DocumentNameTooltip(data);

            case "documentculture":
                // Dynamically load document flags control
                DocumentFlagsControl ucDocFlags = Page.LoadControl("~/CMSAdminControls/UI/DocumentFlags.ascx") as DocumentFlagsControl;

                // Set document flags properties
                if (ucDocFlags != null)
                {
                    // Get node ID
                    currentNodeId = ValidationHelper.GetInteger(parameter, 0);

                    ucDocFlags.ID = "docFlags" + currentNodeId;
                    ucDocFlags.SiteCultures = SiteCultures;
                    ucDocFlags.NodeID = currentNodeId;
                    ucDocFlags.StopProcessing = true;

                    // Keep the control for later usage
                    FlagsControls.Add(ucDocFlags);
                    return ucDocFlags;
                }
                break;
            case "modifiedwhen":
            case "modifiedwhentooltip":
                if (string.IsNullOrEmpty(parameter.ToString()))
                {
                    return "";
                }
                else
                {
                    DateTime modifiedWhen = ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME);
                    if (currentUserInfo == null)
                    {
                        currentUserInfo = CMSContext.CurrentUser;
                    }
                    if (currentSiteInfo == null)
                    {
                        currentSiteInfo = CMSContext.CurrentSite;
                    }

                    if (sourceName == "modifiedwhen")
                    {
                        return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(modifiedWhen,
                            currentUserInfo, currentSiteInfo, out usedTimeZone);
                    }
                    else
                    {
                        if (TimeZoneHelper.TimeZonesEnabled() && (usedTimeZone == null))
                        {
                            TimeZoneHelper.GetCurrentTimeZoneDateTimeString(modifiedWhen,
                                                        currentUserInfo, currentSiteInfo, out usedTimeZone);
                        }
                        return TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone);
                    }
                }
        }
        return parameter;
    }

    #endregion


    #region "Methods"

    private string GetWhereCondition()
    {
        string where = null;
        string documentName = txtName.Text.Trim().Replace("'", "''");
        documentName = TextHelper.LimitLength(documentName, 100);
        // Name
        if (documentName != string.Empty)
        {
            switch (drpOperator.SelectedValue)
            {
                case "NOT LIKE":
                case "LIKE":
                    where = SqlHelperClass.AddWhereCondition(where, "DocumentName " + drpOperator.SelectedValue.Replace("'", "''") + " N'%" + documentName + "%'");
                    break;

                default:
                    where = SqlHelperClass.AddWhereCondition(where, "DocumentName " + drpOperator.SelectedValue.Replace("'", "''") + " N'" + documentName + "'");
                    break;
            }
        }

        string documentType = txtType.Text.Trim().Replace("'", "''");
        documentType = TextHelper.LimitLength(documentType, 100);
        // Type
        if (documentType != string.Empty)
        {
            switch (drpOperator2.SelectedValue)
            {
                case "NOT LIKE":
                case "LIKE":
                    where = SqlHelperClass.AddWhereCondition(where, "ClassDisplayName " + drpOperator2.SelectedValue.Replace("'", "''") + " N'%" + documentType + "%'");
                    break;

                default:
                    where = SqlHelperClass.AddWhereCondition(where, "ClassDisplayName " + drpOperator2.SelectedValue.Replace("'", "''") + " N'" + documentType + "'");
                    break;
            }
        }

        // Translated
        string val = ValidationHelper.GetString(cultureElem.Value, string.Empty);
        if (val == string.Empty)
        {
            val = "##ANY##";
        }

        if (val != "##ANY##")
        {
            switch (val)
            {
                case "##ALL##":
                    where = SqlHelperClass.AddWhereCondition(where, "((SELECT COUNT(*) FROM View_CMS_Tree_Joined AS TreeView WHERE TreeView.NodeID = View_CMS_Tree_Joined_Versions.NodeID) " + drpLanguage.SelectedValue.Replace("'", "''") + " " + SiteCultures.Tables[0].Rows.Count + ")");
                    break;

                default:
                    string oper = (drpLanguage.SelectedValue == "<>") ? "NOT" : "";
                    where = SqlHelperClass.AddWhereCondition(where, "NodeID " + oper + " IN (SELECT NodeID FROM View_CMS_Tree_Joined AS TreeView WHERE TreeView.NodeID = NodeID AND DocumentCulture = '" + val.Replace("'", "''") + "')");
                    break;
            }
        }
        else if (drpLanguage.SelectedValue == "<>")
        {
            where = SqlHelperClass.NO_DATA_WHERE;
        }

        return where;
    }


    private void InitDropdowLists()
    {
        // Init cultures
        cultureElem.DropDownCultures.CssClass = "SelectorDropDown";
        cultureElem.AddDefaultRecord = false;
        cultureElem.UpdatePanel.RenderMode = UpdatePanelRenderMode.Inline;
        cultureElem.SpecialFields = new string[,] { 
            { ResHelper.GetString("transman.anyculture"), "##ANY##" } ,
            { ResHelper.GetString("transman.allcultures"), "##ALL##" } 
        };

        // Init operands
        if (drpLanguage.Items.Count == 0)
        {
            drpLanguage.Items.Add(new ListItem(ResHelper.GetString("transman.translatedto"), "="));
            drpLanguage.Items.Add(new ListItem(ResHelper.GetString("transman.nottranslatedto"), "<>"));
        }

        // Init operands
        if (drpOperator.Items.Count == 0)
        {
            drpOperator.Items.Add(new ListItem("LIKE", "LIKE"));
            drpOperator.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
            drpOperator.Items.Add(new ListItem("=", "="));
            drpOperator.Items.Add(new ListItem("<>", "<>"));
        }

        // Init operands
        if (drpOperator2.Items.Count == 0)
        {
            drpOperator2.Items.Add(new ListItem("LIKE", "LIKE"));
            drpOperator2.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
            drpOperator2.Items.Add(new ListItem("=", "="));
            drpOperator2.Items.Add(new ListItem("<>", "<>"));
        }
    }


    /// <summary>
    /// Returns Correct URL of the copy or move dialog.
    /// </summary>
    /// <param name="nodeIds">IDs of the nodes to be copied or moved</param>
    /// <param name="action">Action to perform</param>
    /// <param name="parentAlias">Parent node alias path</param>
    /// <param name="whereCondition">Where condition for all filtered documents</param>
    private string GetDialogUrl(string nodeIds, Action action, string parentAlias, string whereCondition)
    {
        Config.CustomFormatCode = action.ToString().ToLower();

        string url = CMSDialogHelper.GetDialogUrl(Config, false, false);

        url = UrlHelper.RemoveParameterFromUrl(url, "hash");
        if (!string.IsNullOrEmpty(parentAlias))
        {
            url = UrlHelper.AddParameterToUrl(url, "parentalias", parentAlias);
        }
        if (!string.IsNullOrEmpty(nodeIds))
        {
            url = UrlHelper.AddParameterToUrl(url, "sourcenodeids", nodeIds);
        }
        url = UrlHelper.AddParameterToUrl(url, "multiple", "true");
        if (!string.IsNullOrEmpty(whereCondition))
        {
            url = UrlHelper.AddParameterToUrl(url, "where", whereCondition);
        }
        url = UrlHelper.AddParameterToUrl(url, "hash", QueryHelper.GetHash(url));
        url = UrlHelper.EncodeQueryString(url);
        return url;
    }

    #endregion


    #region "ICallbackEventHandler Members"

    public string GetCallbackResult()
    {
        string whereCondition = string.Empty;

        if (!string.IsNullOrEmpty(where))
        {
            whereCondition = HttpUtility.UrlPathEncode(where).Replace("&", "%26").Replace("#", "%23").Replace("'", "\'");
        }

        string returnUrl = string.Empty;

        switch (callbackAction)
        {
            case Action.Copy:
            case Action.Move:
            case Action.Link:
                returnUrl = GetDialogUrl(urlParameter, callbackAction, (allSelected ? aliasPath : null), whereCondition);
                break;

            case Action.Delete:
                string deleteUrl = "../Delete.aspx?multiple=true";
                if (allSelected)
                {
                    if (!string.IsNullOrEmpty(aliasPath))
                    {
                        deleteUrl += "&parentaliaspath=" + aliasPath;
                    }
                    if (!string.IsNullOrEmpty(whereCondition))
                    {
                        deleteUrl += "&where=" + whereCondition;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(urlParameter))
                    {
                        deleteUrl += "&nodeid=" + urlParameter;
                    }
                }
                returnUrl = ResolveUrl(deleteUrl);
                break;

            case Action.Archive:
                string archiveUrl = "../PublishArchive.aspx?multiple=true&action=" + Action.Archive;
                if (allSelected)
                {
                    if (!string.IsNullOrEmpty(aliasPath))
                    {
                        archiveUrl += "&parentaliaspath=" + aliasPath;
                    }
                    if (!string.IsNullOrEmpty(whereCondition))
                    {
                        archiveUrl += "&where=" + whereCondition;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(urlParameter))
                    {
                        archiveUrl += "&nodeids=" + urlParameter;
                    }
                }
                returnUrl = ResolveUrl(archiveUrl);
                break;

            case Action.Publish:
                string publishUrl = "../PublishArchive.aspx?multiple=true&action=" + Action.Publish;
                if (allSelected)
                {
                    if (!string.IsNullOrEmpty(aliasPath))
                    {
                        publishUrl += "&parentaliaspath=" + aliasPath;
                    }
                    if (!string.IsNullOrEmpty(whereCondition))
                    {
                        publishUrl += "&where=" + whereCondition;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(urlParameter))
                    {
                        publishUrl += "&nodeids=" + urlParameter;
                    }
                }
                returnUrl = ResolveUrl(publishUrl);
                break;
        }
        return returnUrl;
    }


    public void RaiseCallbackEvent(string eventArgument)
    {
        string[] arguments = eventArgument.Trim('|').Split('|');
        if (arguments.Length > 1)
        {
            // Parse callback arguments
            callbackAction = (Action)Enum.Parse(typeof(Action), arguments[(int)Argument.Action]);
            allSelected = ValidationHelper.GetBoolean(arguments[(int)Argument.AllSelected], false);
        }

        if (arguments.Length > 2)
        {
            if (allSelected)
            {
                // Get where condition
                where = ValidationHelper.GetString(arguments[(int)Argument.WhereCondition], null);
            }
            else
            {
                // Get selected node identifiers
                for (int i = (int)Argument.Items; i < arguments.Length; i++)
                {
                    urlParameter += arguments[i] + "|";
                }
            }
        }
    }

    #endregion
}
