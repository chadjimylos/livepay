<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QueryEdit.ascx.cs" Inherits="CMSModules_AdminControls_Controls_Class_QueryEdit" %>
<%@ Register Src="~/CMSAdminControls/UI/Selectors/LoadGenerationSelector.ascx" TagName="LoadGenerationSelector"
    TagPrefix="uc1" %>
<asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
    Visible="false" />
<table style="width: 100%">
    <tr>
        <td class="FieldLabel">
            <cms:LocalizedLabel runat="server" ID="lblQueryName" EnableViewState="false" ResourceString="DocumentType_Edit_Query_Edit.QueryName" />
        </td>
        <td>
            <table width="100%">
                <tr>
                    <td>
                        <asp:TextBox runat="server" ID="txtQueryName" CssClass="TextBoxField" MaxLength="100"
                            EnableViewState="false" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorQueryName" runat="server" EnableViewState="false"
                            ControlToValidate="txtQueryName" Display="dynamic" />
                    </td>
                    <td class="TextRight">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <cms:LocalizedLabel runat="server" ID="lblQueryType" EnableViewState="false" ResourceString="DocumentType_Edit_Query_Edit.QueryType" />
        </td>
        <td>
            <asp:RadioButtonList runat="server" ID="rblQueryType" RepeatDirection="Horizontal"
                EnableViewState="false">
                <asp:ListItem Selected="True" Value="SQLQuery">Query text</asp:ListItem>
                <asp:ListItem Value="StoredProcedure">Stored procedure</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <cms:LocalizedLabel runat="server" ID="lblTransaction" EnableViewState="false" ResourceString="DocumentType_Edit_Query_Edit.TransactionRequired" />
        </td>
        <td>
            <asp:CheckBox runat="server" ID="chbTransaction" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <cms:LocalizedLabel runat="server" ID="lblAutoUpdates" EnableViewState="false" ResourceString="DocumentType_Edit_Query_Edit.AutoUpdates" />
        </td>
        <td>
            <asp:CheckBox runat="server" ID="chbAutoUpdates" EnableViewState="false" />
        </td>
    </tr>
    <asp:PlaceHolder runat="server" ID="plcIsCustom" EnableViewState="false">
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblIsCustom" EnableViewState="false" ResourceString="Query.IsCustom"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:CheckBox ID="chckIsCustom" runat="server" Checked="true" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td style="vertical-align: text-top; padding-top: 5px" class="FieldLabel">
            <cms:LocalizedLabel runat="server" ID="lblQueryText" EnableViewState="false" ResourceString="DocumentType_Edit_Query_Edit.QueryText" />
        </td>
        <td style="width: 85%">
            <cms:LocalizedButton ID="btnGenerate" runat="server" CssClass="XLongButton" OnClick="btnGenerate_Click"
                ResourceString="DocumentType_Edit_Query_Edit.btnGenerate" EnableViewState="false"
                Visible="false" />
            <asp:TextBox runat="server" ID="txtQueryText" TextMode="multiline" CssClass="TextAreaLarge"
                EnableViewState="false" Width="100%" />
        </td>
    </tr>
    <asp:PlaceHolder runat="server" ID="plcLoadGeneration" EnableViewState="false">
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblLoadGeneration" EnableViewState="false"
                    ResourceString="LoadGeneration.Title" />
            </td>
            <td>
                <uc1:LoadGenerationSelector ID="drpGeneration" runat="server" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            <cms:LocalizedButton runat="server" ID="btnOk" EnableViewState="false" OnClick="btnOK_Click"
                ResourceString="general.ok" CssClass="SubmitButton" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <asp:Label ID="lblHelpHeader" runat="server" CssClass="InfoLabel" EnableViewState="false" />
            <asp:Table ID="tblHelp" runat="server" EnableViewState="false" GridLines="Horizontal"
                CellPadding="3" CellSpacing="0" CssClass="UniGridGrid" Width="100%">
            </asp:Table>
        </td>
    </tr>
</table>
