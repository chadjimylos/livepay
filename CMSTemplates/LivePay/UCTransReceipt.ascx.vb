﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports LivePay_ESBBridge
Imports System.Collections.Generic
Imports System.Globalization


Partial Class CMSTemplates_LivePay_UCTransReceipt
    Inherits CMSUserControl


    Public ReadOnly Property TransGUID() As String
        Get
            Dim sTransID As String = String.Empty
            If Not String.IsNullOrEmpty(Request("TransID")) Then
                sTransID = Request("TransID")
            End If
            Return sTransID
        End Get
    End Property

    Protected ReadOnly Property Result() As String
        Get
            Dim sResult As String = String.Empty
            If Not String.IsNullOrEmpty(Request("Result")) Then
                sResult = Request("Result").ToLower
            End If
            Return sResult
        End Get
    End Property

    Private mSystem As LivePay_ESBBridge.SystemEnum
    Public Property System() As LivePay_ESBBridge.SystemEnum
        Get
            Return mSystem
        End Get
        Set(ByVal value As LivePay_ESBBridge.SystemEnum)
            mSystem = value
        End Set
    End Property

    Private TimeoutLivePayBridge As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutLivePayBridge"))

#Region "Methods"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        btnSave.ImageUrl = ResHelper.LocalizeString("{$=/app_themes/LivePay/BtnSave.png|en-us=/app_themes/LivePay/BtnSave_en-us.png$}")
        divDateAndTime.InnerHtml = ResHelper.LocalizeString("{$=Ημερομηνία & Ώρα|en-us=Date & Time$}")

        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub


    Public Overloads Sub ReloadData()
        Session("TransReceipt_TransIDForExport") = String.Empty
        Session("TransReceipt_SystemForExport") = String.Empty

        
        Dim IsLogedIn As Boolean = CMSContext.CurrentUser.IsPublic()
        Dim CustID As Integer = 0
        MessageTitle.InnerHtml = ResHelper.LocalizeString("{$=Πραγματοποιήσατε με επιτυχία την παρακάτω πληρωμή|en-us=You have successfully completed the following payment$}")
        Dim UserID As Int32 = 0

        'Dim xaxa As Guid = New Guid("59B4B220-6D92-4C35-8AB3-4C3767EF3D08")

        If Me.TransGUID <> String.Empty AndAlso Me.Result = String.Empty Then
            'xaxa.ToString <> String.Empty AndAlso Me.Result = String.Empty Then

            Dim TransactionID As String = "0"
            Dim PublicTransactionID As String = String.Empty
            Dim ClassName As String = String.Empty
            Dim MerchantName As String = String.Empty
            Dim myGuid As Guid = Nothing
            myGuid = New Guid(Me.TransGUID) 'xaxa '

            Dim dt As DataTable = DBConnection.GetMerchantByGUID(myGuid)

            If dt.Rows.Count > 0 Then
                TransactionID = Trim(dt.Rows(0)("MerchantTransactionID").ToString)
                PublicTransactionID = Trim(dt.Rows(0)("PublicTransID").ToString)
                ClassName = dt.Rows(0)("ClassName").ToString
                UserID = Convert.ToInt32(dt.Rows(0)("UserID").ToString())
                'Response.Write("dt.Rows(0)('Public').ToString - > " & dt.Rows(0)("Public").ToString)
                'Response.End()
                Me.System = IIf(dt.Rows(0)("Public").ToString.ToLower = "true", LivePay_ESBBridge.SystemEnum.Public, LivePay_ESBBridge.SystemEnum.Merchants)
                If Me.System = LivePay_ESBBridge.SystemEnum.Public Then
                    Session("TransReceipt_TransIDForExport") = PublicTransactionID
                    Session("TransReceipt_SystemForExport") = Me.System
                    divDateAndTime.InnerHtml = ResHelper.LocalizeString("{$=Ημερομηνία|en-us=Date$}")
                Else
                    Session("TransReceipt_TransIDForExport") = TransactionID
                    Session("TransReceipt_SystemForExport") = Me.System
                    divDateAndTime.InnerHtml = ResHelper.LocalizeString("{$=Ημερομηνία & Ώρα|en-us=Date & Time$}")
                End If
                MerchantName = dt.Rows(0)("DiscreetTitle").ToString

                If dt.Rows(0)("AcknowledgementTransaction").ToString <> String.Empty Then
                    Session("AcknowledgementTransaction_TransIDForExport") = dt.Rows(0)("AcknowledgementTransaction").ToString
                    tdAcknowledgementTransaction.InnerHtml = dt.Rows(0)("AcknowledgementTransaction").ToString
                Else
                    Session("AcknowledgementTransaction_TransIDForExport") = ""
                    tdAcknowledgementTransaction.InnerHtml = ""
                End If

            End If

            'Response.Write("TransactionID & - > " & TransactionID)
            'Response.Write("PublicTransactionID & - > " & PublicTransactionID)

            If TransactionID <> "0" Or PublicTransactionID <> "" Then
                Dim w As New LivePay_ESBBridge.Bridge
                w.Timeout = TimeoutLivePayBridge
                'Response.Write("IIf(CMSContext.CurrentDocument.NodeClassName = " & IIf(ClassName = "LivePay.MerchantPublicSector", LivePay_ESBBridge.SystemEnum.Public, LivePay_ESBBridge.SystemEnum.Merchants))
                'Response.Write(" TransactionID --> " & TransactionID)
                'Response.End()
                Dim TransResponse As New TransactionDetailsResponse
                If PublicTransactionID <> "" Then
                    'Response.Write(LivePay_ESBBridge.SystemEnum.Merchants)
                    TransResponse = w.GetTransactionDetails(PublicTransactionID, IIf(ClassName = "LivePay.MerchantPublicSector", LivePay_ESBBridge.SystemEnum.Public, LivePay_ESBBridge.SystemEnum.Merchants))
                Else
                    TransResponse = w.GetTransactionDetails(TransactionID, IIf(ClassName = "LivePay.MerchantPublicSector", LivePay_ESBBridge.SystemEnum.Public, LivePay_ESBBridge.SystemEnum.Merchants))
                End If

                Dim TransInfo As TransactionDetailsInfo = TransResponse.TransactionDetails

                'Response.Write("TransInfo --> " & Not IsNothing(TransInfo))
                'Response.Write("TransInfo.customerId --> " & TransInfo.customerId)
                'Response.End()
                'Response.Write("TransInfo.info1 --> " & Not IsNothing(TransInfo))
                If Not IsNothing(TransInfo) Then
                    'Response.Write("TransInfo.info1 --> " & TransInfo.info1)
                    'Response.End()
                    CustID = TransInfo.customerId
                    tdMerchant.InnerHtml = MerchantName

                    If Me.System = LivePay_ESBBridge.SystemEnum.Public Then
                        tdDateTime.InnerHtml = TransInfo.transactionDate.ToString("d", CultureInfo.CurrentCulture)
                        'Response.Write("TransInfo.info1 -> " & TransInfo.info1)
                        'tdTransCode.InnerHtml = TransInfo.info1
                        'tdTransCode.InnerHtml = "---"
                        txtTransCode.Visible = False
                        tdTransCode.Visible = False
                    Else
                        tdDateTime.InnerHtml = TransInfo.transactionDate
                        tdTransCode.InnerHtml = TransInfo.transactionId
                        'AcknowledgementTransaction.InnerHtml = TransInfo.TransInfo.transactionId
                    End If
                    'tdTransCode.InnerHtml = TransInfo.transactionId
                    tdCustCode.InnerHtml = TransInfo.customerId


                    If TransInfo.installments > 0 Then
                        tdInstallments.InnerHtml = TransInfo.installments
                        pnlInstallments.Visible = True
                    End If

                    Dim amount As String = Replace(TransInfo.transactionAmount, ".", ",")
                    Dim FinalAmount As String
                    If amount.Split(",").Length > 1 Then
                        FinalAmount = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
                    ElseIf amount.Split(",").Length = 1 Then
                        FinalAmount = amount & ",00"
                    Else
                        FinalAmount = amount
                    End If

                    tdPrice.InnerHtml = FinalAmount

                    If TransInfo.commission <> 0 Then
                        plc_Commission.Visible = True
                        tdCommission.InnerHtml = TransInfo.commission.ToString("#0.00").PadLeft(3).Replace(".", ",")
                    End If

                    Dim CardNo As String = Replace(TransInfo.cardNumber, " ", String.Empty)
                    tdCard.InnerHtml = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                    CreateCustomFields(TransInfo.merchantId, TransInfo)


                End If
            End If
        End If

        If Result <> String.Empty Then
            imgResult.Src = "/app_themes/LivePay/ErrorIcon.png"
            MessageTitle.Style.Add("color", "#dc2c09")
            MessageTitle.InnerHtml = ResHelper.LocalizeString("{$=Παρουσιάστηκε σφάλμα|en-us=Transaction error$}")
            MainDivForm.Style.Add("display", "none")
            MainDivErrorContainer.Style.Add("display", "")
            Dim ErrorMessage As String = String.Empty
            If Me.Result = "esberror" Then
                ErrorMessage = String.Concat(ErrorMessage, "<div class=''>" & ResHelper.LocalizeString("{$=Για περισσότερες πληροφορίες σχετικά με τη αδυναμία ολοκλήρωσης της πληρωμής σας παρακαλούμε επικοινωνήστε με το EuroPhone Banking στο 2109555019|en-us=For additional information regarding the transaction error, please contact EuroPhone Banking  at 210-9555019$}") & "</div><div>")
            Else
                ErrorMessage = String.Concat(ErrorMessage, "<div  class=''>" & ResHelper.LocalizeString("{$=Για περισσότερες πληροφορίες σχετικά με τη αδυναμία ολοκλήρωσης της πληρωμής σας παρακαλούμε επικοινωνήστε με το EuroPhone Banking στο 2109555019|en-us=For additional information regarding the transaction error, please contact EuroPhone Banking  at 210-9555019$}") & "</div><div>")
            End If
            Try : ErrorMessage = String.Concat(GetESBError(Session("MakePaymentError")), ErrorMessage) : Catch : End Try
            ErrorMessage = String.Concat(ErrorMessage, "</div>")
            MainDivErrorContainer.InnerHtml = ErrorMessage

        End If

        'Response.Write("CustID -> " & CustID)
        'If CustID = 0 Then    '- ----- and o xristis den einai melos
        'Response.Write(CMSContext.CurrentUser.IsAuthenticated())

        'Response.Write(CMSContext.CurrentUser.IsAuthenticated())

        'If Not CMSContext.CurrentUser.IsAuthenticated() AndAlso Not (CMSContext.CurrentUser.UserID = UserID) Then    '- ----- and o xristis den einai melos
        If Not (CMSContext.CurrentUser.UserID = UserID) Then    '- ----- and o xristis den einai melos
            'If True Then    '- ----- and o xristis den einai melos
            btnSave.Visible = False
        End If
        If Not CMSContext.CurrentUser.IsAuthenticated() Then
            btnSave.Visible = False
        End If

    End Sub

    Private Function GetESBError(ByVal ErrorCode As String) As String
        Dim dt As DataTable = DBConnection.GetESBError(CMSContext.CurrentDocumentCulture.CultureCode, ErrorCode)
        If dt.Rows.Count > 0 Then
            Return ErrorCode & ": " & dt.Rows(0)("ErrorMessage").ToString
        End If
    End Function

    Private Sub CreateCustomFields(ByVal MerchantID As String, ByVal TransInfo As TransactionDetailsInfo)

        Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(MerchantID, CMSContext.CurrentDocumentCulture.CultureCode)
        Dim LivePay_MerchantID As Integer = 0
        If dtMerc.Rows.Count > 0 Then
            LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
        End If

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(LivePay_MerchantID)
        Dim dt As DataTable = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim CustomTitle As New HtmlGenericControl("div")
            Dim CustomValue As New HtmlGenericControl("div")
            Dim CustomClear As New HtmlGenericControl("div")
            CustomTitle.Attributes.Add("class", "TransRecLeftRow")
            CustomValue.Attributes.Add("class", "TransRecRightRow")
            CustomClear.Attributes.Add("class", "Clear")
            CustomTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
            Select Case i
                Case 0
                    CustomValue.InnerHtml = TransInfo.info1
                Case 1
                    CustomValue.InnerHtml = TransInfo.info2
                Case 2
                    CustomValue.InnerHtml = TransInfo.info3
                Case 3
                    CustomValue.InnerHtml = TransInfo.info4
                Case 4
                    CustomValue.InnerHtml = TransInfo.info5
                Case 5
                    CustomValue.InnerHtml = TransInfo.info6
                Case 6
                    CustomValue.InnerHtml = TransInfo.info7
                Case 7
                    CustomValue.InnerHtml = TransInfo.info8
                Case 8
                    CustomValue.InnerHtml = TransInfo.info9
                Case 9
                    CustomValue.InnerHtml = TransInfo.info10
            End Select
            With CustomFieldsPanel.Controls
                .Add(CustomTitle)
                .Add(CustomValue)
                .Add(CustomClear)
            End With

        Next
    End Sub

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim TransactionID As String = "0"
        Dim myGuid As Guid = Nothing
        myGuid = New Guid(Me.TransGUID)

        Dim dt As DataTable = DBConnection.GetMerchantByGUID(myGuid)
        If dt.Rows.Count > 0 Then
            TransactionID = dt.Rows(0)("MerchantTransactionID").ToString
            DBConnection.UpdateTransaction(TransactionID, 2, 1)

            For Each Field As CustomFields In LivePayCustomField.ArrCustomField
                If Field.UserID = CMSContext.CurrentUser.UserID Then
                    DBConnection.InsertFavoritesCustomFields(TransactionID, Field.FieldID, Field.FieldValue)
                End If

            Next

            Response.Redirect("SavedTransactions.aspx")
        End If

    End Sub

    'Protected Sub btnSaveToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveToPDF.Click
    '    Dim TransactionID As String = "0"
    '    Dim myGuid As Guid = Nothing
    '    myGuid = New Guid(Me.TransGUID)
    '    Dim dt As DataTable = DBConnection.GetMerchantByGUID(myGuid)
    '    If dt.Rows.Count > 0 Then
    '        TransactionID = dt.Rows(0)("MerchantTransactionID").ToString
    '        Session("TransReceipt_TransIDForExport") = TransactionID
    '        Response.Redirect("/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=TransReceipt")
    '    End If


    'End Sub

End Class
