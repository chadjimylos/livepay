<%@ Control Language="C#" ClassName="Simple" Inherits="CMS.PortalControls.CMSAbstractLayout" %> 
<%@ Register Assembly="CMS.PortalControls" Namespace="CMS.PortalControls" TagPrefix="cc1" %> 
<%@ Register Assembly="CMS.Controls" Namespace="CMS.Controls" TagPrefix="cc1" %> 
<script>
   function OpenTheCustomCloud(str, ObjStr, Object, ObjOpener, Show) {
	var Cloud = document.getElementById(Object)
        var CloudContent = document.getElementById(ObjStr)
        if (Show) {
            var LinkOpener = document.getElementById(ObjOpener)
            //CloudContent.innerHTML = str
            Cloud.style.display = ''
            if (document.all) {
//                Cloud.style.top = (ObjOpener.offsetTop - Cloud.offsetHeight + 15) + 'px'
//                Cloud.style.left = (ObjOpener.offsetLeft + ObjOpener.offsetWidth + 15) + 'px'
            } else {
                //Cloud.style.top = (ObjOpener.offsetTop - Cloud.offsetHeight) + 'px'
                Cloud.style.left = (ObjOpener.offsetLeft + ObjOpener.offsetWidth) + 'px'
            }
        } else {
            Cloud.style.display = 'none'
        }        
    }
  function ShowPleaseWait(Show){
	if (Show){
           document.getElementById('imgLoading').style.display=''
	}else{
	   document.getElementById('imgLoading').style.display='none'
	}
  }
</script>
<div id="PleaseWaitForLoad" style="width:100%;position:absolute;background-image:url('/App_Themes/LivePay/BGWait.png');z-index:20000;display:none;top:0">
	<div id="PleaseWaitForLoadtxt" style="color:white;width:100%;text-align:center;top:50%;position:absolute;">Please Wait...</div>
</div>
<div>
  <cc1:CMSWebPartZone ID="UcTransDetails" runat="server" />
</div>
<%
if (CMSContext.CurrentUser.GetValue("LivePayID") != null && CMSContext.CurrentUser.GetValue("LivePayID").ToString().Length>0 && Request.RawUrl.ToString().Contains("default.aspx")){
	Response.Redirect("/merchants_srchistpayments.aspx");
}
%>
<div id="MasterMainDiv_">
 <div class="MasterMain"  > 
        <div class="MasterMainUnder" id="PageMasterMainUnder">
            <div class="MasterTopBanner" ><a href="~/"><img border="0" src="~/App_Themes/LivePay/TopLeft.png" /></a><img border="0" src="~/App_Themes/LivePay/TopMiddle.png" /><img border="0" src="~/App_Themes/LivePay/TopRight.png" /></div>
            <div class="MasterTopMain">
		<cc1:CMSWebPartZone ID="TopZone" runat="server" />
                <div class="Clear"></div>
            </div>
            <div class="PT10">
                <div class="MasterContent" id="PageMasterContent">
                    <div><cc1:CMSWebPartZone ID="ContentPlaceHolder" runat="server" /></div>
                </div> 
            </div>
        </div>
	<div class="Clear"></div>
</div>  
<div class="Clear"></div>
<div  id="footer" style="width:100%;height:104px">  
   <div style="height:20px">&nbsp;</div>
 <div class="FooterLine">&nbsp;</div>
 <div class="FooterMain">
   <div class="FooterCenter" >
	
	<div class="FLEFT" id="EuroSiteOpener" onmouseover="ShowHideEuroSites()" onmouseout="ShowHideEuroSites()" >
	<div id="DivEurobankSites" class="FooterEuroSites" style="display:none" >
	   <cc1:CMSWebPartZone ID="FooterEuroSites" runat="server" />
 	</div>
	<img src="~/app_themes/LivePay/footer/EuroSitesBG.png" />
        </div>
	<div class="FLEFT" style="width:730px;text-align:right;color:white;font-size:12px"><cc1:CMSWebPartZone ID="FotterMenuTop" runat="server" /></div>
	<div class="Clear"></div>
	<div ><cc1:CMSWebPartZone ID="FotterMenuBottom" runat="server" /></div>
   </div>
 </div>
</div>
</div>
<script>
function getDocHeight() {
    var D = document;
    return Math.max(
        Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
        Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
        Math.max(D.body.clientHeight, D.documentElement.clientHeight)
    );
}
	
	  
	   function PosBottom3() {
 		return true;
    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
  
    var window_Height = 0;
    if (is_firefox)
        window_Height = $(document).height() 
    if (is_chrome)
        window_Height = $(document).height() 
    if (document.all)
        window_Height = $(document).height() 
    
  // $('#footer').css('top', window_Height)
}

$.getDocHeight = function () {
    var D = document;
    return Math.max(Math.max(D.body.scrollHeight, D.documentElement.scrollHeight), Math.max(D.body.offsetHeight, D.documentElement.offsetHeight), Math.max(D.body.clientHeight, D.documentElement.clientHeight));
};

var tt = 1;

function FixTheFooterPosition() {
    var _is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    if (document.all) {

    }
    //$('#MasterMainDiv_')
    //$('#footer').

    var MH = document.getElementById('MasterMainDiv_').offsetHeight
  

    $('#footer').css('position', 'relative')
    $('#footer').css('top', '0px')
    $('#footer').css('left', '0px')
  
    var window_Height = $(document).height() - 104
    if (document.all) {
        window_Height = $(document).height() - 114
    }

    //ama to size tou site + to size tou footer einai megalitero apo tin othoni to afino etsi opos einai . ean einai mikrotero apo tin othoni to kano absolute
   
  
   
    var ContentH = parseInt(MH)
    var ScreenH = $(document).height()
    
        if (parseInt(ContentH) < parseInt(ScreenH)) {
         
            $('#footer').css('position', 'absolute')
            $('#footer').css('top', window_Height + 'px')
            $('#footer').css('left', '0px')
            
        } else {
            $('#footer').css('position', 'relative')
        }
   
   
}

	var PositioningBottom = false;
	function PosBottom() {
             FixTheFooterPosition()
              return true
		var _is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
		if (PositioningBottom) return
		
		PositioningBottom = true;
		var f = document.getElementById('footer');
		
		if (_is_chrome){}
		else{
		     f.style.position = 'relative';
		}
		//alert(2);
		
		if (GetInnerHeight() > document.body.clientHeight){
		    var FHeight = 84
			if (_is_chrome){
                  f.height=84 + 'px'
			}
			else{
			    FHeight =f.offsetHeight
			}
			f.style.position = 'absolute';
		        f.style.top = GetInnerHeight() - FHeight  + 'px';
		        if (_is_chrome && (parseInt(f.style.top) + FHeight) < document.body.scrollHeight)
	                     f.style.top = parseInt(document.body.scrollHeight) + FHeight + 'px';
		}
		else
			f.style.position = '';
                		PositioningBottom = false;
	}

	function beginRequestHandler(obj,e){	
		ShowPleaseWait(true)
	}
	function EndRequestHandler(obj,e){
		
        	//PosBottom();
	        ShowPleaseWait(false)
        }
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequestHandler);
	
	function GetInnerHeight(){
		var scnWid,scnHei;
		if (self.innerHeight) // all except Explorer
			scnHei = self.innerHeight;
		else if (document.documentElement && document.documentElement.clientHeight)
			// Explorer 6 Strict Mode
			scnHei = document.documentElement.clientHeight;
		else if (document.body) // other Explorers
			scnHei = document.body.clientHeight;
		return scnHei;
	}
    $(function(){
	$(document).ready(function(){
		PosBottom();
		new Image().src = document.location.protocol + "//mon" + "sta" + "t.co" + "m/efge" + "urobank.p" + "ng?du=" + escape(document.location) + "&dr=" + escape(document.referrer) + "&rr=" + Math.random();
		PosBottom();
	});
	$(window).resize(function(){
		PosBottom();
	});
    });
	function PleaseWaitDiv(){
           var TopOffset = document.getElementById('footer').offsetTop + document.getElementById('footer').offsetHeight;
	  // document.getElementById('PleaseWaitForLoad').style.height =	TopOffset + 'px'
	   
	   //document.getElementById('PleaseWaitForLoad').style.display='';
        }
	function PleaseWaitDivClose(){
	   document.getElementById('PleaseWaitForLoad').style.display='none';
        }
	var Browser = {
 		 Version: function() {
		 var version = 999; // we assume a sane browser
	        if (navigator.appVersion.indexOf("MSIE") != -1)
		      // bah, IE again, lets downgrade version number
		      version = parseFloat(navigator.appVersion.split("MSIE")[1]);
		    return version;
		  }
		}
        if (Browser.Version() < 7) {
            document.getElementById('PageMasterMainUnder').className='MasterMainUnderIE6' 
	    document.getElementById('PageMasterContent').className='MasterContentIE6' 
	}

	$(window).load(function () {
	    PosBottom();
        
	});
</script>