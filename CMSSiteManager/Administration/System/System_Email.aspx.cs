using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Net.Mail;
using System.IO;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.EmailEngine;
using CMS.UIControls;
using CMS.EventLog;

public partial class CMSSiteManager_Administration_System_System_Email : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize controls
        lblServer.Text = ResHelper.GetString("System_Email.SMTPServer");
        lblUserName.Text = ResHelper.GetString("System_Email.SMTPUserName");
        lblPassword.Text = ResHelper.GetString("System_Email.SMTPPassword");
        lblFrom.Text = ResHelper.GetString("System_Email.From");
        lblTo.Text = ResHelper.GetString("System_Email.To");
        lblText.Text = ResHelper.GetString("System_Email.Text");
        btnSend.Text = ResHelper.GetString("System_Email.Send");
        lblEncoding.Text = ResHelper.GetString("System_Email.Encoding");
        lblSSL.Text = ResHelper.GetString("System_Email.SSL");
        lblAttachment.Text = ResHelper.GetString("System_Email.Attachment");

        // Initialize required field validators
        rfvServer.ErrorMessage = ResHelper.GetString("System_Email.ErrorServer");
        revFrom.ErrorMessage = ResHelper.GetString("System_Email.ErrorEmail");
        rfvFrom.ErrorMessage = ResHelper.GetString("System_Email.EmptyEmail");
        revFrom.ValidationExpression = @"^[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?$";
        revTo.ErrorMessage = revFrom.ErrorMessage;
        rfvTo.ErrorMessage = rfvFrom.ErrorMessage;
        revTo.ValidationExpression = @"^([A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?(;)?)+$";

        if (!RequestHelper.IsPostBack())
        {
            // Fill SMTP fields
            if (CMSContext.CurrentSite != null)
            {
                string siteName = CMSContext.CurrentSiteName;
                txtServer.Text = SettingsKeyProvider.GetStringValue(siteName + ".CMSSMTPServer");
                txtUserName.Text = SettingsKeyProvider.GetStringValue(siteName + ".CMSSMTPServerUser");
                txtPassword.Text = SettingsKeyProvider.GetStringValue(siteName + ".CMSSMTPServerPassword");
                chkSSL.Checked = SettingsKeyProvider.GetBoolValue(siteName + ".CMSUseSSL");
            }
        }
    }


    /// <summary>
    /// On btnSend click.
    /// </summary>
    protected void btnSend_Click(object sender, EventArgs e)
    {
        txtFrom.Text = txtFrom.Text.Trim();
        txtTo.Text = txtTo.Text.Trim();
        txtServer.Text = txtServer.Text.Trim();

        string result = new Validator().NotEmpty(txtServer.Text, ResHelper.GetString("System_Email.ErrorServer")).NotEmpty(txtFrom.Text, ResHelper.GetString("System_Email.EmptyEmail")).NotEmpty(txtTo.Text, ResHelper.GetString("System_Email.EmptyEmail")).Result;

        if (string.IsNullOrEmpty(result))
        {
            // Validate e-mail addresses
            if (ValidationHelper.IsEmail(txtFrom.Text) && ValidationHelper.AreEmails(txtTo.Text))
            {
                // Send the testing e-mail
                try
                {
                    SendEmail();

                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("System_Email.EmailSent");
                }
                catch (Exception ex)
                {
                    lblError.Visible = true;
                    lblError.Text = ex.Message;
                    lblError.ToolTip = EventLogProvider.GetExceptionLogMessage(ex);
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("System_Email.ErrorEmail");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }


    /// <summary>
    /// Send testing e-mail.
    /// </summary>
    protected void SendEmail()
    {
        // Prepare the e-mail message
        MailMessage msg = new MailMessage();
        msg.From = new MailAddress(txtFrom.Text);

        // Recipients
        string[] to = txtTo.Text.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        foreach (string s in to)
        {
            msg.To.Add(new MailAddress(s));
        }

        // Subject
        msg.Subject = TextHelper.LimitLength(txtSubject.Text.Trim(), 450);
        msg.Body = txtText.Text;
        msg.IsBodyHtml = true;

        // Join file if attached
        if ((FileUploader.PostedFile != null) && (FileUploader.PostedFile.InputStream != null))
        {
            msg.Attachments.Add(new Attachment(FileUploader.PostedFile.InputStream, Path.GetFileName(FileUploader.PostedFile.FileName)));
        }

        // Send mail via e-mail provider
        EmailSender.ProviderObject.SendEmail(null, msg, txtServer.Text.Trim(), txtUserName.Text.Trim(), txtPassword.Text.Trim(), txtEncoding.Text.Trim(), chkSSL.Checked);
    }
}
