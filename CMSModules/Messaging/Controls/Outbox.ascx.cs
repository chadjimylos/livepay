using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Messaging;
using CMS.SiteProvider;
using CMS.UIControls;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Messaging_Controls_Outbox : CMSUserControl
{
    #region "Variables"

    protected string confirmationScript = null;
    private string mZeroRowsText = null;
    private int mPageSize = 10;
    private bool mShowOriginalMessage = true;
    private bool mMarkReadMessage = false;
    protected string currentViewButtonClientId = null;
    private TimeZoneInfo usedTimeZone = null;
    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Current message id
    /// </summary>
    protected int CurrentMessageId
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["CurrentMessageId"], 0);
        }
        set
        {
            ViewState["CurrentMessageId"] = value;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Zero rows text
    /// </summary>
    public string ZeroRowsText
    {
        get
        {
            return mZeroRowsText;
        }
        set
        {
            mZeroRowsText = value;
            EnsureChildControls();
            outboxGrid.ZeroRowsText = value;
        }
    }


    /// <summary>
    /// Size of the page
    /// </summary>
    public int PageSize
    {
        get
        {
            return mPageSize;
        }
        set
        {
            mPageSize = value;
            EnsureChildControls();
            outboxGrid.PageSize = value.ToString();
        }
    }


    /// <summary>
    /// True if original message should be shown
    /// </summary>
    public bool ShowOriginalMessage
    {
        get
        {
            return mShowOriginalMessage;
        }
        set
        {
            mShowOriginalMessage = value;
        }
    }


    /// <summary>
    /// Mark read messages.
    /// </summary>
    public bool MarkReadMessage
    {
        get
        {
            return mMarkReadMessage;
        }
        set
        {
            mMarkReadMessage = value;
        }
    }

    #endregion


    #region "Page events"

    protected override void EnsureChildControls()
    {
        base.EnsureChildControls();
        if (outboxGrid == null)
        {
            pnlOutbox.LoadContainer();
        }
    }


    /// <summary>
    /// Page load.
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event arguments</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SetupControls();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Set info message
        if (ucViewMessage.InformationText != string.Empty)
        {
            lblInfo.Visible = true;
            lblInfo.Text = ucViewMessage.InformationText;
        }

        // Set error message
        if (ucViewMessage.ErrorText != string.Empty)
        {
            lblError.Visible = true;
            lblError.Text = ucViewMessage.ErrorText;
        }
    }

    #endregion


    #region "Public methods"

    /// <summary>
    /// Sets up controls
    /// </summary>
    public void SetupControls()
    {
        if (StopProcessing)
        {
            // Do nothing
            ucSendMessage.StopProcessing = true;
            ucViewMessage.StopProcessing = true;
            outboxGrid.StopProcessing = true;
        }
        else
        {
            if (!RequestHelper.IsPostBack())
            {
                outboxGrid.DelayedReload = false;
            }
            else
            {
                // Find postback invoker
                string invokerName = Page.Request.Params.Get("__EVENTTARGET");
                // If postback was caused by other control
                if (!invokerName.Contains(UniqueID) && !string.IsNullOrEmpty(invokerName))
                {
                    outboxGrid.DelayedReload = false;
                }
            }

            // Show content only for authenticated users
            if (CMSContext.CurrentUser.IsAuthenticated())
            {
                // Show control
                Visible = true;
                // Initialize unigrid
                outboxGrid.OnExternalDataBound += outboxGrid_OnExternalDataBound;
                outboxGrid.GridView.RowDataBound += GridView_RowDataBound;
                outboxGrid.OnAction += outboxGrid_OnAction;
                // Get only named columns
                outboxGrid.Columns = "MessageID,MessageRecipientNickName,MessageSent,MessageSubject,MessageRead,MessageRecipientDeleted,MessageLastModified";
                // Set where condition clause
                outboxGrid.WhereCondition = "MessageSenderUserID=" + CMSContext.CurrentUser.UserID + " AND (MessageSenderDeleted=0 OR MessageSenderDeleted IS NULL)";
                outboxGrid.GridView.DataBound += GridView_DataBound;
                outboxGrid.GridView.PageSize = PageSize;
                outboxGrid.ZeroRowsText = ZeroRowsText;
                outboxGrid.OnBeforeDataReload += outboxGrid_OnBeforeDataReload;
                outboxGrid.OnBeforeSorting += outboxGrid_OnBeforeSorting;
                outboxGrid.OnShowButtonClick += outboxGrid_OnShowButtonClick;
                outboxGrid.OnPageSizeChanged += outboxGrid_OnPageSizeChanged;
                outboxGrid.IsLiveSite = IsLiveSite;
                // Setup inner controls
                ucSendMessage.IsLiveSite = IsLiveSite;
                ucViewMessage.IsLiveSite = IsLiveSite;
                ucViewMessage.MessageMode = MessageModeEnum.Outbox;
                ucViewMessage.MessageId = CurrentMessageId;

                // Hide helper labels
                lblInfo.Visible = false;
                lblError.Visible = false;

                // Create javascript confirmations
                confirmationScript = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("Messsaging.DeletionConfirmation")) + ");";
                string confirmationScriptAll = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("Messsaging.AllDeletionConfirmation")) + ");";

                // Assign javascripts
                btnDelete.OnClientClick = confirmationScript;
                btnDeleteAll.OnClientClick = confirmationScriptAll;

                // Register events
                ucSendMessage.SendButtonClick += ucSendMessage_SendButtonClick;
                btnNewMessage.Click += btnNewMessage_Click;
                btnDeleteAll.Click += btnDeleteAll_Click;
                btnForward.Click += btnForward_Click;
                btnDelete.Click += btnDelete_Click;
                btnBackToList.Click += btnBackToList_Click;
            }
            else
            {
                Visible = false;
            }
        }
    }

    #endregion


    #region "Message actions"

    /// <summary>
    /// New message
    /// </summary>
    private void NewMessage()
    {
        // Initialize new message control
        pnlBackToList.Visible = true;
        pnlList.Visible = false;
        pnlNew.Visible = true;
        pnlActions.Visible = false;

        // Initilaize new and view message
        ucSendMessage.StopProcessing = false;
        ucSendMessage.SendMessageMode = CMSModules_Messaging_Controls_SendMessage.SendMessageEnum.NewMessage;
        ucSendMessage.MessageId = 0;
        ucSendMessage.ReloadData();
    }


    /// <summary>
    /// View message
    /// </summary>
    private void ViewMessage()
    {
        pnlActions.Visible = true;
        pnlList.Visible = false;
        pnlNew.Visible = false;
        pnlView.Visible = true;
        pnlBackToList.Visible = true;

        // Initialize view message control
        ucViewMessage.StopProcessing = false;
        ucViewMessage.MessageId = CurrentMessageId;
        ucViewMessage.ReloadData();
    }


    /// <summary>
    /// Forward message
    /// </summary>
    private void ForwardMesssage()
    {
        pnlBackToList.Visible = true;
        pnlList.Visible = false;
        pnlNew.Visible = true;
        pnlView.Visible = ShowOriginalMessage;
        pnlActions.Visible = false;

        // Initilaize new and view message
        ucSendMessage.StopProcessing = false;
        ucSendMessage.MessageId = CurrentMessageId;
        ucSendMessage.MessageSubject = ResHelper.GetString("Messaging.ForwardSign");
        ucSendMessage.SendMessageMode = CMSModules_Messaging_Controls_SendMessage.SendMessageEnum.ForwardMessage;
        ucSendMessage.ReloadData();
        ucViewMessage.StopProcessing = false;
        ucViewMessage.MessageId = CurrentMessageId;
        ucViewMessage.ReloadData();
    }


    /// <summary>
    /// Delete message
    /// </summary>
    private void DeleteMessage()
    {
        try
        {
            MessageInfoProvider.DeleteSentMessage(CurrentMessageId);
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("Messsaging.MessageDeleted");
            pnlList.Visible = true;
            pnlNew.Visible = false;
            pnlView.Visible = false;
            pnlBackToList.Visible = false;
            outboxGrid.DelayedReload = false;
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    #endregion


    #region "Buttons actions"

    protected void outboxGrid_OnShowButtonClick(object sender, EventArgs e)
    {
        outboxGrid.DelayedReload = false;
    }


    protected void btnBackToList_Click(object sender, EventArgs e)
    {
        pnlList.Visible = true;
        pnlNew.Visible = false;
        pnlView.Visible = false;
        pnlBackToList.Visible = false;
        outboxGrid.ReloadData();
    }


    protected void btnDeleteAll_Click(object sender, EventArgs e)
    {
        try
        {
            MessageInfoProvider.DeleteSentMessages(CMSContext.CurrentUser.UserID);
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("Messsaging.AllMessagesDeleted");
            pnlList.Visible = true;
            pnlNew.Visible = false;
            pnlView.Visible = false;
            outboxGrid.ReloadData();
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        DeleteMessage();
        outboxGrid.ReloadData();
    }


    protected void btnForward_Click(object sender, EventArgs e)
    {
        ForwardMesssage();
    }


    protected void btnNewMessage_Click(object sender, EventArgs e)
    {
        NewMessage();
    }


    protected void ucSendMessage_SendButtonClick(object sender, EventArgs e)
    {
        // If no error, inform user
        if (ucSendMessage.ErrorText == string.Empty)
        {
            lblInfo.Visible = true;
            lblInfo.Text = ucSendMessage.InformationText;
            btnBackToList_Click(sender, e);
        }
        else
        {
            ucViewMessage.StopProcessing = false;
            ucViewMessage.ReloadData();
        }
    }

    #endregion


    #region "Grid methods"

    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataRowView rowView = ((DataRowView)e.Row.DataItem);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Mark read message
            if (MarkReadMessage)
            {
                DateTime messageRead = ValidationHelper.GetDateTime(DataHelper.GetDataRowValue(rowView.Row, "MessageRead"), DateTimeHelper.ZERO_TIME);
                if (messageRead == DateTimeHelper.ZERO_TIME)
                {
                    e.Row.CssClass += " Unread";
                }
            }
        }
    }


    protected void outboxGrid_OnBeforeDataReload()
    {
        // Bind footer
        BindFooter();
    }


    protected void outboxGrid_OnPageSizeChanged()
    {
        outboxGrid.ReloadData();
    }


    protected void outboxGrid_OnBeforeSorting(object sender, EventArgs e)
    {
        outboxGrid.ReloadData();
    }


    protected object outboxGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView drv = null;
        switch (sourceName.ToLower())
        {
            case "messagerecipientnickname":
            case "messagesubject":
                // Avoid XSS
                return HTMLHelper.HTMLEncode(Convert.ToString(parameter));

            case "messagesent":
                if (currentUserInfo == null)
                {
                    currentUserInfo = CMSContext.CurrentUser;
                }
                if (currentSiteInfo == null)
                {
                    currentSiteInfo = CMSContext.CurrentSite;
                }
                DateTime currentDateTime = ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME);
                if (IsLiveSite)
                {
                    return CMSContext.ConvertDateTime(currentDateTime, this);
                }
                else
                {
                    return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(currentDateTime, currentUserInfo, currentSiteInfo, out usedTimeZone);
                }

            case "messageread":
                // Get date read
                drv = GetDataRowView((DataControlFieldCell)sender);
                return GetDateRead(drv["MessageRead"]);

        }
        return parameter;
    }


    protected void GridView_DataBound(object sender, EventArgs e)
    {
        // Setup column styles
        GridView inboxGridView = outboxGrid.GridView;
        inboxGridView.Columns[0].ItemStyle.CssClass = "MessageActions";
        inboxGridView.Columns[1].ItemStyle.CssClass = "MessageUserName";
        inboxGridView.Columns[2].ItemStyle.CssClass = "MessageSubject";
        inboxGridView.Columns[3].ItemStyle.CssClass = "MessageDate";
        inboxGridView.Columns[4].ItemStyle.CssClass = "MessageRead";
        if (!MarkReadMessage)
        {
            outboxGrid.GridView.Columns[4].Visible = false;
        }
    }


    protected void outboxGrid_OnAction(string actionName, object actionArgument)
    {
        CurrentMessageId = ValidationHelper.GetInteger(actionArgument, 0);
        switch (actionName)
        {
            // Delete message
            case "delete":
                DeleteMessage();
                break;

            // View message
            case "view":
                ViewMessage();
                break;
        }
    }


    #endregion


    #region "Protected methods"

    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }


    /// <summary>
    /// Bind the data
    /// </summary>
    private void BindFooter()
    {
        lblFooter.Text = string.Format(ResHelper.GetString("Messaging.NumOfMessages"), MessageInfoProvider.GetSentMessagesCount(CMSContext.CurrentUser.UserID));
    }


    protected string GetDateRead(object messageReadDate)
    {
        DateTime messageRead = ValidationHelper.GetDateTime(messageReadDate, DateTimeHelper.ZERO_TIME);

        if (currentUserInfo == null)
        {
            currentUserInfo = CMSContext.CurrentUser;
        }
        if (currentSiteInfo == null)
        {
            currentSiteInfo = CMSContext.CurrentSite;
        }
        DateTime currentDateTime = ValidationHelper.GetDateTime(messageReadDate, DateTimeHelper.ZERO_TIME);

        if (messageRead != DateTimeHelper.ZERO_TIME)
        {
            if (IsLiveSite)
            {
                return CMSContext.ConvertDateTime(currentDateTime, this).ToString();
            }
            else
            {
                return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(currentDateTime, currentUserInfo, currentSiteInfo, out usedTimeZone);
            }
        }
        else
        {
            return ResHelper.GetString("Messaging.OutboxMessageUnread");
        }
    }

    #endregion
}
