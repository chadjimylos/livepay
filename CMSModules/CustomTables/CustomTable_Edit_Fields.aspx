<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomTable_Edit_Fields.aspx.cs"
    Inherits="CMSModules_CustomTables_CustomTable_Edit_Fields" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Custom talble edit - Fields" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/FieldEditor.ascx" TagName="FieldEditor"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" EnableViewState="false" CssClass="ErrorLabel" />
    <asp:PlaceHolder ID="plcGUID" runat="server" Visible="false" EnableViewState="false">
        <asp:Label ID="lblGUID" runat="server" EnableViewState="false" /><br />
        <asp:LinkButton ID="btnGUID" runat="server" EnableViewState="false" />
    </asp:PlaceHolder>
    <cms:FieldEditor ID="FieldEditor" runat="server" Visible="false" />
</asp:Content>
