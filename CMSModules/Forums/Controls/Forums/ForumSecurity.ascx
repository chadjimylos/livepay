<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForumSecurity.ascx.cs"
    Inherits="CMSModules_Forums_Controls_Forums_ForumSecurity" %>
<%@ Register Src="~/CMSAdminControls/UI/UniControls/UniMatrix.ascx" TagName="UniMatrix" TagPrefix="cms" %>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
<cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="InfoLabel" />
<asp:Table runat="server" ID="tblMatrix" GridLines="horizontal" CssClass="PermissionMatrix"
    CellPadding="3" CellSpacing="0">
</asp:Table>
<br />
<cms:UniMatrix ID="gridMatrix" runat="server" QueryName="Forums.ForumRole.getpermissionMatrix"
    RowItemIDColumn="RoleID" ColumnItemIDColumn="PermissionID" RowItemDisplayNameColumn="RoleDisplayName"
    ColumnItemDisplayNameColumn="PermissionDisplayName" RowTooltipColumn="RowDisplayName"
    ColumnTooltipColumn="PermissionDescription" ItemTooltipColumn="PermissionDescription"
    FirstColumnsWidth="28" FixedWidth="12" UsePercentage="true" />
<br />
<cms:LocalizedCheckBox runat="server" ID="chkChangeName" ResourceString="forum_security.changename"
    AutoPostBack="true" />
