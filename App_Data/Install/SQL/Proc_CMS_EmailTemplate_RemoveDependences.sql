-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_CMS_EmailTemplate_RemoveDependences] 
	@EmailTemplateID int
AS
BEGIN
	SET NOCOUNT ON;
    BEGIN TRANSACTION;
    DELETE FROM [CMS_EmailTemplate] WHERE [EmailTemplateID]=@EmailTemplateID
    DELETE FROM [CMS_MetaFile] WHERE [MetaFileObjectID]=@EmailTemplateID AND [MetaFileObjectType]='EmailTemplate' AND [MetaFileGroupName]='Template'
    COMMIT TRANSACTION;
END
