<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InlineControl_Properties.aspx.cs"
    Inherits="CMSSiteManager_Development_InlineControls_InlineControl_Properties"
    Title="Document Type Edit - Properties" ValidateRequest="false" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/FieldEditor.ascx" TagName="FieldEditor"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:FieldEditor ID="FieldEditor" runat="server" />
</asp:Content>
