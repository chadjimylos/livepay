CREATE TABLE [COM_UserDepartment] (
		[UserID]           int NOT NULL,
		[DepartmentID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_UserDepartment]
	ADD
	CONSTRAINT [PK_COM_UserDepartment]
	PRIMARY KEY
	([UserID], [DepartmentID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_UserDepartment]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Department]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_UserDepartment_DepartmentID_COM_Department]') IS NULL
BEGIN
		ALTER TABLE [COM_UserDepartment]
			ADD CONSTRAINT [FK_COM_UserDepartment_DepartmentID_COM_Department]
			FOREIGN KEY ([DepartmentID]) REFERENCES [COM_Department] ([DepartmentID])
END
IF OBJECT_ID(N'[COM_UserDepartment]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_UserDepartment_UserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [COM_UserDepartment]
			ADD CONSTRAINT [FK_COM_UserDepartment_UserID_CMS_User]
			FOREIGN KEY ([UserID]) REFERENCES [CMS_User] ([UserID])
END
