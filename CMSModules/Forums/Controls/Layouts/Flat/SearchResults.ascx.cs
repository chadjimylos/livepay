using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

using CMS.GlobalHelper;
using CMS.Forums;
using CMS.CMSHelper;

public partial class CMSModules_Forums_Controls_Layouts_Flat_SearchResults : ForumViewer
{

    private string pathSeparator = "&nbsp;&gt;&nbsp;";

    protected void Page_Load(object sender, EventArgs e)
    {
        DataSet results = GetSearchDataSet();

        // Bind the results to the control displaying it
        if (!DataHelper.DataSourceIsEmpty(results))
        {
            this.uniForumPosts.DataSource = results;
            this.uniForumPosts.DataBind();

            #region "Results info"

            string path = String.Empty;
            string[] splited = null;

            if (this.SearchInThreads != String.Empty)
            {
                splited = this.SearchInThreads.Split(new string[1] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                if ((splited != null) && (splited.Length == 1))
                {
                    ForumPostInfo fpi = ForumPostInfoProvider.GetForumPostInfo(ValidationHelper.GetInteger(splited[0], 0));
                    if (fpi != null)
                    {
                        path = HTMLHelper.HTMLEncode(fpi.PostSubject);

                        ForumInfo fi = ForumInfoProvider.GetForumInfo(fpi.PostForumID);
                        if (fi != null)
                        {
                            path = HTMLHelper.HTMLEncode(fi.ForumDisplayName) + pathSeparator + path;

                            ForumGroupInfo fgi = ForumGroupInfoProvider.GetForumGroupInfo(fi.ForumGroupID);
                            if (fgi != null)
                            {
                                path = HTMLHelper.HTMLEncode(fgi.GroupDisplayName) + pathSeparator + path;
                            }
                        }

                        path = ResHelper.GetString("ForumSearch.InThread") + path;
                    }
                }
            }
            else if (this.SearchInForums != String.Empty)
            {
                splited = this.SearchInForums.Split(new string[1] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                if ((splited != null) && (splited.Length == 1))
                {
                    ForumInfo fi = ForumInfoProvider.GetForumInfo(ValidationHelper.GetInteger(splited[0], 0));
                    if (fi != null)
                    {
                        path = HTMLHelper.HTMLEncode(fi.ForumDisplayName);

                        ForumGroupInfo fgi = ForumGroupInfoProvider.GetForumGroupInfo(fi.ForumGroupID);
                        if (fgi != null)
                        {
                            path = HTMLHelper.HTMLEncode(fgi.GroupDisplayName) + pathSeparator + path;
                        }

                        path = ResHelper.GetString("ForumSearch.InForum") + path;
                    }
                }
            }
            else if (this.SearchInGroups != null)
            {
                splited = this.SearchInGroups.Split(new string[1] { ";" }, StringSplitOptions.RemoveEmptyEntries);

                if ((splited != null) && (splited.Length == 1))
                {
                    ForumGroupInfo fgi = ForumGroupInfoProvider.GetForumGroupInfo(splited[0], CMSContext.CurrentSiteID, this.CommunityGroupID);
                    if (fgi != null)
                    {
                        path = HTMLHelper.HTMLEncode(fgi.GroupDisplayName);

                        path = ResHelper.GetString("ForumSearch.InForumGroup") + path;
                    }
                }
            }


            ltlResultsInfo.Text = "<div class=\"ForumSearchResultsInfo\">" + String.Format(ResHelper.GetString("ForumSearch.SearchResultsInfo"), HTMLHelper.HTMLEncode(QueryHelper.GetString("searchtext", "")), path, results.Tables[0].Rows.Count) + "</div>";

            #endregion
        }
        else
        {
            lblNoResults.Visible = true;
            plcResults.Visible = false;
            lblNoResults.Text = this.SearchNoResults;
        }
    }


    /// <summary>
    /// Encode text
    /// </summary>
    /// <param name="value">Input value</param>
    public string Encode(object value)
    {
        return HTMLHelper.HTMLEncode(ValidationHelper.GetString(value, ""));
    }
}

