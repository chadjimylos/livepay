<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StoreSettings_ChangeCurrency.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_StoreSettings_StoreSettings_ChangeCurrency"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master" %>

<%@ Register Src="~/CMSModules/ECommerce/FormControls/CurrencySelector.ascx" TagName="CurrencySelector"
    TagPrefix="cms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <div class="PageContent">
        <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <cms:CurrencySelector runat="server" ID="currencyElem" AddSiteDefaultCurrency="false"
            ExcludeSiteDefaultCurrency="true" IsLiveSite="false" DisplayItems="Enabled" />
        <br />
        <br />
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblExchangeRate" runat="server" EnableViewState="false" />&nbsp;
                </td>
                <td>
                    <asp:TextBox ID="txtEchangeRate" runat="server" MaxLength="10" EnableViewState="false"
                        CssClass="EditableTextTextBox" />
                    <asp:Image ID="imgHelp" runat="server" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblRound" runat="server" EnableViewState="false" />&nbsp;
                </td>
                <td>
                    <asp:TextBox ID="txtRound" runat="server" EnableViewState="false" CssClass="EditableTextTextBox"
                        MaxLength="1" Text="2" />
                    <asp:Image ID="imgRoundHelp" runat="server" EnableViewState="false" />
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkExchangeRates" runat="server" CssClass="ContentCheckBox" Checked="true"
                        EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkProductPrices" runat="server" CssClass="ContentCheckBox" Checked="true"
                        EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkFlatTaxes" runat="server" CssClass="ContentCheckBox" Checked="true"
                        EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkFlatDiscountsCoupons" runat="server" CssClass="ContentCheckBox"
                        Checked="true" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkFlatVolumeDiscounts" runat="server" CssClass="ContentCheckBox"
                        Checked="true" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkCredit" runat="server" CssClass="ContentCheckBox" Checked="true"
                        EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkShipping" runat="server" CssClass="ContentCheckBox" Checked="true"
                        EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkDocuments" runat="server" CssClass="ContentCheckBox" Checked="true"
                        EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:CheckBox ID="chkFreeShipping" runat="server" CssClass="ContentCheckBox" Checked="true"
                        EnableViewState="false" />
                </td>
            </tr>
        </table>
    </div>
    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOk_Click"
            EnableViewState="false" /><cms:CMSButton ID="btnCancel" runat="server" CssClass="SubmitButton"
                OnClientClick="window.close(); return false;" EnableViewState="false" />
    </div>
</asp:Content>
