CREATE TABLE [CMS_Country] (
		[CountryID]               int NOT NULL IDENTITY(1, 1),
		[CountryDisplayName]      nvarchar(200) NOT NULL,
		[CountryName]             nvarchar(200) NOT NULL,
		[CountryGUID]             uniqueidentifier NOT NULL,
		[CountryLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Country]
	ADD
	CONSTRAINT [PK_CMS_Country]
	PRIMARY KEY
	NONCLUSTERED
	([CountryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_Country_CountryDisplayName]
	ON [CMS_Country] ([CountryDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
