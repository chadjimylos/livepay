using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Products_Product_Edit_Options : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get product ID form url
        ucOptions.ProductID = QueryHelper.GetInteger("productid", 0);
    }
}
