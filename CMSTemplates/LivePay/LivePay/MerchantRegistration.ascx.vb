﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports LivePay_ESBBridge
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.FormEngine
Imports CMS.SettingsProvider
Imports CMS.DataEngine

Partial Class CMSTemplates_LivePay_MerchantRegistration
    Inherits CMSUserControl


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        ElseIf Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
        SetErrorMessages()
    End Sub

    Public Overloads Sub ReloadData()
        SetErrorMessages()
    End Sub

    Private Sub SetErrorMessages()
        Dim dt As DataTable = DBConnection.GetErrorMessages(3)
        Dim JsStringMessages As String = String.Empty
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim ErrorMessageStr As String = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("ErrorMessageGR").ToString, "|en-us=", dt.Rows(i)("ErrorMessageEN").ToString, "$}"))
            ErrorMessageStr = Replace(ErrorMessageStr, "'", "\'")
            JsStringMessages = String.Concat(JsStringMessages, " ", dt.Rows(i)("CodeName").ToString, " = '", ErrorMessageStr, "'; ")
        Next
        JsScript(JsStringMessages)
    End Sub
    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOK.Click
        Dim bizFormName As String = "UserDetails"
        Dim siteName As String = CMSContext.CurrentSiteName
        Dim bfi As BizFormInfo = BizFormInfoProvider.GetBizFormInfo(bizFormName, siteName)
        If Not IsNothing(bfi) Then
            Dim dci As DataClassInfo = DataClassInfoProvider.GetDataClass(bfi.FormClassID)
            If Not IsNothing(dci) Then
                Dim genConn As GeneralConnection = ConnectionHelper.GetConnection()
                Dim formRecord As DataClass = New DataClass(dci.ClassName, genConn)
                formRecord.SetValue("FormInserted", DateTime.Now)
                formRecord.SetValue("FormUpdated", DateTime.Now)
                formRecord.SetValue("CompanyName", StripHTMLFunctions.StripTags(CompanyName.Text))
                formRecord.SetValue("CommunicationsOfficer", StripHTMLFunctions.StripTags(CommunicationsOfficer.Text))
                formRecord.SetValue("ContactPhone", StripHTMLFunctions.StripTags(ContactPhone.Text))
                formRecord.SetValue("Email", StripHTMLFunctions.StripTags(txtEmail.Text))
                formRecord.SetValue("AFM", StripHTMLFunctions.StripTags(txtAFM.Text))
                formRecord.SetValue("Comments", StripHTMLFunctions.StripTags(Comments.Text))
                formRecord.Insert()
                Response.Redirect("~/Merchants_Thanks.aspx")
            End If
        End If

    End Sub
End Class
