CREATE TABLE [CMS_PageTemplate] (
		[PageTemplateID]                              int NOT NULL IDENTITY(1, 1),
		[PageTemplateDisplayName]                     nvarchar(200) NOT NULL,
		[PageTemplateCodeName]                        nvarchar(100) NULL,
		[PageTemplateDescription]                     nvarchar(max) NULL,
		[PageTemplateFile]                            nvarchar(400) NOT NULL,
		[PageTemplateIsPortal]                        bit NULL,
		[PageTemplateCategoryID]                      int NULL,
		[PageTemplateLayoutID]                        int NULL,
		[PageTemplateWebParts]                        nvarchar(max) NULL,
		[PageTemplateIsReusable]                      bit NULL,
		[PageTemplateShowAsMasterTemplate]            bit NULL,
		[PageTemplateInheritPageLevels]               nvarchar(200) NULL,
		[PageTemplateLayout]                          nvarchar(max) NULL,
		[PageTemplateLayoutCheckedOutFileName]        nvarchar(450) NULL,
		[PageTemplateLayoutCheckedOutByUserID]        int NULL,
		[PageTemplateLayoutCheckedOutMachineName]     nvarchar(50) NULL,
		[PageTemplateVersionGUID]                     nvarchar(50) NULL,
		[PageTemplateHeader]                          nvarchar(max) NULL,
		[PageTemplateGUID]                            uniqueidentifier NOT NULL,
		[PageTemplateLastModified]                    datetime NOT NULL,
		[PageTemplateSiteID]                          int NULL,
		[PageTemplateForAllPages]                     bit NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_PageTemplate]
	ADD
	CONSTRAINT [PK_CMS_PageTemplate]
	PRIMARY KEY
	NONCLUSTERED
	([PageTemplateID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_PageTemplate]
	ADD
	CONSTRAINT [DEFAULT_CMS_PageTemplate_PageTemplateForAllPages]
	DEFAULT ((1)) FOR [PageTemplateForAllPages]
IF OBJECT_ID(N'[CMS_PageTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_PageTemplateCategory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplate_PageTemplateCategoryID_CMS_PageTemplateCategory]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplate]
			ADD CONSTRAINT [FK_CMS_PageTemplate_PageTemplateCategoryID_CMS_PageTemplateCategory]
			FOREIGN KEY ([PageTemplateCategoryID]) REFERENCES [CMS_PageTemplateCategory] ([CategoryID])
END
IF OBJECT_ID(N'[CMS_PageTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplate_PageTemplateLayoutCheckedOutByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplate]
			ADD CONSTRAINT [FK_CMS_PageTemplate_PageTemplateLayoutCheckedOutByUserID_CMS_User]
			FOREIGN KEY ([PageTemplateLayoutCheckedOutByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[CMS_PageTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Layout]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplate_PageTemplateLayoutID_CMS_Layout]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplate]
			ADD CONSTRAINT [FK_CMS_PageTemplate_PageTemplateLayoutID_CMS_Layout]
			FOREIGN KEY ([PageTemplateLayoutID]) REFERENCES [CMS_Layout] ([LayoutID])
END
IF OBJECT_ID(N'[CMS_PageTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplate_PageTemplateSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplate]
			ADD CONSTRAINT [FK_CMS_PageTemplate_PageTemplateSiteID_CMS_Site]
			FOREIGN KEY ([PageTemplateSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_CMS_PageTemplate_PageTemplateCategoryID]
	ON [CMS_PageTemplate] ([PageTemplateCategoryID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_PageTemplate_PageTemplateCodeName_PageTemplateDisplayName]
	ON [CMS_PageTemplate] ([PageTemplateCodeName], [PageTemplateDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_PageTemplate_PageTemplateIsReusable_PageTemplateForAllPages_PageTemplateShowAsMasterTemplate]
	ON [CMS_PageTemplate] ([PageTemplateIsReusable], [PageTemplateForAllPages], [PageTemplateShowAsMasterTemplate])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_PageTemplate_PageTemplateLayoutCheckedOutByUserID]
	ON [CMS_PageTemplate] ([PageTemplateLayoutCheckedOutByUserID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_PageTemplate_PageTemplateLayoutID]
	ON [CMS_PageTemplate] ([PageTemplateLayoutID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_PageTemplate_PageTemplateSiteID_PageTemplateCodeName_PageTemplateGUID]
	ON [CMS_PageTemplate] ([PageTemplateSiteID], [PageTemplateCodeName], [PageTemplateGUID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
