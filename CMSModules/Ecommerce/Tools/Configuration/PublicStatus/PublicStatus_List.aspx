<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PublicStatus_List.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_PublicStatus_PublicStatus_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Public status - List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="gridElem" GridName="PublicStatus_List.xml" OrderBy="PublicStatusDisplayName"
        IsLiveSite="false" Columns="PublicStatusID,PublicStatusDisplayName,PublicStatusEnabled" />
</asp:Content>
