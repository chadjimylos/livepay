<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportCategory_General.aspx.cs"
    Inherits="CMSModules_Reporting_Tools_ReportCategory_General" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top;">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCategoryDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCategoryDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtCategoryDisplayName" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCategoryCodeName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCategoryCodeName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtCategoryCodeName" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
