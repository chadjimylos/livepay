using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.Messaging;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Messaging_Controls_Inbox : CMSUserControl
{
    #region "Variables"

    protected string confirmationScript = null;
    private string mZeroRowsText = null;
    private int mPageSize = 10;
    private bool mPasteOriginalMessage = true;
    private bool mShowOriginalMessage = true;
    protected string currentViewButtonClientId = null;
    private TimeZoneInfo usedTimeZone = null;
    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Current message id
    /// </summary>
    protected int CurrentMessageId
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["CurrentMessageId"], 0);
        }
        set
        {
            ViewState["CurrentMessageId"] = value;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Zero rows text
    /// </summary>
    public string ZeroRowsText
    {
        get
        {
            return mZeroRowsText;
        }
        set
        {
            mZeroRowsText = value;
            EnsureChildControls();
            inboxGrid.ZeroRowsText = value;
        }
    }


    /// <summary>
    /// Size of the page
    /// </summary>
    public int PageSize
    {
        get
        {
            return mPageSize;
        }
        set
        {
            mPageSize = value;
            EnsureChildControls();
            inboxGrid.PageSize = value.ToString();
        }
    }


    /// <summary>
    /// True if original message should be pasted to the current
    /// </summary>
    public bool PasteOriginalMessage
    {
        get
        {
            return mPasteOriginalMessage;
        }
        set
        {
            mPasteOriginalMessage = value;
            EnsureChildControls();
            ucSendMessage.PasteOriginalMessage = value;
        }
    }


    /// <summary>
    /// True if original message should be shown
    /// </summary>
    public bool ShowOriginalMessage
    {
        get
        {
            return mShowOriginalMessage;
        }
        set
        {
            mShowOriginalMessage = value;
        }
    }

    #endregion


    #region "Page events"

    protected override void EnsureChildControls()
    {
        base.EnsureChildControls();
        if (inboxGrid == null)
        {
            pnlInbox.LoadContainer();
        }
    }


    /// <summary>
    /// Page load.
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event arguments</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        SetupControls();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Set info message
        if (ucViewMessage.InformationText != string.Empty)
        {
            lblInfo.Visible = true;
            lblInfo.Text = ucViewMessage.InformationText;
        }

        // Set error message
        if (ucViewMessage.ErrorText != string.Empty)
        {
            lblError.Visible = true;
            lblError.Text = ucViewMessage.ErrorText;
        }
    }

    #endregion


    #region "Public methods"

    /// <summary>
    /// Sets up controls
    /// </summary>
    public void SetupControls()
    {
        if (StopProcessing)
        {
            // Do nothing
            ucSendMessage.StopProcessing = true;
            ucViewMessage.StopProcessing = true;
            inboxGrid.StopProcessing = true;
        }
        else
        {
            if (!RequestHelper.IsPostBack())
            {
                inboxGrid.DelayedReload = false;
            }
            else
            {
                // Find postback invoker
                string invokerName = Page.Request.Params.Get("__EVENTTARGET");
                // If postback was caused by other control
                if (!invokerName.Contains(UniqueID) && !string.IsNullOrEmpty(invokerName))
                {
                    inboxGrid.DelayedReload = false;
                }
            }

            // Show content only for authenticated users
            if (CMSContext.CurrentUser.IsAuthenticated())
            {
                // Show control
                Visible = true;
                // Initialize unigrid
                inboxGrid.OnExternalDataBound += inboxGrid_OnExternalDataBound;
                inboxGrid.GridView.RowDataBound += GridView_RowDataBound;
                inboxGrid.OnAction += inboxGrid_OnAction;
                // Get only named columns
                inboxGrid.Columns = "MessageID,MessageSenderNickName,MessageSubject,MessageRead,MessageSenderDeleted,MessageLastModified,MessageSent";
                // Set where condition clause
                inboxGrid.WhereCondition = "MessageRecipientUserID=" + CMSContext.CurrentUser.UserID + " AND (MessageRecipientDeleted=0 OR MessageRecipientDeleted IS NULL)";
                inboxGrid.GridView.DataBound += GridView_DataBound;
                inboxGrid.GridView.PageSize = PageSize;
                inboxGrid.ZeroRowsText = ZeroRowsText;
                inboxGrid.OnBeforeDataReload += inboxGrid_OnBeforeDataReload;
                inboxGrid.OnBeforeSorting += inboxGrid_OnBeforeSorting;
                inboxGrid.OnShowButtonClick += inboxGrid_OnShowButtonClick;
                inboxGrid.OnPageSizeChanged += inboxGrid_OnPageSizeChanged;
                inboxGrid.IsLiveSite = IsLiveSite;
                // Setup inner controls
                ucSendMessage.IsLiveSite = IsLiveSite;
                ucViewMessage.IsLiveSite = IsLiveSite;
                ucViewMessage.MessageMode = MessageModeEnum.Inbox;
                ucViewMessage.MessageId = CurrentMessageId;

                // Hide helper labels
                lblInfo.Visible = false;
                lblError.Visible = false;

                // Create javascript confirmations
                confirmationScript = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("Messsaging.DeletionConfirmation")) + ");";
                string confirmationScriptAll = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("Messsaging.AllDeletionConfirmation")) + ");";

                // Assign javascripts
                btnDelete.OnClientClick = confirmationScript;
                btnDeleteAll.OnClientClick = confirmationScriptAll;

                // Register events
                ucSendMessage.SendButtonClick += ucSendMessage_SendButtonClick;
                btnNewMessage.Click += btnNewMessage_Click;
                btnDeleteAll.Click += btnDeleteAll_Click;
                btnReply.Click += btnReply_Click;
                btnForward.Click += btnForward_Click;
                btnDelete.Click += btnDelete_Click;
                btnBackToList.Click += btnBackToList_Click;
            }
            else
            {
                Visible = false;
            }
        }
    }

    #endregion


    #region "Message actions"

    /// <summary>
    /// New message
    /// </summary>
    private void NewMessage()
    {
        // Initialize new message control
        pnlBackToList.Visible = true;
        pnlList.Visible = false;
        pnlNew.Visible = true;
        pnlActions.Visible = false;

        // Initilaize new and view message
        ucSendMessage.StopProcessing = false;
        ucSendMessage.SendMessageMode = CMSModules_Messaging_Controls_SendMessage.SendMessageEnum.NewMessage;
        ucSendMessage.MessageId = 0;
        ucSendMessage.ReloadData();
    }


    /// <summary>
    /// Reply message
    /// </summary>
    private void ReplyMessage()
    {
        pnlBackToList.Visible = true;
        pnlList.Visible = false;
        pnlNew.Visible = true;
        pnlView.Visible = ShowOriginalMessage;
        pnlActions.Visible = false;

        // Initilaize new and view message
        ucSendMessage.StopProcessing = false;
        ucSendMessage.MessageId = CurrentMessageId;
        ucSendMessage.MessageSubject = ResHelper.GetString("Messaging.ReSign");
        ucSendMessage.SendMessageMode = CMSModules_Messaging_Controls_SendMessage.SendMessageEnum.ReplyMessage;
        ucSendMessage.ReloadData();
        ucViewMessage.StopProcessing = false;
        ucViewMessage.MessageId = CurrentMessageId;
        ucViewMessage.ReloadData();
    }


    /// <summary>
    /// Forward message
    /// </summary>
    private void ForwardMesssage()
    {
        pnlBackToList.Visible = true;
        pnlList.Visible = false;
        pnlNew.Visible = true;
        pnlView.Visible = ShowOriginalMessage;
        pnlActions.Visible = false;

        // Initilaize new and view message
        ucSendMessage.StopProcessing = false;
        ucSendMessage.MessageId = CurrentMessageId;
        ucSendMessage.MessageSubject = ResHelper.GetString("Messaging.ForwardSign");
        ucSendMessage.SendMessageMode = CMSModules_Messaging_Controls_SendMessage.SendMessageEnum.ForwardMessage;
        ucSendMessage.ReloadData();
        ucViewMessage.StopProcessing = false;
        ucViewMessage.MessageId = CurrentMessageId;
        ucViewMessage.ReloadData();
    }


    /// <summary>
    /// View message
    /// </summary>
    private void ViewMessage()
    {
        pnlActions.Visible = true;
        pnlList.Visible = false;
        pnlNew.Visible = false;
        pnlView.Visible = true;
        pnlBackToList.Visible = true;

        // Initialize view message control
        ucViewMessage.StopProcessing = false;
        ucViewMessage.MessageId = CurrentMessageId;
        ucViewMessage.ReloadData();
    }


    /// <summary>
    /// Set message to read message
    /// </summary>
    private void ReadMessage()
    {
        MessageInfo message = MessageInfoProvider.GetMessageInfo(CurrentMessageId);
        if ((message != null) && (message.MessageRead == DateTimeHelper.ZERO_TIME))
        {
            message.MessageRead = DateTime.Now;
            MessageInfoProvider.SetMessageInfo(message);
        }
    }


    /// <summary>
    /// Delete message
    /// </summary>
    private void DeleteMessage()
    {
        try
        {
            inboxGrid.DelayedReload = false;
            MessageInfoProvider.DeleteRecievedMessage(CurrentMessageId);
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("Messsaging.MessageDeleted");
            pnlList.Visible = true;
            pnlNew.Visible = false;
            pnlView.Visible = false;
            pnlBackToList.Visible = false;
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }

    #endregion


    #region "Buttons actions"

    protected void inboxGrid_OnShowButtonClick(object sender, EventArgs e)
    {
        inboxGrid.DelayedReload = false;
    }


    protected void btnBackToList_Click(object sender, EventArgs e)
    {
        pnlList.Visible = true;
        pnlNew.Visible = false;
        pnlView.Visible = false;
        pnlBackToList.Visible = false;
        inboxGrid.ReloadData();
    }


    protected void btnDeleteAll_Click(object sender, EventArgs e)
    {
        try
        {
            MessageInfoProvider.DeleteRecievedMessages(CMSContext.CurrentUser.UserID);
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("Messsaging.AllMessagesDeleted");
            pnlList.Visible = true;
            pnlNew.Visible = false;
            pnlView.Visible = false;
            inboxGrid.ReloadData();
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        DeleteMessage();
        inboxGrid.ReloadData();
    }


    protected void btnForward_Click(object sender, EventArgs e)
    {
        ForwardMesssage();
    }


    protected void btnReply_Click(object sender, EventArgs e)
    {
        ReplyMessage();
    }


    protected void btnNewMessage_Click(object sender, EventArgs e)
    {
        NewMessage();
    }


    protected void ucSendMessage_SendButtonClick(object sender, EventArgs e)
    {
        // If no error, inform user
        if (ucSendMessage.ErrorText == string.Empty)
        {
            lblInfo.Visible = true;
            lblInfo.Text = ucSendMessage.InformationText;
            btnBackToList_Click(sender, e);
        }
        else
        {
            ucViewMessage.StopProcessing = false;
            ucViewMessage.ReloadData();
        }
    }

    #endregion


    #region "Grid methods"

    protected static void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataRowView rowView = ((DataRowView)e.Row.DataItem);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Mark unread message
            DateTime messageRead = ValidationHelper.GetDateTime(DataHelper.GetDataRowValue(rowView.Row, "MessageRead"), DateTimeHelper.ZERO_TIME);
            if (messageRead == DateTimeHelper.ZERO_TIME)
            {
                e.Row.CssClass += " Unread";
            }
        }
    }


    protected void inboxGrid_OnBeforeSorting(object sender, EventArgs e)
    {
        inboxGrid.ReloadData();
    }

    protected void inboxGrid_OnPageSizeChanged()
    {
        inboxGrid.ReloadData();
    }


    protected void inboxGrid_OnBeforeDataReload()
    {
        // Bind grid footer
        BindFooter();
    }


    protected void GridView_DataBound(object sender, EventArgs e)
    {
        // Setup column styles
        GridView inboxGridView = inboxGrid.GridView;
        inboxGridView.Columns[0].ItemStyle.CssClass = "MessageActions";
        inboxGridView.Columns[1].ItemStyle.CssClass = "MessageUserName";
        inboxGridView.Columns[2].ItemStyle.CssClass = "MessageSubject";
        inboxGridView.Columns[3].ItemStyle.CssClass = "MessageDate";
    }


    protected object inboxGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "messagesendernickname":
            case "messagesubject":
                // Avoid XSS
                return HTMLHelper.HTMLEncode(Convert.ToString(parameter));

            case "messagesent":
                if (currentUserInfo == null)
                {
                    currentUserInfo = CMSContext.CurrentUser;
                }
                if (currentSiteInfo == null)
                {
                    currentSiteInfo = CMSContext.CurrentSite;
                }
                DateTime currentDateTime = ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME);
                if (IsLiveSite)
                {
                    return CMSContext.ConvertDateTime(currentDateTime, this);
                }
                else
                {
                    return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(currentDateTime, currentUserInfo, currentSiteInfo, out usedTimeZone);
                }
        }
        return parameter;
    }


    protected void inboxGrid_OnAction(string actionName, object actionArgument)
    {
        CurrentMessageId = ValidationHelper.GetInteger(actionArgument, 0);
        switch (actionName)
        {
            // Delete message
            case "delete":
                DeleteMessage();
                break;

            // Reply message
            case "reply":
                ReadMessage();
                ReplyMessage();
                break;

            // View message
            case "view":
                ReadMessage();
                ViewMessage();
                break;
        }
    }

    #endregion


    #region "Protected methods"

    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }


    /// <summary>
    /// Bind the data
    /// </summary>
    protected void BindFooter()
    {
        int userId = CMSContext.CurrentUser.UserID;
        lblFooter.Text = string.Format(ResHelper.GetString("Messaging.UnreadOfAll"), MessageInfoProvider.GetUnreadMessagesCount(userId), MessageInfoProvider.GetMessagesCount(userId));
    }

    #endregion
}
