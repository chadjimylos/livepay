<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSearchHistPay.ascx.vb" Inherits="CMSTemplates_LivePay_UCSearchHistPay" %>
<%@ Register Src="~/CMSTemplates/livepay/PopUp/UCReceipt.ascx" TagName="Receipt" TagPrefix="uc"  %>
<%@ Register Src="~/CMSTemplates/livepay/PopUp/UCTransDetails.ascx" TagName="TransDetails" TagPrefix="uc"  %>
<script src="../../CMSScripts/LivePay/LivePay.js" type="text/javascript"></script>
<script src="../../CMSScripts/LivePay/RegisteredUsers.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript" >

    function Hist_RebindRpt(CachValue) {
        setTimeout("Hist_ExecSearch('" + CachValue + "')", 400)
    }

    function Hist_ExecSearch(CachValue) {
        var RealValue = document.getElementById('<%=txtMerchant.ClientID %>').value
        if (RealValue == CachValue) {
            var text = document.getElementById('<%=txtMerchant.clientID %>');
            if (text.value != null && text.value.length > 0) {
                GetMerchantHistData(RealValue)
            } else {
                document.getElementById('<%=HiddentxtMerchantID.clientID %>').value = ''
                Hist_ShowHideQuickRes(false, 0)
            }
        }
    }

    function GetMerchantHistData(val) {
        var x;
        if (window.ActiveXObject) {
            x = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else {
            x = new XMLHttpRequest();
        }
        x.open("GET", "/CMSTemplates/LivePay/xmlHttpMerchants.aspx?MerchantKeyWord=" + val, true);
        x.onreadystatechange = function () {
            if (x.readyState == 4) {
                var Results = x.responseText;
                if (Results.length > 0) {
                    document.getElementById('Hist_QuickSrcResAll').innerHTML = Results;
                    Hist_ShowHideQuickRes(true, 0);
                } else {
                    Hist_ShowHideQuickRes(false, 0);
                }

            }
        }
        x.send(null);
    }

    function Hist_ShowHideQuickRes(IsVisible, RowCount) {
        if (IsVisible) {
            document.getElementById('Hist_QuickResDiv').style.display = '';
        }
        else {
            document.getElementById('Hist_QuickResDiv').style.display = 'none';
        }
    }

    function SetSuggestValue(txt, Val) {
        document.getElementById('<%=txtMerchant.clientID %>').value = txt;
        document.getElementById('<%=HiddentxtMerchantID.clientID %>').value = Val;
        document.getElementById('Hist_QuickResDiv').style.display = 'none';
    }

    function ShowTransDetails() {
        $.blockUI({ css: { top: '20%', border: '0px', backgroundColor: 'transparent', cursor: 'default' }, message: $('#ReceiptPopUp') });
    }


    function ExportFile(url) {
        document.getElementById('<%=exportFrame.ClientID %>').src = '/CMSTemplates/livepay/' + url;
        return false
    }
</script>



<style type="text/css">
div.growlUI { background: url(check48.png) no-repeat 10px 10px;width:200px }
div.growlUI h1, div.growlUI h2 {font-size:12px;color: white;text-align: left;width:200px}
.test
{
    display:none;
}
</style>
<asp:UpdatePanel ID="uptPanel" runat="server" UpdateMode="Always" >
<ContentTemplate >
<div id="ReceiptPopUp" style="display:none">
        <uc:Receipt ID="Receipt" runat="server" />
</div>
<div id="TransDetailsPopUp" style="display:none">
        <uc:TransDetails ID="TransDetails" runat="server" />
</div>
<asp:Label ID="tt" runat="server"></asp:Label>

 <div class="SrHistPayBG">
	<div class="SrHistPayTitle"><%= ResHelper.LocalizeString("{$=�������� ��������|en-us=�������� ��������$}")%></div>
	<div class="SrHistPaySrcTitle"><%= ResHelper.LocalizeString("{$=��������� ��������|en-us=��������� ��������$}")%></div>
	<div class="SrHistPaySrcDescr"><%=ResHelper.LocalizeString("{$=��� ������ ���� �������� �� ����� ��� ��������� �������� ��� ����� ���������������. ��������������� �� �������� �������� ���������� ��� �� ����������� �� ������������|en-us=��� ������ ���� �������� �� ����� ��� ��������� �������� ��� ����� ���������������. ��������������� �� �������� �������� ���������� ��� �� ����������� �� ������������$}") %></div>
    <div style="padding-top:8px;padding-top:expression(5)"><img src="/app_themes/LivePay/SrcBorderForm.png"</div>
	<div>
    <div id="lblError" runat="server" visible="false" style="color:Red;font-weight:bold;padding-left:20px"></div>
	   <div class="SrHistPaySrcDtmTitle"><%=ResHelper.LocalizeString("{$=��������|en-us=��������$}") %>:</div>
	   <div class="SrHistPaySrcDtmRBL">
	    <asp:RadioButtonList ID="rbtDate" runat="server" RepeatDirection="Horizontal" cssClass="SrHistPayrbl" >
	        <asp:ListItem Text="������" Value="1" Selected="True"></asp:ListItem>
            <asp:ListItem Text="1 ��������" Value="2" ></asp:ListItem>
	        <asp:ListItem Text="1 �����" Value="3" ></asp:ListItem>
	        <asp:ListItem Text="2 �����" Value="4" ></asp:ListItem>
	        <asp:ListItem Text="6 �����" Value="5" ></asp:ListItem>
	    </asp:RadioButtonList>
	   </div>
	   <div class="Clear"></div>
	   <div class="PT5">
		<div class="SrHistPaySrcDtmFrom"><%= ResHelper.LocalizeString("{$=���|en-us=���$}")%>:</div>
		<div class="SrHistPaySrcFrom"><asp:textbox CssClass="SrcHisPayTxt" id="txtFrom" runat="server" autocomplete="off" style="width:142px" /></div>
		<div class="SrHistPaySrcDtmTo"><%=ResHelper.LocalizeString("{$=���|en-us=���$}") %>:</div>
		<div class="SrHistPaySrcTo"><asp:textbox CssClass="SrcHisPayTxt" id="txtTo" runat="server" autocomplete="off" style="border:1px solid #3b6db4;width:142px" /></div>
		<div class="Clear"></div>
	   </div>
       <div><img src="/app_themes/LivePay/SrcBorderForm.png"</div>
       <div id="DivFullSrc" class="SrHistPayBigForm" style="display:none">
            <div class="PB10PF10">
                <div class="SrHistPayTableTitle"><%=ResHelper.LocalizeString("{$=���� ��������|en-us=���� ��������$}") %>:</div>
                <div class="SrHistPayTableFrom"><%=ResHelper.LocalizeString("{$=���|en-us=���$}") %>:</div>
                <div class="SrHistPayTableTxt"><asp:textbox onkeyup="FixMoney(this,false,false)" onblur="FixMoney(this,true,false)" CssClass="SrcHisPayTxt" id="txtFromPrice" runat="server" style="border:1px solid #3b6db4;width:142px" /></div>
                <div class="SrHistPayTableTo"><%=ResHelper.LocalizeString("{$=���|en-us=���$}") %>:</div>
                <div class="SrHistPayTableTxt"><asp:textbox onkeyup="FixMoney(this,false,false)" onblur="FixMoney(this,true,false)" CssClass="SrcHisPayTxt" id="txtToPrice" runat="server" style="border:1px solid #3b6db4;width:142px" /></div>
                <div class="Clear"></div>
            </div>
            <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitle"><%=ResHelper.LocalizeString("{$=������� ����|en-us=������� ����$}") %>:</div>
                <div style="float:left"><asp:textbox  CssClass="SrcHisPayTxt" id="txtMerchant" autocomplete="off" onkeyup="Hist_RebindRpt(this.value);" runat="server" style="border:1px solid #3b6db4;width:178px" /></div>
                <div class="Clear"></div><asp:Button style="display:none" Text="hidbtn" ID="Hist_hidBtn" runat="server" /><asp:TextBox ID="HiddentxtMerchantID" runat="server" style="display:none"></asp:TextBox>
                <div class="HistQuickSrcResTopMainNewPay" id="Hist_QuickResDiv" style="display:none">
                    <div class="QuickSrcResMain">
                        <div class="QuickSrcResAll" id="Hist_QuickSrcResAll">
                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="PB10" ><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitle"><%= ResHelper.LocalizeString("{$=�����|en-us=�����$}")%>:</div>
                <div style="float:left"><asp:DropDownList CssClass="SrcHisPayDDL"  ID="drpSavedCards" runat="server" ></asp:DropDownList></div>
                <div class="Clear"></div>
            </div>
            <div class="PB10" ><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitle"><%= ResHelper.LocalizeString("{$=����������|en-us=����������$}")%>:</div>
                <div style="float:left"><asp:DropDownList  CssClass="SrcHisPayDDL" ID="drpTxnResult" runat="server" ></asp:DropDownList></div>
                <div class="Clear"></div>
            </div>
            <div class="PB10" ><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitle"><%= ResHelper.LocalizeString("{$=������� ��������|en-us=������� ��������$}")%>:</div>
                <div style="float:left">
                    <asp:DropDownList  CssClass="SrcHisPayDDL" ID="drpTransCount" runat="server" >
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>40</asp:ListItem>
                        <asp:ListItem>100</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="Clear"></div>
            </div>
            <div class="PB10" ><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
       </div>
       <div class="SrHistPayResTitle" ><a onclick="ShowHideSrcForm()" class="Chand"><img border="0" id="imgPointSrcType" src="~/app_themes/LivePay/PointerDown.gif"/> <span ID="spnSrcType"><%= ResHelper.LocalizeString("{$=��������� ���������|en-us=��������� ���������$}")%></span></a></div>
	   <div class="SrHistPayResBtnSearch"><asp:ImageButton ImageUrl="/app_themes/LivePay/btnSearch.png" runat="server" ID="btnSearchPayments" /></div>
	   <div class="Clear"></div>

       <div class="SrcHisPayMainGrid">
            <div class="SrcHisPayGridBGTop">
                <div class="SrcHisPayGridTopTitle" id="divHeader" runat="server" visible="false"><%=ResHelper.LocalizeString("{$=��������� ��������|en-us=��������� ��������$}") %></Div>
                <div class="SrcHisPayGridDiv">
                    <asp:GridView ID="GridViewPayments" EmptyDataTemplate-CssClass="SrcHisPayPagerEmpty" PagerStyle-CssClass="SrcHisPayPager" GridLines="None" runat="server" PageSize="8" AutoGenerateColumns="false" AllowPaging="true" Width="642px">
                    <HeaderStyle BackColor="#ffffff" ForeColor="#58595b" Height="35px" HorizontalAlign="Center"/>
                    <AlternatingRowStyle Height="25px" BackColor="#ffffff" ForeColor="#58595b" Font-Size="11px" Font-Names="tahoma" HorizontalAlign="Center"/>
                    <RowStyle Height="25px" BackColor="#f7f7f7" ForeColor="#58595b" Font-Size="11px" Font-Names="tahoma" HorizontalAlign="Center" />
                        <Columns >
                            <asp:TemplateField HeaderText="���������� ����������" HeaderStyle-CssClass="SrcHisPayGridHeader" ItemStyle-CssClass="SrcHisPayGridItem"  >
                                <ItemTemplate>
                                    <%#Eval("transactiondDte") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="������� ����"  >
                                <ItemTemplate>
                                    <asp:Label ID="lblMerchantID" runat="server" Text='<%#Eval("merchantId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="�����"  >
                                <ItemTemplate>
                                    <%#Eval("CardNumber") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="����" HeaderStyle-CssClass="SrcHisPayGridHeaderPrice" ItemStyle-CssClass="SrcHisPayGridItemPrice" ItemStyle-HorizontalAlign="Right"  >
                                <ItemTemplate>
                                    <asp:HiddenField ID="hidAmount" runat="server" value='<%#Eval("transactioAamount") %>' />
                                    <asp:Label ID="lblAmount" runat="server" />&nbsp;&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="����������" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                    <%#Eval("transactionStatus") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="&nbsp;" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="SrcHisPayGridHeaderLast" ItemStyle-CssClass="SrcHisPayGridItemLast" >
                                <ItemTemplate>
                                   <asp:LinkButton ID="openPopup" runat="server" OnClick="openPopup_Click" CommandArgument='<%#Eval("transactionId") %>'><img border="0" src="/app_themes/LivePay/InfoBtn.png" /></asp:LinkButton>  
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                        </Columns>
                        <EmptyDataTemplate >
                            <div class="SrcHisPayPagerEmpty">
                                <div style="padding-top:5px;padding-left:10px"><%=ResHelper.LocalizeString("{$=��� �������� ����������|en-us=NoRecords$}") %></div>
                            </div>
                        </EmptyDataTemplate>
                        <PagerTemplate >
                        <div class="SrcHisPayPagerSpace" >
                            <div>
                                <div class="GridPagerNoLinkDivLeft"><asp:linkbutton ID="lnkPrev" CssClass="PagerText" onclick="ChangePageByLinkNumber_Before" runat="server"><%= ResHelper.LocalizeString("{$=�����������|en-us=�����������$}")%></asp:linkbutton>&nbsp;</div>
                                <div class="GridPagerNoLinkDivCenter" id="lblPageIndex" runat="server">&nbsp;</div>
                                <div class="GridPagerNoLinkDivRight">&nbsp;<asp:linkbutton ID="lnkNext" CssClass="PagerText"  onclick="ChangePageByLinkNumber_Next" runat="server"><%=ResHelper.LocalizeString("{$=�������|en-us=�������$}") %></asp:linkbutton></div>
                                <div class="PagerCompleteSec"><img class="Chand" OnClick="ExportFile('ExportToPDF.aspx?ExportType=SrcHisPay');" src="/app_themes/LivePay/btnSavePDF.png" /></div>
                                <div class="GridPagerLast"><img class="Chand" OnClick="ExportFile('ExprotToExcel.aspx?ExportType=SrcHisPay');" src="/app_themes/LivePay/btnSaveExcel.png" /></div>
                                <div class="Clear"></div>
                            </div>
                       </div>
                         </PagerTemplate>
                    </asp:GridView>
                </div>
                <asp:LinkButton ID="HiddenPageBtn" style="display:none" runat="server" Text="0"></asp:LinkButton>
                <asp:TextBox ID="HiddenPage"  style="display:none" runat="server" Text="1"></asp:TextBox>
                <asp:TextBox ID="HiddenSearchType"  style="display:none" runat="server" Text="0"></asp:TextBox>
                <div id="BottomDiv" style="display:none" runat="server" class="SrcHisPayGridBGBottom"></div> 
            </div>
            
       </div>
       <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
	</div>
 </div>
 </div>
 <div style="display:none" id="FramDiv" runat="server" >
   <iframe id="exportFrame"  runat="server"></iframe>
 </div>
 </ContentTemplate>
</asp:UpdatePanel>

 <script language="javascript" type="text/javascript" >


     CheckAddtributes()
     function CheckAddtributes() {
         var GetSrcType = document.getElementById('<%=HiddenSearchType.ClientID %>').value
         var GetForm = document.getElementById('DivFullSrc');
         if (GetSrcType == '1') {
             GetForm.style.display = ''
         }
     }

     function GoToPage(no) {
         document.getElementById('<%=HiddenPage.ClientID %>').value = no;
         document.getElementById('<%=HiddenPageBtn.ClientID %>').innerHTML = no;
         var GetID = '<%=HiddenPageBtn.ClientID %>'
         GetID = GetID.replace(/[_]/gi, "$")
         __doPostBack(GetID, '')
     }

   

     function ShowHideSrcForm(checkfirst) {
         var GetForm = document.getElementById('DivFullSrc');
         var GetimgPoint = document.getElementById('imgPointSrcType');
         var GetSrcType = document.getElementById('spnSrcType');

         if (checkfirst) {
             var SectedValue = document.getElementById('<%=HiddenSearchType.ClientID %>').value
             if (SectedValue == '0') { //- Closed Form
                 GetForm.style.display = 'none'
                 document.getElementById('<%=HiddenSearchType.ClientID %>').value = '0';
                 GetimgPoint.src = '/app_themes/LivePay/PointerDown.gif'
                 GetSrcType.innerHTML = '��������� ���������'
             } else {
                 GetForm.style.display = ''
                 document.getElementById('<%=HiddenSearchType.ClientID %>').value = '1';
                 GetimgPoint.src = '/app_themes/LivePay/PointerUp.png'
                 GetSrcType.innerHTML = '���� ���������'
             }
         } else {
             if (GetForm.style.display == '') {
                 GetForm.style.display = 'none'
                 document.getElementById('<%=HiddenSearchType.ClientID %>').value = '0';
                 GetimgPoint.src = '/app_themes/LivePay/PointerDown.gif'
                 GetSrcType.innerHTML = '��������� ���������'
             } else {
                 GetForm.style.display = ''
                 document.getElementById('<%=HiddenSearchType.ClientID %>').value = '1';
                 GetimgPoint.src = '/app_themes/LivePay/PointerUp.png'
                 GetSrcType.innerHTML = '���� ���������'
             }
         }
     }

     function SetRblDates(val, Today, Oneweek, Onemonth, Towmonth, Sixmonths) {
         var GetVal = $("[id$=" + val.id + "] input:checked").val();
         var StartDtm = Today;
         var EndDtm;
         if (GetVal == '1') { // Simera
             EndDtm = Today;
         }
         if (GetVal == '2') { // 1 week
             EndDtm = Oneweek;
         }
         if (GetVal == '3') { // 1 month
             EndDtm = Onemonth;
         }
         if (GetVal == '4') { // 2 month
             EndDtm = Towmonth;
         }
         if (GetVal == '5') { // 6 months
             EndDtm = Sixmonths;
         }

         document.getElementById('<%=txtFrom.clientid %>').value = EndDtm
         document.getElementById('<%=txtTo.clientid %>').value = StartDtm
     }

 
   


    
   
</script>