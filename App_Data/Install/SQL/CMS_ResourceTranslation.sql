CREATE TABLE [CMS_ResourceTranslation] (
		[TranslationID]              int NOT NULL IDENTITY(1, 1),
		[TranslationStringID]        int NOT NULL,
		[TranslationUICultureID]     int NOT NULL,
		[TranslationText]            nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_ResourceTranslation]
	ADD
	CONSTRAINT [PK_CMS_ResourceTranslation]
	PRIMARY KEY
	([TranslationID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_ResourceTranslation]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_ResourceString]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_ResourceTranslation_TranslationStringID_CMS_ResourceString]') IS NULL
BEGIN
		ALTER TABLE [CMS_ResourceTranslation]
			ADD CONSTRAINT [FK_CMS_ResourceTranslation_TranslationStringID_CMS_ResourceString]
			FOREIGN KEY ([TranslationStringID]) REFERENCES [CMS_ResourceString] ([StringID])
END
IF OBJECT_ID(N'[CMS_ResourceTranslation]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_UICulture]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_ResourceTranslation_TranslationUICultureID_CMS_UICulture]') IS NULL
BEGIN
		ALTER TABLE [CMS_ResourceTranslation]
			ADD CONSTRAINT [FK_CMS_ResourceTranslation_TranslationUICultureID_CMS_UICulture]
			FOREIGN KEY ([TranslationUICultureID]) REFERENCES [CMS_UICulture] ([UICultureID])
END
CREATE INDEX [IX_CMS_ResourceTranslation_TranslationStringID]
	ON [CMS_ResourceTranslation] ([TranslationStringID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_ResourceTranslation_TranslationUICultureID]
	ON [CMS_ResourceTranslation] ([TranslationUICultureID])
	ON [PRIMARY]
