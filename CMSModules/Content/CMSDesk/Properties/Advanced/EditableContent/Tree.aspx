<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Tree.aspx.cs" Inherits="CMSModules_Content_CMSDesk_Properties_Advanced_EditableContent_Tree"
    Theme="default" MasterPageFile="~/CMSMasterPages/UI/Tree.master" %>

<%@ Register Src="~/CMSModules/Content/Controls/ContentTree.ascx" TagName="ContentTree"
    TagPrefix="cms" %>
<asp:Content ID="plcBeforeTree" runat="server" ContentPlaceHolderID="plcBeforeTree">
    <div class="MenuBox" style="width: 100%;">
        <asp:Panel ID="pnlMenu" runat="server" CssClass="TreeMenu" Style="margin-bottom: 5px;">
            <asp:Panel ID="pnlMenuContent" runat="server" CssClass="TreeMenuContent">
                <table width="100%">
                    <tr>
                        <td style="width: 50%">
                            <div>
                                <asp:Image ID="imgNewItem" runat="server" CssClass="NewItemImage" />
                                <cms:LocalizedHyperlink ID="lnkNewItem" runat="server" NavigateUrl="#" CssClass="NewItemLink"
                                    ResourceString="editablecontent.newitem" />
                            </div>
                        </td>
                        <td>
                            <div>
                                <asp:Image ID="imgDeleteItem" runat="server" CssClass="NewItemImage" />
                                <asp:HyperLink ID="lnkDeleteItem" runat="server" NavigateUrl="#" CssClass="NewItemLinkDisabled" />
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcTree">
    <asp:Panel ID="pnlContent" runat="server" CssClass="WebPartTree">
        <asp:HiddenField ID="hdnCurrentNodeType" runat="server" />
        <asp:HiddenField ID="hdnCurrentNodeName" runat="server" />
        <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />

        <script type="text/javascript">
            //<![CDATA[
            var currentNode = null;

            // Refresh node action
            function RefreshNode(nodeName, nodeType, nodeId) {
                currentNode.firstChild.innerHTML = nodeName;
                // Dynamically create onclick event
                currentNode.onclick = function() { SelectNode(nodeName, nodeType, this); };
            }

            //]]>
        </script>

        <asp:Panel runat="server" ID="pnlBody" CssClass="ContentTree">
            <asp:TreeView ID="webpartsTree" runat="server" ShowLines="True" EnableViewState="False" />
            <br />
            <asp:TreeView ID="regionsTree" runat="server" ShowLines="True" EnableViewState="False" />
        </asp:Panel>
    </asp:Panel>
</asp:Content>
