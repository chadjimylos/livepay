<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Edit.aspx.cs" Inherits="CMSModules_Content_CMSDesk_Edit_Edit"
    ValidateRequest="false" Theme="Default" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Content - Edit</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }
        .ContentEditArea
        {
            height: 100%;
            width: 100%;
            position: absolute;
            z-index: 1; /*overflow: -moz-scrollbars-vertical;*/
            overflow: auto;
        }
    </style>

    <script src="../../../../CMSAdminControls/SpellChecker/spell.js" type="text/javascript"></script>

    <script src="edit.js" type="text/javascript"></script>

    <asp:Literal ID="ltlSpellScript" runat="server" EnableViewState="false" />

</head>
<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptManager" runat="server" />
    <input type="hidden" name="saveChanges" id="saveChanges" value="0" />

    <script type="text/javascript" src="<%= ResolveUrl("~/CMSScripts/savechanges.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/CMSScripts/tooltip/wz_tooltip.js") %>"></script>

    <div class="ContentEditArea">
        <input type="hidden" id="hidAnother" name="hidAnother" value="" />
        <asp:Panel runat="server" ID="pnlWorkflowInfo" CssClass="PageManagerWorkflowInfo"
            EnableViewState="false">
            <asp:Label ID="lblWorkflowInfo" runat="server" CssClass="WorkflowInfo" EnableViewState="false" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlBody" CssClass="PageBody">
            <asp:Panel runat="server" ID="pnlContent" CssClass="PageContent">
                <cms:CMSForm runat="server" ID="formElem" Visible="false" HtmlAreaToolbarLocation="Out:FCKToolbar"
                    ShowOkButton="false" IsLiveSite="false" />
                <br />
                <asp:PlaceHolder ID="plcNewProduct" runat="server">
                    <asp:CheckBox ID="chkCreateProduct" runat="server" CssClass="EditingFormLabel" />
                    <asp:Panel ID="pnlNewProduct" runat="server" />
                </asp:PlaceHolder>
            </asp:Panel>
        </asp:Panel>
        <cms:CMSButton ID="btnSave" runat="server" CssClass="HiddenButton" OnClick="btnSave_Click"
            EnableViewState="false" />
        <cms:CMSButton ID="btnApprove" runat="server" CssClass="HiddenButton" EnableViewState="false"
            OnClick="btnApprove_Click" />
        <cms:CMSButton ID="btnReject" runat="server" CssClass="HiddenButton" EnableViewState="false"
            OnClick="btnReject_Click" />
        <cms:CMSButton ID="btnCheckIn" runat="server" CssClass="HiddenButton" EnableViewState="false"
            OnClick="btnCheckIn_Click" />
        <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    </div>
    </form>
</body>
</html>
