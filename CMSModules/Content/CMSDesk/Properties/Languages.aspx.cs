using System;
using System.Data;
using System.IO;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.WorkflowEngine;
using CMS.TreeEngine;
using CMS.LicenseProvider;
using CMS.SettingsProvider;

using TreeNode = CMS.TreeEngine.TreeNode;
using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Content_CMSDesk_Properties_Languages : CMSPropertiesPage
{
    #region "Private variables"

    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;
    private TimeZoneInfo usedTimeZone = null;
    private bool dataReloaded = false;

    #endregion


    #region "Protected variables"

    protected int nodeId = 0;
    protected DateTime defaultLastModification = DateTimeHelper.ZERO_TIME;
    protected DateTime defaultLastPublished = DateTimeHelper.ZERO_TIME;
    protected string mDefaultSiteCulture = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Default culture of the site
    /// </summary>
    protected string DefaultSiteCulture
    {
        get
        {
            if (mDefaultSiteCulture == null)
            {
                mDefaultSiteCulture = CultureHelper.GetDefaultCulture(CMSContext.CurrentSiteName);
            }
            return mDefaultSiteCulture;
        }
    }

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.Languages"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.Languages");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get ID of node
        nodeId = QueryHelper.GetInteger("nodeid", 0);

        gridLanguages.FilteredZeroRowsText = ResHelper.GetString("transman.nodocumentculture");
        gridLanguages.OnPageChanged += new EventHandler<EventArgs>(gridLanguages_OnPageChanged);
    }


    void gridLanguages_OnPageChanged(object sender, EventArgs e)
    {
        ReloadData();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Check license limitations
        if (!CultureInfoProvider.LicenseVersionCheck())
        {
            LicenseHelper.GetAllAvailableKeys(FeatureEnum.Multilingual);
        }

        // Set selected tab
        UIContext.PropertyTab = PropertyTabEnum.Languages;

        // Reload data
        ReloadData();
    }

    #endregion


    #region "Grid events"

    protected object gridLanguages_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        TranslationStatusEnum status = TranslationStatusEnum.NotAvailable;

        sourceName = sourceName.ToLower();
        switch (sourceName)
        {
            case "action":
                // Create new image
                status = GetTranslationStatus(sender as DataControlFieldCell);
                ImageButton img = new ImageButton();
                // Set appropriate icon
                switch (status)
                {
                    case TranslationStatusEnum.NotAvailable:
                        img.ImageUrl = GetImageUrl("CMSModules/CMS_Content/Properties/addculture.png");
                        img.ToolTip = ResHelper.GetString("transman.createnewculture");
                        break;

                    default:
                        img.ImageUrl = GetImageUrl("CMSModules/CMS_Content/Properties/editculture.png");
                        img.ToolTip = ResHelper.GetString("transman.editculture");
                        break;
                }
                // Register redirect script
                string culture = GetDocumentCulture(sender as DataControlFieldCell);
                img.OnClientClick = "RedirectItem(" + nodeId + ", '" + culture + "');";
                img.ID = "imgAction";
                return img;

            case "translationstatus":
                if (parameter == DBNull.Value)
                {
                    status = TranslationStatusEnum.NotAvailable;
                }
                else
                {
                    status = (TranslationStatusEnum)parameter;
                }
                string statusName = ResHelper.GetString("transman." + status);
                string statusHtml = "<span class=\"" + status + "\">" + statusName + "</span>";
                // .Outdated
                return statusHtml;

            case "documentculturedisplayname":
                DataRowView drv = (DataRowView)parameter;

                // Add icon
                return UniGridFunctions.DocumentCultureFlag(drv, this.Page);

            case "documentmodifiedwhen":
            case "documentmodifiedwhentooltip":
                if (string.IsNullOrEmpty(parameter.ToString()))
                {
                    return "-";
                }
                else
                {
                    DateTime modifiedWhen = ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME);
                    if (currentUserInfo == null)
                    {
                        currentUserInfo = CMSContext.CurrentUser;
                    }
                    if (currentSiteInfo == null)
                    {
                        currentSiteInfo = CMSContext.CurrentSite;
                    }

                    if (sourceName == "documentmodifiedwhen")
                    {
                        return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(modifiedWhen,
                            currentUserInfo, currentSiteInfo, out usedTimeZone);
                    }
                    else
                    {
                        if (TimeZoneHelper.TimeZonesEnabled() && (usedTimeZone == null))
                        {
                            TimeZoneHelper.GetCurrentTimeZoneDateTimeString(modifiedWhen,
                                                        currentUserInfo, currentSiteInfo, out usedTimeZone);
                        }
                        return TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone);
                    }
                }

            case "versionnumber":
                if (string.IsNullOrEmpty(parameter.ToString()))
                {
                    return "-";
                }
                break;

            case "documentname":
                if (string.IsNullOrEmpty(parameter.ToString()))
                {
                    parameter = "-";
                }
                return HTMLHelper.HTMLEncode(parameter.ToString());

            case "published":
                bool published = ValidationHelper.GetBoolean(parameter, false);
                if (published)
                {
                    return "<span class=\"DocumentPublishedYes\">" + ResHelper.GetString("General.Yes") + "</span>";
                }
                else
                {
                    return "<span class=\"DocumentPublishedNo\">" + ResHelper.GetString("General.No") + "</span>";
                }
        }
        return parameter;
    }

    #endregion


    #region "Protected methods"

    /// <summary>
    /// Parses status from given string
    /// </summary>
    /// <param name="status">Status to parse</param>
    /// <returns>Translation status.</returns>
    protected TranslationStatusEnum GetTranslationStatus(string status)
    {
        return (TranslationStatusEnum)Enum.Parse(typeof(TranslationStatusEnum), status);
    }


    /// <summary>
    /// Gets translation status value from DataControlFieldCell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Translation status of document</returns>
    protected TranslationStatusEnum GetTranslationStatus(DataControlFieldCell dcf)
    {
        // Get datarowview
        DataRowView drv = GetDataRowView(dcf);
        if ((drv != null) && (drv.Row["TranslationStatus"] != DBNull.Value))
        {
            // Get translation status
            return (TranslationStatusEnum)drv.Row["TranslationStatus"];
        }
        else
        {
            return TranslationStatusEnum.NotAvailable;
        }
    }


    /// <summary>
    /// Gets document culture value from DataControlFieldCell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Culture of document</returns>
    protected string GetDocumentCulture(DataControlFieldCell dcf)
    {
        // Get datarowview
        DataRowView drv = GetDataRowView(dcf);
        if (drv != null)
        {
            // Get culture
            return ValidationHelper.GetString(drv.Row["DocumentCulture"], string.Empty);
        }
        else
        {
            return string.Empty;
        }
    }


    /// <summary>
    /// Gets document culture name value from DataControlFieldCell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Culture name of document</returns>
    protected string GetDocumentCultureName(DataControlFieldCell dcf)
    {
        // Get datarowview
        DataRowView drv = GetDataRowView(dcf);
        if (drv != null)
        {
            // Get culture
            return ValidationHelper.GetString(drv.Row["DocumentCultureDisplayName"], string.Empty);
        }
        else
        {
            return string.Empty;
        }
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }


    protected void ReloadData()
    {
        if (nodeId != 0)
        {
            if (!dataReloaded)
            {
                dataReloaded = true;
                // Get node
                TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                TreeNode node = tree.SelectSingleNode(nodeId);
                // Check if node is not null
                if (node != null)
                {
                    // Get documents
                    int topN = gridLanguages.GridView.PageSize * (gridLanguages.GridView.PageIndex + 1 + gridLanguages.GridView.PagerSettings.PageButtonCount);
                    DataSet documentsDS = DocumentHelper.GetDocuments(CMSContext.CurrentSiteName, node.NodeAliasPath, TreeProvider.ALL_CULTURES, false, null, null, null, -1, false, topN, TreeProvider.SELECTNODES_REQUIRED_COLUMNS + ", DocumentModifiedWhen, DocumentLastPublished, DocumentName, VersionNumber, Published", tree);
                    DataTable documents = documentsDS.Tables[0];

                    if (!DataHelper.DataSourceIsEmpty(documents))
                    {
                        // Get site cultures
                        DataSet allSiteCultures = CultureInfoProvider.GetSiteCultures(CMSContext.CurrentSiteName).Copy();

                        // Rename culture column to enable row transfer
                        allSiteCultures.Tables[0].Columns[2].ColumnName = "DocumentCulture";
                        // Create where condition for row transfer
                        string where = "DocumentCulture NOT IN (";
                        foreach (DataRow row in documents.Rows)
                        {
                            where += "'" + row["DocumentCulture"] + "',";
                        }
                        where = where.TrimEnd(',') + ")";

                        // Transfer missing cultures, keep original list of site cultures
                        DataHelper.TransferTableRows(documents, allSiteCultures.Copy().Tables[0], where, null);
                        DataHelper.EnsureColumn(documents, "DocumentCultureDisplayName", typeof(string));

                        // Ensure culture names
                        foreach (DataRow cultDR in documents.Rows)
                        {
                            string cultureCode = cultDR["DocumentCulture"].ToString();
                            DataRow[] cultureRow = allSiteCultures.Tables[0].Select("DocumentCulture='" + cultureCode + "'");
                            if (cultureRow.Length > 0)
                            {
                                cultDR["DocumentCultureDisplayName"] = cultureRow[0]["CultureName"].ToString();
                            }
                        }

                        // Initialize unigrid
                        gridLanguages.OnExternalDataBound += gridLanguages_OnExternalDataBound;

                        // Ensure default culture to be first
                        DataRow defaultCultureRow = documents.Select("DocumentCulture='" + DefaultSiteCulture + "'")[0];

                        DataRow dr = documents.NewRow();
                        dr.ItemArray = defaultCultureRow.ItemArray;
                        documents.Rows.InsertAt(dr, 0);
                        documents.Rows.Remove(defaultCultureRow);

                        // Get last modification date of default culture
                        defaultCultureRow = documents.Select("DocumentCulture='" + DefaultSiteCulture + "'")[0];
                        defaultLastModification = ValidationHelper.GetDateTime(defaultCultureRow["DocumentModifiedWhen"], DateTimeHelper.ZERO_TIME);
                        defaultLastPublished = ValidationHelper.GetDateTime(defaultCultureRow["DocumentLastPublished"], DateTimeHelper.ZERO_TIME);
                        // Add column containing translation status
                        documents.Columns.Add("TranslationStatus", typeof(TranslationStatusEnum));

                        // Get proper translation status and store it to datatable
                        foreach (DataRow document in documents.Rows)
                        {
                            TranslationStatusEnum status = TranslationStatusEnum.NotAvailable;
                            int documentId = ValidationHelper.GetInteger(document["DocumentID"], 0);
                            if (documentId == 0)
                            {
                                status = TranslationStatusEnum.NotAvailable;
                            }
                            else
                            {
                                string versionNumber = ValidationHelper.GetString(DataHelper.GetDataRowValue(document, "VersionNumber"), null);
                                DateTime lastModification = DateTimeHelper.ZERO_TIME;

                                // Check if document is outdated
                                if (versionNumber != null)
                                {
                                    lastModification = ValidationHelper.GetDateTime(document["DocumentLastPublished"], DateTimeHelper.ZERO_TIME);
                                    status = (lastModification < defaultLastPublished) ? TranslationStatusEnum.Outdated : TranslationStatusEnum.Translated;
                                }
                                else
                                {
                                    lastModification = ValidationHelper.GetDateTime(document["DocumentModifiedWhen"], DateTimeHelper.ZERO_TIME);
                                    status = (lastModification < defaultLastModification) ? TranslationStatusEnum.Outdated : TranslationStatusEnum.Translated;
                                }
                            }
                            document["TranslationStatus"] = status;
                        }

                        // Bind datasource
                        DataSet filteredDocuments = documentsDS.Clone();
                        DataRow[] filteredDocs = documents.Select(gridLanguages.GetDataTableFilter());
                        foreach (DataRow row in filteredDocs)
                        {
                            filteredDocuments.Tables[0].ImportRow(row);
                        }

                        gridLanguages.DataSource = filteredDocuments;
                        gridLanguages.ReloadData();
                    }
                }
            }
        }
    }

    #endregion
}
