using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.FormEngine;
using CMS.UIControls;

public partial class CMSSiteManager_Development_DocumentTypes_AlternativeForms_AlternativeForms_Fields : SiteManagerPage
{
    protected int altFormId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        altFormId = QueryHelper.GetInteger("altformid", 0);

        altFormFieldEditor.AlternativeFormID = altFormId;
        altFormFieldEditor.DisplayedControls = FieldEditorControlsEnum.DocumentTypes;
        altFormFieldEditor.EnableSystemFields = true;
    }
}
