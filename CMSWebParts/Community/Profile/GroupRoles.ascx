<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Community/Profile/GroupRoles.ascx.cs"
    Inherits="CMSWebParts_Community_Profile_GroupRoles" %>
<%@ Register Src="~/CMSSiteManager/Administration/Roles/Controls/Roles.ascx" TagName="Roles"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/System/PermissionMessage.ascx" TagName="PermissionMessage"
    TagPrefix="cms" %>
<cms:PermissionMessage ID="messageElem" runat="server" Visible="false" EnableViewState="false" />
<cms:Roles ID="rolesElem" runat="server" />
