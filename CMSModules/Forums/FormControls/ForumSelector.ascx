<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForumSelector.ascx.cs" Inherits="CMSModules_Forums_FormControls_ForumSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server" RenderMode="InLine">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" ObjectType="Forums.Forum" SelectionMode="SingleDropDownList"  AllowEditTextBox="true" />        
    </ContentTemplate>
</cms:CMSUpdatePanel>
