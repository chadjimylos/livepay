CREATE TABLE [CMS_WebFarmTask] (
		[TaskID]              int NOT NULL IDENTITY(1, 1),
		[TaskType]            nvarchar(50) NOT NULL,
		[TaskTextData]        nvarchar(max) NULL,
		[TaskBinaryData]      varbinary(max) NULL,
		[TaskCreated]         datetime NULL,
		[TaskEnabled]         bit NULL,
		[TaskTarget]          nvarchar(450) NULL,
		[TaskMachineName]     nvarchar(450) NULL,
		[TaskGUID]            uniqueidentifier NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WebFarmTask]
	ADD
	CONSTRAINT [PK_CMS_WebFarmTask]
	PRIMARY KEY
	([TaskID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_WebFarmTask]
	ADD
	CONSTRAINT [DEFAULT_CMS_WebFarmTask_TaskGUID]
	DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [TaskGUID]
CREATE INDEX [IX_CMS_WebFarmTask_TaskEnabled]
	ON [CMS_WebFarmTask] ([TaskEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
