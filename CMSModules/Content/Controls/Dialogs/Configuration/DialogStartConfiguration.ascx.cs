using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections;
using System.Data;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;

public partial class CMSModules_Content_Controls_Dialogs_Configuration_DialogStartConfiguration : CMSUserControl, IFieldEditorControl
{
    #region "Variables"

    protected bool communityLoaded = false;
    protected bool mediaLoaded = false;
    private bool mDisplayAutoResize = true;
    private bool mDisplayEmailTabSetting = true;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets the value which determines whether the control allows the user to set the E-mail tab setting.
    /// </summary>
    public bool DisplayEmailTabSetting
    {
        get
        {
            return this.mDisplayEmailTabSetting;
        }
        set
        {
            this.mDisplayEmailTabSetting = value;
            this.plcDisplayEmail.Visible = value;
        }
    }


    /// <summary>
    /// Indicates if configuration of the dimensions for image auto resizing should be displayed to the user.
    /// </summary>
    public bool DisplayAutoResize
    {
        get
        {
            return mDisplayAutoResize;
        }
        set
        {
            mDisplayAutoResize = value;
        }
    }

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        communityLoaded = ModuleEntry.IsModuleLoaded(ModuleEntry.COMMUNITY);
        mediaLoaded = ModuleEntry.IsModuleLoaded(ModuleEntry.MEDIALIBRARY);

        this.plcMedia.Visible = mediaLoaded;
        this.plcGroups.Visible = communityLoaded;
        
        if (communityLoaded)
        {
            this.drpGroups.SelectedIndexChanged += new EventHandler(drpGroups_SelectedIndexChanged);
        }

        LoadSites();
        if (!RequestHelper.IsPostBack())
        {            
            LoadSiteLibraries(null);
            LoadSiteGroups(null);
            LoadGroupLibraries(null, null);
        }

        this.plcAutoResize.Visible = this.DisplayAutoResize;
        this.plcDisplayEmail.Visible = this.DisplayEmailTabSetting;
    }

    protected void drpGroups_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelectGroup();
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelectorMediaSites_OnSelectionChanged(object sender, EventArgs e)
    {
        string selectedSite = ValidationHelper.GetString(siteSelectorMedia.Value, String.Empty);
        SelectSite(selectedSite);
    }

    #endregion


    #region "Private methods"


    /// <summary>
    /// Processes the data loading after the site is selected.
    /// </summary>
    private void SelectSite(string selectedSite)
    {
        if (communityLoaded)
        {
            LoadSiteGroups(selectedSite);
        }
        LoadSiteLibraries(selectedSite);
        SelectGroup();
    }


    /// <summary>
    /// Processes the data loading after the group is selected.
    /// </summary>
    private void SelectGroup()
    {
        if (drpGroups.SelectedValue == "#none#")
        {
            drpGroupLibraries.Items.Clear();
            drpGroupLibraries.Items.Insert(0, new ListItem(ResHelper.GetString("general.selectnone"), ""));
            drpGroupLibraries.SelectedIndex = 0;
            drpGroupLibraries.Enabled = false;
        }
        else
        {
            drpGroupLibraries.Items.Clear();
            drpGroupLibraries.Enabled = true;

            string selectedMediaSite = ValidationHelper.GetString(siteSelectorMedia.Value, String.Empty);
            LoadGroupLibraries(selectedMediaSite, drpGroups.SelectedValue);
        }
    }


    /// <summary>
    /// Loads the site dropdownlists.
    /// </summary>
    private void LoadSites()
    {
        // Set site selector
        siteSelectorContent.DropDownSingleSelect.AutoPostBack = true;
        siteSelectorContent.AllowAll = false;
        siteSelectorContent.UseCodeNameForSelection = true;
        siteSelectorContent.UniSelector.SpecialFields = new string[2, 2] { {ResHelper.GetString("general.selectall"), "##all##" }, { ResHelper.GetString("dialogs.config.currentsite"), "##current##"} };        

        siteSelectorMedia.DropDownSingleSelect.AutoPostBack = true;
        siteSelectorMedia.AllowAll = false;
        siteSelectorMedia.UseCodeNameForSelection = true;
        siteSelectorMedia.UniSelector.SpecialFields = new string[2, 2] { { ResHelper.GetString("general.selectall"), "##all##" }, { ResHelper.GetString("dialogs.config.currentsite"), "##current##" } };
        
        if (mediaLoaded)
        {
            siteSelectorMedia.UniSelector.OnSelectionChanged += new EventHandler(UniSelectorMediaSites_OnSelectionChanged);
        }
    }


    /// <summary>
    /// Reloads the site groups.
    /// </summary>
    /// <param name="siteName">Name of the site</param>
    private void LoadSiteGroups(string siteName)
    {
        if (communityLoaded && mediaLoaded)
        {
            drpGroups.Items.Clear();

            if (siteName != null)
            {
                DataSet dsGroups = ModuleCommands.CommunityGetSiteGroups(siteName);
                if (!DataHelper.DataSourceIsEmpty(dsGroups))
                {
                    dsGroups.Tables[0].DefaultView.Sort = "GroupDisplayName";
                    drpGroups.DataValueField = "GroupName";
                    drpGroups.DataTextField = "GroupDisplayName";
                    drpGroups.DataSource = dsGroups.Tables[0].DefaultView;
                    drpGroups.DataBind();
                }
            }
            drpGroups.Items.Insert(0, new ListItem(ResHelper.GetString("general.selectall"), ""));
            drpGroups.Items.Insert(1, new ListItem(ResHelper.GetString("general.selectnone"), "#none#"));
            drpGroups.Items.Insert(2, new ListItem(ResHelper.GetString("dialogs.config.currentgroup"), "#current#"));
        }
    }


    /// <summary>
    /// Reloads the site media libraries.
    /// </summary>
    /// <param name="siteName">Name of the site</param>
    private void LoadSiteLibraries(string siteName)
    {
        if (mediaLoaded)
        {
            drpSiteLibraries.Items.Clear();

            if (siteName != null)
            {
                DataSet dsLibraries = ModuleCommands.MediaLibraryGetSiteLibraries(siteName);
                if (!DataHelper.DataSourceIsEmpty(dsLibraries))
                {
                    dsLibraries.Tables[0].DefaultView.Sort = "LibraryDisplayName";
                    drpSiteLibraries.DataValueField = "LibraryName";
                    drpSiteLibraries.DataTextField = "LibraryDisplayName";
                    drpSiteLibraries.DataSource = dsLibraries.Tables[0].DefaultView;
                    drpSiteLibraries.DataBind();
                }
            }
            drpSiteLibraries.Items.Insert(0, new ListItem(ResHelper.GetString("general.selectall"), ""));
            drpSiteLibraries.Items.Insert(1, new ListItem(ResHelper.GetString("general.selectnone"), "#none#"));
            drpSiteLibraries.Items.Insert(2, new ListItem(ResHelper.GetString("dialogs.config.currentlibrary"), "#current#"));
        }
    }


    /// <summary>
    /// Reloads the group media libraries.
    /// </summary>
    /// <param name="siteName">Name of the site</param>
    /// <param name="groupName">Name of the group</param>
    private void LoadGroupLibraries(string siteName, string groupName)
    {
        if (mediaLoaded && communityLoaded)
        {
            drpGroupLibraries.Items.Clear();

            if ((siteName != null) && (groupName != null))
            {
                DataSet dsLibraries = ModuleCommands.MediaLibraryGetGroupLibraries(siteName, groupName);
                if (!DataHelper.DataSourceIsEmpty(dsLibraries))
                {
                    dsLibraries.Tables[0].DefaultView.Sort = "LibraryDisplayName";
                    drpGroupLibraries.DataValueField = "LibraryName";
                    drpGroupLibraries.DataTextField = "LibraryDisplayName";
                    drpGroupLibraries.DataSource = dsLibraries.Tables[0].DefaultView;
                    drpGroupLibraries.DataBind();
                }
            }
            drpGroupLibraries.Items.Insert(0, new ListItem(ResHelper.GetString("general.selectall"), ""));
            drpGroupLibraries.Items.Insert(1, new ListItem(ResHelper.GetString("dialogs.config.currentlibrary"), "#current#"));
        }
    }


    /// <summary>
    /// Selects correct item in given DDL.
    /// </summary>
    /// <param name="config">Configuration hashtable</param>
    /// <param name="ddl">Dropdownlist with the data</param>
    /// <param name="origKey">Key in hashtable which determines whether the value is special or specific item</param>
    /// <param name="singleItemKey">Key in hashtable for specified item</param>
    private void SelectInDDL(Hashtable config, DropDownList ddl, string origKey, string singleItemKey)
    {
        string item = ValidationHelper.GetString(config[origKey], "").ToLower();
        if (item == "#single#")
        {
            item = ValidationHelper.GetString(config[singleItemKey], "");
        }
        ListItem li = ddl.Items.FindByValue(item);
        if (li != null)
        {
            ddl.SelectedValue = li.Value;
        }
    }

    #endregion


    #region "Public methods"

    /// <summary>
    /// Sets inner controls according to the parameters and their values included in configuration collection. Parameters collection will be passed from Field editor
    /// </summary>
    /// <param name="config">Parameters collection.</param>
    public void LoadConfiguration(Hashtable config)
    {
        if (config.Count > 0)
        {
            if (this.DisplayAutoResize)
            {
                elemAutoResize.LoadConfiguration(config);
            }

            // Content tab
            this.chkDisplayContentTab.Checked = !ValidationHelper.GetBoolean(config["dialogs_content_hide"], false);
            this.selectPathElem.Value = ValidationHelper.GetString(config["dialogs_content_path"], "");

            siteSelectorContent.DropDownSingleSelect.SelectedValue = ValidationHelper.GetString(config["dialogs_content_site"], null);

            // Media tab
            if (mediaLoaded)
            {
                this.chkDisplayMediaTab.Checked = !ValidationHelper.GetBoolean(config["dialogs_libraries_hide"], false);

                // Site DDL                
                string libSites = ValidationHelper.GetString(config["dialogs_libraries_site"], null);
                siteSelectorMedia.DropDownSingleSelect.SelectedValue = libSites;
                SelectSite(libSites);

                // Site libraries DDL
                SelectInDDL(config, drpSiteLibraries, "dialogs_libraries_global", "dialogs_libraries_global_libname");

                if (communityLoaded)
                {
                    // Groups DDL
                    SelectInDDL(config, drpGroups, "dialogs_groups", "dialogs_groups_name");
                    SelectGroup();

                    // Group libraries DDL
                    SelectInDDL(config, drpGroupLibraries, "dialogs_libraries_group", "dialogs_libraries_group_libname");
                }

                // Starting path
                this.txtMediaStartPath.Text = ValidationHelper.GetString(config["dialogs_libraries_path"], "");
            }

            // Other tabs        
            this.chkDisplayAttachments.Checked = !ValidationHelper.GetBoolean(config["dialogs_attachments_hide"], false);
            this.chkDisplayAnchor.Checked = !ValidationHelper.GetBoolean(config["dialogs_anchor_hide"], false);
            this.chkDisplayEmail.Checked = !ValidationHelper.GetBoolean(config["dialogs_email_hide"], false);
            this.chkDisplayWeb.Checked = !ValidationHelper.GetBoolean(config["dialogs_web_hide"], false);
        }
    }


    /// <summary>
    /// Updates collection of parameters and values according to the values of the inner controls. Parameters collection which should be updated will be passed from Field editor.
    /// </summary>
    /// <param name="config">Parameters collection.</param>
    public void UpdateConfiguration(Hashtable config)
    {
        if (this.DisplayAutoResize)
        {
            elemAutoResize.UpdateConfiguration(config);
        }

        // Clear dialogs settings
        config.Remove("dialogs_content_hide");
        config.Remove("dialogs_content_path");
        config.Remove("dialogs_content_site");
        config.Remove("dialogs_libraries_hide");
        config.Remove("dialogs_libraries_site");
        config.Remove("dialogs_libraries_global");
        config.Remove("dialogs_libraries_global_libname");
        config.Remove("dialogs_groups");
        config.Remove("dialogs_groups_name");
        config.Remove("dialogs_libraries_group");
        config.Remove("dialogs_libraries_group_libname");
        config.Remove("dialogs_libraries_path");
        config.Remove("dialogs_attachments_hide");
        config.Remove("dialogs_anchor_hide");
        config.Remove("dialogs_email_hide");
        config.Remove("dialogs_web_hide");

        // Content tab
        if (!this.chkDisplayContentTab.Checked)
        {
            config["dialogs_content_hide"] = true;
        }
        if ((string)this.selectPathElem.Value != "")
        {
            config["dialogs_content_path"] = this.selectPathElem.Value;
        }


        string selectedSite = ValidationHelper.GetString(siteSelectorContent.Value, String.Empty);
        if (selectedSite != String.Empty)
        {
            config["dialogs_content_site"] = selectedSite;
        }

        // Media tab
        if (mediaLoaded)
        {
            if (!this.chkDisplayMediaTab.Checked)
            {
                config["dialogs_libraries_hide"] = true;
            }

            selectedSite = ValidationHelper.GetString(siteSelectorMedia.Value, String.Empty);
            if (selectedSite != String.Empty)
            {
                config["dialogs_libraries_site"] = selectedSite;
            }

            // Site libraries DDL
            string value = this.drpSiteLibraries.SelectedValue;
            if ((value == "#none#") || (value == "#current#"))
            {
                config["dialogs_libraries_global"] = value;
            }
            else if (value != "")
            {
                config["dialogs_libraries_global"] = "#single#";
                config["dialogs_libraries_global_libname"] = this.drpSiteLibraries.SelectedValue;
            }

            if (communityLoaded)
            {
                // Groups DDL
                value = this.drpGroups.SelectedValue;
                if ((value == "#none#") || (value == "#current#"))
                {
                    config["dialogs_groups"] = value;
                }
                else if (value != "")
                {
                    config["dialogs_groups"] = "#single#";
                    config["dialogs_groups_name"] = this.drpGroups.SelectedValue;
                }

                // Group libraries DDL
                value = this.drpGroupLibraries.SelectedValue;
                if ((value == "#none#") || (value == "#current#"))
                {
                    config["dialogs_libraries_group"] = value;
                }
                else if (value != "")
                {
                    config["dialogs_libraries_group"] = "#single#";
                    config["dialogs_libraries_group_libname"] = this.drpGroupLibraries.SelectedValue;
                }
            }

            // Starting path
            value = this.txtMediaStartPath.Text.Trim();
            if (value != "")
            {
                config["dialogs_libraries_path"] = value;
            }

        }

        // Other tabs
        if (!this.chkDisplayAttachments.Checked)
        {
            config["dialogs_attachments_hide"] = true;
        }
        if (!this.chkDisplayAnchor.Checked)
        {
            config["dialogs_anchor_hide"] = true;
        }
        if (!this.chkDisplayEmail.Checked)
        {
            config["dialogs_email_hide"] = true;
        }
        if (!this.chkDisplayWeb.Checked)
        {
            config["dialogs_web_hide"] = true;
        }
    }


    public string Validate()
    {
        if (this.DisplayAutoResize)
        {
            return elemAutoResize.Validate();
        }
        return "";
    }


    public void ClearControl()
    {
        if (this.DisplayAutoResize)
        {
            elemAutoResize.ClearControl();
        }

        // Load default values for media libraries
        siteSelectorMedia.Value = "##all##";
        SelectSite(null);
        txtMediaStartPath.Text = "";


        // Load default values for content
        siteSelectorContent.Value = "##all##";
        selectPathElem.Value = "";

        // Mark all checkboxes as checked        
        chkDisplayAttachments.Checked = true;
        chkDisplayContentTab.Checked = true;
        chkDisplayMediaTab.Checked = true;
        chkDisplayAnchor.Checked = true;
        chkDisplayEmail.Checked = true;
        chkDisplayWeb.Checked = true;
    }

    #endregion
}