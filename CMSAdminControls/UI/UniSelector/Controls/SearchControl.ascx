<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchControl.ascx.cs"
    Inherits="CMSAdminControls_UI_UniSelector_Controls_SearchControl" %>
<asp:Panel CssClass="Filter" runat="server" ID="pnlSearch">
    <asp:DropDownList ID="drpCondition" runat="server" />
    <asp:TextBox ID="txtSearch" runat="server" /><cms:LocalizedButton runat="server"
        ID="btnSelect" OnClick="btnSelect_Click" EnableViewState="false" ResourceString="general.search"
        CssClass="ContentButton" />
</asp:Panel>
