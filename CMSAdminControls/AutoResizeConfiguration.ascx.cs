using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;

public partial class CMSAdminControls_AutoResizeConfiguration : CMSUserControl, ICallbackEventHandler, IFieldEditorControl
{
    /// <summary>
    /// Indicates if control is enabled.
    /// </summary>
    public bool Enabled
    {
        get
        {
            return drpSettings.Enabled;
        }
        set
        {
            this.txtWidth.Enabled = value;
            this.txtHeight.Enabled = value;
            this.txtMax.Enabled = value;
            drpSettings.Enabled = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.StopProcessing)
        {
            // Registred scripts
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AutoResize_EnableDisableForm", GetScriptEnableDisableForm());
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AutoResize_ReceiveDimensions", GetScriptReceiveDimensions());
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AutoResize_LoadSiteSettings", ScriptHelper.GetScript("function GetDimensions(txtWidthID, txtHeightID, txtMaxID){ return " + this.Page.ClientScript.GetCallbackEventReference(this, "txtWidthID + ';' + txtHeightID + ';' + txtMaxID", "ReceiveDimensions", null) + " } \n"));

            // Initialize form
            drpSettings.Attributes.Add("onchange", GetEnableDisableFormDefinition());
            EnableDisableForm();            
        }
        else
        {
            this.Visible = false;
        }
    }

    #region " Private methods "

    private string GetScriptReceiveDimensions()
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("function ReceiveDimensions(rValue, context){");
        builder.Append("var dimensions = rValue.split(\";\");");

        builder.Append("var width = (dimensions[0] > 0) ? dimensions[0]: '';");
        builder.Append("var txtWidth = document.getElementById(dimensions[3]);\n");
        builder.Append("if (txtWidth != null)\n");
        builder.Append("{ txtWidth.value = width; }\n");

        builder.Append("var height = (dimensions[1] > 0) ? dimensions[1]: '';");
        builder.Append("var txtHeight = document.getElementById(dimensions[4]);\n");
        builder.Append("if (txtHeight != null)\n");
        builder.Append("{ txtHeight.value = height; }\n");

        builder.Append("var max = (dimensions[2] > 0) ? dimensions[2]: '';");
        builder.Append("var txtMax = document.getElementById(dimensions[5]);\n");
        builder.Append("if (txtMax != null)\n");
        builder.Append("{ txtMax.value = max; }\n");

        builder.Append("txtWidth.disabled=true;\n");
        builder.Append("txtHeight.disabled=true;\n");
        builder.Append("txtMax.disabled=true;\n}");

        return ScriptHelper.GetScript(builder.ToString());
    }


    private string GetScriptEnableDisableForm()
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("function EnableDisableForm(drpSettingsID, txtWidthID, txtHeightID, txtMaxID){");
        builder.Append("var drpSettings = document.getElementById(drpSettingsID);\n");
        builder.Append("var txtWidth = document.getElementById(txtWidthID);\n");
        builder.Append("var txtHeight = document.getElementById(txtHeightID);\n");
        builder.Append("var txtMax = document.getElementById(txtMaxID);\n");
        
        builder.Append("if ((drpSettings != null) && (txtWidth != null) && (txtHeight != null) && (txtMax != null)) {\n");
        builder.Append("var disabled = (drpSettings.value != 'custom');\n");
        builder.Append("txtWidth.disabled = disabled;\n");
        builder.Append("txtHeight.disabled = disabled;\n");
        builder.Append("txtMax.disabled = disabled;\n");
        builder.Append("if (drpSettings.value == '') {\n");
        builder.Append("  GetDimensions(txtWidthID, txtHeightID, txtMaxID); }\n");        
        builder.Append("else if (drpSettings.value == 'noresize') {\n");
        builder.Append("  txtWidth.value = ''; txtHeight.value = ''; txtMax.value = '';}\n");
        builder.Append("}}\n");

        return ScriptHelper.GetScript(builder.ToString());
    }

    private string GetEnableDisableFormDefinition()
    {
        return string.Format("EnableDisableForm('{0}', '{1}', '{2}', '{3}');", 
            this.drpSettings.ClientID, 
            this.txtWidth.ClientID, 
            this.txtHeight.ClientID, 
            this.txtMax.ClientID);
    }

    private void EnableDisableForm()
    {
        ltlScript.Text = ScriptHelper.GetScript(GetEnableDisableFormDefinition());
    }


    private object GetKeyValue(Hashtable config, string key)
    {
        if (config.ContainsKey(key))
        {
            return config[key];
        }
        return null;
    }


    /// <summary>
    /// Sets inner controls according to the parameters and their values included in configuration collection. Parameters collection will be passed from Field editor
    /// </summary>    
    private void LoadConfiguration(string autoresize, int width, int height, int maxSideSize)
    {        
        switch (autoresize.ToLower())
        {
            case "noresize":
                drpSettings.SelectedValue = "noresize";
                width = 0;
                height = 0;
                maxSideSize = 0;
                break;


            // Use custom settings
            case "custom":
                drpSettings.SelectedValue = "custom";
                break;


            // Use site settings
            default:

                drpSettings.SelectedValue = "";

                string siteName = CMSContext.CurrentSiteName;
                width = ImageHelper.GetAutoResizeToWidth(siteName);
                height = ImageHelper.GetAutoResizeToHeight(siteName);
                maxSideSize = ImageHelper.GetAutoResizeToMaxSideSize(siteName);
                break;
        }

        SetDimension(txtWidth, width);
        SetDimension(txtHeight, height);
        SetDimension(txtMax, maxSideSize);

        EnableDisableForm();
    }


    /// <summary>
    /// Sets specified dimension to the specified text box
    /// </summary>
    /// <param name="txt">Textbox the dimensions should be set to.</param>
    /// <param name="dimension">Dimension to be set.</param>
    private void SetDimension(TextBox txt, int dimension)
    {
        if (dimension > 0)
        {
            txt.Text = dimension.ToString();
        }
        else
        {
            txt.Text = "";
        }
    }

    #endregion


    #region " Public methods "


    /// <summary>
    /// Sets inner controls according to the parameters and their values included in configuration collection.
    /// </summary>
    /// <param name="config">Parameters collection.</param>
    public void LoadConfiguration(Hashtable config)
    {
        int width = ValidationHelper.GetInteger(config["autoresize_width"], 0);
        int height = ValidationHelper.GetInteger(config["autoresize_height"], 0);
        int maxSideSize = ValidationHelper.GetInteger(config["autoresize_maxsidesize"], 0);
        string autoresize = ValidationHelper.GetString(config["autoresize"], "");

        LoadConfiguration(autoresize, width, height, maxSideSize);
    }


    /// <summary>
    /// Sets inner controls according to the parameters and their values included in configuration collection.
    /// </summary>
    /// <param name="config">Parameters collection.</param>
    public void LoadConfiguration(XmlData config)
    {
        int width = ValidationHelper.GetInteger(config["autoresize_width"], 0);
        int height = ValidationHelper.GetInteger(config["autoresize_height"], 0);
        int maxSideSize = ValidationHelper.GetInteger(config["autoresize_maxsidesize"], 0);
        string autoresize = ValidationHelper.GetString(config["autoresize"], "");

        LoadConfiguration(autoresize, width, height, maxSideSize);
    }


    /// <summary>
    /// Updates parameters collection of parameters and values according to the values of the inner controls. Parameters collection which should be updated will be passed from Field editor.
    /// </summary>
    /// <param name="config">Parameters collection.</param>
    public void UpdateConfiguration(Hashtable config)
    {
        config.Remove("autoresize");
        config.Remove("autoresize_width");
        config.Remove("autoresize_height");
        config.Remove("autoresize_maxsidesize");

        // Save custom settings
        if (drpSettings.SelectedValue == "custom")
        {            
            config["autoresize"] = "custom";

            if (txtWidth.Text.Trim() != "")
            {
                config["autoresize_width"] = txtWidth.Text.Trim();
            }

            if (txtHeight.Text.Trim() != "")
            {
                config["autoresize_height"] = txtHeight.Text.Trim();
            }

            if (txtMax.Text.Trim() != "")
            {
                config["autoresize_maxsidesize"] = txtMax.Text.Trim();
            }
        }
        // Save no resize settings
        else if (drpSettings.SelectedValue == "noresize")
        {
            config["autoresize"] = "noresize";
        }
    }


    /// <summary>
    /// Updates parameters collection of parameters and values according to the values of the inner controls.
    /// </summary>
    /// <param name="config">Parameters collection.</param>
    public void UpdateConfiguration(XmlData config)
    {
        config["autoresize"] = null;
        config["autoresize_width"] = null;
        config["autoresize_height"] = null;
        config["autoresize_maxsidesize"] = null;

        // Save custom settings
        if (drpSettings.SelectedValue == "custom")
        {
            config["autoresize"] = "custom";

            if (txtWidth.Text.Trim() != "")
            {
                config["autoresize_width"] = txtWidth.Text.Trim();
            }

            if (txtHeight.Text.Trim() != "")
            {
                config["autoresize_height"] = txtHeight.Text.Trim();
            }

            if (txtMax.Text.Trim() != "")
            {
                config["autoresize_maxsidesize"] = txtMax.Text.Trim();
            }
        }
        // Save no resize settings
        else if (drpSettings.SelectedValue == "noresize")
        {
            config["autoresize"] = "noresize";
        }
    }


    /// <summary>
    /// Clears inner controls - sets them to the default values.
    /// </summary>
    public void ClearControl()
    {
        LoadConfiguration(new Hashtable());      
    }


    /// <summary>
    /// Returns TRUE if input values are valid, otherwise returns FALSE and dislays error message.
    /// </summary>    
    public string Validate()
    {
        if (drpSettings.SelectedValue == "custom")
        {
            bool error = false;

            // Validate width
            if (txtWidth.Text.Trim() != "")
            {
                if (ValidationHelper.GetInteger(txtWidth.Text.Trim(), 0) <= 0)
                {
                    error = true;
                }
            }

            // Validate height
            if (!error && (txtHeight.Text.Trim() != ""))
            {
                if (ValidationHelper.GetInteger(txtHeight.Text.Trim(), 0) <= 0)
                {
                    error = true;
                }
            }
            
            // Validate max side size
            if (!error && (txtMax.Text.Trim() != ""))
            {
                if (ValidationHelper.GetInteger(txtMax.Text.Trim(), 0) <= 0)
                {
                    error = true;
                }
            }

            if (error)
            {
                // Display error message                   
                return ResHelper.GetString("dialogs.resize.wrongformat");
            }
        }
        return "";
    }

    #endregion


    #region "Callback handling"

    string dimensions = "";

    public string GetCallbackResult()
    {
        return dimensions;
    }

    public void RaiseCallbackEvent(string eventArgument)
    {
        // Get site settings
        string siteName = CMSContext.CurrentSiteName;
        int width = ImageHelper.GetAutoResizeToWidth(siteName);
        int height = ImageHelper.GetAutoResizeToHeight(siteName);
        int max = ImageHelper.GetAutoResizeToMaxSideSize(siteName);

        string [] IDs = eventArgument.Split(';');

        // Returns site settings back to the client
        dimensions = string.Format("{0};{1};{2};{3};{4};{5}", width, height, max, IDs[0], IDs[1], IDs[2]); 
    }

    #endregion
}
