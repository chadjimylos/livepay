using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Countries_Country_Edit_General : SiteManagerPage
{
    protected int countryId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblCountryDisplayName.Text = ResHelper.GetString("Country_Edit.CountryDisplayNameLabel");
        lblCountryCodeName.Text = ResHelper.GetString("Country_Edit.CountryNameLabel");
        btnOk.Text = ResHelper.GetString("general.ok");
        rfvCountryDisplayName.ErrorMessage = ResHelper.GetString("Country_Edit_General.ErrorEmptyCountryDisplayName");
        rfvCountryCodeName.ErrorMessage = ResHelper.GetString("Country_Edit_General.ErrorEmptyCountryCodeName");

        countryId = QueryHelper.GetInteger("countryId", 0);
        if (!RequestHelper.IsPostBack())
        {
            LoadData();

            if (QueryHelper.GetBoolean("saved", false))
            {
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("general.changessaved");
            }
        }
    }


    /// <summary>
    /// Handles btnOK's OnClick event - Update resource info
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // finds whether required fields are not empty
        string result = new Validator()
            .NotEmpty(txtCountryDisplayName.Text, rfvCountryDisplayName.ErrorMessage)
            .NotEmpty(txtCountryCodeName.Text, rfvCountryCodeName.ErrorMessage)
            .IsCodeName(txtCountryCodeName.Text, ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat")).Result;

        if (result == "")
        {
            CountryInfo ci = CountryInfoProvider.GetCountryInfo(txtCountryCodeName.Text.Trim());

            // country code exists
            if (ci != null)
            {
                // if country code already exists and it is just editing country -> update
                if (ci.CountryID == countryId)
                {
                    UpdateCountry();
                }
                // if country code already exists and it is another country -> error
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Country_Edit.CountryNameExists");
                }
            }
            // country code doesnt exists -> update
            else
            {
                UpdateCountry();
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }


    /// <summary>
    /// Loads data from DB into textboxes.
    /// </summary>
    protected void LoadData()
    {
        CountryInfo ci = CountryInfoProvider.GetCountryInfo(countryId);
        if (ci != null)
        {
            txtCountryDisplayName.Text = ci.CountryDisplayName;
            txtCountryCodeName.Text = ci.CountryName;
        }
    }


    /// <summary>
    /// Update country
    /// </summary>
    protected void UpdateCountry()
    {
        CountryInfo ci = CountryInfoProvider.GetCountryInfo(countryId);
        if (ci != null)
        {
            ci.CountryDisplayName = txtCountryDisplayName.Text.Trim();
            ci.CountryName = txtCountryCodeName.Text;
            CountryInfoProvider.SetCountryInfo(ci);

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("general.changessaved");
        }
    }
}
