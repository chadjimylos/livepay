CREATE TABLE [COM_TaxClassCountry] (
		[TaxClassID]      int NOT NULL,
		[CountryID]       int NOT NULL,
		[TaxValue]        float NOT NULL,
		[IsFlatValue]     bit NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_TaxClassCountry]
	ADD
	CONSTRAINT [PK_COM_TaxClassCountry]
	PRIMARY KEY
	([TaxClassID], [CountryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_TaxClassCountry]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Country]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_TaxCategoryCountry_CountryID_CMS_Country]') IS NULL
BEGIN
		ALTER TABLE [COM_TaxClassCountry]
			ADD CONSTRAINT [FK_COM_TaxCategoryCountry_CountryID_CMS_Country]
			FOREIGN KEY ([CountryID]) REFERENCES [CMS_Country] ([CountryID])
END
IF OBJECT_ID(N'[COM_TaxClassCountry]') IS NOT NULL
	AND OBJECT_ID(N'[COM_TaxClass]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_TaxCategoryCountry_TaxClassID_COM_TaxClass]') IS NULL
BEGIN
		ALTER TABLE [COM_TaxClassCountry]
			ADD CONSTRAINT [FK_COM_TaxCategoryCountry_TaxClassID_COM_TaxClass]
			FOREIGN KEY ([TaxClassID]) REFERENCES [COM_TaxClass] ([TaxClassID])
END
