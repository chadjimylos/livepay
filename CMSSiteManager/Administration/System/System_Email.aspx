<%@ Page Language="C#" AutoEventWireup="true" CodeFile="System_Email.aspx.cs" Inherits="CMSSiteManager_Administration_System_System_Email"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Administration - System - Email" %>

<%@ Register Src="~/CMSAdminControls/MetaFiles/File.ascx" TagName="File" TagPrefix="uc1" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblServer" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtServer" runat="server" CssClass="TextBoxField" />
                <asp:RequiredFieldValidator ID="rfvServer" runat="server" ControlToValidate="txtServer"
                    Display="dynamic" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblUserName" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtUserName" runat="server" CssClass="TextBoxField" MaxLength="200" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblPassword" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtPassword" runat="server" CssClass="TextBoxField" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblEncoding" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtEncoding" runat="server" CssClass="TextBoxField" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblSSL" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkSSL" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblFrom" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtFrom" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ControlToValidate="txtFrom"
                    Display="dynamic" EnableViewState="false" />
                <asp:RegularExpressionValidator ID="revFrom" runat="server" ControlToValidate="txtFrom"
                    Display="dynamic" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblTo" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtTo" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvTo" runat="server" ControlToValidate="txtTo" Display="dynamic"
                    EnableViewState="false" />
                <asp:RegularExpressionValidator ID="revTo" runat="server" ControlToValidate="txtTo"
                    Display="dynamic" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblSubject" runat="server" EnableViewState="false" ResourceString="general.subject"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtSubject" runat="server" CssClass="TextBoxField" MaxLength="450" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblText" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtText" runat="server" CssClass="TextAreaField" TextMode="MultiLine" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblAttachment" runat="server" EnableViewState="false" />
            </td>
            <td>
                <cms:Uploader ID="FileUploader" runat="server" BorderStyle="none" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <cms:CMSButton ID="btnSend" runat="server" CssClass="SubmitButton" OnClick="btnSend_Click"
                    EnableViewState="false" />
            </td>
        </tr>
    </table>
</asp:Content>
