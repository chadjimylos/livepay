<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageEdit.aspx.cs" Inherits="CMSModules_Content_CMSDesk_MasterPage_PageEdit"
    Theme="Default" ValidateRequest="false" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Master page - page edit" %>

<%@ Register Src="~/CMSFormControls/Inputs/LargeTextArea.ascx" TagName="LargeTextArea" TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" runat="server">

    <script type="text/javascript">
        //<![CDATA[
        parent.parent.frames['contenteditheader'].SetTabsContext('master');
        //]]>
    </script>

    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    <cms:CMSButton ID="btnSave" runat="server" CssClass="HiddenButton" EnableViewState="false"
        OnClick="btnSave_Click" />
    <table cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td style="">
                <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" Visible="false" EnableViewState="false" />
                <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
                <asp:TextBox runat="server" ID="txtDocType" Width="90%" TextMode="MultiLine" Rows="2" /><br />
                <asp:Label runat="server" ID="lblAfterDocType" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <%=mHead%>
                <div style="padding-left: 30px; margin-top: 5px; margin-bottom: 5px;">
                    <cms:LargeTextArea ID="txtHeadTags" runat="server" Width="90%" Rows="2" AllowMacros="false" />
                </div>
                <asp:Label runat="server" ID="lblAfterHeadTags" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblBodyStart" EnableViewState="false" />
                <asp:TextBox runat="server" ID="txtBodyCss" Width="60%" />
                <asp:Label runat="server" ID="lblBodyEnd" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <%=mBeforeLayout%>
                <asp:Label runat="server" ID="lblChecked" Visible="false" CssClass="ErrorLabel" EnableViewState="false" />
                <div style="padding-left: 30px; margin-top: 5px; margin-bottom: 5px;">
                    <cms:ExtendedTextArea runat="server" ID="txtLayout" CssClass="PagePlaceholderDesignLayoutCode"
                        TextMode="MultiLine" Width="90%" Height="300"></cms:ExtendedTextArea>
                </div>
                <asp:Label runat="server" ID="lblAfterLayout" EnableViewState="false" />
                <%=mAfterLayout%>
                <br />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
