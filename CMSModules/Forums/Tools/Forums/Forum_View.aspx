<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Forum_View.aspx.cs" Inherits="CMSModules_Forums_Tools_Forums_Forum_View"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" ValidateRequest="false" %>

<%@ Register Src="~/CMSModules/Forums/Controls/ForumDivider.ascx" TagName="ForumFlatView"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:ScriptManager runat="server" ID="manScript" />
    <cms:ForumFlatView ID="ForumFlatView1" runat="server" RedirectToUserProfile="false"  />
</asp:Content>
