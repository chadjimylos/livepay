<%@ Control Language="C#" AutoEventWireup="true" CodeFile="media_library.ascx.cs"
    Inherits="CMSModules_ImportExport_Controls_Import_media_library" %>

<script type="text/javascript">
    //<![CDATA[
    function medCheckChange() {
        medChck2.disabled = !medChck1.checked;
    }

    function medInitCheckboxes() {
        if (!medChck1.checked) {
            medChck2.disabled = true;
        }
    }
    //]]>
</script>

<asp:Panel runat="server" ID="pnlCheck" CssClass="WizardHeaderLine" BackColor="transparent">
    <asp:PlaceHolder ID="plcCheck" runat="Server">
        <div>
            <asp:CheckBox ID="chkFiles" runat="server" />
        </div>
        <div>
            <asp:CheckBox ID="chkPhysicalFiles" runat="server" />
        </div>
    </asp:PlaceHolder>
</asp:Panel>
<asp:Literal ID="ltlScript" EnableViewState="false" runat="Server" />