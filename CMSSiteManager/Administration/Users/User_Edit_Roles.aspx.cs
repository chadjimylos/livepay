using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSSiteManager_Administration_Users_User_Edit_Roles : CMSUsersPage
{
    #region "Protected variables"

    protected int siteId = 0;
    protected int userId = 0;
    protected UserInfo ui = null;
    protected string currentValues = string.Empty;

    #endregion


    #region "Events"

    /// <summary>
    /// Page_load event.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get user id and site Id from query
        userId = QueryHelper.GetInteger("userid", 0);
        siteId = QueryHelper.GetInteger("siteid", 0);

        if (siteId != 0 && !CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            plcSelectSite.Visible = false;
        }

        if (userId != 0)
        {
            // Check that only global administrator can edit global administrator's accouns
            ui = UserInfoProvider.GetUserInfo(userId);

            if (!CheckGlobalAdminEdit(ui))
            {
                plcTable.Visible = false;
                lblErrorDeskAdmin.Text = ResHelper.GetString("Administration-User_List.ErrorGlobalAdmin");
                lblErrorDeskAdmin.Visible = true;
                return;
            }


            // Set site selector
            siteSelector.DropDownSingleSelect.AutoPostBack = true;
            siteSelector.AllowAll = false;
            siteSelector.AllowEmpty = false;
            // Only sites assigned to user
            siteSelector.UserId = userId;
            siteSelector.OnlyRunningSites = false;
            siteSelector.UniSelector.OnSelectionChanged += new EventHandler(UniSelector_OnSelectionChanged);

            if (!RequestHelper.IsPostBack())
            {
                siteId = CMSContext.CurrentSiteID;

                // If user is member of current site
                if (UserSiteInfoProvider.GetUserSiteInfo(userId, siteId) != null)
                {
                    // Force uniselector to preselect current site
                    siteSelector.Value = siteId;
                }

                // Force to load data
                siteSelector.UpdateWhereCondition();
                siteSelector.Reload(true);
            }

            // Get truly selected item
            siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
        }

        usRoles.OnSelectionChanged += new EventHandler(usRoles_OnSelectionChanged);
        usRoles.WhereCondition = "SiteID =" + siteId + " AND RoleGroupID IS NULL";

        // Exclude generic roles
        string genericWhere = null;
        ArrayList genericRoles = RoleInfoProvider.GetGenericRoles();
        if (genericRoles.Count != 0)
        {
            foreach (string role in genericRoles)
            {
                genericWhere += "'" + role.Replace("'", "''") + "',";
            }

            genericWhere = genericWhere.TrimEnd(',');
            usRoles.WhereCondition += " AND ( RoleName NOT IN (" + genericWhere + ") )";
        }

        // Get the active roles for this site
        DataSet ds = UserRoleInfoProvider.GetUserRoles("RoleID", "UserID = " + userId + "AND RoleID IN (SELECT RoleID FROM CMS_Role WHERE SiteID = " + siteId + ")", null, 0, null);
        if (!DataHelper.DataSourceIsEmpty(ds))
        {
            currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "RoleID"));
        }


        // If not postback or site selection changed
        if (!RequestHelper.IsPostBack() || (siteId != Convert.ToInt32(ViewState["rolesOldSiteId"])))
        {
            // Set values
            usRoles.Value = currentValues;
        }

        // Store selected site id
        ViewState["rolesOldSiteId"] = siteId;
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        // Display message if user has no site
        if (!siteSelector.UniSelector.HasData)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Administration-User_Edit_Roles.UserWithNoSites");
        }
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        this.pnlUpdate.Update();
    }


    protected void usRoles_OnSelectionChanged(object sender, EventArgs e)
    {
        SaveData();
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Check whether current user is allowed to modify another user.
    /// </summary>
    /// <param name="userId">Modified user</param>
    /// <returns>"" or error message.</returns>
    protected static string ValidateGlobalAndDeskAdmin(UserInfo ui)
    {
        string result = String.Empty;

        if (CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            return result;
        }

        if (ui == null)
        {
            result = ResHelper.GetString("Administration-User.WrongUserId");
        }
        else
        {
            if (ui.IsGlobalAdministrator)
            {
                result = ResHelper.GetString("Administration-User.NotAllowedToModify");
            }
        }
        return result;
    }


    /// <summary>
    /// Saves data.
    /// </summary>
    private void SaveData()
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify"))
        {
            RedirectToAccessDenied("CMS.Users", "Modify");
        }

        string result = ValidateGlobalAndDeskAdmin(ui);
        if (result != String.Empty)
        {
            lblErrorDeskAdmin.Visible = true;
            lblErrorDeskAdmin.Text = result;
            return;
        }

        // Remove old items
        string newValues = ValidationHelper.GetString(usRoles.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int roleID = ValidationHelper.GetInteger(item, 0);
                    UserRoleInfoProvider.RemoveUserFromRole(userId, roleID);
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int roleID = ValidationHelper.GetInteger(item, 0);
                    UserRoleInfoProvider.AddUserToRole(userId, roleID);
                }
            }
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }

    #endregion
}
