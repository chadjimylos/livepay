<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportTable_Edit.aspx.cs"
    Inherits="CMSModules_Reporting_Tools_ReportTable_Edit" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master"
    Title="ReportTable Edit" ValidateRequest="false" EnableEventValidation="false" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <div class="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <table style="vertical-align: top; width: 100%;">
            <asp:Panel runat="server" ID="pnlElements">
                <tr>
                    <td class="FieldLabel">
                        <cms:LocalizedLabel ID="lblDisplayName" runat="server" ResourceString="general.displayname"
                            DisplayColon="true" AssociatedControlID="txtDisplayName" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtDisplayName" MaxLength="450" runat="server" CssClass="TextBoxField" />
                        <br />
                        <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <cms:LocalizedLabel ID="lblCodeName" runat="server" ResourceString="general.codename"
                            DisplayColon="true" AssociatedControlID="txtCodeName" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtCodeName" MaxLength="100" runat="server" CssClass="TextBoxField" />
                        <br />
                        <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtCodeName"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <asp:Label ID="lblQuery" runat="server" AssociatedControlID="txtQuery" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtQuery" TextMode="MultiLine" runat="server" CssClass="TextAreaLarge" />
                        <br />
                        <asp:RequiredFieldValidator ID="rfvQuery" runat="server" ControlToValidate="txtQuery"
                            Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <asp:Label ID="lblStoredProcedure" runat="server" AssociatedControlID="chkStoredProcedure" />
                    </td>
                    <td>
                        <asp:CheckBox ID="chkStoredProcedure" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <asp:Label ID="lblSkinID" runat="server" AssociatedControlID="txtSkinID" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtSkinID" runat="server" CssClass="TextBoxField" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <cms:LocalizedLabel ID="lblEnablePaging" runat="server" AssociatedControlID="chkEnablePaging"
                            ResourceString="Reporting_ReportTable_Edit.lblEnablePaging" DisplayColon="true" />                        
                    </td>
                    <td>
                        <asp:CheckBox ID="chkEnablePaging" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <cms:LocalizedLabel ID="lblPageSize" runat="server" AssociatedControlID="txtPageSize"
                            ResourceString="Reporting_ReportTable_Edit.lblPageSize" DisplayColon="true" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtPageSize" runat="server" Text="10" CssClass="TextBoxField" />
                    </td>
                </tr>
                <tr>
                    <td>                    
                        <cms:LocalizedLabel ID="lblPageMode" runat="server" AssociatedControlID="drpPageMode"
                            ResourceString="Reporting_ReportTable_Edit.lblPageMode" DisplayColon="true" />
                    </td>
                    <td>
                        <asp:DropDownList ID="drpPageMode" runat="server" CssClass="DropDownField" />
                    </td>
                </tr>
            </asp:Panel>
        </table>
        <asp:HiddenField ID="txtNewTableHidden" runat="server" />
        <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
    </div>
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click" /><cms:CMSButton
            ID="btnCancel" runat="server" OnClientClick="wopener.ReloadPage(); window.close(); return false;"
            CssClass="SubmitButton" /><cms:CMSButton ID="btnPreview" runat="server" CssClass="LongSubmitButton"
                OnClick="btnPreview_Click" />
    </div>
</asp:Content>
