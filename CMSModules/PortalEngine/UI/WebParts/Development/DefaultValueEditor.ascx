<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DefaultValueEditor.ascx.cs"
    Inherits="CMSModules_PortalEngine_UI_WebParts_Development_DefaultValueEditor" %>
<asp:Panel runat="server" ID="pnlEditor" CssClass="DefaultValueEditorPanel" />
<br />
<div class="TextRight">
    <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOk_Click" CssClass="SubmitButton" />
</div>
