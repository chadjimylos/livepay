<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WidgetProperties_Buttons.aspx.cs"
    Inherits="CMSModules_Widgets_Dialogs_WidgetProperties_Buttons" Theme="default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Widget properties - Buttons</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
            background-color: #f5f3ec;
        }
    </style>
</head>
<body class="<%=mBodyClass%> Buttons">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="pnlScroll" CssClass="PageFooterLine">
        <div class="FloatLeft">
            <asp:CheckBox runat="server" ID="chkRefresh" Checked="true" />
        </div>
        <div class="FloatRight">
            <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" /><cms:CMSButton
                ID="btnCancel" runat="server" CssClass="SubmitButton" /><cms:CMSButton ID="btnApply"
                    runat="server" CssClass="SubmitButton" />
        </div>
    </asp:Panel>
    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
    </form>

    <script type="text/javascript">
        //<![CDATA[
        function Apply() {
            parent.frames['widgetpropertiescontent'].OnApplyButton(GetRefreshStatus());
        }

        function Save() {
            parent.frames['widgetpropertiescontent'].OnOKButton(GetRefreshStatus());
        }

        function Close() {
            top.window.close();
        }
        //]]>
    </script>

</body>
</html>
