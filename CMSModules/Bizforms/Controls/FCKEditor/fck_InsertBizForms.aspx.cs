using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.FormEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.PortalEngine;

public partial class CMSModules_BizForms_Controls_FCKEditor_fck_InsertBizForms : CMSPage
{
    private bool reload = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        SetBrowserClass();
        bodyElem.Attributes["class"] = mBodyClass;

        uniGrid.Pager.DefaultPageSize = 10;
        uniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(uniGrid_OnExternalDataBound);
        uniGrid.OnPageChanged += new EventHandler<EventArgs>(uniGrid_OnPageChanged);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
        {
            // Register custom css if exists
            RegisterDialogCSSLink();
        }

        if (reload)
        {
            this.ReloadData(uniGrid.GridView.PageIndex);
        }
    }


    void uniGrid_OnPageChanged(object sender, EventArgs e)
    {
        this.ReloadData(uniGrid.Pager.CurrentPage - 1);
        reload = false;
    }


    /// <summary>
    /// Databound handler.
    /// </summary>
    object uniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "formdisplayname":
                HyperLink link = new HyperLink();
                link.NavigateUrl = "#";

                // Get DataRowView
                DataRowView drv = GetDataRowView(sender as DataControlFieldCell);

                link.Text = HTMLHelper.HTMLEncode(ValidationHelper.GetString(drv.Row["FormDisplayName"], null));
                link.ToolTip = HTMLHelper.HTMLEncode(ValidationHelper.GetString(drv.Row["FormDisplayName"], null));

                // Prepare script for hyperlink               
                string script = "javascript:insertBizForms(\"%%control:BizFormControl?" + ValidationHelper.GetString(drv.Row["FormName"], null) + "%%\");";

                link.Attributes.Add("onclick", script);
                return link;
        }
        return null;
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }


    //<summary>
    //Reloads list of available bizforms.
    //</summary>
    protected void ReloadData(int newIndex)
    {
        int topN = uniGrid.GridView.PageSize * (newIndex + 1 + uniGrid.GridView.PagerSettings.PageButtonCount);
        string orderBy = String.IsNullOrEmpty(uniGrid.SortDirect) ? "FormDisplayName" : uniGrid.SortDirect;

        uniGrid.DataSource = BizFormInfoProvider.GetBizFormsForSite(CMSContext.CurrentSiteID, uniGrid.WhereClause, orderBy, topN, "FormID, FormName, FormDisplayName");
        uniGrid.ReloadData();
    }
}

