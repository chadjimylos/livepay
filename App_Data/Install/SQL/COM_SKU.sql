CREATE TABLE [COM_SKU] (
		[SKUID]                    int NOT NULL IDENTITY(1, 1),
		[SKUNumber]                nvarchar(200) NULL,
		[SKUName]                  nvarchar(440) NOT NULL,
		[SKUDescription]           nvarchar(max) NULL,
		[SKUPrice]                 float NOT NULL,
		[SKUEnabled]               bit NOT NULL,
		[SKUDepartmentID]          int NOT NULL,
		[SKUManufacturerID]        int NULL,
		[SKUInternalStatusID]      int NULL,
		[SKUPublicStatusID]        int NULL,
		[SKUSupplierID]            int NULL,
		[SKUAvailableInDays]       int NULL,
		[SKUGUID]                  uniqueidentifier NOT NULL,
		[SKUImagePath]             nvarchar(450) NULL,
		[SKUWeight]                float NULL,
		[SKUWidth]                 float NULL,
		[SKUDepth]                 float NULL,
		[SKUHeight]                float NULL,
		[SKUAvailableItems]        int NULL,
		[SKUSellOnlyAvailable]     bit NULL,
		[SKUCustomData]            nvarchar(max) NULL,
		[SKUOptionCategoryID]      int NULL,
		[SKUOrder]                 int NULL,
		[SKULastModified]          datetime NOT NULL,
		[SKUCreated]               datetime NULL
)
ON [PRIMARY]
ALTER TABLE [COM_SKU]
	ADD
	CONSTRAINT [PK_COM_SKU]
	PRIMARY KEY
	NONCLUSTERED
	([SKUID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [COM_SKU]
	ADD
	CONSTRAINT [DEFAULT_COM_SKU_SKUName]
	DEFAULT ('') FOR [SKUName]
ALTER TABLE [COM_SKU]
	ADD
	CONSTRAINT [DEFAULT_COM_SKU_SKUSellOnlyAvailable]
	DEFAULT ((0)) FOR [SKUSellOnlyAvailable]
IF OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Department]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_SKU_SKUDepartmentID_COM_Department]') IS NULL
BEGIN
		ALTER TABLE [COM_SKU]
			ADD CONSTRAINT [FK_COM_SKU_SKUDepartmentID_COM_Department]
			FOREIGN KEY ([SKUDepartmentID]) REFERENCES [COM_Department] ([DepartmentID])
END
IF OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[COM_InternalStatus]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_SKU_SKUInternalStatusID_COM_InternalStatus]') IS NULL
BEGIN
		ALTER TABLE [COM_SKU]
			ADD CONSTRAINT [FK_COM_SKU_SKUInternalStatusID_COM_InternalStatus]
			FOREIGN KEY ([SKUInternalStatusID]) REFERENCES [COM_InternalStatus] ([InternalStatusID])
END
IF OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Manufacturer]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_SKU_SKUManufacturerID_COM_Manifacturer]') IS NULL
BEGIN
		ALTER TABLE [COM_SKU]
			ADD CONSTRAINT [FK_COM_SKU_SKUManufacturerID_COM_Manifacturer]
			FOREIGN KEY ([SKUManufacturerID]) REFERENCES [COM_Manufacturer] ([ManufacturerID])
END
IF OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[COM_OptionCategory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_SKU_SKUOptionCategoryID_COM_OptionCategory]') IS NULL
BEGIN
		ALTER TABLE [COM_SKU]
			ADD CONSTRAINT [FK_COM_SKU_SKUOptionCategoryID_COM_OptionCategory]
			FOREIGN KEY ([SKUOptionCategoryID]) REFERENCES [COM_OptionCategory] ([CategoryID])
END
IF OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[COM_PublicStatus]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_SKU_SKUPublicStatusID_COM_PublicStatus]') IS NULL
BEGIN
		ALTER TABLE [COM_SKU]
			ADD CONSTRAINT [FK_COM_SKU_SKUPublicStatusID_COM_PublicStatus]
			FOREIGN KEY ([SKUPublicStatusID]) REFERENCES [COM_PublicStatus] ([PublicStatusID])
END
IF OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Supplier]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_SKU_SKUSupplierID_COM_Supplier]') IS NULL
BEGIN
		ALTER TABLE [COM_SKU]
			ADD CONSTRAINT [FK_COM_SKU_SKUSupplierID_COM_Supplier]
			FOREIGN KEY ([SKUSupplierID]) REFERENCES [COM_Supplier] ([SupplierID])
END
CREATE INDEX [IX_COM_SKU_SKUDepartmentID]
	ON [COM_SKU] ([SKUDepartmentID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_SKU_SKUEnabled_SKUAvailableItems]
	ON [COM_SKU] ([SKUEnabled], [SKUAvailableItems])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_SKU_SKUInternalStatusID]
	ON [COM_SKU] ([SKUInternalStatusID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_SKU_SKUManufacturerID]
	ON [COM_SKU] ([SKUManufacturerID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_SKU_SKUName_SKUEnabled]
	ON [COM_SKU] ([SKUName], [SKUEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_SKU_SKUOptionCategoryID]
	ON [COM_SKU] ([SKUOptionCategoryID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_SKU_SKUPrice]
	ON [COM_SKU] ([SKUPrice])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_SKU_SKUPublicStatusID]
	ON [COM_SKU] ([SKUPublicStatusID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_SKU_SKUSupplierID]
	ON [COM_SKU] ([SKUSupplierID])
	ON [PRIMARY]
