CREATE TABLE [Reporting_ReportGraph] (
		[GraphID]                         int NOT NULL IDENTITY(1, 1),
		[GraphName]                       nvarchar(100) NOT NULL,
		[GraphDisplayName]                nvarchar(450) NOT NULL,
		[GraphQuery]                      nvarchar(max) NOT NULL,
		[GraphQueryIsStoredProcedure]     bit NOT NULL,
		[GraphType]                       nvarchar(50) NOT NULL,
		[GraphReportID]                   int NOT NULL,
		[GraphTitle]                      nvarchar(200) NULL,
		[GraphXAxisTitle]                 nvarchar(200) NULL,
		[GraphYAxisTitle]                 nvarchar(200) NULL,
		[GraphWidth]                      int NULL,
		[GraphHeight]                     int NULL,
		[GraphLegendPosition]             int NULL,
		[GraphSettings]                   nvarchar(max) NULL,
		[GraphGUID]                       uniqueidentifier NOT NULL,
		[GraphLastModified]               datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Reporting_ReportGraph]
	ADD
	CONSTRAINT [PK_Reporting_ReportGraph]
	PRIMARY KEY
	([GraphID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Reporting_ReportGraph]') IS NOT NULL
	AND OBJECT_ID(N'[Reporting_Report]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Reporting_ReportGraph_GraphReportID_Reporting_Report]') IS NULL
BEGIN
		ALTER TABLE [Reporting_ReportGraph]
			ADD CONSTRAINT [FK_Reporting_ReportGraph_GraphReportID_Reporting_Report]
			FOREIGN KEY ([GraphReportID]) REFERENCES [Reporting_Report] ([ReportID])
END
CREATE UNIQUE INDEX [IX_Reporting_ReportGraph_GraphGUID]
	ON [Reporting_ReportGraph] ([GraphGUID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE UNIQUE INDEX [IX_Reporting_ReportGraph_GraphReportID_GraphName]
	ON [Reporting_ReportGraph] ([GraphReportID], [GraphName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
