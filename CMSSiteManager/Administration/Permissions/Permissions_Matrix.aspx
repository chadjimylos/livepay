<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Permissions_Matrix.aspx.cs"
    Inherits="CMSSiteManager_Administration_Permissions_Permissions_Matrix" Theme="Default"
    MaintainScrollPositionOnPostback="true" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Permissions - Permission Matrix" %>

<%@ Register Src="~/CMSAdminControls/UI/UniControls/UniMatrix.ascx" TagName="UniMatrix" TagPrefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <asp:Panel ID="pnlMatrix" runat="server" CssClass="PageContent">
        <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" />
        <cms:UniMatrix ID="gridMatrix" runat="server" QueryName="cms.permission.getpermissionMatrix" RowItemIDColumn="RoleID" ColumnItemIDColumn="PermissionID" RowItemDisplayNameColumn="RoleDisplayName" ColumnItemDisplayNameColumn="PermissionDisplayName" RowTooltipColumn="RowDisplayName" ColumnTooltipColumn="PermissionDescription" ItemTooltipColumn="PermissionDescription" />
    </asp:Panel>
</asp:Content>
