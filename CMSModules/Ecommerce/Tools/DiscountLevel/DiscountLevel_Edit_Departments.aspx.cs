using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Ecommerce;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Tools_DiscountLevel_DiscountLevel_Edit_Departments : CMSEcommercePage
{
    protected int discountLevelId = 0;
    protected string currentValues = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblAvialable.Text = ResHelper.GetString("com.discountlevel.departmentsavailable");
        discountLevelId = QueryHelper.GetInteger("discountLevelId", 0);
        if (discountLevelId > 0)
        {
            // Get the active departments
            DataSet ds = DiscountLevelInfoProvider.GetDepartments(discountLevelId);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "DepartmentID"));
            }

            if (!IsPostBack)
            {
                this.uniSelector.Value = currentValues;
            }
        }

        this.uniSelector.IconPath = GetObjectIconUrl("ecommerce.department", "object.png");
        this.uniSelector.OnSelectionChanged += uniSelector_OnSelectionChanged;
    }


    protected void uniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        SaveItems();
    }


    protected void SaveItems()
    {
        // Remove old items
        string newValues = ValidationHelper.GetString(uniSelector.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to department
                foreach (string item in newItems)
                {
                    int departmentId = ValidationHelper.GetInteger(item, 0);
                    DiscountLevelDepartmentInfoProvider.RemoveDiscountLevelFromDepartment(discountLevelId, departmentId);
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to department
                foreach (string item in newItems)
                {
                    int departmentId = ValidationHelper.GetInteger(item, 0);
                    DiscountLevelDepartmentInfoProvider.AddDiscountLevelToDepartment(discountLevelId, departmentId);
                }
            }
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}
