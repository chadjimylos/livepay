<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyMessages_Frameset.aspx.cs"
    Inherits="CMSModules_Messaging_MyMessages_MyMessages_Frameset" Title="Untitled Page"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>My desk - My messages</title>
</head>
<frameset border="0" rows="72, *" id="rowsFrameset">
    <frame name="messagesMenu" src="MyMessages_Header.aspx?userid=<%=userId%>" frameborder="0" scrolling="no" noresize="noresize" />
    <frame name="messagesContent" src="MyMessages_Inbox.aspx?userid=<%=userId%>" frameborder="0" />
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
</frameset>
</html>
