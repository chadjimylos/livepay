CREATE PROCEDURE Proc_BadWords_Word_RemoveDependencies
@WordID int
AS
BEGIN
DELETE FROM [BadWords_WordCulture] WHERE WordID = @WordID;
END
