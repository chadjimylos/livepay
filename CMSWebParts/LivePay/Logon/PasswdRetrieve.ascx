<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/LivePay/Logon/PasswdRetrieve.ascx.cs" Inherits="CMSWebParts_LivePay_Logon_PasswdRetrieve" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cms" %>
<style>
.passwdret-rieval
{
    width:670px;
}
.passwdret-rieval .label
{
    float:left;
    padding-right:10px;
    line-height:22px;
}
.passwdret-rieval .text
{
    float:left;
}

.button
{
    background:transparent url(/App_Themes/LivePay/Contact/btnSend.png) no-repeat;
    width:80px;
    height:23px;
    border:0px;
    cursor:hand;
    border-top-width:0px;
    border-bottom-width:0px;
}

.buttonen
{
    background:transparent url(/App_Themes/LivePay/Contact/btnSend_en-us.png) no-repeat;
    width:80px;
    height:23px;
    border:0px;
    cursor:hand;
    border-top-width:0px;
    border-bottom-width:0px;
}
</style>
<asp:Panel ID="Panel1" runat="server">
    <div class="CUDDarkBlueBGTitle">
        <div class="SvdCardsTopTitle"><asp:Literal ID="litTitle" runat="server" EnableViewState="false" /></div>
        <div class="CUDContentBG">
            <div style="line-height: 25px; padding-left: 15px; font-family: Tahoma; color: rgb(0, 0, 0); font-size: 12px; vertical-align: middle;" id="DivInsertAns"><asp:Literal ID="litText" runat="server" EnableViewState="false" /></div>
        </div>
        <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" alt="" /></div>
    </div>

    <div class="logon-form" style="width:670px;background-color:#f1f1f1;padding-top:0px;font-size:12px;min-height:130px">
        <div style="padding:15px 0px 10px 15px">
                <asp:Panel ID="pnlPasswdRetrieval" runat="server" DefaultButton="btnPasswdRetrieval">

                    <div class="passwdret-rieval">
                        <div class="label" style="line-height:15px" id="divPasswdRetrievalHeader" runat="server">
                         <asp:Label ID="lblPasswdRetrieval" runat="server" EnableViewState="false" AssociatedControlID="txtPasswordRetrieval" />
                                <br /><br />
                                <span style="color:Black;font:normal 12px Tahoma">
                                <asp:label ID="lblPasswdRetrivalCaptcha" runat="server">&nbsp;</asp:label>
                                </span>
                                
                        </div>

                        <div class="text" id="EmailDiv" runat="server" style="line-height:5px;">
                                <div><asp:TextBox ID="txtPasswordRetrieval" runat="server" /><font  style="color:#094595;font-weight:bold">*</font><a id="UserNameMsg" href="#"></a></div><br />
                                <div class="HideIt"><asp:CustomValidator ID="ReqFldVal_UserName" ClientValidationFunction="PassRetr_validateControls" ValidateEmptyText="true" IsUserName="yes" runat="server" ControlToValidate="txtPasswordRetrieval" text="*" ValidationGroup="PsswdRetrieval"/></div> 
                               <div class="PT5" style="padding-bottom:5px;margin-left:-77px;background-image:url('/app_themes/LivePay/bgborder.png');background-repeat:repeat-x;height:3px;width:665px;font-size:1px">&nbsp;</div>
                                <asp:UpdatePanel runat="server" ID="RegUpdatePanel" UpdateMode="Always">
                                    <ContentTemplate>
                                    <div style="float:left"><cms:CaptchaControl ID="scCaptcha" runat="server" BorderWidth="1px" BorderColor="#3b6db4" BorderStyle="Solid"
	                                    CaptchaBackgroundNoise="None"
	                                    CaptchaLength="5" 
	                                    CaptchaHeight="58"
	                                    CaptchaWidth="236"
	                                    Width="236"
	                                    CaptchaLineNoise="Extreme" 
	                                    CacheStrategy="HttpRuntime"
	                                    CaptchaMaxTimeout="240" 
                                    /></div>
                                    <div style="float:left">&nbsp;<asp:LinkButton style="color:#094595" ID="lnkRefresh" runat="server" ></asp:LinkButton></div>
                                    <div class="Clear"></div>
                                    <br />
                                    </ContentTemplate>
                                </asp:UpdatePanel><br /><br />
                                <font color="#094595"><%=ResHelper.LocalizeString("{$=�������� ���� �������� ���������� |en-us=Enter security code$}")%></font><br /><br /><br />
                                <div><asp:TextBox ID="txtCaptcha" runat="server" /><font  style="color:#094595;font-weight:bold">*</font><a id="CaptchaMsg" href="#"></a></div>
                                <div class="HideIt"><asp:CustomValidator ID="CustomValidator1" ClientValidationFunction="PassRetr_validateControls" ValidateEmptyText="true" runat="server" IsCaptcha="yes" ControlToValidate="txtCaptcha" text="*" ValidationGroup="PsswdRetrieval"/></div> 
                                <div  style="padding-bottom:5px;margin-left:-77px;background-image:url('/app_themes/LivePay/bgborder.png');background-repeat:repeat-x;height:3px;width:665px;font-size:1px">&nbsp;</div>
                                <div class="REgUsersBottomMust" style="padding-bottom:12px">
                                    <%= ResHelper.LocalizeString("{$=�� ����� �� ��� ��������� (*) ����� �����������|en-us=Fields marked as (*) are obligatory$}")%>
                                </div>
                                <div class="PT5" style="padding-bottom:5px;margin-left:-77px;background-image:url('/app_themes/LivePay/bgborder.png');background-repeat:repeat-x;height:3px;width:665px;font-size:1px">&nbsp;</div><br />
                                <cms:CMSButton ID="btnPasswdRetrieval" runat="server" CssClass="button" ValidationGroup="PsswdRetrieval" EnableViewState="false" Text="23333" /><br /><br /><br />
                                <div style="width:300px" id="lblErrorDIV" runat="server"><asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="false" /></div>
                                <div class="PT5" style="padding-bottom:5px;margin-left:-77px;background-image:url('/app_themes/LivePay/bgborder.png');background-repeat:repeat-x;height:3px;width:665px;font-size:1px">&nbsp;</div>
                                
                              
                                <br /><br /><br />
                                 
                        </div>
                        <div class="Clear"></div>

                        <div class="text" style="display:none;color:black" id="QuestDiv" runat="server">
                            <div id="QuestAnsDescr" runat="server" visible="false" style="padding-bottom:10px;font-weight:bold"><%=ResHelper.LocalizeString("{$=��������� ����� ���� ������� ���������|en-us=Please provide with the correct answer to the security question$}")%></div> 
                            <div class="Clear"></div>
                            <div class="FLEFT" style="color:Black"><%=ResHelper.LocalizeString("{$=������� ���������|en-us=Security question$}")%>:</div>
                            <div class="FLEFT" style="color:Black;padding-left:10px;font-weight:bold" id="quest" runat="server" ></div>
                            <div class="Clear"></div>
                            <div style="padding-top:5px">
                                <div class="FLEFT" style="color:Black"><%=ResHelper.LocalizeString("{$=��������|en-us=Answer$}")%>:</div>
                                <div class="FLEFT" style="padding-left:10px;">
                                    <asp:TextBox  Width="300px" ID="txtAnswer" runat="server"></asp:TextBox>
                                </div>
                                <div class="Clear"></div>
                                <div style="padding-left:70px;padding-top:5px"><cms:CMSButton ID="CMSBtnSendAns" runat="server" CssClass="button" EnableViewState="false"  />&nbsp;<asp:Label ID="lblEroorAns" runat="server" Visible="false" EnableViewState="false" /></div>
                            </div>
                        </div> 
                    </div>
                    <asp:Label ID="lblResult" runat="server" Visible="false" EnableViewState="false" />
                </asp:Panel>
                <asp:Panel ID="pnlNoUserQuestions" runat="server" Visible="false">
                    <span style="color:Black;font:normal 12px Tahoma">
                        <asp:Literal ID="litErrorMsg" runat="server" />
                    </span>
                </asp:Panel>
        </div>
    </div>
    <div class="HideIt"><asp:Label ID="UserErrorMessage" runat="server" ></asp:Label>
    <asp:Label ID="CaptchErrorMessage" runat="server" ></asp:Label></div>
</asp:Panel>
<asp:HiddenField runat="server" ID="hdnPasswDisplayed" />

<script language="javascript" type="text/javascript">
    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    function PassRetr_validateControls(oSrc, args) {
        var bIsValid = true
        var MyVal = args.Value

        if (MyVal.length == 0) {
            bIsValid = false
        }

        if (oSrc.getAttribute('IsUserName') == 'yes') {
            if (bIsValid == false)
                ShowTheCloud('#UserNameMsg', '<%=UserErrorMessage.Text %>', true)
            else
                ShowTheCloud('#UserNameMsg', '<%=UserErrorMessage.Text %>', false)
        }
        if (oSrc.getAttribute('IsCaptcha') == 'yes') {
            if (bIsValid == false)
                ShowTheCloud('#CaptchaMsg', '<%=CaptchErrorMessage.Text %>', true)
            else
                ShowTheCloud('#CaptchaMsg', '<%=CaptchErrorMessage.Text %>', false)
        }

        args.IsValid = bIsValid;
    }

    function ShowTheCloud(CloudID, CloudContent, Show) {
        $(CloudID).poshytip('hide');
        var OfX = 185
        if (is_chrome) {
            $(CloudID).css('position', 'absolute');
            OfX = 25
        }
        var OffY = -25;
        if (is_firefox) {
            OffY = -10
            OfX = 25
        }

        $(CloudID).poshytip({
            className: 'tip-livepay',
            content: CloudContent,
            showOn: 'none',
            alignTo: 'target',
            alignX: 'right',
            offsetX: OfX,
            offsetY: OffY, hideAniDuration: false
        });
        $(CloudID).poshytip('hide');
        if (Show)
            $(CloudID).poshytip('show');

    }
</script>
<asp:Literal ID="litScript" runat="server" ></asp:Literal>
