<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Order_Edit_CustomerFrameset.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Orders_Order_Edit_CustomerFrameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Order - edit - CustomerFrameset</title>
</head>
<frameset border="0" rows="72, *" id="rowsFrameset">
    <frame name="CustomerHeader" src="../Customers/Customer_Edit_Header.aspx?customerid=<%=Request.QueryString["customerid"]%>&showtitle=1&hidebreadcrumbs=1" frameborder="0" scrolling="no" noresize="noresize" />
    <frame name="CustomerContent" src="../Customers/Customer_Edit_General.aspx?customerid=<%=Request.QueryString["customerid"]%>" frameborder="0" />
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
</frameset>
</html>