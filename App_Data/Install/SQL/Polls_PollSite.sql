CREATE TABLE [Polls_PollSite] (
		[PollID]     int NOT NULL,
		[SiteID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Polls_PollSite]
	ADD
	CONSTRAINT [PK_Polls_PollSite]
	PRIMARY KEY
	NONCLUSTERED
	([PollID], [SiteID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Polls_PollSite]') IS NOT NULL
	AND OBJECT_ID(N'[Polls_Poll]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Polls_PollSite_PollID_Polls_Poll]') IS NULL
BEGIN
		ALTER TABLE [Polls_PollSite]
			ADD CONSTRAINT [FK_Polls_PollSite_PollID_Polls_Poll]
			FOREIGN KEY ([PollID]) REFERENCES [Polls_Poll] ([PollID])
END
IF OBJECT_ID(N'[Polls_PollSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Polls_PollSite_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Polls_PollSite]
			ADD CONSTRAINT [FK_Polls_PollSite_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
