<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/LivePay/Logon/MobileLogonform.ascx.cs"
    Inherits="CMSWebParts_LivePay_Logon_MobileLogonform" %>

<%@ Register TagPrefix="UC" TagName="EditableText" Src="~/CMSWebParts/Text/editabletext.ascx" %>

<script type="text/javascript">
    $(function () {
        $('#btnEisodos').click(function () {
            $('[id$=_mobileLogin]').show();
            $('[id$=_Eisodos]').hide();
            ChangeIPhoneTitle('������� ������');
        });

    });
</script>    

<style type="text/css">
a{border:0px;}
</style>   

<div id="Eisodos" runat="server">
    <center>
        <div style="text-align: center;"><img src="/App_Themes/LivePay/Mobile/AppiPhone/cards.jpg" alt="" /></div>
        <div style="color: rgb(54, 54, 54); text-align: center;">
            <UC:EditableText ID="editableText" runat="server" RegionType="HtmlEditor" HtmlAreaToolbarLocation="Out:FCKToolbar" WordWrap="true" CheckPermissions="false" SelectOnlyPublished="false" />
        </div>
        <div style="padding-top: 10px;"><a id="btnEisodos" href="javascript:void(0);"><img alt="" src="/App_Themes/LivePay/Mobile/AppiPhone/btnEisodos.jpg" /></a></div>
        <div style="padding-top: 10px;"><a id="btnSynexeia" runat="server" href="javascript:void(0);"><img alt="" src="/App_Themes/LivePay/Mobile/AppiPhone/btnSynexeia.jpg" /></a></div>
    </center>
</div>


<div id="mobileLogin" style="display:none;" runat="server">
<div>
    <UC:EditableText ID="txtEisodosMelus" runat="server" RegionType="HtmlEditor" HtmlAreaToolbarLocation="Out:FCKToolbar" WordWrap="true" CheckPermissions="false" SelectOnlyPublished="false" />
</div>
<div class="PL10PT30">
<asp:Login ID="loginElem" runat="server" DestinationPageUrl="~/Mobile/SearchMerchants.aspx" EnableViewState="false">
    <LayoutTemplate>
        <asp:Panel ID="pnlLogonMiniForm" runat="server" DefaultButton="btnLogon" EnableViewState="false">
            <div>
                <div class="login_text">
                    <cms:LocalizedLabel ID="lblUserName" runat="server" AssociatedControlID="UserName" EnableViewState="false" />
                </div>
                <div class="RegUsersTab2_TxtDiv RegUsersTab2_TxtDiv_text">
                    <asp:TextBox ID="UserName" runat="server" CssClass="LogonField" EnableViewState="false" />
                </div>
                <div style="float:left">
                    <asp:RequiredFieldValidator ID="rfvUserNameRequired" runat="server" ControlToValidate="UserName"
                        ValidationGroup="MiniLogin" Display="Dynamic" EnableViewState="false">*</asp:RequiredFieldValidator>
                </div>
                <div style="clear:both"></div>
            </div>
            <div>
                <div class="login_text">
                    <cms:LocalizedLabel ID="lblPassword" runat="server" AssociatedControlID="Password" EnableViewState="false" />
                </div>
                <div class="RegUsersTab2_TxtDiv RegUsersTab2_TxtDiv_text">
                    <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="LogonField" EnableViewState="false" />
                </div>
                <div style="clear:both"></div>
            </div>
            <div style="float: left; padding-top: 20px;">
                <asp:ImageButton ID="btnPrev" OnClick="btnPrev_Click" runat="server" ImageUrl="/App_Themes/LivePay/Mobile/AppiPhone/BtnPrev.png" EnableViewState="false" />                
            </div>
            <div class="btnLogin" style="overflow: hidden;padding-left:28px;padding-top: 20px;">
                <cms:LocalizedButton ID="btnLogon" runat="server" ResourceString="LogonForm.LogOnButton" CommandName="Login" ValidationGroup="MiniLogin" EnableViewState="false" />
                <asp:ImageButton ID="btnImageLogon" runat="server" Visible="false" CommandName="Login"
                    ValidationGroup="MiniLogin" EnableViewState="false" />
            </div>
            <div>
                <asp:Label ID="FailureText" CssClass="ErrorLabel" runat="server" EnableViewState="false" />
            </div>
        </asp:Panel>
    </LayoutTemplate>
</asp:Login>
</div>
</div>

<div style="display:none;position:absolute;color:White;font-size:11px;" id="AppMobileInfo_">
    <div>
        <div style="float:left;"><img src="/App_Themes/LivePay/Info/LeftCorner.png" /></div>
        <div style="float:left;background-image:url('/App_Themes/LivePay/Info/Top.png');background-repeat:repeat-x;height:14px;width:auto" id="AppMobileRptInfoTop_">
            <div style="background-color:#0c4593;margin-top:8px;margin-left:-12px;margin-right:-12px;" id="AppMainWidth_">
                <div id="AppMobileInfoContent_" style="float:left;padding-left:10px;width:auto">
            
                </div>
                <div  style="float:right;padding-right:10px;text-align:right;">
                    <img src="/App_Themes/LivePay/Mobile/AppiPhone/Close.png" onclick="$('#AppMobileInfo_').hide()" />
                </div>
                <div style="clear:both"></div>
            </div>
            <div style="margin-left:-12px;float:left;margin-top:-1px"><img src="/App_Themes/LivePay/Info/BottomLeftCorner.png" /></div>
            <div style="float:left;background-image:url('/App_Themes/LivePay/Info/Bottom.png');background-repeat:repeat-x;height:14px;margin-top:-1px;" id="AppMobileRptInfoBottom_">&nbsp;</div> 
            <div style="float:left;margin-top:-1px;position:absolute" id="AppRightBottom_"><img src="/App_Themes/LivePay/Info/RightBottomCorner.png" /></div>
            <div style="clear:both"></div>
        </div>
        <div style="float:left;" ><img src="/App_Themes/LivePay/Info/RightCorner.png" /></div>
        <div style="clear:both"></div>
    </div>
    <div>
    </div>
</div>