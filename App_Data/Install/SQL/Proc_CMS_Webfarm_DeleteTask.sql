CREATE PROCEDURE [Proc_CMS_Webfarm_DeleteTask]
	@ServerId int,
	@TaskId int
AS
BEGIN
DECLARE @ExistingTaskID int
BEGIN TRANSACTION
	-- Removes task/server binding
	DELETE FROM [CMS_WebFarmServerTask] Where [TaskId]=@TaskId AND[ServerId]=@ServerId
COMMIT TRANSACTION
	-- Returns id of existing taks
	SET @ExistingTaskID = (SELECT TOP 1 TaskID FROM CMS_WebFarmServerTask WITH (NOLOCK) WHERE [TaskId]=@TaskId)
	--  Removes all unasigned tasks
	IF @ExistingTaskID IS NULL
	DELETE FROM CMS_WebFarmTask WHERE [TaskId]=@TaskId
END
