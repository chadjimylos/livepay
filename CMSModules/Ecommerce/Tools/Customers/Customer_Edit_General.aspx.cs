using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Customers_Customer_Edit_General : CMSEcommercePage
{
    private int customerid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Init controls
        btnOk.Text = ResHelper.GetString("General.OK");
        lblCustomerLastName.Text = ResHelper.GetString("Customers_Edit.CustomerLastNameLabel");
        lblCustomerFirstName.Text = ResHelper.GetString("Customers_Edit.CustomerFirstNameLabel");
        lblCustomerCompany.Text = ResHelper.GetString("Customers_Edit.CustomerCompanyLabel");
        lblCustomerFax.Text = ResHelper.GetString("customers_edit.CustomerFax");
        lblCustomerPhone.Text = ResHelper.GetString("customers_edit.CustomerPhone");
        lblCustomerPreferredCurrency.Text = ResHelper.GetString("customers_edit.CustomerCurrency");
        lblCustomerPreferredShippingOption.Text = ResHelper.GetString("customers_edit.CustomerShipping");
        lblCustomerPrefferedPaymentOption.Text = ResHelper.GetString("customers_edit.CustomerPayment");
        lblCustomerEmail.Text = ResHelper.GetString("general.email");
        lblCustomerCountry.Text = ResHelper.GetString("Customers_Edit.CustomerCountry");
        lblTaxRegistrationID.Text = ResHelper.GetString("Customers_Edit.lblTaxRegistrationID");
        lblOrganizationID.Text = ResHelper.GetString("Customers_Edit.lblOrganizationID");
        lblCustomerDiscountLevel.Text = ResHelper.GetString("Customers_Edit.lblCustomerDiscountLevel");

        customerid = ValidationHelper.GetInteger(Request.QueryString["customerid"], 0);
        if (customerid > 0)
        {
            CustomerInfo customerObj = CustomerInfoProvider.GetCustomerInfo(customerid);
            if (customerObj != null)
            {
                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    // Fill form
                    txtCustomerCompany.Text = customerObj.CustomerCompany;
                    txtCustomerEmail.Text = customerObj.CustomerEmail;
                    txtCustomerFax.Text = customerObj.CustomerFax;
                    txtCustomerFirstName.Text = customerObj.CustomerFirstName;
                    txtCustomerLastName.Text = customerObj.CustomerLastName;
                    txtCustomerPhone.Text = customerObj.CustomerPhone;
                    txtOraganizationID.Text = customerObj.CustomerOrganizationID;
                    txtTaxRegistrationID.Text = customerObj.CustomerTaxRegistrationID;
                    chkCustomerEnabled.Checked = customerObj.CustomerEnabled;

                    if (customerObj.CustomerCountryID > 0)
                    {
                        drpCountry.CountryID = customerObj.CustomerCountryID;
                    }
                    if (customerObj.CustomerStateID > 0)
                    {
                        drpCountry.StateID = customerObj.CustomerStateID;
                    }

                    // show that the customer was created or updated successfully
                    if (QueryHelper.GetString("saved", "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }

            LoadDataSelectors(customerObj);
        }
    }


    /// <summary>
    /// Load data
    /// </summary>
    public void LoadDataSelectors(CustomerInfo customerObj)
    {
        if (!UrlHelper.IsPostback())
        {
            if (customerObj.CustomerPreferredCurrencyID > 0)
            {
                drpCurrency.CurrencyID = customerObj.CustomerPreferredCurrencyID;
            }
            if (customerObj.CustomerPrefferedPaymentOptionID > 0)
            {
                drpPayment.PaymentID = customerObj.CustomerPrefferedPaymentOptionID;
            }
            if (customerObj.CustomerPreferredShippingOptionID > 0)
            {
                drpShipping.ShippingID = customerObj.CustomerPreferredShippingOptionID;
            }
            if (customerObj.CustomerDiscountLevelID > 0)
            {
                drpDiscountLevel.DiscountLevel = customerObj.CustomerDiscountLevelID;
            }
        }
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        string errorMessage = "";

        if ((txtCustomerCompany.Text.Trim() == "") &&
            ((txtCustomerFirstName.Text.Trim() == "") || (txtCustomerLastName.Text.Trim() == "")))
        {
            errorMessage = ResHelper.GetString("Customers_Edit.errorInsert");
        }
        else if (CMSContext.CurrentSite.SiteRequireOrgTaxRegIDs && (txtCustomerCompany.Text.Trim() != "" || txtOraganizationID.Text.Trim() != "" || txtTaxRegistrationID.Text.Trim() != ""))
        {
            errorMessage = new Validator().NotEmpty(txtCustomerCompany.Text.Trim(), ResHelper.GetString("customers_edit.errorcompany"))
                .NotEmpty(txtOraganizationID.Text.Trim(), ResHelper.GetString("customers_edit.errororganizationid"))
                .NotEmpty(txtTaxRegistrationID.Text.Trim(), ResHelper.GetString("customers_edit.errortaxregid")).Result;
        }
        else if ((txtCustomerEmail.Text.Trim() != "") && !ValidationHelper.IsEmail(txtCustomerEmail.Text))
        {
            errorMessage = ResHelper.GetString("Customers_Edit.errorEmail");
        }

        if (errorMessage == "")
        {
            CustomerInfo customerObj = CustomerInfoProvider.GetCustomerInfo(customerid);

            // if customer doesnt already exist, create new one
            if (customerObj == null)
            {
                customerObj = new CustomerInfo();
            }

            customerObj.CustomerPreferredCurrencyID = drpCurrency.CurrencyID;
            customerObj.CustomerEmail = txtCustomerEmail.Text.Trim();
            customerObj.CustomerPrefferedPaymentOptionID = drpPayment.PaymentID;
            customerObj.CustomerFax = txtCustomerFax.Text.Trim();
            customerObj.CustomerPreferredShippingOptionID = drpShipping.ShippingID;
            customerObj.CustomerLastName = txtCustomerLastName.Text.Trim();
            customerObj.CustomerPhone = txtCustomerPhone.Text.Trim();
            customerObj.CustomerFirstName = txtCustomerFirstName.Text.Trim();
            customerObj.CustomerCompany = txtCustomerCompany.Text.Trim();
            customerObj.CustomerCountryID = drpCountry.CountryID;
            customerObj.CustomerStateID = drpCountry.StateID;
            customerObj.CustomerEnabled = chkCustomerEnabled.Checked;
            customerObj.CustomerOrganizationID = txtOraganizationID.Text.Trim();
            customerObj.CustomerTaxRegistrationID = txtTaxRegistrationID.Text.Trim();
            customerObj.CustomerDiscountLevelID = drpDiscountLevel.DiscountLevel;

            CustomerInfoProvider.SetCustomerInfo(customerObj);

            // Enable/disable coresponding registered user
            if (customerObj.CustomerUserID > 0)
            {
                UserInfo ui = UserInfoProvider.GetUserInfo(customerObj.CustomerUserID);
                if (ui != null)
                {
                    ui.SetValue("UserEnabled", chkCustomerEnabled.Checked);
                    UserInfoProvider.SetUserInfo(ui);
                }
            }

            UrlHelper.Redirect("Customer_Edit_General.aspx?customerid=" + Convert.ToString(customerObj.CustomerID) + "&saved=1");
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
