﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCTransDetails.ascx.vb" Inherits="CMSTemplates_LivePay_PopUp_UCTransDetails" %>
<script>
    function ExportDetails(url) {
        document.getElementById('<%=exportpopFrame.ClientID %>').src = url;
        return false
    }
</script>
<div style="display:none">
<iframe id="exportpopFrame" runat="server" ></iframe>
</div>
<table cellpadding="0" cellspacing="0" border="0" class="PopUpMainTable">
    <tr>
	<td class="PopTopLeft">&nbsp;</td>
        <td class="PopTopCenter">&nbsp;</td>
        <td class="PopTopClose"><img src="/App_Themes/LivePay/PopUp/Close.png"  alt="Close" title="Close" style="cursor:pointer;" onclick="$.unblockUI()"/></td>
	<td class="PopTopRight">&nbsp;</td>  
    </tr>
    <tr>
	<td class="PopCenterLeft">&nbsp;</td>
	<td colspan="2" class="PopCenter">
        <table cellpadding="0" cellspacing="0">
            <tr><td class="PopTopTitle" id="PageTitle" runat="server"></td></tr>
            <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>
            <tr id="ReportRow" runat="server" style="display:none"><td>
               <table cellpadding="0" cellspacing="0">
                    <tr><td class="PopTopSep">&nbsp;</td></tr>
                    <tr><td class="PopTopAlertBG">
                    <div class="PopTopAlertText"><%=ResHelper.LocalizeString("{$=Έχετε επιλέξει να αντιλογήσετε την παρακάτω συναλλαγή |en-us=Έχετε επιλέξει να αντιλογήσετε την παρακάτω συναλλαγή $}") %></div>
                    </td></tr>
               </table>
            </td></tr>
            <tr id="CancelRow" runat="server" style="display:none"><td>
               <table cellpadding="0" cellspacing="0">
                <tr><td class="PopTopSep">&nbsp;</td></tr>
                <tr><td class="PopTopCancelBG">
                    <div class="PopTopCancelText"><%=ResHelper.LocalizeString("{$=Έχετε επιλέξει να ακυρώσετε την παρακάτω συναλλαγή |en-us=Έχετε επιλέξει να ακυρώσετε την παρακάτω συναλλαγή $}") %></div>
                </td></tr>
               </table>
            </td></tr>
            <tr><td class="PopContent">
                <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center" >
                   <tr>
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Αριθμός Πακέτου |en-us=Αριθμός Πακέτου $}") %></td>
                        <td class="PopTransDetValue" id="tdBoxNo" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ημερομηνία Κλεισίματος Πακέτου |en-us=Ημερομηνία Κλεισίματος Πακέτου $}")%></td>
                        <td class="PopTransDetValue" id="tdBoxCloseDtm" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Κατάσταση Πακέτου |en-us=Κατάσταση Πακέτου $}") %></td>
                        <td class="PopTransDetValue" id="tdBoxStatus" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Κωδικός Συναλλαγής |en-us=Κωδικός Συναλλαγής $}") %></td>
                        <td class="PopTransDetValue" id="tdTransCode" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ημερομηνία Συναλλαγής |en-us=Ημερομηνία Συναλλαγής $}")%></td>
                        <td class="PopTransDetValue" id="tdTransDtm" runat="server"></td>
                    </tr>

                    <tr>
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Ονοματεπώνυμο Πελάτη |en-us=Ονοματεπώνυμο Πελάτη $}") %></td>
                        <td class="PopTransDetValue" id="tdCustFullName" runat="server"></td>
                    </tr>
                    <tr id="TRPrice" runat="server">
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ποσό |en-us=Ποσό $}")%></td>
                        <td class="PopTransDetValue"  ID="tdPrice" runat="server" ></td>
                    </tr>
                    <tr id="TRPriceTxt" runat="server">
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Ποσό |en-us=Ποσό $}") %></td>
                        <td class="PopTransDetValue"  ID="TdPriceTxt" runat="server" ><asp:TextBox onkeyup="FixMoney(this,false,false)" onblur="FixMoney(this,true,false)" CssClass="PopTransREp" ID="txtPrice" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Τύπος Συναλλαγής |en-us=Τύπος Συναλλαγής $}")%></td>
                        <td class="PopTransDetValue" id="tdTransType" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle"><%= ResHelper.LocalizeString("{$=Κατάσταση Συναλλαγής |en-us=Κατάσταση Συναλλαγής $}")%></td>
                        <td class="PopTransDetValue"  id="tdTransStatus" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Τύπος Κάρτας |en-us=Τύπος Κάρτας $}")%></td>
                        <td class="PopTransDetValue" id="tdCardType" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Αριθμός Κάρτας |en-us=Αριθμός Κάρτας $}")%></td>
                        <td class="PopTransDetValue" id="tdCardNo" runat="server"></td>
                    </tr>
                    <tr>
                        <td colspan="2" width="100%">
                            <table cellpadding="0" cellspacing="0" id="tblCustomFields" runat="server">
                            </table>
                        </td>
                    </tr>
                     <tr>
                        <td class="PopTransDetTitle" >E-mail</td>
                        <td class="PopTransDetValue" id="tdEmail" runat="server"></td>
                    </tr>
                     <tr>
                        <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Τηλέφωνο Επικοινωνίας |en-us=Τηλέφωνο Επικοινωνίας $}") %></td>
                        <td class="PopTransDetValue" id="tdContactPhone" runat="server"></td>
                    </tr>
                     <tr>
                        <td class="PopTransDetTitle"><%=ResHelper.LocalizeString("{$=Σημειώσεις |en-us=Σημειώσεις $}") %></td>
                        <td class="PopTransDetNotes" id="tdNotes" runat="server">-</td>
                    </tr>
                    <tr id="DetailsBtnRow" runat="server" style="display:none">
                        <td colspan="2" class="PopTransDetButtons"><asp:LinkButton runat="server" ID="btnSaveToPDF" runat="server" ><img class="Chand" border="0" src="/app_themes/LivePay/popup/btnpdf.png"  /></asp:LinkButton>
                        &nbsp;&nbsp;&nbsp;
                        <img border="0" onclick="window.print()" class="Chand" src="/app_themes/LivePay/popup/btnPrint.png" /></td>
                    </tr>
                     <tr id="CancelBtnRow" runat="server" style="display:none">
                        <td colspan="2" class="PopTransReportButtons"><img class="Chand" border="0" src="/app_themes/LivePay/popup/btnreturn.png" onclick="$.unblockUI()" />&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton runat="server" ID="btnCancel" runat="server" ><img border="0"  class="Chand" src="/app_themes/LivePay/popup/btnTrans.png" /></asp:LinkButton></td>
                    </tr>
                     <tr id="ReportBtnRow" runat="server" style="display:none">
                        <td colspan="2" class="PopTransReportButtons">
                        <img border="0" src="/app_themes/LivePay/popup/btnreturn.png"  onclick="$.unblockUI()" class="Chand" />&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton runat="server" ID="btnRefund" runat="server" ><img border="0" class="Chand" src="/app_themes/LivePay/popup/btncont.png" /></asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="PopTransDEtBotDescr"><%=ResHelper.LocalizeString("{$=Για οποιαδήποτε πληροφορία καλέστε στο 801-111-1144 από σταθερό τηλέφωνο ή στο  210 - 9555000 από κινητό (24 ώρες το 24 ωρό) ή αποστείλατε e-mail στο ebanking@eurobank.gr |en-us=Για οποιαδήποτε πληροφορία καλέστε στο 801-111-1144 από σταθερό τηλέφωνο ή στο  210 - 9555000 από κινητό (24 ώρες το 24 ωρό) ή αποστείλατε e-mail στο ebanking@eurobank.gr $}") %></td>
                    </tr>
                   
                </table>
            </td></tr>
        </table>
    </td>
	<td class="PopCenterRight">&nbsp;</td>
    </tr>
    <tr>
	<td class="PopBotLeft">&nbsp;</td>
	<td colspan="2" class="PopBotCenter"></td>
	<td class="PopBotRight">&nbsp;</td>
    </tr>
</table>	


<table cellpadding="0" cellspacing="0" border="0" style="display:none" class="PopUpMainTable">
    <tr>
	<td class="PopTopLeft">&nbsp;</td>
        <td class="PopTopCenter">&nbsp;</td>
        <td class="PopTopClose"><img src="/App_Themes/LivePay/PopUp/Close.png"  alt="Close" title="Close" style="cursor:pointer;"/></td>
	<td class="PopTopRight">&nbsp;</td>  
    </tr>
    <tr>
	<td class="PopCenterLeft">&nbsp;</td>
	<td colspan="2" class="PopCenter">
        <table cellpadding="0" cellspacing="0">
            <tr><td class="PopTopTitle">Πληροφορίες Συναλλαγής</td></tr>
            <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>
            <tr><td></td></tr>
        </table>
    </td>
	<td class="PopCenterRight">&nbsp;</td>
    </tr>
    <tr>
	<td class="PopBotLeft">&nbsp;</td>
	<td colspan="2" class="PopBotCenter"></td>
	<td class="PopBotRight">&nbsp;</td>
    </tr>
</table>	

<script language="javascript" type="text/javascript">
    function SetIt() {
        var txtPrice = document.getElementById('<%=txtPrice.clientID %>')
        var txtBackPrice = document.getElementById(TestControl)
        var PriceLimit = '<%=btnRefund.CommandName%>'
        PriceLimit = PriceLimit.replace(",", ".")
        txtPrice.focus()
        txtPrice.blur()
        txtBackPrice.value = txtPrice.value.replace(".", ",")
        if (parseFloat(PriceLimit) >= parseFloat(txtPrice.value)) {
            return true
        } else {

            alert('<%=ResHelper.LocalizeString("{$=Το ποσό που συμπληρώσατε(' + parseFloat(txtPrice.value) + ') είναι μεγαλύτερο από το ποσό(' + parseFloat(PriceLimit) + ') της συναλλαγής!|en-us=Το ποσό που συμπληρώσατε(' + parseFloat(txtPrice.value) + ') είναι μεγαλύτερο από το ποσό(' + parseFloat(PriceLimit) + ') της συναλλαγής!$}") %>')
            return false
        }
        
    } 
</script>