using System;

using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Edit_EditFrameset : CMSContentPage
{
    #region "Variables"

    protected string viewpage = null;
    protected string menupage = null;
    protected string toolbarpage = null;

    protected string toolbarSize = "78";
    protected string menuSize = "43";

    private int nodeId = 0;
    private int classId = 0;

    private TreeProvider tree = null;
    private TreeNode node = null;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        nodeId = QueryHelper.GetInteger("nodeid", 0);

        // Get current node
        tree = new TreeProvider(CMSContext.CurrentUser);
        node = tree.SelectSingleNode(nodeId);

        //   current nodeID
        if (node != null)
        {
            // Check read permissions
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
            }
        }
        //   classID, used when creating new document
        classId = QueryHelper.GetInteger("classid", 0);

        if (classId > 0)
        {
            DataClassInfo ci = DataClassInfoProvider.GetDataClass(classId);
            if (ci != null)
            {
                // check permission to create new document
                if (CMSContext.CurrentUser.IsAuthorizedToCreateNewDocument(nodeId, ci.ClassName))
                {
                    InitPage();
                }
                else
                {
                    RedirectToAccessDenied(ResHelper.GetString("cmsdesk.notauthorizedtocreatedocument"));
                }
            }
        }
        else
        {
            InitPage();
        }
    }


    protected void InitPage()
    {
        menupage = "editmenu.aspx" + Request.Url.Query;
        toolbarpage = "edittoolbar.aspx" + Request.Url.Query;

        switch (QueryHelper.GetString("mode", string.Empty).ToLower())
        {
            case "edit":
                ltlScript.Text += ScriptHelper.GetScript("SetTabsContext('page');");
                break;

            case "editform":
                ltlScript.Text += ScriptHelper.GetScript("SetTabsContext('edit');");
                break;

            case "design":
                ltlScript.Text += ScriptHelper.GetScript("SetTabsContext('design');");
                break;
        }

        if (classId <= 0)
        {
            if (node != null)
            {
                // Set view mode to edit if new document
                string action = QueryHelper.GetString("action", string.Empty);
                switch (action.ToLower())
                {
                    case "new":
                    case "newlink":
                    case "newculture":
                        CMSContext.ViewMode = ViewModeEnum.EditForm;
                        break;
                }

                switch (UpdateViewMode(node, ViewModeEnum.Edit))
                {
                    case ViewModeEnum.Design:
                        menuSize = "0";
                        menupage = "../separator.aspx";
                        break;

                    case ViewModeEnum.Edit:
                        if (node.NodeClassName.ToLower() == "cms.file")
                        {
                            menuSize = "0";
                            menupage = "../blank.htm";
                        }
                        break;
                }
            }
        }

        toolbarpage = "../blank.htm";
        viewpage = "editpage.aspx" + Request.Url.Query;

        toolbarSize = "0";
    }
}
