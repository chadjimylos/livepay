﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileSystemPathProperties.ascx.cs"
    Inherits="CMSModules_Content_Controls_Dialogs_Properties_FileSystemPathProperties" %>

<script type="text/javascript" language="javascript">
    //<![CDATA[
    // -- function insertItem() {
    //    RaiseHiddenPostBack();
    //}%>
    //]]>
</script>

<div class="DialogInfoArea">
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    <asp:Label ID="lblEmpty" runat="server" />
    <div class="LeftAlign" style="width: 600px;">
        <asp:UpdatePanel ID="plnPathUpdate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
                    Visible="false" />
                <table style="vertical-align: top; white-space: nowrap;" width="100%">
                    <asp:PlaceHolder runat="server" ID="plcPathText">
                        <tr>
                            <td>
                                <cms:LocalizedLabel ID="lblPathText" runat="server" EnableViewState="false" ResourceString="general.path"
                                    DisplayColon="true" />
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtPathText" CssClass="VeryLongTextBox" />
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder runat="server" ID="plcFileSize">
                        <tr>
                            <td>
                                <cms:LocalizedLabel ID="lblFileSize" runat="server" EnableViewState="false" ResourceString="media.file.filesize"
                                    DisplayColon="true" />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblFileSizeText" />
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="Hidden">
            <asp:UpdatePanel ID="plnFileSystemButtonsUpdate" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Button ID="hdnButton" runat="server" OnClick="hdnButton_Click" CssClass="HiddenButton"
                        EnableViewState="false" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
