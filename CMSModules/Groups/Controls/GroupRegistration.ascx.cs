using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Community;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.SettingsProvider;

using TreeNode = CMS.TreeEngine.TreeNode;
using CMS.UIControls;

public partial class CMSModules_Groups_Controls_GroupRegistration : CMSUserControl
{
    #region "Variables"

    private int mSiteId = 0;
    private bool mRequireApproval = true;
    private string mGroupTemplateSourceAliasPath = null;
    private string mGroupTemplateTargetAliasPath = null;
    private string mGroupProfileURLPath = String.Empty;
    private bool mCombineWithDefaultCulture = false;
    private string mRedirectToURL = "";
    private string mGroupNameLabelText = null;
    private string mSuccessfullRegistrationText;
    private string mSuccessfullRegistrationWaitingForApprovalText;
    private bool mHideFormAfterRegistration = true;
    private string codeName = "";

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether form should be hidden after successful registration
    /// </summary>

    public bool HideFormAfterRegistration
    {
        get
        {
            return mHideFormAfterRegistration;
        }
        set
        {
            mHideFormAfterRegistration = value;
        }
    }


    /// <summary>
    /// Gets or sets text which should be displayed after successful registration
    /// </summary>
    public string SuccessfullRegistrationText
    {
        get
        {
            return DataHelper.GetNotEmpty(mSuccessfullRegistrationText, ResHelper.GetString("group.group.succreg"));
        }
        set
        {
            mSuccessfullRegistrationText = value;
        }
    }


    /// <summary>
    /// Gets or sets text which should be displayed after successful registration and waiting for approving
    /// </summary>
    public string SuccessfullRegistrationWaitingForApprovalText
    {
        get
        {
            return DataHelper.GetNotEmpty(mSuccessfullRegistrationWaitingForApprovalText, ResHelper.GetString("group.group.succregapprove"));
        }
        set
        {
            mSuccessfullRegistrationWaitingForApprovalText = value;
        }
    }


    /// <summary>
    /// Gets or sets the label text of group name
    /// </summary>
    public string GroupNameLabelText
    {
        get
        {
            if (mGroupNameLabelText == null)
            {
                mGroupNameLabelText = ResHelper.GetString("general.displayname") + ResHelper.Colon;
            }
            return mGroupNameLabelText;
        }
        set
        {
            mGroupNameLabelText = value;
            this.lblDisplayName.Text = value;
        }
    }


    /// <summary>
    /// Current site ID
    /// </summary>
    public int SiteID
    {
        get
        {
            return this.mSiteId;
        }
        set
        {
            this.mSiteId = value;
        }
    }


    /// <summary>
    /// If true, the group must be approved before it can be active.
    /// </summary>
    public bool CombineWithDefaultCulture
    {
        get
        {
            return this.mCombineWithDefaultCulture;
        }
        set
        {
            this.mCombineWithDefaultCulture = value;
        }
    }


    /// <summary>
    /// If true, the group must be approved before it can be active.
    /// </summary>
    public bool RequireApproval
    {
        get
        {
            return this.mRequireApproval;
        }
        set
        {
            this.mRequireApproval = value;
        }
    }


    /// <summary>
    /// Alias path of the document structure which will be copied as the group content.
    /// </summary>
    public string GroupTemplateSourceAliasPath
    {
        get
        {
            return this.mGroupTemplateSourceAliasPath;
        }
        set
        {
            this.mGroupTemplateSourceAliasPath = value;
        }
    }


    /// <summary>
    /// Alias where the group content will be created by copying the source template.
    /// </summary>
    public string GroupTemplateTargetAliasPath
    {
        get
        {
            return this.mGroupTemplateTargetAliasPath;
        }
        set
        {
            this.mGroupTemplateTargetAliasPath = value;
        }
    }


    /// <summary>
    /// Gets or sets the document URL under which will be accessible the profile of newly created group 
    /// </summary>
    public string GroupProfileURLPath
    {
        get
        {
            return mGroupProfileURLPath;
        }
        set
        {
            mGroupProfileURLPath = value;
        }
    }


    /// <summary>
    /// Gets or sets the url, where is user redirected after registration
    /// </summary>
    public string RedirectToURL
    {
        get
        {
            return this.mRedirectToURL;
        }
        set
        {
            this.mRedirectToURL = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RequestHelper.IsPostBack())
        {
            InitializeForm();
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        SaveData();
    }


    #region "Public methods"

    /// <summary>
    /// Updates the current Group or creates new if no GroupID is present.
    /// </summary>
    public void SaveData()
    {
        // Check banned ip
        if (!BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.AllNonComplete))
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("General.BannedIP");
            return;
        }

        // Validate form entries
        string errorMessage = ValidateForm();
        if (errorMessage == "")
        {
            try
            {
                codeName = GetSafeCodeName();
                codeName = GetUniqueCodeName(codeName);

                GroupInfo group = new GroupInfo();
                group.GroupDisplayName = this.txtDisplayName.Text;
                group.GroupName = codeName;
                group.GroupDescription = this.txtDescription.Text;
                group.GroupAccess = GetGroupAccess();
                group.GroupSiteID = this.mSiteId;
                group.GroupApproveMembers = GetGroupApproveMembers();

                // Set columns GroupCreatedByUserID and GroupApprovedByUserID to current user
                CurrentUserInfo user = CMSContext.CurrentUser;

                if (user != null)
                {
                    group.GroupCreatedByUserID = user.UserID;

                    if ((!this.RequireApproval) || (CurrentUserIsAdmin()))
                    {
                        group.GroupApprovedByUserID = user.UserID;
                        group.GroupApproved = true;
                    }
                }

                // Save Group in the database
                GroupInfoProvider.SetGroupInfo(group);

                // Create group admin role
                RoleInfo roleInfo = new RoleInfo();
                roleInfo.DisplayName = "Group admin";
                roleInfo.RoleName = group.GroupName + "_groupadmin";
                roleInfo.RoleGroupID = group.GroupID;
                roleInfo.RoleIsGroupAdministrator = true;
                roleInfo.SiteID = this.mSiteId;
                // Save group admin role
                RoleInfoProvider.SetRoleInfo(roleInfo);

                if (user != null)
                {
                    // Set user as member of group
                    GroupMemberInfo gmi = new GroupMemberInfo();
                    gmi.MemberUserID = user.UserID;
                    gmi.MemberGroupID = group.GroupID;
                    gmi.MemberJoined = DateTime.Now;
                    gmi.MemberStatus = GroupMemberStatus.Approved;
                    gmi.MemberApprovedWhen = DateTime.Now;
                    gmi.MemberApprovedByUserID = user.UserID;

                    // Save user as member of group
                    GroupMemberInfoProvider.SetGroupMemberInfo(gmi);

                    // Set user as member of admin group role
                    UserRoleInfo userRole = new UserRoleInfo();
                    userRole.UserID = user.UserID;
                    userRole.RoleID = roleInfo.RoleID;

                    // Save user as member of admin group role
                    UserRoleInfoProvider.SetUserRoleInfo(userRole);
                }

                // Clear user session a request
                CMSContext.CurrentUser.Invalidate();
                CMSContext.CurrentUser = null;

                // Copy document
                errorMessage = CopyGroupDocument(group);

                if (errorMessage != "")
                {
                    // Display error message
                    this.lblError.Text = errorMessage;
                    this.lblError.Visible = true;
                    return;
                }

                // Display information on success
                this.lblInfo.Text = ResHelper.GetString("group.group.createdinfo");
                this.lblInfo.Visible = true;

                // If URL is set, redirect user to specified page
                if (!String.IsNullOrEmpty(this.RedirectToURL))
                {
                    UrlHelper.Redirect(ResolveUrl(CMSContext.GetUrl(this.RedirectToURL)));
                }

                // After registration message
                if ((this.RequireApproval) && (!CurrentUserIsAdmin()))
                {
                    this.lblInfo.Text = this.SuccessfullRegistrationWaitingForApprovalText;
                }
                else
                {
                    string groupPath = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSGroupProfilePath");
                    string url = String.Empty;

                    if (!String.IsNullOrEmpty(groupPath))
                    {
                        url = TreePathUtils.GetUrl(groupPath.Replace("{GroupName}", group.GroupName));
                    }
                    this.lblInfo.Text = String.Format(this.SuccessfullRegistrationText, url);
                }

                // Hide form
                if (this.HideFormAfterRegistration)
                {
                    this.plcForm.Visible = false;
                }
                else
                {
                    ClearForm();
                }
            }
            catch (Exception ex)
            {
                // Display error message
                this.lblError.Text = ResHelper.GetString("general.erroroccurred") + ex.Message;
                this.lblError.Visible = true;
            }
        }
        else
        {
            // Display error message
            this.lblError.Text = errorMessage;
            this.lblError.Visible = true;
        }
    }

    #endregion


    #region "Private methods"


    /// <summary>
    /// Clears the fields of the form to default values.
    /// </summary>
    private void ClearForm()
    {
        this.txtDescription.Text = "";
        this.txtDisplayName.Text = "";
        this.radGroupMembers.Checked = false;
        this.radSiteMembers.Checked = false;
        this.radMembersApproved.Checked = false;
        this.radMembersInvited.Checked = false;
        this.radMembersAny.Checked = true;
        this.radAnybody.Checked = true;
    }


    /// <summary>
    /// Returns true iff current user is Global administrator or Community administrator.
    /// </summary>
    private bool CurrentUserIsAdmin()
    {
        CurrentUserInfo ui = CMSContext.CurrentUser;
        if (ui != null)
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(this.SiteID);
            if (si != null)
            {
                return ui.IsInRole("CMSCommunityAdmin", si.SiteName) || ui.IsGlobalAdministrator;
            }
        }
        return false;
    }


    /// <summary>
    /// Copies the sourcealiaspath document to targetaliaspath
    /// </summary>
    /// <param name="groupId">Group ID</param>
    private string CopyGroupDocument(GroupInfo gi)
    {
        if ((CMSContext.CurrentDocument != null) && (gi != null) && (gi.GroupID > 0))
        {
            int groupId = gi.GroupID;

            TreeProvider tp = new TreeProvider(CMSContext.CurrentUser);
            TreeNode nodeSource = tp.SelectSingleNode(CMSContext.CurrentSiteName, this.mGroupTemplateSourceAliasPath, CMSContext.CurrentDocument.DocumentCulture, this.mCombineWithDefaultCulture);
            TreeNode nodeTarget = tp.SelectSingleNode(CMSContext.CurrentSiteName, this.mGroupTemplateTargetAliasPath, CMSContext.CurrentDocument.DocumentCulture, this.mCombineWithDefaultCulture);
            if ((nodeSource != null) && (nodeTarget != null))
            {
                // Check allowed child class
                int targetClassId = ValidationHelper.GetInteger(nodeTarget.GetValue("NodeClassID"), 0);
                int sourceClassId = ValidationHelper.GetInteger(nodeSource.GetValue("NodeClassID"), 0);
                if (!DataClassInfoProvider.IsChildClassAllowed(targetClassId, sourceClassId))
                {
                    return ResHelper.GetString("group.registration.templatecopyfailed");
                }

                // Check cyclic copying (copying of the node to some of its child nodes)
                if (nodeTarget.NodeAliasPath.StartsWith(nodeSource.NodeAliasPath + "/", StringComparison.CurrentCultureIgnoreCase))
                {
                    return ResHelper.GetString("group.registration.templatecopyfailed");
                }

                TreeNode node = DocumentHelper.CopyDocument(nodeSource, nodeTarget.NodeID, true, tp, CMSContext.CurrentUser.UserID, groupId);
                node.DocumentName = codeName;
                node.NodeAlias = codeName;

                // Ensure user friendly menu caption
                node.DocumentMenuCaption = gi.GroupDisplayName;

                // Set group profile page url
                node.DocumentUrlPath = this.GroupProfileURLPath.ToLower().Replace("{groupname}", codeName);

                // Update copied menu item name
                node.SetValue("MenuItemName", codeName);

                // Update original values => Document alis for source document is not created
                bool originalValue = tp.EnableDocumentAliases;
                tp.EnableDocumentAliases = false;
                
                // Update document
                DocumentHelper.UpdateDocument(node, tp);

                // Restore original value
                tp.EnableDocumentAliases = originalValue;

                // Update search index for node
                if ((node.PublishedVersionExists) && (SearchIndexInfoProvider.SearchEnabled))
                {
                    SearchTaskInfoProvider.CreateTask(SearchTaskTypeEnum.Update, PredefinedObjectType.DOCUMENT, SearchHelper.ID_FIELD, node.GetSearchID());
                }

                // Check document culture, set current culture properties
                if (node.DocumentCulture != nodeSource.DocumentCulture)
                {
                    node = DocumentHelper.GetDocument(node.NodeID, nodeSource.DocumentCulture, tp);
                    if (node != null)
                    {
                        node.DocumentName = codeName;
                        node.NodeAlias = codeName;

                        // Update copied menu item name
                        node.SetValue("MenuItemName", codeName);

                        DocumentHelper.UpdateDocument(node, tp);

                        // Update search index for node
                        if ((node.PublishedVersionExists) && (SearchIndexInfoProvider.SearchEnabled))
                        {
                            SearchTaskInfoProvider.CreateTask(SearchTaskTypeEnum.Update, PredefinedObjectType.DOCUMENT, SearchHelper.ID_FIELD, node.GetSearchID());
                        }
                    }
                }

                // Set group node guid
                gi.GroupNodeGUID = node.NodeGUID;
                GroupInfoProvider.SetGroupInfo(gi);
            }
            else
            {
                return ResHelper.GetString("group.registration.templatecopyfailed");
            }
        }
        else
        {
            return ResHelper.GetString("group.registration.templatecopyfailed");
        }

        return "";
    }


    /// <summary>
    /// Initializes the contols in the form
    /// </summary>
    private void InitializeForm()
    {
        // Intialize labels
        this.lblDisplayName.Text = this.GroupNameLabelText;
        this.lblDescription.Text = ResHelper.GetString("general.description") + ResHelper.Colon;
        this.lblApproveMembers.Text = ResHelper.GetString("group.group.approvemembers") + ResHelper.Colon;
        this.lblContentAccess.Text = ResHelper.GetString("group.group.contentaccess") + ResHelper.Colon;

        // Initialize radiobuttons
        this.radAnybody.Text = ResHelper.GetString("group.group.accessanybody");
        this.radGroupMembers.Text = ResHelper.GetString("group.group.accessgroupmembers");
        this.radSiteMembers.Text = ResHelper.GetString("group.group.accesssitemembers");
        this.radMembersAny.Text = ResHelper.GetString("group.group.approveany");
        this.radMembersApproved.Text = ResHelper.GetString("group.group.approveapproved");
        this.radMembersInvited.Text = ResHelper.GetString("group.group.approveinvited");

        // Initialize errors
        this.rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");

        // Initialize buttons
        this.btnSave.Text = ResHelper.GetString("general.ok");
    }


    /// <summary>
    /// Returns correct number according to radiobutton selection
    /// </summary>
    private SecurityAccessEnum GetGroupAccess()
    {
        if (this.radSiteMembers.Checked)
        {
            return SecurityAccessEnum.AuthenticatedUsers;
        }
        else if (this.radGroupMembers.Checked)
        {
            return SecurityAccessEnum.GroupMembers;
        }
        else
        {
            return SecurityAccessEnum.AllUsers;
        }
    }


    /// <summary>
    /// Returns correct number according to radiobutton selection
    /// </summary>
    private GroupApproveMembersEnum GetGroupApproveMembers()
    {
        if (this.radMembersApproved.Checked)
        {
            return GroupApproveMembersEnum.ApprovedCanJoin;
        }
        else if (this.radMembersInvited.Checked)
        {
            return GroupApproveMembersEnum.InvitedWithoutApproval;
        }
        else
        {
            return GroupApproveMembersEnum.AnyoneCanJoin;
        }
    }


    /// <summary>
    /// Returns safe codename
    /// </summary>
    private string GetSafeCodeName()
    {
        return UrlHelper.GetSafeUrlPart(ValidationHelper.GetCodeName(this.txtDisplayName.Text.Trim(), null, 89, true, true), CMSContext.CurrentSiteName);
    }


    /// <summary>
    /// Returns safe and unique codename for specified display name
    /// </summary>
    private string GetUniqueCodeName(string codeName)
    {
        int maxLength = 89;
        codeName = codeName.Replace("'", "''");
        string originalCodename = codeName;

        int postfixValue = 1;

        // Loop until unique code name is not found
        while (true)
        {
            string where = "(GroupName = N'" + codeName + "'";
            if (this.mSiteId > 0)
            {
                where += " AND GroupSiteID = " + this.mSiteId;
            }
            where += ")";

            DataSet ds = GroupInfoProvider.GetGroups(where, null, 0, "groupid");
            // Default codename is unique
            if (DataHelper.DataSourceIsEmpty(ds))
            {
                return codeName;
            }

            codeName = GenerateCodeName(originalCodename, maxLength, "_" + postfixValue);
            postfixValue++;
        }
    }


    /// <summary>
    /// Generates unique codename with dependenc on maximal length and postfix value
    /// </summary>
    /// <param name="codeName">Codename value</param>
    /// <param name="maxLength">Maximal length</param>
    /// <param name="postFix">Postfix value</param>
    private string GenerateCodeName(string codeName, int maxLength, string postFix)
    {
        if ((codeName.Length + postFix.Length) < maxLength)
        {
            return codeName + postFix;
        }

        codeName = codeName.Substring(0, codeName.Length - postFix.Length);
        return codeName + postFix;
    }


    /// <summary>
    /// Validates the form entries
    /// </summary>
    /// <returns>Empty string if validation passed otherwise error message is returned</returns>
    private string ValidateForm()
    {
        string errorMessage = new Validator().NotEmpty(this.txtDisplayName.Text, this.rfvDisplayName.ErrorMessage).Result;

        if (errorMessage != "")
        {
            return errorMessage;
        }
        return errorMessage;
    }

    #endregion
}
