using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Web.Caching;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.FileManager;
using CMS.SiteProvider;
using CMS.URLRewritingEngine;
using CMS.PortalEngine;
using CMS.DataEngine;
using CMS.MediaLibrary;
using CMS.UIControls;
using CMS.DirectoryUtilities;
using CMS.SettingsProvider;

public partial class CMSPages_GetMediaFile : GetFilePage
{
    #region "Advanced settings"

    /// <summary>
    /// Set to false to disable the client caching
    /// </summary>
    protected bool useClientCache = true;

    /// <summary>
    /// Set to 0 if you do not wish to cache large files
    /// </summary>
    protected int largeFilesCacheMinutes = 1;

    #endregion


    #region "Variables"

    protected CMSOutputMediaFile outputFile = null;
    protected Guid fileGuid = Guid.Empty;
    protected MediaFileInfo fileInfo = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Returns true if file preview is used for sending.
    /// </summary>
    public bool Preview
    {
        get
        {
            return QueryHelper.GetBoolean("preview", false);
        }
    }


    /// <summary>
    /// Returns true if the process allows cache
    /// </summary>
    public override bool AllowCache
    {
        get
        {
            if (mAllowCache == null)
            {
                // By default, cache for the metafiles is based on the view mode
                if (ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSAlwaysCacheMediaFiles"], false))
                {
                    mAllowCache = true;
                }
                else
                {
                    mAllowCache = (this.ViewMode == ViewModeEnum.LiveSite);
                }
            }

            return mAllowCache.Value;
        }
        set
        {
            mAllowCache = value;
        }
    }
    
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        DebugHelper.SetContext("GetMediaFile");

        // Load the site name
        LoadSiteName();

        // Check the site
        if (string.IsNullOrEmpty(CurrentSiteName))
        {
            throw new Exception("[GetMediaFile.aspx]: Site not running.");
        }

        int cacheMinutes = this.CacheMinutes;

        // Try to get data from cache
        using (CachedSection<CMSOutputMediaFile> cs = new CachedSection<CMSOutputMediaFile>(ref outputFile, cacheMinutes, true, null, "getmediafile", this.CurrentSiteName, CMSContext.GetBaseCacheKey(true, false), Request.QueryString))
        {
            if (cs.LoadData)
            {
                // Process the file
                ProcessFile();

                // Ensure the cache settings
                if (cs.Cached)
                {
                    // Prepare the cache dependency
                    CacheDependency cd = null;
                    if (outputFile != null)
                    {
                        if (outputFile.MediaFile != null)
                        {
                            // Do not cache too big files
                            if ((outputFile.MediaFile != null) && !CacheHelper.CacheImageAllowed(CurrentSiteName, (int)outputFile.MediaFile.FileSize))
                            {
                                cacheMinutes = largeFilesCacheMinutes;
                            }

                            // Set dependency on this particular file
                            if (cacheMinutes > 0)
                            {
                                string[] dependencies = MediaFileInfoProvider.GetDependencyCacheKeys(outputFile.MediaFile, this.Preview);
                                cd = CacheHelper.GetCacheDependency(dependencies);
                            }
                        }
                    }

                    if ((cd == null) && (cacheMinutes > 0))
                    {
                        // Set default cache dependency by file GUID
                        cd = CacheHelper.GetCacheDependency(new string[] { "mediafile|" + fileGuid.ToString().ToLower(), "mediafilepreview|" + fileGuid.ToString().ToLower() });
                    }

                    // Cache the data
                    cs.CacheMinutes = cacheMinutes;
                    cs.CacheDependency = cd;
                    cs.Data = outputFile;
                }
            }
        }

        // Send the data
        SendFile(outputFile);

        DebugHelper.ReleaseContext();
    }


    /// <summary>
    /// Processes the file.
    /// </summary>
    protected void ProcessFile()
    {
        outputFile = null;

        // Get file guid from querystring
        fileGuid = QueryHelper.GetGuid("fileguid", Guid.Empty);
        if (fileGuid != Guid.Empty)
        {
            ProcessFile(fileGuid, this.Preview);
        }
    }


    /// <summary>
    /// Processes the specified file.
    /// </summary>
    /// <param name="fileGuid">File guid</param>
    /// <param name="preview">Use preview</param>
    protected void ProcessFile(Guid fileGuid, bool preview)
    {
        // Get the file info if doesn't retrieved yet
        fileInfo = ((fileInfo == null) ? MediaFileInfoProvider.GetMediaFileInfo(fileGuid, CurrentSiteName) : fileInfo);
        if (fileInfo != null)
        {
            #region "Security"

            // Check if user is allowed to see the library file content if required
            bool checkPermissions = SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSCheckMediaFilePermissions");
            if (checkPermissions)
            {
                // If required file was specified
                if (fileGuid != Guid.Empty)
                {
                    // Check the library access for the current user and stop file processing if not allowed
                    MediaLibraryInfo mli = MediaLibraryInfoProvider.GetMediaLibraryInfo(fileInfo.FileLibraryID);
                    if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(mli, "LibraryAccess"))
                    {
                        UrlHelper.Redirect(URLRewriter.AccessDeniedPageURL(CurrentSiteName));
                        return;
                    }
                }
            }

            #endregion

            if (preview)
            {
                // Get file path
                string path = MediaFileInfoProvider.GetMediaFilePath(fileInfo.FileLibraryID, fileInfo.FilePath);
                string pathDirectory = Path.GetDirectoryName(path);
                string hiddenFolderPath = MediaLibraryHelper.GetMediaFileHiddenFolder(CurrentSiteName);

                // Ensure hidden folder exists
                DirectoryHelper.EnsureDiskPath(pathDirectory + "\\" + hiddenFolderPath, pathDirectory);
                // Get preview file
                string[] files = Directory.GetFiles(pathDirectory + "\\" + hiddenFolderPath, MediaLibraryHelper.GetPreviewFileName(fileInfo.FileName, fileInfo.FileExtension, ".*", CurrentSiteName));
                if (files.Length > 0)
                {
                    bool resizeImage = (ImageHelper.IsImage(Path.GetExtension(files[0])) && MediaFileInfoProvider.CanResizeImage(files[0], this.Width, this.Height, this.MaxSideSize));

                    // Get the data
                    if ((outputFile == null) || (outputFile.MediaFile == null))
                    {
                        outputFile = new CMSOutputMediaFile(fileInfo, null);
                        outputFile.UsePreview = true;
                        outputFile.Width = this.Width;
                        outputFile.Height = this.Height;
                        outputFile.MaxSideSize = this.MaxSideSize;
                        outputFile.Resized = resizeImage;
                        outputFile.MediaFile.FileExtension = Path.GetExtension(files[0]);
                        outputFile.MimeType = MimeTypeHelper.GetMimetype(outputFile.MediaFile.FileExtension);
                    }
                }
            }
            else
            {
                bool resizeImage = (ImageHelper.IsImage(fileInfo.FileExtension) && MediaFileInfoProvider.CanResizeImage(fileInfo, this.Width, this.Height, this.MaxSideSize));

                // Get the data
                if ((outputFile == null) || (outputFile.MediaFile == null))
                {
                    outputFile = new CMSOutputMediaFile(fileInfo, null);
                    outputFile.Width = this.Width;
                    outputFile.Height = this.Height;
                    outputFile.MaxSideSize = this.MaxSideSize;
                    outputFile.Resized = resizeImage;
                }
            }
        }
    }


    /// <summary>
    /// Sends the given file within response
    /// </summary>
    /// <param name="file">File to send</param>
    protected void SendFile(CMSOutputMediaFile file)
    {
        // Clear response.
        Response.Cookies.Clear();
        Response.Clear();

        Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);

        if (file != null)
        {
            // Prepare etag
            string etag = "";
            if (file.MediaFile != null)
            {
                etag += file.MediaFile.FileModifiedWhen.ToUniversalTime();
            }

            // Put etag into ""
            etag = "\"" + etag + "\"";

            // Client caching - only on the live site
            if (useClientCache && AllowCache && CacheHelper.CacheImageEnabled(CurrentSiteName))
            {
                // Determine the last modified date and etag sent from the browser
                string currentETag = Request.Headers["If-None-Match"];
                string ifModifiedString = Request.Headers["If-Modified-Since"];
                if ((ifModifiedString != null) && (currentETag == etag))
                {
                    // IE-browsers send something like this:
                    // If-Modified-Since: Wed, 19 Jul 2006 11:19:59 GMT; length=3350
                    DateTime ifModified;
                    if (DateTime.TryParse(ifModifiedString.Split(";".ToCharArray())[0], out ifModified))
                    {
                        // If not changed, let the browser use the cached data
                        if (file.LastModified <= ifModified.AddSeconds(1))
                        {
                            RespondNotModified(etag, true);
                            return;
                        }
                    }
                }
            }
            else
            {
                SetCacheability();
            }

            // If physical file not present, try to load
            bool fileLoaded = EnsurePhysicalFile(outputFile);

            // If the output data should be cached, return the output data
            bool cacheOutputData = false;
            if ((file.MediaFile != null) && (this.CacheMinutes > 0))
            {
                cacheOutputData = CacheHelper.CacheImageAllowed(CurrentSiteName, (int)file.MediaFile.FileSize);
            }

            // Ensure the file data if physical file not present
            if (!file.DataLoaded && !fileLoaded)
            {
                byte[] cachedData = GetCachedOutputData();
                if (file.EnsureData(cachedData))
                {
                    if ((cachedData == null) && cacheOutputData)
                    {
                        SaveOutputDataToCache(file.OutputData, GetOutputDataDependency(file.MediaFile));
                    }
                }
            }

            // Send the file
            if ((file.OutputData != null) || (file.PhysicalFile != ""))
            {
                // Setup the mime type - Fix the special types
                string mimetype = file.MimeType;
                string extension = file.MediaFile.FileExtension;
                switch (extension.ToLower())
                {
                    case ".flv":
                        mimetype = "video/x-flv";
                        break;
                }

                // Prepare response
                Response.ContentType = mimetype;
                SetDisposition(file.MediaFile.FileName + extension, extension);

                // Setup Etag property
                ETag = etag;

                // Set if resumable downloads should be supported
                AcceptRange = !IsExtensionExcludedFromRanges(extension);

                if (useClientCache && AllowCache && (CacheHelper.CacheImageEnabled(CurrentSiteName)))
                {
                    DateTime expires = DateTime.Now;

                    // Send last modified header to allow client caching
                    Response.Cache.SetLastModified(file.LastModified);

                    Response.Cache.SetCacheability(HttpCacheability.Public);
                    if (DocumentBase.AllowClientCache() && DocumentBase.UseFullClientCache)
                    {
                        expires = DateTime.Now.AddMinutes(CacheHelper.CacheImageMinutes(CurrentSiteName));
                    }

                    Response.Cache.SetExpires(expires);
                    Response.Cache.SetETag(etag);
                }

                // Add the file data
                if ((file.PhysicalFile != "") && (file.OutputData == null))
                {
                    // Stream the file from the file system
                    file.OutputData = WriteFile(file.PhysicalFile, cacheOutputData);
                }
                else
                {
                    // Use output data of the file in memory if present
                    WriteBytes(file.OutputData);
                }
            }
            else
            {
                Response.Write("<html></html>");
            }
        }
        else
        {
            Response.Write("<html></html>");
        }

        CompleteRequest();
    }


    /// <summary>
    /// Returns the output data dependency based on the given attachment record
    /// </summary>
    /// <param name="mi">MediaFile object</param>
    protected CacheDependency GetOutputDataDependency(MediaFileInfo mi)
    {
        if (mi == null)
        {
            return null;
        }

        return CacheHelper.GetCacheDependency(MediaFileInfoProvider.GetDependencyCacheKeys(mi, this.Preview));
    }


    /// <summary>
    /// Ensures the physical file
    /// </summary>
    /// <param name="file">Output file</param>
    public bool EnsurePhysicalFile(CMSOutputMediaFile file)
    {
        // Try to link to file system
        if ((file != null) && (file.MediaFile != null) && (file.MediaFile.FileID > 0))
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(file.MediaFile.FileSiteID);
            if (si != null)
            {
                bool generateThumbnails = ValidationHelper.GetBoolean(SettingsKeyProvider.GetStringValue(si.SiteName + ".CMSGenerateThumbnails"), true);
                string filePath = null;

                if (file.Resized && generateThumbnails)
                {
                    filePath = MediaFileInfoProvider.EnsureThumbnailFile(file.MediaFile, file.SiteName, this.Width, this.Height, this.MaxSideSize, file.UsePreview);
                }
                else
                {
                    if (file.UsePreview)
                    {
                        string hiddenFolderName = SettingsKeyProvider.GetStringValue(si.SiteName + ".CMSMediaFileHiddenFolder");
                        string previewSuffix = SettingsKeyProvider.GetStringValue(si.SiteName + ".CMSMediaFilePreviewSuffix");
                        string hiddenFolder = MediaLibraryInfoProvider.GetMediaLibraryFolderPath(file.MediaFile.FileLibraryID);
                        string fileDir = Path.GetDirectoryName(file.MediaFile.FilePath);
                        string relativePath = "";
                        if (!String.IsNullOrEmpty(fileDir))
                        {
                            hiddenFolder += "\\" + fileDir;
                            relativePath += "\\" + fileDir;
                        }
                        if (!hiddenFolder.EndsWith(hiddenFolderName))
                        {
                            hiddenFolder += "\\" + hiddenFolderName;
                            relativePath += "\\" + hiddenFolderName;
                        }
                        string searchPattern = file.MediaFile.FileName + "_*" + previewSuffix + ".*";
                        if (Directory.Exists(hiddenFolder))
                        {
                            string[] preview = Directory.GetFiles(hiddenFolder, searchPattern);
                            if ((preview != null) && (preview.Length > 0))
                            {
                                if (!String.IsNullOrEmpty(fileDir))
                                {
                                    filePath = fileDir + "\\";
                                }
                                filePath = relativePath + "\\" + Path.GetFileName(preview[0]);
                            }
                        }

                    }
                    else
                    {
                        filePath = file.MediaFile.FilePath;
                    }
                }

                if (filePath != null)
                {
                    // Link to the physical file
                    file.InvalidatePhysicalPath();
                    file.MediaFile.FilePath = filePath;
                    return true;
                }
            }
        }
        return false;
    }
}
