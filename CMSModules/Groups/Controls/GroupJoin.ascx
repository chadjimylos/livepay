<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupJoin.ascx.cs" Inherits="CMSModules_Groups_Controls_GroupJoin" %>
<table class="JoinTable">
    <tr>
        <td colspan="2" class="InfoArea">
            <asp:Label runat="server" ID="lblInfo" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td colspan="2" class="ButtonsArea">
            <cms:CMSButton runat="server"  CssClass="ContentButton" ID="btnJoin" OnClick="btnJoin_Click" EnableViewState="false" />&nbsp;<cms:CMSButton
                runat="server" CssClass="ContentButton" ID="btnCancel" EnableViewState="false" />
        </td>
    </tr>
</table>
