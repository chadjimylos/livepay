using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.PortalEngine;
using CMS.DataEngine;
using CMS.IDataConnectionLibrary;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_General : SiteManagerPage
{
    private int documentTypeId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // gets classID from querystring        
        documentTypeId = QueryHelper.GetInteger("documenttypeid", 0);

        // initializes labels
        lblFullCodeName.Text = ResHelper.GetString("DocumentType_Edit_General.FullCodeName");
        lblCodeName.Text = ResHelper.GetString("DocumentType_Edit_General.CodeName");
        lblNamespaceName.Text = ResHelper.GetString("DocumentType_Edit_General.Namespace");
        lblDisplayName.Text = ResHelper.GetString("DocumentType_Edit_General.DisplayName");
        lblEditingPage.Text = ResHelper.GetString("DocumentType_Edit_General.EditingPage");
        lblNewPage.Text = ResHelper.GetString("DocumentType_Edit_General.NewPage");
        lblListPage.Text = ResHelper.GetString("DocumentType_Edit_General.ListPage");
        lblTemplateSelection.Text = ResHelper.GetString("DocumentType_Edit_General.TemplateSelection");
        lblViewPage.Text = ResHelper.GetString("DocumentType_Edit_General.ViewPage");
        lblPreviewPage.Text = ResHelper.GetString("DocumentType_Edit_General.PreviewPage");
        lblClassUsePublishFromTo.Text = ResHelper.GetString("DocumentType_Edit_General.UsePublishFromTo");
        lblIsMenuItem.Text = ResHelper.GetString("DocumentType_Edit_General.IsMenuItem");
        lblDefaultTemplate.Text = ResHelper.GetString("DocumentType_Edit_General.DefaultTemplate");
        lblIsProduct.Text = ResHelper.GetString("DocumentType_Edit_General.IsProduct");
        lblTableName.Text = ResHelper.GetString("class.TableName");

        this.lblLoadGeneration.Text = ResHelper.GetString("LoadGeneration.Title");
        this.plcLoadGeneration.Visible = SettingsKeyProvider.DevelopmentMode;

        btnOk.Text = ResHelper.GetString("general.ok");

        // initializes validators
        RequiredFieldValidatorDisplayName.ErrorMessage = ResHelper.GetString("DocumentType_Edit_General.DisplayNameRequired");
        RequiredFieldValidatorNamespaceName.ErrorMessage = ResHelper.GetString("DocumentType_Edit_General.NamespaceNameRequired");
        RequiredFieldValidatorCodeName.ErrorMessage = ResHelper.GetString("DocumentType_Edit_General.CodeNameRequired");
        RegularExpressionValidatorNameSpaceName.ErrorMessage = ResHelper.GetString("DocumentType_Edit_General.NamespaceNameIdentificator");
        RegularExpressionValidatorCodeName.ErrorMessage = ResHelper.GetString("DocumentType_Edit_General.CodeNameIdentificator");

        // sets regular expression for identificator validation
        RegularExpressionValidatorNameSpaceName.ValidationExpression = "^([A-Za-z]|_[A-Za-z])\\w*$";
        RegularExpressionValidatorCodeName.ValidationExpression = "^([A-Za-z]|_[A-Za-z])\\w*$";

        if (!RequestHelper.IsPostBack())
        {
            // loads data to the form
            LoadData();
        }
    }


    /// <summary>
    /// Loads data of edited document type from DB.
    /// </summary>
    protected void LoadData()
    {
        if (documentTypeId > 0)
        {
            DataClassInfo classInfo = DataClassInfoProvider.GetDataClass(documentTypeId);

            string className = classInfo.ClassName;
            string[] fullName = ClassNameParser(className);

            tbDisplayName.Text = classInfo.ClassDisplayName;
            tbNamespaceName.Text = fullName[0];
            tbCodeName.Text = fullName[1];
            tbEditingPage.Text = classInfo.ClassEditingPageURL;
            tbListPage.Text = classInfo.ClassListPageURL;
            txtViewPage.Text = classInfo.ClassViewPageUrl;
            txtPreviewPage.Text = classInfo.ClassPreviewPageUrl;
            txtNewPage.Text = classInfo.ClassNewPageURL;
            chkClassUsePublishFromTo.Checked = classInfo.ClassUsePublishFromTo;
            chkTemplateSelection.Checked = classInfo.ClassShowTemplateSelection;
            chkIsMenuItem.Checked = classInfo.ClassIsMenuItemType;
            chkIsProduct.Checked = classInfo.ClassIsProduct;
            this.templateDefault.PageTemplateID = classInfo.ClassDefaultPageTemplateID;
            chkTemplateSelection_CheckedChanged(null, null);
            this.lblTableNameValue.Text = classInfo.ClassTableName;
            this.drpGeneration.Value = classInfo.ClassLoadGeneration;
        }
    }


    /// <summary>
    /// Parses the class name into the string array
    /// </summary>
    /// <param name="className">Class name to parse</param>
    private static string[] ClassNameParser(string className)
    {
        string[] fullName = new string[2];
        // finds last dot in full class name
        int lastDot = className.LastIndexOf('.');

        if (lastDot > 0)
        {
            // full class name = namespace + '.' + class name
            fullName[0] = className.Substring(0, lastDot);
            fullName[1] = className.Substring(lastDot + 1, className.Length - lastDot - 1);
        }
        else
        {
            // full transformation name = query name
            fullName[0] = "";
            fullName[1] = className;
        }

        return fullName;
    }


    /// <summary>
    /// Handles the ButtonOK's onClick event
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // finds whether required fields are not empty or well filled
        string result = new Validator().NotEmpty(tbDisplayName.Text, ResHelper.GetString("DocumentType_Edit_General.DisplayNameRequired")).
            NotEmpty(tbNamespaceName.Text, ResHelper.GetString("DocumentType_Edit_General.NamespaceNameRequired")).
            NotEmpty(tbCodeName.Text, ResHelper.GetString("DocumentType_Edit_General.CodeNameRequired")).
            IsIdentificator(tbNamespaceName.Text, ResHelper.GetString("DocumentType_Edit_General.NamespaceNameIdentificator")).
            IsCodeName(tbCodeName.Text, ResHelper.GetString("DocumentType_Edit_General.CodeNameIdentificator")).Result;

        // Get full class name
        string newClassName = tbNamespaceName.Text.Trim() + "." + tbCodeName.Text.Trim();
        DataClassInfo classInfo = DataClassInfoProvider.GetDataClass(documentTypeId);

        // Check if class exists
        if (classInfo == null)
        {
            return;
        }

        string className = classInfo.ClassName;
        
        // Check if new class name is unique
        if (String.Compare(className,newClassName, true) != 0)
        {
            DataClassInfo ci = DataClassInfoProvider.GetDataClass(newClassName);
            if ((ci != null) && (ci != classInfo))
            {
                result += String.Format(ResHelper.GetString("class.exists"), newClassName);
            }
        }

        if (result == "")
        {
            // sets properties of the classInfo
            classInfo.ClassDisplayName = tbDisplayName.Text;
            classInfo.ClassName = newClassName;
            classInfo.ClassNewPageURL = txtNewPage.Text;
            classInfo.ClassEditingPageURL = tbEditingPage.Text;
            classInfo.ClassListPageURL = tbListPage.Text;
            classInfo.ClassViewPageUrl = txtViewPage.Text;
            classInfo.ClassPreviewPageUrl = txtPreviewPage.Text;
            classInfo.ClassUsePublishFromTo = chkClassUsePublishFromTo.Checked;
            classInfo.ClassShowTemplateSelection = chkTemplateSelection.Checked;
            classInfo.ClassIsMenuItemType = chkIsMenuItem.Checked;
            classInfo.ClassDefaultPageTemplateID = templateDefault.PageTemplateID;
            classInfo.ClassIsProduct = chkIsProduct.Checked;
            classInfo.ClassLoadGeneration = drpGeneration.Value;

            // Refresh the tabs according to ClassIsProduct setting
            ltlScript.Text = ScriptHelper.GetScript("parent.frames['menu'].location.replace(parent.frames['menu'].location);");

            try
            {
                // updates dataclass in DB (inner unique classname check)
                DataClassInfoProvider.SetDataClass(classInfo);

                if ((className.ToLower() != classInfo.ClassName.ToLower()) && (classInfo.ClassIsDocumentType))
                {
                    // Create view for document types
                    SqlGenerator.GenerateDefaultView(classInfo, null);

                    // Class name was changed -> update queries
                    SqlGenerator.GenerateQuery(classInfo.ClassName, "searchtree", SqlOperationTypeEnum.SearchTree, false);
                    SqlGenerator.GenerateQuery(classInfo.ClassName, "selectdocuments", SqlOperationTypeEnum.SelectDocuments, false);
                    SqlGenerator.GenerateQuery(classInfo.ClassName, "selectversions", SqlOperationTypeEnum.SelectVersions, false);
                }

                // Actualize doctype icon
                string sourceFile = GetDocumentTypeIconUrl(className, false);
                string destFile = GetDocumentTypeIconUrl(classInfo.ClassName, false);

                // Rename icon file
                if (File.Exists(Server.MapPath(sourceFile)))
                {
                    if (className.ToLower() != classInfo.ClassName.ToLower())
                    {
                        try
                        {
                            File.Move(Server.MapPath(sourceFile), Server.MapPath(destFile));
                        }
                        catch
                        {
                        }
                    }
                }
                // Create new icon file
                else
                {
                    if (!classInfo.ClassIsCoupledClass)
                    {
                        sourceFile = GetDocumentTypeIconUrl("defaultcontainer", false);
                    }
                    else
                    {
                        sourceFile = GetDocumentTypeIconUrl("default", false);
                    }

                    try
                    {
                        File.Copy(Server.MapPath(sourceFile), Server.MapPath(destFile));
                    }
                    catch
                    {
                    }
                }

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
            catch (Exception ex)
            {
                // Most probably exception with non unique class name
                lblError.Text = ex.Message;
                lblError.Visible = true;
            }

        }
        else
        {
            // hides asp validators in case the javascript is disabled
            RequiredFieldValidatorDisplayName.Visible = false;
            RequiredFieldValidatorNamespaceName.Visible = false;
            RequiredFieldValidatorCodeName.Visible = false;
            RegularExpressionValidatorNameSpaceName.Visible = false;
            RegularExpressionValidatorCodeName.Visible = false;

            lblError.Visible = true;
            lblError.Text = result;
        }
    }


    protected void chkTemplateSelection_CheckedChanged(object sender, EventArgs e)
    {
        this.templateDefault.Enabled = !this.chkTemplateSelection.Checked;
        if (!this.templateDefault.Enabled)
        {
            templateDefault.PageTemplateID = 0;
        }
    }
}
