<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UICulture_List.aspx.cs" Inherits="CMSSiteManager_Development_UICultures_UICulture_List"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false" /><br />
    <cms:UniGrid ID="UniGridUICultures" runat="server" GridName="UICulture_List.xml"
        IsLiveSite="false" />
</asp:Content>
