<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Library_Edit_Files.aspx.cs"
    EnableEventValidation="false" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master"
    Title="Media library - Files" Inherits="CMSModules_MediaLibrary_Tools_Library_Edit_Files"
    Theme="Default" %>

<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/MediaLibrary.ascx"
    TagName="MediaLibrary" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:ScriptManager ID="scriptManager" runat="server">
    </asp:ScriptManager>
    <asp:Panel runat="server" ID="pnlTabsBody" CssClass="TabsPageBody">
        <asp:Panel runat="server" ID="pnlTabsScroll" CssClass="TabsPageScrollArea">
            <asp:Panel runat="server" ID="pnltab" CssClass="tabspagecontent">
                <asp:UpdatePanel ID="pnlUpdateSelectMedia" runat="server" UpdateMode="Conditional"
                    ChildrenAsTriggers="false">
                    <ContentTemplate>
                        <cms:MediaLibrary ID="libraryElem" runat="server" DisplayFilesCount="true" IsLiveSite="false" DisplayMode="Simple" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>

    <script type="text/javascript">
            //<![CDATA[
            function Refresh() {
              window.location.replace(window.location);
            }
            //]]>
    </script>

</asp:Content>
