<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PriceSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_Controls_UI_PriceSelector" %>
<asp:TextBox ID="txtPrice" runat="server" CssClass="TextBoxField" MaxLength="20"
    EnableViewState="false" /><asp:Literal runat="server" ID="ltlBreak" EnableViewState="false" />
<asp:RangeValidator ID="rvPrice" runat="server" ControlToValidate="txtPrice" MaximumValue="9999999999"
    MinimumValue="-9999999999" Type="Double" Display="Dynamic" EnableViewState="false" />
<asp:RequiredFieldValidator ID="rfvPrice" runat="server" ControlToValidate="txtPrice"
    Display="Dynamic" EnableViewState="false" />