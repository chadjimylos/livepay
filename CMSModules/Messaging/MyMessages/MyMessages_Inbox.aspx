<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyMessages_Inbox.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Messaging_MyMessages_MyMessages_Inbox"
    Title="My messages - Inbox" ValidateRequest="false" Theme="Default" %>

<%@ Register Src="~/CMSModules/Messaging/Controls/Inbox.ascx" TagName="Inbox" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:Inbox ID="inboxElem" runat="server" IsLiveSite="false" />
</asp:Content>
