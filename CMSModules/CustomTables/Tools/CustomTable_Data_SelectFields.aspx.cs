using System;
using System.Collections;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.FormEngine;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_CustomTables_Tools_CustomTable_Data_SelectFields : CMSCustomTablesModalPage
{
    #region "Variables"

    protected int customTableId = 0;
    private DataClassInfo dci = null;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentMaster.Title.TitleText = ResHelper.GetString("customtable.data.selectdisplayedfields");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_CustomTables/selectfields.png");
        CurrentMaster.DisplayActionsPanel = true;

        // Get custom table id from url
        customTableId = QueryHelper.GetInteger("customtableid", 0);

        dci = DataClassInfoProvider.GetDataClass(customTableId);

        // If class doesn't exists do nothing
        if (dci == null)
        {
            return;
        }

        // Check 'Read' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.customtables", "Read") &&
            !CMSContext.CurrentUser.IsAuthorizedPerClassName(dci.ClassName, "Read"))
        {
            lblError.Visible = true;
            lblError.Text = String.Format(ResHelper.GetString("customtable.permissiondenied.read"), dci.ClassName);
            plcContent.Visible = false;
            return;
        }

        btnSelectAll.Text = ResHelper.GetString("customtable.data.selectall");

        Hashtable reportFields = new Hashtable();
        FormInfo fi = new FormInfo();


        if (!RequestHelper.IsPostBack())
        {
            btnOk.Text = ResHelper.GetString("General.OK");
            btnCancel.Text = ResHelper.GetString("General.Cancel");

            if (dci != null)
            {
                // Get report fields
                if (!String.IsNullOrEmpty(dci.ClassShowColumns))
                {
                    reportFields.Clear();

                    foreach (string field in dci.ClassShowColumns.Split(';'))
                    {
                        // Add field key to hastable
                        reportFields[field] = null;
                    }
                }

                // Get columns names
                fi.LoadXmlDefinition(dci.ClassFormDefinition);
                string[] columnNames = fi.GetColumnNames();

                if (columnNames != null)
                {
                    foreach (string name in columnNames)
                    {
                        FormFieldInfo ffi = fi.GetFormField(name);

                        // Add checkboxes to the list
                        ListItem item = new ListItem(ResHelper.LocalizeString(GetFieldCaption(ffi, name)), name);
                        if (reportFields.Contains(name))
                        {
                            // Select checkbox if field is reported
                            item.Selected = true;
                        }
                        chkListFields.Items.Add(item);
                    }
                }
            }
        }
    }

    #endregion


    #region "Button handling"

    /// <summary>
    /// Button OK clicked.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (dci != null)
        {
            string reportFields = string.Empty;
            bool noItemSelected = (chkListFields.SelectedIndex == -1) ? true : false;

            foreach (ListItem item in chkListFields.Items)
            {
                // Display all fields
                if (noItemSelected)
                {
                    reportFields += item.Value + ";";
                }
                // Display only selected fields
                else if (item.Selected)
                {
                    reportFields += item.Value + ";";
                }
            }

            if (reportFields != string.Empty)
            {
                // Remove ending ';'
                reportFields = reportFields.TrimEnd(';');
            }


            // Save report fields
            dci.ClassShowColumns = reportFields;
            DataClassInfoProvider.SetDataClass(dci);

            // Close dialog window
            ltlScript.Text = ScriptHelper.GetScript("CloseAndRefresh();");
        }
    }

    #endregion


    #region "Protected methods"

    /// <summary>
    /// Returns field caption of the specified column.
    /// </summary>
    /// <param name="ffi">Form field info.</param>
    /// <param name="columnName">Column name.</param>    
    protected string GetFieldCaption(FormFieldInfo ffi, string columnName)
    {
        // Get field caption        
        return ((ffi == null) || (ffi.Caption == string.Empty)) ? columnName : ffi.Caption;
    }

    #endregion
}
