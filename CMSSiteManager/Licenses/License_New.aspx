<%@ Page Language="C#" AutoEventWireup="true" CodeFile="License_New.aspx.cs" Inherits="CMSSiteManager_Licenses_License_New"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Licenses - New License" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <asp:Label ID="lblInfo" runat="server" />
    <asp:Label runat="server" ID="lblError" ForeColor="red" EnableViewState="false" Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <asp:Label ID="lbLicenseKey" runat="server" EnableViewState="False" /></td>
            <td>
                <asp:TextBox ID="tbLicenseKey" runat="server" CssClass="TextAreaHigh" TextMode="MultiLine"
                    MaxLength="100" Width="600" />
                <asp:RequiredFieldValidator ID="rfvLicenseKey" runat="server" EnableViewState="false"
                    ControlToValidate="tbLicenseKey" Display="dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton"
                    EnableViewState="false" />
            </td>
        </tr>
    </table>
</asp:Content>
