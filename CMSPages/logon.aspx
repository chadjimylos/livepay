<%@ Page Language="C#" AutoEventWireup="true" CodeFile="logon.aspx.cs" Inherits="CMSPages_logon"
    Theme="Default" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>CMS Login</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 98% !important;
        }
                     
        form
        {
            padding: 0px;
        }
    </style>
    <base target="_self" />
</head>
<body class="<%=mBodyClass%> LogonPageBody">
    <div class="loginMargin">
        <div class="loginContainer">
            <div class="loginBox">
                <div class="LogonFormLogo"></div>
                <form id="form1" runat="server">
                <asp:Panel ID="pnlBody" runat="server" CssClass="LogonPageBackground">
                    <asp:Login ID="Login1" runat="server" DestinationPageUrl="~/Default.aspx">
                        <LayoutTemplate>
                            <%-- <asp:Label ID="lblLogonTitle" runat="server" CssClass="LogonTitle" Text="Please log on" />--%>
                            <asp:Panel ID="pnlContainer" runat="server" DefaultButton="LoginButton">
                                <div class="LogonData">
                                    <table cellpadding="1" cellspacing="0" style="width: 358px;">
                                        <tr>
                                            <td colspan="2" class="LogonTitle">
                                                <cms:LocalizedLabel ID="lblLogOn" runat="server" ResourceString="LogonForm.LogOn" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 84px;">
                                                <cms:LocalizedLabel ID="lblUserName" runat="server" AssociatedControlID="UserName" />
                                            </td>
                                            <td style="width: 260px;">
                                                <asp:TextBox ID="UserName" runat="server" MaxLength="100" CssClass="LogonTextBox" />
                                                <asp:RequiredFieldValidator ID="rfvUserNameRequired" runat="server" ControlToValidate="UserName"
                                                    ValidationGroup="Login1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 84px;">
                                                <cms:LocalizedLabel ID="lblPassword" runat="server" AssociatedControlID="Password" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Password" runat="server" TextMode="Password" MaxLength="100" CssClass="LogonTextBox" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <cms:LocalizedCheckBox ID="chkRememberMe" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="center" style="color: Red; height: 40px;">
                                                <cms:LocalizedLabel ID="FailureText" runat="server" EnableViewState="False" />
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="LogonButtons">
                                    <table width="100%" cellpadding="1" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="lnkLanguage" runat="server" Visible="false" />
                                            </td>
                                            <td class="LanguageSelectorTd">
                                                <div id="pnlLanguage" runat="server" class="LogonDialog" style="display: none;">
                                                    <asp:DropDownList ID="drpCulture" runat="server" CssClass="LogonDropDownList" />
                                                </div>
                                            </td>
                                            <td class="LogonButtonTd">
                                                <cms:LocalizedButton ID="LoginButton" runat="server" CommandName="Login" ValidationGroup="Login1"
                                                    CssClass="LogonButton" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                        </LayoutTemplate>
                    </asp:Login>
                </asp:Panel>
                <asp:Literal ID="ltlScript" EnableViewState="false" runat="server" />
                </form>
                <div class="loginLine">
                    <asp:Label ID="lblVersion" runat="server" />
                </div>
            </div>
        </div>
    </div>
</body>
</html>
