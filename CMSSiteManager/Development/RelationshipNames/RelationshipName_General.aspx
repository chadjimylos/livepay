<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RelationshipName_General.aspx.cs"
    Inherits="CMSSiteManager_Development_RelationshipNames_RelationshipName_General"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Relationship names - new" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" ForeColor="red" EnableViewState="false" Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblRelationshipNameDisplayName" runat="server" EnableViewState="false"
                    ResourceString="RelationshipNames.DisplayName" />
            </td>
            <td>
                <asp:TextBox ID="txtRelationshipNameDisplayName" runat="server" CssClass="TextBoxField"
                    MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDisplayName" runat="server"
                    EnableViewState="false" ControlToValidate="txtRelationshipNameDisplayName" Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblRelationshipNameCodeName" runat="server" EnableViewState="false"
                    ResourceString="RelationshipNames.CodeName" />
            </td>
            <td>
                <asp:TextBox ID="txtRelationshipNameCodeName" runat="server" CssClass="TextBoxField"
                    MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodeName" runat="server" EnableViewState="false"
                    ControlToValidate="txtRelationshipNameCodeName" Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblRelationshipNameType" runat="server" EnableViewState="false"
                    ResourceString="RelationshipNames.Type" />
            </td>
            <td>
                <asp:DropDownList ID="drpRelType" runat="server" AutoPostBack="false" CssClass="DropDownField" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:LocalizedButton ID="btnOk" runat="server" OnClick="btnOk_Click" CssClass="SubmitButton"
                    EnableViewState="false" ResourceString="General.OK" />
            </td>
        </tr>
    </table>
</asp:Content>
