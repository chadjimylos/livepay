var fopener = null;

function GetFOpener() {
    if (wopener != null) {
        fopener = wopener;
    }
    if ((fopener == null) || (fopener.FCK == null)) {
        fopener = (window.top.opener ? window.top.opener : window.top.dialogArguments);
    }
}


function InsertSelectedItem(obj) {
    if (obj) {
        obj = UnescapeChars(obj);
        // Get FCK opener window
        GetFOpener();
        if ((fopener != null) && (fopener.FCK != null)) {
            if (obj.img_url) {
                if (obj.img_behavior == "hover") {
                    // Create inline img
                    fopener.FCKMedia.Add(obj);
                }
                else {
                    var selElem = fopener.FCK.Selection.GetSelectedElement();
                    if (!selElem) {
                        selElem = fopener.FCK.Selection.GetBoundaryParentElement();
                    }
                    var imgElem = null;
                    if ((obj.img_link != null) && (obj.img_link != '')) {
                        var link = GetLinkElement(selElem);
                        if (link != null) {
                            link.href = obj.img_link;
                            link.setAttribute("_fcksavedurl", obj.img_link);
                            if ((obj.img_target != null) && (obj.img_target != '')) {
                                link.target = obj.img_target;
                            }
                            else if ((obj.img_behavior != null) && (obj.img_behavior != '')) {
                                link.target = obj.img_behavior;
                            }
                            // Create img html
                            imgElem = createImageElem(obj, fopener.FCK.EditorDocument);
                        }
                        else {
                            // Create img html with link
                            imgElem = createImageElem(obj, fopener.FCK.EditorDocument, true);
                        }
                    }
                    else {
                        // Create img html
                        imgElem = createImageElem(obj, fopener.FCK.EditorDocument);
                        // Clear existing links
                        fopener.FCK.ExecuteNamedCommand('Unlink', null, false, true);
                    }

                    // Add the append part
                    var append = fopener.FCKConfig.AppendToImagePath;
                    if ((append != null) && (append != "")) {
                        var url = imgElem.getAttribute('src');

                        var re = new RegExp('[&?]' + append, 'i');
                        if (!re.test(url)) {
                            if (/\?/.test(url)) {
                                url += '&' + append;
                            }
                            else {
                                url += '?' + append;
                            }
                        }

                        imgElem.setAttribute('src', url);
                    }
                    
                    fopener.FCK.InsertElement(imgElem);
                }
            }
            else {
                if ((obj.link_url) || (obj.anchor_name) || (obj.email_to)) {
                    // Create link
                    fopener.FCKLink.Add(obj);
                }
                else {
                    if (obj.youtube_url) {
                        // Create youtube inline control
                        fopener.FCKYouTubeVideo.Add(obj);
                    }
                    else {
                        // Create inline control
                        fopener.FCKMedia.Add(obj);
                    }
                }
            }
        }
    }
}


function GetSelectedItem() {
    GetFOpener();
    var obj = null;
    if ((fopener != null) && (fopener.FCK != null)) {
        var editorInst = fopener.FCKeditorAPI.GetInstance(fopener.FCK_ACTIVE);
        var selElem = fopener.FCK.Selection.GetSelectedElement();
        if (!selElem) {
            selElem = fopener.FCK.Selection.GetBoundaryParentElement();
        }
        if (selElem) {
            if (selElem.tagName.toLowerCase() == "img") {
                if (selElem.getAttribute('_fckmedia')) {
                    var origElem = fopener.FCK.GetRealElement(selElem);
                    obj = new Object();
                    obj = fopener.FCKMedia.Parse(origElem.data);
                    if (obj.img_url != null) {
                        // Get link from selected Elem
                        obj = GetLinkObj(obj, selElem);
                    }
                }
                else if (selElem.getAttribute('_fckyoutube')) {
                    var origElem = fopener.FCK.GetRealElement(selElem);
                    obj = new Object();
                    obj = fopener.FCKYouTubeVideo.Parse(origElem.data);
                    if (obj.youtube_url != null) {
                        // Get link from selected Elem
                        obj = GetLinkObj(obj, selElem);
                    }
                }
                else {
                    obj = new Object();
                    obj.img_url = selElem.getAttribute('_fcksavedurl');
                    if (obj.img_url == null) {
                        obj.img_url = selElem.src;
                    }
                    obj.img_alt = selElem.alt;
                    obj.img_width = (parseInt(selElem.style.width, 10) > 0 ? parseInt(selElem.style.width, 10) : parseInt(selElem.width, 10));
                    obj.img_height = (parseInt(selElem.style.height, 10) > 0 ? parseInt(selElem.style.height, 10) : parseInt(selElem.height, 10));
                    obj.img_borderwidth = (parseInt(selElem.style.borderWidth, 10) > 0 ? parseInt(selElem.style.borderWidth, 10) : parseInt(selElem.border, 10));
                    obj.img_bordercolor = GetColor(selElem.style.borderColor);
                    obj.img_align = (selElem.align ? selElem.align : (selElem.style.cssFloat ? selElem.style.cssFloat : (selElem.style.styleFloat ? selElem.style.styleFloat : (selElem.style['vartical-align'] ? selElem.style['vartical-align'] : selElem.style.verticalAlign))));
                    obj.img_hspace = (parseInt(selElem.hspace, 10) > 0 ? selElem.hspace : selElem.style.marginLeft);
                    obj.img_vspace = (parseInt(selElem.vspace, 10) > 0 ? selElem.vspace : selElem.style.marginTop);
                    // Advanced tab
                    obj.img_id = selElem.id;
                    obj.img_tooltip = selElem.title;
                    obj.img_class = selElem.className;

                    // Skip border if parsing failed (for named colors)
                    var skipBorder = false;
                    if ((selElem.style.borderColor != null) && (selElem.style.borderWidth != '') && (obj.img_bordercolor == '')) {
                        obj.img_borderwidth = -1;
                        skipBorder = true;
                    }

                    // Skip borders if there is different border for sides
                    if ((selElem.style.borderTop != selElem.style.borderRight) || (selElem.style.borderRight != selElem.style.borderBottom) || (selElem.style.borderBottom != selElem.style.borderLeft)) {
                        skipBorder = true;
                    }

                    // Skip margins if there is different margin for sides
                    var skipMargin = false;
                    if ((selElem.style.marginLeft != selElem.style.marginRight) || (selElem.style.marginTop != selElem.style.marginBottom)) {
                        skipMargin = true;
                    }

                    var s = selElem.style.cssText.toLowerCase().split(';');
                    var currentStyle = null;
                    var outStyle = '';
                    for (var i = 0; i < s.length; i++) {
                        currentStyle = s[i].replace(/^\s+|\s+$/g, '');
                        if (currentStyle != '') {
                            if ((currentStyle.indexOf('border') >= 0)) {
                                if (skipBorder) {
                                    outStyle += currentStyle + ';';
                                    // Reset border if margins are diferent
                                    obj.img_borderwidth = -1;
                                    obj.img_bordercolor = -1;
                                }
                            }
                            else if ((currentStyle.indexOf('margin') >= 0)) {
                                if (skipMargin) {
                                    outStyle += currentStyle + ';';
                                    // Reset H. Space and V. Space if margins are diferent
                                    obj.img_hspace = -1;
                                    obj.img_vspace = -1;
                                }
                            }
                            else if ((currentStyle.indexOf('width') == -1) &&
                                     (currentStyle.indexOf('height') == -1) &&
                                     (currentStyle.indexOf('float') == -1) &&
                                     (currentStyle.indexOf('vertical-align') == -1) &&
                                     (currentStyle.indexOf('solid:') == -1)) {
                                outStyle += currentStyle + ';';
                            }
                        }
                    }

                    obj.img_style = outStyle;
                    // Get link from selected Elem
                    obj = GetLinkObj(obj, selElem);
                    // Additional parameters
                    obj.img_dir = selElem.dir;
                    obj.img_usemap = selElem.useMap;
                    obj.img_longdescription = selElem.longDesc;
                    obj.img_lang = selElem.lang;
                }
            }
            else {
                var link = GetLinkElement(selElem);
                if (link) {
                    obj = new Object();
                    obj = GetLinkObj(obj, link);
                }
                else {
                    // Ensure selected text for new links from selection
                    selElem = fopener.FCK.Selection.GetSelection();
                    if (selElem != null) {
                        var range = null;
                        var d = 0;
                        if (selElem.getRangeAt) {
                            range = selElem.getRangeAt(0);
                            d = range.endOffset - range.startOffset;
                        }
                        else {
                            range = selElem.createRange();
                            if (range.text != null) {
                                d = range.text.length;
                            }
                        }
                        if (Math.abs(d) > 0) {
                            obj = new Object();
                            obj.link_text = "##LINKTEXT##";
                            obj.anchor_linktext = "##LINKTEXT##";
                            obj.email_linktext = "##LINKTEXT##";
                        }
                    }
                }
            }
        }
    }
    // Escape all values
    for (var i in obj) {
        obj[i] = encodeURIComponent(obj[i]);
    }
    return obj;
}

function GetAnchorNames() {
    GetFOpener();
    var i;
    var aAnchors = new Array();

    if ((fopener != null) && (fopener.FCK != null)) {
        var oImages = fopener.FCK.EditorDocument.getElementsByTagName('IMG');
        for (i = 0; i < oImages.length; i++) {
            if (oImages[i].getAttribute('_fckanchor'))
                aAnchors[aAnchors.length] = fopener.FCK.GetRealElement(oImages[i]).name;
        }

        var oLinks = fopener.FCK.EditorDocument.getElementsByTagName('A');
        for (i = 0; i < oLinks.length; i++) {
            if (oLinks[i].name && (oLinks[i].name.length > 0))
                aAnchors[aAnchors.length] = oLinks[i].name;
        }
    }
    return aAnchors;
}

function GetIds() {
    GetFOpener();
    if ((fopener != null) && (fopener.FCKTools != null)) {
        return fopener.FCKTools.GetAllChildrenIds(fopener.FCK.EditorDocument.body);
    }
}

function isImage(ext) {
    ext = ext.replace('.', '');
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'gif':
        case 'png':
        case 'tif':
        case 'tiff':
            return true;
    }
    return false;
}

function isFlash(ext) {
    ext = ext.replace('.', '');
    if (ext == 'swf') {
        return true;
    }
    else {
        return false;
    }
}

function isAudioVideo(ext) {
    ext = ext.replace('.', '');
    switch (ext) {
        case 'wav':
        case 'wma':
        case 'mp2':
        case 'mp3':
        case 'mid':
        case 'midi':
        case 'mpga':
        case 'avi':
        case 'mp4':
        case 'mpg':
        case 'mpeg':
        case 'wmv':
        case 'qt':
        case 'mov':
        case 'rm':
            return true;
    }
    return false;
}

function GetLinkObj(obj, selElem) {
    if (selElem != null) {
        var link = GetLinkElement(selElem);
        if (link != null) {
            if (obj == null) {
                obj = new Object();
            }
            var isEmail = /mailto:(.*)/i.test(link.href);
            var isAnchor = /((.*)\/)*(.*)#([^#]*)/.test(link.href);
            var hideText = fopener.FCK.Selection.GetSelection();
            var url = link.getAttribute('_fcksavedurl');
            if (url == null) {
                url = link.href;
            }
            if (isEmail) {
                var to = url.match(/mailto:([^?]*)(?:.*)/i);
                var cc = url.match(/(?:.*)(?:[^b]cc=([^&]*)&?)(?:.*)/i);
                var bcc = url.match(/(?:.*)(?:bcc=([^&]*)&?)(?:.*)/i);
                var subject = url.match(/(?:.*)(?:subject=([^&]*)&?)(?:.*)/i);
                var body = url.match(/(?:.*)(?:body=([^&]*)&?)(?:.*)/i);
                obj.email_linktext = link.textContent;
                obj.email_protocol = this.GetProtocol(url);
                obj.email_url = link.href;
                obj.email_to = (to ? to[1] : '');
                obj.email_cc = (cc ? cc[1] : '');
                obj.email_bcc = (bcc ? bcc[1] : '');
                obj.email_subject = (subject ? subject[1] : '');
                obj.email_body = (body ? body[1] : '');
                obj.email_target = link.target;
                obj.email_id = link.id;
                obj.email_name = link.name;
                obj.email_tooltip = link.title;
                obj.email_class = link.className.replace('FCK__AnchorC', '');
                obj.email_style = (link.style.cssText ? link.style.cssText.toLowerCase() : '');
                obj.img_link = url;
            }
            else if (isAnchor) {
                var anchor_name = url.match(/(?:.*)#(.*)/);
                obj.anchor_linktext = link.innerText;
                obj.anchor_target = link.target;
                obj.anchor_id = link.id;
                obj.anchor_name = (anchor_name[1] ? anchor_name[1] : '');
                obj.anchor_tooltip = link.title;
                obj.anchor_class = link.className.replace('FCK__AnchorC', '');
                obj.anchor_style = (link.style.cssText ? link.style.cssText.toLowerCase() : '');
                obj.anchor_protocol = this.GetProtocol(url);
                obj.img_link = url;
            }
            else {
                obj.img_link = url;
                obj.img_target = link.target;
                obj.link_target = link.target;
                var protocolIndex = url.indexOf('://');
                if (protocolIndex != -1) {
                    obj.link_url = url.substring(protocolIndex + 3);
                    obj.link_protocol = url.substring(0, protocolIndex + 3);
                }
                else {
                    obj.link_url = url;
                    obj.link_protocol = 'other';
                }
                obj.link_text = (link.textContent ? link.textContent : '');
                obj.link_id = link.id;
                obj.link_name = link.name;
                obj.link_tooltip = link.title;
                obj.link_class = link.className.replace('FCK__AnchorC', '');
                obj.link_style = (link.style.cssText ? link.style.cssText.toLowerCase() : '');
            }
        }
    }
    return obj;
}

function createImageElem(obj, document, createLink) {

    // Create IMG from parameters
    var imgOut = document.createElement('IMG');
    if (obj.img_id) {
        imgOut.id = obj.img_id;
    }
    if (obj.img_url) {
        imgOut.src = obj.img_url;
        imgOut.setAttribute("_fcksavedurl", obj.img_url);
    }
    if (obj.img_alt) {
        imgOut.alt = obj.img_alt;
    }
    if (obj.img_tooltip) {
        imgOut.title = obj.img_tooltip;
    }
    if (obj.img_class) {
        imgOut.setAttribute("class", obj.img_class);
    }
    if (obj.img_dir) {
        imgOut.dir = obj.img_dir;
    }
    if (obj.img_usemap) {
        imgOut.useMap = obj.img_usemap;
    }
    if (obj.img_longdescription) {
        imgOut.setAttribute('longdesc', obj.img_longdescription);
    }
    if (obj.img_lang) {
        imgOut.lang = obj.img_lang;
    }
    if (obj.img_style) {
        imgOut.setAttribute('style', obj.img_style);
        if (imgOut.style.cssStyle != undefined) {
            imgOut.style.cssStyle = obj.img_style;
        }
        if (imgOut.style.cssText != undefined) {
            imgOut.style.cssText = obj.img_style;
        }
    }
    if ((obj.img_width) && (parseInt(obj.img_width, 10) >= 0)) {
        imgOut.style.width = parseInt(obj.img_width, 10) + 'px';
    }
    if ((obj.img_height) && (parseInt(obj.img_height, 10) >= 0)) {
        imgOut.style.height = parseInt(obj.img_height, 10) + 'px';
    }
    if ((obj.img_originalwidth) && (parseInt(obj.img_originalwidth, 10) >= 0)) {
        imgOut.setAttribute("_fcksavedwidth", obj.img_originalwidth);
    }
    if ((obj.img_originalheight) && (parseInt(obj.img_originalheight, 10) >= 0)) {
        imgOut.setAttribute("_fcksavedheight", obj.img_originalheight);
    }
    if ((obj.img_borderwidth) && (parseInt(obj.img_borderwidth, 10) >= 0)) {
        imgOut.style.borderWidth = obj.img_borderwidth + 'px';
        imgOut.style.borderStyle = 'solid';
    }
    if (obj.img_bordercolor) {
        try {
            imgOut.style.borderColor = obj.img_bordercolor;
        }
        catch (e) {
            imgOut.style.borderColor = '';
        }
    }
    if (obj.img_align) {
        if ((obj.img_align == 'left') || (obj.img_align == 'right')) {
            if (imgOut.style.cssFloat != undefined) {
                imgOut.style.cssFloat = obj.img_align;
            }
            if (imgOut.style.styleFloat != undefined) {
                imgOut.style.styleFloat = obj.img_align;
            }
        }
        else {
            if (imgOut.style.verticalAlign != undefined) {
                imgOut.style.verticalAlign = obj.img_align;
            }
            if (imgOut.style['vartical-align'] != undefined) {
                imgOut.style['vartical-align'] = obj.img_align;
            }
        }
    }

    if ((obj.img_vspace) || (obj.img_hspace)) {
        var vspace = parseInt(obj.img_vspace, 10);
        var hspace = parseInt(obj.img_hspace, 10);
        if ((vspace == hspace) && (vspace >= 0)) {
            imgOut.style.margin = hspace + 'px';
        }
        else if ((vspace == hspace) && (vspace == -1)) {
            // No margin will be inserted
        }
        else {
            imgOut.style.margin = (vspace >= 0 ? vspace + 'px ' : 'auto ') + (hspace >= 0 ? hspace + 'px' : 'auto');
        }
    }

    if ((createLink) && (((obj.img_link) && (obj.img_link != "")) || ((obj.img_behavior) && (obj.img_behavior != "")))) {
        var imgLink = document.createElement('A');

        if ((obj.img_link) && (obj.img_link != "")) {
            imgLink.href = obj.img_link;
            imgLink.setAttribute('_fcksavedurl', obj.img_link);
        }
        if ((obj.img_target) && (obj.img_target != "")) {
            imgLink.setAttribute('target', obj.img_target);
        }
        else if ((obj.img_behavior) && (obj.img_behavior != "")) {
            imgLink.setAttribute('target', obj.img_behavior);
        }
        imgLink.appendChild(imgOut);
        return imgLink;
    }
    else {
        return imgOut;
    }
}


function GetLinkElement(node) {
    if (node != null) {
        if (node.tagName.toLowerCase() == "a") {
            return node;
        }
        else if (node.tagName.toLowerCase() == "body") {
            return null;
        }
        else {
            return GetLinkElement(node.parentNode);
        }
    }
}

function GetColor(color) {
    color = color.toLowerCase();
    if (color.indexOf('rgb') != -1) {
        var match = color.match(/rgb(?:[^0-9]*)([0-9]+)(?:[^0-9]*)([0-9]+)(?:[^0-9]*)([0-9]+)(?:[^0-9]*)/);
        var r = parseInt(match[1], 10);
        var g = parseInt(match[2], 10);
        var b = parseInt(match[3], 10);
        return '#' + (r > 16 ? r.toString(16) : '0' + r.toString(16)) + (g > 16 ? g.toString(16) : '0' + g.toString(16)) + (b > 16 ? b.toString(16) : '0' + b.toString(16));
    }
    else if (color.indexOf('#') != -1) {
        var match = color.match(/(?:[^#0-9a-f]*)([a-f0-9]+)/);
        if (match[1] != null) {
            return '#' + match[1];
        }
    }
    else {
        return '';
    }
}

function GetProtocol(url) {
    var i = url.indexOf('://');
    if (i > 0) {
        return url.substring(0, i + 3);
    }
    return '';
}

function UnescapeChars(obj) {
    for (var i in obj) {
        obj[i] = decodeURIComponent(obj[i]);
    }
    return obj;
}
