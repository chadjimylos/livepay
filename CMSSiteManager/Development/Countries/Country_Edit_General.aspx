<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Country_Edit_General.aspx.cs"
    Inherits="CMSSiteManager_Development_Countries_Country_Edit_General" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Country Edit - General" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCountryDisplayName" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCountryDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvCountryDisplayName" runat="server" ControlToValidate="txtCountryDisplayName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCountryCodeName" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCountryCodeName" runat="server" CssClass="TextBoxField" MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvCountryCodeName" runat="server" ControlToValidate="txtCountryCodeName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton"
                    EnableViewState="false" />
            </td>
        </tr>
    </table>
</asp:Content>
