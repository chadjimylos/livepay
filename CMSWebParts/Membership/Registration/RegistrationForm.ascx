<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Membership/Registration/RegistrationForm.ascx.cs"
    Inherits="CMSWebParts_Membership_Registration_RegistrationForm" %>
<%@ Register Src="~/CMSFormControls/Inputs/SecurityCode.ascx" TagName="SecurityCode" TagPrefix="cms" %>
<asp:Label ID="lblError" runat="server" ForeColor="red" EnableViewState="false" />
<asp:Label ID="lblText" runat="server" Visible="false" EnableViewState="false" />
<asp:Panel ID="pnlForm" runat="server" DefaultButton="btnOK">
    <table>
        <tr>
            <td>
                <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName" EnableViewState="false" />
            </td>
            <td>
                <cms:ExtendedTextBox ID="txtFirstName" EnableEncoding="true" runat="server" CssClass="LogonTextBox"
                    MaxLength="100" /><br />
                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ValidationGroup="pnlForm"
                    ControlToValidate="txtFirstName" Display="Dynamic" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" />
            </td>
            <td>
                <cms:ExtendedTextBox ID="txtLastName" EnableEncoding="true" runat="server" CssClass="LogonTextBox"
                    MaxLength="100" /><br />
                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ValidationGroup="pnlForm"
                    ControlToValidate="txtLastName" Display="Dynamic" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail" EnableViewState="false" />
            </td>
            <td>
                <cms:ExtendedTextBox ID="txtEmail" runat="server" CssClass="LogonTextBox" MaxLength="100" /><br />
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ValidationGroup="pnlForm"
                    ControlToValidate="txtEmail" Display="Dynamic" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblPassword" runat="server" AssociatedControlID="txtPassword" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="LogonTextBox"
                    MaxLength="100" /><br />
                <asp:RequiredFieldValidator ID="rfvPassword" ValidationGroup="pnlForm" runat="server"
                    ControlToValidate="txtPassword" Display="Dynamic" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblConfirmPassword" runat="server" AssociatedControlID="txtConfirmPassword"
                    EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" CssClass="LogonTextBox"
                    MaxLength="100" /><br />
                <asp:RequiredFieldValidator ID="rfvConfirmPassword" ValidationGroup="pnlForm" runat="server"
                    ControlToValidate="txtConfirmPassword" Display="Dynamic" EnableViewState="false" />
            </td>
        </tr>
        <asp:PlaceHolder runat="server" ID="plcCaptcha">
            <tr>
                <td>
                    <asp:Label ID="lblCaptcha" runat="server" AssociatedControlID="scCaptcha" EnableViewState="false" />
                </td>
                <td>
                    <cms:SecurityCode ID="scCaptcha" GenerateNumberEveryTime="false" runat="server" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <tr>
            <td style="height: 26px">
            </td>
            <td style="height: 26px">
                <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" ValidationGroup="pnlForm"
                    CssClass="ContentButton" EnableViewState="false" />
            </td>
        </tr>
    </table>
</asp:Panel>
