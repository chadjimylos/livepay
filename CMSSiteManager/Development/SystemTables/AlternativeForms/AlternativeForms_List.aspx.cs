using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormEngine;
using CMS.UIControls;

public partial class CMSSiteManager_Development_SystemTables_AlternativeForms_AlternativeForms_List : SiteManagerPage
{
    private int classId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        classId = QueryHelper.GetInteger("classid", 0);

        // Init alternative forms listing
        listElem.FormClassID = classId;
        listElem.OnEdit += new OnEditDeleteActionEventHandler(listElem_OnEdit);
        listElem.OnDelete += new OnEditDeleteActionEventHandler(listElem_OnDelete);

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("altforms.newformlink");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("AlternativeForms_New.aspx?classid=" + classId.ToString());
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/CMS_AlternativeForm/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    void listElem_OnEdit(object actionArgument)
    {
        UrlHelper.Redirect("AlternativeForms_Frameset.aspx?classid=" + classId.ToString() + "&altformid=" + ValidationHelper.GetInteger(actionArgument, 0));
    }


    void listElem_OnDelete(object actionArgument)
    {
        AlternativeFormInfoProvider.DeleteAlternativeFormInfo(ValidationHelper.GetInteger(actionArgument, 0));
    }
}
