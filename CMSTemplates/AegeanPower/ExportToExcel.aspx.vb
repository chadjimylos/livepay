﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.IO

Partial Class CMSTemplates_AegeanPower_ExportToExcel
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ReferenceOfCounts.Columns(0).HeaderText = ResHelper.LocalizeString("{$=Αριθμός Παροχής|en-us=Ημερομηνία Συναλλαγής$}")
        ReferenceOfCounts.Columns(1).HeaderText = ResHelper.LocalizeString("{$=Επωνυμία Πελάτη|en-us=Πληρωμή προς$}")
        ReferenceOfCounts.Columns(2).HeaderText = ResHelper.LocalizeString("{$=Ενδειξη Μετρητή|en-us=Κάρτα$}")
        ReferenceOfCounts.Columns(3).HeaderText = ResHelper.LocalizeString("{$=Άεργος ένδειξη μετρητή|en-us=Ποσό(€)$}")
        ReferenceOfCounts.Columns(4).HeaderText = ResHelper.LocalizeString("{$=Νυχτερινή ένδειξη μετρητή|en-us=Αποτέλεσμα$}")
        ReferenceOfCounts.Columns(5).HeaderText = ResHelper.LocalizeString("{$=Ημερομηνία Καταχώρησης|en-us=Αποτέλεσμα$}")


            ReferenceOfCounts.DataSource = Session("DataReferenceOfCounts")
            ReferenceOfCounts.DataBind()
        Dim strFileName As String = "Measurements -" & Year(Now) & "-" & Month(Now) & "-" & Day(Now)
            Dim tw As New StringWriter()
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            Dim frm As HtmlForm = New HtmlForm()
            Page.Response.ContentType = "application/vnd.ms-excel"
            Page.Response.AddHeader("content-disposition", "filename=" & strFileName & ".xls")
            Page.Response.HeaderEncoding = Encoding.UTF8
            Page.Response.Charset = ""
            Page.EnableViewState = False
            frm.Attributes("runat") = "server"
            Controls.Add(frm)
            frm.Controls.Add(ReferenceOfCounts)
            frm.RenderControl(hw)
            Response.Write(tw.ToString())
            Response.End()
            ReferenceOfCounts.DataBind()
    End Sub

End Class
