<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PublicStatusSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_FormControls_PublicStatusSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%PublicStatusDisplayName%}"
            ObjectType="ecommerce.publicstatus" ResourcePrefix="publicstatusselector"
            SelectionMode="SingleDropDownList" AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
