using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Reporting;
using CMS.GlobalHelper;
using CMS.EventLog;
using CMS.DataEngine;
using CMS.CMSHelper;

public partial class CMSModules_Reporting_Controls_ReportTable : AbstractReportControl
{
    #region "Variables"

    private string mReportTableName = "";
    private GridView gvTable = null;
    private object mNewPage = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Table name
    /// </summary>
    private string ReportTableName
    {
        get
        {
            return mReportTableName;
        }
        set
        {
            mReportTableName = value;
        }
    }


    /// <summary>
    /// Gets or sets current grid view page. Can contain number determining direct page or string valus previous, next, first, last.
    /// </summary>
    private object NewPage
    {
        get
        {
            return mNewPage;
        }
        set
        {
            mNewPage = value;
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Created grid view based on parameter from report table
    /// </summary>
    protected override void CreateChildControls()
    {
        base.CreateChildControls();

        // Set table name from inline parameter
        this.ReportTableName = this.Parameter;

        // Get report table info
        ReportTableInfo rti = ReportTableInfoProvider.GetReportTableInfo(this.ReportTableName);
        if (rti != null)
        {
            this.QueryIsStoredProcedure = rti.TableQueryIsStoredProcedure;
            this.QueryText = ResolveMacros(rti.TableQuery);

            // Setup grid view in codebehind, so SkinID can be set
            gvTable = new GridView();
            gvTable.AllowPaging = ValidationHelper.GetBoolean(rti.TableSettings["enablepaging"], false);
            if (gvTable.AllowPaging)
            {
                gvTable.PageSize = ValidationHelper.GetInteger(rti.TableSettings["pagesize"], 10);
                gvTable.PagerSettings.Mode = (PagerButtons)ValidationHelper.GetInteger(rti.TableSettings["pagemode"], (int)PagerButtons.Numeric);
                gvTable.PageIndexChanging += new GridViewPageEventHandler(gvTable_PageIndexChanging);
            }
            gvTable.AllowSorting = false;

            // Get SkinID from reportTable custom data
            string skinId = ValidationHelper.GetString(rti.TableSettings["skinid"], "");
            if (skinId != String.Empty)
            {
                gvTable.SkinID = skinId;
            }

            gvTable.ID = "reportGrid";

            // Add grid view control to the page
            this.plcGrid.Controls.Clear();
            this.plcGrid.Controls.Add(gvTable);
        }
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData(bool forceLoad)
    {
        try
        {
            EnsureChildControls();

            ReportTableInfo rti = ReportTableInfoProvider.GetReportTableInfo(this.ReportTableName);
            if (rti == null)
            {
                // Display error message, if data load fail
                lblError.Visible = true;
                lblError.Text = "[ReportTable.ascx] Report table '" + this.ReportTableName + "' not found.";

                EventLogProvider ev = new EventLogProvider();
                ev.LogEvent("Report table", "E", new Exception("Report table '" + this.ReportTableName + "' not found."));

                return;
            }

            // Only use base parameters in case of stored procedure
            if (this.QueryIsStoredProcedure)
            {
                this.AllParameters = SpecialFunctions.ConvertDataRowToParams(this.ReportParameters, null);
            }

            // Load data
            DataSet ds = this.LoadData();

            // If no data load, set empty dataset
            if (DataHelper.DataSourceIsEmpty(ds))
            {
                ds = new DataSet();
                ds.Tables.Add();
            }
            else
            {
                // Resolve macros in column names
                int i = 0;
                foreach (DataColumn dc in ds.Tables[0].Columns)
                {
                    if (dc.ColumnName == "Column" + ((int)(i + 1)).ToString())
                    {
                        dc.ColumnName = ResolveMacros(ds.Tables[0].Rows[0][i].ToString());
                    }
                    else
                    {
                        dc.ColumnName = ResolveMacros(dc.ColumnName);
                    }
                    i++;
                }

                // Resolve macros in dataset
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    foreach (DataColumn dc in ds.Tables[0].Columns)
                    {
                        if (dc.DataType.FullName.ToLower() == "system.string")
                        {
                            dr[dc.ColumnName] = ResolveMacros(ValidationHelper.GetString(dr[dc.ColumnName], ""));
                        }
                    }
                }
            }

            // Databind to gridview control
            gvTable.DataSource = ds;

            if (this.NewPage != null)
            {
                gvTable.PageIndex = DetermineNewPageIndex(ds, gvTable.PageSize, gvTable.PageIndex, this.NewPage);
            }

            gvTable.DataBind();
        }
        catch (Exception ex)
        {
            // Display error message, if data load fail
            lblError.Visible = true;
            lblError.Text = "[ReportTable.ascx] Error loading the data: " + ex.Message;
            EventLogProvider ev = new EventLogProvider();
            ev.LogEvent("Report table", "E", ex);
        }
    }


    /// <summary>
    /// Returns new page index determined from specified parameters.
    /// </summary>
    /// <param name="ds">Source data</param>
    /// <param name="pagesize">Page size</param>
    /// <param name="currentPage">Current page</param>
    /// <param name="newPage">New page (may contain string like first, last, ...)</param>    
    private int DetermineNewPageIndex(DataSet ds, int pagesize, int currentPage, object newPage)
    {
        // Validate parameters
        if (DataHelper.DataSourceIsEmpty(ds) || (newPage == null) || (pagesize <= 0) || (currentPage < 0))
        {
            return 0;
        }

        int newPageIndex = 0;

        // Direct page
        if (newPage is int)
        {
            return (int)currentPage;
        }
        else if (newPage is string)
        {
            int rowCount = ds.Tables[0].Rows.Count;
            string page = (string)newPage;

            if (ValidationHelper.IsInteger(page))
            {
                newPageIndex = ValidationHelper.GetInteger(page, 0);

                // Make page zero based, but ensure that it is not negative
                newPageIndex = Math.Max(newPageIndex - 1, 0);
            }
            else
            {
                switch (page.ToLower())
                {
                    case "first":
                        newPageIndex = 0;
                        break;

                    case "last":
                        newPageIndex = (rowCount - 1) / pagesize;
                        break;

                    case "prev":
                        newPageIndex = Math.Max(currentPage - 1, 0);
                        break;

                    case "next":
                        newPageIndex = currentPage + 1;
                        break;
                }
            }
        }

        return newPageIndex;
    }


    /// <summary>
    /// OnLoad
    /// </summary>    
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        string eventTarget = ValidationHelper.GetString(Request.Params["__EVENTTARGET"], String.Empty);

        // Handle paging manually because of lifecycle of the control
        if (!String.IsNullOrEmpty(eventTarget))
        {
            eventTarget = eventTarget.Replace("$", "_");

            // Ensure grid view to get it's client ID
            EnsureChildControls();

            if ((gvTable != null) && (eventTarget == gvTable.ClientID))
            {
                // Get the new page
                string eventArg = ValidationHelper.GetString(Request.Params["__EVENTARGUMENT"], String.Empty);
                string[] args = eventArg.Split('$');
                if ((args.Length == 2) && (args[0].ToLower() == "page"))
                {
                    this.NewPage = args[1];
                }
            }
        }
    }


    /// <summary>
    /// OnPreRender
    /// </summary>    
    protected override void OnPreRender(EventArgs e)
    {
        // Style frid view
        if (gvTable != null)
        {
            gvTable.CellPadding = 3;
            gvTable.GridLines = GridLines.Horizontal;
            gvTable.HeaderStyle.HorizontalAlign = HorizontalAlign.Left;

            if (RenderCssClasses)
            {
                gvTable.HeaderStyle.CssClass = "UniGridHead";
                gvTable.HeaderStyle.Wrap = false;
                gvTable.CssClass = "UniGridGrid";
                gvTable.RowStyle.CssClass = "EvenRow";
                gvTable.AlternatingRowStyle.CssClass = "OddRow";
            }
        }

        base.OnPreRender(e);
    }


    /// <summary>
    /// Handles paging on live site
    /// </summary>
    protected void gvTable_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTable.PageIndex = e.NewPageIndex;
    }

    #endregion
}
