<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Analytics_Statistics.aspx.cs"
    Inherits="CMSModules_WebAnalytics_Tools_Analytics_Statistics" Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/UI/Trees/TreeBorder.ascx" TagName="TreeBorder" TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Statistics</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }
    </style>
</head>
<body class="TreeBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel ID="pnlBody" runat="server" CssClass="ContentTree">
        <cms:TreeBorder ID="borderElem" runat="server" MinSize="10,*" FramesetName="colsFramesetAnalytics" />
        <div class="TreeArea">
            <div class="TreeAreaTree">
                <asp:TreeView ID="treeStats" runat="server" ShowLines="true" CssClass="ContentTree" />
            </div>
        </div>
    </asp:Panel>
    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
    <br />
    <asp:HiddenField ID="hdnMonthsSelectedBar" runat="server" />
    <asp:HiddenField ID="hdnDaysSelectedBar" runat="server" />
    <asp:HiddenField ID="hdnYearSelected" runat="server" />
    </form>

    <script type="text/javascript">
        //<![CDATA[
        var currentNode = document.getElementById('treeSelectedNode');
        var monthSelBar = document.getElementById('hdnMonthsSelectedBar');
        var daySelBar = document.getElementById('hdnDaysSelectedBar');
        var yearSel = document.getElementById('hdnYearSelected');

        function ShowDesktopContent(contentUrl, nodeElem) {
            if ((currentNode != null) && (nodeElem != null)) {
                currentNode.className = 'ContentTreeItem';
            }

            parent.frames['analmonths'].location.href = contentUrl +
		    '&hdnmonthsel=' + monthSelBar.value +
		    '&hdndaysel=' + daySelBar.value +
		    '&hdnyearsel=' + yearSel.value +
		    '&barclicked=1';

            if (nodeElem != null) {
                currentNode = nodeElem;
                currentNode.className = 'ContentTreeSelectedItem';
            }
        }

        function SetSelectedBar(hdnName, value) {
            document.getElementById(hdnName).value = value;
        }

        //]]>    
    </script>

</body>
</html>
