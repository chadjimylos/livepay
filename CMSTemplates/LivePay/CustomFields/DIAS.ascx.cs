using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;

public partial class CMSFormControls_DIAS : LivePayFormEngineUserControl
{
    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;

            this.DIAS.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (IsEmpty())
            {
                return "";
            }

            return DIAS.Text;
        }
        set
        {
            string number = (string)value;
            Clear();

            // Parse numbers from incoming string.
            if ((number != null) && (number != ""))
            {
                try
                {
                    DIAS.Text = number;
                }
                catch
                {
                }
            }
        }
    }

    /// <summary>
    /// Gets or sets field MaxLength.
    /// </summary>
    public override int MaxLength
    {
        get
        {
            return base.MaxLength;
        }
        set
        {
            base.MaxLength = value;
            DIAS.MaxLength = value + 1;
        }
    }

    /// <summary>
    /// Gets or sets field MaxLength.
    /// </summary>
    public override int TypePayment
    {
        get
        {
            return ValidationHelper.GetInteger(base.TypePayment, 0);
        }
        set
        {
            base.TypePayment = value;
        }
    }

    /// <summary>
    /// Gets or sets field MaxLength.
    /// </summary>
    public override int OrganismCode
    {
        get
        {
            return ValidationHelper.GetInteger(base.OrganismCode, 0);
        }
        set
        {
            base.OrganismCode = value;
        }
    }

    /// <summary>
    /// Clears current value
    /// </summary>
    public void Clear()
    {
        DIAS.Text = "";
    }


    /// <summary>
    /// Returns true if the number is empty
    /// </summary>
    public bool IsEmpty()
    {
        return (DataHelper.IsEmpty(DIAS.Text));
    }


    /// <summary>
    /// Returns true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        if (IsEmpty() && this.Mandatory)
        {
            return false;
        }
        
        Validator val = new Validator();
        DIAS.Text = Right("00000000000000000000" + DIAS.Text, this.MaxLength);
        string result = val.IsRegularExp(DIAS.Text, @"\d{" + this.MaxLength + "}", "error").Result;

        if (result != "")
        {
            return false;
        }

        int PedeTeleftea = Convert.ToInt32(Right(DIAS.Text, 5));
        int organismCode = Convert.ToInt32(Left(PedeTeleftea.ToString(), 3));
        //int typePayment = Convert.ToInt32(Mid(PedeTeleftea.ToString(), 3, 1));
        int controlDigit = Convert.ToInt32(Mid(PedeTeleftea.ToString(), 4, 1));

        //if (organismCode != this.OrganismCode || typePayment != this.TypePayment)
        if (organismCode != this.OrganismCode)
        {
            return false;
        }

        int cd = 0; //Control Digit

        cd = modulus11(DIAS.Text, this.MaxLength);
        if (controlDigit != cd)
        {
            return false;
        }

        return true;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomCloud_div.InnerHtml = string.Concat("<a id='CustomCloud_", this.RowCustomField, "' href='#'></a>");

        string ObjDiasScript = string.Empty;

        ObjDiasScript = "var CMSObjDias = { \n" +
                            "maxLength: " + (this.MaxLength + 1) + ", \n" +
                            //"typePayment: " + this.TypePayment + ", \n" +
                            "organismCode: " + this.OrganismCode + ", \n" +
                            "value:'" + this.Value + "' \n" +
                        "}; \n";

        //1234012345678930202
        ScriptManager.RegisterStartupScript(this.Page, typeof(String), "DIAS_" + Guid.NewGuid().ToString(), ObjDiasScript, true);

        cv_DIAS.Attributes.Add("ControlValidated", DIAS.ClientID);
        cv_DIAS.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", this.RowCustomField));
        cv_DIAS.Attributes.Add("CustomValidatorErrorMessage", this.ValidationError);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

    }

}

