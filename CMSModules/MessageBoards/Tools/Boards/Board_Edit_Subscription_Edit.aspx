<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="Board_Edit_Subscription_Edit.aspx.cs" Inherits="CMSModules_MessageBoards_Tools_Boards_Board_Edit_Subscription_Edit"
    Title="Board - Subscription - Edit" Theme="Default" %>

<%@ Register Src="~/CMSModules/MessageBoards/Controls/Boards/BoardSubscription.ascx"
    TagName="BoardSubscription" TagPrefix="cms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" Visible="false" EnableViewState="false" />
    <cms:BoardSubscription ID="boardSubscription" runat="server" />
</asp:Content>
