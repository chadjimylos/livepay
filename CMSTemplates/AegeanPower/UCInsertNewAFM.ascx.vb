﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.Controls
Partial Class CMSTemplates_AegeanPower_UCInsertNewAFM
    Inherits CMSUserControl

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ChkMyAFM.Text = ResHelper.LocalizeString("{$=Είμαι ο νόμιμος κάτοχος του αναγραφόμενου Α.Φ.Μ.|en-us=I am the legitimate owner of the VAT number$}")
        End If

        If Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ChkMyAFM.Text = ResHelper.LocalizeString("{$=Είμαι ο νόμιμος κάτοχος του αναγραφόμενου Α.Φ.Μ.|en-us=I am the legitimate owner of the VAT number$}")
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Dim UserID As Integer = CMSContext.CurrentUser.UserID
            Dim dt As DataTable = AegeanPower_DBConnection.InserNewAFM(UserID, txtAfm.Text, txtProvider.Text)
            If dt.Rows.Count > 0 Then
                If dt.Rows(0)("result").ToString = "1" Then
                    ResultDiv.Attributes.Add("class", "NewAfmSuccess")
                    ResultDiv.InnerHtml = ResHelper.LocalizeString("{$=Το ΑΦΜ σας καταχωρήθηκε επιτυχώς|en-us=Your Vat Number was saved Successfully$}")
                    NewAfmMainForm.Visible = False
                    btnSave.Visible = False
                    DivbtnClose.Visible = True
                    btnClose.Attributes.Add("alt", ResHelper.LocalizeString("{$=Επιστροφή|en-us=Return$}"))
                    btnClose.Attributes.Add("title", ResHelper.LocalizeString("{$=Επιστροφή|en-us=Return$}"))
                    btnClose.Src = ResHelper.LocalizeString("{$=/App_Themes/AegeanPower/Images/btnReturn.png|en-us=/App_Themes/AegeanPower/Images/btnReturnEn.png$}")
                    JsScript(" OnSuccess(); ")
                Else
                    '---Same Afm
                    ResultDiv.Attributes.Add("class", "NewAfmError")
                    ResultDiv.InnerHtml = ResHelper.LocalizeString("{$=Το ΑΦΜ ανήκει σε άλλο λογαριασμό|en-us=Your Vat Number belongs to another user$}")

                End If
            End If
        End If
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub
End Class
