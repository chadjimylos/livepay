using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.CMSHelper;
using CMS.SettingsProvider;

public partial class CMSSiteManager_Development_Cultures_Culture_Edit_Sites : SiteManagerPage
{
    protected int cultureID = 0;
    protected string currentValues = String.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblAvialable.Text = ResHelper.GetString("Culture_Edit_Sites.SiteTitle");

        cultureID = QueryHelper.GetInteger("cultureid", 0);
        if (cultureID > 0)
        {
            // Get the active sites
            currentValues = GetCultureSites();

            if (!RequestHelper.IsPostBack())
            {
                usSites.Value = currentValues;
            }
        }

        usSites.OnSelectionChanged += usSites_OnSelectionChanged;
    }


    /// <summary>
    /// Returns string with culture sites
    /// </summary>    
    private string GetCultureSites()
    {
        DataSet ds = CultureSiteInfoProvider.GetCultureSites("SiteID", "CultureID = " + cultureID, null, 0, null);
        if (!DataHelper.DataSourceIsEmpty(ds))
        {
            return String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "SiteID"));
        }

        return String.Empty;
    }


    protected void usSites_OnSelectionChanged(object sender, EventArgs e)
    {
        SaveSites();
    }


    protected void SaveSites()
    {
        // Remove old items
        string newValues = ValidationHelper.GetString(usSites.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        bool somethingChanged = false;

        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int siteId = ValidationHelper.GetInteger(item, 0);

                    SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);
                    CultureInfo ci = CultureInfoProvider.GetCultureInfo(cultureID);
                    if ((si != null) && (ci != null))
                    {
                        // Check if site does not contain document from this culture
                        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                        DataSet nodes = tree.SelectNodes(si.SiteName, "/%", ci.CultureCode, false, null, null, null, -1, false, 1, "NodeID");
                        if (DataHelper.DataSourceIsEmpty(nodes))
                        {
                            CultureInfoProvider.RemoveCultureFromSite(cultureID, siteId);
                            somethingChanged = true;
                        }
                        else
                        {
                            lblError.Visible = true;
                            lblError.Text = String.Format(ResHelper.GetString("Culture_Edit_Sites.ErrorRemoveSiteFromCulture"), si.DisplayName);
                            continue;
                        }
                    }
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int siteId = ValidationHelper.GetInteger(item, 0);

                    // Add cullture to site
                    SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);
                    if (si != null)
                    {
                        if (CultureInfoProvider.LicenseVersionCheck(si.DomainName, FeatureEnum.Multilingual, VersionActionEnum.Insert))
                        {
                            CultureInfoProvider.AddCultureToSite(cultureID, siteId);
                            somethingChanged = true;
                        }
                        else
                        {
                            lblError.Visible = true;
                            lblError.Text = ResHelper.GetString("licenselimitation.siteculturesexceeded");
                            break;
                        }
                    }
                }
            }
        }

        // If there were some errors, reload uniselector
        if (lblError.Visible)
        {
            usSites.Value = GetCultureSites();
            usSites.Reload(true);
        }

        if (somethingChanged)
        {
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }
    }
}
