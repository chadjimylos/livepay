CREATE VIEW [View_CMS_Site_DocumentCount]
AS
SELECT     CMS_Site.*, (SELECT COUNT(*) AS Documents FROM CMS_Tree WHERE (NodeSiteID = CMS_Site.SiteID)) AS Documents FROM CMS_Site
