CREATE TABLE [CMS_SiteDomainAlias] (
		[SiteDomainAliasID]              int NOT NULL IDENTITY(1, 1),
		[SiteDomainAliasName]            nvarchar(400) NOT NULL,
		[SiteID]                         int NOT NULL,
		[SiteDefaultVisitorCulture]      nvarchar(50) NULL,
		[SiteDomainGUID]                 uniqueidentifier NOT NULL,
		[SiteDomainLastModified]         datetime NOT NULL,
		[SiteDomainDefaultAliasPath]     nvarchar(450) NULL,
		[SiteDomainRedirectUrl]          nvarchar(450) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_SiteDomainAlias]
	ADD
	CONSTRAINT [PK_CMS_SiteDomainAlias]
	PRIMARY KEY
	([SiteDomainAliasID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_SiteDomainAlias]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_SiteDomainAlias_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_SiteDomainAlias]
			ADD CONSTRAINT [FK_CMS_SiteDomainAlias_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE INDEX [IX_CMS_SiteDomainAlias_SiteDomainAliasName]
	ON [CMS_SiteDomainAlias] ([SiteDomainAliasName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_SiteDomainAlias_SiteID]
	ON [CMS_SiteDomainAlias] ([SiteID])
	ON [PRIMARY]
