using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Customers_Customer_Edit_Address_List : CMSEcommercePage
{
    protected int customerId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        customerId = ValidationHelper.GetInteger(Request.QueryString["customerId"], 0);

        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);
        UniGrid.WhereCondition = "AddressCustomerID = " + customerId;

        // Intialize the master page elements
        InitializeMasterPage();
    }


    /// <summary>
    /// Intializes the master page elements
    /// </summary>
    private void InitializeMasterPage()
    {
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("Customer_Edit_Address_List.NewItemCaption");
        actions[0, 3] = "~/CMSModules/Ecommerce/Tools/Customers/Customer_Edit_Address_Edit.aspx?customerId=" + customerId;
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_Address/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    protected object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "addressenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
            case "addressisbilling":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
            case "addressisshipping":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
            case "addressiscompany":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Customer_Edit_Address_Edit.aspx?customerId=" + customerId + "&addressId=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // check 'EcommerceModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
            }

            int addressId = ValidationHelper.GetInteger(actionArgument, 0);
            // Check for the address dependences            
            if (AddressInfoProvider.CheckDependencies(addressId))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                return;
            }

            // delete AddressInfo object from database
            AddressInfoProvider.DeleteAddressInfo(addressId);
        }
    }
}
