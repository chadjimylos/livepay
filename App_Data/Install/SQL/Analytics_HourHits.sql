CREATE TABLE [Analytics_HourHits] (
		[HitsID]               int NOT NULL IDENTITY(1, 1),
		[HitsStatisticsID]     int NOT NULL,
		[HitsStartTime]        datetime NOT NULL,
		[HitsEndTime]          datetime NOT NULL,
		[HitsCount]            int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Analytics_HourHits]
	ADD
	CONSTRAINT [PK_Analytics_HourHits]
	PRIMARY KEY
	NONCLUSTERED
	([HitsID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Analytics_HourHits]') IS NOT NULL
	AND OBJECT_ID(N'[Analytics_Statistics]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Analytics_HourHits_HitsStatisticsID_Analytics_Statistics]') IS NULL
BEGIN
		ALTER TABLE [Analytics_HourHits]
			ADD CONSTRAINT [FK_Analytics_HourHits_HitsStatisticsID_Analytics_Statistics]
			FOREIGN KEY ([HitsStatisticsID]) REFERENCES [Analytics_Statistics] ([StatisticsID])
END
CREATE CLUSTERED INDEX [IX_Analytics_HourHits_HitsStartTime_HitsEndTime]
	ON [Analytics_HourHits] ([HitsStartTime] DESC, [HitsEndTime] DESC)
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Analytics_HourHits_HitsStatisticsID]
	ON [Analytics_HourHits] ([HitsStatisticsID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
