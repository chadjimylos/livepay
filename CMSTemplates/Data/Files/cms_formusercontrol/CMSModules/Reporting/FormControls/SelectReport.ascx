<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectReport.ascx.cs" Inherits="CMSModules_Reporting_FormControls_SelectReport" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="usReports" runat="server" ObjectType="reporting.report" SelectionMode="SingleTextBox" AllowEditTextBox="true"
         DisplayNameFormat="{%ReportDisplayName%}" ReturnColumnName="ReportName" />
    </ContentTemplate>
</cms:CMSUpdatePanel>

