using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.FormEngine;
using CMS.Staging;
using CMS.UIControls;

public partial class CMSModules_BizForms_Tools_AlternativeForms_AlternativeForms_Layout : CMSBizFormPage
{
    private int formId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'ReadForm' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "ReadForm"))
        {
            RedirectToCMSDeskAccessDenied("cms.form", "ReadForm");
        }

        formId = QueryHelper.GetInteger("formid", 0);

        layoutElem.FormType = CMSModules_AdminControls_Controls_Class_Layout.FORMTYPE_BIZFORM;
        layoutElem.ObjectID = QueryHelper.GetInteger("altformid", 0);
        layoutElem.IsAlternative = true;
        layoutElem.OnBeforeSave += new OnLayoutBeforeSaveEventHandler(layoutElem_OnBeforeSave);
        layoutElem.OnAfterSave += new OnLayoutAfterSaveEventHandler(layoutElem_OnAfterSave);
        // Load CSS style sheet to editor area
        if (CMSContext.CurrentSite != null)
        {
            int cssId = CMSContext.CurrentSite.SiteDefaultEditorStylesheet;
            if (cssId == 0) // Use default site CSS if none editor CSS is specified
            {
                cssId = CMSContext.CurrentSite.SiteDefaultStylesheetID;
            }
            layoutElem.CssStyleSheetID = cssId;
        }
    }

    void layoutElem_OnAfterSave()
    {
        // Log synchronization
        BizFormInfo bfi = BizFormInfoProvider.GetBizFormInfo(formId);
        TaskInfoProvider.LogSynchronization(bfi, TaskTypeEnum.UpdateObject, null, CMSContext.CurrentSiteName);
    }


    void layoutElem_OnBeforeSave()
    {
        // Check 'EditForm' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "EditForm"))
        {
            RedirectToCMSDeskAccessDenied("cms.form", "EditForm");
        }
    }
}
