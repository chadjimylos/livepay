CREATE TABLE [Polls_PollRoles] (
		[PollID]     int NOT NULL,
		[RoleID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Polls_PollRoles]
	ADD
	CONSTRAINT [PK_Polls_PollRoles]
	PRIMARY KEY
	NONCLUSTERED
	([PollID], [RoleID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Polls_PollRoles]') IS NOT NULL
	AND OBJECT_ID(N'[Polls_Poll]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Polls_PollRoles_PollID_Polls_Poll]') IS NULL
BEGIN
		ALTER TABLE [Polls_PollRoles]
			ADD CONSTRAINT [FK_Polls_PollRoles_PollID_Polls_Poll]
			FOREIGN KEY ([PollID]) REFERENCES [Polls_Poll] ([PollID])
END
IF OBJECT_ID(N'[Polls_PollRoles]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Polls_PollRoles_RoleID_CMS_Role]') IS NULL
BEGIN
		ALTER TABLE [Polls_PollRoles]
			ADD CONSTRAINT [FK_Polls_PollRoles_RoleID_CMS_Role]
			FOREIGN KEY ([RoleID]) REFERENCES [CMS_Role] ([RoleID])
END
