using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_UICultures_UICulture_Edit : SiteManagerPage
{
    protected int UIcultureID = 0;
    string currentUICulture = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set up text values
        this.Title = "UI Culture Edit";

        // Set up Error text values
        rfvUICultureName.ErrorMessage = ResHelper.GetString("UICulture_Edit_General.ErrorEmptyCultureName");
        rfvUICultureCode.ErrorMessage = ResHelper.GetString("UICulture_Edit_General.ErrorEmptyCultureCode");

        if (QueryHelper.GetBoolean("update", false))
        {
            lblInfo.Visible = true;
        }

        // Get UIcultureID
        UIcultureID = QueryHelper.GetInteger("UIcultureID", 0);
        try
        {
            // Get UI culture object
            UICultureInfo myUICultureInfo = UICultureInfoProvider.GetUICultureInfo(UIcultureID);
            if (!RequestHelper.IsPostBack())
            {
                txtUICultureName.Text = myUICultureInfo.UICultureName;
                txtUICultureCode.Text = myUICultureInfo.UICultureCode;
            }

            currentUICulture = myUICultureInfo.UICultureName;
        }
        catch
        {
            // If UIcultureID = 0 -> Create new culture
            if (UIcultureID == 0)
            {
                currentUICulture = ResHelper.GetString("UICulture_Edit.NewCulture");
            }
            // Redirect to list page, because no culture with given ID exists in DB
            else
            {
                UrlHelper.Redirect("UICutlure_List.aspx");
            }
        }
    }
   

    /// <summary>
    /// Handles btnOK's OnClick event - Update resource info
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        string result = new Validator().NotEmpty(txtUICultureName.Text, rfvUICultureName.ErrorMessage).NotEmpty(txtUICultureCode.Text, rfvUICultureCode.ErrorMessage).Result;
        try
        {
            System.Globalization.CultureInfo objekt = new System.Globalization.CultureInfo(txtUICultureCode.Text);
        }
        catch
        {
            result = ResHelper.GetString("UICulture.ErrorNoGlobalCulture");
        }

        if (string.IsNullOrEmpty(result))
        {
            // Create new UI culture
            if (UIcultureID == 0)
            {
                try
                {
                    UICultureInfoProvider.GetUICultureInfo(txtUICultureCode.Text);
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("UICulture_New.CultureExists");
                }
                catch
                {
                    //Save culture info
                    UICultureInfo ci = new UICultureInfo();
                    ci.UICultureID = UIcultureID;
                    ci.UICultureName = txtUICultureName.Text;
                    ci.UICultureCode = txtUICultureCode.Text;
                    UICultureInfoProvider.SetUICultureInfo(ci);
                    UrlHelper.Redirect("UICulture_Edit.aspx?UICultureID=" + ci.UICultureID);
                }
            }
            else // Update UI culture
            {
                try
                {
                    UICultureInfo ci = UICultureInfoProvider.GetUICultureInfo(txtUICultureCode.Text);

                    if (ci.UICultureID == UIcultureID)
                    {
                        UpdateUICulture();
                        UrlHelper.Redirect("UICulture_Edit.aspx?UIcultureID=" + UIcultureID.ToString());
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("UICulture_New.CultureExists");
                    }
                }
                // When culture code doesnt exists -> update
                catch
                {
                    UpdateUICulture();
                    UrlHelper.Redirect("UICulture_Edit.aspx?UIcultureID=" + UIcultureID.ToString() + "&update=1");
                }
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
            lblInfo.Visible = false;
        }
    }


   /// <summary>
   /// Update just editing culture
   /// </summary>
    protected void UpdateUICulture()
    {
        UICultureInfo ci = new UICultureInfo();
        ci.UICultureID = UIcultureID;
        ci.UICultureName = txtUICultureName.Text;
        ci.UICultureCode = txtUICultureCode.Text;
        UICultureInfoProvider.SetUICultureInfo(ci);
    }
}

