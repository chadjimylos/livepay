CREATE VIEW [View_Messaging_IgnoreList]
AS
SELECT     Messaging_IgnoreList.IgnoreListUserID, Messaging_IgnoreList.IgnoreListIgnoredUserID, CMS_User.UserName, 
                      CMS_UserSettings.UserNickName
FROM         CMS_User INNER JOIN
                      CMS_UserSettings ON CMS_User.UserID = CMS_UserSettings.UserSettingsUserID INNER JOIN
                      Messaging_IgnoreList ON Messaging_IgnoreList.IgnoreListIgnoredUserID = CMS_User.UserID
