using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.UIControls;

public partial class CMSAdminControls_UI_PageElements_FrameResizer : CMSUserControl
{
    #region "Variables"

    protected string minimizeUrl = null;
    protected string maximizeUrl = null;

    protected string originalSize = null;
    protected string minSize = null;

    protected string mFramesetName = null;
    protected bool mVertical = false;
    protected bool mAll = false;
    protected string mCssPrefix = "";
    protected int mParentLevel = 1;

    #endregion


    #region "Properties"

    /// <summary>
    /// Frameset minimalized size
    /// </summary>
    public string MinSize
    {
        get
        {
            return minSize;
        }
        set
        {
            minSize = value;
        }
    }


    /// <summary>
    /// Vertical / horizontal mode
    /// </summary>
    public bool Vertical
    {
        get
        {
            return mVertical;
        }
        set
        {
            mVertical = value;
        }
    }


    /// <summary>
    /// Minimize / maximize all the resizers on the page
    /// </summary>
    public bool All
    {
        get
        {
            return mAll;
        }
        set
        {
            mAll = value;
        }
    }


    /// <summary>
    /// Frameset name
    /// </summary>
    public string FramesetName
    {
        get
        {
            return ValidationHelper.GetString(mFramesetName, (this.Vertical ? "rowsFrameset" : "colsFrameset"));
        }
        set
        {
            mFramesetName = value;
        }
    }


    /// <summary>
    /// Css prefix
    /// </summary>
    public string CssPrefix
    {
        get
        {
            return mCssPrefix;
        }
        set
        {
            mCssPrefix = value;
        }
    }


    /// <summary>
    /// Parent level (1 = immediate parent)
    /// </summary>
    public int ParentLevel
    {
        get
        {
            return mParentLevel;
        }
        set
        {
            mParentLevel = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.All)
        {
            minimizeUrl = GetImageUrl("Design/Controls/FrameResizer/All/minimizeall.png");
            maximizeUrl = GetImageUrl("Design/Controls/FrameResizer/All/maximizeall.png");

            this.plcAll.Visible = true;
            this.plcStandard.Visible = false;
        }
        else
        {
            this.plcStandard.Visible = true;
            this.plcAll.Visible = false;

            if (this.Vertical)
            {
                // Vertical mode
                minimizeUrl = GetImageUrl("Design/Controls/FrameResizer/Vertical/minimize.png");
                maximizeUrl = GetImageUrl("Design/Controls/FrameResizer/Vertical/maximize.png");
            }
            else
            {
                // Horizontal mode
                if (CultureHelper.IsUICultureRTL())
                {
                    minSize = ControlsHelper.GetReversedColumns(minSize);
                }
                minimizeUrl = GetImageUrl("Design/Controls/FrameResizer/Horizontal/minimize.png");
                maximizeUrl = GetImageUrl("Design/Controls/FrameResizer/Horizontal/maximize.png");
            }

            // Define javascript variables
            this.ltlScript.Text = ScriptHelper.GetScript("var minSize = '" + minSize + "'; " +
                "var framesetName = '" + FramesetName + "'; " +
                "var resizeVertical = " + (this.Vertical ? "true" : "false") + "; " +
                "var parentLevel = " + this.ParentLevel + "; "
                );

            if (RequestHelper.IsPostBack())
            {
                originalSize = Request.Params["originalsize"];
            }
        }
    }
}
