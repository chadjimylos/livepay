// Register script displaying safe modal page dialog
function modalDialog(url, name, width, height, otherParams){
    win = window;
    var dHeight = height;
    var dWidth = width;
    
    if ((document.all) && (navigator.appName != 'Opera')) {
        if (otherParams == undefined) {
            otherParams = 'resizable:yes;scroll:no';
        }
        var vopenwork = true;
        try {
            win = wopener.window;
        } 
        catch (e) {
            vopenwork = false;
        }
        
        if (parseInt(navigator.appVersion.substr(22, 1)) < 7) {
            dWidth += 4;
            dHeight += 58;
        };
        
        try {
            dialog = win.showModalDialog(url, this, 'dialogWidth:' + dWidth + 'px;dialogHeight:' + dHeight + 'px;' + otherParams);
        } 
        catch (e) {
            if (vopenwork) {
                window.showModalDialog(url, this, 'dialogWidth:' + dWidth + 'px;dialogHeight:' + dHeight + 'px;' + otherParams);
            }
        }
    }
    else {
        if (otherParams == undefined) {
            otherParams = 'toolbar=no,directories=no,menubar=no,modal=yes,dependent=yes,resizable=yes';
        }
        oWindow = win.open(url, name, 'width=' + dWidth + ',height=' + dHeight + ',' + otherParams);
        oWindow.opener = this;
        oWindow.focus();
    }
}


// Create and define new FCKEditor button command openning the popup window
var InsertYouTubeVideoCommand = function(){
}
InsertYouTubeVideoCommand.GetState = function(){
	if (FCK.EditMode != FCK_EDITMODE_WYSIWYG) 
		return FCK_TRISTATE_DISABLED;
	else {
		return FCK_TRISTATE_OFF;
	}
}

InsertYouTubeVideoCommand.Execute = function(){
    var url = '';
    if (FCKConfig.IsLiveSite) {
        url = '../../../../../CMSFormControls/LiveSelectors/InsertYouTubeVideo/Default.aspx';
    }
    else {
        url = '../../../../../CMSFormControls/Selectors/InsertYouTubeVideo/Default.aspx';
    }
    modalDialog(FCKPlugins.Items['InsertLink'].Path + url, FCKLang.InsertYouTubeVideoTitle, 1024, 660, null);
}


// Register the youtube command.
FCKCommands.RegisterCommand('InsertYouTubeVideo', InsertYouTubeVideoCommand);


var oInsertYouTubeVideo = new FCKToolbarButton('InsertYouTubeVideo', FCKLang.InsertYouTubeVideoButton);
oInsertYouTubeVideo.IconPath = FCKPlugins.Items['InsertYouTubeVideo'].Path + 'fckYouTubeVideo.gif';
FCKToolbarItems.RegisterItem('InsertYouTubeVideo', oInsertYouTubeVideo);

FCK.ContextMenu.RegisterListener({
    AddItems: function(contextMenu, tag, tagName){
        if (tagName == 'IMG' || tagName == 'img') {
            if (tag.getAttribute('_fckyoutube')) {
                contextMenu.AddSeparator();
                contextMenu.AddItem('InsertYouTubeVideo', FCKLang.InsertYouTubeVideoEdit, FCKPlugins.Items['InsertYouTubeVideo'].Path + 'fckYouTubeVideo.gif');
            }
        }
    }
});

//==================================================================================================
//                                     FCK YouTube Video object
//==================================================================================================

var FCKYouTubeVideo = new Object();

FCKYouTubeVideo.oFakeImage = null;
FCKYouTubeVideo.oInline = null;

FCKYouTubeVideo.OnDoubleClick = function(img){
    if (img.tagName == 'IMG' || img.tagName == 'img') {
        if (img.getAttribute('_fckyoutube')) {
            FCKCommands.GetCommand('InsertYouTubeVideo').Execute();
        }
    }
}

FCK.RegisterDoubleClickHandler(FCKYouTubeVideo.OnDoubleClick, 'IMG');

FCKYouTubeVideo.Add = function(obj){
    if ((obj.youtube_url)&&(obj.youtube_url != '')) {
        this.oFakeImage = FCK.Selection.GetSelectedElement();
        this.oInline = null;
        FCKYouTubeVideo._Create(obj)
    }
}

FCKYouTubeVideo.Redraw = function(){
    if (FCK.EditMode != FCK_EDITMODE_WYSIWYG) {
        var arr = new Array();
        var fBody = FCK.EditorDocument.body;
        FCKYouTubeVideo._BuildNodes(fBody, arr);
        alert(arr.length);
        return;
    }
    
    var arr = new Array();
    var fBody = FCK.EditorDocument.body;
    FCKYouTubeVideo._BuildNodes(fBody, arr);
    
    for (var i = 0; i < arr.length; i++) {
        if (/{\^(?:YouTubeVideo)(?:[^}]*)}/i.test(arr[i].data)) {
            // text node value
            var nodeValue = arr[i].data;
            // text node parrent node
            var node = arr[i].parentNode;
            FCKYouTubeVideo._CreateNodes(node, arr[i], nodeValue);
            // remove original text node
            node.removeChild(arr[i]);
        }
    }
}

FCKYouTubeVideo.Parse = function(nodeValue){
    var match = nodeValue.match(/\([^\(\)]+\)|[^|\^{}]+/g);
    // Get all arrguments
    var arr = new Array();
    if (match) {
        for (var i = 1; i < match.length; i = i + 2) {
            arr[match[i].toLowerCase().replace(/^\(+|\)+$/g, '')] = match[i + 1];
        }
    }
    
    var obj = new Object();
    // Create youtube object
    obj.youtube_url = (arr['url'] ? arr['url'] : '');
    obj.youtube_width = (arr['width'] ? arr['width'] : '');
    obj.youtube_height = (arr['height'] ? arr['height'] : '');
    obj.youtube_fs = (arr['fs'] ? arr['fs'] : '');
    obj.youtube_hd = (arr['hd'] ? arr['hd'] : '');
    obj.youtube_autoplay = (arr['autoplay'] ? arr['autoplay'] : '');
    obj.youtube_loop = (arr['loop'] ? arr['loop'] : '');
    obj.youtube_rel = (arr['rel'] ? arr['rel'] : '');
    obj.youtube_cookies = (arr['cookies'] ? arr['cookies'] : '');
    obj.youtube_border = (arr['border'] ? arr['border'] : '');
    obj.youtube_color1 = (arr['color1'] ? arr['color1'] : '');
    obj.youtube_color2 = (arr['color2'] ? arr['color2'] : '');
    
    return obj;
}

FCKYouTubeVideo._Create = function(obj){
    if (this.oFakeImage) {
        if (this.oFakeImage.tagName == 'IMG' && this.oFakeImage.getAttribute('_fckyoutube')) {
            this.oInline = FCK.GetRealElement(this.oFakeImage);
        }
        else {
            this.oFakeImage = null;
        }
    }
    if (!this.oInline) {
        this.oInline = FCK.EditorDocument.createTextNode(' ');
        this.oFakeImage = null;
    }
    
    FCKYouTubeVideo._UpdateInline(this.oInline, this.oFakeImage, obj);
    
    if (!this.oFakeImage) {
        this.oFakeImage = FCKDocumentProcessor_CreateFakeImage('FCK__Media FCK__YouTube_Object', this.oInline);
        this.oFakeImage.setAttribute('_fckyoutube', 'true', 0);
        this.oFakeImage = FCK.InsertElement(this.oFakeImage);
    }
    
    FCKYouTubeVideo._RefreshView(this.oInline, this.oFakeImage);
}
FCKYouTubeVideo._RefreshView = function(originalInline, fakeImage){
    var obj = FCKYouTubeVideo.Parse(originalInline.data);
    var width = parseInt(obj.youtube_width, 10);
    var height = parseInt(obj.youtube_height, 10);
    
    if (width > 0) 
        fakeImage.style.width = width + 'px';
    
    if (height > 0) 
        fakeImage.style.height = height + 'px';
}

FCKYouTubeVideo._UpdateInline = function(inline, fakeImg, obj){
    var inlineTxt = '';
    if (obj.youtube_url) {
        inlineTxt = '{^YouTubeVideo|(url)' + obj.youtube_url;
        
        inlineTxt += '|(width)' + obj.youtube_width + '|(height)' + obj.youtube_height;
        
        if ((obj.youtube_fs != null) && (obj.youtube_fs.toLowerCase() == "true")) {
            inlineTxt += '|(fs)1';
        }
        if ((obj.youtube_hd != null) && (obj.youtube_hd.toLowerCase() == "true")) {
            inlineTxt += '|(hd)1';
        }
        if ((obj.youtube_autoplay != null) && (obj.youtube_autoplay.toLowerCase() == "true")) {
            inlineTxt += '|(autoplay)1';
        }
        if ((obj.youtube_loop != null) && (obj.youtube_loop.toLowerCase() == "true")) {
            inlineTxt += '|(loop)1';
        }
        if ((obj.youtube_rel != null) && (obj.youtube_rel.toLowerCase() == "true")) {
            inlineTxt += '|(rel)1';
        }
        if ((obj.youtube_cookies != null) && (obj.youtube_cookies.toLowerCase() == "true")) {
            inlineTxt += '|(cookies)1';
        }
        if ((obj.youtube_border != null) && (obj.youtube_border.toLowerCase() == "true")) {
            inlineTxt += '|(border)1';
        }
        if ((obj.youtube_color1 != null) && (obj.youtube_color1.toLowerCase() != "#666666")) {
            inlineTxt += '|(color1)' + obj.youtube_color1;
        }
        if ((obj.youtube_color2 != null) && (obj.youtube_color2.toLowerCase() != "#efefef")) {
            inlineTxt += '|(color2)' + obj.youtube_color2;
        }
        inlineTxt += '^}';
    }
    
    inline.nodeValue = inlineTxt;
}

FCKYouTubeVideo._BuildNodes = function(node, array){
    for (var i = 0; i < node.childNodes.length; i++) {
        if (node.childNodes[i].nodeName == '#text') {
            array.push(node.childNodes[i]);
        }
        if (node.childNodes[i].childNodes.length > 0) {
            FCKYouTubeVideo._BuildNodes(node.childNodes[i], array);
        }
    }
}

FCKYouTubeVideo._CreateNodes = function(node, origNode, val){
    var index = val.search(/{\^(?:YouTubeVideo)(?:[^}]*)}/i);
    if (index != -1) {
        var leftCont = RegExp.leftContext;
        var searchCont = RegExp.lastMatch;
        var rightCont = RegExp.rightContext;
        if (leftCont != '') {
            // Create left textnode if some
            node.insertBefore(FCK.EditorDocument.createTextNode(leftCont), origNode);
        }
        if (searchCont != '') {
            // Create inline text node
            node.insertBefore(FCK.EditorDocument.createTextNode(searchCont), origNode);
        }
        if (rightCont != '') {
            // Recursive _CreateNodes id right content exist
            FCKYouTubeVideo._CreateNodes(node, origNode, rightCont);
        }
    }
    else {
        // If not find then create textnode from val
        node.insertBefore(FCK.EditorDocument.createTextNode(val), origNode);
    }
}

var FCKYouTubeObjectProcessor = FCKDocumentProcessor.AppendNew();
FCKYouTubeObjectProcessor.ProcessDocument = function(document){
    var arr = new Array();
    FCKYouTubeVideo._BuildNodes(document.body, arr);
    // Create all inline text nodes
    for (var i = 0; i < arr.length; i++) {
        if (/{\^(?:YouTubeVideo)(?:[^}]*)}/i.test(arr[i].data)) {
            // text node value
            var nodeValue = arr[i].data;
            // text node inline
            var node = arr[i].parentNode;
            FCKYouTubeVideo._CreateNodes(node, arr[i], nodeValue);
            // remove original text node
            node.removeChild(arr[i]);
        }
    }
    // Clear text nodes array
    arr = new Array();
    // Get all new text nodes
    FCKYouTubeVideo._BuildNodes(document.body, arr);
    for (var i = 0; i < arr.length; i++) {
        if (/{\^(?:YouTubeVideo)(?:[^}]*)}/i.test(arr[i].data)) {
        
            var oCloned = arr[i].cloneNode(false);
            var oImg = FCKDocumentProcessor_CreateFakeImage('FCK__Media FCK__YouTube_Object', oCloned);
            oImg.setAttribute('_fckyoutube', 'true', 0);
            
            FCKYouTubeVideo._RefreshView(oCloned, oImg);
            
            arr[i].parentNode.insertBefore(oImg, arr[i]);
            arr[i].parentNode.removeChild(arr[i]);
        }
    }
}

FCKYouTubeObjectProcessor.RefreshInline = function(fakeImage, originalInline){
    if ((fakeImage.style.width != '') && (fakeImage.style.height != '')) {
        var inline = originalInline.data;
        
        inline = inline.replace(/\|\(width\)[^\|]*\|/ig, '|(width)' + parseInt(fakeImage.style.width, 10) + '|');
        inline = inline.replace(/\|\(height\)[^\|]*\|/ig, '|(height)' + parseInt(fakeImage.style.height, 10) + '|');
        
        originalInline.data = inline;
    }
}

FCK.GetRealElement = function(fakeElement){
    var e = FCKTempBin.Elements[fakeElement.getAttribute('_fckrealelement')];
    
    if (fakeElement.getAttribute('_fckflash')) {
        if (fakeElement.style.width.length > 0) 
            e.width = FCKTools.ConvertStyleSizeToHtml(fakeElement.style.width);
        
        if (fakeElement.style.height.length > 0) 
            e.height = FCKTools.ConvertStyleSizeToHtml(fakeElement.style.height);
    }
    else 
        if (fakeElement.getAttribute('_fckmedia') && FCKMediaObjectProcessor) {
            FCKMediaObjectProcessor.RefreshInline(fakeElement, e);
        }
        else 
            if (fakeElement.getAttribute('_fckyoutube') && FCKYouTubeObjectProcessor) {
                FCKYouTubeObjectProcessor.RefreshInline(fakeElement, e);
            }
    
    return e;
}
