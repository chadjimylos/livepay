using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.MessageBoard ;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.Community;

public partial class CMSModules_Groups_Tools_MessageBoards_Boards_Board_Edit_Moderators : CMSGroupPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int boardId = QueryHelper.GetInteger("boardid", 0);
        this.boardModerators.BoardID = boardId;

        this.boardModerators.OnCheckPermissions += new CMSAdminControl.CheckPermissionsEventHandler(boardModerators_OnCheckPermissions);
    }

    void boardModerators_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        int groupId = 0;
        BoardInfo bi = BoardInfoProvider.GetBoardInfo(QueryHelper.GetInteger("boardid", 0));
        if (bi != null)
        {
            groupId = bi.BoardGroupID;
        }

        CheckPermissions(groupId, CMSAdminControl.PERMISSION_MANAGE);
    }
}
