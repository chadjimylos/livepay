using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;

public partial class CMSModules_Content_Controls_Dialogs_Properties_HTMLEmailProperties : ItemProperties
{
    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get reffernce causing postback to hidden button
        string postBackRef = ControlsHelper.GetPostBackEventReference(this.hdnButton, "");
        string raiseOnAction = " function RaiseHiddenPostBack(){" + postBackRef + ";}\n";
        
        this.ltlScript.Text = ScriptHelper.GetScript(raiseOnAction);

        if (ValidationHelper.GetBoolean(SessionHelper.GetValue("HideLinkText"), false))
        {
            this.plcLinkText.Visible = false;
        }

        if (!RequestHelper.IsPostBack())
        {
            Hashtable properties = SessionHelper.GetValue("DialogParameters") as Hashtable;
            if ((properties != null) && (properties.Count > 0))
            {
                // Hide the link text
                this.plcLinkText.Visible = false;
                SessionHelper.SetValue("HideLinkText", true);

                LoadItemProperties(properties);
            }
            else
            {
                properties = SessionHelper.GetValue("DialogSelectedParameters") as Hashtable;
                if ((properties != null) && (properties.Count > 0))
                {
                    LoadItemProperties(properties);
                }
            }
        }

        postBackRef = ControlsHelper.GetPostBackEventReference(this.hdnButtonUpdate, "");
        string postBackKeyDownRef = "var keynum;if(window.event){keynum = event.keyCode;}else if(event.which){keynum = event.which;}if(keynum == 13){" + postBackRef + "; return false;}";

        this.txtLinkText.Attributes["onchange"] = postBackRef;
        this.txtLinkText.Attributes["onkeydown"] = postBackKeyDownRef;
        this.txtTo.Attributes["onchange"] = postBackRef;
        this.txtTo.Attributes["onkeydown"] = postBackKeyDownRef;
        this.txtCc.Attributes["onchange"] = postBackRef;
        this.txtCc.Attributes["onkeydown"] = postBackKeyDownRef;
        this.txtBcc.Attributes["onchange"] = postBackRef;
        this.txtBcc.Attributes["onkeydown"] = postBackKeyDownRef;
        this.txtSubject.Attributes["onchange"] = postBackRef;
        this.txtSubject.Attributes["onkeydown"] = postBackKeyDownRef;
        this.txtBody.Attributes["onchange"] = postBackRef;
    }


    protected void hdnButton_Click(object sender, EventArgs e)
    {
        if (this.Validate())
        {
            // Get selected item information
            Hashtable properties = GetItemProperties();

            // Get JavaScript for inserting the item
            string script = CMSDialogHelper.GetEmailItem(properties);
            if (!string.IsNullOrEmpty(script))
            {
                ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "insertItemScript", ScriptHelper.GetScript(script));
            }
        }
    }


    protected void hdnButtonUpdate_Click(object sender, EventArgs e)
    {
        SaveSession();
    }


    /// <summary>
    /// Save current properties into session
    /// </summary>
    private void SaveSession()
    {
        Hashtable savedProperties = SessionHelper.GetValue("DialogSelectedParameters") as Hashtable;
        if (savedProperties == null)
        {
            savedProperties = new Hashtable();
        }
        Hashtable properties = GetItemProperties();
        foreach (DictionaryEntry entry in properties)
        {
            savedProperties[entry.Key] = entry.Value;
        }
        SessionHelper.SetValue("DialogSelectedParameters", savedProperties);
    }

    #endregion

    #region "Overriden methods"

    /// <summary>
    /// Loads the properites into control
    /// </summary>
    /// <param name="properties"></param>
    public override void LoadItemProperties(Hashtable properties)
    {
        if (properties != null)
        {
            string linkText = ValidationHelper.GetString(properties[DialogParameters.EMAIL_LINKTEXT], "");
            string linkSubject = ValidationHelper.GetString(properties[DialogParameters.EMAIL_SUBJECT], "");
            string linkTo = ValidationHelper.GetString(properties[DialogParameters.EMAIL_TO], "");
            string linkCc = ValidationHelper.GetString(properties[DialogParameters.EMAIL_CC], "");
            string linkBcc = ValidationHelper.GetString(properties[DialogParameters.EMAIL_BCC], "");
            string linkBody = ValidationHelper.GetString(properties[DialogParameters.EMAIL_BODY], "");

            this.txtLinkText.Text = linkText;
            this.txtSubject.Text = HttpUtility.UrlDecode(linkSubject);
            this.txtCc.Text = HttpUtility.UrlDecode(linkCc);
            this.txtBcc.Text = HttpUtility.UrlDecode(linkBcc);
            this.txtTo.Text = HttpUtility.UrlDecode(linkTo);
            this.txtBody.Text = HttpUtility.UrlDecode(linkBody);
        }
    }


    /// <summary>
    /// Returns all parameters of the selected item as name – value collection.
    /// </summary>
    public override Hashtable GetItemProperties()
    {
        Hashtable retval = new Hashtable();

        retval[DialogParameters.EMAIL_LINKTEXT] = this.txtLinkText.Text.Trim().Replace("%", "%25");
        retval[DialogParameters.EMAIL_SUBJECT] = HttpUtility.UrlPathEncode(this.txtSubject.Text.Trim().Replace("%", "%25"));
        retval[DialogParameters.EMAIL_TO] = HttpUtility.UrlPathEncode(this.txtTo.Text.Trim().Replace("%", "%25"));
        retval[DialogParameters.EMAIL_CC] = HttpUtility.UrlPathEncode(this.txtCc.Text.Trim().Replace("%", "%25"));
        retval[DialogParameters.EMAIL_BCC] = HttpUtility.UrlPathEncode(this.txtBcc.Text.Trim().Replace("%", "%25"));
        retval[DialogParameters.EMAIL_BODY] = HttpUtility.UrlPathEncode(this.txtBody.Text.Trim().Replace("%", "%25"));

        retval[DialogParameters.EDITOR_CLIENTID] = QueryHelper.GetString(DialogParameters.EDITOR_CLIENTID, "").Replace("%", "%25");
        return retval;
    }


    /// <summary>
    /// Validates From, Cc and Bcc e-mails.
    /// </summary>
    public override bool Validate()
    {
        string errorMessage = "";

        if (string.IsNullOrEmpty(this.txtTo.Text)) 
        {
            errorMessage += "<br/>" + ResHelper.GetString("general.requireemail");
        }
        else if(!ValidationHelper.AreEmails(this.txtTo.Text.Trim()))
        {
            errorMessage += "<br/>" + ResHelper.GetString("dialogs.email.invalidto");
        }
        if ((this.txtCc.Text.Trim() != "") && !ValidationHelper.AreEmails(this.txtCc.Text.Trim()))
        {
            errorMessage += "<br/>" + ResHelper.GetString("dialogs.email.invalidcc");
        }
        if ((this.txtBcc.Text.Trim() != "") && !ValidationHelper.AreEmails(this.txtBcc.Text.Trim()))
        {
            errorMessage += "<br/>" + ResHelper.GetString("dialogs.email.invalidbcc");
        }
        errorMessage = errorMessage.Trim();

        if (errorMessage != "")
        {
            this.lblError.Text = errorMessage;
            this.lblError.Visible = true;
            this.plnEmailUpdate.Update();
            return false;
        }

        return true;
    }


    /// <summary>
    /// Clears the properties form.
    /// </summary>
    public override void ClearProperties(bool hideProperties)
    {

        this.txtLinkText.Text = "";
        this.txtSubject.Text = "";
        this.txtTo.Text = "";
        this.txtCc.Text = "";
        this.txtBcc.Text = "";
        this.txtBody.Text = "";

    }

    #endregion
}
