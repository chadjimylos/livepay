<%@ Page Language="C#" MasterPageFile="Root.master" AutoEventWireup="true" CodeFile="Home.aspx.cs"
    Inherits="CMSTemplates_CorporateSiteASPX_Home" ValidateRequest="false" %>

<%@ Register Src="~/CMSWebParts/Newsletters/NewsletterSubscriptionWebPart.ascx" TagName="NewsletterSubscription"
    TagPrefix="uc1" %>
<%@ Register Src="~/CMSWebParts/Polls/Poll.ascx" TagName="poll" TagPrefix="uc1" %>
<%@ Register Src="~/CMSWebParts/Viewers/Effects/ScrollingText.ascx" TagName="ScrollingNews"
    TagPrefix="uc1" %>
<%@ Register Src="~/CMSWebParts/Ecommerce/Products/RandomProducts.ascx" TagName="RandomProducts"
    TagPrefix="uc1" %>
<asp:Content ID="cntMain" ContentPlaceHolderID="plcMain" runat="Server">
    <div class="homeTopImage3cols">
        <!-- Top image -->
        <div class="zoneTop">
            <cms:CMSEditableImage runat="server" ID="EditableImage" AlternateText="Home" DisplaySelectorTextBox="false"
                ImageTitle="" />
        </div>
        <!-- Left column -->
        <div class="zoneLeft" style="float: left;">
            <cms:WebPartContainer ID="wpcNewsletterSubscription" runat="server" ContainerName="BlackBox"
                ContainerTitle="Newsletter subscription">
                <uc1:NewsletterSubscription runat="server" ID="NewsletterSubscription" NewsletterName="CorporateNewsletter"
                    DisplayFirstName="true" DisplayLastName="true" SendConfirmationEmail="true" EnableViewState="false" />
            </cms:WebPartContainer>
            <cms:WebPartContainer ID="wpcPoll" runat="server" ContainerName="BlackBox" ContainerTitle="Polls">
                <uc1:poll runat="server" ID="Poll" PollCodeName="ProductSurvey" CountType="Absolute" />
            </cms:WebPartContainer>
        </div>
        <!-- Center column -->
        <div class="zoneCenter" style="float: left;">
            <cms:CMSEditableRegion runat="server" ID="MainContentText" RegionTitle="Main text"
                DialogHeight="320" RegionType="HtmlEditor" />
        </div>
        <!-- Right column -->
        <div class="zoneRight" style="float: left;">
            <cms:WebPartContainer ID="wpcScrollingNews" runat="server" ContainerName="BlackBox"
                ContainerTitle="Latest news">
                <uc1:ScrollingNews runat="server" ID="ScrollingNews" Path="/News/%" ClassNames="CMS.News"
                    SelectTopN="10" TransformationName="CMS.News.NewsPreviewWithSummary" JsMoveTime="1000"
                    JsStopTime="5000" DivWidth="200" DivHeight="115" OrderBy="NewsReleaseDate DESC" EnableViewState="false" />
            </cms:WebPartContainer>
            <cms:WebPartContainer ID="wpcRandomProduct" runat="server" ContainerName="BlackBox"
                ContainerTitle="Featured product">
                <uc1:RandomProducts runat="server" ID="RandomProducts" OnlyNRandomProducts="1" Path="/Products/%"
                    TransformationName="ecommerce.transformations.Product_Featured" Visible="true" CacheMinutes="1" EnableViewState="false" />
            </cms:WebPartContainer>
        </div>
        <div style="clear: both; line-height: 0px; height: 0px;">
        </div>
    </div>
</asp:Content>
