using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Manufacturers_Manufacturer_Edit : CMSEcommercePage
{
    protected int manufacturerId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        // Field validator error messages initialization
        rfvDisplayName.ErrorMessage = ResHelper.GetString("manufacturer_Edit.errorEmptyDisplayName");

        // Control initializations				
        lblManufacturerDisplayName.Text = ResHelper.GetString("Manufacturer_Edit.ManufacturerDisplayNameLabel");
        lblManufactureHomepage.Text = ResHelper.GetString("Manufacturer_Edit.ManufactureHomepageLabel");
        btnOk.Text = ResHelper.GetString("General.OK");

        string currentManufacturer = ResHelper.GetString("Manufacturer_Edit.NewItemCaption");

        // Get manufacturer id from querystring		
        manufacturerId = ValidationHelper.GetInteger(Request.QueryString["manufacturerId"], 0);
        if (manufacturerId > 0)
        {
            ManufacturerInfo manufacturerObj = ManufacturerInfoProvider.GetManufacturerInfo(manufacturerId);
            if (manufacturerObj != null)
            {
                currentManufacturer = manufacturerObj.ManufacturerDisplayName;

                // Fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(manufacturerObj);

                    // Show that the manufacturer was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
        }

        this.CurrentMaster.Title.HelpTopicName = "newedit_manufacturer";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Manufacturer_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Manufacturers/Manufacturer_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentManufacturer;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// Load data of editing manufacturer.
    /// </summary>
    /// <param name="manufacturerObj">Manufacturer object.</param>
    protected void LoadData(ManufacturerInfo manufacturerObj)
    {
        txtManufacturerDisplayName.Text = manufacturerObj.ManufacturerDisplayName;
        txtManufactureHomepage.Text = manufacturerObj.ManufactureHomepage;
        chkManufacturerEnabled.Checked = manufacturerObj.ManufacturerEnabled;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        // Check input values from textboxes and other contrlos - new Validator()
        string errorMessage = new Validator().NotEmpty(txtManufacturerDisplayName.Text, ResHelper.GetString("manufacturer_Edit.errorEmptyDisplayName")).Result;

        if (errorMessage == "")
        {
            // ManufacturerName must to be unique
            ManufacturerInfo manufacturerObj = ManufacturerInfoProvider.GetManufacturerInfo(txtManufacturerDisplayName.Text.Trim());

            // If manufacturerName value is unique														
            if ((manufacturerObj == null) || (manufacturerObj.ManufacturerID == manufacturerId))
            {
                // Get object
                if (manufacturerObj == null)
                {
                    manufacturerObj = ManufacturerInfoProvider.GetManufacturerInfo(manufacturerId);
                    if (manufacturerObj == null)
                    {
                        manufacturerObj = new ManufacturerInfo();
                    }
                }

                manufacturerObj.ManufacturerID = manufacturerId;
                manufacturerObj.ManufacturerDisplayName = txtManufacturerDisplayName.Text.Trim();
                manufacturerObj.ManufactureHomepage = txtManufactureHomepage.Text.Trim();
                manufacturerObj.ManufacturerEnabled = chkManufacturerEnabled.Checked;

                ManufacturerInfoProvider.SetManufacturerInfo(manufacturerObj);

                UrlHelper.Redirect("Manufacturer_Edit.aspx?manufacturerId=" + Convert.ToString(manufacturerObj.ManufacturerID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Manufacturer_Edit.ManufacturerNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
