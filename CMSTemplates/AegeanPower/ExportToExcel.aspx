﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExportToExcel.aspx.vb" Inherits="CMSTemplates_AegeanPower_ExportToExcel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:GridView ID="ReferenceOfCounts" runat="server" AutoGenerateColumns="false" GridLines="None">
            <AlternatingRowStyle CssClass="BillsGv_RowB" />
            <RowStyle  CssClass="BillsGv_RowA" />
            <Columns>

    <asp:TemplateField HeaderText="Αριθμός Παροχής" HeaderStyle-Width="100px" HeaderStyle-CssClass="BillsGv_Title">
        <ItemTemplate>
            <asp:Label ID="lblSelectCounter" runat="server" Text='<%#Eval("SelectCounter") %>'></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>
    
    <asp:TemplateField HeaderText="Επωνυμία Πελάτη" HeaderStyle-Width="150px" HeaderStyle-CssClass="BillsGv_Title">
        <ItemTemplate>
            <asp:Label ID="lblFullName" runat="server" Text='<%#Eval("FullName") %>' ></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="¨Ενδειξη Μετρητή" HeaderStyle-Width="100px" HeaderStyle-CssClass="BillsGv_Title">
        <ItemTemplate>
            <asp:Label ID="lblIndication" runat="server" Text='<%#Eval("Indication") %>' ></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="Άεργος ένδειξη μετρητή" HeaderStyle-Width="130px" HeaderStyle-CssClass="BillsGv_Title">
        <ItemTemplate>
            <asp:Label ID="lblReactiveMeterReading" runat="server" Text='<%#Eval("ReactiveMeterReading") %>' ></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="Νυχτερινή ένδειξη μετρητή" HeaderStyle-Width="145px" HeaderStyle-CssClass="BillsGv_Title">
        <ItemTemplate>
            <asp:Label ID="lblNightlifeMeterReading" runat="server" Text='<%#Eval("NightlifeMeterReading") %>' ></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="Ημερομηνία Καταχώρησης" HeaderStyle-Width="140px" HeaderStyle-CssClass="BillsGv_Title">
        <ItemTemplate>
            <asp:Label ID="lblFormInserted" runat="server" Text='<%#Eval("FormInserted") %>' ></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

</Columns>
<EmptyDataTemplate>
<div style="padding:10px 0px 5px 10px;"><%= ResHelper.LocalizeString("{$=Δεν βρέθηκαν Δεδομένα.|en-us=No Data found.$}")%></div>
</EmptyDataTemplate>
</asp:GridView>
    </div>
    </form>
</body>
</html>
