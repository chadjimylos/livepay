<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Analytics_Months.aspx.cs"
    Inherits="CMSModules_WebAnalytics_Tools_Analytics_Months" Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/UI/HTMLGraph.ascx" TagName="HTMLGraph" TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Analytics - Months</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
            overflow: hidden;
        }
    </style>
</head>

<script type="text/javascript">
    //<![CDATA[
    function ucGraphMonths_ShowDesktopContent(contentUrl, contentUrl2) {
        if (contentUrl != "") parent.frames['analdays'].location.href = contentUrl;
        if (contentUrl2 != "") parent.frames['analreport'].location.href = contentUrl2;
    }

    function ucGraphMonths_OnSelectBar(sender) {
        parent.frames['analtree'].SetSelectedBar('hdnMonthsSelectedBar', sender.id);
        parent.frames['analtree'].SetSelectedBar('hdnDaysSelectedBar', '');
    }
    //]]>    
</script>

<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="pnlHeader" CssClass="PageTitleHeader">
        <table style="width: 100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="width: 100%">
                    <asp:Image ID="imgTitle" runat="server" CssClass="PageTitleImage" EnableViewState="false" />
                    <asp:Label ID="lblTitle" runat="server" CssClass="PageTitle" EnableViewState="false" />
                </td>
                <td>
                    <asp:Label ID="lblYears" runat="server" EnableViewState="false" />&nbsp;
                    <asp:DropDownList ID="drpYears" runat="server" CssClass="DropDownFieldSmall" /><cms:CMSButton ID="btnShow"
                        runat="server" CssClass="ContentButton" OnClientClick=" parent.frames['analtree'].SetSelectedBar('hdnMonthsSelectedBar', ''); parent.frames['analtree'].SetSelectedBar('hdnDaysSelectedBar', ''); return true;"
                            OnClick="btnShow_Click" EnableViewState="false" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDisabled" CssClass="DisabledInfoPanel" Visible="false">
        <asp:Label runat="server" ID="lblDisabled" EnableViewState="false" />
    </asp:Panel>
    <div style="padding: 5px 0px 5px 0px;" class="PageHeaderLine">
        <cms:HTMLGraph ID="ucGraphMonths" runat="server" />
    </div>
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="False" Visible="false" />
    </form>
</body>
</html>
