using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.ExtendedControls;
using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.CMSHelper;
using CMS.PortalEngine;
using CMS.UIControls;
using CMS.LicenseProvider;

using TreeNode = CMS.TreeEngine.TreeNode;
using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Content_CMSDesk_Properties_General : CMSPropertiesPage
{
    #region "Private variables"

    protected string mForums = null;
    protected string mMessageBoards = null;
    protected string mEditableContent = null;
    protected int nodeId = 0;
    protected string mSave = null;
    protected bool canEditOwner = false;
    protected bool canEdit = true;
    protected TreeNode node = null;
    protected FormEngineUserControl fcDocumentGroupSelector = null;

    bool hasAdHocBoard = false;
    bool hasAdHocForum = false;
    FormEngineUserControl usrOwner = null;

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.General"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.General");
        }

        // Redirect to information page when no UI elements displayed
        if (this.pnlUIAdvanced.IsHidden && this.pnlUICache.IsHidden && this.pnlUIDesign.IsHidden &&
                this.pnlUIOther.IsHidden && this.pnlUIOwner.IsHidden)
        {
            RedirectToUINotAvailable();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the scripts
        ScriptHelper.RegisterProgress(this.Page);
        ScriptHelper.RegisterTooltip(this.Page);
        ScriptHelper.RegisterDialogScript(this.Page);

        usrOwner = this.Page.LoadControl("~/CMSModules/Membership/FormControls/Users/selectuser.ascx") as FormEngineUserControl;
        usrOwner.ID = "ctrlUsrOwner";
        usrOwner.IsLiveSite = false;
        usrOwner.SetValue("ShowSiteFilter", false);
        usrOwner.StopProcessing = this.pnlUIOwner.IsHidden;
        plcUsrOwner.Controls.Add(usrOwner);

        UIContext.PropertyTab = PropertyTabEnum.General;

        nodeId = QueryHelper.GetInteger("nodeid", 0);

        // Init strings
        pnlDesign.GroupingText = ResHelper.GetString("GeneralProperties.DesignGroup");
        pnlCache.GroupingText = ResHelper.GetString("GeneralProperties.CacheGroup");
        pnlOther.GroupingText = ResHelper.GetString("GeneralProperties.OtherGroup");
        pnlAdvanced.GroupingText = ResHelper.GetString("GeneralProperties.AdvancedGroup");
        pnlOwner.GroupingText = ResHelper.GetString("GeneralProperties.OwnerGroup");

        // Advanced section
        mEditableContent = ResHelper.GetString("GeneralProperties.EditableContent");
        mForums = ResHelper.GetString("PageProperties.Forums");
        mMessageBoards = ResHelper.GetString("PageProperties.MessageBoards");
        lnkEditableContent.OnClientClick = "ShowEditableContent(); return false;";
        lnkMessageBoards.OnClientClick = "ShowMessageBoards(); return false;";
        lnkForums.OnClientClick = "ShowForums(); return false;";
        imgEditableContent.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditableContent/editablecontent.png");
        imgMessageBoards.ImageUrl = GetImageUrl("CMSModules/CMS_MessageBoards/module.png");
        imgForums.ImageUrl = GetImageUrl("CMSModules/CMS_Forums/module.png");

        radInherit.Text = ResHelper.GetString("GeneralProperties.radInherit");
        radYes.Text = ResHelper.GetString("general.yes");
        radNo.Text = ResHelper.GetString("general.no");

        lblCacheMinutes.Text = ResHelper.GetString("GeneralProperties.cacheMinutes");

        lblNameTitle.Text = ResHelper.GetString("GeneralProperties.Name");
        lblNamePathTitle.Text = ResHelper.GetString("GeneralProperties.NamePath");
        lblAliasPathTitle.Text = ResHelper.GetString("GeneralProperties.AliasPath");
        lblTypeTitle.Text = ResHelper.GetString("GeneralProperties.Type");
        lblNodeIDTitle.Text = ResHelper.GetString("GeneralProperties.NodeID");
        lblLastModifiedByTitle.Text = ResHelper.GetString("GeneralProperties.LastModifiedBy");
        lblLastModifiedTitle.Text = ResHelper.GetString("GeneralProperties.LastModified");
        lblLiveURLTitle.Text = ResHelper.GetString("GeneralProperties.LiveURL");
        lblGUIDTitle.Text = ResHelper.GetString("GeneralProperties.GUID");
        lblCultureTitle.Text = ResHelper.GetString("GeneralProperties.Culture");
        lblCreatedByTitle.Text = ResHelper.GetString("GeneralProperties.CreatedBy");
        lblCreatedTitle.Text = ResHelper.GetString("GeneralProperties.Created");
        lblOwnerTitle.Text = ResHelper.GetString("GeneralProperties.Owner");
        lblCssStyle.Text = ResHelper.GetString("PageProperties.CssStyle");
        lblPublishedTitle.Text = ResHelper.GetString("PageProperties.Published");

        chkCssStyle.Text = ResHelper.GetString("Metadata.Inherit");

        ctrlSiteSelectStyleSheet.CurrentSelector.SpecialFields = new string[1, 2] { { ResHelper.GetString("general.defaultchoice"), "-1" } };
        ctrlSiteSelectStyleSheet.CurrentSelector.ReturnColumnName = "StyleSheetID";
        ctrlSiteSelectStyleSheet.CurrentSelector.WhereCondition = "StyleSheetID IN (SELECT StylesheetID FROM CMS_CssStylesheetSite WHERE SiteID= " + CMSContext.CurrentSiteID + " )";

        if (CMSContext.CurrentSite != null)
        {
            usrOwner.SetValue("SiteID", CMSContext.CurrentSite.SiteID);
        }

        imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        mSave = ResHelper.GetString("general.save");

        int documentId = 0;

        // Get the document
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        node = tree.SelectSingleNode(nodeId, CMSContext.PreferredCultureCode, tree.CombineWithDefaultCulture);
        if (node != null)
        {
            documentId = node.DocumentID;
            canEditOwner = (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.ModifyPermissions) == AuthorizationResultEnum.Allowed);

            ReloadData();
        }

        // Generate executive script
        string script = "function ShowEditableContent() { modalDialog('" + ResolveUrl("Advanced/EditableContent/default.aspx") + "?nodeid=" + nodeId + "', 'EditableContent', 1015, 700); } \n";

        if (hasAdHocBoard)
        {
            plcAdHocBoards.Visible = true;
            script += "function ShowMessageBoards() { modalDialog('" + ResolveUrl("~/CMSModules/MessageBoards/Content/Properties/default.aspx") + "?documentid=" + documentId + "', 'MessageBoards', 1020, 680); } \n";
        }

        if (hasAdHocForum)
        {
            plcAdHocForums.Visible = true;
            script += "function ShowForums() { modalDialog('" + ResolveUrl("~/CMSModules/Forums/Content/Properties/default.aspx") + "?documentid=" + documentId + "', 'Forums', 1050, 680); } \n";
        }

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ModalDialogsToAdvancedSection", ScriptHelper.GetScript(script));
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (canEdit)
        {
            // Shortcuts script
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
                    "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>" +
                    ScriptHelper.GetScript("function SaveDocument() { " + ClientScript.GetPostBackEventReference(lnkSave, null) + " } \n"
                ));
        }
    }

    #endregion


    #region "Private methods"

    private void ReloadData()
    {
        if (node != null)
        {
            // Check read permissions
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
            }
            else
            {
                // Show document group owner selector
                if (ModuleEntry.IsModuleLoaded(ModuleEntry.COMMUNITY) && canEditOwner && (LicenseHelper.CurrentEdition == ProductEditionEnum.Ultimate))
                {
                    plcOwnerGroup.Controls.Clear();
                    // Initialize table
                    TableRow rowOwner = new TableRow();
                    TableCell cellTitle = new TableCell();
                    TableCell cellSelector = new TableCell();

                    // Initialize caption
                    LocalizedLabel lblOwnerGroup = new LocalizedLabel();
                    lblOwnerGroup.EnableViewState = false;
                    lblOwnerGroup.ResourceString = "community.group.documentowner";
                    lblOwnerGroup.ID = "lblOwnerGroup";
                    cellTitle.Controls.Add(lblOwnerGroup);

                    // Initialize selector
                    fcDocumentGroupSelector = (FormEngineUserControl)Page.LoadControl("~/CMSAdminControls/UI/Selectors/DocumentGroupSelector.ascx");
                    fcDocumentGroupSelector.ID = "fcDocumentGroupSelector";
                    fcDocumentGroupSelector.StopProcessing = this.pnlUIOwner.IsHidden;
                    cellSelector.Controls.Add(fcDocumentGroupSelector);
                    fcDocumentGroupSelector.Value = ValidationHelper.GetInteger(node.GetValue("NodeGroupID"), 0);
                    fcDocumentGroupSelector.SetValue("siteid", CMSContext.CurrentSiteID);
                    fcDocumentGroupSelector.SetValue("nodeid", nodeId);

                    // Add controls to containers
                    rowOwner.Cells.Add(cellTitle);
                    rowOwner.Cells.Add(cellSelector);
                    plcOwnerGroup.Controls.Add(rowOwner);
                    plcOwnerGroup.Visible = true;
                }
                
                // Check modify permissions
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                {
                    // disable form editing                                                            
                    DisableFormEditing();

                    // show access denied message
                    lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                    lblInfo.Visible = true;
                }

                // Show owner editing only when authorized to change the permissions
                if (canEditOwner)
                {
                    lblOwner.Visible = false;
                    usrOwner.Visible = true;
                }
                else 
                {
                    usrOwner.Visible = false;
                }

                if (!RequestHelper.IsPostBack())
                {
                    if (canEditOwner)
                    {
                        usrOwner.Value = node.GetValue("NodeOwner");
                    }
                }

                // Load the data
                lblName.Text = HttpUtility.HtmlEncode(node.DocumentName);
                lblNamePath.Text = HttpUtility.HtmlEncode(Convert.ToString(node.GetValue("DocumentNamePath")));
                lblAliasPath.Text = Convert.ToString(node.NodeAliasPath);
                lblType.Text = DataClassInfoProvider.GetDataClass(node.NodeClassName).ClassDisplayName;
                lblNodeID.Text = Convert.ToString(node.NodeID);

                // Modifier
                SetUserLabel(lblLastModifiedBy, "DocumentModifiedByUserId");

                TimeZoneInfo usedTimeZone = null;
                DateTime lastModified = ValidationHelper.GetDateTime(node.GetValue("DocumentModifiedWhen"), DateTimeHelper.ZERO_TIME);
                lblLastModified.Text = TimeZoneHelper.GetCurrentTimeZoneDateTimeString(lastModified, CMSContext.CurrentUser, CMSContext.CurrentSite, out usedTimeZone);
                ScriptHelper.AppendTooltip(lblLastModified, TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone),  "help");
                
                if (!canEditOwner)
                {
                    // Owner
                    SetUserLabel(lblOwner, "NodeOwner");
                }

                // Creator
                SetUserLabel(lblCreatedBy, "DocumentCreatedByUserId");
                DateTime createdWhen = ValidationHelper.GetDateTime(node.GetValue("DocumentCreatedWhen"), DateTimeHelper.ZERO_TIME);
                lblCreated.Text = TimeZoneHelper.GetCurrentTimeZoneDateTimeString(createdWhen, CMSContext.CurrentUser, CMSContext.CurrentSite, out usedTimeZone);
                ScriptHelper.AppendTooltip(lblCreated, TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone), "help");

                // URL
                string liveUrl = node.IsLink ? CMSContext.GetUrl(node.NodeAliasPath, null) : CMSContext.GetUrl(node.NodeAliasPath, node.DocumentUrlPath);
                lnkLiveURL.Text = ResolveUrl(liveUrl);
                lnkLiveURL.NavigateUrl = liveUrl;

                lblGUID.Text = Convert.ToString(node.NodeGUID);

                // Culture
                CultureInfo ci = CultureInfoProvider.GetCultureInfo(node.DocumentCulture);
                lblCulture.Text = ((ci != null) ? ci.CultureName : node.DocumentCulture);

                lblPublished.Text = (node.IsPublished ? "<span class=\"DocumentPublishedYes\">" + ResHelper.GetString("General.Yes") + "</span>" : "<span class=\"DocumentPublishedNo\">" + ResHelper.GetString("General.No") + "</span>");

                bool isRoot = (node.NodeClassName.ToLower() == "cms.root");
                if (!RequestHelper.IsPostBack())
                {
                    // Init radio buttons
                    if (isRoot)
                    {
                        radInherit.Visible = false;
                        chkCssStyle.Visible = false;
                        switch (node.NodeCacheMinutes)
                        {
                            case -1:
                                radNo.Checked = true;
                                radYes.Checked = false;
                                radInherit.Checked = false;
                                txtCacheMinutes.Text = "";
                                break;

                            case 0:
                                radNo.Checked = true;
                                radYes.Checked = false;
                                radInherit.Checked = false;
                                txtCacheMinutes.Text = "";
                                break;

                            default:
                                radNo.Checked = false;
                                radYes.Checked = true;
                                radInherit.Checked = false;
                                txtCacheMinutes.Text = node.NodeCacheMinutes.ToString();
                                break;
                        }
                    }
                    else
                    {
                        switch (node.NodeCacheMinutes)
                        {
                            case -1:
                                radNo.Checked = false;
                                radYes.Checked = false;
                                radInherit.Checked = true;
                                txtCacheMinutes.Text = "";
                                break;

                            case 0:
                                radNo.Checked = true;
                                radYes.Checked = false;
                                radInherit.Checked = false;
                                txtCacheMinutes.Text = "";
                                break;

                            default:
                                radNo.Checked = false;
                                radYes.Checked = true;
                                radInherit.Checked = false;
                                txtCacheMinutes.Text = node.NodeCacheMinutes.ToString();
                                break;
                        }
                    }

                    if (!radYes.Checked)
                    {
                        txtCacheMinutes.Enabled = false;
                    }
                }


                if (!RequestHelper.IsPostBack())
                {
                    if (node.GetValue("DocumentStylesheetID") == null)
                    {
                        ctrlSiteSelectStyleSheet.Value = -1;
                    }
                    else
                    {
                        if (ValidationHelper.GetInteger(node.GetValue("DocumentStylesheetID"), 0) == -1)
                        {
                            if (!isRoot)
                            {
                                ctrlSiteSelectStyleSheet.Enabled = false;
                                chkCssStyle.Checked = true;
                                string value = PageInfoProvider.GetParentProperty(CMSContext.CurrentSite.SiteID, node.NodeAliasPath, "DocumentStylesheetID <> -1", "DocumentStylesheetID", null);

                                if (String.IsNullOrEmpty(value))
                                {
                                    ctrlSiteSelectStyleSheet.Value = -1;
                                }
                                else
                                {
                                    ctrlSiteSelectStyleSheet.Value = value;
                                }
                            }
                        }
                        else
                        {
                            ctrlSiteSelectStyleSheet.Value = node.GetValue("DocumentStylesheetID");
                        }
                    }
                }

                if (!isRoot && ValidationHelper.GetInteger(node.GetValue("DocumentStylesheetID"), 0) == -1)
                {
                    ctrlSiteSelectStyleSheet.Enabled = false;
                }

                RefreshCntRatingResult();

                double rating = 0.0f;
                if (node.DocumentRatings > 0)
                {
                    rating = node.DocumentRatingValue / node.DocumentRatings;
                }
                ratingControl.MaxRating = 10;
                ratingControl.CurrentRating = rating;
                ratingControl.Visible = true;
                ratingControl.Enabled = false;

                btnResetRating.Text = ResHelper.GetString("general.reset");
                btnResetRating.OnClientClick = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("GeneralProperties.ResetRatingConfirmation")) + ")) return false;";

                object[] param = new object[1];
                param[0] = node.DocumentID;

                // Check ad-hoc forum counts
                hasAdHocForum = (ModuleCommands.ForumsGetDocumentForumsCount(node.DocumentID) > 0);

                // Ad-Hoc message boards check
                hasAdHocBoard = (ModuleCommands.MessageBoardGetDocumentBoardsCount(node.DocumentID) > 0);

                plcAdHocForums.Visible = hasAdHocForum;
                plcAdHocBoards.Visible = hasAdHocBoard;
            }
        }
        else
        {
            btnResetRating.Visible = false;
        }
    }


    private void SetUserLabel(Label label, string columnName)
    {
        int userId = ValidationHelper.GetInteger(node.GetValue(columnName), 0);
        if (userId > 0)
        {
            UserInfo ui = null;
            string key = "user_" + userId;
            object userObject = RequestStockHelper.GetItem(key);
            if (userObject != null)
            {
                ui = (UserInfo)userObject;
            }
            else
            {
                ui = UserInfoProvider.GetUserInfo(userId);
                RequestStockHelper.Add(key, ui);
            }

            if (ui != null)
            {
                label.Text = HTMLHelper.HTMLEncode(ui.FullName);
            }
        }
        else
        {
            label.Text = ResHelper.GetString("general.selectnone");
        }
    }


    protected void lnkSave_Click(object sender, EventArgs e)
    {
        // Get the document
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        node = tree.SelectSingleNode(nodeId, CMSContext.PreferredCultureCode);
        if (node != null)
        {
            bool correct = true;

            // OWNER group is displayed by UI profile
            if (!this.pnlUIOwner.IsHidden)
            {
                // Set owner
                int ownerId = ValidationHelper.GetInteger(usrOwner.Value, 0);
                if (ownerId > 0)
                {
                    node.SetValue("NodeOwner", usrOwner.Value);
                }
                else
                {
                    node.SetValue("NodeOwner", null);
                }
            }

            // DESIGN group is displayed by UI profile
            if (!this.pnlUIDesign.IsHidden)
            {
                node.SetValue("DocumentStylesheetID", -1);
                if (!chkCssStyle.Checked)
                {
                    int selectedCssId = ValidationHelper.GetInteger(ctrlSiteSelectStyleSheet.Value, 0);
                    if (selectedCssId < 1)
                    {
                        node.SetValue("DocumentStylesheetID", null);
                    }
                    else
                    {
                        node.SetValue("DocumentStylesheetID", selectedCssId);
                    }

                    ctrlSiteSelectStyleSheet.CurrentDropDown.Enabled = true;
                }
                else
                {
                    ctrlSiteSelectStyleSheet.CurrentDropDown.Enabled = false;
                }
            }            

            // CACHE group is displayed by UI profile
            bool clearCache = false;
            if (!this.pnlUICache.IsHidden)
            {
                // Cache minutes
                int cacheMinutes = 0;
                if (radNo.Checked)
                {
                    cacheMinutes = 0;
                }
                else if (radYes.Checked)
                {
                    string siteName = CMSContext.CurrentSiteName;

                    if (txtCacheMinutes.Text == "")
                    {
                        if (radYes.Checked)
                        {
                            correct = false;
                        }
                        else
                        {
                            txtCacheMinutes.Text = SettingsKeyProvider.GetStringValue(siteName + ".CMSCacheMinutes");
                        }
                    }
                    else
                    {
                        int isInteger = ValidationHelper.GetInteger(txtCacheMinutes.Text, -5);
                        if (isInteger <= 0)
                        {
                            correct = false;
                        }
                    }

                    cacheMinutes = ValidationHelper.GetInteger(txtCacheMinutes.Text, 0);
                }
                else if (radInherit.Checked)
                {
                    cacheMinutes = -1;
                }

                // Set cache minutes                
                if (cacheMinutes != node.NodeCacheMinutes)
                {
                    node.NodeCacheMinutes = cacheMinutes;
                    clearCache = true;
                }
            }

            if (correct)
            {
                // Save the data
                node.Update();

                // Update search index for node
                if ((node.PublishedVersionExists) && (SearchIndexInfoProvider.SearchEnabled))
                {
                    SearchTaskInfoProvider.CreateTask(SearchTaskTypeEnum.Update, PredefinedObjectType.DOCUMENT, SearchHelper.ID_FIELD, node.GetSearchID());
                }

                // Log synchronization
                DocumentHelper.LogSynchronization(node, CMS.Staging.TaskTypeEnum.UpdateDocument, tree);

                // Clear cache if cache settings changed
                if (clearCache)
                {
                    CacheHelper.ClearCache(null);
                }

                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                lblInfo.Visible = true;
                ReloadData();
            }
            else
            {
                lblError.Text = ResHelper.GetString("GeneralProperties.BadCacheMinutes");
                lblError.Visible = true;
            }
        }
    }


    /// <summary>
    /// Disables form editing.
    /// </summary>
    protected void DisableFormEditing()
    {
        canEdit = false;

        pnlDesign.Enabled = false;
        pnlOther.Enabled = false;
        pnlCache.Enabled = false;
        pnlOwner.Enabled = false;

        // Disable 'save button'
        lnkSave.Enabled = false;
        lnkSave.CssClass = "MenuItemEditDisabled";
        imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/savedisabled.png");
        btnResetRating.Enabled = false;
        usrOwner.Enabled = false;
        if (fcDocumentGroupSelector != null)
        {
            fcDocumentGroupSelector.Enabled = false;
        }
    }


    protected void radInherit_CheckedChanged(object sender, EventArgs e)
    {
        txtCacheMinutes.Enabled = false;

        if (radYes.Checked)
        {
            txtCacheMinutes.Enabled = true;
        }
    }


    protected void chkCssStyle_CheckedChanged(object sender, EventArgs e)
    {
        if (chkCssStyle.Checked)
        {
            ctrlSiteSelectStyleSheet.CurrentDropDown.Enabled = false;
            string value = PageInfoProvider.GetParentProperty(CMSContext.CurrentSite.SiteID, node.NodeAliasPath, "DocumentStylesheetID <> -1", "DocumentStylesheetID", null);
            if (String.IsNullOrEmpty(value))
            {
                ctrlSiteSelectStyleSheet.CurrentDropDown.SelectedValue = "-1";
            }
            else
            {
                try
                {
                    ctrlSiteSelectStyleSheet.CurrentDropDown.SelectedValue = value;
                }
                catch
                {
                }
            }


        }
        else
        {
            ctrlSiteSelectStyleSheet.CurrentDropDown.Enabled = true;
        }
    }


    /// <summary>
    /// Refreshes current rating result. 
    /// </summary>
    protected void RefreshCntRatingResult()
    {
        string msg = null;
        // Avoid division by zero
        if ((node != null) && (node.DocumentRatings > 0))
        {
            msg = String.Format(ResHelper.GetString("GeneralProperties.ContentRatingResult"), (node.DocumentRatingValue * 10) / node.DocumentRatings, node.DocumentRatings);
        }

        if (msg == null)
        {
            msg = ResHelper.GetString("general.na");
        }

        lblContentRatingResult.Text = msg;
    }


    /// <summary>
    /// Resets content rating score.
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Args</param>
    protected void btnResetRating_Click(object sender, EventArgs e)
    {
        if (node != null)
        {
            TreeProvider.ResetRating(node);
            RefreshCntRatingResult();
            ratingControl.CurrentRating = 0.0;
            ratingControl.ReloadData();
        }
    }

    #endregion
}
