using System;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_UICultures_UICulture_Header : SiteManagerPage
{
    protected int uiCultureID = 0;
    protected string currentUICultureName = "";
    protected UICultureInfo uici = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        uiCultureID = QueryHelper.GetInteger("UIcultureID", 0);

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }

        uici = UICultureInfoProvider.GetUICultureInfo(uiCultureID);
        if (uici != null)
        {
            currentUICultureName = uici.UICultureName;
        }

        InitializeMasterPage();
    }


    /// <summary>
    /// Initializes edit menu.
    /// </summary>
    protected void InitalizeMenu()
    {
        string[,] tabs = new string[2, 4];

        tabs[0, 0] = ResHelper.GetString("UICulture.Strings");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'strings_tab');";
        tabs[0, 2] = "UICulture_Strings_List.aspx?uicultureid=" + uiCultureID;

        tabs[1, 0] = ResHelper.GetString("General.General");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'new_culturegeneral_tab');";
        tabs[1, 2] = "UICulture_Edit.aspx?uicultureid=" + uiCultureID;

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "content";
    }


    /// <summary>
    /// Initializes MasterPage.
    /// </summary>
    protected void InitializeMasterPage()
    {
        // Initializes breadcrumbs
        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("UICulture_Edit.UICultures");
        breadcrumbs[0, 1] = "~/CMSSiteManager/Development/UICultures/UICulture_List.aspx";
        breadcrumbs[0, 2] = "_parent";
        breadcrumbs[1, 0] = currentUICultureName;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Development-UICulture_Edit.Title");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_UICulture/object.png");

        // Initializes help icon
        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.Title.HelpTopicName = "strings_tab";
    }
}
