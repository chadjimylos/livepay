using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Community;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.LicenseProvider;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Groups_Controls_Members_MemberEdit : CMSAdminEditControl
{
    #region "Variables"

    private GroupMemberInfo gmi = null;
    bool newItem = false;
    bool currentRolesLoaded = false;

    protected string currentValues = string.Empty;

    #endregion


    #region "Events"

    public event EventHandler OnApprove;
    public event EventHandler OnReject;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Current group member ID
    /// </summary>
    public int MemberID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["MemberID"], 0);
        }
        set
        {
            ViewState["MemberID"] = value;
        }
    }


    /// <summary>
    /// Current group ID
    /// </summary>
    public int GroupID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["GroupID"], 0);
        }
        set
        {
            ViewState["GroupID"] = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        RaiseOnCheckPermissions(CMSAdminControl.PERMISSION_READ, this);

        if (this.StopProcessing)
        {
            return;
        }

        // Is live site
        userSelector.IsLiveSite = this.IsLiveSite;
        userSelector.ShowSiteFilter = false;
        userSelector.HideHiddenUsers = true;
        usRoles.IsLiveSite = this.IsLiveSite;

        // In case of uniselector's callback calling must be where condition set here
        string where = CreateWhereCondition();
        usRoles.WhereCondition = where;

        if (!RequestHelper.IsPostBack() && !IsLiveSite)
        {
            ReloadData();
        }

        if (CMSContext.CurrentSite != null)
        {
            this.userSelector.SiteID = CMSContext.CurrentSite.SiteID;
        }

        this.btnSave.Click += new EventHandler(btnSave_Click);
        this.btnApprove.Click += new EventHandler(btnApprove_Click);
        this.btnReject.Click += new EventHandler(btnReject_Click);
    }


    /// <summary>
    /// Gets roles for current user 
    /// </summary>
    private void LoadCurrentRoles()
    {
        if (this.gmi != null)
        {
            DataSet ds = UserRoleInfoProvider.GetUserRoles("RoleID", "UserID = " + gmi.MemberUserID + "AND RoleID IN (SELECT RoleID FROM CMS_Role WHERE RoleGroupID = " + gmi.MemberGroupID + ")", null, 0, null);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "RoleID"));
            }

            currentRolesLoaded = true;
        }
    }


    /// <summary>
    /// Reloads data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        if ((this.MemberID > 0))
        {
            this.gmi = GroupMemberInfoProvider.GetGroupMemberInfo(this.MemberID);
        }

        // Get roles for current user 
        LoadCurrentRoles();

        string where = CreateWhereCondition();
        usRoles.WhereCondition = where;

        // Show message or uniselector?
        if (DataHelper.DataSourceIsEmpty(RoleInfoProvider.GetRoles("RoleID", where, null, 1, null)))
        {
            usRoles.Visible = false;
            lblRole.Visible = true;
        }
        else
        {
            usRoles.Visible = true;
            lblRole.Visible = false;
        }

        // Enable or disable buttons according to state of user's approval process
        if (gmi != null)
        {
            // Current user cannot approve/reject him self
            if (IsLiveSite && (gmi.MemberUserID == CMSContext.CurrentUser.UserID))
            {
                btnApprove.Enabled = false;
                btnReject.Enabled = false;
            }
            else if (gmi.MemberStatus == GroupMemberStatus.Approved)
            {
                btnApprove.Enabled = false;
                btnReject.Enabled = true;
            }
            else if (gmi.MemberStatus == GroupMemberStatus.Rejected)
            {
                btnApprove.Enabled = true;
                btnReject.Enabled = false;
            }
            else if (gmi.MemberStatus == GroupMemberStatus.WaitingForApproval)
            {
                btnApprove.Enabled = true;
                btnReject.Enabled = true;
            }
        }

        InitializeForm();
        usRoles.Value = currentValues;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (SaveData())
        {
            this.lblInfo.Text = ResHelper.GetString("general.changessaved");
            this.lblInfo.Visible = true;

            this.RaiseOnSaved();
        }
    }


    protected void btnApprove_Click(object sender, EventArgs e)
    {
        if (ApproveMember())
        {
            this.lblInfo.Text = ResHelper.GetString("group.member.userhasbeenapproved");
            this.lblInfo.Visible = true;
            ReloadData();

            if (OnApprove != null)
            {
                OnApprove(this, null);
            }
        }
    }


    protected void btnReject_Click(object sender, EventArgs e)
    {
        if (RejectMember())
        {
            this.lblInfo.Text = ResHelper.GetString("group.member.userhasbeenrejected");
            this.lblInfo.Visible = true;
            ReloadData();

            if (OnReject != null)
            {
                OnReject(this, null);
            }
        }
    }


    #region "Public methods"

    /// <summary>
    /// Resets the form to default values.
    /// </summary>
    public override void ClearForm()
    {
        this.txtComment.Text = "";
        this.chkApprove.Checked = true;
    }


    /// <summary>
    /// Approves member.
    /// </summary>
    public bool ApproveMember()
    {
        if (!CheckPermissions("cms.groups", CMSAdminControl.PERMISSION_MANAGE, this.GroupID))
        {
            return false;
        }

        EnsureMember();
        if ((this.gmi != null) && (CMSContext.CurrentUser != null))
        {
            this.gmi.MemberApprovedByUserID = CMSContext.CurrentUser.UserID;
            this.gmi.MemberStatus = GroupMemberStatus.Approved;
            this.gmi.MemberApprovedWhen = DateTime.Now;
            this.gmi.MemberRejectedWhen = DataHelper.DATETIME_NOT_SELECTED;
            GroupMemberInfoProvider.SetGroupMemberInfo(this.gmi);
            GroupInfo group = GroupInfoProvider.GetGroupInfo(this.GroupID);
            if ((group != null) && (group.GroupSendWaitingForApprovalNotification))
            {
                GroupMemberInfoProvider.SendNotificationMail("Groups.MemberApproved", CMSContext.CurrentSiteName, this.gmi, false);
            }

            this.lblMemberApproved.Text = GetApprovalInfoText(gmi.MemberApprovedWhen, gmi.MemberApprovedByUserID);
            this.lblMemberRejected.Text = GetApprovalInfoText(gmi.MemberRejectedWhen, gmi.MemberApprovedByUserID);
            return true;
        }
        return false;
    }


    /// <summary>
    /// Approves member.
    /// </summary>
    public bool RejectMember()
    {
        if (!CheckPermissions("cms.groups", CMSAdminControl.PERMISSION_MANAGE, this.GroupID))
        {
            return false;
        }

        EnsureMember();
        if ((this.gmi != null) && (CMSContext.CurrentUser != null))
        {
            this.gmi.MemberApprovedByUserID = CMSContext.CurrentUser.UserID;
            this.gmi.MemberStatus = GroupMemberStatus.Rejected;
            this.gmi.MemberApprovedWhen = DataHelper.DATETIME_NOT_SELECTED;
            this.gmi.MemberRejectedWhen = DateTime.Now;
            GroupMemberInfoProvider.SetGroupMemberInfo(this.gmi);
            GroupInfo group = GroupInfoProvider.GetGroupInfo(this.GroupID);
            if ((group != null) && (group.GroupSendWaitingForApprovalNotification))
            {
                GroupMemberInfoProvider.SendNotificationMail("Groups.MemberRejected", CMSContext.CurrentSiteName, this.gmi, false);
            }

            this.lblMemberApproved.Text = GetApprovalInfoText(gmi.MemberApprovedWhen, gmi.MemberApprovedByUserID);
            this.lblMemberRejected.Text = GetApprovalInfoText(gmi.MemberRejectedWhen, gmi.MemberApprovedByUserID);
            return true;
        }
        return false;
    }


    /// <summary>
    /// Updates the current Group or creates new if no MemberID is present.
    /// </summary>
    public bool SaveData()
    {
        if (!CheckPermissions("cms.groups", CMSAdminControl.PERMISSION_MANAGE, this.GroupID))
        {
            return false;
        }

        EnsureMember();

        newItem = (this.MemberID == 0);

        if (gmi != null)
        {
            UserInfo ui = UserInfoProvider.GetUserInfo(gmi.MemberUserID);
            if (ui != null)
            {
                // Save user roles
                SaveRoles(ui.UserID);

                gmi.MemberComment = this.txtComment.Text;
                GroupMemberInfoProvider.SetGroupMemberInfo(gmi);

                return true;
            }
        }
        else
        {
            // New member
            if (newItem)
            {
                int userId = ValidationHelper.GetInteger(userSelector.Value, 0);

                // Check if some user was selected
                if (userId == 0)
                {
                    lblError.Visible = true;
                    lblError.ResourceString = "group.member.selectuser";
                    return false;
                }

                // Check if user is not already group member
                gmi = GroupMemberInfoProvider.GetGroupMemberInfo(userId, this.GroupID);
                if (gmi != null)
                {
                    lblError.Visible = true;
                    lblError.ResourceString = "group.member.userexists";
                    return false;
                }

                gmi = new GroupMemberInfo();
                gmi.MemberGroupID = this.GroupID;
                gmi.MemberJoined = DateTime.Now;
                gmi.MemberUserID = userId;
                gmi.MemberComment = this.txtComment.Text;
                if (this.chkApprove.Checked)
                {
                    gmi.MemberStatus = GroupMemberStatus.Approved;
                    gmi.MemberApprovedWhen = DateTime.Now;
                    gmi.MemberApprovedByUserID = CMSContext.CurrentUser.UserID;
                }
                else
                {
                    gmi.MemberStatus = GroupMemberStatus.WaitingForApproval;
                    gmi.MemberApprovedByUserID = CMSContext.CurrentUser.UserID;
                }

                GroupMemberInfoProvider.SetGroupMemberInfo(gmi);
                GroupInfo group = GroupInfoProvider.GetGroupInfo(this.GroupID);
                if (group != null)
                {
                    if ((this.chkApprove.Checked) && (group.GroupSendWaitingForApprovalNotification))
                    {
                        GroupMemberInfoProvider.SendNotificationMail("Groups.MemberJoinedConfirmation", CMSContext.CurrentSiteName, gmi, false);
                    }
                    else
                    {
                        if (group.GroupSendWaitingForApprovalNotification)
                        {
                            GroupMemberInfoProvider.SendNotificationMail("Groups.MemberJoinedWaitingForApproval ", CMSContext.CurrentSiteName, gmi, false);
                        }
                    }
                }

                // Save user roles
                SaveRoles(userId);

                this.MemberID = gmi.MemberID;
                return true;
            }
        }

        return false;
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Ensures the memberinfo object is initialized
    /// </summary>
    private void EnsureMember()
    {
        if (this.MemberID > 0)
        {
            if (this.gmi == null)
            {
                this.gmi = GroupMemberInfoProvider.GetGroupMemberInfo(this.MemberID);
            }
        }
    }


    /// <summary>
    /// Creates where condition for unigrid with roles
    /// </summary>
    private string CreateWhereCondition()
    {
        string where = null;

        if (this.gmi != null)
        {
            where = "(RoleGroupID = " + this.gmi.MemberGroupID + ")";
        }
        else
        {
            where = "(RoleGroupID = " + this.GroupID + ")";
        }

        return where;
    }


    /// <summary>
    /// Initializes the contols in the form
    /// </summary>
    private void InitializeForm()
    {
        newItem = (this.MemberID == 0);

        // Intialize UI
        this.plcEdit.Visible = !newItem;
        this.plcNew.Visible = newItem;
        this.userSelector.Visible = newItem;
        this.btnApprove.Visible = !newItem;
        this.btnReject.Visible = !newItem;
        this.pnlRoles.GroupingText = ResHelper.GetString("group.member.memberinroles");

        if (newItem)
        {
            this.pnlRoles.GroupingText = ResHelper.GetString("group.member.addmemberinroles");
        }

        this.lblFullNameLabel.Text = ResHelper.GetString("general.user") + ResHelper.Colon;
        this.lblComment.Text = ResHelper.GetString("group.member.comment") + ResHelper.Colon;
        this.lblMemberApprovedLabel.Text = ResHelper.GetString("group.member.approved") + ResHelper.Colon;
        this.lblMemberApprove.Text = ResHelper.GetString("general.approve") + ResHelper.Colon;
        this.lblMemberRejectedLabel.Text = ResHelper.GetString("group.member.rejected") + ResHelper.Colon;
        this.lblMemberJoinedLabel.Text = ResHelper.GetString("group.member.joined") + ResHelper.Colon;

        // Initialize buttons
        this.btnSave.Text = ResHelper.GetString("general.ok");
        this.btnApprove.Text = ResHelper.GetString("general.approve");
        this.btnReject.Text = ResHelper.GetString("general.reject");

        ClearForm();

        // Handle existing Group editing - prepare the data
        if (this.MemberID > 0)
        {
            HandleExistingMember(gmi);
        }
    }


    /// <summary>
    /// Fills the data into form for specified Group member
    /// </summary>
    private void HandleExistingMember(GroupMemberInfo gmi)
    {
        if (gmi != null)
        {
            int userId = ValidationHelper.GetInteger(gmi.MemberUserID, 0);
            UserInfo ui = UserInfoProvider.GetUserInfo(userId);
            if (ui != null)
            {
                this.lblFullName.Text = HTMLHelper.HTMLEncode(ui.FullName);
            }

            this.txtComment.Text = gmi.MemberComment;
            this.lblMemberApproved.Text = GetApprovalInfoText(gmi.MemberApprovedWhen, gmi.MemberApprovedByUserID);
            this.lblMemberRejected.Text = GetApprovalInfoText(gmi.MemberRejectedWhen, gmi.MemberApprovedByUserID);
            this.lblMemberJoined.Text = CMSContext.ConvertDateTime(gmi.MemberJoined, this).ToString();
        }
    }


    /// <summary>
    /// Returns the approval text in format "date (approved by user full name)".
    /// </summary>
    /// <param name="date">Date time</param>
    /// <param name="userId">User id</param>
    private string GetApprovalInfoText(DateTime date, int userId)
    {
        string retval = "";

        if (date != DataHelper.DATETIME_NOT_SELECTED)
        {
            retval = CMSContext.ConvertDateTime(date, this).ToString();

            UserInfo ui = UserInfoProvider.GetUserInfo(userId);
            if (ui != null)
            {
                retval += " (" + HTMLHelper.HTMLEncode(ui.FullName) + ")";
            }
        }
        return retval;
    }


    /// <summary>
    /// Saves roles of specified user.
    /// </summary>    
    private void SaveRoles(int userID)
    {
        // Load user's roles
        if (!currentRolesLoaded)
        {
            LoadCurrentRoles();
        }

        // Remove old items
        string newValues = ValidationHelper.GetString(usRoles.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Removes relationship between user and role
                foreach (string item in newItems)
                {
                    int roleID = ValidationHelper.GetInteger(item, 0);
                    UserRoleInfoProvider.RemoveUserFromRole(userID, roleID);
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add relationship between user and role
                foreach (string item in newItems)
                {
                    int roleID = ValidationHelper.GetInteger(item, 0);
                    UserRoleInfoProvider.AddUserToRole(userID, roleID);
                }
            }
        }
    }

    #endregion
}
