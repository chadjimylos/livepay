using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.ExtendedControls;

public partial class CMSAdminControls_UI_PageElements_HeaderActions : HeaderActions
{
    #region "Variables"

    private string[,] mActions = null;
    private string mIconCssClass = "NewItemImage";
    private string mLinkCssClass = "NewItemLink";
    private string mPanelCssClass = null;
    private int mSeparatorWidth = 20;
    private bool mEnabled = true;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets the array of actions. The meaning of the indexes:
    /// (0,0): Type of the link (HyperLink or LinkButton), 
    /// (0,1): Text of the action link, 
    /// (0,2): JavaScript command to be performed OnClick action, 
    /// (0,3): NavigationUrl of the HyperLink (or PostBackUrl of LinkButton), 
    /// (0,4): Tooltip of the action link, 
    /// (0,5): Action image url, 
    /// (0,6): CommandName of the LinkButton Command event, 
    /// (0,7): CommandArgument of the LinkButton Command event.
    /// (0,8): Register shortcut action (TRUE/FALSE)
    /// (0,9): Enabled state of the action (TRUE/FALSE)
    /// (0,10): Visibility of the action (TRUE/FALSE)
    /// At least first two arguments must be defined.
    /// </summary>
    public override string[,] Actions
    {
        get
        {
            return this.mActions;
        }
        set
        {
            this.mActions = value;
        }
    }


    /// <summary>
    /// Gets or sets the enabled property of all actions.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return this.mEnabled;
        }
        set
        {
            Panel pnlActions = this.FindControl("pnlActions") as Panel;
            if (pnlActions != null)
            {
                foreach (Control c in pnlActions.Controls)
                {
                    LinkButton lnkButton = c as LinkButton;
                    if (lnkButton != null)
                    {
                        lnkButton.Enabled = value;
                        continue;
                    }
                    HyperLink lnkLink = c as HyperLink;
                    if (lnkLink != null)
                    {
                        lnkLink.Enabled = value;
                        continue;
                    }
                }
                this.mEnabled = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets the space between two actions (in pixels).
    /// </summary>
    public override int SeparatorWidth
    {
        get
        {
            return this.mSeparatorWidth;
        }
        set
        {
            this.mSeparatorWidth = value;
        }
    }


    /// <summary>
    /// Gets or sets CssClass of the Image element of the action.
    /// </summary>
    public override string IconCssClass
    {
        get
        {
            return this.mIconCssClass;
        }
        set
        {
            this.mIconCssClass = value;
        }
    }


    /// <summary>
    /// Gets or sets CssClass of the HyperLink or LinkButton element of the action.
    /// </summary>
    public override string LinkCssClass
    {
        get
        {
            return this.mLinkCssClass;
        }
        set
        {
            this.mLinkCssClass = value;
        }
    }


    /// <summary>
    /// Gets or sets CssClass of the panel where all the actions are placed.
    /// </summary>
    public override string PanelCssClass
    {
        get
        {
            return this.mPanelCssClass;
        }
        set
        {
            this.mPanelCssClass = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        ReloadData();
    }


    /// <summary>
    /// Reloads the actions.
    /// </summary>
    public override void ReloadData()
    {
        if (mActions != null)
        {
            pnlActions.Controls.Clear();

            if (!String.IsNullOrEmpty(this.mPanelCssClass))
            {
                pnlActions.CssClass = this.mPanelCssClass;
            }

            // Get the number of actions
            int actionCount = mActions.GetUpperBound(0) + 1;

            // Get the array size (number of arguments)
            int arraySize = mActions.GetUpperBound(1) + 1;

            // Exit if nothing about the action is specified
            if (arraySize < 2)
            {
                return;
            }

            // If there is more then one action render them to table
            bool useTable = (actionCount > 1);
            if (useTable)
            {
                pnlActions.Controls.Add(new LiteralControl("<table cellpadding=\"0\" cellspacing=\"0\"><tr>"));
            }

            // Generate the actions
            for (int i = 0; i < actionCount; i++)
            {
                // If the caption is not specified or visibility is false, skip the action
                string caption = ((mActions[i, 1] != null) ? mActions[i, 1] : null);
                bool visible = ((arraySize > 10) ? ValidationHelper.GetBoolean(mActions[i, 10], true) : true);
                if ((caption == null) || !visible)
                {
                    // Skip empty action
                    continue;
                }

                // Start tag of the table if more then one action is defined
                if (useTable)
                {
                    pnlActions.Controls.Add(new LiteralControl("<td>"));
                }

                // Get the action parameters
                bool useLinkButton = ((mActions[i, 0] != null) && (mActions[i, 0].ToLower() == TYPE_LINKBUTTON));
                bool useSaveButton = ((mActions[i, 0] != null) && (mActions[i, 0].ToLower() == TYPE_SAVEBUTTON));
                string javascript = ((arraySize > 2) ? mActions[i, 2] : null);
                string url = ((arraySize > 3) ? mActions[i, 3] : null);
                string tooltip = ((arraySize > 4) ? mActions[i, 4] : null);
                string iconUrl = ((arraySize > 5) ? mActions[i, 5] : null);
                string actionParamName = ((arraySize > 6) ? mActions[i, 6] : null);
                string actionParamArg = ((arraySize > 7) ? mActions[i, 7] : null);
                bool registerScript = ((arraySize > 8) ? ValidationHelper.GetBoolean(mActions[i, 8], false) : false);
                bool enabled = ((arraySize > 9) ? ValidationHelper.GetBoolean(mActions[i, 9], true) : true);
                string target = ((arraySize > 11) ? ValidationHelper.GetString(mActions[i, 11], "") : "");

                // Create image if url is not empty and add it to panel
                Image img = null;
                if (iconUrl != null)
                {
                    img = new Image();
                    img.ImageUrl = iconUrl;
                    img.ToolTip = tooltip;
                    img.AlternateText = caption;
                    img.CssClass = this.mIconCssClass;
                    img.Enabled = enabled;
                }

                if (!useSaveButton && img != null)
                {
                    pnlActions.Controls.Add(img);
                }

                if (useLinkButton)
                {
                    // Add LinkButton
                    LinkButton link = new LinkButton();
                    link.ID = this.ID + "HeaderAction" + i.ToString();
                    link.Text = caption;
                    link.OnClientClick = javascript;
                    link.PostBackUrl = url;
                    link.ToolTip = tooltip;
                    link.CommandName = actionParamName;
                    link.CommandArgument = actionParamArg;
                    link.CssClass = this.mLinkCssClass;
                    link.Enabled = enabled;
                    link.Command += new CommandEventHandler(RaiseActionPerformed);                    
                    pnlActions.Controls.Add(link);
                }
                else if (useSaveButton)
                {
                    // Add LinkButton
                    LinkButton link = new LinkButton();
                    link.OnClientClick = javascript;
                    link.ID = this.ID + "HeaderAction" + i.ToString();
                    link.PostBackUrl = url;
                    link.ToolTip = tooltip;
                    link.CommandName = actionParamName;
                    link.CommandArgument = actionParamArg;
                    link.CssClass = this.mLinkCssClass;
                    link.Enabled = enabled;
                    link.Command += new CommandEventHandler(RaiseActionPerformed);                    

                    if (img != null)
                    {
                        link.Controls.Add(img);
                    }

                    link.Controls.Add(new LiteralControl(caption));
                    pnlActions.Controls.Add(link);

                    // Register the CRTL+S shortcut
                    if (registerScript)
                    {
                        ControlsHelper.RegisterClientScriptBlock(this, this.Page, typeof(string), "EditShortcuts",
                        "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>" +
                        ScriptHelper.GetScript("function SaveDocument() { " + ControlsHelper.GetPostBackEventReference(link, actionParamArg) + " } \n"
                        ));
                    }
                }
                else
                {
                    // Add HyperLink
                    HyperLink link = new HyperLink();
                    link.Text = caption;
                    link.Attributes.Add("OnClick", javascript);
                    link.NavigateUrl = url;
                    link.ToolTip = tooltip;
                    link.CssClass = this.mLinkCssClass;
                    link.Enabled = enabled;
                    link.Target = target;
                    pnlActions.Controls.Add(link);
                }

                // Add free separator cell in table if it's not the last action
                if (useTable)
                {
                    pnlActions.Controls.Add(new LiteralControl((i < actionCount - 1 ? "</td><td style=\"width:" + this.mSeparatorWidth + "px; \" />" : "</td>")));
                }
            }

            // End tag of the table
            if (useTable)
            {
                pnlActions.Controls.Add(new LiteralControl("</tr></table>"));
            }
        }

    }


    /// <summary>
    /// Clears content rendered by header actions control
    /// </summary>
    public void Clear() 
    {
        this.pnlActions.Controls.Clear();
    }
}
