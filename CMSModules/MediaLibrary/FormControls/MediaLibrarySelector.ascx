<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MediaLibrarySelector.ascx.cs"
    Inherits="CMSModules_MediaLibrary_FormControls_MediaLibrarySelector" %>
<%@ Register Src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" TagName="UniSelector"
    TagPrefix="cms" %>
<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" ResourcePrefix="medialibraryselect"
            ObjectType="media.library" OrderBy="LibraryName" AllowEditTextBox="true" SelectionMode="SingleDropDownList" DisplayNameFormat="{%LibraryDisplayName%}" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
