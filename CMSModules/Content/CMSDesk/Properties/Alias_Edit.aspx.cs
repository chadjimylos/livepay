using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.WorkflowEngine;
using CMS.Staging;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Properties_Alias_Edit : CMSPropertiesPage
{
    #region "Private variables"

    protected int aliasId = 0;
    protected int nodeId = 0;

    protected TreeNode node = null;
    protected TreeProvider tree = null;

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.URLs"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.URLs");
        }

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "URLs.Aliases")) 
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "URLs.Aliases");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {       
        nodeId = ValidationHelper.GetInteger(Request.QueryString["nodeid"],0);

        if (nodeId > 0)
        {
            // Get the node
            tree = new TreeProvider(CMSContext.CurrentUser);
            node = tree.SelectSingleNode(nodeId);

            if (node != null)
            {
                // Check read permissions
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                {
                    RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
                }
                // Check modify permissions
                else if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                {                   
                    this.lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                    txtTrackCampaign.Enabled = false;
                    txtURLExtensions.Enabled = false;
                    txtURLPath.Enabled = false;
                    cultureSelector.Enabled = false;
                }
                
                if (QueryHelper.GetInteger("saved", 0) == 1)
                {
                    lblInfo.Text = ResHelper.GetString("general.changessaved");
                }
                
                lblDocumentCulture.Text = ResHelper.GetString("general.culture") + ResHelper.Colon;
                lblTrackCampaign.Text = ResHelper.GetString("doc.urls.trackcampaign") + ResHelper.Colon;
                lblURLPath.Text = ResHelper.GetString("doc.urls.urlpath") + ResHelper.Colon;
                lblURLExtensions.Text = ResHelper.GetString("doc.urls.urlextensions") + ResHelper.Colon;
                btnOk.Text = ResHelper.GetString("general.ok");
                valURLPath.ErrorMessage = ResHelper.GetString("doc.urls.requiresurlpath");


                // Initialiaze page title

                string urls = ResHelper.GetString("Properties.Urls");
                string urlsUrl = "~/CMSModules/Content/CMSDesk/Properties/Alias_List.aspx?nodeid=" + nodeId.ToString();
                string addAlias = ResHelper.GetString("doc.urls.addnewalias");                
                // initializes page title
                string[,] pageTitleTabs = new string[2, 3];
                pageTitleTabs[0, 0] = urls;
                pageTitleTabs[0, 1] = urlsUrl;
                pageTitleTabs[0, 2] = "";
                pageTitleTabs[1, 0] = addAlias;
                pageTitleTabs[1, 1] = "";
                pageTitleTabs[1, 2] = "";
                pageAlias.Breadcrumbs = pageTitleTabs;

                this.cultureSelector.AddDefaultRecord = false;
                this.cultureSelector.SpecialFields = new string[,] { { ResHelper.GetString("general.selectall"), "" } };
                this.cultureSelector.CssClass = "ContentMenuLangDrop";

                aliasId = ValidationHelper.GetInteger(Request.QueryString["aliasid"], 0);


                if (RequestHelper.IsPostBack())
                {
                    lblInfo.Text = ResHelper.GetString("general.changessaved");
                }
                else
                {
                    this.cultureSelector.Value = node.DocumentCulture;

                    // Edit existing alias
                    if (aliasId > 0)
                    {                        
                        DocumentAliasInfo dai = DocumentAliasInfoProvider.GetDocumentAliasInfo(aliasId);

                        if (dai != null)
                        {
                            txtTrackCampaign.Text = dai.AliasCampaign;
                            txtURLExtensions.Text = dai.AliasExtensions;
                            txtURLPath.Text = dai.AliasURLPath;
                            cultureSelector.Value = dai.AliasCulture;
                            pageTitleTabs[1, 0] = addAlias = dai.AliasURLPath;
                        }
                    }                   
                }

                pageAlias.Breadcrumbs = pageTitleTabs;
            }
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.lblInfo.Visible = (this.lblInfo.Text != "");
        this.lblError.Visible = (this.lblError.Text != "");
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        DocumentAliasInfo dai = null ;

        // Edit existing alias
        if (aliasId > 0 )
        {
            dai = DocumentAliasInfoProvider.GetDocumentAliasInfo(aliasId);
          
        }

        if (dai == null)
        {
            dai = new DocumentAliasInfo();
        }

        // Add "/" if not present
        string urlPath = txtURLPath.Text.Trim();
        if (!urlPath.StartsWith("/"))
        {
            urlPath = "/" + urlPath;
        }

        // Set object properties
        dai.AliasURLPath = urlPath;
        dai.AliasCampaign = txtTrackCampaign.Text.Trim();
        dai.AliasExtensions = txtURLExtensions.Text.Trim();
        dai.AliasCulture = ValidationHelper.GetString(cultureSelector.Value, "");
        dai.AliasSiteID = CMSContext.CurrentSite.SiteID;

        if (nodeId > 0)
        {
            dai.AliasNodeID = nodeId;
        }
        

        //Insert into database
        dai.SetObject();

        // Log synchronization
        DocumentHelper.LogSynchronization(node, TaskTypeEnum.UpdateDocument, tree, SynchronizationInfoProvider.ENABLED_SERVERS);

        nodeId = dai.AliasNodeID;
        aliasId = dai.AliasID; 
        UrlHelper.Redirect("Alias_Edit.aspx?saved=1&nodeid=" + nodeId.ToString() + "&aliasid=" + aliasId.ToString()); 
    }
    
}
