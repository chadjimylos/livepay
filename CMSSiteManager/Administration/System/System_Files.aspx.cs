using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_System_System_Files : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblInfo.Text = ResHelper.GetString("System_Files.TestInfo");
        btnTest.Text = ResHelper.GetString("System_Files.TestButton");
    }


    /// <summary>
    /// On btnTest click.
    /// </summary>
    protected void btnTest_Click(object sender, EventArgs e)
    {
        // path to 'TestFiles' directory
        string path = "~/CMSSiteUtils/TestFiles/";
        if (HttpContext.Current != null)
        {
            path = HttpContext.Current.Server.MapPath(path);
        }
        
        DirectoryInfo di = new DirectoryInfo(path);

        if (di.Exists)
        {
            CreateDeleteTest(path, di);
            // Modify file
            ModifyTest(path);
        }
        else
        {
            ltlInfo.Text += "Directory " + path + " wasn't found!";
        }
    }


    /// <summary>
    /// Create&Delete directory/file test.
    /// </summary>
    /// <param name="path">Path where to create subdirectory.</param>
    /// <param name="di">Parent directory info.</param>
    protected void CreateDeleteTest(string path, DirectoryInfo di)
    {
        FileInfo fi = null;
        DirectoryInfo sdi = null;

        string strCreating = ResHelper.GetString("System_Files.Creating");
        string strDeleting = ResHelper.GetString("System_Files.Deleting");
        string strOK = ResHelper.GetString("General.OK");
        string strFailed = ResHelper.GetString("System_Files.Failed");

        DateTime dt = DateTime.Now;
        // Create subdirectory name
        string subdir = "TestFolder" + dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() +
            dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString();

        ltlInfo.Text += strCreating + " '~/CMSSiteUtils/TestFiles/" + subdir + "/' - ";
        try
        {
            // Create subdirectory
            sdi = di.CreateSubdirectory(subdir);
            ltlInfo.Text += strOK;
        }
        catch
        {
            ltlInfo.Text += strFailed;
        }

        // Create test file name
        string fileName = "TestFile" + dt.Year.ToString() + dt.Month.ToString() + dt.Day.ToString() +
            dt.Hour.ToString() + dt.Minute.ToString() + dt.Second.ToString() + ".txt";

        ltlInfo.Text += "<br /><br />" + strCreating + " '~/CMSSiteUtils/TestFiles/" + subdir + "/" + fileName + "' - ";
        try
        {
            // Create test file
            fi = new FileInfo(path + subdir + "/" + fileName);
            using (StreamWriter sw = fi.CreateText())
            {
                sw.WriteLine("Edited - " + dt.ToString());
            }
            ltlInfo.Text += strOK;
        }
        catch
        {
            ltlInfo.Text += strFailed;
        }

        ltlInfo.Text += "<br /><br />" + strDeleting + " '~/CMSSiteUtils/TestFiles/" + subdir + "/" + fileName + "' - ";
        try
        {
            // Delete test file
            fi.Delete();
            ltlInfo.Text += strOK;
        }
        catch
        {
            ltlInfo.Text += strFailed;
        }


        ltlInfo.Text += "<br /><br />" + strDeleting + " '~/CMSSiteUtils/TestFiles/" + subdir + "/' - ";
        try
        {
            // Delete subdirectory
            sdi.Delete();
            ltlInfo.Text += strOK;
        }
        catch
        {
            ltlInfo.Text += strFailed;
        }
    }


    /// <summary>
    /// Testing '~/CMSSiteUtils/TestFiles/TestModify.txt' modification.
    /// </summary>
    protected void ModifyTest(string path)
    {
        ltlInfo.Text += "<br /><br />" + ResHelper.GetString("System_Files.Modifying") + " '~/CMSSiteUtils/TestFiles/TestModify.txt' - ";
        string filePath = path + "TestModify.txt";
        string strOK = ResHelper.GetString("General.OK");
        string strFailed = ResHelper.GetString("System_Files.Failed");
        
        FileInfo fi = new FileInfo(filePath);
        // If the file exists, try to append some text
        if (fi.Exists)
        {
            try
            {
                using (StreamWriter sw = fi.CreateText())
                {
                    sw.WriteLine(ltlInfo.Text.Replace("<br />", "\n") + strOK);
                }
                ltlInfo.Text += strOK;
            }
            catch
            {
                ltlInfo.Text += strFailed;
            }
        }
        else
        {
            ltlInfo.Text += strFailed;
        }
    }
}
