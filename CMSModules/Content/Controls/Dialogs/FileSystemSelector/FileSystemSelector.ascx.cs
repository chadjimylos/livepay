﻿using System;
using System.Web.UI;
using System.Collections;
using System.IO;
using System.Text;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.ExtendedControls.Dialogs;
using CMS.CMSHelper;
using CMS.ExtendedControls;
using System.Security;

public partial class CMSModules_Content_Controls_Dialogs_FileSystemSelector_FileSystemSelector : CMSUserControl//LinkMediaSelector
{
    #region "Private variables"

    // Content variables
    private string mNodeID = "";
    protected FileSystemDialogConfiguration mConfig = null;
    private bool mIsAction = false;
    private Hashtable mParameters = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets last searched value.
    /// </summary>
    private string LastSearchedValue
    {
        get
        {
            return this.hdnLastSearchedValue.Value;
        }
        set
        {
            this.hdnLastSearchedValue.Value = value;
        }
    }


    /// <summary>
    /// Gets current action name.
    /// </summary>
    private string CurrentAction
    {
        get
        {
            return this.hdnAction.Value.ToLower().Trim();
        }
        set
        {
            this.hdnAction.Value = value;
        }
    }


    /// <summary>
    /// Gets current action argument value.
    /// </summary>
    private string CurrentArgument
    {
        get
        {
            return this.hdnArgument.Value;
        }
    }


    /// <summary>
    /// Returns current properties (according to OutputFormat).
    /// </summary>
    protected ItemProperties Properties
    {
        get
        {
            return this.pathProperties;
        }
    }


    /// <summary>
    /// Update panel where properties control resides.
    /// </summary>
    protected UpdatePanel PropertiesUpdatePanel
    {
        get
        {
            return this.pnlUpdateProperties;
        }
    }


    /// <summary>
    /// Gets or sets ID of the node selected in the content tree.
    /// </summary>
    private string NodeID
    {
        get
        {
            if (String.IsNullOrEmpty(this.mNodeID))
            {
                this.mNodeID = ValidationHelper.GetString(this.hdnLastNodeSlected.Value, "");
            }
            return this.mNodeID;
        }
        set
        {
            this.mNodeID = value;
            this.hdnLastNodeSlected.Value = value.ToString();
        }
    }


    /// <summary>
    /// Maximum number of tree nodes displayed within the tree.
    /// </summary>
    private int MaxTreeNodes
    {
        get
        {
            string siteName = CMSContext.CurrentSiteName;
            return SettingsKeyProvider.GetIntValue((String.IsNullOrEmpty(siteName) ? "" : siteName + ".") + "CMSMaxTreeNodes");
        }
    }


    /// <summary>
    /// Indicates whether the asynchronous postback occurs on the page.
    /// </summary>
    private bool IsAsyncPostback
    {
        get
        {
            return ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack;
        }
    }


    /// <summary>
    /// Indicates whether the post back is result of some hidden action.
    /// </summary>
    private bool IsAction
    {
        get
        {
            return this.mIsAction;
        }
        set
        {
            this.mIsAction = value;
        }
    }


    /// <summary>
    /// Indicates whether the content tree is displaying more than max tree nodes.
    /// </summary>
    private bool IsDisplayMore
    {
        get
        {
            return this.fileSystemView.IsDisplayMore;
        }
        set
        {
            this.fileSystemView.IsDisplayMore = value;
        }
    }


    /// <summary>
    /// Gets or sets selected item to colorize.
    /// </summary>
    private String ItemToColorize
    {
        get
        {
            return ValidationHelper.GetString(ViewState["ItemToColorize"], String.Empty);
        }
        set
        {
            ViewState["ItemToColorize"] = value;
        }
    }


    /// <summary>
    /// Indicates if properties are displayed in full height mode
    /// </summary>
    private bool IsFullDisplay
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["IsFullDisplay"], false);
        }
        set
        {
            ViewState["IsFullDisplay"] = value;
        }
    }


    /// <summary>
    /// Value of node under which more content should be displayed
    /// </summary>
    private string MoreContentNode
    {
        get
        {
            return ValidationHelper.GetString(ViewState["MoreContentNode"], "");
        }
        set
        {
            ViewState["MoreContentNode"] = value;
        }
    }


    /// <summary>
    /// Dialog configuration
    /// </summary>
    protected FileSystemDialogConfiguration Config
    {
        get
        {
            if (mConfig == null)
            {
                mConfig = new FileSystemDialogConfiguration();
            }
            return mConfig;
        }
        set
        {
            mConfig = value;
        }
    }


    /// <summary>
    /// Full file system starting path of dialog
    /// </summary>
    public string FullStartingPath
    {
        get
        {
            if (this.Config.StartingPath.StartsWith("~"))
            {
                return Server.MapPath(this.Config.StartingPath).TrimEnd('\\');
            }
            else
            {
                if (this.Config.StartingPath.EndsWith(":\\"))
                {
                    return this.Config.StartingPath;
                }
            }
            return this.Config.StartingPath.TrimEnd('\\');

        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Dialog parameters collection.
    /// </summary>
    public Hashtable Parameters
    {
        get
        {
            if (this.mParameters == null)
            {
                // Try to get parameters from the session
                object dp = SessionHelper.GetValue("DialogParameters");
                if (dp != null)
                {
                    this.mParameters = (dp as Hashtable);
                }
            }
            return this.mParameters;
        }
        set
        {
            this.mParameters = value;
            SessionHelper.SetValue("DialogParameters", value);
        }
    }

    #endregion


    #region "Public properties"



    #endregion


    #region "Control methods"

    /// <summary>
    /// Init
    /// </summary>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        InitFromQueryString();

    }


    /// <summary>
    /// Pre render
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // High-light item being edited
        if (this.ItemToColorize != String.Empty)
        {
            ColorizeRow(this.ItemToColorize);
        }

        // Display info on listing more content
        if (this.IsDisplayMore && !this.Config.ShowFolders) //curently selected more object && (this.TreeNodeObj != null))
        {
            string closeLink = "<span class=\"ListingClose\" style=\"cursor: pointer;\" onclick=\"SetAction('closelisting', ''); RaiseHiddenPostBack(); return false;\">" + ResHelper.GetString("general.close") + "</span>";
            string currentPath = "<span class=\"ListingPath\">";

            // Display relative paths with tilda
            if (this.Config.StartingPath.StartsWith("~"))
            {
                string serverPath = Server.MapPath(this.Config.StartingPath).TrimEnd('\\');
                currentPath += this.NodeID.Replace(serverPath.Substring(0, serverPath.LastIndexOf('\\') + 1), "");
            }
            else
            {
                currentPath += this.NodeID.Replace(this.NodeID.Substring(0, this.Config.StartingPath.TrimEnd('\\').LastIndexOf('\\') + 1), "");
            }
            currentPath += "</span>";

            string listingMsg = string.Format(ResHelper.GetString("dialogs.filesystem.listinginfo"), currentPath, closeLink);
            this.fileSystemView.DisplayListingInfo(listingMsg);
        }
    }


    /// <summary>
    /// Page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.StopProcessing)
        {
            SetupControls();
            EnsureLoadedData();
        }
        else
        {
            this.Visible = false;
        }
    }

    #endregion


    #region "Control initialization"

    /// <summary>
    /// Initializes additional controls
    /// </summary>
    private void SetupControls()
    {
        // Initialize design scripts
        InitializeDesignScripts();

        this.fileSystemView.IsLiveSite = this.IsLiveSite;
        InitializeFileSystemView();

        if (!IsAsyncPostback)
        {
            // Initialize scripts
            InitializeControlScripts();
            
            // Initialize content tree control
            InitializeFileSystemTree();
            string parameter = this.FullStartingPath;
            if (!String.IsNullOrEmpty(this.Config.DefaultPath))
            {
                parameter += "\\" + this.Config.DefaultPath;
            }
            HandleFolderAction(parameter.Replace("'", "\\'"), false);
            if (!String.IsNullOrEmpty(this.Config.SelectedPath))
            {
                string fsPath = this.Config.SelectedPath;
                if(this.Config.StartingPath.StartsWith("~"))
                {
                    fsPath = Server.MapPath(fsPath);
                }

                bool isFile = File.Exists(fsPath);
                bool exist = isFile || Directory.Exists(fsPath);
                if (exist)
                {
                    string size = "";
                    try
                    {

                        if (isFile)
                        {
                            FileInfo fi = new FileInfo(fsPath);
                            size = DataHelper.GetSizeString(ValidationHelper.GetLong(fi.Length, 0));
                        }
                        else
                        {
                            DirectoryInfo di = new DirectoryInfo(fsPath);
                        }
                    }
                    catch (Exception)
                    {
                    }
                    string selectedPath = this.Config.SelectedPath;
                    //if (this.Config.SelectedPath.StartsWith("~"))
                    //{
                    //    selectedPath = Server.MapPath(this.Config.SelectedPath);
                    //}
                    SelectMediaItem(fsPath + ';' + size + ';' + isFile);
                }
            }
        }

        

    }


    /// <summary>
    /// Initialize design jQuery scripts
    /// </summary>
    private void InitializeDesignScripts()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("setTimeout('InitializeDesign();',10);");
        sb.Append("$j(window).resize(function() { InitializeDesign(); });");
        ScriptManager.RegisterStartupScript(Page, typeof(Page), "designScript", sb.ToString(), true);
    }


    /// <summary>
    /// Initializes all the script required for communication between controls.
    /// </summary>
    private void InitializeControlScripts()
    {
        // SetAction function setting action name and passed argument
        string setAction = "function SetAction(action, argument) {                                              " +
                           "    var hdnAction = document.getElementById('" + this.hdnAction.ClientID + "');     " +
                           "    var hdnArgument = document.getElementById('" + this.hdnArgument.ClientID + "'); " +
                           @"    if ((hdnAction != null) && (hdnArgument != null)) {                             
                                   if (action != null) {                                                       
                                       hdnAction.value = action;                                               
                                   }                                                                           
                                   if (argument != null) {                                                     
                                       hdnArgument.value = argument;                                           
                                   }                                                                           
                               }                                                                               
                           }                                                                                    ";

        // Get reffernce causing postback to hidden button
        string postBackRef = ControlsHelper.GetPostBackEventReference(this.hdnButton, "");
        string raiseOnAction = " function RaiseHiddenPostBack(){" + postBackRef + ";}\n";

        this.ltlScript.Text = ScriptHelper.GetScript(setAction + raiseOnAction);
    }


    /// <summary>
    /// Initialization of file grid control
    /// </summary>
    private void InitializeFileSystemView()
    {
        this.fileSystemView.Config = this.Config;

    }


    /// <summary>
    /// Initializes content tree element.
    /// </summary>
    private void InitializeFileSystemTree()
    {
        this.treeFileSystem.Visible = true;

        this.treeFileSystem.DeniedNodePostback = false;
        this.treeFileSystem.AllowMarks = false;
        this.treeFileSystem.NodeTextTemplate = "<span class=\"ContentTreeItem\" onclick=\"SelectNode('##NODEID##', this); SetAction('contentselect', '##NODEID##;##NODECHILDNODESCOUNT##'); RaiseHiddenPostBack(); return false;\">##ICON##<span class=\"Name\">##NODENAME##</span></span>";
        this.treeFileSystem.SelectedNodeTextTemplate = "<span id=\"treeSelectedNode\" class=\"ContentTreeSelectedItem\" onclick=\"SelectNode('##NODEID##', this); SetAction('contentselect', '##NODEID##;##NODECHILDNODESCOUNT##'); RaiseHiddenPostBack(); return false;\">##ICON##<span class=\"Name\">##NODENAME##</span></span>";
        this.treeFileSystem.MaxTreeNodeText = "<span class=\"ContentTreeItem\" onclick=\"SetAction('morecontentselect', '##PARENTNODEID##'); RaiseHiddenPostBack(); return false;\"><span class=\"Name\" style=\"font-style: italic;\">" + ResHelper.GetString("ContentTree.SeeListing") + "</span></span>";
        this.treeFileSystem.IsLiveSite = this.IsLiveSite;
        this.treeFileSystem.ExpandDefaultPath = true;
        this.treeFileSystem.StartingPath = this.Config.StartingPath;
        if (this.treeFileSystem.DefaultPath == String.Empty)
        {
            this.treeFileSystem.DefaultPath = this.Config.DefaultPath;
        }
        this.treeFileSystem.AllowedFolders = this.Config.AllowedFolders;
        this.treeFileSystem.ExcludedFolders = this.Config.ExcludedFolders;

    }


    /// <summary>
    /// Ensures that required data are displayed
    /// </summary>
    private void EnsureLoadedData()
    {
        // If no action takes place
        if ((this.CurrentAction == "") && (UrlHelper.IsPostback()))
        {
            this.fileSystemView.StartingPath = this.NodeID;
        }

    }

    #endregion


    #region "Common event methods"

    /// <summary>
    /// Behaves as mediator in communication line between control taking action and the rest of the same level controls.
    /// </summary>
    protected void hdnButton_Click(object sender, EventArgs e)
    {
        this.IsAction = true;

        switch (this.CurrentAction)
        {
            case "insertitem":
                GetSelectedItem();
                break;

            case "search":
                HandleSearchAction(this.CurrentArgument);
                break;

            case "select":
                HandleSelectAction(this.CurrentArgument);
                break;

            case "morecontentselect":
            case "contentselect":
                // Reset previous filter value
                ResetSearchFilter();

                string[] argArr = this.CurrentArgument.Split(';');

                int childNodesCnt = 0;
                if (argArr.Length == 2)
                {
                    childNodesCnt = ValidationHelper.GetInteger(argArr[1], 0);
                }

                // If more content is requested
                this.IsDisplayMore = (!this.IsDisplayMore ? (((this.CurrentAction == "morecontentselect") || (childNodesCnt > this.MaxTreeNodes))) : this.IsDisplayMore);
                HandleFolderAction(argArr[0], this.IsDisplayMore);
                if (this.Config.ShowFolders)
                {
                    HandleSelectAction(argArr[0]+";;false");
                }
                break;

            case "parentselect":
                try
                {
                    DirectoryInfo dir = new DirectoryInfo(this.CurrentArgument);
                    int childNodes = 0;
                    childNodes = dir.GetDirectories().Length;
                    if (childNodes > this.MaxTreeNodes)
                    {
                        this.IsDisplayMore = (!this.IsDisplayMore ? (childNodes > this.MaxTreeNodes) : this.IsDisplayMore);
                    }
                }
                // If error occured don't do a thing
                catch
                {
                }

                HandleFolderAction(this.CurrentArgument, true);
                break;

            case "closelisting":
                this.IsDisplayMore = false;
                this.MoreContentNode = null;
                HandleFolderAction(this.NodeID.ToString(), false);
                break;

            case "cancelfolder":
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "EnsureTopWindow", "if (self.focus) { self.focus(); }", true);
                ClearActionElems();
                break;

            default:
                ColorizeLastSelectedRow();
                this.pnlUpdateView.Update();
                break;
        }
    }
    

    /// <summary>
    /// Handles actions occuring when some text is searched.
    /// </summary>
    /// <param name="argument">Argument holding information on searched text.</param>
    private void HandleSearchAction(string argument)
    {
        this.LastSearchedValue = argument;

        // Load new data filtered by searched text 
        this.fileSystemView.SearchText = argument;
        this.fileSystemView.StartingPath = this.NodeID;
        // Reload content
        this.fileSystemView.Reload();
        this.pnlUpdateView.Update();

        // Keep focus in search text box
        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "SetSearchFocus", "setTimeout('SetSearchFocus();', 200);", true);
        
        // Forget recent action
        ClearActionElems();
    }


    /// <summary>
    /// Handles actions occuring when some item is selected.
    /// </summary>
    /// <param name="argument">Argument holding information on selected item.</param>
    private void HandleSelectAction(string argument)
    {
        // Create new selected media item
        SelectMediaItem(argument);

        // Forget recent action
        ClearActionElems();
    }


    /// <summary>
    /// Handles actions related to the folders.
    /// </summary>
    /// <param name="argument">Argument related to the folder action.</param>
    /// <param name="isNewFolder">Indicates if is new folder</param>
    private void HandleFolderAction(string argument, bool forceReload)
    {
        HandleFolderAction(argument, forceReload, true);
    }


    /// <summary>
    /// Handles actions related to the folders.
    /// </summary>
    /// <param name="argument">Argument related to the folder action.</param>
    /// <param name="isNewFolder">Indicates if is new folder</param>
    /// <param name="callSelection">Indicates if selection should be called</param>
    private void HandleFolderAction(string argument, bool forceReload, bool callSelection)
    {
        this.NodeID = ValidationHelper.GetString(argument, "");       

        // Reload content tree if neccessary
        if (forceReload)
        {
            InitializeFileSystemTree();

            // Fill with new info
            this.treeFileSystem.DefaultPath = this.NodeID;
            this.treeFileSystem.ExpandDefaultPath = true;

            this.treeFileSystem.ReloadData();
            this.pnlUpdateTree.Update();

            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "EnsureTopWindow", "if (self.focus) { self.focus(); }", true);
        }

        ColorizeLastSelectedRow();

        // Get parent node ID info
        string parentId = "";
        if (this.FullStartingPath.ToLower() != this.NodeID.ToLower())
        try
        {
            parentId = (new DirectoryInfo(this.NodeID)).Parent.FullName;
        }
        // Access denied to parent 
        catch (SecurityException)
        {
        }

        this.fileSystemView.ShowParentButton = !String.IsNullOrEmpty(parentId);
        this.fileSystemView.NodeParentID = parentId;
        this.fileSystemView.Config = this.Config;

        // Load new data
        if ((this.Config.ShowFolders) && (argument.LastIndexOf('\\') != -1))
        {
            this.fileSystemView.StartingPath = argument.Substring(0, argument.LastIndexOf('\\') + 1);
        }

        this.fileSystemView.StartingPath = argument;

        // Reload view control's content
        this.fileSystemView.Reload();
        this.pnlUpdateView.Update();

        ClearActionElems();
    }

    #endregion


    #region "Helper methods"

    /// <summary>
    /// Returns selected item parameters as name-value collection.
    /// </summary>
    public void GetSelectedItem()
    {
        if (this.Properties.Validate())
        {
            // Get selected item information
            Hashtable properties = this.Properties.GetItemProperties();

            // Get JavaScript for inserting the item
            string script = CMSDialogHelper.GetFileSystemItem(properties);
            if (!string.IsNullOrEmpty(script))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "insertItemScript", script, true);
            }
        }
        else
        {
            // Display error message
            this.pnlUpdateProperties.Update();
        }
    }
    
    
    /// <summary>
    /// Performs actions necessary to select particular item from a list.
    /// </summary>
    private void SelectMediaItem(string argument)
    {
        if (!string.IsNullOrEmpty(argument))
        {
            string[] argArr = argument.ToString().Split(';');
            if (argArr.Length >= 2)
            {
                // Get information from argument
                string path = argArr[0];
                string size = ValidationHelper.GetString(argArr[1], "");
                bool isFile = ValidationHelper.GetBoolean(argArr[2], true);

                bool avoidPropUpdate = this.ItemToColorize.Equals(path);

                if ((isFile) && (File.Exists(path)))
                {
                    FileInfo fi = new FileInfo(path);
                    path = fi.FullName;
                }
                else if(Directory.Exists(path))
                {
                    DirectoryInfo di = new DirectoryInfo(path);
                    path = di.FullName;
                }

                this.ItemToColorize = path.Replace("\\", "\\\\").Replace("'", "\\'").ToLower();

                if (!avoidPropUpdate)
                {
                    // Get selected properties from session
                    Hashtable selectedParameters = SessionHelper.GetValue("DialogSelectedParameters") as Hashtable;
                    if (selectedParameters == null)
                    {
                        selectedParameters = new Hashtable();
                    }

                    // Update selected properties
                    selectedParameters[DialogParameters.ITEM_PATH] = path;
                    selectedParameters[DialogParameters.ITEM_SIZE] = size;
                    selectedParameters[DialogParameters.ITEM_ISFILE] = isFile;
                    selectedParameters[DialogParameters.ITEM_RELATIVEPATH] = this.Config.StartingPath.StartsWith("~");

                    // Force media properties control to load selected item
                    this.Properties.LoadItemProperties(selectedParameters);

                    // Update properties panel
                    this.PropertiesUpdatePanel.Update();
                }
            }
        }
    }


    /// <summary>
    /// Highlights item specified by its ID.
    /// </summary>
    /// <param name="itemId">String representation of item ID.</param>
    protected void ColorizeRow(string itemId)
    {
        // Keep item selected
        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ColorizeSelectedRow", "function tryColorizeRow(itemId) { if(typeof(ColorizeRow) == 'function'){ ColorizeRow(itemId); } else { setTimeout(\'tryColorizeRow(\"" + itemId + "\");\', 500); } }; tryColorizeRow(\"" + itemId + "\");", true);
    }


    /// <summary>
    /// Clears hidden control elements fo future use.
    /// </summary>
    private void ClearActionElems()
    {
        this.CurrentAction = "";
        this.hdnArgument.Value = "";
    }


    /// <summary> 
    /// Highlights row recently selected.
    /// </summary>
    protected void ColorizeLastSelectedRow()
    {
        // Keep item selected
        ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "ColorizeLastSelectedRow", "if(typeof(ColorizeLastRow) == 'function'){ ColorizeLastRow(); }", true);
    }


    /// <summary>
    /// Loads selected item parameters into the selector.
    /// </summary>
    public void LoadItemConfiguration()
    {

        // Load properties
        this.Properties.LoadItemProperties(this.Parameters);
        this.pnlUpdateProperties.Update();

        // Remember item to colorize
        this.ItemToColorize = this.NodeID.Replace("\\", "\\\\").Replace("'", "\\'");

    }


    /// <summary>
    /// Initialization from query string
    /// </summary>
    public void InitFromQueryString()
    {

        // Get allowed and excluded folders and extensions        
        this.Config.AllowedExtensions = QueryHelper.GetString("allowed_extensions", "");
        this.Config.AllowedFolders = QueryHelper.GetString("allowed_folders", "");
        this.Config.ExcludedExtensions = QueryHelper.GetString("excluded_extensions", "");
        this.Config.ExcludedFolders = QueryHelper.GetString("excluded_folders", "");

        // Get path properties
        this.Config.SelectedPath = QueryHelper.GetString("selected_path", "");
        if (this.Config.SelectedPath.StartsWith("~"))
        {
            this.Config.SelectedPath = this.Config.SelectedPath.Replace("\\","/").TrimEnd('/');
        }
        else
        {
            this.Config.SelectedPath = this.Config.SelectedPath.Replace("/", "\\").TrimEnd('\\');
        }
        this.Config.StartingPath = QueryHelper.GetString("starting_path", "~");
        if (this.Config.StartingPath.StartsWith("~"))
        {
            this.Config.StartingPath = this.Config.StartingPath.TrimEnd('/');
        }
        else
        {
            if (!this.Config.StartingPath.EndsWith(":\\"))
            {
                this.Config.StartingPath = this.Config.StartingPath.TrimEnd('\\');
            }
        }
        
        if (String.IsNullOrEmpty(this.Config.SelectedPath))
        {
            this.Config.DefaultPath = QueryHelper.GetString("default_path", "");
        }
        else
        {
            if (this.Config.SelectedPath.StartsWith(this.Config.StartingPath))
            {
                // item to be selected
                string selectedItem = this.Config.SelectedPath.Replace(this.Config.StartingPath, "");
                char slashChar = '\\';
                if (this.Config.SelectedPath.StartsWith("~"))
                {
                    slashChar = '/';
                }

                selectedItem = selectedItem.TrimStart(slashChar).TrimEnd(slashChar);
                if (selectedItem.LastIndexOf(slashChar) != -1)
                {
                    selectedItem = selectedItem.Substring(0, selectedItem.LastIndexOf(slashChar));
                }
                this.Config.DefaultPath = selectedItem;
            }
            else
            {
                this.Config.DefaultPath = "";
            }
        }
        
        string defaultPath = this.Config.StartingPath + '\\' + this.Config.DefaultPath;
        if (this.Config.StartingPath.StartsWith("~"))
        {
            defaultPath = Server.MapPath(defaultPath);

        }
        if (!Directory.Exists(defaultPath))
        {
            this.Config.DefaultPath = "";
        }
        // Get mode
        this.Config.ShowFolders = QueryHelper.GetBoolean("show_folders", false);
    }


    /// <summary>
    /// Ensures that filter is no more applied.
    /// </summary>
    private void ResetSearchFilter()
    {
        fileSystemView.ResetSearch();
        LastSearchedValue = "";
        
    }

    #endregion
}