CREATE TABLE [CMS_DocumentTag] (
		[DocumentID]     int NOT NULL,
		[TagID]          int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_DocumentTag]
	ADD
	CONSTRAINT [PK_CMS_DocumentTag]
	PRIMARY KEY
	([DocumentID], [TagID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_DocumentTag]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Document]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_DocumentTag_DocumentID_CMS_Document]') IS NULL
BEGIN
		ALTER TABLE [CMS_DocumentTag]
			ADD CONSTRAINT [FK_CMS_DocumentTag_DocumentID_CMS_Document]
			FOREIGN KEY ([DocumentID]) REFERENCES [CMS_Document] ([DocumentID])
END
IF OBJECT_ID(N'[CMS_DocumentTag]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Tag]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_DocumentTag_TagID_CMS_Tag]') IS NULL
BEGIN
		ALTER TABLE [CMS_DocumentTag]
			ADD CONSTRAINT [FK_CMS_DocumentTag_TagID_CMS_Tag]
			FOREIGN KEY ([TagID]) REFERENCES [CMS_Tag] ([TagID])
END
