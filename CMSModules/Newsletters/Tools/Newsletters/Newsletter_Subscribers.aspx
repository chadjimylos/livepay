<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletter_Subscribers.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_Subscribers" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Newsletters - Subscribers"
    EnableEventValidation="false" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Membership/FormControls/Roles/selectrole.ascx" TagName="SelectRole"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Membership/FormControls/Users/selectuser.ascx" TagName="SelectUser"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Newsletters/FormControls/NewsletterSubscriberSelector.ascx"
    TagName="SelectSubscriber" TagPrefix="cms" %>
<asp:Content ID="contentControls" ContentPlaceHolderID="plcControls" runat="server">
    <div class="PageHeaderItem">
        <cms:SelectSubscriber runat="server" ID="selectSubscriber" />
    </div>
    <div class="PageHeaderItem">
        <cms:SelectUser runat="server" ID="selectUser" />
    </div>
    <div class="PageHeaderItem">
        <cms:SelectRole runat="server" ID="selectRole" />
    </div>
    <div class="ClearBoth">
        &nbsp;</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:CMSUpdatePanel runat="server" ID="pnlUpdate" UpdateMode="Conditional">
        <ContentTemplate>
            <cms:LocalizedLabel ID="lblInfo" runat="server" Visible="false" ResourceString="general.nodatafound"
                EnableViewState="false" />
            <cms:LocalizedLabel ID="lblError" runat="server" Visible="false" CssClass="ErrorLabel"
                EnableViewState="false" />
            <cms:UniGrid ID="UniGridSubscribers" runat="server" GridName="Newsletter_Subscribers.xml"
                OrderBy="SubscriberFullName" Columns="SubscriberID, SubscriberFullName, SubscriberEmail, Email"
                IsLiveSite="false" />
            <br />
            <asp:CheckBox ID="chkSendConfirmation" runat="server" />
            <br />
        </ContentTemplate>
    </cms:CMSUpdatePanel>
    <asp:Literal ID="ltlScript" runat="server" />
</asp:Content>
