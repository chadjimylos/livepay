﻿using System;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.WorkflowEngine;
using CMS.EventLog;

using TreeNode = CMS.TreeEngine.TreeNode;

/// <summary>
/// Control for handling global content actions
/// </summary>
public abstract class ContentActionsControl : CMSUserControl
{
    #region "Variables"

    private CurrentUserInfo mCurrentUser = null;
    private TreeProvider mTreeProvider = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Tree provider
    /// </summary>
    public TreeProvider TreeProvider
    {
        get
        {
            return mTreeProvider ?? (mTreeProvider = new TreeProvider(CurrentUser));
        }
    }


    /// <summary>
    /// Current user
    /// </summary>
    public CurrentUserInfo CurrentUser
    {
        get
        {
            return mCurrentUser ?? (mCurrentUser = CMSContext.CurrentUser);
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Processes the given action
    /// </summary>
    /// <param name="node">Node to process</param>
    /// <param name="targetNode">Target node</param>
    /// <param name="action">Action to process</param>
    /// <param name="childNodes">Process also child nodes</param>
    /// <param name="throwExceptions">If true, the exceptions are thrown</param>
    protected TreeNode ProcessAction(TreeNode node, TreeNode targetNode, string action, bool childNodes, bool throwExceptions)
    {
        int nodeId = node.NodeID;
        int targetId = targetNode.NodeID;

        // Perform the action
        switch (action.ToLower())
        {
            case "linknodeposition":
                {
                    try
                    {
                        // Set the parent ID
                        int newTargetId = targetId;
                        int newPosition = 0;

                        if (targetNode.NodeClassName.ToLower() != "cms.root")
                        {
                            // Init the node orders in parent
                            TreeProvider.InitNodeOrders(targetNode.NodeParentID);
                            targetNode = TreeProvider.SelectSingleNode(targetId);

                            // Get the target order and real parent ID
                            newTargetId = targetNode.NodeParentID;
                            newPosition = targetNode.NodeOrder + 1;

                            targetNode = TreeProvider.SelectSingleNode(newTargetId);
                        }

                        // Link the node
                        TreeNode newNode = LinkNode(node, targetNode, TreeProvider);
                        if (newNode != null)
                        {
                            nodeId = newNode.NodeID;

                            // Reposition the node
                            if (newPosition == 0)
                            {
                                // First position
                                int newOrder = TreeProvider.SetNodeOrder(nodeId, DocumentOrderEnum.First);
                                newNode.SetValue("NodeOrder", newOrder);
                            }
                            else
                            {
                                targetNode = TreeProvider.SelectSingleNode(targetId);
                                newPosition = targetNode.NodeOrder + 1;

                                // After the target
                                TreeProvider.SetNodeOrder(nodeId, newPosition, false);
                                newNode.SetValue("NodeOrder", newPosition);
                            }
                        }

                        return newNode;
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider.LogException("Content", "LINKDOC", ex);

                        AddError(ResHelper.GetString("ContentRequest.LinkFailed"));
                        if (throwExceptions)
                        {
                            throw;
                        }
                        return null;
                    }
                }

            case "linknode":
                {
                    try
                    {
                        // Link the node
                        TreeNode newNode = LinkNode(node, targetNode, TreeProvider);
                        if (newNode != null)
                        {
                            // Set default position
                            int newOrder = TreeProvider.SetNodeOrder(newNode.NodeID, DocumentOrderEnum.First);
                            newNode.SetValue("NodeOrder", newOrder);

                        }
                        return newNode;
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider.LogException("Content", "LINKDOC", ex);

                        AddError(ResHelper.GetString("ContentRequest.LinkFailed"));
                        if (throwExceptions)
                        {
                            throw;
                        }
                        return null;
                    }
                }

            case "copynodeposition":
                {
                    try
                    {
                        // Set the parent ID
                        int newTargetId = targetId;
                        int newPosition = 0;

                        if (targetNode.NodeClassName.ToLower() != "cms.root")
                        {
                            // Init the node orders in parent
                            TreeProvider.InitNodeOrders(targetNode.NodeParentID);
                            targetNode = TreeProvider.SelectSingleNode(targetId);

                            // Get the target order and real parent ID
                            newTargetId = targetNode.NodeParentID;
                            newPosition = targetNode.NodeOrder + 1;

                            targetNode = TreeProvider.SelectSingleNode(newTargetId);
                        }

                        // Copy the node
                        TreeNode newNode = CopyNode(node, targetNode, childNodes, TreeProvider);
                        if (newNode != null)
                        {
                            nodeId = newNode.NodeID;

                            // Reposition the node
                            if (newPosition == 0)
                            {
                                // First position
                                int newOrder = TreeProvider.SetNodeOrder(nodeId, DocumentOrderEnum.First);
                                newNode.SetValue("NodeOrder", newOrder);
                            }
                            else
                            {
                                targetNode = TreeProvider.SelectSingleNode(targetId);
                                newPosition = targetNode.NodeOrder + 1;

                                // After the target
                                TreeProvider.SetNodeOrder(nodeId, newPosition, false);
                                newNode.SetValue("NodeOrder", newPosition);
                            }

                            return newNode;
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider.LogException("Content", "COPYDOC", ex);

                        AddError(ResHelper.GetString("ContentRequest.CopyFailed"));
                        if (throwExceptions)
                        {
                            throw;
                        }
                        return null;
                    }
                }
                break;

            case "copynode":
                {
                    try
                    {
                        // Copy the node
                        TreeNode newNode = CopyNode(node, targetNode, childNodes, TreeProvider);
                        if (newNode != null)
                        {
                            // Set default position
                            int newOrder = TreeProvider.SetNodeOrder(newNode.NodeID, DocumentOrderEnum.First);
                            newNode.SetValue("NodeOrder", newOrder);

                        }
                        return newNode;
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider.LogException("Content", "COPYDOC", ex);

                        AddError(ResHelper.GetString("ContentRequest.CopyFailed"));
                        if (throwExceptions)
                        {
                            throw;
                        }
                        return null;
                    }
                }

            case "movenodeposition":
                {
                    try
                    {
                        // Check the permissions for document
                        if (CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Allowed)
                        {
                            // Set the parent ID
                            int newTargetId = targetId;
                            int newPosition = 0;

                            if (targetNode.NodeClassName.ToLower() != "cms.root")
                            {
                                // Init the node orders in parent
                                TreeProvider.InitNodeOrders(targetNode.NodeParentID);
                                targetNode = TreeProvider.SelectSingleNode(targetId);

                                // Get the target order and real parent ID
                                newTargetId = targetNode.NodeParentID;
                                newPosition = targetNode.NodeOrder + 1;

                                targetNode = TreeProvider.SelectSingleNode(newTargetId);
                            }

                            //Move the node under the right parent
                            if (MoveNode(node, targetNode, TreeProvider))
                            {
                                if (targetNode.NodeID == nodeId)
                                {
                                    return null;
                                }

                                // Reposition the node
                                if (newPosition == 0)
                                {
                                    // First position
                                    int newOrder = TreeProvider.SetNodeOrder(nodeId, DocumentOrderEnum.First);
                                    node.SetValue("NodeOrder", newOrder);
                                }
                                else
                                {
                                    // After the target
                                    TreeProvider.SetNodeOrder(nodeId, newPosition, false);
                                    node.SetValue("NodeOrder", newPosition);
                                }

                                return node;
                            }
                        }
                        else
                        {
                            string encodedAliasPath = " (" + HTMLHelper.HTMLEncode(node.NodeAliasPath) + ")";
                            AddError(ResHelper.GetString("ContentRequest.NotAllowedToMove") + encodedAliasPath);
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider.LogException("Content", "MOVEDOC", ex);

                        AddError(ResHelper.GetString("ContentRequest.MoveFailed"));
                        if (throwExceptions)
                        {
                            throw;
                        }
                        return null;
                    }
                }
                break;

            case "movenode":
                {
                    try
                    {
                        // Move the node
                        if (MoveNode(node, targetNode, TreeProvider))
                        {
                            // Set default position
                            int newOrder = TreeProvider.SetNodeOrder(nodeId, DocumentOrderEnum.First);
                            node.SetValue("NodeOrder", newOrder);

                            return node;
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider.LogException("Content", "MOVEDOC", ex);

                        AddError(ResHelper.GetString("ContentRequest.MoveFailed"));
                        if (throwExceptions)
                        {
                            throw;
                        }
                        return null;
                    }
                }
                break;
        }

        return null;
    }



    /// <summary>
    /// Links the node to the specified target
    /// </summary>
    /// <param name="node">Node to move</param>
    /// <param name="targetNode">Target node</param>
    /// <param name="tree">Tree provider</param>
    protected TreeNode LinkNode(TreeNode node, TreeNode targetNode, TreeProvider tree)
    {
        string encodedAliasPath = " (" + HTMLHelper.HTMLEncode(node.NodeAliasPath) + ")";
        int targetId = targetNode.NodeID;

        // Check create permission
        if (!IsUserAuthorizedToCopy(node, targetId, node.NodeClassName))
        {
            AddError(ResHelper.GetString("ContentRequest.NotAllowedToCopy") + encodedAliasPath);
            return null;
        }

        // Check allowed child class
        int targetClassId = ValidationHelper.GetInteger(targetNode.GetValue("NodeClassID"), 0);
        int nodeClassId = ValidationHelper.GetInteger(node.GetValue("NodeClassID"), 0);
        if (!DataClassInfoProvider.IsChildClassAllowed(targetClassId, nodeClassId))
        {
            AddError(ResHelper.GetString("ContentRequest.ErrorChildClassNotAllowed"));
            return null;
        }

        // Copy the document
        DocumentHelper.InsertDocumentAsLink(node, targetId, tree);
        SetExpandedNode(node.NodeParentID);

        return node;
    }


    /// <summary>
    /// Moves the node to the specified target
    /// </summary>
    /// <param name="node">Node to move</param>
    /// <param name="targetNode">Target node</param>
    /// <param name="tree">Tree provider</param>
    /// <param name="childNodes">Copy also child nodes</param>
    protected TreeNode CopyNode(TreeNode node, TreeNode targetNode, bool childNodes, TreeProvider tree)
    {
        string encodedAliasPath = " (" + HTMLHelper.HTMLEncode(node.NodeAliasPath) + ")";
        int targetId = targetNode.NodeID;

        // Do not copy child nodes in case of no child nodes
        if (node.NodeChildNodesCount == 0)
        {
            childNodes = false;
        }

        // Get the document to copy
        int nodeId = node.NodeID;
        if ((nodeId == targetId) && childNodes)
        {
            AddError(ResHelper.GetString("ContentRequest.CannotCopyToItself") + encodedAliasPath);
            return null;
        }

        // Check move permission
        if (!IsUserAuthorizedToCopy(node, targetId, node.NodeClassName))
        {
            AddError(ResHelper.GetString("ContentRequest.NotAllowedToCopy") + encodedAliasPath);
            return null;
        }

        // Check cyclic copying (copying of the node to some of its child nodes)
        if (childNodes && (targetNode.NodeSiteID == node.NodeSiteID) && targetNode.NodeAliasPath.StartsWith(node.NodeAliasPath + "/", StringComparison.CurrentCultureIgnoreCase))
        {
            AddError(ResHelper.GetString("ContentRequest.CannotCopyToChild"));
            return null;
        }

        // Check allowed child class
        int targetClassId = ValidationHelper.GetInteger(targetNode.GetValue("NodeClassID"), 0);
        int nodeClassId = ValidationHelper.GetInteger(node.GetValue("NodeClassID"), 0);
        if (!DataClassInfoProvider.IsChildClassAllowed(targetClassId, nodeClassId))
        {
            AddError(ResHelper.GetString("ContentRequest.ErrorChildClassNotAllowed"));
            return null;
        }

        // Copy the document
        node = DocumentHelper.CopyDocument(node, targetId, childNodes, tree);
        SetExpandedNode(node.NodeParentID);

        return node;
    }


    /// <summary>
    /// Moves the node to the specified target
    /// </summary>
    /// <param name="node">Node to move</param>
    /// <param name="targetNode">Target node</param>
    /// <param name="tree">Tree provider</param>
    protected bool MoveNode(TreeNode node, TreeNode targetNode, TreeProvider tree)
    {
        int targetId = targetNode.NodeID;
        string encodedAliasPath = " (" + HTMLHelper.HTMLEncode(node.NodeAliasPath) + ")";

        // If node parent ID is already the target ID, do not move it
        if (node.NodeParentID == targetId)
        {
            return true;
        }

        // Get the document to copy
        int nodeId = node.NodeID;
        if (nodeId == targetId)
        {
            AddError(ResHelper.GetString("ContentRequest.CannotMoveToItself") + encodedAliasPath);
            return false;
        }

        // Check move permission
        if (!IsUserAuthorizedToMove(node, targetId, node.NodeClassName))
        {
            AddError(ResHelper.GetString("ContentRequest.NotAllowedToMove") + encodedAliasPath);
            return false;
        }

        // Check cyclic movement (movement of the node to some of its child nodes)
        if ((targetNode.NodeSiteID == node.NodeSiteID) && targetNode.NodeAliasPath.StartsWith(node.NodeAliasPath + "/", StringComparison.CurrentCultureIgnoreCase))
        {
            AddError(ResHelper.GetString("ContentRequest.CannotMoveToChild"));
            return false;
        }

        // Check allowed child classes
        int targetClassId = ValidationHelper.GetInteger(targetNode.GetValue("NodeClassID"), 0);
        int nodeClassId = ValidationHelper.GetInteger(node.GetValue("NodeClassID"), 0);
        if (!DataClassInfoProvider.IsChildClassAllowed(targetClassId, nodeClassId))
        {
            AddError(ResHelper.GetString("ContentRequest.ErrorChildClassNotAllowed"));
            return false;
        }

        // Move the document
        DocumentHelper.MoveDocument(node, targetId, tree);
        SetExpandedNode(node.NodeParentID);

        return true;
    }


    /// <summary>
    /// Adds the alert error message to the response
    /// </summary>
    /// <param name="message">Message</param>
    protected virtual void AddError(string message)
    {
        throw new NotImplementedException();
    }


    /// <summary>
    /// Sets the expanded node ID
    /// </summary>
    /// <param name="nodeId">Node ID to set</param>
    protected virtual void SetExpandedNode(int nodeId)
    {
    }


    /// <summary>
    /// Determines whether current user is authorized to move document.
    /// </summary>
    /// <param name="sourceNode">Source node.</param>
    /// <param name="targetNodeId">Target node class name.</param>
    /// <param name="sourceNodeClassName">Source node class name.</param>
    /// <returns>True if authorized</returns>
    private bool IsUserAuthorizedToMove(TreeNode sourceNode, int targetNodeId, string sourceNodeClassName)
    {
        bool isAuthorized = false;

        // Check 'modify permission' to source document
        if (CurrentUser.IsAuthorizedPerDocument(sourceNode, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Allowed)
        {
            // Check 'create permission'
            if (CurrentUser.IsAuthorizedToCreateNewDocument(targetNodeId, sourceNodeClassName))
            {
                isAuthorized = true;
            }
        }

        return isAuthorized;
    }


    /// <summary>
    /// Determines whether current user is authorized to copy document.
    /// </summary>
    /// <param name="sourceNode">Source node.</param>
    /// <param name="targetNodeId">Target node class name.</param>
    /// <param name="sourceNodeClassName">Source node class name.</param>
    /// <returns>True if authorized</returns>
    private bool IsUserAuthorizedToCopy(TreeNode sourceNode, int targetNodeId, string sourceNodeClassName)
    {
        bool isAuthorized = false;

        // Check 'read permission' to source document
        if (CurrentUser.IsAuthorizedPerDocument(sourceNode, NodePermissionsEnum.Read) == AuthorizationResultEnum.Allowed)
        {
            // Check 'create permission'
            if (CurrentUser.IsAuthorizedToCreateNewDocument(targetNodeId, sourceNodeClassName))
            {
                isAuthorized = true;
            }
        }

        return isAuthorized;
    }

    #endregion
}
