﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.Controls

Partial Class CMSTemplates_AegeanPower_UCReferenceOfCountsOfAdmin
    Inherits CMSUserControl

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If

        If Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub


    Public Overloads Sub ReloadData()
        SetDatePickers()
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
        SetDatePickers()
    End Sub

    Private Sub SetDatePickers()
        Dim DateNamesSmall As String = ResHelper.LocalizeString("{$='Κυ', 'Δε', 'Τρ', 'Τε', 'Πε', 'Πα', 'Σα'|en-us='Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'$}")
        Dim DateNames As String = ResHelper.LocalizeString("{$='Κυριακή', 'Δευτέρα', 'Τρίτη', 'Τετάρτη', 'Πέμπτη', 'Παρασκευή', 'Σάββατο'|en-us='Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'$}")
        Dim MonthNames As String = ResHelper.LocalizeString("{$='Ιανουάριος','Φεβρουάριος','Μάρτιος','Απρίλιος','Μάιος','Ιούνιος','Ιούλιος','Αύγουστος','Σεπτέμβριος','Οκτώβριος','Νοέμβριος','Δεκέμβριος'|en-us='Januar','Februar','Marts','April','Maj','Juni','Juli','August','September','Oktober','November','December'$}")
        JsScript(String.Concat(" $(""[id$=txtboxFrom]"").datepicker({ monthNames: [", MonthNames, "], dayNames: [", DateNames, "], dayNamesMin: [", DateNamesSmall, "], dateFormat: 'dd/mm/yy' }); $(""[id$=txtboxTo]"").datepicker({ monthNames: [", MonthNames, "], dayNames: [", DateNames, "], dayNamesMin: [", DateNamesSmall, "],dateFormat: 'dd/mm/yy' });  "))
    End Sub

    Private Sub SetAttributes()

    End Sub
    Private Sub SetUserProviders()

    End Sub

    Private Sub Bind()
        Dim ProviderNo As String = txtboxProviderNumber.Text
        Dim fromDtm As String = txtboxFrom.Text
        Dim toDtm As String = txtboxTo.Text
        ExcelExport.Visible = False

        If fromDtm = String.Empty Then
            fromDtm = String.Empty
        ElseIf IsDate(fromDtm) = False Then
            fromDtm = String.Empty
            JsScript(" alert('Συμπλήρωσε σωστά την ημερομηνία Από') ;")
        Else
            fromDtm = String.Concat(Year(fromDtm), Month(fromDtm).ToString.PadLeft(2, "0"c), Day(fromDtm).ToString.PadLeft(2, "0"c))
        End If

        If toDtm = String.Empty Then
            toDtm = String.Empty
        ElseIf IsDate(toDtm) = False Then
            toDtm = String.Empty
            JsScript(" alert('Συμπλήρωσε σωστά την ημερομηνία εώς') ;")
        Else
            toDtm = String.Concat(Year(toDtm), Month(toDtm).ToString.PadLeft(2, "0"c), Day(toDtm).ToString.PadLeft(2, "0"c))
        End If

        Dim dt As DataTable = AegeanPower_DBConnection.GetReferenceOfCounts(ProviderNo, txtboxCustomer.Text, fromDtm, toDtm)
        Session("DataReferenceOfCounts") = dt
        ReferenceOfCounts.DataSource = dt
        ReferenceOfCounts.DataBind()
        AnswerBanner.Visible = True

        If dt.Rows.Count > 0 Then
            ExcelExport.Visible = True
        End If

    End Sub

    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Bind()
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

    Protected Sub gridView1_PageIndexChanging(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles ReferenceOfCounts.PageIndexChanging
        ReferenceOfCounts.DataSource = Session("DataReferenceOfCounts")
        ReferenceOfCounts.PageIndex = e.NewPageIndex
        ReferenceOfCounts.DataBind()
    End Sub


    Protected Sub TaskGridView_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs)

        'Retrieve the table from the session object.
        Dim dt As DataTable = TryCast(Session("DataReferenceOfCounts"), DataTable)

        If dt IsNot Nothing Then

            'Sort the data.
            dt.DefaultView.Sort = e.SortExpression & " " & GetSortDirection(e.SortExpression)
            ReferenceOfCounts.DataSource = Session("DataReferenceOfCounts")
            ReferenceOfCounts.DataBind()

        End If

    End Sub


    Private Function GetSortDirection(ByVal column As String) As String

        ' By default, set the sort direction to ascending.
        Dim sortDirection As String = "ASC"

        ' Retrieve the last column that was sorted.
        Dim sortExpression As String = TryCast(ViewState("SortExpression"), String)

        If sortExpression IsNot Nothing Then
            ' Check if the same column is being sorted.
            ' Otherwise, the default value can be returned.
            If sortExpression = column Then
                Dim lastDirection As String = TryCast(ViewState("SortDirection"), String)
                If lastDirection IsNot Nothing _
                  AndAlso lastDirection = "ASC" Then

                    sortDirection = "DESC"

                End If
            End If
        End If

        ' Save new values in ViewState.
        ViewState("SortDirection") = sortDirection
        ViewState("SortExpression") = column

        Return sortDirection

    End Function

End Class
