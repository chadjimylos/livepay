﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_Widgets_UI_WidgetCategory_Edit : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        categoryEdit.ItemID = QueryHelper.GetInteger("categoryid", 0);
        categoryEdit.ParentCategoryID = QueryHelper.GetInteger("parentid", 0);

        string pageTitleText;
        string pageTitleImage;
        string categoryName = String.Empty;

        // Edit cateogry
        if (categoryEdit.ItemID > 0)
        {
            pageTitleText = ResHelper.GetString("widgets.categoryproperties");
            pageTitleImage = GetImageUrl("Objects/CMS_WidgetCategory/object.png");

            WidgetCategoryInfo wci = WidgetCategoryInfoProvider.GetWidgetCategoryInfo(categoryEdit.ItemID);
            if (wci != null)
            {
                categoryName = wci.WidgetCategoryDisplayName;

                // Set already loaded object to inner control
                categoryEdit.CategoryInfo = wci;
            }
        }
        // New category
        else
        {
            pageTitleText = ResHelper.GetString("widgets.newcategory");
            pageTitleImage = GetImageUrl("Objects/CMS_WidgetCategory/new.png");
            categoryName = pageTitleText;
        }


        string[,] pageTitleTabs = new string[2, 3];

        pageTitleTabs[0, 0] = ResHelper.GetString("widgets.categories");
        pageTitleTabs[0, 1] = "~/CMSModules/Widgets/UI/WidgetFrameset.aspx";
        pageTitleTabs[0, 2] = "_parent";
        pageTitleTabs[1, 0] = categoryName;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        // Set masterpage
        this.CurrentMaster.Title.TitleText = pageTitleText;
        this.CurrentMaster.Title.TitleImage = pageTitleImage;
        this.CurrentMaster.Title.HelpTopicName = "widgets_category";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;

        categoryEdit.OnSaved += new EventHandler(categoryEdit_OnSaved);
    }


    protected void categoryEdit_OnSaved(object sender, EventArgs e)
    {
        if (categoryEdit.CategoryInfo != null)
        {            
            UrlHelper.Redirect("WidgetCategory_Edit.aspx?categoryid=" + categoryEdit.CategoryInfo.WidgetCategoryID + "&parentid=" + categoryEdit.CategoryInfo.WidgetCategoryParentID + "&saved=1");
        }
    }
}
