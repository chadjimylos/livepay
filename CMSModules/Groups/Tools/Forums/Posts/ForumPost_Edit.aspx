<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ForumPost_Edit.aspx.cs" Inherits="CMSModules_Groups_Tools_Forums_Posts_ForumPost_Edit"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" ValidateRequest="false" %>
<%@ Register Src="~/CMSModules/Forums/Controls/Posts/PostEdit.ascx" TagName="PostEdit" TagPrefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:PostEdit ID="postEdit" runat="server" />
    <asp:Literal ID="ltlScript" runat="server" />
</asp:Content>