<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Permissions_Header.aspx.cs"
    Inherits="CMSSiteManager_Administration_Permissions_Permissions_Header" Theme="default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/SelectModule.ascx" TagName="ModuleSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Classes/SelectClass.ascx" TagName="ClassSelector" TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <cms:CMSUpdatePanel runat="server" ID="pnlUpdate" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="PanelOptions" runat="server" CssClass="PageHeaderLine">
                <table>
                    <tr>
                        <td class="FieldLabel">
                            <cms:LocalizedLabel ID="lblSite" runat="server" Text="Label" ResourceString="general.site"
                                DisplayColon="true" EnableViewState="false" />
                        </td>
                        <td>
                            <asp:Panel ID="PanelSites" runat="server">
                                <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" />
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPermissionType" runat="server" EnableViewState="false" />
                        </td>
                        <td>
                            <asp:DropDownList ID="drpPermissionType" runat="server" OnSelectedIndexChanged="drpPermissionType_SelectedIndexChanged"
                                AutoPostBack="True" CssClass="DropDownField" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            <asp:Label ID="LabelPermissionMatrix" runat="server" EnableViewState="false" />
                        </td>
                        <td>
                            <cms:ModuleSelector runat="server" ID="moduleSelector" IsLiveSite="false" DisplayOnlyWithPermission="true" />
                            <cms:ClassSelector runat="server" ID="docTypeSelector" IsLiveSite="false" OnlyDocumentTypes="true" />
                            <cms:ClassSelector runat="server" ID="customTableSelector" IsLiveSite="false" OnlyCustomTables="true" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </cms:CMSUpdatePanel>
</asp:Content>
