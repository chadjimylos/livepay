using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.Ecommerce;
using CMS.EcommerceProvider;
using CMS.GlobalHelper;

public partial class CMSModules_Ecommerce_Controls_ShoppingCart_ShoppingCartPaymentShipping : ShoppingCartStep
{
    #region "ViewState Constants"

    private const string SHIPPING_OPTION_ID = "OrderShippingOptionID";
    private const string PAYMENT_OPTION_ID = "OrderPaymenOptionID";

    #endregion


    /// <summary>
    /// On page load.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Event arguments.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        lblTitle.Text = ResHelper.GetString("ShoppingCart.ShippingPaymentOptions");

        if ((ShoppingCartInfoObj != null) && (ShoppingCartInfoObj.CountryID == 0) && (CMSContext.CurrentSite != null))
        {
            ShoppingCartInfoObj.CountryID = CMSContext.CurrentSite.SiteDefaultCountryID;
        }
        // Initialize labels
        lblPayment.Text = ResHelper.GetString("ShoppingCartPaymentShipping.Payment");
        lblShipping.Text = ResHelper.GetString("ShoppingCartPaymentShipping.Shipping");

        drpShipping.SelectedIndexChanged += new EventHandler(drpShipping_SelectedIndexChanged);


        if (!ShoppingCartControl.IsCurrentStepPostBack)
        {
            if (CMSContext.CurrentSite != null)
            {
                int siteId = CMSContext.CurrentSite.SiteID;

                // Initialize shipping                
                InitializeShippingList(siteId);
                SelectShippingOption();

                // Initialize payment 
                InitializePaymentList(siteId);
                SelectPaymentOption();
            }
        }
    }


    /// <summary>
    /// Load related payment options
    /// </summary>
    void drpShipping_SelectedIndexChanged(object sender, EventArgs e)
    {
        InitializePaymentList(0);
    }


    /// <summary>
    /// Initialize payment option dropdown list.
    /// </summary>
    /// <param name="siteId">Site ID.</param>
    protected void InitializePaymentList(int siteId)
    {
        int shippingId = ValidationHelper.GetInteger(drpShipping.SelectedValue, 0);

        drpPayment.DataSource = shippingId > 0 ? PaymentOptionInfoProvider.GetShippingPayments(shippingId, true, false) : PaymentOptionInfoProvider.GetAllPaymentsForSite(siteId, StatusEnum.Enabled);

        drpPayment.DataTextField = "PaymentOptionDisplayName";
        drpPayment.DataValueField = "PaymentOptionID";
        drpPayment.DataBind();

        // If empty -> hide payment
        plcPayment.Visible = !DataHelper.DataSourceIsEmpty(drpPayment.DataSource);
    }


    /// <summary>
    /// Initialize shipping option dropdown list.
    /// </summary>
    /// <param name="siteId">SiteID.</param>
    protected void InitializeShippingList(int siteId)
    {
        if ((ShoppingCartInfoObj != null) && (CMSContext.CurrentSite != null))
        {

            // Ensure free shipping info
            double siteStoreFreeLimit = CMSContext.CurrentSite.SiteStoreShippingFreeLimit;
            if ((siteStoreFreeLimit >= 0) && (ShoppingCartInfoObj.TotalPrice >= siteStoreFreeLimit))
            {
                this.lblFreeShippingInfo.Visible = true;
            }

            ListItem li = null;
            DataSet ds = ShippingOptionInfoProvider.GetShippingOptions(ShoppingCartInfoObj, StatusEnum.Enabled);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                // for each row in shipping options
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    li = new ListItem();

                    // Apply free shipping limit
                    double shipping = ShippingOptionInfoProvider.ApplyShippingFreeLimit(
                                               ValidationHelper.GetDouble(dr["ShippingOptionCharge"], 0) / ShoppingCartInfoObj.ExchangeRate,
                                               ShoppingCartInfoObj.TotalPrice, siteStoreFreeLimit);

                    // Get ShippingOptionCharge, apply free shipping, format the price into corrent format
                    string detailInfo = "";
                    if (shipping > 0)
                    {
                        detailInfo = "(" + CurrencyInfoProvider.GetFormatedPrice(shipping, ShoppingCartInfoObj.CurrencyInfoObj) + ")";
                    }

                    if (CultureHelper.IsUICultureRTL())
                    {
                        li.Text += (detailInfo == "" ? "" : detailInfo + " ") + Convert.ToString(dr["ShippingOptionDisplayName"]);
                    }
                    else
                    {
                        li.Text = Convert.ToString(dr["ShippingOptionDisplayName"]) + (detailInfo == "" ? "" : " " + detailInfo);
                    }
                    li.Value = Convert.ToString(dr["ShippingOptionID"]);
                    drpShipping.Items.Add(li);
                }
            }
        }
        // If empty -> hide shipping
        plcShipping.Visible = drpShipping.Items.Count > 0;
    }


    /// <summary>
    /// Back button actions
    /// </summary>
    public override void ButtonBackClickAction()
    {
        // Save the values to ShoppingCart ViewState
        this.ShoppingCartControl.SetTempValue(SHIPPING_OPTION_ID, this.drpShipping.SelectedValue);
        this.ShoppingCartControl.SetTempValue(PAYMENT_OPTION_ID, this.drpPayment.SelectedValue);

        base.ButtonBackClickAction();
    }


    public override bool ProcessStep()
    {
        try
        {
            // Cleanup the ShoppingCart ViewState
            this.ShoppingCartControl.SetTempValue(SHIPPING_OPTION_ID, null);
            this.ShoppingCartControl.SetTempValue(PAYMENT_OPTION_ID, null);

            ShoppingCartInfoObj.ShoppingCartShippingOptionID = ValidationHelper.GetInteger(drpShipping.SelectedValue, 0);
            ShoppingCartInfoObj.ShoppingCartPaymentOptionID = ValidationHelper.GetInteger(drpPayment.SelectedValue, 0);

            // Update changes in database only when on the live site
            if (!ShoppingCartControl.IsInternalOrder)
            {
                ShoppingCartInfoProvider.SetShoppingCartInfo(ShoppingCartInfoObj);
            }
            return true;
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
            return false;
        }
    }


    /// <summary>
    /// Select saved payment option.
    /// </summary>
    protected void SelectPaymentOption()
    {
        try
        {
            // Try to select payment from ViewState first
            object viewStateValue = this.ShoppingCartControl.GetTempValue(PAYMENT_OPTION_ID);
            if (viewStateValue != null)
            {
                drpPayment.SelectedValue = Convert.ToString(viewStateValue);
            }
            // try to select payment option accroding to saved option in shopping cart object
            else if (ShoppingCartInfoObj.ShoppingCartPaymentOptionID > 0)
            {
                drpPayment.SelectedValue = Convert.ToString(ShoppingCartInfoObj.ShoppingCartPaymentOptionID);
            }
            // try to select payment option according to user preffered option
            else
            {
                if (ShoppingCartInfoObj.CustomerInfoObj != null)
                {
                    drpPayment.SelectedValue = Convert.ToString(ShoppingCartInfoObj.CustomerInfoObj.CustomerPrefferedPaymentOptionID);
                }
            }
        }
        catch
        {
        }
    }


    /// <summary>
    /// Select saved shipping option.
    /// </summary>
    protected void SelectShippingOption()
    {
        try
        {
            // Try to select shipping from ViewState first
            object viewStateValue = this.ShoppingCartControl.GetTempValue(SHIPPING_OPTION_ID);
            if (viewStateValue != null)
            {
                drpShipping.SelectedValue = Convert.ToString(viewStateValue);
            }
            // try to select shipping option accroding to saved option in shopping cart object
            else if (ShoppingCartInfoObj.ShoppingCartShippingOptionID > 0)
            {
                drpShipping.SelectedValue = Convert.ToString(ShoppingCartInfoObj.ShoppingCartShippingOptionID);
            }
            // try to select shipping option according to user preffered option
            else
            {
                if (ShoppingCartInfoObj.CustomerInfoObj != null)
                {
                    drpShipping.SelectedValue = Convert.ToString(ShoppingCartInfoObj.CustomerInfoObj.CustomerPreferredShippingOptionID);
                }
            }
        }
        catch
        {
        }
    }


    public override bool IsValid()
    {
        string errorMessage = "";

        // If shipping is required
        if (plcShipping.Visible)
        {
            if (ValidationHelper.GetInteger(drpShipping.SelectedValue, 0) <= 0)
            {
                errorMessage = ResHelper.GetString("Order_New.NoShippingOption");
            }
        }

        // If payment is required
        if (plcPayment.Visible)
        {
            if ((errorMessage == "") && (ValidationHelper.GetInteger(drpPayment.SelectedValue, 0) <= 0))
            {
                errorMessage = ResHelper.GetString("Order_New.NoPaymentMethod");
            }
        }


        if (errorMessage == "")
        {
            // Form is valid
            return true;
        }
        // Form is not valid
        lblError.Visible = true;
        lblError.Text = errorMessage;
        return false;
    }
}
