<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SystemTable_Edit_Fields.aspx.cs"
    Inherits="CMSSiteManager_Development_SystemTables_SystemTable_Edit_Fields" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="System table edit" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/FieldEditor.ascx" TagName="FieldEditor"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" EnableViewState="false" />
    <cms:FieldEditor ID="FieldEditor" runat="server" />
</asp:Content>
