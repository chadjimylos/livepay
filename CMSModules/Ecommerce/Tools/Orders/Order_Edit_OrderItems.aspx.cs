using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.Ecommerce;
using CMS.EcommerceProvider;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_OrderItems : CMSEcommercePage
{
    protected int orderId = 0;
    private string mSessionKey = "CMSDeskOrderItemsShoppingCart";


    /// <summary>
    /// Shopping cart to use.
    /// </summary>
    private ShoppingCartInfo ShoppingCart
    {
        get
        {
            if (Session[mSessionKey] == null)
            {
                Session[mSessionKey] = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(orderId);
            }
            return (ShoppingCartInfo)Session[mSessionKey];
        }
        set
        {
            Session[mSessionKey] = value;
        }
    }


    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get order id
        orderId = ValidationHelper.GetInteger(Request.QueryString["OrderId"], 0);

        if (!RequestHelper.IsPostBack())
        {
            if (!ValidationHelper.GetBoolean(Request.QueryString["cartexist"], false))
            {
                this.ShoppingCart = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(orderId);
            }
        }

        if (orderId > 0)
        {
            this.Cart.LocalShoppingCart = this.ShoppingCart;
            this.Cart.EnableProductPriceDetail = true;
            this.Cart.ShoppingCartInfoObj.IsCreatedFromOrder = true;
            this.Cart.CheckoutProcessType = CheckoutProcessEnum.CMSDeskOrderItems;
            this.Cart.OnPaymentCompleted += new EventHandler(Cart_OnPaymentCompleted);
            this.Cart.OnPaymentSkipped += new EventHandler(Cart_OnPaymentSkipped);            
        }


        if (!RequestHelper.IsPostBack())
        {
            // Display 'Changes saved' message        
            if (ValidationHelper.GetBoolean(Request.QueryString["saved"], false))
            {
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
            // Display 'Payment completed' message        
            else if (ValidationHelper.GetBoolean(Request.QueryString["paymentcompleted"], false))
            {
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("PaymentGateway.CMSDeskOrderItems.PaymentCompleted");
            }
        }            
    }


    void Cart_OnPaymentCompleted(object sender, EventArgs e)
    {
        this.Cart.CleanUpShoppingCart();
        UrlHelper.Redirect("~/CMSModules/Ecommerce/Tools/Orders/Order_Edit_OrderItems.aspx?orderID=" + this.ShoppingCart.OrderId.ToString() + "&paymentcompleted=1");    
    }


    void Cart_OnPaymentSkipped(object sender, EventArgs e)
    {
        string saved = "";

        // Payment skipped because of no payment gateway was specified
        if (this.Cart.PaymentGatewayProvider != null)
        {            
            // Do nothing
        }
        // Payment skipped from shopping cart step with payment - Button 'Skip payment' was pressed
        else
        {
            // Display 'Changes saved' message after page redirect
            saved = "&saved=1";
        }

        this.Cart.CleanUpShoppingCart();
        UrlHelper.Redirect("~/CMSModules/Ecommerce/Tools/Orders/Order_Edit_OrderItems.aspx?orderID=" + this.ShoppingCart.OrderId.ToString() + saved);
    }


    /// <summary>
    /// On prerender.
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        this.ShoppingCart = this.Cart.LocalShoppingCart;
        base.OnPreRender(e);
    }
}
