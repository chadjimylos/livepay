using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Polls;
using CMS.UIControls;

public partial class CMSModules_Polls_Controls_PollProperties : CMSAdminEditControl
{
    #region "Variables"

    private int mSiteID = 0;
    private int mGroupID = 0;
    private PollInfo pollObj = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or Sets site ID of the poll
    /// </summary>
    public int SiteID
    {
        get
        {
            return mSiteID;
        }
        set
        {
            mSiteID = value;
        }
    }


    /// <summary>
    /// Gets or sets group ID
    /// </summary>
    public int GroupID
    {
        get
        {
            return this.mGroupID;
        }
        set
        {
            mGroupID = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Hide code name editing in simple mode
        if (DisplayMode == ControlDisplayModeEnum.Simple)
        {
            plcCodeName.Visible = false;
        }

        // Required field validator error messages initialization
        rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvQuestion.ErrorMessage = ResHelper.GetString("Polls_New.QuestionError");

        rfvMaxLength.ErrorMessage = ResHelper.GetString("general.errortexttoolong");
        rfvMaxLengthResponse.ErrorMessage = ResHelper.GetString("general.errortexttoolong");

        // Control initializations
        lblTitle.Text = ResHelper.GetString("Polls_Edit.PollTitleLabel");
        lblQuestion.Text = ResHelper.GetString("Polls_Edit.PollQuestionLabel");
        lblOpenFrom.Text = ResHelper.GetString("Polls_Edit.PollOpenFromLabel");
        lblOpenTo.Text = ResHelper.GetString("Polls_Edit.PollOpenToLabel");
        lblResponseMessage.Text = ResHelper.GetString("Polls_Edit.PollResponseMessageLabel");
        lblAllowMultipleAnswers.Text = ResHelper.GetString("Polls_Edit.PollAllowMultipleAnswersLabel");
        btnOk.Text = ResHelper.GetString("General.OK");
        dtPickerOpenFrom.SupportFolder = "~/CMSAdminControls/Calendar";
        dtPickerOpenTo.SupportFolder = "~/CMSAdminControls/Calendar";

        if (!RequestHelper.IsPostBack())
        {
            // Show possible license limitation error relayed from new poll page
            string error = QueryHelper.GetText("error", null);
            if (!String.IsNullOrEmpty(error))
            {
                lblError.Text = error;
                lblError.Visible = true;
            }
        }

        if (pollObj == null)
        {
            pollObj = PollInfoProvider.GetPollInfo(this.ItemID);
        }

        if (pollObj != null)
        {
            // Fill editing form
            if (!RequestHelper.IsPostBack() && !IsLiveSite)
            {
                ReloadData();
            }
        }
    }


    /// <summary>
    /// Clears data.
    /// </summary>
    public override void ClearForm()
    {
        base.ClearForm();
        txtDisplayName.Text = null;
        txtCodeName.Text = null;
        txtTitle.Text = null;
        txtQuestion.Text = null;
        dtPickerOpenFrom.DateTimeTextBox.Text = null;
        dtPickerOpenTo.DateTimeTextBox.Text = null;
        txtResponseMessage.Text = null;
        chkAllowMultipleAnswers.Checked = false;
    }


    /// <summary>
    /// Reloads control with new data.
    /// </summary>
    public override void ReloadData()
    {
        this.ClearForm();
        if (pollObj == null)
        {
            pollObj = PollInfoProvider.GetPollInfo(this.ItemID);
        }
        // Load the fields
        if (pollObj != null)
        {
            txtCodeName.Text = pollObj.PollCodeName;
            txtDisplayName.Text = pollObj.PollDisplayName;
            txtTitle.Text = pollObj.PollTitle;
            txtQuestion.Text = pollObj.PollQuestion;
            if (pollObj.PollOpenFrom != DataHelper.DATETIME_NOT_SELECTED)
            {
                dtPickerOpenFrom.SelectedDateTime = pollObj.PollOpenFrom;
            }
            if (pollObj.PollOpenTo != DataHelper.DATETIME_NOT_SELECTED)
            {
                dtPickerOpenTo.SelectedDateTime = pollObj.PollOpenTo;
            }
            txtResponseMessage.Text = pollObj.PollResponseMessage;
            chkAllowMultipleAnswers.Checked = pollObj.PollAllowMultipleAnswers;
        }
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (!CheckPermissions("cms.polls", CMSAdminControl.PERMISSION_MODIFY))
        {
            return;
        }

        // Get trimmed inputs
        string codeName = txtCodeName.Text.Trim();
        string displayName = txtDisplayName.Text.Trim();
        string question = txtQuestion.Text.Trim();

        // Validate the fields
        string errorMessage = new Validator().NotEmpty(codeName, rfvCodeName.ErrorMessage)
            .NotEmpty(displayName, rfvDisplayName.ErrorMessage).NotEmpty(question, rfvQuestion.ErrorMessage).Result;

        if (!ValidationHelper.IsCodeName(codeName))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }
        else
        {
            // From/to date validation
            if (string.IsNullOrEmpty(errorMessage) && (!ValidationHelper.IsIntervalValid(dtPickerOpenFrom.SelectedDateTime, dtPickerOpenTo.SelectedDateTime, true)))
            {
                errorMessage = ResHelper.GetString("General.DateOverlaps");
            }
        }

        if (string.IsNullOrEmpty(errorMessage))
        {
            // Check uniqeness
            PollInfo secondPoll = PollInfoProvider.GetPollInfo(codeName, this.SiteID, this.GroupID);
            if ((secondPoll == null) || (secondPoll.PollID == this.ItemID))
            {
                if (pollObj == null)
                {
                    pollObj = PollInfoProvider.GetPollInfo(this.ItemID);
                }
                if (pollObj != null)
                {
                    if (pollObj.PollSiteID != this.SiteID)
                    {
                        throw new Exception("[PollProperties.ascx]: Wrong poll object recieved since SiteID parameter wasn't provided.");
                    }

                    // Store the fields
                    pollObj.PollCodeName = codeName;
                    pollObj.PollDisplayName = displayName;
                    pollObj.PollTitle = txtTitle.Text.Trim();
                    pollObj.PollQuestion = question;
                    pollObj.PollOpenFrom = dtPickerOpenFrom.SelectedDateTime;
                    pollObj.PollOpenTo = dtPickerOpenTo.SelectedDateTime;
                    pollObj.PollResponseMessage = txtResponseMessage.Text.Trim();
                    pollObj.PollAllowMultipleAnswers = chkAllowMultipleAnswers.Checked;

                    // Save the data
                    PollInfoProvider.SetPollInfo(pollObj);

                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
                else
                {
                    // Error message - Poll does not exist
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("polls.pollnotexist");
                }
            }
            else
            {
                // Error message - Code name already exist
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("polls.codenameexists");
            }
        }
        else
        {
            // Error message - validation
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
