using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_Roles_Role_Edit_General : CMSRolesPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize the editing control
        roleEditElem.ItemID = QueryHelper.GetInteger("roleid", 0);
        int siteId = QueryHelper.GetInteger("siteid", 0);
        if (siteId == 0)
        {
            siteId = QueryHelper.GetInteger("selectedsiteid", 0);
        }
        roleEditElem.SiteID = siteId;
        roleEditElem.OnSaved += roleEditElem_OnSaved;
        roleEditElem.OnCheckPermissions += new CMSAdminControl.CheckPermissionsEventHandler(roleEditElem_OnCheckPermissions);
    }

    
    protected void roleEditElem_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Roles", permissionType))
        {
            RedirectToCMSDeskAccessDenied("CMS.Roles", permissionType);
        }
    }


    /// <summary>
    /// OnOK click event only if new group was created.
    /// </summary>
    protected void roleEditElem_OnSaved(object sender, EventArgs e)
    {
        //CMS desk site context
        string siteId = QueryHelper.GetText("siteid", string.Empty);
        if (siteId != String.Empty)
        {
            siteId = "&siteid=" + siteId;
        }

        //CMS site manager site context
        string selectedsiteId = QueryHelper.GetText("selectedsiteid", string.Empty);
        if (selectedsiteId != String.Empty)
        {
            selectedsiteId = "&selectedsiteid=" + selectedsiteId;
        }

        UrlHelper.Redirect("Role_Edit_Frameset.aspx?roleId=" + roleEditElem.ItemID + siteId + selectedsiteId);
    }
}
