using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.Caching;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.SiteProvider;

public partial class CMSWebParts_General_languageselection : CMSAbstractWebPart
{
    private string mSeparator = " ";

    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether the link for current culture should be hidden
    /// </summary>
    public bool HideCurrentCulture
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("HideCurrentCulture"), false);
        }
        set
        {
            SetValue("HideCurrentCulture", value);
        }
    }


    /// <summary>
    /// Gets or sets the display layout
    /// </summary>
    public string DisplayLayout
    {
        get
        {
            return ValidationHelper.GetString(GetValue("DisplayLayout"), "");
        }
        set
        {
            SetValue("DisplayLayout", value);
            mSeparator = value.ToLower() == "vertical" ? "<br />" : " ";
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data for partial caching
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do not process
        }
        else
        {
            GeneralConnection genconn = ConnectionHelper.GetConnection();

            string culture = CMSContext.PreferredCultureCode;

            mSeparator = DisplayLayout.ToLower() == "vertical" ? "<br />" : " ";

            DataSet ds = null;

            // Try to get data from cache
            using (CachedSection<DataSet> cs = new CachedSection<DataSet>(ref ds, this.CacheMinutes, true, this.CacheItemName, "languageselection", CMSContext.CurrentSiteName))
            {
                if (cs.LoadData)
                {
                    // Get the data
                    ds = CultureInfoProvider.GetSiteCultures(CMSContext.CurrentSiteName);

                    // Add to the cache
                    if (cs.Cached)
                    {
                        cs.CacheDependency = CacheHelper.GetCacheDependency(new string[] { "cms.culturesite|all" });
                        cs.Data = ds;
                    }
                }
            }

            if (!DataHelper.DataSourceIsEmpty(ds) && (ds.Tables[0].Rows.Count > 1))
            {
                // Build current URL
                string url = UrlHelper.CurrentURL;
                url = UrlHelper.RemoveParameterFromUrl(url, UrlHelper.LanguageParameterName);
                url = UrlHelper.RemoveParameterFromUrl(url, UrlHelper.AliasPathParameterName);

                // Render the cultures
                ltlHyperlinks.Text = "";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string cultureCode = dr["CultureCode"].ToString();
                    string cultureShortName = dr["CultureShortName"].ToString();
                    if (!(HideCurrentCulture && (String.Compare(CMSContext.CurrentDocument.DocumentCulture, cultureCode, true) == 0)))
                    {
                        if (culture.ToLower() != cultureCode.ToLower())
                        {
                            ltlHyperlinks.Text += "<a href=\"" + UrlHelper.AddParameterToUrl(url, UrlHelper.LanguageParameterName, cultureCode) + "\">" + HTMLHelper.HTMLEncode(cultureShortName) + "</a>";
                        }
                        else
                        {
                            ltlHyperlinks.Text += cultureShortName;
                        }
                        ltlHyperlinks.Text += mSeparator;
                    }
                }
            }
            else
            {
                Visible = false;
            }
        }
    }


    /// <summary>
    /// Clears the cached items
    /// </summary>
    public override void ClearCache()
    {
        string useCacheItemName = DataHelper.GetNotEmpty(CacheItemName, CMSContext.BaseCacheKey + "|" + Context.Request.Url + "|" + ClientID);

        CacheHelper.ClearCache(useCacheItemName);
    }
}
