CREATE TABLE [COM_OptionCategory] (
		[CategoryID]                 int NOT NULL IDENTITY(1, 1),
		[CategoryDisplayName]        nvarchar(200) NOT NULL,
		[CategoryName]               nvarchar(200) NOT NULL,
		[CategorySelectionType]      nvarchar(200) NOT NULL,
		[CategoryDefaultOptions]     nvarchar(200) NULL,
		[CategoryDescription]        nvarchar(max) NULL,
		[CategoryDefaultRecord]      nvarchar(200) NOT NULL,
		[CategoryEnabled]            bit NOT NULL,
		[CategoryGUID]               uniqueidentifier NOT NULL,
		[CategoryLastModified]       datetime NOT NULL,
		[CategoryDisplayPrice]       bit NULL
)
ON [PRIMARY]
ALTER TABLE [COM_OptionCategory]
	ADD
	CONSTRAINT [PK_COM_OptionCategory]
	PRIMARY KEY
	NONCLUSTERED
	([CategoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [COM_OptionCategory]
	ADD
	CONSTRAINT [DEFAULT_COM_OptionCategory_CategoryDisplayPrice]
	DEFAULT ((1)) FOR [CategoryDisplayPrice]
CREATE CLUSTERED INDEX [IX_COM_OptionCategory_CategoryDisplayName_CategoryEnabled]
	ON [COM_OptionCategory] ([CategoryDisplayName], [CategoryEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
