using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Countries_Country_New : SiteManagerPage
{    
    protected void Page_Load(object sender, EventArgs e)
    {
       
        // control initializations
        lblCountryDisplayName.Text = ResHelper.GetString("Country_Edit.CountryDisplayNameLabel");
        lblCountryCodeName.Text = ResHelper.GetString("Country_Edit.CountryNameLabel");
        rfvCountryDisplayName.ErrorMessage = ResHelper.GetString("Country_Edit_General.ErrorEmptyCountryDisplayName");
        rfvCountryCodeName.ErrorMessage = ResHelper.GetString("Country_Edit_General.ErrorEmptyCountryCodeName");
        btnOk.Text = ResHelper.GetString("General.OK");        

        // initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Country_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/Countries/Country_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = ResHelper.GetString("Country_Edit.NewItemCaption");
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Country_New.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Country/new.png");
        this.CurrentMaster.Title.HelpTopicName = "new_country";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // finds whether required fields are not empty
        string errorMessage = new Validator()
            .NotEmpty(txtCountryDisplayName.Text, rfvCountryDisplayName.ErrorMessage)
            .NotEmpty(txtCountryCodeName.Text, rfvCountryCodeName.ErrorMessage)
            .IsCodeName(txtCountryCodeName.Text, ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat")).Result;

        if (errorMessage == "")
        {
            // countryName must be unique
            CountryInfo testObj = CountryInfoProvider.GetCountryInfo(txtCountryCodeName.Text.Trim());

            // if countryName value is unique														
            if (testObj == null)
            {
                CountryInfo ci = new CountryInfo();
                ci.CountryDisplayName = txtCountryDisplayName.Text.Trim();
                ci.CountryName = txtCountryCodeName.Text.Trim();

                CountryInfoProvider.SetCountryInfo(ci);

                UrlHelper.Redirect("Country_Edit_Frameset.aspx?countryID=" + Convert.ToString(ci.CountryID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Country_Edit.CountryNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
