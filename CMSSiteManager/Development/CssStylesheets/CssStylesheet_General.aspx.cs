using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.DirectoryUtilities;
using CMS.UIControls;
using CMS.ExtendedControls;

public partial class CMSSiteManager_Development_CssStylesheets_CssStylesheet_General : CMSModalDesignPage
{
    #region "Variables"

    protected int cssStylesheetId = 0;
    private bool onlyCodeEdit = false;
    private CMSMasterPage currentMaster = null;

    #endregion


    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Page has been opened in CMSDesk and only stylesheet style editing is allowed
        onlyCodeEdit = QueryHelper.GetBoolean("editonlycode", false);

        if (onlyCodeEdit)
        {
            this.MasterPageFile = "~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master";
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        lblCssStylesheetDisplayName.Text = ResHelper.GetString("CssStylesheet_General.DisplayName");
        lblCssStylesheetName.Text = ResHelper.GetString("CssStylesheet_General.Name");
        lblCssStylesheetText.Text = ResHelper.GetString("CssStylesheet_General.Text");

        rfvDisplayName.ErrorMessage = ResHelper.GetString("CssStylesheet_General.EmptyDisplayName");
        rfvName.ErrorMessage = ResHelper.GetString("CssStylesheet_General.EmptyName");
        rfvText.ErrorMessage = ResHelper.GetString("CssStylesheet_General.EmptyText");

        if (ValidationHelper.GetString(Request.QueryString["saved"], "") != "")
        {
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }

        cssStylesheetId = QueryHelper.GetInteger("cssstylesheetid", 0);
        CssStylesheetInfo si = CssStylesheetInfoProvider.GetCssStylesheetInfo(cssStylesheetId);
        if (si != null)
        {
            // Page has been opened in CMSDesk and only stylesheet style editing is allowed
            if (onlyCodeEdit)
            {
                // Set CSS class
                this.pnlContainer.CssClass = "PageContent";

                // Add close button
                LocalizedButton btnClose = new LocalizedButton();
                btnClose.ID = "btnClose";
                btnClose.ResourceString = "general.close";
                btnClose.EnableViewState = false;
                btnClose.OnClientClick = "window.close(); return false;";
                btnClose.CssClass = "SubmitButton";
                Panel pnlButton = new Panel();
                pnlButton.ID = "pnlButton";
                pnlButton.CssClass = "FloatRight";
                pnlButton.Controls.Add(btnClose);
                this.CurrentMaster.PanelFooter.Controls.Add(pnlButton);

                // Check if user can edit the stylesheet
                CurrentUserInfo currentUser = CMSContext.CurrentUser;
                if (!currentUser.IsAuthorizedPerUIElement("CMS.Content", new string[] { "Properties", "Properties.General", "General.Design" }, CMSContext.CurrentSiteName))
                {
                    RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties;Properties.General;General.Design");
                }

                // Get CMS master page
                currentMaster = (CMSMasterPage)this.Page.Master;

                // Set page title
                currentMaster.Title.TitleText = ResHelper.GetString("CssStylesheet.CssStylesheetProperties");
                currentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_CSSStylesheet/object.png");

                // Code editing
                txtCssStylesheetDisplayName.Enabled = false;
                txtCssStylesheetName.Enabled = false;
            }
            else
            {
                // Otherwise the user must be global admin
                CheckGlobalAdministrator();
            }

            if (!RequestHelper.IsPostBack())
            {
                txtCssStylesheetDisplayName.Text = si.StylesheetDisplayName;
                txtCssStylesheetName.Text = si.StylesheetName;
                txtCssStylesheetText.Text = si.StylesheetText;
            }

            if (si.StylesheetCheckedOutByUserID > 0)
            {
                this.txtCssStylesheetText.ReadOnly = true;
                string username = null;
                UserInfo ui = UserInfoProvider.GetUserInfo(si.StylesheetCheckedOutByUserID);
                if (ui != null)
                {
                    username = HTMLHelper.HTMLEncode(ui.FullName);
                }

                // Checked out by current machine
                if (si.StylesheetCheckedOutMachineName.ToLower() == HTTPHelper.MachineName.ToLower())
                {
                    this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("CssStylesheet.CheckedOut"), Server.MapPath(si.StylesheetCheckedOutFilename));
                }
                else
                {
                    this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("CssStylesheet.CheckedOutOnAnotherMachine"), si.StylesheetCheckedOutMachineName, username);
                }
            }
            else
            {
                this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("CssStylesheet.CheckOutInfo"), Server.MapPath(CssStylesheetInfoProvider.GetVirtualStylesheetUrl(si.StylesheetName, null)));
            }
        }

        InitializeHeaderActions(si);
    }


    /// <summary>
    /// Initializes header action control
    /// </summary>
    private void InitializeHeaderActions(CssStylesheetInfo si)
    {
        // Header actions
        string[,] actions = new string[4, 11];

        // Save button
        actions[0, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[0, 1] = ResHelper.GetString("General.Save");
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        actions[0, 6] = "save";
        actions[0, 8] = "true";

        // CheckOut
        actions[1, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[1, 1] = ResHelper.GetString("General.CheckOut");
        actions[1, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkout.png");
        actions[1, 6] = "checkout";
        actions[1, 10] = "false";

        // CheckIn
        actions[2, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[2, 1] = ResHelper.GetString("General.CheckIn");
        actions[2, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkin.png");
        actions[2, 6] = "checkin";
        actions[2, 10] = "false";

        // UndoCheckOut
        actions[3, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[3, 1] = ResHelper.GetString("General.UndoCheckOut");
        actions[3, 2] = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("General.ConfirmUndoCheckOut")) + ");";
        actions[3, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/undocheckout.png");
        actions[3, 6] = "undocheckout";
        actions[3, 10] = "false";

        if (si != null)
        {
            if (si.StylesheetCheckedOutByUserID > 0)
            {
                // Checked out by current machine
                if (si.StylesheetCheckedOutMachineName.ToLower() == HTTPHelper.MachineName.ToLower())
                {
                    actions[2, 10] = "true";
                }
                if (CMSContext.CurrentUser.IsGlobalAdministrator)
                {
                    actions[3, 10] = "true";
                }
            }
            else
            {
                actions[1, 10] = "true";
            }
        }

        this.CurrentMaster.HeaderActions.LinkCssClass = "ContentSaveLinkButton";
        this.CurrentMaster.HeaderActions.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);
        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    /// <summary>
    /// Actions handler
    /// </summary>
    protected void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "save":

                SaveStylesheet();
                break;

            case "checkout":

                if (!SaveStylesheet())
                {
                    return;
                }

                try
                {
                    SiteManagerFunctions.CheckOutStylesheet(cssStylesheetId);
                }
                catch (Exception ex)
                {
                    this.lblError.Text = ResHelper.GetString("CssStylesheet.ErrorCheckout") + ": " + ex.Message;
                    this.lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;

            case "checkin":

                try
                {
                    SiteManagerFunctions.CheckInStylesheet(cssStylesheetId);
                }
                catch (Exception ex)
                {
                    this.lblError.Text = ResHelper.GetString("CssStylesheet.ErrorCheckin") + ": " + ex.Message;
                    this.lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;

            case "undocheckout":

                try
                {
                    SiteManagerFunctions.UndoCheckOutStylesheet(cssStylesheetId);
                }
                catch (Exception ex)
                {
                    this.lblError.Text = ResHelper.GetString("CssStylesheet.ErrorUndoCheckout") + ": " + ex.Message;
                    this.lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;
        }
    }


    /// <summary>
    /// Saves the stylesheet, returns true if successful
    /// </summary>
    private bool SaveStylesheet()
    {
        string result = new Validator().NotEmpty(txtCssStylesheetDisplayName.Text, ResHelper.GetString("CssStylesheet_General.EmptyDisplayName")).NotEmpty(txtCssStylesheetName.Text, ResHelper.GetString("CssStylesheet_General.EmptyName")).NotEmpty(txtCssStylesheetText, ResHelper.GetString("CssStylesheet_General.EmptyText"))
            .IsCodeName(txtCssStylesheetName.Text, ResHelper.GetString("general.invalidcodename"))
            .Result;

        if (result == "")
        {
            CssStylesheetInfo si = CssStylesheetInfoProvider.GetCssStylesheetInfo(cssStylesheetId);
            if (si != null)
            {
                si.StylesheetDisplayName = txtCssStylesheetDisplayName.Text;
                si.StylesheetName = txtCssStylesheetName.Text;
                si.StylesheetID = cssStylesheetId;

                if (si.StylesheetCheckedOutByUserID <= 0)
                {
                    si.StylesheetText = txtCssStylesheetText.Text;
                }

                try
                {
                    CssStylesheetInfoProvider.SetCssStylesheetInfo(si);
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
                catch (Exception ex)
                {
                    lblError.Visible = true;
                    lblError.Text = ex.Message.Replace("%%name%%", txtCssStylesheetName.Text);
                    return false;
                }
                return true;
            }
            return false;
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
            return false;
        }
    }
}
