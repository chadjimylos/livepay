﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCOnlinePay.ascx.vb" Inherits="CMSTemplates_AegeanPower_UCOnlinePay" %>

<style type="text/css">
.OutPart{float:left;margin:6px 13px 0px 12px;padding:0px 0px 20px 16px;width:473px;}
.boxstyle{margin-left:5px;float:left;border:1px solid #d5d5d5;width:130px;}
.AreaToWrite{float:left;color:#7AC142;margin-left:3px;}
.ContentButton
{
    border: none;
    background: url('/App_Themes/AegeanPower/Images/OnLinePay/btnOnlinePay.jpg') no-repeat top left;
    width:197px;
    height:23px;
}

.ErrorLabel{float:left;display:block;margin-left:10px;}
.ErrorLabelAmmount{float:left;margin-left:5px;color:Red;}
</style>

<script language="javascript" type="text/javascript">

    function FixMoney(obj, Blur) {

        //--- Replace All NoneNumeric Characters
        var Nums = '1234567890.'
        var Val = obj.value
        var part_num = 0;
        while (part_num < Val.length) {
            if (Nums.indexOf(Val.charAt(part_num)) == -1) {
                if (Val.charAt(part_num) == ',') {
                    Val = Val.replace(Val.charAt(part_num), ".")
                } else {
                    Val = Val.replace(Val.charAt(part_num), "")
                }
            }
            part_num += 1;
        }

        //-- Keep Only the Last Dot
        var DotLength = Val.split(".").length - 1;
        if (DotLength > 1) {
            var part_num2 = 0;
            var col_array = Val.split(".")
            var SearchSymbol = '.'
            var FinalVal = Val
            var Counter = 0
            while (part_num2 < col_array.length) {
                Counter = Counter + 1
                if ((Counter) < (col_array.length - 1)) {
                    FinalVal = FinalVal.replace(".", "")
                }
                part_num2 += 1;
            }
            Val = FinalVal
        }
        if (DotLength > 0) {
            var ValueNow = Val.split(".")[1]
            if (ValueNow.length > 2) {      // -- if length Value after Dot is more than 2 clear the others
                Val = Val.replace(("." + ValueNow), "." + Left(ValueNow, 2))
            }
            var ValBeforeDot = Val.split(".")[0]
            if (ValBeforeDot.length == 0) {      // -- if length Value after Dot is more than 2 clear the others
                Val = Val.replace(".", "")
            }
        }

        //-- Running only on Blur
        if (Blur) {
            if (DotLength > 0) {
                var ValueNow = Val.split(".")[1]
                if (ValueNow == '') {           // -- Clear Dot if after Dot is nothing
                    Val = Val.replace(".", "")
                }
            }

            //--- If there is not dot put '.00'
            DotLength = Val.split(".").length - 1;
            if (DotLength == 0) {
                Val = Val + '.00'
            }
            if (DotLength > 0) { // -- IF after Dot is only one number put at the end one '0'
                var ValueNowAfterDot = Val.split(".")[1]
                if (ValueNow.length < 2) {
                    Val = Val + '0'
                }
            }
        }
        if (Val == '.00') Val = '';
        obj.value = Val
    }

    function ValidatePaymentCode(e, args) {
        var paymentCode = document.getElementById('<%=txtboxOnlineNumber.ClientID %>').value;
        args.IsValid = (paymentCode.length == 16);
    }

    function ValidatePayment() {
        Page_ClientValidate('OnlinePay');
        if (Page_IsValid) {
            var paymentCode = document.getElementById('<%=txtboxOnlineNumber.ClientID %>').value;
            var amount = document.getElementById('<%=txtboxAmmount.ClientID %>').value.replace('.','');
            var o = window.open('/Transaction/Initiation.aspx?PaymentCode=' + paymentCode + '&Amount=' + amount + '&Lang=<%=CMSContext.CurrentDocumentCulture.CultureCode.ToString().split("-")(1) %>', 'Payment', 'width=535,height=425');
            o.focus();
        }
        return false;
    }

</script>
<div class="OutPart">

    <div style="margin:10px 0px; float:left; width:450px;height:22px;">
        <div class="AdminUploadTitle" style="float:left;width:164px;">
            <%= ResHelper.LocalizeString("{$=Αριθμός Ηλεκτρονικής Πληρωμής:|en-us=Αριθμός Παροχής:$}")%>
        </div>
        <asp:TextBox class="boxstyle" ID="txtboxOnlineNumber" MaxLength="16"  Onkeyup="FixMoney(this,false)" runat="server" ValidationGroup="OnlinePay"></asp:TextBox>
        <div class="AreaToWrite">*</div>
        <asp:CustomValidator runat="server" ClientValidationFunction="ValidatePaymentCode"  ControlToValidate="txtboxOnlineNumber" ValidationGroup="OnlinePay" ErrorMessage="&nbsp;Εισάγετε Αριθμό Πληρωμής" ValidateEmptyText="true" ForeColor="Red"></asp:CustomValidator>
        <%--<div id="ErrorOnline" runat="server" class="ErrorLabelAmmount" visible="false"><%=ResHelper.LocalizeString("{$=Εισάγεται Αριθμό Παροχής|en-us=Εισάγεται Αριθμό Παροχής$}") %></div>--%>
        <%--<asp:CustomValidator runat="server" ValidateEmptyText="true" text="*" CssClass="ErrorLabel" ControlToValidate="txtboxOnlineNumber" ID="vstxtboxOnlineNumber" Visible="false" ValidationGroup="OnlinePay" />--%>
    </div>

    <div style="margin-bottom:10px; float:left; width:450px;height:22px;">
        <div class="AdminUploadTitle" style="text-align:right;float:left;width:164px;">
            <%= ResHelper.LocalizeString("{$=Ποσό:|en-us=Αριθμός Παροχής:$}")%>
        </div>
        <asp:TextBox class="boxstyle" ID="txtboxAmmount" Onkeyup="FixMoney(this,false)" onblur="FixMoney(this,true)" runat="server" MaxLength="8" ValidationGroup="OnlinePay"></asp:TextBox>
        <div class="AreaToWrite">*</div>
        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtboxAmmount" ValidationGroup="OnlinePay" ErrorMessage="&nbsp;Εισάγετε Ποσό" ForeColor="Red"></asp:RequiredFieldValidator>
        <%--<div id="ErrorAmmount" runat="server" class="ErrorLabelAmmount" visible="false"><%= ResHelper.LocalizeString("{$=Εισάγεται Ποσό|en-us=Εισάγεται Ποσό$}")%></div>--%>
        <%--<asp:CustomValidator ID="vsAmount" ValidateEmptyText="true" runat="server" ControlToValidate="txtboxAmmount" text="*" ValidationGroup="OnlinePay" Visible="true"/>--%>
    </div>

    <div style="float:left;padding-top:2px;width:366px;height:24px;">
        <cms:CMSButton ID="btnOk" runat="server" OnClientClick="return ValidatePayment()" ValidationGroup="OnlinePay" CssClass="ContentButton" EnableViewState="false" />
    </div>
    
    <div class="Clear"></div>

</div>