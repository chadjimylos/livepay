using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.Reporting;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Reporting_Tools_ReportValue_Edit : CMSReportingModalPage
{

    protected ReportValueInfo valueInfo = null;
    protected int valueId;
    protected ReportInfo reportInfo = null;


    /// <summary>
    /// PreRender event handler
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        this.AddNoCacheTag();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        this.RegisterModalPageScripts();

        int reportId = ValidationHelper.GetInteger(Request.QueryString["reportid"], 0);
        if (reportId > 0)
            reportInfo = ReportInfoProvider.GetReportInfo(reportId);
        if (reportInfo != null) //must be valid reportid parameter
        {
            string valueName = ValidationHelper.GetString(Request.QueryString["itemname"], "");
            if (ValidationHelper.IsCodeName(valueName))
            {
                valueInfo = ReportValueInfoProvider.GetReportValueInfo(valueName);
                valueId = valueInfo.ValueID;
            }

            rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");
            rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
            rfvQuery.ErrorMessage = ResHelper.GetString("Reporting_ReportValue_Edit.ErrorQuery");

            // Control initializations
            lblQuery.Text = ResHelper.GetString("Reporting_ReportValue_Edit.Query");
            lblStoredProcedure.Text = ResHelper.GetString("Reporting_ReportValue_Edit.StoredProcedure");
            lblFormatString.Text = ResHelper.GetString("Reporting_ReportValue_Edit.FormatString");

            if (valueInfo != null)
            {
                this.CurrentMaster.Title.TitleText = ResHelper.GetString("Reporting_ReportValue_Edit.TitleText");
                this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Reporting_ReportValue/object.png");
            }
            else //new item
            {
                this.CurrentMaster.Title.TitleText = ResHelper.GetString("Reporting_ReportValue_Edit.NewItemTitleText");
                this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Reporting_ReportValue/new.png");
            }

            // set help key
            this.CurrentMaster.Title.HelpTopicName = "report_value_properties";

            if (!RequestHelper.IsPostBack())
            {
                LoadData();
            }
        }
        else
        {
            pnlElements.Visible = false;
            btnOk.Visible = false;
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Reporting_ReportValue_Edit.InvalidReportId");
        }
        btnOk.Text = ResHelper.GetString("General.OK");
        btnCancel.Text = ResHelper.GetString("General.Cancel");


    }


    protected void LoadData()
    {

        if (valueInfo != null)
        {
            txtDisplayName.Text = valueInfo.ValueDisplayName;
            txtCodeName.Text = valueInfo.ValueName;
            txtQuery.Text = valueInfo.ValueQuery;
            chkStoredProcedure.Checked = valueInfo.ValueQueryIsStoredProcedure;
            txtFormatString.Text = valueInfo.ValueFormatString;
        }
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (Save())
        {
            ltlScript.Text += ScriptHelper.GetScript("wopener.ReloadPage();window.close();");
        }
    }


    /// <summary>
    /// Save data
    /// </summary>
    /// <returns>return true if save was successfull</returns>
    protected bool Save()
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
        {
            RedirectToAccessDenied("cms.reporting", "Modify");
        }

        string errorMessage = new Validator().NotEmpty(txtDisplayName.Text, rfvDisplayName.ErrorMessage).NotEmpty(txtCodeName.Text, rfvCodeName.ErrorMessage).NotEmpty(txtQuery.Text, rfvQuery.ErrorMessage).Result;

        if ((errorMessage == "") && (!ValidationHelper.IsIdentificator(txtCodeName.Text.Trim())))
        {
            errorMessage = ResHelper.GetString("general.erroridentificatorformat");
        }

        string fullName = reportInfo.ReportName + "." + txtCodeName.Text.Trim();
        ReportValueInfo codeNameCheck = ReportValueInfoProvider.GetReportValueInfo(fullName);

        if ((errorMessage == "") && (codeNameCheck != null) && (codeNameCheck.ValueID != valueId))
        {
            errorMessage = ResHelper.GetString("Reporting_ReportValue_Edit.ErrorCodeNameExist");
        }

        if ((errorMessage == ""))
        {
            if (valueInfo == null) //new Value
                valueInfo = new ReportValueInfo();

            valueInfo.ValueDisplayName = txtDisplayName.Text.Trim();
            valueInfo.ValueName = txtCodeName.Text.Trim();
            valueInfo.ValueQuery = txtQuery.Text.Trim();
            valueInfo.ValueQueryIsStoredProcedure = chkStoredProcedure.Checked;
            valueInfo.ValueFormatString = txtFormatString.Text.Trim();
            valueInfo.ValueReportID = reportInfo.ReportID;

            ReportValueInfoProvider.SetReportValueInfo(valueInfo);
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
            return false;
        }
        return true;
    }
}
