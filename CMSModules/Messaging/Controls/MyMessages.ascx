<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyMessages.ascx.cs" Inherits="CMSModules_Messaging_Controls_MyMessages" %>
<%@ Register Src="~/CMSModules/Messaging/Controls/Inbox.ascx" TagName="Inbox" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Messaging/Controls/Outbox.ascx" TagName="Outbox" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Messaging/Controls/ContactList.ascx" TagName="ContactList"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Messaging/Controls/IgnoreList.ascx" TagName="IgnoreList"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" TagName="title" TagPrefix="cms" %>
<asp:Panel runat="server" ID="pnlBody" CssClass="MyMessages">
    <div class="TabsHeader">
        <asp:Panel runat="server" ID="pnlLeft" CssClass="TabsLeft" />
        <asp:Panel runat="server" ID="pnlTabs" CssClass="TabsTabs">
            <asp:Panel runat="server" ID="pnlWhite" CssClass="TabsWhite">
                <cms:BasicTabControl ID="tabMenu" runat="server" />
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlRight" CssClass="TabsRight" />
    </div>
    <div class="TabsContent MessagingBox">
        <cms:Inbox ID="ucInbox" runat="server" />
        <cms:Outbox ID="ucOutbox" runat="server" />
        <cms:ContactList ID="ucContactList" runat="server" />
        <cms:IgnoreList ID="ucIgnoreList" runat="server" />
    </div>
</asp:Panel>
<asp:Literal runat="server" ID="litMessage" Visible="false" EnableViewState="false" />
