<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductDocuments.ascx.cs"
    Inherits="CMSModules_Ecommerce_Controls_UI_ProductDocuments" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Filters/DocumentFilter.ascx" TagName="DocumentFilter"
    TagPrefix="cms" %>
<cms:DocumentFilter ID="filterDocuments" runat="server" LoadSites="true" AllowSiteAutopostback="false"
    IncludeSiteCondition="true" />
<br />
<br />
<cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <cms:UniGrid ID="uniGridDocs" runat="server" GridName="~/CMSModules/Ecommerce/Controls/UI/ProductDocuments.xml"
            HideControlForZeroRows="false" IsLiveSite="false" Columns="ClassName, ClassDisplayName, StepName, NodeID, DocumentCulture, DocumentName, DocumentNamePath, NodeLinkedNodeID, SiteName" />
        <cms:LocalizedLabel ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
