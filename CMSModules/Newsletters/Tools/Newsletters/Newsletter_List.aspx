<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletter_List.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Tools - Newsletters" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Newsletter_List.xml" OrderBy="NewsletterDisplayName"
        IsLiveSite="false" />
</asp:Content>
