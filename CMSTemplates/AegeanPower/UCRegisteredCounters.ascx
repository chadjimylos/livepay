﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCRegisteredCounters.ascx.vb" Inherits="CMSTemplates_AegeanPower_UCRegisteredCounters" %>
<style type="text/css">
    .RegCountersMain{padding-left:12px;padding-right:11px;padding-top:10px}
    .RegCounterGvSelRow{border:1px solid #7ac142;background-color:White}
    .RegCounterGvSelRow2{border:0px;background-color:transparent}
    
    
    .RegCounterGvLastDiv{float:left;width:163px;height:29px}
    .RegCounterGvLastDivSel{float:left;width:161px;height:29px}
    
    .RegCounterPagerNumbers{padding-right:5px;color:#000000;text-decoration:none;cursor:pointer}
    .RegCounterPagerNumbersSel{padding-right:5px;color:#7ac142;font-weight:bold;text-decoration:underline;cursor:pointer}
    
    .RegCounterGvRowGlobal{border-right:1px solid #f0f1f4;border-bottom:1px solid #f0f1f4;width:119px;height:29px}
    .RegCounterGvRowLast{border-bottom:1px solid #f0f1f4;width:119px;height:29px}
</style>
<script language="javascript" type="text/javascript">

    function SelectAfmRow(obj,SecondObj) {
        var GvMainDiv = document.getElementById('GvMainDiv')
        var GvDivLast = document.getElementById(SecondObj)

        var AllSelect = GvMainDiv.getElementsByTagName('div');
        for (i = 0; i < AllSelect.length; i++) {
            if (AllSelect[i].id.indexOf('GvAfmRow_GvRpt') != -1) {
                AllSelect[i].className = 'RegCounterGvSelRow2'
            }
        }
        var MyRow = document.getElementById(obj)
        MyRow.className = 'RegCounterGvSelRow'
        GvDivLast.className = 'RegCounterGvLastDivSel'
    }

    function SetDLPage(PageNo) {
        document.getElementById('<%=SelectedPage.ClientID %>').value = PageNo;
        var GetID = '<%=HiddenBtnNextPage.ClientID %>'
        GetID = GetID.replace(/[_]/gi, "$")
        __doPostBack(GetID, '')
    }

    function AddNewAfm() {
        $("a[id$=HiddenLink]").click()
    }
</script>
<style type="text/css">
.lightbox .t{width:420px}
.lightbox .m{width:420px}
.lightbox .inside{width:420px}
.lightbox .close{left:403px}
.lightbox .holder{width:400px}
.lightbox .b{width:420px}
</style>

<a id="HiddenLink" rel="403|175"  class="iframe-popup" runat="server" style="display:none"  href="~/αρχική-σελίδα/κέντρο-πελατών/μέτρα-μόνος-σου/ibuttons/insertnewafm.aspx"></a> 
<asp:UpdatePanel ID="uptPanel" runat="server" UpdateMode="Always">
<ContentTemplate>
<asp:Button id="btnRebindGVAFM" runat="server" style="display:none" />

<div class="RegCountersMain">
    <div style="height:20px;background-color:#ecedf1;">
        <div style="padding-left:10px;float:left;padding-top:3px">
            <a id="NewAfmLink" runat="server" onclick="AddNewAfm()" style="text-decoration:none;font-size:11px;font-weight:bold;color:#004990;cursor:pointer;" >Προσθήκη Νέου ΑΦΜ</a>        
        </div>
        <div style="float:left;padding-top:4px;padding-left:5px"><img src="/App_Themes/AegeanPower/Images/RegisteredCounters/GreenPlus.png" /></div>
        <div class="Clear"></div>
    </div>
    <div class="PT1">
        <div style="float:left;background-color:#e3f2f7;border-right:1px solid white;width:163px;height:29px">
            <div style="color:#004990;font-size:11px;padding-top:8px;padding-left:10px"><%=ResHelper.LocalizeString("{$=ΑΦΜ|en-us=ΑΦΜ$}") %></div>
        </div>
        <div style="float:left;background-color:#e3f2f7;border-right:1px solid white;width:163px;height:29px">
            <div style="color:#004990;font-size:11px;padding-top:8px;padding-left:10px"><%=ResHelper.LocalizeString("{$=Status|en-us=Status$}") %></div>
        </div>
        <div style="float:left;background-color:#e3f2f7;width:163px;height:29px">
            <div style="color:#004990;font-size:11px;padding-top:8px;padding-left:10px"><%=ResHelper.LocalizeString("{$=Διαθέσιμοι Μετρητές|en-us=Διαθέσιμοι Μετρητές$}") %></div>
        </div>
         <div class="Clear"></div>
    </div>
     <div class="PT1" id="GvMainDiv" style="padding-bottom:3px">
            <asp:GridView ID="GvAFM" runat="server" ShowHeader="false" AutoGenerateColumns="false" GridLines="None" >
            <AlternatingRowStyle BackColor="#f7f9f9" />
                <Columns>
                     <asp:TemplateField >
                        <ItemTemplate>
                           <div id="GvAfmRow_GvRpt" runat="server">
                             <div style="float:left;width:163px;height:29px">
                                  <div style="color:#000000;font-size:11px;padding-top:8px;padding-left:10px"><asp:Label ID="lblAFM" runat="server" Text='<%#Eval("Afm") %>' ></asp:Label></div>
                             </div>
                             <div style="float:left;width:163px;height:29px">
                                <div style="color:#7ac142;font-size:11px;padding-top:8px;padding-left:10px;font-weight:bold"><asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>' ></asp:Label></div>
                             </div>
                             <div class="RegCounterGvLastDiv" id="GvAfmLastDiv_GvRpt" runat="server">
                                <div style="padding-top:2px;padding-left:10px;font-weight:bold">
                                  <asp:ImageButton OnClick="GvAFM_CounterBtn_Click" ID="imgCounters" runat="server" ImageUrl="/App_Themes/AegeanPower/Images/RegisteredCounters/icon_greenpen.png" />  
                                </div>
                             </div>
                             <div class="Clear"></div>
                           </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </div>
        <div style="background-color:#e3e3e3;font-size:1px;height:1px;width:100%">&nbsp;</div>
        <div style="padding-top:20px" id="MainCounter" runat="server" visible="false">
            <div style="height:20px;background-color:#ecedf1;">
                <div style="padding-left:10px;padding-top:1px;float:left"><img src="/App_Themes/AegeanPower/Images/RegisteredCounters/icon_greenpensmall.png" /></div>
                <div style="padding-left:10px;float:left;padding-top:3px;font-size:11px;font-weight:bold;color:#004990"><%=ResHelper.LocalizeString("{$=Διαθέσιμοι Μετρητές για το|en-us=Διαθέσιμοι Μετρητές για το$}") %></div>
                <div style="padding-left:5px;float:left;padding-top:3px;font-size:11px;font-weight:bold;color:#7ac142"><%=ResHelper.LocalizeString("{$=ΑΦΜ:  |en-us=ΑΦΜ:  $}") %></div>
                <div style="padding-left:5px;float:left;padding-top:3px;font-size:11px;font-weight:bold;color:#7ac142" id="DivAfmTitle" runat="server"></div>
                <div class="Clear"></div>
            </div>
            <div style="font-size:1px;height:3px">&nbsp;</div>
            <div style="border:1px solid #7ac142;width:488px" >
                <div>
                    <asp:DataList ID="dlCounters" GridLines="None" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" ShowHeader="false" Width="486px">
                        <ItemTemplate>
                        <div>
                            <div class="RegCounterGvRowGlobal" id="GvRow" runat="server">
                                <div style="text-align:center;padding-top:7px;color:#000000"><asp:Label ID="lblProviderNumber" runat="server" Text='<%#Eval("Provider_Number")  %>&nbsp;' ></asp:Label></div>
                            </div>
                        </div>
                        </ItemTemplate>
                    </asp:DataList>
                
                </div>
                <div id="dlCountersPager" runat="server" visible="false" style="width:488px;text-align:center;">
                    <div style="background-color:#7ac142;font-size:1px;height:1px;width:100%">&nbsp;</div>
                     <div style="padding-top:10px;padding-bottom:10px;">
                            <div style="float:left;padding-top:1px;padding-left:10px;width:20px"><img class="Chant" ID="PagerPrev" runat="server" src="/App_Themes/AegeanPower/Images/RegisteredCounters/PagerGreenPrev.png" />&nbsp;</div>
                            <div style="float:left;width:430px;text-align:center" id="PagerNumbers" runat="server"></div> 
                            <div style="float:left;padding-top:1px;">&nbsp;<img class="Chant"  ID="PagerNext" runat="server" src="/App_Themes/AegeanPower/Images/RegisteredCounters/PagerGreenNext.png" /></div>
                            <div class="Clear"></div>
                     </div>
                </div>
            </div>
        </div>
</div>
<div style="display:none">
<asp:Button ID="HiddenBtnNextPage" runat="server" />
<asp:TextBox ID="SelectedPage" runat="server" Text="1"></asp:TextBox>
<asp:TextBox ID="SelectedAfm" runat="server" ></asp:TextBox>
<asp:TextBox ID="SelectedAFMRow" runat="server" ></asp:TextBox>
</div>
</ContentTemplate>
</asp:UpdatePanel>
