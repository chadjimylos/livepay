using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Ecommerce;
using CMS.EcommerceProvider;
using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;

public partial class CMSModules_Ecommerce_Controls_ShoppingCart_ShoppingCartPreview : ShoppingCartStep
{
    #region "ViewState Constants"

    private const string ORDER_NOTE = "OrderNote";

    #endregion


    SiteInfo currentSite = null;
    private int mAddressCount = 1;

    protected void Page_Load(object sender, EventArgs e)
    {
        currentSite = CMSContext.CurrentSite;

        this.lblTitle.Text = ResHelper.GetString("ShoppingCartPreview.Title");

        if ((ShoppingCartInfoObj != null) && (ShoppingCartInfoObj.CountryID == 0) && (currentSite != null))
        {
            ShoppingCartInfoObj.CountryID = currentSite.SiteDefaultCountryID;
        }

        this.ShoppingCartControl.ButtonNext.Text = ResHelper.GetString("Ecommerce.OrderPreview.NextButtonText");

        // addresses initialization
        pnlBillingAddress.GroupingText = ResHelper.GetString("Ecommerce.CartPreview.BillingAddressPanel");
        pnlShippingAddress.GroupingText = ResHelper.GetString("Ecommerce.CartPreview.ShippingAddressPanel");
        pnlCompanyAddress.GroupingText = ResHelper.GetString("Ecommerce.CartPreview.CompanyAddressPanel");

        FillBillingAddressForm(ShoppingCartInfoObj.ShoppingCartBillingAddressID);
        FillShippingAddressForm(ShoppingCartInfoObj.ShoppingCartShippingAddressID);

        // Load company address
        if (ShoppingCartInfoObj.ShoppingCartCompanyAddressID > 0)
        {
            lblCompany.Text = OrderInfoProvider.GetAddress(ShoppingCartInfoObj.ShoppingCartCompanyAddressID);
            mAddressCount++;
            tdCompanyAddress.Visible = true;
        }
        else
        {
            tdCompanyAddress.Visible = false;
        }

        // Enable sending order notifications when creating order from CMSDesk
        if ((this.ShoppingCartControl.CheckoutProcessType == CheckoutProcessEnum.CMSDeskOrder) ||
            this.ShoppingCartControl.CheckoutProcessType == CheckoutProcessEnum.CMSDeskCustomer)
        {
            chkSendEmail.Visible = true;
            chkSendEmail.Checked = currentSite.SiteSendOrderNotification;
            chkSendEmail.Text = ResHelper.GetString("ShoppingCartPreview.SendNotification");
        }

        // Show tax registration ID and organization ID
        InitializeIDs();

        // shopping cart content table initialization
        gridData.Columns[4].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.SKUName");
        gridData.Columns[5].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.SKUUnits");
        gridData.Columns[6].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.UnitPrice");
        gridData.Columns[7].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.UnitDiscount");
        gridData.Columns[8].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.Tax");
        gridData.Columns[9].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.Subtotal");

        // tax summary table initialiazation
        gridTaxSummary.Columns[0].HeaderText = ResHelper.GetString("Ecommerce.CartContent.TaxDisplayName");
        gridTaxSummary.Columns[1].HeaderText = ResHelper.GetString("Ecommerce.CartContent.TaxSummary");

        ReloadData();

        // order note initialization
        lblNote.Text = ResHelper.GetString("ecommerce.cartcontent.notelabel");
        if (!this.ShoppingCartControl.IsCurrentStepPostBack)
        {
            // Try to select payment from ViewState first
            object viewStateValue = this.ShoppingCartControl.GetTempValue(ORDER_NOTE);
            if (viewStateValue != null)
            {
                this.txtNote.Text = Convert.ToString(viewStateValue);
            }
            else
            {
                this.txtNote.Text = ShoppingCartInfoObj.ShoppingCartNote;
            }
        }
        // Display/Hide column with applied discount
        gridData.Columns[7].Visible = this.ShoppingCartInfoObj.IsDiscountApplied;


        if (mAddressCount == 2)
        {
            tblAddressPreview.Attributes["class"] = "AddressPreviewWithTwoColumns";
        }
        else if (mAddressCount == 3)
        {
            tblAddressPreview.Attributes["class"] = "AddressPreviewWithThreeColumns";
        }
    }


    protected void Page_Prerender(object sender, EventArgs e)
    {
        // Hide columns with identifiers
        gridData.Columns[0].Visible = false;
        gridData.Columns[1].Visible = false;
        gridData.Columns[2].Visible = false;
        gridData.Columns[3].Visible = false;

        // Disable default button in the order preview to 
        // force approvement of the order by mouse click
        if (this.ShoppingCartControl.ShoppingCartContainer != null)
        {
            this.ShoppingCartControl.ShoppingCartContainer.DefaultButton = "";
        }
    }


    protected void ReloadData()
    {
        gridData.DataSource = ShoppingCartInfoObj.ShoppingCartContentTable;
        gridData.DataBind();

        gridTaxSummary.DataSource = ShoppingCartInfoObj.ShoppingCartTaxTable;
        gridTaxSummary.DataBind();

        // shipping option, payment method initialization
        InitPaymentShipping();
    }


    /// <summary>
    /// Fills billing address form.
    /// </summary>
    /// <param name="addressId">Billing address id.</param>
    protected void FillBillingAddressForm(int addressId)
    {
        this.lblBill.Text = OrderInfoProvider.GetAddress(addressId);
    }


    /// <summary>
    /// Fills shipping address form.
    /// </summary>
    /// <param name="addressId">Shipping address id.</param>
    protected void FillShippingAddressForm(int addressId)
    {
        this.lblShip.Text = OrderInfoProvider.GetAddress(addressId);
    }


    /// <summary>
    /// Back button actions
    /// </summary>
    public override void ButtonBackClickAction()
    {
        // Save the values to ShoppingCart ViewState
        this.ShoppingCartControl.SetTempValue(ORDER_NOTE, this.txtNote.Text);

        base.ButtonBackClickAction();
    }


    /// <summary>
    /// Saves order information from ShoppingCartInfo object to database as new order.
    /// </summary>
    public override bool ProcessStep()
    {
        // Load first step if there is no currency or no address
        if ((this.ShoppingCartInfoObj.ShoppingCartBillingAddressID <= 0) || (this.ShoppingCartInfoObj.ShoppingCartCurrencyID <= 0))
        {
            this.ShoppingCartControl.LoadStep(0);
            return false;
        }

        // Check available items before order is finished
        ShoppingCartInfoProvider.EvaluateShoppingCartContent(ShoppingCartInfoObj);

        if (ShoppingCartInfoObj.ItemsNotAvailable)
        {
            ReloadData();
            return false;
        }
        else
        {
            // Deal with order note
            this.ShoppingCartControl.SetTempValue(ORDER_NOTE, null);
            this.ShoppingCartInfoObj.ShoppingCartNote = this.txtNote.Text.Trim();

            try
            {
                // Create order
                ShoppingCartInfoProvider.SetOrder(this.ShoppingCartInfoObj);

                // Save customers' prefferd options
                CustomerInfo ci = ShoppingCartInfoObj.CustomerInfoObj;
                if (ci != null)
                {
                    ci.CustomerPreferredCurrencyID = ShoppingCartInfoObj.ShoppingCartCurrencyID;
                    ci.CustomerPreferredShippingOptionID = ShoppingCartInfoObj.ShoppingCartShippingOptionID;
                    ci.CustomerPrefferedPaymentOptionID = ShoppingCartInfoObj.ShoppingCartPaymentOptionID;

                    CustomerInfoProvider.SetCustomerInfo(ci);
                }

                // Set order culture
                ShoppingCartInfoObj.ShoppingCartCulture = CMSContext.PreferredCultureCode;

            }
            catch (Exception ex)
            {
                lblError.Text = ResHelper.GetString("Ecommerce.OrderPreview.ErrorOrderSave") + " (" + ex.Message + ")";
                lblError.Visible = true;
                return false;
            }

            // Track order conversion
            this.ShoppingCartControl.TrackOrderConversion();

            // Raise finish order event
            this.ShoppingCartControl.RaiseOrderCompletedEvent();

            // When in CMSDesk
            if (this.ShoppingCartControl.IsInternalOrder)
            {
                if (chkSendEmail.Checked)
                {
                    // Send order notification emails
                    OrderInfoProvider.SendOrderNotificationToAdministrator(this.ShoppingCartInfoObj);
                    OrderInfoProvider.SendOrderNotificationToCustomer(this.ShoppingCartInfoObj);
                }
            }
            // When on the live site
            else if (CMSContext.CurrentSite.SiteSendOrderNotification)
            {
                // Send order notification emails
                OrderInfoProvider.SendOrderNotificationToAdministrator(this.ShoppingCartInfoObj);
                OrderInfoProvider.SendOrderNotificationToCustomer(this.ShoppingCartInfoObj);
            }

            return true;
        }
    }


    protected void InitPaymentShipping()
    {
        // shipping option and payment method
        lblShippingOption.Text = ResHelper.GetString("Ecommerce.CartContent.ShippingOption");
        lblPaymentMethod.Text = ResHelper.GetString("Ecommerce.CartContent.PaymentMethod");
        lblShipping.Text = ResHelper.GetString("Ecommerce.CartContent.Shipping");

        if (currentSite != null)
        {
            // get shipping option name
            ShippingOptionInfo shippingObj = ShippingOptionInfoProvider.GetShippingOptionInfo(ShoppingCartInfoObj.ShoppingCartShippingOptionID);
            if (shippingObj != null)
            {
                mAddressCount++;
                //plcShippingAddress.Visible = true;
                tdShippingAddress.Visible = true;
                plcShipping.Visible = true;
                plcShippingOption.Visible = true;
                lblShippingOptionValue.Text = HTMLHelper.HTMLEncode(shippingObj.ShippingOptionDisplayName);
                lblShippingValue.Text = CurrencyInfoProvider.GetFormatedPrice(ShoppingCartInfoObj.TotalShipping, ShoppingCartInfoObj.CurrencyInfoObj);
            }
            else
            {
                //plcShippingAddress.Visible = false;
                tdShippingAddress.Visible = false;
                plcShippingOption.Visible = false;
                plcShipping.Visible = false;
            }
        }

        // get payment method name
        PaymentOptionInfo paymentObj = PaymentOptionInfoProvider.GetPaymentOptionInfo(ShoppingCartInfoObj.ShoppingCartPaymentOptionID);
        if (paymentObj != null)
        {
            lblPaymentMethodValue.Text = HTMLHelper.HTMLEncode(paymentObj.PaymentOptionDisplayName);
        }


        // total price initialization
        lblTotalPrice.Text = ResHelper.GetString("ecommerce.cartcontent.totalprice");
        lblTotalPriceValue.Text = CurrencyInfoProvider.GetFormatedPrice(ShoppingCartInfoObj.RoundedTotalPrice, ShoppingCartInfoObj.CurrencyInfoObj);
    }


    /// <summary>
    /// Displays product error message in shopping cart content table
    /// </summary>
    /// <param name="skuErrorMessage">Error message to be displayed.</param>
    protected string DisplaySKUErrorMessage(object skuErrorMessage)
    {
        string err = ValidationHelper.GetString(skuErrorMessage, "");
        if (err != "")
        {
            return "<br /><span class=\"ItemsNotAvailable\">" + err + "</span>";
        }
        return "";
    }


    /// <summary>
    /// Initializes tax registration ID and orgranization ID.
    /// </summary>
    protected void InitializeIDs()
    {
        SiteInfo si = CMSContext.CurrentSite;
        if (si != null)
        {
            if ((si.SiteShowOrganizationID) && (this.ShoppingCartInfoObj.CustomerInfoObj != null))
            {
                // Initialize organization ID
                plcIDs.Visible = true;
                lblOrganizationID.Text = ResHelper.GetString("OrderPreview.OrganizationID");
                lblOrganizationIDVal.Text = this.ShoppingCartInfoObj.CustomerInfoObj.CustomerOrganizationID;
            }
            else
            {
                lblOrganizationID.Visible = false;
                lblOrganizationIDVal.Visible = false;
            }

            if ((si.SiteShowTaxRegistrationID) && (this.ShoppingCartInfoObj.CustomerInfoObj != null))
            {
                // Initialize tax registration ID
                plcIDs.Visible = true;
                lblTaxRegistrationID.Text = ResHelper.GetString("OrderPreview.TaxRegistrationID");
                lblTaxRegistrationIDVal.Text = this.ShoppingCartInfoObj.CustomerInfoObj.CustomerTaxRegistrationID;
            }
            else
            {
                lblTaxRegistrationID.Visible = false;
                lblTaxRegistrationIDVal.Visible = false;
            }
        }
    }


    /// <summary>
    /// Returns formated value string.
    /// </summary>
    /// <param name="value">Value to format.</param>
    protected string GetFormattedValue(object value)
    {
        double price = ValidationHelper.GetDouble(value, 0);
        return CurrencyInfoProvider.GetFormatedValue(price, this.ShoppingCartInfoObj.CurrencyInfoObj);
    }


    /// <summary>
    /// Returns formatted and localized SKU name.
    /// </summary>
    /// <param name="value">SKU name.</param>
    /// <param name="parentItemGuid">Product option parent product GUID.</param>
    protected string GetSKUName(object value, object parentItemGuid)
    {
        string name = ResHelper.LocalizeString((string)value);

        // If it is a product option
        if (IsProductOption(parentItemGuid))
        {
            return "<span style=\"font-size:90%\"> - " + HTMLHelper.HTMLEncode(name) + "</span>";
        }
        // If it is a parent product
        else
        {
            return HTMLHelper.HTMLEncode(name);
        }
    }


    /// <summary>
    /// Determines whether it is a preduct option or not.
    /// </summary>
    /// <param name="parentProductGuid">Parent product GUID.</param>
    protected bool IsProductOption(object parentProductGuid)
    {
        return (ValidationHelper.GetGuid(parentProductGuid, Guid.Empty) != Guid.Empty);
    }
}
