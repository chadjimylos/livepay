﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;

public partial class CMSFormControls_LiveSelectors_InsertYouTubeVideo_Content : CMSLiveModalPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Page.Title = ResHelper.GetString("dialogs.youtube.inserttitle");
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("dialogs.youtube.inserttitle");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Content/Dialogs/YouTube.png");
        this.CurrentMaster.Body.Attributes.Add("onbeforeunload", "$j('.YouTubePreviewBox').remove();");
        this.CurrentMaster.DisplayControlsPanel = true;
        this.Page.Header.Controls.Add(new LiteralControl("<style>.PageTitleImage { vertical-align: middle; padding: 0px 5px 5px 0px; width: 49px; height: 24px; }</style>"));

        this.lnkYouTube.Text = ResHelper.GetString("dialogs.youtube.goto");
    }
}
