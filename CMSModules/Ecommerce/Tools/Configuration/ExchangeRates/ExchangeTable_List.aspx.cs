using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_ExchangeRates_ExchangeTable_List : CMSEcommerceConfigurationPage
{
    int currentTableId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        // Pagetitle
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("ExchangeTable_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_ExchangeTable/object.png");
        this.CurrentMaster.Title.HelpTopicName = "exchange_rates_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("ExchangeTable_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("ExchangeTable_Edit.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_ExchangeTable/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        // Get current table (it will be highlighted in the listing)
        ExchangeTableInfo eti = ExchangeTableInfoProvider.GetLastValidExchangeTableInfo();
        if (eti != null)
        {
            currentTableId = eti.ExchangeTableID;
        }

        gridElem.GridView.RowDataBound += new GridViewRowEventHandler(GridView_RowDataBound);
        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("ExchangeTable_Edit.aspx?exchangeid=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // check 'ConfigurationModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
            }

            // delete ExchangeTableInfo object from database
            ExchangeTableInfoProvider.DeleteExchangeTableInfo(ValidationHelper.GetInteger(actionArgument, 0));
        }
    }


    /// <summary>
    /// Handles the databoud event (for coloring valid/invalid exchange rates)
    /// </summary>
    void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataRowView drv = e.Row.DataItem as DataRowView;
        if (drv != null)
        {
            int tableId = ValidationHelper.GetInteger(drv.Row["ExchangeTableID"], 0);
            if (tableId == currentTableId)
            {
                // Exchange rate wchich will be used for all operations is highlighted
                e.Row.Style.Add("background-color", "rgb(221, 250, 222)");
            }
        }
    }
}
