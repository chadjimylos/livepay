﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/SmartSearch/SearchDialogWithResults.ascx.cs"
    Inherits="CMSWebParts_SmartSearch_SearchDialogWithResults" %>
<%@ Register Src="~/CMSModules/SmartSearch/Controls/SearchResults.ascx" TagName="SearchResults"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/SmartSearch/Controls/SearchDialog.ascx" TagName="SearchDialog"
    TagPrefix="cms" %>
<div class="SearchDialog">
    <cms:SearchDialog ID="srchDialog" runat="server" />
</div>
<div class="SearchResults">
    <cms:SearchResults ID="srchResults" runat="server" />
</div>
