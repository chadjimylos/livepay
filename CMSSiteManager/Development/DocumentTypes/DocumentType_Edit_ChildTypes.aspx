<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentType_Edit_ChildTypes.aspx.cs"
    Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_ChildTypes"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Document Type Edit - Child Types" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>


<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <strong><asp:Label runat="server" ID="lblTitle" CssClass="InfoLabel" EnableViewState="false" /></strong>
    <cms:UniSelector ID="uniSelector" runat="server" IsLiveSite="false" ObjectType="cms.documenttype"
        SelectionMode="Multiple" />
</asp:Content>
