using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_OrderStatus_OrderStatus_Edit : CMSEcommerceConfigurationPage
{
    protected int orderStatusId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        rfvStatusDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvStatusName.ErrorMessage = ResHelper.GetString("general.requirescodename");

        // control initializations				
        lblStatusEmail.Text = ResHelper.GetString("OrderStatus_Edit.StatusSendEmail");
        lblStatusDisplayName.Text = ResHelper.GetString("OrderStatus_Edit.StatusDisplayNameLabel");
        lblStatusName.Text = ResHelper.GetString("OrderStatus_Edit.StatusNameLabel");
        lblStatusColor.Text = ResHelper.GetString("OrderStatus_Edit.StatusColor");
        clrPicker.SupportFolder = "~/CMSAdminControls/ColorPicker";        

        btnOk.Text = ResHelper.GetString("General.OK");

        string currentOrderStatus = ResHelper.GetString("OrderStatus_Edit.NewItemCaption");

        // get order status id from querystring		
        orderStatusId = ValidationHelper.GetInteger(Request.QueryString["orderStatusId"], 0);
        if (orderStatusId > 0)
        {
            OrderStatusInfo orderStatusObj = OrderStatusInfoProvider.GetOrderStatusInfo(orderStatusId);
            if (orderStatusObj != null)
            {
                currentOrderStatus = ResHelper.LocalizeString(orderStatusObj.StatusDisplayName);

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(orderStatusObj);

                    // show that the orderStatus was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }

            this.CurrentMaster.Title.TitleText = ResHelper.GetString("OrderStatus_Edit.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_OrderStatus/object.png");
        }
        else
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("OrderStatus_Edit.NewItemCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_OrderStatus/new.png");
        }
        this.CurrentMaster.Title.HelpTopicName = "newedit_order";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Initializes page title breadcrumbs control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("OrderStatus_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/OrderStatus/OrderStatus_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentOrderStatus;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// Load data of editing orderStatus.
    /// </summary>
    /// <param name="orderStatusObj">OrderStatus object.</param>
    protected void LoadData(OrderStatusInfo orderStatusObj)
    {
        chkStatusEnabled.Checked = orderStatusObj.StatusEnabled;
        txtStatusDisplayName.Text = orderStatusObj.StatusDisplayName;
        txtStatusName.Text = orderStatusObj.StatusName;
        clrPicker.SelectedColor = orderStatusObj.StatusColor;
        chkStatusEmail.Checked = orderStatusObj.StatusSendNotification;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        string errorMessage = new Validator()
            .NotEmpty(txtStatusName.Text, ResHelper.GetString("general.requirescodename"))
            .NotEmpty(txtStatusDisplayName.Text, ResHelper.GetString("general.requiresdisplayname")).Result;

        if (!ValidationHelper.IsCodeName(txtStatusName.Text))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

        if (errorMessage == "")
        {
            OrderStatusInfo orderStatusObj = OrderStatusInfoProvider.GetOrderStatusInfo(txtStatusName.Text);

			// if currencyName value is unique														
            if ((orderStatusObj == null) || (orderStatusObj.StatusID == orderStatusId))
            {
                // if currencyName value is unique -> determine whether it is update or insert 
                if ((orderStatusObj == null))
                {
                    // get CurrencyInfo object by primary key
                    orderStatusObj = OrderStatusInfoProvider.GetOrderStatusInfo(orderStatusId);
                    if (orderStatusObj == null)
                    {
                        // create new item -> insert
                        orderStatusObj = new OrderStatusInfo();
                    }
                }

                orderStatusObj.StatusSendNotification = chkStatusEmail.Checked;
                orderStatusObj.StatusEnabled = chkStatusEnabled.Checked;
                orderStatusObj.StatusDisplayName = txtStatusDisplayName.Text.Trim();
                orderStatusObj.StatusName = txtStatusName.Text.Trim();
                if (orderStatusObj.StatusOrder == 0)
                {
                    orderStatusObj.StatusOrder = OrderStatusInfoProvider.GetLastStatusOrder() + 1;
                }
                orderStatusObj.StatusColor = clrPicker.SelectedColor;

                OrderStatusInfoProvider.SetOrderStatusInfo(orderStatusObj);

                UrlHelper.Redirect("OrderStatus_Edit.aspx?orderStatusId=" + Convert.ToString(orderStatusObj.StatusID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("OrderStatus_Edit.OrderStatusExist");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
