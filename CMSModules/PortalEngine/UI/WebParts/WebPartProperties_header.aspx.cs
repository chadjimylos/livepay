using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.FormEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_WebParts_WebPartProperties_header : CMSWebPartPropertiesPage
{
    #region "Variables"

    private bool showCodeTab = ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSShowWebPartCodeTab"], false);
    private bool showBindingTab = ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSShowWebPartBindingTab"], false);
    private bool isNew = false;

    #endregion


    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        this["TabControl"] = tabsElem;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize page title
        PageTitle.TitleText = ResHelper.GetString("WebpartProperties.Title");
        PageTitle.TitleImage = GetImageUrl("CMSModules/CMS_PortalEngine/Webpartproperties.png");

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }

        this.tabsElem.OnTabCreated += new UITabs.TabCreatedEventHandler(tabElem_OnTabCreated);

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.NEWWINDOW_SCRIPT_KEY, ScriptHelper.NewWindowScript);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Call the script for tab which is selected
        if (!tabsElem.TabsEmpty)
        {
            ScriptHelper.RegisterStartupScript(this.Page, typeof(string), "FirstTabSelection", ScriptHelper.GetScript(" redir('" + this.tabsElem.Tabs[0, 2] + "','" + this.tabsElem.UrlTarget + "'); " + this.tabsElem.Tabs[0, 1]));
        }
        else
        {
            ScriptHelper.RegisterStartupScript(this.Page, typeof(string), "FirstTabSelection", ScriptHelper.GetScript(" redir('" + UrlHelper.ResolveUrl("~/CMSMessages/Information.aspx") + "?message=" + ResHelper.GetString("uiprofile.uinotavailable") + "','" + this.tabsElem.UrlTarget + "'); "));
        }
    }


    /// <summary>
    /// Initializes menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string aliasPath = QueryHelper.GetString("aliaspath", "");
        string webpartId = QueryHelper.GetString("webpartid", "");
        string zoneId = QueryHelper.GetString("zoneid", "");
        Guid instanceGuid = QueryHelper.GetGuid("instanceguid", Guid.Empty);
        isNew = QueryHelper.GetBoolean("isnew", false);

        if ((webpartId != "") && (aliasPath != ""))
        {
            // get pageinfo
            PageInfo pi = PageInfoProvider.GetPageInfo(CMSContext.CurrentSiteName, aliasPath, CMSContext.PreferredCultureCode, null, CMSContext.CurrentSite.CombineWithDefaultCulture, null);
            if (pi == null)
            {
                this.Visible = false;
                return;
            }

            PageTemplateInfo pti = pi.PageTemplateInfo;
            if (pti != null)
            {
                WebPartInfo wi = null;

                // Get web part
                WebPartInstance webPart = pti.GetWebPart(instanceGuid, webpartId);
                if (webPart != null)
                {
                    wi = WebPartInfoProvider.GetWebPartInfo(webPart.WebPartType);
                    if (ValidationHelper.GetString(webPart.GetValue("WebPartCode"), "").Trim() != "")
                    {
                        showCodeTab = true;
                    }
                    if (webPart.Bindings.Count > 0)
                    {
                        showBindingTab = true;
                    }
                }
                else
                {
                    wi = WebPartInfoProvider.GetWebPartInfo(ValidationHelper.GetInteger(webpartId, 0));
                }

                if (wi != null)
                {
                    // Generate documentation link
                    Literal ltr = new Literal();
                    PageTitle.RightPlaceHolder.Controls.Add(ltr);

                    string docScript = "NewWindow('" + ResolveUrl("~/CMSModules/PortalEngine/UI/WebParts/WebPartDocumentationPage.aspx") + "?webpartid=" + wi.WebPartName + "', 'WebPartPropertiesDocumentation', 800, 800); return false;";

                    ltr.Text = "<table cellpadding=\"0\" cellspacing=\"0\"><tr><td>";
                    ltr.Text += "<a onclick=\"" + docScript + "\" href=\"#\"><img src=\"" + ResolveUrl(GetImageUrl("CMSModules/CMS_PortalEngine/Documentation.png")) + "\" style=\"border-width: 0px;\"></a>";
                    ltr.Text += "</td>";
                    ltr.Text += "<td>";
                    ltr.Text += "<a onclick=\"" + docScript + "\" href=\"#\">" + ResHelper.GetString("WebPartPropertie.DocumentationLink") + "</a>";
                    ltr.Text += "</td></tr></table>";

                    PageTitle.TitleText = ResHelper.GetString("WebpartProperties.Title") + " (" + wi.WebPartDisplayName + ")";
                }

            }
        }

        CurrentUserInfo currentUser = CMSContext.CurrentUser;

        tabsElem.UrlTarget = "webpartpropertiescontent";
    }


    protected string[] tabElem_OnTabCreated(CMS.SiteProvider.UIElementInfo element, string[] parameters, int tabIndex)
    {
        switch (element.ElementName.ToLower())
        {
            case "webpartproperties.code":
                if (!showCodeTab || isNew)
                {
                    return null;
                }
                break;

            case "webpartproperties.bindings":
                if (!showBindingTab || isNew)
                {
                    return null;
                }
                break;

            case "webpartproperties.layout":
                if (isNew)
                {
                    return null;
                }
                break;
        }

        return parameters;
    }
}
