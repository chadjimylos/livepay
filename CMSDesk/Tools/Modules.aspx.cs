using System;
using System.Collections;
using System.Data;
using System.IO;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.SiteProvider;
using CMS.CMSHelper;

public partial class CMSDesk_Tools_Modules : CMSToolsPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize page
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Header.Tools");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Tools/module.png");

        this.guide.OnGuideItemCreated += new CMSAdminControls_UI_UIProfiles_UIGuide.GuideItemCreatedEventHandler(guide_OnGuideItemCreated);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // No modules allowed
        if (this.guide.GuideEmpty)
        {
            RedirectToUINotAvailable();
        }
    }


    object[] guide_OnGuideItemCreated(UIElementInfo uiElement, object[] defaultItem)
    {
        if (!IsToolsUIElementAvailable(uiElement.ElementName))
        {
            return null;
        }
        return defaultItem;
    }
}
