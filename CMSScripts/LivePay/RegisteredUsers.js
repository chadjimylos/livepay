﻿



Array.prototype.findIndex = function (predicate) { var position = -1; for (var i = 0; i < this.length; i++) { if (predicate(this[i], i)) { position = i; break; } } return position; }
Array.prototype.find = function (predicate) { var position = this.findIndex(predicate); return (position > -1 ? this[position] : null); }

var Tab1CloudCustCodeTop = 0;
var Tab1CloudCustCodeLeft = 0;
function GetAttributes() {

}

function ChangeRadioChoice(ObjToCheck, ObjToUncheck) {
    $('#' + ObjToCheck).attr('checked', true);
    $('#' + ObjToUncheck).attr('checked', false);
    //document.getElementById(ObjToCheck).checked = true;
    //document.getElementById(ObjToUncheck).checked = false;
    
    CloseAllClouds()



    if (ObjToCheck.indexOf('Tab2_RbNewCard') != -1) {
        document.getElementById(HiddenChoice).value = '2'
        
        NewCard_CheckValidCard(false, true)
        $('[id$=pnlNewCard]').show();
    }
    else {
        document.getElementById(HiddenChoice).value = '1'

        if (mobileDivice.isMobile()) {
            CheckValidSaveCardMobile(1)
        } else {
            CheckValidCard(1)
        }
        
        $('[id$=pnlNewCard]').hide();
        //CloudInstallments();
    }

    PosBottom();
}


function validateControlsSaveMobileCardTabB(oSrc, args) {
    var bIsValid = false;
    var MyVal = args.Value

}

function validateControlsTabB(oSrc, args) {
      var bIsValid = true
      var MyVal = args.Value
      var ControlToValidate = '';
      if (document.all) {
          ControlToValidate = oSrc.getAttribute('ControlToValidate')
      } else {
          ControlToValidate = oSrc.getAttribute('ControlValidated')
      }
      var CheckForEmpty = true
      var IsNewCard = (document.getElementById(HiddenChoice).value == 2)

      //$("select[id$=DDL_Installments] > option").not('option:[value=-1]').not('option:[value=0]').remove();

      if ($('[id$=pnlInstallments]').css('display') != "none") {
          if (oSrc.getAttribute('Installments') == 'yes') {
              var Installments = $("select[id$=DDL_Installments] option:selected").val();
              if (Installments == "-1") {
                  bIsValid = false;
                  ValidationCloud('#CloudInstallmentsMsg', $('[id$=lbl_DDLInstallmentsErrorMessage]').html(), true, '')
              } else {
                  bIsValid = true;
                  ValidationCloud('#CloudInstallmentsMsg', '', false, '')
              }
          }
      }


      if (IsNewCard) { /// New Card Choice Selected

          if (oSrc.getAttribute('IsFriendlyName') == 'yes' && document.getElementById(chkCardForSave).checked == false) {
              CheckForEmpty = false;
              ValidationCloud('#NewCardFriendly', '', false,'')
          }
          

          if (CheckForEmpty && oSrc.getAttribute('IsSavedCards') != 'yes') {
              if (oSrc.getAttribute('IsInvoice') != 'yes') {
                
                  if (oSrc.getAttribute('isCVVChecked') == 'yes') {
                      if (MyVal.length == 0 || MyVal.length == '')
                          bIsValid = false
                  }

                  if (MyVal.length == 0) {
                      bIsValid = false
                  }

                  if (IsNumeric(MyVal) == false && oSrc.getAttribute('IsNumber') == 'yes') {
                      bIsValid = false
                  }

                  var MaxLenght = MyVal.length
                  if (oSrc.getAttribute('CheckLength') == 'yes' && MaxLenght != oSrc.getAttribute('LengthLimit')) {
                      bIsValid = false
                  }


                

                  if (oSrc.getAttribute('IsFullName') == 'yes') {
                      if (bIsValid == false) {
                          //ValidationCloud('#NewCardFullNameMsg', FullNameErrorMessage, true, '')
                          ValidationCloud('#NewCardFullNameMsg', $('[id$=lbl_FullNameErrorMessage]').html(), true, '')
                      } else {
                          ValidationCloud('#NewCardFullNameMsg', '', false, '')
                      }
                  }


                  if (oSrc.getAttribute('IsNewCardCVV') == 'yes') {
                      if (bIsValid == false) {
                          $('[id$=lbl_NewCardCVV2ErrorMessage]').html()
                          //ValidationCloud('#NewCardCVVMsg', NewCardCVV2ErrorMessage, true, '')
                          ValidationCloud('#NewCardCVVMsg', $('[id$=lbl_NewCardCVV2ErrorMessage]').html(), true, '')
                      } else {
                          ValidationCloud('#NewCardCVVMsg', '', false, '')
                      }
                  }


                  if (oSrc.getAttribute('IsFriendlyName') == 'yes') {
                      
                      if (bIsValid == false && document.getElementById(chkCardForSave).checked == true) {
                          ValidationCloud('#NewCardFriendly', NewCardFriendlyErrorMessage, true, '')
                      } else {
                          ValidationCloud('#NewCardFriendly', '', false, '')
                      }
                  }

                  if (oSrc.getAttribute('IsCardType') == 'yes') {
                    
                      if (bIsValid == false) {
                          //ValidationCloud('#CardTypeMsg', CardTypeTopErrorMessage, true, '')
                          ValidationCloud('#CardTypeMsg', $('[id$=lbl_CardTypeTopErrorMessage]').html(), true, '')
                      } else {
                          ValidationCloud('#CardTypeMsg', '', false, '')
                      }
                  }

                  if (oSrc.getAttribute('IsNewCardNo') == 'yes') {
                       //NewCard_CheckValidCard(true,false)
                  }


                
                 //--- UnRegistered  Users
                 if (IsDirectPage != 'True' && oSrc.getAttribute('IsUnRegField') == 'yes') {
                     bIsValid = true
                 }

                
                 if (IsDirectPage && oSrc.getAttribute('IsPhone') == 'yes') {
                     var phone = new RegExp(/^\d{10,15}$/);
                     var bIsValid = phone.test(MyVal);
                   
                     if (bIsValid) {
                         ValidationCloud('#UnRegUserPhoneMsg', '', false, '')
                     } else {
                         ValidationCloud('#UnRegUserPhoneMsg', Tab2_PhoneErrorMessage, true, '')
                     }
                 }

                 if (IsDirectPage && oSrc.getAttribute('IsEmail') == 'yes') {

                     if (MyVal.length > 0) {
                         if (checkTheEmail(MyVal) == false) {
                             bIsValid = false
                         }
                         if (bIsValid == false) {
                             ValidationCloud('#UnRegUserEmailMsg', Tab2_EmailErrorMessage, true, '')
                         } else {
                             ValidationCloud('#UnRegUserEmailMsg', '', false, '')
                         }
                     } else {
                         bIsValid =true
                     }
                 }
                 
                //--- 
                  
              } else { //----------------- Invoice Form


      }

  }



     } else {

         if (oSrc.getAttribute('IsSavedCards') == 'yes') {
             
             //PleaseWaitDiv()
              if (MyVal.length == 0)
                  bIsValid = false

              if (IsNumeric(MyVal) == false && oSrc.getAttribute('IsNumber') == 'yes')
                  bIsValid = false

              var MaxLenght = MyVal.length
              if (oSrc.getAttribute('CheckLength') == 'yes' && MaxLenght != oSrc.getAttribute('LengthLimit'))
                  bIsValid = false

              if (oSrc.getAttribute('IsNumber') == 'yes') {
                  if (bIsValid == false) {
                      ValidationCloudTop('#SavedCardCVV2Msg', SavedCardCVV2ErrorMessage, true)
                  } else {
                      ValidationCloudTop('#SavedCardCVV2Msg', '', false)
                  }
              }
              if (oSrc.getAttribute('isDDLSavedCards') == 'yes') {
                  if (MyVal.length == 0) {
                      ValidationCloudTop('#SavedCardMsgSec', SaveCardTopErrorMessage, true)
                  } else {
                      if (document.getElementById(NewCard_NotValidCard).value == 'ok' && document.getElementById(NewCard_NotValidMerchant).value == 'ok'){
                            ValidationCloudTop('#SavedCardMsgSec', '', false)
                          }
                  }
                }

               // if (bIsValid == false)
                    //PleaseWaitDivClose()
               
            }

        }

        if (oSrc.getAttribute('IsSavedMobileCards') == 'yes') {
            if (oSrc.getAttribute('IsSaveCardMobileCVV') == 'yes') {
                if (MyVal.length == 3) {
                    ValidationCloud('#SaveCardMobileCVVMsg', '', false, '');
                } else {
                    ValidationCloud('#SaveCardMobileCVVMsg', $('[id$=lbl_NewCardCVV2ErrorMessage]').html(), true, '');
                    bIsValid = false;
                }
            }
        }

        //alert('check bIsValid SaveMobileCard -> ' + bIsValid)

        //--- INVOICE CHECK

        if ((!transPublic) && oSrc.getAttribute('IsInvoice') == 'yes') {
            if (document.getElementById(Tab2_ChlIWantInvoice).checked) {

                if (MyVal.length == 0) {
                    bIsValid = false
                }

                if (IsNumeric(MyVal) == false && oSrc.getAttribute('IsNumber') == 'yes') {
                    bIsValid = false
                }

                var MaxLenght = MyVal.length
                if (oSrc.getAttribute('CheckLength') == 'yes' && MaxLenght != oSrc.getAttribute('LengthLimit')) {

                    bIsValid = false
                }


                if (oSrc.getAttribute('IsInv_CompanyName') == 'yes') {
                    if (bIsValid == false) {
                        ValidationCloud('#Inv_CompanyNameMsg', Inv_CompanyNameErrorMessage, true, '')
                    } else {
                        ValidationCloud('#Inv_CompanyNameMsg', '', false, '')
                    }
                }


                if (oSrc.getAttribute('IsInv_Occupation') == 'yes') {
                    if (bIsValid == false) {
                        ValidationCloud('#Inv_OccupationMsg', Inv_OccupationErrorMessage, true, '')
                    } else {
                        ValidationCloud('#Inv_OccupationMsg', '', false, '')
                    }
                }


                if (oSrc.getAttribute('IsInv_Address') == 'yes') {
                    if (bIsValid == false) {
                        ValidationCloud('#Inv_AddressMsg', Inv_AddressErrorMessage, true, '')
                    } else {
                        ValidationCloud('#Inv_AddressMsg', '', false, '')
                    }
                }


                if (oSrc.getAttribute('IsInv_TK') == 'yes') {

                    if (bIsValid == false) {
                        ValidationCloud('#Inv_TKMsg', Inv_TKErrorMessage, true, '')
                    } else {
                        ValidationCloud('#Inv_TKMsg', '', false, '')
                    }
                }


                if (oSrc.getAttribute('IsInv_City') == 'yes') {
                    if (bIsValid == false) {
                        ValidationCloud('#Inv_CityMsg', Inv_CityErrorMessage, true, '')
                    } else {
                        ValidationCloud('#Inv_CityMsg', '', false, '')
                    }
                }


                if (oSrc.getAttribute('IsInv_AFM') == 'yes') {
                    if (checkAFM(MyVal) == false) {
                        bIsValid = false;
                    }

                    if (bIsValid == false) {
                        ValidationCloud('#Inv_AFMMsg', Inv_AFMErrorMessage, true, '')
                    } else {
                        ValidationCloud('#Inv_AFMMsg', '', false, '')
                    }
                }

                if (oSrc.getAttribute('IsInv_DOYMsg') == 'yes') {
                    if (bIsValid == false) {
                        ValidationCloud('#Inv_DOYMsg', Inv_DOYErrorMessage, true, '')
                    } else {
                        ValidationCloud('#Inv_DOYMsg', '', false, '')
                    }
                }

            }
        }
        args.IsValid = bIsValid;
  }

  function checkTheEmail(MailValue) {
      var filter = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
      if (!filter.test(MailValue))
          return false
      else
          return true
  }

  function GetSelectedCardType(str, sep,no) {
      var CardTypeId = 0
      var col_array = str.split(sep);
      var part_num = 0;
      while (part_num < col_array.length) {
          if (part_num == no)
              CardTypeId = col_array[part_num]
          part_num += 1;
      }
      return CardTypeId
  }

  function ShowHideTabs(TabA, TabB, TaBC) {
     
      document.getElementById('Tab1').style.display = TabA;
      document.getElementById('Tab2').style.display = TabB;
      document.getElementById('Tab3').style.display = TaBC;
      CloseAllClouds();
      if (TabB=='')
          CheckCheckboxes()

      PleaseWaitDivClose();

      if (mobileDivice.isMobile()) {
          mobileInit();
      }
      PosBottom();
  }

  function CheckCheckboxes() {
      if (!transPublic) {
          ShowHideInvoice(document.getElementById(Tab2_ChlIWantInvoice));
      }
      if (!mobileDivice.isMobile()) {
          CardForSave(document.getElementById(chkCardForSave)); 
      }
  }


  function CardForSave(obj) {
      var objFriendlyName = document.getElementById('friendlyNameMainDiv')
      
      if (obj.checked) {
          objFriendlyName.style.display = ''
          if (document.getElementById(HiddenChoice).value == 2 && document.getElementById(Tab2_txtFriendName).value.length == 0) {
              ValidationCloud('#NewCardFriendly', NewCardFriendlyErrorMessage, true, '')
          } else {
              ValidationCloud('#NewCardFriendly', '', false, '')
          }
      }
      else {
          objFriendlyName.style.display = 'none'
          ValidationCloud('#NewCardFriendly', '', false, '')
      }
  }



  function HideClound(obj) {
      var Object = document.getElementById(obj)
      Object.style.top = Tab1CloudCustCodeTop + 'px'
      Object.style.left = Tab1CloudCustCodeLeft + 'px'
      Object.style.display = 'none'
      Tab1CloudCustCodeTop = 0;
      Tab1CloudCustCodeLeft = 0;

  }

  function ShowClound(obj, img) {
      var Object = document.getElementById(obj)
      Object.style.display = ''
      if (Tab1CloudCustCodeTop == 0)
          Tab1CloudCustCodeTop = Object.offsetTop
      if (Tab1CloudCustCodeLeft == 0)
          Tab1CloudCustCodeLeft = Object.offsetLeft

      var ExtraPx = 0;
      if (document.all) {
          ExtraPx = 10
      } else {
          ExtraPx = -10
      }

      Object.style.top = (Tab1CloudCustCodeTop - Object.offsetHeight + ExtraPx) + 'px'
      Object.style.left = (Tab1CloudCustCodeLeft - ExtraPx) + 'px'

  }

  function IsNumeric(sText) {
      var ValidChars = "0123456789";
      var IsNumber = true;
      var Char;
      for (i = 0; i < sText.length && IsNumber == true; i++) {
          Char = sText.charAt(i);
          if (ValidChars.indexOf(Char) == -1) {
              IsNumber = false;
          }
      }
      return IsNumber;
  }

 

  $(document).ready(function () {
      $('#SavedCardMsg').poshytip();
      $('#SavedCardMsgNewCard').poshytip();

  })


  function GetLimit(limits, No) {
      var LimitValue = '';
      var col_array = limits.split('>');
      var part_num = 0;
      while (part_num < col_array.length) {
          if (part_num == No)
              LimitValue = col_array[part_num]
          part_num += 1;
      }

      LimitValue = LimitValue.replace("!", "")
      LimitValue = LimitValue.replace("@", "")
      return LimitValue
  }


  function ShowHideInvoice(chb) {
      if (chb.checked) {
          document.getElementById('InvoiceDiv').style.display = ''
          document.getElementById('Tab2_BtnNextDiv').style.display = 'none'
      } else {
          document.getElementById('InvoiceDiv').style.display = 'none'
          document.getElementById('Tab2_BtnNextDiv').style.display = ''
      }
      CloseAllClouds();
      PosBottom();
  }


  function PleaseWait(objId, Show) {
      if (Show) {
          document.getElementById(objId).className = 'PleaseWaitShow'
      } else {
          document.getElementById(objId).className = 'PleaseWait'
      }
  }



 


  function ClearCharcters(obj, BlurType) {
      var Nums = '1234567890'
      var Val = obj.value
      var part_num = 0;
      while (part_num < Val.length) {
          if (Nums.indexOf(Val.charAt(part_num)) == -1) {
              
              var r = new RegExp(Val.charAt(part_num), "gi");
              Val = Val.replace(r, "");
          }
          part_num += 1;
      }
      obj.value = Val
      if (Val.length > 0) {
          if (isNaN(Val)) {
              ClearCharcters(obj, BlurType)
          }
      }

      if (BlurType)
          NewCard_CheckValidCard(true,false)
  }


 


   function Left(str, n) {
       if (n <= 0)
           return "";
       else if (n > String(str).length)
           return str;
       else
           return String(str).substring(0, n);
   }



   //---- Price Validations and Functions
   function Tab1_Price_Validation(obj) {
       var MyVal = obj.value
       var objIsValid = true
       var objPriceFinalError = ''
       if (MyVal.length == 0) {
           objPriceFinalError = Tab1_PriceErrorMessage
           objIsValid = false
       }
       if (objIsValid) {
           if (IsMoney(MyVal) == false) {
               objPriceFinalError = Tab1_PriceErrorMessage
               objIsValid = false
           }
       }
       if (objIsValid) {
           var MinPrice = document.getElementById(MinimumTransactionAmount).value
           var MaxPrice = document.getElementById(MaximumTransactionAmount).value
           if (parseFloat(MyVal) < parseFloat(MinPrice)) {
               objPriceFinalError = Tab1_PriceErrorMessageLimit
               objIsValid = false
           }
           if (parseFloat(MyVal) > parseFloat(MaxPrice)) {
               objPriceFinalError = Tab1_PriceErrorMessageLimit
               objIsValid = false
           }
       }
       if (objIsValid == false) {
           Tab1_ValidationCloud('#Tab1_PriceMsg', objPriceFinalError, true, '-75')
       } else {
           Tab1_ValidationCloud('#Tab1_PriceMsg', '', false, '')
       }
   }


   function FixMoney(obj, Blur,WithValid) {

   //--- Replace All NoneNumeric Characters
       var Nums = '1234567890.'
       var Val = obj.value
       var part_num = 0;
       while (part_num < Val.length) {
           if (Nums.indexOf(Val.charAt(part_num)) == -1) {
               if (Val.charAt(part_num) == ',') {
                   Val = Val.replace(Val.charAt(part_num), ".")
               } else {
                   Val = Val.replace(Val.charAt(part_num), "")
               }
           }
           part_num += 1;
       }

  //-- Keep Only the Last Dot
       var DotLength = Val.split(".").length - 1;
       if (DotLength > 1) {
           var part_num2 = 0;
           var col_array = Val.split(".")
           var SearchSymbol = '.'
           var FinalVal = Val
           var Counter = 0
           while (part_num2 < col_array.length) {
               Counter = Counter + 1
               if ((Counter) < (col_array.length - 1)) {
                   FinalVal = FinalVal.replace(".", "")
               }
               part_num2 += 1;
           }
           Val = FinalVal
       }
       if (DotLength > 0) {
           var ValueNow = Val.split(".")[1]
           if (ValueNow.length > 2) {      // -- if length Value after Dot is more than 2 clear the others
               Val = Val.replace(("." + ValueNow), "." + Left(ValueNow, 2))
           }
           var ValBeforeDot = Val.split(".")[0]
           if (ValBeforeDot.length == 0) {      // -- if length Value after Dot is more than 2 clear the others
               Val = Val.replace(".", "")
           }
       }

       //-- Running only on Blur
       if (Blur) {
           if (DotLength > 0) { 
               var ValueNow = Val.split(".")[1]
               if (ValueNow == '') {           // -- Clear Dot if after Dot is nothing
                   Val = Val.replace(".", "")
               }
           }

           //--- If there is not dot put '.00'
           DotLength = Val.split(".").length - 1;
           if (DotLength == 0 && Val!='') {
               Val = Val + '.00'
           }
           if (DotLength > 0) { // -- IF after Dot is only one number put at the end one '0'
               var ValueNowAfterDot = Val.split(".")[1]
               if (ValueNow.length < 2) {
                   Val = Val + '0'
               }
           }


           if (WithValid) {
               Tab1_Price_Validation(obj) // -- Call Custom Validation (only for textbox Price)
           }
       }
       obj.value = Val
   }




   function IsMoney(sText) {
       var ValidChars = "0123456789.";
       var IsNumber = true;
       var Char;
       for (i = 0; i < sText.length && IsNumber == true; i++) {
           Char = sText.charAt(i);
           if (ValidChars.indexOf(Char) == -1) {
               IsNumber = false;
           }
       }
       return IsNumber;
   }



   function checkAFM(objVal) {
       var afm = objVal;
       var IsValidAfm = true
       if (!afm.match(/^\d{9}$/)) {
           IsValidAfm = false;
           return false;
       }

       afm = afm.split('').reverse().join('');

       var Num1 = 0;
       for (var iDigit = 1; iDigit <= 8; iDigit++) {
           Num1 += afm.charAt(iDigit) << iDigit;
       }

       if ((Num1 % 11) % 10 == afm.charAt(0)) {
           IsValidAfm = true;
           return true;
       }
       else {
           IsValidAfm = false;
           return false;
       }
   }

   //Tab1_PriceMsg
   function validateControls(oSrc, args) {
       PleaseWaitDiv(); 
       var PriceFinalError = ''
       var ControlToValidate = '';
       if (document.all) {
           ControlToValidate = oSrc.getAttribute('ControlToValidate')

       } else {
           ControlToValidate = oSrc.getAttribute('ControlValidated')
       }
       var MyVal = args.Value
       var bIsValid = true
       if (MyVal.length == 0) {
           PriceFinalError = Tab1_PriceErrorMessage
           bIsValid = false
       }
       if (oSrc.getAttribute('IsNumber') == 'yes' && bIsValid) {
           if (IsMoney(MyVal) == false) {
               PriceFinalError = Tab1_PriceErrorMessage
               bIsValid = false
           }
       }

       if (oSrc.getAttribute('IsNumber') == 'yes' && bIsValid) {
           var MinPrice = document.getElementById(MinimumTransactionAmount).value
           var MaxPrice = document.getElementById(MaximumTransactionAmount).value


           if (parseFloat(MyVal) < parseFloat(MinPrice) && MinPrice != 0) {
               PriceFinalError = Tab1_PriceErrorMessageLimit
               bIsValid = false
           }
           if (parseFloat(MyVal) > parseFloat(MaxPrice) && MaxPrice != 0) {
               PriceFinalError = Tab1_PriceErrorMessageLimit
               bIsValid = false
           }
       }
       if (oSrc.getAttribute('IsNumber') == 'yes') {
           if (bIsValid == false) {
              
               Tab1_ValidationCloud('#Tab1_PriceMsg', PriceFinalError, true, '-75')
           } else {
               Tab1_ValidationCloud('#Tab1_PriceMsg', '', false, '')
           }
       }

       var IsCutomValid = oSrc.getAttribute('IsCustomValidator')
       var CustomErrorMess = oSrc.getAttribute('CustomValidatorErrorMessage')
       if (IsCutomValid != null) {
           if (IsCutomValid.indexOf('CustomCloud_') != -1) {
             if (bIsValid == false) {
                 Tab1_ValidationCloud('#' + IsCutomValid, CustomErrorMess, true, '')
           } else {
                 Tab1_ValidationCloud('#' + IsCutomValid, '', false, '')
           }
           }
           
       }

       if (bIsValid) {
       } else {
           PleaseWaitDivClose()
       }
       args.IsValid = bIsValid;
   }

   function CreateTheCloud_SavedCard(CloudID, CloudContent, CardNotValid, ForValidation, NewCardForm) {
      
       if (is_chrome) {
           $(CloudID).css('position', 'absolute');
       }
       

          if (CloudID == '#SavedCardMsg') {
              $('#SavedCardMsgSec').poshytip('hide');
          }
          $(CloudID).poshytip('hide');
          if (NewCardForm) {

          } else {
              if (CardNotValid) {
                  document.getElementById(NotValidCard).value = '';
                  document.getElementById(NotValidMerchant).value = 'ok';
              } else {
                  document.getElementById(NotValidCard).value = 'ok';
                  document.getElementById(NotValidMerchant).value = '';
              }
          }
       
          if (ForValidation) {
              $(CloudID).poshytip({
                  className: 'tip-livepay',
                  content: CloudContent,
                  showOn: 'none',
                  alignTo: 'target',
                  alignX: 'inner-left',
                  offsetX: 218,
                  offsetY: 5, hideAniDuration: false
              });
          } else {
              $(CloudID).poshytip({
                  className: 'tip-livepay',
                  content: CloudContent,
                  showOn: 'none',
                  alignTo: 'target',
                  alignX: 'inner-left',
                  offsetX: 1,
                  offsetY: 0, hideAniDuration: false
              });
          }

          $(CloudID).poshytip('show');

   }


   function CreateTheCloud(CloudID, CloudContent, CardNotValid, ForValidation, NewCardForm, YposCustom) {
       var IsNotMobile = CheckIfIsnotMobile() ;
    
           if (CloudID == '#SavedCardMsg') {
               $('#SavedCardMsgSec').poshytip('hide');
           }
           $(CloudID).poshytip('hide');
           if (NewCardForm) {

           } else {
               if (CardNotValid) {
                   document.getElementById(NotValidCard).value = '';
                   document.getElementById(NotValidMerchant).value = 'ok';
               } else {
                   document.getElementById(NotValidCard).value = 'ok';
                   document.getElementById(NotValidMerchant).value = '';
               }
           }

           var YPos = -25;
           var Xpos = 270
           if (is_chrome) {
               $(CloudID).css('position', 'absolute')
               //YPos = -37
               Xpos = 286
               if (YposCustom != '' && YposCustom.length > 0) {
                   YPos = YposCustom;
                   Xpos = 282
               }
           }

           if (is_firefox) {
               YPos = -10
               if (YposCustom != '' && YposCustom.length > 0) {
                   YPos = -10
               }
           }

           if (IsNotMobile) {

               if (ForValidation) {
                   $(CloudID).poshytip({
                       className: 'tip-livepay',
                       content: CloudContent,
                       showOn: 'none',
                       alignTo: 'target',
                       offsetX: Xpos,
                       alignX: 'right',
                       offsetY: YPos, hideAniDuration: false
                   });
               } else {
                   $(CloudID).poshytip({
                       className: 'tip-livepay',
                       content: CloudContent,
                       showOn: 'none',
                       alignTo: 'target',
                       alignX: 'inner-left',
                       offsetX: 1,
                       offsetY: 0, hideAniDuration: false
                   });
               }

               $(CloudID).poshytip('show');
       } else {
               $(CloudID + '_Mob').hide()
         
           if (CloudContent != '') {
              $(CloudID + '_Mob').show()
              $(CloudID + '_Mob').html(CloudContent)
           }
           
            
       }

     
   }

   function Tab1_ValidationCloud(CloudID, CloudContent, show, YposCustom) {
      
        var IsNotMobile = CheckIfIsnotMobile()
        if (IsNotMobile) {
         
            var YPos = -25;
            var Xpos = 268
            if (is_chrome) {
                $(CloudID).css('position', 'absolute');
              
                YPos = -27
               
            }

            if (is_firefox) {
                YPos = -25
                if (YposCustom != '' && YposCustom.length > 0) {
                    YPos = -10
                }
            }
          
            //$(CloudID).each(function () {
            //$(this).poshytip('hide');
            //});

            $(CloudID).poshytip('hide');
         
            $(CloudID).poshytip({
                className: 'tip-livepay',
                content: CloudContent,
                showOn: 'none',
                alignTo: 'target',
                alignX: 'right',
                offsetX: Xpos,
                offsetY: YPos, hideAniDuration: false
            });
            $(CloudID).poshytip('hide');
            if (show)
                $(CloudID).poshytip('show');


        } else {
           
            CloudID = CloudID + '_Mob'

            $(CloudID).hide()

            if (show) {
                $(CloudID).show()
                $(CloudID).html(CloudContent)
            }
            
            
        }
   }



   function ValidationCloud(CloudID, CloudContent, Show, YposCustom) {
       var IsNotMobile = CheckIfIsnotMobile();
     
       if (IsNotMobile) {
          
           var YPos = -25;
           var Xpos = 270
           if (is_chrome) {
               $(CloudID).css('position', 'absolute')
              // YPos = -37
               Xpos = 286
//               if (YposCustom != '' && YposCustom.length > 0) {
//                   YPos = YposCustom;
//                   Xpos = 282
//               }
           }

           if (is_firefox) {
               YPos = -10
               if (YposCustom != '' && YposCustom.length > 0) {
                   YPos = -10
               }

           }
           $(CloudID).poshytip('hide');
           $(CloudID).poshytip({
               className: 'tip-livepay',
               content: CloudContent,
               showOn: 'none',
               alignTo: 'target',
               alignX: 'right',
               offsetX: Xpos,
               offsetY: YPos, hideAniDuration: false
           });

           $(CloudID).poshytip('hide');
           if (Show)
               $(CloudID).poshytip('show');

       } else {
          
           $(CloudID + '_Mob').hide()
           if (Show) {
               $(CloudID + '_Mob').show();
               $(CloudID + '_Mob').html(CloudContent);

           }
          

       }
   }

   function ValidationCloudTop(CloudID, CloudContent, Show) {
   
       if (is_chrome) {
           $(CloudID).css('position', 'absolute');
       }
       
             $(CloudID).poshytip('hide');

             $(CloudID).poshytip({
                 className: 'tip-livepay',
                 content: CloudContent,
                 showOn: 'none',
                 alignTo: 'target',
                 alignX: 'inner-left',
                 offsetX: 28,
                 offsetY: 0, hideAniDuration: false
             });
             $(CloudID).poshytip('hide');
             if (Show)
                 $(CloudID).poshytip('show');
        
   }

   function padWithZero(thisObj) {
       var current = thisObj.value.replace(/[^0-9]/g, "").replace(/^0*/g, "");
       var isLength = thisObj.maxLength-1;
       var zero = "";

       for (i = 0; i < isLength; i++)
           zero += "0";

       current = zero + current;

       thisObj.value = current.substr(current.length - isLength, isLength);
   }

   function modulus11(value, length) {
       var g = 0; //Ginomeno
       var cd = 0; //Control Digit
       var i = 0;
       i = parseInt(length)-1;

       for (var s = 2; s < length; s++) {
           g += (i * value.charAt(s - 2));
           i--;
       }

       var y = 0; //Ypoloipo
       y = g % 11;

       if (y == 0) { cd = 1; }
       else if (y == 1) { cd = 0; } 
       else { cd = 11 - y;}

       return cd;
   }

   function Left(str, n) {
       if (n <= 0)
           return "";
       else if (n > String(str).length)
           return str;
       else
           return String(str).substring(0, n);
   }

   function Right(str, n) {
       if (n <= 0)
           return "";
       else if (n > String(str).length)
           return str;
       else {
           var iLen = String(str).length;
           return String(str).substring(iLen, iLen - n);
       }
   }



