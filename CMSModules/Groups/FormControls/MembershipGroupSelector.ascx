<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MembershipGroupSelector.ascx.cs"
    Inherits="CMSModules_Groups_FormControls_MembershipGroupSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ObjectType="community.group" SelectionMode="MultipleTextBox" ReturnColumnName="GroupName"
            OrderBy="GroupDisplayName" ResourcePrefix="groups" runat="server" AllowEmpty="false"
            ID="usGroups" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
