<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaxClass_Frameset.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Configuration_TaxClasses_TaxClass_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" enableviewstate="false">
    <title>Tax class - Edit</title>
</head>
	<frameset border="0" rows="102, *" id="frameTaxClass" runat="server">
		<frame name="taxClassHeader" src="TaxClass_Header.aspx?taxclassid=<%=Request.QueryString["taxclassid"]%>&hidebreadcrumbs=<%=Request.QueryString["hidebreadcrumbs"]%>" scrolling="no" frameborder="0" noresize="noresize" />		
	    <frame name="taxClassContent" src="TaxClass_General.aspx?taxclassid=<%=Request.QueryString["taxclassid"]%>&saved=<%=Request.QueryString["saved"]%> " scrolling="auto" frameborder="0" noresize="noresize" />
	<noframes>
		<body>
		 <p id="p1">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		 </p>
		</body>
	</noframes>
</frameset>
</html>
