using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.FormControls;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Controls_UI_ProductOptions : CMSAdminControl
{
    #region "Private & protected variables"

    protected string currentValues = string.Empty;
    private int mProductId = 0;

    #endregion


    #region "Properties"

    /// <summary>
    /// Product ID.
    /// </summary>
    public int ProductID
    {
        get
        {
            return mProductId;
        }
        set
        {
            mProductId = value;
        }
    }


    /// <summary>
    /// Indicates whether form is enabled.
    /// </summary>
    public bool Enabled
    {
        get
        {
            return this.uniSelector.Enabled;
        }
        set
        {
            this.uniSelector.Enabled = value;
        }
    }


    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        lblAvailable.Text = ResHelper.GetString("com.sku.categoriesavailable");
        if (ProductID > 0)
        {
            // Get the active users
            DataSet ds = OptionCategoryInfoProvider.GetSKUOptionCategories(ProductID, null);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "CategoryID"));
            }

            if (!IsPostBack)
            {
                this.uniSelector.Value = currentValues;
            }
        }

        this.uniSelector.IconPath = GetObjectIconUrl("ecommerce.optioncategory", "object.png");
        this.uniSelector.OnSelectionChanged += uniSelector_OnSelectionChanged;
    }


    protected void uniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        SaveItems();
    }


    protected void SaveItems()
    {
        // Remove old items
        string newValues = ValidationHelper.GetString(uniSelector.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to user
                foreach (string item in newItems)
                {
                    int categoryId = ValidationHelper.GetInteger(item, 0);
                    OptionCategoryInfoProvider.RemoveOptionCategoryFromSKU(categoryId, ProductID);
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to user
                foreach (string item in newItems)
                {
                    int categoryId = ValidationHelper.GetInteger(item, 0);
                    OptionCategoryInfoProvider.AddOptionCategoryToSKU(categoryId, ProductID);
                }
            }
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}
