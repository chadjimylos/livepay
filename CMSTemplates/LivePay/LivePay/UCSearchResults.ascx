﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSearchResults.ascx.vb" Inherits="CMSTemplates_LivePay_UCSearchResults" %>
<asp:UpdatePanel ID="SrcUpdatePanel" runat="server" UpdateMode="Always">
<ContentTemplate >
 <asp:GridView ID="GVMerchants" GridLines="None" ShowHeader="false" runat="server" PageSize="20" AutoGenerateColumns="false" AllowPaging="true" Width="690" >
  <Columns >
    <asp:TemplateField    >
        <ItemTemplate>
            <div class="SeachResults"><div><a href="<%# CMS.CMSHelper.CMSContext.GetUrl(Eval("nodealiasPath")) %>"><%# Eval("DiscreetTitle")%></a></div></div>
        </ItemTemplate>
    </asp:TemplateField>
  </Columns>
   <PagerTemplate >
    <div class="SrcHisPayPagerSpace" >
        <div>
            <div class="GridPagerNoLinkDivLeftBefore"><asp:linkbutton ID="lnkPrev" CssClass="PagerText" onclick="ChangePageByLinkNumber_Before" runat="server"><%= ResHelper.LocalizeString("{$=Προηγούμενη|en-us=Προηγούμενη$}")%></asp:linkbutton>&nbsp;</div>
            <div class="GridPagerNoLinkDivLeftFirst"><asp:linkbutton ID="lnkFirstPage" CssClass="PagerText" onclick="ChangePageByLinkNumber_First" runat="server"><%= ResHelper.LocalizeString("{$=Πρωτη|en-us=Πρωτη$}")%></asp:linkbutton>&nbsp;</div>
            <div class="GridPagerNoLinkDivCenterSrc" id="lblPageIndex" runat="server">&nbsp;</div>
            <div class="GridPagerNoLinkDivRightLast"><asp:linkbutton ID="lnkLastPage" CssClass="PagerText" onclick="ChangePageByLinkNumber_Last" runat="server"><%= ResHelper.LocalizeString("{$=Τελευταία|en-us=Τελευταία$}")%></asp:linkbutton>&nbsp;</div>
            <div class="GridPagerNoLinkDivRightNext">&nbsp;<asp:linkbutton ID="lnkNext" CssClass="PagerText"  onclick="ChangePageByLinkNumber_Next" runat="server"><%=ResHelper.LocalizeString("{$=Επόμενη|en-us=Επόμενη$}") %></asp:linkbutton></div>
       </div>
      </div>
    </PagerTemplate>
  </asp:GridView> 
   <asp:LinkButton ID="HiddenPageBtn" style="display:none" runat="server" Text="0"></asp:LinkButton>
    <asp:TextBox ID="HiddenPage"  style="display:none" runat="server" Text="1"></asp:TextBox>
    <asp:TextBox ID="HiddenSearchType"  style="display:none" runat="server" Text="0"></asp:TextBox>
    <div id="BottomDiv" style="display:none" runat="server" class="SrcHisPayGridBGBottom"></div> 
</ContentTemplate>
</asp:UpdatePanel>

    <script language="javascript" type="text/javascript" >
     function GoToPage(no) {
         document.getElementById('<%=HiddenPage.ClientID %>').value = no;
         document.getElementById('<%=HiddenPageBtn.ClientID %>').innerHTML = no;
         var GetID = '<%=HiddenPageBtn.ClientID %>'
         GetID = GetID.replace(/[_]/gi, "$")
         __doPostBack(GetID, '')
     }
 </script>