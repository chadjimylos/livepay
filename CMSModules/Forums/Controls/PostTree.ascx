<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PostTree.ascx.cs" Inherits="CMSModules_Forums_Controls_PostTree" %>
<%@ Register Src="~/CMSModules/Forums/Controls/Posts/ForumPost.ascx" TagName="ForumPost" TagPrefix="uc1" %>
<div class="PostTree">
    <asp:TreeView ID="treeElem" runat="server" ShowLines="True" CssClass="ForumTree" OnTreeNodePopulate="treeElem_TreeNodePopulate" />
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
</div>
