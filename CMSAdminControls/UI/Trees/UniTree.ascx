<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UniTree.ascx.cs" Inherits="CMSAdminControls_UI_Trees_UniTree" %>
<div id="<%= this.ClientID %>">
    <asp:Panel runat="server" ID="pnlMain" CssClass="TreeMain">
        <asp:Panel runat="server" ID="pnltreeTree" CssClass="TreeTree">
            <asp:TreeView runat="server" ID="treeElem" OnTreeNodePopulate="treeElem_TreeNodePopulate"
                NodeStyle-CssClass="UniTreeNode" ShowLines="true" />
            <asp:HiddenField ID="hdnSelectedItem" runat="server" EnableViewState="true" />
            <cms:CMSUpdatePanel ID="pnlUpdateSelected" runat="server">
                <ContentTemplate>
                    <cms:CMSButton ID="btnItemSelected" runat="server" EnableViewState="false" OnClick="btnItemSelected_Click" />
                </ContentTemplate>
            </cms:CMSUpdatePanel>
        </asp:Panel>
    </asp:Panel>
</div>
<asp:Literal runat="server" ID="ltlScript" EnableViewState="false"></asp:Literal>
