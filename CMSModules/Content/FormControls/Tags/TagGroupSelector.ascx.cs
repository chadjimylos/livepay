using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.SiteProvider;
using CMS.PortalControls;
using CMS.CMSHelper;

public partial class CMSModules_Content_FormControls_Tags_TagGroupSelector : FormEngineUserControl
{
    #region "Variables"

    private bool mUseDropdownList = false;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (this.UseGroupNameForSelection)
            {
                return this.TagGroupName;
            }
            else
            {
                return this.TagGroupID;
            }
        }
        set
        {
            if (this.UseGroupNameForSelection)
            {
                this.TagGroupName = ValidationHelper.GetString(value, "");
            }
            else
            {
                this.TagGroupID = ValidationHelper.GetInteger(value, 0);
            }
        }
    }


    /// <summary>
    /// Gets or sets the TagGroup ID.
    /// </summary>
    public int TagGroupID
    {
        get
        {
            if (this.UseGroupNameForSelection)
            {
                string name = ValidationHelper.GetString(this.uniSelector.Value, "");
                TagGroupInfo tgi = TagGroupInfoProvider.GetTagGroupInfo(name, CMSContext.CurrentSite.SiteID);
                if (tgi != null)
                {
                    return tgi.TagGroupID;
                }
                return 0;
            }
            else
            {
                return ValidationHelper.GetInteger(this.uniSelector.Value, 0);
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.UseGroupNameForSelection)
            {
                TagGroupInfo tgi = TagGroupInfoProvider.GetTagGroupInfo(value);
                if (tgi != null)
                {
                    this.uniSelector.Value = tgi.TagGroupName;
                }
            }
            else
            {
                this.uniSelector.Value = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets the TagGroup code name.
    /// </summary>
    public string TagGroupName
    {
        get
        {
            if (this.UseGroupNameForSelection)
            {
                return ValidationHelper.GetString(this.uniSelector.Value, "");
            }
            else
            {
                int id = ValidationHelper.GetInteger(this.uniSelector.Value, 0);
                TagGroupInfo tgi = TagGroupInfoProvider.GetTagGroupInfo(id);
                if (tgi != null)
                {
                    return tgi.TagGroupName;
                }
                return "";
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.UseGroupNameForSelection)
            {
                this.uniSelector.Value = value;
            }
            else
            {
                TagGroupInfo tgi = TagGroupInfoProvider.GetTagGroupInfo(value, CMSContext.CurrentSite.SiteID);
                if (tgi != null)
                {
                    this.uniSelector.Value = tgi.TagGroupID;
                }
            }
        }
    }


    /// <summary>
    ///  If true, selected value is TagGroupName, if false, selected value is TagGroupID
    /// </summary>
    public bool UseGroupNameForSelection
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("UseGroupNameForSelection"), true);
        }
        set
        {
            this.SetValue("UseGroupNameForSelection", value);
            if (this.uniSelector != null)
            {
                this.uniSelector.ReturnColumnName = (value ? "TagGroupName" : "TagGroupID");
            }
        }
    }


    /// <summary>
    /// Indicates whether to use dropdownlist or textbox selection mode.
    /// </summary>
    public bool UseDropdownList
    {
        get
        {
            return this.mUseDropdownList;
        }
        set
        {
            this.mUseDropdownList = value;

            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (value)
            {
                this.uniSelector.SelectionMode = SelectionModeEnum.SingleDropDownList;
            }
            else
            {
                this.uniSelector.SelectionMode = SelectionModeEnum.SingleTextBox;
            }
        }
    }


    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.uniSelector.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add none item record to the dropdownlist.
    /// </summary>
    public bool AddNoneItemsRecord
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("AddNoneItemsRecord"), true);
        }
        set
        {
            this.SetValue("AddNoneItemsRecord", value);
            if (this.uniSelector != null)
            {
                this.uniSelector.AllowEmpty = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether use autopostback or not.
    /// </summary>
    public bool UseAutoPostback
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("UseAutoPostback"), false);
        }
        set
        {
            this.SetValue("UseAutoPostback", value);
            if (this.uniSelector != null)
            {
                this.uniSelector.DropDownSingleSelect.AutoPostBack = value;
            }
        }
    }


    /// <summary>
    /// Gets the inner UniSelector control
    /// </summary>
    public UniSelector UniSelector
    {
        get
        {
            return uniSelector;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            this.uniSelector.DisplayNameFormat = "{%TagGroupDisplayName%}";
            if (this.UseDropdownList)
            {
                this.uniSelector.SelectionMode = SelectionModeEnum.SingleDropDownList;
            }
            else
            {
                this.uniSelector.SelectionMode = SelectionModeEnum.SingleTextBox;
            }
            this.uniSelector.AllowEditTextBox = true;
            this.uniSelector.WhereCondition = "TagGroupSiteID = " + CMSContext.CurrentSiteID;
            this.uniSelector.ReturnColumnName = (this.UseGroupNameForSelection ? "TagGroupName" : "TagGroupID");
            this.uniSelector.OrderBy = "TagGroupDisplayName";
            this.uniSelector.ObjectType = SiteObjectType.TAGGROUP;
            this.uniSelector.ResourcePrefix = "taggroupselect";
            this.uniSelector.DropDownSingleSelect.AutoPostBack = this.UseAutoPostback;
            this.uniSelector.AllowEmpty = this.AddNoneItemsRecord;
            this.uniSelector.IsLiveSite = this.IsLiveSite;

            if (this.UseGroupNameForSelection)
            {
                this.uniSelector.AllRecordValue = "";
                this.uniSelector.NoneRecordValue = "";
            }
        }
    }


    /// <summary>
    /// Loads public status according to the control settings.
    /// </summary>
    public void ReloadData()
    {
        this.uniSelector.Reload(true);
    }

}
