CREATE TABLE [CMS_WebFarmServerTask] (
		[ServerID]         int NOT NULL,
		[TaskID]           int NOT NULL,
		[ErrorMessage]     nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WebFarmServerTask]
	ADD
	CONSTRAINT [PK_CMS_WebFarmServerTask]
	PRIMARY KEY
	([ServerID], [TaskID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_WebFarmServerTask]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WebFarmServer]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WebFarmServerTask_ServerID_CMS_WebFarmServer]') IS NULL
BEGIN
		ALTER TABLE [CMS_WebFarmServerTask]
			ADD CONSTRAINT [FK_CMS_WebFarmServerTask_ServerID_CMS_WebFarmServer]
			FOREIGN KEY ([ServerID]) REFERENCES [CMS_WebFarmServer] ([ServerID])
END
IF OBJECT_ID(N'[CMS_WebFarmServerTask]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WebFarmTask]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WebFarmServerTask_TaskID_CMS_WebFarmTask]') IS NULL
BEGIN
		ALTER TABLE [CMS_WebFarmServerTask]
			ADD CONSTRAINT [FK_CMS_WebFarmServerTask_TaskID_CMS_WebFarmTask]
			FOREIGN KEY ([TaskID]) REFERENCES [CMS_WebFarmTask] ([TaskID])
END
