<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageLayout_Edit.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_PageLayouts_PageLayout_Edit" Theme="Default"
    ValidateRequest="false" EnableEventValidation="false" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Page Layout Edit" %>

<%@ Register Src="~/CMSAdminControls/MetaFiles/FileList.ascx" TagName="FileList"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/MetaFiles/File.ascx" TagName="File" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Panel ID="pnlCheckOutInfo" runat="server" CssClass="InfoLabel" Visible="false"
        EnableViewState="false">
        <asp:Label runat="server" ID="lblCheckOutInfo" />
        <br />
    </asp:Panel>
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="width: 100%">
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lbLayoutDisplayName" runat="server" EnableViewState="False" />
            </td>
            <td>
                <asp:TextBox ID="tbLayoutDisplayName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvLayoutDisplayName" runat="server" EnableViewState="false"
                    ControlToValidate="tbLayoutDisplayName" Display="dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbCodeName" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="tbCodeName" runat="server" CssClass="TextBoxField" /><br />
                <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="tbCodeName"
                    Display="Dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <asp:Label ID="lbLayoutDescription" runat="server" EnableViewState="False" />
            </td>
            <td>
                <asp:TextBox ID="tbLayoutDescription" runat="server" CssClass="TextAreaField" MaxLength="100"
                    TextMode="MultiLine"></asp:TextBox>
                <br />
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: middle">
                <asp:Label ID="lblUploadFile" runat="server" Visible="false" EnableViewState="false" />
            </td>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <cms:File ID="UploadFile" runat="server" Visible="false" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <asp:Label ID="lbLayoutCode" runat="server" EnableViewState="False" />
            </td>
            <td style="width: 85%">
                <asp:Literal ID="ltlHint" runat="server" EnableViewState="false" /><br />
                <br />
                <asp:Literal runat="server" ID="ltlDirectives" EnableViewState="false" /><br />
                <asp:PlaceHolder runat="server" ID="plcVirtualInfo" Visible="false">
                    <br />
                    <asp:Label runat="server" ID="lblVirtualInfo" CssClass="ErrorLabel" EnableViewState="false" />
                </asp:PlaceHolder>
                <cms:ExtendedTextArea ID="tbLayoutCode" runat="server" CssClass="TextAreaCode" TextMode="MultiLine"
                    Width="98%" /><br />
                <asp:RequiredFieldValidator ID="rfvLayoutCode" runat="server" ControlToValidate="tbLayoutCode"
                    Display="Dynamic" EnableViewState="False"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
</asp:Content>
