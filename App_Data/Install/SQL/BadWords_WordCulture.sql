CREATE TABLE [BadWords_WordCulture] (
		[WordID]        int NOT NULL,
		[CultureID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [BadWords_WordCulture]
	ADD
	CONSTRAINT [PK_BadWords_WordCulture]
	PRIMARY KEY
	([WordID], [CultureID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[BadWords_WordCulture]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Culture]') IS NOT NULL
	AND OBJECT_ID(N'[FK_BadWords_WordCulture_CultureID_CMS_Culture]') IS NULL
BEGIN
		ALTER TABLE [BadWords_WordCulture]
			ADD CONSTRAINT [FK_BadWords_WordCulture_CultureID_CMS_Culture]
			FOREIGN KEY ([CultureID]) REFERENCES [CMS_Culture] ([CultureID])
END
IF OBJECT_ID(N'[BadWords_WordCulture]') IS NOT NULL
	AND OBJECT_ID(N'[BadWords_Word]') IS NOT NULL
	AND OBJECT_ID(N'[FK_BadWords_WordCulture_WordID_BadWords_Word]') IS NULL
BEGIN
		ALTER TABLE [BadWords_WordCulture]
			ADD CONSTRAINT [FK_BadWords_WordCulture_WordID_BadWords_Word]
			FOREIGN KEY ([WordID]) REFERENCES [BadWords_Word] ([WordID])
END
