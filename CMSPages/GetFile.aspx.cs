using System;
using System.Web;
using System.IO;
using System.Web.Caching;

using CMS.FileManager;
using CMS.WorkflowEngine;
using CMS.TreeEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.PortalEngine;
using CMS.URLRewritingEngine;
using CMS.WebAnalytics;
using CMS.UIControls;

public partial class CMSPages_GetFile : GetFilePage
{
    #region "Advanced settings"

    /// <summary>
    /// Set to false to disable the client caching
    /// </summary>
    protected bool useClientCache = true;

    /// <summary>
    /// Set to 0 if you do not wish to cache large files
    /// </summary>
    protected int largeFilesCacheMinutes = 1;

    #endregion


    #region "Variables"

    protected CMSOutputFile outputFile = null;

    protected AttachmentManager mAttachmentManager = null;
    protected TreeProvider mTreeProvider = null;
    protected GeneralConnection mConnection = null;

    protected TreeNode node = null;
    protected PageInfo pi = null;

    protected int? mVersionHistoryID = null;
    protected bool mIsLatestVersion = false;
    protected bool? mIsLiveSite = null;
    protected Guid guid = Guid.Empty;
    protected string mCulture = null;
    protected Guid nodeGuid = Guid.Empty;

    protected string aliasPath = null;
    protected string fileName = null;

    protected int latestForDocumentId = 0;
    protected int latestForHistoryId = 0;

    protected bool allowLatestVersion = false;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets the language for current file
    /// </summary>
    public string CultureCode
    {
        get
        {
            if (mCulture == null)
            {
                string culture = QueryHelper.GetString(UrlHelper.LanguageParameterName, CMSContext.PreferredCultureCode);
                if (!CultureInfoProvider.IsCultureAllowed(culture, CurrentSiteName))
                {
                    culture = CMSContext.PreferredCultureCode;
                }
                mCulture = culture;
            }

            return mCulture;
        }
    }


    /// <summary>
    /// Attachment manager
    /// </summary>
    public AttachmentManager AttachmentManager
    {
        get
        {
            if (mAttachmentManager == null)
            {
                mAttachmentManager = new AttachmentManager(this.Connection);
            }

            return mAttachmentManager;
        }
    }


    /// <summary>
    /// Connection
    /// </summary>
    public GeneralConnection Connection
    {
        get
        {
            if (mConnection == null)
            {
                mConnection = ConnectionHelper.GetConnection();
            }

            return mConnection;
        }
    }


    /// <summary>
    /// Tree provider
    /// </summary>
    public TreeProvider TreeProvider
    {
        get
        {
            if (mTreeProvider == null)
            {
                mTreeProvider = new TreeProvider(null, this.Connection);
            }

            return mTreeProvider;
        }
    }


    /// <summary>
    /// Document version history ID
    /// </summary>
    public int VersionHistoryID
    {
        get
        {
            if (mVersionHistoryID == null)
            {
                mVersionHistoryID = QueryHelper.GetInteger("versionhistoryid", 0);
            }
            return mVersionHistoryID.Value;
        }
    }


    /// <summary>
    /// Indicates if the file is latest version or comes from version history
    /// </summary>
    public bool LatestVersion
    {
        get
        {
            return mIsLatestVersion || (VersionHistoryID > 0);
        }
    }


    /// <summary>
    /// Indicates if live site mode
    /// </summary>
    public bool IsLiveSite
    {
        get
        {
            if (mIsLiveSite == null)
            {
                mIsLiveSite = (this.ViewMode == ViewModeEnum.LiveSite);
            }
            return mIsLiveSite.Value;
        }
    }


    /// <summary>
    /// Returns true if the process allows cache
    /// </summary>
    public override bool AllowCache
    {
        get
        {
            if (mAllowCache == null)
            {
                // By default, cache for the files is disabled outside of the live site
                if (ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSAlwaysCacheFiles"], false))
                {
                    mAllowCache = true;
                }
                else
                {
                    mAllowCache = IsLiveSite;
                }
            }

            return mAllowCache.Value;
        }
        set
        {
            mAllowCache = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        DebugHelper.SetContext("GetFile");

        // Load the site name
        LoadSiteName();

        // Check the site
        if (CurrentSiteName == "")
        {
            throw new Exception("[GetFile.aspx]: Site not running.");
        }

        // Validate the culture
        PreferredCultureOnDemand culture = new PreferredCultureOnDemand();
        URLRewriter.ValidateCulture(CurrentSiteName, culture, null);

        // Set campaign
        if (IsLiveSite)
        {
            // Store campaign name if present
            string campaign = AnalyticsHelper.CurrentCampaign(CurrentSiteName);
            if (campaign != "")
            {
                // Log campaign
                if ((CMSContext.CurrentPageInfo != null) && AnalyticsHelper.SetCampaign(campaign, CurrentSiteName, CMSContext.CurrentPageInfo.NodeAliasPath))
                {
                    CMSContext.Campaign = campaign;
                }
            }
        }

        int cacheMinutes = this.CacheMinutes;

        // Try to get data from cache
        using (CachedSection<CMSOutputFile> cs = new CachedSection<CMSOutputFile>(ref outputFile, cacheMinutes, true, null, "getfile", this.CurrentSiteName, CMSContext.BaseCacheKey, Request.QueryString))
        {
            if (cs.LoadData)
            {
                // Process the file
                ProcessAttachment();

                if (cs.Cached)
                {
                    // Do not cache if too big file which would be stored in memory
                    if ((outputFile != null) &&
                        (outputFile.Attachment != null) &&
                        !CacheHelper.CacheImageAllowed(CurrentSiteName, outputFile.Attachment.AttachmentSize) &&
                        !AttachmentManager.StoreFilesInFileSystem(CurrentSiteName))
                    {
                        cacheMinutes = largeFilesCacheMinutes;
                    }

                    if (cacheMinutes > 0)
                    {
                        // Prepare the cache dependency
                        CacheDependency cd = null;
                        if (outputFile != null)
                        {
                            string[] dependencies = new string[] { 
                            "node|" + CurrentSiteName.ToLower() + "|" + outputFile.AliasPath.ToLower(), 
                            "" 
                        };

                            // Do not cache if too big file which would be stored in memory
                            if (outputFile.Attachment != null)
                            {
                                if (!CacheHelper.CacheImageAllowed(CurrentSiteName, outputFile.Attachment.AttachmentSize) && !AttachmentManager.StoreFilesInFileSystem(CurrentSiteName))
                                {
                                    cacheMinutes = largeFilesCacheMinutes;
                                }

                                dependencies[1] = "attachment|" + outputFile.Attachment.AttachmentGUID.ToString().ToLower();
                            }

                            cd = CacheHelper.GetCacheDependency(dependencies);
                        }

                        if (cd == null)
                        {
                            // Set default dependency
                            if (guid != Guid.Empty)
                            {
                                // By attachment GUID
                                cd = CacheHelper.GetCacheDependency(new string[] { "attachment|" + guid.ToString().ToLower() });
                            }
                            else if (nodeGuid != Guid.Empty)
                            {
                                // By node GUID
                                cd = CacheHelper.GetCacheDependency(new string[] { "nodeguid|" + CurrentSiteName.ToLower() + "|" + nodeGuid.ToString().ToLower() });
                            }
                            else if (aliasPath != null)
                            {
                                // By node alias path
                                cd = CacheHelper.GetCacheDependency(new string[] { "node|" + CurrentSiteName.ToLower() + "|" + aliasPath.ToLower() });
                            }
                        }

                        cs.CacheDependency = cd;
                    }

                    // Cache the data
                    cs.CacheMinutes = cacheMinutes;
                    cs.Data = outputFile;
                }
            }
        }

        // Do not cache images in the browser if cache is not allowed
        if (LatestVersion)
        {
            useClientCache = false;
        }

        // Send the data
        SendFile(outputFile);

        DebugHelper.ReleaseContext();
    }


    /// <summary>
    /// Sends the given file within response
    /// </summary>
    /// <param name="file">File to send</param>
    protected void SendFile(CMSOutputFile file)
    {
        // Clear response.
        Response.Cookies.Clear();
        Response.Clear();

        Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);

        if ((file != null) && file.IsValid)
        {
            // Redirect if the file should be redirected
            if (file.RedirectTo != "")
            {
                // Log hit before redirecting
                if (IsLiveSite && (file.FileNode != null) && (file.FileNode.NodeClassName.ToLower() == "cms.file"))
                {
                    if (AnalyticsHelper.AnalyticsEnabled(CurrentSiteName) && AnalyticsHelper.TrackFileDownloadsEnabled(CurrentSiteName) && !AnalyticsHelper.IsFileExtensionExcluded(CurrentSiteName, file.Attachment.AttachmentExtension) && !AnalyticsHelper.IsIPExcluded(CurrentSiteName, HttpContext.Current.Request.UserHostAddress) && !AnalyticsHelper.IsSearchEngineExcluded(CurrentSiteName))
                    {
                        HitLogProvider.LogHit(HitLogProvider.FILE_DOWNLOADS, CurrentSiteName, file.FileNode.DocumentCulture, file.FileNode.NodeAliasPath, file.FileNode.NodeID);
                    }
                }

                UrlHelper.Redirect(file.RedirectTo, true, this.CurrentSiteName);
            }

            // Check authentication if secured file
            if (file.IsSecured)
            {
                URLRewriter.CheckSecured(this.CurrentSiteName, this.ViewMode);
            }

            // Prepare etag
            string etag = file.CultureCode.ToLower();
            if (file.Attachment != null)
            {
                etag += "|" + file.Attachment.GUID + "|" + file.Attachment.LastModified.ToUniversalTime();
            }

            if (file.IsSecured)
            {
                // For secured files, add user name to etag
                etag += "|" + HttpContext.Current.User.Identity.Name;
            }

            // Put etag into ""
            etag = "\"" + etag + "\"";


            // Client caching - only on the live site
            if (useClientCache && AllowCache && CacheHelper.CacheImageEnabled(CurrentSiteName))
            {
                // Determine the last modified date and etag sent from the browser
                string currentETag = Request.Headers["If-None-Match"];
                string ifModifiedString = Request.Headers["If-Modified-Since"];
                if ((ifModifiedString != null) && (currentETag == etag))
                {
                    // IE-browsers send something like this:
                    // If-Modified-Since: Wed, 19 Jul 2006 11:19:59 GMT; length=3350
                    DateTime ifModified;
                    if (DateTime.TryParse(ifModifiedString.Split(';')[0], out ifModified))
                    {
                        // If not changed, let the browser use the cached data
                        if (file.LastModified <= ifModified.AddSeconds(1))
                        {
                            RespondNotModified(etag, !file.IsSecured);
                            return;
                        }
                    }
                }
            }

            // If physical file not present, try to load
            if (file.PhysicalFile == null)
            {
                EnsurePhysicalFile(outputFile);
            }

            // If the output data should be cached, return the output data
            bool cacheOutputData = false;
            if (file.Attachment != null)
            {
                // Cache data if allowed
                if (!LatestVersion && (this.CacheMinutes > 0))
                {
                    cacheOutputData = CacheHelper.CacheImageAllowed(CurrentSiteName, file.Attachment.AttachmentSize);
                }
            }

            // Ensure the file data if physical file not present
            if (!file.DataLoaded && (file.PhysicalFile == ""))
            {
                byte[] cachedData = GetCachedOutputData();
                if (file.EnsureData(cachedData))
                {
                    if ((cachedData == null) && cacheOutputData)
                    {
                        SaveOutputDataToCache(file.OutputData, GetOutputDataDependency(file.Attachment));
                    }
                }
            }

            // Send the file
            if ((file.OutputData != null) || (file.PhysicalFile != ""))
            {
                // Setup the mime type - Fix the special types
                string mimetype = file.MimeType;
                string extension = file.Attachment.AttachmentExtension;
                switch (extension.ToLower())
                {
                    case ".flv":
                        mimetype = "video/x-flv";
                        break;
                }

                // Prepare response
                Response.ContentType = mimetype;
                SetDisposition(file.Attachment.AttachmentName, extension);

                // Setup Etag property
                ETag = etag;

                // Set if resumable downloads should be supported
                AcceptRange = !IsExtensionExcludedFromRanges(extension);

                if (useClientCache && AllowCache && (CacheHelper.CacheImageEnabled(CurrentSiteName)))
                {
                    DateTime expires = DateTime.Now;

                    // Send last modified header to allow client caching
                    Response.Cache.SetLastModified(file.LastModified);
                    if (!file.IsSecured)
                    {
                        Response.Cache.SetCacheability(HttpCacheability.Public);
                        if (DocumentBase.AllowClientCache() && DocumentBase.UseFullClientCache)
                        {
                            expires = DateTime.Now.AddMinutes(CacheHelper.CacheImageMinutes(CurrentSiteName));
                        }
                    }

                    Response.Cache.SetExpires(expires);
                    Response.Cache.SetETag(etag);
                }
                else
                {
                    SetCacheability();
                }

                // Log hit
                if (IsLiveSite && (file.FileNode != null) && (file.FileNode.NodeClassName.ToLower() == "cms.file"))
                {
                    if (AnalyticsHelper.AnalyticsEnabled(CurrentSiteName) && AnalyticsHelper.TrackFileDownloadsEnabled(CurrentSiteName) && !AnalyticsHelper.IsFileExtensionExcluded(CurrentSiteName, file.Attachment.AttachmentExtension) && !AnalyticsHelper.IsIPExcluded(CurrentSiteName, HttpContext.Current.Request.UserHostAddress) && !AnalyticsHelper.IsSearchEngineExcluded(CurrentSiteName))
                    {
                        HitLogProvider.LogHit(HitLogProvider.FILE_DOWNLOADS, CurrentSiteName, file.FileNode.DocumentCulture, file.FileNode.NodeAliasPath, file.FileNode.NodeID);
                    }
                }

                // Add the file data
                if ((file.PhysicalFile != "") && (file.OutputData == null))
                {
                    // Stream the file from the file system
                    file.OutputData = WriteFile(file.PhysicalFile, cacheOutputData);
                }
                else
                {
                    // Use output data of the file in memory if present
                    WriteBytes(file.OutputData);
                }
            }
            else
            {
                NotFound();
            }
        }
        else
        {
            NotFound();
        }

        CompleteRequest();
    }


    /// <summary>
    /// Processes the attachment.
    /// </summary>
    protected void ProcessAttachment()
    {
        outputFile = null;

        // If guid given, process the attachment
        guid = QueryHelper.GetGuid("guid", Guid.Empty);
        allowLatestVersion = CheckAllowLatestVersion();

        if (guid != Guid.Empty)
        {
            // Check version
            if (VersionHistoryID > 0)
            {
                ProcessFile(guid, VersionHistoryID);
            }
            else
            {
                ProcessFile(guid);
            }
        }
        else
        {
            // Get by node GUID
            nodeGuid = QueryHelper.GetGuid("nodeguid", Guid.Empty);
            if (nodeGuid != Guid.Empty)
            {
                // If node GUID given, process the file
                ProcessNode(nodeGuid);
            }
            else
            {
                // Get by alias path and file name
                aliasPath = QueryHelper.GetString("aliaspath", null);
                fileName = QueryHelper.GetString("filename", null);
                if (aliasPath != null)
                {
                    ProcessNode(aliasPath, fileName);
                }
            }
        }

        // If chset specified, do not cache
        string chset = QueryHelper.GetString("chset", null);
        if (chset != null)
        {
            mIsLatestVersion = true;
        }
    }


    /// <summary>
    /// Processes the specified file and returns the data to the output stream
    /// </summary>
    /// <param name="attachmentGuid">Attachment guid.</param>
    protected void ProcessFile(Guid attachmentGuid)
    {
        AttachmentInfo atInfo = null;

        bool requiresData = true;
        // Check if it is necessary to load the file data
        if (useClientCache && IsLiveSite && CacheHelper.CacheImageEnabled(CurrentSiteName))
        {
            // If possibly cached by client, do not load data (may not be sent)
            string ifModifiedString = Request.Headers["If-Modified-Since"];
            if (ifModifiedString != null)
            {
                requiresData = false;
            }
        }

        // If output data available from cache, do not require loading the data
        byte[] cachedData = GetCachedOutputData();
        if (cachedData != null)
        {
            requiresData = false;
        }

        // Get AttachmentInfo object
        if (!IsLiveSite)
        {
            // Not livesite mode - get latest version
            if (node != null)
            {
                atInfo = DocumentHelper.GetAttachment(node, attachmentGuid, this.TreeProvider, true);
            }
            else
            {
                atInfo = DocumentHelper.GetAttachment(attachmentGuid, this.TreeProvider, CurrentSiteName);
            }
        }
        else
        {
            if (!requiresData || AttachmentManager.StoreFilesInFileSystem(CurrentSiteName))
            {
                // Do not require data from DB - Not necessary or available from file system
                atInfo = this.AttachmentManager.GetAttachmentInfoWithoutBinary(attachmentGuid, CurrentSiteName);
            }
            else
            {
                // Require data from DB - Stored in DB
                atInfo = this.AttachmentManager.GetAttachmentInfo(attachmentGuid, CurrentSiteName);
            }

            // If attachment not found, 
            if (allowLatestVersion && ((atInfo == null) || (latestForHistoryId > 0) || (atInfo.AttachmentDocumentID == latestForDocumentId)))
            {
                // Get latest version
                if (node != null)
                {
                    atInfo = DocumentHelper.GetAttachment(node, attachmentGuid, this.TreeProvider, true);
                }
                else
                {
                    atInfo = DocumentHelper.GetAttachment(attachmentGuid, this.TreeProvider, CurrentSiteName);
                }

                // If not attachment for the required document, do not return
                if ((atInfo.AttachmentDocumentID != latestForDocumentId) && (latestForHistoryId == 0))
                {
                    atInfo = null;
                }
                else
                {
                    mIsLatestVersion = true;
                }
            }
        }

        if (atInfo != null)
        {
            // Temporary attachment is always latest version
            if (atInfo.AttachmentFormGUID != Guid.Empty)
            {
                mIsLatestVersion = true;
            }

            bool checkPublishedFiles = AttachmentManager.CheckPublishedFiles(this.CurrentSiteName);
            bool checkFilesPermissions = AttachmentManager.CheckFilesPermissions(this.CurrentSiteName);

            // Get the document node
            if ((node == null) && (checkPublishedFiles || checkFilesPermissions))
            {
                // Try to get data from cache
                using (CachedSection<TreeNode> cs = new CachedSection<TreeNode>(ref node, this.CacheMinutes, true, null, "getfilenodebydocumentid", atInfo.AttachmentDocumentID))
                {
                    if (cs.LoadData)
                    {
                        // Get the document
                        node = this.TreeProvider.SelectSingleDocument(atInfo.AttachmentDocumentID, false);
                        if (node != null)
                        {
                            if (checkFilesPermissions)
                            {
                                // Load secured values
                                node.LoadInheritedValues(new string[] { "IsSecuredNode", "RequiresSSL" });
                            }

                            if (cs.Cached)
                            {
                                // Add to the cache
                                cs.CacheDependency = CacheHelper.GetCacheDependency(new string[] { "node|" + this.CurrentSiteName.ToLower() + "|" + node.NodeAliasPath.ToLower() });
                                cs.Data = node;
                            }
                        }
                        else
                        {
                            // Do not cache in case not cached
                            cs.CacheMinutes = 0;
                        }
                    }
                }
            }

            bool secured = false;
            if ((node != null) && checkFilesPermissions)
            {
                secured = (node.IsSecuredNode == 1);

                // Check secured pages
                if (secured)
                {
                    URLRewriter.CheckSecuredAreas(this.CurrentSiteName, false, this.ViewMode);
                }
                if (node.RequiresSSL == 1)
                {
                    URLRewriter.RequestSecurePage(false, node.RequiresSSL, this.ViewMode, this.CurrentSiteName);
                }

                // Check permissions
                bool checkPermissions = false;
                switch (URLRewriter.CheckPagePermissions(CurrentSiteName))
                {
                    case PageLocationEnum.All:
                        checkPermissions = true;
                        break;

                    case PageLocationEnum.SecuredAreas:
                        checkPermissions = secured;
                        break;
                }

                // Check the read permission for the page
                if (checkPermissions)
                {
                    if (CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                    {
                        UrlHelper.Redirect(URLRewriter.AccessDeniedPageURL(CurrentSiteName));
                    }
                }
            }


            bool resizeImage = (ImageHelper.IsImage(atInfo.AttachmentExtension) && AttachmentManager.CanResizeImage(atInfo, this.Width, this.Height, this.MaxSideSize));

            // If the file should be redirected, redirect the file
            if (IsLiveSite && SettingsKeyProvider.GetBoolValue(CurrentSiteName + ".CMSRedirectFilesToDisk"))
            {
                if (AttachmentManager.StoreFilesInFileSystem(CurrentSiteName))
                {
                    string path = null;
                    if (!resizeImage)
                    {
                        path = AttachmentManager.GetFilePhysicalURL(CurrentSiteName, atInfo.AttachmentGUID.ToString(), atInfo.AttachmentExtension);
                    }
                    else
                    {
                        int[] newDim = ImageHelper.EnsureImageDimensions(this.Width, this.Height, this.MaxSideSize, atInfo.AttachmentImageWidth, atInfo.AttachmentImageHeight);
                        path = AttachmentManager.GetFilePhysicalURL(CurrentSiteName, atInfo.AttachmentGUID.ToString(), atInfo.AttachmentExtension, newDim[0], newDim[1]);
                    }

                    // If path is valid, redirect
                    if (path != null)
                    {
                        // Check if file exists
                        string filePath = Server.MapPath(path);
                        if (File.Exists(filePath))
                        {
                            outputFile = new CMSOutputFile();
                            outputFile.IsSecured = secured;
                            outputFile.RedirectTo = path;
                            outputFile.Attachment = atInfo;
                        }
                    }
                }
            }

            // Get the data
            if ((outputFile == null) || (outputFile.Attachment == null))
            {
                outputFile = new CMSOutputFile(atInfo, null);
                outputFile.Width = this.Width;
                outputFile.Height = this.Height;
                outputFile.MaxSideSize = this.MaxSideSize;
                outputFile.SiteName = this.CurrentSiteName;
                outputFile.Resized = resizeImage;

                // Load the data if required
                if (requiresData)
                {
                    // Try to get the physical file, if not latest version
                    if (!mIsLatestVersion)
                    {
                        EnsurePhysicalFile(outputFile);
                    }
                    bool loadData = string.IsNullOrEmpty(outputFile.PhysicalFile);

                    // Load data if necessary
                    if (loadData)
                    {
                        if (atInfo.AttachmentBinary != null)
                        {
                            // Load from the attachment
                            outputFile.LoadData(atInfo.AttachmentBinary, this.AttachmentManager);
                        }
                        else
                        {
                            // Load from the disk
                            byte[] data = this.AttachmentManager.GetFile(atInfo, CurrentSiteName);
                            outputFile.LoadData(data, this.AttachmentManager);
                        }

                        // Save data to the cache, if not latest version
                        if (!mIsLatestVersion && (this.CacheMinutes > 0))
                        {
                            SaveOutputDataToCache(outputFile.OutputData, GetOutputDataDependency(outputFile.Attachment));
                        }
                    }
                }
                else if (cachedData != null)
                {
                    // Load the cached data if available
                    outputFile.OutputData = cachedData;
                }
            }

            if (outputFile != null)
            {
                outputFile.IsSecured = secured;

                // Add node data
                if (node != null)
                {
                    outputFile.AliasPath = node.NodeAliasPath;
                    outputFile.CultureCode = node.DocumentCulture;
                    outputFile.FileNode = node;

                    // Set the file validity
                    if (IsLiveSite && !mIsLatestVersion)
                    {
                        outputFile.ValidFrom = ValidationHelper.GetDateTime(node.GetValue("DocumentPublishFrom"), DateTime.MinValue);
                        outputFile.ValidTo = ValidationHelper.GetDateTime(node.GetValue("DocumentPublishTo"), DateTime.MaxValue);
                    }

                    // Set the archived flag
                    if (IsLiveSite && !mIsLatestVersion && AttachmentManager.CheckPublishedFiles(this.CurrentSiteName))
                    {
                        outputFile.IsPublished = node.IsPublished;
                    }
                }
            }
        }
    }


    /// <summary>
    /// Processes the specified document node
    /// </summary>
    /// <param name="aliasPath">Alias path</param>
    /// <param name="fileName">File name</param>
    protected void ProcessNode(string aliasPath, string fileName)
    {
        // Load the document node
        if (node == null)
        {
            // Try to get data from cache
            using (CachedSection<TreeNode> cs = new CachedSection<TreeNode>(ref node, this.CacheMinutes, true, null, "getfilenodebyaliaspath|", this.CurrentSiteName, CMSContext.GetBaseCacheKey(false, true), aliasPath))
            {
                if (cs.LoadData)
                {
                    // Get the document
                    string className = null;
                    bool combineWithDefaultCulture = SettingsKeyProvider.GetBoolValue(CurrentSiteName + ".CMSCombineImagesWithDefaultCulture");
                    string culture = this.CultureCode;

                    // Get the document
                    if (fileName == null)
                    {
                        // CMS.File
                        className = "CMS.File";
                    }

                    // Get the document data
                    if (!IsLiveSite)
                    {
                        node = DocumentHelper.GetDocument(this.CurrentSiteName, aliasPath, culture, combineWithDefaultCulture, className, null, null, -1, false, null, this.TreeProvider);
                    }
                    else
                    {
                        node = this.TreeProvider.SelectSingleNode(this.CurrentSiteName, aliasPath, culture, combineWithDefaultCulture, className, null, null, -1, false, null);
                    }

                    if (node != null)
                    {
                        if (AttachmentManager.CheckFilesPermissions(this.CurrentSiteName))
                        {
                            // Load secured values
                            node.LoadInheritedValues(new string[] { "IsSecuredNode", "RequiresSSL" });
                        }

                        if (cs.Cached)
                        {
                            // Prepare the cache dependency
                            cs.CacheDependency = CacheHelper.GetCacheDependency(new string[] { "node|" + this.CurrentSiteName.ToLower() + "|" + node.NodeAliasPath.ToLower() });
                            cs.Data = node;
                        }
                    }
                    else
                    {
                        // Do not cache in case not cached
                        cs.CacheMinutes = 0;
                    }
                }
            }
        }

        // Process the document
        ProcessNode(node, null, fileName);
    }


    /// <summary>
    /// Processes the specified document node
    /// </summary>
    /// <param name="nodeGuid">Node GUID</param>
    protected void ProcessNode(Guid nodeGuid)
    {
        // Load the document node
        string columnName = QueryHelper.GetString("columnName", String.Empty);
        if (node == null)
        {
            // Try to get data from cache
            using (CachedSection<TreeNode> cs = new CachedSection<TreeNode>(ref node, this.CacheMinutes, true, null, "getfilenodebyguid|", this.CurrentSiteName, CMSContext.GetBaseCacheKey(false, true), nodeGuid))
            {
                if (cs.LoadData)
                {
                    // Get the document
                    bool combineWithDefaultCulture = SettingsKeyProvider.GetBoolValue(CurrentSiteName + ".CMSCombineImagesWithDefaultCulture");
                    string culture = this.CultureCode;
                    string where = "NodeGUID = '" + nodeGuid.ToString() + "'";

                    // Get the document
                    string className = null;
                    if (columnName == "")
                    {
                        // CMS.File
                        className = "CMS.File";
                    }
                    else
                    {
                        // Other document types
                        TreeNode srcNode = this.TreeProvider.SelectSingleNode(nodeGuid, this.CultureCode, this.CurrentSiteName);
                        if (srcNode != null)
                        {
                            className = srcNode.NodeClassName;
                        }
                    }

                    // Get the document data
                    if (!IsLiveSite)
                    {
                        node = DocumentHelper.GetDocument(this.CurrentSiteName, null, culture, combineWithDefaultCulture, className, where, null, -1, false, null, this.TreeProvider);
                    }
                    else
                    {
                        node = this.TreeProvider.SelectSingleNode(this.CurrentSiteName, null, culture, combineWithDefaultCulture, className, where, null, -1, false, null);
                    }

                    if (node != null)
                    {
                        if (AttachmentManager.CheckFilesPermissions(this.CurrentSiteName))
                        {
                            // Load secured values
                            node.LoadInheritedValues(new string[] { "IsSecuredNode", "RequiresSSL" });
                        }

                        // Save to the cache
                        if (cs.Cached)
                        {
                            cs.CacheDependency = CacheHelper.GetCacheDependency(new string[] { "node|" + this.CurrentSiteName.ToLower() + "|" + node.NodeAliasPath.ToLower() });
                            cs.Data = node;
                        }
                    }
                    else
                    {
                        // Do not cache in case not cached
                        cs.CacheMinutes = 0;
                    }
                }
            }
        }

        // Process the document node
        ProcessNode(node, columnName, null);
    }


    /// <summary>
    /// Processes the specified document node
    /// </summary>
    /// <param name="node">Document node to process</param>
    /// <param name="columnName">Column name</param>
    /// <param name="fileName">File name</param>
    protected void ProcessNode(TreeNode node, string columnName, string fileName)
    {
        if (node != null)
        {
            // Check if latest or live site version is required
            bool latest = !IsLiveSite;
            if (allowLatestVersion && ((node.DocumentID == latestForDocumentId) || (node.DocumentCheckedOutVersionHistoryID == latestForHistoryId)))
            {
                latest = true;
            }

            // If not published, return no content
            if (!latest && !node.IsPublished)
            {
                outputFile = new CMSOutputFile(null, null);
                outputFile.AliasPath = node.NodeAliasPath;
                outputFile.CultureCode = node.DocumentCulture;
                if (IsLiveSite && AttachmentManager.CheckPublishedFiles(this.CurrentSiteName))
                {
                    outputFile.IsPublished = node.IsPublished;
                }
                outputFile.FileNode = node;
                outputFile.Height = this.Height;
                outputFile.Width = this.Width;
                outputFile.MaxSideSize = this.MaxSideSize;
            }
            else
            {
                // Get valid site name if link
                if (node.IsLink)
                {
                    TreeNode origNode = this.TreeProvider.GetOriginalNode(node);
                    if (origNode != null)
                    {
                        SiteInfo si = SiteInfoProvider.GetSiteInfo(origNode.NodeSiteID);
                        if (si != null)
                        {
                            CurrentSiteName = si.SiteName;
                        }
                    }
                }

                // Process the node
                if (node != null)
                {
                    // Get from specific column
                    if (String.IsNullOrEmpty(columnName) && node.NodeClassName.Equals("CMS.File", StringComparison.InvariantCultureIgnoreCase))
                    {
                        columnName = "FileAttachment";
                    }
                    if (!String.IsNullOrEmpty(columnName))
                    {
                        // File document type or specified by column
                        Guid attachmentGuid = ValidationHelper.GetGuid(node.GetValue(columnName), Guid.Empty);
                        if (attachmentGuid != Guid.Empty)
                        {
                            ProcessFile(attachmentGuid);
                        }
                    }
                    else
                    {
                        // Get by file name
                        if (fileName == null)
                        {
                            // CMS.File - Get 
                            Guid attachmentGuid = ValidationHelper.GetGuid(node.GetValue("FileAttachment"), Guid.Empty);
                            if (attachmentGuid != Guid.Empty)
                            {
                                ProcessFile(attachmentGuid);
                            }
                        }
                        else
                        {
                            // Other document types, get the attachment by file name
                            AttachmentInfo ai = null;
                            if (latest)
                            {
                                // Not livesite mode - get latest version
                                ai = DocumentHelper.GetAttachment(node, fileName, this.TreeProvider, false);
                            }
                            else
                            {
                                // Live site mode, get directly from database
                                ai = this.AttachmentManager.GetAttachmentInfo(node.DocumentID, fileName, false);
                            }

                            if (ai != null)
                            {
                                ProcessFile(ai.AttachmentGUID);
                            }
                        }
                    }
                }
            }
        }
    }


    /// <summary>
    /// Processes the specified version of the file and returns the data to the output stream
    /// </summary>
    /// <param name="attachmentGuid">Attachment GUID</param>
    /// <param name="versionHistoryId">Document version history ID</param>
    protected void ProcessFile(Guid attachmentGuid, int versionHistoryId)
    {
        AttachmentInfo atInfo = GetFile(attachmentGuid, versionHistoryId);
        if (atInfo != null)
        {
            // If attachment is image, try resize
            byte[] mFile = atInfo.AttachmentBinary;
            if (mFile != null)
            {
                string mimetype = null;
                if (ImageHelper.IsImage(atInfo.AttachmentExtension))
                {
                    if (AttachmentManager.CanResizeImage(atInfo, this.Width, this.Height, this.MaxSideSize))
                    {
                        // Do not search thumbnail on the disk
                        mFile = this.AttachmentManager.GetImageThumbnail(atInfo, CurrentSiteName, this.Width, this.Height, this.MaxSideSize, false);
                        mimetype = "image/jpeg";
                    }
                }

                if (mFile != null)
                {
                    outputFile = new CMSOutputFile(atInfo, mFile);
                }
                else
                {
                    outputFile = new CMSOutputFile();
                }
                outputFile.Height = this.Height;
                outputFile.Width = this.Width;
                outputFile.MaxSideSize = this.MaxSideSize;
                outputFile.MimeType = mimetype;
            }

            // Get the file document
            if (node == null)
            {
                node = this.TreeProvider.SelectSingleDocument(atInfo.AttachmentDocumentID);
            }

            if (node != null)
            {
                // Check secured area
                SiteInfo si = SiteInfoProvider.GetSiteInfo(node.NodeSiteID);
                if (si != null)
                {
                    if (pi == null)
                    {
                        pi = PageInfoProvider.GetPageInfo(si.SiteName, node.NodeAliasPath, node.DocumentCulture, node.DocumentUrlPath, false, this.Connection);
                    }
                    if (pi != null)
                    {
                        URLRewriter.RequestSecurePage(pi, false, this.ViewMode, this.CurrentSiteName);
                        URLRewriter.CheckSecuredAreas(this.CurrentSiteName, pi, false, this.ViewMode);
                    }
                }

                // Check the permissions for the document
                if ((CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Allowed) || (node.NodeOwner == CurrentUser.UserID))
                {
                    outputFile.AliasPath = node.NodeAliasPath;
                    outputFile.CultureCode = node.DocumentCulture;
                    if (IsLiveSite && AttachmentManager.CheckPublishedFiles(this.CurrentSiteName))
                    {
                        outputFile.IsPublished = node.IsPublished;
                    }
                    outputFile.FileNode = node;
                }
                else
                {
                    outputFile = null;
                }
            }
        }
    }


    /// <summary>
    /// Gets the file from version history
    /// </summary>
    /// <param name="attachmentGuid">Atachment GUID</param>
    /// <param name="versionHistoryId">Version history ID</param>
    protected AttachmentInfo GetFile(Guid attachmentGuid, int versionHistoryId)
    {
        VersionManager vm = new VersionManager(this.TreeProvider);
        IDataClass dc = vm.GetAttachmentVersion(versionHistoryId, attachmentGuid);
        if ((dc == null) || (dc.IsEmpty()))
        {
            return null;
        }
        else
        {
            AttachmentInfo ai = new AttachmentInfo(dc.DataRow, this.Connection);
            ai.AttachmentVersionHistoryID = versionHistoryId;
            return ai;
        }
    }


    /// <summary>
    /// Returns the output data dependency based on the given attachment record
    /// </summary>
    /// <param name="ai">Attachment object</param>
    protected CacheDependency GetOutputDataDependency(AttachmentInfo ai)
    {
        return (ai == null) ? null : CacheHelper.GetCacheDependency(AttachmentManager.GetDependencyCacheKeys(ai));
    }


    /// <summary>
    /// Ensures the physical file
    /// </summary>
    /// <param name="file">Output file</param>
    public bool EnsurePhysicalFile(CMSOutputFile file)
    {
        if (file == null)
        {
            return false;
        }

        // Try to link to file system
        if ((file.Attachment != null) && (file.Attachment.AttachmentVersionHistoryID == 0) && AttachmentManager.StoreFilesInFileSystem(file.SiteName))
        {
            string filePath = this.AttachmentManager.EnsurePhysicalFile(file.Attachment, file.SiteName);
            if (filePath != null)
            {
                if (file.Resized)
                {
                    // If resized, ensure the thumbnail file
                    if (AttachmentManager.GenerateThumbnails(file.SiteName))
                    {
                        filePath = this.AttachmentManager.EnsureThumbnailFile(file.Attachment, file.SiteName, this.Width, this.Height, this.MaxSideSize);
                        if (filePath != null)
                        {
                            // Link to the physical file
                            file.PhysicalFile = filePath;
                            return true;
                        }
                    }
                }
                else
                {
                    // Link to the physical file
                    file.PhysicalFile = filePath;
                    return false;
                }
            }
        }

        file.PhysicalFile = "";
        return false;
    }


    /// <summary>
    /// Returns true if latest version of the document is allowed    
    /// </summary>
    public bool CheckAllowLatestVersion()
    {
        // Check if latest version is required
        latestForDocumentId = QueryHelper.GetInteger("latestfordocid", 0);
        latestForHistoryId = QueryHelper.GetInteger("latestforhistoryid", 0);

        if ((latestForDocumentId > 0) || (latestForHistoryId > 0))
        {
            // Validate the hash
            string hash = QueryHelper.GetString("hash", "");
            string validate = (latestForDocumentId > 0) ? "d" + latestForDocumentId.ToString() : "h" + latestForHistoryId.ToString();

            if (!String.IsNullOrEmpty(hash) && QueryHelper.ValidateHashString(validate, hash))
            {
                return true;
            }
        }

        return false;
    }
}
