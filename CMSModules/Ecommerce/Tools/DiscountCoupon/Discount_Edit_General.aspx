<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Discount_Edit_General.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_DiscountCoupon_Discount_Edit_General" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Discount coupon - Edit" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblDiscountCouponDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtDiscountCouponDisplayName" runat="server" CssClass="TextBoxField"
                    MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" Display="Dynamic"
                    ValidationGroup="Discount" ControlToValidate="txtDiscountCouponDisplayName" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblDiscountCouponCode" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtDiscountCouponCode" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvCouponCode" runat="server" Display="Dynamic" ValidationGroup="Discount"
                    ControlToValidate="txtDiscountCouponCode" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="Label1" EnableViewState="false" />
            </td>
            <td>
                <asp:RadioButton runat="server" ID="radFixed" GroupName="radValues" Checked="true" /><asp:RadioButton
                    runat="server" ID="radPercentage" GroupName="radValues" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblDiscountCouponValue" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtDiscountCouponValue" runat="server" CssClass="TextBoxField" MaxLength="10"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblDiscountCouponValidFrom" EnableViewState="false" />
            </td>
            <td>
                <cms:DateTimePicker ID="dtPickerDiscountCouponValidFrom" runat="server" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblDiscountCouponValidTo" EnableViewState="false" />
            </td>
            <td>
                <cms:DateTimePicker ID="dtPickerDiscountCouponValidTo" runat="server" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" ValidationGroup="Discount" />
            </td>
        </tr>
    </table>
</asp:Content>
