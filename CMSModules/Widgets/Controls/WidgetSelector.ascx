﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetSelector.ascx.cs"
    Inherits="CMSModules_Widgets_Controls_WidgetSelector" %>
<%@ Register Src="~/CMSModules/Widgets/Controls/WidgetFlatSelector.ascx" TagName="WidgetFlatSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Widgets/Controls/WidgetTree.ascx" TagName="WidgetTree"
    TagPrefix="cms" %>
<table class="SelectorTable" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <div class="SelectorTree">
                <cms:WidgetTree ID="treeElem" runat="server" SelectWidgets="false" UsePostBack="false"
                    ShowRecentlyUsed="true" EnableViewState="false" />
            </div>
        </td>
        <td class="SelectorBorder">
            <div class="SelectorBorderGlue">
            </div>
        </td>
        <td class="ItemSelectorArea">
            <div class="ItemSelector">
                <cms:WidgetFlatSelector ID="flatElem" runat="server" />
            </div>
        </td>
    </tr>
</table>
