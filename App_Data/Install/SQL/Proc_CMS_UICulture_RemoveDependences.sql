-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_CMS_UICulture_RemoveDependences]
	@UICultureID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
	-- CMS_ResourceTranslation
    DELETE FROM [CMS_ResourceTranslation] WHERE TranslationUICultureID=@UICultureID;
	COMMIT TRANSACTION;
END
