CREATE TABLE [CMS_UIElement] (
		[ElementID]               int NOT NULL IDENTITY(1, 1),
		[ElementDisplayName]      nvarchar(200) NOT NULL,
		[ElementName]             nvarchar(200) NOT NULL,
		[ElementCaption]          nvarchar(200) NULL,
		[ElementTargetURL]        nvarchar(450) NULL,
		[ElementResourceID]       int NOT NULL,
		[ElementParentID]         int NULL,
		[ElementChildCount]       int NOT NULL,
		[ElementOrder]            int NULL,
		[ElementLevel]            int NOT NULL,
		[ElementIDPath]           nvarchar(450) NOT NULL,
		[ElementIconPath]         nvarchar(200) NULL,
		[ElementIsCustom]         bit NOT NULL,
		[ElementLastModified]     datetime NOT NULL,
		[ElementGUID]             uniqueidentifier NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_UIElement]
	ADD
	CONSTRAINT [PK_CMS_UIElement]
	PRIMARY KEY
	NONCLUSTERED
	([ElementID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_UIElement]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_UIElement]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UIElement_ElementParentID_CMS_UIElement]') IS NULL
BEGIN
		ALTER TABLE [CMS_UIElement]
			ADD CONSTRAINT [FK_CMS_UIElement_ElementParentID_CMS_UIElement]
			FOREIGN KEY ([ElementParentID]) REFERENCES [CMS_UIElement] ([ElementID])
END
IF OBJECT_ID(N'[CMS_UIElement]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Resource]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_UIElement_ElementResourceID_CMS_Resource]') IS NULL
BEGIN
		ALTER TABLE [CMS_UIElement]
			ADD CONSTRAINT [FK_CMS_UIElement_ElementResourceID_CMS_Resource]
			FOREIGN KEY ([ElementResourceID]) REFERENCES [CMS_Resource] ([ResourceID])
END
CREATE CLUSTERED INDEX [IX_CMS_UIElement_ElementResourceID_ElementLevel_ElementParentID_ElementOrder_ElementCaption]
	ON [CMS_UIElement] ([ElementResourceID], [ElementLevel], [ElementParentID], [ElementOrder], [ElementCaption])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
