<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailTemplate_Edit.aspx.cs"
    Inherits="CMSSiteManager_Development_EmailTemplates_EmailTemplate_Edit" Theme="Default"
    ValidateRequest="false" EnableEventValidation="false" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Email template - Properties" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>

<%@ Register Src="~/CMSAdminControls/MetaFiles/FileList.ascx" TagName="FileList"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/Selectors/MacroSelector.ascx" TagName="MacroSelector"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:ScriptManager ID="manScript" runat="server" />
    <cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <cms:LocalizedLabel runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top; width: 100%">
        <tr>
            <td class="FieldLabel" style="width: 150px;">
                <cms:LocalizedLabel runat="server" ID="lblDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDisplayName" runat="server"
                    EnableViewState="false" ControlToValidate="txtDisplayName" CssClass="ContentValidator"
                    Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblCodeName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodeName" runat="server" EnableViewState="false"
                    ControlToValidate="txtCodeName" CssClass="ContentValidator" Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblFrom" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtFrom" runat="server" CssClass="TextBoxField" MaxLength="250" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblCc" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCc" runat="server" CssClass="TextBoxField" MaxLength="250" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblBcc" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtBcc" runat="server" CssClass="TextBoxField" MaxLength="250" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblSubject" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtSubject" runat="server" CssClass="TextBoxField" MaxLength="250" />
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; padding-top: 5px" class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblText" EnableViewState="false" />
            </td>
            <td>
                <cms:ExtendedTextArea ID="txtText" runat="server" TextMode="MultiLine" CssClass="TextAreaLarge"
                    Width="100%" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td style="padding-top: 5px;">
                <cms:MacroSelector ID="macroSelectorElm" runat="server" JavaScripFunction="InsertMacro" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; padding-top: 5px" class="FieldLabel">
                <cms:LocalizedLabel ID="lblTextPlain" runat="server" EnableViewState="false" />
            </td>
            <td>
                <cms:ExtendedTextArea ID="txtPlainText" runat="server" TextMode="MultiLine" CssClass="TextAreaLarge"
                    Width="100%" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td style="padding-top: 5px;">
                <cms:MacroSelector ID="macroSelectorPlain" runat="server" JavaScripFunction="InsertMacroPlain" />
            </td>
        </tr>
    </table>
    <br />
    <cms:PageTitle ID="AttachmentTitle" runat="server" Visible="false" TitleCssClass="SubTitleHeader" />
    <br />
    <cms:FileList ID="AttachmentList" runat="server" Visible="false" />
</asp:Content>
