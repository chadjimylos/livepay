-- =============================================
-- Author:		<Marek Becka>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_COM_ShippingOption_RemoveDependences] 
	
	@ShippingOptionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	BEGIN TRANSACTION;
    DELETE FROM COM_PaymentShipping WHERE ShippingOptionID = @ShippingOptionId;
	COMMIT TRANSACTION;
END
