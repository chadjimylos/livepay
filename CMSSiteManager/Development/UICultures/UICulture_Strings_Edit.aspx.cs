using System;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.ResourceManager;
using CMS.Staging;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_UICultures_UICulture_Strings_Edit : SiteManagerPage
{
    #region "Variables"

    protected int stringID;
    protected int uiCultureID;
    protected bool showBack;
    protected string[,] tabs = new string[2, 3];
    protected UICultureInfo uic = null;

    #endregion


    #region "Properties

    protected int BackCount
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["BackCount"], 1);
        }
        set
        {
            ViewState["BackCount"] = value;
        }
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get parameters from query string
        stringID = QueryHelper.GetInteger("stringID", 0);
        uiCultureID = QueryHelper.GetInteger("uicultureID", 0);

        if (QueryHelper.GetBoolean("saved", false))
        {
            // Diplay 'Save' info after inserting new string
            lblInfo.Visible = true;
        }

        lblEnglishText.Text = String.Format(ResHelper.GetString("Administration-UICulture_String_New.EnglishText"), CultureHelper.DefaultUICulture);
        rfvKey.ErrorMessage = ResHelper.GetString("Administration-UICulture_String_New.EmptyKey");

        ResourceStringInfo ri = SqlResourceManager.GetResourceStringInfo(stringID, uiCultureID);
        // Initialize master page
        InitializeMasterPage(ri);

        uic = UICultureInfoProvider.GetUICultureInfo(uiCultureID);
        if (uic.UICultureCode == CultureHelper.DefaultUICulture)
        {
            // Default culture
            txtEnglishText.Visible = false;
            lblEnglishText.Visible = false;
            txtKey.Visible = true;
            lblKeyEng.Visible = false;

            if (!RequestHelper.IsPostBack())
            {
                txtKey.Text = ri.StringKey;
                txtText.Text = SqlResourceManager.GetStringStrictly(ri.StringKey, CultureHelper.DefaultUICulture);
            }
        }
        else
        {
            // Other cultures
            txtEnglishText.Visible = true;
            lblEnglishText.Visible = true;
            txtKey.Visible = false;
            rfvKey.Enabled = false;
            lblKeyEng.Visible = true;
            lblKeyEng.Text = ri.StringKey;
            txtEnglishText.Text = ValidationHelper.GetString(SqlResourceManager.GetStringStrictly(ri.StringKey, CultureHelper.DefaultUICulture), "");

            if (!RequestHelper.IsPostBack())
            {
                txtText.Text = ValidationHelper.GetString(SqlResourceManager.GetStringStrictly(ri.StringKey, uic.UICultureCode), "");
            }
        }

        chkCustomString.Checked = ri.StringIsCustom;
    }


    /// <summary>
    /// Initializes Maste Page
    /// </summary>
    protected void InitializeMasterPage(ResourceStringInfo ri)
    {
        // Initializes page breadcrumbs
        tabs[0, 0] = ResHelper.GetString("UICultures_Strings.Strings");
        tabs[0, 1] = "~/CMSSiteManager/Development/UICultures/UICulture_Strings_List.aspx?UIcultureID=" + uiCultureID;
        tabs[0, 2] = "";
        tabs[1, 0] = ri.StringKey;
        tabs[1, 1] = "";
        tabs[1, 2] = "";
        CurrentMaster.Title.Breadcrumbs = tabs;

        // Set actions
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("Development-UICulture_Strings_List.NewString");
        actions[0, 3] = ResolveUrl("UICulture_String_New.aspx?UIcultureID=" + uiCultureID);
        actions[0, 5] = GetImageUrl("CMSModules/CMS_UICultures/addstring.png");

        CurrentMaster.HeaderActions.Actions = actions;
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        // History back count
        BackCount++;
        string result = null;

        // Trim the key before save
        txtKey.Text = txtKey.Text.Trim();

        // Validate the code name if default culture
        if (uic.UICultureCode == CultureHelper.DefaultUICulture)
        {
            result = new Validator().NotEmpty(txtKey.Text, rfvKey.ErrorMessage).IsCodeName(txtKey.Text, ResHelper.GetString("Administration-UICulture_String_New.InvalidCodeName")).Result;
        }

        if (!string.IsNullOrEmpty(result))
        {
            // Display error message
            lblError.Text = result;
            lblError.Visible = true;
            lblInfo.Visible = false;
            return;
        }

        // Update the string
        ResourceStringInfo ri = SqlResourceManager.GetResourceStringInfo(stringID, uiCultureID);
        if (ri != null)
        {
            // Check if string with given key is not already defined
            ResourceStringInfo existing = SqlResourceManager.GetResourceStringInfo(txtKey.Text.Trim());
            if ((existing == null) || (existing.StringId == ri.StringId))
            {
                ri.StringIsCustom = chkCustomString.Checked;
                ri.UICultureCode = uic.UICultureCode;
                ri.TranslationText = txtText.Text;

                if (txtKey.Visible)
                {
                    // If key changed, log deletion of old string
                    string newKey = txtKey.Text.Trim();
                    if ((ri.StringKey.ToLower() != newKey.ToLower()) && (ri.LogSynchronization == SynchronizationTypeEnum.LogSynchronization))
                    {
                        TaskInfoProvider.LogSynchronization(ri, TaskTypeEnum.DeleteObject, null, null);
                    }

                    ri.StringKey = txtKey.Text.Trim();
                }

                // Update key
                SqlResourceManager.SetResourceStringInfo(ri);

                lblInfo.Visible = true;
                lblError.Visible = false;

                tabs[1, 0] = ri.StringKey;
            }
            else
            {
                lblError.Text = String.Format(ResHelper.GetString("Administration-UICulture_String_New.StringExists"), txtKey.Text.Trim());
                lblError.Visible = true;
                lblInfo.Visible = false;
            }
        }
    }

    #endregion
}
