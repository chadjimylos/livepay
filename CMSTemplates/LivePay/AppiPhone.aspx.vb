﻿Imports CMS.CMSHelper
Imports CMS.TreeEngine
Imports System.Data
Imports System.Globalization
Imports System.Threading

Partial Class CMSTemplates_LivePay_AppiPhone
    Inherits System.Web.UI.Page


    Private _MerchantID As Integer
    Public ReadOnly Property MerchantID() As Integer
        Get
            If Not String.IsNullOrEmpty(Request("merchantid")) Then
                _MerchantID = ValidationHelper.GetInteger(Request("merchantid"), 0)
            End If
            Return _MerchantID
        End Get
    End Property

    Private _Price As String
    Public ReadOnly Property Price() As String
        Get
            If Not String.IsNullOrEmpty(Request("price")) Then
                _Price = ValidationHelper.GetString(Request("price"), "0")
            End If
            Return _Price
        End Get
    End Property

    Private _Barcode As String
    Public ReadOnly Property Barcode() As String
        Get
            If Not String.IsNullOrEmpty(Request("barcode")) Then
                _Barcode = ValidationHelper.GetString(Request("barcode"), "0")
            End If
            Return _Barcode
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR")
      
        If MerchantID <> 0 Then
            Dim node As TreeNode = TreeHelper.SelectSingleNode(MerchantID)
            Dim url As String = String.Empty

            'If node.DocumentExtensions <> "" Then
            '    url = node.NodeAliasPath & node.DocumentExtensions
            'Else
            '    url = node.NodeAliasPath & ".aspx?selprice=" & Price
            'End If
            'url = "~/appiphone/payment.aspx"
            'Response.Write(CMS.CMSHelper.CMSContext.CurrentUser.IsPublic)
            'Response.Write(CMS.CMSHelper.CMSContext.CurrentUser.UserName)
            'Response.End()

            'Dim rf As System.Reflection.FieldInfo = GetType(FormsAuthentication).GetField("_Timeout", System.Reflection.BindingFlags.NonPublic Or System.Reflection.BindingFlags.Instance Or System.Reflection.BindingFlags.[Static])
            'Response.Write(rf.GetValue(Nothing))
            'Response.End()

            Dim StartDate As New DateTime(2012, 3, 5, 14, 0, 0)
            Dim NowDate As New DateTime
            NowDate = DateTime.Now()

            If CMS.CMSHelper.CMSContext.CurrentUser.IsPublic() AndAlso NowDate > StartDate Then
                url = "~/appiphone/login.aspx"
                url = UrlHelper.AddParameterToUrl(url, "login", "1")
            Else
                url = "~/appiphone/payment.aspx"
            End If


            Dim CustomPrice As Decimal

            url = UrlHelper.AddParameterToUrl(url, "nodeid", node.NodeID)
            If Barcode IsNot Nothing OrElse Barcode <> "" Then
                'Response.Write(Barcode)
                'Response.End()
                'Response.Write("LivePayID " & node.GetValue("LivePayID") & "<br />")
                'Response.Write(Request.RawUrl.ToString() & "<br />" & Request.Url.ToString())
                'Response.End()
                Select Case node.GetValue("LivePayID")
                    Case 1025 'DEH
                        If Barcode.Length = 26 Then
                            Dim Code As String = Barcode.ToString.Substring(0, 11)
                            If IsNumeric(Code) Then
                                Dim cd As String = mod11DEH(Code)
                                Dim Barcode1 As String = Barcode.Split("|")(0)
                                Dim FormatPrice As String = Barcode1.ToString.Substring(Barcode1.Length - 10, 7) & "," & Barcode1.ToString.Substring(Barcode1.Length - 3, 2)
                                CustomPrice = Convert.ToDecimal(FormatPrice).ToString("N2")
                                url = UrlHelper.AddParameterToUrl(url, "selprice", CustomPrice.ToString.Replace(",", "."))
                                url = UrlHelper.AddParameterToUrl(url, "info0", Code & cd)
                            End If


                            'Response.Write("Barcode " & Barcode.ToString.Length)
                            'Response.Write("<br/>CustomPrice " & CustomPrice)
                            'Response.Write("<br/>info0 " & Code & cd)
                            'Response.End()
                        End If

                    Case 1111 'OTE
                        'Dim Code As String = Barcode.ToString.Substring(0, 9)
                        Dim Code As String = Barcode.Split("|")(1).ToString().Substring(14, 8)

                        If IsNumeric(Code) Then
                            Dim cd As String = mod11OTE(Code, 9)
                            url = UrlHelper.AddParameterToUrl(url, "info0", Code & cd)
                            'Response.Write("Kodikos Pliromis:" & Code & cd & "<br />")
                            Dim ExpDate As String = Barcode.Split("|")(1).ToString().Substring(2, 6)
                            ExpDate = String.Format("{0}/{1}/20{2}", ExpDate.Substring(4, 2), ExpDate.Substring(2, 2), ExpDate.Substring(0, 2))
                            'Response.Write("Hmerominia Liksis:" & ExpDate & "<br />")
                            Dim PayAmount As String = Barcode.Split("|")(0).ToString().Substring(23, 11)
                            PayAmount = Integer.Parse(PayAmount.Substring(0, PayAmount.Length - 2)) & "." & PayAmount.Substring(PayAmount.Length - 2, 2)
                            'PayAmount = 
                            'Response.Write("Hm/nia Liksis:" & PayAmount)

                            url = UrlHelper.AddParameterToUrl(url, "info0", Code & cd)
                            url = UrlHelper.AddParameterToUrl(url, "info1", ExpDate)
                            url = UrlHelper.AddParameterToUrl(url, "selprice", PayAmount)
                        End If

                    Case 1200 'EYDAP
                        Dim cd As String = Barcode.Substring(0, 16) & "-" & wsNum2Digit(Barcode)
                        Dim dt As Date = GetDateFromEydapString(Barcode.Substring(16, 17))
                        Dim PayAmount As String = Barcode.ToString().Substring(16, 9)
                        PayAmount = Integer.Parse(PayAmount.Substring(0, PayAmount.Length - 2)) & "." & PayAmount.Substring(PayAmount.Length - 2, 2)

                        url = UrlHelper.AddParameterToUrl(url, "info0", cd)
                        url = UrlHelper.AddParameterToUrl(url, "info1", dt)
                        url = UrlHelper.AddParameterToUrl(url, "selprice", PayAmount)
                    Case Else
                        url = UrlHelper.AddParameterToUrl(url, "selprice", Price)
                End Select
            End If
            'Response.Redirect("https://market.android.com/?hl=el")
            'Response.Write(node.GetValue("LivePayID"))
            'Response.Write(Price1.ToString("#.00", Globalization.CultureInfo.InvariantCulture))

            'Response.Write(Price1.Format("#,###"))
            'Response.Write("https://uat.livepay.gr/appiphone/payment.aspx?nodeid=250")
            'Response.End()
            'Response.Redirect("https://uat.livepay.gr/appiphone/payment.aspx?nodeid=250")
            Response.Redirect(url)
        End If
    End Sub

    Private Function mod11DEH(ByVal value As String) As Integer
        Dim g As Int64 = 0
        Dim cd As Integer = 0
        
        g = value
        Dim y As Integer = 0
        y = g Mod 11

        If y = 10 Then
            cd = 1
        Else
            cd = y
        End If

        Return cd
    End Function

    Private Function mod11OTE(ByVal value As String, ByVal length As Integer) As Integer
        Dim g As Integer = 0
        Dim cd As Integer = 0
        Dim i As Integer = length

        For s As Integer = 3 To length + 1
            g += (i * Convert.ToInt32(value.Substring(s - 3, 1)))
            'Response.Write(Convert.ToInt32(value.Substring(s - 2, 1)) & vbCrLf)
            i -= 1
            'Response.Write(i & vbCrLf)
        Next

        Dim y As Integer = 0
        y = g Mod 11

        If y = 0 Then
            cd = 1
        ElseIf y = 1 Then
            cd = 0
        Else
            cd = 11 - y
        End If

        Return cd
    End Function

    Function wsNum2Digit(ByVal str As String) As String
        str = str.Substring(16, 17)

        Dim expDate As Date = GetDateFromEydapString(str)
        'FORM THE NEW DATE STRING 
        Dim strExpDate As String = String.Format("{0}{1}{2}", expDate.Year, Right("0" & expDate.Month, 2), Right("0" & expDate.Day, 2))

        'ADD IT TO THE AMOUNT
        str = str.Substring(0, 9) & strExpDate
        'REVERSE THE WHOLE THING (NEEDED FOR ALGORITHM BELOW)
        str = StrReverse(str)

        Dim outC As Integer = 0
        For i As Integer = 0 To str.Length - 1
            'Response.Write(str.Substring(i, 1) & "-" & 2 ^ (i) & "=" & str.Substring(i, 1) * 2 ^ (i) & "<br />")
            outC += str.Substring(i, 1) * 2 ^ (i)
        Next

        'RETURN MOD 99
        Return (outC Mod 99)
    End Function

    Function GetDateFromEydapString(ByVal str As String) As Date
        Dim strExpDate As String = str.Substring(9, 8)
        'SUBTRACT 20 DAYS. BECAUSE WE WANT TO. WHAT A CONVENTION!
        Dim expDate As Date = New Date(strExpDate.Substring(0, 4), strExpDate.Substring(4, 2), strExpDate.Substring(6, 2)).AddDays(-21)

        'Response.Write(expDate)
        'Response.End()
        Return expDate


    End Function
End Class
