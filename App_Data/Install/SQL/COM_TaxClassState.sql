CREATE TABLE [COM_TaxClassState] (
		[TaxClassID]      int NOT NULL,
		[StateID]         int NOT NULL,
		[TaxValue]        float NOT NULL,
		[IsFlatValue]     bit NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_TaxClassState]
	ADD
	CONSTRAINT [PK_COM_TaxClassState]
	PRIMARY KEY
	([TaxClassID], [StateID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_TaxClassState]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_State]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_TaxClassState_StateID_CMS_State]') IS NULL
BEGIN
		ALTER TABLE [COM_TaxClassState]
			ADD CONSTRAINT [FK_COM_TaxClassState_StateID_CMS_State]
			FOREIGN KEY ([StateID]) REFERENCES [CMS_State] ([StateID])
END
IF OBJECT_ID(N'[COM_TaxClassState]') IS NOT NULL
	AND OBJECT_ID(N'[COM_TaxClass]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_TaxClassState_TaxClassID_COM_TaxClass]') IS NULL
BEGIN
		ALTER TABLE [COM_TaxClassState]
			ADD CONSTRAINT [FK_COM_TaxClassState_TaxClassID_COM_TaxClass]
			FOREIGN KEY ([TaxClassID]) REFERENCES [COM_TaxClass] ([TaxClassID])
END
