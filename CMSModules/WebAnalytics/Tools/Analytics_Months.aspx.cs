using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Globalization;

using CMS.GlobalHelper;
using CMS.WebAnalytics;
using CMS.CMSHelper;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_WebAnalytics_Tools_Analytics_Months : CMSWebAnalyticsPage
{
    #region "Code name consts"

    const string ANAL_CODE_PREFIX = "analytics_codename.";
    const string ANAL_CODE_VISITS = "visits";
    const string ANAL_CODE_VISIT_LIST = "visitfirst;visitreturn;";

    #endregion


    #region "Private variables"

    bool reportingModuleIsLoaded = false;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Display disabled information
        if (!AnalyticsHelper.AnalyticsEnabled(CMSContext.CurrentSiteName))
        {
            this.pnlDisabled.Visible = true;
            this.lblDisabled.Text = ResHelper.GetString("WebAnalytics.Disabled");
        }

        // Check if reporting module is available
        reportingModuleIsLoaded = ModuleEntry.IsModuleLoaded(ModuleEntry.REPORTING);

        // Get statistics code name (pageviews, pagenotfound, filedownloads)
        string statCodeName = QueryHelper.GetString("statCodeName", String.Empty);
        string dataCodeName = QueryHelper.GetString("dataCodeName", String.Empty);
        string reportCodeName = QueryHelper.GetString("reportCodeName", String.Empty);
        string additionalCodeName = QueryHelper.GetString("additionalCodeName", String.Empty);

        imgTitle.ImageUrl = GetImageUrl("CMSModules/CMS_WebAnalytics/module.png");

        if ((statCodeName != String.Empty && ANAL_CODE_VISIT_LIST.Contains(statCodeName.ToLower() + ";")) ||
            (additionalCodeName != String.Empty && ANAL_CODE_VISIT_LIST.Contains(additionalCodeName.ToLower() + ";")))
        {
            lblTitle.Text = ResHelper.GetString(ANAL_CODE_PREFIX + ANAL_CODE_VISITS);
        }
        else
        {
            lblTitle.Text = ResHelper.GetString(ANAL_CODE_PREFIX + statCodeName);
        }

        ScriptHelper.RegisterTitleScript(this, lblTitle.Text);

        lblYears.Text = ResHelper.GetString("Analytics_Months.lblyears");
        btnShow.Text = ResHelper.GetString("general.show");

        int year = QueryHelper.GetInteger("hdnyearsel", DateTime.Today.Year);
        if (drpYears.SelectedValue != null)
        {
            year = ValidationHelper.GetInteger(drpYears.SelectedValue, year);
        }

        // Get culture specific year
        DateTime time = new DateTime(year, DateTime.Today.Month, DateTime.Today.Day);
        string culture = CultureHelper.GetPreferredUICulture();
        if (culture == null)
        {
            culture = CultureHelper.DefaultUICulture;
        }
        string cultureYear = time.ToString("yyyy", new System.Globalization.CultureInfo(culture));
        int cultureYearInt = ValidationHelper.GetInteger(cultureYear, time.Year);

        // Dropdownlist initialization
        // Insert current year (or selected year), 2 years before and 2 years after
        drpYears.Items.Clear();
        for (int i = -2; i < 3; i++)
        {
            drpYears.Items.Add(new ListItem((cultureYearInt + i) + "", (year + i) + ""));
        }
        drpYears.SelectedValue = year.ToString();

        // Get selected bars
        string selectedBar = QueryHelper.GetString("hdnmonthsel", String.Empty);
        int barIndex = -1;
        int tableIndex = -1;

        string barclicked = (!RequestHelper.IsPostBack()) ? "&barclicked=1" : "";
        barclicked = String.Empty;

        if (!RequestHelper.IsPostBack() && selectedBar != String.Empty)
        {
            int i = selectedBar.LastIndexOf('_');
            if (i > 0)
            {
                tableIndex = ValidationHelper.GetInteger(selectedBar.Substring(i + 1, selectedBar.Length - i - 1), -1);
                selectedBar = selectedBar.Remove(i);
                i = selectedBar.LastIndexOf('_');
                if (i > 0)
                {
                    barIndex = ValidationHelper.GetInteger(selectedBar.Substring(i + 1, selectedBar.Length - i - 1), -1);
                }
            }
        }

        string[] tmp = LoadMonthData(year, statCodeName, dataCodeName, reportCodeName, additionalCodeName, tableIndex, barIndex);

        string daysSel = String.Empty;
        if (!RequestHelper.IsPostBack())
        {
            daysSel = QueryHelper.GetString("hdndaysel", String.Empty);
        }

        if (daysSel == String.Empty)
        {
            if (tmp[0] != String.Empty)
            {
                AddScript("ucGraphMonths_ShowDesktopContent('', '" + tmp[0] + "&hdndaysel=" + daysSel + barclicked + "'); ");
            }
            else
            {
                AddScript("ucGraphMonths_ShowDesktopContent('', '" + (reportingModuleIsLoaded ?

                    ResolveUrl("~/CMSModules/Reporting/Tools/Analytics_Report.aspx?statCodeName=") + statCodeName + "&dataCodeName=" + dataCodeName + "&reportCodeName=" + reportCodeName +
                    "&additionalCodeName=" + additionalCodeName + "&year=" + year + "&hdndaysel=" + daysSel + barclicked :
                    ResolveUrl("~/CMSDesk/blank.htm")) + "'); ");
            }
        }

        if (tmp[1] != String.Empty)
        {
            AddScript("ucGraphMonths_ShowDesktopContent('" + tmp[1] + "&hdndaysel=" + daysSel + barclicked + " ', ''); ");
        }
        else
        {
            AddScript("ucGraphMonths_ShowDesktopContent('Analytics_Days.aspx?statCodeName=" + statCodeName + "&dataCodeName=" + dataCodeName + "&reportCodeName=" + reportCodeName + "&additionalCodeName=" + additionalCodeName + "&year=" + year + "&month=" + DateTime.Now.Month
                + "&hdndaysel=" + daysSel + barclicked + "', ''); ");
        }
    }


    /// <summary>
    /// Loads month statistics and creates graph.
    /// </summary>
    /// <param name="year">Current year.</param>
    /// <param name="statCodeName">Statistics code name (visitors, pageviews...).</param>
    /// <param name="dataCodeName">Database code name.</param>
    /// <param name="reportCodeName">Report code name.</param>
    /// <param name="additionalCodeName">Additional stats code name.</param>
    /// <param name="selectedMonth">Selected month.</param>
    protected string[] LoadMonthData(int year, string statCodeName, string dataCodeName, string reportCodeName, string additionalCodeName, int selectedTable, int selectedBar)
    {
        // Prepare variables
        string tmp, result1 = String.Empty, result2 = String.Empty;
        string monthName = "";
        int maxsum = 0;
        DataSet ds = null;
        DataRow[] dr = null;

        // Prepare graph
        ucGraphMonths.AddGraph(40, "MonthGraph");

        // Set starting date
        DateTime dt = new DateTime(year, 1, 1);

        // Load values for all months
        if (!String.IsNullOrEmpty(additionalCodeName))
        {
            ds = HitsInfoProvider.GetAllHitsInfoBetween(CMSContext.CurrentSite.SiteID,
                    HitsIntervalEnum.Month, new string[] { dataCodeName, additionalCodeName }, dt, dt.AddYears(1), "HitsStartTime, HitsEndTime, HitsCount");
        }
        else
        {
            ds = HitsInfoProvider.GetAllHitsInfoBetween(CMSContext.CurrentSite.SiteID,
                    HitsIntervalEnum.Month, new string[] { dataCodeName }, dt, dt.AddYears(1), "HitsStartTime, HitsEndTime, HitsCount");
        }

        // Display results for each month
        for (int i = 0; i < 12; i++)
        {
            int sum = 0;

            if (ds != null)
            {
                // Select month
                dr = ds.Tables[0].Select("HitsStartTime <= #" + dt.ToString(new CultureInfo(AnalyticsHelper.AnalyticsCulture)) + "# AND #" + dt.ToString(new CultureInfo(AnalyticsHelper.AnalyticsCulture)) + "# < HitsEndTime");
                foreach (DataRow row in dr)
                {
                    // Add browser result to month sum
                    sum += ValidationHelper.GetInteger(row["HitsCount"], 0);
                }
            }

            // Prepare parameters
            monthName = DateTimeHelper.GetMonthName(dt.Month, CMSContext.CurrentUser.PreferredUICultureCode);
            monthName = monthName[0].ToString().ToUpper() + monthName.Substring(1);

            tmp = "statCodeName=" + statCodeName + "&dataCodeName=" + dataCodeName + "&reportCodeName=" + reportCodeName + "&year=" + year + "&month=" + dt.Month;
            ucGraphMonths.AddGraphData(sum,
                DateTimeHelper.GetMonthAbbrevName(dt.Month, CMSContext.CurrentUser.PreferredUICultureCode),
                monthName + " (" + sum + ")",
                "Analytics_Days.aspx?barclicked=1&" + tmp + "&additionalCodeName=" + additionalCodeName,
                (reportingModuleIsLoaded ?
                ResolveUrl("~/CMSModules/Reporting/Tools/Analytics_Report.aspx?barclicked=1&") + tmp :
                ResolveUrl("~/CMSDesk/blank.htm")),
                1);

            // Set graph page
            if (i == selectedBar)
            {
                result1 = (reportingModuleIsLoaded ?
                    ResolveUrl("~/CMSModules/Reporting/Tools/Analytics_Report.aspx?") + tmp :
                    ResolveUrl("~/CMSDesk/blank.htm"));
                result2 = "Analytics_Days.aspx?" + tmp + "&additionalCodeName=" + additionalCodeName;
            }

            // Update maximum sum
            if (sum > maxsum)
            {
                maxsum = sum;
            }

            // Increase date by 1 month
            dt = dt.AddMonths(1);
        }

        // Display graph
        ucGraphMonths.SelectBar(selectedTable, selectedBar, true);
        ucGraphMonths.RenderGraph();

        return new string[] { result1, result2 };
    }


    protected void btnShow_Click(object sender, EventArgs e)
    {
        AddScript(" parent.frames['analtree'].SetSelectedBar('hdnYearSelected', " + drpYears.SelectedValue + "); ");
    }


    /// <summary>
    /// Add another script to literal.
    /// </summary>
    public override void AddScript(string script)
    {
        ltlScript.Text += ScriptHelper.GetScript(script);

        if (!ltlScript.Visible)
        {
            ltlScript.Visible = true;
        }
    }
}
