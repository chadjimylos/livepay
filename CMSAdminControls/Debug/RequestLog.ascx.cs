using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSAdminControls_Debug_RequestLog : RequestProcessLog
{
    #region "Variables"

    protected string cmsVersion = null;

    protected int index = 0;

    protected DateTime firstTime = DateTime.MinValue;
    protected DateTime lastTime = DateTime.MinValue;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        this.EnableViewState = false;
        this.Visible = true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.Visible = false;

        if (this.Log != null)
        {
            this.Visible = true;

            // Log request values
            RequestHelper.LogRequestValues(true, true, true);

            if (this.Log.ValueCollections != null)
            {
                this.tblResC.Title = ResHelper.GetString("RequestLog.ResponseCookies");
                this.tblResC.Table = this.Log.ValueCollections.Tables["ResponseCookies"];

                this.tblReqC.Title = ResHelper.GetString("RequestLog.RequestCookies");
                this.tblReqC.Table = this.Log.ValueCollections.Tables["RequestCookies"];

                this.tblVal.Title = ResHelper.GetString("RequestLog.Values");
                this.tblVal.Table = this.Log.ValueCollections.Tables["Values"];
            }

            // Get the log table
            DataTable dt = this.Log.LogTable;

            cmsVersion = ResHelper.GetString("Footer.Version") + "&nbsp;" + CMSContext.SYSTEM_VERSION + "&nbsp;" + ResHelper.GetString("Footer.Build") + "&nbsp;" + CMSContext.FullSystemVersion;

            gridCache.Columns[1].HeaderText = ResHelper.GetString("RequestLog.Operation");
            gridCache.Columns[2].HeaderText = ResHelper.GetString("RequestLog.Parameter");
            gridCache.Columns[3].HeaderText = ResHelper.GetString("RequestLog.FromLast");
            gridCache.Columns[4].HeaderText = ResHelper.GetString("RequestLog.FromStart");

            if (LogStyle != "")
            {
                this.ltlInfo.Text = "<div style=\"padding: 2px; font-weight: bold; background-color: #eecccc; border-bottom: solid 1px #ff0000;\">" + ResHelper.GetString("RequestLog.Info") + "</div>";
            }

            // Bind the data
            gridCache.DataSource = dt;
            gridCache.DataBind();
        }
    }


    /// <summary>
    /// Gets the item index
    /// </summary>
    protected int GetIndex()
    {
        return ++index;
    }


    protected string GetBeginIndent(object ind)
    {
        int indent = ValidationHelper.GetInteger(ind, 0);
        string result = "";
        for (int i = 0; i < indent; i++)
        {
            result += "&gt;"; //"<div style=\"padding-left: 10px;\">";
        }

        return result;
    }


    protected string GetEndIndent(object ind)
    {
        int indent = ValidationHelper.GetInteger(ind, 0);
        string result = "";
        /*
        for (int i = 0; i < indent; i++)
        {
            result += "</div>";
        }*/

        return result;
    }


    /// <summary>
    /// Gets the time from the last action
    /// </summary>
    /// <param name="time">Time of the action</param>
    protected string GetFromLast(object time)
    {
        DateTime t = ValidationHelper.GetDateTime(time, DateTime.MinValue);
        if (lastTime == DateTime.MinValue)
        {
            return null;
        }
        else
        {
            double duration = t.Subtract(lastTime).TotalSeconds;
            return duration.ToString("F3");
        }
    }


    /// <summary>
    /// Gets the time from the first action
    /// </summary>
    /// <param name="time">Time of the action</param>
    protected string GetFromStart(object time)
    {
        DateTime t = ValidationHelper.GetDateTime(time, DateTime.MinValue);
        if (firstTime == DateTime.MinValue)
        {
            firstTime = t;
        }

        lastTime = t;
        this.TotalDuration = t.Subtract(firstTime).TotalSeconds;

        return this.TotalDuration.ToString("F3");
    }
}
