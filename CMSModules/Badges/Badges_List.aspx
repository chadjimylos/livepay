<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Badges_List.aspx.cs" Inherits="CMSModules_Badges_Badges_List"
    ValidateRequest="false" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" ContentPlaceHolderID="plcContent" runat="server">
    <cms:UniGrid runat="server" ID="UniGrid" GridName="~/CMSModules/Badges/BadgesList.xml"
        OrderBy="BadgeTopLimit DESC" IsLiveSite="false" />
</asp:Content>
