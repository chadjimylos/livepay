using System;

using CMS.GlobalHelper;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.LicenseProvider;
using CMS.SettingsProvider;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_New_Edit : CMSModalPage
{
    protected int newsletterId = 0;
    protected int templateId = 0;
    protected Issue issue = null;

    private int IssueId
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["IssueID"], 0);
        }
        set
        {
            ViewState["IssueID"] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check the license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), "") != "")
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Newsletters);
        }

        // Check site availability
        if (!ResourceSiteInfoProvider.IsResourceOnSite("CMS.Newsletter", CMSContext.CurrentSiteName))
        {
            RedirectToResourceNotAvailableOnSite("CMS.Newsletter");
        }

        CurrentUserInfo user = CMSContext.CurrentUser;

        // Check 'NewsletterRead' permission
        if (!user.IsAuthorizedPerResource("CMS.Newsletter", "Read"))
        {
            RedirectToCMSDeskAccessDenied("CMS.Newsletter", "Read");
        }

        // Check 'Author issues' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "authorissues"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "authorissues");
        }
        
        chkShowInArchive.Text = ResHelper.GetString("NewsletterTemplate_Edit.ShowInArchive");

        string tmp = " document.getElementById('" + txtSubject.ClientID + "').value, " +
                     " document.getElementById('" + chkShowInArchive.ClientID + "').checked ";

        btnSave.Attributes.Add("onclick", "ClearToolbar();frames[0].SaveContent(false, " + tmp + ");return false;");
        btnNext.Attributes.Add("onclick", "ClearToolbar();frames[0].SaveContent(true, " + tmp + ");return false;");

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "NextFunction", ScriptHelper.GetScript("" +
        "function NextClick(issueId){" +
        "document.getElementById('" + hdnNext.ClientID + "').value = issueId; " +
        this.Page.ClientScript.GetPostBackEventReference(btnNextHidden, null) +
        " }"));

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SaveFunction", ScriptHelper.GetScript("" +
        "function SaveClick(issueId){" +
        "document.getElementById('" + hdnNext.ClientID + "').value = issueId; " +
        this.Page.ClientScript.GetPostBackEventReference(btnSaveHidden, null) +
        " }"));

        lblError.Text = ResHelper.GetString("NewsletterContentEditor.SubjectRequired");
        lblError.Style.Add("display", "none");
        lblInfo.Text = ResHelper.GetString("general.changessaved");
        lblInfo.Style.Add("display", "none");

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "MsgInfo", ScriptHelper.GetScript(
            "function MsgInfo(err) { \n" +
            "    if (err == 0) { \n" +
            "         document.getElementById('" + lblError.ClientID + "').style.display = \"none\"; \n" +
            "         document.getElementById('" + lblInfo.ClientID + "').style.display = \"block\"; \n" +
            "    } \n" +
            "    if (err == 1) { \n" +
            "         document.getElementById('" + lblInfo.ClientID + "').style.display = \"none\"; \n" +
            "         document.getElementById('" + lblError.ClientID + "').style.display = \"block\"; \n" +
            "    } \n" +
            "} \n"));

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SaveDocument",
            ScriptHelper.GetScript(
"            function SaveDocument() \n" +
"            { \n" +
"                ClearToolbar(); \n" +
"                window.frames['iframeContent'].SaveContent(false, document.getElementById('" + txtSubject.ClientID + "').value, " +
                                                                 " document.getElementById('" + chkShowInArchive.ClientID + "').checked ); \n" +
"            } \n"));


        newsletterId = QueryHelper.GetInteger("newsletterid", 0);
        IssueId = QueryHelper.GetInteger("issueid", IssueId);

        if ((newsletterId == 0) && (IssueId == 0))
        {
            return;
        }

        if (IssueId > 0) //user comes back from Newsletter_Issue_New_Preview.aspx page
        {
            contentBody.IssueID = IssueId;

            issue = IssueProvider.GetIssue(IssueId);
            if (newsletterId == 0)
            {
                newsletterId = issue.IssueNewsletterID;
            }

            contentBody.NewsletterID = newsletterId;

            txtSubject.Text = issue.IssueSubject;
            chkShowInArchive.Checked = issue.IssueShowInNewsletterArchive;
        }
        else //user is creating new issue
        {
            contentBody.NewsletterID = newsletterId;
            Newsletter newsletter = NewsletterProvider.GetNewsletter(newsletterId);
            if (newsletter != null)
            {
                templateId = newsletter.NewsletterTemplateID;
            }
        }

        // Initializes page title control		
        this.Page.Title = ResHelper.GetString("newsletter_issue_list.title");
        this.CurrentMaster.Title.TitleText = this.Page.Title;
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Newsletter_Issue/new.png");

        ucHeader.Title = ResHelper.GetString("newsletter_issue_list.step1");
        ucHeader.Header = ResHelper.GetString("newsletter_issue_list.header");
        ucHeader.DescriptionVisible = false;
        btnNext.Text = ResHelper.GetString("general.next") + " >";
        btnSave.Text = ResHelper.GetString("general.save");
        btnClose.Text = ResHelper.GetString("general.close");
        AttachmentTitle.TitleText = ResHelper.GetString("general.attachments");

        if (IssueId > 0)
        {
            AttachmentList.Visible = true;
            AttachmentList.ObjectID = IssueId;
            AttachmentList.SiteID = CMSContext.CurrentSite.SiteID;
            AttachmentList.AllowPasteAttachments = true;
            AttachmentList.ObjectType = NewsletterObjectType.NEWSLETTERISSUE;
            AttachmentList.Category = MetaFileInfoProvider.OBJECT_CATEGORY_ISSUE;
            AttachmentList.OnAfterUpload += new EventHandler(AttachmentList_OnAfterUpload);
            AttachmentList.UploadOnClickScript = "SaveDocument();";
            AttachmentList.AllowEdit = true;
            lblAttInfo.Visible = false;
        }
        else
        {
            lblAttInfo.Text = ResHelper.GetString("newsletter_issue_list.attachmentinfo");
        }
    }
    

    protected override void OnPreRender(EventArgs e)
    {
        this.lblInfo.Visible = (this.lblInfo.Text != "");
        this.lblError.Visible = (this.lblError.Text != "");
        this.pnlInfo.Visible = (this.lblInfo.Text != "") || (this.lblError.Text != "");
        base.OnPreRender(e);
    }


    protected void btnNext_Click(object sender, EventArgs e)
    {
        UrlHelper.Redirect("Newsletter_Issue_New_Preview.aspx?issueid=" + hdnNext.Value + "&newsletterid=" + newsletterId.ToString());
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        UrlHelper.Redirect("Newsletter_Issue_New_Edit.aspx?issueid=" + hdnNext.Value + "&newsletterid=" + newsletterId.ToString() + "&saved=1");
    }


    void AttachmentList_OnAfterUpload(object sender, EventArgs e)
    {
        // Show info message after upload/delete
        lblInfo.Style.Remove("display");
        lblInfo.Style.Add("display", "block");
    }
}
