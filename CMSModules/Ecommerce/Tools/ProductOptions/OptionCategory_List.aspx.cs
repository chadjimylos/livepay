using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_ProductOptions_OptionCategory_List : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize controls
        this.CurrentMaster.HeaderActions.HelpName = "helpTopic";
        this.CurrentMaster.HeaderActions.HelpTopicName = "list_categories";

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("OptionCategory_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("OptionCategory_New.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_OptionCategory/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        // Initialize unigrid
        OptionCategoryGrid.OrderBy = "CategoryDisplayName ASC";
        OptionCategoryGrid.OnAction += new OnActionEventHandler(OptionCategoryGrid_OnAction);
        OptionCategoryGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(OptionCategoryGrid_OnExternalDataBound);
        OptionCategoryGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    protected object OptionCategoryGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "categoryselectiontype":
                switch (ValidationHelper.GetString(parameter, ""))
                {
                    case "dropdown":
                        return ResHelper.GetString("OptionCategory_List.DropDownList");

                    case "checkboxhorizontal":
                        return ResHelper.GetString("OptionCategory_List.checkboxhorizontal");

                    case "checkboxvertical":
                        return ResHelper.GetString("OptionCategory_List.checkboxvertical");

                    case "radiobuttonhorizontal":
                        return ResHelper.GetString("OptionCategory_List.radiobuttonhorizontal");

                    case "radiobuttonvertical":
                        return ResHelper.GetString("OptionCategory_List.radiobuttonvertical");
                }
                break;

            case "categoryenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }

        return parameter;
    }


    /// <summary>
    /// Handles the OptionCategoryGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void OptionCategoryGrid_OnAction(string actionName, object actionArgument)
    {
        int categoryId = ValidationHelper.GetInteger(actionArgument, 0);

        // Set actions
        if (actionName == "edit")
        {
            UrlHelper.Redirect("OptionCategory_Edit.aspx?CategoryID=" + categoryId);
        }
        else if (actionName == "delete")
        {
            // Check 'ConfigurationModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
            }

            // Check dependencies
            if (OptionCategoryInfoProvider.CheckDependencies(categoryId))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                return;
            }

            // Delete option category from database
            OptionCategoryInfoProvider.DeleteOptionCategoryInfo(categoryId);
        }
    }
}
