using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_SystemTables_SystemTable_Edit_Query_Edit : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get class id from querystring, used when creating new query
        int classId = QueryHelper.GetInteger("classid", 0);
        queryEdit.ClassID = classId;

        string queryName = ResHelper.GetString("systbl.queryedit.newlink");

        queryEdit.RefreshPageURL = "SystemTable_Edit_Query_Edit.aspx";
        queryEdit.QueryParamID = "?queryid";

        int queryId = QueryHelper.GetInteger("queryid", 0);
        if (queryId > 0)
        {
            // Get information of current query
            Query query = QueryProvider.GetQuery(queryId);

            if (query != null)
            {
                queryEdit.QueryID = queryId;
                classId = query.QueryClassId;
                queryName = query.QueryName;
            }
        }

        // Initializes page title
        string queries = ResHelper.GetString("systbl.queryedit.querylist");

        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = queries;
        breadcrumbs[0, 1] = ResolveUrl("SystemTable_Edit_Query_List.aspx?classid=" + classId.ToString());
        breadcrumbs[0, 2] = "";
        breadcrumbs[1, 0] = queryName;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
    }
}
