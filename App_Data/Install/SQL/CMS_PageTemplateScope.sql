CREATE TABLE [CMS_PageTemplateScope] (
		[PageTemplateScopeID]               int NOT NULL IDENTITY(1, 1),
		[PageTemplateScopePath]             nvarchar(450) NOT NULL,
		[PageTemplateScopeLevels]           nvarchar(450) NULL,
		[PageTemplateScopeCultureID]        int NULL,
		[PageTemplateScopeClassID]          int NULL,
		[PageTemplateScopeTemplateID]       int NOT NULL,
		[PageTemplateScopeSiteID]           int NULL,
		[PageTemplateScopeLastModified]     datetime NOT NULL,
		[PageTemplateScopeGUID]             uniqueidentifier NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_PageTemplateScope]
	ADD
	CONSTRAINT [PK_CMS_PageTemplateScope]
	PRIMARY KEY
	NONCLUSTERED
	([PageTemplateScopeID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_PageTemplateScope]
	ADD
	CONSTRAINT [DEFAULT_CMS_PageTemplateScope_PageTemplateScopeGUID]
	DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [PageTemplateScopeGUID]
ALTER TABLE [CMS_PageTemplateScope]
	ADD
	CONSTRAINT [DEFAULT_CMS_PageTemplateScope_PageTemplateScopeLastModified]
	DEFAULT ('2/22/2010 9:30:07 AM') FOR [PageTemplateScopeLastModified]
ALTER TABLE [CMS_PageTemplateScope]
	ADD
	CONSTRAINT [DEFAULT_CMS_PageTemplateScope_PageTemplateScopePath]
	DEFAULT ('') FOR [PageTemplateScopePath]
ALTER TABLE [CMS_PageTemplateScope]
	ADD
	CONSTRAINT [DEFAULT_CMS_PageTemplateScope_PageTemplateScopeTemplateID]
	DEFAULT ((0)) FOR [PageTemplateScopeTemplateID]
IF OBJECT_ID(N'[CMS_PageTemplateScope]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplateScope_PageTemplateScopeClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplateScope]
			ADD CONSTRAINT [FK_CMS_PageTemplateScope_PageTemplateScopeClassID_CMS_Class]
			FOREIGN KEY ([PageTemplateScopeClassID]) REFERENCES [CMS_Class] ([ClassID])
END
IF OBJECT_ID(N'[CMS_PageTemplateScope]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Culture]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplateScope_PageTemplateScopeCultureID_CMS_Culture]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplateScope]
			ADD CONSTRAINT [FK_CMS_PageTemplateScope_PageTemplateScopeCultureID_CMS_Culture]
			FOREIGN KEY ([PageTemplateScopeCultureID]) REFERENCES [CMS_Culture] ([CultureID])
END
IF OBJECT_ID(N'[CMS_PageTemplateScope]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplateScope_PageTemplateScopeSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplateScope]
			ADD CONSTRAINT [FK_CMS_PageTemplateScope_PageTemplateScopeSiteID_CMS_Site]
			FOREIGN KEY ([PageTemplateScopeSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[CMS_PageTemplateScope]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_PageTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplateScope_PageTemplateScopeTemplateID_CMS_PageTemplate]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplateScope]
			ADD CONSTRAINT [FK_CMS_PageTemplateScope_PageTemplateScopeTemplateID_CMS_PageTemplate]
			FOREIGN KEY ([PageTemplateScopeTemplateID]) REFERENCES [CMS_PageTemplate] ([PageTemplateID])
END
CREATE INDEX [IX_CMS_PageTemplateScope_PageTemplateScopeClassID]
	ON [CMS_PageTemplateScope] ([PageTemplateScopeClassID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_PageTemplateScope_PageTemplateScopeCultureID]
	ON [CMS_PageTemplateScope] ([PageTemplateScopeCultureID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_PageTemplateScope_PageTemplateScopeLevels]
	ON [CMS_PageTemplateScope] ([PageTemplateScopeLevels])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_PageTemplateScope_PageTemplateScopePath]
	ON [CMS_PageTemplateScope] ([PageTemplateScopePath])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_PageTemplateScope_PageTemplateScopeSiteID]
	ON [CMS_PageTemplateScope] ([PageTemplateScopeSiteID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_PageTemplateScope_PageTemplateScopeTemplateID]
	ON [CMS_PageTemplateScope] ([PageTemplateScopeTemplateID])
	ON [PRIMARY]
