using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSSiteManager_Administration_Permissions_Permissions_Header : CMSPermissionsPage
{
    #region "Variables"

    protected int siteIdQuery = 0;
    protected int siteId = 0;
    enum permissionTypes { DocumentType = 1, Modules, CustomTables };

    #endregion


    #region "Page events"

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        ((Panel)this.CurrentMaster.PanelBody.FindControl("pnlContent")).CssClass = "";
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Setup page title text and image
        CurrentMaster.Title.TitleText = ResHelper.GetString("Administration-Permissions_Header.PermissionsTitle");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Permission/object.png");

        CurrentMaster.Title.HelpTopicName = "permissions_list";
        CurrentMaster.Title.HelpName = "helpTopic";

        lblPermissionType.Text = ResHelper.GetString("Administration-Permissions_Header.PermissionType");
        LabelPermissionMatrix.Text = ResHelper.GetString("Administration-Permissions_Header.PermissionMatrix");

        siteIdQuery = GetSiteID(QueryHelper.GetString("siteid", ""));

        if (siteIdQuery > 0)
        {
            PanelSites.Visible = false;
            lblSite.Visible = false;

            siteId = siteIdQuery;
        }
        else
        {
            // Set site selector
            siteSelector.DropDownSingleSelect.AutoPostBack = true;
            siteSelector.OnlyRunningSites = false;
            siteSelector.AllowAll = false;
            siteSelector.AllowEmpty = false;

            if (!RequestHelper.IsPostBack())
            {
                siteId = CMSContext.CurrentSiteID;
                siteSelector.Value = siteId;

                // Force siteselector to reload
                siteSelector.Reload(false);
            }

            // Get truly selected value
            siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
        }

        if (!RequestHelper.IsPostBack())
        {
            // Initialize permission type drop down
            InitializeDropDownListPermissionType();
        }

        InitializeDropDownListPermissionMatrix();

        moduleSelector.DropDownSingleSelect.AutoPostBack = true;
        docTypeSelector.DropDownSingleSelect.AutoPostBack = true;
        customTableSelector.DropDownSingleSelect.AutoPostBack = true;
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        string id = "0";
        string type = "";
        if (drpPermissionType.SelectedIndex > -1)
        {
            if (drpPermissionType.SelectedValue == permissionTypes.Modules.ToString())
            {
                if (this.moduleSelector.UniSelector.HasData)
                {
                    id = ValidationHelper.GetString(this.moduleSelector.Value, "0");
                }
                type = "r";
            }
            else if (drpPermissionType.SelectedValue == permissionTypes.DocumentType.ToString())
            {
                if (this.docTypeSelector.UniSelector.HasData)
                {
                    id = ValidationHelper.GetString(this.docTypeSelector.Value, "0");
                }
                type = "c";
            }
            else
            {
                if (this.customTableSelector.UniSelector.HasData)
                {
                    id = ValidationHelper.GetString(this.customTableSelector.Value, "0");
                }
                type = "c";
            }
        }

        // Register script for load permission matrix to other frame
        string scriptText = ScriptHelper.GetScript("parent.frames['content'].location = 'Permissions_Matrix.aspx?id=" + id + "&type=" + type + "&siteid=" + siteId + "';");
        ScriptHelper.RegisterStartupScript(this.Page, typeof(string), "InitMatrix", scriptText);
    }

    #endregion


    #region "Private & protected methods"

    private void InitializeDropDownListPermissionType()
    {
        // Initialize drop down list with types
        drpPermissionType.Items.Clear();
        ListItem li = new ListItem(ResHelper.GetString("designertreedata.resources"), permissionTypes.Modules.ToString());
        drpPermissionType.Items.Add(li);
        li = new ListItem(ResHelper.GetString("documenttype_edit_header.documenttypes"), permissionTypes.DocumentType.ToString());
        drpPermissionType.Items.Add(li);
        li = new ListItem(ResHelper.GetString("development.customtables"), permissionTypes.CustomTables.ToString());
        drpPermissionType.Items.Add(li);
    }


    protected void InitializeDropDownListPermissionMatrix()
    {
        if (siteIdQuery == 0)
        {
            siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
        }
        else
        {
            siteId = siteIdQuery;
        }

        string permissionType = null;
        if (drpPermissionType.SelectedIndex > -1)
        {
            permissionType = drpPermissionType.SelectedValue;
        }

        if (siteId > 0 && !String.IsNullOrEmpty(permissionType))
        {
            this.moduleSelector.Visible = (permissionType == permissionTypes.Modules.ToString());
            this.docTypeSelector.Visible = (permissionType == permissionTypes.DocumentType.ToString());
            this.customTableSelector.Visible = (permissionType == permissionTypes.CustomTables.ToString());
        }
    }

    #endregion


    #region "SelectedIndexChanged handlers"

    protected void DropDownListSite_SelectedIndexChanged(object sender, EventArgs e)
    {
        InitializeDropDownListPermissionType();
        drpPermissionType_SelectedIndexChanged(null, null);
    }


    protected void drpPermissionType_SelectedIndexChanged(object sender, EventArgs e)
    {
        InitializeDropDownListPermissionMatrix();
    }

    #endregion
}
