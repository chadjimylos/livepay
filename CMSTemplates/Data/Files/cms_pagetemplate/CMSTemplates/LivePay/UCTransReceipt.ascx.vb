﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports LivePay_ESBBridge

Partial Class CMSTemplates_LivePay_UCTransReceipt
    Inherits CMSUserControl


    Public ReadOnly Property TransGUID() As String
        Get
            Dim sTransID As String = String.Empty
            If Not String.IsNullOrEmpty(Request("TransID")) Then
                sTransID = Request("TransID")
            End If
            Return sTransID
        End Get
    End Property

    Protected ReadOnly Property Result() As String
        Get
            Dim sResult As String = String.Empty
            If Not String.IsNullOrEmpty(Request("Result")) Then
                sResult = Request("Result").ToLower
            End If
            Return sResult
        End Get
    End Property

#Region "Methods"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub


    Public Overloads Sub ReloadData()
        Dim IsLogedIn As Boolean = CMSContext.CurrentUser.IsPublic()
        Dim CustID As Integer = 0
        MessageTitle.InnerHtml = ResHelper.LocalizeString("{$=Πραγματοποιήσατε με επιτυχία την παρακάτω πληρωμή|en-us=Πραγματοποιήσατε με επιτυχία την παρακάτω πληρωμή$}")
        If Me.TransGUID <> String.Empty AndAlso Me.Result = String.Empty Then
            Dim TransactionID As String = "0"
            Dim MerchantName As String = String.Empty
            Dim myGuid As Guid = Nothing
            myGuid = New Guid(Me.TransGUID)

            Dim dt As DataTable = DBConnection.GetMerchantByGUID(myGuid)
            If dt.Rows.Count > 0 Then
                TransactionID = dt.Rows(0)("MerchantTransactionID").ToString
                Session("TransReceipt_TransIDForExport") = TransactionID
                MerchantName = dt.Rows(0)("CompanyName").ToString
            End If

            If TransactionID <> "0" Then
                Dim w As New LivePay_ESBBridge.Bridge
                Dim TransResponse As TransactionDetailsResponse = w.GetTransactionDetails(TransactionID)
                Dim TransInfo As TransactionDetailsInfo = TransResponse.TransactionDetails

                If Not IsNothing(TransInfo) Then
                    CustID = TransInfo.customerId
                    tdDateTime.InnerHtml = TransInfo.transactionDate
                    tdMerchant.InnerHtml = MerchantName
                    tdTransCode.InnerHtml = TransInfo.transactionId
                    tdCustCode.InnerHtml = TransInfo.customerId
                    Dim amount As String = Replace(TransInfo.transactionAmount, ".", ",")
                    Dim FinalAmount As String
                    If amount.Split(",").Length > 1 Then
                        FinalAmount = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
                    ElseIf amount.Split(",").Length = 1 Then
                        FinalAmount = amount & ",00"
                    Else
                        FinalAmount = amount
                    End If
                    tdPrice.InnerHtml = FinalAmount
                    Dim CardNo As String = Replace(TransInfo.cardNumber, " ", String.Empty)
                    tdCard.InnerHtml = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                    CreateCustomFields(TransInfo.merchantId, TransInfo)
                End If
            End If
        End If

        If Result <> String.Empty Then
            imgResult.Src = "/app_themes/LivePay/ErrorIcon.png"
            MessageTitle.Style.Add("color", "#dc2c09")
            MessageTitle.InnerHtml = ResHelper.LocalizeString("{$=Παρουσιάστηκε σφάλμα|en-us=Παρουσιάστηκε σφάλμα$}")
            MainDivForm.Style.Add("display", "none")
            MainDivErrorContainer.Style.Add("display", "")
            Dim ErrorMessage As String = String.Empty
            If Me.Result = "esberror" Then
                ErrorMessage = String.Concat(ErrorMessage, "<div class='txtError'>Το ESB (EUROBANK) παρουσίασε το παρακάτω σφάλμα : </div><div>")
            Else
                ErrorMessage = String.Concat(ErrorMessage, "<div  class='txtError'>General Error:</div><div>")
            End If
            ErrorMessage = String.Concat(ErrorMessage, Session("MakePaymentError"))
            ErrorMessage = String.Concat(ErrorMessage, "</div>")
            MainDivErrorContainer.InnerHtml = ErrorMessage

        End If
        If CustID = 0 Then    '- ----- and o xristis den einai melos
            btnSave.Visible = False
        End If
    End Sub

    Private Sub CreateCustomFields(ByVal MerchantID As String, ByVal TransInfo As TransactionDetailsInfo)

        Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(MerchantID)
        Dim LivePay_MerchantID As Integer = 0
        If dtMerc.Rows.Count > 0 Then
            LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
        End If

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(LivePay_MerchantID)
        Dim dt As DataTable = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim CustomTitle As New HtmlGenericControl("div")
            Dim CustomValue As New HtmlGenericControl("div")
            Dim CustomClear As New HtmlGenericControl("div")
            CustomTitle.Attributes.Add("class", "TransRecLeftRow")
            CustomValue.Attributes.Add("class", "TransRecRightRow")
            CustomClear.Attributes.Add("class", "Clear")
            CustomTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
            Select Case i
                Case 0
                    CustomValue.InnerHtml = TransInfo.info1
                Case 1
                    CustomValue.InnerHtml = TransInfo.info2
                Case 2
                    CustomValue.InnerHtml = TransInfo.info3
                Case 3
                    CustomValue.InnerHtml = TransInfo.info4
                Case 4
                    CustomValue.InnerHtml = TransInfo.info5
                Case 5
                    CustomValue.InnerHtml = TransInfo.info6
                Case 6
                    CustomValue.InnerHtml = TransInfo.info7
                Case 7
                    CustomValue.InnerHtml = TransInfo.info8
                Case 8
                    CustomValue.InnerHtml = TransInfo.info9
                Case 9
                    CustomValue.InnerHtml = TransInfo.info10
            End Select
            With CustomFieldsPanel.Controls
                .Add(CustomTitle)
                .Add(CustomValue)
                .Add(CustomClear)
            End With

        Next
    End Sub

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        Dim TransactionID As String = "0"
        Dim myGuid As Guid = Nothing
        myGuid = New Guid(Me.TransGUID)

        Dim dt As DataTable = DBConnection.GetMerchantByGUID(myGuid)
        If dt.Rows.Count > 0 Then
            TransactionID = dt.Rows(0)("MerchantTransactionID").ToString
            DBConnection.UpdateTransaction(TransactionID, 2, 1)
            Response.Redirect("Home-Page.aspx")
        End If

    End Sub
   
    'Protected Sub btnSaveToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveToPDF.Click
    '    Dim TransactionID As String = "0"
    '    Dim myGuid As Guid = Nothing
    '    myGuid = New Guid(Me.TransGUID)
    '    Dim dt As DataTable = DBConnection.GetMerchantByGUID(myGuid)
    '    If dt.Rows.Count > 0 Then
    '        TransactionID = dt.Rows(0)("MerchantTransactionID").ToString
    '        Session("TransReceipt_TransIDForExport") = TransactionID
    '        Response.Redirect("/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=TransReceipt")
    '    End If


    'End Sub
End Class
