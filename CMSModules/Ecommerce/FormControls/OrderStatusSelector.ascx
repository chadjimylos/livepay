<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrderStatusSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_FormControls_OrderStatusSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%StatusDisplayName%}"
            ObjectType="ecommerce.orderstatus" ResourcePrefix="orderstatusselector"
            SelectionMode="SingleDropDownList" AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
