CREATE TABLE [CMS_Permission] (
		[PermissionID]                  int NOT NULL IDENTITY(1, 1),
		[PermissionDisplayName]         nvarchar(100) NOT NULL,
		[PermissionName]                nvarchar(100) NOT NULL,
		[ClassID]                       int NULL,
		[ResourceID]                    int NULL,
		[PermissionGUID]                uniqueidentifier NOT NULL,
		[PermissionLastModified]        datetime NOT NULL,
		[PermissionDescription]         nvarchar(max) NULL,
		[PermissionDisplayInMatrix]     bit NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Permission]
	ADD
	CONSTRAINT [PK_CMS_Permission]
	PRIMARY KEY
	([PermissionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_Permission]
	ADD
	CONSTRAINT [DEFAULT_CMS_Permission_PermissionDisplayInMatrix]
	DEFAULT ((1)) FOR [PermissionDisplayInMatrix]
IF OBJECT_ID(N'[CMS_Permission]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Permission_ClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_Permission]
			ADD CONSTRAINT [FK_CMS_Permission_ClassID_CMS_Class]
			FOREIGN KEY ([ClassID]) REFERENCES [CMS_Class] ([ClassID])
END
IF OBJECT_ID(N'[CMS_Permission]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Resource]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Permission_ResourceID_CMS_Resource]') IS NULL
BEGIN
		ALTER TABLE [CMS_Permission]
			ADD CONSTRAINT [FK_CMS_Permission_ResourceID_CMS_Resource]
			FOREIGN KEY ([ResourceID]) REFERENCES [CMS_Resource] ([ResourceID])
END
CREATE INDEX [IX_CMS_Permission_ClassID_PermissionName]
	ON [CMS_Permission] ([ClassID], [PermissionName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Permission_ResourceID_PermissionName]
	ON [CMS_Permission] ([ResourceID], [PermissionName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
