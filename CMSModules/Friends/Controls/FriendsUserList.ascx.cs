using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.Community;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.SiteProvider;

public partial class CMSModules_Friends_Controls_FriendsUserList : CMSUserControl
{
    #region "Variables"

    private bool mShowItemAsLink = true;
    private bool mShowAddLink = false;
    private bool mDisplayFilter = true;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Zero rows text
    /// </summary>
    public string ZeroRowsText
    {
        get
        {
            return gridFriends.ZeroRowsText;
        }
        set
        {
            gridFriends.ZeroRowsText = value;
        }
    }


    /// <summary>
    /// Size of the page
    /// </summary>
    public string PageSize
    {
        get
        {
            return gridFriends.PageSize;
        }
        set
        {
            gridFriends.PageSize = value;
        }
    }


    /// <summary>
    /// Determines whether to show add new item link
    /// </summary>
    public bool ShowAddLink
    {
        get
        {
            return mShowAddLink;
        }
        set
        {
            mShowAddLink = value;
        }
    }


    /// <summary>
    /// Determines whether to show items as links
    /// </summary>
    public bool ShowItemAsLink
    {
        get
        {
            return mShowItemAsLink;
        }
        set
        {
            mShowItemAsLink = value;
        }
    }


    /// <summary>
    /// Gets and sets whether to show filter.
    /// </summary>
    public bool DisplayFilter
    {
        get
        {
            return mDisplayFilter;
        }
        set
        {
            mDisplayFilter = value;
            plcFilter.Visible = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            // Stop processing
        }
        else
        {
            // Content is visible only for authenticated users
            if (CMSContext.CurrentUser.IsAuthenticated())
            {
                if (string.IsNullOrEmpty(ZeroRowsText))
                {
                    ZeroRowsText = ResHelper.GetString("friends.nofriendsfound");
                }
                // Register modal dialog JS function
                ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

                gridFriends.OrderBy = "UserName";
                gridFriends.OnExternalDataBound += gridFriends_OnExternalDataBound;
                gridFriends.OnDataReload += new UniGrid.OnDataReloadEventHandler(gridFriends_OnDataReload);
                gridFriends.IsLiveSite = IsLiveSite;
            }
            else
            {
                Visible = false;
            }
        }
    }

    #endregion


    #region "Grid methods"

    protected object gridFriends_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName)
        {
            case "formattedusername":
                DataRowView drv = GetDataRowView((DataControlFieldCell)(sender));

                string userName = ValidationHelper.GetString(drv["UserName"], String.Empty);
                userName = Functions.GetFormattedUserName(userName, this.IsLiveSite);

                int userId = ValidationHelper.GetInteger(drv["UserID"], 0);

                return GetItemText(userId, userName, drv["UserNickName"]);
        }

        return parameter;
    }


    protected DataSet gridFriends_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        DataSet friends = FriendInfoProvider.GetApprovedUserFriends(CMSContext.CurrentUser.UserID, searchBox.WhereCondition, null, currentTopN, "UserID, UserName, UserNickName");

        // Hide filter when not needed
        if (!DataHelper.DataSourceIsEmpty(friends))
        {
            int rowsCount = friends.Tables[0].Rows.Count;

            if ((gridFriends.FilterLimit > 0) && !RequestHelper.IsPostBack() && (rowsCount <= gridFriends.FilterLimit))
            {
                DisplayFilter = false;
            }
        }
        else if (searchBox.FilterIsSet)
        {
            DisplayFilter = true;
        }
        else
        {
            DisplayFilter = false;
        }

        totalRecords = DataHelper.GetItemsCount(friends);
        return friends;
    }


    /// <summary>
    /// Renders row item according to control settings
    /// </summary>
    /// <param name="userId">Selected user id.</param>
    /// <param name="username">User Name</param>
    /// <param name="usernickname">Nickname</param>
    protected string GetItemText(int userId, object username, object usernickname)
    {
        string usrName = username.ToString();
        string nick = HTMLHelper.HTMLEncode(UserInfoProvider.GetFullUserName(usrName, usernickname.ToString()));

        if (ShowItemAsLink)
        {
            return "<a href=\"javascript: window.parent.CloseAndRefresh(" + userId + ", " + ScriptHelper.GetString(HTMLHelper.HTMLEncode(usrName)) + ", " +
                ScriptHelper.GetString(QueryHelper.GetText("mid", String.Empty)) + ", " + ScriptHelper.GetString(QueryHelper.GetText("hidid", String.Empty)) +
                ")\">" + nick + "</a>";
        }

        return nick;
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }

    #endregion
}
