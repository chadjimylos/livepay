<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageTemplate_Frameset.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Development - Page templates</title>
</head>
<frameset border="0" rows="38, *" framespacing="0" id="rowsFrameset">
    <frame name="pt_menu" src="PageTemplate_Menu.aspx" frameborder="0" scrolling="no"
        noresize="noresize" />
    <frameset border="0" cols="270,*,0" framespacing="0" runat="server" id="colsFrameset">
        <frame name="pt_tree" src="PageTemplate_Tree.aspx" frameborder="0" scrolling="no" runat="server" />
        <frame name="pt_edit" src="../blank.htm" frameborder="0" runat="server" />
        <frame name="pt_request" src="../blank.htm" frameborder="0" scrolling="no" noresize="noresize" runat="server" />
    </frameset>
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
</frameset>
</html>
