﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSavedCards.ascx.vb" Inherits="CMSTemplates_LivePay_UCSavedCards" %>
<asp:UpdatePanel ID="uptpanel" runat="server" UpdateMode="Always" >
<ContentTemplate>

<style type="text/css">
.SvdCardsGridHeaderA{margin-left:10px;}
.SrvCards-HeaderCss{margin-top:5px}
.SrcHisPayPagerEmpty th{margin-left:-10px;}
</style>
 <div class="SvdCardsDarkBlueBGTitle">
    <div class="SvdCardsTopTitle"><%= ResHelper.LocalizeString("{$=Αποθηκευμένες Κάρτες|en-us=Saved Cards$}")%></div>
    <div class="SvdCardsContentBG">
        <div class="PL15 SvdCardsTopDescr"><%= ResHelper.LocalizeString("{$=Στην ενότητα αυτή μπορείτε να δείτε τις αποθηκευμένες κάρτες σας|en-us=In this section you can manage your saved Cards.$}")%></div>
        <div class="SrcHisPayMainGrid">
            <div class="SrcHisPayGridBGTop">
                <div class="SrcHisPayGridTopTitle"  ><%=ResHelper.LocalizeString("{$=Αποθηκευμένες Κάρτες|en-us=Saved Cards$}") %></div>
                    <div class="SrcHisPayGridDiv">
                        <asp:GridView ID="GridViewPayments" EmptyDataTemplate-CssClass="SrcHisPayPagerEmpty" PagerStyle-CssClass="SrcHisPayPager" GridLines="None" runat="server"  AutoGenerateColumns="false"  Width="642px" >
                        <EmptyDataRowStyle CssClass="SrcHisPayGridItem SrcHisPayGridHeaderLast" BackColor="White" Font-Bold="true" />
                     <HeaderStyle  BackColor="#f8f8f8" ForeColor="#58595b" Height="35px" HorizontalAlign="Center"/>
                    <AlternatingRowStyle Height="25px" BackColor="#ffffff" ForeColor="#58595b" Font-Size="12px" Font-Names="tahoma" HorizontalAlign="Center"/>
                    <RowStyle Height="25px" BackColor="#ffffff" ForeColor="#58595b" Font-Size="12px" Font-Names="tahoma" HorizontalAlign="Center" />
                        <Columns >
                            <asp:TemplateField   HeaderStyle-CssClass="SrcHisPayGridHeader SvdCardsGridHeaderA" ItemStyle-CssClass="SrcHisPayGridItem" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" >
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbxCard" runat="server" Text='<%#Eval("FriendlyName") %>' ></asp:CheckBox>
                                    <asp:Label ID="lblID" style="display:none" runat="server" Text='<%#Eval("CardID") %>' ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" >
                                <ItemTemplate>
                                    <asp:label ID="lbltype" runat="server" Text='<%#Eval("CardDescr") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField  ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" >
                                <ItemTemplate>
                                    <asp:label ID="lblcardno" runat="server" Text='<%#"xxxxxxxx-xxxx-" & Eval("LastFor") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" HeaderStyle-CssClass="SrcHisPayGridHeaderLast" ItemStyle-CssClass="SrcHisPayGridItemLast" ItemStyle-Width="100px" >
                                <ItemTemplate>
                                     <asp:label ID="lbldtm" runat="server" Text='<%#Eval("InsDate") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        <EmptyDataTemplate>                        
                            <div>
                                <div style="padding-top:5px;padding-left:10px"><%=ResHelper.LocalizeString("{$=Δεν υπάρχουν αποθηκευμένες κάρτες|en-us=No saved cards found$}") %></div>
                            </div>
                        </EmptyDataTemplate>
                       
                    </asp:GridView>
                        <div class="SrcHisPayGridItem SrcHisPayGridItemLast" style="background-color:White;width:630px;text-align:right">
                        <div style="padding-top:5px"><asp:ImageButton ID="btnDeleteCard"   runat="server" />&nbsp;&nbsp;&nbsp;</div> 
                        </div>
                       <div class="SrcHisPayGridItem SrcHisPayGridItemLast" style="background-color:White;width:630px"><div style="padding-top:15px"><font style="font-size:10px">></font><font style="font-size:11px;color:#43474a;font-family:Tahoma"> <%= ResHelper.LocalizeString("{$=Για να προσθέσετε νέα κάρτα θα πρέπει να επιλέξετε Αποθήκευση Κάρτας στο Προφίλ μου κατά την πραγματοποίηση μιας Πληρωμής μέσα στο Live Pay|en-us=To add a New Saved Credit Card you need to select 'Save Card to my Profile'  while making a payment$}")%></div></div>
                        <div class="SrcHisPayGridBGBottom"></div> 
                    </div> 
                </div> 
        </div>
               
                <asp:TextBox ID="HiddenCbxList" style="display:none" runat="server" Text="0"></asp:TextBox>
               
                
                
                
                <div id="BottomDiv" style="display:none" runat="server" class="SrcHisPayGridBGBottom"></div> 
    </div>
    <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
</div> 
</ContentTemplate>
</asp:UpdatePanel>
<script language ="javascript" type="text/javascript" >

    function DeleteCards() {
       
            var HiddenCbxList = document.getElementById('<%=HiddenCbxList.ClientID %>')

            if (HiddenCbxList.value != '0') {

                if (confirm('<%=ResHelper.LocalizeString("{$=Επιθυμείτε την διαγραφή των επιλεγμένων καρτών?|en-us=Do you wish to delete the selected Cards?$}") %>')) {
                    return true
                } else {
                    return false
                }

            } else {
                alert('<%=ResHelper.LocalizeString("{$=Παρακαλώ επιλέξτε Κάρτα|en-us=Please choose Card$}") %>')
                return false
            }
    }


    function SendChbxValue(obj, val) {
        var GetCheckBox = obj
        var HiddenCbxList = document.getElementById('<%=HiddenCbxList.ClientID %>')
        var MyVal = HiddenCbxList.value
        if (GetCheckBox.checked) {
            MyVal = MyVal + ',' + val + '_'
        } else {
            
            MyVal = MyVal.replace("," + val + "_", "")
        }
        HiddenCbxList.value = MyVal
    }

  

   
</script>