<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeFile="Product_List.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Ecommerce_Tools_Products_Product_List"
    Theme="Default" Title="Product list" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/FormControls/PublicStatusSelector.ascx"
    TagName="PublicStatusSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/FormControls/InternalStatusSelector.ascx"
    TagName="InternalStatusSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/FormControls/DepartmentSelector.ascx" TagName="DepartmentSelector"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <table>
        <tr>
            <td>
                <asp:Label ID="lblName" runat="server" CssClass="ContentLabel" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server" CssClass="TextBoxField" MaxLength="450"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNumber" runat="server" CssClass="ContentLabel" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtNumber" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblDepartment" runat="server" CssClass="ContentLabel" EnableViewState="false" />
            </td>
            <td>
                <cms:DepartmentSelector runat="server" ID="departmentElem" AddAllMyRecord="true"
                    AddAllItemsRecord="false" AddNoneRecord="false" UseDepartmentNameForSelection="false"
                    IsLiveSite="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblStoreStatus" runat="server" CssClass="ContentLabel" EnableViewState="false" />
            </td>
            <td>
                <cms:PublicStatusSelector runat="server" ID="publicStatusElem" AddAllItemsRecord="true"
                    AddNoneRecord="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblInternalStatus" runat="server" CssClass="ContentLabel" EnableViewState="false" />
            </td>
            <td>
                <cms:InternalStatusSelector runat="server" ID="internalStatusElem" AddAllItemsRecord="true"
                    AddNoneRecord="false" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblSites" runat="server" CssClass="ContentLabel" EnableViewState="false" />
            </td>
            <td>
                <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton ID="btnFilter" runat="server" CssClass="ContentButton" EnableViewState="false" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" Visible="false" EnableViewState="false" />
    <cms:UniGrid ID="gridData" runat="server" GridName="Product_List.xml" OrderBy="SKUName"
        IsLiveSite="false" Columns="SKUID, SKUName, SKUNumber, SKUPrice, SKUAvailableItems, SKUEnabled" />
</asp:Content>
