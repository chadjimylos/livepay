using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;

public partial class CMSModules_Reporting_Controls_GradientControl : System.Web.UI.UserControl
{
    private string mValue = "";
    private bool mDisableToColorField;


    /// <summary>
    /// Gets or sets the color gradient
    /// </summary>
    public string Value
    {
        get {
            string str = txtFromColor.Text + ";" + txtToColor.Text + ";" + txtAngle.Text;
            return str; 
        }
        set { 
            if (value != null) {
                string[] values = value.Split(';');
                if (values.Length >= 3)
                {
                    txtFromColor.Text = ValidationHelper.GetString(values[0], "");
                    txtToColor.Text = ValidationHelper.GetString(values[1], "");
                    txtAngle.Text = ValidationHelper.GetString(values[2], "");
                }
                mValue = value; //save for later(LoadData) initialization
            }
        }
    }
    

    /// <summary>
    /// If true hides the second color settings
    /// </summary>
    public bool DisableToColorField
    {
        get
        {
        	 return mDisableToColorField; 
        }
        set
        {
        	 mDisableToColorField = value; 
        }
    }
	
	
    /// <summary>
    /// Handles page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RequestHelper.IsPostBack())
        {
            lblFromColor.Text = ResHelper.GetString("rep.graph.FromColor");
            lblToColor.Text = ResHelper.GetString("rep.graph.ToColor");
            lblAngle.Text = ResHelper.GetString("rep.graph.Angle");

            LoadData();

            if (DisableToColorField)
            {
                //lblToColor.Visible = false;
                txtToColor.ReadOnly = true;
            }
        }        
    }


    /// <summary>
    /// Prepares UI and load data from value
    /// </summary>
    private void LoadData()
    {
        // Set by value
        string[] values = mValue.Split(';');

        if (values.Length >= 3)
        {
            txtFromColor.Text = values[0];
            txtToColor.Text = values[1];
            txtAngle.Text = values[2];
        }
    }
}
