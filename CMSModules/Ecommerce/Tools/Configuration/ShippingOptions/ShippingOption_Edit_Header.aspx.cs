using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_ShippingOptions_ShippingOption_Edit_Header : CMSEcommerceConfigurationPage
{
    protected int shippingOptionID = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        shippingOptionID = ValidationHelper.GetInteger(Request.QueryString["ShippingOptionID"], 0);

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }

        string shippingOptionName = "";
        ShippingOptionInfo soi = ShippingOptionInfoProvider.GetShippingOptionInfo(shippingOptionID);
        if (soi != null)
        {
            shippingOptionName = ResHelper.LocalizeString(soi.ShippingOptionDisplayName);
        }
            

        // initializes page title and BreadCrumbs
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("ShippingOption_EditHeader.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/ShippingOptions/ShippingOption_List.aspx";
        pageTitleTabs[0, 2] = "configEdit";
        pageTitleTabs[1, 0] = shippingOptionName;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("ShippingOption_EditHeader.TitleText");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_ShippingOption/object.png");
        this.CurrentMaster.Title.HelpTopicName = "newgeneral_tab2";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Initializes edit menu
    /// </summary>
    protected void InitalizeMenu()
    {
        this.CurrentMaster.Tabs.Tabs = new string[2, 4];
        this.CurrentMaster.Tabs.Tabs[0, 0] = ResHelper.GetString("General.General");
        this.CurrentMaster.Tabs.Tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'newgeneral_tab2');";
        this.CurrentMaster.Tabs.Tabs[0, 2] = "ShippingOption_Edit_General.aspx?shippingOptionID=" + shippingOptionID.ToString();
        this.CurrentMaster.Tabs.Tabs[1, 0] = ResHelper.GetString("ShippingOption_EditHeader.PaymentMethods");
        this.CurrentMaster.Tabs.Tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'payment_methods');";
        this.CurrentMaster.Tabs.Tabs[1, 2] = "ShippingOption_Edit_Payment.aspx?shippingOptionID=" + shippingOptionID.ToString();
        this.CurrentMaster.Tabs.UrlTarget = "shippingOptionContent";
        
       
    }
}
