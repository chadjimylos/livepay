using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.FormEngine;
using CMS.UIControls;

public partial class CMSFormControls_Selectors_AlternativeFormSelection : DesignerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RequestHelper.IsPostBack())
        {
            DataSet dsClasses = AlternativeFormInfoProvider.GetClassesWithAlternativeForms();

            drpClass.DataSource = dsClasses;
            drpClass.DataValueField = "FormClassID";
            drpClass.DataTextField = "ClassName";
            drpClass.DataBind();

            if (!DataHelper.DataSourceIsEmpty(dsClasses))
            {
                // Try to preselect class from drop-down list
                string className = QueryHelper.GetString("classname", string.Empty);
                if (className != string.Empty)
                {
                    drpClass.SelectedIndex = drpClass.Items.IndexOf(drpClass.Items.FindByText(className));
                }
                else
                {
                    drpClass.SelectedIndex = 0;
                }
                // Load alternative forms for selected class
                LoadAltFormsList();
            }

            lblClass.Text = ResHelper.GetString("general.class") + ResHelper.Colon;
            btnOk.Text = ResHelper.GetString("general.ok");
            btnCancel.Text = ResHelper.GetString("general.cancel");
        }

        string txtClientId = QueryHelper.GetString("txtelem", "");
        string lblClientId = QueryHelper.GetString("lblelem", "");
        btnOk.OnClientClick = "SelectCurrentAlternativeForm('" + txtClientId + "','" + lblClientId + "'); return false;";
        btnCancel.OnClientClick = "Cancel(); return false;";

        CurrentMaster.Title.TitleText = ResHelper.GetString("altforms.selectaltform");
        CurrentMaster.Title.TitleImage = GetImageUrl("Design/Selectors/selectaltform.png");

        ltlScript.Text = ScriptHelper.GetScript("var lstAlternativeForms = document.getElementById('" + lstAlternativeForms.ClientID + "');");
    }


    /// <summary>
    /// Fills alternative form list according to selection in class dropdownlist.
    /// </summary>
    protected void LoadAltFormsList()
    {
        int formClassId = ValidationHelper.GetInteger(drpClass.SelectedValue, 0);
        DataSet ds = AlternativeFormInfoProvider.GetForms("FormClassID=" + formClassId, "FormName");

        lstAlternativeForms.Items.Clear();

        if (!DataHelper.DataSourceIsEmpty(ds))
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                AlternativeFormInfo afi = new AlternativeFormInfo(dr);
                if (afi != null)
                {
                    if ((afi.FormDisplayName != String.Empty) && (afi.FormName != String.Empty))
                    {
                        lstAlternativeForms.Items.Add(new ListItem(afi.FormDisplayName, afi.FullName));
                    }
                }
            }
            lstAlternativeForms.SelectedValue = null;
            lstAlternativeForms.DataBind();
        }

        ds.Dispose();
    }


    protected void drpClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Load alternative forms for selected class
        LoadAltFormsList();
    }
}
