using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSModules_Blogs_Tools_Blogs_Header : CMSBlogsPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Intialize the control
        SetupControl();
    }


    /// <summary>
    /// Initializes the controls
    /// </summary>
    private void SetupControl()
    {
        // Set the page title when existing category is being edited
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("blogs.header.title");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Blog/object.png");

        InitalizeMenu();
    }


    /// <summary>
    /// Initialize the tab control on the master page
    /// </summary>
    private void InitalizeMenu()
    {
        // Collect tabs data
        string[,] tabs = new string[2, 4];
        tabs[0, 0] = ResHelper.GetString("blogs.header.comments");
        tabs[0, 2] = "Blogs_Comments_List.aspx";

        tabs[1, 0] = ResHelper.GetString("blogs.header.blogs");
        tabs[1, 2] = "Blogs_Blogs_List.aspx";

        // Set the target iFrame
        this.CurrentMaster.Tabs.UrlTarget = "blogsContent";

        // Assign tabs data
        this.CurrentMaster.Tabs.Tabs = tabs;
    }
}
