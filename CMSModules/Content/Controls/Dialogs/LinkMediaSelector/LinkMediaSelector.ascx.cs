using System;
using System.Web;
using System.Web.UI;
using System.Collections;
using System.Data;
using System.IO;
using System.Text;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.ExtendedControls;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.FileManager;
using CMS.DataEngine;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_Controls_Dialogs_LinkMediaSelector_LinkMediaSelector : LinkMediaSelector
{
    #region "Private variables"

    // Content variables
    private int mSiteID = 0;
    private int mNodeID = 0;

    private bool mIsAction = false;

    private TreeNode mTreeNodeObj = null;
    private AttachmentManager mAttachmentManager = null;
    private AttachmentInfo mCurrentAttachment = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Gets or sets last searched value.
    /// </summary>
    private string LastSearchedValue
    {
        get
        {
            return hdnLastSearchedValue.Value;
        }
        set
        {
            hdnLastSearchedValue.Value = value;
        }
    }


    /// <summary>
    /// Gets current action name.
    /// </summary>
    private string CurrentAction
    {
        get
        {
            return hdnAction.Value.ToLower().Trim();
        }
        set
        {
            hdnAction.Value = value;
        }
    }


    /// <summary>
    /// Gets current action argument value.
    /// </summary>
    private string CurrentArgument
    {
        get
        {
            return hdnArgument.Value;
        }
    }


    /// <summary>
    /// Returns current properties (according to OutputFormat).
    /// </summary>
    protected override ItemProperties Properties
    {
        get
        {
            switch (Config.OutputFormat)
            {
                case OutputFormatEnum.HTMLMedia:
                    return htmlMediaProp;

                case OutputFormatEnum.HTMLLink:
                    return htmlLinkProp;

                case OutputFormatEnum.BBMedia:
                    return bbMediaProp;

                case OutputFormatEnum.BBLink:
                    return bbLinkProp;

                case OutputFormatEnum.NodeGUID:
                    return nodeGuidProp;

                default:
                    if ((Config.CustomFormatCode == "copy") || (Config.CustomFormatCode == "move") || (Config.CustomFormatCode == "link") || (Config.CustomFormatCode == "linkdoc"))
                    {
                        return docCopyMoveProp;
                    }
                    return urlProp;
            }
        }
    }


    /// <summary>
    /// Update panel where properties control resides.
    /// </summary>
    protected override UpdatePanel PropertiesUpdatePanel
    {
        get
        {
            return pnlUpdateProperties;
        }
    }


    /// <summary>
    /// Gets ID of the site files are being displayed for.
    /// </summary>
    private int SiteID
    {
        get
        {
            if (mSiteID == 0)
            {
                mSiteID = siteSelector.SiteID;
            }
            return mSiteID;
        }
        set
        {
            mSiteID = value;
        }
    }


    /// <summary>
    /// Gets name of the site files are being displayed for.
    /// </summary>
    private string SiteName
    {
        get
        {
            return siteSelector.SiteName;
        }
        set
        {
            siteSelector.SiteName = value;
        }
    }


    /// <summary>
    /// Gets or sets ID of the node selected in the content tree.
    /// </summary>
    private int NodeID
    {
        get
        {
            if (mNodeID == 0)
            {
                mNodeID = ValidationHelper.GetInteger(hdnLastNodeSlected.Value, 0);
            }
            return mNodeID;
        }
        set
        {
            mNodeID = value;
            hdnLastNodeSlected.Value = value.ToString();
            mTreeNodeObj = null;
        }
    }


    /// <summary>
    /// Maximum number of tree nodes displayed within the tree.
    /// </summary>
    private int MaxTreeNodes
    {
        get
        {
            return SettingsKeyProvider.GetIntValue(SiteName + ".CMSMaxTreeNodes");
        }
    }


    /// <summary>
    /// Indicates whether the attachments are temporary.
    /// </summary>
    private bool AttachmentsAreTemporary
    {
        get
        {
            return ((Config.AttachmentFormGUID != Guid.Empty) && (Config.AttachmentDocumentID == 0));
        }
    }


    /// <summary>
    /// Gets the node attachments are related to.
    /// </summary>
    private TreeNode TreeNodeObj
    {
        get
        {
            if (mTreeNodeObj == null)
            {
                // Content tab
                if (SourceType == MediaSourceEnum.Content)
                {
                    if (this.NodeID > 0)
                    {
                        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                        tree.CombineWithDefaultCulture = true;
                        this.mTreeNodeObj = DocumentHelper.GetDocument(this.NodeID, TreeProvider.ALL_CULTURES, tree);
                    }
                }
                // Attachments tab
                else if (!AttachmentsAreTemporary)
                {
                    TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                    mTreeNodeObj = DocumentHelper.GetDocument(Config.AttachmentDocumentID, tree);
                }

                mediaView.TreeNodeObj = mTreeNodeObj;
            }
            return mTreeNodeObj;
        }
        set
        {
            mTreeNodeObj = value;
        }
    }


    /// <summary>
    /// Gets manager object for manipulating with attachments.
    /// </summary>
    private AttachmentManager AttachmentManager
    {
        get
        {
            if (mAttachmentManager == null)
            {
                mAttachmentManager = new AttachmentManager();
            }
            return mAttachmentManager;
        }
    }


    /// <summary>
    /// Gets or sets GUID of the recently selected attachment.
    /// </summary>
    private Guid LastAttachmentGuid
    {
        get
        {
            return ValidationHelper.GetGuid(ViewState["LastAttachmentGuid"], Guid.Empty);
        }
        set
        {
            ViewState["LastAttachmentGuid"] = value;
        }
    }


    /// <summary>
    /// Currently selected item.
    /// </summary>
    private AttachmentInfo CurrentAttachmentInfo
    {
        get
        {
            return mCurrentAttachment;
        }
        set
        {
            mCurrentAttachment = value;
        }
    }


    /// <summary>
    /// Indicates whether the asynchronous postback occurs on the page.
    /// </summary>
    private bool IsAsyncPostback
    {
        get
        {
            return ScriptManager.GetCurrent(Page).IsInAsyncPostBack;
        }
    }


    /// <summary>
    /// Indicates whether the post back is result of some hidden action.
    /// </summary>
    private bool IsAction
    {
        get
        {
            return mIsAction;
        }
        set
        {
            mIsAction = value;
        }
    }


    /// <summary>
    /// Indicates if full listing mode is enabled. This mode enables navigation to child and parent folders/documents from current view.
    /// </summary>
    private bool IsFullListingMode
    {
        get
        {
            return mediaView.IsFullListingMode;
        }
        set
        {
            mediaView.IsFullListingMode = value;
        }
    }


    /// <summary>
    /// Gets or sets selected item to colorize.
    /// </summary>
    private Guid ItemToColorize
    {
        get
        {
            return ValidationHelper.GetGuid(ViewState["ItemToColorize"], Guid.Empty);
        }
        set
        {
            ViewState["ItemToColorize"] = value;
        }
    }


    /// <summary>
    /// Gets or sets ID of the node reflecting new root specified by starting path.
    /// </summary>
    private int StartingPathNodeID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["StartingPathNodeID"], 0);
        }
        set
        {
            ViewState["StartingPathNodeID"] = value;
        }
    }


    /// <summary>
    /// Indicates if properties are displayed in full height mode
    /// </summary>
    private bool IsFullDisplay
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["IsFullDisplay"], false);
        }
        set
        {
            ViewState["IsFullDisplay"] = value;
        }
    }


    /// <summary>
    /// Indicates whether the control is displayed as part of the copy/move dialog.
    /// </summary>
    private bool IsCopyMoveLinkDialog
    {
        get
        {
            switch (Config.CustomFormatCode)
            {
                case "copy":
                case "move":
                case "link":
                case "linkdoc":
                case "selectpath":
                case "relationship":
                    return true;

                default:
                    return false;
            }
        }
    }


    /// <summary>
    /// Indicates whether the folder was already selected for the current site.
    /// </summary>
    private bool FolderNotSelectedYet
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["FolderNotSelectedYet"], true);
        }
        set
        {
            ViewState["FolderNotSelectedYet"] = value;
        }
    }

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        menuElem.Config = Config;
        mediaView.Config = Config;

        // Get source type according URL parameters
        SourceType = CMSDialogHelper.GetMediaSource(QueryHelper.GetString("source", "attachments"));
        mediaView.OutputFormat = Config.OutputFormat;

        IsFullListingMode = IsCopyMoveLinkDialog;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // High-light item being edited
        if (ItemToColorize != Guid.Empty)
        {
            ColorizeRow(ItemToColorize.ToString());
        }

        if (!UrlHelper.IsPostback() && (siteSelector.DropDownSingleSelect.SelectedItem == null))
        {
            EnsureLoadedSite();
        }

        // Display info on listing more content
        if (mediaView.ShowParentButton && (StartingPathNodeID != NodeID))
        {
            mediaView.ShowParentButton = IsFullListingMode;
        }
        else
        {
            mediaView.ShowParentButton = false;
        }
        if (!IsCopyMoveLinkDialog && IsFullListingMode && (TreeNodeObj != null))
        {
            string closeLink = "<span class=\"ListingClose\" style=\"cursor: pointer;\" onclick=\"SetAction('closelisting', ''); RaiseHiddenPostBack(); return false;\">" + ResHelper.GetString("general.close") + "</span>";
            string docNamePath = "<span class=\"ListingPath\">" + TreeNodeObj.DocumentNamePath + "</span>";

            string listingMsg = string.Format(ResHelper.GetString("dialogs.content.listingInfo"), docNamePath, closeLink);
            mediaView.DisplayListingInfo(listingMsg);
        }

        if (IsCopyMoveLinkDialog)
        {
            DisplayMediaElements();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!StopProcessing)
        {
            CheckPermissions();

            SetupProperties();

            InitializeDialogs();

            SetupControls();

            EnsureLoadedData();
        }
        else
        {
            siteSelector.StopProcessing = true;
            Visible = false;
        }
    }


    #region "Inherited methods"

    /// <summary>
    /// Initializes its properties according to the URL parameters.
    /// </summary>
    public void InitFromQueryString()
    {
        switch (Config.OutputFormat)
        {
            case OutputFormatEnum.HTMLMedia:
                SelectableContent = SelectableContentEnum.OnlyMedia;
                break;

            case OutputFormatEnum.HTMLLink:
                SelectableContent = SelectableContentEnum.AllContent;
                break;

            case OutputFormatEnum.BBMedia:
                SelectableContent = SelectableContentEnum.OnlyImages;
                break;

            case OutputFormatEnum.BBLink:
                SelectableContent = SelectableContentEnum.AllContent;
                break;

            case OutputFormatEnum.URL:
            case OutputFormatEnum.NodeGUID:
                string content = QueryHelper.GetString("content", "");
                SelectableContent = CMSDialogHelper.GetSelectableContent(content);
                break;
        }
    }


    /// <summary>
    /// Returns selected item parameters as name-value collection.
    /// </summary>
    public void GetSelectedItem()
    {
        // Clear unused information from session
        ClearSelectedItemInfo();
        ClearActionElems();
        if (Properties.Validate())
        {
            // Store tab information in the user's dialogs configuration
            StoreDialogsConfiguration();

            // Get selected item information
            Hashtable properties = Properties.GetItemProperties();

            // Get JavaScript for inserting the item
            string script = GetInsertItem(properties);
            if (!string.IsNullOrEmpty(script))
            {
                ScriptHelper.RegisterStartupScript(Page, typeof(Page), "insertItemScript", ScriptHelper.GetScript(script));
            }
            if ((Config.CustomFormatCode.ToLower() == "copy") || (Config.CustomFormatCode.ToLower() == "move") || (Config.CustomFormatCode.ToLower() == "link") || (Config.CustomFormatCode.ToLower() == "linkdoc"))
            {
                // Reload the iframe
                pnlUpdateProperties.Update();
            }
        }
        else
        {
            // Display error message
            pnlUpdateProperties.Update();
        }
    }

    #endregion


    #region "Dialog configuration"

    /// <summary>
    /// Stores current tab's configuration for the user.
    /// </summary>
    private void StoreDialogsConfiguration()
    {
        UserInfo ui = UserInfoProvider.GetUserInfo(CMSContext.CurrentUser.UserID);
        if (ui != null)
        {
            // Store configuration based on the current tab
            switch (SourceType)
            {
                case MediaSourceEnum.Content:
                    // Actualize configuration
                    ui.UserSettings.UserDialogsConfiguration["content.sitename"] = SiteName;
                    ui.UserSettings.UserDialogsConfiguration["content.path"] = GetContentPath(NodeID);
                    ui.UserSettings.UserDialogsConfiguration["content.viewmode"] = CMSDialogHelper.GetDialogViewMode(menuElem.SelectedViewMode);
                    break;

                case MediaSourceEnum.DocumentAttachments:
                    // Actualize configuration
                    ui.UserSettings.UserDialogsConfiguration["attachments.viewmode"] = CMSDialogHelper.GetDialogViewMode(menuElem.SelectedViewMode);
                    break;
            }
            ui.UserSettings.UserDialogsConfiguration["selectedtab"] = CMSDialogHelper.GetMediaSource(SourceType);

            UserInfoProvider.SetUserInfo(ui);
        }
    }


    /// <summary>
    /// Initializes dialogs according URL configuration, selected item or user configuration.
    /// </summary>
    private void InitializeDialogs()
    {
        if (!UrlHelper.IsPostback())
        {
            LoadDialogConfiguration();

            // Item is selected in the editor
            if (MediaSource != null)
            {
                LoadItemConfiguration();
            }
            else if (!IsCopyMoveLinkDialog)
            {
                LoadUserConfiguration();
            }
        }
    }


    /// <summary>
    /// Loads dialogs according configuration comming from the URL.
    /// </summary>
    private void LoadDialogConfiguration()
    {
        if (SourceType == MediaSourceEnum.Content)
        {
            // Initialize site selector
            siteSelector.StopProcessing = false;
            siteSelector.UniSelector.WhereCondition = GetSiteWhere();

            if (!string.IsNullOrEmpty(Config.ContentSelectedSite))
            {
                contentTree.SiteName = Config.ContentSelectedSite;
                siteSelector.SiteName = Config.ContentSelectedSite;
            }
            else
            {
                // Select default site
                string siteName = CMSContext.CurrentSiteName;
                // Try select current site
                if (!string.IsNullOrEmpty(Config.ContentSelectedSite))
                {
                    contentTree.SiteName = siteName;
                    siteSelector.SiteName = siteName;
                }
                else
                {
                    // Select first site from all sites (no running site case)
                    DataSet ds = SiteInfoProvider.GetAllSites();
                    if (!DataHelper.DataSourceIsEmpty(ds))
                    {
                        siteName = ValidationHelper.GetString(ds.Tables[0].Rows[0]["SiteName"], String.Empty);
                        if (!String.IsNullOrEmpty(siteName))
                        {
                            contentTree.SiteName = siteName;
                            siteSelector.SiteName = siteName;
                        }
                    }
                }
            }

            pnlUpdateSelectors.Update();
        }
    }


    /// <summary>
    /// Gets WHERE condition for available sites according field configuration.
    /// </summary>
    private string GetSiteWhere()
    {
        // Only running sites
        string where = "SiteStatus = 'RUNNING'";

        string siteName = "";

        // First check configuration
        if (Config.ContentSites == AvailableSitesEnum.OnlySingleSite)
        {
            siteName = Config.ContentSelectedSite;
        }
        else if (Config.ContentSites == AvailableSitesEnum.OnlyCurrentSite)
        {
            siteName = CMSContext.CurrentSiteName;
        }

        if (siteName != "")
        {
            // Ensure that selected site is allways loaded (cancel the "only running" condition)
            where = SqlHelperClass.AddWhereCondition("", "SiteName LIKE N'" + siteName + "'");
        }

        // Get only current user's sites
        if (!CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            where = SqlHelperClass.AddWhereCondition(where, "SiteID IN (SELECT SiteID FROM CMS_UserSite WHERE UserID = " + CMSContext.CurrentUser.UserID + ")");
        }

        return where;
    }


    /// <summary>
    /// Loads selected item parameters into the selector.
    /// </summary>
    public void LoadItemConfiguration()
    {
        if (MediaSource != null)
        {
            IsItemLoaded = true;

            switch (MediaSource.SourceType)
            {
                case MediaSourceEnum.Content:
                    siteSelector.SiteID = MediaSource.SiteID;
                    contentTree.SiteName = SiteName;

                    // Try to select node in the tree
                    if (MediaSource.NodeID > 0)
                    {
                        NodeID = MediaSource.NodeID;
                        contentTree.NodeID = NodeID;
                    }
                    break;

                default:
                    break;
            }

            // Reload HTML properties
            if (Config.OutputFormat == OutputFormatEnum.HTMLMedia)
            {
                // Force media properties control to load selected item
                htmlMediaProp.ViewMode = MediaSource.MediaType;
            }

            // Display properties in full size
            if (SourceType == MediaSourceEnum.Content)
            {
                DisplayFull();
            }

            htmlMediaProp.HistoryID = MediaSource.HistoryID;
        }

        // Load properties
        Properties.LoadItemProperties(Parameters);
        pnlUpdateProperties.Update();

        // Remember item to colorize
        ItemToColorize = (SourceType == MediaSourceEnum.Content) ? MediaSource.NodeGuid : MediaSource.AttachmentGuid;
        LastAttachmentGuid = ItemToColorize;

        ClearSelectedItemInfo();
    }


    /// <summary>
    /// Loads dialogs according user's configuration.
    /// </summary>
    private void LoadUserConfiguration()
    {
        if ((CMSContext.CurrentUser.UserSettings.UserDialogsConfiguration != null) &&
            (CMSContext.CurrentUser.UserSettings.UserDialogsConfiguration.ColumnNames != null))
        {
            XmlData dialogConfig = CMSContext.CurrentUser.UserSettings.UserDialogsConfiguration;

            string siteName = "";
            string aliasPath = "";
            string viewMode = "";

            // Store configuration based on the current tab
            switch (SourceType)
            {
                case MediaSourceEnum.Content:
                    siteName = (dialogConfig.ContainsColumn("content.sitename") ? dialogConfig["content.sitename"] : "");
                    aliasPath = (dialogConfig.ContainsColumn("content.path") ? dialogConfig["content.path"] : "");
                    viewMode = (dialogConfig.ContainsColumn("content.viewmode") ? dialogConfig["content.viewmode"] : "");
                    break;

                case MediaSourceEnum.DocumentAttachments:
                    viewMode = (dialogConfig.ContainsColumn("attachments.viewmode") ? dialogConfig["attachments.viewmode"] : "");
                    break;
            }

            // Update dialog configuration (only if ContentSelectedSite is not set)
            if (!string.IsNullOrEmpty(siteName) && string.IsNullOrEmpty(Config.ContentSelectedSite))
            {
                siteSelector.SiteName = siteName;
            }

            if (!string.IsNullOrEmpty(aliasPath))
            {
                if (aliasPath.StartsWith(Config.ContentStartingPath.ToLower()))
                {
                    NodeID = GetContentNodeId(aliasPath);
                }
                else
                {
                    NodeID = GetContentNodeId(Config.ContentStartingPath);
                }

                // Initialize root node
                if (NodeID == 0)
                {
                    NodeID = GetContentNodeId("/");
                    mediaView.ShowParentButton = false;
                }

                // Select and expand node
                contentTree.NodeID = NodeID;
                contentTree.ExpandNodeID = NodeID;
            }
            else
            {
                // Reset selected node to root node
                NodeID = GetContentNodeId("/");
                contentTree.NodeID = NodeID;
                contentTree.ExpandNodeID = NodeID;
                mediaView.ShowParentButton = false;
            }

            if (!string.IsNullOrEmpty(viewMode))
            {
                menuElem.SelectedViewMode = CMSDialogHelper.GetDialogViewMode(viewMode);
            }
        }
        else
        {
            // Initialize site selector
            siteSelector.SiteID = CMSContext.CurrentSiteID;
        }
    }


    /// <summary>
    /// Loads currently selected item.
    /// </summary>
    private void LoadSelectedItem()
    {
        if (CurrentAttachmentInfo != null)
        {
            SelectMediaItem(CurrentAttachmentInfo.AttachmentName, CurrentAttachmentInfo.AttachmentExtension,
                CurrentAttachmentInfo.AttachmentImageWidth, CurrentAttachmentInfo.AttachmentImageHeight, CurrentAttachmentInfo.AttachmentSize, CurrentAttachmentInfo.AttachmentUrl);
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Performs necessary permissions check.
    /// </summary>
    private void CheckPermissions()
    {
        // Check 'READ' permission for the specific document if attachments are being created
        if ((SourceType == MediaSourceEnum.DocumentAttachments) && (!AttachmentsAreTemporary))
        {
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(TreeNodeObj, NodePermissionsEnum.Read) != AuthorizationResultEnum.Allowed)
            {
                string errMsg = string.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), TreeNodeObj.DocumentName);

                // Redirect to access denied page
                AccessDenied(errMsg);
            }
        }
    }


    /// <summary>
    /// Ensures that required data are displayed
    /// </summary>
    private void EnsureLoadedData()
    {
        bool processLoad = true;
        bool isLink = (Config.OutputFormat == OutputFormatEnum.BBLink || Config.OutputFormat == OutputFormatEnum.HTMLLink) ||
            (Config.OutputFormat == OutputFormatEnum.URL && SelectableContent == SelectableContentEnum.AllContent);

        // If all content is selctable do not select root by default - leave selection empty
        if ((SelectableContent == SelectableContentEnum.AllContent) && !isLink && !UrlHelper.IsPostback())
        {
            // Even no file is selected by default load source for the Attachment tab
            processLoad = (SourceType == MediaSourceEnum.DocumentAttachments);

            NodeID = 0;
            contentTree.NodeID = NodeID;
            Properties.ClearProperties(true);
        }

        // Clear properties if link dialog is opened and no link is edited
        if (!UrlHelper.IsPostback() && isLink && !IsItemLoaded)
        {
            Properties.ClearProperties(true);
        }

        // If no action takes place
        if ((CurrentAction == "") ||
            !(isLink && CurrentAction.Contains("edit")))
        {
            if (!CurrentAction.Contains("edit") && processLoad && !(!UrlHelper.IsPostback() && processLoad))
            {
                LoadDataSource();
            }

            // Select folder comming from user/selected item configuration
            if (!UrlHelper.IsPostback() && processLoad)
            {
                HandleFolderAction(NodeID.ToString(), false, false);
            }
        }
    }


    /// <summary>
    /// Ensures that loaded site is really the one selected in the drop-down list.
    /// </summary>
    private void EnsureLoadedSite()
    {
        if (this.SourceType != MediaSourceEnum.DocumentAttachments)
        {
            siteSelector.Reload(true);

            // Name of the site selected in the site DDL
            string siteName = "";

            int siteId = siteSelector.SiteID;
            if (siteId > 0)
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);
                if (si != null)
                {
                    siteName = si.SiteName;
                }
            }

            if (siteName != this.SiteName)
            {
                this.SiteName = siteName;

                // Get site root by default
                TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                if (tree != null)
                {
                    this.TreeNodeObj = tree.SelectSingleNode(this.SiteName, "/", null, false, "cms.root", null, null, 1, true, TreeProvider.SELECTNODES_REQUIRED_COLUMNS);
                    this.NodeID = this.TreeNodeObj.NodeID;

                    InitializeContentTree();

                    contentTree.NodeID = NodeID;
                    contentTree.ExpandNodeID = NodeID;

                    EnsureLoadedData();
                }
            }
        }
    }


    /// <summary>
    /// Makes sure that media elements aren't active while no folder is selected.
    /// </summary>
    private void DisplayMediaElements()
    {
        if (this.FolderNotSelectedYet)
        {
            // Hide media elemnents
            mediaView.StopProcessing = true;
            mediaView.Visible = false;

            // Disable New folder button
            ScriptHelper.RegisterStartupScript(Page, typeof(Page), "DisableNewFolderOnLoad", ScriptHelper.GetScript("if( typeof(DisableNewFolderBtn) == 'function' ) { DisableNewFolderBtn(); }"));
        }
        else if (!mediaView.Visible)
        {
            mediaView.Visible = true;
            mediaView.Reload();
        }
    }


    /// <summary>
    /// Initializes properties controls.
    /// </summary>
    private void SetupProperties()
    {
        htmlLinkProp.Visible = false;
        htmlMediaProp.Visible = false;
        bbLinkProp.Visible = false;
        bbMediaProp.Visible = false;
        urlProp.Visible = false;
        docCopyMoveProp.Visible = false;

        Properties.Visible = true;

        htmlLinkProp.StopProcessing = !htmlLinkProp.Visible;
        htmlMediaProp.StopProcessing = !htmlMediaProp.Visible;
        bbLinkProp.StopProcessing = !bbLinkProp.Visible;
        bbMediaProp.StopProcessing = !bbMediaProp.Visible;
        urlProp.StopProcessing = !urlProp.Visible;
        nodeGuidProp.StopProcessing = !nodeGuidProp.Visible;
        docCopyMoveProp.StopProcessing = !docCopyMoveProp.Visible;

        Properties.Config = Config;
    }


    /// <summary>
    /// Initializes additional controls
    /// </summary>
    private void SetupControls()
    {
        // Generate permanent URLs whenever node GUID output required        
        if (Config.OutputFormat != OutputFormatEnum.NodeGUID)
        {
            UsePermanentUrls = SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSUsePermanentURLs");
        }
        else
        {
            // Select current site and disable change
            siteSelector.SiteID = SiteID = CMSContext.CurrentSiteID;
            siteSelector.UserId = CMSContext.CurrentUser.UserID;
            siteSelector.Enabled = false;

        }

        if (SourceType != MediaSourceEnum.DocumentAttachments)
        {
            siteSelector.DropDownSingleSelect.AutoPostBack = true;
            siteSelector.UniSelector.OnSelectionChanged += UniSelector_OnSelectionChanged;
            siteSelector.DropDownSingleSelect.CssClass = "DialogSiteDropdown";
        }
        else
        {
            siteSelector.StopProcessing = true;
            pnlUpdateSelectors.Visible = false;
        }

        mediaView.UsePermanentUrls = UsePermanentUrls;
        // Set editor client id for properties
        Properties.EditorClientID = Config.EditorClientID;

        InitializeMenuElement();
        InitializeDesignScripts();

        mediaView.IsLiveSite = IsLiveSite;
        mediaView.SelectableContent = SelectableContent;
        mediaView.SourceType = SourceType;
        mediaView.ViewMode = menuElem.SelectedViewMode;
        mediaView.ResizeToHeight = Config.ResizeToHeight;
        mediaView.ResizeToMaxSideSize = Config.ResizeToMaxSideSize;
        mediaView.ResizeToWidth = Config.ResizeToWidth;
        mediaView.AtachmentNodeParentID = Config.AttachmentParentID;
        mediaView.ListReloadRequired += new MediaView.OnListReloadRequired(mediaView_ListReloadRequired);

        Properties.IsLiveSite = IsLiveSite;
        Properties.SourceType = SourceType;

        if (!IsAsyncPostback)
        {
            // Initialize scripts
            InitializeControlScripts();

            if (UrlHelper.IsPostback() && (SourceType != MediaSourceEnum.DocumentAttachments))
            {
                UniSelector_OnSelectionChanged(this, null);
            }
        }

        // Based on the required source type perform setting of necessary controls
        if (SourceType == MediaSourceEnum.Content)
        {
            if (!IsAsyncPostback)
            {
                // Initialize content tree control
                InitializeContentTree();
            }
            else
            {
                contentTree.Visible = false;
                contentTree.StopProcessing = true;
            }
        }
        else
        {
            // Hide and disable content related controls
            HideContentControls();
        }

        // If folder was changed reset current page index for control displaying content
        switch (this.CurrentAction)
        {
            case "morecontentselect":
            case "contentselect":
            case "parentselect":
                ResetPageIndex();
                break;

            default:
                break;
        }
    }


    /// <summary>
    /// Initialize design jQuery scripts
    /// </summary>
    private void InitializeDesignScripts()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("setTimeout('InitializeDesign();',50);");
        sb.Append("$j(window).resize(function() { InitializeDesign(); });");
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "designScript", ScriptHelper.GetScript(sb.ToString()));
    }


    /// <summary>
    /// Initializes menu element.
    /// </summary>
    private void InitializeMenuElement()
    {
        // Let child controls now what the source type is
        menuElem.IsCopyMoveLinkDialog = IsCopyMoveLinkDialog;
        menuElem.DisplayMode = DisplayMode;
        menuElem.IsLiveSite = IsLiveSite;
        menuElem.SourceType = SourceType;
        menuElem.ResizeToHeight = Config.ResizeToHeight;
        menuElem.ResizeToMaxSideSize = Config.ResizeToMaxSideSize;
        menuElem.ResizeToWidth = Config.ResizeToWidth;

        // Based on the required source type perform setting of necessary controls
        if (SourceType == MediaSourceEnum.Content)
        {
            menuElem.NodeID = NodeID;
        }
        else
        {
            // Initialize menu element for attachments
            menuElem.DocumentID = Config.AttachmentDocumentID;
            menuElem.FormGUID = Config.AttachmentFormGUID;
            menuElem.ParentNodeID = Config.AttachmentParentID;
        }
        menuElem.UpdateViewMenu();
    }


    /// <summary>
    /// Initializes all the script required for communication between controls.
    /// </summary>
    private void InitializeControlScripts()
    {
        // SetAction function setting action name and passed argument
        string setAction = "function SetAction(action, argument) {                                              " +
                           "    var hdnAction = document.getElementById('" + hdnAction.ClientID + "');     " +
                           "    var hdnArgument = document.getElementById('" + hdnArgument.ClientID + "'); " +
                           @"    if ((hdnAction != null) && (hdnArgument != null)) {                             
                                   if (action != null) {                                                       
                                       hdnAction.value = action;                                               
                                   }                                                                           
                                   if (argument != null) {                                                     
                                       hdnArgument.value = argument;                                           
                                   }                                                                           
                               }                                                                               
                           }                                                                                    ";

        // Prepare for upload
        string refreshType = CMSDialogHelper.GetMediaSource(SourceType);
        string cmdName = (SourceType == MediaSourceEnum.DocumentAttachments) ? "attachment" : "content";

        string initRefresh = @" function InitRefresh_" + refreshType + "(message, fullRefresh, itemInfo, action) {      " +
                             @" if((message != null) && (message != ''))                                   
                                     {                                                                          
                                        window.alert(message);                                                  
                                     }                                                                          
                                     else                                                                       
                                     {
                                        if(action == 'insert')
                                        {
                                            SetAction('" + cmdName + "created', itemInfo);                              " +
                             @"         }
                                        else if(action == 'update')
                                        {
                                            SetAction('" + cmdName + "updated', itemInfo);                              " +
                             @"         }                                                                               
                                        else if(action == 'refresh')
                                        {
                                            SetAction('" + cmdName + "edit', itemInfo);                                 " +
                             @"         }                                                                                                                                                                   
                                        RaiseHiddenPostBack();                                                  
                                     }                                                                          
                                   }                                                                                    ";

        setAction += initRefresh;

        // Get reffernce causing postback to hidden button
        string postBackRef = ControlsHelper.GetPostBackEventReference(hdnButton, "");
        string raiseOnAction = " function RaiseHiddenPostBack(){" + postBackRef + ";}\n";

        ltlScript.Text = ScriptHelper.GetScript(setAction + raiseOnAction);
    }


    /// <summary>
    /// Resolves macros used in update script according specified item information.
    /// </summary>
    /// <param name="attachment">Attachment information.</param>
    /// <param name="url">URL of the specified attachment.</param>
    /// <param name="argSet">Set of arguments holding additional information on updated item.</param>
    private string ResolveUpdateScript(AttachmentInfo attachment, string url, string argSet)
    {
        string newViewAction = "javascript: window.open('@URL@'); return false;";
        string newSelectAction = "ColorizeRow('" + attachment.AttachmentGUID + "'); SetSelectAction(" + ScriptHelper.GetString(argSet + "|" + url) + "); return false;";
        string newEditAction = "EditImage(\"AttachmentGUID=" + attachment.AttachmentGUID +
            ((attachment.AttachmentLastHistoryID > 0) ? "&versionHistoryId=" + attachment.AttachmentLastHistoryID : "") + "\"); return false;";
        string customJavaScript = "btnEdit.attr('onclick', '').unbind('click').bind('click', function () { " +
            "if($j(this).hasClass('Edited')) { " + newEditAction + " } else { $j(this).attr('class', '@EDITCLASS@'); return false; } } ); " +
            "if($j('body').hasClass('Gecko') || $j('body').hasClass('Gecko3')) { btnEdit.attr('class', '@EDITCLASS@'); };";

        string ext = "." + attachment.AttachmentExtension.ToLower().TrimStart('.');

        string thumbUrl = this.mediaView.GetItemUrl(argSet, 0, 0, 48, false);

        string updateScript = "";
        if (menuElem.SelectedViewMode == DialogViewModeEnum.ListView)
        {
            string iconUrl = GetFileIconUrl(ext, "list");

            // Get thumbnail dimensions
            int[] dims = ImageHelper.EnsureImageDimensions(0, 0, 200, attachment.AttachmentImageWidth, attachment.AttachmentImageHeight);

            customJavaScript = "var btnEdit = $j(\"#@GUID@ input[id$='action_edit']\");" + customJavaScript.Replace("@EDITCLASS@", "UnigridActionButton Edited");

            updateScript = UPDT_SCRIPT_LIST;
            updateScript = updateScript.Replace("@THUMBWIDTH@", dims[0].ToString()).Replace("@THUMBHEIGHT@", dims[1].ToString());
            updateScript = updateScript.Replace("@NEWICONURL@", iconUrl);
        }
        else if (menuElem.SelectedViewMode == DialogViewModeEnum.TilesView)
        {
            // Get thumbnail dimensions
            int[] dims = ImageHelper.EnsureImageDimensions(0, 0, 200, attachment.AttachmentImageWidth, attachment.AttachmentImageHeight);

            customJavaScript = "var btnEdit = $j(\"#@GUID@ input[id$='btnEdit']\");" + customJavaScript.Replace("@EDITCLASS@", "Edited");

            updateScript = UPDT_SCRIPT_TILES;
            updateScript = updateScript.Replace("@THUMBWIDTH@", dims[0].ToString()).Replace("@THUMBHEIGHT@", dims[1].ToString());
            updateScript = updateScript.Replace("@MAXSIDESIZE@", "48");
        }
        else
        {
            int[] dims = CMSDialogHelper.GetThumbImageDimensions(attachment.AttachmentImageHeight, attachment.AttachmentImageWidth, 95, 160);

            customJavaScript = "var btnEdit = $j(\"#@GUID@ input[id$='btnEdit']\");" + customJavaScript.Replace("@EDITCLASS@", "Edited");

            updateScript = UPDT_SCRIPT_THUMBS;
            updateScript = updateScript.Replace("@WIDTH@", dims[1].ToString()).Replace("@HEIGHT@", dims[0].ToString());
        }
        updateScript = updateScript.Replace("@CUSTOMSCRIPT@", customJavaScript);
        updateScript = updateScript.Replace("@VIEWACTION@", newViewAction);
        updateScript = updateScript.Replace("@URL@", UrlHelper.RemoveQuery(url)).Replace("@THUMBURL@", UrlHelper.RemoveQuery(thumbUrl));
        updateScript = updateScript.Replace("@GUID@", attachment.AttachmentGUID.ToString()).Replace("@ACTION@", newSelectAction).Replace("@NEWGUID@", Guid.NewGuid().ToString());

        // Update List & Tiles view specific information
        if ((menuElem.SelectedViewMode == DialogViewModeEnum.ListView) || (menuElem.SelectedViewMode == DialogViewModeEnum.TilesView))
        {
            // Update size after conversion
            long size = ValidationHelper.GetLong(attachment.AttachmentSize, 0);
            updateScript = updateScript.Replace("@NEWSIZE@", DataHelper.GetSizeString(size)).Replace("@NEWEXT@", ext);
        }

        return updateScript;
    }


    /// <summary>
    /// Loads all files for the view control.
    /// </summary>
    private void LoadDataSource()
    {
        if (SourceType == MediaSourceEnum.Content)
        {
            LoadContentDataSource(LastSearchedValue);
        }
        else
        {
            LoadAttachmentsDataSource(LastSearchedValue);
        }
    }


    private void mediaView_ListReloadRequired()
    {
        LoadDataSource();
    }


    /// <summary>
    /// Performs actions necessary to select particular item from a list.
    /// </summary>
    private void SelectMediaItem(string argument)
    {
        if (!string.IsNullOrEmpty(argument))
        {
            string[] argArr = argument.Split('|');
            if (argArr.Length >= 2)
            {
                // Get information from argument
                string name = argArr[0];
                string ext = argArr[1];
                int imageWidth = ValidationHelper.GetInteger(argArr[2], 0);
                int imageHeight = ValidationHelper.GetInteger(argArr[3], 0);
                int nodeID = ValidationHelper.GetInteger(argArr[7], NodeID);
                long size = ValidationHelper.GetLong(argArr[argArr.Length - 2], 0);
                string url = argArr[argArr.Length - 1];
                string aliasPath = null;

                // Do not update properties when selecting recently edited image item
                bool avoidPropUpdate = false;

                // Remember last selected attachment GUID
                if (SourceType == MediaSourceEnum.DocumentAttachments)
                {
                    Guid attGuid = ValidationHelper.GetGuid(argArr[4], Guid.Empty);

                    avoidPropUpdate = (LastAttachmentGuid == attGuid);

                    LastAttachmentGuid = attGuid;
                    ItemToColorize = LastAttachmentGuid;
                }
                else
                {
                    aliasPath = argArr[10];
                    Guid nodeAttGuid = ValidationHelper.GetGuid(argArr[11], Guid.Empty);
                    Properties.SiteDomainName = mediaView.SiteObj.DomainName;

                    avoidPropUpdate = (ItemToColorize == nodeAttGuid);

                    ItemToColorize = nodeAttGuid;
                    if (ItemToColorize == Guid.Empty)
                    {
                        ItemToColorize = ValidationHelper.GetGuid(argArr[6], Guid.Empty);
                    }
                }

                avoidPropUpdate = (avoidPropUpdate && IsEditImage);
                if (!avoidPropUpdate)
                {
                    if (SourceType == MediaSourceEnum.DocumentAttachments)
                    {
                        int versionHistoryId = 0;

                        if (TreeNodeObj != null)
                        {
                            // Get the node workflow
                            WorkflowManager wm = new WorkflowManager(TreeNodeObj.TreeProvider);
                            WorkflowInfo wi = wm.GetNodeWorkflow(TreeNodeObj);
                            if (wi != null)
                            {
                                // Get the document version
                                versionHistoryId = TreeNodeObj.DocumentCheckedOutVersionHistoryID;
                            }
                        }

                        MediaItem item = InitializeMediaItem(name, ext, imageWidth, imageHeight, size, url, null, versionHistoryId, nodeID, aliasPath);

                        SelectMediaItem(item);
                    }
                    else
                    {
                        // Select item
                        SelectMediaItem(name, ext, imageWidth, imageHeight, size, url, null, nodeID, aliasPath);
                    }
                }
            }
        }
    }


    /// <summary>
    /// Performs actions necessary to select particular item from a list.
    /// </summary>
    private void SelectMediaItem(string docName, string url, string aliasPath)
    {
        SelectMediaItem(docName, null, 0, 0, 0, url, null, NodeID, aliasPath);
    }


    /// <summary>
    /// Clears hidden control elements fo future use.
    /// </summary>
    private void ClearActionElems()
    {
        CurrentAction = "";
        hdnArgument.Value = "";
    }


    /// <summary>
    /// Displays properties in full size.
    /// </summary>
    private void DisplayFull()
    {
        if (divDialogView.Attributes["class"] == "DialogViewContent")
        {
            divDialogView.Attributes["class"] = "DialogElementHidden";
            divDialogResizer.Attributes["class"] = "DialogElementHidden";
            divDialogProperties.Attributes["class"] = "DialogPropertiesFullSize";

            if (IsFullDisplay)
            {
                pnlUpdateView.Update();
                pnlUpdateProperties.Update();
            }
            else
            {
                pnlUpdateContent.Update();
                IsFullDisplay = true;
            }
        }
    }


    /// <summary>
    /// Displays properties in default size.
    /// </summary>
    private void DisplayNormal()
    {
        if (divDialogView.Attributes["class"] == "DialogElementHidden")
        {
            divDialogView.Attributes["class"] = "DialogViewContent";
            divDialogResizer.Attributes["class"] = "DialogResizerVLine";
            divDialogProperties.Attributes["class"] = "DialogProperties";

            if (IsFullDisplay)
            {
                pnlUpdateContent.Update();
                IsFullDisplay = false;
            }
            else
            {
                pnlUpdateView.Update();
                pnlUpdateProperties.Update();
            }
        }
    }


    /// <summary>
    /// Ensures that filter is no more applied.
    /// </summary>
    private void ResetSearchFilter()
    {
        mediaView.ResetSearch();
        LastSearchedValue = "";
    }


    /// <summary>
    /// Ensures first page is displayed in the control displaying the content.
    /// </summary>
    private void ResetPageIndex()
    {
        mediaView.ResetPageIndex();
    }

    #endregion


    #region "Content methods"

    /// <summary>
    /// Hides and disables content related controls.
    /// </summary>
    private void HideContentControls()
    {
        pnlLeftContent.Visible = false;
        plcSeparator.Visible = false;
        pnlRightContent.CssClass = "DialogCompleteBlock";
        siteSelector.StopProcessing = true;
        contentTree.StopProcessing = true;
    }


    /// <summary>
    /// Initializes content tree element.
    /// </summary>
    private void InitializeContentTree()
    {
        contentTree.Visible = true;

        contentTree.DeniedNodePostback = false;
        contentTree.AllowMarks = true;
        contentTree.NodeTextTemplate = "<span class=\"ContentTreeItem\" onclick=\"SelectNode(##NODEID##, this); SetAction('contentselect', '##NODEID##|##NODECHILDNODESCOUNT##'); RaiseHiddenPostBack(); return false;\">##ICON##<span class=\"Name\">##NODENAME##</span></span>";
        contentTree.SelectedNodeTextTemplate = "<span id=\"treeSelectedNode\" class=\"ContentTreeSelectedItem\" onclick=\"SelectNode(##NODEID##, this); SetAction('contentselect', '##NODEID##|##NODECHILDNODESCOUNT##'); RaiseHiddenPostBack(); return false;\">##ICON##<span class=\"Name\">##NODENAME##</span></span>";
        contentTree.MaxTreeNodeText = "<span class=\"ContentTreeItem\" onclick=\"SetAction('morecontentselect', ##PARENTNODEID##); RaiseHiddenPostBack(); return false;\"><span class=\"Name\" style=\"font-style: italic;\">" + ResHelper.GetString("ContentTree.SeeListing") + "</span></span>";
        contentTree.IsLiveSite = IsLiveSite;
        contentTree.SelectOnlyPublished = IsLiveSite;
        contentTree.SelectPublishedData = IsLiveSite;

        contentTree.SiteName = SiteName;

        // Starting path node ID
        StartingPathNodeID = GetStartNodeId();
    }


    /// <summary>
    /// Returns ID of the starting node according current starting path settings.
    /// </summary>
    private int GetStartNodeId()
    {
        if (!string.IsNullOrEmpty(Config.ContentStartingPath))
        {
            contentTree.Path = Config.ContentStartingPath;
        }
        else if (!string.IsNullOrEmpty(CMSContext.CurrentUser.UserStartingAliasPath))
        {
            contentTree.Path = CMSContext.CurrentUser.UserStartingAliasPath;
        }
        return GetContentNodeId(contentTree.Path);
    }


    /// <summary>
    /// Returns path of the node specified by its ID.
    /// </summary>
    /// <param name="nodeId">ID of the node.</param>
    private int GetParentNodeID(int nodeId)
    {
        if (nodeId > 0)
        {
            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
            if (tree != null)
            {
                // Get node and return its alias path
                TreeNode node = tree.SelectSingleNode(nodeId);
                if (node != null)
                {
                    return node.NodeParentID;
                }
            }
        }

        return 0;
    }

    /// <summary>
    /// Returns path of the node specified by its ID.
    /// </summary>
    /// <param name="nodeId">ID of the node.</param>
    private string GetContentPath(int nodeId)
    {
        if (nodeId > 0)
        {
            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
            if (tree != null)
            {
                // Get node and return its alias path
                TreeNode node = tree.SelectSingleNode(nodeId);
                if (node != null)
                {
                    return ((node.NodeChildNodesCount == 0) ? TreePathUtils.GetParentPath(node.NodeAliasPath) : node.NodeAliasPath).ToLower();
                }
            }
        }

        return string.Empty;
    }


    /// <summary>
    /// Returns ID of the content node specified by its alias path.
    /// </summary>
    /// <param name="aliasPath">Alias path of the node.</param>
    private int GetContentNodeId(string aliasPath)
    {
        if (!string.IsNullOrEmpty(aliasPath))
        {
            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
            if (tree != null)
            {
                // Return node's ID
                TreeNode node = tree.SelectSingleNode(SiteName, aliasPath, CMSContext.PreferredCultureCode);
                if (node != null)
                {
                    return node.NodeID;
                }
            }
        }

        return 0;
    }


    /// <summary>
    /// Applies loaded nodes to the view control.
    /// </summary>
    /// <param name="nodes">Nodes to load.</param>
    private void LoadNodes(DataSet nodes)
    {
        bool originalNotEmpty = !DataHelper.DataSourceIsEmpty(nodes);
        if (!DataHelper.DataSourceIsEmpty(nodes))
        {
            mediaView.DataSource = nodes;
        }
        else if (originalNotEmpty && IsLiveSite)
        {
            mediaView.InfoText = ResHelper.GetString("dialogs.document.NotAuthorizedToViewAny");
        }
    }


    /// <summary>
    /// Gets all child nodes in the specified parent path.
    /// </summary>
    /// <param name="searchText">Text to filter searched nodes.</param>
    /// <param name="tree">Tree provider used to obtain nodes.</param>
    /// <param name="parentAliasPath">Alias path of the parent.</param>
    /// <param name="siteName">Name of the related site.</param>
    private DataSet GetNodes(string searchText, TreeProvider tree, string parentAliasPath, string siteName)
    {
        // Create WHERE condition
        string where = "(NodeAliasPath <> '/')";
        if (!string.IsNullOrEmpty(searchText))
        {
            string searchTextSafe = searchText.Replace("'", "''");
            where += " AND ((AttachmentName LIKE N'%" + searchTextSafe + "%') OR (DocumentName LIKE N'%" + searchTextSafe + "%'))";
        }

        // If not all content is selectable and no additional content being displayed
        if ((SelectableContent != SelectableContentEnum.AllContent) && !IsFullListingMode)
        {
            where += " AND (ClassName = 'CMS.File')";
        }

        string columns = "ClassDisplayName, AttachmentName, AttachmentExtension, AttachmentImageWidth, AttachmentImageHeight, NodeSiteID," +
             " SiteName, NodeGUID, DocumentUrlPath, NodeAlias, NodeAliasPath, AttachmentGUID, AttachmentID, DocumentName, AttachmentSize," +
             " NodeClassID, DocumentModifiedWhen, NodeACLID, NodeChildNodesCount, DocumentCheckedOutVersionHistoryID, NodeOwner";

        int topN = mediaView.CurrentTopN;

        // Get files
        tree.SelectQueryName = "selectattachments";
        DataSet nodes = null;
        if (IsLiveSite)
        {
            // Get published files
            columns = TreeProvider.SELECTNODES_REQUIRED_COLUMNS + ", " + columns;
            nodes = tree.SelectNodes(siteName, parentAliasPath.TrimEnd('/') + "/%", TreeProvider.ALL_CULTURES, true, null, where, "NodeOrder", 1, true, topN, columns);
        }
        else
        {
            // Get latest files
            columns = DocumentHelper.GETDOCUMENTS_REQUIRED_COLUMNS + ", " + columns;
            nodes = DocumentHelper.GetDocuments(siteName, parentAliasPath.TrimEnd('/') + "/%", TreeProvider.ALL_CULTURES, true, null, where, "NodeOrder", 1, false, topN, columns, tree);
        }

        // Check permissions
        return TreeSecurityProvider.FilterDataSetByPermissions(nodes, NodePermissionsEnum.Read, CMSContext.CurrentUser);
    }


    /// <summary>
    /// Loads all files for the view control.
    /// </summary>
    /// <param name="searchText">Text to filter loaded files.</param>
    private void LoadContentDataSource(string searchText)
    {
        DataSet nodes = null;

        // Load data
        if (NodeID > 0)
        {
            // Get selected node            
            TreeNode node = TreeNodeObj;
            if ((TreeNodeObj != null) && !(Config.OutputFormat == OutputFormatEnum.NodeGUID && node.NodeSiteID != CMSContext.CurrentSiteID))
            {
                // Get selected node site info
                SiteInfo si = SiteInfoProvider.GetSiteInfo(node.NodeSiteID);
                if (si != null)
                {
                    // List view
                    if (node.NodeClassName.ToLower() != "cms.file")
                    {
                        // Check permissions
                        if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.ExploreTree) == AuthorizationResultEnum.Allowed || !IsLiveSite)
                        {
                            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                            tree.UseCache = false;
                            nodes = GetNodes(searchText, tree, node.NodeAliasPath, si.SiteName);

                            LoadNodes(nodes);

                            // If all content selectable
                            bool selectableAll = SelectableContent == SelectableContentEnum.AllContent;
                            if (selectableAll && !IsFullListingMode && (IsAction || !UrlHelper.IsPostback()))
                            {
                                string url = mediaView.GetContentItemUrl(node.NodeGUID, node.DocumentUrlPath, node.NodeAlias,
                                    node.NodeAliasPath, 0, 0, 0, true);

                                ItemToColorize = node.NodeGUID;

                                SelectMediaItem(node.DocumentName, url, node.NodeAliasPath);
                            }

                            // Display full-size properties if detailed view required
                            if (!IsCopyMoveLinkDialog && !IsFullListingMode && selectableAll && (node.NodeChildNodesCount == 0))
                            {
                                DisplayFull();
                            }
                            else
                            {
                                DisplayNormal();
                            }
                        }
                        else
                        {
                            mediaView.InfoText = ResHelper.GetString("dialogs.document.NotAuthorizedToExpolore");
                        }
                    }
                    else
                    {
                        // Check permissions
                        if ((CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Allowed))
                        {
                            // Get attachment info and initialize displayed attachment properties
                            Guid attachmentGUID = ValidationHelper.GetGuid(node.GetValue("FileAttachment"), Guid.Empty);
                            if (attachmentGUID != Guid.Empty)
                            {
                                // Get the attachment
                                TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                                tree.UseCache = false;
                                AttachmentInfo atInfo = DocumentHelper.GetAttachment(node, attachmentGUID, tree, false);
                                if (atInfo != null)
                                {
                                    // Get the data
                                    string extension = atInfo.AttachmentExtension;
                                    bool isContentFile = (node.NodeClassName.ToLower() == "cms.file");

                                    if (CMSDialogHelper.IsItemSelectable(SelectableContent, extension, isContentFile))
                                    {
                                        // Set 'get file path'
                                        atInfo.AttachmentUrl = mediaView.GetContentItemUrl(node.NodeGUID, node.DocumentUrlPath, node.NodeAlias, node.NodeAliasPath, 0, 0, 0, false);

                                        CurrentAttachmentInfo = atInfo;

                                        if (!IsCopyMoveLinkDialog && !IsFullListingMode && node.NodeChildNodesCount == 0)
                                        {
                                            // Display properties in full size
                                            DisplayFull();
                                        }
                                        else
                                        {
                                            if (node.NodeChildNodesCount == 0)
                                            {
                                                // Load child nodes                                            
                                                nodes = GetNodes(searchText, tree, node.NodeAliasPath, si.SiteName);

                                                LoadNodes(nodes);
                                            }

                                            DisplayNormal();
                                        }
                                    }
                                    else
                                    {
                                        mediaView.InfoText = ResHelper.GetString("dialogs.item.notselectable");

                                        DisplayNormal();
                                    }
                                }
                            }
                            else
                            {
                                DisplayNormal();
                            }
                        }
                        else
                        {
                            DisplayNormal();

                            mediaView.InfoText = ResHelper.GetString("dialogs.document.NotAuthorizedToViewNode");
                        }
                    }
                }
            }
        }

        mediaView.DataSource = nodes;
    }


    /// <summary>
    /// Handles actions related to the folders.
    /// </summary>
    /// <param name="argument">Argument related to the folder action.</param>
    /// <param name="reloadTree">Indicates if the content tree should be reloaded.</param>
    private void HandleFolderAction(string argument, bool reloadTree)
    {
        HandleFolderAction(argument, reloadTree, true);
    }


    /// <summary>
    /// Handles actions related to the folders.
    /// </summary>
    /// <param name="argument">Argument related to the folder action.</param>
    /// <param name="isNewFolder">Indicates if is new folder</param>
    /// <param name="callSelection">Indicates if selection should be called</param>
    private void HandleFolderAction(string argument, bool reloadTree, bool callSelection)
    {
        NodeID = ValidationHelper.GetInteger(argument, 0);

        // Update new folder information
        menuElem.NodeID = NodeID;
        menuElem.UpdateActionsMenu();

        // Reload content tree if new folder was created
        if (reloadTree)
        {
            InitializeContentTree();

            // Fill with new info
            contentTree.NodeID = NodeID;
            contentTree.ExpandNodeID = NodeID;

            contentTree.ReloadData();
            pnlUpdateTree.Update();

            ScriptHelper.RegisterStartupScript(Page, typeof(Page), "EnsureTopWindow", ScriptHelper.GetScript("if (self.focus) { self.focus(); }"));
        }

        // Load new data 
        LoadDataSource();

        // Load selected item
        if (CurrentAttachmentInfo != null)
        {
            string fileName = Path.GetFileNameWithoutExtension(CurrentAttachmentInfo.AttachmentName);

            if (callSelection)
            {
                SelectMediaItem(fileName, CurrentAttachmentInfo.AttachmentExtension, CurrentAttachmentInfo.AttachmentImageWidth,
                    CurrentAttachmentInfo.AttachmentImageHeight, CurrentAttachmentInfo.AttachmentSize, CurrentAttachmentInfo.AttachmentUrl);
            }

            ItemToColorize = CurrentAttachmentInfo.AttachmentGUID;
            ColorizeRow(ItemToColorize.ToString());
        }
        else
        {
            ColorizeLastSelectedRow();
        }

        // Get parent node ID info
        int parentId = 0;
        if (StartingPathNodeID != NodeID)
        {
            parentId = GetParentNodeID(NodeID);
        }
        mediaView.ShowParentButton = (parentId > 0);
        mediaView.NodeParentID = parentId;

        // Reload view control's content
        mediaView.Reload();
        pnlUpdateView.Update();

        ClearActionElems();
    }


    /// <summary>
    /// Handles actions occuring when new content (CMS.File) document was created.
    /// </summary>
    /// <param name="argument">Argument holding information on new document node ID.</param>
    private void HandleContentFileCreatedAction(string argument)
    {
        string[] argArr = argument.Split('|');
        if (argArr.Length == 1)
        {
            HandleFolderAction(argArr[0], true);
        }
    }


    /// <summary>
    /// Handles attachment edit action.
    /// </summary>
    /// <param name="argument">Attachment GUID comming from view control.</param>
    private void HandleContentEdit(string argument)
    {
        IsEditImage = true;

        if (!string.IsNullOrEmpty(argument))
        {
            string[] argArr = argument.Split('|');

            Guid attachmentGuid = ValidationHelper.GetGuid(argArr[0], Guid.Empty);

            // Node ID was specified
            int nodeId = 0;
            if (argArr.Length == 2)
            {
                nodeId = ValidationHelper.GetInteger(argArr[1], 0);
            }

            AttachmentInfo ai = AttachmentManager.GetAttachmentInfo(attachmentGuid, SiteName);
            if (ai != null)
            {
                // Get attachment node by ID
                TreeNode node = null;
                if (nodeId > 0)
                {
                    node = TreeHelper.SelectSingleNode(nodeId);
                }
                else
                {
                    node = TreeHelper.SelectSingleDocument(ai.AttachmentDocumentID);
                }

                if (node != null)
                {
                    // Check node site
                    if (node.NodeSiteID != CMSContext.CurrentSiteID)
                    {
                        mediaView.SiteObj = SiteInfoProvider.GetSiteInfo(node.NodeSiteID);
                    }

                    // Get node URL
                    string url = mediaView.GetContentItemUrl(node.NodeGUID, node.DocumentUrlPath, node.NodeAlias, node.NodeAliasPath, 0, 0, 0, false);

                    // Update properties if node is currently selected
                    if (attachmentGuid == ItemToColorize)
                    {
                        SelectMediaItem(ai.AttachmentName, ai.AttachmentExtension, ai.AttachmentImageWidth, ai.AttachmentImageHeight, ai.AttachmentSize, url);
                    }

                    // Update select action to reflect changes made during editing
                    UpdateContentSelectScript(ai, node, url);
                }
            }
        }

        ClearActionElems();
    }


    /// <summary>
    /// Updates JavaScript used to cause asynchronous post-back when item is selected in view.
    /// </summary>
    /// <param name="att">Updated content file.</param>
    /// <param name="node">Tree node</param>
    /// <param name="url">URL of updated content file.</param>
    private void UpdateContentSelectScript(AttachmentInfo att, TreeNode node, string url)
    {
        string argSet = node.DocumentName + "|" + att.AttachmentExtension + "|" + att.AttachmentImageWidth + "|" + att.AttachmentImageHeight +
                        "|" + node.NodeSiteID + "|" + mediaView.SiteObj.SiteName + "|" + node.NodeGUID + "|" + node.NodeID + "|" + node.DocumentUrlPath +
                        "|" + node.NodeAlias + "|" + node.NodeAliasPath + "|" + att.AttachmentGUID + "|" + att.AttachmentSize;

        string docNameScript = "";
        string docName = node.DocumentName.Replace("." + att.AttachmentExtension.TrimStart('.'), "");
        string fileName = Path.GetFileNameWithoutExtension(att.AttachmentName);
        if (this.menuElem.SelectedViewMode == DialogViewModeEnum.ThumbnailsView)
        {
            fileName = node.DocumentName;
        }

        // Document name is different than attachment name so display both
        if ((this.SourceType == MediaSourceEnum.Content) && (this.menuElem.SelectedViewMode != DialogViewModeEnum.ThumbnailsView))
        {
            if (this.menuElem.SelectedViewMode == DialogViewModeEnum.TilesView)
            {
                // Script to remove original
                docNameScript = "if((docNameSpan!=null) && (docNameSpan.length>0)) { docNameSpan.remove(); }" +
                            "if((docNameBr!=null) && (docNameBr.length>0)) { docNameBr.remove(); }";
            }

            if (docName.ToLower() != fileName.ToLower())
            {
                fileName = HTMLHelper.HTMLEncode(att.AttachmentName);
                if (this.menuElem.SelectedViewMode == DialogViewModeEnum.ListView)
                {
                    fileName = node.DocumentName + " (" + fileName + ")";
                }
                else if (this.menuElem.SelectedViewMode == DialogViewModeEnum.TilesView)
                {
                    // Add document name label at the top of info area
                    docNameScript += "fileNameSpan.before('<span id=\"" + Guid.NewGuid() + "_lblDocumentName\" class=\"DialogTileTitleBold\">" + node.DocumentName + "</span><br>');";
                }
            }
        }

        string updateScript = ResolveUpdateScript(att, url, argSet);
        updateScript = updateScript.Replace("@NEWNAME@", fileName) + docNameScript;

        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "UpdateSelectAction", ScriptHelper.GetScript(updateScript));
    }


    /// <summary>
    /// Ensures content tree is refreshed when new folder is created in Copy/Move dialog.
    /// </summary>
    private void RefreshContentTree()
    {
        // Refresh content tree
        string refScript = @"   var wopener = (window.top.opener ? window.top.opener : window.top.dialogArguments);
                                if (wopener == null) {
                                    wopener = opener;
                                }              
                                if (wopener.parent != null) {
                                    if (wopener.parent.frames['contenttree'] != null) {
                                        if (wopener.parent.frames['contenttree'].RefreshTree != null) {
                                            wopener.parent.frames['contenttree'].RefreshTree();
                                        }
                                    }
                                }";
        ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "RefreshContentTree", ScriptHelper.GetScript(refScript));
    }

    #endregion


    #region "Attachment methods"

    /// <summary>
    /// Loads all attachments for the view control.
    /// </summary>
    /// <param name="searchText">Text to filter loaded files.</param>
    private void LoadAttachmentsDataSource(string searchText)
    {
        DataSet attachments = null;

        int topN = mediaView.CurrentTopN;

        // Only unsorted attachments are being displayed
        string where = "(AttachmentIsUnsorted = 1)";

        if (!string.IsNullOrEmpty(searchText))
        {
            where += " AND (AttachmentName LIKE N'%" + searchText.Replace("'", "''") + "%')";
        }

        // Get document attachments
        if (Config.AttachmentDocumentID != 0)
        {
            if (TreeNodeObj != null)
            {
                // Check permissions
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(TreeNodeObj, NodePermissionsEnum.Read) == AuthorizationResultEnum.Allowed)
                {
                    TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                    tree.UseCache = false;
                    attachments = DocumentHelper.GetAttachments(TreeNodeObj, where, "AttachmentOrder, AttachmentName", false, tree, topN);
                }
            }
        }
        // Get form attachments 
        else if (AttachmentsAreTemporary)
        {
            where += " AND (AttachmentFormGUID = '" + Config.AttachmentFormGUID + "')";

            attachments = AttachmentManager.GetAttachments(where, "AttachmentOrder, AttachmentName", false, topN);
        }

        mediaView.DataSource = attachments;
    }


    /// <summary>
    /// Checks attachment permissions.
    /// </summary>
    private string CheckAttachmentPermissions()
    {
        string message = "";

        // For new document
        if (Config.AttachmentFormGUID != Guid.Empty)
        {
            if (Config.AttachmentParentID == 0)
            {
                message = "Node parent node ID has to be set.";
            }

            if (!RaiseOnCheckPermissions("Create", this))
            {
                if (!CMSContext.CurrentUser.IsAuthorizedToCreateNewDocument(Config.AttachmentParentID, "CMS.File"))
                {
                    message = ResHelper.GetString("attach.actiondenied");
                }
            }
        }
        // For existing document
        else if (Config.AttachmentDocumentID > 0)
        {
            // Get document
            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
            TreeNode node = DocumentHelper.GetDocument(Config.AttachmentDocumentID, tree);
            if (node == null)
            {
                message = "Given document doesn't exist!";
            }

            if (!RaiseOnCheckPermissions("Modify", this))
            {
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) != AuthorizationResultEnum.Allowed)
                {
                    message = ResHelper.GetString("attach.actiondenied");
                }
            }
        }

        return message;
    }


    /// <summary>
    /// Handles new attachment create action.
    /// </summary>
    /// <param name="argument">Argument comming from upload control.</param>
    private void HandleAttachmentCreatedAction(string argument)
    {
        HandleAttachmentAction(argument, false);

        // Reload view
        LoadDataSource();

        mediaView.Reload();
        pnlUpdateView.Update();
    }


    /// <summary>
    /// Handles attachment update action.
    /// </summary>
    /// <param name="argument">Argument comming from upload control.</param>
    private void HandleAttachmentUpdatedAction(string argument)
    {
        HandleAttachmentAction(argument, true);
    }


    /// <summary>
    /// Handles attachment edit action.
    /// </summary>
    /// <param name="argument">Attachment GUID comming from view control.</param>
    private void HandleAttachmentEdit(string argument)
    {
        IsEditImage = true;

        if (!string.IsNullOrEmpty(argument))
        {
            string[] argArr = argument.Split('|');

            Guid attachmentGuid = ValidationHelper.GetGuid(argArr[0], Guid.Empty);

            AttachmentInfo ai = null;

            int versionHistoryId = TreeNodeObj.DocumentCheckedOutVersionHistoryID;
            if (versionHistoryId == 0)
            {
                ai = AttachmentManager.GetAttachmentInfo(attachmentGuid, CMSContext.CurrentSiteName);
            }
            else
            {
                VersionManager vm = new VersionManager(TreeNodeObj.TreeProvider);
                if (vm != null)
                {
                    IDataClass dc = vm.GetAttachmentVersion(versionHistoryId, attachmentGuid);
                    if ((dc == null) || (dc.IsEmpty()))
                    {
                        ai = null;
                    }
                    else
                    {
                        ai = new AttachmentInfo(dc.DataRow, ConnectionHelper.GetConnection());
                        ai.AttachmentID = ValidationHelper.GetInteger(dc.DataRow["AttachmentHistoryID"], 0);
                    }
                    if (ai != null)
                    {
                        ai.AttachmentLastHistoryID = versionHistoryId;
                    }
                }
            }

            if (ai != null)
            {
                string nodeAliasPath = "";
                if (TreeNodeObj != null)
                {
                    nodeAliasPath = TreeNodeObj.NodeAliasPath;
                }

                string url = mediaView.GetAttachmentItemUrl(ai.AttachmentGUID, ai.AttachmentName, nodeAliasPath, ai.AttachmentImageHeight, ai.AttachmentImageWidth, 0);

                if (LastAttachmentGuid == attachmentGuid)
                {
                    SelectMediaItem(ai.AttachmentName, ai.AttachmentExtension, ai.AttachmentImageWidth, ai.AttachmentImageHeight, ai.AttachmentSize, url);
                }

                // Update select action to reflect changes made during editing
                UpdateAtachmentSelectScript(ai, url);
            }
        }

        ClearActionElems();
    }


    /// <summary>
    /// Updates JavaScript used to cause asynchronous post-back when item is selected in view.
    /// </summary>
    /// <param name="att">Updated atachment file.</param>
    /// <param name="url">URL of updated attachment file.</param>
    private void UpdateAtachmentSelectScript(AttachmentInfo att, string url)
    {
        string name = AttachmentHelper.GetFullFileName(Path.GetFileNameWithoutExtension(att.AttachmentName), att.AttachmentExtension);

        string argSet = name + "|" + att.AttachmentExtension + "|" + att.AttachmentImageWidth + "|" + att.AttachmentImageHeight +
                        "|" + att.AttachmentGUID + "|" + att.AttachmentSiteID + "|" + att.AttachmentFormGUID +
                        "|" + att.AttachmentDocumentID + "|" + att.AttachmentSize;

        string fileName = att.AttachmentName;

        // No extension in the file name unless in thumbnails view mode
        if (this.menuElem.SelectedViewMode != DialogViewModeEnum.ThumbnailsView)
        {
            fileName = Path.GetFileNameWithoutExtension(fileName);
        }

        string updateScript = ResolveUpdateScript(att, url, argSet);
        updateScript = updateScript.Replace("@NEWNAME@", fileName);

        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "UpdateSelectAction", ScriptHelper.GetScript(updateScript));
    }


    /// <summary>
    /// Handles attachment action.
    /// </summary>
    /// <param name="argument">Argument comming from upload control.</param>
    private void HandleAttachmentAction(string argument, bool isUpdate)
    {
        // Get attachment URL first
        Guid attachmentGuid = ValidationHelper.GetGuid(argument, Guid.Empty);
        if (attachmentGuid != Guid.Empty)
        {
            // Get attachment info
            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);

            // Ensure site information
            SiteInfo si = CMSContext.CurrentSite;
            if ((TreeNodeObj != null) && (si.SiteID != TreeNodeObj.NodeSiteID))
            {
                si = SiteInfoProvider.GetSiteInfo(TreeNodeObj.NodeSiteID);
            }

            AttachmentInfo ai = DocumentHelper.GetAttachment(attachmentGuid, tree, si.SiteName, false);
            if (ai != null)
            {
                string nodeAliasPath = (TreeNodeObj != null) ? TreeNodeObj.NodeAliasPath : null;

                if (CMSDialogHelper.IsItemSelectable(SelectableContent, ai.AttachmentExtension))
                {
                    // Get attachment URL
                    string url = mediaView.GetAttachmentItemUrl(ai.AttachmentGUID, ai.AttachmentName, nodeAliasPath, 0, 0, 0);

                    // Remember last selected attachment GUID
                    if (SourceType == MediaSourceEnum.DocumentAttachments)
                    {
                        LastAttachmentGuid = ai.AttachmentGUID;
                    }

                    // Get the node workflow
                    int versionHistoryId = 0;
                    if (TreeNodeObj != null)
                    {
                        WorkflowManager wm = new WorkflowManager(TreeNodeObj.TreeProvider);
                        WorkflowInfo wi = wm.GetNodeWorkflow(TreeNodeObj);
                        if (wi != null)
                        {
                            // Ensure the document version
                            VersionManager vm = new VersionManager(TreeNodeObj.TreeProvider);
                            versionHistoryId = vm.EnsureVersion(TreeNodeObj, TreeNodeObj.IsPublished);
                        }
                    }

                    MediaItem item = InitializeMediaItem(ai.AttachmentName, ai.AttachmentExtension, ai.AttachmentImageWidth, ai.AttachmentImageHeight, ai.AttachmentSize, url, null, versionHistoryId, 0, "");

                    SelectMediaItem(item);

                    ItemToColorize = attachmentGuid;

                    ColorizeRow(ItemToColorize.ToString());
                }

                mediaView.InfoText = (isUpdate ? ResHelper.GetString("dialogs.attachment.updated") : ResHelper.GetString("dialogs.attachment.created"));

                pnlUpdateView.Update();
            }
        }

        ClearActionElems();
    }


    /// <summary>
    /// Hadnles actions occuring when attachment is moved.
    /// </summary>
    /// <param name="argument">Argument holding information on attachment being moved.</param>
    /// <param name="action">Action specifying whether the attachment is moved up/down.</param>
    private void HandleAttachmentMoveAction(string argument, string action)
    {
        // Check permissions
        string errMsg = CheckAttachmentPermissions();

        if (errMsg == "")
        {
            Guid attachmentGuid = ValidationHelper.GetGuid(argument, Guid.Empty);
            if (attachmentGuid != Guid.Empty)
            {
                // Move temporary attachement
                if (!AttachmentsAreTemporary)
                {
                    if (action == "attachmentmoveup")
                    {
                        DocumentHelper.MoveAttachmentUp(attachmentGuid, TreeNodeObj);
                    }
                    else
                    {
                        DocumentHelper.MoveAttachmentDown(attachmentGuid, TreeNodeObj);
                    }
                }
                else
                {
                    if (action == "attachmentmoveup")
                    {
                        AttachmentManager.MoveAttachmentUp(attachmentGuid, 0);
                    }
                    else
                    {
                        AttachmentManager.MoveAttachmentDown(attachmentGuid, 0);
                    }
                }

                // Reload data
                LoadDataSource();

                plcError.Visible = false;

                mediaView.Reload();
            }
        }
        else
        {
            // Display error
            lblError.Text = errMsg;
            plcError.Visible = true;
        }

        ClearActionElems();

        ColorizeLastSelectedRow();

        pnlUpdateView.Update();
    }


    /// <summary>
    /// Hadnles actions occuring when some attachment is being removed.
    /// </summary>
    /// <param name="argument">Argument holding information on attachment.</param>
    private void HandleDeleteAttachmentAction(string argument)
    {
        string errMsg = CheckAttachmentPermissions();

        if (errMsg == "")
        {
            Guid attachmentGuid = ValidationHelper.GetGuid(argument, Guid.Empty);
            if (attachmentGuid != Guid.Empty)
            {
                if (!AttachmentsAreTemporary)
                {
                    TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);

                    DocumentHelper.DeleteAttachment(TreeNodeObj, attachmentGuid, tree);
                }
                else
                {
                    // Delete temporary attachment
                    AttachmentManager.DeleteTemporaryAttachment(attachmentGuid, CMSContext.CurrentSiteName);
                }

                // Reload data
                LoadDataSource();
                mediaView.Reload();

                // Selected attachment was removed
                if (LastAttachmentGuid == attachmentGuid)
                {
                    // Reset properties
                    Properties.ClearProperties();
                    pnlUpdateProperties.Update();
                }
                else
                {
                    ColorizeLastSelectedRow();
                }
            }
        }
        else
        {
            // Display error
            lblError.Text = errMsg;
            plcError.Visible = true;
        }

        pnlUpdateView.Update();
    }

    #endregion


    #region "Common event methods"

    /// <summary>
    /// Handles actions occuring when some text is searched.
    /// </summary>
    /// <param name="argument">Argument holding information on searched text.</param>
    private void HandleSearchAction(string argument)
    {
        LastSearchedValue = argument;

        // Load new data filtered by searched text 
        LoadDataSource();

        // Reload content
        mediaView.Reload();
        pnlUpdateView.Update();

        // Keep focus in search text box
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "SetSearchFocus", ScriptHelper.GetScript("setTimeout('SetSearchFocus();', 200);"));
    }


    /// <summary>
    /// Handles actions occuring when some item is selected.
    /// </summary>
    /// <param name="argument">Argument holding information on selected item.</param>
    private void HandleSelectAction(string argument)
    {
        // Create new selected media item
        SelectMediaItem(argument);

        // Forget recent action
        ClearActionElems();
    }


    /// <summary>
    /// Handles actions occuring when some item in copy/move/link/select path dialog is selected.
    /// </summary>
    private void HandleDialogSelect()
    {
        if (TreeNodeObj != null)
        {
            string columns = "ClassDisplayName, AttachmentName, AttachmentExtension, AttachmentImageWidth, AttachmentImageHeight, NodeSiteID," +
                 " SiteName, NodeGUID, DocumentUrlPath, NodeAlias, NodeAliasPath, AttachmentGUID, AttachmentID, DocumentName, AttachmentSize, NodeClassID, DocumentModifiedWhen, NodeACLID, NodeChildNodesCount, DocumentCheckedOutVersionHistoryID";

            // Get files
            TreeNodeObj.TreeProvider.SelectQueryName = "selectattachments";

            DataSet nodeDetails = null;
            if (IsLiveSite)
            {
                // Get published files
                columns = TreeProvider.SELECTNODES_REQUIRED_COLUMNS + ", " + columns;
                nodeDetails = TreeNodeObj.TreeProvider.SelectNodes(SiteName, TreeNodeObj.NodeAliasPath, TreeProvider.ALL_CULTURES, false, null, null, "DocumentName", 1, true, 1, columns);
            }
            else
            {
                // Get latest files
                columns = DocumentHelper.GETDOCUMENTS_REQUIRED_COLUMNS + ", " + columns;
                nodeDetails = DocumentHelper.GetDocuments(SiteName, TreeNodeObj.NodeAliasPath, TreeProvider.ALL_CULTURES, false, null, null, "DocumentName", 1, false, 1, columns, TreeNodeObj.TreeProvider);
            }

            // If node details exists
            if (!DataHelper.IsEmpty(nodeDetails))
            {
                DataRow nodeDetailRow = nodeDetails.Tables[0].Rows[0];

                string ext = ValidationHelper.GetString(nodeDetailRow["AttachmentExtension"], "");
                int imageWidth = ValidationHelper.GetInteger(nodeDetailRow["AttachmentImageWidth"], 0);
                int imageHeight = ValidationHelper.GetInteger(nodeDetailRow["AttachmentImageHeight"], 0);
                long size = ValidationHelper.GetLong(nodeDetailRow["AttachmentSize"], 0);

                string arg = mediaView.GetArgumentSet(nodeDetailRow);
                string url = mediaView.GetItemUrl(arg, 0, 0, 300, false);

                SelectMediaItem(TreeNodeObj.DocumentName, ext, imageWidth, imageHeight, size, url, null, NodeID, TreeNodeObj.NodeAliasPath);
            }
        }

        // Do NOT select anything
        ItemToColorize = Guid.Empty;
        ClearColorizedRow();

        // Forget recent action
        ClearActionElems();
    }

    #endregion


    #region "Event handlers"

    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        IsAction = true;
        FolderNotSelectedYet = true;

        // Update information on current site 
        SiteID = siteSelector.SiteID;
        if (SiteID > 0)
        {
            mediaView.SiteObj = SiteInfoProvider.GetSiteInfo(SiteID);
        }

        // Reset selected node to root node
        NodeID = StartingPathNodeID = GetStartNodeId();
        if (NodeID == 0)
        {
            NodeID = GetContentNodeId("/");
            mediaView.ShowParentButton = false;
        }

        if (SelectableContent != SelectableContentEnum.AllContent)
        {
            contentTree.NodeID = NodeID;
            contentTree.ExpandNodeID = NodeID;

            // Reload media view for new site
            LoadDataSource();
        }
        else
        {
            mediaView.DataSource = null;
        }

        // Update information on parent node ID for new folder creation
        menuElem.NodeID = NodeID;
        menuElem.UpdateActionsMenu();

        // Reload content tree for new site
        contentTree.SiteName = siteSelector.SiteName;
        InitializeContentTree();
        pnlUpdateTree.Update();

        // Load selected item
        if (CurrentAttachmentInfo != null)
        {
            SelectMediaItem(CurrentAttachmentInfo.AttachmentName, CurrentAttachmentInfo.AttachmentExtension,
                CurrentAttachmentInfo.AttachmentImageWidth, CurrentAttachmentInfo.AttachmentImageHeight, CurrentAttachmentInfo.AttachmentSize, CurrentAttachmentInfo.AttachmentUrl);
        }

        // Setup media view
        mediaView.Reload();
        pnlUpdateView.Update();

        // Setup properties
        DisplayNormal();
        Properties.ClearProperties();
        pnlUpdateProperties.Update();
    }


    /// <summary>
    /// Behaves as mediator in communication line between control taking action and the rest of the same level controls.
    /// </summary>
    protected void hdnButton_Click(object sender, EventArgs e)
    {
        IsAction = true;

        switch (CurrentAction)
        {
            case "insertitem":
                GetSelectedItem();
                break;

            case "search":
                HandleSearchAction(CurrentArgument);
                break;

            case "select":
                HandleSelectAction(CurrentArgument);
                break;

            case "morecontentselect":
            case "contentselect":
                FolderNotSelectedYet = false;

                ResetSearchFilter();

                string[] argArr = CurrentArgument.Split('|');

                int childNodesCnt = 0;
                if (argArr.Length == 2)
                {
                    childNodesCnt = ValidationHelper.GetInteger(argArr[1], 0);
                }

                if ((Config.OutputFormat == OutputFormatEnum.BBLink) || (Config.OutputFormat == OutputFormatEnum.HTMLLink))
                {
                    CurrentAttachmentInfo = null;
                }

                // If more content is requested
                IsFullListingMode = (!IsFullListingMode ? ((CurrentAction == "morecontentselect") || (childNodesCnt > MaxTreeNodes)) : IsFullListingMode);
                HandleFolderAction(argArr[0], IsFullListingMode);

                if (IsCopyMoveLinkDialog)
                {
                    HandleDialogSelect();
                }
                break;

            case "parentselect":
                ResetSearchFilter();

                HandleFolderAction(CurrentArgument, true);

                if (IsCopyMoveLinkDialog)
                {
                    HandleDialogSelect();
                }
                break;

            case "refreshtree":
                ResetSearchFilter();
                HandleFolderAction(CurrentArgument, true);
                break;

            case "contentcreated":
                HandleContentFileCreatedAction(CurrentArgument);

                if (IsCopyMoveLinkDialog)
                {
                    HandleDialogSelect();
                }
                break;

            case "closelisting":
                IsFullListingMode = false;
                HandleFolderAction(NodeID.ToString(), false);
                break;

            case "newfolder":
                ResetSearchFilter();
                HandleFolderAction(CurrentArgument, true);

                if (IsCopyMoveLinkDialog)
                {
                    // Refresh content tree when new folder is created in Copy/Move dialog
                    RefreshContentTree();
                    HandleDialogSelect();
                }
                break;

            case "cancelfolder":
                ScriptHelper.RegisterStartupScript(Page, typeof(Page), "EnsureTopWindow", ScriptHelper.GetScript("if (self.focus) { self.focus(); }"));
                ClearActionElems();
                break;

            case "attachmentmoveup":
                HandleAttachmentMoveAction(CurrentArgument, CurrentAction);
                break;

            case "attachmentmovedown":
                HandleAttachmentMoveAction(CurrentArgument, CurrentAction);
                break;

            case "attachmentdelete":
                HandleDeleteAttachmentAction(CurrentArgument);
                break;

            case "attachmentcreated":
                HandleAttachmentCreatedAction(CurrentArgument);
                break;

            case "attachmentupdated":
                HandleAttachmentUpdatedAction(CurrentArgument);
                break;

            case "attachmentedit":
                HandleAttachmentEdit(CurrentArgument);
                break;

            case "contentedit":
                HandleContentEdit(CurrentArgument);
                break;

            default:
                ColorizeLastSelectedRow();
                pnlUpdateView.Update();
                break;
        }
    }

    #endregion
}
