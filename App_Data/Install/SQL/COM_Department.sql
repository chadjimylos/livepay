CREATE TABLE [COM_Department] (
		[DepartmentID]                    int NOT NULL IDENTITY(1, 1),
		[DepartmentName]                  nvarchar(200) NOT NULL,
		[DepartmentDisplayName]           nvarchar(200) NOT NULL,
		[DepartmentDefaultTaxClassID]     int NULL,
		[DepartmentGUID]                  uniqueidentifier NOT NULL,
		[DepartmentLastModified]          datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_Department]
	ADD
	CONSTRAINT [PK_COM_Department]
	PRIMARY KEY
	NONCLUSTERED
	([DepartmentID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_Department]') IS NOT NULL
	AND OBJECT_ID(N'[COM_TaxClass]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Department_DepartmentDefaultTaxClassID_COM_TaxClass]') IS NULL
BEGIN
		ALTER TABLE [COM_Department]
			ADD CONSTRAINT [FK_COM_Department_DepartmentDefaultTaxClassID_COM_TaxClass]
			FOREIGN KEY ([DepartmentDefaultTaxClassID]) REFERENCES [COM_TaxClass] ([TaxClassID])
END
CREATE INDEX [IX_COM_Department_DepartmentDefaultTaxClassID]
	ON [COM_Department] ([DepartmentDefaultTaxClassID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_Department_DepartmentDisplayName]
	ON [COM_Department] ([DepartmentDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE UNIQUE INDEX [IX_COM_Department_DepartmentName]
	ON [COM_Department] ([DepartmentName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
