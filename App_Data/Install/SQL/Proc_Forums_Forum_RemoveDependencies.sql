CREATE PROCEDURE [Proc_Forums_Forum_RemoveDependencies] 
    @ID int
AS
BEGIN
UPDATE Forums_ForumPost SET PostParentID=NULL WHERE (PostForumId =@ID);
DELETE FROM [Forums_ForumSubscription] WHERE SubscriptionPostID IN (SELECT PostID FROM Forums_ForumPost WHERE (PostForumId =@ID));
DELETE FROM [Forums_Attachment] WHERE AttachmentPostID IN (SELECT PostID FROM Forums_ForumPost WHERE (PostForumId =@ID));
DELETE FROM [Forums_UserFavorites] WHERE PostID IN (SELECT PostID FROM Forums_ForumPost WHERE (PostForumId =@ID));
DELETE FROM [Forums_UserFavorites] WHERE ForumID = @ID; 
DELETE FROM [Forums_ForumPost] WHERE (PostForumId =@ID);
DELETE FROM [Forums_ForumRoles ] WHERE ForumId = @ID;
DELETE FROM [Forums_ForumModerators ] WHERE ForumId = @ID;
DELETE FROM [Forums_ForumSubscription] WHERE SubscriptionForumId = @ID;
END
