using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.FormEngine;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSPages_Dialogs_Documentation : CMSPage
{
    #region "Variables"

    string menu = "";
    string content = "";

    int documentation = 0;
    bool img = false;
    int properties = 0;

    public string querystringvalue = "?generate=full";

    bool development = false;

    ContextResolver resolver = null;
    bool isFull = false;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get current resolver
        resolver = CMSContext.CurrentResolver.CreateChild();

        DataSet ds = null;
        DataSet cds = null;

        // Get webpart from querystring
        bool isWebpartInQuery = false;
        string webpartQueryParam = QueryHelper.GetString("webpart", "");        
        if (!string.IsNullOrEmpty(webpartQueryParam))
        {            
            isWebpartInQuery = true;
        }        

        if (!isWebpartInQuery)
        {
            // Get all webpart categories
            cds = WebPartCategoryInfoProvider.GetAllCategories();
        }
        else
        {
            // Split weparts
            string[] webparts = webpartQueryParam.Split(';');
            if (webparts.Length > 0)
            {
                string webpartWhere = String.Empty;
                foreach (string webpart in webparts)
                {
                    webpartWhere += "'" + webpart.Replace("'","''") + "'" + "," ; 
                }

                // Get webparts
                webpartWhere = "WebpartName IN (" + webpartWhere.TrimEnd(',') + ")";
                ds = WebPartInfoProvider.GetWebParts(webpartWhere,null) ;

                if(!DataHelper.DataSourceIsEmpty(ds))
                {
                    string categoryWhere = "";
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        categoryWhere += ValidationHelper.GetString(dr["WebpartCategoryID"], "NULL") + ",";
                    }

                    categoryWhere = "CategoryID IN (" + categoryWhere.TrimEnd(',') + ")";
                    cds = WebPartCategoryInfoProvider.GetCategories(categoryWhere, null);                    
                }

            }
        }

        // Check generate parameter
        if (Request.QueryString["generate"] != null)
        {
            // Set full generate if is required
            if (Request.QueryString["generate"].ToString().ToLower() == "full")
            {
                isFull = true;
            }
        }

        // Check details parameters
        if (Request.QueryString["details"] != null)
        {
            // Set development option if is required
            if (Request.QueryString["details"].ToString() == "1")
            {
                development = true;
                querystringvalue += "&details=1";
            }
        }

        // Check whether at least one category is present
        if (!DataHelper.DataSourceIsEmpty(cds))
        {
            // Loop through all web part categories
            foreach (DataRow cdr in cds.Tables[0].Rows)
            {
                // Get all webpart in the categories
                if (!isWebpartInQuery)
                {
                    ds = WebPartInfoProvider.GetAllWebParts(Convert.ToInt32(cdr["CategoryId"]));
                }

                // Check whether current category contains at least one webpart
                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    // Generate category name code
                    menu += "<br /><strong>" + cdr["CategoryDisplayName"].ToString() + "</strong><br /><br />";
                    // Loop through all web web parts in categories
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        // Documentation text
                        documentation = 0;
                        // Webpart images
                        img = false;
                        // Dispaly properties 
                        properties = 0;

                        // Get WebPart info
                        WebPartInfo wpi = new WebPartInfo(dr);

                        // Process only weparts in current category
                        if (isWebpartInQuery)
                        {
                            if (wpi.WebPartCategoryID != ValidationHelper.GetInteger(cdr["CategoryId"], 0))
                            {
                                wpi = null;
                            }
                        }

                        // Check whether web part exist
                        if (wpi != null)
                        {
                            // generate link guid
                            Guid mguid = Guid.NewGuid();

                            // Generate navigation link
                            GenerateNavigation(wpi, mguid);

                            // Generate webpart header
                            content += "<table style=\"width:100%;\"><tr><td><h1><a name=\"_" + mguid.ToString() + "\">" + cdr["CategoryDisplayName"].ToString() + "&nbsp;>&nbsp;" + wpi.WebPartDisplayName + "</a></h1></td><td style=\"text-align:right;\">&nbsp;<a href=\"#top\" class=\"noprint\">top</a></td></tr></table>";

                            bool Isdescription = true;

                            // If require full generation add more infos
                            if (isFull)
                            {
                                Isdescription = false;

                                //Image url
                                string wimgurl = "";
                                // Get metafile info for current webpart
                                DataSet mds = MetaFileInfoProvider.GetMetaFiles(wpi.WebPartID, PortalObjectType.WEBPART);
                                // Check whether image exists
                                if (!DataHelper.DataSourceIsEmpty(mds))
                                {   // Get metafile info object
                                    MetaFileInfo mtfi = new MetaFileInfo(mds.Tables[0].Rows[0]);
                                    if (mtfi != null)
                                    {
                                        // Get image url
                                        wimgurl = ResolveUrl("~/CMSPages/GetMetaFile.aspx?fileguid=" + mtfi.MetaFileGUID.ToString());
                                    }
                                }
                                else
                                {
                                    // Get WebPart default image 
                                    wimgurl = GetImageUrl("CMSModules/CMS_PortalEngine/WebpartProperties/imagenotavailable.png");
                                }

                                // Set description text
                                string descriptionText = wpi.WebPartDescription;

                                // Get parent description if webpart is inherited
                                if (wpi.WebPartParentID > 0 && (descriptionText == null || descriptionText.Trim() == ""))
                                {
                                    // Get parent webpart
                                    WebPartInfo pwpi = WebPartInfoProvider.GetWebPartInfo(wpi.WebPartParentID);
                                    if (pwpi != null)
                                    {
                                        // Set description from parent
                                        descriptionText = pwpi.WebPartDescription;
                                    }
                                }

                                if (descriptionText.Trim().Length > 0)
                                {
                                    Isdescription = true;
                                }

                                // Generate WebPart content
                                content += "" +
                                    "<table style=\"width: 100%; height: 200px; border: solid 1px #DDDDDD;\">" +
                                    "<tr>" +
                                    "<td style=\"width: 50%; text-align:center; border-right: solid 1px #DDDDDD; vertical-align: middle;margin-left: auto; margin-right:auto; text-align:center;\">" +
                                    "<img src=\"" + wimgurl + "\" alt=\"imageTeaser\">" +
                                    "</td>" +
                                    "<td style=\"width: 50%; vertical-align: center;text-align:center;\">" +
                                    descriptionText +
                                    "</td>" +
                                    "</tr>" +
                                    "</table>";
                            }

                            // Properties content
                            content += "<div class=\"DocumentationWebPartsProperties\">";

                            // Generate content
                            GenerateDocContent(wpi, mguid, cdr["CategoryDisplayName"].ToString());

                            // Close content area
                            content += "</div>";

                            // If full documentatiuon is required generate documentation part
                            if (isFull)
                            {
                                // Get documentation text
                                string docText = wpi.WebPartDocumentation;

                                // Set documentation text from parent if WebPart is inherited
                                if ((wpi.WebPartDocumentation == null || wpi.WebPartDocumentation.Trim() == "") && wpi.WebPartParentID > 0)
                                {
                                    WebPartInfo pwpi = WebPartInfoProvider.GetWebPartInfo(wpi.WebPartParentID);
                                    if (pwpi != null)
                                    {
                                        docText = pwpi.WebPartDocumentation;
                                    }
                                }

                                // Generate documentation text content
                                content += "<br /><div style=\"border: solid 1px #dddddd;width: 100%;\">" +
                                DataHelper.GetNotEmpty(HTMLHelper.ResolveUrls(docText, null), "<strong>Additional documentation text is not provided.</strong>") +
                                "</div>";
                            }

                            // Set page break tag for print
                            content += "<br /><p style=\"page-break-after: always;width:100%\">&nbsp;</p><hr class=\"noprint\" />";

                            // Get Image object
                            DataSet mfds = MetaFileInfoProvider.GetMetaFiles(wpi.WebPartID, PortalObjectType.WEBPART);

                            if (!DataHelper.DataSourceIsEmpty(mfds))
                            {
                                img = true;
                            }

                            // If development is required - highlight missing description, images and doc. text
                            if (development)
                            {
                                // Check image
                                if (!img)
                                {
                                    menu += "<span style=\"color:Brown;\">image&nbsp;</span>";
                                }

                                // Check properties
                                if (properties > 0)
                                {
                                    menu += "<span style=\"color:Red;\">properties(" + properties + ")&nbsp;</span>";
                                }

                                // Check properties
                                if (!Isdescription)
                                {
                                    menu += "<span style=\"color:#37627F;\">description&nbsp;</span>";
                                }

                                // Check documentation text
                                if (wpi.WebPartDocumentation != null && wpi.WebPartDocumentation != "")
                                {
                                }
                                else
                                {
                                    documentation = 1;

                                    // Check documentation text of parent webpart
                                    if (wpi.WebPartParentID > 0)
                                    {
                                        WebPartInfo pwpi = WebPartInfoProvider.GetWebPartInfo(wpi.WebPartParentID);
                                        if (pwpi != null && pwpi.WebPartDocumentation != null && pwpi.WebPartDocumentation != "")
                                        {
                                            documentation = 2;
                                        }
                                    }
                                }

                                // Display information about missing documentation
                                if (documentation == 1)
                                {
                                    menu += "<span style=\"color:Green;\">documentation&nbsp;</span>";
                                }

                                // Display information about inherited documentation
                                if (documentation == 2)
                                {
                                    menu += "<span style=\"color:Green;\">documentation (inherited)&nbsp;</span>";
                                }
                            }

                            menu += "<br />";
                        }

                    }

                }
            }
        }

        ltrContent.Text = menu + "<br /><p style=\"page-break-after: always;width:100%\">&nbsp;</p><hr class=\"noprint\" />" + content;
    }


    /// <summary>
    /// Generate navigation link
    /// </summary>
    /// <param name="wp">WebPart info</param>
    /// <param name="gd">Guid</param>
    protected void GenerateNavigation(WebPartInfo wp, Guid gd)
    {
        menu += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"#_" + gd.ToString() + "\">" + wp.WebPartDisplayName + "</a>&nbsp;";
    }


    /// <summary>
    /// Generate document content
    /// </summary>
    /// <param name="wpi">WebPart info</param>
    /// <param name="gd">Guid</param>
    /// <param name="category">Category</param>
    protected void GenerateDocContent(WebPartInfo wpi, Guid gd, string category)
    {
        // Check whether webpart info is defined
        if (wpi != null)
        {
            // Get parent webpart if webpart is inherited
            if (wpi.WebPartParentID != 0)
            {
                WebPartInfo pwpi = WebPartInfoProvider.GetWebPartInfo(wpi.WebPartParentID);
                if (pwpi != null)
                {
                    wpi = pwpi;
                }
            }

            // Load form definition
            FormInfo fi = new FormInfo();
            fi.LoadXmlDefinition(wpi.WebPartProperties);

            // Get defintion elements
            ArrayList infos = fi.GetFormElements(true, false);

            bool isOpenSubTable = false;

            string currentGuid = "";

            // Check all items in object array
            foreach (object contrl in infos)
            {
                // Generate row for form category
                if (contrl is FormCategoryInfo)
                {
                    // Load castegory info
                    FormCategoryInfo fci = contrl as FormCategoryInfo;
                    if (fci != null)
                    {
                        if (isOpenSubTable)
                        {
                            content += "<tr class=\"PropertyBottom\"><td class=\"PropertyLeftBottom\">&nbsp;</td><td colspan=\"2\">&nbsp;</td><td class=\"PropertyRightBottom\">&nbsp;</td></tr></table>";
                            isOpenSubTable = false;
                        }

                        if (currentGuid == "")
                        {
                            currentGuid = Guid.NewGuid().ToString().Replace("-", "_");
                        }

                        // Generate table
                        content += "<br />" +
                            "<table cellpadding=\"0\" cellspacing=\"0\" class=\"CategoryTable\">" +
                            "<tr>" +
                            "<td class=\"CategoryLeftBorder\">&nbsp;</td>" +
                            "<td class=\"CategoryTextCell\">" + fci.CategoryName + "</td>" +
                            "<td class=\"CategoryRightBorder\">&nbsp;</td>" +
                            "</tr></table>";
                    }
                }
                else
                {
                    // Get form field info
                    FormFieldInfo ffi = contrl as FormFieldInfo;

                    if (ffi != null)
                    {
                        //if (firstLoad)
                        //{
                        //    if (currentGuid == "")
                        //    {
                        //        currentGuid = Guid.NewGuid().ToString().Replace("-", "_");
                        //    }

                        //    firstLoad = false;

                        //    // Generate field table
                        //    //content += "<br />" +
                        //    //"<table cellpadding=\"0\" cellspacing=\"0\" class=\"CategoryTable\">" +
                        //    //"<tr>" +
                        //    //"<td class=\"CategoryLeftBorder\">&nbsp;</td>" +
                        //    //"<td class=\"CategoryTextCell\">Default</td>" +

                        //    //"<td class=\"CategoryRightBorder\">&nbsp;</td>" +
                        //    //"</tr></table>";
                        //}

                        if (!isOpenSubTable)
                        {
                            isOpenSubTable = true;
                            content += "" +
                                "<table cellpadding=\"0\" cellspacing=\"0\" id=\"_" + currentGuid + "\" class=\"PropertiesTable\" >";
                            currentGuid = "";
                        }


                        string doubleDot = "";
                        if (!ffi.Caption.EndsWith(":"))
                        {
                            doubleDot = ":";
                        }

                        content += "" +
                            "<tr>" +
                            "<td class=\"PropertyLeftBorder\" >&nbsp;</td>" +
                            "<td class=\"PropertyContent\" style=\"width:200px;\">" + ffi.Caption + doubleDot + "</td>" +
                            "<td>" + HTMLHelper.HTMLEncode(resolver.ResolveMacros(DataHelper.GetNotEmpty(ffi.Description, ResHelper.GetString("WebPartDocumentation.DescriptionNoneAvailable")))) + "</td>" +
                            "<td class=\"PropertyRightBorder\">&nbsp;</td>" +
                            "</tr>";

                        if (ffi.Description == null || ffi.Description.Trim() == "")
                        {
                            properties++;
                        }
                    }
                }
            }

            if (isOpenSubTable)
            {
                content += "<tr class=\"PropertyBottom\"><td class=\"PropertyLeftBottom\">&nbsp;</td><td colspan=\"2\">&nbsp;</td><td class=\"PropertyRightBottom\">&nbsp;</td></tr></table>";
            }
        }
    }


    /// <summary>
    /// OnLoad override
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        // Disable caching
        Response.Cache.SetNoStore();
        base.OnLoad(e);
    }
}
