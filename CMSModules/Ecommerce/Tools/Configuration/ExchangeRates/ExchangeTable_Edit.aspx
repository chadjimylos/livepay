<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExchangeTable_Edit.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_ExchangeRates_ExchangeTable_Edit"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Exchange table - properties" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblExchangeTableDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtExchangeTableDisplayName" runat="server" CssClass="TextBoxField"
                    MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvDsiplayName" runat="server" Display="Dynamic"
                    ValidationGroup="Exchange" ControlToValidate="txtExchangeTableDisplayName" EnableViewState="false" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblExchangeTableValidFrom" EnableViewState="false" />
            </td>
            <td>
                <cms:DateTimePicker ID="dtPickerExchangeTableValidFrom" runat="server" EnableViewState="false" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblExchangeTableValidTo" EnableViewState="false" />
            </td>
            <td>
                <cms:DateTimePicker ID="dtPickerExchangeTableValidTo" runat="server" EnableViewState="false" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <asp:PlaceHolder ID="plcGrid" runat="server">
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" style="vertical-align: top;">
                    <asp:Label runat="server" ID="lblRates" EnableViewState="false" />
                </td>
                <td>
                    <asp:GridView ID="editGrid" runat="server" AutoGenerateColumns="false" CssClass="UniGridGrid"
                        GridLines="Horizontal" EnableViewState="false">
                        <HeaderStyle CssClass="UniGridHead" />
                        <AlternatingRowStyle CssClass="OddRow" />
                        <RowStyle CssClass="EvenRow" />
                        <Columns>
                            <asp:BoundField DataField="CurrencyCode">
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField>
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
                <td style="vertical-align: top;">
                    <asp:Image ID="imgHelp" runat="server" EnableViewState="false" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" ValidationGroup="Exchange" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
