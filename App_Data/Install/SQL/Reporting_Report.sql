CREATE TABLE [Reporting_Report] (
		[ReportID]               int NOT NULL IDENTITY(1, 1),
		[ReportName]             nvarchar(100) NOT NULL,
		[ReportDisplayName]      nvarchar(440) NOT NULL,
		[ReportLayout]           nvarchar(max) NOT NULL,
		[ReportParameters]       nvarchar(max) NOT NULL,
		[ReportCategoryID]       int NOT NULL,
		[ReportAccess]           int NOT NULL,
		[ReportGUID]             uniqueidentifier NOT NULL,
		[ReportLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Reporting_Report]
	ADD
	CONSTRAINT [PK_Reporting_Report]
	PRIMARY KEY
	NONCLUSTERED
	([ReportID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Reporting_Report]
	ADD
	CONSTRAINT [DEFAULT_Reporting_Report_ReportDisplayName]
	DEFAULT ('') FOR [ReportDisplayName]
IF OBJECT_ID(N'[Reporting_Report]') IS NOT NULL
	AND OBJECT_ID(N'[Reporting_ReportCategory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Reporting_Report_ReportCategoryID_Reporting_ReportCategory]') IS NULL
BEGIN
		ALTER TABLE [Reporting_Report]
			ADD CONSTRAINT [FK_Reporting_Report_ReportCategoryID_Reporting_ReportCategory]
			FOREIGN KEY ([ReportCategoryID]) REFERENCES [Reporting_ReportCategory] ([CategoryID])
END
CREATE CLUSTERED INDEX [IX_Reporting_Report_ReportCategoryID_ReportDisplayName]
	ON [Reporting_Report] ([ReportDisplayName], [ReportCategoryID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Reporting_Report_ReportGUID_ReportName]
	ON [Reporting_Report] ([ReportGUID], [ReportName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE UNIQUE INDEX [IX_Reporting_Report_ReportName]
	ON [Reporting_Report] ([ReportName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
