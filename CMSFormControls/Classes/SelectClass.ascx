<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectClass.ascx.cs" Inherits="CMSFormControls_Classes_SelectClass" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%ClassDisplayName%}"
            ReturnColumnName="ClassID" ObjectType="cms.documenttype" ResourcePrefix="allowedclasscontrol"
            SelectionMode="SingleDropDownList" AllowEmpty="false" AllowAll="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
