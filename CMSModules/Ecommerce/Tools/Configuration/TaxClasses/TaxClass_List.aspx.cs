using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_TaxClasses_TaxClass_List : CMSEcommerceConfigurationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("TaxClass_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_TaxClass/object.png");
        this.CurrentMaster.Title.HelpTopicName = "tax_classes_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        // Prepare the new payment option header element
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("TaxClass_List.NewItemCaption");
        actions[0, 3] = "~/CMSModules/Ecommerce/Tools/Configuration/TaxClasses/TaxClass_New.aspx";
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_TaxClass/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("TaxClass_Frameset.aspx?taxclassid=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // check 'ConfigurationModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
            }

            TaxClassInfoProvider.DeleteTaxClassInfo(ValidationHelper.GetInteger(actionArgument, 0));
        }
    }
}
