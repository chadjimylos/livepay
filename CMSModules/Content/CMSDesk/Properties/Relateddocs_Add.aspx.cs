using System;
using System.Web;
using System.Web.UI;

using CMS.TreeEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.WorkflowEngine;
using CMS.Staging;
using CMS.UIControls;
using CMS.ExtendedControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Properties_Relateddocs_Add : CMSPropertiesPage
{
    #region "Protected variables"

    protected int currentNodeId = 0;
    protected TreeNode node = null;
    protected DialogConfiguration mConfig = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Gets the configuration for Copy and Move dialog.
    /// </summary>
    private DialogConfiguration Config
    {
        get
        {
            if (mConfig == null)
            {
                mConfig = new DialogConfiguration();
                mConfig.HideLibraries = true;
                mConfig.ContentSelectedSite = CMSContext.CurrentSiteName;
                mConfig.HideAnchor = true;
                mConfig.HideAttachments = true;
                mConfig.HideContent = false;
                mConfig.HideEmail = true;
                mConfig.HideLibraries = true;
                mConfig.HideWeb = true;
                mConfig.ContentSelectedSite = CMSContext.CurrentSiteName;
                mConfig.OutputFormat = OutputFormatEnum.Custom;
                mConfig.CustomFormatCode = "relationship";
                mConfig.SelectableContent = SelectableContentEnum.AllContent;
            }
            return mConfig;
        }
    }

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.RelatedDocs"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.RelatedDocs");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        relNameSelector.IsLiveSite = false;

        // Register the dialog script
        ScriptHelper.RegisterDialogScript(Page);

        // Initialize dialog scripts
        Config.EditorClientID = txtLeftNode.ClientID;
        btnLeftNode.OnClientClick = "modalDialog('" + CMSDialogHelper.GetDialogUrl(Config, false) + "', 'contentselectnode', '90%', '85%'); return false;";

        Config.EditorClientID = txtRightNode.ClientID;
        btnRightNode.OnClientClick = "modalDialog('" + CMSDialogHelper.GetDialogUrl(Config, false) + "', 'contentselectnode', '90%', '85%'); return false;";

        currentNodeId = QueryHelper.GetInteger("nodeid", 0);

        leftCell.Text = ResHelper.GetString("Relationship.leftSideDoc");
        middleCell.Text = ResHelper.GetString("Relationship.RelationshipName");
        rightCell.Text = ResHelper.GetString("Relationship.rightSideDoc");

        // Initializes page breadcrumbs
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Relationship.RelatedDocs");
        pageTitleTabs[0, 1] = "~/CMSModules/Content/CMSDesk/Properties/Relateddocs_List.aspx?nodeid=" + currentNodeId;
        pageTitleTabs[0, 2] = "propedit";
        pageTitleTabs[1, 0] = ResHelper.GetString("Relationship.AddRelatedDocs");
        pageTitleTabs[1, 1] = string.Empty;
        pageTitleTabs[1, 2] = string.Empty;
        titleElem.Breadcrumbs = pageTitleTabs;

        if (currentNodeId > 0)
        {
            TreeProvider treeProvider = new TreeProvider(CMSContext.CurrentUser);
            string nodeDocumentName = treeProvider.SelectSingleNode(currentNodeId).DocumentName;
            lblRightNode.Text = lblLeftNode.Text = (string.IsNullOrEmpty(nodeDocumentName)) ? "/" : HTMLHelper.HTMLEncode(nodeDocumentName);
        }
    }

    #endregion


    #region "Control events"

    protected void btnOk_Click(object sender, EventArgs e)
    {
        bool currentNodeIsOnLeftSide = ValidationHelper.GetBoolean(Request.Params["currentOnLeft"], false);
        int selectedNodeId = ValidationHelper.GetInteger(Request.Params["selectedNodeId"], 0);

        // Try to get by path if not selected
        if (selectedNodeId <= 0)
        {
            string aliaspath = currentNodeIsOnLeftSide ? txtRightNode.Text.Trim() : txtLeftNode.Text.Trim();

            if (aliaspath != string.Empty)
            {
                TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                node = tree.SelectSingleNode(CMSContext.CurrentSiteName, aliaspath, TreeProvider.ALL_CULTURES);
                if (node != null)
                {
                    selectedNodeId = node.NodeID;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("relationship.selectcorrectrelateddoc");
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("relationship.selectrelateddoc");
            }
        }
        if (selectedNodeId <= 0)
        {
            if (!currentNodeIsOnLeftSide)
            {
                ScriptHelper.RegisterStartupScript(this, typeof(string), "switchSidesScript", ScriptHelper.GetScript("SwitchSides();"));
            }
        }

        int selectedValue = ValidationHelper.GetInteger(relNameSelector.Value, 0);

        if ((currentNodeId > 0) && (selectedNodeId > 0) && (selectedValue > 0))
        {
            int relationshipNameId = selectedValue;

            try
            {
                if (currentNodeIsOnLeftSide)
                {
                    RelationshipProvider.AddRelationship(currentNodeId, selectedNodeId, relationshipNameId);
                }
                else
                {
                    RelationshipProvider.AddRelationship(selectedNodeId, currentNodeId, relationshipNameId);
                }

                string aliasPath = (node == null) ? TreePathUtils.GetAliasPathByNodeId(currentNodeId) : node.NodeAliasPath;

                // Log synchronization
                DocumentHelper.LogSynchronization(CMSContext.CurrentSiteName, aliasPath, TaskTypeEnum.UpdateDocument, null, SynchronizationInfoProvider.ENABLED_SERVERS, false);

                UrlHelper.Redirect("Relateddocs_List.aspx?nodeid=" + currentNodeId);
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
            }
        }
    }

    #endregion
}
