<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_Selection.aspx.cs"
    Inherits="CMSModules_Ecommerce_Content_Product_Product_Selection" Theme="Default"
    ValidateRequest="false" %>

<%@ Register Src="~/CMSModules/Ecommerce/Controls/UI/ProductEdit.ascx" TagName="NewProduct"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/FormControls/SKUSelector.ascx" TagName="SKUSelector"
    TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Content - Product</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>

    <script type="text/javascript">
        //<![CDATA[
        parent.frames['contenteditheader'].SetTabsContext('product');
        //]]>
    </script>

</head>
<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" ID="scriptManager" />
    <asp:Panel runat="server" ID="pnlHeader" CssClass="PageheaderLine" EnableViewState="false">
        <asp:Label runat="server" ID="lblGlobalInfo" CssClass="InfoLabel" EnableViewState="false" />
        <asp:CheckBox ID="chkMarkDocAsProd" runat="server" OnCheckedChanged="chkMarkDocAsProd_CheckedChanged"
            AutoPostBack="true" />
    </asp:Panel>
    <asp:Panel ID="pnlBody" runat="server" CssClass="PageBody">
        <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
            <asp:RadioButton ID="radSelect" runat="server" GroupName="product" OnCheckedChanged="radSelect_CheckedChanged"
                AutoPostBack="true" />
            <br />
            <br />
            <asp:Panel ID="pnlSelect" runat="server" Style="margin-left: 20px;">
                <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
                    Visible="false" />
                <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
                    Visible="false" />
                <cms:SKUSelector runat="server" ID="skuElem" IsLiveSite="false" DisplayItems="Enabled" />
            </asp:Panel>
            <br />
            <div>
                <asp:RadioButton ID="radCreate" runat="server" GroupName="product" OnCheckedChanged="radCreate_CheckedChanged"
                    AutoPostBack="true" />
            </div>
            <asp:Panel ID="pnlNew" runat="server" Style="margin-left: 20px; padding-bottom: 10px;">
                <cms:NewProduct ID="ctrlProduct" runat="server" Visible="false" />
            </asp:Panel>
            <br />
            <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOk_Click"
                EnableViewState="false" />
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
