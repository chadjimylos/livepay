<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User_Edit_Friends_Approved.aspx.cs"
    Inherits="CMSModules_Friends_Administration_Users_User_Edit_Friends_Approved"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="default" %>

<%@ Register Src="~/CMSModules/Friends/Controls/FriendsList.ascx" TagName="FriendsList"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
    <asp:PlaceHolder ID="plcTable" runat="server">
        <cms:FriendsList ID="FriendsList" runat="server" Visible="true" IsLiveSite="false" />
        <cms:LocalizedLabel ID="lblInfo" runat="server" ResourceString="friends.nouserfriends"
            EnableViewState="false" />
    </asp:PlaceHolder>
</asp:Content>
