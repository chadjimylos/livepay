FCKLang.InsertQuoteButton			= 'Insert Quote' ;
FCKLang.InsertQuoteTitle			= 'Insert Quote' ;

FCKLang.InsertUrlButton	            = 'Insert URL' ;
FCKLang.InsertUrlTitle		    	= 'Insert URL' ;
FCKLang.InsertUrlPromptUrl          = 'Please enter URL:'
FCKLang.InsertUrlPromptDesc         = 'Please enter URL decription:'

FCKLang.InsertImageButton			= 'Insert Image' ;
FCKLang.InsertImageTitle			= 'Insert Image' ;
FCKLang.InsertImagePromptUrl        = 'Please enter Image URL:'