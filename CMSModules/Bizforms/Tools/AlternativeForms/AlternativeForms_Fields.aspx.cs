using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.FormEngine;
using CMS.CMSHelper;
using CMS.Staging;
using CMS.UIControls;

public partial class CMSModules_BizForms_Tools_AlternativeForms_AlternativeForms_Fields : CMSBizFormPage
{
    protected int altFormId = 0;
    protected int formId = 0;

    private BizFormInfo bfi = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'ReadForm' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "ReadForm"))
        {
            RedirectToCMSDeskAccessDenied("cms.form", "ReadForm");
        }

        altFormId = QueryHelper.GetInteger("altformid", 0);

        formId = QueryHelper.GetInteger("formid", 0);
        bfi = BizFormInfoProvider.GetBizFormInfo(formId);
        if (bfi == null)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("general.invalidid");
            altFormFieldEditor.Visible = false;
            return;
        }

        altFormFieldEditor.AlternativeFormID = altFormId;
        altFormFieldEditor.DisplayedControls = FieldEditorControlsEnum.Bizforms;
        altFormFieldEditor.OnBeforeSave += new OnAlternativeFieldEditorSaveEventHandler(altFormFieldEditor_OnBeforeSave);
        altFormFieldEditor.OnAfterSave += new OnAlternativeFieldEditorSaveEventHandler(altFormFieldEditor_OnAfterSave);
    }


    void altFormFieldEditor_OnAfterSave()
    {
        // Log synchronization
        TaskInfoProvider.LogSynchronization(bfi, TaskTypeEnum.UpdateObject, null, CMSContext.CurrentSiteName);
    }


    void altFormFieldEditor_OnBeforeSave()
    {
        // Check 'EditForm' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "EditForm"))
        {
            RedirectToCMSDeskAccessDenied("cms.form", "EditForm");
        }
    }
}
