using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_OrderStatus_OrderStatus_List : CMSEcommerceConfigurationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Pagetitle
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("OrderStatus_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_OrderStatus/object.png");
        this.CurrentMaster.Title.HelpTopicName = "order_status_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("OrderStatus_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("OrderStatus_Edit.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_OrderStatus/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        gridElem.GridView.AllowSorting = false;
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }

    object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "statuscolor":
                if (parameter != DBNull.Value)
                {
                    return "<div style=\"height:13px; widht=25px; background-color:" + parameter.ToString() + "\"></div>";
                }

                return "-";

            case "statussendnotification":
            case "statusenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }

        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "edit":
                UrlHelper.Redirect("OrderStatus_Edit.aspx?orderstatusid=" + Convert.ToString(actionArgument));
                break;

            case "delete":
                // check 'ConfigurationModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
                {
                    RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
                }

                if (OrderStatusInfoProvider.CheckDependencies(ValidationHelper.GetInteger(actionArgument, 0)))
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                    return;
                }

                // delete OrderStatusInfo object from database
                OrderStatusInfoProvider.DeleteOrderStatusInfo(ValidationHelper.GetInteger(actionArgument, 0));
                break;

            case "moveup":
                // check 'ConfigurationModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
                {
                    RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
                }


                OrderStatusInfoProvider.MoveStatusUp(ValidationHelper.GetInteger(actionArgument, 0));
                break;

            case "movedown":
                // check 'ConfigurationModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
                {
                    RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
                }

                OrderStatusInfoProvider.MoveStatusDown(ValidationHelper.GetInteger(actionArgument, 0));
                break;
        }
    }
}