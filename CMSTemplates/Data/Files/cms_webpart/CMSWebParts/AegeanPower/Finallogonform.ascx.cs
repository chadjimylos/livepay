﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.ExtendedControls;
using CMS.DataEngine;
using CMS.EmailEngine;
using CMS.SiteProvider;
using CMS.EventLog;
using CMS.URLRewritingEngine;
using CMS.MembershipProvider;
using CMS.PortalEngine;

public partial class CMSWebParts_AegeanPower_Finallogonform: CMSAbstractWebPart
{

    #region "Private properties"

    private string mDefaultTargetUrl = "";
    private TextBox user = null;
    private TextBox pass = null;
    private LocalizedButton login = null;
    private LocalizedLabel lblUserName = null;
    private LocalizedLabel lblPassword = null;
    private ImageButton loginImg = null;
    private RequiredFieldValidator rfv = null;
    private Panel container = null;
    private string mUserNameText = "login";

    #endregion

    #region "Public properties SignOut"

    /// <summary>
    /// PasswdRetrieval
    /// </summary>
    public string HeaderTitleLogout
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("HeaderTitleLogout"), "");
        }
        set
        {
            this.SetValue("HeaderTitleLogout", value);
        }
    }


    /// <summary>
    /// Gets or sets the URL where user is redirected after sign out
    /// </summary>
    public string RedirectToUrl
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("RedirectToURL"), URLRewriter.CurrentURL);
        }
        set
        {
            SetValue("RedirectToURL", value);
        }
    }   


    #endregion


    #region "Public properties"

    ///// <summary>
    ///// Gets or sets the value that indicates whether retrieving of forgotten password is enabled
    ///// </summary>
    //public bool AllowPasswordRetrieval
    //{
    //    get
    //    {
    //        return ValidationHelper.GetBoolean(this.GetValue("AllowPasswordRetrieval"), true);
    //    }
    //    set
    //    {
    //        this.SetValue("AllowPasswordRetrieval", value);
    //        this.lnkPasswdRetrieval.Visible = value;
    //    }
    //}


    /// <summary>
    /// Gets or sets the sender e-mail (from)
    /// </summary>
    public string SendEmailFrom
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("SendEmailFrom"), SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSSendPasswordEmailsFrom"));
        }
        set
        {
            this.SetValue("SendEmailFrom", value);
        }
    }


    /// <summary>
    /// Gets or sets the default target url (rediredction when the user is logged in)
    /// </summary>
    public string DefaultTargetUrl
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("DefaultTargetUrl"), mDefaultTargetUrl);
        }
        set
        {
            this.SetValue("DefaultTargetUrl", value);
            this.mDefaultTargetUrl = value;
        }
    }


    /// <summary>
    /// Gets or sets the SkinID of the logon form
    /// </summary>
    public override string SkinID
    {
        get
        {
            return base.SkinID;
        }
        set
        {
            base.SkinID = value;
            SetSkinID(value);
        }
    }

    /// <summary>
    /// Label HeaderTitle
    /// </summary>
    public string HeaderTitle
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("HeaderTitle"), "");
        }
        set
        {
            this.SetValue("HeaderTitle", value);
        }
    }

    /// <summary>
    /// UserName
    /// </summary>
    public string UserName
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("UserName"), "");
        }
        set
        {
            this.SetValue("UserName", value);
        }
    }

    /// <summary>
    /// Password
    /// </summary>
    public string Password
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("Password"), "");
        }
        set
        {
            this.SetValue("Password", value);
        }
    }

    /// <summary>
    /// LogonButton
    /// </summary>
    public string LogonButton
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("LogonButton"), "");
        }
        set
        {
            this.SetValue("LogonButton", value);
        }
    }

    /// <summary>
    /// PasswdRetrieval Text
    /// </summary>
    public string PasswdRetrieval
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("PasswdRetrieval"), "");
        }
        set
        {
            this.SetValue("PasswdRetrieval", value);
        }
    }

    /// <summary>
    /// PasswdRetrievalURL
    /// </summary>
    public string PasswdRetrievalURL
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("PasswdRetrievalURL"), "");
        }
        set
        {
            this.SetValue("PasswdRetrievalURL", value);
        }
    }

    /// <summary>
    /// Gets or sets the logon failure text
    /// </summary>
    public string FailureText
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("FailureText"), "");
        }
        set
        {
            if (value.ToString().Trim() != "")
            {
                this.SetValue("FailureText", value);
                this.Login1.FailureText = value;
            }
        }
    }

    /// <summary>
    /// Member
    /// </summary>
    public string MemberText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("MemberText"), "");
        }
        set
        {
            this.SetValue("MemberText", value);
        }
    }

    /// <summary>
    /// Member URL
    /// </summary>
    public string MemberURL
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("MemberURL"), "");
        }
        set
        {
            this.SetValue("MemberURL", value);
        }
    }


    /// <summary>
    /// TextShort1
    /// </summary>
    //public string TextShort1
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("TextShort1"), ResHelper.LocalizeString("{$=text link1|en-us=text link1$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("TextShort1", value);
    //        lnkShort1.Text = value;
    //    }
    //}
    /// <summary>
    /// LinkShort1
    /// </summary>
    //public string LinkShort1
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("LinkShort1"), ResHelper.LocalizeString("{$=link link1|en-us=link link1$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("LinkShort1", value);
    //        lnkShort1.PostBackUrl = value;
    //    }
    //}


    /// <summary>
    /// TextShort2
    /// </summary>
    //public string TextShort2
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("TextShort2"), ResHelper.LocalizeString("{$=text link2|en-us=text link2$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("TextShort2", value);
    //        lnkShort2.Text = value;
    //    }
    //}
    /// <summary>
    /// LinkShort2
    /// </summary>
    //public string LinkShort2
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("LinkShort2"), ResHelper.LocalizeString("{$=link link2|en-us=link link2$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("LinkShort2", value);
    //        lnkShort2.PostBackUrl = value;
    //    }
    //}


    /// <summary>
    /// TextShort3
    /// </summary>
    //public string TextShort3
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("TextShort3"), ResHelper.LocalizeString("{$=text link3|en-us=text link3$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("TextShort3", value);
    //        lnkShort3.Text = value;
    //    }
    //}
    /// <summary>
    /// LinkShort3
    /// </summary>
    //public string LinkShort3
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("LinkShort3"), ResHelper.LocalizeString("{$=link link3|en-us=link link3$}"));
    //    }
    //    set
    //    {
    //        this.SetValue("LinkShort3", value);
    //        lnkShort3.PostBackUrl = value;
    //    }
    //}   


    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            //this.rqValue.Visible = false;
            // Do not process
        }
        else
        {
            if (CMSContext.CurrentUser.IsPublic())
            {

                // Set HeaderTitle text
                //lblHeaderTitle.Text = HeaderTitle;

                // Set Member text
                //lnkMember.Text = MemberText;
                //lnkMember.PostBackUrl = MemberURL;

                lblNewCustomer.Text = ResHelper.LocalizeString("{$=Nέος πελάτης|en-us=Nέος πελάτης$}");
                lblForgotPassword.Text = ResHelper.LocalizeString("{$=Ξεχάσατε τον Κωδικό σας;|en-us=Ξεχάσατε τον Κωδικό σας;$}");

                // Set failure text
                if (!string.IsNullOrEmpty(FailureText))
                {
                    Login1.FailureText = ResHelper.LocalizeString(FailureText);
                }
                else
                {
                    Login1.FailureText = ResHelper.GetString("Login_FailureText");
                }

                // Set strings
                lnkPasswdRetrieval.Text = PasswdRetrieval;//ResHelper.GetString("LogonForm.lnkPasswordRetrieval");
                lnkPasswdRetrieval.PostBackUrl = PasswdRetrievalURL;
                //lblPasswdRetrieval.Text = ResHelper.GetString("LogonForm.lblPasswordRetrieval");
                //btnPasswdRetrieval.Text = ResHelper.GetString("LogonForm.btnPasswordRetrieval");
                //rqValue.ErrorMessage = ResHelper.GetString("LogonForm.rqValue");

                // Set logon strings
                //LocalizedLabel lblItem = (LocalizedLabel)Login1.FindControl("lblUserName");
                //if (lblItem != null)
                //{
                //    lblItem.Text = UserName;//"{$LogonForm.UserName$}";
                //}

                lblUserName = (LocalizedLabel)Login1.FindControl("lblUserName");
                if (lblUserName != null)
                {
                    lblUserName.Text = ResHelper.LocalizeString("{$=E-mail:|en-us=E-mail:$}");
                }

                lblPassword = (LocalizedLabel)Login1.FindControl("lblPassword");
                if (lblPassword != null)
                {
                    lblPassword.Text = ResHelper.LocalizeString("{$=Κωδικός:|en-us=Code:$}");
                }
                //LocalizedCheckBox chkItem = (LocalizedCheckBox)Login1.FindControl("chkRememberMe");
                //if (chkItem != null)
                //{
                //    chkItem.Text = "{$LogonForm.RememberMe$}";
                //}
                LocalizedButton LoginButton = (LocalizedButton)Login1.FindControl("LoginButton");
                if (LoginButton != null)
                {
                    LoginButton.Text = ResHelper.LocalizeString("{$=   Είσοδος|en-us=   Log in$}");
                }

                //this.ltlScript.Text = ScriptHelper.GetScript("function ShowPasswdRetrieve(id){var hdnDispl = document.getElementById('" + this.hdnPasswDisplayed.ClientID + "'); style=document.getElementById(id).style;style.display=(style.display == 'block')?'none':'block'; if(hdnDispl != null){ if(style.display == 'block'){hdnDispl.value='1';}else{hdnDispl.value='';}}}");
                //this.lnkPasswdRetrieval.Attributes.Add("onclick", "ShowPasswdRetrieve('" + pnlPasswdRetrieval.ClientID + "'); return false;");

                //this.lnkPasswdRetrieval.Visible = this.AllowPasswordRetrieval;

                Login1.LoggedIn += new EventHandler(Login1_LoggedIn);
                Login1.LoggingIn += new LoginCancelEventHandler(Login1_LoggingIn);

                //btnPasswdRetrieval.Click += new EventHandler(btnPasswdRetrieval_Click);

                if (!RequestHelper.IsPostBack())
                {

                    Login1.UserName = ValidationHelper.GetString(Request.QueryString["username"], "");
                    // Set SkinID properties
                    if (!this.StandAlone && (this.PageCycle < PageCycleEnum.Initialized) && (ValidationHelper.GetString(this.Page.StyleSheetTheme, "") == ""))
                    {
                        SetSkinID(this.SkinID);
                    }
                }

                //if (!this.AllowPasswordRetrieval)
                //{
                    //pnlPasswdRetrieval.Visible = false;
                //}
            }
            else
            {
                //if (CMSContext.CurrentUser.IsInRole("LivePayMerchants", CMSContext.CurrentSiteName) && CMSContext.CurrentUser.GetValue("LivePayID").ToString() == "")
                //{
                //    Response.Write("222");
                //}
                //Response.Write(CMSContext.CurrentUser.IsInRole("LivePayMerchants", CMSContext.CurrentSiteName));

                //Set LinkShort text
                //lnkShort1.Text = this.TextShort1;
                //lnkShort1.PostBackUrl = this.LinkShort1;
                //lnkShort2.Text = this.TextShort2;
                //lnkShort2.PostBackUrl = this.LinkShort2;
                //lnkShort3.Text = this.TextShort3;
                //lnkShort3.PostBackUrl = this.LinkShort3;

                // Set HeaderTitleLogout text
                //lblHeaderTitleLogout.Text = HeaderTitleLogout;

                Panel1.Visible = false;
                Panel_Logout.Visible = true;
                lblFullName.Text = CMSContext.CurrentUser.FullName;
                lblWelcomeMessage1.Text = ResHelper.LocalizeString("{$=Καλώς ήλθατε |en-us=Welcome$}");
                lblWelcomeMessage2.Text = ResHelper.LocalizeString("{$=στην Αegean Power|en-us=at Aegean Power$}");
                lblUserNameText.Text = ResHelper.LocalizeString("{$=Όνομα Χρήστη: |en-us=User Name: $}");
                btnSignOut.Text = ResHelper.LocalizeString("{$=Αποσύνδεση|en-us=Log out$}");
                
                //if (CMSContext.CurrentUser.GetValue("LivePayID") != null && CMSContext.CurrentUser.GetValue("LivePayID").ToString().Length > 0)
                //{
                //    phUserLinks.Visible = false;
                //    phMerchantLinks.Visible = true;
                //}
                //else
                //{
                //    phUserLinks.Visible = true;
                //    phMerchantLinks.Visible = false;
                //}
            }
        }
    }


    /// <summary>
    /// SignOut handler
    /// </summary>
    protected void btnSignOut_Click(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            // Do not process
        }
        else
        {
            if (CMSContext.CurrentUser.IsAuthenticated())
            {
                FormsAuthentication.SignOut();
                CMSContext.ClearShoppingCart();

                string redirectUrl = RedirectToUrl;

                CMSContext.ViewMode = ViewModeEnum.LiveSite;
                CMSContext.CurrentUser = null;

                Response.Cache.SetNoStore();
                UrlHelper.Redirect(redirectUrl);
            }
            //else
            //{
            //    string returnUrl = null;
            //    string signInUrl = null;

            //    if (SignInUrl != "")
            //    {
            //        signInUrl = ResolveUrl(CMSContext.GetUrl(SignInUrl));
            //    }
            //    else
            //    {
            //        signInUrl = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSSecuredAreasLogonPage");
            //    }

            //    if (ReturnPath != "")
            //    {
            //        returnUrl = ResolveUrl(CMSContext.GetUrl(ReturnPath));
            //    }
            //    else
            //    {
            //        returnUrl = UrlHelper.CurrentURL;
            //    }

            //    if (signInUrl != "")
            //    {
            //        UrlHelper.Redirect(UrlHelper.AddParameterToUrl(signInUrl, "returnurl", returnUrl));
            //    }
            //}
        }
    }


    /// <summary>
    /// OnLoad override (show hide password retrieval)
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        //if (hdnPasswDisplayed.Value == "")
        //{
        //    //this.pnlPasswdRetrieval.Attributes.Add("style", "display:none;");
        //}
        //else
        //{
        //    //this.pnlPasswdRetrieval.Attributes.Add("style", "display:block;");
        //}
    }


    /// <summary>
    /// Sets SkinId to all controls in logon form
    /// </summary>
    void SetSkinID(string skinId)
    {
        if (skinId != "")
        {
            Login1.SkinID = skinId;

            LocalizedLabel lbl = (LocalizedLabel)this.Login1.FindControl("lblUserName");
            if (lbl != null)
            {
                lbl.SkinID = skinId;
            }
            lbl = (LocalizedLabel)this.Login1.FindControl("lblPassword");
            if (lbl != null)
            {
                lbl.SkinID = skinId;
            }

            TextBox txt = (TextBox)this.Login1.FindControl("UserName");
            if (txt != null)
            {
                txt.SkinID = skinId;
            }
            txt = (TextBox)this.Login1.FindControl("Password");
            if (txt != null)
            {
                txt.SkinID = skinId;
            }

            //LocalizedCheckBox chk = (LocalizedCheckBox)this.Login1.FindControl("chkRememberMe");
            //if (chk != null)
            //{
            //    chk.SkinID = skinId;
            //}

            LocalizedButton btn = (LocalizedButton)this.Login1.FindControl("LoginButton");
            if (btn != null)
            {
                btn.SkinID = skinId;
            }
        }
    }


    /// <summary>
    /// Applies given stylesheet skin
    /// </summary>
    public override void ApplyStyleSheetSkin(Page page)
    {
        SetSkinID(this.SkinID);
        
        base.ApplyStyleSheetSkin(page);
    }


    ///// <summary>
    ///// Retrieve the user password
    ///// </summary>
    //void btnPasswdRetrieval_Click(object sender, EventArgs e)
    //{
    //    string value = txtPasswordRetrieval.Text.Trim();
        
    //    if (value != String.Empty)
    //    {
    //        lblResult.Text = UserInfoProvider.ForgottenEmailRequest(value, CMSContext.CurrentSiteName, "LOGONFORM", this.SendEmailFrom, CMSContext.CurrentResolver);
    //        lblResult.Visible = true;

    //        this.pnlPasswdRetrieval.Attributes.Add("style", "display:block;");            
    //    }
    //}


    /// <summary>
    /// Logged in handler
    /// </summary>
    void Login1_LoggedIn(object sender, EventArgs e)
    {

        // Set view mode to live site after login to prevent bar with "Close preview mode"
        CMSContext.ViewMode = CMS.PortalEngine.ViewModeEnum.LiveSite;        

        // Ensure response cookie
        CookieHelper.EnsureResponseCookie(FormsAuthentication.FormsCookieName);

        // Set cookie expiration
        if (Login1.RememberMeSet)
        {
            CookieHelper.ChangeCookieExpiration(FormsAuthentication.FormsCookieName, DateTime.Now.AddYears(1), false);
        }
        else
        {
            // Extend the expiration of the authentication cookie if required
            if (!UserInfoProvider.UseSessionCookies && (HttpContext.Current != null) && (HttpContext.Current.Session != null))
            {
                CookieHelper.ChangeCookieExpiration(FormsAuthentication.FormsCookieName, DateTime.Now.AddMinutes(Session.Timeout), false);
            }
        }

        // Current username
        string userName = Login1.UserName;


        // Check whether safe user name is required and if so get safe username
        if (RequestHelper.IsMixedAuthentication() && UserInfoProvider.UseSafeUserName)
        {
            userName = ValidationHelper.GetSafeUserName(this.Login1.UserName, CMSContext.CurrentSiteName);
            FormsAuthentication.SetAuthCookie(userName, this.Login1.RememberMeSet);
        }

        // Redirect user to the return url, or if is not defined redirect to the default target url
        if (ValidationHelper.GetString(Request.QueryString["ReturnURL"], "") != "")
        {
            UrlHelper.Redirect(ResolveUrl(ValidationHelper.GetString(Request.QueryString["ReturnURL"], "")));
        }
        else
        {
            if (this.DefaultTargetUrl != "")
            {
                UrlHelper.Redirect(ResolveUrl(this.DefaultTargetUrl));
            }
            else
            {
                UrlHelper.Redirect(URLRewriter.CurrentURL);
            }
        }
    }

    
    /// <summary>
    /// Ligging in handler
    /// </summary>
    void Login1_LoggingIn(object sender, LoginCancelEventArgs e)
    {
        // Ban IP addresses which are blocked for login
        if (!BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.Login))
        {
            e.Cancel = true;

            LocalizedLiteral failureLit = Login1.FindControl("FailureText") as LocalizedLiteral;
            if (failureLit != null)
            {
                failureLit.Visible = true;
                failureLit.Text = ResHelper.GetString("banip.ipisbannedlogin");
            }                        
        }

        //if (((CheckBox)Login1.FindControl("chkRememberMe")).Checked)
        //{
        //    Login1.RememberMeSet = true;
        //}
        //else
        //{
        //    Login1.RememberMeSet = false;
        //}
    }


    ///<summary>
    /// Overrides the generation of the SPAN tag with custom tag.
    ///</summary>
    protected HtmlTextWriterTag TagKey
    {
        get
        {
            if (CMSContext.CurrentSite != null)
            {
                if (SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSControlElement").ToLower().Trim() == "div")
                {
                    return HtmlTextWriterTag.Div;
                }
                else
                {
                    return HtmlTextWriterTag.Span;
                }
            }
            return HtmlTextWriterTag.Span;
        }
    }
}
