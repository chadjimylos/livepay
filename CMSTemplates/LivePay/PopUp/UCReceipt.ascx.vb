﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports LivePay_ESBBridge
Imports CMS.Controls

Partial Class CMSTemplates_LivePay_PopUp_UCReceipt
    Inherits CMSUserControl
#Region "Variables"
    Private sTransID As String = Nothing
    Private bIsForSearch As Boolean = False
    Private mSystem As LivePay_ESBBridge.SystemEnum
    Private TimeoutLivePayBridge As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutLivePayBridge"))
#End Region

#Region "Properties"

    Public Property TransID() As String
        Get
            Return sTransID
        End Get
        Set(ByVal value As String)
            sTransID = value
        End Set
    End Property

    Public Property System() As LivePay_ESBBridge.SystemEnum
        Get
            Return mSystem
        End Get
        Set(ByVal value As LivePay_ESBBridge.SystemEnum)
            mSystem = value
        End Set
    End Property
#End Region

#Region "Methods"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        btnPrint.Src = ResHelper.LocalizeString("{$=/app_themes/LivePay/popup/btnPrint.png|en-us=/app_themes/LivePay/popup/btnPrint_en-us.png$}")
        tdPopTransDateTime.InnerHtml = ResHelper.LocalizeString("{$=Ημερομηνία & Ώρα|en-us=Date & Time $}")

        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()

        End If
    End Sub

    Public Overloads Sub ReloadData()
        If Me.TransID <> String.Empty Then


            'Response.Write("TransID1  -> " & TransID)
            If Me.System = LivePay_ESBBridge.SystemEnum.Merchants Then
                Me.TransID = Replace(Me.TransID, " ", String.Empty)
                If Me.TransID.StartsWith("01") Then Me.TransID = Right(Me.TransID, (Me.TransID.Length - 2))

                'Get Invoice
                Dim Invoice As LivePay.Invoice
                Invoice = LivePay.Invoice.GetInvoice(Me.TransID)

                If Not Invoice Is Nothing Then
                    ph_Invoice.Visible = True
                    tdCompanyName.InnerText = Invoice.CompanyName
                    tdProfession.InnerText = Invoice.Profession
                    tdAddress.InnerText = Invoice.Address
                    tdPostalCode.InnerText = Invoice.PostalCode
                    tdCity.InnerText = Invoice.City
                    tdVATNumber.InnerText = Invoice.VATNumber
                    tdTaxOffice.InnerText = Invoice.TaxOffice
                Else
                    ph_Invoice.Visible = False
                End If
            End If



            Dim MerchantName As String = String.Empty
            Dim w As New LivePay_ESBBridge.Bridge
            w.Timeout = TimeoutLivePayBridge

            Dim TransResponse As TransactionDetailsResponse = w.GetTransactionDetails(TransID, Me.System)
            Dim TransInfo As TransactionDetailsInfo = TransResponse.TransactionDetails


            btnSaveToPDF.CommandArgument = Me.TransID
            Session("TransReceipt_TransIDForExport") = Me.TransID
            Session("TransReceipt_SystemForExport") = Me.System

            btnSaveToPDF.Attributes.Add("onclick", "return ExportDetails('/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=TransReceipt&lang=" & CMSContext.CurrentDocumentCulture.CultureCode & "')")
            btnPrint.Attributes.Add("onclick", "window.open('/CMSTemplates/LivePay/PrintPage.aspx?ExportType=TransReceipt&lang=" & CMSContext.CurrentDocumentCulture.CultureCode & "')")
            'Response.Write("TransID21  -> " & TransID)
            'Response.Write(" Me.System  -> " & Me.System)
            'Response.End()
            tdPrice.InnerHtml = Me.TransID
            If Not IsNothing(TransInfo) Then
                If TransInfo.transactionStatus = TxnResultEnum.Failed Then
                    ErrosCell.Visible = True
                    tdErrorMessage.InnerHtml = GetESBError(TransInfo.spdhresp)
                End If

                tdDtm.InnerHtml = TransInfo.transactionDate
                If Me.System = LivePay_ESBBridge.SystemEnum.Public Then
                    'tdPayCode.InnerHtml = TransInfo.info1
                    tdPayCode.InnerHtml = "---"
                    tdPopTransDateTime.InnerHtml = ResHelper.LocalizeString("{$=Ημερομηνία|en-us=Date$}")
                Else
                    tdPayCode.InnerHtml = TransInfo.transactionId
                End If
                tdCustCode.InnerHtml = TransInfo.customerId
                tdTransType.InnerHtml = TransInfo.transactionType.ToString
                Dim amount As String = Replace(TransInfo.transactionAmount, ".", ",")


                Dim FinalAmount As String
                If amount.Split(",").Length > 1 Then
                    FinalAmount = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
                ElseIf amount.Split(",").Length = 1 Then
                    FinalAmount = amount & ",00"
                Else
                    FinalAmount = amount
                End If
                tdPrice.InnerHtml = FinalAmount
                Dim CardNo As String = Replace(TransInfo.cardNumber, " ", String.Empty)
                tdCardNo.InnerHtml = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))

                If TransInfo.installments > 1 Then
                    tdInstallments.InnerHtml = TransInfo.installments
                    plc_Installments.Visible = True
                End If

                Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(TransInfo.merchantId, CMSContext.CurrentDocumentCulture.CultureCode)
                Dim LivePay_MerchantID As Integer = 0

                If dtMerc.Rows.Count > 0 Then
                    LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
                End If

                Dim dt As DataTable = DBConnection.GetMerchantByID(LivePay_MerchantID, CMSContext.CurrentDocumentCulture.CultureCode)

                If dt.Rows.Count > 0 Then
                    MerchantName = dt.Rows(0)("DiscreetTitle").ToString
                    Me.imgLogo.ImageUrl = "/getattachment/" & dt.Rows(0)("Logo").ToString & "/logo.aspx"
                    If dt.Rows(0)("Logo").ToString.Length = 0 Then
                        Me.imgLogo.Visible = False
                    End If

                End If
                TdCompanyTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=Πληροφορίες συναλλαγής - ", MerchantName, "|en-us=Transaction Information - ", MerchantName, "$}")) ' MerchantName
                tdCompPay.InnerHtml = MerchantName
                CreateCustomFields(TransInfo.merchantId, TransInfo)
            End If
        End If
    End Sub

    Private Sub CreateCustomFields(ByVal MerchantID As String, ByVal TransInfo As TransactionDetailsInfo)
        Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(MerchantID, CMSContext.CurrentDocumentCulture.CultureCode)
        Dim LivePay_MerchantID As Integer = 0
        If dtMerc.Rows.Count > 0 Then
            LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
        End If

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(LivePay_MerchantID)
        Dim dt As DataTable = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim CustomTR As New HtmlTableRow
            Dim CustomTDTitle As New HtmlTableCell
            Dim CustomTDValue As New HtmlTableCell
            CustomTDTitle.Attributes.Add("class", "PopTransRecTitle")
            CustomTDValue.Attributes.Add("class", "PopTransDetValue")
            CustomTDTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
            Select Case i
                Case 0
                    CustomTDValue.InnerHtml = TransInfo.info1
                Case 1
                    CustomTDValue.InnerHtml = TransInfo.info2
                Case 2
                    CustomTDValue.InnerHtml = TransInfo.info3
                Case 3
                    CustomTDValue.InnerHtml = TransInfo.info4
                Case 4
                    CustomTDValue.InnerHtml = TransInfo.info5
                Case 5
                    CustomTDValue.InnerHtml = TransInfo.info6
                Case 6
                    CustomTDValue.InnerHtml = TransInfo.info7
                Case 7
                    CustomTDValue.InnerHtml = TransInfo.info8
                Case 8
                    CustomTDValue.InnerHtml = TransInfo.info9
                Case 9
                    CustomTDValue.InnerHtml = TransInfo.info10
            End Select

            CustomTR.Controls.Add(CustomTDTitle)
            CustomTR.Controls.Add(CustomTDValue)
            tblCustomFields.Controls.Add(CustomTR)
        Next
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

    Function GetTransactionStatusDescr(ByVal status As LivePay_ESBBridge.TxnResultEnum) As String
        'Failed  1, Reversal 2, Successful 0, Successful_Uploaded 4, Voided 3, Voided_Uploaded 5
        Select Case status
            Case 0 : Return "Επιτυχημένη"
            Case 1 : Return "Αποτυχημένη"
            Case 2 : Return "Αντιλογισμός"
            Case 3 : Return "Ακυρωμένη"
            Case 4 : Return "Επιτυχημένη Συναλλαγή"
            Case 5 : Return "Επιτυχημένη Συναλλαγή"
        End Select
        Return status
    End Function

#End Region

    Private Function GetESBError(ByVal ErrorCode As String) As String
        'Response.Write("CMSContext.CurrentDocumentCulture.CultureCode & " & CMSContext.CurrentDocumentCulture.CultureCode)
        ''ErrorCode = String.Empty
        'Response.Write("ErrorCode" & ErrorCode = "")
        'Response.End()
        Dim dt As DataTable = DBConnection.GetESBError(CMSContext.CurrentDocumentCulture.CultureCode, "")
        If dt.Rows.Count > 0 Then
            Return ErrorCode & ": " & dt.Rows(0)("ErrorMessage").ToString
        End If
    End Function

    Protected Sub btnSaveToPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveToPDF.Click
        Session("TransReceipt_TransIDForExport") = btnSaveToPDF.CommandArgument
        Response.Redirect("/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=TransReceipt&lang=" & CMSContext.CurrentDocumentCulture.CultureCode)
    End Sub
End Class
