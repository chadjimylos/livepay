using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;

using CMS.GlobalHelper;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_ImportExportSubscribers_Subscriber_Export : CMSNewsletterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Set the page title
        CurrentMaster.Title.TitleText = ResHelper.GetString("Subscriber_Export.HeaderCaption");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Newsletter/exportsubscriber.png");
        CurrentMaster.Title.HelpTopicName = "export_subscribers_tab";
        CurrentMaster.Title.HelpName = "helpTopic";

        usNewsletters.WhereCondition = "NewsletterSiteID = " + CMSContext.CurrentSiteID;
        
        btnExport.Click += new EventHandler(btnExport_Click);

        // Initialize texts
        lblExportedSub.Text = ResHelper.GetString("Subscriber_Export.lblExportedSub");
        lblSelectSub.Text = ResHelper.GetString("Subscriber_Export.lblSelectSub");
        btnExport.Text = ResHelper.GetString("Subscriber_Export.btnExport");
    }


    /// <summary>
    /// Handle export button click
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event arguments</param>
    void btnExport_Click(object sender, EventArgs e)
    {
        // Check "manage subscribers" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "managesubscribers"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "managesubscribers");
        }

        // Get selected newsletters
        ArrayList newsletterIds = new ArrayList();
        string values = ValidationHelper.GetString(usNewsletters.Value, null);
        if (!String.IsNullOrEmpty(values))
        {
            string[] newItems = values.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                foreach (string item in newItems)
                {
                    newsletterIds.Add(ValidationHelper.GetInteger(item, 0));
                }
            }
        }

        // Export subscribers
        string subscribers = "";
        if (CMSContext.CurrentSite != null)
        {
            subscribers = SubscriberProvider.ExportSubscribersFromSite(newsletterIds, CMSContext.CurrentSite.SiteID, true);
        }

        // No subscribers exported
        if (subscribers == "")
        {
            lblInfo.Text = ResHelper.GetString("Subscriber_Export.noSubscribersExported");
            lblInfo.Visible = true;
            txtExportSub.Enabled = false;
        }
        else
        {
            lblInfo.Text = ResHelper.GetString("Subscriber_Export.subscribersExported");
            lblInfo.Visible = true;
            txtExportSub.Enabled = true;
        }

        txtExportSub.Text = subscribers;
    }
}