<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ItemsList.ascx.cs" Inherits="CMSModules_Reporting_Tools_ItemsList" %>
<table>
    <tr>
        <td>
            <asp:ListBox ID="lstItems" runat="server" Width="200px" Height="120px" />
        </td>
        <td style="vertical-align: top">
            <table cellspacing="0" cellpadding="1">
                <tr>
                    <td>
                        <cms:LocalizedButton runat="server" ID="btnAdd" EnableViewState="false" CssClass="ContentButton"
                            ResourceString="general.add" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <cms:LocalizedButton runat="server" ID="btnEdit" EnableViewState="false" CssClass="ContentButton"
                            ResourceString="general.edit" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <cms:LocalizedButton runat="server" ID="btnDelete" EnableViewState="false" CssClass="ContentButton"
                            ResourceString="general.delete" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <cms:LocalizedButton runat="server" ID="btnInsert" EnableViewState="false" CssClass="ContentButton"
                            ResourceString="ItemList.Insert" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<cms:CMSButton runat="server" ID="btnHdnDelete" CssClass="HiddenButton" OnClick="btnHdnDelete_Click" EnableViewState="false" />
<asp:HiddenField runat="server" ID="hdnItemId" />
