using System;
using System.Data;
using System.Drawing;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSAdminControls_UI_Selectors_MacroSelector : CMSUserControl
{
    #region "Variables"

    private bool mShowExtendedControls = false;

    /// <summary>
    /// Macro resolver.
    /// </summary>
    protected MacroResolver mResolver = null;


    /// <summary>
    /// Javascript function called on submit.
    /// </summary>
    protected string mJavaScripFunction = "InsertMacro";

    #endregion


    #region "Properties"

    /// <summary>
    /// Macro resolver
    /// </summary>
    public MacroResolver Resolver
    {
        get
        {
            return mResolver;
        }
        set
        {
            mResolver = value;
        }
    }


    /// <summary>
    /// Javascript function called on submit
    /// </summary>
    public string JavaScripFunction
    {
        get
        {
            return mJavaScripFunction;
        }
        set
        {
            mJavaScripFunction = value;
        }
    }


    /// <summary>
    /// Show extended controls
    /// </summary>
    public bool ShowExtendedControls
    {
        get
        {
            return mShowExtendedControls;
        }
        set
        {
            mShowExtendedControls = value;
        }
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (mResolver == null)
        {
            // Hide all controls
            pnlTextBox.Visible = false;
            pnlTextDataBox.Visible = false;
            pnlMacroType.Visible = false;
            pnlDrop1.Visible = false;
            pnlDrop2.Visible = false;
            pnlInsert.Visible = false;

            lblError.Text = "[MacroSelector.ascx]: You need to define \"Resolver\" property to work this control properly.";
            lblError.ForeColor = Color.Red;
            lblError.Visible = true;

            return;
        }

        if (!RequestHelper.IsPostBack())
        {
            // Hide all controls
            pnlTextBox.Visible = false;
            pnlDrop1.Visible = false;
            pnlDrop2.Visible = false;
            lblError.Visible = false;

            pnlExtend.Visible = this.ShowExtendedControls;
            pnlBottomInsert.Visible = this.ShowExtendedControls;
            pnlInsert.Visible = !this.ShowExtendedControls;

            // Get default macro list (categories)
            ArrayList list = mResolver.GetAvailableMacros("", "");
            if (list.Count != 0)
            {
                // Clear dropdown list
                drpMacroType.Items.Clear();

                // Fill dropdown list with data
                foreach (MacroDefinition macroDef in list)
                {
                    drpMacroType.Items.Add(new ListItem(macroDef.DisplayName, macroDef.CodeName));
                }

                MacroDefinition firstMacro = (MacroDefinition)list[0];

                // Save macro type
                ltlType.Text = firstMacro.Type.ToString();

                // Save macro pattern
                ltlPattern.Text = firstMacro.Macro;

                // Show dropdown
                pnlDrop1.Visible = true;
                pnlTextDataBox.Visible = false;
                pnlTextBox.Visible = false;

                // Run dropdown method
                drpMacroType_SelectedIndexChanged(sender, e);
            }
        }
    }


    protected void drpMacroType_SelectedIndexChanged(object sender, EventArgs e)
    {
        // Get list of macros
        ArrayList list = mResolver.GetAvailableMacros(drpMacroType.SelectedValue, "");

        // Clear dropdowns
        drpDataTable.Items.Clear();
        drpDataColumn.Items.Clear();

        if (list.Count != 0)
        {
            if (list.Count > 0)
            {
                // Fill table data dropdown
                foreach (MacroDefinition macroDef in list)
                {
                    drpDataTable.Items.Add(new ListItem(macroDef.DisplayName, macroDef.CodeName));
                }
            }

            MacroDefinition firstMacro = (MacroDefinition)list[0];

            // Save macro type
            ltlType.Text = firstMacro.Type.ToString();

            // Save macro patter
            ltlPattern.Text = firstMacro.Macro;

            // If text input macro
            if ((list.Count == 1) && (firstMacro.Type == MacroDefinitionTypeEnum.Textvalue))
            {
                // Show textbox
                pnlDrop1.Visible = false;
                pnlDrop2.Visible = false;
                pnlTextBox.Visible = true;
                pnlTextDataBox.Visible = false;
            }
            else
            {
                // Run dropdown method
                drpDataTable_SelectedIndexChanged(sender, e);
            }
        }
    }


    protected void drpDataTable_SelectedIndexChanged(object sender, EventArgs e)
    {
        // List of macros
        ArrayList list;

        // If category is data get column names for data
        if (drpMacroType.SelectedValue == MacroResolver.MACRO_CATEGORY_DATA)
        {
            list = mResolver.GetAvailableMacros(MacroResolver.MACRO_CATEGORY_DATA, drpDataTable.SelectedValue);
        }
        // If category is context value get additional context data
        else
        {
            if (drpMacroType.SelectedValue == MacroResolver.MACRO_CATEGORY_CONTEXT)
            {
                list = mResolver.GetAvailableMacros(MacroResolver.MACRO_CATEGORY_CONTEXT, drpDataTable.SelectedValue);
            }
            else
            {
                list = mResolver.GetAvailableMacros(drpDataTable.SelectedValue, "");
            }
        }

        // Clear dropdown
        drpDataColumn.Items.Clear();

        if (list.Count != 0)
        {
            if (list.Count > 0)
            {
                // Fill table column dropdown
                foreach (MacroDefinition macroDef in list)
                {
                    drpDataColumn.Items.Add(new ListItem(macroDef.DisplayName, macroDef.CodeName));
                }

                // Show dropdown
                pnlDrop1.Visible = true;

                // Save macro type
                ltlType.Text = ((MacroDefinition)list[0]).Type.ToString();

                // Save macro pattern if not text value
                ltlPattern.Text = ((MacroDefinition)list[0]).Macro;

                switch (drpMacroType.SelectedValue)
                {
                    case MacroResolver.MACRO_CATEGORY_DATA:

                        if (((MacroDefinition)list[0]).Type == MacroDefinitionTypeEnum.Textvalue)
                        {
                            // If Data category and text value show textbox
                            pnlDrop2.Visible = false;
                            pnlTextBox.Visible = false;
                            pnlTextDataBox.Visible = true;
                        }
                        else
                        {
                            // If Data category and no text value show dropdown
                            pnlDrop2.Visible = true;
                            pnlTextBox.Visible = false;
                            pnlTextDataBox.Visible = false;
                        }

                        break;

                    case MacroResolver.MACRO_CATEGORY_CONTEXT:

                        if (((MacroDefinition)list[0]).Type == MacroDefinitionTypeEnum.Textvalue)
                        {
                            // If Context category and text value hide textbox and dropdown
                            pnlDrop2.Visible = false;
                            pnlTextBox.Visible = false;
                            pnlTextDataBox.Visible = false;
                        }
                        else
                        {
                            // If Context category and no text value show dropdown
                            pnlDrop2.Visible = true;
                            pnlTextBox.Visible = false;
                            pnlTextDataBox.Visible = false;
                        }

                        break;

                    default:

                        // Hide second dropdown and textbox in othe cases
                        pnlDrop2.Visible = false;
                        pnlTextBox.Visible = false;
                        pnlTextDataBox.Visible = false;

                        break;
                }
            }
        }
        else
        {
            // Clear hidden values, hide textbox and show empty dropdown
            ltlType.Text = "";
            ltlPattern.Text = "";
            pnlDrop2.Visible = true;
            pnlTextBox.Visible = false;
            pnlTextDataBox.Visible = false;
        }
    }


    protected void drpDataColumn_SelectedIndexChanged(object sender, EventArgs e)
    {
        // List of macros
        ArrayList list;

        // If category is data get column names for data
        if (drpMacroType.SelectedValue == MacroResolver.MACRO_CATEGORY_DATA)
        {
            list = mResolver.GetAvailableMacros(MacroResolver.MACRO_CATEGORY_DATA, drpDataTable.SelectedValue + "." + drpDataColumn.SelectedValue);
        }
        // If category is context value get additional context data
        else
        {
            if (drpMacroType.SelectedValue == MacroResolver.MACRO_CATEGORY_CONTEXT)
            {
                list = mResolver.GetAvailableMacros(MacroResolver.MACRO_CATEGORY_CONTEXT, drpDataTable.SelectedValue);
            }
            else
            {
                list = mResolver.GetAvailableMacros(drpDataTable.SelectedValue, "");
            }
        }

        if (list.Count != 0)
        {
            // Save macro type
            ltlType.Text = ((MacroDefinition)list[0]).Type.ToString();

            // Save macro pattern
            ltlPattern.Text = ((MacroDefinition)list[0]).Macro;

            // Show both dropdowns
            pnlDrop1.Visible = true;
            pnlDrop2.Visible = true;
            pnlTextBox.Visible = false;
            pnlTextDataBox.Visible = false;
        }
    }


    protected void btnInsert_Click(object sender, EventArgs e)
    {
        string macro = createMacro(ltlPattern.Text);
        ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "btnScript", ScriptHelper.GetScript(insertScript(macro)));
    }


    /// <summary>
    /// Return macro string from selected values
    /// </summary>
    private string createMacro(string pattern)
    {
        if (pattern != null)
        {
            string macro = null;
            string macroValue = null;

            // If there is macro pattern
            if (pattern.Contains("##VALUE##"))
            {
                // If data or context category create macro from table and column value
                if ((drpMacroType.SelectedValue == MacroResolver.MACRO_CATEGORY_DATA) || (drpMacroType.SelectedValue == MacroResolver.MACRO_CATEGORY_CONTEXT))
                {
                    // If is selected data column
                    if (drpDataColumn.SelectedValue != "")
                    {
                        if ((drpDataTable.SelectedValue == MacroResolver.MACRO_DATA_DEFAULT) || (drpDataTable.SelectedValue == MacroResolver.MACRO_CONTEXT_DEFAULT))
                        {
                            // For default macros use only column value
                            macroValue = ValidationHelper.GetString(drpDataColumn.SelectedValue, "");}
                        else
                        {
                            // For normal data macros use table.column
                            macroValue = ValidationHelper.GetString(drpDataTable.SelectedValue, "") + "." + ValidationHelper.GetString(drpDataColumn.SelectedValue, "");
                        }
                    }
                    else
                    {
                        if (txtInputData.Visible == false)
                        {
                            // If no data column [for some context macros]
                            macroValue = ValidationHelper.GetString(drpDataTable.SelectedValue, "");
                        }
                        else
                        {
                            if (drpDataTable.SelectedValue == MacroResolver.MACRO_DATA_MANUAL)
                            {
                                // For manual data macros
                                macroValue = ValidationHelper.GetString(txtInputData.Text, "");
                            }
                            else
                            {
                                // For custom data macros
                                macroValue = ValidationHelper.GetString(drpDataTable.SelectedValue, "") + "." + ValidationHelper.GetString(txtInputData.Text, "");
                            }
                        }
                    }
                }
                else
                {
                    macroValue = ValidationHelper.GetString(txtInput.Text, "");
                }

                if (macroValue.Trim() == "")
                {
                    return null;
                }

                // Add culture parameter
                if (txtCulture.Text.Trim() != "")
                {
                    macroValue += "|(culture)" + txtCulture.Text.Trim();
                }

                // Add format parameter
                if (txtFormat.Text.Trim() != "")
                {
                    macroValue += "|(format)" + txtFormat.Text.Trim();
                }

                // Add default parameter
                if (txtDefault.Text.Trim() != "")
                {
                    macroValue += "|(default)" + txtDefault.Text.Trim();
                }

                macro = pattern.Replace("##VALUE##", macroValue);
            }
            else
            {
                if (pattern.Trim() != "")
                {
                    // Add parameters
                    macroValue = "";

                    // Add culture parameter
                    if (txtCulture.Text.Trim() != "")
                    {
                        macroValue += "|(culture)" + txtCulture.Text.Trim();
                    }

                    // Add format parameter
                    if (txtFormat.Text.Trim() != "")
                    {
                        macroValue += "|(format)" + txtFormat.Text.Trim();
                    }

                    // Add default parameter
                    if (txtDefault.Text.Trim() != "")
                    {
                        macroValue += "|(default)" + txtDefault.Text.Trim();
                    }

                    macro = pattern.Insert(pattern.Length - 2, macroValue);
                    if (macro.Length == 4)
                    {
                        // Length 4 = empty pattern
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            // Return macro string
            return macro.ToLower();
        }
        return null;
    }


    /// <summary>
    /// Returns insert script for macro.
    /// </summary>
    private string insertScript(string macro)
    {
        StringBuilder builder = new StringBuilder();

        // Call javascript macro function
        builder.Append("if (typeof(window." + mJavaScripFunction + ") == 'function') {");
        builder.Append(mJavaScripFunction + "('" + macro + "');");
        builder.Append("} else { alert('No insert function!'); } ");

        return builder.ToString();
    }

    #endregion
}
