using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Forums;
using CMS.UIControls;
using CMS.SiteProvider;

public partial class CMSModules_Forums_FormControls_ForumSelector : CMS.FormControls.FormEngineUserControl
{
    #region "Properties"

    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            EnsureChildControls();
            base.Enabled = value;
            uniSelector.Enabled = value;
            uniSelector.TextBoxSelect.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            EnsureChildControls();
            return uniSelector.Value;
        }
        set
        {
            EnsureChildControls();
            uniSelector.Value = value;

        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to allow more than one user to select
    /// </summary>
    public SelectionModeEnum SelectionMode
    {
        get
        {
            EnsureChildControls();
            return uniSelector.SelectionMode;
        }
        set
        {
            EnsureChildControls();
            uniSelector.SelectionMode = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether 'Create adHoc forum' option should be displayed
    /// </summary>
    public bool DisplayAdHocOption
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("DisplayAdHocOption"), true);
        }
        set
        {
            EnsureChildControls();
            this.SetValue("DisplayAdHocOption", value);
        }
    }


    /// <summary>
    /// Gets or sets the sitename. If sitename is defined selector displays forums only from this site. 
    /// If sitename is not defined, selector displays forums for current site
    /// </summary>
    public string SiteName
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("SiteName"), CMSContext.CurrentSiteName);
        }
        set
        {
            EnsureChildControls();
            this.SetValue("SiteName", value);
        }
    }


    /// <summary>
    /// Indicates if control is used on live site
    /// </summary>
    public override bool IsLiveSite
    {
        get
        {
            return base.IsLiveSite;
        }
        set
        {
            EnsureChildControls();
            base.IsLiveSite = value;
            uniSelector.IsLiveSite = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether 'All forums' option should be displayed in dropdown list
    /// </summary>
    public bool DisplayAllForumsOption
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("DisplayAllForumsOption"), false);
        }
        set
        {
            EnsureChildControls();
            this.SetValue("DisplayAllForumsOption", value);
        }
    }


    /// <summary>
    /// Sets the property value of control, setting the value affects only local property value
    /// </summary>
    /// <param name="propertyName">Property name</param>
    /// <param name="value">Value</param>
    public override void SetValue(string propertyName, object value)
    {
        // Add special behavior for selection mode
        if (propertyName.ToLower() == "selectionmode")
        {
            this.SelectionMode = (SelectionModeEnum)Enum.Parse(typeof(SelectionModeEnum), Convert.ToString(value));
        }
        
        base.SetValue(propertyName, value);
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set uniselector
        uniSelector.DisplayNameFormat = "{%ForumDisplayName%}";
        uniSelector.ReturnColumnName = "ForumName";
        uniSelector.AllowEmpty = false;
        uniSelector.AllowAll = false;
        if (this.DisplayAllForumsOption)
        {

            uniSelector.SpecialFields = new string[2, 2] { { ResHelper.GetString("general.selectall"), "" }, 
                                                           { ResHelper.GetString("ForumSelector.AdHocForum"), "ad_hoc_forum" } };
        }
        else
        {
            uniSelector.SpecialFields = new string[1, 2] { { ResHelper.GetString("ForumSelector.AdHocForum"), "ad_hoc_forum" } };
        }

        // Set resource prefix based on mode
        if ((this.SelectionMode == SelectionModeEnum.Multiple) || (this.SelectionMode == SelectionModeEnum.MultipleButton) || (this.SelectionMode == SelectionModeEnum.MultipleTextBox))
        {
            uniSelector.ResourcePrefix = "forumsselector";
            this.uniSelector.FilterControl = "~/CMSModules/Forums/Filters/ForumGroupFilter.ascx";
        }
        else
        {
            uniSelector.ResourcePrefix = "forumselector";
        }

        int siteId = 0;

        SiteInfo si = SiteInfoProvider.GetSiteInfo(this.SiteName);
        if (si != null)
        {
            siteId = si.SiteID;
        }

        // Select non group forum of current site
        uniSelector.WhereCondition = "ForumDocumentID IS NULL AND ForumGroupID IN (SELECT GroupID FROM Forums_ForumGroup WHERE GroupGroupID IS NULL AND GroupSiteID = " + siteId + ")";
        uniSelector.SetValue("SiteID", siteId);
    }


    /// <summary>
    /// Creates child controls and loads update panle container if it is required
    /// </summary>
    protected override void CreateChildControls()
    {
        // If selector is not defined load updat panel container
        if (uniSelector == null)
        {
            this.pnlUpdate.LoadContainer();
        }
        // Call base method
        base.CreateChildControls();
    }

    #endregion
}
