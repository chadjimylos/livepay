using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.FormControls;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_FormControls_CurrencySelector : FormEngineUserControl
{
    #region "Variables"

    private bool mShowAllItems = false;
    private bool mAddNoneRecord = false;
    private bool mCheckUser = false;
    private bool mRenderInline = false;
    private bool mAddSiteDefaultCurrency = false;
    private bool mExcludeSiteDefaultCurrency = false;
    private StatusEnum mDisplayItems = StatusEnum.Both;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Indicates whether to show all items ("more items" is not displayed).
    /// </summary>
    public bool ShowAllItems
    {
        get
        {
            return this.mShowAllItems;
        }
        set
        {
            this.mShowAllItems = value;
        }
    }


    /// <summary>
    /// Indicates whether to render update panel in inline mode.
    /// </summary>
    public bool RenderInline
    {
        get
        {
            return this.mRenderInline;
        }
        set
        {
            this.mRenderInline = value;
        }
    }


    /// <summary>
    /// If is true, currency is checke if is enabled and have current exchange table
    /// </summary>
    public bool CheckUser
    {
        get
        {
            return mCheckUser;
        }
        set
        {
            mCheckUser = value;
        }
    }


    /// <summary>
    /// Add none record to the dropdownlist
    /// </summary>
    public bool AddNoneRecord
    {
        get
        {
            return mAddNoneRecord;
        }
        set
        {
            mAddNoneRecord = value;

            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            this.uniSelector.AllowEmpty = value;
        }
    }


    /// <summary>
    /// Indicates whether to add current site default currency
    /// </summary>
    public bool AddSiteDefaultCurrency
    {
        get
        {
            return mAddSiteDefaultCurrency;
        }
        set
        {
            mAddSiteDefaultCurrency = value;
        }
    }


    /// <summary>
    /// Indicates whether to exclude current site default currency
    /// </summary>
    public bool ExcludeSiteDefaultCurrency
    {
        get
        {
            return mExcludeSiteDefaultCurrency;
        }
        set
        {
            mExcludeSiteDefaultCurrency = value;
        }
    }


    /// <summary>
    /// Indicates which currencies should be displayed (Enabled/Disabled/Both). Default values is 'Both'.
    /// </summary>
    public StatusEnum DisplayItems
    {
        get
        {
            return mDisplayItems;
        }
        set
        {
            mDisplayItems = value;
        }
    }


    /// <summary>
    /// Currency ID
    /// </summary>
    public int CurrencyID
    {
        get
        {
            return ValidationHelper.GetInteger(uniSelector.Value, 0);
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }
            uniSelector.Value = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.CurrencyID;
        }
        set
        {
            this.CurrencyID = ValidationHelper.GetInteger(value, 0);
        }
    }


    /// <summary>
    /// Returns ClientID of the dropdownlist.
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect.ClientID;
        }
    }


    /// <summary>
    /// Returns inner DropDownList control.
    /// </summary>
    public DropDownList DropDownSingleSelect
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect;
        }
    }


    /// <summary>
    /// Returns inner UniSelector control.
    /// </summary>
    public UniSelector UniSelector
    {
        get
        {
            return this.uniSelector;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        if (this.RenderInline)
        {
            this.pnlUpdate.RenderMode = UpdatePanelRenderMode.Inline;
        }
        if (this.ShowAllItems)
        {
            this.uniSelector.MaxDisplayedItems = 1000;
        }

        this.uniSelector.EnabledColumnName = "CurrencyEnabled";
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.AllowEmpty = this.AddNoneRecord;

        CurrencyInfo main = CurrencyInfoProvider.GetMainCurrency();

        string where = "";
        if (this.CheckUser && (CMSContext.CurrentUser != null) && !CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            ExchangeTableInfo tableInfo = ExchangeTableInfoProvider.GetLastValidExchangeTableInfo();
            if (tableInfo != null)
            {
                where = "(CurrencyID = " + main.CurrencyID + " OR CurrencyID IN (SELECT ExchangeRateToCurrencyID FROM COM_CurrencyExchangeRate WHERE COM_CurrencyExchangeRate.ExchangeTableID = " + tableInfo.ExchangeTableID + ") AND CurrencyEnabled = 1)";
            }
        }

        if (this.AddSiteDefaultCurrency)
        {
            if (where != "")
            {
                where += " OR ";
            }

            if (main != null)
            {
                where += "CurrencyID = " + main.CurrencyID;
            }
        }

        if (this.ExcludeSiteDefaultCurrency)
        {
            if (where != "")
            {
                where += " AND ";
            }

            if (main != null)
            {
                where += "(NOT CurrencyID = " + main.CurrencyID + ")";
            }
        }

        if (this.DisplayItems == StatusEnum.Disabled)
        {
            if (where != "")
            {
                where += " AND ";
            }
            where += "CurrencyEnabled = 0";
        }
        else if (this.DisplayItems == StatusEnum.Enabled)
        {
            if (where != "")
            {
                where += " AND ";
            }
            where += "CurrencyEnabled = 1";
        }

        this.uniSelector.WhereCondition = where;
    }
}
