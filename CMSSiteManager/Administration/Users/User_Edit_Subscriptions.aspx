<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User_Edit_Subscriptions.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSSiteManager_Administration_Users_User_Edit_Subscriptions"
    Theme="Default" %>

<%@ Register Src="~/CMSModules/Membership/Controls/Subscriptions.ascx" TagName="Subscriptions"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:LocalizedLabel runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" ResourceString="Administration-User_List.ErrorGlobalAdmin" />
    <cms:Subscriptions ID="elemSubscriptions" runat="server" IsLiveSite="false" />
</asp:Content>
