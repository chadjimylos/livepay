<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyProfile_ChangePassword.aspx.cs"
    Inherits="CMSDesk_MyDesk_MyProfile_MyProfile_ChangePassword" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="MyProfile - Change password" %>

<%@ Register Src="~/CMSAdminControls/UI/ChangePassword.ascx"
    TagName="ChangePassword" TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:ChangePassword ID="ucChangePassword" runat="server" Visible="true" AllowEmptyPassword="true" />
</asp:Content>
