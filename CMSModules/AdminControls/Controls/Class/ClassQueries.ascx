<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClassQueries.ascx.cs"
    Inherits="CMSModules_AdminControls_Controls_Class_ClassQueries" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<cms:UniGrid runat="server" ID="uniGrid" GridName="ClassQueries.xml" OrderBy="QueryName"
    IsLiveSite="true" />