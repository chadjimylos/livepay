using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Threading;

using CMS.GlobalHelper;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_ShowPreview : CMSNewsletterPage
{
    #region "Variables"

    protected Guid subscriberGuid = Guid.Empty;
    protected int newsletterIssueId = 0;
    private Issue issue = null;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        subscriberGuid = QueryHelper.GetGuid("subscriberguid", Guid.Empty);
        newsletterIssueId = QueryHelper.GetInteger("issueid", 0);

        // Get newsletter issue
        issue = IssueProvider.GetIssue(newsletterIssueId);
        if (issue == null)
        {
            return;
        }

        // Get subscriber
        Subscriber subscriber = SubscriberProvider.GetSubscriber(subscriberGuid);

        // Get the newsletter
        Newsletter news = NewsletterProvider.GetNewsletter(issue.IssueNewsletterID);
        if (news == null)
        {
            return;
        }

        // Get site default culture
        string culture = CultureHelper.GetDefaultCulture(CMSContext.CurrentSiteName);

        // Ensure preview in default site culture
        // Keep old culture
        System.Globalization.CultureInfo oldUICulture = Thread.CurrentThread.CurrentUICulture;

        // Set current culture
        Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(culture);

        string htmlPage = "";
        try
        {
            if (subscriber != null)
            {
                // Get subscriber's member (different for user or role subscribers)
                SortedDictionary<int, Subscriber> subscribers = SubscriberProvider.GetSubscribers(subscriber, 1, 0, null);
                foreach (KeyValuePair<int, Subscriber> item in subscribers)
                {
                    // Get 1st subscriber's member
                    Subscriber sb = item.Value;

                    // Prepare data
                    object[] data = new object[3];
                    data[0] = issue;
                    data[1] = sb;
                    data[2] = news;

                    htmlPage = IssueProvider.GetEmailBody(issue, news, null, sb, true, CMSContext.CurrentSiteName, null, null, null);
                    htmlPage = IssueHelper.ResolveMacros(data, htmlPage, culture);
                }
            }

            if (string.IsNullOrEmpty(htmlPage))
            {
                // Prepare data
                object[] data = new object[2];
                data[0] = issue;
                data[1] = news;

                htmlPage = IssueProvider.GetEmailBody(issue, news, null, null, true, CMSContext.CurrentSiteName, null, null, null);
                htmlPage = IssueHelper.ResolveMacros(data, htmlPage, culture);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            // Set back to old culture
            Thread.CurrentThread.CurrentUICulture = oldUICulture;
        }

        Response.Clear();
        Response.Write(htmlPage);
        
        RequestHelper.EndResponse();
    }
}
