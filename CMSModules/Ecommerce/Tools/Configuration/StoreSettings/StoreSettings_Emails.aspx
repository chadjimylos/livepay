<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StoreSettings_Emails.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_StoreSettings_StoreSettings_Emails"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <div class="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <table style="vertical-align: top">
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblEmailFrom" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtEmailFrom" runat="server" CssClass="TextBoxField" MaxLength="100"
                        EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblEmailTo" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtEmailTo" runat="server" CssClass="TextBoxField" MaxLength="100"
                        EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblSendOrderNotification" EnableViewState="false" />
                </td>
                <td>
                    <asp:CheckBox ID="chkSendOrderNotification" runat="server" CssClass="CheckBoxMovedLeft"
                        EnableViewState="false" Checked="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblSendPaymentNotification" EnableViewState="false" />
                </td>
                <td>
                    <asp:CheckBox ID="chkSendPaymentNotification" runat="server" CssClass="CheckBoxMovedLeft"
                        EnableViewState="false" Checked="false" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                        CssClass="SubmitButton" ValidationGroup="Internal" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
