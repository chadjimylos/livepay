<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailTemplate_List.aspx.cs"
    Inherits="CMSSiteManager_Development_EmailTemplates_EmailTemplate_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Email Templates - Template List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector" TagPrefix="cms" %>
<asp:Content ID="cntSiteSelector" runat="server" ContentPlaceHolderID="plcControls">
    <cms:LocalizedLabel runat="server" ID="lblSite" EnableViewState="false" DisplayColon="true"
        ResourceString="General.Site" />
    <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" />
</asp:Content>
<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cms:UniGrid runat="server" ID="uniGrid" GridName="emailtemplate_list.xml" OrderBy="EmailTemplateDisplayName"
                columns="EmailTemplateID, EmailTemplateDisplayName, EmailTemplateSiteID" IsLiveSite="false" />
        </ContentTemplate>
    </cms:CMSUpdatePanel>
</asp:Content>
