﻿Imports System.Data
Imports CMS.SettingsProvider
Imports CMS.SiteProvider
Imports CMS.DataEngine

Imports System
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Partial Class CMSTemplates_LivePay_xmlHttpCheckUsername
    Inherits System.Web.UI.Page

    Protected ReadOnly Property NewUserName() As String
        Get
            Dim sNewUserName As String = String.Empty
            If Not String.IsNullOrEmpty(Request("NewUserName")) Then
                sNewUserName = Request("NewUserName")
            End If
            Return sNewUserName
        End Get
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim IsValidMail As Integer = 0
        If Me.NewUserName <> String.Empty Then
            Dim ui As UserInfo = UserInfoProvider.GetUserInfo(Me.NewUserName)
            Dim CurrentUser As String = CMSContext.CurrentUser.UserName
            If Me.NewUserName = CurrentUser Then
                IsValidMail = 1
            Else
                If Not IsNothing(ui) Then
                    IsValidMail = 0
                Else
                    IsValidMail = 1
                End If
            End If
        Else
            IsValidMail = 0
        End If
        Response.Write(IsValidMail)
    End Sub

End Class
