using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.FormControls;
using CMS.UIControls;
using CMS.URLRewritingEngine;

public partial class CMSSiteManager_Settings_keys : SiteManagerPage
{
    #region "Protected variables"

    protected int siteId;
    protected int categoryId;
    protected string siteName = String.Empty;
    protected SettingsCategoryInfo sci = null;

    #endregion

    /// <summary>
    /// Settings key form controls information
    /// </summary>
    public struct SettingsKeyItem
    {
        public string settingsKey;       // Settings key code name
        public string inputControlId;    // Checkbox id, textbox id
        public string inheritId;         // Inherit checkbox id
        public bool isInherited;
        public string errorLabelId;      // Error label id
        public string value;             // Value
        public string type;              // Type (int, boolean)
        public string validation;        // Regex vor validation
        public bool changed;             // Changed flag
    }


    /// <summary>
    /// Settings keys list of current category
    /// </summary>
    readonly List<SettingsKeyItem> keyItems = new List<SettingsKeyItem>();


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register tooltip script
        RegisterTooltipScript();

        categoryId = QueryHelper.GetInteger("categoryid", 0);
        sci = SettingsCategoryInfoProvider.GetSettingsCategoryInfo(categoryId);

        if (sci == null)
        {
            plcContent.Controls.Add(new LiteralControl(ResHelper.GetString("Settings-Keys.NoCategorySelectedMsg")));
            return;
        }

        siteId = QueryHelper.GetInteger("siteid", 0);
        if (siteId > 0)
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);
            if (si != null)
            {
                siteName = si.SiteName;
            }
        }

        CurrentMaster.Title.TitleText = ResHelper.LocalizeString(sci.CategoryDisplayName);
        CurrentMaster.Title.TitleImage = GetImageUrlForHeader(sci.CategoryName);

        string[,] actions = new string[2, 9];

        // Save changes button
        actions[0, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[0, 1] = ResHelper.GetString("Header.Settings.SaveChanged");
        actions[0, 2] = " SaveDocument(); ";
        actions[0, 3] = null;
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        actions[0, 6] = "lnkSaveChanges_Click";
        actions[0, 7] = String.Empty;
        actions[0, 8] = true.ToString();

        actions[1, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[1, 1] = ResHelper.GetString("Header.Settings.ResetToDefault");
        actions[1, 2] = " if (confirm(" + ScriptHelper.GetString(ResHelper.GetString("Header.Settings.ResetToDefaultConfirmation")) +
                        ")) { return true; } return false;";
        actions[1, 3] = null;
        actions[1, 4] = null;
        actions[1, 5] = GetImageUrl("Objects/CMS_SettingsKey/object.png");
        actions[1, 6] = "lnkResetToDefault_Click";
        actions[1, 7] = String.Empty;

        CurrentMaster.HeaderActions.Actions = actions;
        CurrentMaster.HeaderActions.ActionPerformed += HeaderActions_ActionPerformed;

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>"
            +
            ScriptHelper.GetScript("function SaveDocument() { " + ClientScript.GetPostBackEventReference(lnkSaveChanges, null) + " } \n"
        ));

        string helpTopicName = String.Empty;
        if (!String.IsNullOrEmpty(sci.CategoryName))
        {
            // Set proper HelpTopicName
            switch (sci.CategoryName.ToLower())
            {
                case "cms.content": helpTopicName = "content_management"; break;

                case "cms.staging": helpTopicName = "content_staging"; break;

                case "cms.ecommerce": helpTopicName = "e_commerce"; break;

                case "cms.email": helpTopicName = "e_mails"; break;

                case "cms.eventmanager": helpTopicName = "booking_system"; break;

                case "cms.files": helpTopicName = "files"; break;

                case "cms.ecommerce.authorizenet": helpTopicName = "payment___authorize_net"; break;

                case "cms.ecommerce.paypal": helpTopicName = "payment___paypal"; break;

                case "cms.security": helpTopicName = "security"; break;

                case "cms.system": helpTopicName = "system"; break;

                case "cms.url": helpTopicName = "urls"; break;

                case "cms.webanalytics": helpTopicName = "web_analytics2"; break;

                case "cms.website": helpTopicName = "web_site"; break;

                case "cms.outputfilter": helpTopicName = "output_filter"; break;

                case "cms.badwords": helpTopicName = "settings_administration"; break;

                case "cms.windowsliveid": helpTopicName = "windowsliveid"; break;

                case "cms.community": helpTopicName = "settings_community"; break;

                case "cms.membership": helpTopicName = "settings_membership"; break;

                case "cms.avatars": helpTopicName = "settings_avatars"; break;

                case "cms.messageboards": helpTopicName = "settings_message_boards"; break;

                case "cms.blogs": helpTopicName = "settings_blogs"; break;

                case "cms.forums": helpTopicName = "settings_forums"; break;

                case "cms.messaging": helpTopicName = "settings_messaging"; break;

                case "cms.medialibraries": helpTopicName = "settings_medialibraries"; break;

                case "cms.openid": helpTopicName = "openid"; break;

                case "cms.facebookconnect": helpTopicName = "fbconnect"; break;

                case "cms.sharepoint": helpTopicName = "sharepoint"; break;

                case "cms.metaweblogapi": helpTopicName = "metaweblogapi"; break;
            }
        }

        CurrentMaster.Title.HelpTopicName = helpTopicName;

        string noCategorySelectedMsg = ResHelper.GetString("Settings-Keys.NoCategorySelectedMsg");

        if (categoryId != 0)
        {
            Label errorLabel;
            Label mLabel = new Label();

            mLabel.ID = "lblInfo";
            mLabel.Visible = false;
            mLabel.EnableViewState = false;
            plcContent.Controls.Add(mLabel);

            if (QueryHelper.GetBoolean("resettodefault", false))
            {
                mLabel.Text = ResHelper.GetString("Settings-Keys.ValuesWereResetToDefault");
                mLabel.Visible = true;
            }

            // Add Global settings
            if (siteId <= 0)
            {
                string noSiteInfo = ResHelper.GetString("Settings-Keys.GlobalSettingsNote");
                string text = ((RequestHelper.IsPostBack() || mLabel.Visible) ? "<br /><br />" + noSiteInfo : noSiteInfo);
                plcContent.Controls.Add(new LiteralControl(text + "<br />"));
            }

            plcContent.Controls.Add(new LiteralControl("<br /><table border=\"0\" cellpadding=\"0\" cellspacing=\"10\">"));

            // Load settings keys into DataSet
            DataSet keysSet = SettingsKeyProvider.GetSettingsKeysOrdered(siteId, categoryId);
            if (!DataHelper.DataSourceIsEmpty(keysSet))
            {
                DataTable keyTable = keysSet.Tables[0];

                int i = 0;
                foreach (DataRow keyRow in keyTable.Rows)
                {
                    i++;
                    plcContent.Controls.Add(new LiteralControl("<tr><td style=\"padding: 0px 0px 0px 0px;\">"));

                    string keyTooltip = ResHelper.LocalizeString(keyRow["KeyDescription"].ToString());

                    // Key display name label
                    Label labelKeyName = new Label();
                    labelKeyName.EnableViewState = false;
                    labelKeyName.ID = "lblDispName_" + i;
                    labelKeyName.Text = ResHelper.LocalizeString(keyRow["KeyDisplayName"].ToString());
                    labelKeyName.Attributes.Add("style", "cursor: help;");
                    labelKeyName.Attributes.Add("onmouseover", "Tip('" + ScriptHelper.FormatTooltipString(keyTooltip, false) + "')");
                    labelKeyName.Attributes.Add("onmouseout", "UnTip()");
                    plcContent.Controls.Add(labelKeyName);
                    plcContent.Controls.Add(new LiteralControl("</td><td>"));

                    // Help image
                    Image helpImg = new Image();
                    helpImg.EnableViewState = false;
                    helpImg.ID = "imgHelp_" + i;
                    helpImg.ImageUrl = GetImageUrl("CMSModules/CMS_Settings/help.png");
                    helpImg.Attributes.Add("onmouseover", "Tip('" + ScriptHelper.FormatTooltipString(keyTooltip, false) + "')");
                    helpImg.Attributes.Add("onmouseout", "UnTip()");
                    plcContent.Controls.Add(helpImg);
                    plcContent.Controls.Add(new LiteralControl("</td><td style=\"padding: 0px 5px 0px 0px\">"));

                    SettingsKeyItem skr = new SettingsKeyItem();

                    CheckBox inheritCheckBox = null;

                    if (siteId > 0)
                    {
                        // Inherit checkbox
                        inheritCheckBox = new CheckBox();
                        inheritCheckBox.ID = "chkInher_" + i;
                        inheritCheckBox.CheckedChanged += inheritCheckBox_CheckedChanged;
                        inheritCheckBox.AutoPostBack = true;
                        inheritCheckBox.Text = ResHelper.GetString("Settings-Keys.CheckBoxInheritGlobalMsg");

                        skr.inheritId = inheritCheckBox.ID;
                    }

                    bool inheritChkChecked = false;

                    // Get formcontrol and load it
                    string formControl = Convert.ToString(keyRow["KeyEditingControlPath"]);
                    FormEngineUserControl control = null;
                    if (!String.IsNullOrEmpty(formControl))
                    {
                        try
                        {
                            control = Page.LoadControl(formControl) as FormEngineUserControl;
                        }
                        catch { }
                    }

                    // Add placeholder for the editing control
                    PlaceHolder plcControl = new PlaceHolder();
                    plcControl.ID = "plcControl" + i;
                    plcContent.Controls.Add(plcControl);

                    skr.settingsKey = keyRow["KeyName"].ToString();

                    plcContent.Controls.Add(new LiteralControl("</td><td>"));

                    if (inheritCheckBox != null)
                    {
                        plcContent.Controls.Add(inheritCheckBox);
                        plcContent.Controls.Add(new LiteralControl("</td><td>"));
                    }

                    if (control != null)
                    {
                        // Initialize usercontrol
                        control.ID = "userKey_" + i;
                        control.IsLiveSite = false;
                        skr.inputControlId = control.ID;

                        if ((keyRow["KeyValue"] == DBNull.Value) && (siteId > 0))
                        {
                            inheritChkChecked = true;
                            control.Value = SettingsKeyProvider.GetValue(Convert.ToString(keyRow["keyName"]).ToLower());
                        }
                        else
                        {
                            inheritChkChecked = false;
                            string keyName = GetFullKeyName(keyRow["keyName"].ToString()).ToLower();
                            control.Value = SettingsKeyProvider.GetValue(keyName);
                        }

                        plcControl.Controls.Add(control);
                        if (UrlHelper.IsPostback() && (inheritCheckBox != null))
                        {
                            control.Enabled = (Request.Form[inheritCheckBox.UniqueID] == null);
                        }
                        else
                        {
                            control.Enabled = !inheritChkChecked;
                        }

                        skr.value = Convert.ToString(control.Value);
                    }
                    else
                    {
                        switch (keyRow["KeyType"].ToString().ToLower())
                        {
                            case "boolean":
                                // CheckBox
                                {
                                    CheckBox chbox = new CheckBox();
                                    if ((keyRow["KeyValue"] == DBNull.Value) && (siteId > 0))
                                    {
                                        inheritChkChecked = true;
                                        chbox.Checked = SettingsKeyProvider.GetBoolValue(keyRow["keyName"].ToString().ToLower());
                                    }
                                    else
                                    {
                                        inheritChkChecked = false;
                                        string keyName = GetFullKeyName(keyRow["keyName"].ToString()).ToLower();
                                        chbox.Checked = SettingsKeyProvider.GetBoolValue(keyName);
                                    }

                                    if (IsPostBack && (inheritCheckBox != null))
                                    {
                                        chbox.Enabled = (Request.Form[inheritCheckBox.UniqueID] == null);
                                    }
                                    else
                                    {
                                        chbox.Enabled = !inheritChkChecked;
                                    }
                                    chbox.ID = "chkKey_" + i;
                                    plcControl.Controls.Add(chbox);
                                    skr.inputControlId = chbox.ID;
                                    skr.value = chbox.Checked.ToString();
                                }
                                break;

                            default:
                                // TextBox
                                {
                                    TextBox tbox = new TextBox();
                                    if ((keyRow["KeyValue"] == DBNull.Value) && (siteId > 0))
                                    {
                                        inheritChkChecked = true;
                                        tbox.Text = SettingsKeyProvider.GetStringValue(keyRow["keyName"].ToString());
                                    }
                                    else
                                    {
                                        inheritChkChecked = false;
                                        tbox.Text = keyRow["KeyValue"].ToString();
                                    }

                                    if (IsPostBack && (inheritCheckBox != null))
                                    {
                                        tbox.Enabled = (Request.Form[inheritCheckBox.UniqueID] == null);
                                    }
                                    else
                                    {
                                        tbox.Enabled = !inheritChkChecked;
                                    }
                                    tbox.ID = "txtKey_" + i;
                                    tbox.CssClass = "TextBoxField";
                                    plcControl.Controls.Add(tbox);
                                    skr.inputControlId = tbox.ID;
                                    skr.value = tbox.Text;
                                }
                                break;
                        }
                    }

                    // Set the inherit checkbox state
                    if ((inheritCheckBox != null) && !IsPostBack)
                    {
                        inheritCheckBox.Checked = inheritChkChecked;
                    }

                    // Set the inherited status
                    skr.isInherited = inheritChkChecked;

                    // Key name label
                    labelKeyName = new Label();
                    labelKeyName.EnableViewState = false;
                    labelKeyName.ID = "lblKeyName_" + i;
                    labelKeyName.Text = keyRow["KeyName"].ToString();
                    labelKeyName.Visible = false;

                    plcContent.Controls.Add(labelKeyName);

                    skr.validation = keyRow["KeyValidation"] == DBNull.Value ? null : ValidationHelper.GetString(keyRow["KeyValidation"], null);

                    if ((siteId > 0) && (skr.validation == null))
                    {
                        SettingsKeyInfo ski = SettingsKeyProvider.GetSettingsKeyInfo(skr.settingsKey, 0);
                        if (ski != null)
                        {
                            skr.validation = ski.KeyValidation;
                        }
                    }

                    // Add error label if KeyType is integer or validation expression defined or FormControl is used
                    if ((keyRow["KeyType"].ToString() == "int") || (skr.validation != null) || (control != null))
                    {
                        errorLabel = new Label();
                        errorLabel.EnableViewState = false;
                        errorLabel.ID = "lblError_" + i;
                        errorLabel.EnableViewState = false;
                        errorLabel.CssClass = "ErrorLabel";
                        errorLabel.Attributes.Add("style", "padding: 4px 0px 0px 0px");
                        plcContent.Controls.Add(errorLabel);

                        skr.errorLabelId = errorLabel.ID;
                    }

                    plcContent.Controls.Add(new LiteralControl("</td>"));

                    skr.type = ValidationHelper.GetString(keyRow["KeyType"], String.Empty);
                    keyItems.Add(skr);
                }
            }

            // Add Export settings link
            plcContent.Controls.Add(new LiteralControl("<tr><td><br /><a href=\"GetSettings.aspx?siteid=" + siteId + "&categoryid=" + categoryId + "\">" + ResHelper.GetString("Settings-Keys.ExportSettings") + "</a></td></tr>"));
            plcContent.Controls.Add(new LiteralControl("</table>"));
        }
        else
        {
            plcContent.Controls.Add(new LiteralControl(noCategorySelectedMsg));
        }
    }


    protected void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "lnksavechanges_click":
                lnkSaveChangesClick();
                break;

            case "lnkresettodefault_click":
                lnkResetToDefaultClick();
                break;
        }
    }


    /// <summary>
    /// Reset current settings category to default values.
    /// </summary>
    protected void lnkResetToDefaultClick()
    {
        if ((siteId >= 0) && (categoryId > 0))
        {
            // Retrieve all keys from specified category
            DataSet ds = SettingsKeyProvider.GetSettingsKeys(siteId, categoryId);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                // Set default value to all keys (one by one)
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    int keyId = ValidationHelper.GetInteger(dr["KeyID"], 0);
                    if (keyId > 0)
                    {
                        SettingsKeyInfo ski = SettingsKeyProvider.GetSettingsKeyInfo(keyId);
                        if (ski != null)
                        {
                            ski.KeyValue = ski.KeyDefaultValue;
                            SettingsKeyProvider.SetValue(ski);
                        }
                    }
                }

                UrlHelper.Redirect(string.Format("keys.aspx?resettodefault=1&categoryid={0}&siteid={1}", categoryId, siteId));
            }
        }
    }


    /// <summary>
    /// Saves changed items only.
    /// </summary>
    public void lnkSaveChangesClick()
    {
        bool validationFailed = false;

        // Loop through current settings items
        for (int i = 0; i < keyItems.Count; i++)
        {
            SettingsKeyItem skr = keyItems[i];

            // Get input control (textbox, checkbox)
            Control cntrl = plcContent.FindControl(skr.inputControlId);
            if ((cntrl == null) || (cntrl.ID == null))
            {
                continue;
            }

            bool changed = false;
            if (cntrl is TextBox)
            {
                TextBox txt = cntrl as TextBox;
                changed = (txt.Text != skr.value);
                skr.value = txt.Text;
            }
            else if (cntrl is CheckBox)
            {
                CheckBox chk = cntrl as CheckBox;
                changed = chk.Checked.ToString() != skr.value;
                skr.value = chk.Checked.ToString();
            }
            else
            {
                FormEngineUserControl control = cntrl as FormEngineUserControl;
                if (control != null)
                {
                    if (control.IsValid())
                    {
                        changed = Convert.ToString(control.Value) != skr.value;
                        skr.value = Convert.ToString(control.Value);
                    }
                    else
                    {
                        Label errorLabel = (Label)plcContent.FindControl(skr.errorLabelId);
                        errorLabel.Text = ResHelper.GetString("Settings.ValidationError");
                        return;
                    }
                }
            }

            // Check inherited checkbox (if any)
            if (skr.inheritId != null)
            {
                Control checkBox = plcContent.FindControl(skr.inheritId);

                if ((checkBox != null) && (checkBox is CheckBox))
                {
                    changed = changed || (checkBox as CheckBox).Checked != skr.isInherited;
                    skr.isInherited = (checkBox as CheckBox).Checked;
                }
            }

            skr.changed = changed;
            if (changed)
            {
                if (skr.isInherited)
                {
                    FormEngineUserControl control = cntrl as FormEngineUserControl;
                    if (control != null)
                    {
                        control.Value = SettingsKeyProvider.GetValue(skr.settingsKey);
                    }
                }

                // Validation result
                string result = String.Empty;

                // Validation using regular expression if any
                if ((skr.validation != null) && (skr.validation.Trim() != String.Empty))
                {
                    result = new Validator().IsRegularExp(skr.value, skr.validation, ResHelper.GetString("Settings.ValidationRegExError")).Result;
                }

                // Validation according to value type (validate only nonempty values)
                if ((result == String.Empty) && !String.IsNullOrEmpty(skr.value))
                {
                    switch (skr.type.ToLower())
                    {
                        case "int":
                            result = new Validator().IsInteger(skr.value, ResHelper.GetString("Settings.ValidationIntError")).Result;
                            break;

                        case "double":
                            result = new Validator().IsDouble(skr.value, ResHelper.GetString("Settings.ValidationDoubleError")).Result;
                            break;
                    }
                }

                if (result != String.Empty)
                {
                    Label errorLabel = (Label)plcContent.FindControl(skr.errorLabelId);
                    errorLabel.Text = result;
                    validationFailed = true;
                }
                else
                {
                    // Update changes
                    keyItems[i] = skr;
                }
            }
        }

        Label lblInfo = (Label)plcContent.FindControl("lblInfo");
        if (validationFailed)
        {
            lblInfo.CssClass = "ErrorLabel";
            lblInfo.Text = ResHelper.GetString("general.saveerror");
            lblInfo.Visible = true;
            return;
        }

        // Update changes in database/hashtables
        bool logSynchronization = (sci.CategoryName.ToLower() != "cms.staging");

        for (int i = 0; i < keyItems.Count; i++)
        {
            SettingsKeyItem tmp = keyItems[i];

            if (tmp.changed)
            {
                object keyValue = DBNull.Value;
                if (!tmp.isInherited)
                {
                    keyValue = tmp.value;
                }
                string keyName = tmp.settingsKey;

                ObjectHelper.SetSettingsKeyValue(GetFullKeyName(keyName), keyValue, logSynchronization);

                // Clear the cached items
                switch (keyName.ToLower())
                {
                    case "cmsgooglesitemapurl":
                        URLRewriter.GoogleSiteMapURL = null;
                        break;

                    case "cmsemailsenabled":
                        if ((siteId <= 0) && (keyValue.ToString().Equals("false", StringComparison.OrdinalIgnoreCase)))
                        {
                            // Stop current sending of e-mails and newsletters
                            // if e-mails are disabled in global settings
                            ModuleCommands.CancelEmailSending();
                            ModuleCommands.CancelNewsletterSending();
                        }
                        break;
                }
            }
        }

        // Clear the cache to apply the settings to the web site
        CacheHelper.ClearCache(null);

        lblInfo.Text = ResHelper.GetString("general.changessaved");
        lblInfo.Visible = true;
    }


    /// <summary>
    /// Handles 'inherit from global settings' checkboxes.
    /// </summary>
    protected void inheritCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chboxSender = (CheckBox)sender;//inherit from global settings?
        int indx = ValidationHelper.GetInteger(chboxSender.ID.Substring(chboxSender.ID.IndexOf('_') + 1), 0);

        if ((indx > 0) && (indx <= keyItems.Count))
        {
            Control ctrl = plcContent.FindControl(keyItems[indx - 1].inputControlId);

            if (ctrl is CheckBox)
            {
                // Set the default value for the control
                CheckBox chbox = (CheckBox)ctrl;
                chbox.Enabled = !chboxSender.Checked;
                chbox.Checked = SettingsKeyProvider.GetBoolValue(keyItems[indx - 1].settingsKey);
            }
            else if (ctrl is TextBox)
            {
                // Set the default value for the control
                TextBox tb = (TextBox)ctrl;
                tb.Enabled = !chboxSender.Checked;
                tb.Text = SettingsKeyProvider.GetStringValue(keyItems[indx - 1].settingsKey);
            }
            else
            {
                FormEngineUserControl control = ctrl as FormEngineUserControl;
                if (control != null)
                {
                    // Set the default value for the control
                    control.Enabled = !chboxSender.Checked;
                    control.Value = SettingsKeyProvider.GetValue(keyItems[indx - 1].settingsKey);
                }
            }
        }
    }


    /// <summary>
    /// Returns settings key full name
    /// </summary>
    /// <param name="keyName">Settings key</param>
    protected string GetFullKeyName(string keyName)
    {
        if (siteId > 0)
        {
            return siteName + "." + keyName;
        }

        return keyName;
    }


    public void lnkSaveChanges_Click(object sender, EventArgs e)
    {
        lnkSaveChangesClick();
    }


    /// <summary>
    /// Returns image URL for category
    /// </summary>
    /// <param name="categoryName">Category name</param>
    private string GetImageUrlForHeader(string categoryName)
    {
        string imageUrl = String.Empty;

        if (categoryName != "")
        {
            imageUrl = GetImageUrl("CMSModules/CMS_Settings/Categories/") + categoryName.Replace(".", "_") + "/module.png";

            // Check if category icon exists and then use it
            if (File.Exists(UrlHelper.GetPhysicalPath(imageUrl)))
            {
                imageUrl = ResolveUrl(imageUrl);
            }
            // Otherwise use default icon
            else
            {
                imageUrl = ResolveUrl(GetImageUrl("CMSModules/CMS_Settings/Categories/module.png"));
            }
        }

        return imageUrl;
    }
}
