using System;

using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.CMSHelper;
using CMS.SiteProvider;

public partial class CMSModules_Content_FormControls_Documents_SelectPath : FormEngineUserControl
{
    #region "Variables"

    private bool mEnableSiteSelection = false;
    private DialogConfiguration mConfig = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Gets the configuration for Copy and Move dialog.
    /// </summary>
    private DialogConfiguration Config
    {
        get
        {
            if (this.mConfig == null)
            {
                this.mConfig = new DialogConfiguration();
                this.mConfig.HideLibraries = true;                
                this.mConfig.HideAnchor = true;
                this.mConfig.HideAttachments = true;
                this.mConfig.HideContent = false;
                this.mConfig.HideEmail = true;
                this.mConfig.HideLibraries = true;
                this.mConfig.HideWeb = true;
                this.mConfig.EditorClientID = this.txtPath.ClientID;
                this.mConfig.ContentSites = (ControlsHelper.CheckControlContext(this, ControlContext.WIDGET_PROPERTIES) ? AvailableSitesEnum.OnlyCurrentSite : AvailableSitesEnum.All);
                this.mConfig.ContentSelectedSite = CMSContext.CurrentSiteName;                
                this.mConfig.OutputFormat = OutputFormatEnum.Custom;
                this.mConfig.CustomFormatCode = "selectpath";
                this.mConfig.SelectableContent = SelectableContentEnum.AllContent;
            }
            return this.mConfig;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.txtPath.Enabled = value;
            this.btnSelectPath.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.txtPath.Text;
        }
        set
        {
            this.txtPath.Text = (string)value;
        }
    }


    /// <summary>
    /// Gets ClientID of the textbox with path
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.txtPath.ClientID;
        }
    }


    /// <summary>
    /// Determines whether to enable site selection or not.
    /// </summary>
    public bool EnableSiteSelection
    {
        get
        {
            return this.mEnableSiteSelection;
        }
        set
        {
            this.mEnableSiteSelection = value;
            this.Config.ContentSites = (value ? AvailableSitesEnum.All : AvailableSitesEnum.OnlyCurrentSite);
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        this.btnSelectPath.Text = ResHelper.GetString("general.select");
        this.btnSelectPath.OnClientClick = "modalDialog('" + GetDialogUrl() + "','PathSelection', '90%', '85%'); return false;";
    }

    /// <summary>
    /// Returns Correct URL of the copy or move dialog.
    /// </summary>
    private string GetDialogUrl()
    {
        string url = CMSDialogHelper.GetDialogUrl(this.Config, this.IsLiveSite, false);
        url = UrlHelper.RemoveProtocolAndDomain(url);
        return url;
    }
}
