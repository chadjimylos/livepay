<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_Edit_CustomFields.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Ecommerce_Tools_Products_Product_Edit_CustomFields"
    Theme="Default" Title="Product edit - Custom fields" %>
    
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" ResourceString="General.ChangesSaved" />
    <cms:DataForm ID="formProductCustomFields" runat="server" IsLiveSite="false" />
</asp:Content>