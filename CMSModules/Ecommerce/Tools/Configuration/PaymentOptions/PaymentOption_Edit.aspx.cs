using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_PaymentOptions_PaymentOption_Edit : CMSEcommerceConfigurationPage
{
    protected int paymentOptionId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        // field validator error messages initialization
        rfvDisplayName.ErrorMessage = ResHelper.GetString("paymentoption_edit.errorEmptyDisplayName");
        rfvCodeName.ErrorMessage = ResHelper.GetString("paymentoption_edit.errorEmptyCodeName");

        // control initializations				
        lblPaymentOptionName.Text = ResHelper.GetString("PaymentOption_Edit.PaymentOptionNameLabel");
        lblPaymentOptionDisplayName.Text = ResHelper.GetString("PaymentOption_Edit.PaymentOptionDisplayNameLabel");
        // Gateway
        lblPaymentGateway.Text = ResHelper.GetString("PaymentOption_Edit.GatewaySettings");
        lblGateUrl.Text = ResHelper.GetString("PaymentOption_Edit.GateUrl");
        lblPaymentAssemblyName.Text = ResHelper.GetString("PaymentOption_Edit.PaymentAssemblyName");
        lblPaymentClassName.Text = ResHelper.GetString("PaymentOption_Edit.PaymentClassName");
        // Statuses
        lblStatusFailed.Text = ResHelper.GetString("PaymentOption_Edit.PaymentFailedStatus");
        lblStatusSucceed.Text = ResHelper.GetString("PaymentOption_Edit.PaymentSucceedStatus");

        btnOk.Text = ResHelper.GetString("General.OK");

        string currentPaymentOption = ResHelper.GetString("PaymentOption_Edit.NewItemCaption");

        // get paymentOption id from querystring		
        paymentOptionId = ValidationHelper.GetInteger(Request.QueryString["paymentOptionId"], 0);
        if (paymentOptionId > 0)
        {
            PaymentOptionInfo paymentOptionObj = PaymentOptionInfoProvider.GetPaymentOptionInfo(paymentOptionId);
            if (paymentOptionObj != null)
            {
                currentPaymentOption = ResHelper.LocalizeString(paymentOptionObj.PaymentOptionDisplayName);

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(paymentOptionObj);

                    // show that the paymentOption was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }

            this.CurrentMaster.Title.TitleText = ResHelper.GetString("PaymentOption_Edit.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_PaymentOption/object.png");
        }
        else // Add new
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("PaymentOption_New.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_PaymentOption/new.png");

            this.succeededElem.DisplayItems = StatusEnum.Enabled;
            this.failedElem.DisplayItems = StatusEnum.Enabled;
        }

        // initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("PaymentOption_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/PaymentOptions/PaymentOption_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentPaymentOption;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.HelpTopicName = "newedit_payment_method";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Load data of editing paymentOption.
    /// </summary>
    /// <param name="paymentOptionObj">PaymentOption object.</param>
    protected void LoadData(PaymentOptionInfo paymentOptionObj)
    {
        txtPaymentOptionName.Text = paymentOptionObj.PaymentOptionName;
        chkPaymentOptionEnabled.Checked = paymentOptionObj.PaymentOptionEnabled;
        txtPaymentOptionDisplayName.Text = paymentOptionObj.PaymentOptionDisplayName;
        txtGateUrl.Text = paymentOptionObj.PaymentOptionPaymentGateUrl;
        txtPaymentAssemblyName.Text = paymentOptionObj.PaymentOptionAssemblyName;
        txtPaymentClassName.Text = paymentOptionObj.PaymentOptionClassName;
        succeededElem.OrderStatusID = paymentOptionObj.PaymentOptionSucceededOrderStatusID;
        failedElem.OrderStatusID = paymentOptionObj.PaymentOptionFailedOrderStatusID;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        // check input values from textboxes and other contrlos
        string errorMessage = new Validator()
            .NotEmpty(txtPaymentOptionDisplayName.Text, ResHelper.GetString("paymentoption_edit.errorEmptyDisplayName"))
            .NotEmpty(txtPaymentOptionName.Text, ResHelper.GetString("paymentoption_edit.errorEmptyCodeName")).Result;

        if (!ValidationHelper.IsCodeName(txtPaymentOptionName.Text))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

        if (errorMessage == "")
        {
            // Check unique name
            PaymentOptionInfo paymentOptionObj = PaymentOptionInfoProvider.GetPaymentOptionInfo(txtPaymentOptionName.Text.Trim(), CMSContext.CurrentSiteName);

            if ((paymentOptionObj == null) || (paymentOptionObj.PaymentOptionID == paymentOptionId))
            {
                // Get the object
                if (paymentOptionObj == null)
                {
                    paymentOptionObj = PaymentOptionInfoProvider.GetPaymentOptionInfo(paymentOptionId);
                    if (paymentOptionObj == null)
                    {
                        paymentOptionObj = new PaymentOptionInfo();
                    }
                }

                paymentOptionObj.PaymentOptionID = paymentOptionId;
                paymentOptionObj.PaymentOptionDisplayName = txtPaymentOptionDisplayName.Text.Trim();
                paymentOptionObj.PaymentOptionName = txtPaymentOptionName.Text.Trim();
                paymentOptionObj.PaymentOptionEnabled = chkPaymentOptionEnabled.Checked;
                paymentOptionObj.PaymentOptionSiteID = CMSContext.CurrentSite.SiteID;
                paymentOptionObj.PaymentOptionPaymentGateUrl = txtGateUrl.Text.Trim();
                paymentOptionObj.PaymentOptionClassName = txtPaymentClassName.Text.Trim();
                paymentOptionObj.PaymentOptionAssemblyName = txtPaymentAssemblyName.Text.Trim();
                paymentOptionObj.PaymentOptionSucceededOrderStatusID = succeededElem.OrderStatusID;
                paymentOptionObj.PaymentOptionFailedOrderStatusID = failedElem.OrderStatusID;

                PaymentOptionInfoProvider.SetPaymentOptionInfo(paymentOptionObj);

                UrlHelper.Redirect("PaymentOption_Edit.aspx?paymentOptionId=" + Convert.ToString(paymentOptionObj.PaymentOptionID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("PaymentOption_Edit.PaymentOptionNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
