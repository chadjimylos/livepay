using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Threading;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.EmailEngine;
using CMS.DirectoryUtilities;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.DataEngine;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_System_Debug_System_DebugThreads : CMSDebugPage
{
    protected string cmsVersion = null;
    protected int index = 0;
    protected TimeSpan totalDuration = new TimeSpan(0);
    protected DateTime now = DateTime.Now;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.btnRunDummy.Text = ResHelper.GetString("DebugThreads.Test");

        cmsVersion = ResHelper.GetString("Footer.Version") + "&nbsp;" + CMSContext.SYSTEM_VERSION + "&nbsp;" + ResHelper.GetString("Footer.Build") + "&nbsp;" + CMSContext.FullSystemVersion;

        this.gridThreads.Columns[1].HeaderText = ResHelper.GetString("unigrid.actions");
        this.gridThreads.Columns[2].HeaderText = ResHelper.GetString("ThreadsLog.Context");
        this.gridThreads.Columns[3].HeaderText = ResHelper.GetString("ThreadsLog.ThreadID");
        this.gridThreads.Columns[4].HeaderText = ResHelper.GetString("ThreadsLog.Status");
        this.gridThreads.Columns[5].HeaderText = ResHelper.GetString("ThreadsLog.Started");
        this.gridThreads.Columns[6].HeaderText = ResHelper.GetString("ThreadsLog.Duration");

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "Cancel", ScriptHelper.GetScript(
            @"function CancelThread(threadGuid) {
                if (confirm('" + ResHelper.GetString("ViewLog.CancelPrompt") + @"')) {
                    document.getElementById('" + this.hdnGuid.ClientID + "').value = threadGuid;" +
                    this.Page.ClientScript.GetPostBackEventReference(this.btnCancel, null) +
                @"}
              }"));
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        ReloadData();
    }


    protected void ReloadData()
    {
        // Process data
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MethodClassName", typeof(string)));
        dt.Columns.Add(new DataColumn("MethodName", typeof(string)));
        dt.Columns.Add(new DataColumn("ThreadStarted", typeof(DateTime)));
        dt.Columns.Add(new DataColumn("ThreadID", typeof(int)));
        dt.Columns.Add(new DataColumn("ThreadFinished", typeof(DateTime)));
        dt.Columns.Add(new DataColumn("RequestUrl", typeof(string)));
        dt.Columns.Add(new DataColumn("Status", typeof(string)));
        dt.Columns.Add(new DataColumn("ThreadGUID", typeof(Guid)));
        dt.Columns.Add(new DataColumn("HasLog", typeof(bool)));

        for (int i = CMSThread.LiveThreads.Count - 1; i >= 0; i--)
        {
            try
            {
                // Get the log
                CMSThread thread = (CMSThread)CMSThread.LiveThreads[i];

                DataRow dr = dt.NewRow();
                dr["MethodClassName"] = thread.MethodClassName;
                dr["MethodName"] = thread.MethodName;
                dr["ThreadStarted"] = thread.ThreadStarted;
                dr["ThreadID"] = thread.ThreadID;
                dr["ThreadFinished"] = thread.ThreadFinished;
                dr["RequestUrl"] = thread.RequestUrl;
                dr["Status"] = thread.InnerThread.ThreadState.ToString();
                dr["ThreadGUID"] = thread.ThreadGUID;
                dr["HasLog"] = (thread.Log != null);

                dt.Rows.Add(dr);
            }
            catch
            {
            }
        }

        now = DateTime.Now;

        this.gridThreads.DataSource = dt;
        this.gridThreads.DataBind();
    }


    protected void btnRunDummy_Click(object sender, EventArgs e)
    {
        LogContext log = LogContext.EnsureLog(Guid.NewGuid());
        log.Reversed = true;
        log.LineSeparator = "<br />";
                
        CMSThread dummy = new CMSThread(RunTest);
        dummy.Start();

        Thread.Sleep(100);
        ReloadData();
    }


    private void RunTest()
    {
        for (int i = 0; i < 50; i++)
        {
            Thread.Sleep(100);
            LogContext.AppendLine("Sample log " + i.ToString());
        }
    }


    protected int GetIndex()
    {
        return ++index;
    }


    protected string GetDuration(object startTime)
    {
        TimeSpan duration = now.Subtract(ValidationHelper.GetDateTime(startTime, now));
        totalDuration = totalDuration.Add(duration);

        return GetDurationString(duration);
    }


    protected string GetDurationString(TimeSpan duration)
    {
        return duration.Hours + ":" + duration.Minutes.ToString().PadLeft(2, '0') + ":" + duration.Seconds.ToString().PadLeft(2, '0') + "." + duration.Milliseconds.ToString().PadLeft(3, '0');
    }


    protected string GetActions(object hasLog, object threadGuid, object status)
    {
        string result = null;

        if (ValidationHelper.GetString(status, null) != "AbortRequested")
        {
            result += "<a href=\"#\" onclick=\"CancelThread('" + threadGuid + "')\"><img src=\"" + ResolveUrl(GetImageUrl("Design/Controls/UniGrid/Actions/Delete.png")) + "\" style=\"border: none;\" alt=\"" + ResHelper.GetString("General.Cancel") + "\" /></a> ";
        }

        bool logAvailable = ValidationHelper.GetBoolean(hasLog, false);
        if (logAvailable)
        {
            result += "<a href=\"System_ViewLog.aspx?threadGuid=" + threadGuid.ToString() + "\" target=\"_blank\"><img src=\"" + ResolveUrl(GetImageUrl("Design/Controls/UniGrid/Actions/View.png")) + "\" style=\"border: none;\" alt=\"" + ResHelper.GetString("General.View") + "\" /></a> ";
        }

        return result;
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Guid threadGuid = ValidationHelper.GetGuid(this.hdnGuid.Value, Guid.Empty);
        CMSThread thread = CMSThread.GetThread(threadGuid);
        if (thread != null)
        {
            thread.Stop();
        }
    }


    protected void timRefresh_Tick(object sender, EventArgs e)
    {

    }
}
