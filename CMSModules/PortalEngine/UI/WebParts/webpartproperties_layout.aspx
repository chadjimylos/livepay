<%@ Page Language="C#" AutoEventWireup="true" CodeFile="webpartproperties_layout.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_WebParts_webpartproperties_layout" Theme="Default"
    EnableEventValidation="false" %>
<%@ Register Src="~/CMSModules/PortalEngine/FormControls/WebPartLayouts/WebPartLayoutSelector.ascx" TagPrefix="cms" TagName="LayoutSelector" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Web part properties - layout</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }
    </style>

    <script type="text/javascript">
        //<![CDATA[
        var wopener = parent.wopener;

        function RefreshPage() {
            wopener.RefreshPage();
        }
        //]]>
    </script>

</head>
<body class="TabsBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="pnlBody" CssClass="TabsPageBody">
        <asp:Panel ID="pnlScroll" runat="server" CssClass="TabsPageScrollArea">
            <asp:Panel ID="pnlTab" runat="server" CssClass="TabsPageContent">
                <asp:Panel ID="pnlMenu" runat="server" CssClass="ContentEditMenu">
                    <table>
                        <tr>
                            <asp:PlaceHolder ID="plcCheckOut" runat="server" Visible="false">
                                <td>
                                    <asp:LinkButton ID="btnCheckOut" OnClick="btnCheckOut_Click" runat="server" CssClass="MenuItemEdit">
                                        <asp:Image ID="imgCheckOut" runat="server" EnableViewState="false" />
                                        <%=mCheckOut%>
                                    </asp:LinkButton>
                                </td>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="plcCheckIn" runat="server" Visible="false">
                                <td>
                                    <asp:LinkButton ID="btnCheckIn" OnClick="btnCheckIn_Click" runat="server" CssClass="MenuItemEdit">
                                        <asp:Image ID="imgCheckIn" runat="server" EnableViewState="false" />
                                        <%=mCheckIn%>
                                    </asp:LinkButton>
                                </td>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="plcUndoCheckOut" runat="server" Visible="false" EnableViewState="false">
                                <td>
                                    <asp:LinkButton ID="btnUndoCheckOut" OnClick="btnUndoCheckOut_Click" runat="server"
                                        CssClass="MenuItemEdit">
                                        <asp:Image ID="imgUndoCheckOut" runat="server" EnableViewState="false" />
                                        <%=mUndoCheckOut%>
                                    </asp:LinkButton>
                                </td>
                            </asp:PlaceHolder>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="pnlCheckOutInfo" runat="server" CssClass="InfoLabel" Visible="false">
                    <asp:Label runat="server" ID="lblCheckOutInfo" />
                </asp:Panel>
                <asp:Panel ID="pnlFormArea" runat="server" CssClass="PageContent">
                    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
                        Visible="false">
                    </asp:Label>
                    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
                        Visible="false" />
                    <asp:PlaceHolder runat="server" Visible="true" ID="plcContent">
                        <table style="width: 100%" cellspacing="2" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <td style="white-space: nowrap;">
                                        <asp:Label ID="lblLayouts" runat="server" />
                                    </td>
                                    <td style="width: 100%;">                                        
                                        <cms:LayoutSelector runat="server" ID="selectLayout" OnChanged="drpLayouts_Changed" IsLiveSite="false" />                            
                                    </td>
                                    <asp:PlaceHolder runat="server" ID="plcDescription" Visible="false">
                                        <td rowspan="3">
                                            <cms:LocalizedLabel ID="lblDescription" runat="server" ResourceString="general.description"
                                                DisplayColon="true" />
                                        </td>
                                        <td rowspan="3">
                                            <asp:TextBox ID="txtDescription" runat="server" CssClass="TextAreaField" Rows="6"
                                                TextMode="MultiLine" />
                                        </td>
                                    </asp:PlaceHolder>
                                </tr>
                                <asp:PlaceHolder runat="server" ID="plcValues" Visible="false">
                                    <tr>
                                        <td style="white-space: nowrap;">
                                            <cms:LocalizedLabel ID="lblLayoutDisplayName" runat="server" ResourceString="general.displayname"
                                                DisplayColon="true" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLayoutDisplayName" runat="server" CssClass="TextBoxField" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="white-space: nowrap;">
                                            <cms:LocalizedLabel ID="lblLayoutName" runat="server" ResourceString="general.codename"
                                                DisplayColon="true" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLayoutName" runat="server" CssClass="TextBoxField" />
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="plcVirtualInfo" Visible="false">
                                    <tr>
                                        <td colspan="4">
                                            <br />
                                            xxxxxxxxxxxx<asp:Label runat="server" ID="lblVirtualInfo" CssClass="ErrorLabel" EnableViewState="false" />
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>
                                <tr>
                                    <td colspan="4">
                                        <br />
                                        <cms:ExtendedTextArea ID="etaCode" runat="server" Rows="20" TextMode="MultiLine"
                                            Width="100%" ReadOnly="true" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:PlaceHolder>
                    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
                    <cms:CMSButton ID="btnOnApply" runat="server" Visible="false" />
                    <cms:CMSButton ID="btnOnOK" runat="server" Visible="false" />
                    <asp:HiddenField runat="server" ID="hidRefresh" Value="0" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
