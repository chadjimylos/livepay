using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.MessageBoard;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.Community;

public partial class CMSModules_Groups_Tools_MessageBoards_Boards_Board_Edit_General : CMSGroupPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.boardEdit.OnCheckPermissions += new CMSAdminControl.CheckPermissionsEventHandler(boardEdit_OnCheckPermissions);
    }


    void boardEdit_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        int groupId = 0;

        BoardInfo bi = BoardInfoProvider.GetBoardInfo(QueryHelper.GetInteger("boardid", 0));
        if (bi != null)
        {
            groupId = bi.BoardGroupID;
        }

        CheckPermissions(groupId, CMSAdminControl.PERMISSION_MANAGE);
        
    }
}
