<%@ Control Language="C#" AutoEventWireup="true" CodeFile="largetextarea.ascx.cs" Inherits="CMSFormControls_Inputs_LargeTextArea" %>
<asp:TextBox ID="txtArea" runat="server" TextMode="MultiLine" CssClass="LargeTextAreaTextBox" />
<cms:CMSButton ID="btnMore" runat="server" Text="..." CssClass="XShortButton LargeTextAreaButton" EnableViewState="false" />
