<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyFriends_Approved.aspx.cs"
    Inherits="CMSModules_Friends_MyFriends_MyFriends_Approved" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Theme="default" %>

<%@ Register Src="~/CMSModules/Friends/Controls/FriendsList.ascx" TagName="FriendsList"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:FriendsList ID="FriendsList" runat="server" Visible="true" IsLiveSite="false" />
    <cms:LocalizedLabel ID="lblError" runat="server" ResourceString="friends.nofriends"
        EnableViewState="false" />
</asp:Content>
