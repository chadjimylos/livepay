using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.EmailEngine;
using CMS.SettingsProvider;
using CMS.EventLog;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_Users_User_Edit_Password : CMSUsersPage
{
    const string hiddenPassword = "********";

    #region "Private fields"

    private int mUserID = 0;
    private UserInfo ui = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Current user ID
    /// </summary>
    private int UserID 
    {
        get 
        {
            if (this.mUserID == 0) 
            {
                this.mUserID = QueryHelper.GetInteger("userid", 0);
            }

            return this.mUserID;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        ButtonSetPassword.Text = ResHelper.GetString("Administration-User_Edit_Password.SetPassword");
        LabelPassword.Text = ResHelper.GetString("Administration-User_Edit_Password.NewPassword");
        LabelConfirmPassword.Text = ResHelper.GetString("Administration-User_Edit_Password.ConfirmPassword");
        this.chkSendEmail.Text = ResHelper.GetString("Administration-User_Edit_Password.SendEmail");
        this.btnGenerateNew.Text = ResHelper.GetString("Administration-User_Edit_Password.gennew");
        this.btnSendPswd.Text = ResHelper.GetString("Administration-User_Edit_Password.sendpswd");        

        if (!RequestHelper.IsPostBack())
        {
            if (this.UserID > 0)            
            {
                // Check that only global administrator can edit global administrator's accouns
                ui = UserInfoProvider.GetUserInfo(UserID);
                if (!CheckGlobalAdminEdit(ui))
                {
                    plcTable.Visible = false;
                    lblError.Text = ResHelper.GetString("Administration-User_List.ErrorGlobalAdmin");
                    lblError.Visible = true;
                    return;
                }
   
                if (ui != null)
                {
                    if (ui.GetValue("UserPassword") != null)
                    {
                        string password = ui.GetValue("UserPassword").ToString();
                        if (password.Length > 0)
                        {
                            TextBoxPassword.Attributes.Add("value", hiddenPassword);
                            TextBoxConfirmPassword.Attributes.Add("value", hiddenPassword);
                        }
                    }
                }
            }
        }

        // Handle 'Send password' button
        DisplaySendPaswd();

        if (!CMSContext.CurrentUser.IsInRole("SecurityAdministrator", CMSContext.CurrentSite.SiteName.ToString()))
        {
            ButtonSetPassword.Visible = false;
        }
    }


    /// <summary>
    /// Check whether current user is allowed to modify another user. Return "" or error message.
    /// </summary>
    /// <param name="userId">Modified user</param>
    protected string ValidateGlobalAndDeskAdmin()
    {
        string result = String.Empty;

        if (CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            return result;
        }

        UserInfo userInfo = UserInfoProvider.GetUserInfo(this.UserID);
        if (userInfo == null)
        {
            result = ResHelper.GetString("Administration-User.WrongUserId");
        }
        else
        {
            if (userInfo.IsGlobalAdministrator)
            {
                result = ResHelper.GetString("Administration-User.NotAllowedToModify");
            }
        }
        return result;
    }


    #region "Event handlers"

    /// <summary>
    /// Generates new password and sends it to the user
    /// </summary>
    protected void btnGenerateNew_Click(object sender, EventArgs e) 
    {

        // Check modify permission
        CheckModifyPermissions();
                
        string result = ValidateGlobalAndDeskAdmin();

        if (result == String.Empty)
        {
            string pswd = RandomPassword.Generate(7, 16); //UserInfoProvider.GenerateNewPassword();
            string userName = UserInfoProvider.GetUserNameById(this.UserID);
            UserInfoProvider.SetPassword(userName, pswd);

            // Show actual information to the user
            if (TextBoxPassword.Text != String.Empty)
            {
                TextBoxPassword.Attributes.Add("value", hiddenPassword);
                TextBoxConfirmPassword.Attributes.Add("value", hiddenPassword);
            }
            else
            {
                TextBoxPassword.Attributes.Add("value", "");
                TextBoxConfirmPassword.Attributes.Add("value", "");
            }

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");


            // Get UserInfo of the specified name
            UserInfo user = CMS.SiteProvider.UserInfoProvider.GetUserInfo(ui.UserName);
            user.SetValue("UserLastPasswordModified", null);
            // Update the UserInfo
            CMS.SiteProvider.UserInfoProvider.SetUserInfo(user);

            // Process e-mail sending
            SendEmail(ResHelper.GetString("Administration-User_Edit_Password.NewGen"), pswd, this.UserID, "changed");

            // Log the event
            EventLogProvider eventLog = new EventLogProvider();
            eventLog.LogEvent("I", DateTime.Now, "User", "Change Password! ( " + ui.UserName + " )", CMSContext.CurrentSite.SiteID);

            ReloadPassword();
        }

        if (result != String.Empty)
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }


    /// <summary>
    /// Sends the actual password of the current user
    /// </summary>
    protected void btnSendPswd_Click(object o, EventArgs e) 
    {
        // Check permissions
        CheckModifyPermissions();

        string result = ValidateGlobalAndDeskAdmin();

        if (result == String.Empty)
        {
            string pswd = UserInfoProvider.GetUserInfo(this.UserID).GetValue("UserPassword").ToString();

            // Process e-mail sending
            SendEmail(ResHelper.GetString("Administration-User_Edit_Password.Resend"), pswd, this.UserID, "RESEND");           
        }

        if (result != String.Empty)
        {
            lblError.Visible = true;
            lblError.Text = result;
        }

        // Handle 'Send password' button
        DisplaySendPaswd();
    }


    /// <summary>
    /// Set password of current user.
    /// </summary>
    protected void ButtonSetPassword_Click(object sender, EventArgs e)
    {
        // Check modify permission
        CheckModifyPermissions();
                
        string result = ValidateGlobalAndDeskAdmin();

        if ((result == String.Empty) && (ui != null))
        {
            if (TextBoxConfirmPassword.Text == TextBoxPassword.Text)
            {
                if (TextBoxPassword.Text != hiddenPassword) //password has been changed
                {
                    string pswd = this.TextBoxPassword.Text;                    
                    UserInfoProvider.SetPassword(ui, TextBoxPassword.Text);

                    // Show actual information to the user
                    if (TextBoxPassword.Text != String.Empty)
                    {
                        TextBoxPassword.Attributes.Add("value", hiddenPassword);
                        TextBoxConfirmPassword.Attributes.Add("value", hiddenPassword);
                    }
                    else
                    {
                        TextBoxPassword.Attributes.Add("value", "");
                        TextBoxConfirmPassword.Attributes.Add("value", "");
                    }

                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");

                    if (this.chkSendEmail.Checked)
                    {
                        // Process e-mail sending
                        SendEmail(ResHelper.GetString("Administration-User_Edit_Password.Changed"), pswd, this.UserID, "CHANGED");
                    }

                    // Log the event
                    EventLogProvider eventLog = new EventLogProvider();
                    eventLog.LogEvent("I", DateTime.Now, "User", "Change Password! ( " + ui.UserName + " )", CMSContext.CurrentSite.SiteID);
                }
            }
            else
            {
                result = ResHelper.GetString("Administration-User_Edit_Password.PasswordsDoNotMatch");
            }
        }

        if (result != String.Empty)
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
        
        // Handle 'Send password' button
        DisplaySendPaswd();
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Loads the user password to the password fields
    /// </summary>
    private void ReloadPassword()
    {
        UserInfo ui = UserInfoProvider.GetUserInfo(this.UserID);
        if (ui != null) 
        {
            string passwd = ui.GetValue("UserPassword").ToString();
            if (!string.IsNullOrEmpty(passwd))
            {
                this.TextBoxPassword.Attributes.Add("value", hiddenPassword);
                this.TextBoxConfirmPassword.Attributes.Add("value", hiddenPassword);
            }
        }
    }


    /// <summary>
    /// Sends e-mail with password if required
    /// </summary>
    /// <param name="pswd">Password to send</param>
    /// <param name="toEmail">E-mail address of the mail recepient</param>
    /// <param name="subject">Subject of the e-mail sent</param>
    /// <param name="emailType">type of the e-mail specificating the template used (NEW, CHANGED, RESEND)</param>
    private void SendEmail(string subject, string pswd, int userId, string emailType)
    {
        // Check whether the 'From' elemtn was specified
        string emailFrom = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSSendPasswordEmailsFrom");
        bool fromMissing = string.IsNullOrEmpty(emailFrom);
        
        if ((!string.IsNullOrEmpty(emailType)) && (ui != null) && (!fromMissing))
        {
            if (!string.IsNullOrEmpty(ui.Email))
            {
                EmailMessage em = new EmailMessage();

                em.From = emailFrom;
                em.Recipients = ui.Email;
                em.Subject = subject;
                em.EmailFormat = EmailFormatEnum.Default;

                string templateName = null;

                // Get e-mail template name
                switch (emailType.ToLower())
                {
                    case "new":
                        templateName = "Membership.NewPassword";
                        break;

                    case "changed":
                        templateName = "Membership.ChangedPassword";
                        break;

                    case "resend":
                        templateName = "Membership.ResendPassword";
                        break;

                    default:
                        break;
                }

                // Get template info object
                if (templateName != null)
                {
                    try
                    {
                        // Get e-mail template
                        EmailTemplateInfo template = EmailTemplateProvider.GetEmailTemplate(templateName, CMSContext.CurrentSiteID);
                        if (template == null) {
                            template = EmailTemplateProvider.GetEmailTemplate(templateName, null);
                        }
                        if (template != null)
                        {
                            em.Body = template.TemplateText;

                            // Macros
                            string[,] macros = new string[3, 2];
                            macros[0, 0] = "UserName";
                            macros[0, 1] = ui.UserName;
                            macros[1, 0] = "Password";
                            macros[1, 1] = pswd;
                            macros[2, 0] = "FullName";
                            macros[2, 1] = ui.FullName;
                            // Create macro resolver
                            ContextResolver resolver = CMSContext.CurrentResolver;
                            resolver.SourceParameters = macros;

                            // Add template attachments
                            MetaFileInfoProvider.ResolveMetaFileImages(em, template.TemplateID, EmailObjectType.EMAILTEMPLATE, MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE);
                            // Send message immediately (+ resolve macros)
                            EmailSender.SendEmailWithTemplateText(CMSContext.CurrentSiteName, em, template, resolver, true);

                            // Inform on success
                            this.lblInfo.Text += " " + ResHelper.GetString("Administration-User_Edit_Password.PasswordsSent") + " " + HTMLHelper.HTMLEncode(ui.Email);
                            this.lblInfo.Visible = true;

                            return;
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log the error to the event log
                        EventLogProvider eventLog = new EventLogProvider();
                        eventLog.LogEvent("Password retrieval", "USERPASSWORD", ex);
                        this.lblError.Text = "Failed to send the password: " + ex.Message;
                    }
                }
            }
            else 
            {
                // Inform on error
                this.lblInfo.Visible = true;
                this.lblInfo.Text = ResHelper.GetString("Administration-User_Edit_Password.PassChangedNotSent");

                return;
            }
        }

        // Inform on error
        this.lblError.Visible = true;
        this.lblError.Text = ResHelper.GetString("Administration-User_Edit_Password.PasswordsNotSent");

        if (fromMissing)
        {            
            this.lblError.Text += ResHelper.GetString("Administration-User_Edit_Password.FromMissing") + " ";
        }
    }


    /// <summary>
    /// Decides whether the 'Send password' button should be displayed or not
    /// </summary>
    private void DisplaySendPaswd()
    {
        if (ui == null)
        {
            ui = UserInfoProvider.GetUserInfo(this.UserID);
        }

        if (ui != null)
        {
            // Password is not hashed by SHA1 function, allow sending
            if (ui.UserPasswordFormat.ToLower() != "sha1")
            {
                this.btnSendPswd.Visible = true;
                return;
            }
        }

        this.btnSendPswd.Visible = false;
    }


    /// <summary>
    /// Checks if the user is alloed to perform this action
    /// </summary>
    private void CheckModifyPermissions()
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify"))
        {
            if (!CMSContext.CurrentUser.IsInRole("EurophoneBanking", CMSContext.CurrentSite.SiteName.ToString()))
            {
                RedirectToAccessDenied("CMS.Users", "Modify");
            }
        }
    }

    #endregion
}
