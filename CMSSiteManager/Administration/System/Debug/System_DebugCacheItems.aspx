<%@ Page Language="C#" AutoEventWireup="true" CodeFile="System_DebugCacheItems.aspx.cs"
    Inherits="CMSSiteManager_Administration_System_Debug_System_DebugCacheItems"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Group list" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="CacheItemsGrid.ascx" TagName="CacheItemsGrid" TagPrefix="uc1" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />
    <div style="text-align: right; padding: 5px;">
        <cms:CMSButton runat="server" ID="btnClear" OnClick="btnClear_Click" CssClass="LongButton"
            EnableViewState="false" />
    </div>
    <uc1:CacheItemsGrid ID="gridItems" runat="server" />
    <uc1:CacheItemsGrid ID="gridDummy" runat="server" ShowDummyItems="true" />
</asp:Content>
