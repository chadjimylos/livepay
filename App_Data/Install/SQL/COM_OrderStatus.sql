CREATE TABLE [COM_OrderStatus] (
		[StatusID]                   int NOT NULL IDENTITY(1, 1),
		[StatusName]                 nvarchar(200) NOT NULL,
		[StatusDisplayName]          nvarchar(200) NOT NULL,
		[StatusOrder]                int NULL,
		[StatusEnabled]              bit NOT NULL,
		[StatusColor]                nvarchar(7) NULL,
		[StatusGUID]                 uniqueidentifier NOT NULL,
		[StatusLastModified]         datetime NOT NULL,
		[StatusSendNotification]     bit NULL
)
ON [PRIMARY]
ALTER TABLE [COM_OrderStatus]
	ADD
	CONSTRAINT [PK_COM_OrderStatus]
	PRIMARY KEY
	NONCLUSTERED
	([StatusID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_OrderStatus_StatusOrder_StatusDisplayName_StatusEnabled]
	ON [COM_OrderStatus] ([StatusOrder], [StatusDisplayName], [StatusEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
