using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.UIControls;

public partial class CMSMessages_Error : MessagePage
{
    protected void Page_Load(object sender, EventArgs e)
    {        
        // Set title
        string title = QueryHelper.GetText("title", "");
        if (title != "")
        {
            // Display custom title
            this.titleElem.TitleText = title;
        }
        else
        {
            // Display general title
            this.titleElem.TitleText = ResHelper.GetString("Error.Header");
        }

        this.titleElem.TitleImage = GetImageUrl("Others/Messages/error.png");

        
        // Set message text
        string pathError = QueryHelper.GetText("aspxerrorpath", "");
        if (pathError != "")
        {
            // Display path error message
            this.lblInfo.Text = String.Format(ResHelper.GetString("Error.Info"), pathError);
        }
        else
        {
            // Display general error message
            this.lblInfo.Text = QueryHelper.GetText("text", "");          
        }


        // Set button
        bool cancel = QueryHelper.GetBoolean("cancel", false);
        if (cancel)
        {
            // Display Cancel button
            this.btnCancel.Visible = true;
            this.btnCancel.Text = ResHelper.GetString("General.Cancel");
        }
        else
        {
            // Displaye link to home page
            this.lnkBack.Visible = true;
            this.lnkBack.Text = ResHelper.GetString("Error.Back");
            this.lnkBack.NavigateUrl = "~/";
        }
    }
}
