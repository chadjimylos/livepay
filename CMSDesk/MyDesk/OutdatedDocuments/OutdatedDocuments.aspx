<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OutdatedDocuments.aspx.cs"
    Inherits="CMSDesk_MyDesk_OutdatedDocuments_OutdatedDocuments" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="MyDesk - Outdated documents" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="server">
    <table>
        <tr>
            <td>
                <asp:Label ID="lblFilter" AssociatedControlID="txtFilter" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtFilter" runat="server" CssClass="SmallTextBox" EnableViewState="false" />
                <asp:DropDownList ID="drpFilter" runat="server" CssClass="ContentDropdown" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:LocalizedLabel ID="lblDocumentName" runat="server" EnableViewState="false" ResourceString="general.documentname"
                    DisplayColon="true" CssClass="FieldLabel" />
            </td>
            <td>
                <asp:DropDownList ID="drpDocumentName" runat="server" CssClass="ContentDropdown" />
                <asp:TextBox ID="txtDocumentName" runat="server" CssClass="SmallTextBox" MaxLength="100" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:LocalizedLabel ID="lblDocumentType" runat="server" EnableViewState="false" ResourceString="general.documenttype"
                    DisplayColon="true" CssClass="FieldLabel" />
            </td>
            <td>
                <asp:DropDownList ID="drpDocumentType" runat="server" CssClass="ContentDropdown" />
                <asp:TextBox ID="txtDocumentType" runat="server" CssClass="SmallTextBox" MaxLength="100" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:LocalizedButton ID="btnShow" runat="server" EnableViewState="false" CssClass="ContentButton"
                    ResourceString="general.show" />
            </td>
        </tr>
    </table>
    <div>
        <br />
        <cms:UniGrid ID="ugOutdatedDocs" runat="server" GridName="OutdatedDocuments.xml"
            OrderBy="DocumentName" IsLiveSite="false" />
    </div>
    <cms:LocalizedLabel ID="lblInfo" runat="server" CssClass="InfoLabel" ResourceString="mydesk.ui.nooutdated"
        Visible="true" EnableViewState="false" />

    <script type="text/javascript">
        //<![CDATA[
        // Select item action
        function SelectItem(nodeId, culture) {
            if (nodeId != 0) {
                parent.parent.location.href = "../../default.aspx?section=content&action=edit&nodeid=" + nodeId + "&culture=" + culture;
            }
        }
        //]]>
    </script>

</asp:Content>
