using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSAdminControls_UI_UniMenu_UniMenu : CMSUserControl
{
    #region "Variables"

    /// <summary>
    /// Groups
    /// </summary>
    private string[,] mGroups = null;
    private List<CMSUserControl> mInnerControls = null;

    private int identifier = 0;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets groups {[n;0] - Caption, [n;1] - Control path, [n;2] - CSS class}
    /// </summary>
    public string[,] Groups
    {
        get
        {
            return mGroups;
        }
        set
        {
            mGroups = value;
        }
    }


    /// <summary>
    /// Description.
    /// </summary>
    public List<CMSUserControl> InnerControls
    {
        get
        {
            return mInnerControls;
        }
        set
        {
            mInnerControls = value;
        }
    }


    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Groups != null)
        {
            ArrayList controlsList = new ArrayList();
            int groupsCount = Groups.GetUpperBound(0) + 1;
            InnerControls = new List<CMSUserControl>(groupsCount);
            for (identifier = 0; identifier < groupsCount; identifier++)
            {
                // Check array dimensions
                if (Groups.GetUpperBound(1) != 2)
                {
                    Controls.Add(GetError(ResHelper.GetString("unimenu.wrongdimensions")));
                    continue;
                }

                string capitonText = Groups[identifier, 0];
                string contentControlPath = Groups[identifier, 1];
                string cssClass = Groups[identifier, 2];

                // Caption and path to content control have to be entered
                if (string.IsNullOrEmpty(capitonText))
                {
                    Controls.Add(GetError(ResHelper.GetString("unimenu.captionempty")));
                    continue;
                }
                if (string.IsNullOrEmpty(contentControlPath))
                {
                    Controls.Add(GetError(ResHelper.GetString("unimenu.pathempty")));
                    continue;
                }
                Control contentControl = null;
                try
                {
                    // Try to load content control
                    contentControl = Page.LoadControl(contentControlPath);
                    contentControl.ID = "ctrlContent" + identifier;
                }
                catch
                {
                    Controls.Add(GetError(ResHelper.GetString("unimenu.errorloadingcontrol")));
                    continue;
                }

                Panel groupPanel = new Panel();
                groupPanel.ID = "pnlGroup" + identifier;

                // In RTL insert separator before group (on the left side of a group)
                if (CultureHelper.IsUICultureRTL() && (groupsCount > 1))
                {
                    groupPanel.Controls.Add(GetGroupSeparator());
                }

                // Add controls to main panel
                groupPanel.Controls.Add(GetLeftBorder());
                groupPanel.Controls.Add(GetContent(contentControl, capitonText));
                groupPanel.Controls.Add(GetRightBorder());

                // If culture is not RTL insert separator after group (on the right side of a group)
                if (!CultureHelper.IsUICultureRTL() && (groupsCount > 1))
                {
                    groupPanel.Controls.Add(GetGroupSeparator());
                }
                groupPanel.CssClass = cssClass;

                // Add group panel to list of controls
                controlsList.Add(groupPanel);

                InnerControls.Insert(identifier, (CMSUserControl)contentControl);
            }
            // Reverse order of controls in case of RTL culture
            if (CultureHelper.IsUICultureRTL())
            {
                controlsList.Reverse();
            }
            // Add group panels to to the control
            foreach (Control control in controlsList)
            {
                Controls.Add(control);
            }
        }
        else
        {
            Controls.Add(GetError(ResHelper.GetString("unimenu.wrongdimensions")));
        }
    }


    /// <summary>
    /// Generates div with left border
    /// </summary>
    /// <returns>Panel with left border</returns>
    protected Panel GetLeftBorder()
    {
        // Create panel and set up
        Panel leftBorder = new Panel();
        leftBorder.ID = "pnlLeftBorder" + identifier;
        leftBorder.EnableViewState = false;
        leftBorder.CssClass = "UniMenuLeftBorder";
        return leftBorder;
    }


    /// <summary>
    /// Generates div with right border
    /// </summary>
    /// <returns>Panel with right border</returns>
    protected Panel GetRightBorder()
    {
        // Create panel and set up
        Panel rightBorder = new Panel();
        rightBorder.ID = "pnlRightBorder" + identifier;
        rightBorder.EnableViewState = false;
        rightBorder.CssClass = "UniMenuRightBorder";
        return rightBorder;
    }


    /// <summary>
    /// Generates separator between groups
    /// </summary>
    /// <returns>Panel separating groups</returns>
    protected Panel GetGroupSeparator()
    {
        // Create panel and set up
        Panel groupSeparator = new Panel();
        groupSeparator.ID = "pnlGroupSeparator" + identifier;
        groupSeparator.EnableViewState = false;
        groupSeparator.CssClass = "UniMenuSeparator";
        return groupSeparator;
    }


    /// <summary>
    /// Generates div with right border
    /// </summary>
    /// <param name="captionText">Caption of group</param>
    /// <returns>Panel with right border</returns>
    protected Panel GetCaption(string captionText)
    {
        // Create literal with caption
        Literal caption = new Literal();
        caption.ID = "ltlCaption" + identifier;
        caption.EnableViewState = false;
        caption.Text = captionText;

        // Create panel and add caption literal
        Panel captionPanel = new Panel();
        captionPanel.ID = "pnlCaption" + identifier;
        captionPanel.EnableViewState = false;
        captionPanel.CssClass = "UniMenuDescription";
        captionPanel.Controls.Add(caption);
        return captionPanel;
    }


    /// <summary>
    /// Generates panel with content and caption 
    /// </summary>
    /// <param name="control">Control to add to the content</param>
    /// <param name="captionText">Caption of group</param>
    /// <returns></returns>
    protected Panel GetContent(Control control, string captionText)
    {
        // Create panel and set up
        Panel content = new Panel();
        content.ID = "pnlContent" + identifier;
        content.CssClass = "UniMenuContent";

        // Add inner content control
        if (control != null)
        {
            content.Controls.Add(control);
        }

        // Add caption
        content.Controls.Add(GetCaption(captionText));
        return content;
    }


    /// <summary>
    /// Generates error label
    /// </summary>
    /// <param name="message">Error message to display</param>
    /// <returns>Label with error message</returns>
    protected Label GetError(string message)
    {
        // If error occures skip this group
        Label errorLabel = new Label();
        errorLabel.ID = "lblError" + identifier;
        errorLabel.EnableViewState = false;
        errorLabel.Text = message;
        errorLabel.CssClass = "ErrorLabel";
        return errorLabel;
    }

    #endregion
}
