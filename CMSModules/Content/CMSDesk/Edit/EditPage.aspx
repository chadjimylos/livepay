<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditPage.aspx.cs" Inherits="CMSModules_Content_CMSDesk_Edit_EditPage"
    Theme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Edit page</title>
    <style type="text/css">
        body, html
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
            overflow: hidden;
        }
        .FCKToolbar
        {
            border-bottom: solid 1px #eeeeee;
            line-height: 0px;
        }
    </style>
</head>
<body class="<%=mBodyClass%>" onload="setInterval('if (window.ResizeFrame) { ResizeFrame(); }', 500);"
    onbeforeunload="if (window.ConfirmClose) { return ConfirmClose(event); }">
    <form id="form1" runat="server">
    <div id="FCKToolbar" class="FCKToolbar">
    </div>

    <script type="text/javascript" src="editpage.js"></script>

    <iframe width="100%" height="<%=height %>" id="pageview" name="pageview" scrolling="auto"
        frameborder="0" enableviewstate="false" src="<%=viewpage%>" onload="<%=loadScript %>"
        style="position: absolute; z-index: 9998;" />
    </form>
</body>
</html>
