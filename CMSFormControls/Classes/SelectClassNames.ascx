<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectClassNames.ascx.cs"
    Inherits="CMSFormControls_Classes_SelectClassNames" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" AllowEditTextBox="true" DisplayNameFormat="{%ClassDisplayName%} ({%ClassName%})"
            WhereCondition="(ClassIsDocumentType = 1)" ReturnColumnName="ClassName" ObjectType="cms.documenttype"
            ResourcePrefix="allowedclasscontrol" SelectionMode="MultipleTextBox" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
