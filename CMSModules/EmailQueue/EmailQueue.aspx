<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailQueue.aspx.cs" Inherits="CMSModules_EmailQueue_EmailQueue"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Viewers/ViewDate.ascx" TagName="ViewDate" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <div class="emailqueuetext">
        <cms:LocalizedLabel ID="lblText" runat="server" EnableViewState="false" ResourceString="EmailQueue.Queue.Text" /><br />
        <cms:LocalizedLabel runat="server" ID="lblDisabled" EnableViewState="false" Visible="false"
            ResourceString="EmailQueue.EmailsDisabled" CssClass="FieldLabel" />
    </div>
    <br />
    <asp:PlaceHolder ID="plcFilter" runat="server" Visible="false">
        <table>
            <tr runat="server">
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblFrom" runat="server" EnableViewState="false" DisplayColon="true"
                        ResourceString="general.from" />
                </td>
                <td>
                    <asp:DropDownList ID="drpFrom" runat="server" CssClass="ExtraSmallDropDown" />
                    <asp:TextBox ID="txtFrom" runat="server" CssClass="SelectorTextBox" />
                </td>
            </tr>
            <tr runat="server">
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblTo" runat="server" EnableViewState="false" DisplayColon="true"
                        ResourceString="general.toemail" />
                </td>
                <td>
                    <asp:DropDownList ID="drpTo" runat="server" CssClass="ExtraSmallDropDown" />
                    <asp:TextBox ID="txtTo" runat="server" CssClass="SelectorTextBox" />
                </td>
            </tr>
            <tr runat="server">
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblSubject" runat="server" EnableViewState="false" DisplayColon="true"
                        ResourceString="general.subject" />
                </td>
                <td>
                    <asp:DropDownList ID="drpSubject" runat="server" CssClass="ExtraSmallDropDown" />
                    <asp:TextBox ID="txtSubject" runat="server" CssClass="SelectorTextBox" />
                </td>
            </tr>
            <tr runat="server">
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblBody" runat="server" EnableViewState="false" DisplayColon="true"
                        ResourceString="general.body" />
                </td>
                <td>
                    <asp:DropDownList ID="drpBody" runat="server" CssClass="ExtraSmallDropDown" />
                    <asp:TextBox ID="txtBody" runat="server" CssClass="SelectorTextBox" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblPriority" runat="server" EnableViewState="false" DisplayColon="true"
                        ResourceString="emailqueue.priority" />
                </td>
                <td>
                    <asp:DropDownList ID="drpPriority" runat="server" CssClass="DropDownFieldFilter" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblStatus" runat="server" EnableViewState="false" DisplayColon="true"
                        ResourceString="emailqueue.status" />
                </td>
                <td>
                    <asp:DropDownList ID="drpStatus" runat="server" CssClass="DropDownFieldFilter" />
                </td>
            </tr>
            <tr runat="server">
                <td class="FieldLabel">
                    <cms:LocalizedLabel ID="lblErrorMessage" runat="server" EnableViewState="false" DisplayColon="true"
                        ResourceString="emailqueue.lastsendresult" />
                </td>
                <td>
                    <asp:DropDownList ID="drpError" runat="server" CssClass="ExtraSmallDropDown" />
                    <asp:TextBox ID="txtErrorMessage" runat="server" CssClass="SelectorTextBox" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <cms:CMSButton ID="btnFilter" runat="server" OnClick="btnFilter_Clicked" CssClass="ContentButton"
                        EnableViewState="false" />
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>
    <asp:Image runat="server" ID="imgShowFilter" CssClass="NewItemImage" />
    <cms:LocalizedLinkButton ID="btnShowFilter" runat="server" /><br />
    <br />
    <br />
    <cms:UniGrid ID="gridElem" runat="server" GridName="EmailQueue.xml" OrderBy="EmailPriority DESC, EmailID"
        Columns="EmailID, EmailSubject, EmailTo, EmailPriority, EmailLastSendResult, EmailLastSendAttempt, EmailStatus, EmailIsMass"
        IsLiveSite="false" />
</asp:Content>
