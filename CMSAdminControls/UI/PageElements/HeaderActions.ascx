<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderActions.ascx.cs"
    Inherits="CMSAdminControls_UI_PageElements_HeaderActions" %>

<table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
    <tr>
        <td style="width: 100%;">
            <asp:Panel ID="pnlActions" runat="server" Visible="true" EnableViewState="false"
                CssClass="Actions">
            </asp:Panel>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
