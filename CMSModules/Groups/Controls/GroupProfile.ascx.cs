using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Community;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Groups_Controls_GroupProfile : CMSAdminEditControl
{
    #region "Variables"

    private int mGroupId = 0;
    private bool mShowGeneralTab = true;
    private bool mShowMembersTab = true;
    private bool mShowRolesTab = true;
    private bool mShowPollsTab = true;
    private bool mShowContentTab = true;
    private bool mShowMediaTab = true;
    private bool mHideWhenGroupIsNotSupplied = false;
    private GroupInfo gi = null;
    private CMSAdminControl ctrl = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Determines whether to hide the content of the control when GroupID is not supplied.
    /// </summary>
    public bool HideWhenGroupIsNotSupplied
    {
        get
        {
            return this.mHideWhenGroupIsNotSupplied;
        }
        set
        {
            this.mHideWhenGroupIsNotSupplied = value;
        }
    }


    /// <summary>
    /// Gets or sets the ID of the group to be edited.
    /// </summary>
    public int GroupID
    {
        get
        {
            return this.mGroupId;
        }
        set
        {
            this.mGroupId = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to show the general tab.
    /// </summary>
    public bool ShowGeneralTab
    {
        get
        {
            return this.mShowGeneralTab;
        }
        set
        {
            this.mShowGeneralTab = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to show the members tab.
    /// </summary>
    public bool ShowMembersTab
    {
        get
        {
            return this.mShowMembersTab;
        }
        set
        {
            this.mShowMembersTab = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to show the roles tab.
    /// </summary>
    public bool ShowRolesTab
    {
        get
        {
            return this.mShowRolesTab;
        }
        set
        {
            this.mShowRolesTab = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to show the polls tab.
    /// </summary>
    public bool ShowPollsTab
    {
        get
        {
            return this.mShowPollsTab;
        }
        set
        {
            this.mShowPollsTab = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to show the content tab.
    /// </summary>
    public bool ShowContentTab
    {
        get
        {
            return this.mShowContentTab;
        }
        set
        {
            this.mShowContentTab = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to show the media tab.
    /// </summary>
    public bool ShowMediaTab
    {
        get
        {
            return this.mShowMediaTab;
        }
        set
        {
            this.mShowMediaTab = value;
        }
    }


    /// <summary>
    /// Gets or sets switch to display appropriate controls
    /// </summary>
    public string SelectedPage
    {
        get
        {
            return ValidationHelper.GetString(this.ViewState[this.ClientID + "SelectedPage"], "list");
        }
        set
        {
            ViewState[this.ClientID + "SelectedPage"] = (object)value;
        }
    }


    #endregion


    protected override void CreateChildControls()
    {
        base.CreateChildControls();

        // Get page url
        string page = QueryHelper.GetText("tab", this.SelectedPage);

        if (RaiseOnCheckPermissions(CMSAdminControl.PERMISSION_MANAGE, this))
        {
            if (this.StopProcessing)
            {
                return;
            }
        }

        if ((this.GroupID == 0) && this.HideWhenGroupIsNotSupplied)
        {
            this.Visible = false;
            return;
        }

        gi = GroupInfoProvider.GetGroupInfo(this.GroupID);

        // If no group, display the info and return
        if (gi == null)
        {
            this.lblInfo.Text = ResHelper.GetString("group.groupprofile.nogroup");
            this.lblInfo.Visible = true;
            this.tabMenu.Visible = false;
            this.pnlContent.Visible = false;
            return;
        }

        // Get current URL
        string absoluteUri = UrlHelper.CurrentURL;

        // Menu initialization
        tabMenu.TabControlIdPrefix = "GroupProfile";
        tabMenu.UrlTarget = "_self";
        tabMenu.Tabs = new string[8, 5];

        tabMenu.UsePostback = true;

        tabMenu.Tabs[0, 0] = ResHelper.GetString("General.General");
        tabMenu.Tabs[0, 2] = HTMLHelper.HTMLEncode(UrlHelper.AddParameterToUrl(absoluteUri, "tab", "general"));

        tabMenu.Tabs[1, 0] = ResHelper.GetString("General.Security");
        tabMenu.Tabs[1, 2] = HTMLHelper.HTMLEncode(UrlHelper.AddParameterToUrl(absoluteUri, "tab", "security"));

        tabMenu.Tabs[2, 0] = ResHelper.GetString("Group.Members");
        tabMenu.Tabs[2, 2] = HTMLHelper.HTMLEncode(UrlHelper.AddParameterToUrl(absoluteUri, "tab", "members"));

        tabMenu.Tabs[3, 0] = ResHelper.GetString("general.roles");
        tabMenu.Tabs[3, 2] = HTMLHelper.HTMLEncode(UrlHelper.AddParameterToUrl(absoluteUri, "tab", "roles"));

        tabMenu.Tabs[4, 0] = ResHelper.GetString("Group.Forums");
        tabMenu.Tabs[4, 2] = HTMLHelper.HTMLEncode(UrlHelper.AddParameterToUrl(absoluteUri, "tab", "forums"));

        tabMenu.Tabs[5, 0] = ResHelper.GetString("Group.MediaLibrary");
        tabMenu.Tabs[5, 2] = HTMLHelper.HTMLEncode(UrlHelper.AddParameterToUrl(absoluteUri, "tab", "medialibrary"));

        tabMenu.Tabs[6, 0] = ResHelper.GetString("Group.MessageBoards");
        tabMenu.Tabs[6, 2] = HTMLHelper.HTMLEncode(UrlHelper.AddParameterToUrl(absoluteUri, "tab", "messageboards"));

        tabMenu.Tabs[7, 0] = ResHelper.GetString("Group.Polls");
        tabMenu.Tabs[7, 2] = HTMLHelper.HTMLEncode(UrlHelper.AddParameterToUrl(absoluteUri, "tab", "polls"));

        // Select current page
        switch (page.ToLower())
        {
            default:
            case "general":
                tabMenu.SelectedTab = 0;

                ctrl = LoadControl("~/CMSModules/Groups/Controls/GroupEdit.ascx") as CMSAdminControl;
                ctrl.ID = "groupEditElem";

                if (ctrl != null)
                {
                    ctrl.SetValue("GroupID", gi.GroupID);
                    ctrl.SetValue("SiteID", CMSContext.CurrentSiteID);
                    ctrl.SetValue("IsLiveSite", this.IsLiveSite);
                    ctrl.OnCheckPermissions += new CheckPermissionsEventHandler(ctrl_OnCheckPermissions);
                    pnlContent.Controls.Add(ctrl);
                }
                break;

            case "security":
                tabMenu.SelectedTab = 1;

                ctrl = LoadControl("~/CMSModules/Groups/Controls/Security/GroupSecurity.ascx") as CMSAdminControl;
                ctrl.ID = "securityElem";

                if (ctrl != null)
                {
                    ctrl.SetValue("GroupID", gi.GroupID);
                    ctrl.SetValue("IsLiveSite", this.IsLiveSite);
                    ctrl.OnCheckPermissions += new CheckPermissionsEventHandler(ctrl_OnCheckPermissions);
                    pnlContent.Controls.Add(ctrl);
                }
                break;

            case "members":
                tabMenu.SelectedTab = 2;

                ctrl = LoadControl("~/CMSModules/Groups/Controls/Members/Members.ascx") as CMSAdminControl;
                ctrl.ID = "securityElem";

                if (ctrl != null)
                {
                    ctrl.SetValue("GroupID", gi.GroupID);
                    ctrl.SetValue("IsLiveSite", this.IsLiveSite);
                    ctrl.OnCheckPermissions += new CheckPermissionsEventHandler(ctrl_OnCheckPermissions);
                    pnlContent.Controls.Add(ctrl);
                }
                break;

            case "forums":
                tabMenu.SelectedTab = 4;
                ctrl = LoadControl("~/CMSModules/Forums/Controls/LiveControls/Groups.ascx") as CMSAdminControl;
                ctrl.ID = "forumElem";

                if (ctrl != null)
                {
                    ctrl.SetValue("GroupID", gi.GroupID);
                    ctrl.SetValue("CommunityGroupGUID", gi.GroupGUID);
                    ctrl.SetValue("IsLiveSite", this.IsLiveSite);
                    ctrl.DisplayMode = ControlDisplayModeEnum.Simple;
                    ctrl.OnCheckPermissions += new CheckPermissionsEventHandler(ctrl_OnCheckPermissions);

                    pnlContent.Controls.Add(ctrl);
                }
                break;

            case "roles":
                tabMenu.SelectedTab = 3;

                ctrl = LoadControl("~/CMSSiteManager/Administration/Roles/Controls/Roles.ascx") as CMSAdminControl;
                ctrl.ID = "rolesElem";

                if (ctrl != null)
                {
                    ctrl.SetValue("GroupID", gi.GroupID);
                    ctrl.SetValue("GroupGUID", gi.GroupGUID);
                    ctrl.SetValue("SiteID", CMSContext.CurrentSiteID);
                    ctrl.SetValue("IsLiveSite", this.IsLiveSite);
                    ctrl.DisplayMode = ControlDisplayModeEnum.Simple;
                    ctrl.OnCheckPermissions += new CheckPermissionsEventHandler(ctrl_OnCheckPermissions);

                    pnlContent.Controls.Add(ctrl);
                }
                break;

            case "polls":
                tabMenu.SelectedTab = 7;
                ctrl = LoadControl("~/CMSModules/Polls/Controls/Polls.ascx") as CMSAdminControl;
                ctrl.ID = "pollsElem";

                if (ctrl != null)
                {
                    ctrl.SetValue("GroupID", gi.GroupID);
                    ctrl.SetValue("GroupGUID", gi.GroupGUID);
                    ctrl.SetValue("SiteID", CMSContext.CurrentSiteID);
                    ctrl.SetValue("IsLiveSite", this.IsLiveSite);
                    ctrl.DisplayMode = ControlDisplayModeEnum.Simple;
                    ctrl.OnCheckPermissions += new CheckPermissionsEventHandler(ctrl_OnCheckPermissions);
                    pnlContent.Controls.Add(ctrl);
                }
                break;

            case "messageboards":
                tabMenu.SelectedTab = 6;

                ctrl = LoadControl("~/CMSModules/MessageBoards/Controls/LiveControls/Boards.ascx") as CMSAdminControl;
                ctrl.ID = "boardElem";

                if (ctrl != null)
                {
                    ctrl.SetValue("GroupID", gi.GroupID);
                    ctrl.SetValue("IsLiveSite", this.IsLiveSite);
                    ctrl.DisplayMode = ControlDisplayModeEnum.Simple;
                    ctrl.OnCheckPermissions += new CheckPermissionsEventHandler(ctrl_OnCheckPermissions);

                    pnlContent.Controls.Add(ctrl);
                }
                break;

            case "medialibrary":
                tabMenu.SelectedTab = 5;

                ctrl = LoadControl("~/CMSModules/MediaLibrary/Controls/LiveControls/MediaLibraries.ascx") as CMSAdminControl;
                ctrl.ID = "libraryElem";

                if (ctrl != null)
                {
                    ctrl.SetValue("GroupGUID", gi.GroupID);
                    ctrl.SetValue("GroupID", gi.GroupID);
                    ctrl.SetValue("IsLiveSite", this.IsLiveSite);
                    ctrl.DisplayMode = ControlDisplayModeEnum.Simple;
                    ctrl.OnCheckPermissions += new CheckPermissionsEventHandler(ctrl_OnCheckPermissions);

                    pnlContent.Controls.Add(ctrl);
                }
                break;
        }

        if (!RequestHelper.IsPostBack())
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads control.
    /// </summary>
    public override void ReloadData()
    {
        if (ctrl != null)
        {
            ctrl.ReloadData();
        }
    }


    #region "Security handlers"

    /// <summary>
    /// Control - Check permission event handler
    /// </summary>
    /// <param name="permissionType"></param>
    void ctrl_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        if (!RaiseOnCheckPermissions(permissionType, sender))
        {
            // Check if user is allowed to create or modify the module records
            if ((!CMSContext.CurrentUser.IsGroupAdministrator(this.GroupID)) && (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Groups", CMSAdminControl.PERMISSION_MANAGE)))
            {
                AccessDenied("CMS.Groups", CMSAdminControl.PERMISSION_MANAGE);
            }

        }
    }

    #endregion
}
