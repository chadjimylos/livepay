<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Polls_Edit_General.aspx.cs" Inherits="CMSModules_Groups_Tools_Polls_Polls_Edit_General" 
Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Groups polls edit - general" %>

<%@ Register Src="~/CMSModules/Polls/Controls/PollProperties.ascx" TagName="PollProperties" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:PollProperties ID="PollProperties" runat="server"  />
</asp:Content>
