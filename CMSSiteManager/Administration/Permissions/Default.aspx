<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="CMSSiteManager_Administration_Permissions_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Administration - Permissions</title>
</head>
		<frameset border="0" rows="<%= headerSize %>, *">
    		<frame name="header" src="<%= headerPage %>" scrolling="no" frameborder="0" />		
		    <frame name="content" src="../../blank.htm" scrolling="auto" frameborder="0" noresize="noresize" />
		    <noframes>
    			<body>
			    <p id="p1">
		    		This HTML frameset displays multiple Web pages. To view this frameset, use a 
	    			Web browser that supports HTML 4.0 and later.
    			 </p>
			    </body>
		    </noframes>
    	</frameset>
</html>
