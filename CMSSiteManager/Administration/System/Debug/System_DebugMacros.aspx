<%@ Page Language="C#" AutoEventWireup="true" CodeFile="System_DebugMacros.aspx.cs"
    Inherits="CMSSiteManager_Administration_System_Debug_System_DebugMacros" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="System - Security" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />
    <div style="text-align: right; padding: 5px;">
        <cms:CMSButton runat="server" ID="btnClear" OnClick="btnClear_Click" CssClass="LongButton" EnableViewState="false" />
    </div>
    <asp:PlaceHolder runat="server" ID="plcLogs" />
</asp:Content>
