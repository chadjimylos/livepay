Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports LivePay_ESBBridge
Imports CMS.SettingsProvider
Imports CMS.SiteProvider
Imports CMS.DataEngine
Imports System.Collections.Generic

Partial Public Class CMSTemplates_LivePay_UCSearchHistPay
    Inherits CMSUserControl
#Region "Variables"

    ''' <summary>
    ''' My property
    ''' </summary>
    Private mMyProperty As String = Nothing

#End Region

#Region "Properties"


    ''' <summary>
    ''' Gets or sets value of MyProperty
    ''' </summary>
    Public Property MyProperty() As String
        Get
            Return mMyProperty
        End Get
        Set(ByVal value As String)
            mMyProperty = value
        End Set
    End Property

    Private TimeoutLivePayBridge As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutLivePayBridge"))

#End Region


#Region "Methods"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        btnSearchPayments.ImageUrl = ResHelper.LocalizeString("{$=/app_themes/LivePay/btnSearch.png|en-us=/app_themes/LivePay/btnSearch_en-us.png$}")
        'If Page.IsPostBack AndAlso Request.UrlReferrer IsNot Nothing AndAlso Not Request.UrlReferrer.ToString.ToLower.Contains("searchhistpayments") Then
        '    Response.Redirect("searchhistpayments")
        'End If
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then

            ReloadData()
        End If
        If Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            SetDatePickers()
        End If
        If Not Page.IsPostBack Then
            txtFrom.Text = Format(Now, "dd/MM/yyyy")
            txtTo.Text = Format(Now, "dd/MM/yyyy")
        End If
        If Not Page.IsPostBack Then
            rbtDate.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=������|en-us=Today$}"), 1))
            rbtDate.Items(0).Selected = True
            rbtDate.Items.Insert(1, New ListItem(ResHelper.LocalizeString("{$=1 ��������|en-us=1 week$}"), 2))
            rbtDate.Items.Insert(2, New ListItem(ResHelper.LocalizeString("{$=1 �����|en-us=1 month$}"), 3))
            rbtDate.Items.Insert(3, New ListItem(ResHelper.LocalizeString("{$=2 �����|en-us=2 months$}"), 4))
            rbtDate.Items.Insert(4, New ListItem(ResHelper.LocalizeString("{$=6 �����|en-us=6 months$}"), 5))
        End If


    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Public Overloads Sub ReloadData()

        Dim Today As String = String.Concat(Now.Day, "/", Now.Month, "/", Now.Year)
        Dim DateToAdd As Date = DateAdd(DateInterval.Weekday, -7, Now)
        Dim OneWeek As String = String.Concat(DateToAdd.Day, "/", DateToAdd.Month, "/", DateToAdd.Year)
        DateToAdd = DateAdd(DateInterval.Month, -1, Now)
        Dim OneMonth As String = String.Concat(DateToAdd.Day, "/", DateToAdd.Month, "/", DateToAdd.Year)
        DateToAdd = DateAdd(DateInterval.Month, -2, Now)
        Dim TowMonth As String = String.Concat(DateToAdd.Day, "/", DateToAdd.Month, "/", DateToAdd.Year)

        DateToAdd = DateAdd(DateInterval.Month, -6, Now)
        Dim SixMonth As String = String.Concat(DateToAdd.Day, "/", DateToAdd.Month, "/", DateToAdd.Year)
        rbtDate.Attributes.Add("onclick", String.Concat("SetRblDates(this,'", Today, "','", OneWeek, "','", OneMonth, "','", TowMonth, "','", SixMonth, "')"))

        drpTxnResult.Items.Clear()
        drpTxnResult.Items.Add(New ListItem(ResHelper.LocalizeString("{$=���|en-us=All$}"), -1))

        Dim ResultType As TxnResultEnum
        For Each ResultType In [Enum].GetValues(GetType(TxnResultEnum))
            If ResultType = TxnResultEnum.Successful_Uploaded OrElse ResultType = TxnResultEnum.Voided_Uploaded Then
            Else
                Dim strMsgType As String = ResultType.ToString()
                drpTxnResult.Items.Add(New ListItem(GetTransactionStatusDescr(ResultType), Integer.Parse(ResultType)))
            End If

        Next
        GetUserCards()

        'Bind()
        SetDatePickers()
    End Sub

    Private Sub SetDatePickers()

        Dim DateNamesSmall As String = ResHelper.LocalizeString("{$='��', '��', '��', '��', '��', '��', '��'|en-us='Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'$}")
        Dim DateNames As String = ResHelper.LocalizeString("{$='�������', '�������', '�����', '�������', '������', '���������', '�������'|en-us='Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'$}")
        Dim MonthNames As String = ResHelper.LocalizeString("{$='����������','�����������','�������','��������','�����','�������','�������','���������','�����������','���������','���������','����������'|en-us='Januar','Februar','Marts','April','Maj','Juni','Juli','August','September','Oktober','November','December'$}")
        JsScript(String.Concat(" $(""[id$=txtFrom]"").datepicker({ monthNames: [", MonthNames, "], dayNames: [", DateNames, "], dayNamesMin: [", DateNamesSmall, "], dateFormat: 'dd/mm/yy' }); $(""[id$=txtTo]"").datepicker({ monthNames: [", MonthNames, "], dayNames: [", DateNames, "], dayNamesMin: [", DateNamesSmall, "],dateFormat: 'dd/mm/yy' }); ShowHideSrcForm(true); "))
    End Sub

    Function GetTransactionStatusDescr(ByVal status As LivePay_ESBBridge.TxnResultEnum) As String
        'Failed  1, Reversal 2, Successful 0, Successful_Uploaded 4, Voided 3, Voided_Uploaded 5
        Select Case status
            Case 0 : Return ResHelper.LocalizeString("{$=�����������|en-us=Successful$}")
            Case 1 : Return ResHelper.LocalizeString("{$=�����������|en-us=Failed$}")
            Case 2 : Return ResHelper.LocalizeString("{$=������������|en-us=Refund$}")
            Case 3 : Return ResHelper.LocalizeString("{$=���������|en-us=Cancelled$}")
            Case 4 : Return ResHelper.LocalizeString("{$=������������ ���������|en-us=Complete Transaction$}")
            Case 5 : Return ResHelper.LocalizeString("{$=��������� ���������|en-us=Transaction Cancelled$}")
        End Select
        Return status
    End Function

    Private Sub GetUserCards()
        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = DBConnection.GetUserCardsForSearch(UserID)
        Dim dtFinal As DataTable = dt
        dtFinal.Columns.Add("Text", GetType(String), "FriendlyName + ' - xxxxxxxx-xxxx-' + LastFor")
        dtFinal.Columns.Add("Value", GetType(String), "CardID")
        drpSavedCards.DataTextField = "Text"
        drpSavedCards.DataValueField = "Value"
        drpSavedCards.DataSource = dtFinal
        drpSavedCards.DataBind()
        drpSavedCards.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=����|en-us=All$}"), 0))

    End Sub

    Private Sub Bind()

        divHeader.Visible = True
        Dim w As New LivePay_ESBBridge.Bridge
        w.Timeout = TimeoutLivePayBridge
        Dim obj As New GetTransactionsObj


        'If txtFrom.Text.Length > 0 Then obj.BatchDateFrom = New Date(txtFrom.Text.Split("/")(2), txtFrom.Text.Split("/")(1), txtFrom.Text.Split("/")(0), 0, 0, 0, 0, DateTimeKind.Local)
        'If txtTo.Text.Length > 0 Then obj.BatchDateTo = New Date(txtTo.Text.Split("/")(2), txtTo.Text.Split("/")(1), txtTo.Text.Split("/")(0), 0, 0, 0, 0, DateTimeKind.Local)
        If txtFromPrice.Text.Length > 0 Then
            obj.AmountFrom = StripHTMLFunctions.StripTags(txtFromPrice.Text.Replace(".", ","))
        Else
            obj.AmountFrom = 0
        End If
        If txtToPrice.Text.Length > 0 Then
            obj.amountTo = StripHTMLFunctions.StripTags(txtToPrice.Text.Replace(".", ","))
        Else
            obj.amountTo = 100000000
        End If

        If txtFrom.Text.Length > 0 Then obj.TransactionDateFrom = New Date(txtFrom.Text.Split("/")(2), txtFrom.Text.Split("/")(1), txtFrom.Text.Split("/")(0), 0, 0, 0, 0, DateTimeKind.Local)
        Dim DateTo As DateTime = New Date(txtTo.Text.Split("/")(2), txtTo.Text.Split("/")(1), txtTo.Text.Split("/")(0), 23, 59, 59, 0, DateTimeKind.Local)
        If DateTo.ToString("yyyyMMdd") = DateTime.Now.ToString("yyyyMMdd") Then
            DateTo = DateTo.AddDays(5)
        End If


        If txtTo.Text.Length > 0 Then obj.TransactionDateTo = DateTo

        If drpTxnResult.SelectedValue > -1 Then
            Dim TR?(0) As LivePay_ESBBridge.TxnResultEnum
            TR(0) = drpTxnResult.SelectedValue
            obj.TransactionResult = TR
            obj.TransactionResultSpecified1 = True
        End If
        If drpSavedCards.SelectedValue <> "0" Then
            'lblError.Visible = True
            'lblError.InnerText = drpSavedCards.SelectedValue

            'GET THE USER CARD NUMBER
            Dim dt As DataTable = DBConnection.GetUserCardsForSearch(CMSContext.CurrentUser.UserID)
            Dim dr() As DataRow = dt.Select(String.Format("CardID='{0}'", drpSavedCards.SelectedValue))
            If dr.Length > 0 Then
                obj.CardNumber = Left(dr(0)("CardNumber"), 4) & CryptoFunctions.DecryptString(Right(dr(0)("CardNumber"), (dr(0)("CardNumber").ToString.Length - 4)))
            End If
        End If

        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        obj.CustomerId = UserID
        If HiddentxtMerchantID.Text <> String.Empty Then
            obj.MerchantId = Replace(StripHTMLFunctions.StripTags(HiddentxtMerchantID.Text), " ", "")
        End If

        Dim currentPage As Integer = 1
        If HiddenPage.Text.Length > 0 Then currentPage = StripHTMLFunctions.StripTags(HiddenPage.Text)

        obj.PageNumber = currentPage - 1

        Dim pageSize As Integer = drpTransCount.SelectedValue 'ddlRecordsPerPage.SelectedValue
        obj.PageSize = pageSize
        GridViewPayments.PageSize = pageSize


        Dim resp As GetTransactionsResponse
        Try
            resp = w.GetTransactions(obj)

            'Response.Write(resp.ErrorMessage)
            'Response.End()
        Catch ex As Exception
            'Response.Write(ex.Message)
            lblError2.Text = "ESB Error: " & ex.Message
            'lblError.Text = "ESB Error: " & ex.ToString
            Exit Sub
        End Try

        Session("GetSrcHisPayDataTable") = resp.Transactions



        If resp.ErrorMessage IsNot Nothing AndAlso resp.ErrorMessage.Length > 0 Then
            lblError2.Text = "ESB Error: " & resp.ErrorMessage.ToString
            'Response.Write("ESB Error: " & resp.ErrorMessage.ToString)
            'Response.End()
            Exit Sub
        End If
        GridViewPayments.Columns(0).HeaderText = ResHelper.LocalizeString("{$=������� ����������|en-us=Transaction Code$}")
        GridViewPayments.Columns(1).HeaderText = ResHelper.LocalizeString("{$=���������� ����������|en-us=Transaction Date$}")
        GridViewPayments.Columns(2).HeaderText = ResHelper.LocalizeString("{$=������� ����|en-us=Payment to$}")

        GridViewPayments.Columns(3).HeaderText = ResHelper.LocalizeString("{$=�����|en-us=Card$}")
        GridViewPayments.Columns(4).HeaderText = ResHelper.LocalizeString("{$=����|en-us=Amount$}") & "(&euro;)"
        GridViewPayments.Columns(5).HeaderText = ResHelper.LocalizeString("{$=����������|en-us=Result$}")
        GridViewPayments.Columns(6).HeaderText = ResHelper.LocalizeString("{$=�����|en-us=Type$}")
        GridViewPayments.Columns(7).HeaderText = ResHelper.LocalizeString("{$=������|en-us=Installments$}")

        GridViewPayments.DataSource = resp.Transactions
        GridViewPayments.DataBind()

        'PAGING
        Dim records As Integer = resp.TotalRecords

        Dim pagerRow As GridViewRow = GridViewPayments.BottomPagerRow
        If pagerRow IsNot Nothing Then pagerRow.Visible = True
        If records > pageSize Then
            Dim lastPage As Integer = Math.Floor(records \ pageSize)
            If records Mod pageSize > 0 Then lastPage += 1
            Dim totalpages As Integer = lastPage 'HOLD A VALUE FOR THE TOTAL RECORDS AVAILABLE

            Dim GetPageLinksIndex As HtmlGenericControl = DirectCast(pagerRow.Cells(0).FindControl("lblPageIndex"), HtmlGenericControl)

            Dim startPage As Integer = currentPage

            If totalpages <= 5 Then
                startPage = 1
                lastPage = totalpages
            Else
                If startPage > 1 Then startPage = currentPage - 1
                If lastPage > startPage + 4 Then lastPage = startPage + 4
            End If


            CreateGridPagerLinks(startPage, lastPage, currentPage, GetPageLinksIndex)


            Dim lnkPrev As LinkButton = pagerRow.Cells(0).FindControl("lnkPrev")
            Dim lnkNext As LinkButton = pagerRow.Cells(0).FindControl("lnkNext")

            If (currentPage > 1) Then
                lnkPrev.Style.Add("color", "#242424")
                lnkPrev.Enabled = True
            Else
                lnkPrev.Style.Add("color", "#adadad")
                lnkPrev.Enabled = False
            End If

            If (currentPage < lastPage) Then
                lnkNext.Style.Add("color", "#242424")
                lnkNext.Enabled = True
            Else
                lnkNext.Enabled = False
                lnkNext.Style.Add("color", "#adadad")
            End If

           
        ElseIf pagerRow IsNot Nothing Then

            pagerRow.Cells(0).FindControl("lnkPrev").Visible = False
            pagerRow.Cells(0).FindControl("lnkNext").Visible = False
        End If

        'Dim w As New LivePay_ESBBridge.Bridge
        'Dim obj As New GetTransactionsObj
        ''obj.TransactionDateFrom = IIf(IsDate(txtFrom), DateTime.MinValue, DateTime.MinValue)
        ''obj.TransactionDateTo = IIf(IsDate(txtTo), DateTime.MinValue, DateTime.MinValue)
        ''obj.AmountFrom = IIf(txtFromPrice.Text = "", -1, txtFromPrice.Text)
        ''obj.amountTo = IIf(txtToPrice.Text = "", -1, txtToPrice.Text)
        ''obj.TransactionResult = drpTxnResult.SelectedValue
        'obj.AmountFrom = 1
        'obj.amountTo = 100000
        'obj.MerchantId = "0003521077"
        'obj.CardNumber = "4015507213501736" 'drpSavedCards.SelectedValue

        'Dim UserID As Integer = CMSContext.CurrentUser.UserID
        '' obj.CustomerId = UserID
        'Dim resp As GetTransactionsResponse = w.GetTransactions(obj)

        'Session("GetSrcHisPayDataTable") = resp.Transactions
        'GridViewPayments.DataSource = resp.Transactions
        'GridViewPayments.DataBind()
    End Sub

    Sub GridViewPayments_DataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridViewPayments.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim amount As String = CType(e.Row.FindControl("hidAmount"), HiddenField).Value.ToString
            If amount.Split(",").Length > 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
            ElseIf amount.Split(",").Length = 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & ",00"
            Else
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount
            End If

            'lbltransactionStatus.Text = GetTransactionStatusDescr(lbltransactionStatus.Text)
        End If
    End Sub

    Protected Sub btnSearchPayments_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchPayments.Click
        GridViewPayments.PageIndex = 0
        HiddenPage.Text = 1
        Bind()
    End Sub

#Region " Paging "

    Protected Sub GvNews_PageIndexChanged(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles GridViewPayments.PageIndexChanging
        GridViewPayments.PageIndex = e.NewPageIndex
        Bind()
    End Sub

    Protected Sub GvNews_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridViewPayments.DataBound
        'Dim pagerRow As GridViewRow = GridViewPayments.BottomPagerRow
        'If GridViewPayments.Rows.Count > 0 Then
        '    Dim GetPageNow As Integer = GridViewPayments.PageIndex + 1
        '    Dim GetPageCount As Integer = GridViewPayments.PageCount
        '    Dim GetPageLinksIndex As HtmlGenericControl = DirectCast(pagerRow.Cells(0).FindControl("lblPageIndex"), HtmlGenericControl)
        '    If pagerRow.Visible = True Then
        '        If GetPageCount <= 7 Then
        '            CreateGridPagerLinks(1, GridViewPayments.PageCount, GetPageNow, GetPageLinksIndex)
        '        Else
        '            Dim GetLeft As Integer = 1
        '            Dim GetRight As Integer = 7
        '            If GetPageNow > 4 Then
        '                GetLeft = GetPageNow - 3
        '                GetRight = GetPageNow + 3
        '                If GetRight > GetPageCount Then
        '                    GetLeft = GetLeft - (GetRight - GetPageCount)
        '                    If GetLeft < 1 Then
        '                        GetLeft = 1
        '                    End If
        '                    GetRight = GetPageCount
        '                End If
        '                CreateGridPagerLinks(GetLeft, GetRight, GetPageNow, GetPageLinksIndex)
        '            Else
        '                CreateGridPagerLinks(GetLeft, GetRight, GetPageNow, GetPageLinksIndex)
        '            End If
        '        End If
        '    End If
        '    pagerRow.Cells(0).FindControl("lnkPrev").Visible = (GridViewPayments.PageIndex > 0)
        '    pagerRow.Cells(0).FindControl("lnkNext").Visible = (GridViewPayments.PageCount - 1 > GridViewPayments.PageIndex)
        '    Dim btnSaveToPDF As ImageButton = DirectCast(pagerRow.Cells(0).FindControl("btnSaveToPDF"), ImageButton)
        '    Dim btnSaveToExcel As ImageButton = DirectCast(pagerRow.Cells(0).FindControl("btnSaveToExcel"), ImageButton)


        '    If pagerRow.Visible = False Then
        '        pagerRow.Visible = True
        '    End If
        'End If
    End Sub

    Private Sub CreateGridPagerLinks(ByVal StartNo As Integer, ByVal EndNo As Integer, ByVal GetPageNow As Integer, ByVal GetPageLinksIndex As HtmlGenericControl)
        For i As Integer = StartNo To EndNo
            Dim NewDiv As New HtmlGenericControl("div")
            Dim NewLink As New LinkButton
            NewLink.ID = "PagerLink_" & i
            NewLink.OnClientClick = String.Concat("GoToPage('", i, "');return false")
            If i = GetPageNow Then
                NewDiv.Attributes.Add("class", "GridPagerNoSel")
                NewLink.CssClass = "GridPagerNolnkSel"
            Else
                NewDiv.Attributes.Add("class", "GridPagerNo")
                NewLink.CssClass = "GridPagerNolnk"
                NewLink.Style.Add("color", "#5a5a5a")
            End If
            NewLink.Text = i
            NewDiv.Controls.Add(NewLink)
            GetPageLinksIndex.Controls.Add(NewDiv)
        Next
    End Sub

    Sub ChangePageByLinkNumber_Before(ByVal sender As Object, ByVal e As EventArgs)
        HiddenPage.Text = HiddenPage.Text - 1
        Bind()
        'If GridViewPayments.PageIndex > 0 Then
        '    Dim pageList As LinkButton = CType(sender, LinkButton)
        '    GridViewPayments.PageIndex = GridViewPayments.PageIndex - 1
        '    GridViewPayments.DataSource = Session("GetSrcHisPayDataTable")
        '    GridViewPayments.DataBind()
        'End If
    End Sub

    Sub ChangePageByLinkNumber_Next(ByVal sender As Object, ByVal e As EventArgs)
        HiddenPage.Text = HiddenPage.Text + 1
        Bind()
        'Dim pageList As LinkButton = CType(sender, LinkButton)
        'GridViewPayments.PageIndex = GridViewPayments.PageIndex + 1
        'GridViewPayments.DataSource = Session("GetSrcHisPayDataTable")
        'GridViewPayments.DataBind()
    End Sub

    Protected Sub HiddenPageBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HiddenPageBtn.Click
        'GridViewPayments.PageIndex = CInt(HiddenPage.Text) - 1
        'GridViewPayments.DataSource = Session("GetSrcHisPayDataTable")
        'GridViewPayments.DataBind()
        Bind()
    End Sub
#End Region

#End Region

#Region " Export Methods "


    Private Function Get_TheObject() As GetTransactionsObj
        Dim w As New LivePay_ESBBridge.Bridge
        w.Timeout = TimeoutLivePayBridge
        Dim obj As New GetTransactionsObj

        If txtFromPrice.Text.Length > 0 Then
            obj.AmountFrom = StripHTMLFunctions.StripTags(txtFromPrice.Text.Replace(".", ","))
        Else
            obj.AmountFrom = 0
        End If
        If txtToPrice.Text.Length > 0 Then
            obj.amountTo = StripHTMLFunctions.StripTags(txtToPrice.Text.Replace(".", ","))
        Else
            obj.amountTo = 100000000
        End If

        If txtFrom.Text.Length > 0 Then obj.TransactionDateFrom = New Date(txtFrom.Text.Split("/")(2), txtFrom.Text.Split("/")(1), txtFrom.Text.Split("/")(0), 0, 0, 0, 0, DateTimeKind.Local)
        Dim DateTo As DateTime = New Date(txtTo.Text.Split("/")(2), txtTo.Text.Split("/")(1), txtTo.Text.Split("/")(0), 23, 59, 59, 0, DateTimeKind.Local)
        If DateTo.ToString("yyyyMMdd") = DateTime.Now.ToString("yyyyMMdd") Then
            DateTo = DateTo.AddDays(5)
        End If

        If txtTo.Text.Length > 0 Then obj.TransactionDateTo = DateTo

        If drpTxnResult.SelectedValue > -1 Then
            Dim TR?(0) As LivePay_ESBBridge.TxnResultEnum
            TR(0) = drpTxnResult.SelectedValue
            obj.TransactionResult = TR
            obj.TransactionResultSpecified1 = True
        End If
        If drpSavedCards.SelectedValue <> "0" Then
            'lblError.Visible = True
            'lblError.InnerText = drpSavedCards.SelectedValue

            'GET THE USER CARD NUMBER
            Dim dt As DataTable = DBConnection.GetUserCardsForSearch(CMSContext.CurrentUser.UserID)
            Dim dr() As DataRow = dt.Select(String.Format("CardID='{0}'", drpSavedCards.SelectedValue))
            If dr.Length > 0 Then
                obj.CardNumber = Left(dr(0)("CardNumber"), 4) & CryptoFunctions.DecryptString(Right(dr(0)("CardNumber"), (dr(0)("CardNumber").ToString.Length - 4)))
            End If
        End If

        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        obj.CustomerId = UserID
        If HiddentxtMerchantID.Text <> String.Empty Then
            obj.MerchantId = Replace(StripHTMLFunctions.StripTags(HiddentxtMerchantID.Text), " ", "")
        End If

        Return obj
    End Function

    Protected Sub btnSaveToPDFClick(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Bind()
        Dim w As New LivePay_ESBBridge.Bridge
        w.Timeout = TimeoutLivePayBridge
        Dim obj As GetTransactionsObj = Get_TheObject()
        obj.PageNumber = 0
        obj.PageSize = 200
        Dim resp As GetTransactionsResponse
        Try
            resp = w.GetTransactions(obj)
        Catch ex As Exception
            tt.Text = ex.Message
            'lblError.Text = "ESB Error: " & ex.ToString
            Exit Sub
        End Try
        Dim TotalRecord As Integer = resp.TotalRecords
        If TotalRecord > 200 Then
            Dim TotalPages As String = TotalRecord / 200
            If TotalPages.Contains(",") Then
                TotalPages = Split(TotalPages, ",")(0) + 1
            End If

            Dim DtArrays As List(Of TransactionInfo) = New List(Of TransactionInfo)
            DtArrays.AddRange(resp.Transactions)

            For iPages As Integer = 2 To TotalPages
                obj.PageNumber = iPages - 1
                obj.PageSize = 200
                Try
                    resp = w.GetTransactions(obj)
                Catch ex As Exception
                    tt.Text = "ESB Error: " & ex.ToString
                    Exit Sub
                End Try
                If resp.ErrorMessage IsNot Nothing AndAlso resp.ErrorMessage.Length > 0 Then
                    tt.Text = "ESB Error: " & resp.ErrorMessage.ToString
                    Exit Sub
                End If
                DtArrays.AddRange(resp.Transactions)
            Next
            Session("GetSrcHisPayDataTable") = DtArrays
        Else
            Session("GetSrcHisPayDataTable") = resp.Transactions
        End If

        Response.Redirect("~/CMSTemplates/livepay/ExportToPDF.aspx?ExportType=SrcHisPay")
    End Sub
    Protected Sub btnSaveToExcelClick(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Bind()
        Dim w As New LivePay_ESBBridge.Bridge
        w.Timeout = TimeoutLivePayBridge
        Dim obj As GetTransactionsObj = Get_TheObject()
        obj.PageNumber = 0
        obj.PageSize = 200
        Dim resp As GetTransactionsResponse
        Try
            resp = w.GetTransactions(obj)
        Catch ex As Exception
            tt.Text = ex.Message
            'lblError.Text = "ESB Error: " & ex.ToString
            Exit Sub
        End Try
        Dim TotalRecord As Integer = resp.TotalRecords
        If TotalRecord > 200 Then
            Dim TotalPages As String = TotalRecord / 200
            If TotalPages.Contains(",") Then
                TotalPages = Split(TotalPages, ",")(0) + 1
            End If

            Dim DtArrays As List(Of TransactionInfo) = New List(Of TransactionInfo)
            DtArrays.AddRange(resp.Transactions)

            For iPages As Integer = 2 To TotalPages
                obj.PageNumber = iPages - 1
                obj.PageSize = 200
                Try
                    resp = w.GetTransactions(obj)
                Catch ex As Exception
                    tt.Text = "ESB Error: " & ex.ToString
                    Exit Sub
                End Try
                If resp.ErrorMessage IsNot Nothing AndAlso resp.ErrorMessage.Length > 0 Then
                    tt.Text = "ESB Error: " & resp.ErrorMessage.ToString
                    Exit Sub
                End If
                DtArrays.AddRange(resp.Transactions)
            Next
            Session("GetSrcHisPayDataTable") = DtArrays
        Else
            Session("GetSrcHisPayDataTable") = resp.Transactions
        End If


        Response.Redirect("~/CMSTemplates/livepay/ExportToExcel.aspx?ExportType=SrcHisPay")
    End Sub

#End Region

    Protected Sub openPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btn As LinkButton = DirectCast(sender, LinkButton)

        Receipt.TransID = btn.CommandArgument.Split("_")(0)
        Receipt.System = DirectCast(Convert.ToInt32(btn.CommandArgument.Split("_")(1).ToString), LivePay_ESBBridge.SystemEnum)

        Receipt.ReloadData()
        JsScript("ShowTransDetails();")
        Bind()
    End Sub

    Private Sub JsScript(ByVal Script As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), Guid.NewGuid.ToString, Script, True)
    End Sub

    Protected Sub GridViewPayments_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewPayments.RowDataBound
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim BtnExportToPDF As ImageButton = DirectCast(e.Row.FindControl("BtnExportToPDF"), ImageButton)
            Dim BtnExportTEXCEL As ImageButton = DirectCast(e.Row.FindControl("BtnExportTEXCEL"), ImageButton)
            BtnExportToPDF.ImageUrl = ResHelper.LocalizeString("{$=~/app_themes/LivePay/btnSavePDF.png|en-us=~/app_themes/LivePay/btnSavePDF_en-us.png$}")
            BtnExportTEXCEL.ImageUrl = ResHelper.LocalizeString("{$=~/app_themes/LivePay/btnSaveExcel.png|en-us=~/app_themes/LivePay/btnSaveExcel_en-us.png$}")

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblMerchantID As Label = DirectCast(e.Row.FindControl("lblMerchantID"), Label)
            Dim lbltransactiondDte As Label = DirectCast(e.Row.FindControl("lbltransactiondDte"), Label)
            Dim lbltransactionCode As Label = DirectCast(e.Row.FindControl("lbltransactionCode"), Label)
            Dim lblInstallments As Label = DirectCast(e.Row.FindControl("lblInstallments"), Label)
            'Dim system As LivePay_ESBBridge.SystemEnum = DirectCast(e.Row.DataItem("system"), LivePay_ESBBridge.SystemEnum)
            Dim myTransactionInfo As TransactionInfo = e.Row.DataItem

            If myTransactionInfo.system = LivePay_ESBBridge.SystemEnum.Merchants Then
                lbltransactionCode.Text = myTransactionInfo.transactionId
            Else
                lbltransactionCode.Text = "---" 'myTransactionInfo.info1
            End If
            If myTransactionInfo.installments > 1 Then
                lblInstallments.Text = myTransactionInfo.installments
            Else
                lblInstallments.Text = ""
            End If


            If Not IsNothing(lblMerchantID) Then
                If lbltransactiondDte.Text.Contains("/0001") Then
                    lbltransactiondDte.Text = ResHelper.LocalizeString("{$=���|en-us=��$}")

                End If


                Dim MerachantID As String = StripHTMLFunctions.StripTags(lblMerchantID.Text)
                Dim dt As DataTable = DBConnection.GetMerchantByESBID(StripHTMLFunctions.StripTags(lblMerchantID.Text), CMSContext.CurrentDocumentCulture.CultureCode)
                If dt.Rows.Count > 0 Then
                    lblMerchantID.Text = dt.Rows(0)("DiscreetTitle").ToString
                End If
            End If
        End If
    End Sub

End Class

