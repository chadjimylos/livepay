using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.TreeEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.PortalEngine;
using CMS.UIControls;
using CMS.URLRewritingEngine;
using CMS.SiteProvider;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_View_LiveSite : CMSContentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Switch the view mode
        CMSContext.ViewMode = ViewModeEnum.LiveSite;

        // Get the document
        int nodeId = 0;
        if (Request.QueryString["nodeid"] != null)
        {
            nodeId = ValidationHelper.GetInteger(Request.QueryString["nodeid"], 0);
        }
        // Get the document
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        TreeNode node = tree.SelectSingleNode(nodeId);

        // Redirect to the live URL
        string url = null;
        if (node != null)
        {
            // Check if the node is published or available
            string siteName = CMSContext.CurrentSiteName;
            if (!node.DocumentCulture.Equals(CMSContext.PreferredCultureCode, StringComparison.InvariantCultureIgnoreCase) && (!SiteInfoProvider.CombineWithDefaultCulture(siteName) || !node.DocumentCulture.Equals(CultureHelper.GetDefaultCulture(siteName), StringComparison.InvariantCultureIgnoreCase)))
            {
                url = "~/CMSMessages/PageNotAvailable.aspx?reason=missingculture";
            }

            if ((url == null) && !node.IsPublished && URLRewriter.PageNotFoundForNonPublished(siteName))
            {
                // Document is not published
                url = "~/CMSMessages/PageNotAvailable.aspx?reason=notpublished";
            }
        }
        else
        {
            url = "~/CMSMessages/PageNotAvailable.aspx";
        }

        if (url == null)
        {
            url = CMSContext.GetUrl(node.NodeAliasPath, node.DocumentUrlPath);
        }

        UrlHelper.Redirect(url);
    }
}
