using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.Staging;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Properties_Alias_List : CMSPropertiesPage
{
    #region "Private variables"

    protected int nodeId = 0;
    protected string mSave = null;

    TreeNode node = null;
    TreeProvider tree = null;

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.UniGridAlias.StopProcessing = this.pnlUIDocumentAlias.IsHidden;

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.URLs"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.URLs");
        }

        // Redirect to information page when no UI elements displayed
        if (this.pnlUIDocumentAlias.IsHidden && this.pnlUIExtended.IsHidden && this.pnlUIPath.IsHidden)
        {
            RedirectToUINotAvailable();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the scripts
        ScriptHelper.RegisterProgress(Page);

        UIContext.PropertyTab = PropertyTabEnum.URLs;

        nodeId = ValidationHelper.GetInteger(Request.QueryString["nodeid"], 0);

        if (nodeId > 0)
        {
            // Get the node
            tree = new TreeProvider(CMSContext.CurrentUser);
            node = tree.SelectSingleNode(nodeId);

            if (node != null)
            {
                imgSave.DisabledImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/savedisabled.png");
                imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
                imgNewAlias.ImageUrl = GetImageUrl("CMSModules/CMS_Content/Properties/adddocumentalias.png");
                imgNewAlias.DisabledImageUrl = GetImageUrl("CMSModules/CMS_Content/Properties/adddocumentaliasdisabled.png");
                if (node.NodeAliasPath == "/")
                {
                    valAlias.Visible = false;
                }

                // Check read permissions
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                {
                    RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
                }
                // Check modify permissions
                else if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                {
                    UniGridAlias.GridView.Enabled = false;
                    lnkNewAlias.Enabled = false;
                    imgNewAlias.Enabled = false;
                    lblInfo.Visible = true;
                    lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);

                    // Disable save button
                    lnkSave.Enabled = false;
                    lnkSave.CssClass = "MenuItemEditDisabled";
                    
                }
                else
                {
                    lblInfo.Visible = false;
                    ltlScript.Text = ScriptHelper.GetScript("var node = " + nodeId + "; \n var deleteMsg = '" + ResHelper.GetString("DocumentAlias.DeleteMsg") + "';");

                    UniGridAlias.OnAction += UniGridAlias_OnAction;
                    UniGridAlias.OnExternalDataBound += UniGridAlias_OnExternalDataBound;                    

                    // Shortcuts script
                    ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
                            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>" +
                            ScriptHelper.GetScript("function SaveDocument() { " + ClientScript.GetPostBackEventReference(lnkSave, null) + " } \n"
                        ));
                }

                UniGridAlias.WhereCondition = "AliasNodeID = " + nodeId;
                lnkNewAlias.Text = ResHelper.GetString("doc.urls.addnewalias");
                lnkNewAlias.NavigateUrl = "Alias_Edit.aspx?nodeid=" + nodeId;
                

                mSave = ResHelper.GetString("general.save");
                lblAlias.Text = ResHelper.GetString("GeneralProperties.Alias");
                lblUrlPath.Text = ResHelper.GetString("GeneralProperties.UrlPath");
                chkCustomUrl.Text = ResHelper.GetString("GeneralProperties.UseCustomUrlPath");
                chkCustomExtensions.Text = ResHelper.GetString("GeneralProperties.UseCustomExtensions");
                valAlias.ErrorMessage = ResHelper.GetString("GeneralProperties.RequiresAlias");

                lblExtensions.Text = ResHelper.GetString("doc.urls.urlextensions") + ResHelper.Colon;
                lblCampaign.Text = ResHelper.GetString("doc.urls.trackcampaign") + ResHelper.Colon;

                pnlPath.GroupingText = ResHelper.GetString("GeneralProperties.PathGroup");
                pnlDocumentAlias.GroupingText = ResHelper.GetString("doc.urls.documentalias");
                pnlExtended.GroupingText = ResHelper.GetString("doc.urls.extendedproperties");


                if (!RequestHelper.IsPostBack())
                {
                    ReloadData();
                }
            }
        }
    }


    protected object UniGridAlias_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "culture":
                return UniGridFunctions.CultureDisplayName(parameter);
        }

        return parameter;
    }


    void UniGridAlias_OnAction(string actionName, object actionArgument)
    {
        // Edit action
        if (DataHelper.GetNotEmpty(actionName, String.Empty) == "edit")
        {
            UrlHelper.Redirect("Alias_Edit.aspx?nodeid=" + nodeId + "&aliasid=" + Convert.ToString(actionArgument));
        }
        // Delete action
        else if (DataHelper.GetNotEmpty(actionName, String.Empty) == "delete")
        {
            int aliasId = ValidationHelper.GetInteger(actionArgument, 0);
            if (aliasId > 0)
            {
                // Delete
                DocumentAliasInfoProvider.DeleteDocumentAliasInfo(aliasId);

                // Log synchronization
                DocumentHelper.LogSynchronization(node, TaskTypeEnum.UpdateDocument, tree, SynchronizationInfoProvider.ENABLED_SERVERS);
            }
        }
    }


    protected void chkCustomUrl_CheckedChanged(object sender, EventArgs e)
    {
        txtUrlPath.Enabled = chkCustomUrl.Checked;
        if (!chkCustomUrl.Checked)
        {
            txtUrlPath.Text = "";
        }
    }


    protected void chkCustomExtensions_CheckedChanged(object sender, EventArgs e)
    {
        txtExtensions.Enabled = chkCustomExtensions.Checked;
        if (!chkCustomExtensions.Checked)
        {
            txtExtensions.Text = node.DocumentExtensions;
        }
    }


    protected void lnkSave_Click(object sender, EventArgs e)
    {
        // Get the document
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        node = tree.SelectSingleNode(nodeId, CMSContext.PreferredCultureCode);
        if (node != null)
        {
            // PATH group is displayed
            if (!this.pnlUIPath.IsHidden)
            {
                node.NodeAlias = txtAlias.Text.Trim();

                string docUrlPath = txtUrlPath.Text.Trim();

                if (docUrlPath != "")
                {
                    docUrlPath = "/" + docUrlPath.TrimStart('/');
                }

                node.DocumentUrlPath = docUrlPath;

                node.DocumentUseNamePathForUrlPath = !chkCustomUrl.Checked;
                if (node.DocumentUseNamePathForUrlPath)
                {
                    string urlPath = TreePathUtils.GetUrlPathFromNamePath(node.DocumentNamePath, node.NodeLevel);
                    node.DocumentUrlPath = urlPath;
                    txtUrlPath.Text = urlPath;
                }
            }

            // EXTENDED group is displayed
            if (!this.pnlUIExtended.IsHidden)
            {
                node.DocumentCampaign = txtCampaign.Text;

                node.DocumentUseCustomExtensions = chkCustomExtensions.Checked;
                if (node.DocumentUseCustomExtensions)
                {
                    node.DocumentExtensions = txtExtensions.Text;
                }
            }

            // Save the data
            node.Update();

            // Update search index
            if ((node.PublishedVersionExists) && (SearchIndexInfoProvider.SearchEnabled))
            {
                SearchTaskInfoProvider.CreateTask(SearchTaskTypeEnum.Update, PredefinedObjectType.DOCUMENT, SearchHelper.ID_FIELD, node.GetSearchID());
            }

            txtAlias.Text = node.NodeAlias;
            txtUrlPath.Text = node.DocumentUrlPath;

            // Log synchronization
            DocumentHelper.LogSynchronization(node, CMS.Staging.TaskTypeEnum.UpdateDocument, tree);

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");

            UniGridAlias.ReloadData();
        }
    }


    private void ReloadData()
    {
        if (node != null)
        {
            // Check read permissions
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
            }
            else
            {
                // Check modify permissions
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                {
                    // show access denied message
                    lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                    pnlPath.Enabled = false;
                    pnlExtended.Enabled = false;
                    pnlDocumentAlias.Enabled = false;
                }


                chkCustomUrl.Checked = !node.DocumentUseNamePathForUrlPath;
                chkCustomExtensions.Checked = node.DocumentUseCustomExtensions;
                txtExtensions.Text = node.DocumentExtensions;
                txtCampaign.Text = node.DocumentCampaign;
                txtAlias.Text = node.NodeAlias;
                txtUrlPath.Text = node.DocumentUrlPath;

                if (!chkCustomUrl.Checked)
                {
                    txtUrlPath.Enabled = false;
                }

                if (!chkCustomExtensions.Checked)
                {
                    txtExtensions.Enabled = false;
                }

                if (node.NodeClassName.ToLower() == "cms.root")
                {
                    txtAlias.Enabled = false;
                    txtUrlPath.Enabled = false;
                    txtUrlPath.Text = "";
                    chkCustomUrl.Enabled = false;
                    valAlias.Enabled = false;
                }

                if (node.IsLink)
                {
                    txtUrlPath.Enabled = false;
                    chkCustomUrl.Enabled = false;
                }
            }
        }
    }
}
