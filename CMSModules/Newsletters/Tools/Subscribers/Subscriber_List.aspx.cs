using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Subscribers_Subscriber_List : CMSNewsletterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.HeaderActions.HelpTopicName = "subscribers_tab2";
        this.CurrentMaster.HeaderActions.HelpName = "helpTopic";

        // Add subscriber link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Subscriber_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("Subscriber_New.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Newsletter_Subscriber/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;

        // Initialize unigrid
        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(uniGrid_OnExternalDataBound);
        UniGrid.WhereCondition = "SubscriberSiteID=" + CMSContext.CurrentSiteID.ToString();
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Unigrid external databound event handler.
    /// </summary>
    protected object uniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        if (sourceName.ToLower() == "email")
        {
            DataRowView dr = (DataRowView)parameter;
            string email = ValidationHelper.GetString(DataHelper.GetDataRowValue(dr.Row, "SubscriberEmail"), "");
            if (!string.IsNullOrEmpty(email))
            {
                return email;
            }
            else
            {
                return ValidationHelper.GetString(DataHelper.GetDataRowValue(dr.Row, "Email"), "");
            }
        }

        return null;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Subscriber_Frameset.aspx?subscriberid=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "managesubscribers"))
            {
                RedirectToCMSDeskAccessDenied("cms.newsletter", "managesubscribers");
            }
            // Delete Subscriber object from database
            SubscriberProvider.DeleteSubscriber(Convert.ToInt32(actionArgument));
        }
    }
}