using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.FormControls;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_Users_User_Edit_CustomFields : CMSUsersPage
{
    protected int userId = 0;


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);



        // Get user ID from query string
        userId = ValidationHelper.GetInteger(Request.QueryString["userID"], 0);

        // Check that only global administrator can edit global administrator's accouns
        UserInfo ui = UserInfoProvider.GetUserInfo(userId);
        if (!CheckGlobalAdminEdit(ui))
        {
            plcUserCustomFields.Visible = false;
            plcUserSettingsCustomFields.Visible = false;
            lblError.Visible = true;
            return;
        }

        // Setup user info for user custom fields dataform
        formUserCustomFields.Info = UserInfoProvider.GetUserInfo(userId);

        // If table has not any custom field hide custom field placeholder
        if ((formUserCustomFields.Info == null) || (formUserCustomFields.BasicForm.FormInformation.GetFormElements(true, false, true).Count <= 0))
        {
            plcUserCustomFields.Visible = false;
        }
        else
        {
            // Setup the User DataForm
            formUserCustomFields.BasicForm.HideSystemFields = true;
            formUserCustomFields.BasicForm.CssClass = "ContentDataFormButton";
            formUserCustomFields.BasicForm.SubmitButton.Visible = false;
            formUserCustomFields.OnAfterSave += new DataForm.OnAfterSaveEventHandler(formCustomFields_OnAfterSave);
        }

        // Setup user settings info for user settings custom fields dataform
        formUserSettingsCustomFields.Info = UserSettingsInfoProvider.GetUserSettingsInfoByUser(userId);

        if ((formUserSettingsCustomFields.Info == null) || (formUserSettingsCustomFields.BasicForm.FormInformation.GetFormElements(true, false, true).Count <= 0))
        {
            plcUserSettingsCustomFields.Visible = false;

            // If user settings has no custom fields show OK button of custom user dataform
            if (plcUserCustomFields.Visible)
            {
                formUserCustomFields.BasicForm.SubmitButton.Visible = true;
            }
        }
        else
        {
            // Setup the UserSettings DataForm
            formUserSettingsCustomFields.OnAfterSave += new DataForm.OnAfterSaveEventHandler(formCustomFields_OnAfterSave);
            formUserSettingsCustomFields.OnBeforeSave += new DataForm.OnBeforeSaveEventHandler(formCustomFields_OnBeforeSave);
            formUserSettingsCustomFields.BasicForm.HideSystemFields = true;
            formUserSettingsCustomFields.BasicForm.CssClass = "ContentDataFormButton";
        }

        
        // Check "modify" permission - Extra Code by Harlem :P
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify"))
        {
            //RedirectToAccessDenied("CMS.Users", "Modify");
            formUserCustomFields.BasicForm.SubmitButton.Visible = false;
        }

        if (CMSContext.CurrentUser.IsInRole("EurophoneBanking", CMSContext.CurrentSite.SiteName.ToString()))
        {
            formUserCustomFields.BasicForm.SubmitButton.Visible = true;

            TextBox LivePayID = (TextBox)formUserCustomFields.BasicForm.FindControl("LivePayID");
            LivePayID.Attributes.Add("disabled", "disabled");

            TextBox UserPhone = (TextBox)formUserCustomFields.BasicForm.FindControl("UserPhone");
            UserPhone.Attributes.Add("disabled", "disabled");

            DropDownList UserQuestions = (DropDownList)formUserCustomFields.BasicForm.FindControl("UserQuestions");
            UserQuestions.Attributes.Add("disabled", "disabled");

            TextBox UserAnswers = (TextBox)formUserCustomFields.BasicForm.FindControl("UserAnswers");
            UserAnswers.Attributes.Add("disabled", "disabled");

            TextBox UserLoginAttempts = (TextBox)formUserCustomFields.BasicForm.FindControl("UserLoginAttempts");
            UserLoginAttempts.Attributes.Add("disabled", "disabled");
            
            TextBox ParentDealer = (TextBox)formUserCustomFields.BasicForm.FindControl("ParentDealer");

            if (ParentDealer != null){
                ParentDealer.Attributes.Add("disabled", "disabled");            
            }


        }

    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (formUserCustomFields.BasicForm != null)
        {
            formUserCustomFields.BasicForm.SubmitButton.CssClass = "SubmitButton";
        }
        if (formUserSettingsCustomFields.BasicForm != null)
        {
            formUserSettingsCustomFields.BasicForm.SubmitButton.CssClass = "SubmitButton";
        }
    }


    /// <summary>
    /// UserSettings dataform OnBeforeSave
    /// </summary>
    void formCustomFields_OnBeforeSave()
    {
        // If user plcUserCustomFields some user custom fields are present - save them too.
        if (plcUserCustomFields.Visible)
        {
            formUserCustomFields.Save();
        }
    }


    /// <summary>
    /// On after save (called from both dataforms)
    /// </summary>
    void formCustomFields_OnAfterSave()
    {
        this.lblInfo.Visible = true;
    }
}
