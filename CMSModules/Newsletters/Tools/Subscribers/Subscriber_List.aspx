<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Subscriber_List.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Subscribers_Subscriber_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Tools - Newsletter subscribers" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Subscriber_List.xml" OrderBy="SubscriberFullName"
        Columns="SubscriberID, SubscriberFullName, SubscriberEmail, Email" IsLiveSite="false" />
</asp:Content>
