CREATE VIEW View_Community_Friend_RequestedFriends
AS
SELECT     Community_Friend.FriendID, Community_Friend.FriendRequestedUserID, Community_Friend.FriendUserID, 
                      Community_Friend.FriendRequestedWhen, Community_Friend.FriendComment, Community_Friend.FriendApprovedBy, 
                      Community_Friend.FriendApprovedWhen, Community_Friend.FriendRejectedBy, Community_Friend.FriendRejectedWhen, 
                      Community_Friend.FriendGUID, Community_Friend.FriendStatus, View_CMS_User.UserID, View_CMS_User.UserName, 
                      View_CMS_User.FirstName, View_CMS_User.MiddleName, View_CMS_User.LastName, View_CMS_User.FullName, 
                      View_CMS_User.Email, View_CMS_User.UserPassword, View_CMS_User.PreferredCultureCode, 
                      View_CMS_User.PreferredUICultureCode, View_CMS_User.UserEnabled, View_CMS_User.UserIsEditor, 
                      View_CMS_User.UserIsGlobalAdministrator, View_CMS_User.UserIsExternal, View_CMS_User.UserPasswordFormat, 
                      View_CMS_User.UserCreated, View_CMS_User.LastLogon, View_CMS_User.UserStartingAliasPath, View_CMS_User.UserGUID, 
                      View_CMS_User.UserLastModified, View_CMS_User.UserLastLogonInfo, View_CMS_User.UserIsHidden, 
                      View_CMS_User.UserVisibility, View_CMS_User.UserIsDomain, View_CMS_User.UserHasAllowedCultures, 
                      View_CMS_User.UserSettingsID, View_CMS_User.UserNickName, View_CMS_User.UserPicture, View_CMS_User.UserSignature, 
                      View_CMS_User.UserURLReferrer, View_CMS_User.UserCampaign, View_CMS_User.UserMessagingNotificationEmail, 
                      View_CMS_User.UserCustomData, View_CMS_User.UserRegistrationInfo, View_CMS_User.UserPreferences, 
                      View_CMS_User.UserActivationDate, View_CMS_User.UserActivatedByUserID, View_CMS_User.UserTimeZoneID, 
                      View_CMS_User.UserAvatarID, View_CMS_User.UserBadgeID, View_CMS_User.UserShowSplashScreen, 
                      View_CMS_User.UserActivityPoints, View_CMS_User.UserForumPosts, View_CMS_User.UserBlogComments, 
                      View_CMS_User.UserGender, View_CMS_User.UserDateOfBirth, View_CMS_User.UserMessageBoardPosts, 
                      View_CMS_User.UserSettingsUserGUID, View_CMS_User.UserSettingsUserID, View_CMS_User.WindowsLiveID, 
                      View_CMS_User.UserBlogPosts, View_CMS_User.UserWaitingForApproval, View_CMS_User.UserDialogsConfiguration, 
                      View_CMS_User.UserDescription, View_CMS_User.UserUsedWebParts, View_CMS_User.UserUsedWidgets, 
                      View_CMS_User.AvatarID, View_CMS_User.AvatarFileName, View_CMS_User.AvatarGUID
FROM         Community_Friend INNER JOIN
                      View_CMS_User ON Community_Friend.FriendRequestedUserID = View_CMS_User.UserID
