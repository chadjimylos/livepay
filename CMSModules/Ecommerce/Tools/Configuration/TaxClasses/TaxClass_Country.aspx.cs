using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_TaxClasses_TaxClass_Country : CMSEcommerceConfigurationPage
{
    protected string mSave = null;
    protected int taxClassId = 0;
    private Hashtable mChangedTextBoxes = new Hashtable();
    private Hashtable mChangedCheckBoxes = new Hashtable();
    //protected string mDeleteImagePath = "";
    //protected string mDeleteTip = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        taxClassId = ValidationHelper.GetInteger(Request.QueryString["taxclassid"], 0);
        if (taxClassId == 0)
        {
            btnSave.Enabled = false;
            return;
        }

        //GridViewCountries.Columns[0].HeaderText = ResHelper.GetString("TaxClass_Country.Actions");
        GridViewCountries.Columns[0].HeaderText = ResHelper.GetString("TaxClass_Country.Country");
        GridViewCountries.Columns[1].HeaderText = ResHelper.GetString("TaxClass_Country.Value");
        GridViewCountries.Columns[2].HeaderText = ResHelper.GetString("TaxClass_Country.IsFlat");
        DataSet ds = TaxClassInfoProvider.GetTaxClassCountries(taxClassId);
        GridViewCountries.Columns[3].Visible = true;
        GridViewCountries.DataSource = ds.Tables[0];
        GridViewCountries.DataBind();
        //after id is copied, the 4th column it's not needed anymore
        GridViewCountries.Columns[3].Visible = false;
        GridViewCountries.GridLines = GridLines.Horizontal;

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>" +
            ScriptHelper.GetScript("function SaveDocument() { " + this.ClientScript.GetPostBackEventReference(this.btnSave, null) + " } \n"
        ));

        this.imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        mSave = ResHelper.GetString("general.save");
    }


    protected void GridViewCountries_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < GridViewCountries.Rows.Count; i++)
        {
            //copy id from 5th column to invisible label in last column
            Label id = new Label();
            id.Visible = false;
            id.Text = GridViewCountries.Rows[i].Cells[3].Text;
            GridViewCountries.Rows[i].Cells[3].Controls.Add(id);

            TextBox txtValue = (TextBox)GridViewCountries.Rows[i].Cells[1].Controls[1];
            txtValue.ID = "txtTaxValue" + id.Text;

            CheckBox chkIsFlat = (CheckBox)GridViewCountries.Rows[i].Cells[2].Controls[1];
            chkIsFlat.ID = "chkIsFlatValue" + id.Text;
        }
    }


    protected void txtTaxValue_Changed(object sender, EventArgs e)
    {
        mChangedTextBoxes[((TextBox)sender).ID] = sender;
    }


    protected void chkIsFlatValue_Changed(object sender, EventArgs e)
    {
        mChangedCheckBoxes[((CheckBox)sender).ID] = sender;
    }


    protected bool ConvertToBoolean(object value)
    {
        return ValidationHelper.GetBoolean(value, false);
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        TaxClassCountryInfo countryInfo;

        Label lblCountryId = null;
        int countryId = 0;

        string stateName;

        TextBox txtValue = null;
        double value = 0.0;

        CheckBox chkIsFlat;
        bool isFlat = false;

        for (int i = 0; i < GridViewCountries.Rows.Count; i++)
        {
            lblCountryId = (Label)GridViewCountries.Rows[i].Cells[3].Controls[0];
            countryId = ValidationHelper.GetInteger(lblCountryId.Text, 0);

            if (countryId > 0)
            {
                stateName = ((Label)GridViewCountries.Rows[i].Cells[0].Controls[1]).Text;
                txtValue = (TextBox)GridViewCountries.Rows[i].Cells[1].Controls[1];
                chkIsFlat = (CheckBox)GridViewCountries.Rows[i].Cells[2].Controls[1];

                if ((mChangedTextBoxes[txtValue.ID] != null) || (mChangedCheckBoxes[chkIsFlat.ID]) != null)
                {
                    //if text box was changed
                    if (txtValue.Text == "")
                    {
                        TaxClassInfoProvider.DeleteTaxClassCountryInfo(countryId, taxClassId);
                        chkIsFlat.Checked = false;
                    }
                    else
                    {
                        //tax value must be greater or equal to zero
                        if (ValidationHelper.IsPositiveNumber(txtValue.Text))
                        {
                            value = ValidationHelper.GetDouble(txtValue.Text, 0);
                            isFlat = chkIsFlat.Checked;

                            countryInfo = new TaxClassCountryInfo();

                            countryInfo.CountryID = countryId;
                            countryInfo.TaxClassID = taxClassId;
                            countryInfo.TaxValue = value;
                            countryInfo.IsFlatValue = isFlat;

                            TaxClassInfoProvider.SetTaxClassCountryInfo(countryInfo);
                        }
                        else
                        {
                            //add state name where an error occurs, error message will be added after
                            lblError.Text += stateName + ", ";
                            lblError.Visible = true;
                        }
                    }
                }
            }
        }

        // add error message text
        if (lblError.Visible == true)
        {
            //delete last comma
            if (lblError.Text.EndsWith(", "))
            {
                lblError.Text = lblError.Text.Remove(lblError.Text.Length - 2, 2);
            }

            lblError.Text += " - " + ResHelper.GetString("taxclass_state.TaxValueError");
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}
