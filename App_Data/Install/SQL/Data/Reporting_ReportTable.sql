SET IDENTITY_INSERT [Reporting_ReportTable] ON
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$general.documentname$}'', 
 SUM(HitsCount) AS ''{$reports_filedownloads_day.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName)  AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY DocumentNamePath, StatisticsObjectName
 ORDER BY SUM(HitsCount) DESC', N'TableDayFileDownloads', '20100219 14:13:10', 102, 55, 'ba4442d3-5e9f-43b1-b13f-6d2ab3625737', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$general.documentname$}'', 
 SUM(HitsCount) AS ''{$reports_filedownloads_month.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY DocumentNamePath, StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableMonthFileDownloads', '20100219 14:15:17', 103, 56, '380150e3-442f-4ea7-a543-c8fd77d7bf6d', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 DocumentNamePath AS ''{$general.documentname$}'', SUM(HitsCount) AS ''{$reports_filedownloads_year.hits_header$}'' FROM 
Analytics_Statistics, Analytics_YearHits, View_CMS_Tree_Joined WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (StatisticsObjectID = NodeID) AND (StatisticsObjectCulture = DocumentCulture) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DocumentNamePath ORDER BY SUM(HitsCount) DESC', N'TableYearFileDownloads', '20081217 12:38:36', 104, 57, '1ff97f22-6fbb-46d9-b102-ec2e00efccc3', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$general.documentname$}'', 
 SUM(HitsCount) AS ''{$reports_filedownloads_hour.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY DocumentNamePath, StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableHourFileDownloads', '20100219 14:14:06', 105, 58, '8127bec1-8f00-48ee-b356-4acbd72bd6ce', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'Select DocumentName AS ''Document name'', NodeAliasPath AS ''Document alias path'', DocumentModifiedWhen AS ''Last modification date'', FirstName + '' ''  + LastName + '' ('' + UserName +'')'' AS ''Last modified by'', StepDisplayName AS ''Current workflow step''  FROM View_CMS_Tree_Joined LEFT JOIN CMS_User ON View_CMS_Tree_Joined.DocumentModifiedByUserID=CMS_User.UserID LEFT JOIN CMS_WorkflowStep ON View_CMS_Tree_Joined.DocumentWorkflowStepID = CMS_WorkflowStep.StepID Where (DocumentModifiedWhen >= @ModifiedSince) AND (NodeSiteID=@CMSContextCurrentSiteID) ORDER BY DocumentModifiedWhen DESC', N'RecentlyModifiedDocsTable', '20091218 12:24:10', 106, 50, '84b838bf-a8e2-4852-a784-c7c99d0f2941', N'Recently modified docs table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$reports_pageviews_day.path_header$}'', 
 SUM(HitsCount) AS ''{$reports_pageviews_day.hits_header$}'' 
 FROM 
 Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID =  Analytics_Statistics.StatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID =  Analytics_Statistics.StatisticsObjectID AND  Analytics_Statistics.StatisticsObjectCulture =  View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) 
 AND (HitsEndTime <= @ToDate) 
 GROUP BY DocumentNamePath,  StatisticsObjectName
 ORDER BY ''{$reports_pageviews_day.hits_header$}'' DESC', N'TableDayPageViews', '20100219 13:45:43', 107, 59, '47f63e99-8a64-49c9-ad7e-4be17f5c9151', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 CASE 
	WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName
	ELSE DocumentNamePath 
 END AS ''{$reports_pageviews_month.path_header$}'', 
 SUM(HitsCount) AS ''{$reports_pageviews_month.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID 
 LEFT JOIN View_CMS_Tree_Joined ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID AND Analytics_Statistics.StatisticsObjectCulture=View_CMS_Tree_Joined.DocumentCulture 
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
 AND (StatisticsCode=@CodeName) 
 GROUP BY DocumentNamePath, StatisticsObjectName
 ORDER BY ''{$reports_pageviews_month.hits_header$}'' DESC', N'TableMonthPageViews', '20100219 13:42:28', 108, 60, '978ecb24-c417-468d-a901-c4c298a8b02a', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'Select DocumentName AS ''Document name'', NodeAliasPath AS ''Document alias path'', FirstName + '' '' + LastName + '' ('' +UserName+'')'' AS ''Checked out by'', DocumentCheckedOutWhen AS ''Checked out on'', FirstName + '' ''  + LastName + '' ('' + UserName +'')'' AS ''Last modified by'', StepDisplayName AS ''Current workflow step'' FROM View_CMS_Tree_Joined LEFT JOIN CMS_User ON CMS_User.UserID = View_CMS_Tree_Joined.DocumentCheckedOutByUserID  LEFT JOIN CMS_WorkflowStep ON View_CMS_Tree_Joined.DocumentWorkflowStepID = CMS_WorkflowStep.StepID  Where (DocumentCheckedOutByUserID IS NOT NULL) AND (NodeSiteID=@CMSContextCurrentSiteID) ORDER BY DocumentCheckedOutWhen DESC', N'CheckedOutDocsTable', '20080313 09:35:00', 109, 51, 'b25b33ab-cdda-4018-b0bf-7cfcaaad651e', N'Checked out docs table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 DocumentNamePath AS ''{$reports_pageviews_year.path_header$}'', SUM(HitsCount) AS ''{$reports_pageviews_year.hits_header$}'' FROM 
Analytics_Statistics, Analytics_YearHits, View_CMS_Tree_Joined WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (StatisticsObjectID = NodeID) AND (StatisticsObjectCulture = DocumentCulture) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DocumentNamePath ORDER BY SUM(HitsCount) DESC', N'TableYearPageViews', '20080410 17:23:29', 110, 61, 'e33dd959-8cfe-4dee-ae4e-e1fef7055e59', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'Select DocumentName AS ''Document name'', NodeAliasPath AS ''Document alias path'', DocumentPublishTo AS ''Expiration date'', FirstName + '' ''  + LastName + '' ('' + UserName +'')'' AS ''Last modified by'', StepDisplayName AS ''Current workflow step'' FROM View_CMS_Tree_Joined LEFT JOIN CMS_User ON View_CMS_Tree_Joined.DocumentModifiedByUserID=CMS_User.UserID LEFT JOIN CMS_WorkflowStep ON View_CMS_Tree_Joined.DocumentWorkflowStepID = CMS_WorkflowStep.StepID Where (DocumentPublishTo IS NOT NULL) AND (DocumentPublishTo < @CMSContextCurrentTime) AND (NodeSiteID=@CMSContextCurrentSiteID) ORDER BY DocumentPublishTo DESC', N'ExpiredDocsTable', '20080313 09:35:00', 111, 52, '305d166c-9965-4076-8ba1-0eb3535815b8', N'Expired docs table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'Select DocumentName AS ''Document name'', NodeAliasPath AS ''Document alias path'', FirstName + '' ''  + LastName + '' ('' + UserName +'')'' AS ''Last modified by'', StepDisplayName AS ''Current workflow step'' FROM View_CMS_Tree_Joined LEFT JOIN CMS_User ON View_CMS_Tree_Joined.DocumentModifiedByUserID=CMS_User.UserID LEFT JOIN CMS_WorkflowStep ON View_CMS_Tree_Joined.DocumentWorkflowStepID = CMS_WorkflowStep.StepID WHERE (DocumentWorkflowStepID IN (SELECT StepID From CMS_WorkflowStep WHERE StepName=''archived'') AND (NodeSiteID=@CMSContextCurrentSiteID)) ORDER BY NodeAliasPath', N'ArchivedDocsTable', '20091220 20:15:23', 112, 53, 'eaff66b0-76df-4898-98c2-f04f748a4bfe', N'Archived docs table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'Select DocumentName AS ''Document name'', NodeAliasPath AS ''Document alias path'', FirstName + '' ''  + LastName + '' ('' + UserName +'')'' AS ''Last modified by'', StepDisplayName AS ''Current workflow step'' FROM View_CMS_Tree_Joined_Versions LEFT JOIN CMS_User ON View_CMS_Tree_Joined_Versions.DocumentModifiedByUserID=CMS_User.UserID LEFT JOIN CMS_WorkflowStep ON View_CMS_Tree_Joined_Versions.DocumentWorkflowStepID = CMS_WorkflowStep.StepID WHERE View_CMS_Tree_Joined_Versions.DocumentWorkflowStepID IN (SELECT StepID FROM CMS_WorkflowStep WHERE (StepName <> ''published'' and StepName <>''archived'')) AND (NodeSiteID=@CMSContextCurrentSiteID) ORDER BY NodeAliasPath', N'DocsWaitingForApproval', '20080313 09:35:00', 113, 54, '15feabe3-d47c-4a6b-8345-1b87c7b3e576', N'Docs waiting for approval')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100
 CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$reports_pageviews_hour.path_header$}'',
 SUM(HitsCount) AS ''{$reports_pageviews_hour.hits_header$}''
 FROM  Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID)
 AND (StatisticsCode=@CodeName)
 AND (HitsStartTime >= @FromDate)
 AND (HitsEndTime <= @ToDate)
 GROUP BY DocumentNamePath, StatisticsObjectName
 ORDER BY ''{$reports_pageviews_hour.hits_header$}'' DESC', N'TableHourPageViews', '20100219 13:47:10', 114, 62, '079ba574-57b1-4d4f-8615-66c576439f55', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_pagenotfound_day.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_pagenotfound_day.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableDayPageNotFound', '20100217 12:48:50', 115, 63, 'fca58f03-6c62-41f6-9bce-67bdb3974abc', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_pagenotfound_month.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_pagenotfound_month.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName)  
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableMonthPageNotFound', '20100217 12:59:05', 116, 64, '36733865-50cb-4775-b1fb-bf5cee65114a', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 StatisticsObjectName AS ''{$reports_pagenotfound_year.name_header$}'', SUM(HitsCount) AS ''{$reports_pagenotfound_year.hits_header$}'' FROM 
Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableYearPageNotFound', '20080410 17:23:29', 117, 65, 'afaa4805-20b0-44cd-98f9-a3083c3b2413', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_pagenotfound_hour.path_header$}'', 
 SUM(HitsCount) AS ''{$reports_pagenotfound_hour.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableHourPageNotFound', '20100217 12:54:24', 118, 66, 'b426ae01-fe3b-4e96-baed-94ac4cd431a3', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_referrals_day.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_referrals_day.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) 
 AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableDayReferrals', '20100217 10:44:33', 119, 67, '4959a779-0ec7-46e0-8c11-1a35f10905ff', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_referrals_month.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_referrals_month.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) 
 AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableMonthReferrals', '20100217 10:49:19', 120, 68, 'ab4b39b9-326c-4f7f-a8a7-7c92f0ca2726', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 StatisticsObjectName AS ''{$reports_referrals_year.name_header$}'', SUM(HitsCount) AS ''{$reports_referrals_year.hits_header$}'' FROM 
Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableYearReferrals', '20080410 17:23:29', 121, 69, '5a96924b-ed83-484a-a84f-bc6d08da0181', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_referrals_hour.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_referrals_hour.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) 
 AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableHourReferrals', '20100217 10:47:09', 122, 70, '4d6192d9-9a19-448b-b70b-61526c7a3a4a', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$general.documentname$}'',  
 StatisticsObjectCulture AS ''{$general.culture$}'', 
 SUM(HitsCount) AS ''{$reports_filedownloads_day.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
 LEFT JOIN View_CMS_Tree_Joined  ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture 
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectCulture, DocumentNamePath, StatisticsObjectName
 ORDER BY SUM(HitsCount) DESC', N'TableDayFileDownloadsCulture', '20100219 14:04:39', 132, 86, 'd2acff50-77c9-4f7e-aca1-8170a88c86f8', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$general.documentname$}'', 
 StatisticsObjectCulture AS ''{$general.culture$}'', 
 SUM(HitsCount) AS ''{$reports_filedownloads_month.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectCulture, DocumentNamePath, StatisticsObjectName
 ORDER BY SUM(HitsCount) DESC', N'TableMonthFileDownloadsCulture', '20100219 14:10:40', 134, 87, 'eb1dac2c-12b2-4e9e-be41-1d7156cafbc1', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 DocumentNamePath AS ''{$general.documentname$}'', StatisticsObjectCulture AS
''{$reports_filedownloads_year.cult_header$}'',
SUM(HitsCount) AS ''{$general.culture$}'' FROM 
Analytics_Statistics, Analytics_YearHits, View_CMS_Tree_Joined WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (StatisticsObjectID = NodeID) AND (StatisticsObjectCulture = DocumentCulture) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectCulture, DocumentNamePath ORDER BY SUM(HitsCount) DESC, StatisticsObjectCulture', N'TableYearFileDownloadsCulture', '20090629 11:12:24', 135, 88, '563dce7e-3087-4103-bce7-94d7aa0a3b9a', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$general.documentname$}'', 
 StatisticsObjectCulture AS ''{$general.culture$}'', 
 SUM(HitsCount) AS ''{$reports_filedownloads_hour.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectCulture, DocumentNamePath, StatisticsObjectName
 ORDER BY SUM(HitsCount) DESC, StatisticsObjectCulture', N'TableHourFileDownloadsCulture', '20100219 14:06:14', 136, 89, 'd32f6ee1-7c21-411c-b4b7-75ce8e36360e', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'DECLARE @myselection TABLE (
   DocumentNamePath varchar(400),
   StatisticsObjectCulture varchar(400),
   Count float   
);
DECLARE @countedAll TABLE (
   Counted float   
);
INSERT INTO @myselection 
	SELECT TOP 100 
	CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$reports_pageviews_day.path_header$}'',  
	StatisticsObjectCulture AS ''{$general.culture$}'', 
	SUM(HitsCount) AS ''{$reports_pageviews_day.hits_header$}'' 
	FROM Analytics_Statistics 
    INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
    LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
    WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture, DocumentNamePath, StatisticsObjectName
	ORDER BY SUM(HitsCount) DESC
	
INSERT INTO @countedAll 
	SELECT
	SUM(HitsCount) AS ''{$reports_pageviews_day.hits_header$}'' 
	FROM Analytics_Statistics 
    INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
    LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
    WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
SELECT DocumentNamePath AS ''{$reports_pageviews_day.path_header$}'',  
StatisticsObjectCulture AS ''{$general.culture$}'', 
Count AS ''{$reports_pageviews_day.hits_header$}'',
CAST((100*Count)/(SELECT Counted FROM  @countedAll) as decimal(10,2)) 
as ''{$reports_pageviews_day.hits_percent_header$}'' 
FROM @myselection', N'TableDayPageViewsCulture', '20100324 10:21:55', 137, 90, 'd088e963-ff55-404f-92c3-19f0976e5ca5', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'DECLARE @myselection TABLE (
   DocumentNamePath varchar(400),
   StatisticsObjectCulture varchar(400),
   Count float   
);
DECLARE @countedAll TABLE (
   Counted float   
);
INSERT INTO @myselection 
	SELECT TOP 100 
	CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END, 
	StatisticsObjectCulture , 
	SUM(HitsCount) AS Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture, DocumentNamePath, StatisticsObjectName 
	ORDER BY SUM(HitsCount) DESC
	
INSERT INTO @countedAll 
	SELECT
	SUM(HitsCount) AS Counted 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
 SELECT DocumentNamePath AS ''{$reports_pageviews_month.path_header$}'', 
 StatisticsObjectCulture AS   ''{$general.culture$}'', 
 Count AS ''{$reports_pageviews_month.hits_header$}'', 
 CAST((100*Count)/(SELECT Counted FROM  @countedAll) as decimal(10,2)) as ''{$reports_pageviews_month.hits_percent_header$}'' 
 FROM @myselection', N'TableMonthPageViewsCulture', '20100324 10:16:21', 138, 91, '555720dc-dfd8-42f7-a93b-90b57f61bcd4', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'DECLARE @myselection TABLE (
   DocumentNamePath varchar(400),
   StatisticsObjectCulture varchar(400),
   Count float   
);
INSERT INTO @myselection SELECT TOP 100 DocumentNamePath, StatisticsObjectCulture, SUM(HitsCount) as Count FROM 
Analytics_Statistics, Analytics_YearHits, View_CMS_Tree_Joined WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (StatisticsObjectID = NodeID) AND (StatisticsObjectCulture = DocumentCulture) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectCulture, DocumentNamePath ORDER BY SUM(HitsCount) DESC, StatisticsObjectCulture
SELECT DocumentNamePath AS ''{$reports_pageviews_year.path_header$}'', StatisticsObjectCulture AS ''{$general.culture$}'',
Count AS ''{$reports_pageviews_year.hits_header$}'',
CAST((100*Count)/(SELECT SUM(Count) FROM @myselection) as decimal(10,2)) AS ''{$reports_pageviews_year.hits_percent_header$}'' FROM @myselection', N'TableYearPageViewsCulture', '20090629 11:16:05', 139, 92, '44dc48c4-090f-4fb6-bec3-4814bdbb2a3d', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'DECLARE @myselection TABLE (
   DocumentNamePath varchar(400),
   StatisticsObjectCulture varchar(400),
   Count float   
);
DECLARE @countedAll TABLE (
   Counted float   
);
INSERT INTO @myselection 
	SELECT TOP 100 
	CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END, 
	StatisticsObjectCulture, SUM(HitsCount) 
	FROM Analytics_Statistics
    INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
    LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture, DocumentNamePath, StatisticsObjectName
	ORDER BY SUM(HitsCount) DESC, StatisticsObjectCulture
	
INSERT INTO @countedAll 
	SELECT
	SUM(HitsCount) 
	FROM Analytics_Statistics
    INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
    LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 	
 SELECT DocumentNamePath AS ''{$reports_pageviews_hour.path_header$}'', 
 StatisticsObjectCulture AS ''{$general.culture$}'', 
 Count AS ''{$reports_pageviews_hour.hits_header$}'',
 CAST((100*Count)/(SELECT Counted FROM  @countedAll) as decimal(10,2)) as ''{$reports_pageviews_hour.hits_percent_header$}'' 
 FROM @myselection', N'TableHourFilePageViewsCulture', '20100324 10:29:03', 140, 93, '9ad190f2-6d1f-4419-92b2-261f14d0d468', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_campaign.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_campaign.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableDayCampaign', '20100217 16:12:55', 141, 94, '2cd0dfae-8282-4a07-8b7b-4c85056537cb', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_campaign.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_campaign.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableMonthCampaign', '20100219 10:43:28', 142, 95, '5667ec53-453a-45c7-ad69-a76ea53b2877', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 StatisticsObjectName AS ''{$reports_campaign.name_header$}'', SUM(HitsCount) AS ''{$reports_campaign.hits_header$}'' FROM 
Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableYearCampaign', '20080410 17:23:31', 143, 96, '14bab9a1-14e9-4c9f-a2cf-4d343a4118d9', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_campaign.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_campaign.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableHourCampaign', '20100219 10:37:46', 144, 97, '1bb584c6-62f9-47f1-b7fd-3f94b9fb2d84', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_conversion.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_conversion.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableDayConversion', '20100217 15:42:26', 145, 98, 'dba28818-8a32-4029-9449-1134db4d52c3', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_conversion.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_conversion.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableMonthConversion', '20100217 15:50:17', 146, 99, 'c350451e-e82d-4f51-89a7-1c7e32a88faa', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 StatisticsObjectName AS ''{$reports_conversion.name_header$}'', SUM(HitsCount) AS ''{$reports_conversion.hits_header$}'' FROM 
Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableYearConversion', '20080410 17:23:31', 147, 100, '9394ce04-7501-4888-ae83-4835bedb43f5', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_conversion.path_header$}'', 
 SUM(HitsCount) AS ''{$reports_conversion.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableHourConversion', '20100217 15:46:15', 148, 101, '75df0d0f-f86e-42bd-af2c-88024f27bd3a', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'DECLARE @myselection TABLE (
  MyYear int,
  Cnt float
);
SELECT DATENAME(year, OrderDate) as ''{$ecommerce.report_year$}'', COUNT(OrderDate) as ''{$ecommerce.report_number$}'' FROM COM_Order
WHERE OrderSiteID = @CMSContextCurrentSiteID
GROUP BY DATENAME(year, OrderDate)
ORDER BY MIN(OrderDate)', N'TableEcommNumOfOrdersPerYear', '20080303 12:03:45', 149, 102, 'ca60fca0-b11f-43ef-850a-dd60514ec00f', N'TableEcommNumOfOrdersPerYear')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT 
Orders.OrderMonth + '' '' + Orders.OrderYear as {$ecommerce.report_month$},
COUNT(Orders.OrderDate) as {$ecommerce.report_number$}
FROM  (SELECT OrderDate, DATENAME(year, OrderDate) as OrderYear, DATENAME(month, OrderDate) as OrderMonth FROM COM_Order 
WHERE OrderSiteID = @CMSContextCurrentSiteID) as Orders
GROUP BY Orders.OrderYear, Orders.OrderMonth
ORDER BY Min(Orders.OrderDate)', N'TableEcommNumOfOrdersPerMonth', '20080428 14:58:26', 150, 103, '4c06baae-93ac-465e-bfb0-97d6713bf057', N'TableEcommNumOfOrdersPerMonth')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT DATENAME(day,OrderDate) as ''{$ecommerce.report_day$}'', COUNT(OrderDate) as ''{$ecommerce.report_number$}'' FROM COM_Order
WHERE @SelectedMonth = DATENAME(year,OrderDate) + '' '' + DATENAME(month,OrderDate)
AND OrderSiteID = @CMSContextCurrentSiteID
GROUP BY DATENAME(day,OrderDate)
ORDER BY MIN(OrderDate)', N'TableEcommNumOfOrdersPerDay', '20080303 12:03:59', 151, 104, 'd177c209-5146-4fbb-97c1-e582c658b5cc', N'TableEcommNumOfOrdersPerDay')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT SKUName as [{$ecommerce.report_skuname$}], SKUNumber as [{$ecommerce.report_productnumber$}], DepartmentDisplayName as [{$ecommerce.report_departmentname$}], SKUAvailableItems as [{$ecommerce.report_availableitems$}] FROM COM_SKU, COM_Department WHERE (SKUAvailableItems < @Limit) AND (SKUDepartmentID = DepartmentID) ORDER BY SKUName ASC', N'TableEcommInventory', '20080421 10:22:27', 152, 105, '09730073-aed3-4e26-991e-11e91b2c8df8', N'TableEcommInventory')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT DATENAME(year, OrderDate) as ''{$ecommerce.report_year$}'', CAST(SUM(OrderTotalPrice) AS decimal(38,2)) as ''{$ecommerce.report_income$}'' FROM COM_Order
WHERE OrderSiteID = @CMSContextCurrentSiteID
GROUP BY DATENAME(year, OrderDate)
ORDER BY MIN(OrderDate)', N'TableEcommSalesPerYear', '20080303 12:02:47', 153, 106, 'a99e9db7-253f-4c6e-902f-7e4cdfc5bdc7', N'TableEcommSalesPerYear')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT  Orders.OrderMonth + '' '' + Orders.OrderYear as {$ecommerce.report_month$}, CAST(SUM(Orders.OrderTotalPrice) as decimal(38,2)) as {$ecommerce.report_income$}
FROM  (SELECT OrderTotalPrice, OrderDate, DATENAME(year, OrderDate) as OrderYear, DATENAME(month, OrderDate) as OrderMonth FROM COM_Order WHERE OrderSiteID = @CMSContextCurrentSiteID) as Orders
GROUP BY Orders.OrderYear, Orders.OrderMonth
ORDER BY Min(Orders.OrderDate)', N'TableEcommSalesPerMonth', '20080428 15:02:09', 154, 107, 'ab940bd2-b35c-49a6-b024-3ce69a92d42e', N'TableEcommSalesPerMonth')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT DATENAME(day,OrderDate) as ''{$ecommerce.report_day$}'', CAST(SUM(OrderTotalPrice) as decimal(38,2)) as ''{$ecommerce.report_income$}'' FROM COM_Order
WHERE @SelectedMonth = DATENAME(year,OrderDate) + '' '' + DATENAME(month,OrderDate) AND
OrderSiteID = @CMSContextCurrentSiteID
GROUP BY DATENAME(day,OrderDate)
ORDER BY MIN(OrderDate)', N'TableEcommSalesPerDay', '20080303 12:03:27', 155, 108, 'd8a0b63e-205a-4e6a-91e0-a2ce2da8934b', N'TableEcommSalesPerDay')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100
CustComp as ''{$ecommerce.report_customer_company$}'',
CustFirst as ''{$ecommerce.report_customer_firstname$}'',
CustLast as ''{$ecommerce.report_customer_lastname$}'',
CustEmail as ''{$ecommerce.report_customer$}'',
Income as ''{$ecommerce.report_income$}'' FROM (
SELECT CustomerEmail as CustEmail, CustomerCompany as CustComp, CustomerFirstName as CustFirst, CustomerLastName as CustLast, 
CAST(SUM(OrderTotalPrice) AS decimal(38,2)) as Income FROM COM_Order, COM_Customer
WHERE CustomerID = OrderCustomerID AND
OrderSiteID = @CMSContextCurrentSiteID
GROUP BY CustomerEmail, CustomerCompany, CustomerFirstName, CustomerLastName
) as tab1 ORDER BY Income DESC', N'TableEcommTop100CustByVolOfSales', '20080303 12:03:35', 156, 109, '6fe00b31-2adf-4d5e-8bf3-8820563ecfca', N'TableEcommTop100CustByVolOfSales')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_browsertype.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_browsertype.hits_header$}'', 
 CAST(CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
	WHERE (StatisticsSiteID=@CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) 
 AS VARCHAR)+''%'' AS ''{$reports_browsertype.percent_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableDayBrowserType', '20100219 10:53:19', 157, 110, '2aaf1e80-15a2-44c6-b0c9-d9b1a09efc24', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_browsertype.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_browsertype.hits_header$}'', 
 CAST(CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID=@CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS VARCHAR)+''%'' AS ''{$reports_browsertype.percent_header$}'' 
	FROM Analytics_Statistics
	INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectName 
	ORDER BY SUM(HitsCount) DESC', N'TableHourBrowserType', '20100219 11:55:44', 158, 111, '4873edaa-a541-4055-bfb9-99f47fa5a916', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_browsertype.name_header$}'',
 SUM(HitsCount) AS ''{$reports_browsertype.hits_header$}'', 
 CAST(CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((
	SELECT SUM(HitsCount) 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID=@CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS VARCHAR)+''%'' AS ''{$reports_browsertype.percent_header$}'' 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectName 
	ORDER BY SUM(HitsCount) DESC', N'TableMonthBrowserType', '20100219 12:02:18', 159, 112, '6ecb01ea-2939-4a26-8d14-922e1c15f60b', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 StatisticsObjectName AS ''{$reports_browsertype.name_header$}'', SUM(HitsCount) AS ''{$reports_browsertype.hits_header$}'', CAST(CAST(100*CAST(SUM(HitsCount) AS float)/NULLIF((SELECT SUM(HitsCount) FROM Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID=@CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)), 0) AS decimal(3)) AS VARCHAR)+''%'' AS ''{$reports_browsertype.percent_header$}'' FROM 
Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableYearBrowserType', '20080910 12:51:49', 160, 113, '98141f5c-a3dc-4137-b1c3-5ff017d53bda', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_countries.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_countries.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_Statistics.StatisticsID = Analytics_DayHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) 
 AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableDayCountries', '20100217 15:17:37', 161, 126, '0095a983-301d-4d44-b620-2bf501574349', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_countries.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_countries.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_Statistics.StatisticsID = Analytics_HourHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableHourCountries', '20100217 15:25:23', 162, 128, '181ec0f9-a70c-4048-a0df-f39d122fdd62', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectName AS ''{$reports_countries.name_header$}'', 
 SUM(HitsCount) AS ''{$reports_countries.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_Statistics.StatisticsID = Analytics_MonthHits.HitsStatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) 
 AND (HitsEndTime <= @ToDate) 
 GROUP BY StatisticsObjectName 
 ORDER BY SUM(HitsCount) DESC', N'TableMonthCountries', '20100217 15:32:34', 163, 129, 'e666ff48-ffdc-40f1-922b-356c177ae49e', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 StatisticsObjectName AS ''{$reports_countries.name_header$}'', SUM(HitsCount) AS ''{$reports_countries.hits_header$}'' FROM 
Analytics_Statistics, Analytics_YearHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectName ORDER BY SUM(HitsCount) DESC', N'TableYearCountries', '20080410 17:23:31', 164, 130, 'cbfb887e-752e-448a-a0bf-77cc756c86fd', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectID AS ''{$reports_registeredusers.UserID_header$}'', 
 StatisticsObjectName AS ''{$reports_registeredusers.UserName_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)', N'TableDayRegisteredUser', '20100217 10:58:11', 173, 147, '23ab891a-562d-4907-ac00-1c729a79ad60', N'Table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectID AS ''{$reports_registeredusers.UserID_header$}'', 
 StatisticsObjectName AS ''{$reports_registeredusers.UserName_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) 
 AND (HitsEndTime <= @ToDate)', N'TableMonthRegisteredUsers', '20100217 12:10:26', 174, 146, '9aced55d-b9a7-4f7c-a1a3-f51fa6a8bdce', N'Table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 StatisticsObjectID AS ''{$reports_registeredusers.UserID_header$}'', 
 StatisticsObjectName AS ''{$reports_registeredusers.UserName_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) 
 AND (HitsEndTime <= @ToDate)', N'TableHourRegisteredUsers', '20100217 12:05:21', 175, 148, '437029af-3429-4d59-93ab-0429fdb2267c', N'Table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 StatisticsObjectID AS ''{$reports_registeredusers.UserID_header$}'', StatisticsObjectName AS ''{$reports_registeredusers.UserName_header$}'' FROM Analytics_Statistics, Analytics_DayHits WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)', N'TableYearRegisteredUsers', '20080822 17:11:22', 176, 149, 'ab0b845a-f3ff-45fb-84dd-004c761fd1e5', N'Table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT NodeAliasPath AS ''Alias Path'', 
	DocumentCulture AS ''Language'', 
	DocumentName AS ''Document name'', 
	DocumentModifiedWhen AS ''Last modified'', 
	UserName AS ''Last modified by'',
	StepDisplayName AS ''Workflow step'',
	DocumentPublishFrom AS ''Publish from'',
	DocumentPublishTo AS ''Publish to''
FROM View_CMS_Tree_Joined
LEFT JOIN CMS_User ON DocumentModifiedByUserID = UserID
LEFT JOIN CMS_WorkFlowStep ON DocumentWorkflowStepID = StepID
WHERE (@OnlyPublished = 0 OR Published = @OnlyPublished) 
AND (NodeSiteID = @CMSContextCurrentSiteID)
AND (@ModifiedFrom IS NULL OR DocumentModifiedWhen >= @ModifiedFrom)
AND (@ModifiedTo IS NULL OR DocumentModifiedWhen < @ModifiedTo) 
AND (NodeAliasPath LIKE @path)
AND (@Language IS NULL OR @Language = ''-1'' OR DocumentCulture = @Language)
AND (@name IS NULL OR DocumentName LIKE ''%''+@name+''%'')
ORDER BY NodeAliasPath', N'InventoryTable', '20100323 15:18:53', 200, 186, '73ad09f4-db03-47ae-a09d-9b5ad4c76e11', N'InventoryTable')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100
 CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$reports_aggviews_hour.path_header$}'',
 SUM(HitsCount) AS ''{$reports_aggviews_hour.hits_header$}''
 FROM  Analytics_Statistics
 INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID)
 AND (StatisticsCode=@CodeName)
 AND (HitsStartTime >= @FromDate)
 AND (HitsEndTime <= @ToDate)
 GROUP BY DocumentNamePath, StatisticsObjectName
 ORDER BY ''{$reports_aggviews_hour.hits_header$}'' DESC', N'TableHourAggViews', '20100304 09:30:33', 201, 187, '5d1ae09c-51cd-4ecd-a49e-92d0cf105ea1', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$reports_aggviews_day.path_header$}'', 
 SUM(HitsCount) AS ''{$reports_aggviews_day.hits_header$}'' 
 FROM 
 Analytics_Statistics
 INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID =  Analytics_Statistics.StatisticsID
 LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID =  Analytics_Statistics.StatisticsObjectID AND  Analytics_Statistics.StatisticsObjectCulture =  View_CMS_Tree_Joined.DocumentCulture
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
 AND (StatisticsCode=@CodeName) 
 AND (HitsStartTime >= @FromDate) 
 AND (HitsEndTime <= @ToDate) 
 GROUP BY DocumentNamePath,  StatisticsObjectName
 ORDER BY ''{$reports_aggviews_day.hits_header$}'' DESC', N'TableDayAggViews', '20100304 09:33:42', 202, 188, 'd1e18337-1946-47e2-9cc4-ff7e2178a3e5', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 
 CASE 
	WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName
	ELSE DocumentNamePath 
 END AS ''{$reports_aggviews_month.path_header$}'', 
 SUM(HitsCount) AS ''{$reports_aggviews_month.hits_header$}'' 
 FROM Analytics_Statistics
 INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID 
 LEFT JOIN View_CMS_Tree_Joined ON Analytics_Statistics.StatisticsObjectID = View_CMS_Tree_Joined.NodeID AND Analytics_Statistics.StatisticsObjectCulture=View_CMS_Tree_Joined.DocumentCulture 
 WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate)
 AND (StatisticsCode=@CodeName) 
 GROUP BY DocumentNamePath, StatisticsObjectName
 ORDER BY ''{$reports_aggviews_month.hits_header$}'' DESC', N'TableMonthAggViews', '20100304 09:34:55', 203, 189, 'db2d3ab2-ef30-430a-9d17-9fe01abb383c', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'SELECT TOP 100 DocumentNamePath AS ''{$reports_aggviews_year.path_header$}'', SUM(HitsCount) AS ''{$reports_aggviews_year.hits_header$}'' FROM 
Analytics_Statistics, Analytics_YearHits, View_CMS_Tree_Joined WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (StatisticsObjectID = NodeID) AND (StatisticsObjectCulture = DocumentCulture) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY DocumentNamePath ORDER BY SUM(HitsCount) DESC', N'TableYearAggViews', '20100304 09:36:42', 204, 190, '464f3553-f5e5-48a9-a1e6-8d88a1427603', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'DECLARE @myselection TABLE (
   DocumentNamePath varchar(400),
   StatisticsObjectCulture varchar(400),
   Count float   
);
DECLARE @countedAll TABLE (
   Counted float   
);
INSERT INTO @myselection 
	SELECT TOP 100 
	CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END, 
	StatisticsObjectCulture , 
	SUM(HitsCount) AS Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture, DocumentNamePath, StatisticsObjectName 
	ORDER BY SUM(HitsCount) DESC
	
INSERT INTO @countedAll 
	SELECT
	SUM(HitsCount) AS Count 
	FROM Analytics_Statistics
	INNER JOIN Analytics_MonthHits ON Analytics_MonthHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
	LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 	
 SELECT DocumentNamePath AS ''{$reports_aggviews_month.path_header$}'', 
 StatisticsObjectCulture AS   ''{$general.culture$}'', 
 Count AS ''{$reports_aggviews_month.hits_header$}'', 
 CAST((100*Count)/(SELECT Counted FROM  @countedAll) as decimal(10,2)) as ''{$reports_aggviews_month.hits_percent_header$}'' 
 FROM @myselection', N'TableMonthAggViewsCulture', '20100324 10:47:48', 205, 191, '70c1bd35-c313-46d2-8ebc-e25d00f5eeac', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'DECLARE @myselection TABLE (
   DocumentNamePath varchar(400),
   StatisticsObjectCulture varchar(400),
   Count float   
);
DECLARE @countedAll TABLE (
   Counted float   
);
INSERT INTO @myselection 
	SELECT TOP 100 
	CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END AS ''{$reports_pageviews_day.path_header$}'',  
	StatisticsObjectCulture AS ''{$general.culture$}'', 
	SUM(HitsCount) AS ''{$reports_pageviews_day.hits_header$}'' 
	FROM Analytics_Statistics 
    INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
    LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
    WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture, DocumentNamePath, StatisticsObjectName
	ORDER BY SUM(HitsCount) DESC
	
INSERT INTO @countedAll 
	SELECT
	SUM(HitsCount) AS ''{$reports_pageviews_day.hits_header$}'' 
	FROM Analytics_Statistics 
    INNER JOIN Analytics_DayHits ON Analytics_DayHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
    LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND Analytics_Statistics.StatisticsObjectCulture = View_CMS_Tree_Joined.DocumentCulture
    WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 	
	
SELECT DocumentNamePath AS ''{$reports_aggviews_day.path_header$}'',  
StatisticsObjectCulture AS ''{$general.culture$}'', 
Count AS ''{$reports_aggviews_day.hits_header$}'',
CAST((100*Count)/(SELECT Counted FROM  @countedAll) as decimal(10,2)) 
as ''{$reports_aggviews_day.hits_percent_header$}'' 
FROM @myselection', N'TableDayAggViewsCulture', '20100324 10:52:38', 206, 192, '21078700-7c9b-464b-9039-a9846e13d8e3', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'DECLARE @myselection TABLE (
   DocumentNamePath varchar(400),
   StatisticsObjectCulture varchar(400),
   Count float   
);
DECLARE @countedAll TABLE (
   Counted float   
);
INSERT INTO @myselection 
	SELECT TOP 100 
	CASE WHEN DocumentNamePath LIKE '''' OR DocumentNamePath IS NULL THEN StatisticsObjectName ELSE DocumentNamePath END, 
	StatisticsObjectCulture, SUM(HitsCount) 
	FROM Analytics_Statistics
    INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
    LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 
	GROUP BY StatisticsObjectCulture, DocumentNamePath, StatisticsObjectName
	ORDER BY SUM(HitsCount) DESC, StatisticsObjectCulture
INSERT INTO @countedAll 
	SELECT
	SUM(HitsCount) 
	FROM Analytics_Statistics
    INNER JOIN Analytics_HourHits ON Analytics_HourHits.HitsStatisticsID = Analytics_Statistics.StatisticsID
    LEFT JOIN View_CMS_Tree_Joined ON View_CMS_Tree_Joined.NodeID = Analytics_Statistics.StatisticsObjectID AND View_CMS_Tree_Joined.DocumentCulture = Analytics_Statistics.StatisticsObjectCulture
	WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) 
	AND (StatisticsCode=@CodeName) 
	AND (HitsStartTime >= @FromDate) 
	AND (HitsEndTime <= @ToDate) 	
	
 SELECT DocumentNamePath AS ''{$reports_aggviews_hour.path_header$}'', 
 StatisticsObjectCulture AS ''{$general.culture$}'', 
 Count AS ''{$reports_aggviews_hour.hits_header$}'',
 CAST((100*Count)/(SELECT Counted FROM  @countedAll) as decimal(10,2)) as ''{$reports_aggviews_hour.hits_percent_header$}'' 
 FROM @myselection', N'TableHourFileAggViewsCulture', '20100324 10:56:00', 207, 193, '5518de48-41fd-4a57-9da9-47e7124497d6', N'table')
INSERT INTO [Reporting_ReportTable] ([TableQueryIsStoredProcedure], [TableQuery], [TableName], [TableLastModified], [TableID], [TableReportID], [TableGUID], [TableDisplayName]) VALUES (0, N'DECLARE @myselection TABLE (
   DocumentNamePath varchar(400),
   StatisticsObjectCulture varchar(400),
   Count float   
);
INSERT INTO @myselection SELECT TOP 100 DocumentNamePath, StatisticsObjectCulture, SUM(HitsCount) as Count FROM 
Analytics_Statistics, Analytics_YearHits, View_CMS_Tree_Joined WHERE (StatisticsSiteID = @CMSContextCurrentSiteID) AND (StatisticsCode=@CodeName) AND (StatisticsID = HitsStatisticsID) AND (StatisticsObjectID = NodeID) AND (StatisticsObjectCulture = DocumentCulture) AND (HitsStartTime >= @FromDate) AND (HitsEndTime <= @ToDate) GROUP BY StatisticsObjectCulture, DocumentNamePath ORDER BY SUM(HitsCount) DESC, StatisticsObjectCulture
SELECT DocumentNamePath AS ''{$reports_aggviews_year.path_header$}'', StatisticsObjectCulture AS ''{$general.culture$}'',
Count AS ''{$reports_aggviews_year.hits_header$}'',
CAST((100*Count)/(SELECT SUM(Count) FROM @myselection) as decimal(10,2)) AS ''{$reports_aggviews_year.hits_percent_header$}'' FROM @myselection', N'TableYearAggViewsCulture', '20100304 10:12:46', 208, 194, '4d861887-7695-4c86-9114-4894444dac8c', N'table')
SET IDENTITY_INSERT [Reporting_ReportTable] OFF
