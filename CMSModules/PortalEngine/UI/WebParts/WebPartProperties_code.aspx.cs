using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.FormEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_WebParts_WebPartProperties_code : CMSWebPartPropertiesPage
{
    #region "Variables"

    protected string mAliasPath = null;
    protected string mZoneId = null;
    protected string mWebpartId = null;

    /// <summary>
    /// Current page info
    /// </summary>
    PageInfo pi = null;

    /// <summary>
    /// Page template info
    /// </summary>
    PageTemplateInfo pti = null;


    /// <summary>
    /// Current web part
    /// </summary>
    WebPartInstance webPart = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Page alias path
    /// </summary>
    public string AliasPath
    {
        get
        {
            return mAliasPath;
        }
        set
        {
            mAliasPath = value;
        }
    }


    /// <summary>
    /// Zone ID
    /// </summary>
    public string ZoneId
    {
        get
        {
            return mZoneId;
        }
        set
        {
            mZoneId = value;
        }
    }


    /// <summary>
    /// Web part ID
    /// </summary>
    public string WebpartId
    {
        get
        {
            return mWebpartId;
        }
        set
        {
            mWebpartId = value;
        }
    }

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // Check permissions for web part properties UI
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        if (!currentUser.IsAuthorizedPerUIElement("CMS.Content", "WebPartProperties.Code"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "WebPartProperties.Code");
        }

        AliasPath = QueryHelper.GetString("aliaspath", "");
        WebpartId = QueryHelper.GetString("webpartid", "");
        ZoneId = QueryHelper.GetString("zoneid", "");
        Guid instanceGuid = QueryHelper.GetGuid("instanceguid", Guid.Empty);

        if ((WebpartId != "") && (AliasPath != ""))
        {
            // get pageinfo
            pi = PageInfoProvider.GetPageInfo(CMSContext.CurrentSiteName, AliasPath, CMSContext.PreferredCultureCode, null, CMSContext.CurrentSite.CombineWithDefaultCulture, null);
            if (pi == null)
            {
                this.Visible = false;
                return;
            }

            pti = pi.GetInheritedTemplateInfo(CMSContext.PreferredCultureCode, CMSContext.CurrentSite.CombineWithDefaultCulture);

            // Get web part
            webPart = pti.GetWebPart(instanceGuid, WebpartId);

            if (webPart != null)
            {
                txtCode.Text = ValidationHelper.GetString(webPart.GetValue("WebPartCode"), "");
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!SettingsKeyProvider.UsingVirtualPathProvider)
        {
            this.lblInfo.Text = ResHelper.GetString("WebPartCode.ProviderNotRunning");
            this.lblInfo.CssClass = "ErrorLabel";
            this.txtCode.Enabled = false;
        }
        else
        {
            this.lblInfo.Text = ResHelper.GetString("WebPartCode.Info");
            btnOnOK.Click += new EventHandler(btnOnOK_Click);
            btnOnApply.Click += new EventHandler(btnOnApply_Click);

            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ApplyButton", ScriptHelper.GetScript(
                "function SetRefresh(refreshpage) { document.getElementById('" + this.hidRefresh.ClientID + "').value = refreshpage; } \n" +
                "function OnApplyButton(refreshpage) { SetRefresh(refreshpage); " + Page.ClientScript.GetPostBackEventReference(btnOnApply, "") + "} \n" +
                "function OnOKButton(refreshpage) { SetRefresh(refreshpage); " + Page.ClientScript.GetPostBackEventReference(btnOnOK, "") + "} \n"
            ));
        }
    }


    /// <summary>
    /// Saves webpart properties.
    /// </summary>
    public void Save()
    {
        webPart.SetValue("WebPartCode", txtCode.Text);
        // Update page template
        PageTemplateInfoProvider.SetPageTemplateInfo(pti);

        string parameters = this.AliasPath + "/" + this.ZoneId + "/" + this.WebpartId;
        string cacheName = "CMSVirtualWebParts|" + parameters.ToLower().TrimStart('/');

        CacheHelper.Remove(cacheName);
    }


    /// <summary>
    /// Saves the webpart properties and closes the window.
    /// </summary>
    protected void btnOnOK_Click(object sender, EventArgs e)
    {
        // Save webpart properties
        Save();

        bool refresh = ValidationHelper.GetBoolean(this.hidRefresh.Value, false);

        string script = "";
        if (refresh)
        {
            script = "RefreshPage(); \n";
        }

        // Close the window
        ltlScript.Text += ScriptHelper.GetScript(script + "top.window.close();");
    }


    /// <summary>
    /// Saves the webpart properties
    /// </summary>
    protected void btnOnApply_Click(object sender, EventArgs e)
    {
        Save();
    }
}
