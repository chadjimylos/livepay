<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletter_Send.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_Send" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Tools - Newsletter send" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label ID="lblInfo" runat="server" EnableViewState="false" Visible="false" CssClass="InfoLabel" />
    <asp:Label ID="lblError" runat="server" EnableViewState="false" Visible="false" CssClass="ErrorLabel" />
    <table>
        <tr>
            <td>
                <asp:RadioButton ID="radSendNow" runat="server" GroupName="Send" AutoPostBack="true"
                    OnCheckedChanged="radGroupSend_CheckedChanged" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButton ID="radSendToSpecified" runat="server" GroupName="Send" AutoPostBack="true"
                    OnCheckedChanged="radGroupSend_CheckedChanged" />
                <div class="UnderRadioContent">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" EnableViewState="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtSendToSpecified" runat="server" Enabled="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="UnderRadioContent">
                    <cms:CMSButton ID="btnSend" runat="server" CssClass="SubmitButton" OnClick="btnSend_Click"
                        EnableViewState="false" />
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
