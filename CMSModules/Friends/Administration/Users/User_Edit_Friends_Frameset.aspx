<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User_Edit_Friends_Frameset.aspx.cs" Inherits="CMSModules_Friends_Administration_Users_User_Edit_Friends_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" enableviewstate="false">
    <title>Administration - User edit friends</title>
</head>
<frameset border="0" rows="72, *" id="rowsFrameset">
    <frame name="friendsMenu" src="User_Edit_Friends_Header.aspx<%=Request.Url.Query%>" frameborder="0" scrolling="no" noresize="noresize" />
    <frame name="friendsContent" src="User_Edit_Friends_Approved.aspx<%=Request.Url.Query%>" frameborder="0" />
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
</frameset>
</html>
