using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using CMS.UIControls;
using CMS.GlobalHelper;

public partial class CMSSiteManager_Administration_System_Debug_System_ViewObject : CMSDebugPage
{
    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set the title
        CurrentMaster.Title.TitleText = ResHelper.GetString("ViewObject.Title");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/__GLOBAL__/Object.png");
        Page.Title = ResHelper.GetString("ViewObject.Title");

        object obj = null;

        string source = QueryHelper.GetString("source", "");
        switch (source.ToLower())
        {
            case "cache":
                // Take the object from the cache
                string key = QueryHelper.GetString("key", "");
                CacheHelper.TryGetItem(key, out obj);
                break;
        }

        objElem.Object = obj;
    }

    #endregion
}
