using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.ExtendedControls;
using CMS.DataEngine;
using CMS.EmailEngine;
using CMS.SiteProvider;
using CMS.EventLog;
using CMS.URLRewritingEngine;
using CMS.MembershipProvider;
using CMS.PortalEngine;
using System.Collections.Specialized;

public partial class CMSWebParts_LivePay_Logon_MobileLogonform: CMSAbstractWebPart
{
    #region "Local variables"

    private TextBox user = null;
    private TextBox pass = null;
    private LocalizedButton login = null;
    private LocalizedLabel lblUserName = null;
    private LocalizedLabel lblPassword = null;
    private ImageButton loginImg = null;
    private RequiredFieldValidator rfv = null;
    private Panel container = null;
    private string mDefaultTargetUrl = string.Empty;
    private string mUserNameText = "login";
    private CMS.TreeEngine.TreeNode _DocumentTreeNode;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether image button is displayed instead of regular button
    /// </summary>
    public bool ShowImageButton
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("ShowImageButton"), false);
        }
        set
        {
            SetValue("ShowImageButton", value);
            login.Visible = !value;
            loginImg.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets an Image button URL
    /// </summary>
    public string ImageUrl
    {
        get
        {
            return ResolveUrl(ValidationHelper.GetString(GetValue("ImageUrl"), loginImg.ImageUrl));
        }
        set
        {
            SetValue("ImageUrl", value);
            loginImg.ImageUrl = value;
        }
    }


    /// <summary>
    /// Gets or sets the logon failure text
    /// </summary>
    public string FailureText
    {
        get
        {
            return ValidationHelper.GetString(GetValue("FailureText"), string.Empty);
        }
        set
        {
            if (!string.IsNullOrEmpty(value.Trim()))
            {
                SetValue("FailureText", value);
                loginElem.FailureText = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets the default target url (rediredction when the user is logged in)
    /// </summary>
    public string DefaultTargetUrl
    {
        get
        {
            return ValidationHelper.GetString(GetValue("DefaultTargetUrl"), mDefaultTargetUrl);
        }
        set
        {
            SetValue("DefaultTargetUrl", value);
            mDefaultTargetUrl = value;
        }
    }


    /// <summary>
    /// Gets or sets the username text
    /// </summary>
    public string UserNameText
    {
        get
        {
            return ValidationHelper.GetString(GetValue("UserNameText"), mUserNameText);
        }
        set
        {
            if (value.Trim() != string.Empty)
            {
                SetValue("UserNameText", value);
                mUserNameText = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets whether show error as popup window
    /// </summary>
    public bool ErrorAsPopup
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("ErrorAsPopup"), false);
        }
        set
        {
            SetValue("ErrorAsPopup", value);
        }
    }


    /// <summary>
    /// Gets or sets whether make login persistent
    /// </summary>
    public bool PersistentLogin
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("PersistentLogin"), false);
        }
        set
        {
            SetValue("PersistentLogin", value);
        }
    }

    #endregion


    #region "Overridden methods"

    /// <summary>
    /// Applies given stylesheet skin
    /// </summary>
    public override void ApplyStyleSheetSkin(Page page)
    {
        SetSkinID(SkinID);
        base.ApplyStyleSheetSkin(page);
    }


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }

    #endregion


    #region "SetupControl and SetSkinID"

    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            if ((PageManager.ViewMode == ViewModeEnum.Edit)) {
                mobileLogin.Style.Add("display", "block");
            }

            if (Request.QueryString["nodeid"] != string.Empty)
            {
                CMS.TreeEngine.TreeProvider tp = new CMS.TreeEngine.TreeProvider(CMS.CMSHelper.CMSContext.CurrentUser);
                CMS.TreeEngine.TreeNode node = tp.SelectSingleNode(Convert.ToInt32(Request.QueryString["nodeid"]));

                _DocumentTreeNode = node;
            }
            else
            {
                _DocumentTreeNode = CMSContext.CurrentDocument;
            }

            this.editableText.ID = "txtTitle_" + this.ID;
            this.txtEisodosMelus.ID = "txtEisodosMelus_" + this.ID;

            this.btnSynexeia.ServerClick += new EventHandler(LinkSynexeia_Click);

            // WAI validation
            lblUserName = (LocalizedLabel)loginElem.FindControl("lblUserName");
            if (lblUserName != null)
            {
                lblUserName.Text = ResHelper.GetString("general.username");
                //lblUserName.Attributes.Add("style", "display: none;");
            }
            lblPassword = (LocalizedLabel)loginElem.FindControl("lblPassword");
            if (lblPassword != null)
            {
                lblPassword.Text = ResHelper.GetString("general.password");
                //lblPassword.Attributes.Add("style", "display: none;");
            }

            // Set properties for validator
            rfv = (RequiredFieldValidator)loginElem.FindControl("rfvUserNameRequired");
            rfv.ErrorMessage = ResHelper.GetString("edituser.erroremptyusername");
            rfv.ToolTip = ResHelper.GetString("edituser.erroremptyusername");

            // Set failure text
            if (!string.IsNullOrEmpty(FailureText))
            {
                loginElem.FailureText = ResHelper.LocalizeString(FailureText);
            }
            else
            {
                loginElem.FailureText = ResHelper.GetString("Login_FailureText");
            }

            // Set visibility of buttons
            login = (LocalizedButton)loginElem.FindControl("btnLogon");
            if (login != null)
            {
                login.Visible = !ShowImageButton;
            }

            loginImg = (ImageButton)loginElem.FindControl("btnImageLogon");
            if (loginImg != null)
            {
                loginImg.Visible = ShowImageButton;
                loginImg.ImageUrl = ImageUrl;
                loginImg.Click += new ImageClickEventHandler(btnImageLogon_Click);
            }

            // Ensure display control as inline and is used right default button
            container = (Panel)loginElem.FindControl("pnlLogonMiniForm");
            if (container != null)
            {
                container.Attributes.Add("style", "display: inline;");
                if (ShowImageButton)
                {
                    if (loginImg != null)
                    {
                        container.DefaultButton = loginImg.ID;
                    }
                    else if (login != null)
                    {
                        container.DefaultButton = login.ID;
                    }
                }
            }

            if (!string.IsNullOrEmpty(UserNameText))
            {
                // Initialize javascript for focus and blur UserName textbox
                user = (TextBox)loginElem.FindControl("UserName");
                user.Attributes.Add("onfocus", "MLUserFocus('focus');");
                user.Attributes.Add("onblur", "MLUserFocus('blur');");
                string focusScript = "function MLUserFocus(type)" +
                                     "{" +
                                     "var userNameBox = document.getElementById('" + user.ClientID + "');" +
                                     "if(userNameBox.value == '" + UserNameText + "' && type == 'focus')" +
                                     "{userNameBox.value = '';}" +
                                     "else if (userNameBox.value == '' && type == 'blur')" +
                                     "{userNameBox.value = '" + UserNameText + "';}" +
                                     "}";

                ScriptHelper.RegisterClientScriptBlock(this, GetType(), "MLUserNameFocus",
                                                            ScriptHelper.GetScript(focusScript));
            }
            loginElem.LoggedIn += loginElem_LoggedIn;
            loginElem.LoggingIn += loginElem_LoggingIn;
            loginElem.LoginError += loginElem_LoginError;

            if (!RequestHelper.IsPostBack())
            {
                // Set SkinID properties
                if (!StandAlone && (PageCycle < PageCycleEnum.Initialized) && (ValidationHelper.GetString(Page.StyleSheetTheme, string.Empty) == string.Empty))
                {
                    SetSkinID(SkinID);
                }
            }
            if (string.IsNullOrEmpty(loginElem.UserName))
            {
                loginElem.UserName = UserNameText;
            }
        }
    }

    /// <summary>
    /// Sets SkinId to all controls in logon form
    /// </summary>
    void SetSkinID(string skinId)
    {
        if (skinId != string.Empty)
        {
            loginElem.SkinID = skinId;

            user = (TextBox)loginElem.FindControl("UserName");
            if (user != null)
            {
                user.SkinID = skinId;
            }

            pass = (TextBox)loginElem.FindControl("Password");
            if (pass != null)
            {
                pass.SkinID = skinId;
            }

            login = (LocalizedButton)loginElem.FindControl("btnLogon");
            if (login != null)
            {
                login.SkinID = skinId;
            }

            loginImg = (ImageButton)loginElem.FindControl("btnImageLogon");
            if (loginImg != null)
            {
                loginImg.SkinID = skinId;
            }
        }
    }

    #endregion


    #region "Logging handlers"

    /// <summary>
    /// Logged in handler
    /// </summary>
    void loginElem_LoggedIn(object sender, EventArgs e)
    {


        System.Reflection.FieldInfo rf = typeof(FormsAuthentication).GetField("_Timeout", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Static);
        rf.SetValue(null, 20);

        // Set view mode to live site after login to prevent bar with "Close preview mode"
        CMSContext.ViewMode = CMS.PortalEngine.ViewModeEnum.LiveSite;

        // Ensure response cookie
        CookieHelper.EnsureResponseCookie(FormsAuthentication.FormsCookieName);

        // Set cookie expiration
        if (loginElem.RememberMeSet)
        {
            CookieHelper.ChangeCookieExpiration(FormsAuthentication.FormsCookieName, DateTime.Now.AddYears(1), false);
        }
        else
        {
            // Extend the expiration of the authentication cookie if required
            if (!UserInfoProvider.UseSessionCookies && (HttpContext.Current != null) && (HttpContext.Current.Session != null))
            {
                CookieHelper.ChangeCookieExpiration(FormsAuthentication.FormsCookieName, DateTime.Now.AddMinutes(Session.Timeout), false);
            }
        }

        // Current username
        string userName = loginElem.UserName;

        // Check whether safe user name is required and if so get safe username
        if (RequestHelper.IsMixedAuthentication() && UserInfoProvider.UseSafeUserName)
        {
            userName = ValidationHelper.GetSafeUserName(this.loginElem.UserName, CMSContext.CurrentSiteName);
            FormsAuthentication.SetAuthCookie(userName, this.loginElem.RememberMeSet);
        }


        // Redirect user to the return url, or if is not defined redirect to the default target url
        if (ValidationHelper.GetString(Request.QueryString["ReturnURL"], "") != "")
        {
            UrlHelper.Redirect(ResolveUrl(ValidationHelper.GetString(Request.QueryString["ReturnURL"], "")));
        }
        else
        {
            if (DefaultTargetUrl != "")
            {

                DefaultTargetUrl = SetQueryString(DefaultTargetUrl);
                UrlHelper.Redirect(ResolveUrl(DefaultTargetUrl));
            }
            else
            {
                UrlHelper.Redirect(URLRewriter.CurrentURL);
            }
        }
    }

    string SetQueryString(string url) {
        //string url = "~/appiphone/payment.aspx";
        int loop1, loop2;
        // Load NameValueCollection object.
        NameValueCollection coll = Request.QueryString;
        // Get names of all keys into a string array.
        String[] arr1 = coll.AllKeys;
        for (loop1 = 0; loop1 < arr1.Length; loop1++)
        {
            //Response.Write("Key: " + Server.HtmlEncode(arr1[loop1]) + "<br>");
            String[] arr2 = coll.GetValues(arr1[loop1]);
            for (loop2 = 0; loop2 < arr2.Length; loop2++)
            {
                //Response.Write("Value " + loop2 + ": " + Server.HtmlEncode(arr2[loop2]) + "<br>");
                url = UrlHelper.AddParameterToUrl(url, Server.HtmlEncode(arr1[loop1]), Server.HtmlEncode(arr2[loop2]));
            }
        }

        return url;
    }


    void btnImageLogon_Click(object sender, ImageClickEventArgs e)
    {
        this.loginElem.Page.Validate("MiniLogin");

        if (!this.loginElem.Page.IsValid)
        {
            ShowLoginElem();
        }
    }

    private void ShowLoginElem() {
        //Eisodos.Visible = false;
        Eisodos.Style.Add("display", "none");
        mobileLogin.Style.Add("display", "block");
    }



    /// <summary>
    /// Logging in handler
    /// </summary>
    void loginElem_LoggingIn(object sender, LoginCancelEventArgs e)
    {
        // Ban IP addresses which are blocked for login
        if (!BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.Login))
        {
            e.Cancel = true;

            LocalizedLiteral failureLit = loginElem.FindControl("FailureText") as LocalizedLiteral;
            if (failureLit != null)
            {
                failureLit.Visible = true;
                failureLit.Text = ResHelper.GetString("banip.ipisbannedlogin");
            }
        }

        if (!Page.IsValid)
        {
            ShowLoginElem();
        }

        loginElem.RememberMeSet = PersistentLogin;
    }


    /// <summary>
    /// Login error handler
    /// </summary>
    protected void loginElem_LoginError(object sender, EventArgs e)
    {
        //Display the failure message in a client-side alert box
        if (ErrorAsPopup)
        {
            ScriptHelper.RegisterStartupScript(this, GetType(), "LoginError", ScriptHelper.GetScript("alert(" + ScriptHelper.GetString(loginElem.FailureText) + ");"));

            Label error = (Label)loginElem.FindControl("FailureText");
            error.Visible = false;
        }

        ShowLoginElem();
    }


    public void LinkSynexeia_Click(object sender, EventArgs e)
    {
        string url = "~/appiphone/payment.aspx";
        url = SetQueryString(url);
        url = UrlHelper.RemoveParameterFromUrl(url, "login");
        UrlHelper.Redirect(ResolveUrl(url));
    }

    public void btnPrev_Click(object sender, EventArgs e)
    {
        Eisodos.Visible = true;
        Eisodos.Style.Add("display", "block");
        mobileLogin.Style.Add("display", "none");
    }

    #endregion
}
