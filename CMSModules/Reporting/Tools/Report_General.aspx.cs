using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Reporting;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Reporting_Tools_Report_General : CMSReportingPage
{
    protected int reportId = 0;
    private ReportInfo ri = null;
    protected string mSave = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "Report General";

        attachmentList.AllowEdit = CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify");

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ReloadPage", ScriptHelper.GetScript("function ReloadPage() { \n" + Page.ClientScript.GetPostBackEventReference(btnHdnReload, null) + "}"));

        reportId = ValidationHelper.GetInteger(Request.QueryString["reportId"], 0);

        // control initializations				
        rfvReportDisplayName.ErrorMessage = ResHelper.GetString("Report_New.EmptyDisplayName");
        rfvReportName.ErrorMessage = ResHelper.GetString("Report_New.EmptyCodeName");

        lblReportDisplayName.Text = ResHelper.GetString("Report_New.DisplayNameLabel");
        lblReportName.Text = ResHelper.GetString("Report_New.NameLabel");
        lblReportCategory.Text = ResHelper.GetString("Report_General.CategoryLabel");
        lblLayout.Text = ResHelper.GetString("Report_General.LayoutLabel");
        lblGraphs.Text = ResHelper.GetString("Report_General.GraphsLabel");
        lblTables.Text = ResHelper.GetString("Report_General.TablesLabel");
        lblValues.Text = ResHelper.GetString("Report_General.TablesValues");
        lblReportAccess.Text = ResHelper.GetString("Report_General.ReportAccessLabel");

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>" +
            ScriptHelper.GetScript("function SaveDocument() { " + ClientScript.GetPostBackEventReference(lnkSave, null) + " } \n"
        ));

        imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        mSave = ResHelper.GetString("general.save");

        AttachmentTitle.TitleText = ResHelper.GetString("general.attachments");

        attachmentList.AllowPasteAttachments = true;
        attachmentList.ObjectID = reportId;
        attachmentList.ObjectType = ReportingObjectType.REPORT;
        attachmentList.Category = MetaFileInfoProvider.OBJECT_CATEGORY_LAYOUT;

        // Get report info
        ri = ReportInfoProvider.GetReportInfo(reportId);

        if (!RequestHelper.IsPostBack())
        {
            LoadData();
        }

        htmlTemplateBody.AutoDetectLanguage = false;
        htmlTemplateBody.DefaultLanguage = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        htmlTemplateBody.EditorAreaCSS = "";

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ReportingHTML", ScriptHelper.GetScript(" var reporting_htmlTemplateBody = '" + htmlTemplateBody.ClientID + "'"));

        // initialize item list controls
        ilGraphs.Report = ri;
        ilTables.Report = ri;
        ilValues.Report = ri;

        ilGraphs.EditUrl = "ReportGraph_Edit.aspx";
        ilTables.EditUrl = "ReportTable_Edit.aspx";
        ilValues.EditUrl = "ReportValue_Edit.aspx";

        ilGraphs.ItemType = ReportItemTypes.Graph;
        ilTables.ItemType = ReportItemTypes.Table;
        ilValues.ItemType = ReportItemTypes.Value;
    }


    /// <summary>
    /// Load data
    /// </summary>
    protected void LoadData()
    {
        if (ri == null)
        {
            return;
        }
        chkReportAccess.Checked = (ri.ReportAccess == ReportAccessEnum.All);
        txtReportDisplayName.Text = ri.ReportDisplayName;
        txtReportName.Text = ri.ReportName;
        htmlTemplateBody.ResolvedValue = ri.ReportLayout;

        selectCategory.Value = ri.ReportCategoryID;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
        {
            RedirectToAccessDenied("cms.reporting", "Modify");
        }
        string errorMessage = new Validator().NotEmpty(txtReportDisplayName.Text.Trim(), rfvReportDisplayName.ErrorMessage).NotEmpty(txtReportName.Text.Trim(), rfvReportName.ErrorMessage).Result;

        if ((errorMessage == "") && (!ValidationHelper.IsCodeName(txtReportName.Text.Trim())))
        {
            errorMessage = ResHelper.GetString("general.invalidcodename");
        }

        ReportAccessEnum reportAccess = ReportAccessEnum.All;

        if (!chkReportAccess.Checked)
        {
            reportAccess = ReportAccessEnum.Authenticated;
        }

        if (errorMessage == "")
        {
            ReportInfo reportInfo = ReportInfoProvider.GetReportInfo(reportId);
            ReportInfo nri = ReportInfoProvider.GetReportInfo(txtReportName.Text.Trim());

            //if report with given name already exists show error message
            if ((nri != null) && (nri.ReportID != reportInfo.ReportID))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Report_New.ReportAlreadyExists");
                return;
            }

            if (reportInfo != null)
            {
                reportInfo.ReportDisplayName = txtReportDisplayName.Text.Trim();
                reportInfo.ReportLayout = htmlTemplateBody.ResolvedValue;

                // If there was a change in report code name change codenames in layout
                if (reportInfo.ReportName != txtReportName.Text.Trim())
                {
                    // part of old macro
                    string oldValue = "?" + reportInfo.ReportName + ".";
                    string newValue = "?" + txtReportName.Text.Trim() + ".";

                    reportInfo.ReportLayout = reportInfo.ReportLayout.Replace(oldValue, newValue);

                    // Set updated text back to HTML editor
                    htmlTemplateBody.ResolvedValue = reportInfo.ReportLayout;
                }

                reportInfo.ReportName = txtReportName.Text.Trim();
                reportInfo.ReportAccess = reportAccess;
                reportInfo.ReportCategoryID = ValidationHelper.GetInteger(selectCategory.Value, reportInfo.ReportCategoryID);

                ReportInfoProvider.SetReportInfo(reportInfo);

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
