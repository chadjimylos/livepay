CREATE TABLE [COM_DepartmentTaxClass] (
		[DepartmentID]     int NOT NULL,
		[TaxClassID]       int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_DepartmentTaxClass]
	ADD
	CONSTRAINT [PK_COM_TaxClassDepartment]
	PRIMARY KEY
	([DepartmentID], [TaxClassID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_DepartmentTaxClass]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Department]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_TaxClassDepartment_DepartmentID_COM_Department]') IS NULL
BEGIN
		ALTER TABLE [COM_DepartmentTaxClass]
			ADD CONSTRAINT [FK_COM_TaxClassDepartment_DepartmentID_COM_Department]
			FOREIGN KEY ([DepartmentID]) REFERENCES [COM_Department] ([DepartmentID])
END
IF OBJECT_ID(N'[COM_DepartmentTaxClass]') IS NOT NULL
	AND OBJECT_ID(N'[COM_TaxClass]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_TaxClassDepartment_TaxClassID_COM_TaxClass]') IS NULL
BEGIN
		ALTER TABLE [COM_DepartmentTaxClass]
			ADD CONSTRAINT [FK_COM_TaxClassDepartment_TaxClassID_COM_TaxClass]
			FOREIGN KEY ([TaxClassID]) REFERENCES [COM_TaxClass] ([TaxClassID])
END
