CREATE TABLE [Community_Invitation] (
		[InvitationID]               int NOT NULL IDENTITY(1, 1),
		[InvitedUserID]              int NULL,
		[InvitedByUserID]            int NOT NULL,
		[InvitationGroupID]          int NULL,
		[InvitationCreated]          datetime NULL,
		[InvitationValidTo]          datetime NULL,
		[InvitationComment]          nvarchar(max) NULL,
		[InvitationGUID]             uniqueidentifier NOT NULL,
		[InvitationLastModified]     datetime NOT NULL,
		[InvitationUserEmail]        nvarchar(200) NULL
)
ON [PRIMARY]
ALTER TABLE [Community_Invitation]
	ADD
	CONSTRAINT [PK_Community_GroupInvitation]
	PRIMARY KEY
	([InvitationID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Community_Invitation]') IS NOT NULL
	AND OBJECT_ID(N'[Community_Group]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Community_GroupInvitation_InvitationGroupID_Community_Group]') IS NULL
BEGIN
		ALTER TABLE [Community_Invitation]
			ADD CONSTRAINT [FK_Community_GroupInvitation_InvitationGroupID_Community_Group]
			FOREIGN KEY ([InvitationGroupID]) REFERENCES [Community_Group] ([GroupID])
END
IF OBJECT_ID(N'[Community_Invitation]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Community_GroupInvitation_InvitedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Community_Invitation]
			ADD CONSTRAINT [FK_Community_GroupInvitation_InvitedByUserID_CMS_User]
			FOREIGN KEY ([InvitedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Community_Invitation]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Community_GroupInvitation_InvitedUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Community_Invitation]
			ADD CONSTRAINT [FK_Community_GroupInvitation_InvitedUserID_CMS_User]
			FOREIGN KEY ([InvitedUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_Community_Invitation_InvitationGroupID]
	ON [Community_Invitation] ([InvitationGroupID])
	ON [PRIMARY]
CREATE INDEX [IX_Community_Invitation_InvitedByUserID]
	ON [Community_Invitation] ([InvitedByUserID])
	ON [PRIMARY]
CREATE INDEX [IX_Community_Invitation_InvitedUserID]
	ON [Community_Invitation] ([InvitedUserID])
	ON [PRIMARY]
