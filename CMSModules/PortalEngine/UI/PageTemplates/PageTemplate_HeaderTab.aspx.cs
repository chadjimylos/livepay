using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_HeaderTab : CMSEditTemplatePage
{
    protected int templateId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblTemplateHeader.Text = ResHelper.GetString("pagetemplate_header.addheader");
        btnOk.Text = ResHelper.GetString("General.OK");

        // Get page template id from url
        templateId = ValidationHelper.GetInteger(Request.QueryString["templateid"], 0);

        if (!RequestHelper.IsPostBack())
        {
            // Load data to form
            PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(templateId);
            if (pti != null)
            {
                txtTemplateHeader.Text = pti.PageTemplateHeader;
            }
        }
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(templateId);
        if (pti != null)
        {
            pti.PageTemplateHeader = txtTemplateHeader.Text.Trim();
            try
            {
                // Save changes
                PageTemplateInfoProvider.SetPageTemplateInfo(pti);
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                lblInfo.Visible = true;
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                lblError.Visible = true;
            }
        }
    }
}
