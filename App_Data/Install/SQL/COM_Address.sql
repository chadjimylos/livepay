CREATE TABLE [COM_Address] (
		[AddressID]               int NOT NULL IDENTITY(1, 1),
		[AddressName]             nvarchar(200) NOT NULL,
		[AddressLine1]            nvarchar(100) NOT NULL,
		[AddressLine2]            nvarchar(100) NOT NULL,
		[AddressCity]             nvarchar(100) NOT NULL,
		[AddressZip]              nvarchar(20) NOT NULL,
		[AddressPhone]            nvarchar(100) NULL,
		[AddressCustomerID]       int NOT NULL,
		[AddressCountryID]        int NOT NULL,
		[AddressStateID]          int NULL,
		[AddressIsBilling]        bit NOT NULL,
		[AddressEnabled]          bit NOT NULL,
		[AddressPersonalName]     nvarchar(200) NOT NULL,
		[AddressIsShipping]       bit NOT NULL,
		[AddressIsCompany]        bit NULL,
		[AddressGUID]             uniqueidentifier NOT NULL,
		[AddressLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_Address]
	ADD
	CONSTRAINT [PK_COM_CustomerAdress]
	PRIMARY KEY
	([AddressID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_Address]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Country]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Address_AddressCountryID_CMS_Country]') IS NULL
BEGIN
		ALTER TABLE [COM_Address]
			ADD CONSTRAINT [FK_COM_Address_AddressCountryID_CMS_Country]
			FOREIGN KEY ([AddressCountryID]) REFERENCES [CMS_Country] ([CountryID])
END
IF OBJECT_ID(N'[COM_Address]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Address_AddressCustomerID_COM_Customer]') IS NULL
BEGIN
		ALTER TABLE [COM_Address]
			ADD CONSTRAINT [FK_COM_Address_AddressCustomerID_COM_Customer]
			FOREIGN KEY ([AddressCustomerID]) REFERENCES [COM_Customer] ([CustomerID])
END
IF OBJECT_ID(N'[COM_Address]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_State]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Address_AddressStateID_CMS_State]') IS NULL
BEGIN
		ALTER TABLE [COM_Address]
			ADD CONSTRAINT [FK_COM_Address_AddressStateID_CMS_State]
			FOREIGN KEY ([AddressStateID]) REFERENCES [CMS_State] ([StateID])
END
CREATE INDEX [IX_COM_Address_AddressCountryID]
	ON [COM_Address] ([AddressCountryID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Address_AddressCustomerID]
	ON [COM_Address] ([AddressCustomerID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Address_AddressEnabled_AddressIsBilling_AddressIsShipping_AddressIsCompany]
	ON [COM_Address] ([AddressEnabled], [AddressIsBilling], [AddressIsShipping], [AddressIsCompany])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_Address_AddressStateID]
	ON [COM_Address] ([AddressStateID])
	ON [PRIMARY]
