﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSavedTransactions.ascx.vb" Inherits="CMSTemplates_LivePay_UCSavedTransactions" %>
<%@ Register Src="~/CMSTemplates/livepay/PopUp/UCReceipt.ascx" TagName="Receipt" TagPrefix="uc"  %>
<script src="../../CMSScripts/LivePay/LivePay.js" type="text/javascript"></script>
<script src="../../CMSScripts/LivePay/RegisteredUsers.js" type="text/javascript"></script>

<script language="javascript" type="text/javascript" >
    function ShowReceipt() {
        $.blockUI({ css: { border: '0px', backgroundColor: 'transparent', cursor: 'default' }, message: $('#ReceiptPopUp') });
    }
</script>
<style type="text/css">
div.growlUI { background: url(check48.png) no-repeat 10px 10px;width:200px }
div.growlUI h1, div.growlUI h2 {font-size:12px;color: white;text-align: left;width:200px}
.test
{
    display:none;
}
</style>
<asp:UpdatePanel ID="updtPanel" runat="server" UpdateMode="Always" >
<ContentTemplate>
<div id="ReceiptPopUp" style="display:none">
        <uc:Receipt ID="Receipt" runat="server" />
</div>
    <div class="SvdTransTitle"><%=ResHelper.LocalizeString("{$=Επιλέξτε την πληρωμή που θέλετε να πραγματοποιήσετε και πάλι από το κουμπί στα αριστερα|en-us=Επιλέξτε την πληρωμή που θέλετε να πραγματοποιήσετε και πάλι από το κουμπί στα αριστερα$}") %></div>
    <div class="SvdTransMain" style="width:670px;">
            <div class="SvdTransDarkBlueBGTitle">
                <div class="SvdTransTopTitle"><%=ResHelper.LocalizeString("{$=Αγαπημένες Συναλλαγές|en-us=Αγαπημένες Συναλλαγές$}") %></div>
                <div class="SvdTransContentBG">
                    <div class="SvdTransGridDiv">
                    <asp:GridView ID="GridViewTrans"  GridLines="None" runat="server" PageSize="8" AutoGenerateColumns="false" AllowPaging="true" Width="642px">
                    <HeaderStyle   CssClass="SvdTransGridHeader" />
                    <AlternatingRowStyle Height="35px" ForeColor="#094595" Font-Size="12px" Font-Names="tahoma" />
                    <RowStyle Height="35px"  ForeColor="#094595" Font-Size="12px" Font-Names="tahoma" />
                        <Columns >
                            <asp:TemplateField HeaderText="Επιλογή<br>Πληρωμής" HeaderStyle-Width="128px"  >
                                <ItemTemplate>
                                    <asp:Label style="display:none" ID="lblID" runat="server" Text='<%#Eval("MerchantID") %>'></asp:Label>
                                    <asp:Label style="display:none" ID="lblMoney" runat="server" Text='<%#Eval("TransactionAmount") %>'></asp:Label>
                                    <asp:Label style="display:none" ID="lblNodeAliasPath" runat="server" Text='<%#Eval("NodeAliasPath") %>'></asp:Label>
                                    
                                    <asp:RadioButton ID='GridRbSelectPay' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Πληρωμή προς" HeaderStyle-Width="176px"  >
                                <ItemTemplate>
                                    <asp:label ID="lblPayTo"  runat="server" Text='<%#Eval("CompanyName") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ποσό" HeaderStyle-Width="108px"  >
                                <ItemTemplate>
                                     <asp:label ID="lblPrice"  runat="server" Text='<%#Eval("TransactionAmount") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ημερομηνία<br>Πληρωμής" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="80px" >
                                <ItemTemplate>
                                    <asp:label ID="lbldtm" runat="server" Text='<%#Eval("TransactionDate") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="&nbsp;" ItemStyle-HorizontalAlign="Left"  HeaderStyle-Width="50px" >
                                <ItemTemplate>
                                   <div style="float:left;"><asp:LinkButton ID="openPopup" runat="server" OnClick="openPopup_Click" CommandArgument='<%#Eval("MerchantTransactionID") %>'><img border="0" src="/app_themes/LivePay/InfoBtn.png" /></asp:LinkButton></div>
                                   <div style="padding-top:1px;padding-left:2px;float:left;">
                                   <asp:LinkButton ID="lnkDelete" runat="server" OnClick="Delete_Click" CommandArgument='<%#Eval("MerchantTransactionID") %>'><img border="0" src="/app_themes/LivePay/SavedTrans/btnDelete.png" /></asp:LinkButton></div> 
                                   <div class="Clear"></div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        <EmptyDataTemplate >
                            <div class="" >
                                <div style="padding-top:5px;padding-left:10px"><%= ResHelper.LocalizeString("{$=Δεν υπάρχουν καταχωρημένες συναλλαγές στο προφίλ σας|en-us=There are no saved transactions in your profile$}")%></div>
                            </div>
                        </EmptyDataTemplate>
                        <PagerTemplate >
                        <div class="SvdTransPagerSpace" >
                            <div class="PagerLeftBorder"><img src="/app_themes/LivePay/SavedTrans/SvdTransBorderForm.png"</div>
                            <div class="SvdTransPagerSpaceBot" >
                                <div class="PagerPrev"><asp:linkbutton ID="lnkPrev" CssClass="PagerText" onclick="ChangePageByLinkNumber_Before" runat="server"><%=ResHelper.LocalizeString("{$=Προηγούμενη|en-us=Προηγούμενη$}") %></asp:linkbutton>&nbsp;</div>
                                <div class="PagerCenterSep" id="lblPageIndex" runat="server">&nbsp;</div>
                                <div class="PagerNext">&nbsp;<asp:linkbutton ID="lnkNext" CssClass="PagerText"  onclick="ChangePageByLinkNumber_Next" runat="server"><%=ResHelper.LocalizeString("{$=Επόμενη|en-us=Επόμενη$}") %></asp:linkbutton></div>
                                <div class="PagerComplete"><asp:ImageButton ID="btnCompletePay" OnClick="btnCompletePayClick"  runat="server" ImageUrl="/app_themes/LivePay/SavedTrans/btnCompletePay.png" /></div>
                            </div>
                            <div>&nbsp;</div>
                       </div>
                         </PagerTemplate>
                    </asp:GridView>
                </div>
               
              </div>
                
                <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
                <asp:LinkButton ID="HiddenPageBtn" style="display:none" runat="server" Text="0"></asp:LinkButton>
                <asp:TextBox ID="HiddenPage"  style="display:none" runat="server" Text="1"></asp:TextBox>
                <asp:TextBox ID="HiddenGridSelectedRow"  style="display:none" runat="server" Text="0"></asp:TextBox>
                <asp:TextBox ID="HiddenGridPrice"  style="display:none" runat="server" Text="0"></asp:TextBox>
                <asp:TextBox ID="HiddenGridSelectedRowAliasURL"  style="display:none" runat="server" Text="0"></asp:TextBox>
                <div id="BottomDiv" style="display:none" runat="server" class="SrcHisPayGridBGBottom"></div> 
            </div>
            
       </div>
</ContentTemplate>
</asp:UpdatePanel>

       <script language ="javascript" type="text/javascript" >
          
           function GoToPage(no) {
               document.getElementById('<%=HiddenPage.ClientID %>').value = no;
               document.getElementById('<%=HiddenPageBtn.ClientID %>').innerHTML = no;
               var GetID = '<%=HiddenPageBtn.ClientID %>'
               GetID = GetID.replace(/[_]/gi, "$")
               __doPostBack(GetID, '')
           }

           function GridRBSelect(obj, ID,Price,AliasPath) {
              
               var GetObj = obj
               ClearAllRadioButtons(GetObj)
               document.getElementById('<%=HiddenGridSelectedRow.ClientID %>').value = ID
               document.getElementById('<%=HiddenGridPrice.ClientID %>').value = Price
               document.getElementById('<%=HiddenGridSelectedRowAliasURL.ClientID %>').value = AliasPath 
           }

           function ClearAllRadioButtons(Choice) {
               var AllSelect = document.getElementsByTagName('input');
              
               for (i = 0; i < AllSelect.length; i++) {
                   if (AllSelect[i].id.indexOf("GridRbSelectPay") > -1 && AllSelect[i].id != Choice) {
                       AllSelect[i].checked = false
                   }
                   if (AllSelect[i].id == Choice) {
                       AllSelect[i].checked = true
                   }
               }
           }

           function CheckGridValues() {
               var RowID = document.getElementById('<%=HiddenGridSelectedRow.ClientID %>').value
               var GetPriceValue = document.getElementById('<%=HiddenGridPrice.ClientID %>').value
               if (RowID == '0') {
                   alert('<%=ResHelper.LocalizeString("{$=Παρακαλώ επιλέξτε συναλλαγή|en-us=Παρακαλώ επιλέξτε συναλλαγή$}") %>')
                   return false
               }
           }

           
       </script>