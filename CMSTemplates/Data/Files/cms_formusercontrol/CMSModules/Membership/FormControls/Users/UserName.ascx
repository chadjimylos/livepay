<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserName.ascx.cs" Inherits="CMSModules_Membership_FormControls_Users_UserName" %>
<asp:TextBox runat="server" ID="txtUserName" MaxLength="100" CssClass="TextBoxField" />
<asp:RequiredFieldValidator ID="RequiredFieldValidatorUserName" runat="server" EnableViewState="false"
    Display="dynamic" ControlToValidate="txtUserName" />
<cms:LocalizedLabel ID="lblUserName" AssociatedControlID="txtUserName" EnableViewState="false"
    ResourceString="general.username" Display="false" runat="server" />