﻿Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Data

Public Class LivePayMerchant
    Private _livePayID As Integer
    Private _NodeID As Integer
    Private _DiscreetTitle As String
    Private _Barcode As Boolean
    Private _OCR As Boolean
    Private _CategoryID As Integer
    Private _CustomFields As List(Of LivePayMerchantCustomField)

    Public Property livePayID() As Integer
        Get
            Return _livePayID
        End Get
        Set(ByVal value As Integer)
            _livePayID = value
        End Set
    End Property

    Public Property NodeID() As Integer
        Get
            Return _NodeID
        End Get
        Set(ByVal value As Integer)
            _NodeID = value
        End Set
    End Property

    Public Property DiscreetTitle() As String
        Get
            Return _DiscreetTitle
        End Get
        Set(ByVal value As String)
            _DiscreetTitle = value
        End Set
    End Property

    Public Property Barcode() As Boolean
        Get
            Return _Barcode
        End Get
        Set(ByVal value As Boolean)
            _Barcode = value
        End Set
    End Property

    Public Property OCR() As Boolean
        Get
            Return _OCR
        End Get
        Set(ByVal value As Boolean)
            _OCR = value
        End Set
    End Property

    Public Property CategoryID() As Integer
        Get
            Return _CategoryID
        End Get
        Set(ByVal value As Integer)
            _CategoryID = value
        End Set
    End Property

    Public Property CustomFields() As List(Of LivePayMerchantCustomField)
        Get
            Return _CustomFields
        End Get
        Set(ByVal value As List(Of LivePayMerchantCustomField))
            _CustomFields = value
        End Set
    End Property

    Public Sub New()
        'Dim cf As New LivePayMerchantCustomField(1)

    End Sub

    Public Sub New(ByVal livepayID As Integer, ByVal nodeID As Integer, ByVal discreetTitle As String, ByVal barcode As Boolean, ByVal ocr As Boolean, ByVal categoryID As Integer)
        _livePayID = livepayID
        _NodeID = nodeID
        _DiscreetTitle = discreetTitle
        _Barcode = barcode
        _OCR = ocr
        _CategoryID = categoryID

        Dim cf As New List(Of LivePayMerchantCustomField)
        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(_NodeID)

        If (Not DataHelper.DataSourceIsEmpty(ds)) Then
            Dim dt As DataTable = ds.Tables(0)
            For Each row As DataRow In dt.Rows
                cf.Add(New LivePayMerchantCustomField(row("ItemID"), "<![CDATA[" & row("NameFieldGr").ToString() & "]]>", row("FieldType")))
            Next
        End If
        _CustomFields = cf

    End Sub

End Class

Public Class LivePayCategory

    Private _CategoryID As Integer
    Private _CategoryName As String
    Private _SubCategories As List(Of LivePayCategory)

    Public Property CategoryID() As Integer
        Get
            Return _CategoryID
        End Get
        Set(ByVal value As Integer)
            _CategoryID = value
        End Set
    End Property

    Public Property CategoryName() As String
        Get
            Return _CategoryName
        End Get
        Set(ByVal value As String)
            _CategoryName = value
        End Set
    End Property

    Public Property SubCategories() As List(Of LivePayCategory)
        Get
            Return _SubCategories
        End Get
        Set(ByVal value As List(Of LivePayCategory))
            _SubCategories = value
        End Set
    End Property

    Public Sub New()
    End Sub
    Public Sub New(ByVal categoryID As Integer)
        Dim node As CMS.TreeEngine.TreeNode = TreeHelper.SelectSingleNode(categoryID)
        _CategoryID = node.NodeID
        _CategoryName = "<![CDATA[" & node.NodeName & "]]>"

        If (node.NodeParentID > 0 AndAlso node.NodeClassName.ToLower = "cms.menuitem") Then
            Dim ds As DataSet = TreeHelper.GetDocuments(CMSContext.CurrentSiteName, "/διαθεσιμες-πληρωμες/%", "el-gr", False, "cms.menuitem", "nodeParentID = " & node.NodeID, "NodeOrder", node.NodeLevel, True, -1)

            If (Not DataHelper.DataSourceIsEmpty(ds)) Then
                For Each row As DataRow In ds.Tables(0).Rows
                    'HttpContext.Current.Response.Write(" DocumentNodeID->" & row("DocumentNodeID").ToString() & vbCrLf)
                    'HttpContext.Current.Response.Write(" NodeLevel->" & row("NodeLevel").ToString() & vbCrLf)
                    'HttpContext.Current.Response.Write(" nodeParentID->" & row("nodeParentID").ToString() & vbCrLf & vbCrLf)
                    If _SubCategories Is Nothing Then _SubCategories = New List(Of LivePayCategory)
                    _SubCategories.Add(New LivePayCategory(row("DocumentNodeID")))
                Next
            End If
        End If
    End Sub

End Class

Public Class LivePayMerchantCustomField
    Private _ID As Integer
    Private _Name As String
    Private _Type As String

    Public Property ID() As String
        Get
            Return _ID
        End Get
        Set(ByVal value As String)
            _ID = value
        End Set
    End Property

    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)
            _Name = value
        End Set
    End Property

    Public Property Type() As String
        Get
            Return _Type
        End Get
        Set(ByVal value As String)
            _Type = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal ItemID As Integer, ByVal NameFieldGR As String, ByVal FieldTypeID As String)
        _ID = ItemID
        _Name = NameFieldGR
        _Type = GetFieldType(FieldTypeID)
    End Sub

    Private Function GetFieldType(ByVal FormControlID As Integer) As String
        Dim customTableClassName As String = "LivePay.FormControls"
        Dim Type As String = String.Empty

        ' Get data class using custom table name
        Dim customTableClassInfo As CMS.SettingsProvider.DataClassInfo = CMS.SettingsProvider.DataClassInfoProvider.GetDataClass(customTableClassName)
        If customTableClassInfo Is Nothing Then
            Throw New Exception("Given custom table does not exist.")
        End If

        ' Initialize custom table item provider with current user info and general connection
        Dim ctiProvider As New CMS.SiteProvider.CustomTableItemProvider(CMSContext.CurrentUser, CMS.DataEngine.ConnectionHelper.GetConnection())

        ' Get custom table items
        Dim dsItems As System.Data.DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, "ItemID = " & FormControlID, Nothing, -1, "ItemName")

        ' Check if DataSet is not empty
        If Not DataHelper.DataSourceIsEmpty(dsItems) Then
            ' Handle the retrieved data
            Type = dsItems.Tables(0).Rows(0)("ItemName").ToString()
        End If

        Return Type
    End Function
End Class