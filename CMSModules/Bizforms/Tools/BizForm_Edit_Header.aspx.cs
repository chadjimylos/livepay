using System;

using CMS.GlobalHelper;
using CMS.FormEngine;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_BizForms_Tools_BizForm_Edit_Header : CMSBizFormPage
{

    private string bizFormName = String.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {

        int formId = QueryHelper.GetInteger("formid", 0);   // BizForm ID
        if (formId > 0)
        {
            BizFormInfo bizForm = BizFormInfoProvider.GetBizFormInfo(formId);
            if (bizForm != null)
            {
                bizFormName = bizForm.FormDisplayName;
            }
        }

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu(formId);
        }

        // Initializes page title
        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("BizForm.BizForms");
        breadcrumbs[0, 1] = "~/CMSModules/BizForms/Tools/BizForm_List.aspx";
        breadcrumbs[0, 2] = "_parent";
        breadcrumbs[1, 0] = bizFormName;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("BizFormHeader.Title");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Form/object.png");

        if (CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "ReadData"))
        {
            this.CurrentMaster.Title.HelpTopicName = "data_tab";
        }
        else
        {
            this.CurrentMaster.Title.HelpTopicName = "general_tab";
        }

        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Initializes edit menu
    /// </summary>
    protected void InitalizeMenu(int formId)
    {
        // Learn if new form was created
        bool newForm = QueryHelper.GetBoolean("newform", false);

        string[,] tabs = new string[8, 4];

        int indx = 0;
        // Check 'ReadData' permission
        if (CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "ReadData"))
        {
            tabs[indx, 0] = ResHelper.GetString("BizForm.Data");
            tabs[indx, 1] = ""; //"SetHelpTopic('helpTopic', 'data_tab');";
            tabs[indx, 2] = "BizForm_Edit_Data.aspx?formid=" + formId;
            indx++;
        }
        
        // Check 'ReadForm' permission
        if (CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "ReadForm") && !((bizFormName == "ContactForm") && CMSContext.CurrentUser.IsInRole("MerchantsSupport", "LivePay")))
        {
            tabs[indx, 0] = ResHelper.GetString("general.general");
            tabs[indx, 1] = ""; //"SetHelpTopic('helpTopic', 'general_tab');";
            tabs[indx, 2] = "BizForm_Edit_General.aspx?formid=" + formId;
            indx++;
            tabs[indx, 0] = ResHelper.GetString("general.fields");
            tabs[indx, 1] = ""; // "SetHelpTopic('helpTopic', 'fields_tab');";
            tabs[indx, 2] = "BizForm_Edit_Fields.aspx?formid=" + formId;
            indx++;
            tabs[indx, 0] = ResHelper.GetString("general.form");
            tabs[indx, 1] = ""; // "SetHelpTopic('helpTopic', 'form_tab2');";
            tabs[indx, 2] = "BizForm_Edit_Layout.aspx?formid=" + formId;
            indx++;
            tabs[indx, 0] = ResHelper.GetString("biz.notificationemail");
            tabs[indx, 1] = ""; // "SetHelpTopic('helpTopic', 'notification_mail_tab');";
            tabs[indx, 2] = "BizForm_Edit_NotificationEmail.aspx?formid=" + formId;
            indx++;
            tabs[indx, 0] = ResHelper.GetString("biz.autoresponder");
            tabs[indx, 1] = ""; // "SetHelpTopic('helpTopic', 'autoresponder_tab');";
            tabs[indx, 2] = "BizForm_Edit_Autoresponder.aspx?formid=" + formId;
            indx++;
            tabs[indx, 0] = ResHelper.GetString("general.security");
            tabs[indx, 1] = ""; // "SetHelpTopic('helpTopic', 'security_tab2');";
            tabs[indx, 2] = "BizForm_Edit_Security.aspx?formid=" + formId;
            indx++;
            tabs[indx, 0] = ResHelper.GetString("biz.header.altforms");
            tabs[indx, 1] = ""; // "SetHelpTopic('helpTopic', 'alternative_forms_tab');";
            tabs[indx, 2] = "AlternativeForms/AlternativeForms_List.aspx?formid=" + formId;
        }

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "BizFormContent";
        // Select second (General) tab if new form was created
        if (newForm)
        {
            this.CurrentMaster.Tabs.SelectedTab = 1;
        }
    }
}
