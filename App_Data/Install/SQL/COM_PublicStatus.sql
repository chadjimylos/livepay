CREATE TABLE [COM_PublicStatus] (
		[PublicStatusID]               int NOT NULL IDENTITY(1, 1),
		[PublicStatusName]             nvarchar(200) NOT NULL,
		[PublicStatusDisplayName]      nvarchar(200) NOT NULL,
		[PublicStatusEnabled]          bit NOT NULL,
		[PublicStatusGUID]             uniqueidentifier NOT NULL,
		[PublicStatusLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_PublicStatus]
	ADD
	CONSTRAINT [PK_COM_PublicStatus]
	PRIMARY KEY
	NONCLUSTERED
	([PublicStatusID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_PublicStatus_PublicStatusDisplayName_PublicStatusEnabled]
	ON [COM_PublicStatus] ([PublicStatusDisplayName], [PublicStatusEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
