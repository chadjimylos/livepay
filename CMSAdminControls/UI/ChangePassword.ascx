<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.ascx.cs"
    Inherits="CMSAdminControls_UI_ChangePassword" %>
<asp:Panel ID="pnlChangePassword" runat="server" DefaultButton="btnOK">
    <div>
        <asp:Label runat="server" ID="lblInfo" EnableViewState="false" />
        <asp:Label runat="server" ID="lblError" EnableViewState="false" ForeColor="Red" />
    </div>
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblExistingPassword" AssociatedControlID="txtExistingPassword"
                    EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtExistingPassword" runat="server" TextMode="Password" MaxLength="100"
                    CssClass="TextBoxField" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblPassword1" AssociatedControlID="txtPassword1" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtPassword1" runat="server" TextMode="Password" MaxLength="100"
                    CssClass="TextBoxField" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblPassword2" AssociatedControlID="txtPassword2" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtPassword2" runat="server" TextMode="Password" MaxLength="100"
                    CssClass="TextBoxField" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="LongSubmitButton" />
            </td>
        </tr>
    </table>
</asp:Panel>
