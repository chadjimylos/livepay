using System;
using System.Text;
using System.Web;
using System.Web.UI;

using CMS.GlobalHelper;
using CMS.Forums;

public partial class CMSModules_Forums_Controls_ForumBreadcrumbs : ForumViewer
{
    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether forum group should be displayed in breadcrumbs
    /// </summary>
    public bool DisplayGroup
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["DisplayGroup"], true);
        }
        set
        {
            ViewState["DisplayGroup"] = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether threads names should be displayed in breadcrumbs
    /// </summary>
    public bool DisplayThreads
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["DisplayThreads"], true);
        }
        set
        {
            ViewState["DisplayThreads"] = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether thread name should be displayed in breadcrumbs
    /// </summary>
    public bool DisplayThread
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["DisplayThread"], true);
        }
        set
        {
            ViewState["DisplayThread"] = value;
        }
    }


    /// <summary>
    /// Gets or sets the breadcrumbs separator
    /// </summary>
    public string BreadcrumbSeparator
    {
        get
        {
            return ValidationHelper.GetString(ViewState["BreadcrumbsSeparator"], "&nbsp;&gt;&nbsp;");
        }
        set
        {
            ViewState["BreadcrumbsSeparator"] = HTMLHelper.HTMLEncode(value);
        }
    }


    /// <summary>
    /// Gets or sets the breadcrumbs prefix which should be displayed before breadcrumbs items
    /// </summary>
    public string BreadcrumbPrefix
    {
        get
        {
            return ValidationHelper.GetString(ViewState["BreadcrumbPrefix"], "");
        }
        set
        {
            ViewState["BreadcrumbPrefix"] = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether current item should be rendered as link
    /// </summary>
    public bool UseLinkForCurrentItem
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["UseLinkForCurrentItem"], false);
        }
        set
        {
            ViewState["UseLinkForCurrentItem"] = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether breadcrumbs should be hidden on forum group page
    /// This option hides only forum breadcrumbs, breadcrumbs prefix is allways visible if is defined
    /// </summary>
    public bool HideBreadcrumbsOnForumGroupPage
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["HideBreadcrumbsOnForumGroupPage"], true);
        }
        set
        {
            ViewState["HideBreadcrumbsOnForumGroupPage"] = value;
        }
    }


    #endregion


    /// <summary>
    /// Generates breadcrumbs 
    /// </summary>
    public virtual string GenerateBreadcrumbs()
    {
        // Create string builder object
        StringBuilder result = new StringBuilder();

        bool separator = false;

        // Check whether breadcrumbs prefix is defined, if so generate it
        if (!String.IsNullOrEmpty(this.BreadcrumbPrefix))
        {
            result.Append(this.BreadcrumbPrefix);
            separator = true;
        }

        // Check hide breadcrumbs on forum page 
        if ((ForumContext.CurrentForum == null) && (HideBreadcrumbsOnForumGroupPage))
        {
            return result.ToString();
        }

        // Check whether group exists and if should be displayed
        if ((this.DisplayGroup) && (ForumContext.CurrentGroup != null))
        {
            if (separator)
            {
                result.Append(this.BreadcrumbSeparator);
            }
            string grUrl = URLCreator("", "", false, null, ForumActionType.ForumGroup);
            grUrl = UrlHelper.RemoveParameterFromUrl(grUrl, "forumId");
            grUrl = UrlHelper.RemoveParameterFromUrl(grUrl, "threadId");
            grUrl = UrlHelper.RemoveParameterFromUrl(grUrl, "mode");
            grUrl = UrlHelper.RemoveParameterFromUrl(grUrl, "postid");
            grUrl = UrlHelper.RemoveParameterFromUrl(grUrl, "replyto");
            grUrl = UrlHelper.RemoveParameterFromUrl(grUrl, "subscribeto");
            grUrl = UrlHelper.RemoveParameterFromUrl(grUrl, "moveto");
            result.Append(CreateItem(grUrl, HTMLHelper.HTMLEncode(ForumContext.CurrentGroup.GroupDisplayName), (ForumContext.CurrentForum != null) ? true : this.UseLinkForCurrentItem));
            separator = true;
        }
        else
        {
            //separator = false;
        }

        // Check whether forum exists and if should be displayed
        if ((this.DisplayThreads) && (ForumContext.CurrentGroup != null || ForumContext.CurrentThread != null) && (ForumContext.CurrentForum != null) && (ForumContext.CurrentForum.ForumID > 0))
        {
            if (separator)
            {
                result.Append(this.BreadcrumbSeparator);
            }

            string frUrl = URLCreator("forumid", ForumContext.CurrentForum.ForumID.ToString(), false, ForumContext.CurrentForum.DataClass.DataRow, ForumActionType.Forum);
            frUrl = UrlHelper.RemoveParameterFromUrl(frUrl, "threadId");
            frUrl = UrlHelper.RemoveParameterFromUrl(frUrl, "mode");
            frUrl = UrlHelper.RemoveParameterFromUrl(frUrl, "postid");
            frUrl = UrlHelper.RemoveParameterFromUrl(frUrl, "replyto");
            frUrl = UrlHelper.RemoveParameterFromUrl(frUrl, "subscribeto");
            frUrl = UrlHelper.RemoveParameterFromUrl(frUrl, "moveto");
            result.Append(CreateItem(frUrl, ResHelper.LocalizeString(HTMLHelper.HTMLEncode(ForumContext.CurrentForum.ForumDisplayName)), (ForumContext.CurrentThread != null) ? true : this.UseLinkForCurrentItem));
            separator = true;
        }
        else
        {
           //separator = false;
        }

        // Check whether thread exists and if should be displayed
        if ((this.DisplayThread) && (ForumContext.CurrentThread != null))
        {
            result.Append(this.BreadcrumbSeparator);
            result.Append(CreateItem(URLCreator("threadid", ForumContext.CurrentThread.PostId.ToString(), false, ForumContext.CurrentThread.DataClass.DataRow), HTMLHelper.HTMLEncode(ForumContext.CurrentThread.PostSubject), this.UseLinkForCurrentItem));
        }

        return result.ToString();
    }


    /// <summary>
    /// Creates link or clear text
    /// </summary>
    /// <param name="url">URL</param>
    /// <param name="text">Link text</param>
    /// <param name="useLink">Set if current item is left</param>
    protected string CreateItem(string url, string text, bool useLink)
    {
        StringBuilder sb = new StringBuilder();

        if (useLink)
        {
            sb.Append("<a href=\"");
            sb.Append(UrlHelper.EncodeQueryString(url));
            sb.Append("\">");
        }
        else
        {
            sb.Append("<span>");
        }

        sb.Append(HTMLHelper.HTMLEncode(text));

        if (useLink)
        {
            sb.Append("</a>");
        }
        else
        {
            sb.Append("</span>");
        }

        return sb.ToString();
    }


    /// <summary>
    /// OnPreRender, call Generate breadcrumbs
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        CopyValuesFromParent(this);

        litBreadcrumbs.Text = GenerateBreadcrumbs();
        base.OnPreRender(e);
    }
}
