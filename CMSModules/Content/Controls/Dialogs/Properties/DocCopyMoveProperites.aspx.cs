using System;
using System.Data;
using System.Threading;
using System.Collections;
using System.Security.Principal;
using System.Web;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.EventLog;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.WorkflowEngine;
using CMS.LicenseProvider;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_Controls_Dialogs_Properties_DocCopyMoveProperites : CMSDeskPage
{
    #region "Private variables & enums"

    private bool multiple = false;

    private int targetId = 0;
    private string parentAlias = string.Empty;
    private string whereCondition = string.Empty;

    private TreeProvider mTree = null;
    private EventLogProvider mEventLog = null;

    private static readonly Hashtable mErrors = new Hashtable();
    private readonly ArrayList nodeIds = new ArrayList();
    private DataSet documentsToProcess = null;

    private CurrentUserInfo currentUser = null;
    private SiteInfo currentSite = null;
    private SiteInfo targetSite = null;

    private static string refreshScript = null;
    private static string canceledString = null;

    private const string underlying = "Underlying";
    private const string lineBreak = "<br />";

    private string currentCulture = CultureHelper.DefaultUICulture;

    protected enum Action
    {
        Move = 0,
        Copy = 1,
        Link = 2,
        LinkDoc = 3
    }

    #endregion


    #region "Private properties"

    /// <summary>
    /// Returns current dialog action
    /// </summary>
    private static Action CurrentAction
    {
        get
        {
            try
            {
                return (Action)Enum.Parse(typeof(Action), QueryHelper.GetString("output", "move"), true);
            }
            catch
            {
                return Action.Move;
            }
        }
    }


    /// <summary>
    /// Perform action
    /// </summary>
    private static bool DoAction
    {
        get
        {
            return QueryHelper.GetBoolean("action", false);
        }
    }


    /// <summary>
    /// Current log context
    /// </summary>
    public LogContext CurrentLog
    {
        get
        {
            return EnsureLog();
        }
    }


    /// <summary>
    /// Ensures the logging context
    /// </summary>
    protected LogContext EnsureLog()
    {
        LogContext currentLog = LogContext.EnsureLog(ctlAsync.ProcessGUID);

        currentLog.Reversed = true;
        currentLog.LineSeparator = "<br />";

        return currentLog;
    }


    /// <summary>
    /// Current Error
    /// </summary>
    private string CurrentError
    {
        get
        {
            return ValidationHelper.GetString(mErrors["ProcessingError_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mErrors["ProcessingError_" + ctlAsync.ProcessGUID] = value;
        }
    }


    /// <summary>
    /// Tree provider
    /// </summary>
    private TreeProvider Tree
    {
        get
        {
            if (mTree == null)
            {
                if (currentUser == null)
                {
                    currentUser = CMSContext.CurrentUser;
                }
                mTree = new TreeProvider(currentUser);
                mTree.AllowAsyncActions = false;
            }
            return mTree;
        }
    }


    /// <summary>
    /// Event log
    /// </summary>
    private EventLogProvider EventLog
    {
        get
        {
            return mEventLog ?? (mEventLog = new EventLogProvider(Tree.Connection));
        }
    }


    /// <summary>
    /// Where condition used for multiple actions
    /// </summary>
    //private static string WhereCondition
    //{
    //    get
    //    {

    //    }
    //}

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!QueryHelper.ValidateHash("hash"))
        {
            return;
        }

        // Initialize current site user and culture
        currentSite = CMSContext.CurrentSite;
        currentUser = CMSContext.CurrentUser;
        currentCulture = CultureHelper.PreferredUICulture;

        // Initialize events
        ctlAsync.OnFinished += ctlAsync_OnFinished;
        ctlAsync.OnError += ctlAsync_OnError;
        ctlAsync.OnRequestLog += ctlAsync_OnRequestLog;
        ctlAsync.OnCancel += ctlAsync_OnCancel;

        if (!RequestHelper.IsCallback())
        {
            // Get the sorce node
            string nodeIdsString = QueryHelper.GetString("sourcenodeids", string.Empty);
            string[] nodeIdsArr = nodeIdsString.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string nodeId in nodeIdsArr)
            {
                int id = ValidationHelper.GetInteger(nodeId, 0);
                if (id != 0)
                {
                    nodeIds.Add(id);
                }
            }
            // Get target node id
            targetId = QueryHelper.GetInteger("targetid", 0);

            TreeNode tn = Tree.SelectSingleNode(targetId);
            if ((tn != null) && (tn.NodeSiteID != currentSite.SiteID))
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(tn.NodeSiteID);
                if (si != null)
                {
                    targetSite = si;
                }
            }
            else
            {
                targetSite = currentSite;
            }

            // Get whether action is multiple
            multiple = QueryHelper.GetBoolean("multiple", false);

            btnCancel.Text = ResHelper.GetString("General.Cancel");

            // Set introducing text
            if (targetId == 0)
            {
                switch (CurrentAction)
                {
                    case Action.Move:
                    case Action.Copy:
                    case Action.Link:
                        lblEmpty.Text = ResHelper.GetString("dialogs.copymove.select");
                        break;

                    case Action.LinkDoc:
                        lblEmpty.Text = ResHelper.GetString("dialogs.linkdoc.select");
                        break;
                }
            }

            if (!RequestHelper.IsPostBack())
            {
                object check = null;

                // Preset checkbox value
                switch (CurrentAction)
                {
                    case Action.Copy:
                        check = WindowHelper.GetItem(Action.Copy + underlying);
                        if (check == null)
                        {
                            WindowHelper.Add(Action.Copy + underlying, true);
                        }
                        chkUnderlying.Checked = ValidationHelper.GetBoolean(check, true);
                        break;

                    case Action.Link:
                    case Action.LinkDoc:
                        check = WindowHelper.GetItem(Action.Link + underlying);
                        if (check == null)
                        {
                            WindowHelper.Add(Action.Link + underlying, false);
                        }
                        chkUnderlying.Checked = ValidationHelper.GetBoolean(check, false);
                        break;
                }
            }

            string listInfoString = string.Empty;

            // Set up layout and strings depending on selected action
            switch (CurrentAction)
            {
                case Action.Move:
                    listInfoString = "dialogs.move.listinfo";
                    canceledString = "ContentRequest.MoveCanceled";
                    plcUnderlying.Visible = false;
                    break;

                case Action.Copy:
                    listInfoString = "dialogs.copy.listinfo";
                    canceledString = "ContentRequest.CopyingCanceled";
                    chkUnderlying.ResourceString = "contentrequest.copyunderlying";
                    plcUnderlying.Visible = true;
                    break;

                case Action.Link:
                    listInfoString = "dialogs.link.listinfo";
                    canceledString = "ContentRequest.LinkCanceled";
                    chkUnderlying.ResourceString = "contentrequest.linkunderlying";
                    plcUnderlying.Visible = true;
                    break;

                case Action.LinkDoc:
                    listInfoString = "dialogs.link.listinfo";
                    canceledString = "ContentRequest.LinkCanceled";
                    chkUnderlying.ResourceString = "contentrequest.linkunderlying";
                    plcUnderlying.Visible = true;
                    break;
            }

            // Localize string
            canceledString = ResHelper.GetString(canceledString);

            // Get alias path of document selected in tree
            string selectedAliasPath = TreePathUtils.GetAliasPathByNodeId(targetId);

            // Set target alias path
            if ((CurrentAction == Action.Copy) || (CurrentAction == Action.Move) || (CurrentAction == Action.Link))
            {
                lblAliasPath.Text = selectedAliasPath;
            }

            if (nodeIds.Count == 1)
            {
                TreeNode sourceNode = null;
                string sourceAliasPath = string.Empty;

                // Get source node
                if ((CurrentAction == Action.Copy) || (CurrentAction == Action.Move) || (CurrentAction == Action.Link))
                {
                    int nodeId = ValidationHelper.GetInteger(nodeIds[0], 0);
                    sourceNode = Tree.SelectSingleNode(nodeId);
                }
                else if (CurrentAction == Action.LinkDoc)
                {
                    sourceNode = Tree.SelectSingleNode(targetId);
                    if (sourceNode != null)
                    {
                        sourceAliasPath = sourceNode.NodeAliasPath;
                    }
                    // Show document to be linked
                    lblDocToCopyList.Text = sourceAliasPath;
                }

                // Hide checkbox if document has no childs
                if (sourceNode != null)
                {
                    if (sourceNode.NodeChildNodesCount == 0)
                    {
                        plcUnderlying.Visible = false;
                    }
                }
            }

            // Set visibility of panels
            pnlGeneralTab.Visible = true;
            pnlLog.Visible = false;

            // Get where condition for multiple operation
            whereCondition = HttpUtility.UrlDecode(QueryHelper.GetString("where", string.Empty)).Replace("%26", "&").Replace("%23", "#");

            // Get the aliaspaths of the documents to copy/move/link
            parentAlias = QueryHelper.GetString("parentAlias", string.Empty);
            if (parentAlias != string.Empty)
            {
                lblDocToCopy.Text = ResHelper.GetString(listInfoString + "all") + ResHelper.Colon;
                lblDocToCopyList.Text = parentAlias;
            }
            else
            {
                lblDocToCopy.Text = ResHelper.GetString(listInfoString) + ResHelper.Colon;

                // Get the list of alias paths
                if (!String.IsNullOrEmpty(nodeIdsString))
                {
                    // Try to get alias paths from session
                    DataSet ds = SessionHelper.GetValue("CopyMoveDocAliasPaths") as DataSet;
                    if (ds == null)
                    {
                        string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeParentID, DocumentName, NodeAliasPath, NodeLinkedNodeID");
                        ds = Tree.SelectNodes(CMSContext.CurrentSiteName, TreeProvider.ALL_DOCUMENTS,
                                              TreeProvider.ALL_CULTURES, true, null,
                                              "NodeID IN (" + nodeIdsString.Replace("|", ",") + ")", null,
                                              TreeProvider.ALL_LEVELS, false, 0, columns);
                        SessionHelper.SetValue("CopyMoveDocAliasPaths", ds);
                    }
                    if (!DataHelper.DataSourceIsEmpty(ds))
                    {
                        // Create list of paths
                        string aliasPaths = string.Empty;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            string aliasPath = ValidationHelper.GetString(dr["NodeAliasPath"], string.Empty);
                            int linkedNodeID = ValidationHelper.GetInteger(dr["NodeLinkedNodeID"], 0);
                            if (linkedNodeID != 0)
                            {
                                aliasPath += UIHelper.GetDocumentMarkImage(Page, DocumentMarkEnum.Link);
                            }
                            aliasPaths += aliasPath + lineBreak;
                        }

                        // Trim last line break
                        if (aliasPaths.Length > lineBreak.Length)
                        {
                            aliasPaths = aliasPaths.Substring(0, aliasPaths.Length - lineBreak.Length);
                        }

                        // Set alias paths
                        if ((CurrentAction == Action.Copy) || (CurrentAction == Action.Move) ||
                            (CurrentAction == Action.Link))
                        {
                            // As source paths
                            lblDocToCopyList.Text = aliasPaths;
                        }
                        else
                        {
                            // As target path
                            lblAliasPath.Text = aliasPaths;
                        }
                    }
                }
            }

            if (!RequestHelper.IsPostBack() && DoAction)
            {
                // Perform Move / Copy / Link action
                PerformAction();
            }

            pnlEmpty.Visible = (targetId <= 0);
            pnlGeneralTab.Visible = (targetId > 0);
        }
    }

    #endregion


    #region "Control events"

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        lblInfo.Text = canceledString;
        ctlAsync.RaiseCancel(sender, e);
    }


    protected void chkUnderlying_OnCheckedChanged(object sender, EventArgs e)
    {
        switch (CurrentAction)
        {
            case Action.Copy:
                WindowHelper.Add(Action.Copy + underlying, chkUnderlying.Checked);
                break;
            case Action.Link:
            case Action.LinkDoc:
                WindowHelper.Add(Action.Link + underlying, chkUnderlying.Checked);
                break;
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Moves document(s)
    /// </summary>
    private void Move(object parameter)
    {
        int oldSiteId = 0;
        int newSiteId = 0;
        int parentId = 0;
        int nodeId = 0;
        TreeNode node = null;

        try
        {
            AddLog(ResHelper.GetString("ContentRequest.StartMove", currentCulture));

            if (targetId == 0)
            {
                AddError(ResHelper.GetString("ContentRequest.ErrorMissingTarget", currentCulture));
                ctlAsync.RaiseError(null, null);
                return;
            }

            // Check if allow child type
            TreeNode targetNode = Tree.SelectSingleNode(targetId, TreeProvider.ALL_CULTURES);
            if (targetNode == null)
            {
                AddError(ResHelper.GetString("ContentRequest.ErrorMissingTarget", currentCulture));
                ctlAsync.RaiseError(null, null);
                return;
            }

            PrepareNodeIdsForAllDocuments(currentSite.SiteName);
            if (DataHelper.DataSourceIsEmpty(documentsToProcess))
            {
                // Create where condition
                string where = SqlHelperClass.GetWhereCondition("NodeID", (int[])nodeIds.ToArray(typeof(int)));
                string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeParentID, DocumentName, NodeAliasPath, NodeLinkedNodeID");
                documentsToProcess = Tree.SelectNodes(currentSite.SiteName, "/%", TreeProvider.ALL_CULTURES, true, null, where, null, TreeProvider.ALL_LEVELS, false, 0, columns);
            }

            if (!DataHelper.DataSourceIsEmpty(documentsToProcess))
            {
                foreach (DataRow nodeRow in documentsToProcess.Tables[0].Rows)
                {
                    nodeId = ValidationHelper.GetInteger(nodeRow["NodeID"], 0);
                    string className = nodeRow["ClassName"].ToString();
                    string aliasPath = nodeRow["NodeAliasPath"].ToString();
                    string docCulture = nodeRow["DocumentCulture"].ToString();

                    node = DocumentHelper.GetDocument(currentSite.SiteName, aliasPath, docCulture, false, className, null, null, TreeProvider.ALL_LEVELS, false, null, Tree);

                    if (node == null)
                    {
                        AddLog(string.Format(ResHelper.GetString("ContentRequest.DocumentNoLongerExists", currentCulture), HTMLHelper.HTMLEncode(aliasPath)));
                        continue;
                    }

                    string encodedAliasPath = " (" + HTMLHelper.HTMLEncode(node.NodeAliasPath) + ")";

                    // Get the document to move
                    if (nodeId == targetId)
                    {
                        AddError(ResHelper.GetString("ContentRequest.CannotMoveToItself", currentCulture) + encodedAliasPath);
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    oldSiteId = node.NodeSiteID;
                    parentId = node.NodeParentID;

                    // Check move permission
                    if (!IsUserAuthorizedToMove(node, targetId, node.NodeClassName))
                    {
                        AddError(ResHelper.GetString("ContentRequest.NotAllowedToMove") + encodedAliasPath);
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    // Impossible to move document to same location
                    if (targetNode.NodeID == parentId)
                    {
                        AddError(ResHelper.GetString("contentrequest.cannotmovetosameloc", currentCulture) + encodedAliasPath);
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    // Check cyclic movement (movement of the node to some of its child nodes)
                    if ((targetNode.NodeSiteID == node.NodeSiteID) && targetNode.NodeAliasPath.StartsWith(node.NodeAliasPath + "/", StringComparison.CurrentCultureIgnoreCase))
                    {
                        AddError(ResHelper.GetString("ContentRequest.CannotMoveToChild", currentCulture));
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    // Check allowed child classes
                    int targetClassId = ValidationHelper.GetInteger(targetNode.GetValue("NodeClassID"), 0);
                    int nodeClassId = ValidationHelper.GetInteger(node.GetValue("NodeClassID"), 0);

                    if (!DataClassInfoProvider.IsChildClassAllowed(targetClassId, nodeClassId))
                    {
                        AddError(ResHelper.GetString("ContentRequest.ErrorChildClassNotAllowed", currentCulture));
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    // Get child documents
                    if (node.NodeChildNodesCount > 0)
                    {
                        string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeAliasPath");
                        DataSet childNodes = Tree.SelectNodes(currentSite.SiteName, node.NodeAliasPath.TrimEnd('/') + "/%", TreeProvider.ALL_CULTURES, true, null, null, null, TreeProvider.ALL_LEVELS, false, 0, columns);

                        if (!DataHelper.DataSourceIsEmpty(childNodes))
                        {
                            foreach (DataRow childNode in childNodes.Tables[0].Rows)
                            {
                                AddLog(HTMLHelper.HTMLEncode(childNode["NodeAliasPath"] + " (" + childNode["DocumentCulture"] + ")"));
                            }
                        }
                    }

                    // Move the document
                    AddLog(HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.DocumentCulture + ")"));
                    DocumentHelper.MoveDocument(node, targetId, Tree);
                    newSiteId = node.NodeSiteID;
                }
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state != CMSThread.ABORT_REASON_STOP)
            {
                // Try to get ID of site
                int siteId = (node != null) ? node.NodeSiteID : CMSContext.CurrentSiteID;
                // Log event to event log
                LogExceptionToEventLog("MOVEDOC", "ContentRequest.MoveFailed", nodeId, ex, siteId);
            }
        }
        catch (Exception ex)
        {
            // Try to get ID of site
            int siteId = (node != null) ? node.NodeSiteID : CMSContext.CurrentSiteID;
            // Log event to event log
            LogExceptionToEventLog("MOVEDOC", "ContentRequest.MoveFailed", nodeId, ex, siteId);
            HandlePossibleErrors();
        }
        finally
        {
            if (multiple)
            {
                refreshScript = "RefreshListing();";
            }
            else
            {
                // Set moved document in current site or parent node if copy to other site
                if (oldSiteId == newSiteId)
                {
                    // Process result
                    refreshScript = "TreeSelectNode(" + nodeId + ");TreeRefreshNode(" + targetId + ", " + nodeId + ");";
                }
                else
                {
                    refreshScript = "TreeRefreshNode(" + parentId + ", " + parentId + ");TreeSelectNode(" + parentId + ");";
                }
            }
        }
    }


    /// <summary>
    /// Copies document(s)
    /// </summary>
    private void Copy(object parameter)
    {
        int nodeId = 0;
        int oldSiteId = 0;
        int newSiteId = 0;
        TreeNode node = null;

        try
        {
            AddLog(ResHelper.GetString("ContentRequest.StartCopy", currentCulture));

            if (targetId == 0)
            {
                AddError(ResHelper.GetString("ContentRequest.ErrorMissingTarget", currentCulture));
                ctlAsync.RaiseError(null, null);
                return;
            }

            TreeNode targetNode = Tree.SelectSingleNode(targetId, TreeProvider.ALL_CULTURES);
            if (targetNode == null)
            {
                AddError(ResHelper.GetString("ContentRequest.ErrorMissingTarget", currentCulture));
                ctlAsync.RaiseError(null, null);
                return;
            }

            PrepareNodeIdsForAllDocuments(currentSite.SiteName);
            if (DataHelper.DataSourceIsEmpty(documentsToProcess))
            {
                // Create where condition
                string where = SqlHelperClass.GetWhereCondition("NodeID", (int[])nodeIds.ToArray(typeof(int)));
                string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeAliasPath, ClassName, DocumentCulture");

                documentsToProcess = Tree.SelectNodes(currentSite.SiteName, "/%", TreeProvider.ALL_CULTURES, true, null, where, null, TreeProvider.ALL_LEVELS, false, 0, columns);
            }

            if (!DataHelper.DataSourceIsEmpty(documentsToProcess))
            {
                foreach (DataRow nodeRow in documentsToProcess.Tables[0].Rows)
                {
                    // Get the current document
                    nodeId = ValidationHelper.GetInteger(nodeRow["NodeID"], 0);
                    string className = nodeRow["ClassName"].ToString();
                    string aliasPath = nodeRow["NodeAliasPath"].ToString();
                    string docCulture = nodeRow["DocumentCulture"].ToString();
                    node = DocumentHelper.GetDocument(currentSite.SiteName, aliasPath, docCulture, false, className, null, null, TreeProvider.ALL_LEVELS, false, null, Tree);

                    if (node == null)
                    {
                        AddLog(string.Format(ResHelper.GetString("ContentRequest.DocumentNoLongerExists", currentCulture), HTMLHelper.HTMLEncode(aliasPath)));
                        continue;
                    }

                    string encodedAliasPath = " (" + HTMLHelper.HTMLEncode(node.NodeAliasPath) + ")";
                    bool includeChildNodes = ValidationHelper.GetBoolean(parameter, false);
                    bool childNodes = (node.NodeChildNodesCount > 0);

                    // Get the document to copy
                    if ((nodeId == targetId) && childNodes && includeChildNodes)
                    {
                        AddError(ResHelper.GetString("ContentRequest.CannotCopyToItself", currentCulture) + encodedAliasPath);
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    // Check permissions
                    if (!IsUserAuthorizedToCopyOrLink(node, targetId, node.NodeClassName))
                    {
                        AddError(ResHelper.GetString("ContentRequest.NotAllowedToCopy") + encodedAliasPath);
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    oldSiteId = node.NodeSiteID;

                    // Check cyclic copying (copying of the node to some of its child nodes)
                    if (includeChildNodes && childNodes && (targetNode.NodeSiteID == node.NodeSiteID) && targetNode.NodeAliasPath.StartsWith(node.NodeAliasPath + "/", StringComparison.CurrentCultureIgnoreCase))
                    {
                        AddError(ResHelper.GetString("ContentRequest.CannotCopyToChild", currentCulture));
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    // Check allowed child classes
                    int targetClassId = ValidationHelper.GetInteger(targetNode.GetValue("NodeClassID"), 0);
                    int nodeClassId = ValidationHelper.GetInteger(node.GetValue("NodeClassID"), 0);
                    if (!DataClassInfoProvider.IsChildClassAllowed(targetClassId, nodeClassId))
                    {
                        AddError(String.Format(ResHelper.GetString("ContentRequest.ErrorDocumentTypeNotAllowed", currentCulture), node.NodeAliasPath, node.NodeClassName));
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    // Copy the document
                    AddLog(HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.DocumentCulture + ")"));
                    node = DocumentHelper.CopyDocument(node, targetId, includeChildNodes, Tree);
                    newSiteId = node.NodeSiteID;
                }
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state != CMSThread.ABORT_REASON_STOP)
            {
                // Try to get ID of site
                int siteId = (node != null) ? node.NodeSiteID : CMSContext.CurrentSiteID;
                // Log event to event log
                LogExceptionToEventLog("COPYDOC", "ContentRequest.CopyFailed", nodeId, ex, siteId);
            }
        }
        catch (Exception ex)
        {
            // Try to get ID of site
            int siteId = (node != null) ? node.NodeSiteID : CMSContext.CurrentSiteID;
            // Log event to event log
            LogExceptionToEventLog("COPYDOC", "ContentRequest.CopyFailed", nodeId, ex, siteId);
            HandlePossibleErrors();
        }
        finally
        {
            if (multiple)
            {
                AddLog(ResHelper.GetString("ContentRequest.CopyOK", currentCulture));
                refreshScript = "RefreshListing();";
            }
            else
            {
                // Set moved document in current site or parent node if copy to other site
                if (oldSiteId == newSiteId)
                {
                    // Process result
                    if (node != null)
                    {
                        nodeId = (multiple ? nodeId : node.NodeID);

                        refreshScript = "TreeSelectNode(" + nodeId + "); TreeRefreshNode(" + targetId + ", " + nodeId + ");";
                    }
                    else
                    {
                        AddError(ResHelper.GetString("ContentRequest.CopyFailed", currentCulture));
                    }
                }
                else
                {
                    AddLog(ResHelper.GetString("ContentRequest.CopyOK", currentCulture));
                    refreshScript = string.Empty;
                }
            }
        }
    }


    private void Link(object parameter)
    {
        bool includeChildNodes = ValidationHelper.GetBoolean(parameter, false);
        Link(includeChildNodes, targetId, nodeIds, Action.Link);
    }


    private void LinkDoc(object parameter)
    {
        if ((nodeIds.Count > 0) && (targetId != 0))
        {
            // Switch parameters
            int currentTargetId = ValidationHelper.GetInteger(nodeIds[0], 0);
            ArrayList currentNodeIds = new ArrayList();
            currentNodeIds.Add(targetId);

            bool includeChildNodes = ValidationHelper.GetBoolean(parameter, false);
            Link(includeChildNodes, currentTargetId, currentNodeIds, Action.LinkDoc);
        }
    }


    /// <summary>
    /// Links selected document(s)
    /// </summary>
    /// <param name="includeChildNodes">Determines whether include child nodes.</param>
    /// <param name="targetNodeId">Target node ID</param>
    /// <param name="sourceNodes">Nodes</param>
    /// <param name="performedAction">Action to be performed</param>
    private void Link(bool includeChildNodes, int targetNodeId, ArrayList sourceNodes, Action performedAction)
    {
        int nodeId = 0;
        int oldSiteId = 0;
        int newSiteId = 0;
        TreeNode node = null;

        try
        {
            AddLog(ResHelper.GetString("ContentRequest.StartLink", currentCulture));

            if (targetNodeId == 0)
            {
                AddError(ResHelper.GetString("ContentRequest.ErrorMissingTarget", currentCulture));
                ctlAsync.RaiseError(null, null);
                return;
            }

            // Check if allow child type
            TreeNode targetNode = Tree.SelectSingleNode(targetNodeId, TreeProvider.ALL_CULTURES);
            if (targetNode == null)
            {
                AddError(ResHelper.GetString("ContentRequest.ErrorMissingTarget", currentCulture));
                ctlAsync.RaiseError(null, null);
                return;
            }

            // Prepare NodeIDs to process
            if (performedAction == Action.LinkDoc)
            {
                PrepareNodeIdsForAllDocuments(targetSite.SiteName);
            }
            else if (performedAction == Action.Link)
            {
                PrepareNodeIdsForAllDocuments(currentSite.SiteName);
            }

            string siteName = (performedAction == Action.LinkDoc) ? targetSite.SiteName : currentSite.SiteName;

            if (DataHelper.DataSourceIsEmpty(documentsToProcess))
            {
                // Create where condition
                string where = SqlHelperClass.GetWhereCondition("NodeID", (int[])sourceNodes.ToArray(typeof(int)));
                string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeParentID, DocumentName, NodeAliasPath, NodeLinkedNodeID");

                documentsToProcess = Tree.SelectNodes(siteName, "/%", TreeProvider.ALL_CULTURES, true, null, where, null, TreeProvider.ALL_LEVELS, false, 0, columns);
            }

            if (!DataHelper.DataSourceIsEmpty(documentsToProcess))
            {
                foreach (DataRow nodeRow in documentsToProcess.Tables[0].Rows)
                {
                    nodeId = ValidationHelper.GetInteger(nodeRow["NodeID"], 0);
                    string className = nodeRow["ClassName"].ToString();
                    string aliasPath = nodeRow["NodeAliasPath"].ToString();
                    string docCulture = nodeRow["DocumentCulture"].ToString();

                    node = DocumentHelper.GetDocument(siteName, aliasPath, docCulture, false, className, null, null, TreeProvider.ALL_LEVELS, false, null, Tree);

                    if (node == null)
                    {
                        AddLog(string.Format(ResHelper.GetString("ContentRequest.DocumentNoLongerExists", currentCulture), HTMLHelper.HTMLEncode(aliasPath)));
                        continue;
                    }

                    bool childNodes = (node.NodeChildNodesCount > 0);

                    string encodedAliasPath = " (" + HTMLHelper.HTMLEncode(node.NodeAliasPath) + ")";

                    // Document can't be copied under itself if child nodes are present
                    if ((nodeId == targetNodeId) && childNodes && includeChildNodes)
                    {
                        AddError(ResHelper.GetString("ContentRequest.CannotLinkToItself", currentCulture) + encodedAliasPath);
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    // Check cyclic linking (linking of the node to some of its child nodes)
                    if ((targetNode.NodeSiteID == node.NodeSiteID) && (targetNode.NodeAliasPath.TrimEnd('/') + "/").StartsWith(node.NodeAliasPath + "/", StringComparison.CurrentCultureIgnoreCase) && includeChildNodes)
                    {
                        AddError(ResHelper.GetString("ContentRequest.CannotLinkToChild", currentCulture));
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    // Check permissions
                    if (!IsUserAuthorizedToCopyOrLink(node, targetNodeId, node.NodeClassName))
                    {
                        AddError(ResHelper.GetString("ContentRequest.NotAllowedToLink") + encodedAliasPath);
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    oldSiteId = node.NodeSiteID;

                    // Check the licence limitations
                    if (node.NodeClassName.ToLower() == "cms.blog")
                    {
                        if (!LicenseHelper.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Blogs, VersionActionEnum.Insert))
                        {
                            AddError(ResHelper.GetString("cmsdesk.bloglicenselimits"));
                            ctlAsync.RaiseError(null, null);
                            continue;
                        }
                    }

                    // Check allowed child classes
                    int targetClassId = ValidationHelper.GetInteger(targetNode.GetValue("NodeClassID"), 0);
                    int nodeClassId = ValidationHelper.GetInteger(node.GetValue("NodeClassID"), 0);
                    if (!DataClassInfoProvider.IsChildClassAllowed(targetClassId, nodeClassId))
                    {
                        AddError(ResHelper.GetString("ContentRequest.ErrorChildClassNotAllowed", currentCulture));
                        ctlAsync.RaiseError(null, null);
                        continue;
                    }

                    // Link the document
                    AddLog(HTMLHelper.HTMLEncode(node.NodeAliasPath));

                    DocumentHelper.InsertDocumentAsLink(node, targetNodeId, Tree, includeChildNodes);
                    newSiteId = node.NodeSiteID;
                }
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state != CMSThread.ABORT_REASON_STOP)
            {
                // Try to get ID of site
                int siteId = (node != null) ? node.NodeSiteID : CMSContext.CurrentSiteID;
                // Log event to event log
                LogExceptionToEventLog("LINKDOC", "ContentRequest.LinkFailed", nodeId, ex, siteId);
            }

        }
        catch (Exception ex)
        {
            // Try to get ID of site
            int siteId = (node != null) ? node.NodeSiteID : CMSContext.CurrentSiteID;
            // Log event to event log
            LogExceptionToEventLog("LINKDOC", "ContentRequest.LinkFailed", nodeId, ex, siteId);
            HandlePossibleErrors();
        }
        finally
        {
            if (multiple)
            {
                AddLog(ResHelper.GetString("ContentRequest.LinkOK", currentCulture));
                refreshScript = "RefreshListing();";
            }
            else
            {
                // Set linked document in current site or parent node if linked to other site
                if (oldSiteId == newSiteId)
                {
                    if (node == null)
                    {
                        AddError(ResHelper.GetString("ContentRequest.LinkFailed", currentCulture));
                    }
                }
                else
                {
                    AddLog(ResHelper.GetString("ContentRequest.LinkOK", currentCulture));
                }

                // Process result
                if (node != null)
                {
                    refreshScript = "TreeSelectNode(" + node.NodeID + "); TreeRefreshNode(" + node.NodeID + ", " + node.NodeID + ");";
                }
            }
        }
    }


    /// <summary>
    /// Performes the Move / Copy / Link action.
    /// </summary>
    public void PerformAction()
    {
        switch (CurrentAction)
        {
            case Action.Move:
                // Process move
                RunAsync(Move);
                break;

            case Action.Copy:
                // Process copy
                RunAsync(Copy);
                break;

            case Action.Link:
                // Process link
                RunAsync(Link);
                break;

            case Action.LinkDoc:
                // Process link
                RunAsync(LinkDoc);
                break;
        }
    }

    #endregion


    #region "Help methods"

    /// <summary>
    /// When exception occures, log it to event log and add message to asnyc log
    /// </summary>
    /// <param name="errorTitle">Error to add to async log</param>
    /// <param name="nodeId">ID of node which caused operation to fail</param>
    /// <param name="ex">Exception to log</param>
    /// <param name="siteId">Site identifier</param>
    /// <param name="eventCode">Code of event</param>
    private void LogExceptionToEventLog(string eventCode, string errorTitle, int nodeId, Exception ex, int siteId)
    {
        EventLog.LogEvent("E", DateTime.Now, "Content", eventCode, currentUser.UserID, currentUser.UserName, nodeId,
                          null, HTTPHelper.GetUserHostAddress(), EventLogProvider.GetExceptionLogMessage(ex), siteId,
                          HTTPHelper.GetAbsoluteUri());

        AddError(ResHelper.GetString(errorTitle, currentCulture) + " : " + EventLogProvider.GetExceptionLogMessage(ex));
    }


    /// <summary>
    /// Prepares IDs of nodes when action is performed for all documents under specified parent
    /// </summary>
    private void PrepareNodeIdsForAllDocuments(string siteName)
    {
        if (!string.IsNullOrEmpty(parentAlias))
        {
            string where = "ClassName <> 'CMS.Root'";
            if (!string.IsNullOrEmpty(whereCondition))
            {
                where = SqlHelperClass.AddWhereCondition(where, whereCondition);
            }
            string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeParentID, DocumentName, NodeAliasPath, NodeLinkedNodeID");
            documentsToProcess = Tree.SelectNodes(siteName, parentAlias.TrimEnd('/') + "/%", TreeProvider.ALL_CULTURES, true, null, where, null, 1, false, 0, columns);
            nodeIds.Clear();
            if (!DataHelper.DataSourceIsEmpty(documentsToProcess))
            {
                foreach (DataRow row in documentsToProcess.Tables[0].Rows)
                {
                    nodeIds.Add(ValidationHelper.GetInteger(row["NodeID"], 0));
                }
            }
        }
    }


    /// <summary>
    /// Adds the alert message to the output request window
    /// </summary>
    /// <param name="message">Message to display</param>
    private void AddAlert(string message)
    {
        ltlScript.Text += ScriptHelper.GetAlertScript(message);
    }


    /// <summary>
    /// Adds the script to the output request window
    /// </summary>
    /// <param name="script">Script to add</param>
    public override void AddScript(string script)
    {
        ltlScript.Text += ScriptHelper.GetScript(script);
    }


    /// <summary>
    /// Determines whether current user is authorized to move document.
    /// </summary>
    /// <param name="sourceNode">Source node.</param>
    /// <param name="targetNodeId">Target node class name.</param>
    /// <param name="sourceNodeClassName">Source node class name.</param>
    /// <returns>True if authorized</returns>
    private bool IsUserAuthorizedToMove(TreeNode sourceNode, int targetNodeId, string sourceNodeClassName)
    {
        bool isAuthorized = false;

        // Check 'modify permission' to source document
        if (currentUser.IsAuthorizedPerDocument(sourceNode, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Allowed)
        {
            // Check 'create permission'
            if (currentUser.IsAuthorizedToCreateNewDocument(targetNodeId, sourceNodeClassName, targetSite.SiteName))
            {
                isAuthorized = true;
            }
        }

        return isAuthorized;
    }


    /// <summary>
    /// Determines whether current user is authorized to copy or link specified document.
    /// </summary>
    /// <param name="sourceNode">Source node.</param>
    /// <param name="targetNodeId">Target node class name.</param>
    /// <param name="sourceNodeClassName">Source node class name.</param>
    /// <returns>True if authorized</returns>
    private bool IsUserAuthorizedToCopyOrLink(TreeNode sourceNode, int targetNodeId, string sourceNodeClassName)
    {
        bool isAuthorized = false;

        // Check 'read permission' to source document
        if (currentUser.IsAuthorizedPerDocument(sourceNode, NodePermissionsEnum.Read) == AuthorizationResultEnum.Allowed)
        {
            // Check 'create permission'
            if (currentUser.IsAuthorizedToCreateNewDocument(targetNodeId, sourceNodeClassName, targetSite.SiteName))
            {
                isAuthorized = true;
            }
        }

        return isAuthorized;
    }

    #endregion


    #region "Handling async thread"

    private void ctlAsync_OnCancel(object sender, EventArgs e)
    {
        if (ctlAsync.Status == AsyncWorkerStatusEnum.Running)
        {
            ctlAsync.Stop();
        }
        pnlLog.Visible = false;
        pnlGeneralTab.Visible = true;

        AddScript("var __pendingCallbacks = new Array(); DestroyLog();");
        RefreshDialogTree();
        AddScript(refreshScript);
        HandlePossibleErrors();
        CurrentLog.Close();
    }


    private void ctlAsync_OnRequestLog(object sender, EventArgs e)
    {
        ctlAsync.Log = CurrentLog.Log;
    }


    private void ctlAsync_OnError(object sender, EventArgs e)
    {
        if (ctlAsync.Status == AsyncWorkerStatusEnum.Running)
        {
            ctlAsync.Stop();
        }
        pnlLog.Visible = false;
        pnlGeneralTab.Visible = true;
        AddScript("DestroyLog();");
        HandlePossibleErrors();
    }


    private void ctlAsync_OnFinished(object sender, EventArgs e)
    {
        if (!HandlePossibleErrors())
        {
            if (refreshScript != null)
            {
                AddScript(refreshScript + "RemoveSelection(); window.top.close();");
            }
        }
        else
        {
            AddScript("DestroyLog();RemoveSelection();");
            RefreshDialogTree();
        }
        CurrentLog.Close();
    }


    /// <summary>
    /// Adds script to refresh dialog tree
    /// </summary>
    private void RefreshDialogTree()
    {
        string script = "if(parent!=null)if(parent.SetAction!=null)parent.SetAction('refreshtree', '" + targetId + "');";
        script += "if(parent!=null)if(parent.RaiseHiddenPostBack!=null)parent.RaiseHiddenPostBack();";
        AddScript(script);
    }


    /// <summary>
    /// Adds the log information
    /// </summary>
    /// <param name="newLog">New log information</param>
    protected void AddLog(string newLog)
    {
        EnsureLog();
        LogContext.AppendLine(newLog);
    }


    /// <summary>
    /// Adds the error to collection of errors
    /// </summary>
    /// <param name="error">Error message</param>
    protected void AddError(string error)
    {
        AddLog(error);
        string separator = multiple ? "<br />" : "\n";
        CurrentError = (error + separator + CurrentError);
    }


    /// <summary>
    /// Runs async thread
    /// </summary>
    /// <param name="action">Method to run</param>
    protected void RunAsync(AsyncAction action)
    {
        pnlLog.Visible = true;
        pnlGeneralTab.Visible = false;

        CurrentLog.Close();
        CurrentError = string.Empty;

        AddScript("InitializeLog();");
        switch (CurrentAction)
        {
            case Action.Copy:
                ctlAsync.Parameter = ValidationHelper.GetBoolean(WindowHelper.GetItem(Action.Copy + underlying), false);
                break;
            case Action.Link:
            case Action.LinkDoc:
                ctlAsync.Parameter = ValidationHelper.GetBoolean(WindowHelper.GetItem(Action.Link + underlying), false);
                break;
        }
        ctlAsync.RunAsync(action, WindowsIdentity.GetCurrent());
    }


    /// <summary>
    /// Ensures any error or info is displayed to user
    /// </summary>
    /// <returns>True if error occurred.</returns>
    protected bool HandlePossibleErrors()
    {
        if (!string.IsNullOrEmpty(CurrentError))
        {
            if (!multiple)
            {
                AddAlert(CurrentError);
            }
            else
            {
                lblError.Text = CurrentError;
            }
            ctlAsync.Log = CurrentLog.Log;
            return true;
        }
        return false;
    }

    #endregion
}
