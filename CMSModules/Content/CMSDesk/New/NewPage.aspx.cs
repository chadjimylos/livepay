using System;
using System.Web.UI;

using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.PortalEngine;
using CMS.WorkflowEngine;
using CMS.FormEngine;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.ExtendedControls;

public partial class CMSModules_Content_CMSDesk_New_NewPage : CMSContentPage, IPostBackEventHandler
{
    int parentNodeId = 0;
    private const string pageClassName = "CMS.MenuItem";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
        {
            ltlScript.Text = "<script type=\"text/javascript\">  //<![CDATA[ spellURL = \"" + CMSContext.ResolveDialogUrl("~/CMSFormControls/LiveSelectors/SpellCheck.aspx") + "\"; function SpellCheck() { checkSpelling(spellURL); } var offsetValue = 7; //]]> </script>";
        }
        else
        {
            ltlScript.Text = "<script type=\"text/javascript\">  //<![CDATA[ spellURL = \"" + CMSContext.ResolveDialogUrl("~/CMSModules/Content/CMSDesk/Edit/SpellCheck.aspx") + "\"; function SpellCheck() { checkSpelling(spellURL); } var offsetValue = 7; //]]> </script>";
        }

        parentNodeId = QueryHelper.GetInteger("nodeid", 0);


        TreeProvider tp = new TreeProvider(CMSContext.CurrentUser);
        // For new node is not document culture important, preffered culture is used
        TreeNode node = tp.SelectSingleNode(parentNodeId);
        if (node != null)
        {
            templateSelector.DocumentID = node.DocumentID;
        }

        // Register progrees script
        ScriptHelper.RegisterProgress(Page);

        // Check permission to create page with redirect
        CheckSecurity(true);

        if (!LicenseHelper.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Documents, VersionActionEnum.Insert))
        {
            RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.documentslicenselimits"), ""));
        }

        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Content", "Design"))
        {
            radCreateBlank.Visible = false;
            layoutSelector.Visible = false;
        }

        // Hide error label
        lblError.Style.Add("display", "none");

        string jsValidation = "function ValidateNewPage(){" +
        " var value = document.getElementById('" + txtPageName.ClientID + "').value;" +
        " value = value.replace(/^\\s+|\\s+$/g, '');" +
        " var errorLabel = document.getElementById('" + lblError.ClientID + "'); " +
        " if (value == '') {" +
        " errorLabel.style.display = ''; errorLabel.innerHTML  = " + ScriptHelper.GetString(ResHelper.GetString("newpage.nameempty")) + "; resizearea(); return false;}";
        if (!radInherit.Checked)
        {
            string errorMessage = String.Empty;
            if (radCreateBlank.Checked)
            {
                errorMessage = ScriptHelper.GetString(ResHelper.GetString("NewPage.LayoutError"));
            }
            else
            {
                errorMessage = ScriptHelper.GetString(ResHelper.GetString("newpage.templateerror"));
            }

            jsValidation += " if (UniFlat_GetSelectedValue) { value = UniFlat_GetSelectedValue(); " +
             " value = value.replace(/^\\s+|\\s+$/g, '');" +
             " if (value == '') {" +
             " errorLabel.style.display = ''; errorLabel.innerHTML  = " + errorMessage + "; resizearea(); return false;}" +
             " }";
        }
        jsValidation += " return true;}";

        // Register validate script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ValidateNewPage", ScriptHelper.GetScript(jsValidation));

        // Register save document script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SaveDocument",
            ScriptHelper.GetScript("function SaveDocument(nodeId, createAnother) {if (ValidateNewPage()) { " + ControlsHelper.GetPostBackEventReference(this, "#", false).Replace("'#'", "createAnother+''") + "; return false; }}"));

        ScriptHelper.RegisterFlatResize(Page);

        radUseTemplate.CheckedChanged += radOptions_CheckedChanged;
        radInherit.CheckedChanged += radOptions_CheckedChanged;
        radCreateBlank.CheckedChanged += radOptions_CheckedChanged;

        // Disable startup focus functionality
        templateSelector.UseStartUpFocus = false;
        // Set  default focun on page name field
        txtPageName.Focus();

        LoadControls();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        // Register the scripts
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SpellCheck_" + ClientID, ltlScript.Text);
    }


    /// <summary>
    /// Check the permission to create page(menuitem) under node
    /// </summary>
    /// <param name="redirect">If true and user doesn't have permission, user is redirected to access denied page</param>
    /// <returns>True if user has permission, otherwise false</returns>
    private bool CheckSecurity(bool redirect)
    {
        // Check if user is allowed to create page under this node
        if (!CMSContext.CurrentUser.IsAuthorizedToCreateNewDocument(parentNodeId, pageClassName))
        {
            // Redirect if enabled
            if (redirect)
            {
                RedirectToAccessDenied(ResHelper.GetString("cmsdesk.notauthorizedtocreatedocument"));
            }
            return false;
        }

        return true;
    }


    /// <summary>
    /// Setups and load control selected by radio buttons
    /// </summary>
    private void LoadControls()
    {
        if (radUseTemplate.Checked)
        {
            ShowTemplateSelector(true);
            ShowLayoutSelector(false);
            ShowInherited(false);
        }
        else if (radInherit.Checked)
        {
            lblIngerited.Text = ResHelper.GetString("NewPage.InheritedTemplateName").Replace("##TEMPLATENAME##", GetParentNodePageTemplate());
            ShowTemplateSelector(false);
            ShowLayoutSelector(false);
            ShowInherited(true);
        }
        else if (radCreateBlank.Checked)
        {
            ShowTemplateSelector(false);
            ShowLayoutSelector(true);
            ShowInherited(false);
        }
    }


    /// <summary>
    /// Enables or disables template selector
    /// </summary>    
    private void ShowTemplateSelector(bool show)
    {
        templateSelector.Visible = show;
        templateSelector.StopProcessing = !show;
    }


    /// <summary>
    /// Enables or disables layout selector
    /// </summary>
    private void ShowLayoutSelector(bool show)
    {
        plcLayout.Visible = show;
        layoutSelector.StopProcessing = !show;
    }


    /// <summary>
    /// Enables or disables inherited label
    /// </summary>
    private void ShowInherited(bool show)
    {
        plcInherited.Visible = show;
    }


    /// <summary>
    /// Returns template name of parent node.
    /// </summary>    
    private string GetParentNodePageTemplate()
    {
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        TreeNode node = tree.SelectSingleNode(parentNodeId, CMSContext.PreferredCultureCode, tree.CombineWithDefaultCulture, false);
        if (node != null)
        {
            int templateId = ValidationHelper.GetInteger(node.GetValue("DocumentPageTemplateID"), 0);
            bool inherited = false;

            // May be inherited template
            if ((templateId == 0) && (node.NodeParentID > 0))
            {
                // Get inherited page template
                object currentPageTemplateId = node.GetValue("DocumentPageTemplateID");
                node.SetValue("DocumentPageTemplateID", DBNull.Value);
                node.LoadInheritedValues(new string[] { "DocumentPageTemplateID" }, false);
                templateId = ValidationHelper.GetInteger(node.GetValue("DocumentPageTemplateID"), 0);
                node.SetValue("DocumentPageTemplateID", currentPageTemplateId);
                inherited = true;
            }

            if (templateId > 0)
            {
                PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(templateId);
                if (pti != null)
                {
                    string templateName = pti.DisplayName;
                    if (inherited)
                    {
                        templateName += " (inherited)";
                    }
                    return templateName;
                }
            }
        }

        return String.Empty;
    }


    /// <summary>
    /// Handles radio button change
    /// </summary>
    protected void radOptions_CheckedChanged(object sender, EventArgs e)
    {
        // Template selector needs to reload its tree
        if (radUseTemplate.Checked)
        {
            templateSelector.ResetToDefault();

            // Reload template tree
            templateSelector.ReloadData(false);
        }
        else if (radCreateBlank.Checked)
        {
            layoutSelector.UniFlatSelector.ResetToDefault();
        }

        // Start rezise after radio change
        ScriptHelper.RegisterStartupScript(this, typeof(string), "ResizeRecount", ScriptHelper.GetScript("resizearea();"));

        // Update panel
        pnlUpdate.Update();
    }


    /// <summary>
    /// Creates new page(CMS.MenuItem) and assignes selected template
    /// </summary>
    protected void Save(bool createAnother)
    {
        // Check security
        CheckSecurity(true);

        string newPageName = txtPageName.Text.Trim();

        string errorMessage = null;

        if (!String.IsNullOrEmpty(newPageName))
        {
            // Limit length to 100 characters
            newPageName = TextHelper.LimitLength(newPageName, 100, String.Empty);
        }
        else
        {
            errorMessage = ResHelper.GetString("newpage.nameempty");
        }

        if (parentNodeId == 0)
        {
            errorMessage = ResHelper.GetString("newpage.invalidparentnode");
        }

        // If error, show error message and return
        if (String.IsNullOrEmpty(errorMessage))
        {

            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
            TreeNode node = new TreeNode("CMS.MenuItem", tree);

            // Load default data
            FormHelper.LoadDefaultValues(node);

            node.DocumentName = newPageName;
            node.SetValue("MenuItemName", newPageName);
            lblError.Style.Remove("display");

            // Template selection
            if (radUseTemplate.Checked)
            {
                // Template page
                int templateId = ValidationHelper.GetInteger(templateSelector.SelectedItem, 0);
                if (templateId > 0)
                {
                    node.DocumentPageTemplateID = templateId;
                }
                else
                {
                    errorMessage = ResHelper.GetString("NewPage.TemplateError");
                }
            }
            else if (radInherit.Checked)
            {
                // Inherited page           
                node.SetValue("DocumentPageTemplateID", DBNull.Value);
            }

            if (radCreateBlank.Checked)
            {
                // Blank page with layout
                int layoutId = ValidationHelper.GetInteger(layoutSelector.SelectedItem, 0);

                if (layoutId > 0)
                {
                    // Create custom template info for the page
                    PageTemplateInfo templateInfo = new PageTemplateInfo(true);

                    templateInfo.LayoutID = layoutId;

                    // Prepare ad-hoc template name
                    string displayName = "Ad-hoc: " + node.DocumentName;

                    // Create ad-hoc template
                    templateInfo = PageTemplateInfoProvider.CloneTemplateAsAdHoc(templateInfo, displayName, CMSContext.CurrentSiteID);

                    // Copy layout to selected template
                    if (chkLayoutPageTemplate.Checked)
                    {
                        templateInfo.LayoutID = 0;
                        LayoutInfo li = LayoutInfoProvider.GetLayoutInfo(layoutId);
                        if (li != null)
                        {
                            templateInfo.PageTemplateLayout = li.LayoutCode;
                        }
                        else
                        {
                            errorMessage = ResHelper.GetString("NewPage.LayoutError");
                        }
                    }

                    if (String.IsNullOrEmpty(errorMessage))
                    {
                        // Set inherit only master 
                        templateInfo.InheritPageLevels = "\\";
                        PageTemplateInfoProvider.SetPageTemplateInfo(templateInfo);

                        if (CMSContext.CurrentSite != null)
                        {
                            PageTemplateInfoProvider.AddPageTemplateToSite(templateInfo.PageTemplateId, CMSContext.CurrentSite.SiteID);
                        }

                        // Assign the template to document
                        node.SetValue("DocumentPageTemplateID", templateInfo.PageTemplateId);
                    }
                }
                else
                {
                    errorMessage = ResHelper.GetString("NewPage.LayoutError");
                }
            }

            // Insert node if no error
            if (String.IsNullOrEmpty(errorMessage))
            {
                node.DocumentCulture = CMSContext.PreferredCultureCode;

                // Insert the document
                DocumentHelper.InsertDocument(node, parentNodeId, tree);
                //node.Insert(parentNodeId);

                // Create default SKU if configured
                if (ModuleEntry.CheckModuleLicense(ModuleEntry.ECOMMERCE, UrlHelper.GetCurrentDomain(), FeatureEnum.Ecommerce, VersionActionEnum.Insert))
                {
                    node.CreateDefaultSKU();
                }

                if (node.NodeSKUID > 0)
                {
                    DocumentHelper.UpdateDocument(node, tree);
                }

                // Process create another flag
                string script = null;
                if (createAnother)
                {
                    script = ScriptHelper.GetScript("parent.PassiveRefresh(" + node.NodeParentID + ", " + node.NodeParentID + "); parent.CreateAnother();");
                }
                else
                {
                    script = ScriptHelper.GetScript("parent.RefreshTree(" + node.NodeID + ", " + node.NodeID + ");");
                    script += ScriptHelper.GetScript("parent.SelectNode(" + node.NodeID + ");");
                }

                ScriptHelper.RegisterClientScriptBlock(Page, typeof(string), "Refresh", script);

            }
        }

        // Insert node if no error
        if (!String.IsNullOrEmpty(errorMessage))
        {
            if (radUseTemplate.Checked)
            {
                templateSelector.ReloadData();
            }

            lblError.Text = errorMessage;
            lblError.Visible = true;
            return;
        }
    }


    #region "IPostBackEventHandler members"

    /// <summary>
    /// Raises event postback event.
    /// </summary>
    /// <param name="eventArgument"></param>
    public void RaisePostBackEvent(string eventArgument)
    {
        bool createAnother = ValidationHelper.GetBoolean(eventArgument, false);

        // Create document
        Save(createAnother);
    }

    #endregion
}
