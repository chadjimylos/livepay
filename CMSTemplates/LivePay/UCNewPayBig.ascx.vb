﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.SettingsProvider
Imports CMS.SiteProvider
Imports CMS.DataEngine
Partial Class CMSTemplates_LivePay_UCNewPayBig
    Inherits CMSUserControl

    Public searchText As String

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        btnSearch.ImageUrl = ResHelper.LocalizeString("{$=/app_themes/LivePay/btnSearchOk.gif|en-us=/app_themes/LivePay/btnSearchOk_en-us.png$}")
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        searchText = ResHelper.LocalizeString("{$=Πληκτρολογήστε Επωνυμία Επιχείρησης|en-us=Enter the Company Name$}")
        JsScript("$('#" + btnSearch.ClientID + "').attr('_defaultvalue','" + searchText + "');")
      
        Me.txtSearch.Text = searchText
        Me.txtSearch.Attributes.Add("onfocus", "if(this.value=='" & searchText & "'){this.value='';}")
        Me.txtSearch.Attributes.Add("onBlur", "if (this.value == '') {this.value = '" & searchText & "';}")

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        Dim key As String = Replace(StripHTMLFunctions.StripTags(txtSearch.Text), "'", String.Empty)
        key = Replace(key, """", String.Empty)
        If key <> String.Empty Then
            Response.Redirect(String.Concat("~/Search.aspx?searchtext=", key, ""))
        End If
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString(), Script, True)
    End Sub
End Class
