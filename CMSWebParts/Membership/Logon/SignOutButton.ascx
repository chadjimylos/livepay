<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Membership/Logon/SignOutButton.ascx.cs"
    Inherits="CMSWebParts_Membership_Logon_SignOutButton" %>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
<cms:CMSButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click" CssClass="signoutButton"
    EnableViewState="false" />
<asp:LinkButton ID="btnSignOutLink" runat="server" OnClick="btnSignOut_Click" CssClass="signoutLink"
    EnableViewState="false" />