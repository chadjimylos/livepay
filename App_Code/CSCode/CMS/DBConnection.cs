﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


/// <summary>
/// Demonstrates how to work with SqlConnection objects
/// </summary>
public static class DBConnection
{
	public static void Update(int ID, decimal Longitude, decimal Latitude)
	{
		//System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString() 
		string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();

		SqlConnection objConnection = new SqlConnection(strConnection);
		string query = "proc_Wind_Stores_Edit";

		objConnection.Open();

		SqlCommand objCommand = new SqlCommand(query, objConnection);
		objCommand.CommandType = CommandType.StoredProcedure;

		objCommand.Parameters.Add("@NodeID", SqlDbType.BigInt).Value = ID;
		objCommand.Parameters.Add("@StoresLongitude", SqlDbType.Decimal).Value = Longitude;
		objCommand.Parameters.Add("@StoresLatitude", SqlDbType.Decimal).Value = Latitude;

		objCommand.ExecuteScalar();

		objConnection.Close();
	}

	public static DataSet GetAllStores(string Culture)
	{
		//DataTable dt = new DataTable("Stores");

		string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();

		SqlConnection objConnection = new SqlConnection(strConnection);
		string query = "proc_Wind_GetAllStores";

		objConnection.Open();

		SqlCommand objCommand = new SqlCommand(query, objConnection);
		objCommand.CommandType = CommandType.StoredProcedure;

		objCommand.Parameters.Add("@Culture", SqlDbType.VarChar).Value = Culture;

		SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
		DataSet objDataSet = new DataSet();
		objAdapter.Fill(objDataSet, "Stores");

		//objCommand.ExecuteScalar();

		objConnection.Close();

		return objDataSet;

	}
    // -----------Nkal Methods ------------------------------
    public static DataSet GetMerchantCustomFields(int MerchantID)
	{
		//DataTable dt = new DataTable("Stores");

		string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();

		SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_GetMerchantCustomFields";

		objConnection.Open();

		SqlCommand objCommand = new SqlCommand(query, objConnection);
		objCommand.CommandType = CommandType.StoredProcedure;

        objCommand.Parameters.Add("@MerchantID", SqlDbType.Int).Value = MerchantID;

		SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
		DataSet objDataSet = new DataSet();
		objAdapter.Fill(objDataSet, "MerchantCustomFields");

		//objCommand.ExecuteScalar();

		objConnection.Close();

		return objDataSet;

	}

    public static DataTable GetAllTheCards()
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_GetAllTheCards";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }


    public static DataSet GetMerchantLimits(int MerchantID, bool MerchantPublicSector)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_GetMerchantLimits";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@MerchantID", SqlDbType.Int).Value = MerchantID;
        objCommand.Parameters.Add("@MerchantPublicSector", SqlDbType.Bit).Value = MerchantPublicSector;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataSet objDataSet = new DataSet();
        objAdapter.Fill(objDataSet, "MerchantLimits");
        objConnection.Close();
        return objDataSet;

    }

    public static DataSet GetCustomFieldsMerchant(string CompanyName, string Culture)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_GetCustomFieldsMerchant";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = CompanyName;
        objCommand.Parameters.Add("@Culture", SqlDbType.NVarChar).Value = Culture;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataSet objDataSet = new DataSet();
        objAdapter.Fill(objDataSet);
        objConnection.Close();
        return objDataSet;

    }


    public static DataTable GetErrorMessages(int FormType)
	{
		string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
		SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_GetErrorMessages";
		objConnection.Open();
		SqlCommand objCommand = new SqlCommand(query, objConnection);
		objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@FormType", SqlDbType.Int).Value = FormType;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
		DataTable objDataTable = new DataTable();
		objAdapter.Fill(objDataTable);
		objConnection.Close();
		return objDataTable;
	}

    public static DataTable GetSecQuestion(int QuestID)
	{
		string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
		SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_GetSecQuestion";
		objConnection.Open();
		SqlCommand objCommand = new SqlCommand(query, objConnection);
		objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@QuestID", SqlDbType.Int).Value = QuestID;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
		DataTable objDataTable = new DataTable();
		objAdapter.Fill(objDataTable);
		objConnection.Close();
		return objDataTable;
	}


    public static DataTable GetESBError(String Culture,String ErrorCode)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_GetESBErrorCode";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@Culture", SqlDbType.VarChar, 10).Value = Culture;
        objCommand.Parameters.Add("@ErrorCode", SqlDbType.VarChar, 30).Value = ErrorCode;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }
    


    public static DataTable GetUserCards(int User_ID)
	{
		string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
		SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_GetUserCards";
		objConnection.Open();
		SqlCommand objCommand = new SqlCommand(query, objConnection);
		objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = User_ID;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
		DataTable objDataTable = new DataTable();
		objAdapter.Fill(objDataTable);
		objConnection.Close();
		return objDataTable;
	}

    public static DataTable GetUserCard(int User_ID, int CardID)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_GetUserCards";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = User_ID;
        objCommand.Parameters.Add("@UserCardID", SqlDbType.Int).Value = CardID;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable GetUserCardsForSearch(int User_ID)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_GetUserCards";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = User_ID;
        objCommand.Parameters.Add("@ForSearch", SqlDbType.Int).Value = 1;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable DeleteUserCards(int User_ID,String CardsForDelete)
	{
		string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
		SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_DeleteUserCards";
		objConnection.Open();
		SqlCommand objCommand = new SqlCommand(query, objConnection);
		objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = User_ID;
        objCommand.Parameters.Add("@UserCards", SqlDbType.NVarChar, 1000).Value = CardsForDelete;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
		DataTable objDataTable = new DataTable();
		objAdapter.Fill(objDataTable);
		objConnection.Close();
		return objDataTable;
	}

    public static DataTable GetAcknowledgementTransaction_ByMerchantID(int MerchantID)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_GetAcknowledgementTransaction_ByMerchantID";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@MerchantID", SqlDbType.Int).Value = MerchantID;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable GetMerchantByGUID(Guid TransID)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_GetTransactionByGUID";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@TransID", SqlDbType.UniqueIdentifier).Value = TransID;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable GetMerchantByID(int MerchantID, string Lang)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_GetMerchant";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@MerchantID", SqlDbType.Int).Value = MerchantID;
        objCommand.Parameters.Add("@Lang", SqlDbType.NVarChar, 10).Value = Lang;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable GetMerchantByESBID(String ESB_MerchantID, string Lang)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_GetMerchant";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@MerchantID", SqlDbType.Int).Value = 0;
        objCommand.Parameters.Add("@ESB_MerchantID", SqlDbType.VarChar, 30).Value = ESB_MerchantID;
        objCommand.Parameters.Add("@Lang", SqlDbType.NVarChar, 10).Value = Lang;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }



    public static DataTable InsertNewCard(int User_ID, String FriendlyName, int CardDescrID, String EncryptedCard, String FullName, String MonthExp, int YearExp, String LastForNo, String CardNumber)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_InsertUserCard";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;

        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = User_ID;
        objCommand.Parameters.Add("@FriendlyName", SqlDbType.NVarChar, 200).Value = FriendlyName;
        objCommand.Parameters.Add("@CardDescrID", SqlDbType.Int).Value = CardDescrID;
        objCommand.Parameters.Add("@EncryptedCard", SqlDbType.VarChar, 300).Value = EncryptedCard;
        objCommand.Parameters.Add("@FullName", SqlDbType.NVarChar, 200).Value = FullName;
        objCommand.Parameters.Add("@MonthExp", SqlDbType.Int).Value = MonthExp;
        objCommand.Parameters.Add("@YearExp", SqlDbType.Int).Value = YearExp;
        objCommand.Parameters.Add("@LastForNo", SqlDbType.VarChar, 4).Value = LastForNo;
        objCommand.Parameters.Add("@CardNumber", SqlDbType.VarChar, 20).Value = CardNumber;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }



    public static DataTable InsertCloseBatch(int MerchantID)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_InsertCloseBatch";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@MerchantID", SqlDbType.Int).Value = MerchantID;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable InsertOtherTypeTransaction(int OldTransID,decimal TransactionAmount, int IsCancel, int IsRefant, int IsCloseBatch)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_InsertOtherTypeTransaction";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@OldTransID", SqlDbType.Int).Value = OldTransID;
        objCommand.Parameters.Add("@TransactionAmount", SqlDbType.Decimal).Value = TransactionAmount;
        objCommand.Parameters.Add("@IsCancel", SqlDbType.Int).Value = IsCancel;
        objCommand.Parameters.Add("@IsRefant", SqlDbType.Int).Value = IsRefant;
        objCommand.Parameters.Add("@IsCloseBatch", SqlDbType.Int).Value = IsCloseBatch;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable InsertMerchantTransaction(int MerchantID, int User_ID, int UserCardID, String CardNo, String CardCustomerName, decimal TransactionAmount, String TransUserPhone, String TransUserEmail, String Inv_CompanyName, String Inv_Occupation, String Inv_Addrees, String Inv_TK, String Inv_City, String Inv_AFM, int Inv_TaxOfficeID, int CardDescrID, int TransStatus, Boolean TransPublic, String ClientIP, string UserAgent)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_InsertMerchantTransaction";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@MerchantID", SqlDbType.Int).Value = MerchantID;
        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = User_ID;
        objCommand.Parameters.Add("@UserCardID", SqlDbType.Int).Value = UserCardID;
        objCommand.Parameters.Add("@CardNo", SqlDbType.VarChar,300).Value = CardNo;
        objCommand.Parameters.Add("@CardCustomerName", SqlDbType.NVarChar, 150).Value = CardCustomerName;
        objCommand.Parameters.Add("@TransactionAmount", SqlDbType.Decimal).Value = TransactionAmount;
        objCommand.Parameters.Add("@TransStatus", SqlDbType.Int).Value = TransStatus;
        objCommand.Parameters.Add("@TransUserPhone", SqlDbType.VarChar, 10).Value = TransUserPhone;
        objCommand.Parameters.Add("@TransUserEmail", SqlDbType.VarChar,150).Value = TransUserEmail;
        objCommand.Parameters.Add("@Inv_CompanyName", SqlDbType.NVarChar, 250).Value = Inv_CompanyName;
        objCommand.Parameters.Add("@Inv_Occupation", SqlDbType.NVarChar, 150).Value = Inv_Occupation;
        objCommand.Parameters.Add("@Inv_Addrees", SqlDbType.NVarChar, 250).Value = Inv_Addrees;
        objCommand.Parameters.Add("@Inv_TK", SqlDbType.VarChar,5).Value = Inv_TK;
        objCommand.Parameters.Add("@Inv_City", SqlDbType.NVarChar, 100).Value = Inv_City;
        objCommand.Parameters.Add("@Inv_AFM", SqlDbType.VarChar,10).Value = Inv_AFM;
        objCommand.Parameters.Add("@Inv_TaxOfficeID", SqlDbType.Int).Value = Inv_TaxOfficeID;
        objCommand.Parameters.Add("@CardDescrID", SqlDbType.Int).Value = CardDescrID;
        objCommand.Parameters.Add("@TransPublic", SqlDbType.Bit).Value = TransPublic;
        objCommand.Parameters.Add("@ClientIP", SqlDbType.NVarChar, 15).Value = ClientIP;
        objCommand.Parameters.Add("@UserAgent", SqlDbType.NVarChar).Value = UserAgent;
        
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable UpdateTransaction(int TransID,int TransStatus,int IsFavorite)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_UpdateTransaction";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@TransID", SqlDbType.Int).Value = TransID;
        objCommand.Parameters.Add("@Status", SqlDbType.Int).Value = TransStatus;
        objCommand.Parameters.Add("@IsFavorite", SqlDbType.Int).Value = IsFavorite;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static void InsertFavoritesCustomFields(int MerchantTransactionID, int FieldsTransactionID, string value)
    {

        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_InsertFavoritesCustomFields";

        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@TransactionID", SqlDbType.Int).Value = MerchantTransactionID;
        objCommand.Parameters.Add("@FieldID", SqlDbType.Int).Value = FieldsTransactionID;
        objCommand.Parameters.Add("@Value", SqlDbType.NVarChar).Value = value;

        try
        {
            objConnection.Open();
            objCommand.ExecuteNonQuery();
        }
        catch (Exception)
        {
            objConnection.Close();
        }

    }

    public static DataTable GetFavoritesCustomFields(int MerchantTransactionID, int FieldsTransactionID, int userID)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_GetFavoritesCustomFields";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@TransactionID", SqlDbType.Int).Value = MerchantTransactionID;
        objCommand.Parameters.Add("@FieldID", SqlDbType.Int).Value = FieldsTransactionID;
        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = userID;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable UpdateTransactionWithStatusMes(int TransID, int TransStatus, int IsFavorite, String StatusMessage, String PublicTransID)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_UpdateTransaction";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@TransID", SqlDbType.Int).Value = TransID;
        objCommand.Parameters.Add("@Status", SqlDbType.Int).Value = TransStatus;
        objCommand.Parameters.Add("@IsFavorite", SqlDbType.Int).Value = IsFavorite;
        objCommand.Parameters.Add("@StatusMessage", SqlDbType.NVarChar, 2000).Value = StatusMessage;
        objCommand.Parameters.Add("@PublicTransID", SqlDbType.NVarChar, 131).Value = PublicTransID;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable UpdateTransaction(int TransID, int IsCancel, int IsRefant, int IsCloseBatch)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_UpdateTransaction";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@TransID", SqlDbType.Int).Value = TransID;
        objCommand.Parameters.Add("@IsCancel", SqlDbType.Int).Value = IsCancel;
        objCommand.Parameters.Add("@IsRefant", SqlDbType.Int).Value = IsRefant;
        objCommand.Parameters.Add("@IsCloseBatch", SqlDbType.Int).Value = IsCloseBatch;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataTable GetTrasactionsByUser(int User_ID, int Status,int IsFavorite, string Lang)
	{
		string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
		SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_GetTrasactionsByUser";
		objConnection.Open();
		SqlCommand objCommand = new SqlCommand(query, objConnection);
		objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = User_ID;
        objCommand.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
        objCommand.Parameters.Add("@IsFavorite", SqlDbType.Int).Value = IsFavorite;
        objCommand.Parameters.Add("@Lang", SqlDbType.NVarChar, 10).Value = Lang;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
		DataTable objDataTable = new DataTable();
		objAdapter.Fill(objDataTable);
		objConnection.Close();
		return objDataTable;
	}


    public static DataTable GetMerchantCardType(int CardID, String CardNo)
	{
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_GetMerchantCardsLimits";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@SavedCardID", SqlDbType.Int).Value = CardID;
        objCommand.Parameters.Add("@CardNo", SqlDbType.VarChar, 300).Value = CardNo;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
	}

    public static DataTable GetMerchantTransaction(int User_ID, int MerchantID, int CardID,String CardNo,int Status,Boolean PerDay)
	{
		string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
		SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "Proc_LivePay_GetMerchantTransaction";
		objConnection.Open();
		SqlCommand objCommand = new SqlCommand(query, objConnection);
		objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@MerchantID", SqlDbType.Int).Value = MerchantID;
        objCommand.Parameters.Add("@UserID", SqlDbType.Int).Value = User_ID;
        objCommand.Parameters.Add("@SavedCardID", SqlDbType.Int).Value = CardID;
        objCommand.Parameters.Add("@CardNo", SqlDbType.VarChar,300).Value = CardNo;
        objCommand.Parameters.Add("@TransStatus", SqlDbType.Int).Value = Status;
        objCommand.Parameters.Add("@PerDay", SqlDbType.Bit).Value = PerDay;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
		DataTable objDataTable = new DataTable();
		objAdapter.Fill(objDataTable);
		objConnection.Close();
		return objDataTable;
	}

    public static DataTable GetTaxOffices()
	{
		string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
		SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "LivePay_GetTaxOffices";
		objConnection.Open();
		SqlCommand objCommand = new SqlCommand(query, objConnection);
		objCommand.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
		DataTable objDataTable = new DataTable();
		objAdapter.Fill(objDataTable);
		objConnection.Close();
		return objDataTable;
	}

    /// <summary>
    /// Get merchant
    /// </summary>
    /// <param name="ID"></param>
    /// <param name="Longitude"></param>
    /// <param name="Latitude"></param>
    public static DataSet SearchMerchant(string Procedure, string CompanyName, string Culture)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();

        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = Procedure;

        objConnection.Open();

        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;

        objCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = CompanyName;
        objCommand.Parameters.Add("@Culture", SqlDbType.NVarChar).Value = Culture;

        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataSet objDataSet = new DataSet();
        objAdapter.Fill(objDataSet, "SearchMerchant");

        //objCommand.ExecuteScalar();

        objConnection.Close();

        return objDataSet;
    }

    public static DataSet SearchMerchant(string Procedure, string CompanyName, string NodePath, int nomos, int area, string Culture)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();

        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = Procedure;

        objConnection.Open();

        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;

        objCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = CompanyName;
        objCommand.Parameters.Add("@NodePath", SqlDbType.NVarChar, 450).Value = NodePath;
        objCommand.Parameters.Add("@nomos", SqlDbType.Int).Value = nomos;
        objCommand.Parameters.Add("@area", SqlDbType.Int).Value = area;
        objCommand.Parameters.Add("@Culture", SqlDbType.NVarChar).Value = Culture;

        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataSet objDataSet = new DataSet();
        objAdapter.Fill(objDataSet, "SearchMerchant");

        //objCommand.ExecuteScalar();

        objConnection.Close();

        return objDataSet;
    }

    public static DataSet SearchMerchant(string Procedure, string CompanyName, string NodePath, int nomos, int area, string Culture, int SearchType)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();

        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = Procedure;

        objConnection.Open();

        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;

        objCommand.Parameters.Add("@CompanyName", SqlDbType.NVarChar).Value = CompanyName;
        objCommand.Parameters.Add("@NodePath", SqlDbType.NVarChar).Value = NodePath;
        objCommand.Parameters.Add("@nomos", SqlDbType.Int).Value = nomos;
        objCommand.Parameters.Add("@area", SqlDbType.Int).Value = area;
        objCommand.Parameters.Add("@Culture", SqlDbType.NVarChar).Value = Culture;
        objCommand.Parameters.Add("@SearchType", SqlDbType.Int).Value = SearchType;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataSet objDataSet = new DataSet();
        objAdapter.Fill(objDataSet, "SearchMerchant");

        //objCommand.ExecuteScalar();

        objConnection.Close();

        return objDataSet;
    }

    /// <summary>
    /// inline control Banner
    /// </summary>
    /// <param name="ParentID"></param>
    /// <param name="Culture"></param>
    /// <param name="TableName"></param>
    /// <param name="WindProcedure"></param>
    /// <returns></returns>
    public static DataSet GetNodesByGeneric(int ParentID, string Culture, string TableName, string WindProcedure)
    {
        //DataTable dt = new DataTable("Stores");

        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();

        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = WindProcedure;

        objConnection.Open();

        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;

        objCommand.Parameters.Add("@NodeParentID", SqlDbType.Int).Value = ParentID;
        objCommand.Parameters.Add("@Culture", SqlDbType.NVarChar).Value = Culture;

        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataSet objDataSet = new DataSet();
        objAdapter.Fill(objDataSet, TableName);

        //objCommand.ExecuteScalar();

        objConnection.Close();

        return objDataSet;

    }


    public static DataTable UdateTallkCounter(int TalkID)
    {
        string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "TedX_UdateTallkCounter";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;
        objCommand.Parameters.Add("@TalkID", SqlDbType.Int).Value = TalkID;
        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataTable objDataTable = new DataTable();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;
    }

    public static DataSet GetNodesMenuLevel2(string SiteName, string cultureCode, string className, int NodeLevel, int NodeParentID)
    {
        //DataTable dt = new DataTable("Stores");

         string strConnection = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        SqlConnection objConnection = new SqlConnection(strConnection);
        string query = "TableMenu_GetNodesMenuLevel2";
        objConnection.Open();
        SqlCommand objCommand = new SqlCommand(query, objConnection);
        objCommand.CommandType = CommandType.StoredProcedure;

        objCommand.Parameters.Add("@SiteName", SqlDbType.NVarChar).Value = SiteName;
        objCommand.Parameters.Add("@cultureCode", SqlDbType.NVarChar).Value = cultureCode;
        objCommand.Parameters.Add("@className", SqlDbType.NVarChar).Value = className;
        objCommand.Parameters.Add("@NodeLevel", SqlDbType.Int).Value = NodeLevel;
        objCommand.Parameters.Add("@NodeParentID", SqlDbType.Int).Value = NodeParentID;
        

        SqlDataAdapter objAdapter = new SqlDataAdapter(objCommand);
        DataSet objDataTable = new DataSet();
        objAdapter.Fill(objDataTable);
        objConnection.Close();
        return objDataTable;

   

    }

    public static DataTable GetMerchantNodeID(int LivePayID)
    {
        DataTable dt = new DataTable();

        // Open the connection 
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        string storedProcedureName = "LivePay_GetMerchantNodeID";

        using (SqlConnection cnn = new SqlConnection(connectionString))
        {
            cnn.Open();

            // Define the command 
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cnn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedureName;
                cmd.Parameters.Add("@LivePayID", SqlDbType.Int).Value = LivePayID;

                // Define the data adapter and fill the dataset 
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);
                }
            }
        }
        return dt;
    }

    public static DataTable GetInvoice(int transactionID)
    {
        DataTable dt = new DataTable();

        // Open the connection 
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        string storedProcedureName = "LivePay_GetInvoice";

        using (SqlConnection cnn = new SqlConnection(connectionString))
        {
            cnn.Open();

            // Define the command 
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cnn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = storedProcedureName;
                cmd.Parameters.Add("@TransactionID", SqlDbType.Int).Value = transactionID;

                // Define the data adapter and fill the dataset 
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);
                    
                }
            }
        }
        return dt;
    }

}