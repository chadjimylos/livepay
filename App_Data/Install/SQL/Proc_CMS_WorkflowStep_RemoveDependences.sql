CREATE PROCEDURE [Proc_CMS_WorkflowStep_RemoveDependences]
	@StepID int
AS
BEGIN
	-- Remove step roles
	DELETE FROM CMS_WorkflowStepRoles WHERE StepID = @StepID;
	-- Clear the documents steps
	UPDATE CMS_Document SET DocumentWorkflowStepID = NULL WHERE DocumentWorkflowStepID = @StepID;
	-- Clear step in workflow history
	UPDATE CMS_WorkflowHistory SET StepID = NULL WHERE StepID = @StepID
END
