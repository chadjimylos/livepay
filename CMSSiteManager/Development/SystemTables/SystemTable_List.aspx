<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SystemTable_List.aspx.cs"
    Inherits="CMSSiteManager_Development_SystemTables_SystemTable_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="System tables - List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:UniGrid ID="UniGridSysTables" runat="server" GridName="SystemTable_List.xml"
        WhereCondition="ClassShowAsSystemTable = 1" OrderBy="ClassDisplayName" Columns="ClassID, ClassDisplayName, ClassName, ClassTableName"
        IsLiveSite="false" />
</asp:Content>
