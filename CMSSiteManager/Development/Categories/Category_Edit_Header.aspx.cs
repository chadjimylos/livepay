using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.SiteProvider;

public partial class CMSSiteManager_Development_Categories_Category_Edit_Header : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string currentCategory = "";

        int categoryId = QueryHelper.GetInteger("categoryid", 0);
        CategoryInfo ci = CategoryInfoProvider.GetCategoryInfo(categoryId);
        if (ci != null)
        {
            currentCategory = ResHelper.LocalizeString(ci.CategoryDisplayName);
        }

        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("Category_Edit.ItemListLink");
        breadcrumbs[0, 1] = "~/CMSSiteManager/Development/Categories/Category_List.aspx";
        breadcrumbs[0, 2] = "_parent";
        breadcrumbs[1, 0] = HTMLHelper.HTMLEncode(currentCategory);
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Category_Edit.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Category/object.png");
        this.CurrentMaster.Title.HelpTopicName = "category_general";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        string[,] tabs = new string[8, 4];
        tabs[0, 0] = ResHelper.GetString("general.general");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'category_general');";
        tabs[0, 2] = "Category_Edit_General.aspx?categoryID=" + categoryId;
        tabs[1, 0] = ResHelper.GetString("Category_Edit.Documents");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'category_documents');";
        tabs[1, 2] = "Category_Edit_Documents.aspx?categoryID=" + categoryId;

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "content";
    }
}
