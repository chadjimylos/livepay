using System;
using System.Web.UI.WebControls;
using CMS.DataEngine;
using CMS.GlobalHelper;
using CMS.IDataConnectionLibrary;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_AdminControls_Controls_Class_QueryEdit : CMSUserControl
{
    #region "Private fields"

    private int mQueryID = 0;
    private int mClassID = 0;
    private bool mShowHelp = true;
    private bool mDevelopmentMode = false;

    private string mClassName = null;
    private string mQueryName = ResHelper.GetString("DocumentType_Edit_Query_Edit.NewQueryName");
    private Query mQuery = null;

    private string mRefreshPageURL = null;
    private string mQueryParamID = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// ID of the current query
    /// </summary>
    public int QueryID
    {
        get
        {
            return mQueryID;
        }
        set
        {
            mQueryID = value;
        }
    }


    /// <summary>
    /// ID of the class current query belongs to
    /// </summary>
    public int ClassID
    {
        get
        {
            return mClassID;
        }
        set
        {
            mClassID = value;
        }
    }


    /// <summary>
    /// Indicates whether the help should be displayed in the bottom of the page
    /// </summary>
    public bool ShowHelp
    {
        get
        {
            return mShowHelp;
        }
        set
        {
            mShowHelp = value;
        }
    }


    /// <summary>
    /// Page that should be refreshed after button click
    /// </summary>
    public string RefreshPageURL
    {
        get
        {
            return mRefreshPageURL;
        }
        set
        {
            mRefreshPageURL = value;
        }
    }


    /// <summary>
    /// Parameter name for new ID
    /// </summary>
    public string QueryParamID
    {
        set
        {
            mQueryParamID = value;
        }
    }


    /// <summary>
    /// Development mode
    /// </summary>
    public bool DevelopmentMode
    {
        get
        {
            return mDevelopmentMode;
        }
        set
        {
            mDevelopmentMode = value;
        }
    }

    #endregion


    #region "Private properties"

    /// <summary>
    /// Query to edit
    /// </summary>
    private Query Query
    {
        get
        {
            return mQuery;
        }
        set
        {
            mQuery = value;
        }
    }


    /// <summary>
    /// Name of the class query belongs to
    /// </summary>
    private string ClassName
    {
        get
        {
            return mClassName;
        }
        set
        {
            mClassName = value;
        }
    }


    /// <summary>
    /// Name of the actual query
    /// </summary>
    private string QueryName
    {
        get
        {
            return mQueryName;
        }
        set
        {
            mQueryName = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize the control
        SetupControl();

        if (!RequestHelper.IsPostBack())
        {
            // Shows that the query was created or updated successfully
            if (QueryHelper.GetInteger("saved", 0) == 1)
            {
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
        }
    }


    #region "Event handlers"

    /// <summary>
    /// Generate default query
    /// </summary>
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        chbAutoUpdates.Checked = false;
        rblQueryType.SelectedIndex = 0;
        try
        {
            txtQueryText.Text = SqlGenerator.GetSqlQuery(ClassName, SqlGenerator.GetQueryType(QueryName), null);
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message;
        }
    }


    /// <summary>
    /// Saves data of edited or new query into DB.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Validate query name for emptyness and code name format
        string result = new Validator().NotEmpty(txtQueryName.Text.Trim(), ResHelper.GetString("DocumentType_Edit_Query_Edit.QueryNameRequired"))
            .IsIdentificator(txtQueryName.Text.Trim(), ResHelper.GetString("DocumentType_Edit.QueryNameFormat"))
            .Result;

        // If the entries were succesfully validated
        if (result == "")
        {
            bool isSaved = false;

            // Gets full query name in the form - "ClassName.QueryName"
            string fullQueryName = !string.IsNullOrEmpty(ClassName) ? ClassName + "." + txtQueryName.Text : txtQueryName.Text;

            // Finds whether new or edited query is unique in the document type
            Query newQuery = QueryProvider.GetQueryFromDB(fullQueryName, null);

            // If query with specified name already exist
            if (newQuery != null)
            {
                // Check if it is the same query as one being edited
                if (newQuery.QueryId == QueryID)
                {
                    Query = newQuery;
                    SaveQuery();
                    isSaved = true;
                }
                else
                {
                    // The new query tries to use the same code name
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("sysdev.queryedit_ascx.UniqueQueryName");
                }
            }
            else
            {
                // Query with specified name doesn't exist yet
                SaveQuery();
                isSaved = true;
            }

            // If the query was successfully saved
            if (RefreshPageURL == null)
            {
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
            else
            {
                if (isSaved)
                {
                    UrlHelper.Redirect(RefreshPageURL + "?queryid=" + Query.QueryId + "&saved=1");
                }
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes the controls
    /// </summary>
    private void SetupControl()
    {
        chckIsCustom.Enabled = !DevelopmentMode;

        // If the existing query is being edited
        if (QueryID > 0)
        {
            // Get information on existing query
            Query = QueryProvider.GetQuery(QueryID);
            if (Query != null)
            {
                ClassName = DataClassInfoProvider.GetClassName(Query.QueryClassId);
                QueryName = Query.QueryName;

                if (!RequestHelper.IsPostBack())
                {
                    // Fills form with the existing query information
                    txtQueryName.Text = QueryName;
                    rblQueryType.SelectedValue = Query.QueryType.ToString();
                    chbTransaction.Checked = Query.RequiresTransaction;
                    chbAutoUpdates.Checked = Query.QueryIsLocked;
                    txtQueryText.Text = Query.QueryText;
                    drpGeneration.Value = Query.QueryLoadGeneration;
                    chckIsCustom.Checked = Query.QueryIsCustom;

                    // Get focus on query text
                    txtQueryText.Focus();
                }
            }
        }
        // New query is being created
        else if (ClassID > 0)
        {
            ClassName = DataClassInfoProvider.GetClassName(ClassID);
            txtQueryName.Focus();
        }

        // Ensure generate button for default queries
        if (SqlGenerator.IsSystemQuery(QueryName))
        {
            btnGenerate.Visible = true;
        }

        // Initialize the labels
        plcLoadGeneration.Visible = SettingsKeyProvider.DevelopmentMode;
        plcIsCustom.Visible = SettingsKeyProvider.DevelopmentMode;

        // Initialize the validator
        RequiredFieldValidatorQueryName.ErrorMessage = ResHelper.GetString("DocumentType_Edit_Query_Edit.QueryNameRequired");

        if (ShowHelp)
        {
            DisplayHelperTable();
        }
    }


    /// <summary>
    /// Displays helper table with transformation examples.
    /// </summary>
    private void DisplayHelperTable()
    {
        // 0 - left column (example), 1 - right column (explanation)
        string[,] tableColumns = new string[4, 2];

        int i = 0;

        //transformation expression examples
        tableColumns[i, 0] = "##WHERE##";
        tableColumns[i++, 1] = ResHelper.GetString("DocumentType_Edit_Queries.Help_WHERE");

        tableColumns[i, 0] = "##TOPN##";
        tableColumns[i++, 1] = ResHelper.GetString("DocumentType_Edit_Queries.Help_TOPN");

        tableColumns[i, 0] = "##ORDERBY##";
        tableColumns[i++, 1] = ResHelper.GetString("DocumentType_Edit_Queries.Help_ORDERBY");

        tableColumns[i, 0] = "##COLUMNS##";
        tableColumns[i, 1] = ResHelper.GetString("DocumentType_Edit_Queries.Help_COLUMNS");

        // Add header row
        AddRow(ResHelper.GetString("DocumentType_Edit_Queries.HelpCaption"), ResHelper.GetString("DocumentType_Edit_Queries.HelpValue"), true, -1);

        for (i = 0; i < tableColumns.GetUpperBound(0) + 1; i++)
        {
            AddRow(tableColumns[i, 0], tableColumns[i, 1], false, i);
        }

        lblHelpHeader.Text = ResHelper.GetString("DocumentType_Edit_Queries.Help_Header");
    }


    private void AddRow(string caption, string value, bool header, int rowIndex)
    {
        TableRow tRow = new TableRow();
        if (header)
        {
            tRow.TableSection = TableRowSection.TableHeader;
            tRow.CssClass = "UniGridHead";
        }
        else
        {
            tRow.CssClass = ((rowIndex % 2) == 0) ? "EvenRow" : "OddRow";
        }
        TableCell leftCell = new TableCell();
        TableCell rightCell = new TableCell();
        Label lblExample = new Label();
        Label lblExplanation = new Label();

        // init labels
        lblExample.Text = caption;
        lblExplanation.Text = value;

        // add labels to the cells
        leftCell.Controls.Add(lblExample);
        rightCell.Controls.Add(lblExplanation);

        leftCell.Wrap = false;

        // add cells to the row
        tRow.Cells.Add(leftCell);
        tRow.Cells.Add(rightCell);

        // add row to the table
        tblHelp.Rows.Add(tRow);
    }


    /// <summary>
    /// Saves new or edited query of the given name and returns to the query list.
    /// </summary>
    private void SaveQuery()
    {
        // The query ID was specified, edit existing
        Query newQuery = QueryID > 0 ? QueryProvider.GetQuery(QueryID) : new Query();

        // Sets query object's properties
        newQuery.QueryName = txtQueryName.Text;
        newQuery.QueryClassId = DataClassInfoProvider.GetDataClass(ClassName).ClassID;

        // Check the query type
        newQuery.QueryType = rblQueryType.SelectedValue == "SQLQuery" ? QueryTypeEnum.SQLQuery : QueryTypeEnum.StoredProcedure;

        newQuery.RequiresTransaction = chbTransaction.Checked;
        newQuery.QueryIsLocked = chbAutoUpdates.Checked;
        newQuery.QueryText = txtQueryText.Text;
        newQuery.QueryLoadGeneration = drpGeneration.Value;
        newQuery.QueryIsCustom = chckIsCustom.Checked;

        // Insert new / update existing query
        QueryProvider.SetQuery(newQuery);

        Query = newQuery;
    }

    #endregion
}
