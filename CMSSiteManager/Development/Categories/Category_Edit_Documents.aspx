<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="Category_Edit_Documents.aspx.cs" Inherits="CMSSiteManager_Development_Categories_Category_Edit_Documents"
    Title="Category - Edit documents" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<%@ Register Src="~/CMSFormControls/Filters/DocumentFilter.ascx" TagName="DocumentFilter"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" ContentPlaceHolderID="plcContent" runat="server">
    <cms:DocumentFilter ID="filterDocuments" runat="server" LoadSites="true" />
    <br />
    <br />
    <cms:CMSUpdatePanel runat="server" ID="pnlUpdate" UpdateMode="Always">
        <ContentTemplate>
            <cms:LocalizedLabel ID="lblTitle" runat="server" ResourceString="categories_edit.documents.used"
                DisplayColon="true" EnableViewState="false" />
            <br />
            <br />
            <cms:UniGrid ID="uniGridDocs" runat="server" GridName="Category_Edit_Documents.xml"
                HideControlForZeroRows="false" IsLiveSite="false" />
        </ContentTemplate>
    </cms:CMSUpdatePanel>
</asp:Content>
