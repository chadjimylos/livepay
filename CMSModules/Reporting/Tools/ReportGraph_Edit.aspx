<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportGraph_Edit.aspx.cs"
    Inherits="CMSModules_Reporting_Tools_ReportGraph_Edit" Theme="Default" ValidateRequest="false"
    EnableEventValidation="false" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master"
    Title="ReportGraph Edit" %>

<%@ Register Src="~/CMSModules/Reporting/Controls/FontSpec.ascx" TagName="FontSpec"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Reporting/Controls/GradientControl.ascx" TagName="Gradient"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <div class="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <asp:Panel runat="server" ID="pnlElements">
            <table style="vertical-align: top; width: 100%;">
                <tr>
                    <td colspan="2">
                        <table style="width: 100%">
                            <tr>
                                <td class="FieldLabel" style="width: 110px">
                                    <cms:LocalizedLabel ID="lblDisplayName" runat="server" ResourceString="general.displayname"
                                        DisplayColon="true" />
                                </td>
                                <td class="FieldLabel">
                                    <asp:TextBox ID="txtDisplayName" runat="server" MaxLength="450" CssClass="TextBoxField" />
                                    <br />
                                    <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    <cms:LocalizedLabel ID="lblCodeName" runat="server" ResourceString="general.codename"
                                        DisplayColon="true" />
                                </td>
                                <td class="FieldLabel">
                                    <asp:TextBox ID="txtCodeName" runat="server" MaxLength="100" CssClass="TextBoxField" />
                                    <br />
                                    <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtCodeName"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    <asp:Label ID="lblQuery" runat="server" />
                                </td>
                                <td class="FieldLabel">
                                    <asp:TextBox ID="txtQuery" TextMode="MultiLine" runat="server" Width="100%" Height="200" />
                                    <br />
                                    <asp:RequiredFieldValidator ID="rfvQuery" runat="server" ControlToValidate="txtQuery"
                                        Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    <asp:Label ID="lblStoredProcedure" runat="server" />
                                </td>
                                <td class="FieldLabel">
                                    <asp:CheckBox ID="chkStoredProcedure" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="vertical-align: top;">
                    <td style="padding-top: 10px;">
                        <table cellpadding="2" cellspacing="0">
                            <tr>
                                <td class="FieldLabel">
                                    <asp:Label ID="lblGraphType" runat="server" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpGraphType" runat="server" CssClass="DropDownField" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                </td>
                                <td class="FieldLabel">
                                    <br />
                                    <table cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkVerticalBars" runat="server" Checked="true" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkReverseYAxis" runat="server" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkTenPowers" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkShowMajorGrid" runat="server" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkFillCurves" runat="server" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkSmoothCurves" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    <asp:Label ID="lblGraphTitle" runat="server" />
                                </td>
                                <td class="FieldLabel">
                                    <asp:TextBox ID="txtGraphTitle" runat="server" CssClass="TextBoxField" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    <asp:Label ID="lblGraphXAxisTitle" runat="server" />
                                </td>
                                <td class="FieldLabel">
                                    <asp:TextBox ID="txtGraphXAxisTitle" runat="server" CssClass="TextBoxField" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    <asp:Label ID="lblGraphYAxisTitle" runat="server" />
                                </td>
                                <td class="FieldLabel">
                                    <asp:TextBox ID="txtGraphYAxisTitle" runat="server" CssClass="TextBoxField" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    <asp:Label ID="lblGraphWidth" runat="server" />
                                </td>
                                <td class="FieldLabel">
                                    <asp:TextBox ID="txtGraphWidth" runat="server" CssClass="TextBoxField" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    <asp:Label ID="lblGraphHeight" runat="server" />
                                </td>
                                <td class="FieldLabel">
                                    <asp:TextBox ID="txtGraphHeight" runat="server" CssClass="TextBoxField" />
                                </td>
                            </tr>
                            <tr>
                                <td class="FieldLabel">
                                    <asp:Label ID="lblGraphLegendPosition" runat="server" />
                                </td>
                                <td class="FieldLabel">
                                    <asp:DropDownList ID="drpGraphLegendPosition" runat="server" CssClass="DropDownField" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="padding-top: 10px;">
                        <table cellpadding="2" cellspacing="0" style="white-space: nowrap">
                            <tr>
                                <td>
                                    <asp:Label ID="lblTitleFont" runat="server" />
                                </td>
                                <td colspan="3">
                                    <cms:FontSpec ID="titleFont" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAxisFont" runat="server" />
                                </td>
                                <td colspan="3">
                                    <cms:FontSpec ID="axisFont" runat="server" />
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblXAxisAngle" runat="server" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtXAxisAngle" runat="server" CssClass="VerySmallTextBox" />
                                </td>
                                <td>
                                    <asp:Label ID="lblYAxisAngle" runat="server" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtYAxisAngle" runat="server" CssClass="VerySmallTextBox" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblScaleMin" runat="server" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtScaleMin" runat="server" CssClass="VerySmallTextBox" />
                                </td>
                                <td>
                                    <asp:Label ID="lblScaleMax" runat="server" />
                                </td>
                                <td style="width: 100%;">
                                    <asp:TextBox ID="txtScaleMax" runat="server" CssClass="VerySmallTextBox" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblColors" runat="server" />
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtColors" runat="server" CssClass="TextBoxField" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblSymbols" runat="server" />
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtSymbols" runat="server" CssClass="TextBoxField" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 10px;">
                                    <asp:Label ID="lblGraphGradient" runat="server" />
                                </td>
                                <td colspan="3" style="padding-top: 10px;">
                                    <cms:Gradient ID="graphGradient" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblChartGradient" runat="server" />
                                </td>
                                <td colspan="3">
                                    <cms:Gradient ID="chartGradient" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblItemGradient" runat="server" />
                                </td>
                                <td colspan="3">
                                    <cms:Gradient ID="itemGradient" DisableToColorField="true" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:HiddenField ID="txtNewGraphHidden" runat="server" />
        <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
    </div>
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click" /><cms:CMSButton
            ID="btnCancel" runat="server" OnClientClick="wopener.ReloadPage(); window.close(); return false;"
            CssClass="SubmitButton" /><cms:CMSButton ID="btnPreview" runat="server" CssClass="LongSubmitButton"
                OnClick="btnPreview_Click" />
    </div>
</asp:Content>
