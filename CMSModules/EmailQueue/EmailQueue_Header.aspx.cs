using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSModules_EmailQueue_EmailQueue_Header : SiteManagerPage
{
    int siteId = 0;
    int selectedTab = 0;

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        this["TabControl"] = BasicTabControlMenu;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RequestHelper.IsPostBack())
        {
            siteId = QueryHelper.GetInteger("siteid", -1);
            selectedTab = QueryHelper.GetInteger("selectedTab", 0);
        }

        // Set site selector
        siteSelector.OnlyRunningSites = false;
        siteSelector.DropDownSingleSelect.AutoPostBack = true;
        siteSelector.UniSelector.SpecialFields = new string[1, 2] { { ResHelper.GetString("general.global"), "0" } };
        siteSelector.UniSelector.OnSelectionChanged += new EventHandler(UniSelector_OnSelectionChanged);

        if (!RequestHelper.IsPostBack())
        {
            siteSelector.Value = siteId;
        }
        else
        {
            siteId = ValidationHelper.GetInteger(siteSelector.Value, -1);
        }


        ScriptHelper.RegisterStartupScript(this, typeof(string), "selectedSiteId", ScriptHelper.GetScript("var selectedSiteId = " + siteId + ";"));

        BasicTabControlMenu.Tabs = new string[3, 4];
        BasicTabControlMenu.Tabs[0, 0] = ResHelper.GetString("emailqueue.queue.title");
        BasicTabControlMenu.Tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'email_queue'); return false;";
        BasicTabControlMenu.Tabs[0, 2] = "EmailQueue.aspx?siteid=' + selectedSiteId+'";
        BasicTabControlMenu.Tabs[1, 0] = ResHelper.GetString("emailqueue.archive.title");
        BasicTabControlMenu.Tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'sent_emails'); return false;";
        BasicTabControlMenu.Tabs[1, 2] = "SentEmails.aspx?siteid='+ selectedSiteId+'";
        BasicTabControlMenu.Tabs[2, 0] = ResHelper.GetString("emailqueue.send.title");
        BasicTabControlMenu.Tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'send_email'); return false;";
        BasicTabControlMenu.Tabs[2, 2] = "SendEmail.aspx?siteid='+ selectedSiteId+'";
        BasicTabControlMenu.UrlTarget = "content";
        BasicTabControlMenu.SelectedTab = selectedTab;


        titleElem.TitleText = ResHelper.GetString("emailqueue.queue.title");
        titleElem.TitleImage = GetImageUrl("CMSModules/CMS_EmailQueue/module.png");

        titleElem.HelpTopicName = "email_queue";
        titleElem.HelpName = "helpTopic";
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        ScriptHelper.RegisterStartupScript(this.Page, typeof(string), "OnSelectedIndex", ScriptHelper.GetScript(
            "function OnSelectedIndex() {\n" +
            "selectedTab = document.getElementById('TabControlSelItemNo');\n" +
            "if ((selectedTab != null) && (typeof(selectedSiteId) != 'undefined')) {\n" +
            "//alert(selectedSiteId);\n" +
            "   if (selectedTab.value == '0') {\n" +
            "     parent.frames['content'].location = 'EmailQueue.aspx?siteid=' + selectedSiteId;\n" +
            "   } else if (selectedTab.value == '1') {\n" +
            "     parent.frames['content'].location = 'SentEmails.aspx?siteid='+ selectedSiteId;\n" +
            "   } else {\n" +
            "     parent.frames['content'].location = 'SendEmail.aspx?siteid='+ selectedSiteId;\n" +
            "   }\n" +
            "}}\n" +
            
            "OnSelectedIndex();"));
    }
}