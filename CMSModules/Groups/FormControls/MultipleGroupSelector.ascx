﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultipleGroupSelector.ascx.cs"
    Inherits="CMSModules_Groups_FormControls_MultipleGroupSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ObjectType="community.group" SelectionMode="Multiple" ReturnColumnName="GroupName"
            OrderBy="GroupDisplayName" ResourcePrefix="groups" AllowEmpty="false" runat="server"
            ID="usGroups" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
