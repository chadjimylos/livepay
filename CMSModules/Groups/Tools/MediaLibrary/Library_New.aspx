<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Library_New.aspx.cs"
  MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Media library - New library"
  Inherits="CMSModules_Groups_Tools_MediaLibrary_Library_New" Theme="Default" %>

<%@ Register Src="~/CMSModules/MediaLibrary/Controls/UI/MediaLibraryEdit.ascx" TagName="LibraryEdit"
    TagPrefix="cms" %>
      
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:LibraryEdit ID="elemEdit" runat="server" />
</asp:Content>


