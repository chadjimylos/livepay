using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.EmailEngine;
using CMS.SettingsProvider;

public partial class CMSModules_EmailQueue_EmailQueue_Details : CMSModalSiteManagerPage
{
    #region "Protected variables"

    protected int emailId = 0;
    protected int prevId = 0;
    protected int nextId = 0;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (QueryHelper.ValidateHash("hash", "emailid"))
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("emailqueue.details.title");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_EmailQueue/emaildetail.png");

            // Get the ORDER BY column and starting event ID
            string orderBy = QueryHelper.GetString("orderby", "EmailID DESC");
            string whereCondition = HttpUtility.UrlDecode(QueryHelper.GetString("where", "")).Replace("%26", "&").Replace("%23", "#");

            // Get e-mail ID from query string
            emailId = QueryHelper.GetInteger("emailid", 0);

            if (!RequestHelper.IsPostBack())
            {
                LoadData();
            }

            // Initialize next/previous buttons
            int[] prevNext = EmailInfoProvider.GetPreviousNext(emailId, whereCondition, orderBy);
            if (prevNext != null)
            {
                prevId = prevNext[0];
                nextId = prevNext[1];

                btnPrevious.Enabled = (prevId != 0);
                btnNext.Enabled = (nextId != 0);

                btnPrevious.Click += btnPrevious_Click;
                btnNext.Click += btnNext_Click;
            }

            // Set button caption
            btnNext.Text = ResHelper.GetString("general.next") + " >";
            btnPrevious.Text = "< " + ResHelper.GetString("general.back");
        }
    }

    #endregion


    #region "Button handling"

    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        // Redirect to previous
        UrlHelper.Redirect(UrlHelper.UpdateParameterInUrl(UrlHelper.CurrentURL, "emailId", "" + prevId));
    }


    protected void btnNext_Click(object sender, EventArgs e)
    {
        // Redirect to next
        UrlHelper.Redirect(UrlHelper.UpdateParameterInUrl(UrlHelper.CurrentURL, "emailId", "" + nextId));
    }

    #endregion


    #region "Protected methods"

    /// <summary>
    /// Loads data of specific e-mail from DB.
    /// </summary>
    protected void LoadData()
    {
        if (emailId > 0)
        {
            // Get specific e-mail
            EmailInfo ei = EmailInfoProvider.GetEmailInfo(emailId);
            if (ei != null)
            {
                lblFromValue.Text = HTMLHelper.HTMLEncode(ei.EmailFrom);
                if (!ei.EmailIsMass)
                {
                    lblToValue.Text = HTMLHelper.HTMLEncode(ei.EmailTo);
                }
                else
                {
                    lblToValue.Text = ResHelper.GetString("emailqueue.detail.multiplerecipients");
                }
                lblCcValue.Text = ei.EmailCc;
                lblBccValue.Text = ei.EmailBcc;
                lblSubjectValue.Text = HTMLHelper.HTMLEncode(ei.EmailSubject);

                string body = null;

                // Use plain text body if used
                if (string.IsNullOrEmpty(ei.EmailPlainTextBody))
                {
                    // Preserve line breaks and remove tags
                    body = ei.EmailBody;
                    body = HTMLHelper.StripTags(body.Replace("<br />", "\r\n"));
                }
                else
                {
                    DiscussionMacroHelper dmh = new DiscussionMacroHelper();
                    dmh.ResolveToPlainText = true;
                    body = dmh.ResolveMacros(ei.EmailPlainTextBody);
                }

                body = HTMLHelper.ReformatHTML(body, "&nbsp;&nbsp;").Trim();

                // Replace line breaks with br tags and modify discussion macros
                ltlBodyValue.Text = DiscussionMacroHelper.RemoveTags(HTMLHelper.HTMLEncodeLineBreaks(body));

                // Show/hide send result message
                if ((ei.EmailLastSendResult != null) && (ei.EmailLastSendResult != String.Empty))
                {
                    lblErrorMessageValue.Text = ei.EmailLastSendResult;
                    plcErrorMessage.Visible = true;
                }
                else
                {
                    plcErrorMessage.Visible = false;
                }

                // Get all attachments attached to current e-mail
                DataSet ds = EmailAttachmentInfoProvider.GetEmailAttachmentInfos(emailId);

                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    plcAttachments.Visible = true;
                    if (ds.Tables.Count > 0)
                    {
                        int i = 0;
                        EmailAttachmentInfo eai = null;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (i > 0)
                            {
                                pnlAttachmentsList.Controls.Add(new LiteralControl("<br />"));
                            }
                            eai = new EmailAttachmentInfo(dr);
                            pnlAttachmentsList.Controls.Add(new LiteralControl(HTMLHelper.HTMLEncode(eai.AttachmentName) +
                                "&nbsp;(" + SqlHelperClass.GetSizeString(eai.AttachmentSize) + ")"));
                            i++;
                        }
                    }
                }
                else
                {
                    plcAttachments.Visible = false;
                }
            }
            else
            {
                plcDetails.Visible = false;
                lblInfo.Text = ResHelper.GetString("emailqueue.details.emailalreadysent");
                lblInfo.Visible = true;
            }
        }
    }

    #endregion
}
