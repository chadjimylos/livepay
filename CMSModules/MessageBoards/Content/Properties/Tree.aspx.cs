using System;
using System.Collections;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.PortalEngine;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.MessageBoard;
using CMS.UIControls;
using CMS.LicenseProvider;
using CMS.SettingsProvider;

using TreeElemNode = System.Web.UI.WebControls.TreeNode;
using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_MessageBoards_Content_Properties_Tree : CMSContentMessageBoardsPage 
{
    #region "Private variables"

    private string navUrl = null;
    private int docId = 0;
    private bool selectedSet = false;
    private string selectedNodeName = String.Empty;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize page
        navUrl = ResolveUrl("~/CMSModules/MessageBoards/Tools/Boards/Board_Edit.aspx") + "?changemaster=1";
        this.PageStatusContainer.Controls.Add(new LiteralControl("<div class=\"TreeMenu TreeMenuPadding\" style=\"height: 25px;\">&nbsp;</div>"));

        // Delete action
        string[,] actions = new string[1, 11];
        actions[0, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[0, 1] = ResHelper.GetString("general.delete");
        actions[0, 2] = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("general.confirmdelete")) +");";
        actions[0, 5] = GetImageUrl("Design/Controls/UniGrid/Actions/delete.png");
        actions[0, 6] = "delete";
        actions[0, 8] = "true";
        this.actionsElem.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);
        this.actionsElem.Actions = actions;

        // Ensure RTL
        if (CultureHelper.IsUICultureRTL())
        {
            treeElem.LineImagesFolder = GetImageUrl("RTL/Design/Controls/Tree/", false, true);
        }
        else
        {
            treeElem.LineImagesFolder = GetImageUrl("Design/Controls/Tree/", false, true);
        }

        selectedNodeName = QueryHelper.GetString("selectednodename", string.Empty);

        RegisterScripts();
        PopulateTree();
    }


    /// <summary>
    /// Actions handler
    /// </summary>
    protected void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "delete":

                BoardInfoProvider.DeleteBoardInfo(ValidationHelper.GetInteger(this.hdnBoardId.Value, 0));
                this.ltlScript.Text += ScriptHelper.GetScript("parent.frames['main'].location.href = '" + ResolveUrl("~/CMSDesk/blank.htm") + "'");
                ltlScript.Text += ScriptHelper.GetScript("window.location.replace(window.location);");
                break;
        }
    }


    #region "Private methods"

    /// <summary>
    /// Populates the tree with the data.
    /// </summary>
    private void PopulateTree()
    {
        // Create root node
        TreeElemNode rootNode = new TreeElemNode();
        rootNode.Text = "<span class=\"ContentTreeItem\" \"><span class=\"Name\">" + ResHelper.GetString("board.header.messageboards") + "</span></span>";
        rootNode.Expanded = true;
        treeElem.Nodes.Add(rootNode);

        // Populate the tree
        docId = QueryHelper.GetInteger("documentid", 0);
        if (docId > 0)
        {
            DataSet ds = BoardInfoProvider.GetMessageBoards("BoardDocumentID = " + docId, null);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AddNode(Convert.ToString(dr["BoardDisplayName"]), ValidationHelper.GetInteger(dr["BoardId"], -1));
                }
            }
        }
    }


    /// <summary>
    /// Registers all necessary scripts.
    /// </summary>
    private void RegisterScripts()
    {
        ltlScript.Text += ScriptHelper.GetScript(
            "var hiddenField = document.getElementById('" + this.hdnBoardId.ClientID + "');" + 
            "var currentNode = document.getElementById('treeSelectedNode');" +
            "var currentNodeName = \"\";" +
            "" +
            "treeUrl = '" + ResolveUrl("~/CMSModules/Content/CMSDesk/Properties/Advanced/MessageBoards/tree.aspx") + "';" +
            "function SelectNode(nodeName, nodeElem, boardId)" +
            "{" +
            "    if ((currentNode != null) && (nodeElem != null))" +
            "    {" +
            "        currentNode.className = 'ContentTreeItem';" +
            "    }" +
            "    " +
            "    parent.frames['main'].location.href = '" + navUrl + "&boardid=' + boardId;" +
            "    currentNodeName = nodeName;" +
            "    " +
            "    if (nodeElem != null)" +
            "    {" +
            "        currentNode = nodeElem;" +
            "        if (currentNode != null)" +
            "        {" +
            "            currentNode.className = 'ContentTreeSelectedItem';" +
            "        }" +
            "    }" +
            "    if (hiddenField != null) {" +
            "        hiddenField.value = boardId;" +
            "    }" + 
            "}");
    }


    /// <summary>
    /// Adds node to the root node
    /// </summary>
    /// <param name="nodeName">Name of node</param>
    private void AddNode(string nodeName, int boardId)
    {
        TreeElemNode newNode = new TreeElemNode();
        string cssClass = "ContentTreeItem";
        string elemId = string.Empty;

        // Select proper node
        if ((!selectedSet) && String.IsNullOrEmpty(selectedNodeName))
        {
            if (!RequestHelper.IsPostBack())
            {
                this.hdnBoardId.Value = boardId.ToString();
            }
            this.ltlScript.Text += ScriptHelper.GetScript("parent.frames['main'].location.href = '" + navUrl + "&boardid=" + boardId + "'");
            selectedSet = true;
            cssClass = "ContentTreeSelectedItem";
            elemId = "id=\"treeSelectedNode\"";
        }
        if (selectedNodeName == nodeName)
        {
            cssClass = "ContentTreeSelectedItem";
            elemId = "id=\"treeSelectedNode\"";
        }

        newNode.Text = "<span class=\"" + cssClass + "\" " + elemId + " onclick=\"SelectNode('" + HttpUtility.UrlEncode(nodeName) + "', this, " + boardId + ");\"><span class=\"Name\">" + HTMLHelper.HTMLEncode(nodeName) + "</span></span>";
        newNode.ImageUrl = GetImageUrl("Objects/Board_Board/list.png");
        newNode.NavigateUrl = "#";

        treeElem.Nodes[0].ChildNodes.Add(newNode);

        return;
    }

    #endregion
}
