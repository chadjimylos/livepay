using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_TaxClasses_TaxClass_Header : CMSEcommerceConfigurationPage
{
    protected int taxClassId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        taxClassId = ValidationHelper.GetInteger(Request.QueryString["taxclassid"], 0);
        bool hideBreadcrumbs = ValidationHelper.GetBoolean(Request.QueryString["hidebreadcrumbs"], false);

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }

        string taxClassName = "";
        TaxClassInfo tc = TaxClassInfoProvider.GetTaxClassInfo(taxClassId);
        if (tc != null)
        {
            taxClassName = ResHelper.LocalizeString(tc.TaxClassDisplayName);
        }

        // initializes page title
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("TaxClass_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/TaxClasses/TaxClass_List.aspx";
        pageTitleTabs[0, 2] = "configEdit";
        pageTitleTabs[1, 0] = taxClassName;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        if (!hideBreadcrumbs)
        {
            this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        }
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("TaxClass_Edit.HeaderCaption-Properties");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_TaxClass/object.png");
        this.CurrentMaster.Title.HelpTopicName = "new_classgeneral_tab";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Initializes edit menu
    /// </summary>
    protected void InitalizeMenu()
    {
        this.CurrentMaster.Tabs.Tabs = new string[3, 4];
        this.CurrentMaster.Tabs.Tabs[0, 0] = ResHelper.GetString("General.General");
        this.CurrentMaster.Tabs.Tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'new_classgeneral_tab');";
        this.CurrentMaster.Tabs.Tabs[0, 2] = "TaxClass_General.aspx?taxclassid=" + taxClassId.ToString();
        this.CurrentMaster.Tabs.Tabs[1, 0] = ResHelper.GetString("TaxClass_Edit.Countries");
        this.CurrentMaster.Tabs.Tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'countries_tab');";
        this.CurrentMaster.Tabs.Tabs[1, 2] = "TaxClass_Country.aspx?taxclassid=" + taxClassId.ToString();
        this.CurrentMaster.Tabs.Tabs[2, 0] = ResHelper.GetString("TaxClass_Edit.Header.States");
        this.CurrentMaster.Tabs.Tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'states_tab');";
        this.CurrentMaster.Tabs.Tabs[2, 2] = "TaxClass_State.aspx?taxclassid=" + taxClassId.ToString();
        this.CurrentMaster.Tabs.UrlTarget = "taxClassContent";
    }
}
