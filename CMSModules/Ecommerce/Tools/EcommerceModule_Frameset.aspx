<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EcommerceModule_Frameset.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_EcommerceModule_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Untitled Page</title>
    <script type="text/javascript">
        var IsCMSDesk = true;
    </script>
</head>
<frameset border="0" rows="72, *" id="rowsFrameset">
    <frame name="ecommerceMenu" src="Header.aspx" frameborder="0" scrolling="no" noresize="noresize" />
    <frame name="ecommerceContent" src="Orders/Order_List.aspx" frameborder="0" />
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
</frameset>
</html>
