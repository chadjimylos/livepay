CREATE TABLE [Notification_Template] (
		[TemplateID]               int NOT NULL IDENTITY(1, 1),
		[TemplateName]             nvarchar(250) NOT NULL,
		[TemplateDisplayName]      nvarchar(250) NOT NULL,
		[TemplateSiteID]           int NULL,
		[TemplateLastModified]     datetime NOT NULL,
		[TemplateGUID]             uniqueidentifier NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Notification_Template]
	ADD
	CONSTRAINT [PK_Notification_Template]
	PRIMARY KEY
	NONCLUSTERED
	([TemplateID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Notification_Template]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Notification_Template_TemplateSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Notification_Template]
			ADD CONSTRAINT [FK_Notification_Template_TemplateSiteID_CMS_Site]
			FOREIGN KEY ([TemplateSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_Notification_Template_TemplateSiteID_TemplateDisplayName]
	ON [Notification_Template] ([TemplateSiteID], [TemplateDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
