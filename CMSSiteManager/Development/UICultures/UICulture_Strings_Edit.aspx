<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UICulture_Strings_Edit.aspx.cs"
    Inherits="CMSSiteManager_Development_UICultures_UICulture_Strings_Edit" ValidateRequest="false"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="Default" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:LocalizedLabel ID="lblInfo" runat="server" CssClass="InfoLabel" Visible="False"
        ResourceString="General.ChangesSaved" />
    <cms:LocalizedLabel ID="lblError" runat="server" CssClass="ErrorLabel" Visible="false" />
    <table>
        <tr>
            <td>
                <cms:LocalizedLabel ID="lblKey" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Administration-UICulture_String_New.Key" />
            </td>
            <td>
                <cms:LocalizedLabel ID="lblKeyEng" runat="server" CssClass="ContentLabel" EnableViewState="false" />
                <asp:TextBox ID="txtKey" runat="server" CssClass="TextBoxField" /><br />
                <asp:RequiredFieldValidator ID="rfvKey" runat="server" ControlToValidate="txtKey"
                    Display="Dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top;">
                <cms:LocalizedLabel ID="lblText" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Administration-UICulture_String_New.Text" />
            </td>
            <td>
                <asp:TextBox ID="txtText" runat="server" CssClass="TextAreaField" TextMode="multiline" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:LocalizedLabel ID="lblEnglishText" runat="server" CssClass="ContentLabel" EnableViewState="false" />
            </td>
            <td>
                <cms:LocalizedLabel ID="txtEnglishText" runat="server" CssClass="ContentLabel" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:LocalizedLabel ID="lblCustomString" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Administration-UICulture_String_New.CustomString" />
            </td>
            <td>
                <asp:CheckBox ID="chkCustomString" runat="server" CssClass="ContentCheckBox" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:LocalizedButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton"
                    EnableViewState="false" ResourceString="general.ok" />
            </td>
        </tr>
    </table>
</asp:Content>
