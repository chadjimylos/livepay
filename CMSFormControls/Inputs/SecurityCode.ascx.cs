using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;

public partial class CMSFormControls_Inputs_SecurityCode : CMS.FormControls.FormEngineUserControl
{
    private bool mGenerateNumberEveryTime = true;
    private bool mShowInfoLabel = false;
    private bool mShowAfterText = false;
    private bool mForumExtension = false;


    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.txtSecurityCode.Enabled = value;
        }
    }


    /// <summary>
    /// Indicates wheather the after text should be displayed.
    /// </summary>
    public bool ShowAfterText
    {
        get
        {
            return mShowAfterText;
        }
        set
        {
            mShowAfterText = value;
        }
    }


    /// <summary>
    /// Forum extension
    /// </summary>
    public bool ForumExtension
    {
        get
        {
            return mForumExtension;
        }
        set
        {
            mForumExtension = value;
        }
    }


    /// <summary>
    /// Indicates wheather new random number is generated after the wrong number was entered.
    /// </summary>
    public bool GenerateNumberEveryTime
    {
        get
        {
            return mGenerateNumberEveryTime;
        }
        set
        {
            mGenerateNumberEveryTime = value;
        }
    }


    /// <summary>
    /// Indicates wheather the info label should be displayed.
    /// </summary>
    public bool ShowInfoLabel
    {
        get
        {
            return mShowInfoLabel;
        }
        set
        {
            mShowInfoLabel = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.txtSecurityCode.Text;
        }
        set
        {
            this.txtSecurityCode.Text = (string)value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Capta image url with anti cache query string parameter
        imgSecurityCode.ImageUrl = ResolveUrl("~/CMSPages/Dialogs/CaptchaImage.aspx?hash=" + Guid.NewGuid().ToString() + "&capcha=" + this.ClientID);

        if (this.ShowInfoLabel)
        {
            this.lblSecurityCode.Text = ResHelper.GetString("SecurityCode.lblSecurityCode");
        }
        else
        {
            this.lblSecurityCode.Visible = false;
        }

        if (this.ShowAfterText)
        {
            this.plcAfterText.Visible = true;
            this.lblAfterText.Text = ResHelper.GetString("SecurityCode.Aftertext");
        }
        else
        {
            this.plcAfterText.Visible = false;
        }

        if (!RequestHelper.IsPostBack())
        {
            // Create a random code and store it in the Session object.
            if (!this.ForumExtension || this.Session["CaptchaImageText" + this.ClientID] == null)
            {
                WindowHelper.Add("CaptchaImageText" + this.ClientID, GenerateRandomCode());
            }
        }
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        // Regenerate security code if it is not valid after postback
        if (RequestHelper.IsPostBack() && !IsValid() && GenerateNumberEveryTime)
        {
            this.txtSecurityCode.Text = "";
            WindowHelper.Add("CaptchaImageText" + this.ClientID, GenerateRandomCode());
        }
    }


    /// <summary>
    /// Generate new code
    /// </summary>
    public void GenerateNewCode()
    {
        this.txtSecurityCode.Text = "";
        WindowHelper.Add("CaptchaImageText" + this.ClientID, GenerateRandomCode());
    }


    /// <summary>
    /// Return true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        bool isValid = false;
        if (WindowHelper.GetItem("CaptchaImageText" + this.ClientID) != null)
        {
            isValid = (txtSecurityCode.Text == Convert.ToString(WindowHelper.GetItem("CaptchaImageText" + this.ClientID)));
        }

        if (!isValid)
        {
            this.ValidationError = ResHelper.GetString("SecurityCode.ValidationError");
        }
        return isValid;
    }


    //
    // Return a string of six random digits.
    //
    private string GenerateRandomCode()
    {
        Random random = new Random(this.ClientID.GetHashCode() + (int)DateTime.Now.Ticks);

        string s = "";
        for (int i = 0; i < 6; i++)
        {
            s = String.Concat(s, random.Next(10).ToString());
        }
        return s;
    }
}
