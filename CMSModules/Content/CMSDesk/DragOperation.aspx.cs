using System;
using System.Data;
using System.Collections;
using System.Threading;
using System.Security.Principal;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;

public partial class CMSModules_Content_CMSDesk_DragOperation : CMSContentPage
{
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!this.IsCallback)
        {
            string action = QueryHelper.GetString("action", "");

            switch (action.ToLower())
            {
                case "movenode":
                case "movenodeposition":
                    {
                        // Setup page title text and image
                        CurrentMaster.Title.TitleText = ResHelper.GetString("dialogs.header.title.movedoc");
                        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Content/Dialogs/titlemove.png");
                    }
                    break;

                case "copynode":
                case "copynodeposition":
                    {
                        // Setup page title text and image
                        CurrentMaster.Title.TitleText = ResHelper.GetString("dialogs.header.title.copydoc");
                        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Content/Dialogs/titlecopy.png");
                    }
                    break;

                case "linknode":
                case "linknodeposition":
                    {
                        // Setup page title text and image
                        CurrentMaster.Title.TitleText = ResHelper.GetString("dialogs.header.title.linkdoc");
                        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Content/Dialogs/titlelink.png");
                    }
                    break;
            }

            if (this.opDrag.Node != null)
            {
                CurrentMaster.Title.TitleText += " \"" + HTMLHelper.HTMLEncode(this.opDrag.Node.DocumentName) + "\"";
            }

            ((Panel)CurrentMaster.PanelBody.FindControl("pnlContent")).CssClass = string.Empty;
        }
    }
}
