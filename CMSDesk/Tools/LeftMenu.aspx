<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeftMenu.aspx.cs" Inherits="CMSDesk_Tools_LeftMenu"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/Tree.master" %>

<%@ Register Src="~/CMSAdminControls/UI/UIProfiles/UIMenu.ascx" TagName="UIMenu"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcTree">
    <cms:UIMenu runat="server" ID="menuElem" ModuleName="CMS.Tools" JavaScriptHandler="LoadItem"
        ModuleAvailabilityForSiteRequired="true" />
</asp:Content>
