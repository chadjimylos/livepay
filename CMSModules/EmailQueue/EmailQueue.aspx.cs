using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Security.Principal;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.EmailEngine;
using CMS.IEmailEngine;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.ExtendedControls;

public partial class CMSModules_EmailQueue_EmailQueue : SiteManagerPage
{
    #region "Variables"

    protected int siteId = 0;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = ResHelper.GetString("emailqueue.queue.title");
        this.btnFilter.Text = ResHelper.GetString("General.Show");

        siteId = QueryHelper.GetInteger("siteid", -1);

        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        InitializeActionMenu();

        string siteName = EmailHelper.GetSiteName(siteId);
        // Display disabled information
        if (!SettingsKeyProvider.GetBoolValue(siteName + ".CMSEmailsEnabled"))
        {
            this.lblDisabled.Visible = true;
        }

        // Load drop-down lists
        if (!IsPostBack)
        {
            drpPriority.Items.Add(new ListItem(ResHelper.GetString("general.all"), "-1"));
            drpPriority.Items.Add(new ListItem(ResHelper.GetString("emailpriority.low"), Convert.ToString((int)EmailPriorityEnum.Low)));
            drpPriority.Items.Add(new ListItem(ResHelper.GetString("emailpriority.normal"), Convert.ToString((int)EmailPriorityEnum.Normal)));
            drpPriority.Items.Add(new ListItem(ResHelper.GetString("emailpriority.high"), Convert.ToString((int)EmailPriorityEnum.High)));

            drpStatus.Items.Add(new ListItem(ResHelper.GetString("general.all"), "-1"));
            drpStatus.Items.Add(new ListItem(ResHelper.GetString("emailstatus.created"), Convert.ToString((int)EmailStatusEnum.Created)));
            drpStatus.Items.Add(new ListItem(ResHelper.GetString("emailstatus.sending"), Convert.ToString((int)EmailStatusEnum.Sending)));
            drpStatus.Items.Add(new ListItem(ResHelper.GetString("emailstatus.waiting"), Convert.ToString((int)EmailStatusEnum.Waiting)));

            InitFilterDropDown(drpFrom);
            InitFilterDropDown(drpTo);
            InitFilterDropDown(drpSubject);
            InitFilterDropDown(drpBody);
            InitFilterDropDown(drpError);

            btnShowFilter.ResourceString = "emailqueue.displayfilter";
            imgShowFilter.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/SortDown.png");
        }

        gridElem.WhereCondition = GetWhereCondition();

        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        // Register script for modal dialog
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "displayModal", ScriptHelper.GetScript(
            "function DisplayRecipients(emailId) { \n" +
            "    if ( emailId != 0 ) { \n" +
            "       modalDialog('MassEmails_Recipients.aspx?emailid=' + emailId, 'emailrecipients', 920, 700); \n" +
            "    } } \n "));

        btnShowFilter.Click += new EventHandler(btnShowFilter_Click);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Enables/disables action buttons according to grid content
        InitializeActionMenu();
        CurrentMaster.HeaderActions.ReloadData();

        if (!DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource))
        {
            // Store e-mails' IDs to session for e-mail detail viewer
            SetEmailIds();
        }
    }

    #endregion


    #region "Private methods"


    /// <summary>
    /// Initializes action menu in master page.
    /// </summary>
    protected void InitializeActionMenu()
    {
        bool enabled = !DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource);
        bool resending = enabled && (ThreadSender.SendingThreads <= 0);

        // Resend all failed
        string[,] actions = new string[7, 10];
        actions[0, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[0, 1] = ResHelper.GetString("emailqueue.queue.resendfailed");
        if (resending)
        {
            actions[0, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EmailQueue.ResendAllFailedConfirmation")) + ")) return false;";
        }
        actions[0, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/resendallfailed.png");
        actions[0, 6] = "resendallfailed";
        actions[0, 9] = resending.ToString();

        // Resend selected
        actions[1, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[1, 1] = ResHelper.GetString("emailqueue.queue.resendselected");
        if (resending)
        {
            actions[1, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EmailQueue.ResendSelectedConfirmation")) + ")) return false;";
        }
        actions[1, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/resendselected.png"); ;
        actions[1, 6] = "resendselected";
        actions[1, 9] = resending.ToString();


        // Resend all
        actions[2, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[2, 1] = ResHelper.GetString("emailqueue.queue.resend");
        if (resending)
        {
            actions[2, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EmailQueue.ResendAllConfirmation")) + ")) return false;";
        }
        actions[2, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/resendall.png");
        actions[2, 6] = "resendall";
        actions[2, 9] = resending.ToString();

        // Delete all failed
        actions[3, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[3, 1] = ResHelper.GetString("emailqueue.queue.deletefailed");
        if (enabled)
        {
            actions[3, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EmailQueue.DeleteAllFailedConfirmation")) + ")) return false;";
        }
        actions[3, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/deleteallfailed.png");
        actions[3, 6] = "deleteallfailed";
        actions[3, 9] = enabled.ToString();


        // Delete selected
        actions[4, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[4, 1] = ResHelper.GetString("emailqueue.queue.deleteselected");
        if (enabled)
        {
            actions[4, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EmailQueue.DeleteSelectedConfirmation")) + ")) return false;";
        }
        actions[4, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/deleteselected.png"); ;
        actions[4, 6] = "deleteselected";
        actions[4, 9] = enabled.ToString();

        // Delete all
        actions[5, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[5, 1] = ResHelper.GetString("emailqueue.queue.delete");
        if (enabled)
        {
            actions[5, 2] = "if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EmailQueue.DeleteAllConfirmation")) + ")) return false;";
        }
        actions[5, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/deleteall.png");
        actions[5, 6] = "deleteall";
        actions[5, 9] = enabled.ToString();

        // Refresh
        actions[6, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[6, 1] = ResHelper.GetString("general.refresh");
        actions[6, 5] = GetImageUrl("CMSModules/CMS_EmailQueue/refresh.png");
        actions[6, 6] = "refresh";

        CurrentMaster.HeaderActions.Actions = actions;
        CurrentMaster.HeaderActions.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);
    }


    /// <summary>
    /// Sets session with all emails' IDs.
    /// </summary>
    protected void SetEmailIds()
    {
        ArrayList idArray = null;
        if (!DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource))
        {
            idArray = new ArrayList();
            // Get data source and sort it
            DataTable dt = DataHelper.GetDataTable(gridElem.GridView.DataSource);

            foreach (DataRow dr in dt.DefaultView.Table.Rows)
            {
                // Add all event ids to 
                idArray.Add(dr["EmailID"]);
            }
        }
        // Set session with array of IDs
        SessionHelper.SetValue("emailIds", idArray);
    }


    #endregion


    #region "Unigrid events"

    /// <summary>
    /// Handles Unigrid's OnExternalDataBound event.
    /// </summary>
    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        sourceName = sourceName.ToLower();
        switch (sourceName)
        {
            case "priority":
                switch ((EmailPriorityEnum)parameter)
                {
                    case EmailPriorityEnum.Low:
                        return ResHelper.GetString("emailpriority.low");
                    case EmailPriorityEnum.Normal:
                        return ResHelper.GetString("emailpriority.normal");
                    case EmailPriorityEnum.High:
                        return ResHelper.GetString("emailpriority.high");
                }
                break;

            case "status":
                switch ((EmailStatusEnum)parameter)
                {
                    case EmailStatusEnum.Created:
                        return ResHelper.GetString("emailstatus.created");
                    case EmailStatusEnum.Waiting:
                        return ResHelper.GetString("emailstatus.waiting");
                    case EmailStatusEnum.Sending:
                        return ResHelper.GetString("emailstatus.sending");
                }
                break;

            case "subject":
                return TextHelper.LimitLength(HTMLHelper.HTMLEncode(parameter.ToString()), 40);
            case "result":
                return TextHelper.LimitLength(parameter.ToString(), 40);
            case "subjecttooltip":
            case "resulttooltip":
                return parameter.ToString().Replace("\r\n", "<br />").Replace("\n", "<br />");
            case "resend":
            case "delete":
                if (sender is ImageButton)
                {
                    // Disable action buttons (and image) if e-mail status is 'created' or 'sending'
                    int status = ValidationHelper.GetInteger(((DataRowView)((GridViewRow)parameter).DataItem).Row["EmailStatus"], -1);
                    if ((status == Convert.ToInt32(EmailStatusEnum.Created)) || (status == Convert.ToInt32(EmailStatusEnum.Sending)))
                    {
                        ImageButton imgBtn = (ImageButton)sender;
                        imgBtn.OnClientClick = null;
                        imgBtn.Enabled = false;
                        if (sourceName == "resend")
                        {
                            imgBtn.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/resendemaildisabled.png");
                        }
                        else
                        {
                            imgBtn.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/deletedisabled.png");
                        }
                    }
                }
                break;

            case "emailto":
                DataRowView dr = (DataRowView)parameter;
                if (ValidationHelper.GetBoolean(dr["EmailIsMass"], false))
                {
                    int id = ValidationHelper.GetInteger(dr["EmailID"], 0);
                    return "<a href=\"#\" onclick=\"javascript: DisplayRecipients(" + id + "); return false; \">"
                    + ResHelper.GetString("emailqueue.queue.massdetails") + "</a>";
                }
                else
                {
                    return HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["EmailTo"], String.Empty));
                }
            case "edit":

                string caption = ResHelper.GetString("Unigrid.EventLog.Actions.Display");
                string imageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/View.png");
                string url = ResolveUrl("~/CMSModules/EmailQueue/EmailQueue_Details.aspx");
                string whereCondition = HttpUtility.UrlPathEncode(this.gridElem.WhereCondition).Replace("&", "%26").Replace("#", "%23").Replace("'", "\'");

                ImageButton viewBtn = (ImageButton)sender;

                url = UrlHelper.AddParameterToUrl(url, "orderby", this.gridElem.SortDirect);
                url = UrlHelper.AddParameterToUrl(url, "where", whereCondition);
                url = UrlHelper.AddParameterToUrl(url, "hash", QueryHelper.GetHash(url, false));
                url = UrlHelper.AddParameterToUrl(url, "emailid", Convert.ToString(viewBtn.CommandArgument));

                string action = "modalDialog(" + ScriptHelper.GetString(url) + ", 'emaildetails', 920, 700);";
                viewBtn.OnClientClick = action;

                break;
        }

        return null;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that threw event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "delete":
                EmailInfoProvider.DeleteEmailInfo(ValidationHelper.GetInteger(actionArgument, 0));
                break;

            case "resend":
                // Resend email info object from queue
                int emailId = ValidationHelper.GetInteger(actionArgument, -1);
                if (emailId > 0)
                {
                    ThreadSender ts = new ThreadSender(emailId);
                    ts.RunAsync(WindowsIdentity.GetCurrent());
                }
                break;
        }
    }

    #endregion


    #region "Button events"

    /// <summary>
    /// Displays/hides filter.
    /// </summary>
    protected void btnShowFilter_Click(object sender, EventArgs e)
    {
        // Hide filter
        if (plcFilter.Visible)
        {
            plcFilter.Visible = false;
            btnShowFilter.ResourceString = "emailqueue.displayfilter";
            imgShowFilter.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/SortDown.png");
        }
        // Display filter
        else
        {
            plcFilter.Visible = true;
            btnShowFilter.ResourceString = "emailqueue.hidefilter";
            imgShowFilter.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/SortUp.png");
        }
    }


    /// <summary>
    /// Filter button clicked.
    /// </summary>
    protected void btnFilter_Clicked(object sender, EventArgs e)
    {
        gridElem.WhereCondition = GetWhereCondition();
        //        gridElem.ReloadData();
    }

    #endregion


    #region "Header action event"

    void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        ThreadSender ts = null;
        ArrayList list = null;
        bool reloaded = false;
        switch (e.CommandName.ToLower())
        {
            case "resendallfailed":
                ts = new ThreadSender(true, false);
                ts.RunAsync(WindowsIdentity.GetCurrent());

                gridElem.ReloadData();
                reloaded = true;

                lblInfo.Text = ResHelper.GetString("emailqueue.sendingemails");
                lblInfo.Visible = true;
                break;

            case "resendselected":
                // Get list of selected e-mails
                list = gridElem.SelectedItems;
                if (list.Count > 0)
                {
                    ts = new ThreadSender();

                    foreach (string emailId in list)
                    {
                        // Resend email info object from queue
                        ts.EmailId = ValidationHelper.GetInteger(emailId, -1);
                        ts.SendFailed = true;
                        ts.SendNew = true;
                        ts.RunAsync(WindowsIdentity.GetCurrent());
                    }
                    gridElem.ResetSelection();
                    gridElem.ReloadData();
                    reloaded = true;

                    lblInfo.Text = ResHelper.GetString("emailqueue.sendingemails");
                    lblInfo.Visible = true;
                }
                break;

            case "resendall":
                ts = new ThreadSender(true, true);
                ts.RunAsync(WindowsIdentity.GetCurrent());

                gridElem.ReloadData();
                reloaded = true;

                lblInfo.Text = ResHelper.GetString("emailqueue.sendingemails");
                lblInfo.Visible = true;
                break;

            case "deleteallfailed":
                EmailInfoProvider.DeleteAllFailed(siteId);
                gridElem.ReloadData();
                reloaded = true;
                break;

            case "deleteselected":
                // Get list of selected e-mails
                list = gridElem.SelectedItems;
                if (list.Count > 0)
                {
                    foreach (string emailId in list)
                    {
                        // Delete specific e-mail
                        EmailInfoProvider.DeleteEmailInfo(ValidationHelper.GetInteger(emailId, 0));
                    }
                    gridElem.ResetSelection();
                    gridElem.ReloadData();
                    reloaded = true;
                }
                break;

            case "deleteall":
                EmailInfoProvider.DeleteAll(siteId, EmailStatusEnum.Waiting, false);
                gridElem.ReloadData();
                reloaded = true;
                break;

            case "refresh":
                gridElem.ReloadData();
                break;
        }
        // Reload on first page if no data found after perfoming action
        if (reloaded && DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource))
        {
            gridElem.Pager.UniPager.CurrentPage = 1;
            gridElem.ReloadData();
        }
    }

    #endregion


    #region "Filter methods"

    /// <summary>
    /// Initializes standard filter dropdown
    /// </summary>
    /// <param name="drp">Dropdown to init</param>
    private void InitFilterDropDown(DropDownList drp)
    {
        if ((drp != null) && (drp.Items.Count <= 0))
        {
            drp.Items.Add(new ListItem("LIKE", "LIKE"));
            drp.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
            drp.Items.Add(new ListItem("=", "="));
            drp.Items.Add(new ListItem("<>", "<>"));
        }
    }


    /// <summary>
    /// Appends WHERE condition for specified column at the end of current/existing WHERE condition.
    /// </summary>
    /// <param name="whereCond">Current WHERE condition</param>
    /// <param name="column">Column name</param>
    /// <param name="value">Value to be searched (if empty no condition is created)</param>
    /// <param name="op">Operator to condition</param>
    private void BuildWhereCond(ref string whereCond, string column, string value, string op)
    {
        // Avoid SQL injenction
        string tempVal = value.Trim().Replace("'", "''");

        // No condition
        if (tempVal == "")
        {
            return;
        }

        // Support for exact phrase search
        if (op.Contains("LIKE"))
        {
            tempVal = "%" + tempVal + "%";
        }

        string where = "(" + column + " " + op + " N'" + tempVal + "')";
        if (!String.IsNullOrEmpty(whereCond))
        {
            where = " AND " + where;
        }

        // Get final where condition
        whereCond += where;
    }


    /// <summary>
    /// Return WHERE condition.
    /// </summary>
    protected string GetWhereCondition()
    {
        string where = "";

        BuildWhereCond(ref where, "EmailFrom", txtFrom.Text, drpFrom.SelectedValue);
        BuildWhereCond(ref where, "EmailSubject", txtSubject.Text, drpSubject.SelectedValue);
        BuildWhereCond(ref where, "EmailBody", txtBody.Text, drpBody.SelectedValue);
        BuildWhereCond(ref where, "EmailLastSendResult", txtErrorMessage.Text, drpError.SelectedValue);

        // EmailTo condition
        if (!String.IsNullOrEmpty(txtTo.Text.Trim()))
        {
            if (!String.IsNullOrEmpty(where))
            {
                where += " AND ";
            }
            string toText = txtTo.Text.Trim().Replace("'", "''");
            string op = drpTo.SelectedValue;
            if (op.Contains("LIKE"))
            {
                toText = "%" + toText + "%";
            }
            toText = " N'" + toText + "'";
            string combineOp = " OR ";
            if ((op == "<>") || op.Contains("NOT"))
            {
                combineOp = " AND ";
            }
            where += "(EmailTo " + op + toText + combineOp +
                      "EmailCc " + op + toText + combineOp +
                      "EmailBcc " + op + toText + ")";
        }

        // Condition for priority
        int priority = ValidationHelper.GetInteger(drpPriority.SelectedValue, -1);
        if (priority >= 0)
        {
            if (!String.IsNullOrEmpty(where))
            {
                where += " AND ";
            }
            where += "EmailPriority=" + drpPriority.SelectedValue;
        }


        // Condition for e-mail status
        int status = ValidationHelper.GetInteger(drpStatus.SelectedValue, -1);
        if (status >= 0)
        {
            if (!String.IsNullOrEmpty(where))
            {
                where += " AND ";
            }

            where += "EmailStatus=" + drpStatus.SelectedValue;
        }


        // Condition for site
        if (!String.IsNullOrEmpty(where))
        {
            where += " AND ";
        }
        where += "(NOT EmailStatus = " + Convert.ToString((int)EmailStatusEnum.Archived) + ")";

        if (siteId == 0)      // Global
        {
            where += " AND (EmailSiteID IS NULL OR  EmailSiteID = 0)";
        }
        else if (siteId > 0)
        {
            where += " AND (EmailSiteID = " + siteId.ToString() + ")";
        }

        return where;
    }

    #endregion
}
