﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UniGridPager.ascx.cs"
    Inherits="CMSAdminControls_UI_UniGrid_Controls_UniGridPager" %>
<table class="UniGridPager" cellpadding="0" cellspacing="0">
    <tr>
        <td style="width: 100%; white-space: nowrap; <%= mBackgroundStyle %>">
            <cms:UniPager ID="pagerElem" runat="server">
                <FirstPageTemplate>
                    <td>
                        <a class="UnigridPagerFirst" href="<%# Eval("FirstURL") %>">&nbsp;</a>
                    </td>
                </FirstPageTemplate>
                <PreviousPageTemplate>
                    <td>
                        <a class="UnigridPagerPrev" href="<%# Eval("PreviousURL") %>">&nbsp;</a>
                    </td>
                </PreviousPageTemplate>
                <PreviousGroupTemplate>
                    <a class="UnigridPagerPage" href="<%# Eval("PreviousGroupURL") %>">...</a>
                </PreviousGroupTemplate>
                <PageNumbersTemplate>
                    <a class="UnigridPagerPage" href="<%# Eval("PageURL") %>">
                        <%# Eval("Page") %></a>
                </PageNumbersTemplate>
                <PageNumbersSeparatorTemplate>
                </PageNumbersSeparatorTemplate>
                <CurrentPageTemplate>
                    <span class="UnigridPagerSelectedPage">
                        <%# Eval("Page") %></span>
                </CurrentPageTemplate>
                <NextGroupTemplate>
                    <a class="UnigridPagerPage" href="<%# Eval("NextGroupURL") %>">...</a>
                </NextGroupTemplate>
                <NextPageTemplate>
                    <td>
                        <a class="UnigridPagerNext" href="<%# Eval("NextURL") %>">&nbsp;</a>
                    </td>
                </NextPageTemplate>
                <LastPageTemplate>
                    <td>
                        <a class="UnigridPagerLast" href="<%# Eval("LastURL") %>">&nbsp;</a>
                    </td>
                </LastPageTemplate>
                <DirectPageTemplate>
                    <td style="white-space: nowrap;">
                        <div class="UnigridPagerDirectPage">
                            <div class="LeftAlign">
                                <cms:LocalizedLabel ID="lblPage" runat="server" ResourceString="UniGrid.Page" />
                                &nbsp;
                            </div>
                            <div class="LeftAlign">
                                <asp:TextBox ID="txtPage" runat="server" Style="width: 25px;" />
                            </div>
                            <div class="LeftAlign">
                                <asp:DropDownList ID="drpPage" runat="server" Style="width: 50px;" />
                            </div>
                            <div class="LeftAlign">
                                &nbsp;/&nbsp;
                                <%# Eval("Pages") %>
                            </div>
                        </div>
                    </td>
                </DirectPageTemplate>
                <LayoutTemplate>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <asp:PlaceHolder runat="server" ID="plcFirstPage"></asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" ID="plcPreviousPage"></asp:PlaceHolder>
                            <td style="white-space: nowrap;" class="UnigridPagerPages">
                                <asp:PlaceHolder runat="server" ID="plcPreviousGroup"></asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="plcPageNumbers"></asp:PlaceHolder>
                                <asp:PlaceHolder runat="server" ID="plcNextGroup"></asp:PlaceHolder>
                            </td>
                            <asp:PlaceHolder runat="server" ID="plcNextPage"></asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" ID="plcLastPage"></asp:PlaceHolder>
                            <asp:PlaceHolder runat="server" ID="plcDirectPage"></asp:PlaceHolder>
                        </tr>
                    </table>
                </LayoutTemplate>
            </cms:UniPager>
        </td>
        <td>
            &nbsp;
        </td>
        <asp:PlaceHolder ID="plcPageSize" runat="server">
            <td style="white-space: nowrap;">
                <div class="UnigridPagerPageSize">
                    <cms:LocalizedLabel ID="lblPageSize" runat="server" EnableViewState="false" ResourceString="UniGrid.ItemsPerPage" />
                    &nbsp;
                    <asp:DropDownList ID="drpPageSize" runat="server" AutoPostBack="true" />
                </div>
            </td>
        </asp:PlaceHolder>
    </tr>
</table>
