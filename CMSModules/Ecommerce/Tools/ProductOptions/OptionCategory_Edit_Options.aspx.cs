using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_ProductOptions_OptionCategory_Edit_Options : CMSEcommercePage
{
    protected int categoryId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get category ID and department ID from url
        categoryId = ValidationHelper.GetInteger(Request.QueryString["categoryid"], 0);

        string[,] actions = new string[2, 7];

        // New item link
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("ProductOptions.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("~/CMSModules/Ecommerce/Tools/Products/Product_New.aspx?categoryid=" + categoryId);
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_SKU/add.png");

        // Sort link & img
        actions[1, 0] = HeaderActions.TYPE_LINKBUTTON;
        actions[1, 1] = ResHelper.GetString("ProductOptions.SortAlphabetically");
        actions[1, 2] = null;
        actions[1, 3] = null;
        actions[1, 4] = null;
        actions[1, 5] = GetImageUrl("CMSModules/CMS_Ecommerce/optionssort.png");
        actions[1, 6] = "lnkSort_Click";

        this.CurrentMaster.HeaderActions.Actions = actions;
        this.CurrentMaster.HeaderActions.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);

        // Unigrid
        grid.OnAction += new OnActionEventHandler(grid_OnAction);
        grid.OnExternalDataBound += new OnExternalDataBoundEventHandler(grid_OnExternalDataBound);
        grid.WhereCondition = "SKUOptionCategoryID = " + categoryId;
    }


    object grid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "skuenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }

        return parameter;
    }


    void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "lnksort_click":
                // Check 'EcommerceModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
                {
                    RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
                }

                OptionCategoryInfoProvider.SortProductOptionsAlphabetically(categoryId);
                grid.ReloadData();
                break;
        }
    }


    void grid_OnAction(string actionName, object actionArgument)
    {
        int skuId = ValidationHelper.GetInteger(actionArgument, 0);

        switch (actionName.ToLower())
        {
            case "edit":
                UrlHelper.Redirect("~/CMSModules/Ecommerce/Tools/Products/Product_Edit_Frameset.aspx?productid=" + skuId);
                break;

            case "delete":

                // Check 'EcommerceModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
                {
                    RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
                }

                // Check dependencies
                if (SKUInfoProvider.CheckDependencies(skuId))
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                    return;
                }

                SKUInfoProvider.DeleteSKUInfo(skuId);
                grid.ReloadData();

                break;

            case "moveup":

                // Check 'EcommerceModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
                {
                    RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
                }

                SKUInfoProvider.MoveSKUOptionUp(skuId);
                break;

            case "movedown":

                // Check 'EcommerceModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
                {
                    RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
                }

                SKUInfoProvider.MoveSKUOptionDown(skuId);
                break;
        }
    }
}
