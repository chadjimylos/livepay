<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupList.ascx.cs" Inherits="CMSModules_Groups_Controls_GroupList" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<%@ Register Src="~/CMSModules/Groups/Controls/GroupFilter.ascx" TagName="GroupFilter"
    TagPrefix="cms" %>
<cms:GroupFilter runat="server" ID="filterGroups" />
<br />
<cms:UniGrid runat="server" ID="gridElem" GridName="~/CMSModules/Groups/Controls/Group_List.xml"
    OrderBy="GroupDisplayName" />
