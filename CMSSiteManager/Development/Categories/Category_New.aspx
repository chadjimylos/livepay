<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="Category_New.aspx.cs" Inherits="CMSSiteManager_Development_Categories_Category_New"
    Title="Category - New" Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/Categories/CategoryEdit.ascx" TagName="CategoryEdit"
    TagPrefix="cms" %>
<asp:Content ID="Content3" ContentPlaceHolderID="plcContent" runat="Server">
    <cms:CategoryEdit ID="catEdit" runat="server" />
</asp:Content>
