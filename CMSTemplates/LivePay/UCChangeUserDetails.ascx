﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCChangeUserDetails.ascx.vb" Inherits="CMSTemplates_LivePay_UCChangeUserDetails" %>

<script language="javascript" type="text/javascript">
    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

    var UpUserDet_Email = ''
    var UpUserDet_EmailBelongToOtherUser = ''
    var UpUserDet_Phone = ''
    var UpUserDet_FullName = ''

    function CheckEmailExist() {
        var xmlhttp;
        if (window.ActiveXObject)
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        else
            xmlhttp = new XMLHttpRequest();

        var NewUserName = document.getElementById('<%=txtEmail.ClientID %>').value
        NewUserName = NewUserName.replace(/[']/gi, "")
        if (NewUserName != '') {
            xmlhttp.open("GET", "/CMSTemplates/LivePay/xmlHttpCheckUsername.aspx?NewUserName=" + NewUserName, true);
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    var UserNotExist = xmlhttp.responseText;
                    if (UserNotExist == 0) {
                        UpUserDetCloud('#EmailMsg', UpUserDet_EmailBelongToOtherUser, true)
                    } else {
                        UpUserDetCloud('#EmailMsg', '', false)
                        document.getElementById('<%=btnSave.ClientID%>').click();
                    }
                }
            }
            xmlhttp.send(null);
        } else {
            document.getElementById('<%=btnSave.ClientID%>').click();
        }
    }

    function UpUser_Validation(oSrc, args) {
        var bIsValid = true
        var MyVal = args.Value

        if (MyVal.length == 0) {
            bIsValid = false
        }

        if (IsNumeric(MyVal) == false && oSrc.getAttribute('IsNumber') == 'yes') {
            bIsValid = false
        }

        var MaxLenght = MyVal.length
        if (oSrc.getAttribute('CheckLength') == 'yes' && MaxLenght != oSrc.getAttribute('LengthLimit')) {
            bIsValid = false
        }


        if (oSrc.getAttribute('IsFullName') == 'yes') {
            if (bIsValid == false) {
                UpUserDetCloud('#FullNameMsg', UpUserDet_FullName, true)
            } else {
                UpUserDetCloud('#FullNameMsg', '', false)
            }
        }

        if (oSrc.getAttribute('IsPhone') == 'yes') {
            if (bIsValid == false) {
                UpUserDetCloud('#PhoneMsg', UpUserDet_Phone, true)
            } else {
                UpUserDetCloud('#PhoneMsg', '', false)
            }
        }

        if (oSrc.getAttribute('IsEmail') == 'yes') {

            if (MyVal.length > 0) {
                if (checkTheEmail(MyVal) == false) {
                    bIsValid = false
                }
                if (bIsValid == false) {
                    UpUserDetCloud('#EmailMsg ', UpUserDet_Email, true)
                } else {
                    UpUserDetCloud('#EmailMsg ', '', false)
                }
            } else {
                UpUserDetCloud('#EmailMsg ', UpUserDet_Email, true)
            }
        }

        args.IsValid = bIsValid;
    }

    function checkTheEmail(MailValue) {
        var filter = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
        if (!filter.test(MailValue))
            return false
        else
            return true
    }

    function IsNumeric(sText) {
        var ValidChars = "0123456789";
        var IsNumber = true;
        var Char;
        for (i = 0; i < sText.length && IsNumber == true; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) == -1) {
                IsNumber = false;
            }
        }
        return IsNumber;
    }

    function UpUserDetCloud(CloudID, CloudContent, Show) {
        $(CloudID).poshytip('hide');
        if (is_chrome) {
            $(CloudID).css('position', 'absolute')
        }
        $(CloudID).poshytip({
            className: 'tip-livepay',
            content: CloudContent,
            showOn: 'none',
            alignTo: 'target',
            alignX: 'right',
            offsetX: 255,
            offsetY: -23, hideAniDuration: false
        });
        $(CloudID).poshytip('hide');
        if (Show)
            $(CloudID).poshytip('show');

    }
</script>

<div class="CUDDarkBlueBGTitle">
    <div class="CUDContentBG">
        <div>
            <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right"><%=ResHelper.LocalizeString("{$=Email Χρήστη:|en-us=e-mail address:$}") %></div>
                <div style="float:left"><a id="EmailMsg" href="#"></a><asp:TextBox ID="txtEmail" runat="server"  MaxLength="100" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/><font  style="color:#094595;font-weight:bold">&nbsp;*</font></div>
                <div class="HideIt"><asp:CustomValidator ID="UpdateDetailsEmailValidation" ClientValidationFunction="UpUser_Validation" ValidateEmptyText="true" runat="server" IsEmail="yes" ControlToValidate="txtEmail" text="*" ValidationGroup="UpdateUserDetailsForm"/></div> 
                <div class="Clear"></div>
            </div>
            <div><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right"><%=ResHelper.LocalizeString("{$=Ονοματεπώνυμο:|en-us=Full Name:$}") %></div>
                <div style="float:left;"><a id="FullNameMsg" href="#"></a><asp:TextBox ID="txtFullName" runat="server" MaxLength="250" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/><font  style="color:#094595;font-weight:bold">&nbsp;*</font></div>
                <div class="HideIt"><asp:CustomValidator ID="UpdateDetailsFullNameValidation" ClientValidationFunction="UpUser_Validation" ValidateEmptyText="true" runat="server" IsFullName="yes" ControlToValidate="txtFullName" text="*" ValidationGroup="UpdateUserDetailsForm"/></div> 
                <div class="Clear"></div>
            </div> 
            <div><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right"><%=ResHelper.LocalizeString("{$=Τηλεφωνο Επικ.:|en-us=Phone Number:$}") %></div>
                <div style="float:left;"><a id="PhoneMsg" href="#"></a><asp:TextBox ID="txtContactPhone" runat="server" MaxLength="10" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/><font  style="color:#094595;font-weight:bold">&nbsp;*</font></div>
                <div class="HideIt"><asp:CustomValidator ID="UpdateDetailsPhoneValidation" ClientValidationFunction="UpUser_Validation" ValidateEmptyText="true" runat="server" IsPhone="yes" IsNumber="yes" LengthLimit="10" CheckLength="yes"  ControlToValidate="txtContactPhone" text="*" ValidationGroup="UpdateUserDetailsForm"/></div> 
                <div class="Clear"></div>
            </div> 
            <div style="width:247px;text-align:right">
                <img id="imgSave" style="cursor:pointer" src='<%=ResHelper.LocalizeString(String.Concat("{$=/App_Themes/LivePay/ChangeUserDetails/BtnSave.png|en-us=/App_Themes/LivePay/ChangeUserDetails/BtnSave_en-us.png$}")) %>' onclick="CheckEmailExist()" />
                <asp:ImageButton ID="btnSave" runat="server" style="display:none" ValidationGroup="UpdateUserDetailsForm" />
            </div>
        </div>
        
    </div>
    <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
</div> 
