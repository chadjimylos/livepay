<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportCategory_List.aspx.cs"
    Inherits="CMSModules_Reporting_Tools_ReportCategory_List" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:UniGrid runat="server" ID="UniGrid" GridName="ReportCategory_List.xml" OrderBy="CategoryDisplayName"
        Columns="CategoryID, CategoryDisplayName" IsLiveSite="false" />
</asp:Content>
