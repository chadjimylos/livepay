<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StoreSettings_Frameset.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Configuration_StoreSettings_StoreSettings_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" enableviewstate="false">
    <title>Store settings - Edit</title>
</head>
	<frameset border="0" rows="72, *" id="rowsFrameset">
		<frame name="storesettingsHeader" src="StoreSettings_Header.aspx " scrolling="no" frameborder="0" noresize="noresize" />		
	    <frame name="storesettingsContent" src="StoreSettings_General.aspx" scrolling="auto" frameborder="0" noresize="noresize" />
	<noframes>
		<body>
		 <p id="p1">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		 </p>
		</body>
	</noframes>
</frameset>
</html>
