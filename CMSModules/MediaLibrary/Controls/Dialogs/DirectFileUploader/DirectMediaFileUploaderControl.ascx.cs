using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data;

using CMS.UIControls;
using CMS.ExtendedControls;
using CMS.GlobalHelper;
using CMS.MediaLibrary;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.Staging;

public partial class CMSModules_MediaLibrary_Controls_Dialogs_DirectFileUploader_DirectMediaFileUploaderControl : DirectFileUploader
{
    #region "Variables"

    private bool? mIsSupportedBrowser = null;
    private MediaLibraryInfo mLibraryInfo = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Library info uploaded file is related to.
    /// </summary>
    private MediaLibraryInfo LibraryInfo
    {
        get
        {
            if (LibraryID > 0)
            {
                mLibraryInfo = MediaLibraryInfoProvider.GetMediaLibraryInfo(LibraryID);
            }
            return mLibraryInfo;
        }
        set
        {
            mLibraryInfo = value;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Uploaded file
    /// </summary>
    public override HttpPostedFile PostedFile
    {
        get
        {
            return ucFileUpload.PostedFile;
        }
    }


    /// <summary>
    /// File upload user control.
    /// </summary>
    public override CMSFileUpload FileUploadControl
    {
        get
        {
            return ucFileUpload;
        }
    }


    /// <summary>
    /// Indicates if supported browser.
    /// </summary>
    public bool IsSupportedBrowser
    {
        get
        {
            if (mIsSupportedBrowser == null)
            {
                string browser = DataHelper.GetNotEmpty(CMSContext.GetBrowserClass(), "").ToLower();
                mIsSupportedBrowser = browser.Contains("ie") || browser.Contains("gecko");
            }
            return mIsSupportedBrowser.Value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            // Do nothing
            Visible = false;
        }
        else
        {
            ScriptHelper.RegisterScriptFile(Page, "~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploader.js");

            // Prepare disabled image
            // Prepare loading image
            imgLoading.ImageUrl = ResolveUrl(LoadingImageUrl);
            lblLoading.CssClass = InnerLoadingDivClass;
            lblLoading.Text = InnerLoadingDivHtml;
            pnlLoading.Style.Add("display", "none");
            bool isRTL = (IsLiveSite && CultureHelper.IsPreferredCultureRTL()) || (!IsLiveSite && CultureHelper.IsUICultureRTL());
            if (isRTL)
            {
                pnlLoading.Style.Add("text-align", "right");
            }

            // Hide uploader if browser is unsupported
            if (!IsSupportedBrowser)
            {
                plcUploader.Visible = false;
                plcStandard.Visible = true;
                imgStandard.ImageUrl = ResolveUrl(ImageUrl);
                lblStandard.Text = InnerDivHtml;
                lblStandard.CssClass = InnerDivClass;

                // Register dialog script
                ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
                string modalDialog = "modalDialog('" + CMSContext.ResolveDialogUrl(GetDialogUrl()) + "', 'imageEditorDialog', 300, 180);";
                imgStandard.Attributes.Add("onclick", modalDialog);
                lblStandard.Attributes.Add("onclick", modalDialog);
            }
            else
            {
                ltlCss.Text = CreateCss();

                // Initialize uploader
                HtmlGenericControl lblContainer = (HtmlGenericControl)FindControl("container");
                lblContainer.Attributes.Add("class", "container");
                FileUploadControl.Attributes.Add("class", "fileUpload");
                FileUploadControl.Attributes.Add("onchange", "document.getElementById('" + pnlLoading.ClientID + "').style.display='';document.getElementById('" + lblContainer.ClientID + "').style.display='none';" + Page.ClientScript.GetPostBackEventReference(btnHidden, null, false));

                ltlScript.Text += ScriptHelper.GetScript("var uploaderInitilaized=false; function InitUploader(){if((window.SI) && (!uploaderInitilaized)){ uploaderInitilaized=true; SI.Files.stylizeById('" + FileUploadControl.ClientID + "');}} InitUploader();");

                string rtl = (isRTL) ? "var rtl = true;" : "var rtl = false;";
                ltlInnerDiv.Text = ScriptHelper.GetScript("var innerDivHtml = '" + InnerDivHtml.Replace("'", "\\\'") + "';\nvar innerDivClass='" + InnerDivClass.Replace("'", "\\\'") + "';" + rtl);
                btnHidden.Attributes.Add("style", "display:none;");
            }
        }
    }


    protected void btnHidden_Click(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            switch (SourceType)
            {
                case MediaSourceEnum.MediaLibraries:
                    HandleLibrariesUpload();
                    break;

                default:
                    break;
            }
        }
    }

    #endregion


    #region "Media libraries"

    /// <summary>
    /// Provides operations necessary to create and store new library file.
    /// </summary>
    private void HandleLibrariesUpload()
    {
        // Get related library info        
        if (LibraryInfo != null)
        {
            MediaFileInfo mediaFile = null;

            // Get the site name
            string siteName = CMSContext.CurrentSiteName;
            SiteInfo si = SiteInfoProvider.GetSiteInfo(LibraryInfo.LibrarySiteID);
            if (si != null)
            {
                siteName = si.SiteName;
            }

            string message = string.Empty;
            try
            {
                // Check the allowed extensions
                CheckAllowedExtensions();

                if (MediaFileID > 0)
                {
                    #region "Check permissions"

                    if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "FileModify"))
                    {
                        throw new Exception(ResHelper.GetString("media.security.nofilemodify"));
                    }

                    #endregion

                    mediaFile = MediaFileInfoProvider.GetMediaFileInfo(MediaFileID);
                    if (mediaFile != null)
                    {
                        if (IsMediaThumbnail)
                        {
                            string newFileExt = Path.GetExtension(ucFileUpload.FileName).TrimStart('.');
                            if ((ImageHelper.IsImage(newFileExt)) && (newFileExt.ToLower() != "ico") &&
                                (newFileExt.ToLower() != "wmf"))
                            {
                                // Update or creation of Media File update
                                string previewSuffix = MediaLibraryHelper.GetMediaFilePreviewSuffix(siteName);

                                if (!String.IsNullOrEmpty(previewSuffix))
                                {
                                    string previewExtension = Path.GetExtension(ucFileUpload.PostedFile.FileName);
                                    string previewName = Path.GetFileNameWithoutExtension(MediaLibraryHelper.GetPreviewFileName(mediaFile.FileName, mediaFile.FileExtension, previewExtension, siteName, previewSuffix));
                                    string previewFolder = MediaLibraryHelper.EnsurePath(LibraryFolderPath.TrimEnd('/')) + "\\" + MediaLibraryHelper.GetMediaFileHiddenFolder(siteName);

                                    byte[] previewFileBinary = new byte[ucFileUpload.PostedFile.ContentLength];
                                    ucFileUpload.PostedFile.InputStream.Read(previewFileBinary, 0, ucFileUpload.PostedFile.ContentLength);

                                    // Delete current preview thumbnails
                                    MediaFileInfoProvider.DeleteMediaFilePreview(siteName, mediaFile.FileLibraryID, mediaFile.FilePath, false);

                                    // Save preview file
                                    MediaFileInfoProvider.SaveFileToDisk(siteName, LibraryInfo.LibraryFolder, previewFolder, previewName, previewExtension, mediaFile.FileGUID, previewFileBinary, true, false);

                                    // Log synchronization task
                                    TaskInfoProvider.LogSynchronization(mediaFile, TaskTypeEnum.UpdateObject, mediaFile.Connection, siteName);
                                }

                                // Drop the cache dependencies
                                CacheHelper.TouchKeys(MediaFileInfoProvider.GetDependencyCacheKeys(mediaFile, true));
                            }
                            else
                            {
                                message = ResHelper.GetString("media.file.onlyimgthumb");
                            }
                        }
                        else
                        {
                            // Remember original file data row - to keep custom field values
                            DataRow classDataRow = mediaFile.DataClass.DataRow;

                            // Delete existing media file
                            MediaFileInfoProvider.DeleteMediaFile(LibraryInfo.LibrarySiteID, LibraryInfo.LibraryID, mediaFile.FilePath, true, false);

                            // Update media file preview
                            if (MediaLibraryHelper.HasPreview(siteName, LibraryInfo.LibraryID, mediaFile.FilePath))
                            {
                                // Get new file path
                                string newPath = Path.GetDirectoryName(mediaFile.FilePath).TrimEnd('/') + "\\" + ucFileUpload.PostedFile.FileName;
                                newPath = MediaLibraryHelper.EnsureUniqueFileName(newPath);

                                // Get new unique file name
                                string newName = Path.GetFileName(newPath);

                                // Rename preview
                                MediaLibraryHelper.MoveMediaFilePreview(mediaFile, newName);

                                // Delete preview thumbnails
                                MediaFileInfoProvider.DeleteMediaFilePreviewThumbnails(mediaFile);
                            }

                            // Receive media info on newly posted file
                            mediaFile = GetUpdatedFile(classDataRow);

                            MediaFileInfoProvider.SetMediaFileInfo(mediaFile);
                        }
                    }
                }
                else
                {
                    // Creation of new media file

                    #region "Check permissions"

                    if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "FileCreate"))
                    {
                        throw new Exception(ResHelper.GetString("media.security.nofilecreate"));
                    }

                    #endregion

                    // No file for upload specified
                    if (!ucFileUpload.HasFile)
                    {
                        throw new Exception(ResHelper.GetString("media.newfile.errorempty"));
                    }

                    // Create new media file record
                    mediaFile = new MediaFileInfo(ucFileUpload.PostedFile, LibraryID, LibraryFolderPath, ResizeToWidth, ResizeToHeight, ResizeToMaxSideSize, LibraryInfo.LibrarySiteID);

                    mediaFile.FileDescription = "";

                    // Save the new file info
                    MediaFileInfoProvider.SetMediaFileInfo(mediaFile);

                }
            }
            catch (Exception ex)
            {
                // Creation of new media file failed
                message = ex.Message;
            }
            finally
            {
                // Create media file info string
                string mediaInfo = "";
                if ((mediaFile != null) && (mediaFile.FileID > 0) && (IncludeNewItemInfo))
                {
                    mediaInfo = mediaFile.FileID + "|" + LibraryFolderPath.Replace('\\', '>').Replace("'", "\\'");
                }

                // Ensure message text
                message = HTMLHelper.EnsureLineEnding(message, " ");

                if (RaiseOnClick)
                {
                    ScriptHelper.RegisterStartupScript(Page, typeof(Page), "UploaderOnClick", ScriptHelper.GetScript("if(typeof(parent.UploaderOnClick) == 'function') { parent.UploaderOnClick('" + MediaFileName.Replace(" ", "").Replace(".", "").Replace("-", "") + "'); }"));
                }

                // Call function to refresh parent window                                                     
                ltlScript.Text += ScriptHelper.GetScript(" if ((window.parent != null) && (/parentelemid=" + ParentElemID + "/i.test(window.location.href)) && (window.parent.InitRefresh_" + ParentElemID + " != null)){window.parent.InitRefresh_" + ParentElemID + "(" + ScriptHelper.GetString(message.Trim()) + ", false, '" + mediaInfo + "'" + (InsertMode ? ", 'insert'" : ", 'update'") + ");}");
            }
        }
    }


    /// <summary>
    /// Gets media file info object representing the updated version of original file
    /// </summary>
    /// <param name="origFileRow"></param>
    private MediaFileInfo GetUpdatedFile(DataRow origFileRow)
    {
        // Get info on media file from uploaded file
        MediaFileInfo mediaFile = new MediaFileInfo(ucFileUpload.PostedFile, LibraryID, LibraryFolderPath, ResizeToWidth, ResizeToHeight, ResizeToMaxSideSize, LibraryInfo.LibrarySiteID);

        // Create new file based on original
        MediaFileInfo updatedMediaFile = new MediaFileInfo(origFileRow);

        // Update necessary information
        updatedMediaFile.FileID = MediaFileID;
        updatedMediaFile.FileName = mediaFile.FileName;
        updatedMediaFile.FileExtension = mediaFile.FileExtension;
        updatedMediaFile.FileSize = mediaFile.FileSize;
        updatedMediaFile.FileMimeType = mediaFile.FileMimeType;
        updatedMediaFile.FilePath = mediaFile.FilePath;
        updatedMediaFile.FileCreatedWhen = mediaFile.FileCreatedWhen;
        updatedMediaFile.FileCreatedByUserID = mediaFile.FileCreatedByUserID;
        updatedMediaFile.FileModifiedByUserID = mediaFile.FileModifiedByUserID;
        updatedMediaFile.FileBinary = mediaFile.FileBinary;
        updatedMediaFile.FileImageHeight = mediaFile.FileImageHeight;
        updatedMediaFile.FileImageWidth = mediaFile.FileImageWidth;
        updatedMediaFile.FileBinaryStream = mediaFile.FileBinaryStream;

        return updatedMediaFile;
    }

    #endregion
}
