﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.SiteProvider;
using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;

public partial class CMSModules_SmartSearch_SearchIndex_Search : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }


    /// <summary>
    /// Search button click handler
    /// </summary>
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string url = UrlHelper.UpdateParameterInUrl(UrlHelper.CurrentURL, "searchText", HttpUtility.UrlEncode(txtSearchFor.Text));
        url = UrlHelper.RemoveParameterFromUrl(url, "page");
        UrlHelper.Redirect(url);
    }


    /// <summary>
    /// Perform search
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        // Check whether current request is not postback
        if (!RequestHelper.IsPostBack())
        {
            // Get current search text from query string
            string searchText = QueryHelper.GetString("searchtext", "");
            // Check whether search text is defined
            if (!string.IsNullOrEmpty(searchText))
            {
                // Get current index id
                int indexId = QueryHelper.GetInteger("IndexId", 0);
                // Get current index info object
                SearchIndexInfo sii = SearchIndexInfoProvider.GetSearchIndexInfo(indexId);
                // Check whether index info exists
                if (sii != null)
                {
                    // Keep search tect in search textbox
                    txtSearchFor.Text = searchText;

                    // Get positions and ranges for search method
                    int startPosition = 0;
                    int numberOfProceeded = 100;
                    int displayResults = 100;
                    int numberOfResults = 0;
                    if (pgrSearch.PageSize != 0 && pgrSearch.GroupSize != 0)
                    {
                        startPosition = (pgrSearch.GetCurrentPage() - 1) * pgrSearch.PageSize;
                        numberOfProceeded = (((pgrSearch.GetCurrentPage() / pgrSearch.GroupSize) + 1) * pgrSearch.PageSize * pgrSearch.GroupSize) + pgrSearch.PageSize;
                        displayResults = pgrSearch.PageSize;
                    }

                    // Search
                    DataSet results = SearchHelper.Search(searchText, null, null, null, "##ALL##", null, false, false, false, sii.IndexName, displayResults, startPosition, numberOfProceeded, (UserInfo)CMSContext.CurrentUser, out numberOfResults, null, null);

                    // Fill repeater with results
                    repSearchResults.DataSource = results;
                    repSearchResults.PagerForceNumberOfResults = numberOfResults;
                    repSearchResults.DataBind();
                }
            }
        }

        base.OnPreRender(e);
    }
}
