using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.Polls;
using CMS.UIControls;

public partial class CMSModules_Polls_Tools_Polls_List : CMSPollsPage
{
    
    protected void Page_Load(object sender, EventArgs e)
	{
        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Polls_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("Polls_New.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Polls_Poll/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        // Set the page title
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Polls_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Polls_Poll/object.png");
        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.Title.HelpTopicName = "polls_list";

        // Set poll list control
        PollsList.OnEdit += new EventHandler(PollsList_OnEdit);
        PollsList.WhereCondition = "PollGroupID IS NULL";
        PollsList.IsLiveSite = false;
	}


    /// <summary>
    /// Edit poll click handler.
    /// </summary>
    void PollsList_OnEdit(object sender, EventArgs e)
    {
        UrlHelper.Redirect(ResolveUrl("Polls_Edit.aspx?pollId=" + this.PollsList.SelectedItemID));
    }
}
