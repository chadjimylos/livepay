using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;

public partial class CMSModules_Content_Controls_Dialogs_Properties_HTMLAnchorProperties : ItemProperties
{
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!this.StopProcessing)
        {
            if (!UrlHelper.IsPostback())
            {
                // Load drop-down list data
                LoadAnchorNames();
                LoadAnchorIds();
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.StopProcessing)
        {
            if (ValidationHelper.GetBoolean(SessionHelper.GetValue("HideLinkText"), false))
            {
                this.txtLinkText.Visible = false;
                this.lblLinkText.Visible = false;
            }

            if (!UrlHelper.IsPostback())
            {
                // Initialize controls
                SetupControls();
            }
        }
        else
        {
            this.Visible = false;
        }
    }


    #region "Private methods"

    /// <summary>
    /// Initializes all the nested controls.
    /// </summary>
    private void SetupControls()
    {
        this.rbAnchorName.Text = ResHelper.GetString("dialogs.anchor.byname");
        this.rbAnchorId.Text = ResHelper.GetString("dialogs.anchor.byid");
        this.rbAnchorText.Text = ResHelper.GetString("dialogs.anchor.bytext");

        // Select by default
        if (!UrlHelper.IsPostback())
        {
            this.rbAnchorText.Checked = true;
            this.drpAnchorId.Enabled = false;
            this.drpAnchorName.Enabled = false;
        }

        Hashtable dialogParameters = SessionHelper.GetValue("DialogParameters") as Hashtable;
        if ((dialogParameters != null) && (dialogParameters.Count > 0))
        {
            // Hide the link text
            this.txtLinkText.Visible = false;
            this.lblLinkText.Visible = false;
            SessionHelper.SetValue("HideLinkText", true);

            LoadItemProperties(dialogParameters);
        }
        else
        {
            dialogParameters = SessionHelper.GetValue("DialogSelectedParameters") as Hashtable;
            if ((dialogParameters != null) && (dialogParameters.Count > 0))
            {
                LoadItemProperties(dialogParameters);
            }
        }

        // Get reffernce causing postback to hidden button
        string postBackRef = ControlsHelper.GetPostBackEventReference(this.hdnButton, "");
        string raiseOnAction = " function RaiseHiddenPostBack(){" + postBackRef + ";}\n";

        this.ltlScript.Text = ScriptHelper.GetScript(raiseOnAction);

        postBackRef = ControlsHelper.GetPostBackEventReference(this.btnHiddenUpdate, "");
        string postBackKeyDownRef = "var keynum;if(window.event){keynum = event.keyCode;}else if(event.which){keynum = event.which;}if(keynum == 13){" + postBackRef + "; return false;}";

        this.txtLinkText.Attributes["onchange"] = postBackRef;
        this.txtLinkText.Attributes["onkeydown"] = postBackKeyDownRef;
        this.rbAnchorName.InputAttributes["onchange"] = postBackRef;
        this.rbAnchorId.InputAttributes["onchange"] = postBackRef;
        this.rbAnchorText.InputAttributes["onchange"] = postBackRef;
        this.drpAnchorName.Attributes["onchange"] = postBackRef;
        this.drpAnchorId.Attributes["onchange"] = postBackRef;
        this.txtAnchorText.Attributes["onchange"] = postBackRef;
        this.txtAnchorText.Attributes["onkeydown"] = postBackKeyDownRef;
    }


    protected void btnHiddenUpdate_Click(object sender, EventArgs e)
    {
        SaveSession();
    }


    /// <summary>
    /// Save current properties into session
    /// </summary>
    private void SaveSession()
    {
        Hashtable savedProperties = SessionHelper.GetValue("DialogSelectedParameters") as Hashtable;
        if (savedProperties == null)
        {
            savedProperties = new Hashtable();
        } 
        Hashtable properties = GetItemProperties();
        foreach (DictionaryEntry entry in properties)
        {
            savedProperties[entry.Key] = entry.Value;
        }
        SessionHelper.SetValue("DialogSelectedParameters", savedProperties);
    }


    /// <summary>
    /// Loads available anchors by IDs passed from the underlying editor.
    /// </summary>
    private void LoadAnchorIds()
    {
        this.drpAnchorId.Items.Clear();

        // Get anchors by ID
        ArrayList anchorIds = (SessionHelper.GetValue("Ids") as ArrayList);
        if (anchorIds != null)
        {
            // Fill drop-down list with items
            foreach (string anchorId in anchorIds)
            {
                this.drpAnchorId.Items.Add(new ListItem(anchorId, anchorId));
            }
        }
        else
        {
            // If no items available
            string none = ResHelper.GetString("general.empty");

            this.drpAnchorId.Items.Add(new ListItem(none, none));
            this.drpAnchorId.Enabled = false;
            this.rbAnchorId.Enabled = false;
        }

        this.drpAnchorId.DataBind();
    }


    /// <summary>
    /// Loads available anchors by name passed from the underlying editor.
    /// </summary>
    private void LoadAnchorNames()
    {
        this.drpAnchorName.Items.Clear();

        // Get anchors by name
        ArrayList anchorNames = (SessionHelper.GetValue("Anchors") as ArrayList);
        if (anchorNames != null)
        {
            // Fill drop-down list with items
            foreach (string anchorName in anchorNames)
            {
                this.drpAnchorName.Items.Add(new ListItem(anchorName, anchorName));
            }
        }
        else
        {
            // If no items available
            string none = ResHelper.GetString("general.empty");

            this.drpAnchorName.Items.Add(new ListItem(none, none));
            this.drpAnchorName.Enabled = false;
            this.rbAnchorName.Enabled = false;
        }

        this.drpAnchorName.DataBind();
    }

    /// <summary>
    /// Setup radio buttons, dropdowns and text according to anchor type
    /// </summary>
    /// <param name="type">Type of anchor ("name","id","text")</param>
    private void SelectAnchorType(string type)
    {
        if (!String.IsNullOrEmpty(type))
        {
            this.rbAnchorName.Checked = false;
            this.rbAnchorId.Checked = false;
            this.rbAnchorText.Checked = false;
            this.drpAnchorName.Enabled = false;
            this.drpAnchorId.Enabled = false;
            this.txtAnchorText.Enabled = false;
            switch (type.ToLower())
            {
                case "name":
                    this.rbAnchorName.Checked = true;
                    this.drpAnchorName.Enabled = true;
                    break;
                case "id":
                    this.rbAnchorId.Checked = true;
                    this.drpAnchorId.Enabled = true;
                    break;
                case "text":
                default:
                    this.rbAnchorText.Checked = true;
                    this.txtAnchorText.Enabled = true;
                    break;
            }
        }
    }

    #endregion


    #region "Overriden methods"

    /// <summary>
    /// Loads the properites into control.
    /// </summary>
    /// <param name="properties">Properties to load.</param>
    public override void LoadItemProperties(Hashtable properties)
    {
        if (properties != null)
        {
            // Get link text and hide the textbox
            string linkText = ValidationHelper.GetString(properties[DialogParameters.ANCHOR_LINKTEXT], "");
            this.txtLinkText.Text = linkText;

            // If anchor by name is selected
            string anchorName = ValidationHelper.GetString(properties[DialogParameters.ANCHOR_NAME], "");
            if (!string.IsNullOrEmpty(anchorName))
            {
                bool found = false;
                foreach (ListItem item in drpAnchorName.Items)
                {
                    if (item.Value.ToLower() == anchorName.ToLower())
                    {
                        SelectAnchorType("name");
                        this.drpAnchorName.SelectedValue = anchorName;
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    foreach (ListItem item in drpAnchorId.Items)
                    {
                        if (item.Value.ToLower() == anchorName.ToLower())
                        {
                            SelectAnchorType("id");
                            this.drpAnchorId.SelectedValue = anchorName;
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                {
                    SelectAnchorType("text");
                    this.txtAnchorText.Text = anchorName.Replace(" ", "_");
                }
            }
        }
    }


    /// <summary>
    /// Returns all parameters of the selected item as name – value collection.
    /// </summary>
    public override Hashtable GetItemProperties()
    {
        Hashtable retval = new Hashtable();

        // Get selected information
        retval[DialogParameters.ANCHOR_LINKTEXT] = this.txtLinkText.Text.Replace("%", "%25");

        if (this.rbAnchorName.Checked)
        {
            retval[DialogParameters.ANCHOR_NAME] = this.drpAnchorName.SelectedValue.Replace("%", "%25");
        }
        else if (this.rbAnchorId.Checked)
        {
            retval[DialogParameters.ANCHOR_NAME] = this.drpAnchorId.SelectedValue.Replace("%", "%25");
        }
        else if (this.rbAnchorText.Checked)
        {
            retval[DialogParameters.ANCHOR_NAME] = this.txtAnchorText.Text.Replace(" ", "_").Replace("%", "%25");
        }
        retval[DialogParameters.EDITOR_CLIENTID] = QueryHelper.GetString(DialogParameters.EDITOR_CLIENTID, "").Replace("%", "%25");

        return retval;
    }


    /// <summary>
    /// Clears the properties form.
    /// </summary>
    public override void ClearProperties(bool hideProperties)
    {

        this.txtAnchorText.Text = "";
        this.txtLinkText.Text = "";
        this.drpAnchorId.SelectedIndex = 0;
        this.drpAnchorName.SelectedIndex = 0;

    }


    /// <summary>
    /// Validates the user input
    /// </summary>
    /// <returns></returns>
    public override bool Validate()
    {
        if (this.rbAnchorText.Checked && (this.txtAnchorText.Text.Trim() == ""))
        {
            this.lblError.Text = ResHelper.GetString("dialogs.anchor.emptyanchor");
            this.lblError.Visible = true;
            this.plnAnchorUpdate.Update();
            return false;
        }
        return true;
    }

    #endregion


    #region "Event handlers"

    protected void rbAnchorName_CheckedChanged(object sender, EventArgs e)
    {
        this.rbAnchorId.Checked = false;
        this.rbAnchorText.Checked = false;
        this.txtAnchorText.Enabled = false;
        this.drpAnchorId.Enabled = false;
        this.drpAnchorName.Enabled = true;
    }


    protected void rbAnchorId_CheckedChanged(object sender, EventArgs e)
    {
        this.rbAnchorName.Checked = false;
        this.rbAnchorText.Checked = false;
        this.txtAnchorText.Enabled = false;
        this.drpAnchorId.Enabled = true;
        this.drpAnchorName.Enabled = false;
    }


    protected void rbAnchorText_CheckedChanged(object sender, EventArgs e)
    {
        this.rbAnchorId.Checked = false;
        this.rbAnchorName.Checked = false;
        this.txtAnchorText.Enabled = true;
        this.drpAnchorId.Enabled = false;
        this.drpAnchorName.Enabled = false;
    }


    protected void hdnButton_Click(object sender, EventArgs e)
    {
        if (this.Validate())
        {
            // Get selected item information
            Hashtable properties = GetItemProperties();

            // Get JavaScript for inserting the item
            string script = CMSDialogHelper.GetAnchorItem(properties);
            if (!string.IsNullOrEmpty(script))
            {
                ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "insertItemScript", ScriptHelper.GetScript(script));
            }
        }
    }

    #endregion
}
