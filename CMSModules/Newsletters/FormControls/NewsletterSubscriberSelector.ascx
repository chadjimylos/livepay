﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewsletterSubscriberSelector.ascx.cs"
    Inherits="CMSModules_Newsletters_FormControls_NewsletterSubscriberSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="usSubscribers" runat="server" ObjectType="newsletter.subscriber"
            SelectionMode="SingleTextBox" AllowEditTextBox="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
