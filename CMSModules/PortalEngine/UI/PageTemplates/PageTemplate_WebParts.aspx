<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageTemplate_WebParts.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_WebParts" Theme="Default"
    ValidateRequest="false" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Page Template Edit - Web Parts" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblWarning" Style="font-weight: bold; display: block"
        EnableViewState="false" />
    <br />
    <table style="width: 100%;" cellpadding="0">
        <tr>
            <td>
                <asp:Label runat="server" ID="lblWPConfig" EnableViewState="false" /><br />
                <asp:TextBox runat="server" ID="txtWebParts" TextMode="MultiLine" Width="98%" Height="480" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton" /></td>
        </tr>
    </table>
</asp:Content>
