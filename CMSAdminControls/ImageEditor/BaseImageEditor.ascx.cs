using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;

using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.FileManager;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.Staging;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;
using CMS.ExtendedControls;
using CMS.EventLog;

using TreeNode = CMS.TreeEngine.TreeNode;

/// <summary>
/// General Image editor class which handles displaing content.
/// </summary>
public partial class CMSAdminControls_ImageEditor_BaseImageEditor : CMSUserControl
{
    #region "Variables"

    protected string attUrl = "";
    protected ImageHelper imgHelper = null;
    protected string currentFormat = null;
    protected int widthTrimValue = 0;
    protected int heightTrimValue = 0;
    protected bool userInputTrimOK = false;
    protected bool userInputResizeOK = false;
    protected int percentResizeValue = 0;
    protected int widthResizeValue = 0;
    protected int heightResizeValue = 0;
    protected ImageHelper.ImageTypeEnum imageType = ImageHelper.ImageTypeEnum.None;
    protected bool forceReload = false;
    protected bool loadingFailed = false;
    protected TreeProvider tree = null;
    protected string mGetNameResult = null;
    protected bool isPreview = false;

    #endregion


    #region "Properties"

    /// <summary>
    /// Attachment URL.
    /// </summary>
    public string AttUrl
    {
        get
        {
            return attUrl;
        }
        set
        {
            attUrl = value;
        }
    }


    /// <summary>
    /// Image type.
    /// </summary>
    public ImageHelper.ImageTypeEnum ImageType
    {
        get
        {
            return imageType;
        }
        set
        {
            imageType = value;
        }
    }


    /// <summary>
    /// Tree of current file.
    /// </summary>
    public TreeProvider Tree
    {
        get
        {
            return tree;
        }
        set
        {
            tree = value;
        }
    }


    /// <summary>
    /// Image helper instance.
    /// </summary>
    public ImageHelper ImgHelper
    {
        get
        {
            return imgHelper;
        }
        set
        {
            imgHelper = value;
        }
    }


    /// <summary>
    /// Indicates if loading failed.
    /// </summary>
    public bool LoadingFailed
    {
        get
        {
            return loadingFailed;
        }
        set
        {
            loadingFailed = value;
        }
    }


    /// <summary>
    /// Loading failed label.
    /// </summary>
    public LocalizedLabel LblLoadFailed
    {
        get
        {
            return lblLoadFailed;
        }
        set
        {
            lblLoadFailed = value;
        }
    }


    /// <summary>
    /// File name text box.
    /// </summary>
    public TextBox TxtFileName
    {
        get
        {
            return txtFileName;
        }
        set
        {
            txtFileName = value;
        }
    }


    /// <summary>
    /// Extension label.
    /// </summary>
    public LocalizedLabel LblExtensionValue
    {
        get
        {
            return lblExtensionValue;
        }
        set
        {
            lblExtensionValue = value;
        }
    }


    /// <summary>
    /// Extension label.
    /// </summary>
    public LocalizedLabel LblImageSizeValue
    {
        get
        {
            return lblImageSizeValue;
        }
        set
        {
            lblImageSizeValue = value;
        }
    }


    /// <summary>
    /// Width label.
    /// </summary>
    public LocalizedLabel LblWidthValue
    {
        get
        {
            return lblWidthValue;
        }
        set
        {
            lblWidthValue = value;
        }
    }


    /// <summary>
    /// Height label.
    /// </summary>
    public LocalizedLabel LblHeightValue
    {
        get
        {
            return lblHeightValue;
        }
        set
        {
            lblHeightValue = value;
        }
    }

    /// <summary>
    /// Current format of image
    /// </summary>
    public string CurrentFormat
    {
        get
        {
            return currentFormat;
        }
        set
        {
            currentFormat = value;
        }
    }


    /// <summary>
    /// Script literal.
    /// </summary>
    public Literal LtlScript
    {
        get
        {
            return ltlScript;
        }
        set
        {
            ltlScript = value;
        }
    }


    /// <summary>
    /// Error label for file name validation.
    /// </summary>
    public LocalizedLabel LblFileNameValidationFailed
    {
        get
        {
            return lblFileNameValidationFailed;
        }
        set
        {
            lblFileNameValidationFailed = value;
        }
    }


    /// <summary>
    /// Result for renaming file.
    /// </summary>
    public string GetNameResult
    {
        get
        {
            return mGetNameResult;
        }
        set
        {
            mGetNameResult = value;
        }
    }


    /// <summary>
    /// Indicates if loaded image is thumbnail.
    /// </summary>
    public bool IsPreview
    {
        get
        {
            return isPreview;
        }
        set
        {
            isPreview = value;
        }
    }

    #endregion


    #region "Events"

    /// <summary>
    /// Loads image type from querystring.
    /// </summary>
    public delegate void OnLoadImageType();
    public event OnLoadImageType LoadImageType;


    /// <summary>
    /// Loads image URL.
    /// </summary>
    public delegate void OnLoadImageUrl();
    public event OnLoadImageUrl LoadImageUrl;


    /// <summary>
    /// Initializes common properties used for processing image. 
    /// </summary>
    public delegate void OnInitializeProperties();
    public event OnInitializeProperties InitializeProperties;


    /// <summary>
    /// Initialize labels according to current image type.
    /// </summary>
    public delegate void OnInitializeLabels(bool reloadName);
    public event OnInitializeLabels InitializeLabels;


    /// <summary>
    /// Saves modified image data.
    /// </summary>
    /// <param name="name">Image name</param>
    /// <param name="extension">Image extension</param>
    /// <param name="mimetype">Image mimetype</param>
    /// <param name="binary">Image binary data</param>
    /// <param name="width">Image width</param>
    /// <param name="height">Image height</param>
    public delegate void OnSaveImage(string name, string extension, string mimetype, byte[] binary, int width, int height);
    public event OnSaveImage SaveImage;


    /// <summary>
    /// Returns image name according to image type.
    /// </summary>
    /// <returns>Image name</returns>
    public delegate void OnGetName();
    public event OnGetName GetName;


    /// <summary>
    /// Sets image name according to file type.
    /// </summary>
    /// <param name="name">New name</param>
    public delegate void OnSetName(string name);
    public event OnSetName SetName;


    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterTrimScript();

        if (LoadImageType != null)
        {
            LoadImageType();
        }

        // Register tooltip script
        ScriptHelper.RegisterTooltip(this.Page);

        ltlScript.Text = "";

        // Use appropriate parameter from URL in parent control

        // Display image if available data
        if (imageType != ImageHelper.ImageTypeEnum.None)
        {
            if (InitializeProperties != null)
            {
                InitializeProperties();
            }

            if (!loadingFailed)
            {
                currentFormat = imgHelper.ImageFormatToString();
            }
        }

        InitializeStrings(!RequestHelper.IsPostBack());
        InitializeFields();
    }


    /// <summary>
    /// Registeres client script for trim buttons.
    /// </summary>
    private void RegisterTrimScript()
    {
        // Script for trimming
        string script =
            "function TrimSelected(button, clientID) {\n" +
                "document.getElementById('" + btnTopLeft.ClientID + "').src = '" + GetImageUrl("Design/Controls/ImageEditor/topleft.png") + "';\n" +
                "document.getElementById('" + btnTopCenter.ClientID + "').src = '" + GetImageUrl("Design/Controls/ImageEditor/topcenter.png") + "';\n" +
                "document.getElementById('" + btnTopRight.ClientID + "').src = '" + GetImageUrl("Design/Controls/ImageEditor/topright.png") + "';\n" +
                "document.getElementById('" + btnMiddleLeft.ClientID + "').src = '" + GetImageUrl("Design/Controls/ImageEditor/middleleft.png") + "';\n" +
                "document.getElementById('" + btnMiddleCenter.ClientID + "').src = '" + GetImageUrl("Design/Controls/ImageEditor/middlecenter.png") + "';\n" +
                "document.getElementById('" + btnMiddleRight.ClientID + "').src = '" + GetImageUrl("Design/Controls/ImageEditor/middleright.png") + "';\n" +
                "document.getElementById('" + btnBottomLeft.ClientID + "').src = '" + GetImageUrl("Design/Controls/ImageEditor/bottomleft.png") + "';\n" +
                "document.getElementById('" + btnBottomCenter.ClientID + "').src = '" + GetImageUrl("Design/Controls/ImageEditor/bottomcenter.png") + "';\n" +
                "document.getElementById('" + btnBottomRight.ClientID + "').src = '" + GetImageUrl("Design/Controls/ImageEditor/bottomright.png") + "';\n" +
                "if (document.getElementById('" + hidTrim.ClientID + "').value != button)\n" +
                "{\n" +
                "   document.getElementById('" + hidTrim.ClientID + "').value = button;\n" +
                "   document.getElementById(clientID).src = '" + GetImageUrl("Design/Controls/ImageEditor/") + "'+ button +'selected.png';\n" +
                "   BTN_Enable('" + btnTrim.ClientID + "');\n" +
                "}\n" +
                "else\n" +
                "{\n" +
                "   document.getElementById('" + hidTrim.ClientID + "').value = '';\n" +
                "   BTN_Disable('" + btnTrim.ClientID + "');\n" +
                "}\n" +
                "return true;\n" +
            "}\n";

        ScriptHelper.RegisterClientScriptBlock(base.Page, typeof(string), "TrimSelected", ScriptHelper.GetScript(script));
    }


    /// <summary>
    /// Set correct image size to the javascript.
    /// </summary>
    private void SetScriptProperties(bool reload)
    {
        string script;

        // Set image size at the moment of Page_Load
        if (!reload)
        {
            script = "var ImageWidth = " + imgHelper.ImageWidth + "; var ImageHeight = " + imgHelper.ImageHeight + ";";
            ScriptHelper.RegisterStartupScript(this, typeof(string), "CorrectSize", ScriptHelper.GetScript(script));
        }
        // Set new image size at the moment of button click event
        else
        {
            script = "ImageWidth = " + imgHelper.ImageWidth + "; ImageHeight = " + imgHelper.ImageHeight + ";";
            ScriptHelper.RegisterStartupScript(this, typeof(string), "NewSize", ScriptHelper.GetScript(script));
        }
    }


    /// <summary>
    /// Registeres client script for trim buttons.
    /// </summary>
    private void RegisterResizeScript()
    {
        // Script for locking aspect ratio - width
        string script = "function ChangedWidth() {\n" +
                "var width = document.getElementById('" + this.txtResizeWidth.ClientID + "');\n" +
                "if ((width.value > 0) && document.getElementById('" + this.chkMaintainRatio.ClientID + "').checked)\n" +
                "{\n" +
                    "var newValue = Math.round((width.value / ImageWidth) * ImageHeight);\n" +
                    "if (newValue > 9999) newValue = 9999;\n" +
                    "document.getElementById('" + this.txtResizeHeight.ClientID + "').value = newValue;\n" +
                "}\n" +
                "return false;\n" +
            "}\n";

        ScriptHelper.RegisterClientScriptBlock(base.Page, typeof(string), "ChangedWidth", ScriptHelper.GetScript(script));

        // Script for locking aspect ratio - height
        script = "function ChangedHeight() {\n" +
                "var height = document.getElementById('" + this.txtResizeHeight.ClientID + "');\n" +
                "if ((height.value > 0) && document.getElementById('" + this.chkMaintainRatio.ClientID + "').checked)\n" +
                "{\n" +
                    "var newValue = Math.round((height.value / ImageHeight) * ImageWidth);\n" +
                    "if (newValue > 9999) newValue = 9999;\n" +
                    "document.getElementById('" + this.txtResizeWidth.ClientID + "').value = newValue;\n" +
                "}\n" +
                "return false;\n" +
            "}\n";

        ScriptHelper.RegisterClientScriptBlock(base.Page, typeof(string), "ChangedHeight", ScriptHelper.GetScript(script));

        // Script for locking aspect ratio - height
        script = "function ChangedPercent() {\n" +
                "var percent = document.getElementById('" + this.txtResizePercent.ClientID + "');\n" +
                "if (percent.value > 0)\n" +
                "{\n" +
                    "var newWidth = Math.round((percent.value * ImageWidth)/100);\n" +
                    "var newHeight = Math.round((percent.value * ImageHeight)/100);\n" +
                    "var newRatio = 1;\n" +
                    "if (newWidth > newHeight)\n" +
                    "{\n" +
                        "if (newWidth > 9999) \n" +
                        "{\n" +
                            "newRatio = (Math.floor((9999/newWidth)*100)/100); newWidth = newRatio * newWidth; newHeight = newRatio * newHeight; percent.value = percent.value * newRatio;\n" +
                        "}\n" +
                    "} else { \n" +
                        "if (newHeight > 9999) \n" +
                        "{\n" +
                            "newRatio = (Math.floor((9999/newHeight)*100)/100); newWidth = newRatio * newWidth; newHeight = newRatio * newHeight; percent.value = percent.value * newRatio;\n" +
                        "}\n" +
                    "}\n" +
                    "document.getElementById('" + this.txtResizeWidth.ClientID + "').value = newWidth;\n" +
                    "document.getElementById('" + this.txtResizeHeight.ClientID + "').value = newHeight;\n" +
                "}\n" +
                "return false;\n" +
            "}\n";

        ScriptHelper.RegisterClientScriptBlock(base.Page, typeof(string), "ChangedPercent", ScriptHelper.GetScript(script));
    }


    /// <summary>
    /// Script to disable/enable convert quality for JPG conversions
    /// </summary>
    private void RegisterDropDownScript()
    {
        string script = "function ChangedConvert() {\n" +
                "var convert = document.getElementById('" + this.drpConvert.ClientID + "');\n" +
                "if (convert.value == 'jpg') {document.getElementById('" + this.txtQuality.ClientID + "').disabled = false;}\n" +
                "else {document.getElementById('" + this.txtQuality.ClientID + "').disabled = true;}\n" +
                "return false;\n" +
            "}\n";

        ScriptHelper.RegisterClientScriptBlock(base.Page, typeof(string), "ChangedConvert", ScriptHelper.GetScript(script));
    }


    /// <summary>
    /// Checks user input
    /// </summary>
    private void CheckTrimFields()
    {
        if (imgHelper != null)
        {
            widthTrimValue = ValidationHelper.GetInteger(txtWidthTrim.Text, 0);
            heightTrimValue = ValidationHelper.GetInteger(txtHeightTrim.Text, 0);

            // If user input fails validation
            if ((widthTrimValue <= 0) || (widthTrimValue > imgHelper.ImageWidth) ||
                (heightTrimValue <= 0) || (heightTrimValue > imgHelper.ImageHeight))
            {
                userInputTrimOK = false;
                ValidationFailedTrim.Visible = true;
                txtResizePercentChanged(null, null);
                txtWidthTrim.Text = imgHelper.ImageWidth.ToString();
                txtHeightTrim.Text = imgHelper.ImageHeight.ToString();
            }
            // If user input is OK
            else
            {
                userInputTrimOK = true;
                ValidationFailedTrim.Visible = false;
            }
        }
    }


    /// <summary>
    /// Checks user input when resizing image
    /// </summary>
    private void CheckResizeFields()
    {
        if (imgHelper != null)
        {
            percentResizeValue = ValidationHelper.GetInteger(this.txtResizePercent.Text, 0);
            widthResizeValue = ValidationHelper.GetInteger(this.txtResizeWidth.Text, 0);
            heightResizeValue = ValidationHelper.GetInteger(this.txtResizeHeight.Text, 0);

            // Check input for percent value
            if (this.radByPercentage.Checked)
            {
                if ((percentResizeValue <= 0))
                {
                    userInputResizeOK = false;
                    lblValidationFailedResize.Visible = true;
                }
                else
                {
                    // Make sure that width and height are corresponding to percent value
                    txtResizePercentChanged(null, null);
                    userInputResizeOK = true;
                }
            }
            // Check input for absolute values
            else if (this.radByAbsolute.Checked)
            {
                // If user input is less then 1 then display error message
                if ((widthResizeValue <= 0) || (heightResizeValue <= 0) || (widthResizeValue > 9999) || (heightResizeValue > 9999))
                {
                    userInputResizeOK = false;
                    lblValidationFailedResize.Visible = true;
                }
                else
                {
                    userInputResizeOK = true;
                }
            }
        }
    }


    /// <summary>
    /// Forces auto updating width and height fields when changing percent value of image size.
    /// </summary>
    protected void txtResizePercentChanged(object sender, EventArgs e)
    {
        int percent = ValidationHelper.GetInteger(this.txtResizePercent.Text, 0);

        // Modify width and height only if percent value is within limits
        if (percent == 0)
        {
            this.txtResizePercent.Text = "100";
            percent = 100;
        }

        // Compute width and height according to % value
        int convertWidth = widthResizeValue = (int)((Convert.ToDouble(txtResizePercent.Text) / (Double)100) * (Convert.ToDouble(imgHelper.ImageWidth)));
        int convertHeight = heightResizeValue = (int)((Convert.ToDouble(txtResizePercent.Text) / (Double)100) * (Convert.ToDouble(imgHelper.ImageHeight)));

        this.txtResizeWidth.Text = convertWidth.ToString();
        this.txtResizeHeight.Text = convertHeight.ToString();
    }


    /// <summary>
    /// Initializes all strings on the page.
    /// </summary>
    protected void InitializeStrings(bool reloadName)
    {
        // Initialize strings that are not dependent on any value
        btnRotate90Left.AlternateText = ResHelper.GetString("img.rotate90left");
        btnRotate90Right.AlternateText = ResHelper.GetString("img.rotate90right");
        btnFlipHorizontal.AlternateText = ResHelper.GetString("img.fliphorizontal");
        btnFlipVertical.AlternateText = ResHelper.GetString("img.flipvertical");
        btnConvert.Text = ResHelper.GetString("general.ok");
        btnGrayscale.AlternateText = ResHelper.GetString("img.grayscale");
        btnGrayscale.Attributes.Add("onclick", "if(confirm(" + ScriptHelper.GetString(ResHelper.GetString("img.confirm")) + ")){}else{return false}");
        lblBtnGrayscale.Attributes.Add("onclick", "if(confirm(" + ScriptHelper.GetString(ResHelper.GetString("img.confirm")) + ")){}else{return false}");
        btnTopLeft.AlternateText = ResHelper.GetString("img.topleft");
        btnTopCenter.AlternateText = ResHelper.GetString("img.topcenter");
        btnTopRight.AlternateText = ResHelper.GetString("img.topright");
        btnMiddleLeft.AlternateText = ResHelper.GetString("img.middleleft");
        btnMiddleCenter.AlternateText = ResHelper.GetString("img.middlecenter");
        btnMiddleRight.AlternateText = ResHelper.GetString("img.middleright");
        btnBottomLeft.AlternateText = ResHelper.GetString("img.bottomleft");
        btnBottomCenter.AlternateText = ResHelper.GetString("img.bottomcenter");
        btnBottomRight.AlternateText = ResHelper.GetString("img.bottomright");
        radByPercentage.AutoPostBack = true;
        radByPercentage.ResourceString = "img.resizebypercent";
        radByAbsolute.ResourceString = "img.resizebyabsolute";
        chkMaintainRatio.ResourceString = "img.maintainratio";
        btnRotate90Left.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/rotate90left.png");
        btnRotate90Right.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/rotate90right.png");
        btnFlipHorizontal.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/fliphorizontal.png");
        btnFlipVertical.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/flipvertical.png");
        btnGrayscale.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/grayscale.png");
        btnChangeFileName.Text = ResHelper.GetString("general.ok");
        lblFileNameValidationFailed.Visible = false;
        lblQualityFailed.Visible = false;
        lblValidationFailedResize.Visible = false;
        ValidationFailedTrim.Visible = false;
        lblLoadFailed.Visible = loadingFailed;

        // Set client script to each trim button
        btnTopLeft.Attributes.Add("onclick", "TrimSelected('topleft', '" + btnTopLeft.ClientID + "'); return false;");
        btnTopCenter.Attributes.Add("onclick", "TrimSelected('topcenter', '" + btnTopCenter.ClientID + "'); return false;");
        btnTopRight.Attributes.Add("onclick", "TrimSelected('topright', '" + btnTopRight.ClientID + "' ); return false;");
        btnMiddleLeft.Attributes.Add("onclick", "TrimSelected('middleleft', '" + btnMiddleLeft.ClientID + "'); return false;");
        btnMiddleCenter.Attributes.Add("onclick", "TrimSelected('middlecenter', '" + btnMiddleCenter.ClientID + "'); return false;");
        btnMiddleRight.Attributes.Add("onclick", "TrimSelected('middleright', '" + btnMiddleRight.ClientID + "'); return false;");
        btnBottomLeft.Attributes.Add("onclick", "TrimSelected('bottomleft', '" + btnBottomLeft.ClientID + "'); return false;");
        btnBottomCenter.Attributes.Add("onclick", "TrimSelected('bottomcenter', '" + btnBottomCenter.ClientID + "'); return false;");
        btnBottomRight.Attributes.Add("onclick", "TrimSelected('bottomright', '" + btnBottomRight.ClientID + "'); return false;");
        btnTrim.Attributes.Add("onclick", "if(confirm(" + ScriptHelper.GetString(ResHelper.GetString("img.confirm")) + ")){}else{return false}");

        btnTopLeft.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/topleft.png");
        btnTopCenter.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/topcenter.png");
        btnTopRight.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/topright.png");
        btnMiddleLeft.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/middleleft.png");
        btnMiddleCenter.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/middlecenterselected.png");
        btnMiddleRight.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/middleright.png");
        btnBottomLeft.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/bottomleft.png");
        btnBottomCenter.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/bottomcenter.png");
        btnBottomRight.ImageUrl = GetImageUrl("Design/Controls/ImageEditor/bottomright.png");

        // Initialize strings that are depending on loaded image
        if (imgHelper != null)
        {
            if ((!RequestHelper.IsPostBack()) || (forceReload))
            {
                txtWidthTrim.Text = imgHelper.ImageWidth.ToString();
                txtHeightTrim.Text = imgHelper.ImageHeight.ToString();
                this.txtResizePercent.Text = "100";
                this.txtResizeWidth.Text = imgHelper.ImageWidth.ToString();
                this.txtResizeHeight.Text = imgHelper.ImageHeight.ToString();

                hidTrim.Value = "middlecenter";
                txtResizePercentChanged(null, null);
            }

            imgMain.Width = new Unit(imgHelper.ImageWidth, UnitType.Pixel);
            imgMain.Height = new Unit(imgHelper.ImageHeight, UnitType.Pixel);

            //Initialize labels depending on image type in parent control
            if (InitializeLabels != null)
            {
                InitializeLabels(reloadName);
            }

            // Register client scripts 
            RegisterResizeScript();
            SetScriptProperties(forceReload);
            RegisterDropDownScript();
            this.txtResizePercent.Attributes.Add("onchange", "ChangedPercent(); return false;");
            this.txtResizeWidth.Attributes.Add("onchange", "ChangedWidth(); return false;");
            this.txtResizeHeight.Attributes.Add("onchange", "ChangedHeight(); return false;");
            this.chkMaintainRatio.Attributes.Add("onchange", "ChangedWidth(); return false;");
            this.drpConvert.Attributes.Add("onchange", "ChangedConvert(); return false;");
        }
    }


    /// <summary>
    /// Loads fields that are changed by Postbacks
    /// </summary>
    protected void InitializeFields()
    {
        // Initialize properties that are dependent on other form values
        if (radByPercentage.Checked)
        {
            txtResizePercent.Enabled = true;
        }
        else
        {
            txtResizePercent.Enabled = false;
        }

        if (radByAbsolute.Checked)
        {
            chkMaintainRatio.Enabled = true;
            txtResizeWidth.Enabled = true;
            txtResizeHeight.Enabled = true;
        }
        else
        {
            chkMaintainRatio.Enabled = false;
            txtResizeWidth.Enabled = false;
            txtResizeHeight.Enabled = false;
        }

        if (this.drpConvert.SelectedValue == "jpg")
        {
            this.txtQuality.Enabled = true;
        }
        else
        {
            this.txtQuality.Enabled = false;
        }

        // Disable changing file name for 
        if (this.isPreview)
        {
            btnChangeFileName.Enabled = false;
            txtFileName.Enabled = false;
        }
    }


    /// <summary>
    /// Populates dropdown list with available image formats.
    /// </summary>
    protected void PopulateConversionDropdownlist()
    {
        drpConvert.Items.Clear();
        if (currentFormat != null)
        {
            foreach (string item in Enum.GetNames(typeof(ImageHelper.SupportedTypesEnum)))
            {
                // If current image type is not equal with one from enumeration then add it to drop-down list
                // If current image type is jpg then add it to drop-down list so that you can change image quality
                if ((item.CompareTo(currentFormat.ToLowerInvariant()) != 0) || (item.CompareTo("jpg") == 0))
                {
                    drpConvert.Items.Add(item);
                }
            }
        }
    }


    /// <summary>
    /// Propagates all changed image properties to other fields.
    /// </summary>
    public void PropagateChanges(bool reloadName)
    {
        forceReload = true;
        if (InitializeProperties != null)
        {
            InitializeProperties();
        }
        InitializeStrings(reloadName);
        InitializeFields();
        forceReload = false;
    }


    /// <summary>
    /// Button btnGrayscale event handler.
    /// </summary>
    protected void btnGrayscaleClick(object sender, EventArgs e)
    {
        if (imgHelper != null)
        {
            // Grayscale attachment or metafile
            if (SaveImage != null)
            {
                SaveImage("", "", "", imgHelper.GetGrayscaledImageData(), 0, 0);
            }
            PropagateChanges(false);
        }
    }


    /// <summary>
    /// Button btnRotate90Left event handler.
    /// </summary>
    protected void btnRotate90LeftClick(object sender, EventArgs e)
    {
        if (imgHelper != null)
        {
            if (SaveImage != null)
            {
                SaveImage("", "", "", imgHelper.GetRotatedImageData(ImageHelper.ImageRotationEnum.Rotate270), imgHelper.ImageHeight, imgHelper.ImageWidth);
            }
            PropagateChanges(false);
        }
    }


    /// <summary>
    /// Button btnRotate90Right  event handler.
    /// </summary>
    protected void btnRotate90RightClick(object sender, EventArgs e)
    {
        if (imgHelper != null)
        {
            if (SaveImage != null)
            {
                SaveImage("", "", "", imgHelper.GetRotatedImageData(ImageHelper.ImageRotationEnum.Rotate90), imgHelper.ImageHeight, imgHelper.ImageWidth);
            }
            PropagateChanges(false);
        }
    }


    /// <summary>
    /// Button FlipHorizontal  event handler.
    /// </summary>
    protected void btnFlipHorizontalClick(object sender, EventArgs e)
    {
        if (imgHelper != null)
        {
            if (SaveImage != null)
            {
                SaveImage("", "", "", imgHelper.GetFlippedImageData(ImageHelper.ImageFlipEnum.FlipHorizontal), 0, 0);
            }
            PropagateChanges(false);
        }
    }


    /// <summary>
    /// Button FlipVertical event handler. 
    /// </summary>
    protected void btnFlipVerticalClick(object sender, EventArgs e)
    {
        if (imgHelper != null)
        {
            if (SaveImage != null)
            {
                SaveImage("", "", "", imgHelper.GetFlippedImageData(ImageHelper.ImageFlipEnum.FlipVertical), 0, 0);
            }
            PropagateChanges(false);
        }
    }


    /// <summary>
    /// Button Convert event handler.
    /// </summary>
    protected void btnConvertClick(object sender, EventArgs e)
    {
        if (imgHelper != null)
        {
            int qualityValue = ValidationHelper.GetInteger(this.txtQuality.Text, 0);
            if ((qualityValue <= 0) || (qualityValue > 100))
            {
                lblQualityFailed.Visible = true;
                txtQuality.Text = "100";
                txtResizePercentChanged(null, null);
                return;
            }
            else
            {
                lblQualityFailed.Visible = false;
            }

            // Create strings to change image name and type
            if (GetName != null)
            {
                GetName();
            }
            string name = GetNameResult;
            string extension = "";
            string mimetype = "";

            // Initialize new image name string
            currentFormat = this.drpConvert.SelectedValue;

            // Initialize rest of the image type string
            extension = "." + currentFormat;
            name += extension;
            mimetype = "image/";

            if (currentFormat == "jpg")
            {
                mimetype += "jpeg";
            }
            else
            {
                mimetype += currentFormat;
            }

            // Convert image
            if (currentFormat == "jpg")
            {
                if (!lblQualityFailed.Visible)
                {
                    if (SaveImage != null)
                    {
                        SaveImage(name, extension, mimetype, imgHelper.GetConvertedImageData(ImageHelper.StringToImageFormat(currentFormat), qualityValue), 0, 0);
                    }
                }
            }
            else
            {
                if (SaveImage != null)
                {
                    SaveImage(name, extension, mimetype, imgHelper.GetConvertedImageData(ImageHelper.StringToImageFormat(currentFormat), 100), 0, 0);
                }
            }

            // Update dropdown list with new conversion values
            lblActualFormat.Text = currentFormat;
            PopulateConversionDropdownlist();
            PropagateChanges(false);

            // Reset textbox values
            this.txtQuality.Enabled = false;
            this.txtQuality.Text = "100";
        }
    }


    /// <summary>
    /// Buttons Trim event handler.
    /// </summary>
    protected void btnTrimClick(object sender, EventArgs e)
    {
        CheckTrimFields();

        if ((imgHelper != null) && (userInputTrimOK))
        {
            byte[] field = null;

            // Use trimming according to sender object
            switch (hidTrim.Value)
            {
                case ("topleft"):
                    field = imgHelper.GetTrimmedImageData(widthTrimValue, heightTrimValue, ImageHelper.ImageTrimAreaEnum.TopLeft);
                    break;

                case ("topcenter"):
                    field = imgHelper.GetTrimmedImageData(widthTrimValue, heightTrimValue, ImageHelper.ImageTrimAreaEnum.TopCenter);
                    break;

                case ("topright"):
                    field = imgHelper.GetTrimmedImageData(widthTrimValue, heightTrimValue, ImageHelper.ImageTrimAreaEnum.TopRight);
                    break;

                case ("middleleft"):
                    field = imgHelper.GetTrimmedImageData(widthTrimValue, heightTrimValue, ImageHelper.ImageTrimAreaEnum.MiddleLeft);
                    break;

                case ("middlecenter"):
                    field = imgHelper.GetTrimmedImageData(widthTrimValue, heightTrimValue, ImageHelper.ImageTrimAreaEnum.MiddleCenter);
                    break;

                case ("middleright"):
                    field = imgHelper.GetTrimmedImageData(widthTrimValue, heightTrimValue, ImageHelper.ImageTrimAreaEnum.MiddleRight);
                    break;

                case ("bottomleft"):
                    field = imgHelper.GetTrimmedImageData(widthTrimValue, heightTrimValue, ImageHelper.ImageTrimAreaEnum.BottomLeft);
                    break;

                case ("bottomcenter"):
                    field = imgHelper.GetTrimmedImageData(widthTrimValue, heightTrimValue, ImageHelper.ImageTrimAreaEnum.BottomCenter);
                    break;

                case ("bottomright"):
                    field = imgHelper.GetTrimmedImageData(widthTrimValue, heightTrimValue, ImageHelper.ImageTrimAreaEnum.BottomRight);
                    break;
            }

            if (SaveImage != null)
            {
                SaveImage("", "", "", field, widthTrimValue, heightTrimValue);
            }
            PropagateChanges(false);
        }
    }


    /// <summary>
    /// Resize button event handler.
    /// </summary>
    protected void btnResizeClick(object sender, EventArgs e)
    {
        if (imgHelper != null)
        {
            // Check user input
            CheckResizeFields();
            //  If validation passed then resize image
            if (userInputResizeOK)
            {
                if (SaveImage != null)
                {
                    try
                    {
                        SaveImage("", "", "", imgHelper.GetResizedImageData(widthResizeValue, heightResizeValue), widthResizeValue, heightResizeValue);
                    }
                    catch (Exception ex)
                    {
                        LblLoadFailed.Visible = true;
                        LblLoadFailed.ResourceString = "img.errors.processing";
                        ScriptHelper.AppendTooltip(LblLoadFailed, ex.Message, "help");
                        EventLogProvider.LogException("Image editor", "RESIZEIMAGE", ex);
                        LoadingFailed = true;
                    }
                }
                PropagateChanges(false);

                txtResizePercentChanged(null, null);
            }
            // Otherwise reset input textboxes
            else
            {
                txtResizePercentChanged(null, null);
            }
        }
    }


    /// <summary>
    /// Change width and height textbox autopostbacks when keeping aspect ratio.
    /// </summary>
    protected void chkMaintainRatioChanged(object sender, EventArgs e)
    {
        if (chkMaintainRatio.Checked)
        {
            txtResizeHeight.AutoPostBack = true;
            txtResizeWidth.AutoPostBack = true;
        }
        else
        {
            txtResizeHeight.AutoPostBack = false;
            txtResizeWidth.AutoPostBack = false;
        }
    }


    /// <summary>
    /// Change file name.
    /// </summary>
    protected void btnChangeFileNameClick(object sender, EventArgs e)
    {
        // Check that user input is valid string
        string newName = UrlHelper.GetSafeFileName(ValidationHelper.GetString(txtFileName.Text.Trim(), ""), CMSContext.CurrentSiteName);
        Validator validator = new Validator().NotEmpty(newName, ResHelper.GetString("img.errors.filename"));

        // Check that user input is bigger then "" and that it is smaller then DB field
        if ((newName.Length <= 245) && (String.IsNullOrEmpty(validator.Result)))
        {
            if (imgHelper != null)
            {
                if (SetName != null)
                {
                    SetName(newName);
                }
            }
        }
        else
        {
            lblFileNameValidationFailed.Visible = true;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Load image URL
        if (LoadImageUrl != null)
        {
            LoadImageUrl();
        }

        if (imageType != ImageHelper.ImageTypeEnum.None)
        {
            if (!loadingFailed)
            {
                // Load image to frame
                HtmlControl frame1 = (HtmlControl)FindControl("frame1");
                frame1.Attributes["src"] = UrlHelper.GetAbsoluteUrl(attUrl);
                // Load image to this page
                imgMain.ImageUrl = attUrl;

                this.lblActualFormat.Text = currentFormat;

                if (!RequestHelper.IsPostBack())
                {
                    PopulateConversionDropdownlist();
                }
            }
        }

        if (LoadingFailed)
        {
            this.btnBottomCenter.Enabled = false;
            this.btnBottomLeft.Enabled = false;
            this.btnBottomRight.Enabled = false;
            this.btnChangeFileName.Enabled = false;
            this.btnConvert.Enabled = false;
            this.btnFlipHorizontal.Enabled = false;
            this.btnFlipVertical.Enabled = false;
            this.btnGrayscale.Enabled = false;
            this.btnMiddleCenter.Enabled = false;
            this.btnMiddleLeft.Enabled = false;
            this.btnMiddleRight.Enabled = false;
            this.btnResize.Enabled = false;
            this.btnRotate90Left.Enabled = false;
            this.btnRotate90Right.Enabled = false;
            this.btnTopCenter.Enabled = false;
            this.btnTopLeft.Enabled = false;
            this.btnTopRight.Enabled = false;
            this.btnTrim.Enabled = false;
        }

        for (int i = 0; i < ajaxAccordion.Panes.Count; i++)
        {
            if (i == ajaxAccordion.SelectedIndex)
            {
                ajaxAccordion.Panes[i].HeaderCssClass = ajaxAccordion.HeaderSelectedCssClass;
            }
            else
            {
                ajaxAccordion.Panes[i].HeaderCssClass = ajaxAccordion.HeaderCssClass;
            }
        }
    }

    #endregion
}
