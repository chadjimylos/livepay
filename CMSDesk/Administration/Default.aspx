<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="CMSDesk_Administration_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>CMSDesk - Administration</title>

    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
</head>
<frameset border="0" rows="6,*">
		<frame name="cmsdeskadminheader" src="header.aspx" scrolling="no" noresize="noresize" frameborder="0" />		
		<frameset border="0" cols="225,*" runat="server" id="colsFrameset" EnableViewState="false">
    		<frame name="frameTree" src="leftmenu.aspx" frameborder="0" scrolling="no" runat="server" id="frameTree" class="TreeFrame" />
    		<frame name="frameMain" src="administration.aspx" frameborder="0" runat="server" id="frameMain" />
		</frameset>
		<noframes>
			<p id="p2">
				This HTML frameset displays multiple Web pages. To view this frameset, use a 
				Web browser that supports HTML 4.0 and later.
			</p>
		</noframes>
	</frameset>
</html>
