﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCInsertNewAFM.ascx.vb" Inherits="CMSTemplates_AegeanPower_UCInsertNewAFM" %>

<script language="javascript" type="text/javascript">

    function validateControls(oSrc, args) {

        var bIsValid = true
        var MyVal = args.Value


        if (MyVal.length == 0) {
            bIsValid = false
        }

        if (IsNumeric(MyVal) == false && oSrc.getAttribute('IsNumber') == 'yes') {
            bIsValid = false
        }

        var MaxLenght = MyVal.length
        if (oSrc.getAttribute('CheckLength') == 'yes' && MaxLenght != oSrc.getAttribute('LengthLimit')) {
            bIsValid = false
        }

        if (oSrc.getAttribute('IsInv_AFM') == 'yes') {
            if (checkAFM(MyVal) == false) {
                bIsValid = false;
            }
        }


       
        if (bIsValid == false) {
            if (oSrc.getAttribute('IsInv_AFM') == 'yes') {
                if (MyVal.length == 0) {
                    document.getElementById('AfmError').innerHTML = '<%=ResHelper.LocalizeString("{$=Παρακαλώ συμπληρώστε το ΑΦΜ|en-us=Please fill in your Vat Number$}") %>'
                } else {
                    document.getElementById('AfmError').innerHTML = '<%=ResHelper.LocalizeString("{$=Το ΑΦΜ δεν είναι σωστό|en-us=The Vat Number is incorrect$}") %>'
                }

            } else {
                if (oSrc.getAttribute('IsMyAFM') == 'yes') {
                    if (MyVal.length == 0) {
                        document.getElementById('ChkAFMError').innerHTML = '<%=ResHelper.LocalizeString("{$=Το πεδίο ειναι υποχρεωτικό|en-us=Mandatory field$}") %>'
                    } 
                } else {
                    if (MyVal.length == 0) {
                        document.getElementById('ProviderError').innerHTML = '<%=ResHelper.LocalizeString("{$=Παρακαλώ συμπληρώστε αρ. μετρητή|en-us=Please fill in your meter number$}") %>'
                    } else {
                        document.getElementById('ProviderError').innerHTML = '<%=ResHelper.LocalizeString("{$=Ο αριθμός μετρητή δεν είναι σωστός|en-us=The meter number is incorrect$}") %>'
                    }
                }
            }
        }

        if (bIsValid == true) {
            if (oSrc.getAttribute('IsInv_AFM') == 'yes') {
                document.getElementById('AfmError').innerHTML = ''
            } else {

                if (oSrc.getAttribute('IsMyAFM') == 'yes') {
                    document.getElementById('ChkAFMError').innerHTML = '';
                } else {
                    document.getElementById('ProviderError').innerHTML = ''
                }
                    }
        }

        args.IsValid = bIsValid;
    }


    function CheckAfmChk(obj) {
        if (obj.checked) {
            document.getElementById('<%=IsMyAFM.ClientID %>').value = '1'
            document.getElementById('ChkAFMError').innerHTML = ''
        } else {
            document.getElementById('<%=IsMyAFM.ClientID %>').value = ''
            document.getElementById('ChkAFMError').innerHTML = '<%=ResHelper.LocalizeString("{$=Το πεδίο ειναι υποχρεωτικό|en-us=Mandatory field$}") %>'
        }
    }
    function IsNumeric(sText) {
        var ValidChars = "0123456789";
        var IsNumber = true;
        var Char;
        for (i = 0; i < sText.length && IsNumber == true; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) == -1) {
                IsNumber = false;
            }
        }
        return IsNumber;
    }

    function checkAFM(objVal) {
        var afm = objVal;
        var IsValidAfm = true
        if (!afm.match(/^\d{9}$/)) {
            IsValidAfm = false;
            return false;
        }

        afm = afm.split('').reverse().join('');

        var Num1 = 0;
        for (var iDigit = 1; iDigit <= 8; iDigit++) {
            Num1 += afm.charAt(iDigit) << iDigit;
        }

        if ((Num1 % 11) % 10 == afm.charAt(0)) {
            IsValidAfm = true;
            return true;
        }
        else {
            IsValidAfm = false;
            return false;
        }
    }

    function CloseThePopUp() {
        parent.jQuery('#aClosePopUp').click();
    }


    function OnSuccess() {
        var MyUrl = parent.document.location.href.toLowerCase()

        var IsNewVat = MyUrl.indexOf('newvat')
     
        if (IsNewVat != -1) {
            MyUrl = MyUrl.replace("?newvat=1", "")
            parent.document.location.href = MyUrl
        } else {
            parent.jQuery('input[id$=btnRebindGVAFM]').click(); 
        }
        
    }
    
</script>

<div class="NewAFMMain">
    <div class="NewAFmTitle"><A onclick="alert(_lightbox)">Προσθήκη Νέου ΑΦΜ</A></div>
    <div class="NewAfmBorder">&nbsp;</div>
    
    <div id="ResultDiv" runat="server"></div>
    <div id="NewAfmMainForm" runat="server">
    <div class="PB5">
    <div class="NewAfmFormTitle"><%=ResHelper.LocalizeString("{$=ΑΦΜ|en-us=ΑΦΜ$}") %></div>
    <div class="Fleft"><asp:TextBox autocomelete="off" MaxLength="9" BorderColor="#d5d5d5" BorderWidth="1" BorderStyle="Solid" CssClass="NewAfmTxtCtrls" ID="txtAfm" runat="server" ></asp:TextBox></div>
    <div id="AfmError" class="NewAfmFieldError"></div>
    <div class="Clear"></div>
    <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvAFM" ClientValidationFunction="validateControls" ValidateEmptyText="true" runat="server" IsInv_AFM="yes" IsNumber="yes" LengthLimit="9" CheckLength="yes" ControlToValidate="txtAfm" text="*" ValidationGroup="NewAfm"/></div>
    </div>
    <div class="NewAfmFormTitle"><%=ResHelper.LocalizeString("{$=Κωδικός Μετρητή|en-us=Κωδικός Μετρητή$}") %></div>
    <div class="Fleft"><asp:TextBox autocomelete="off" ID="txtProvider" MaxLength="11" BorderColor="#d5d5d5" BorderWidth="1" BorderStyle="Solid" CssClass="NewAfmTxtCtrls" runat="server" ></asp:TextBox></div>
    <div id="ProviderError" class="NewAfmFieldError"></div>
    <div class="Clear"></div>
    <div style="display:none"><asp:CustomValidator ID="CustomValidator1" ClientValidationFunction="validateControls" ValidateEmptyText="true" runat="server" IsNumber="yes" ControlToValidate="txtProvider" text="*" ValidationGroup="NewAfm"/></div>
    
    <div style="float:left"></div>
    <div class="Fleft"><asp:CheckBox onclick="CheckAfmChk(this)" ID="ChkMyAFM" runat="server"  /></div>
    
    <div class="Clear"></div>
    <div id="ChkAFMError" class="NewAfmChkFieldError"></div>
    <div style="display:none"><asp:CustomValidator ID="CustomValidator2" ClientValidationFunction="validateControls" ValidateEmptyText="true" runat="server" IsMyAFM="yes" ControlToValidate="IsMyAFM" text="*" ValidationGroup="NewAfm"/></div>
    </div>
    <div class="NewAfmbtnSave">
    <asp:ImageButton ID="btnSave" runat="server" ImageUrl="/App_Themes/AegeanPower/Images/SubmitButtonInputData.jpg" ValidationGroup="NewAfm" />
    </div>
    <div class="NewAfmbtnReturn" id="DivbtnClose" runat="server" visible="false">
        <img id="btnClose" class="Chant"  runat="server"  onclick="CloseThePopUp()" />
    </div> 
    <asp:TextBox ID="IsMyAFM" runat="server" style="display:none"></asp:TextBox>
</div>