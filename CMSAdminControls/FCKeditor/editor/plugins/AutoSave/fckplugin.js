// Register the related command.
var AutoSaveComboCommand = function() {
}

AutoSaveComboCommand.prototype =
{
    Name: 'AutoSave',

    Execute: function(name, item) {
        item.combo.DeselectAll(false);
        item.combo.SetLabel('Select...');
        FCK.ToolbarSet.CurrentInstance.SetData(item.content);
    },

    GetState: function() {
        return FCK_TRISTATE_OFF;
    }
}

FCKCommands.RegisterCommand('AutoSave', new AutoSaveComboCommand());

var AutoSaveCombo = function(tooltip, style) {
    this.CommandName = 'AutoSave';
    this.Label = this.GetLabel();
    this.Tooltip = tooltip ? tooltip : this.Label;
    this.Style = style ? style : FCK_TOOLBARITEM_ICONTEXT;

    this.DefaultLabel = FCKConfig.DefaultFontSizeLabel || '';

    this.FieldWidth = 60;
    this.PanelWidth = 300;
}

// Inherit from FCKToolbarSpecialCombo.
AutoSaveCombo.prototype = new FCKToolbarSpecialCombo;

AutoSaveCombo.prototype.GetLabel = function() {
    return FCKLang.AutoSave;
}

AutoSaveCombo.prototype.GetStyles = function() {
}

var editorInstance = null;

var savedDataKey = "saveddata";

AutoSaveCombo.prototype.CreateItems = function(targetSpecialCombo) {
    if (editorInstance == null) {
        editorInstance = FCK.ToolbarSet.CurrentInstance;
    }

    targetSpecialCombo.ClearItems();

    var index = 0;
    while (true) {
        var value = GetCookie(savedDataKey + index);
        if (value == null) {
            break;
        }

        value = RemoveHTMLTags(value);
        var lineIndex = value.indexOf('\n');
        var time = value.substr(0, lineIndex);
        var content = value.substr(lineIndex + 1);

        if (content.length > 100) {
            value = content.substr(0, 50) + ' ...<br />... ' + content.substr(content.length - 50, 50);
        }
        value = '<strong>' + time + '</strong><br />' + value;

        var item = targetSpecialCombo.AddItem("item" + index, value)
        item.combo = targetSpecialCombo;
        item.content = content;

        index++;
    }

    this._Combo.SetLabel('Select...');
}

function RemoveHTMLTags(code) {
    code = code.replace(/&(lt|gt);/g, function(strMatch, p1) {
        return (p1 == "lt") ? "<" : ">";
    });
    return code.replace(/<\/?[^>]+(>|$)/g, "");
}

function GetCookie(name) {
    var allcookies = document.cookie.split(';');
    var tempcookie = '';
    var cookiename = '';
    var cookievalue = '';
    var cookiefound = false;

    for (i = 0; i < allcookies.length; i++) {
        tempcookie = allcookies[i].split('=');
        cookiename = tempcookie[0].replace(/^\s+|\s+$/g, '');
        if (cookiename == name) {
            cookiefound = true;
            if (tempcookie.length > 1) {
                cookievalue = unescape(tempcookie[1].replace(/^\s+|\s+$/g, ''));
            }
            return cookievalue;
            break;
        }
        tempcookie = null;
        cookiename = '';
    }
    if (!cookiefound) {
        return null;
    }
}

function SetCookie(name, value, expires, path, domain, secure) {
    var today = new Date();
    today.setTime(today.getTime());

    if (expires) {
        expires = expires * 1000 * 60;
    }
    var expires_date = new Date(today.getTime() + (expires));

    document.cookie = name + "=" + escape(value) +
        ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
        ((path) ? ";path=" + path : "") +
        ((domain) ? ";domain=" + domain : "") +
        ((secure) ? ";secure" : "");
}

function GetTimeStamp() {
    var time = new Date();
    return time.toLocaleString();
}

AutoSaveCombo.prototype.RefreshActiveItems = function(targetSpecialCombo) {
    targetSpecialCombo.SetLabel('Select...');
}

var oAutoSaveCombo = new AutoSaveCombo();
FCKToolbarItems.RegisterItem('AutoSave', oAutoSaveCombo);

var lastData = "";
var dataIndex = 0;

function AutoSaveData() {
    if (editorInstance != null) {
        var value = editorInstance.GetData();
        if (value != lastData) {
            SetCookie(savedDataKey + dataIndex, GetTimeStamp() + '\n' + value, 60, '/', '', '');
            dataIndex++;
            lastData = value;
        }

        if (oAutoSaveCombo._Combo != null) {
            oAutoSaveCombo.CreateItems(oAutoSaveCombo._Combo);
        }
    }
}

var saveInterval = setInterval("AutoSaveData()", 5000);


