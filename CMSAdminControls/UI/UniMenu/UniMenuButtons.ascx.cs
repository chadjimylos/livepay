using System;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSAdminControls_UI_UniMenu_UniMenuButtons : CMSUserControl
{
    #region "Variables"

    /// <summary>
    /// Buttons
    /// </summary>
    private string[,] mButtons = null;

    private bool mAllowSelection = false;

    private int identifier = 0;

    protected int mSelectedIndex = 0;

    private List<Panel> mInnerControls = null;

    private int mMaximumItems = 1;

    private bool mHorizontalLayout = true;

    #endregion


    #region "Constants"

    public const string SelectedSuffix = " Selected";

    #endregion


    #region "Public properties"

    /// <summary>
    /// Determines whether buttons are selectable
    /// </summary>
    public bool AllowSelection
    {
        get
        {
            return mAllowSelection;
        }
        set
        {
            mAllowSelection = value;
        }
    }


    /// <summary>
    /// If selection is enabled, determines which button is implicitly selected
    /// </summary>
    public int SelectedIndex
    {
        get
        {
            return mSelectedIndex;
        }
        set
        {
            mSelectedIndex = value;
        }
    }


    /// <summary>
    /// Array of buttons. 
    /// {
    /// [n, 0] - Caption of button
    /// [n, 1] - Button tooltip (string.empty: empty, null: copies caption or custom string)
    /// [n, 2] - CSS class of button
    /// [n, 3] - JavaScript OnClick action
    /// [n, 4] - Button's redirect URL
    /// [n, 5] - Image path of button
    /// [n, 6] - Image alternate text (string.empty: empty, null: copies caption or custom string
    /// [n, 7] - ImageAlign enum (default: NotSet)
    /// [n, 8] - Minimal width of middle part of the button (in pixels)
    /// }
    /// </summary> 
    public string[,] Buttons
    {
        get
        {
            return mButtons;
        }
        set
        {
            mButtons = value;
        }
    }


    /// <summary>
    /// Description.
    /// </summary>
    public List<Panel> InnerControls
    {
        get
        {
            return mInnerControls;
        }
        set
        {
            mInnerControls = value;
        }
    }


    /// <summary>
    /// Indicates whether to repeat items horizontally. Value is true by default. Otherwise items will be rendered vertically.
    /// </summary>
    public bool HorizontalLayout
    {
        get
        {
            return mHorizontalLayout;
        }
        set
        {
            mHorizontalLayout = value;
        }
    }


    /// <summary>
    /// Specifies number of menu items to be rendered in single row or column (depending on RepeatHorizontal property).
    /// </summary>
    public int MaximumItems
    {
        get
        {
            return mMaximumItems;
        }
        set
        {
            mMaximumItems = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            if (Buttons != null)
            {
                if (AllowSelection)
                {
                    ScriptHelper.RegisterJQuery(Page);
                    StringBuilder selectionScript = new StringBuilder();
                    selectionScript.AppendLine("function SelectButton(elem)");
                    selectionScript.AppendLine("{");
                    selectionScript.AppendLine("    var selected = '" + SelectedSuffix + "';");
                    selectionScript.AppendLine("    var jElem =$j(elem);");
                    selectionScript.AppendLine("    var parentId =jElem.parent('div').parent('div').attr('id');");
                    selectionScript.AppendLine("    $j('#'+parentId+' .'+elem.className).removeClass(selected);");
                    selectionScript.AppendLine("    jElem.addClass(selected);");
                    selectionScript.AppendLine("}");
                    ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "selectionScript", ScriptHelper.GetScript(selectionScript.ToString()));

                    StringBuilder indexSelection = new StringBuilder();
                    indexSelection.AppendLine("function SelectButtonIndex_" + ClientID + "(index)");
                    indexSelection.AppendLine("{");
                    indexSelection.AppendLine("    var elem = document.getElementById('" + ClientID + "_pnlButton'+index);");
                    indexSelection.AppendLine("    SelectButton(elem);");
                    indexSelection.AppendLine("");
                    indexSelection.AppendLine("}");
                    ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "indexSelection_" + ClientID, ScriptHelper.GetScript(indexSelection.ToString()));
                }

                int buttonsCount = Buttons.GetUpperBound(0) + 1;
                InnerControls = new List<Panel>(buttonsCount);
                for (identifier = 0; identifier < buttonsCount; identifier++)
                {
                    // Check array dimensions
                    if (Buttons.GetUpperBound(1) != 8)
                    {
                        Controls.Add(GetError(ResHelper.GetString("unimenubuttons.wrongdimensions")));
                        continue;
                    }

                    string caption = Buttons[identifier, 0];
                    string tooltip = Buttons[identifier, 1];
                    string cssClass = Buttons[identifier, 2];
                    string onClick = Buttons[identifier, 3];
                    string redirectUrl = Buttons[identifier, 4];
                    string imagePath = Buttons[identifier, 5];
                    string alt = Buttons[identifier, 6];
                    string align = Buttons[identifier, 7];
                    string minWidth = Buttons[identifier, 8];
                    ImageAlign imageAlign = ParseImageAlign(align);

                    // Generate button image
                    Image buttonImage = new Image();
                    buttonImage.ID = "imgButton" + identifier;
                    buttonImage.EnableViewState = false;
                    buttonImage.AlternateText = alt ?? caption;
                    if (!string.IsNullOrEmpty(imagePath))
                    {
                        buttonImage.ImageUrl = ResolveUrl(imagePath);
                    }
                    buttonImage.ImageAlign = imageAlign;

                    // Generate button text
                    Literal captionLiteral = new Literal();
                    captionLiteral.ID = "ltlCaption" + identifier;
                    captionLiteral.EnableViewState = false;
                    string separator = (imageAlign == ImageAlign.Top) ? "<br />" : "\n";
                    captionLiteral.Text = separator + caption;

                    // Generate button link
                    HyperLink buttonLink = new HyperLink();
                    buttonLink.ID = "lnkButton" + identifier;
                    buttonLink.EnableViewState = false;

                    buttonLink.Controls.Add(buttonImage);
                    buttonLink.Controls.Add(captionLiteral);

                    if (!string.IsNullOrEmpty(redirectUrl))
                    {
                        buttonLink.NavigateUrl = ResolveUrl(redirectUrl);
                    }

                    // Generate left border
                    Panel pnlLeft = new Panel();
                    pnlLeft.ID = "pnlLeft" + identifier;
                    pnlLeft.EnableViewState = false;
                    pnlLeft.CssClass = "Left" + cssClass;

                    // Generate middle part of button
                    Panel pnlMiddle = new Panel();
                    pnlMiddle.ID = "pnlMiddle" + identifier;
                    pnlMiddle.EnableViewState = false;
                    pnlMiddle.CssClass = "Middle" + cssClass;
                    pnlMiddle.Controls.Add(buttonLink);
                    if (!string.IsNullOrEmpty(minWidth))
                    {
                        pnlMiddle.Style.Add("min-width", minWidth + "px");
                    }

                    // Generate right border
                    Panel pnlRight = new Panel();
                    pnlRight.ID = "pnlRight" + identifier;
                    pnlRight.EnableViewState = false;
                    pnlRight.CssClass = "Right" + cssClass;

                    // Generate whole button
                    Panel pnlButton = new Panel();
                    pnlButton.ID = "pnlButton" + identifier;
                    pnlButton.EnableViewState = false;
                    if (AllowSelection && (identifier == SelectedIndex))
                    {
                        cssClass += SelectedSuffix;
                    }
                    pnlButton.CssClass = cssClass;

                    // Add inner controls
                    pnlButton.Controls.Add(pnlLeft);
                    pnlButton.Controls.Add(pnlMiddle);
                    pnlButton.Controls.Add(pnlRight);

                    pnlButton.ToolTip = tooltip ?? caption;
                    if (AllowSelection)
                    {
                        onClick = "SelectButton(this);" + onClick;
                    }
                    pnlButton.Attributes["onclick"] = onClick;

                    // In case of horizontal layout
                    if (HorizontalLayout)
                    {
                        // Stack buttons underneath
                        pnlButton.Style.Add("clear", "both");
                    }
                    else
                    {
                        // Stack buttons side-by-sode
                        pnlButton.Style.Add("float", "left");
                    }

                    // Fill collection of buttons
                    InnerControls.Insert(identifier, pnlButton);
                }

                // Calculate number of needed panels
                int panelCount = InnerControls.Count / MaximumItems;
                if ((InnerControls.Count % MaximumItems) > 0)
                {
                    panelCount++;
                }

                // Initialize list of panels
                List<Panel> panels = new List<Panel>();

                // Fill each panel
                for (int panelIndex = 0; panelIndex < panelCount; panelIndex++)
                {
                    // Create new instance of panel
                    Panel outerPanel = new Panel();
                    outerPanel.EnableViewState = false;
                    outerPanel.ID = "pnlOuter" + panelIndex;

                    // In case of horizontal layout
                    if (HorizontalLayout)
                    {
                        // Stack panels side-by-side
                        outerPanel.Style.Add("float", "left");
                    }
                    else
                    {
                        // Stack panels underneath
                        outerPanel.Style.Add("clear", "both");
                    }
                    // Add buttons to panel
                    for (int buttonIndex = (panelIndex * MaximumItems); buttonIndex < (panelCount * MaximumItems) + panelCount; buttonIndex++)
                    {
                        if (((buttonIndex % MaximumItems) < MaximumItems) && (InnerControls.Count > buttonIndex))
                        {
                            outerPanel.Controls.Add(InnerControls[buttonIndex]);
                        }
                    }
                    // Add panel to collection of panels
                    panels.Add(outerPanel);
                }

                // Switch order of panels in case of RTL culture
                if (CultureHelper.IsUICultureRTL())
                {
                    panels.Reverse();
                }
                // Add all panels to control
                foreach (Panel panel in panels)
                {
                    Controls.Add(panel);
                }
            }
            else
            {
                Controls.Add(GetError(ResHelper.GetString("unimenubuttons.wrongdimensions")));
            }
        }
    }

    #endregion


    #region "Other methods"

    /// <summary>
    /// Parses image align from string value
    /// </summary>
    /// <param name="align">Value to parse</param>
    /// <returns>Align of an image</returns>
    private static ImageAlign ParseImageAlign(string align)
    {
        ImageAlign imageAlign = default(ImageAlign); ;
        if (align != null)
        {
            try
            {
                // Try to get image align
                imageAlign = (ImageAlign)Enum.Parse(typeof(ImageAlign), align);

                // RTL switch
                if (CultureHelper.IsUICultureRTL())
                {
                    if (imageAlign == ImageAlign.Left)
                    {
                        imageAlign = ImageAlign.Right;
                    }
                    else if (imageAlign == ImageAlign.Right)
                    {
                        imageAlign = ImageAlign.Left;
                    }
                }
            }
            catch
            {
                imageAlign = ImageAlign.NotSet;
            }
        }
        else
        {
            imageAlign = ImageAlign.NotSet;
        }
        return imageAlign;
    }


    /// <summary>
    /// Generates error label
    /// </summary>
    /// <param name="message">Error message to display</param>
    /// <returns>Label with error message</returns>
    protected Label GetError(string message)
    {
        // If error occures skip this group
        Label errorLabel = new Label();
        errorLabel.ID = "lblError" + identifier;
        errorLabel.EnableViewState = false;
        errorLabel.Text = message;
        errorLabel.CssClass = "ErrorLabel";
        return errorLabel;
    }

    #endregion
}
