using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormEngine;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.SiteProvider;

public partial class CMSSiteManager_Development_SystemTables_AlternativeForms_AlternativeForms_Edit_General : SiteManagerPage
{
    protected int altFormId = 0;
    AlternativeFormInfo afi = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        altFormId = QueryHelper.GetInteger("altformid", 0);

        // Validate
        afi = GetAndValidateFormInfo(altFormId);
        if (afi == null)
        {
            return;
        }

        // Initialize controls
        btnOk.Text = ResHelper.GetString("general.ok");
        rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");

        if (!RequestHelper.IsPostBack())
        {
            // Init values
            LoadData();
        }

        btnOk.Click += new EventHandler(btnOK_Click);
    }
    

    /// <summary>
    /// Loads form info
    /// </summary>
    private void LoadData()
    {
        this.txtCodeName.Text = afi.FormName;
        this.txtDisplayName.Text = afi.FormDisplayName;

        string className = DataClassInfoProvider.GetClassName(afi.FormClassID);
        if (className.ToLower().Trim() == SiteObjectType.USER.ToLower())
        {
            this.pnlCombineUserSettings.Visible = true;
            this.chkCombineUserSettings.Checked = (afi.FormCoupledClassID > 0);
        }
    }


    /// <summary>
    /// Click event - updates new values.
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Params</param>
    void btnOK_Click(object sender, EventArgs e)
    {
        // Code name validation
        string err = new Validator().IsIdentificator(txtCodeName.Text, ResHelper.GetString("general.erroridentificatorformat")).Result;
        if (err != String.Empty)
        {
            lblError.Visible = true;
            lblError.Text = err;
            lblInfo.Visible = false;
            return;
        }

        // Validate form id
        AlternativeFormInfo afi = GetAndValidateFormInfo(altFormId);
        if (afi == null)
        {
            return;
        }

        // Checking for duplicate items
        DataSet ds = AlternativeFormInfoProvider.GetForms("FormName='" + txtCodeName.Text.Replace("'", "''") +
            "' AND FormClassID=" + afi.FormClassID, null);

        if (!DataHelper.DataSourceIsEmpty(ds))
        {
            if (!((ds.Tables.Count == 1) && (ds.Tables[0].Rows.Count == 1) && (
                ValidationHelper.GetInteger(ds.Tables[0].Rows[0]["FormID"], 0) == altFormId)))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("general.codenameexists");
                lblInfo.Visible = false;
                return;
            }
        }

        afi.FormDisplayName = txtDisplayName.Text;
        afi.FormName = txtCodeName.Text;
        AlternativeFormInfoProvider.SetAlternativeFormInfo(afi);

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("general.changessaved");
    }


    /// <summary>
    /// Gets and validates (test for null) alternative form info.
    /// </summary>
    /// <param name="formId">Alternative form id</param>
    protected AlternativeFormInfo GetAndValidateFormInfo(int formId)
    {
        AlternativeFormInfo afi = AlternativeFormInfoProvider.GetAlternativeFormInfo(formId);
        if (afi == null)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("general.invalidid");
            pnlGeneral.Visible = false;
            lblInfo.Visible = false;
        }
        return afi;
    }
}
