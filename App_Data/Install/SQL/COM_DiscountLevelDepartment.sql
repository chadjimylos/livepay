CREATE TABLE [COM_DiscountLevelDepartment] (
		[DiscountLevelID]     int NOT NULL,
		[DepartmentID]        int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_DiscountLevelDepartment]
	ADD
	CONSTRAINT [PK_COM_DiscountLevelDepartment]
	PRIMARY KEY
	([DiscountLevelID], [DepartmentID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_DiscountLevelDepartment]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Department]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_DiscountLevelDepartment_DepartmentID_COM_Department]') IS NULL
BEGIN
		ALTER TABLE [COM_DiscountLevelDepartment]
			ADD CONSTRAINT [FK_COM_DiscountLevelDepartment_DepartmentID_COM_Department]
			FOREIGN KEY ([DepartmentID]) REFERENCES [COM_Department] ([DepartmentID])
END
IF OBJECT_ID(N'[COM_DiscountLevelDepartment]') IS NOT NULL
	AND OBJECT_ID(N'[COM_DiscountLevel]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_DiscountLevelDepartment_DiscountLevelID_COM_DiscountLevel]') IS NULL
BEGIN
		ALTER TABLE [COM_DiscountLevelDepartment]
			ADD CONSTRAINT [FK_COM_DiscountLevelDepartment_DiscountLevelID_COM_DiscountLevel]
			FOREIGN KEY ([DiscountLevelID]) REFERENCES [COM_DiscountLevel] ([DiscountLevelID])
END
