<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocumentNameFilter.ascx.cs"
    Inherits="CMSAdminControls_UI_UniGrid_Filters_DocumentNameFilter" %>
<asp:Panel CssClass="Filter" runat="server" ID="pnlSearch">
    <asp:PlaceHolder ID="plcLabel" runat="server">
        <cms:LocalizedLabel ID="lblName" runat="server" DisplayColon="true" ResourceString="general.documentname"
            CssClass="FieldLabel" />&nbsp;</asp:PlaceHolder>
    <asp:DropDownList ID="drpOperator" runat="server" CssClass="ContentDropdown" />
    <asp:TextBox ID="txtDocumentName" runat="server" MaxLength="100" Class="SelectorTextBox" />
</asp:Panel>
