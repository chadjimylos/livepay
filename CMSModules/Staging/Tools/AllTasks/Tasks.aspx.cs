using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Threading;
using System.Security.Principal;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.Staging;
using CMS.UIControls;
using CMS.DataEngine;

public partial class CMSModules_Staging_Tools_AllTasks_Tasks : CMSStagingPage
{
    #region "Protected variables"

    protected static Hashtable mLogs = new Hashtable();
    protected static Hashtable mErrors = new Hashtable();
    protected static Hashtable mInfos = new Hashtable();

    protected IDataClass mEventLog = null;

    private int serverId = 0;
    private string eventCode = null;
    private string eventType = null;

    protected string viewImage = string.Empty;
    protected string deleteImage = string.Empty;
    protected string syncImage = string.Empty;

    protected string viewTooltip = string.Empty;
    protected string deleteTooltip = string.Empty;
    protected string syncTooltip = string.Empty;

    protected string syncCurrent = null;
    protected CurrentUserInfo currentUser = null;
    protected GeneralConnection mConnection = null;

    protected string objectType = string.Empty;
    protected int siteId = 0;

    protected int currentSiteId = 0;
    protected string currentSiteName = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Connection
    /// </summary>
    public GeneralConnection Connection
    {
        get
        {
            return mConnection ?? (mConnection = ConnectionHelper.GetConnection());
        }
    }


    /// <summary>
    /// Current Error
    /// </summary>
    public string CurrentError
    {
        get
        {
            return ValidationHelper.GetString(mErrors["SyncError_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mErrors["SyncError_" + ctlAsync.ProcessGUID] = value;
        }
    }


    /// <summary>
    /// Current Info
    /// </summary>
    public string CurrentInfo
    {
        get
        {
            return ValidationHelper.GetString(mInfos["SyncInfo_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mInfos["SyncInfo_" + ctlAsync.ProcessGUID] = value;
        }
    }


    /// <summary>
    /// Current log context
    /// </summary>
    public LogContext CurrentLog
    {
        get
        {
            return EnsureLog();
        }
    }


    /// <summary>
    /// Creates new event log
    /// </summary>
    private IDataClass EventLog
    {
        get
        {
            if (mEventLog == null)
            {
                try
                {
                    // New event declaration
                    mEventLog = DataClassFactory.NewDataClass("CMS.EventLog", Connection);

                    mEventLog.SetValue("EventType", eventType);
                    mEventLog.SetValue("EventTime", DateTime.Now);
                    mEventLog.SetValue("Source", "Staging");
                    mEventLog.SetValue("EventCode", eventCode);
                    mEventLog.SetValue("UserID", currentUser.UserID);
                    mEventLog.SetValue("UserName", TextHelper.LimitLength(currentUser.UserName, 250, string.Empty));
                    mEventLog.SetValue("EventDescription", string.Empty);
                    mEventLog.SetValue("IPAddress", TextHelper.LimitLength(HTTPHelper.GetUserHostAddress(), 100));
                    mEventLog.SetValue("EventMachineName", TextHelper.LimitLength(HTTPHelper.MachineName, 100));
                    mEventLog.SetValue("SiteID", currentSiteId);
                }
                catch
                {
                    // Unable to log into eventlog
                }
            }
            return mEventLog;
        }
        set
        {
            mEventLog = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register script for pendingCallbacks repair
        ScriptHelper.FixPendingCallbacks(Page);

        // Initialize current user for the async actions
        currentUser = CMSContext.CurrentUser;

        if (!RequestHelper.IsCallback())
        {
            // Check 'Manage object tasks' permission
            if (!currentUser.IsAuthorizedPerResource("cms.staging", "ManageAllTasks"))
            {
                RedirectToAccessDenied("cms.staging", "ManageAllTasks");
            }

            currentSiteId = CMSContext.CurrentSiteID;
            currentSiteName = CMSContext.CurrentSiteName;

            // Register the dialog script
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
            serverId = QueryHelper.GetInteger("serverid", 0);

            // Get the selected types
            ObjectTypeTreeNode selectedNode = TaskInfoProvider.ObjectTree.FindNode(objectType, (siteId > 0));
            objectType = (selectedNode != null) ? selectedNode.GetObjectTypes(true) : string.Empty;

            // Setup title
            titleElem.TitleImage = GetImageUrl("/CMSModules/CMS_Staging/synchronization.png");

            if (!CausedPostback(btnSyncSelected, btnSyncAll))
            {
                viewImage = GetImageUrl("Design/Controls/UniGrid/Actions/View.png");
                deleteImage = GetImageUrl("Design/Controls/UniGrid/Actions/Delete.png");
                syncImage = GetImageUrl("Design/Controls/UniGrid/Actions/Synchronize.png");

                syncTooltip = ResHelper.GetString("Tasks.SyncTooltip");
                deleteTooltip = ResHelper.GetString("general.delete");
                viewTooltip = ResHelper.GetString("general.view");

                syncCurrent = ResHelper.GetString("ObjectTasks.SyncCurrent");

                btnDeleteAll.OnClientClick = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("Tasks.ConfirmDeleteAll")) + ");";
                btnDeleteSelected.OnClientClick = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("general.confirmdelete")) + ");";

                btnSyncSelected.OnClientClick = "return !IsSelectionEmpty_" + gridTasks.ClientID + "();";

                pnlContent.Visible = true;
                btnCancel.Attributes.Add("onclick", "CancelAction(); return false;");
                btnCancel.Text = ResHelper.GetString("General.Cancel");

                gridTasks.ZeroRowsText = ResHelper.GetString("Tasks.NoTasks");
                gridTasks.OnAction += gridTasks_OnAction;
                gridTasks.OnDataReload += gridTasks_OnDataReload;
                gridTasks.OnExternalDataBound += gridTasks_OnExternalDataBound;

                pnlLog.Visible = false;
            }
        }

        ctlAsync.OnFinished += ctlAsync_OnFinished;
        ctlAsync.OnError += ctlAsync_OnError;
        ctlAsync.OnRequestLog += ctlAsync_OnRequestLog;
        ctlAsync.OnCancel += ctlAsync_OnCancel;
    }


    protected override void OnPreRender(EventArgs e)
    {
        lblError.Visible = (lblError.Text != string.Empty);
        lblInfo.Visible = (lblInfo.Text != string.Empty);
        base.OnPreRender(e);
    }

    #endregion


    #region "Grid events & methods"

    protected void gridTasks_OnAction(string actionName, object actionArgument)
    {
        int taskId = ValidationHelper.GetInteger(actionArgument, 0);
        switch (actionName.ToLower())
        {
            case "delete":
                // Delete task
                SynchronizationInfoProvider.DeleteSynchronizationInfo(taskId, serverId, currentSiteId);
                break;

            case "synchronize":
                try
                {
                    // Run task synchronization
                    string result = StagingHelper.RunSynchronization(taskId, serverId, true, currentSiteId);

                    if (!string.IsNullOrEmpty(result))
                    {
                        lblError.Text = ResHelper.GetString("Tasks.SynchronizationFailed");
                    }
                    else
                    {
                        lblInfo.Text = ResHelper.GetString("Tasks.SynchronizationOK");
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = ResHelper.GetString("Tasks.SynchronizationFailed") + " " + ex.Message;
                }
                break;
        }
    }


    protected object gridTasks_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView drv = null;
        int taskId = 0;
        switch (sourceName.ToLower())
        {
            case "taskresult":
                drv = GetDataRowView((DataControlFieldCell)sender);
                int failedCount = ValidationHelper.GetInteger(drv["FailedCount"], 0);
                taskId = ValidationHelper.GetInteger(drv["TaskID"], 0);
                return GetResultLink(failedCount, taskId);

            case "view":
                ImageButton btnView = (ImageButton)sender;
                drv = GetDataRowView((DataControlFieldCell)btnView.Parent);
                taskId = ValidationHelper.GetInteger(drv["TaskID"], 0);
                string url = ResolveUrl("~/CMSModules/Staging/Tools/Objects/View.aspx?taskid=") + taskId;
                btnView.OnClientClick = "window.open('" + url + "');return false;";
                return btnView;

            case "tasktime":
                return DateTime.Parse(parameter.ToString()).ToString();
        }
        return parameter;
    }


    protected DataSet gridTasks_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        // Get the tasks
        DataSet ds = TaskInfoProvider.SelectTaskList(currentSiteId, serverId, completeWhere, currentOrder, 0, "TaskID, TaskSiteID, TaskDocumentID, TaskNodeAliasPath, TaskTitle, TaskTime, TaskType, TaskObjectType, TaskObjectID, TaskRunning, (SELECT COUNT(*) FROM Staging_Synchronization WHERE SynchronizationTaskID = TaskID AND SynchronizationErrorMessage IS NOT NULL AND (SynchronizationServerID = @ServerID OR (@ServerID = 0 AND (@TaskSiteID = 0 OR SynchronizationServerID IN (SELECT ServerID FROM Staging_Server WHERE ServerSiteID = @TaskSiteID AND ServerEnabled=1))))) AS FailedCount", currentOffset, currentPageSize, ref totalRecords);

        pnlFooter.Visible = (totalRecords > 0);
        return ds;
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }


    /// <summary>
    /// Returns the result link for the synchronization log
    /// </summary>
    /// <param name="failedCount">Failed items count</param>
    /// <param name="taskId">Task ID</param>
    protected string GetResultLink(object failedCount, object taskId)
    {
        int count = ValidationHelper.GetInteger(failedCount, 0);
        if (count > 0)
        {
            string logUrl = "../Tasks/log.aspx?taskid=" + taskId + "&serverId=" + serverId;
            return "<a target=\"_blank\" href=\"" + logUrl + "\" onclick=\"modalDialog('" + logUrl + "', 'tasklog', 700, 500); return false;\">" + ResHelper.GetString("Tasks.ResultFailed") + "</a>";
        }
        else
        {
            return string.Empty;
        }
    }

    #endregion


    #region "Async methods"

    /// <summary>
    /// All items synchronization
    /// </summary>
    protected void SynchronizeAll(object parameter)
    {
        string result = string.Empty;
        eventCode = "SYNCALLOBJECTS";
        try
        {
            AddLog(ResHelper.GetString("Synchronization.RunningTasks"));

            // Get the tasks
            DataSet ds = TaskInfoProvider.SelectTaskList(currentSiteId, serverId, null, "TaskID", -1, "TaskID, TaskTitle");
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string taskTitle = ValidationHelper.GetString(row["TaskTitle"], string.Empty);
                    AddLog(string.Format(ResHelper.GetAPIString("synchronization.running", "Processing '{0}' task"), HTMLHelper.HTMLEncode(taskTitle)));

                    // Get ID
                    int taskId = ValidationHelper.GetInteger(row["TaskID"], 0);
                    if (taskId > 0)
                    {
                        result += StagingHelper.RunSynchronization(taskId, serverId, true, currentSiteId);
                    }
                }
            }

            if (result != string.Empty)
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, null);
            }
            else
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationOK");
                AddLog(CurrentInfo);
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, result);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            FinalizeEventLog();
        }
    }


    public void SynchronizeSelected(object parameter)
    {
        if (parameter == null)
        {
            return;
        }

        string result = string.Empty;
        eventCode = "SYNCSELECTEDOBJECT";
        ArrayList list = (ArrayList)parameter;
        try
        {
            AddLog(ResHelper.GetString("Synchronization.RunningTasks"));

            foreach (string taskIdString in list)
            {
                int taskId = ValidationHelper.GetInteger(taskIdString, 0);
                if (taskId > 0)
                {
                    // Synchronize the task
                    TaskInfo task = TaskInfoProvider.GetTaskInfo(taskId);
                    if (task != null)
                    {
                        AddLog(string.Format(ResHelper.GetAPIString("synchronization.running", "Processing '{0}' task"), HTMLHelper.HTMLEncode(task.TaskTitle)));
                        result += StagingHelper.RunSynchronization(taskId, serverId, true, currentSiteId);
                    }
                }
            }

            if (result != string.Empty)
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, null);
            }
            else
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationOK");
                AddLog(CurrentInfo);
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, result);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            FinalizeEventLog();
        }
    }


    /// <summary>
    /// Deletes selected tasks
    /// </summary>
    protected void DeleteSelected(object parameter)
    {
        if (parameter == null)
        {
            return;
        }

        eventCode = "DELETESELECTEDTASKS";
        ArrayList list = (ArrayList)parameter;
        try
        {
            AddLog(ResHelper.GetString("Synchronization.DeletingTasks"));

            foreach (string taskIdString in list)
            {
                int taskId = ValidationHelper.GetInteger(taskIdString, 0);
                if (taskId > 0)
                {
                    TaskInfo task = TaskInfoProvider.GetTaskInfo(taskId);

                    if (task != null)
                    {
                        AddLog(string.Format(ResHelper.GetAPIString("deletion.running", "Deleting '{0}' task"), HTMLHelper.HTMLEncode(task.TaskTitle)));
                        SynchronizationInfoProvider.DeleteSynchronizationInfo(task, serverId, currentSiteId);
                    }
                }
            }

            CurrentInfo = ResHelper.GetString("Tasks.DeleteOK");
            AddLog(CurrentInfo);
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.DeletionCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.DeletionFailed");
                AddErrorLog(CurrentError);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.DeletionFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            FinalizeEventLog();
        }
    }


    /// <summary>
    /// Deletes all tasks
    /// </summary>
    protected void DeleteAll(object parameter)
    {
        eventCode = "DELETEALLTASKS";
        try
        {
            AddLog(ResHelper.GetString("Synchronization.DeletingTasks"));
            // Get the tasks
            DataSet ds = TaskInfoProvider.SelectTaskList(currentSiteId, serverId, null, "TaskID", -1, null);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    TaskInfo task = new TaskInfo(row);
                    if (task != null)
                    {
                        AddLog(string.Format(ResHelper.GetAPIString("deletion.running", "Deleting '{0}' task"), HTMLHelper.HTMLEncode(task.TaskTitle)));
                        SynchronizationInfoProvider.DeleteSynchronizationInfo(task, serverId, currentSiteId);
                    }
                }
            }

            CurrentInfo = ResHelper.GetString("Tasks.DeleteOK");
            AddLog(CurrentInfo);
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.DeletionCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.DeletionFailed");
                AddErrorLog(CurrentError);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.DeletionFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            FinalizeEventLog();
        }
    }

    #endregion


    #region "Button handling"

    protected void btnSyncSelected_Click(object sender, EventArgs e)
    {
        if (gridTasks.SelectedItems.Count > 0)
        {
            titleElem.TitleText = ResHelper.GetString("Synchronization.Title");
            ctlAsync.Parameter = gridTasks.SelectedItems;
            RunAsync(SynchronizeSelected);
        }
    }


    protected void btnSyncAll_Click(object sender, EventArgs e)
    {
        titleElem.TitleText = ResHelper.GetString("Synchronization.Title");
        RunAsync(SynchronizeAll);
    }


    protected void btnDeleteAll_Click(object sender, EventArgs e)
    {
        titleElem.TitleText = ResHelper.GetString("Synchronization.DeletingTasksTitle");
        RunAsync(DeleteAll);
    }


    protected void btnDeleteSelected_Click(object sender, EventArgs e)
    {
        titleElem.TitleText = ResHelper.GetString("Synchronization.DeletingTasksTitle");
        if (gridTasks.SelectedItems.Count > 0)
        {
            ctlAsync.Parameter = gridTasks.SelectedItems;
            RunAsync(DeleteSelected);
        }
    }

    #endregion


    #region "Async processing"

    protected void ctlAsync_OnRequestLog(object sender, EventArgs e)
    {
        ctlAsync.Log = CurrentLog.Log;
    }


    protected void ctlAsync_OnError(object sender, EventArgs e)
    {
        gridTasks.ResetSelection();

        lblError.Text = CurrentError;
        lblInfo.Text = CurrentInfo;
    }


    protected void ctlAsync_OnFinished(object sender, EventArgs e)
    {
        gridTasks.ResetSelection();

        lblError.Text = CurrentError;
        lblInfo.Text = CurrentInfo;
    }


    protected void ctlAsync_OnCancel(object sender, EventArgs e)
    {
        gridTasks.ResetSelection();

        lblError.Text = CurrentError;
        lblInfo.Text = CurrentInfo;
    }


    protected void RunAsync(AsyncAction action)
    {
        pnlLog.Visible = true;
        CurrentLog.Close();
        EnsureLog();
        CurrentError = string.Empty;
        CurrentInfo = string.Empty;
        EventLog = null;
        eventType = "I";
        pnlContent.Visible = false;

        ctlAsync.RunAsync(action, WindowsIdentity.GetCurrent());
    }

    #endregion


    #region "Log handling"

    /// <summary>
    /// Adds the log information
    /// </summary>
    /// <param name="newLog">New log information</param>
    protected void AddLog(string newLog)
    {
        EnsureLog();
        LogContext.AppendLine(newLog);
        AddEventLog(CurrentLog.Log);
    }


    /// <summary>
    /// Adds the log error
    /// </summary>
    /// <param name="newLog">New log information</param>
    protected void AddErrorLog(string newLog)
    {
        AddErrorLog(newLog, null);
    }


    /// <summary>
    /// Adds the log error
    /// </summary>
    /// <param name="newLog">New log information</param>
    /// <param name="errorMessage">Error message</param>
    protected void AddErrorLog(string newLog, string errorMessage)
    {
        LogContext.AppendLine(newLog);

        string logMessage = CurrentLog.Log;
        if (errorMessage != null)
        {
            logMessage = errorMessage + "<br />" + logMessage;
        }
        eventType = "E";
        AddEventLog(logMessage);
    }


    /// <summary>
    /// Adds message to event log object and updates event type
    /// </summary>
    /// <param name="logMessage">Message to log</param>
    protected void AddEventLog(string logMessage)
    {
        // If there is an EventLog
        if (EventLog != null)
        {
            EventLog.SetValue("EventDescription", logMessage);
            EventLog.SetValue("EventType", eventType);
        }
    }


    /// <summary>
    /// Updates event log
    /// </summary>
    protected void FinalizeEventLog()
    {
        // If there is an EventLog
        if (EventLog != null)
        {
            // Insert a new event
            mEventLog.Insert();
        }
    }


    /// <summary>
    /// Ensures the logging context
    /// </summary>
    protected LogContext EnsureLog()
    {
        LogContext log = LogContext.EnsureLog(ctlAsync.ProcessGUID);
        log.Reversed = true;
        log.LineSeparator = "<br />";
        return log;
    }

    #endregion


    #region "Helper methods"

    private bool CausedPostback(params Control[] controls)
    {
        foreach (Control control in controls)
        {
            string uniqueID = control.UniqueID;
            bool toReturn = (Request.Form[uniqueID] != null) || ((Request.Form["__EVENTTARGET"] != null) && Request.Form["__EVENTTARGET"].Equals(uniqueID)) || ((Request.Form[uniqueID + ".x"] != null) && (Request.Form[uniqueID + ".y"] != null));
            if (toReturn)
            {
                return true;
            }
        }
        return false;
    }

    #endregion
}