<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocTypeFilter.ascx.cs"
    Inherits="CMSFormControls_Filters_DocTypeFilter" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<table>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblClassType" runat="server" ResourceString="queryselection.lblclasstype"
                EnableViewState="false" />
        </td>
        <td>
            <asp:DropDownList ID="drpClassType" runat="server" CssClass="DropDownField" AutoPostBack="True"
                Width="100%" OnSelectedIndexChanged="drpClassType_SelectedIndexChanged" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblDocType" runat="server" />
        </td>
        <td>
            <cms:UniSelector ID="uniSelector" runat="server" />
        </td>
    </tr>
</table>
