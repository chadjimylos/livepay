using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.WorkflowEngine;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.GlobalHelper;
using CMS.Controls;
using CMS.SettingsProvider;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.SiteProvider;

using TreeNode = CMS.TreeEngine.TreeNode;
using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Content_Controls_UserContributions_ContributionList : CMSUserControl
{
    #region "Variables"

    /// <summary>
    /// On after delete event
    /// </summary>
    public event EventHandler OnAfterDelete = null;


    private bool mAllowInsert = true;
    private bool mAllowDelete = true;
    private bool mAllowEdit = true;
    private string mNewItemPageTemplate = "";
    private string mAllowedChildClasses = "";
    private string mAlternativeFormName = null;
    private string mValidationErrorMessage = null;
    private bool mCheckPermissions = false;
    private bool mCheckGroupPermissions = false;
    private UserContributionAllowUserEnum mAllowUsers = UserContributionAllowUserEnum.DocumentOwner;
    private TreeNode mParentNode = null;

    /// <summary>
    /// Data properties variable
    /// </summary>
    protected CMSDataProperties mDataProperties = new CMSDataProperties();

    #endregion


    #region "Document properties"

    /// <summary>
    /// Class names
    /// </summary>
    public string ClassNames
    {
        get
        {
            return this.mDataProperties.ClassNames;
        }
        set
        {
            this.mDataProperties.ClassNames = value;
        }
    }


    /// <summary>
    /// Combine with default culture
    /// </summary>
    public bool CombineWithDefaultCulture
    {
        get
        {
            return this.mDataProperties.CombineWithDefaultCulture;
        }
        set
        {
            this.mDataProperties.CombineWithDefaultCulture = value;
        }
    }


    /// <summary>
    /// Culture code
    /// </summary>
    public string CultureCode
    {
        get
        {
            return this.mDataProperties.CultureCode;
        }
        set
        {
            this.mDataProperties.CultureCode = value;
        }
    }


    /// <summary>
    /// Maximal relative level
    /// </summary>
    public int MaxRelativeLevel
    {
        get
        {
            return this.mDataProperties.MaxRelativeLevel;
        }
        set
        {
            this.mDataProperties.MaxRelativeLevel = value;
        }
    }


    /// <summary>
    /// Order by clause
    /// </summary>
    public string OrderBy
    {
        get
        {
            return this.mDataProperties.OrderBy;
        }
        set
        {
            this.mDataProperties.OrderBy = value;
        }
    }


    /// <summary>
    /// Nodes path 
    /// </summary>
    public string Path
    {
        get
        {
            return this.mDataProperties.Path;
        }
        set
        {
            this.mDataProperties.Path = value;
        }
    }


    /// <summary>
    /// Select only published nodes
    /// </summary>
    public bool SelectOnlyPublished
    {
        get
        {
            return this.mDataProperties.SelectOnlyPublished;
        }
        set
        {
            this.mDataProperties.SelectOnlyPublished = value;
        }
    }


    /// <summary>
    /// Site name
    /// </summary>
    public string SiteName
    {
        get
        {
            return this.mDataProperties.SiteName;
        }
        set
        {
            this.mDataProperties.SiteName = value;
        }
    }


    /// <summary>
    /// Where condition
    /// </summary>
    public string WhereCondition
    {
        get
        {
            return this.mDataProperties.WhereCondition;
        }
        set
        {
            this.mDataProperties.WhereCondition = value;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets Edit form control.
    /// </summary>
    public CMSModules_Content_Controls_UserContributions_EditForm EditForm
    {
        get
        {
            return this.editDoc;
        }
    }


    /// <summary>
    /// Indicates whether the list of documents should be displayed.
    /// </summary>
    public bool DisplayList
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["DisplayList"], true);
        }
        set
        {
            ViewState["DisplayList"] = value;
        }
    }


    /// <summary>
    /// Path for new created documents.
    /// </summary>
    public string NewDocumentPath
    {
        get
        {
            string newDocPath = ValidationHelper.GetString(ViewState["NewDocumentPath"], "");
            // If new document path is not set, use source path
            if (newDocPath.Trim() == "")
            {
                newDocPath = Path;
            }

            // Ensure correct format of the path
            if (newDocPath.EndsWith("/%"))
            {
                newDocPath = newDocPath.Remove(newDocPath.Length - 2);
            }

            if (newDocPath == "")
            {
                newDocPath = "/";
            }

            return newDocPath;
        }
        set
        {
            ViewState["NewDocumentPath"] = value;
        }
    }


    /// <summary>
    /// Indicates whether inserting new document is allowed.
    /// </summary>
    public bool AllowInsert
    {
        get
        {
            return mAllowInsert;
        }
        set
        {
            mAllowInsert = value;
        }
    }


    /// <summary>
    /// Indicates whether editing document is allowed.
    /// </summary>
    public bool AllowEdit
    {
        get
        {
            return mAllowEdit;
        }
        set
        {
            mAllowEdit = value;
        }
    }


    /// <summary>
    /// Indicates whether deleting document is allowed.
    /// </summary>
    public bool AllowDelete
    {
        get
        {
            return mAllowDelete;
        }
        set
        {
            mAllowDelete = value;
            this.editDoc.AllowDelete = value;
        }
    }


    /// <summary>
    /// Page template the new items are assigned to.
    /// </summary>
    public string NewItemPageTemplate
    {
        get
        {
            return mNewItemPageTemplate;
        }
        set
        {
            mNewItemPageTemplate = value;
        }
    }


    /// <summary>
    /// Type of the child documents that are allowed to be created.
    /// </summary>
    public string AllowedChildClasses
    {
        get
        {
            return mAllowedChildClasses;
        }
        set
        {
            mAllowedChildClasses = value;
        }
    }


    /// <summary>
    /// Alternative form name.
    /// </summary>
    public string AlternativeFormName
    {
        get
        {
            return mAlternativeFormName;
        }
        set
        {
            mAlternativeFormName = value;
        }
    }


    /// <summary>
    /// Form validation error message
    /// </summary>
    public string ValidationErrorMessage
    {
        get
        {
            return mValidationErrorMessage;
        }
        set
        {
            mValidationErrorMessage = value;
        }
    }


    /// <summary>
    /// Indicates whether permissions should be checked.
    /// </summary>
    public bool CheckPermissions
    {
        get
        {
            return mCheckPermissions;
        }
        set
        {
            mCheckPermissions = value;
        }
    }


    /// <summary>
    /// Indicates whether group permissions should be checked
    /// </summary>
    public bool CheckGroupPermissions
    {
        get
        {
            return mCheckGroupPermissions;
        }
        set
        {
            mCheckGroupPermissions = value;
        }
    }


    /// <summary>
    /// Which group of users can work with the documents
    /// </summary>
    public UserContributionAllowUserEnum AllowUsers
    {
        get
        {
            return this.mAllowUsers;
        }
        set
        {
            this.mAllowUsers = value;
        }
    }


    /// <summary>
    /// Gets or sets New item button label
    /// </summary>
    public string NewItemButtonText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("NewItemButtonText"), "ContributionList.lnkNewDoc");
        }
        set
        {
            this.SetValue("NewItemButtonText", value);
            this.btnNewDoc.ResourceString = value;
        }
    }


    /// <summary>
    /// Gets or sets List button label
    /// </summary>
    public string ListButtonText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ListButtonText"), "ContributionList.lnkDocs");
        }
        set
        {
            this.SetValue("ListButtonText", value);
            this.btnList.ResourceString = value;
        }
    }

    #endregion


    #region "Private properties"

    /// <summary>
    /// Parent node
    /// </summary>
    private TreeNode ParentNode
    {
        get
        {
            if (mParentNode == null)
            {
                mParentNode = GetParentNode();
            }
            return mParentNode;
        }
    }


    /// <summary>
    /// Parent node id
    /// </summary>
    private int ParentNodeID
    {
        get
        {
            return (this.ParentNode == null ? 0 : this.ParentNode.NodeID);
        }
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check license
        LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.UserContributions);

        this.mDataProperties.ParentControl = this;

        ReloadData();

        // Control initialization
        gridDocs.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridDocs_OnExternalDataBound);
        gridDocs.OnAction += new OnActionEventHandler(gridDocs_OnAction);
        gridDocs.GridView.CssClass = "ContributionsGrid";
        gridDocs.IsLiveSite = this.IsLiveSite;
        btnNewDoc.ResourceString = this.NewItemButtonText;
        btnList.ResourceString = this.ListButtonText;

        // Hide/Show edit document form
        pnlEdit.Visible = (editDoc.NodeID > 0);
        editDoc.SiteName = this.SiteName;
        editDoc.CultureCode = this.CultureCode;
        editDoc.FunctionsPrefix = this.ID + "_";
    }


    protected override void OnPreRender(EventArgs e)
    {
        // Register the scripts
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ContributionList_" + this.ClientID, this.ltlScript.Text);
        this.ltlScript.Text = "";

        base.OnPreRender(e);
    }


    private void ReloadData()
    {
        if (this.StopProcessing)
        {
            // Do nothing
            editDoc.StopProcessing = true;
        }
        else
        {
            if (((this.AllowUsers == UserContributionAllowUserEnum.Authenticated) || (this.AllowUsers == UserContributionAllowUserEnum.DocumentOwner))
                && !CMSContext.CurrentUser.IsAuthenticated())
            {
                // Not authenticated, do not display anything
                pnlList.Visible = false;
                pnlEdit.Visible = false;
                this.StopProcessing = true;
            }
            else
            {
                this.SetContext();

                // Hide document list
                gridDocs.Visible = false;

                // If the list of documents should be displayed ...
                if (this.DisplayList)
                {
                    // Get all documents of the current user
                    TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);

                    // Generate additional where condition
                    string classWhere = null;
                    if (this.ClassNames != "")
                    {
                        string[] names = this.ClassNames.Split(';');
                        foreach (string name in names)
                        {
                            if (name != "")
                            {
                                if (classWhere != null)
                                {
                                    classWhere += ", ";
                                }
                            }
                            classWhere += "'" + name.Replace("'", "''") + "'";
                        }
                        if (classWhere != null)
                        {
                            classWhere = "ClassName IN (" + classWhere + ")";
                        }
                    }

                    string where = SqlHelperClass.AddWhereCondition(this.WhereCondition, classWhere);

                    // Add user condition
                    if (this.AllowUsers == UserContributionAllowUserEnum.DocumentOwner)
                    {
                        where = SqlHelperClass.AddWhereCondition(where, "NodeOwner = " + CMSContext.CurrentUser.UserID);
                    }

                    // Get the documents
                    DataSet ds = DocumentHelper.GetDocuments(SiteName, CMSContext.ResolveCurrentPath(Path), CultureCode, CombineWithDefaultCulture, null, where, OrderBy, MaxRelativeLevel, SelectOnlyPublished, tree);
                    if (this.CheckPermissions)
                    {
                        ds = TreeSecurityProvider.FilterDataSetByPermissions(ds, NodePermissionsEnum.Read, CMSContext.CurrentUser);
                    }

                    if (!DataHelper.DataSourceIsEmpty(ds))
                    {
                        // Display and initialize grid if datasource is not empty
                        gridDocs.Visible = true;
                        gridDocs.DataSource = ds;
                        gridDocs.OrderBy = this.OrderBy;
                        editDoc.AlternativeFormName = this.AlternativeFormName;
                    }
                }

                bool isAuthorizedToCreateDoc = false;
                if (this.ParentNode != null)
                {
                    // Check user's permission to create new document if allowed
                    isAuthorizedToCreateDoc = !this.CheckPermissions || CMSContext.CurrentUser.IsAuthorizedToCreateNewDocument(this.ParentNodeID, null);
                    // Check group's permission to create new document if allowed
                    isAuthorizedToCreateDoc &= CheckGroupPermission("createpages");
                    
                    if (this.AllowUsers == UserContributionAllowUserEnum.DocumentOwner)
                    {
                        // Check if user is document owner (or global admin)
                        isAuthorizedToCreateDoc = isAuthorizedToCreateDoc && ((this.ParentNode.NodeOwner == CMSContext.CurrentUser.UserID) || CMSContext.CurrentUser.IsGlobalAdministrator);
                    }
                }

                // Enable/disable inserting new document
                pnlNewDoc.Visible = (isAuthorizedToCreateDoc && this.AllowInsert);

                if (!gridDocs.Visible && !pnlNewDoc.Visible && pnlList.Visible)
                {
                    // Not authenticated to create new docs and grid is hidden
                    this.StopProcessing = true;
                }

                this.ReleaseContext();
            }
        }
    }


    /// <summary>
    /// Initializes and shows edit form with available documents.
    /// </summary>
    protected void btnNewDoc_Click(object sender, EventArgs e)
    {
        // Initialize EditForm control
        editDoc.EnableViewState = true;
        editDoc.AllowDelete = this.AllowDelete && CheckGroupPermission("deletepages");
        editDoc.AllowedChildClasses = this.AllowedChildClasses;
        editDoc.NewItemPageTemplate = this.NewItemPageTemplate;
        editDoc.CheckPermissions = this.CheckPermissions;
        editDoc.Action = "new";
        // Set parent nodeId
        editDoc.NodeID = this.ParentNodeID;
        editDoc.ClassID = 0;
        editDoc.AlternativeFormName = this.AlternativeFormName;
        editDoc.ValidationErrorMessage = this.ValidationErrorMessage;

        pnlEdit.Visible = true;
        pnlList.Visible = false;
    }


    /// <summary>
    /// Get parent node ID.
    /// </summary>
    private TreeNode GetParentNode()
    {
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        return tree.SelectSingleNode(this.SiteName, CMSContext.ResolveCurrentPath(this.NewDocumentPath), TreeProvider.ALL_CULTURES);
    }


    /// <summary>
    /// Displays document list and hides edit form.
    /// </summary>
    protected void btnList_Click(object sender, EventArgs e)
    {
        pnlList.Visible = true;
        pnlEdit.Visible = false;
        editDoc.NodeID = 0;
        editDoc.EnableViewState = false;
    }


    /// <summary>
    /// UniGrid action buttons event handler.
    /// </summary>
    void gridDocs_OnAction(string actionName, object actionArgument)
    {
        TreeProvider tree = null;
        TreeNode node = null;

        switch (actionName.ToLower())
        {
            case "edit":
                // Check group's permission to edit document if allowed
                if (CheckGroupPermission("editpages"))
                {
                    editDoc.NodeID = ValidationHelper.GetInteger(actionArgument, 0);
                    editDoc.Action = "edit";
                    editDoc.CheckPermissions = this.CheckPermissions;

                    pnlEdit.Visible = true;
                    pnlList.Visible = false;
                }
                break;

            case "delete":
                tree = new TreeProvider(CMSContext.CurrentUser);

                // Delete specified node
                int documentId = ValidationHelper.GetInteger(actionArgument, 0);
                node = DocumentHelper.GetDocument(documentId, tree);
                if (node != null)
                {
                    // Check user's permission to delete document if allowed
                    bool hasUserDeletePermission = !this.CheckPermissions || IsUserAuthorizedToDeleteDocument(node);
                    // Check group's permission to delete document if allowed
                    hasUserDeletePermission &= CheckGroupPermission("deletepages");

                    if (hasUserDeletePermission)
                    {
                        DocumentHelper.DeleteDocument(node, tree, false, false, true);

                        // Fire OnAfterDelete
                        RaiseOnAfterDelete();

                        ReloadData();
                    }
                    // Access denied - not authorized to delete the document
                    else
                    {
                        AddAlert(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtodeletedocument"), node.NodeAliasPath));
                        return;
                    }
                }
                break;
        }
    }


    /// <summary>
    /// UniGrid external data bound.
    /// </summary>
    object gridDocs_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        sourceName = sourceName.ToLower();
        switch (sourceName)
        {
            // Column 'DocumentWorkflowStepID'
            case "documentworkflowstepid":
                int stepId = ValidationHelper.GetInteger(parameter, 0);
                if (stepId > 0)
                {
                    // Get workflow step display name
                    WorkflowStepInfo wsi = WorkflowStepInfoProvider.GetWorkflowStepInfo(stepId);
                    if (wsi != null)
                    {
                        return wsi.StepDisplayName;
                    }
                }
                break;

            // Column 'Published'
            case "published":
                bool published = ValidationHelper.GetBoolean(parameter, true);
                return (published ? ResHelper.GetString("General.Yes") : ResHelper.GetString("General.No"));

            case "documentmodifiedwhen":
            case "documentmodifiedwhentooltip":

                TimeZoneInfo tzi = null;

                // Get current time for user contribution list on live site
                string result = CMSContext.GetDateTimeForControl(this, ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME), out tzi).ToString();

                // Display time zone shift if needed
                if ((tzi != null) && TimeZoneHelper.TimeZonesEnabled())
                {
                    if (sourceName.EndsWith("tooltip"))
                    {
                        result = TimeZoneHelper.GetGMTLongStringOffset(tzi);
                    }
                    else
                    {
                        result += TimeZoneHelper.GetGMTStringOffset(" (GMT{0:+00.00;-00.00})", tzi);
                    }
                }
                return result;

            // Action 'edit'
            case "edit":
                ((Control)sender).Visible = this.AllowEdit && CheckGroupPermission("editpages");
                break;

            // Action 'delete'
            case "delete":
                ((Control)sender).Visible = this.AllowDelete && CheckGroupPermission("deletepages");
                break;
        }

        return parameter;
    }


    /// <summary>
    /// Checks whether the user is authorized to delete document.
    /// </summary>
    /// <param name="node">Document node</param>
    protected bool IsUserAuthorizedToDeleteDocument(TreeNode node)
    {
        bool isAuthorized = true;

        // Check delete permission
        if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Delete) != AuthorizationResultEnum.Allowed)
        {
            isAuthorized = false;
        }

        return isAuthorized;
    }


    /// <summary>
    /// Adds the alert message to the output request window
    /// </summary>
    /// <param name="message">Message to display</param>
    private void AddAlert(string message)
    {
        this.ltlScript.Text += ScriptHelper.GetAlertScript(message);
    }


    /// <summary>
    /// Returns true if group permissions should be checked and specified permission is allowed in current group.
    /// Also returns true if group permissions should not be checked.
    /// </summary>
    /// <param name="permissionName">Permission to check (createpages, editpages, deletepages)</param>
    protected bool CheckGroupPermission(string permissionName)
    {
        if (this.CheckGroupPermissions)
        {
            // Get current group ID
            int groupId = ModuleCommands.CommunityGetCurrentGroupID();
            if (groupId > 0)
            {
                // Returns true if current user is authorized for specified action in current group
                return ModuleCommands.CommunityCheckGroupPermission(permissionName, groupId) || CMSContext.CurrentUser.IsGroupAdministrator(groupId);
            }

            return false;
        }

        return true;
    }
    

    /// <summary>
    /// Raises the OnAfterDelete event
    /// </summary>
    private void RaiseOnAfterDelete()
    {
        if (OnAfterDelete != null)
        {
            OnAfterDelete(this, null);
        }
    }

    #endregion
}
