using System;

using CMS.LicenseProvider;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSSiteManager_Administration_Permissions_Default : CMSPermissionsPage
{
    protected string headerPage = "Permissions_Header.aspx";
    protected string headerSize = "133";

    protected void Page_Load(object sender, EventArgs e)
    {
        int siteId = QueryHelper.GetInteger("siteid", 0);
        if (siteId > 0)
        {
            headerPage += "?siteid=" + siteId;
            headerSize = "113";
        }
    }
}
