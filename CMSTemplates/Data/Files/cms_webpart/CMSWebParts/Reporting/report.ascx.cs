using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.CMSHelper;
using CMS.Reporting;

public partial class CMSWebParts_Reporting_Report : CMSAbstractWebPart
{
    #region "Properties"

    /// <summary>
    /// Gets or sets the code name of the report, which should be displayed
    /// </summary>
    public string ReportName
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("ReportName"), this.viewReport.ReportName);
        }
        set
        {
            this.SetValue("ReportName", value);
        }
    }


    /// <summary>
    /// Determines whether to show parameter filter or not
    /// </summary>
    public bool DisplayFilter
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("DisplayFilter"), false);
        }
        set
        {
            this.SetValue("DisplayFilter", value);
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            this.SetContext();

            this.viewReport.DisplayFilter = this.DisplayFilter;
            this.viewReport.ReportName = this.ReportName;
            this.viewReport.ReloadData(true);

            this.ReleaseContext();
        }
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        this.viewReport.DisplayFilter = this.DisplayFilter;
        this.viewReport.ForceLoadDefaultValues = true;
        this.viewReport.IngnoreWasInit = true;
        this.viewReport.ReportName = this.ReportName;
        this.viewReport.ReloadData(true);
    }
}
