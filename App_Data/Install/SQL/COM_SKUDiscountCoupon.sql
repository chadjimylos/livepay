CREATE TABLE [COM_SKUDiscountCoupon] (
		[SKUID]                int NOT NULL,
		[DiscountCouponID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_SKUDiscountCoupon]
	ADD
	CONSTRAINT [PK_COM_SKUDiscountCoupon]
	PRIMARY KEY
	([SKUID], [DiscountCouponID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_SKUDiscountCoupon]') IS NOT NULL
	AND OBJECT_ID(N'[COM_DiscountCoupon]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_SKUDiscountCoupon_DiscountCouponID_COM_DiscountCoupon]') IS NULL
BEGIN
		ALTER TABLE [COM_SKUDiscountCoupon]
			ADD CONSTRAINT [FK_COM_SKUDiscountCoupon_DiscountCouponID_COM_DiscountCoupon]
			FOREIGN KEY ([DiscountCouponID]) REFERENCES [COM_DiscountCoupon] ([DiscountCouponID])
END
IF OBJECT_ID(N'[COM_SKUDiscountCoupon]') IS NOT NULL
	AND OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_SKUDiscountCoupon_SKUID_COM_SKU]') IS NULL
BEGIN
		ALTER TABLE [COM_SKUDiscountCoupon]
			ADD CONSTRAINT [FK_COM_SKUDiscountCoupon_SKUID_COM_SKU]
			FOREIGN KEY ([SKUID]) REFERENCES [COM_SKU] ([SKUID])
END
