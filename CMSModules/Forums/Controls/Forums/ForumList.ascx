<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ForumList.ascx.cs" Inherits="CMSModules_Forums_Controls_Forums_ForumList" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<cms:UniGrid runat="server" ID="gridElem" GridName="~/CMSModules/Forums/Controls/Forums/Forum_List.xml" DelayedReload="true"
    Columns="ForumID, ForumDisplayName, ForumOpen, ForumModerated, ForumThreadsAbsolute, ForumPostsAbsolute, ForumLastPostTimeAbsolute, ForumLastPostUserNameAbsolute" />
