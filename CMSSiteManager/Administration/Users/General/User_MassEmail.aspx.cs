using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.EmailEngine;
using CMS.SiteProvider;
using CMS.FormControls;

public partial class CMSSiteManager_Administration_Users_General_User_MassEmail : CMSUsersPage
{
    private int siteId = 0;
    private UserInfo currentUser = null;
    private FormEngineUserControl groupsControl = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.DisplayControlsPanel = true;

        this.btnSend.Click += new EventHandler(btnSend_Click);
        this.siteSelector.UniSelector.OnSelectionChanged += new EventHandler(Site_Changed);
        this.siteSelector.DropDownSingleSelect.AutoPostBack = true;        

        // Try to get site ID from query string
        siteId = GetSiteID(Request.QueryString["siteId"]);

        // Disable sites dropdown in cmsdesk
        if (siteId > 0)
        {
            // Hide site selector
            this.CurrentMaster.DisplayControlsPanel = false;
            siteSelector.StopProcessing = true;            
        }
        else
        {            
            // Load selected site from site selector
            if (RequestHelper.IsPostBack())
            {
                siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
            }            
        }

        // Set siteid to other selectors
        this.roles.SiteID = siteId;
        this.roles.CurrentSelector.SelectionMode = SelectionModeEnum.Multiple;
        this.roles.CurrentSelector.ResourcePrefix = "addroles";
        
        // Show site filter in sitemanager
        this.roles.ShowSiteFilter = ((siteId <= 0) && CMSContext.CurrentUser.IsGlobalAdministrator);
        
        this.users.SiteID = siteId;
        this.users.IsLiveSite = false;
        this.users.UniSelector.ReturnColumnName = "UserName";

        // Show site filter in sitemanager
        this.users.ShowSiteFilter = ((siteId <= 0) && CMSContext.CurrentUser.IsGlobalAdministrator);        

        // Display/hide specific selectors according to selected site
        EnsureSelectorPanels();

        // Add current user email at first load
        currentUser = CMSContext.CurrentUser;        
        if (!RequestHelper.IsPostBack() && (currentUser != null)) 
        {
            txtFrom.Text = CMSContext.CurrentUser.Email;
        }

        // Initialize masterpage and other controls
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_User/massemail.png");
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("administration.users_header.massemails");
        this.CurrentMaster.Title.HelpTopicName = "User_MassEmail";

        this.lblSite.Text = ResHelper.GetString("general.site") + ResHelper.Colon;
        this.lblFrom.Text = ResHelper.GetString("general.fromemail") + ResHelper.Colon;        
        this.lblSubject.Text = ResHelper.GetString("general.subject") + ResHelper.Colon;
        this.btnSend.Text = ResHelper.GetString("general.send");
        this.uploader.AddButtonImagePath = GetImageUrl("Objects/CMS_User/addattachment.png");
        this.pnlAttachments.GroupingText = ResHelper.GetString("general.attachments");

        ScriptHelper.RegisterClientScriptBlock(this.Page,typeof(string), "IsSubjectEmpty", ScriptHelper.GetScript(@"
        function IsEmailEmpty()
        {
            var obj = document.getElementById('" + txtSubject.ClientID + @"');
            if (obj != null)
            {
                if ((obj.value == null) || obj.value == '' || obj.value.replace(/ /g,'') == '')
                {
                    if (!confirm(" + ScriptHelper.GetString(ResHelper.GetString("massemail.emptyemailsubject")) + @"))
                    {
                        return false;
                    }
                }
            }

            return true;
        }
        "));

        btnSend.OnClientClick = "return IsEmailEmpty()";   

        // Hides HTML or text area according to e-mail format in settings
        EnsureEmailFormatRegions();

        // Initialize the e-mail text editor
        InitHTMLEditor();
    }


    /// <summary>
    /// Handles change in site selection.
    /// </summary>
    protected void Site_Changed(object sender, EventArgs e)
    {
        // Hide HTML or text area according to e-mail format in settings
        EnsureEmailFormatRegions();
        
        pnlUpdate.Update();
    }


    /// <summary>
    /// Displays or hides specific selectors according to selected site.
    /// </summary>
    protected void EnsureSelectorPanels()
    {
        if (siteId > 0)
        {
            // Display roles panel
            pnlAccordionRoles.Visible = true;
            // Check group availability and try to load group selector
            if (AddGroupSelector(siteId))
            {
                // Display groups panel
                pnlAccordionGroups.Visible = true;
            }
            else
            {
                // Hide groups panel
                pnlAccordionGroups.Visible = false;
            }
        }
        else
        {
            // Hide roles and groups panels
            pnlAccordionRoles.Visible = false;
            pnlAccordionGroups.Visible = false;
        }
    }


    /// <summary>
    /// Loads group selector control to the page.
    /// </summary>
    /// <param name="siteId">Site ID</param>
    /// <returns>Returns true if site contains group and group selector was loaded</returns>
    private bool AddGroupSelector(int siteId)
    {
        SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);
        if ((si != null) && (ModuleCommands.CommunitySiteHasGroup(si.SiteID)))
        {
            groupsControl = Page.LoadControl("~/CMSModules/Groups/FormControls/MultipleGroupSelector.ascx") as FormEngineUserControl;
            if (groupsControl != null)
            {
                this.groupsControl.FormControlParameter = siteId;
                this.groupsControl.IsLiveSite = false;
                this.plcGroupSelector.Controls.Add(groupsControl);
                this.groupsControl.SetValue("reloaddata", true);

                return true;
            }
        }

        return false;
    }


    /// <summary>
    /// Sends the email.
    /// </summary>
    protected void btnSend_Click(object sender, EventArgs e)
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify"))
        {
            RedirectToAccessDenied("CMS.Users", "Modify");
        }

        // Validate first
        string errorMessage = new Validator().IsEmail(this.txtFrom.Text, ResHelper.GetString("general.correctemailformat")).Result;

        // Get recipients
        string groupNames = null;
        if (groupsControl != null)
        {
            groupNames = Convert.ToString(this.groupsControl.Value);
        }
        string userNames = Convert.ToString(this.users.Value);
        string roleNames = Convert.ToString(this.roles.Value);

        if (string.IsNullOrEmpty(groupNames) && string.IsNullOrEmpty(userNames) && string.IsNullOrEmpty(roleNames))
        {
            errorMessage = ResHelper.GetString("massemail.norecipients");
        }

        if (!string.IsNullOrEmpty(errorMessage))
        {
            this.lblError.Text = errorMessage;
            this.lblError.Visible = true;
            return;
        }

        // Create the message
        EmailMessage message = new EmailMessage();
        message.Subject = this.txtSubject.Text;
        message.From = this.txtFrom.Text;
        if (plcText.Visible)
        {
            message.Body = htmlText.ResolvedValue;
        }
        if (plcPlainText.Visible)
        {
            message.PlainTextBody = txtPlainText.Text;
        }

        // Get the attachments
        HttpPostedFile[] attachments = this.uploader.PostedFiles;
        foreach (HttpPostedFile att in attachments)
        {
            message.Attachments.Add(new EmailAttachment(att.InputStream, Path.GetFileName(att.FileName), Guid.NewGuid(), DateTime.Now, siteId));
        }

        // Send the message using email engine
        EmailSender.SendMassEmail(message, userNames, roleNames, groupNames, siteId);

        this.lblInfo.Text = ResHelper.GetString("massemail.emailsent");
        this.lblInfo.Visible = true;
    }


    /// <summary>
    /// Initializes HTML editor's settings.
    /// </summary>
    protected void InitHTMLEditor()
    {
        this.htmlText.AutoDetectLanguage = false;
        this.htmlText.DefaultLanguage = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        this.htmlText.ToolbarSet = "SimpleEdit";
        this.htmlText.MediaDialogConfig.UseFullURL = true;
        this.htmlText.LinkDialogConfig.UseFullURL = true;
        this.htmlText.QuickInsertConfig.UseFullURL = true;
    }


    /// <summary>
    /// Hides HTML or text area according to e-mail format in settings.
    /// </summary>
    protected void EnsureEmailFormatRegions()
    {
        string siteName = null;
        plcPlainText.Visible = true;
        plcText.Visible = true;

        if (siteId > 0)
        {
            // Get site info object to reach site name
            SiteInfo site = SiteInfoProvider.GetSiteInfo(siteId);
            if (site != null)
            {
                siteName = site.SiteName;
            }
        }

        // Get e-mail format from settings
        EmailFormatEnum emailFormat = EmailHelper.GetEmailFormat(SettingsKeyProvider.GetStringValue(siteName + ".CMSEmailFormat"), EmailFormatEnum.Html);
        switch (emailFormat)
        {
            case EmailFormatEnum.Html:
                plcPlainText.Visible = false;
                break;

            case EmailFormatEnum.PlainText:
                plcText.Visible = false;
                break;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Set proper header CSS classes for accordeon
        for (int i = 0; i < ajaxAccordion.Panes.Count; i++)
        {
            if (i == ajaxAccordion.SelectedIndex)
            {
                ajaxAccordion.Panes[i].HeaderCssClass = ajaxAccordion.HeaderSelectedCssClass;
            }
            else
            {
                ajaxAccordion.Panes[i].HeaderCssClass = ajaxAccordion.HeaderCssClass;
            }
        }
    }
}
