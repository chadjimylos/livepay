CREATE PROCEDURE [Proc_CMS_WebfarmTask_Insert]
	@TaskType nvarchar(50), 
	@TaskTextData ntext, 
	@TaskCreated datetime,
	@TaskMachineName nvarchar(450),
	@TaskTarget nvarchar(450),
	@TaskBinary image
AS
BEGIN
	INSERT INTO [CMS_WebFarmTask] ( [TaskType], [TaskTextData], [TaskBinaryData], [TaskCreated], [TaskEnabled], [TaskMachineName], [TaskTarget]) VALUES (  @TaskType, @TaskTextData, @TaskBinary, @TaskCreated, 0, @TaskMachineName, @TaskTarget); SELECT SCOPE_IDENTITY() AS [TaskID] 
END
