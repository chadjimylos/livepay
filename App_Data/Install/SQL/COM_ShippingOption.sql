CREATE TABLE [COM_ShippingOption] (
		[ShippingOptionID]               int NOT NULL IDENTITY(1, 1),
		[ShippingOptionName]             nvarchar(200) NOT NULL,
		[ShippingOptionDisplayName]      nvarchar(200) NOT NULL,
		[ShippingOptionCharge]           float NOT NULL,
		[ShippingOptionEnabled]          bit NOT NULL,
		[ShippingOptionSiteID]           int NOT NULL,
		[ShippingOptionGUID]             uniqueidentifier NOT NULL,
		[ShippingOptionLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_ShippingOption]
	ADD
	CONSTRAINT [PK_COM_ShippingOption]
	PRIMARY KEY
	NONCLUSTERED
	([ShippingOptionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_ShippingOption]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShippingOption_ShippingOptionSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [COM_ShippingOption]
			ADD CONSTRAINT [FK_COM_ShippingOption_ShippingOptionSiteID_CMS_Site]
			FOREIGN KEY ([ShippingOptionSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_COM_ShippingOption_ShippingOptionSiteID_ShippingOptionDisplayName_ShippingOptionEnabled]
	ON [COM_ShippingOption] ([ShippingOptionSiteID], [ShippingOptionDisplayName], [ShippingOptionEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
