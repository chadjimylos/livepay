CREATE TABLE [CMS_WorkflowScope] (
		[ScopeID]               int NOT NULL IDENTITY(1, 1),
		[ScopeStartingPath]     nvarchar(450) NOT NULL,
		[ScopeWorkflowID]       int NOT NULL,
		[ScopeClassID]          int NULL,
		[ScopeSiteID]           int NOT NULL,
		[ScopeGUID]             uniqueidentifier NOT NULL,
		[ScopeLastModified]     datetime NOT NULL,
		[ScopeCultureID]        int NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WorkflowScope]
	ADD
	CONSTRAINT [PK_CMS_WorkflowScope]
	PRIMARY KEY
	NONCLUSTERED
	([ScopeID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_WorkflowScope]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WorkflowScope_ScopeClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_WorkflowScope]
			ADD CONSTRAINT [FK_CMS_WorkflowScope_ScopeClassID_CMS_Class]
			FOREIGN KEY ([ScopeClassID]) REFERENCES [CMS_Class] ([ClassID])
END
IF OBJECT_ID(N'[CMS_WorkflowScope]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Culture]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WorkflowScope_ScopeCultureID_CMS_Culture]') IS NULL
BEGIN
		ALTER TABLE [CMS_WorkflowScope]
			ADD CONSTRAINT [FK_CMS_WorkflowScope_ScopeCultureID_CMS_Culture]
			FOREIGN KEY ([ScopeCultureID]) REFERENCES [CMS_Culture] ([CultureID])
END
IF OBJECT_ID(N'[CMS_WorkflowScope]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WorkflowScope_ScopeSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_WorkflowScope]
			ADD CONSTRAINT [FK_CMS_WorkflowScope_ScopeSiteID_CMS_Site]
			FOREIGN KEY ([ScopeSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[CMS_WorkflowScope]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Workflow]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WorkflowScope_ScopeWorkflowID_CMS_WorkflowID]') IS NULL
BEGIN
		ALTER TABLE [CMS_WorkflowScope]
			ADD CONSTRAINT [FK_CMS_WorkflowScope_ScopeWorkflowID_CMS_WorkflowID]
			FOREIGN KEY ([ScopeWorkflowID]) REFERENCES [CMS_Workflow] ([WorkflowID])
END
CREATE INDEX [IX_CMS_WorkflowScope_ScopeClassID]
	ON [CMS_WorkflowScope] ([ScopeClassID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WorkflowScope_ScopeCultureID]
	ON [CMS_WorkflowScope] ([ScopeCultureID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WorkflowScope_ScopeSiteID]
	ON [CMS_WorkflowScope] ([ScopeSiteID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_WorkflowScope_ScopeStartingPath]
	ON [CMS_WorkflowScope] ([ScopeStartingPath])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WorkflowScope_ScopeWorkflowID]
	ON [CMS_WorkflowScope] ([ScopeWorkflowID])
	ON [PRIMARY]
