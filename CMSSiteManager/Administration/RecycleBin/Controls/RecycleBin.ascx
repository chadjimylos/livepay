<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecycleBin.ascx.cs" Inherits="CMSSiteManager_Administration_RecycleBin_Controls_RecycleBin" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>

<%@ Register Src="~/CMSAdminControls/AsyncControl.ascx" TagName="AsyncControl" TagPrefix="cms" %>

<asp:Panel runat="server" ID="pnlLog" Visible="false">
    <div class="AsyncLogBackground">
        &nbsp;</div>
    <div class="AsyncLogArea">
        <div style="width: 96%;">
            <asp:Panel ID="pnlAsyncBody" runat="server" CssClass="PageBody">
                <asp:Panel ID="pnlTitleAsync" runat="server" CssClass="PageHeader" EnableViewState="false">
                    <cms:PageTitle ID="titleElemAsync" runat="server" EnableViewState="false" />
                </asp:Panel>
                <asp:Panel ID="pnlCancel" runat="server" CssClass="PageHeaderLine" EnableViewState="false">
                    <cms:LocalizedButton runat="server" ID="btnCancel" ResourceString="general.cancel" CssClass="SubmitButton"
                        EnableViewState="false" />
                </asp:Panel>
                <asp:Panel ID="pnlAsyncContent" runat="server" CssClass="PageContent">
                    <cms:AsyncControl ID="ctlAsync" runat="server" />
                </asp:Panel>
            </asp:Panel>
        </div>
    </div>
</asp:Panel>
<cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Panel ID="pnlActions" runat="server" CssClass="PageHeaderLine" EnableViewState="false">
            <%-- RTL fix - do not remove --%>
            <span style="visibility: hidden;">a</span>
            <asp:LinkButton ID="lnkRestoreAll" runat="server" OnClick="btnRestoreAll_Click" CssClass="MenuItemEdit"
                EnableViewState="false">
                <asp:Image ID="imgRestoreAll" runat="server" EnableViewState="false" />
                <%=ResHelper.GetString("RecycleBin.RestoreAll")%>
            </asp:LinkButton>
            <asp:LinkButton ID="lnkRestoreSelected" runat="server" OnClick="btnRestoreSelected_Click"
                CssClass="MenuItemEdit" EnableViewState="false">
                <asp:Image ID="imgRestoreSelected" runat="server" EnableViewState="false" />
                <%=ResHelper.GetString("RecycleBin.RestoreSelected")%>
            </asp:LinkButton>
            <asp:LinkButton ID="lnkEmptyRecBin" runat="server" OnClick="btnEmptyRecBin_Click"
                CssClass="MenuItemEdit" EnableViewState="false">
                <asp:Image ID="imgEmptyRecBin" runat="server" EnableViewState="false" />
                <%=ResHelper.GetString("RecycleBin.EmptyRecycleBin")%>
            </asp:LinkButton>
            <br style="clear: both;" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlContent" CssClass="PageContent">
            <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
                Visible="false" />
            <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
                Visible="false" />
            <div>
                <cms:UniGrid ID="ugRecycleBin" runat="server" GridName="~/CMSSiteManager/Administration/RecycleBin/Controls/RecycleBin.xml"
                    IsLiveSite="false" HideControlForZeroRows="true" />
            </div>
        </asp:Panel>
    </ContentTemplate>
</cms:CMSUpdatePanel>

<script src="<%= ResolveUrl("~/CMSScripts/cms.js") %>" type="text/javascript"></script>

