using System;
using System.Data;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Polls;

public partial class CMSModules_Polls_FormControls_PollSelector : CMS.FormControls.FormEngineUserControl
{
    private string mPollCodeName = null;


    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            drpPolls.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return ValidationHelper.GetString(drpPolls.SelectedValue, "");
        }
        set
        {
            mPollCodeName = ValidationHelper.GetString(value, "");
        }
    }


    /// <summary>
    /// Gets ClientID of the dropdownlist with polls
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return drpPolls.ClientID;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if ((drpPolls.Items.Count == 0) && (CMSContext.CurrentSite != null))
        {
            DataSet ds = PollInfoProvider.GetPolls(CMSContext.CurrentSiteName, -1, "PollDisplayName, PollCodeName");
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                drpPolls.DataSource = ds;
                drpPolls.DataTextField = "PollDisplayName";
                drpPolls.DataValueField = "PollCodeName";
                drpPolls.DataBind();
            }

            if (!string.IsNullOrEmpty(mPollCodeName) && (drpPolls.Items != null) && (drpPolls.Items.FindByValue(mPollCodeName) != null))
            {
                drpPolls.SelectedValue = mPollCodeName;
            }
        }
    }
}
