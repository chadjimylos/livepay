﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UcHomeSearch.ascx.vb" Inherits="CMSTemplates_LivePay_UcHomeSearch" %>
<script language="javascript" type="text/javascript" >
    function handle_keydownHome(e) {
        document.getElementById('<%=btnSearch.ClientId %>').focus();
        return (true);
    }

    function RebindRpt(CachValue) {

        CachValue = CachValue.replace(/[']/gi, "") 
        setTimeout("ExecSearch('" + CachValue + "')", 500)
    }

    function ExecSearch(CachValue) {
        var RealValue = document.getElementById('<%=txtSearch.ClientID %>').value
        RealValue = RealValue.replace(/[']/gi, "") 

        if (RealValue == CachValue) {
            var text = document.getElementById('<%=txtSearch.clientID %>').value;
            text = text.replace(/[']/gi, "") 
            if (text != null && text.length > 0) {
               
                GetHomeMerchantData(RealValue)

            } else {
                ShowHideQuickRes(false, 0)
            }
        }
    }

    function GetHomeMerchantData(val) {
        var x;
        if (window.ActiveXObject) {
            x = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else {
            x = new XMLHttpRequest();
        }
        val = escape(val)
        x.open("GET", "/CMSTemplates/LivePay/xmlHttpMerchants.aspx?MerchantKeyWord=" + val + "&RecordsNo=3&FormPage=home", true);
       
        x.onreadystatechange = function () {
            if (x.readyState == 4) {
                var Results = x.responseText;
                var RowCount = Results.split("#]")[0]
                Results = Results.split("#]")[1]
                if (Results.length > 0) {
                    document.getElementById('QuickSrcResAll').innerHTML = Results;
                    ShowHideQuickRes(true, RowCount);
                } else {
                    ShowHideQuickRes(false, 0);
                }

            }
        }
        x.send(null);
    }

    function SetFocus() {
        var elem = document.getElementById('<%=txtSearch.clientID %>');
        var caretPos = elem.value.length
        if (elem != null) {
            if (elem.createTextRange) {
                var range = elem.createTextRange();
                range.move('character', caretPos);
                range.select();
            }
            else {
                elem.setSelectionRange(caretPos, caretPos);
                elem.focus();
                // Workaround for FF overflow no scroll problem 
                // Trigger a "space" keypress. 
                var evt = document.createEvent("KeyboardEvent");
                evt.initKeyEvent("keypress", true, true, null, false, false, false, false, 0, 32);
                elem.dispatchEvent(evt);
                // Trigger a "backspace" keypress. 
                evt = document.createEvent("KeyboardEvent");
                evt.initKeyEvent("keypress", true, true, null, false, false, false, false, 8, 0);
                elem.dispatchEvent(evt);

            }
        }
    }


    function ShowHideQuickRes(IsVisible, RowCount) {
        document.getElementById('ViewMoreResults').style.display = 'none';
        if (IsVisible) {
            document.getElementById('QuickResDiv').style.display = '';
            if (RowCount > 2) {
                document.getElementById('ViewMoreResults').style.display = '';
                var val = document.getElementById('<%=txtSearch.clientID %>').value
                document.getElementById('lnkViewMoreResults').href = 'Search.aspx?searchtext=' + val;
            }
        }
        else {
            document.getElementById('QuickResDiv').style.display = 'none';
        }
    }

   
</script>



<asp:UpdatePanel ID="upnl" runat="server" UpdateMode="Always"  >
<ContentTemplate >
    <div class="QuickSrcMain">
        <div style="padding-top:40px">
            <div class="QuickSrcTitleTop"><%=ResHelper.LocalizeString("{$=>Καλώς ήρθατε στην υπηρεσία|en-us=>Καλώς ήρθατε στην υπηρεσία$}") %></div>
            <div class="QuickSrcTitleTopSec"><%=ResHelper.LocalizeString("{$=ηλεκτρονικών πληρωμών και εισπράξεων,|en-us=ηλεκτρονικών πληρωμών και εισπράξεων,$}") %> </div>
            <div class="QuickSrcTitleTopThird"><%=ResHelper.LocalizeString("{$=Live-Pay!|en-us=Live-Pay!$}") %></div>
            <div class="QuickSrcMainSrcDiv">
                <div class="QuickSrcMainSrcDivBG">
                    <div style="text-align:left;" >
                        <div style="color:White;font-size:12px;padding:7px 0px 0px 9px">Άμεση Πληρωμή Λογαριασμού</div>
                        <div class="QuickSrcTxt" style="padding-top:12px"><asp:TextBox BorderWidth="0" BackColor="Transparent" onkeydown=" if (event.keyCode == 13) return handle_keydownHome(event);" autocomplete="off" cssClass="SearchTxt" onkeyup="RebindRpt(this.value);" ID="txtSearch" runat="server" ></asp:TextBox></div>
                        <div class="QuickSrcBtn" style="padding-top:12px"><asp:ImageButton ImageUrl="/app_themes/LivePay/btnSearchOk.gif" runat="server" ID="btnSearch" /></div>
                        <div class="Clear"></div>
                        <div class="QuickSrcResTopMain" id="QuickResDiv" style="display:none">
                            <div class="QuickSrcResMain">
                                <div class="QuickSrcResAll" id="QuickSrcResAll">
                                
                                </div>
                                <div class="QuickSrcResViewMore" id="ViewMoreResults" style="display:none">
                                 <div class="QuickSrcResTitleUnder"><a href="#" id="lnkViewMoreResults" >» <%=ResHelper.LocalizeString("{$=Δείτε Περισσότερα Αποτελέσματα|en-us=Δείτε Περισσότερα Αποτελέσματα$}") %></a></div>
                                 <div class="QuickSrcResTitleSmall"><%=ResHelper.LocalizeString("{$=Εμφανίζονται τα πρώτα  -3- αποτελέσματα|en-us=Εμφανίζονται τα πρώτα  -3- αποτελέσματα$}") %></div>
                                 
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</ContentTemplate>
</asp:UpdatePanel>

