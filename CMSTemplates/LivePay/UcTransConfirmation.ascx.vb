﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports LivePay_ESBBridge

Partial Class CMSTemplates_LivePay_UcTransConfirmation
    Inherits CMSUserControl


#Region "Variables"

    ''' <summary>
    ''' My property
    ''' </summary>
    Private mMyProperty As String = Nothing

    Private TimeoutLivePayBridge As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutLivePayBridge"))
#End Region

#Region "Properties"
    Protected ReadOnly Property PageType() As String
        Get
            Dim sPageType As String = String.Empty
            If Not String.IsNullOrEmpty(Request("PageType")) Then
                sPageType = Request("PageType").ToLower
            End If
            Return sPageType
        End Get
    End Property


    Protected ReadOnly Property Result() As String
        Get
            Dim sResult As String = String.Empty
            If Not String.IsNullOrEmpty(Request("Result")) Then
                sResult = Request("Result").ToLower
            End If
            Return sResult
        End Get
    End Property


    Public ReadOnly Property TransGUID() As String
        Get
            Dim sTransID As String = "0"
            If Not String.IsNullOrEmpty(Request("TransID")) Then
                sTransID = Request("TransID")
            End If
            Return sTransID
        End Get
    End Property

#End Region

#Region "Methods"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        imgExportPDF.Src = ResHelper.LocalizeString("{$=/app_themes/LivePay/btnSavePDF.gif|en-us=/app_themes/LivePay/btnSavePDF_en-us.png$}")
        btnPrint.Src = ResHelper.LocalizeString("{$=/app_themes/LivePay/TransConfirmation/btnPrintReceipt.png|en-us=/app_themes/LivePay/TransConfirmation/btnPrintReceipt_en-us.png$}")
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()

        imgExportPDF.Attributes.Add("onclick", "ExportDetails('/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=MerchantTransDetails&PageType=" & Me.PageType & "&lang=" & CMSContext.CurrentDocumentCulture.CultureCode & "');")
        btnPrint.Attributes.Add("onclick", "window.open('/CMSTemplates/LivePay/PrintPage.aspx?ExportType=MerchantTransDetails&PageType=" & Me.PageType & "&lang=" & CMSContext.CurrentDocumentCulture.CultureCode & "');")
        If Result = "ok" Then

            Select Case Me.PageType

                Case "refund"
                    MessageTitle.InnerHtml = ResHelper.LocalizeString("{$=Ολοκληρώσατε με επιτυχία τον αντιλογισμό της παρακάτω συναλλαγής|en-us=Ολοκληρώσατε με επιτυχία τον αντιλογισμό της παρακάτω συναλλαγής$}")
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Αντιλογισμός Συναλλαγής|en-us=Αντιλογισμός Συναλλαγής$}")
                    tdCancelRefuntTitle.InnerHtml = ResHelper.LocalizeString("{$=Ημερομηνία Αντιλογισμού|en-us=Ημερομηνία Αντιλογισμού$}")
                Case "cancel"
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Ακύρωση Συναλλαγής|en-us=Ακύρωση Συναλλαγής$}")
                    MessageTitle.InnerHtml = ResHelper.LocalizeString("{$=Ολοκληρώσατε με επιτυχία την ακύρωση της παρακάτω συναλλαγής|en-us=Ολοκληρώσατε με επιτυχία την ακύρωση της παρακάτω συναλλαγής$}")
                    tdCancelRefuntTitle.InnerHtml = ResHelper.LocalizeString("{$=Ημερομηνία Ακύρωσης|en-us=Ημερομηνία Ακύρωσης$}")
                    '---- Hide Batch Fields
                    FieldBatchNo.Visible = False
                    FieldBatchCloseDtm.Visible = False
                    FieldBatchStatus.Visible = False
                    '---- Hide Batch Fields
                Case "closebatch"
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Kλείσιμο πακέτου|en-us=Kλείσιμο πακέτου$}")
                    MessageTitle.InnerHtml = ResHelper.LocalizeString("{$=Ολοκληρώσατε με επιτυχία τo κλείσιμο πακέτου |en-us=Ολοκληρώσατε με επιτυχία τo κλείσιμο πακέτου$}")
                    MainDivForm.Style.Add("display", "none")
            End Select

            If TransGUID <> "0" Then
                GetData()
            End If
        Else
            imgResult.Src = "/app_themes/LivePay/ErrorIcon.png"
            MainDivForm.Style.Add("display", "none")
            MainDivErrorContainer.Style.Add("display", "")
            MessageTitle.Style.Add("color", "#dc2c09")
            MessageTitle.InnerHtml = ResHelper.LocalizeString("{$=Παρουσιάστηκε σφάλμα|en-us=Παρουσιάστηκε σφάλμα$}")
            'GetESBError

            Dim ErrorMessage As String = String.Empty

            If Me.Result = "esberror" Then
                ErrorMessage = String.Concat(ErrorMessage, "<div class='txtError'>Το ESB (EUROBANK) παρουσίασε το παρακάτω σφάλμα : </div><div>")
            Else
                ErrorMessage = String.Concat(ErrorMessage, "<div  class='txtError'>General Error:</div><div>")
            End If


            Select Case Me.PageType.ToLower
                Case "refund"
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Αντιλογισμός Συναλλαγής|en-us=Αντιλογισμός Συναλλαγής$}")
                    ErrorMessage = String.Concat(ErrorMessage, GetESBError(Session("RefundError")))
                Case "cancel"
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Ακύρωση Συναλλαγής|en-us=Ακύρωση Συναλλαγής$}")
                    ErrorMessage = String.Concat(ErrorMessage, GetESBError(Session("CancelError")))
                Case "closebatch"
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Kλείσιμο πακέτου|en-us=Kλείσιμο πακέτου$}")
                    ErrorMessage = String.Concat(ErrorMessage, GetESBError(Session("CloseBatchError")))
            End Select
            ErrorMessage = String.Concat(ErrorMessage, "</div>")
            MainDivErrorContainer.InnerHtml = ErrorMessage
        End If

    End Sub


    Private Function GetESBError(ByVal ErrorCode As String) As String
        Dim dt As DataTable = DBConnection.GetESBError(CMSContext.CurrentDocumentCulture.CultureCode, ErrorCode)
        If dt.Rows.Count > 0 Then
            Return ErrorCode & ": " & dt.Rows(0)("ErrorMessage").ToString
        End If
    End Function


    Private Sub GetData()
        If Me.TransGUID <> String.Empty Then
            Dim TransactionID As String = "0"
            Dim TransSystem As LivePay_ESBBridge.SystemEnum
            Dim MerchantName As String = String.Empty
            Dim myGuid As Guid = Nothing
            myGuid = New Guid(Me.TransGUID)

            Dim dt As DataTable = DBConnection.GetMerchantByGUID(myGuid)
            If dt.Rows.Count > 0 Then
                TransactionID = dt.Rows(0)("MerchantTransactionID").ToString

                If dt.Rows(0)("ClassName").ToString = "LivePay.MerchantPublicSector" Then
                    TransSystem = LivePay_ESBBridge.SystemEnum.Public
                Else
                    TransSystem = LivePay_ESBBridge.SystemEnum.Merchants
                End If

                Session("TransDetails_TransIDForExport") = TransactionID
                Session("TransDetails_SystemForExport") = TransSystem

                MerchantName = dt.Rows(0)("CompanyName").ToString
            End If

            If TransactionID <> "0" Then
                Dim w As New LivePay_ESBBridge.Bridge
                w.Timeout = TimeoutLivePayBridge
                Dim TransResponse As TransactionDetailsResponse = w.GetTransactionDetails(TransactionID, TransSystem)
                Dim TransInfo As TransactionDetailsInfo = TransResponse.TransactionDetails
                If Not IsNothing(TransInfo) Then
                    tdBoxNo.InnerHtml = TransInfo.batchId
                    If TransInfo.batchDate = DateTime.MinValue Then
                        tdBoxCloseDtm.InnerHtml = "-"
                    Else
                        tdBoxCloseDtm.InnerHtml = TransInfo.batchDate
                    End If

                    tdBoxStatus.InnerHtml = IIf(TransInfo.batchClosed, ResHelper.LocalizeString("{$=Κλειστό|en-us=Closed$}"), ResHelper.LocalizeString("{$=Ανοιχτό|en-us=Open$}"))
                    tdTransCode.InnerHtml = TransInfo.transactionId
                    tdTransDtm.InnerHtml = TransInfo.transactionDate
                    tdCustFullName.InnerHtml = TransInfo.customerName
                    tdCancelDTM.InnerHtml = Now
                    Dim amount As String = Replace(TransInfo.transactionAmount, ".", ",")
                    Dim FinalAmount As String
                    If amount.Split(",").Length > 1 Then
                        FinalAmount = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
                    ElseIf amount.Split(",").Length = 1 Then
                        FinalAmount = amount & ",00"
                    Else
                        FinalAmount = amount
                    End If

                    tdPrice.InnerHtml = FinalAmount
                    tdTransType.InnerHtml = TransInfo.transactionType.ToString
                    tdTransStatus.InnerHtml = TransInfo.transactionStatus.ToString

                    Dim CardTypeDescr As String = "Visa"
                    If TransInfo.cardType = 1 Then
                        CardTypeDescr = "MasterCard"
                    ElseIf TransInfo.cardType = 2 Then
                        CardTypeDescr = "EuroLine"
                    End If

                    tdCardType.InnerHtml = CardTypeDescr
                    Dim CardNo As String = Replace(TransInfo.cardNumber, " ", String.Empty)
                    tdCardNo.InnerHtml = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                    tdEmail.InnerHtml = TransInfo.customerEmail
                    tdContactPhone.InnerHtml = TransInfo.customerTelephone
                    CreateCustomFields(TransInfo.merchantId, TransInfo)
                End If
            End If
        End If
    End Sub

    Private Sub CreateCustomFields(ByVal MerchantID As String, ByVal TransInfo As TransactionDetailsInfo)
        Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(MerchantID, CMSContext.CurrentDocumentCulture.CultureCode)
        Dim LivePay_MerchantID As Integer = 0
        If dtMerc.Rows.Count > 0 Then
            LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
        End If

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(LivePay_MerchantID)
        Dim dt As DataTable = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim CustomDivTitle As New HtmlGenericControl("div")
            Dim CustomDivValue As New HtmlGenericControl("div")
            Dim CustomClear As New HtmlGenericControl("div")
            CustomDivTitle.Attributes.Add("class", "TransConfTitle")
            CustomDivValue.Attributes.Add("class", "TransConfValue")
            CustomClear.Attributes.Add("class", "Clear")
            CustomDivTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
            Select Case i
                Case 0
                    CustomDivValue.InnerHtml = TransInfo.info1
                Case 1
                    CustomDivValue.InnerHtml = TransInfo.info2
                Case 2
                    CustomDivValue.InnerHtml = TransInfo.info3
                Case 3
                    CustomDivValue.InnerHtml = TransInfo.info4
                Case 4
                    CustomDivValue.InnerHtml = TransInfo.info5
                Case 5
                    CustomDivValue.InnerHtml = TransInfo.info6
                Case 6
                    CustomDivValue.InnerHtml = TransInfo.info7
                Case 7
                    CustomDivValue.InnerHtml = TransInfo.info8
                Case 8
                    CustomDivValue.InnerHtml = TransInfo.info9
                Case 9
                    CustomDivValue.InnerHtml = TransInfo.info10
            End Select

            CustomFieldsPanel.Controls.Add(CustomDivTitle)
            CustomFieldsPanel.Controls.Add(CustomDivValue)
            CustomFieldsPanel.Controls.Add(CustomClear)
        Next
    End Sub

#End Region

    'Protected Sub btnSaveToPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSaveToPDF.Click
    '    If TransGUID <> "0" Then
    '        Dim TransactionID As String = "0"
    '        Dim myGuid As Guid = Nothing
    '        myGuid = New Guid(Me.TransGUID)
    '        Dim dt As DataTable = DBConnection.GetMerchantByGUID(myGuid)
    '        If dt.Rows.Count > 0 Then
    '            TransactionID = dt.Rows(0)("MerchantTransactionID").ToString
    '            Session("TransDetails_TransIDForExport") = TransactionID
    '            Response.Redirect("/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=MerchantTransDetails")
    '        End If
    '    End If

    'End Sub
End Class
