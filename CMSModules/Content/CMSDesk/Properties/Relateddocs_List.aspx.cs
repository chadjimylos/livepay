using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Staging;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Properties_Relateddocs_List : CMSPropertiesPage
{
    #region "Protected variables"

    protected int nodeId = 0;
    protected TreeNode node = null;
    protected TreeProvider tree = null;

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.RelatedDocs"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.RelatedDocs");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        UIContext.PropertyTab = PropertyTabEnum.RelatedDocs;

        UniGridRelationship.Columns = "LeftNodeID, RightNodeID, RelationshipNameID, LeftNodeName, RightNodeName, RelationshipDisplayName";
        UniGridRelationship.OnExternalDataBound += UniGridRelationship_OnExternalDataBound;
        UniGridRelationship.OnAction += UniGridRelationship_OnAction;
        UniGridRelationship.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        nodeId = QueryHelper.GetInteger("nodeid", 0);

        if (nodeId > 0)
        {
            // Get the node
            tree = new TreeProvider(CMSContext.CurrentUser);
            node = tree.SelectSingleNode(nodeId, CMSContext.PreferredCultureCode, tree.CombineWithDefaultCulture);

            if (node != null)
            {
                // Check read permissions
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                {
                    RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
                }
                // Check modify permissions
                else if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                {
                    UniGridRelationship.GridView.Enabled = false;
                    lnkNewRelationship.Enabled = false;
                    imgNewRelationship.Enabled = false;
                    lblInfo.Visible = true;
                    lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                }
                else
                {
                    lblInfo.Visible = false;
                }

                // Initialize unigrid
                UniGridRelationship.WhereCondition = "(LeftNodeID = " + nodeId + ") OR (RightNodeID = " + nodeId + ")";

                // Initialize controls
                lnkNewRelationship.NavigateUrl = "Relateddocs_Add.aspx?nodeid=" + nodeId;
                imgNewRelationship.ImageUrl = GetImageUrl("CMSModules/CMS_Content/Properties/addrelationship.png");
                imgNewRelationship.DisabledImageUrl = GetImageUrl("CMSModules/CMS_Content/Properties/addrelationshipdisabled.png");
            }
        }
    }


    /// <summary>
    /// Fires on the grid action
    /// </summary>
    void UniGridRelationship_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "delete")
        {
            string[] parameters = ((string)actionArgument).Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (parameters.Length == 3)
            {
                // Parse parameters
                int leftNodeId = ValidationHelper.GetInteger(parameters[0], 0);
                int rightNodeId = ValidationHelper.GetInteger(parameters[1], 0);
                int relationshipNameId = ValidationHelper.GetInteger(parameters[2], 0);

                // If parameters are valid
                if ((leftNodeId > 0) && (rightNodeId > 0) && (relationshipNameId > 0))
                {
                    // Remove relationship
                    RelationshipProvider.RemoveRelationship(leftNodeId, rightNodeId, relationshipNameId);

                    // Log synchronization
                    DocumentHelper.LogSynchronization(CMSContext.CurrentSiteName, node.NodeAliasPath, TaskTypeEnum.UpdateDocument, tree, SynchronizationInfoProvider.ENABLED_SERVERS, false);
                }
            }
        }
    }


    /// <summary>
    /// Binds the grid columns
    /// </summary>
    protected object UniGridRelationship_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "leftnodename":
            case "rightnodename":
                return HTMLHelper.HTMLEncode(DataHelper.GetNotEmpty(parameter, "/"));

            case "delete":
                if (!this.UniGridRelationship.GridView.Enabled)
                {
                    ImageButton btn = ((ImageButton)sender);
                    btn.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/UniGrid/Actions/", this.UniGridRelationship.IsLiveSite, true) + "Deletedisabled.png");
                }
                break;
        }

        return parameter;
    }

    #endregion
}