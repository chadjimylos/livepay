using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.UIControls;


public partial class CMSModules_Content_CMSDesk_MasterPage_PageEditHeader : CMSContentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentUserInfo user = CMSContext.CurrentUser;

        // Check UIProfile
        if (!user.IsAuthorizedPerUIElement("CMS.Content", "MasterPage"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "MasterPage");
        }

        // Check "Design" permission
        if (!user.IsAuthorizedPerResource("CMS.Content", "Design"))
        {
            RedirectToAccessDenied("CMS.Content", "Design");
        }

        // Register the scripts
        ScriptHelper.RegisterProgress(this.Page);
        
        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        
        // Save button
        CMS.skmMenuControl.MenuItem saveItem = null;

        saveItem = new CMS.skmMenuControl.MenuItem();
        saveItem.ToolTip = ResHelper.GetString("EditMenu.Save");
        saveItem.JavascriptCommand = "SaveMasterPage();";
        saveItem.ImageAltText = saveItem.ToolTip;
        saveItem.Text = ResHelper.GetString("general.save");
        saveItem.Image = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        saveItem.MouseOverImage = saveItem.Image;
        saveItem.MouseOverCssClass = "MenuItemEdit";
        saveItem.CssClass = "MenuItemEdit";
        menuElem.Items.Add(saveItem);

        this.menuElem.Layout = CMS.skmMenuControl.MenuLayout.Horizontal;
    }


    protected void lnkSave_Click(object sender, EventArgs e)
    {
    }
}
