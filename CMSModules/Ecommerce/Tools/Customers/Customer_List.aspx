<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="Customer_List.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Customers_Customer_List"
    Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Customers_List.xml" OrderBy="CustomerLastName"
        IsLiveSite="false" Columns="CustomerID, CustomerLastName, CustomerCompany, CustomerFirstName, CountryDisplayName, StateDisplayName, CustomerEmail, CustomerCreated, CustomerEnabled" />
</asp:Content>
