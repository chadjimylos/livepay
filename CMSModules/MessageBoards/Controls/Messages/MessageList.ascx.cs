using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.MessageBoard;
using CMS.SiteProvider;
using CMS.ExtendedControls;

public partial class CMSModules_MessageBoards_Controls_Messages_MessageList : CMSAdminListControl
{
    #region "Private variables"

    private int mBoardId = 0;
    private int mGroupId = 0;
    private string mEditPageUrl = "";
    protected string mPostBackRefference = "";
    string filterParams = "";
    private int siteId = 0;
    private bool mHideWhenGroupIsNotSupplied = false;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Determines whether to hide the content of the control when GroupID is not supplied.
    /// </summary>
    public bool HideWhenGroupIsNotSupplied
    {
        get
        {
            return this.mHideWhenGroupIsNotSupplied;
        }
        set
        {
            this.mHideWhenGroupIsNotSupplied = value;
        }
    }


    /// <summary>
    /// ID of the current board
    /// </summary>
    public int BoardID
    {
        get
        {
            return this.mBoardId;
        }
        set
        {
            this.mBoardId = value;
        }
    }


    /// <summary>
    /// ID of the current group
    /// </summary>
    public int GroupID
    {
        get
        {
            return this.mGroupId;
        }
        set
        {
            this.mGroupId = value;
            this.boardSelector.GroupID = value;
        }
    }


    /// <summary>
    /// Target URL for the modal dialog message is editied in
    /// </summary>
    public string EditPageUrl
    {
        get
        {
            return this.mEditPageUrl;
        }
        set
        {
            this.mEditPageUrl = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check if the group was supplied and hide control if necessary
        if ((this.GroupID == 0) && (this.HideWhenGroupIsNotSupplied))
        {
            this.Visible = false;
        }

        // If control should be hidden save view state memory
        if (this.StopProcessing || !this.Visible)
        {
            this.EnableViewState = false;
        }

        this.SetContext();

        // Initializes the controls
        SetupControls();

        this.ReleaseContext();
    }


    protected void btnRefreshHdn_Command(object sender, CommandEventArgs e)
    {
        PreselectFilter(Convert.ToString(e.CommandArgument));
        ReloadData();
    }


    #region "Private methods"

    /// <summary>
    /// Initializes the controls
    /// </summary>
    private void SetupControls()
    {
        this.btnFilter.Text = ResHelper.GetString("general.show");
        this.btnOk.Text = ResHelper.GetString("general.ok");

        this.lblSiteName.AssociatedControlClientID = this.siteSelector.DropDownSingleSelect.ClientID;
        this.lblBoardName.AssociatedControlClientID = this.boardSelector.DropDownSingleSelect.ClientID;

        gridElem.IsLiveSite = this.IsLiveSite;
        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        this.btnOk.OnClientClick += "return MassConfirm('" + this.drpActions.ClientID + "'," + ScriptHelper.GetString(ResHelper.GetString("General.ConfirmGlobalDelete")) + ");";

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        if (this.GroupID > 0)
        {
            // Hide site selection
            siteSelector.Visible = false;
            this.lblSiteName.Visible = false;
        }

        ReloadFilter();

        if (!RequestHelper.IsPostBack())
        {
            // Preselect filter data
            PreselectFilter(";;" + this.GroupID + ";;;NO;;");
        }

        if (this.BoardID > 0)
        {
            // Hide board selection
            this.plcBoard.Visible = false;

            // Hide site selection
            this.pnlSite.Visible = false;

            if ((this.GroupID > 0) && this.IsLiveSite)
            {
                InitializeGroupNewMessage();
            }
        }

        siteSelector.UniSelector.OnSelectionChanged += new EventHandler(UniSelector_OnSelectionChanged);

        // Reload message list script        
        string board = (this.BoardID > 0 ? this.BoardID.ToString() : this.boardSelector.Value.ToString());
        string group = this.GroupID.ToString();
        string user = HTMLHelper.HTMLEncode(this.txtUserName.Text);
        string message = HTMLHelper.HTMLEncode(this.txtMessage.Text);
        string approved = this.drpApproved.SelectedItem.Value;
        string spam = this.drpSpam.SelectedItem.Value;
        bool changemaster = QueryHelper.GetBoolean("changemaster", false);

        // Set site selector
        siteSelector.DropDownSingleSelect.AutoPostBack = true;
        siteSelector.AllowAll = true;
        siteSelector.IsLiveSite = this.IsLiveSite;
        //siteSelector.UniSelector.OnSelectionChanged += new EventHandler(UniSelector_OnSelectionChanged);

        boardSelector.IsLiveSite = this.IsLiveSite;
        boardSelector.GroupID = this.GroupID;

        siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
        if (siteId == 0)
        {
            siteId = CMSContext.CurrentSiteID;
            siteSelector.Value = siteId;
        }

        // Generate filter selection parameters
        filterParams = "&site=" + siteId + "&board=" + board + "&groupid=" + group + "&username=" + user + "&message=" + message +
            "&approved=" + approved + "&isspam=" + spam + "&changemaster=" + changemaster;
        filterParams = filterParams.Replace("\'", "%27");

        string cmdArg = siteId + ";" + board + ";" + group + ";" + user.Replace(";", "#sc#") + ";" +
            message.Replace(";", "#sc#") + ";" + approved + ";" + spam + ";" + changemaster;

        this.btnRefreshHdn.CommandArgument = cmdArg;
        this.mPostBackRefference = ControlsHelper.GetPostBackEventReference(this.btnRefreshHdn, null);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "RefreshBoardList", ScriptHelper.GetScript("function RefreshBoardList(filterParams){" +
            mPostBackRefference + "}"));

        this.siteSelector.OnlyRunningSites = this.IsLiveSite;

        string where = "";

        // Sites dropdownlist
        if (siteId > 0)
        {
            where += "BoardSiteId = " + siteId + " AND";
        }
        // Approved dropdownlist
        if (drpApproved.SelectedIndex > 0)
        {
            switch (drpApproved.SelectedValue)
            {
                case "YES":
                    where += " MessageApproved = 1 AND";
                    break;

                case "NO":
                    where += " MessageApproved = 0 AND";
                    break;
            }
        }
        // Spam dropdownlist
        if (drpSpam.SelectedIndex > 0)
        {
            switch (drpSpam.SelectedValue)
            {
                case "YES":
                    where += " MessageIsSpam = 1 AND";
                    break;

                case "NO":
                    where += " MessageIsSpam = 0 AND";
                    break;
            }
        }
        int selectedBoardId = 0;
        if (mBoardId > 0)
        {
            where += " MessageBoardID = " + mBoardId.ToString() + " AND";
            selectedBoardId = mBoardId;
        }
        else
        {
            // Board dropdownlist
            selectedBoardId = ValidationHelper.GetInteger(boardSelector.Value, 0);
            if (selectedBoardId > 0)
            {
                where += " MessageBoardID = " + selectedBoardId + " AND";
            }
        }
        if (txtUserName.Text.Trim() != "")
        {
            where += " MessageUserName LIKE '%" + txtUserName.Text.Trim().Replace("'", "''") + "%' AND";
        }
        if (txtMessage.Text.Trim() != "")
        {
            where += " MessageText LIKE '%" + txtMessage.Text.Trim().Replace("'", "''") + "%' AND";
        }
        bool isAuthrorized = false;
        if (selectedBoardId > 0)
        {
            BoardInfo selectedBoard = BoardInfoProvider.GetBoardInfo(selectedBoardId);
            if (selectedBoard != null)
            {
                isAuthrorized = BoardInfoProvider.IsUserAuthorizedToManageMessages(selectedBoard);
            }
        }
        if (!isAuthrorized && (!(CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.MessageBoards", "Modify") || CMSContext.CurrentUser.IsGroupAdministrator(this.mGroupId))))
        {
            where += " EXISTS( SELECT * FROM Board_Moderator WHERE Board_Moderator.UserID = " + CMSContext.CurrentUser.UserID + ") AND";
        }
        if (this.mGroupId > 0)
        {
            where += " BoardGroupID =" + this.mGroupId + " AND";
        }
        else
        {
            where += "(BoardGroupID =0 OR BoardGroupID IS NULL) AND";
        }
        if (where != "")
        {
            where = where.Remove(where.Length - 4); // 4 = " AND".Length
        }
        gridElem.WhereCondition = where;
    }


    /// <summary>
    /// Initializes New message action for group message board
    /// </summary>
    private void InitializeGroupNewMessage()
    {
        this.plcNewMessageGroups.Visible = true;

        // New message link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Board.MessageList.NewMessage");
        actions[0, 2] = "modalDialog('" + CMSContext.ResolveDialogUrl("~/CMSModules/Groups/CMSPages/Message_Edit.aspx") + "?boardId=" + this.BoardID + "&groupid=" + this.GroupID + "&changemaster=" + QueryHelper.GetBoolean("changemaster", false) + "', 'Message Edit', 415, 400); return false;";
        actions[0, 3] = "#";
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Board_Message/add.png");

        this.headerActions.Actions = actions;
    }


    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        this.boardSelector.SiteID = ValidationHelper.GetInteger(this.siteSelector.Value, 0);
        this.boardSelector.ReloadData(true);

        ReloadData();
    }


    /// <summary>
    /// Gets the information on last selected filter configuration and pre-selects the actual values
    /// </summary>
    private void PreselectFilter(string filter)
    {
        string site = "";
        string board = "";
        string username = "";
        string message = "";
        string approved = "";
        string isspam = "";

        string filterParams = HttpUtility.HtmlDecode(filter);

        if (string.IsNullOrEmpty(filterParams))
        {
            // Get filter values from the query string by default 
            // or ViewState if the control is used as part of GroupProfile on Live site
            site = QueryHelper.GetString("site", "");
            board = QueryHelper.GetString("board", "");
            username = QueryHelper.GetString("username", "");
            message = QueryHelper.GetString("message", "");
            approved = QueryHelper.GetString("approved", "");
            isspam = QueryHelper.GetString("isspam", "");

            this.GroupID = (this.GroupID == 0) ? QueryHelper.GetInteger("groupid", 0) : this.GroupID;
        }
        else
        {
            string[] paramsArr = filterParams.Split(';');
            site = paramsArr[0];
            board = paramsArr[1];
            username = paramsArr[3].Replace("#sc#", ";");
            message = paramsArr[4].Replace("#sc#", ";");
            approved = paramsArr[5];
            isspam = paramsArr[6];

            this.GroupID = ValidationHelper.GetInteger(paramsArr[2], 0);
        }

        if (site != "")
        {
            siteId = ValidationHelper.GetInteger(site, 0);
            siteSelector.Value = siteId;
        }
        else
        {
            if (this.BoardID == 0)
            {
                siteId = CMSContext.CurrentSiteID;
                siteSelector.Value = siteId;
            }
        }

        if (board != "")
        {
            if (this.boardSelector.UniSelector.HasData)
            {
                this.boardSelector.Value = board;
            }
        }

        if (username != "")
        {
            this.txtUserName.Text = username;
        }

        if (message != "")
        {
            this.txtMessage.Text = message;
        }

        if (approved != "")
        {
            if (this.drpApproved.Items.Count > 0)
            {
                this.drpApproved.SelectedValue = approved;
            }
        }

        if (isspam != "")
        {
            if (this.drpSpam.Items.Count > 0)
            {
                this.drpSpam.SelectedValue = isspam;
            }
        }
    }


    /// <summary>
    /// Load data according to filter setings
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        this.gridElem.ReloadData();
    }

    #endregion


    #region "Public methods"

    /// <summary>
    /// Sets the filter default values
    /// </summary>
    public void ReloadFilter()
    {
        if (this.BoardID == 0)
        {
            this.boardSelector.IsLiveSite = this.IsLiveSite;
            this.boardSelector.SiteID = CMSContext.CurrentSiteID;
            this.boardSelector.GroupID = this.GroupID;
            this.boardSelector.ReloadData(false);
        }

        if (this.drpApproved.Items.Count == 0)
        {
            drpApproved.Items.Add(new ListItem(ResHelper.GetString("general.selectall"), "ALL"));
            drpApproved.Items.Add(new ListItem(ResHelper.GetString("general.yes"), "YES"));
            drpApproved.Items.Add(new ListItem(ResHelper.GetString("general.no"), "NO"));
            this.drpApproved.SelectedIndex = (this.BoardID > 0) ? 0 : 2;
        }

        if (this.drpSpam.Items.Count == 0)
        {
            drpSpam.Items.Add(new ListItem(ResHelper.GetString("general.selectall"), "ALL"));
            drpSpam.Items.Add(new ListItem(ResHelper.GetString("general.yes"), "YES"));
            drpSpam.Items.Add(new ListItem(ResHelper.GetString("general.no"), "NO"));
            this.drpSpam.SelectedIndex = 0;
        }

        if (CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            this.pnlSite.Visible = true;
        }

        if (this.drpActions.Items.Count == 0)
        {
            drpActions.Items.Add(new ListItem(ResHelper.GetString("Board.MessageList.Action.Select"), "SELECT"));
            drpActions.Items.Add(new ListItem(ResHelper.GetString("Board.MessageList.Action.Approve"), "APPROVE"));
            drpActions.Items.Add(new ListItem(ResHelper.GetString("Board.MessageList.Action.Reject"), "REJECT"));
            drpActions.Items.Add(new ListItem(ResHelper.GetString("Board.MessageList.Action.Spam"), "SPAM"));
            drpActions.Items.Add(new ListItem(ResHelper.GetString("Board.MessageList.Action.NoSpam"), "NOSPAM"));
            drpActions.Items.Add(new ListItem(ResHelper.GetString("Board.MessageList.Action.Delete"), "DELETE"));
        }
    }

    #endregion


    #region "Event handlers"

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (DataHelper.DataSourceIsEmpty(gridElem.GridView.DataSource))
        {
            this.lblActions.Visible = false;
            this.drpActions.Visible = false;
            this.btnOk.Visible = false;
        }
        else
        {
            // Hide column containing board name when reviewing specific board
            if (this.BoardID > 0)
            {
                this.gridElem.GridView.Columns[6].Visible = false;
            }

            this.gridElem.ReloadData();

            this.lblActions.Visible = true;
            this.drpActions.Visible = true;
            this.btnOk.Visible = true;
        }
    }


    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        bool approve = false;
        switch (sourceName.ToLower())
        {
            case "messageapproved":
                return UniGridFunctions.ColoredSpanYesNo(parameter);

            case "messageisspam":
                return UniGridFunctions.ColoredSpanYesNoReversed(parameter);

            case "messagetext":
                string text = parameter.ToString();
                if (text.Length > 30)
                {
                    text = text.Substring(0, 30) + "...";
                }
                return HTMLHelper.HTMLEncode(text);

            case "messagetooltip":
                return HTMLHelper.HTMLEncodeLineBreaks(parameter.ToString());

            case "edit":
                ImageButton editButton = ((ImageButton)sender);

                string url = "~/CMSModules/MessageBoards/Tools/Messages/Message_Edit.aspx";
                if (this.IsLiveSite)
                {
                    url = "~/CMSModules/MessageBoards/CMSPages/Message_Edit.aspx";
                }

                editButton.OnClientClick = "modalDialog('" + CMSContext.ResolveDialogUrl(((this.EditPageUrl == "") ? url : this.EditPageUrl)) +
                    "?boardId=" + this.mBoardId + "&messageId=" + editButton.CommandArgument + filterParams + "', 'Message Edit', 415, 400); return false;";
                break;

            case "approve":
                approve = ValidationHelper.GetBoolean(((DataRowView)((GridViewRow)parameter).DataItem).Row["MessageApproved"], false);
                if (!approve)
                {
                    ImageButton button = ((ImageButton)sender);
                    button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Approve.png");
                    button.ToolTip = ResHelper.GetString("general.approve");
                }
                else
                {
                    ImageButton button = ((ImageButton)sender);
                    button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Reject.png");
                    button.ToolTip = ResHelper.GetString("general.reject");
                }
                break;

            case "messageinserted":
                return CMSContext.ConvertDateTime(ValidationHelper.GetDateTime(parameter, DataHelper.DATETIME_NOT_SELECTED), this).ToString();

        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        BoardMessageInfo message = BoardMessageInfoProvider.GetBoardMessageInfo(Convert.ToInt32(actionArgument));
        string[] argument = null;

        switch (actionName)
        {
            case "delete":
            case "approve":
                if (!CheckPermissions("cms.messageboards", CMSAdminControl.PERMISSION_MODIFY))
                {
                    return;
                }
                break;
        }

        switch (actionName)
        {
            case "delete":
                if (message != null)
                {
                    BoardMessageInfoProvider.DeleteBoardMessageInfo(message);
                }
                break;

            case "approve":
                if (message != null)
                {
                    if (message.MessageApproved)
                    {
                        // Reject message
                        message.MessageApproved = false;
                        message.MessageApprovedByUserID = 0;
                    }
                    else
                    {
                        // Approve message
                        message.MessageApproved = true;
                        message.MessageApprovedByUserID = CMSContext.CurrentUser.UserID;
                    }
                    BoardMessageInfoProvider.SetBoardMessageInfo(message);
                }
                break;

            default:
                break;
        }

        this.RaiseOnAction(actionName, ((argument == null) ? actionArgument : argument));
    }


    protected void btnOk_Clicked(object sender, EventArgs e)
    {
        if (!CheckPermissions("cms.messageboards", CMSAdminControl.PERMISSION_MODIFY))
        {
            return;
        }

        if (drpActions.SelectedValue != "SELECT")
        {
            ArrayList list = gridElem.SelectedItems;
            if (list.Count > 0)
            {
                foreach (string messageId in list)
                {
                    BoardMessageInfo message = BoardMessageInfoProvider.GetBoardMessageInfo(Convert.ToInt32(messageId));
                    switch (drpActions.SelectedValue)
                    {
                        case "DELETE":
                            BoardMessageInfoProvider.DeleteBoardMessageInfo(message);
                            break;

                        case "APPROVE":
                            if (!message.MessageApproved)
                            {
                                message.MessageApproved = true;
                                message.MessageApprovedByUserID = CMSContext.CurrentUser.UserID;
                                BoardMessageInfoProvider.SetBoardMessageInfo(message);
                            }
                            break;

                        case "REJECT":
                            if (message.MessageApproved)
                            {
                                message.MessageApproved = false;
                                message.MessageApprovedByUserID = 0;
                                BoardMessageInfoProvider.SetBoardMessageInfo(message);
                            }
                            break;

                        case "SPAM":
                            if (!message.MessageIsSpam)
                            {
                                message.MessageIsSpam = true;
                                BoardMessageInfoProvider.SetBoardMessageInfo(message);
                            }
                            break;

                        case "NOSPAM":
                            if (message.MessageIsSpam)
                            {
                                message.MessageIsSpam = false;
                                BoardMessageInfoProvider.SetBoardMessageInfo(message);
                            }
                            break;
                    }
                }
            }
            else
            {
                ltlScript.Text += ScriptHelper.GetAlertScript(ResHelper.GetString("general.noitems"));
            }
        }
        gridElem.ResetSelection();
    }

    #endregion
}
