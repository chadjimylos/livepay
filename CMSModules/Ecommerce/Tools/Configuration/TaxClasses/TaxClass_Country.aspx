<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TaxClass_Country.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_TaxClasses_TaxClass_Country"
    Theme="Default" EnableEventValidation="false" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <asp:Panel runat="server" ID="pnlMenu" CssClass="TabsEditMenu" EnableViewState="false">
        <table>
            <tr>
                <td>
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="MenuItemEdit" OnClick="btnSave_Click"
                        EnableViewState="false">
                        <asp:Image ID="imgSave" runat="server" EnableViewState="false" />
                        <% =mSave %>
                    </asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <asp:GridView ID="GridViewCountries" runat="server" AutoGenerateColumns="false" OnDataBound="GridViewCountries_DataBound"
            CellPadding="3" CssClass="UniGridGrid" Width="50%" EnableViewState="false">
            <HeaderStyle HorizontalAlign="Left" CssClass="UniGridHead" Wrap="false" />
            <AlternatingRowStyle CssClass="OddRow" />
            <RowStyle CssClass="EvenRow" />
            <Columns>
                <asp:TemplateField>
                    <ItemStyle Wrap="false" />
                    <HeaderStyle Wrap="false" />
                    <ItemTemplate>
                        <asp:Label ID="lblName" runat="server" Text='<%# HTMLHelper.HTMLEncode(Eval("CountryDisplayName").ToString()) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:TextBox ID="txtTaxValue" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TaxValue") %>'
                            MaxLength="10" CssClass="ShortTextBox" OnTextChanged="txtTaxValue_Changed" EnableViewState="false"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:CheckBox ID="chkIsFlatValue" runat="server" Checked='<%# ConvertToBoolean(DataBinder.Eval(Container.DataItem, "IsFlatValue")) %>'
                            OnCheckedChanged="chkIsFlatValue_Changed" EnableViewState="false" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CountryID">
                    <ItemStyle />
                </asp:BoundField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
