﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SubscriberFilter.ascx.cs"
    Inherits="CMSModules_Newsletters_Controls_SubscriberFilter" %>
<asp:Panel runat="server" ID="pnlSearch">
    <asp:DropDownList ID="filter" runat="server" CssClass="ContentDropdown" />
    <asp:TextBox ID="txtEmail" runat="server" style="margin-left:1px" />
</asp:Panel>
