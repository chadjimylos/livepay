<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectCssStylesheet.ascx.cs"
    Inherits="CMSFormControls_SelectCssStylesheet" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ObjectType="cms.cssstylesheet" SelectionMode="SingleDropDownList"
            ReturnColumnName="StylesheetName" OrderBy="StylesheetDisplayName" ResourcePrefix="cssselect"
            AllowEmpty="false" runat="server" ID="usStyleSheet" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
