<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomTableDataList.ascx.cs"
    Inherits="CMSModules_CustomTables_Controls_CustomTableDataList" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<asp:Label ID="lblError" runat="server" Visible="false" CssClass="ErrorLabel" EnableViewState="false" />
<cms:UniGrid runat="server" ID="gridData" GridName="~/CMSModules/CustomTables/Controls/CustomTableDataList.xml" OrderBy="ItemID" />
<asp:Literal ID="ltlScript" runat="server" />