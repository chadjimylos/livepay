﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCNewPay.ascx.vb" Inherits="CMSTemplates_LivePay_UCNewPay" %>
<script language="javascript" type="text/javascript" >
    function RebindRpt(CachValue) {
        setTimeout("ExecSearch('" + CachValue + "')", 500)
    }

    function ExecSearch(CachValue) {
        var RealValue = document.getElementById('<%=txtSearch.ClientID %>').value
        if (RealValue == CachValue) {
            var text = document.getElementById('<%=txtSearch.clientID %>');
            if (text.value != null && text.value.length > 0) {

                GetHomeMerchantData(RealValue)

            } else {
                ShowHideQuickRes(false, 0)
            }
        }
    }

    function GetHomeMerchantData(val) {
        var x;
        if (window.ActiveXObject) {
            x = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else {
            x = new XMLHttpRequest();
        }
        val = escape(val)
        
        x.open("GET", "/CMSTemplates/LivePay/xmlHttpMerchants.aspx?MerchantKeyWord=" + val + "&RecordsNo=3&FormPage=home", true);

        x.onreadystatechange = function () {
            if (x.readyState == 4) {
               
                var Results = x.responseText;
                var RowCount = Results.split("#]")[0]
                Results = Results.split("#]")[1]
                if (Results.length > 0) {
                    document.getElementById('QuickSrcResAll').innerHTML = Results;
                    ShowHideQuickRes(true, RowCount);
                } else {
                    ShowHideQuickRes(false, 0);
                }

            }
        }
        x.send(null);
    }

    function SetFocus() {
        var elem = document.getElementById('<%=txtSearch.clientID %>');
        var caretPos = elem.value.length
        if (elem != null) {
            if (elem.createTextRange) {
                var range = elem.createTextRange();
                range.move('character', caretPos);
                range.select();
            }
            else {
                elem.setSelectionRange(caretPos, caretPos);
                elem.focus();
                // Workaround for FF overflow no scroll problem 
                // Trigger a "space" keypress. 
                var evt = document.createEvent("KeyboardEvent");
                evt.initKeyEvent("keypress", true, true, null, false, false, false, false, 0, 32);
                elem.dispatchEvent(evt);
                // Trigger a "backspace" keypress. 
                evt = document.createEvent("KeyboardEvent");
                evt.initKeyEvent("keypress", true, true, null, false, false, false, false, 8, 0);
                elem.dispatchEvent(evt);

            }
        }
    }


    function ShowHideQuickRes(IsVisible, RowCount) {
        document.getElementById('ViewMoreResults').style.display = 'none';
        if (IsVisible) {
            document.getElementById('QuickResDiv').style.display = '';
            if (RowCount > 2) {
                document.getElementById('ViewMoreResults').style.display = '';
                var val = document.getElementById('<%=txtSearch.clientID %>').value
                document.getElementById('lnkViewMoreResults').href = 'Search.aspx?searchtext=' + val;
            }
        }
        else {
            document.getElementById('QuickResDiv').style.display = 'none';
        }
    }
</script>

<div class="SearchBG">
    <div class="SearchLbl"><%= ResHelper.LocalizeString("{$=Νέα πληρωμή|en-us=New Payment$}")%></div>
    <div class="SearchTxtDiv"><asp:TextBox BorderWidth="0" BackColor="Transparent" cssClass="SearchTxt" autocomplete="off" ID="txtSearch" onkeyup="RebindRpt(this.value);" runat="server" ></asp:TextBox></div>
    <div class="SearchBTN"><asp:ImageButton ImageUrl="/app_themes/LivePay/btnSearchOk.gif" runat="server" ID="btnSearch" /></div>
    <div class="Clear"></div>
    <div class="QuickSrcResTopMainNewPay" id="QuickResDiv" style="display:none">
        <div class="QuickSrcResMain">
            <div class="QuickSrcResAll" id="QuickSrcResAll">
           
            </div>
            <div class="QuickSrcResViewMore" id="ViewMoreResults" style="display:none">
                <div class="QuickSrcResTitleUnder"><a href="#" id="lnkViewMoreResults">» <%=ResHelper.LocalizeString("{$=Δείτε Περισσότερα Αποτελέσματα|en-us=View more results$}") %></a></div>
                <div class="QuickSrcResTitleSmall"><%= ResHelper.LocalizeString("{$=Εμφανίζονται τα πρώτα -3- αποτελέσματα|en-us=First - 3 - results en $}")%></div>
            </div> 
        </div>
    </div>
  </div>
  
