using System;

using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_CssStylesheets_CssStylesheet_New : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblCssStylesheetDisplayName.Text = ResHelper.GetString("CssStylesheet_General.DisplayName");
        lblCssStylesheetName.Text = ResHelper.GetString("CssStylesheet_General.Name");
        lblCssStylesheetText.Text = ResHelper.GetString("CssStylesheet_General.Text");

        btnOk.Text = ResHelper.GetString("general.ok");

        // check 'Assign with current web site' check box
        if ((CMSContext.CurrentSite != null) && (!RequestHelper.IsPostBack()))
        {
            chkAssign.Text = ResHelper.GetString("General.AssignWithWebSite") + " " + CMSContext.CurrentSite.DisplayName;
            chkAssign.Checked = true;
            chkAssign.Visible = true;
        }

        rfvDisplayName.ErrorMessage = ResHelper.GetString("CssStylesheet_New.EmptyDisplayName");
        rfvName.ErrorMessage = ResHelper.GetString("CssStylesheet_New.EmptyName");
        rfvText.ErrorMessage = ResHelper.GetString("CssStylesheet_New.EmptyText");

        string cssStylesheets = ResHelper.GetString("CssStylesheet.CssStylesheets");
        string currentCssStylesheet = ResHelper.GetString("CssStylesheet.NewCssStylesheet");
        string title = ResHelper.GetString("CssStylesheet.TitleNew");

        const string cssStylesheetUrl = "~/CMSSiteManager/Development/CssStylesheets/CssStylesheet_List.aspx";

        // initializes page title
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = cssStylesheets;
        pageTitleTabs[0, 1] = cssStylesheetUrl;        
        pageTitleTabs[1, 0] = currentCssStylesheet;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = title;
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_CSSStylesheet/new.png");
        this.CurrentMaster.Title.HelpTopicName = "new_sheet";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        // finds whether required fields are not empty
        string result = new Validator().NotEmpty(txtDisplayName.Text, ResHelper.GetString("CssStylesheet_New.EmptyDisplayName")).NotEmpty(txtName.Text, ResHelper.GetString("CssStylesheet_New.EmptyName")).NotEmpty(txtText.Text, ResHelper.GetString("CssStylesheet_New.EmptyText"))
            .IsCodeName(txtName.Text, ResHelper.GetString("general.invalidcodename"))
            .Result;

        if (result == "")
        {
            try
            {
                int cssStylesheetId = SaveNewCssStylesheet();

                if (cssStylesheetId > 0)
                {
                    UrlHelper.Redirect("CssStylesheet_Edit.aspx?cssstylesheetid=" + cssStylesheetId + "&saved=1");
                }
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message.Replace("%%name%%",txtName.Text);
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }


    protected int SaveNewCssStylesheet()
    {
        CssStylesheetInfo cs = new CssStylesheetInfo();
        cs.StylesheetDisplayName = txtDisplayName.Text;
        cs.StylesheetName = txtName.Text;
        cs.StylesheetText = txtText.Text;

        CssStylesheetInfoProvider.SetCssStylesheetInfo(cs);

        if ((chkAssign.Visible) && (chkAssign.Checked) && (CMSContext.CurrentSite != null) && (cs.StylesheetID > 0))
        {
            // Add new stylesheet to the actual site
            CssStylesheetSiteInfoProvider.AddCssStylesheetToSite(cs.StylesheetID, CMSContext.CurrentSite.SiteID);
        }

        return cs.StylesheetID;
    }
}
