﻿<%@ Page Language="C#" Theme="Default" AutoEventWireup="true" CodeFile="Content.aspx.cs"
    Inherits="CMSFormControls_Selectors_SelectFileOrFolder_Content" EnableEventValidation="false"  %>

<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/FileSystemSelector/FileSystemSelector.ascx"
    TagName="FileSystem" TagPrefix="uc1" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Insert filesystem path</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
        .ImageExtraClass
        {
            position: absolute;
        }
        .ImageTooltip
        {
            border: 1px solid #ccc;
            background-color: #fff;
            padding: 3px;
            display: block;
        }
    </style>
</head>
<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptManager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="pnlUpdateSelectMedia" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:FileSystem ID="fileSystem" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    </form>
</body>
</html>
