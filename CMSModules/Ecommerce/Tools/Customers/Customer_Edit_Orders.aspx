<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Customer_Edit_Orders.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Ecommerce_Tools_Customers_Customer_Edit_Orders"
    Theme="Default" Title="Ecommerce - Customer order list" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Order_List.xml" OrderBy="OrderID"
        IsLiveSite="false" />
</asp:Content>
