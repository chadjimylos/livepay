CREATE TABLE [Media_LibraryRolePermission] (
		[LibraryID]        int NOT NULL,
		[RoleID]           int NOT NULL,
		[PermissionID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Media_LibraryRolePermission]
	ADD
	CONSTRAINT [PK_Media_LibraryRolePermission]
	PRIMARY KEY
	([LibraryID], [RoleID], [PermissionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Media_LibraryRolePermission]') IS NOT NULL
	AND OBJECT_ID(N'[Media_Library]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Media_LibraryRolePermission_LibraryID_Media_Library]') IS NULL
BEGIN
		ALTER TABLE [Media_LibraryRolePermission]
			ADD CONSTRAINT [FK_Media_LibraryRolePermission_LibraryID_Media_Library]
			FOREIGN KEY ([LibraryID]) REFERENCES [Media_Library] ([LibraryID])
END
IF OBJECT_ID(N'[Media_LibraryRolePermission]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Permission]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Media_LibraryRolePermission_PermissionID_CMS_Permission]') IS NULL
BEGIN
		ALTER TABLE [Media_LibraryRolePermission]
			ADD CONSTRAINT [FK_Media_LibraryRolePermission_PermissionID_CMS_Permission]
			FOREIGN KEY ([PermissionID]) REFERENCES [CMS_Permission] ([PermissionID])
END
IF OBJECT_ID(N'[Media_LibraryRolePermission]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Media_LibraryRolePermission_RoleID_CMS_Role]') IS NULL
BEGIN
		ALTER TABLE [Media_LibraryRolePermission]
			ADD CONSTRAINT [FK_Media_LibraryRolePermission_RoleID_CMS_Role]
			FOREIGN KEY ([RoleID]) REFERENCES [CMS_Role] ([RoleID])
END
