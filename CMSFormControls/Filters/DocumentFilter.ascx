<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocumentFilter.ascx.cs"
    Inherits="CMSFormControls_Filters_DocumentFilter" %>
<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/Filters/ClassNameFilter.ascx" TagName="ClassNameFilter"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/Filters/DocumentNameFilter.ascx"
    TagName="DocumentNameFilter" TagPrefix="cms" %>
<table>
    <asp:PlaceHolder ID="plcSites" runat="server">
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblSites" runat="server" DisplayColon="true" ResourceString="general.site" />
            </td>
            <td>
                <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" OnlyRunningSites="false" UseCodeNameForSelection="true" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcPath" runat="server">
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblName" runat="server" DisplayColon="true" ResourceString="general.documentname" />
            </td>
            <td>
                <cms:DocumentNameFilter ID="nameFilter" runat="server" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcClass" runat="server">
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblClass" runat="server" DisplayColon="true" ResourceString="general.documenttype" />
            </td>
            <td>
                <cms:ClassNameFilter ID="classFilter" runat="server" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td>
        </td>
        <td>
            <cms:LocalizedButton ID="btnShow" runat="server" ResourceString="general.show" CssClass="ContentButton" />
        </td>
    </tr>
</table>
