using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.PortalControls;
using CMS.WorkflowEngine;
using CMS.PortalEngine;
using CMS.Controls;
using CMS.SiteProvider;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSWebParts_Events_EventCalendar : CMSAbstractWebPart
{
    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether permissions are checked
    /// </summary>
    public bool CheckPermissions
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("CheckPermissions"), this.calItems.CheckPermissions);
        }
        set
        {
            SetValue("CheckPermissions", value);
            this.calItems.CheckPermissions = value;
        }
    }


    /// <summary>
    /// Get or sets the class names
    /// </summary>
    public string ClassNames
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("ClassNames"), this.calItems.ClassNames), this.calItems.ClassNames);
        }
        set
        {
            SetValue("ClassNames", value);
            this.calItems.ClassNames = value;
        }
    }


    /// <summary>
    ///  Gets or sets the value that indicates whether data are combined with default control
    /// </summary>
    public bool CombineWithDefaultCulture
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("CombineWithDefaultCulture"), this.calItems.CombineWithDefaultCulture);
        }
        set
        {
            SetValue("CombineWithDefaultCulture", value);
            this.calItems.CombineWithDefaultCulture = value;
        }
    }


    /// <summary>
    /// Gets or sets the culture code.
    /// </summary>
    public string CultureCode
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("CultureCode"), this.calItems.CultureCode), this.calItems.CultureCode);
        }
        set
        {
            SetValue("CultureCode", value);
            this.calItems.CultureCode = value;
        }
    }


    /// <summary>
    ///  Gets or sets the max. relative level
    /// </summary>
    public int MaxRelativeLevel
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("MaxRelativeLevel"), this.calItems.MaxRelativeLevel);
        }
        set
        {
            SetValue("MaxRelativeLevel", value);
            this.calItems.MaxRelativeLevel = value;
        }
    }


    /// <summary>
    ///  Gets or sets the order by value
    /// </summary>
    public string OrderBy
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("OrderBy"), this.calItems.OrderBy), this.calItems.OrderBy);
        }
        set
        {
            SetValue("OrderBy", value);
            this.calItems.OrderBy = value;
        }
    }


    /// <summary>
    /// Gets or sets the path
    /// </summary>
    public string Path
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("Path"), this.calItems.Path), this.calItems.Path);
        }
        set
        {
            SetValue("Path", value);
            this.calItems.Path = value;
        }
    }


    /// <summary>
    ///  Gets or sets the value that indicates whether related node is on the lef side
    /// </summary>
    public bool RelatedNodeIsOnTheLeftSide
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("RelatedNodeIsOnTheLeftSide"), this.calItems.RelatedNodeIsOnTheLeftSide);
        }
        set
        {
            SetValue("RelatedNodeIsOnTheLeftSide", value);
            this.calItems.RelatedNodeIsOnTheLeftSide = value;
        }
    }


    /// <summary>
    ///  Gets or sets the relationship name
    /// </summary>
    public string RelationshipName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("RelationshipName"), this.calItems.RelationshipName), this.calItems.RelationshipName);
        }
        set
        {
            SetValue("RelationshipName", value);
            this.calItems.RelationshipName = value;
        }
    }


    /// <summary>
    ///  Gets or sets the relationShip with node guid
    /// </summary>
    public Guid RelationshipWithNodeGuid
    {
        get
        {
            return ValidationHelper.GetGuid(this.GetValue("RelationshipWithNodeGuid"), this.calItems.RelationshipWithNodeGuid);
        }
        set
        {
            SetValue("RelationshipWithNodeGuid", value);
            this.calItems.RelationshipWithNodeGuid = value;
        }
    }


    /// <summary>
    ///  Gets or sets the value that indicates whether selected documents must be only published
    /// </summary>
    public bool SelectOnlyPublished
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("SelectOnlyPublished"), this.calItems.SelectOnlyPublished);
        }
        set
        {
            SetValue("SelectOnlyPublished", value);
            this.calItems.SelectOnlyPublished = value;
        }
    }


    /// <summary>
    ///  Gets or sets the site name
    /// </summary>
    public string SiteName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("SiteName"), this.calItems.SiteName), this.calItems.SiteName);
        }
        set
        {
            SetValue("SiteName", value);
            this.calItems.SiteName = value;
        }
    }


    /// <summary>
    ///  Gets or sets the name of the transforamtion which is used for displaying the results
    /// </summary>
    public string TransformationName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("TransformationName"), "cms.bookingevent.EventCalendarItem"), "cms.bookingevent.EventCalendarItem");
        }
        set
        {
            SetValue("TransformationName", value);
            this.calItems.TransformationName = value;
        }
    }


    /// <summary>
    ///  Gets or sets the where condition.
    /// </summary>
    public string WhereCondition
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("WhereCondition"), this.calItems.WhereCondition), this.calItems.WhereCondition);
        }
        set
        {
            SetValue("WhereCondition", value);
            this.calItems.WhereCondition = value;
        }
    }


    /// <summary>
    ///  Gets or sets the day field
    /// </summary>
    public string DayField
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("DayField"), "EventDate"), "EventDate");
        }
        set
        {
            SetValue("DayField", value);
            this.calItems.DayField = value;
        }
    }


    /// <summary>
    ///  Gets or sets the no event transformation name
    /// </summary>
    public string NoEventTransformationName
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("NoEventTransformationName"), "cms.bookingevent.calendarnoevent");
        }
        set
        {
            SetValue("NoEventTransformationName", value);
            this.calItems.NoEventTransformationName = value;
        }
    }


    /// <summary>
    /// Gets or sets the SkinID which should be used
    /// </summary>
    public override string SkinID
    {
        get
        {
            return base.SkinID;
        }
        set
        {
            base.SkinID = value;
            if ((this.calItems != null) && (this.PageCycle < PageCycleEnum.Initialized))
            {
                this.calItems.SkinID = this.SkinID;
            }
        }
    }


    /// <summary>
    /// Calendar control
    /// </summary>
    public CMSCalendar CalendarControl
    {
        get
        {
            return this.calItems;
        }
    }


    /// <summary>
    /// Filter name.
    /// </summary>
    public string FilterName
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("FilterName"), calItems.FilterName);
        }
        set
        {
            this.SetValue("FilterName", value);
            this.calItems.FilterName = value;
        }
    }


    /// <summary>
    /// Gets or sets the cache minutes
    /// </summary>
    public override int CacheMinutes
    {
        get
        {
            return base.CacheMinutes;
        }
        set
        {
            base.CacheMinutes = value;
            calItems.CacheMinutes = value;
        }
    }


    /// <summary>
    /// Gets or sets the cache item dependencies
    /// </summary>
    public override string CacheDependencies
    {
        get
        {
            return base.CacheDependencies;
        }
        set
        {
            base.CacheDependencies = value;
            calItems.CacheDependencies = value;
        }
    }


    /// <summary>
    /// Gets or sets the name of the cache item. If not explicitly specified, the name is automatically 
    /// created based on the control unique ID
    /// </summary>
    public override string CacheItemName
    {
        get
        {
            return base.CacheItemName;
        }
        set
        {
            base.CacheItemName = value;
            calItems.CacheItemName = value;
        }
    }

    #endregion


    #region "Repeater properties"

    /// <summary>
    /// Event detail transformation
    /// </summary>
    public string EventDetailTransformation
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("EventDetailTransformation"), "cms.bookingevent.Default");
        }
        set
        {
            SetValue("EventDetailTransformation", value);
            this.repEvent.SelectedItemTransformationName = value;
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads control data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            this.calItems.StopProcessing = true;
        }
        else
        {
            this.calItems.ControlContext = this.ControlContext;
            this.repEvent.ControlContext = this.ControlContext;

            // Calendar properties
            this.calItems.CacheItemName = this.CacheItemName;
            this.calItems.CacheDependencies = this.CacheDependencies;
            this.calItems.CacheMinutes = this.CacheMinutes;
            this.calItems.CheckPermissions = this.CheckPermissions;
            this.calItems.ClassNames = this.ClassNames;
            this.calItems.CombineWithDefaultCulture = this.CombineWithDefaultCulture;

            this.calItems.CultureCode = this.CultureCode;
            this.calItems.MaxRelativeLevel = this.MaxRelativeLevel;
            this.calItems.OrderBy = this.OrderBy;
            this.calItems.Path = this.Path;
            this.calItems.RelatedNodeIsOnTheLeftSide = this.RelatedNodeIsOnTheLeftSide;

            this.calItems.RelationshipName = this.RelationshipName;
            this.calItems.RelationshipWithNodeGuid = this.RelationshipWithNodeGuid;
            this.calItems.SelectOnlyPublished = this.SelectOnlyPublished;
            this.calItems.SiteName = this.SiteName;
            this.calItems.FilterName = this.FilterName;

            this.calItems.TransformationName = this.TransformationName;
            this.calItems.WhereCondition = this.WhereCondition;
            this.calItems.DayField = this.DayField;
            this.calItems.TodaysDate = CMSContext.ConvertDateTime(DateTime.Now, this);

            this.calItems.NoEventTransformationName = this.NoEventTransformationName;

            // If calendar event path is defined event is loaded in accordance to the selected path
            if ((Request.QueryString["CalendarEventPath"] != null) && (Request.QueryString["CalendarEventPath"].ToString() != ""))
            {
                repEvent.Visible = true;
                repEvent.StopProcessing = false;
                repEvent.Path = Request.QueryString["EventPath"].ToString();
                repEvent.SelectedItemTransformationName = this.EventDetailTransformation;
                repEvent.ClassNames = this.ClassNames;

                // Get document
                TreeNode node = TreeHelper.GetDocument(this.SiteName, repEvent.Path, this.CultureCode, this.CombineWithDefaultCulture, this.ClassNames, this.SelectOnlyPublished, this.CheckPermissions, CMSContext.CurrentUser);

                if (node != null)
                {
                    object value = DataHelper.GetDataRowValue(node.DataRow, this.DayField);
                    if (ValidationHelper.GetDateTime(value, DataHelper.DATETIME_NOT_SELECTED) != DataHelper.DATETIME_NOT_SELECTED)
                    {
                        this.calItems.TodaysDate = CMSContext.ConvertDateTime((DateTime)value, this);
                    }
                }
            }

            // By default select current event from current document value
            PageInfo currentPage = CMSContext.CurrentPageInfo;
            if ((currentPage != null) && (currentPage.ClassName.ToLower() == "cms.bookingevent"))
            {
                repEvent.Visible = true;
                repEvent.StopProcessing = false;
                repEvent.Path = currentPage.NodeAliasPath;
                repEvent.SelectedItemTransformationName = this.EventDetailTransformation;
                repEvent.ClassNames = this.ClassNames;

                object value = DataHelper.GetDataRowValue(CMSContext.CurrentDocument.DataRow, this.DayField);
                if (ValidationHelper.GetDateTime(value, DataHelper.DATETIME_NOT_SELECTED) != DataHelper.DATETIME_NOT_SELECTED)
                {
                    this.calItems.TodaysDate = CMSContext.ConvertDateTime((DateTime)value, this);
                }
            }
        }
    }


    /// <summary>
    /// Applies given stylesheet skin
    /// </summary>
    public override void ApplyStyleSheetSkin(Page page)
    {
        this.calItems.SkinID = this.SkinID;
        base.ApplyStyleSheetSkin(page);
    }


    /// <summary>
    /// Clear cache.
    /// </summary>
    public override void ClearCache()
    {
        this.repEvent.ClearCache();
    }
}
