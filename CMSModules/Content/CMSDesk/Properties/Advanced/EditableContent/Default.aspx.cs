using System;

using CMS.ExtendedControls;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSModules_Content_CMSDesk_Properties_Advanced_EditableContent_Default : CMSModalPage
{
    protected override void OnPreRender(EventArgs e)
    {
        if (CultureHelper.IsUICultureRTL())
        {
            ControlsHelper.ReverseFrames(colsFrameset);
        }

        tree.Attributes["src"] = "tree.aspx" + Request.Url.Query;
        main.Attributes["src"] = "main.aspx" + Request.Url.Query;
        base.OnPreRender(e);
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        // Check 'read' permissions
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Content", "Read"))
        {
            RedirectToAccessDenied("CMS.Content", "Read");
        }
    }
}
