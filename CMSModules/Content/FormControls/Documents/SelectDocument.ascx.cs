using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.ExtendedControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_FormControls_Documents_SelectDocument : CMS.FormControls.FormEngineUserControl
{
    #region "Variables"

    private bool mEnableSiteSelection = false;
    private DialogConfiguration mConfig = null;
    private TreeProvider mTreeProvider = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Returns TreeProvider object.
    /// </summary>
    private TreeProvider TreeProvider
    {
        get
        {
            if (this.mTreeProvider == null)
            {
                mTreeProvider = new TreeProvider(CMSContext.CurrentUser);
            }
            return this.mTreeProvider;
        }
    }


    /// <summary>
    /// Gets the configuration for Copy and Move dialog.
    /// </summary>
    private DialogConfiguration Config
    {
        get
        {
            if (this.mConfig == null)
            {
                this.mConfig = new DialogConfiguration();
                this.mConfig.HideLibraries = true;                
                this.mConfig.HideAnchor = true;
                this.mConfig.HideAttachments = true;
                this.mConfig.HideContent = false;
                this.mConfig.HideEmail = true;
                this.mConfig.HideLibraries = true;
                this.mConfig.HideWeb = true;
                this.mConfig.EditorClientID = this.txtName.ClientID;
                this.mConfig.ContentSelectedSite = CMSContext.CurrentSiteName;
                this.mConfig.OutputFormat = OutputFormatEnum.Custom;
                this.mConfig.CustomFormatCode = "selectpath";
                this.mConfig.SelectableContent = SelectableContentEnum.AllContent;
            }
            return this.mConfig;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.txtName.Enabled = value;
            this.btnSelect.Enabled = value;
            this.btnClear.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return GetNodeGuid();
        }
        set
        {
            if (ValidationHelper.GetGuid(value, Guid.Empty) != Guid.Empty)
            {
                this.txtName.Text = GetNodeName(value);
            }
        }
    }


    /// <summary>
    /// Gets ClientID of the textbox with path
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.txtName.ClientID;
        }
    }


    /// <summary>
    /// Determines whether to enable site selection or not.
    /// </summary>
    public bool EnableSiteSelection
    {
        get
        {
            return this.mEnableSiteSelection;
        }
        set
        {
            this.mEnableSiteSelection = value;
            this.Config.ContentSites = (value ? AvailableSitesEnum.All : AvailableSitesEnum.OnlyCurrentSite);
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ClearDocument", ScriptHelper.GetScript("function ClearDocument(txtClientID){ document.getElementById(txtClientID).value = ''; }"));

        this.btnSelect.Text = ResHelper.GetString("general.select");
        this.btnSelect.OnClientClick = "modalDialog('" + GetDialogUrl() + "','PathSelection', '90%', '85%'); return false;";

        this.btnClear.Text = ResHelper.GetString("FormControls_SelectDocument.btnClear");
        this.btnClear.OnClientClick = "ClearDocument('" + txtName.ClientID + "'); return false;";

        this.txtName.Attributes.Add("readonly", "readonly");
    }

    /// <summary>
    /// Returns Correct URL of the copy or move dialog.
    /// </summary>
    private string GetDialogUrl()
    {
        string url = CMSDialogHelper.GetDialogUrl(this.Config, this.IsLiveSite, false);
        url = url.Replace(UrlHelper.GetFullDomain(), "").Replace("https://", "").Replace("http://", "");
        url = UrlHelper.RemoveParameterFromUrl(url, "hash");
        url = UrlHelper.AddParameterToUrl(url, "selectionmode", "single");
        url = UrlHelper.AddParameterToUrl(url, "hash", QueryHelper.GetHash(url));
        url = UrlHelper.EncodeQueryString(url);
        return url;
    }


    /// <summary>
    /// Get node name from guid
    /// </summary>
    /// <param name="objGuid">Guid object</param>
    private string GetNodeName(object objGuid)
    {
        Guid nodeGuid = ValidationHelper.GetGuid(objGuid, Guid.Empty);
        if (nodeGuid != Guid.Empty)
        {
            TreeNode node = this.TreeProvider.SelectSingleNode(nodeGuid, CMSContext.PreferredCultureCode, CMSContext.CurrentSiteName);
            if (node != null)
            {
                return node.NodeAliasPath;
            }
        }

        return "";
    }


    /// <summary>
    /// Get node guid from alias path.
    /// </summary>
    private Guid GetNodeGuid()
    {
        if (this.txtName.Text != "")
        {
            TreeNode node = this.TreeProvider.SelectSingleNode(CMSContext.CurrentSiteName, this.txtName.Text, CMSContext.PreferredCultureCode);
            if (node != null)
            {
                return node.NodeGUID;
            }
        }
        return Guid.Empty;
    }
}
