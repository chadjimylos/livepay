﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports LivePay_ESBBridge
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.SiteProvider
Imports CMS.DataEngine
Imports CMS.SettingsProvider
Imports CMS.FormControls
Imports System.IO
Imports System.Collections.Generic

'Imports LivePayCustomField
Public Structure CustomFieldsType
    Private _TypeName As String
    Private _ErrorDivID As String
    Private _TypePath As String
    Private _price As Double




    Public Property TypeName() As String
        Get
            Return _TypeName
        End Get
        Set(ByVal value As String)
            _TypeName = value
        End Set
    End Property

    Public Property ErrorDivID() As String
        Get
            Return _ErrorDivID
        End Get
        Set(ByVal value As String)
            _ErrorDivID = value
        End Set
    End Property

    Public Property TypePath() As String
        Get
            Return _TypePath
        End Get
        Set(ByVal value As String)
            _TypePath = value
        End Set
    End Property


End Structure

'Public Class LivePayCustomFieldType
'    Shared Sub New()
'    End Sub

'    Public Shared Function AddType(ByVal TypeName As String, ByVal TypePath As String) As CustomFieldsType
'        Dim fieldType As New CustomFieldsType
'        fieldType.TypeName = TypeName
'        fieldType.TypePath = TypePath
'        Return fieldType
'    End Function
'End Class

' ''' <summary>
' ''' Settings key form controls information
' ''' </summary>
'Public Structure SettingsKeyItem
'    Public settingsKey As String
'    ' Settings key code name
'    Public inputControlId As String
'    ' Checkbox id, textbox id
'    Public inheritId As String
'    ' Inherit checkbox id
'    Public isInherited As Boolean
'    Public errorLabelId As String
'    ' Error label id
'    Public value As String
'    ' Value
'    Public type As String
'    ' Type (int, boolean)
'    Public validation As String
'    ' Regex vor validation
'    Public changed As Boolean
'    ' Changed flag
'End Structure


Partial Class CMSTemplates_LivePay_UCRegisteredUsers
    Inherits CMSUserControl

    'Private LivePayMerchant As String
    'Private pathToGroupselector As String = "~/CMSModules/Groups/FormControls/MembershipGroupSelector.ascx"
    Private pathToGroupselector As String = "~/CMSTemplates/LivePay/CustomFields/USphone.ascx"
    Private selectInGroups As FormEngineUserControl
    'Private ArrCustomFieldType As New ArrayList()
    ReadOnly FieldType As New List(Of CustomFieldsType)()
    'Private mMerchantPublicSector As Boolean = False

    Private TimeoutLivePayBridge As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutLivePayBridge"))

#Region " Properties "
    Private _DocumentTreeNode As CMS.TreeEngine.TreeNode

    Public ReadOnly Property TransPublic As Boolean
        Get
            Return IIf(_DocumentTreeNode.NodeClassName = "LivePay.MerchantPublicSector", True, False)
        End Get
    End Property

    Protected ReadOnly Property MerchantID() As Integer
        Get
            Dim iMerchantID As Integer = 1

            If TransPublic Then
                iMerchantID = _DocumentTreeNode.GetValue("MerchantPublicSectorID")
            Else
                iMerchantID = _DocumentTreeNode.GetValue("MerchantID")
            End If
            'iMerchantID = _DocumentTreeNode.GetValue("MerchantID")
            Return iMerchantID
        End Get
    End Property

    Protected ReadOnly Property LivePayID() As String
        Get
            Dim _LivePayID As String = String.Empty
            _LivePayID = _DocumentTreeNode.GetValue("LivePayID")
            Return _LivePayID
        End Get
    End Property

    Protected ReadOnly Property SelectedPrice() As String
        Get
            Dim sPrice As String = "0"
            If Not String.IsNullOrEmpty(Request("SelPrice")) Then
                sPrice = Request("SelPrice")
            End If
            Return sPrice
        End Get
    End Property

    Protected ReadOnly Property TransactionID() As String
        Get
            Dim mTransactionID As String = "0"
            If Not String.IsNullOrEmpty(Request("TransactionID")) Then
                mTransactionID = Request("TransactionID")
            End If
            Return mTransactionID
        End Get
    End Property


    Private _ResultGetCommission As Double
    Public Property ResultGetCommission() As Double
        Get
            Return _ResultGetCommission
        End Get
        Set(ByVal value As Double)
            _ResultGetCommission = value
        End Set
    End Property



    Private MinimumTransactionAmount As Decimal = 0
    Private MaximumTransactionAmount As Decimal = 0
    Private MaximumTransactionsPerDayCard As Integer = 0
    Private MaximumTransactionsPerDayGlobally As Integer = 0
    Private MaximumSumTransactionsPerDayInCard As Decimal = 0
    Private MaxTransactionsPerSessionPerCard As Integer = 0
    Private DTMerchantCardTypes As DataTable = Nothing
    Private CompanyName As String = String.Empty
    Private CompanyLogo As String = String.Empty
    Private MerchantValidCards As String = String.Empty
#End Region

#Region "Methods"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If Not Page.IsPostBack Then
            'SAVE URL IN A SESSION, WILL NEED IT FOR appiPhone VERSION
            Session("appiPhoneInitialCallURL") = Request.Url.ToString()
        End If
        If Request.QueryString("nodeID") <> String.Empty Then
            Dim tp As CMS.TreeEngine.TreeProvider = New CMS.TreeEngine.TreeProvider(CMS.CMSHelper.CMSContext.CurrentUser)
            Dim node As CMS.TreeEngine.TreeNode = tp.SelectSingleNode(Request.QueryString("nodeID"))

            _DocumentTreeNode = node
            JsScript(" ChangeIPhoneTitle('" + _DocumentTreeNode.GetValue("DiscreetTitle") + "'); ")
        Else
            _DocumentTreeNode = CMSContext.CurrentDocument
        End If

        JsScript(" var transPublic = " & Me.TransPublic.ToString.ToLower() & "; " & vbCrLf & _
                 " var IsLogedIn = " & CMSContext.CurrentUser.IsPublic.ToString.ToLower() & "; " & vbCrLf)

        If Not TransPublic Then Tab2_ChlIWantInvoice.Visible = True

        Tab1_BtnNext.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/BtnNext.png|en-us=/App_Themes/LivePay/RegisteredUsers/BtnNext_en-us.png$}")
        Tab2_btnReturn.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnReturn.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnReturn_en-us.png$}")
        ImageButton2.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnReturn.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnReturn_en-us.png$}")
        Tab3_btnReturn.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnReturn.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnReturn_en-us.png$}")
        Tab3_btnComplete.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnComplete.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnComplete_en-us.png$}")
        'LivePayMerchant = CMSContext.CurrentDocument.NodeID
        GetInstallments()

        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        ElseIf Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
        If BrowserHelper.IsMobileDevice() Then
            If Not CMSContext.CurrentUser.IsPublic Then
                Dim UserID As Integer = CMSContext.CurrentUser.UserID
                Dim dt As DataTable = DBConnection.GetUserCards(UserID)
                Dim dtFinal As DataTable = dt
                ForRegUsersTopMobile.Visible = True

                Tab2_ChlIWantInvoice.Visible = False
                'Tab2_ChlIWantInvoice.Enabled = False
                'Tab2_ChlIWantInvoice.Style.Add("display", "none")
                'JsAlert("Tab2_ChlIWantInvoice.Visible -> " & Tab2_ChlIWantInvoice.Visible)
                'Tab2_btnReturn_Mobile.EnableViewState = False
                Tab2_btnReturn_Mobile.Style.Add("display", "none")
                Tab2_btnReturn.Style.Add("display", "block")
                Tab2_ChlIWantInvoice.EnableViewState = False

                Tab2_btnReturn.OnClientClick = "ShowHideTabs('','none','none');return false"

                Dim rowIndex As Integer = 0
                For Each row As DataRow In dtFinal.Rows
                    Dim lnkBtn As New LinkButton
                    lnkBtn.Text = row("FriendlyName").ToString
                    lnkBtn.ID = row("CardID").ToString()
                    '    lnkBtn.Text = row("FriendlyName").ToString
                    'lnkBtn.ID = row("CardID").ToString
                    '    lnkBtn.OnClientClick = "ShowHideTabs('none','','none');"
                    '    lnkBtn.OnClientClick = "$('#Tab1').hide();"
                    AddHandler lnkBtn.Click, AddressOf LinkButtonMobileCards_Click

                    Dim dynDiv As New System.Web.UI.HtmlControls.HtmlGenericControl("DIV")
                    dynDiv.Attributes("class") = "btnSaveCardMobile"
                    dynDiv.Controls.Add(lnkBtn)
                    ForRegUsersTopMobile.Controls.AddAt(rowIndex, dynDiv)
                    rowIndex += 1
                Next


                JsScript(" $('.REgUsersBottomMust').hide();ChangeIPhoneTitle('Επιλογή Κάρτας'); ")
            Else
                Dim login As String = Request.QueryString("login") 'UrlHelper.GetUrlParameter("", "login")
                Dim url1 As String = "---"
                url1 = SetQueryString(url1)
                'JsAlert(url1)
                'JsAlert(login)
                If login = "1" Then
                    Dim url As String = "~/appiphone/login.aspx"
                    url = SetQueryString(url)
                    Response.Redirect(url)
                End If
            End If

        End If

        'JsAlert("false")
        'ForRegUsersTopMobile.Visible = True
        'If ForRegUsersTopMobile.Visible Then
        '    Dim lnkBtn As New LinkButton
        '    lnkBtn.Text = "Test 1"
        '    lnkBtn.ID = "test_id"
        '    'lnkBtn.OnClientClick = "ShowHideTabs('none','','none');"
        '    'lnkBtn.OnClientClick = "$('#Tab1').hide();"
        '    AddHandler lnkBtn.Click, AddressOf LinkButtonMobileCards_Click
        '    ForRegUsersTopMobile.Controls.Add(lnkBtn)
        '    JsAlert("aaaaa")
        'End If

        CreateCustomFields()
        If Not CMSContext.CurrentUser.IsPublic() Then
            Tab3_chkTermsAccept.Visible = False
            Div_Terms.Visible = False
        End If
    End Sub

    Public Overloads Sub ReloadData()
        'mMerchantPublicSector = TransPublic

        If (TransPublic) Then
            Tab1_txtNotes.Visible = False
            div_txtNones.Visible = False
            divNotes.Visible = False
        End If

        'GetInstallments()



        ' Response.Redirect("/CMSTemplates/LivePay/xmlHttpMerchants.aspx")
        If IsNothing(Session("UserTransactions")) Then
            Session("UserTransactions") = 0
        End If

        'If Not Page.IsPostBack Then
        Tab2_RbSavedCard.Attributes.Add("onclick", String.Concat("ChangeRadioChoice('", Tab2_RbSavedCard.ClientID, "','", Tab2_RbNewCard.ClientID, "')"))
        Tab2_RbNewCard.Attributes.Add("onclick", String.Concat("ChangeRadioChoice('", Tab2_RbNewCard.ClientID, "','", Tab2_RbSavedCard.ClientID, "')"))
        'End If

        CreateCustomFields()


        SetDefaults()
        GetMerchantLimits()
        If Not BrowserHelper.IsMobileDevice() Then
            GetUserCards()
        End If
        If Me.SelectedPrice <> "0" Then
            Tab1_txtPrice.Text = SelectedPrice
        End If


        'pnlNewCard.Style.Add("display", "none")

        Tab2_ChkSaveDetails.Text = ResHelper.LocalizeString("{$=Αποθήκευση Στοιχείων Κάρτας για Μελλοντική Χρήση|en-us=Save Credit Card Info for future use$}")
        Dim IsLogedIn As Boolean = CMSContext.CurrentUser.IsPublic()
        If IsLogedIn Then
            If BrowserHelper.IsMobileDevice() Then ForRegUsersTop.Visible = False
            pnlNewCard.Style.Add("display", "block")
            ForRegUsersTop.Style.Add("display", "none")
            Tab2_RbNewCardDivCont.Style.Add("display", "none")
            Tab2_RbNewCard.Checked = True
            pnlPhoneMail.Visible = True
            ForRegUsersChoiceToSave.Style.Add("display", "none")

            JsScript(String.Concat("; ChangeRadioChoice('", Tab2_RbNewCard.ClientID, "','", Tab2_RbSavedCard.ClientID, "'); " & vbCrLf))
        End If

        'Page.Validate("Tab1_PayForm")
        'Tab1_BtnNext_Click(New Object, New ImageClickEventArgs(0, 0))

        'Tab1_BtnNext.

    End Sub

    Private Sub SetErrorMessages()
        Dim dt As DataTable = DBConnection.GetErrorMessages(1)
        Dim JsStringMessages As String = String.Empty
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim ErrorMessageStr As String = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("ErrorMessageGR").ToString, "|en-us=", dt.Rows(i)("ErrorMessageEN").ToString, "$}"))
            ErrorMessageStr = Replace(ErrorMessageStr, "'", "\'")

            ErrorMessageStr = ErrorMessageStr.Replace(vbCr, String.Empty).Replace(vbLf, String.Empty)

            Select Case dt.Rows(i)("CodeName").ToString.ToLower.Trim
                Case "emptyselectedcarderrormessage"
                    lbl_EmptySelectedCardErrorMessage.Text = ErrorMessageStr
                Case "newcardcvv2errormessage"
                    lbl_NewCardCVV2ErrorMessage.Text = ErrorMessageStr
                Case "termsofuse"
                    Tab3_lblTerms.Text = ErrorMessageStr
                Case "fullnameerrormessage"
                    lbl_FullNameErrorMessage.Text = ErrorMessageStr
                Case "cardtypetoperrormessage"
                    lbl_CardTypeTopErrorMessage.Text = ErrorMessageStr
                Case "ddlinstallmentserrormessage"
                    lbl_DDLInstallmentsErrorMessage.Text = ErrorMessageStr
                Case "errormaximumtransactionsperdayglobally"
                    lbl_ErrorMaximumTransactionsPerDayGlobally.Text = ErrorMessageStr
                Case "errortranscountpermerchantpercard"
                    lbl_ErrorTransCountPerMerchantPerCard.Text = ErrorMessageStr
                Case "errortranssumamountpercardpermerchant"
                    lbl_ErrorTransSumAmountPerCardPerMerchant.Text = ErrorMessageStr
                Case Else
                    JsStringMessages = String.Concat(JsStringMessages, " ", dt.Rows(i)("CodeName").ToString, " = '", ErrorMessageStr, "'; " & vbCrLf)
            End Select


            'If dt.Rows(i)("CodeName").ToString = "TermsOfUse" Then
            '    'JsStringMessages = String.Concat(JsStringMessages, " ", dt.Rows(i)("CodeName").ToString, " = '", CMS.GlobalHelper.HTMLHelper.StripTags(ErrorMessageStr).Replace(vbCr, String.Empty).Replace(vbLf, String.Empty), "'; ")

            '    'JsStringMessages = String.Concat(JsStringMessages, " ", dt.Rows(i)("CodeName").ToString, " = '", ErrorMessageStr.Replace(vbCr, String.Empty).Replace(vbLf, String.Empty), "'; " & vbCrLf)

            '    Tab3_lblTerms.Text = ErrorMessageStr.Replace(vbCr, String.Empty).Replace(vbLf, String.Empty)
            'Else
            '    JsStringMessages = String.Concat(JsStringMessages, " ", dt.Rows(i)("CodeName").ToString, " = '", ErrorMessageStr.Replace(vbCr, String.Empty).Replace(vbLf, String.Empty), "'; " & vbCrLf)

            '    'Response.Write(ErrorMessageStr.Replace(vbCr, String.Empty).Replace(vbLf, String.Empty) & "<br/>")
            'End If


        Next
        'JsStringMessages = String.Concat(JsStringMessages, " $('#", Tab3_lblTerms.ClientID, "').html(TermsOfUse);  " & vbCrLf)
        JsScript(JsStringMessages)
    End Sub

    Private Sub GetMerchantLimits()
        SetErrorMessages()
        'Dim ds As DataSet = DBConnection.GetMerchantLimits(Me.MerchantID)
        'Response.Write("->" & Me.MerchantID & "<-")
        'Response.Write("LivePayID -> " & Me.LivePayID)

        Dim ds As DataSet

        ds = DBConnection.GetMerchantLimits(Me.MerchantID, TransPublic)

        Dim dt As DataTable = ds.Tables(0)
        'Response.Write(dt.Rows.Count)
        If dt.Rows.Count > 0 Then
            Dim row As DataRow = dt.Rows(0)
            MinimumTransactionAmount = row("MinimumTransactionAmount")
            MaximumTransactionAmount = row("MaximumTransactionAmount")
            MaximumTransactionsPerDayCard = row("MaximumTransactionsPerDayCard")
            MaximumTransactionsPerDayGlobally = row("MaximumTransactionsPerDayGlobally")
            'MaximumSumTransactionsPerDayInCard --> MaximumSumTransactionsPerDayCard
            MaximumSumTransactionsPerDayInCard = Convert.ToDecimal(row("MaximumSumTransactionsPerDayInCard").ToString)
            CompanyName = row("CompanyName").ToString
            CompanyLogo = row("Logo").ToString
            HiddenMinimumTransactionAmount.Text = row("MinimumTransactionAmount").ToString
            HiddenMaximumTransactionAmount.Text = row("MaximumTransactionAmount").ToString
            HiddenMaximumTransactionsPerDayCard.Text = row("MaximumTransactionsPerDayCard").ToString
            HiddenMaximumTransactionsPerDayGlobally.Text = row("MaximumTransactionsPerDayGlobally").ToString
            HiddenMaximumSumTransactionsPerDayInCard.Text = row("MaximumSumTransactionsPerDayInCard").ToString
            HiddenCompanyName.Text = row("CompanyName").ToString
            'Session("NewPayment_Merchant_LivePay_ID") = row("LivePayID").ToString
            If BrowserHelper.IsMobileDevice() OrElse Request("nodeid") <> String.Empty Then
                HiddenMerchantID.Text = Request.QueryString("nodeid").ToString()
            Else
                HiddenMerchantID.Text = CMSContext.CurrentDocument.NodeID
            End If

            '--- Per Session
            Dim SessionLimits As Integer = 10
            HiddenMaxTransactionsPerSessionPerCard.Text = SessionLimits
            If Session("UserTransactions") > SessionLimits Then

                GotToTransErrorPage("SessionError")
            End If

            Dim UserID As Integer = CMSContext.CurrentUser.UserID
            Dim dtMaxMerchantTransLimit As DataTable = DBConnection.GetMerchantTransaction(UserID, Convert.ToInt32(HiddenMerchantID.Text), 0, String.Empty, 2, True)
            If dtMaxMerchantTransLimit.Rows.Count > 0 Then
                Dim MerchantsTrans As Integer = dtMaxMerchantTransLimit.Rows(0)("TransCountPerMerchant") 'Μέγιστες επιτρεπόμενες συναλλαγές <br />  ανά Ημέρα συνολικά
                If MerchantsTrans >= MaximumTransactionsPerDayGlobally Then
                    ErrorText.Text = lbl_ErrorMaximumTransactionsPerDayGlobally.Text
                    'GotToTransErrorPage("MaximumTransError")
                    Tab1_BtnNext.Visible = False
                End If
            End If

            '--- Accepted Card Types
            DTMerchantCardTypes = ds.Tables(1)
            For i As Integer = 0 To DTMerchantCardTypes.Rows.Count - 1
                If MerchantValidCards = String.Empty Then
                    MerchantValidCards = String.Concat("[", DTMerchantCardTypes.Rows(i)("itemID").ToString, "]")
                Else
                    MerchantValidCards = String.Concat(MerchantValidCards, ",[", DTMerchantCardTypes.Rows(i)("itemID").ToString, "]")
                End If
                '---- Card Types DropDown

            Next
            HiddenMerchantValidCards.Text = MerchantValidCards



        End If
    End Sub

    Private Function SetQueryString(ByVal url As String) As String
        'string url = "~/appiphone/payment.aspx";
        Dim loop1 As Integer, loop2 As Integer
        ' Load NameValueCollection object.
        Dim coll As NameValueCollection = Request.QueryString
        ' Get names of all keys into a string array.
        Dim arr1 As [String]() = coll.AllKeys
        For loop1 = 0 To arr1.Length - 1
            'Response.Write("Key: " + Server.HtmlEncode(arr1[loop1]) + "<br>");
            Dim arr2 As [String]() = coll.GetValues(arr1(loop1))
            For loop2 = 0 To arr2.Length - 1
                'Response.Write("Value " + loop2 + ": " + Server.HtmlEncode(arr2[loop2]) + "<br>");
                url = UrlHelper.AddParameterToUrl(url, Server.HtmlEncode(arr1(loop1)), Server.HtmlEncode(arr2(loop2)))
            Next
        Next

        Return url
    End Function

    Private Sub GotToTransErrorPage(ByVal ErrorName As String)
        Select Case ErrorName
            Case "SessionError"
                'JsScript("alert(UsertMaxTransSessionErrorMessage);location.href='default.aspx';" & vbCrLf)
            Case "MaximumTransError"
                'JsScript("alert(MerchantMaxTransErrorMessage);location.href='default.aspx';" & vbCrLf)
        End Select


    End Sub

    Private Sub GetUserCardsMobile()
        'Dim UserID As Integer = CMSContext.CurrentUser.UserID
        'Dim dt As DataTable = DBConnection.GetUserCards(UserID)
        'Dim dtFinal As DataTable = dt
        ''If BrowserHelper.IsMobileDevice() Then
        'For Each row As DataRow In dtFinal.Rows
        '    Dim lnkBtn As New LinkButton
        '    lnkBtn.Text = row("FriendlyName").ToString
        '    lnkBtn.ID = row("CardID").ToString
        '    'lnkBtn.OnClientClick = "ShowHideTabs('none','','none');"
        '    lnkBtn.OnClientClick = "$('#Tab1').hide();"
        '    AddHandler lnkBtn.Click, AddressOf LinkButtonMobileCards_Click
        '    ForRegUsersTopMobile.Controls.Add(lnkBtn)
        'Next


        'Dim lnkBtn1 As New LinkButton
        'lnkBtn1.Text = "text 1111"
        'lnkBtn1.ID = "textID_1"
        'ForRegUsersTopMobile.Controls.Add(lnkBtn1)


        ForRegUsersTopMobile.Visible = True
        pnlCardMobileCVV.Visible = False

        pnlNewCard.Visible = False
        ForRegUsersTop.Visible = False
        Tab2_RbNewCard.Visible = False
        Tab2_btnReturn.Style.Add("display", "block")
        ForRegUsersTopMobile.Style.Add("display", "block")

        'Tab2_btnNext.Visible = False
        JsScript("$('#Tab2_btnNextCheckNewCard').hide()")
        'JsScript("alert('a')")

        'End If
    End Sub

    'Protected Sub Tab2_btnReturn_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Tab2_btnReturn.Click
    '    ForRegUsersTopMobile.Visible = True
    '    pnlCardMobileCVV.Visible = False

    '    Tab2_btnReturn.Style.Add("display", "")
    '    Tab2_btnReturn.OnClientClick = "ShowHideTabs('','none','none');return false"

    'End Sub


    Protected Sub Tab3_btnReturn_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab3_btnReturn.Click

        JsScript("ShowHideTabs('none','','none');" & vbCrLf)
        GetUserCardsMobile()
    End Sub

    Sub LinkButtonMobileCards_Click(ByVal sender As Object, ByVal e As EventArgs)

        If Not TransPublic Then Tab2_ChlIWantInvoice.Visible = True

        Dim CardNoID As String = CType(sender, LinkButton).ID
        JsScript(" ChangeIPhoneTitle('" & _DocumentTreeNode.GetValue("DiscreetTitle") & "') ")


        hid_MobileSelectedSaveCardID.Text = CardNoID

        GetMerchantLimits()
        If CheckMerchantLimitsPerCard() Then
            'JsAlert("CType(sender, LinkButton).ID -> " & CType(sender, LinkButton).ID)
            pnlNewCard.Visible = False
            ForRegUsersTop.Visible = False
            Tab2_RbNewCard.Visible = False
            ForRegUsersTopMobile.Style.Add("display", "none")
            pnlCardMobileCVV.Visible = True

            JsScript(" $('.REgUsersBottomMust').show();")
            ''ImageButton2.OnClientClick = "alert('sdfasf asd fsadf'); $('[id$=_pnlCardMobileCVV]').hide(); $('[id$=_ForRegUsersTopMobile]').show();return false"
            Tab2_btnReturn.Style.Add("display", "none")
            Tab2_btnReturn_Mobile.OnClientClick = "$('.REgUsersBottomMust').hide();$('[id$=_pnlCardMobileCVV]').hide(); $('[id$=_ForRegUsersTopMobile]').show(); $('[id$=_pnlInstallments]').hide(); $('[id$=_Tab2_btnReturn_Mobile]').hide();$('[id$=_Tab2_btnReturn]').show();$('[id$=Tab2_btnNextCheckNewCard]').hide();$('[id$=_Tab2_ChlIWantInvoice]').hide();$('[for$=_Tab2_ChlIWantInvoice]').hide(); ChangeIPhoneTitle('Επιλογή Κάρτας'); return false"
            'Tab2_btnReturn_Mobile.OnClientClick = "alert('sdfasf asd fsadf'); $('[id$=_pnlCardMobileCVV]').hide(); $('[id$=_ForRegUsersTopMobile]').show(); $('[id$=_pnlInstallments]').hide(); $('[id$=_Tab2_btnReturn]').show();return false"
            Tab2_btnReturn.OnClientClick = "$('.REgUsersBottomMust').show();$('[id$=_Tab2_btnReturn_Mobile]').hide();ShowHideTabs('','none','none');ChangeIPhoneTitle('" & _DocumentTreeNode.GetValue("DiscreetTitle") & "');return false"
            Tab2_btnReturn_Mobile.Style.Add("display", "block")
            Tab2_ChlIWantInvoice.Text = ResHelper.LocalizeString("{$=Επιθυμώ να μου αποσταλεί τιμολόγιο|en-us=I wish to have an Invoice issued$}")


            Tab3_btnReturn.OnClientClick = "" 'ShowHideTabs('none','','none');AllCardsValidation('');return false


            HiddenChoice.Text = 1
            'ForRegUsersTopMobile.Visible = False




            GetInstallmentsSaveCardMobile()
            JsScript("ShowHideTabs('none','','none');" & vbCrLf)
        End If
    End Sub

    Private Sub GetInstallmentsSaveCardMobile()

        Dim dtCards As DataTable = DBConnection.GetMerchantCardType(hid_MobileSelectedSaveCardID.Text, String.Empty)
        Dim CardLimitID As String = String.Empty
        Dim CardsJson As StringBuilder = New StringBuilder()
        For i As Integer = 0 To dtCards.Rows.Count - 1
            Dim Limits As String = dtCards.Rows(i)("Limit").ToString

            If Limits <> String.Empty Then
                Dim LimitLength As Integer = Split(Limits, ",").Length - 1


                For iCount As Integer = 0 To LimitLength
                    Dim CardLimitNo As String = Split(Limits, ",")(iCount)
                    'JsAlert("Me.dtCards.Rows(i)(CardNo).ToString -> " & dtCards.Rows(i)("CardNo").ToString)
                    If Not String.IsNullOrEmpty(dtCards.Rows(i)("CardNo").ToString) AndAlso dtCards.Rows(i)("CardNo").ToString.Length > 3 Then
                        Dim DecryptedCard As String = Left(dtCards.Rows(i)("CardNo").ToString, 4) & CryptoFunctions.DecryptString(Right(dtCards.Rows(i)("CardNo").ToString, dtCards.Rows(i)("CardNo").ToString.Length - 4))
                        If DecryptedCard.StartsWith(CardLimitNo) Then
                            CardLimitID &= dtCards.Rows(i)("itemid").ToString & "|"
                            If CardsJson.Length > 0 Then CardsJson.Append(", ")

                            CardsJson.AppendFormat("{{""id"":{0}, ""binds"":{1}}}", dtCards.Rows(i)("itemid").ToString, CardLimitNo.Trim.Length)
                        End If
                    End If
                Next

            End If

        Next
        'hiddenInstallments.Text '<> 0 AndAlso hiddenInstallments.Text <> "-1") 

        'hiddenInstallments.Text = "0"
        JsScript("$('Tab1').hide(); ddlInstallments(findCardID(" & String.Format("jQuery.parseJSON('{{""cards"" :{0}}}')", String.Concat("[", CardsJson.ToString(), "]")) & ", '" & HiddenMerchantValidCards.Text & "'));")

    End Sub

    Sub LinkButtonMobileNewCard_Click(ByVal sender As Object, ByVal e As EventArgs)
        If Not TransPublic Then Tab2_ChlIWantInvoice.Visible = True
        Tab2_ChlIWantInvoice.Text = ResHelper.LocalizeString("{$=Επιθυμώ να μου αποσταλεί τιμολόγιο|en-us=I wish to have an Invoice issued$}")
        JsScript(" ChangeIPhoneTitle('" & _DocumentTreeNode.GetValue("DiscreetTitle") & "') ")
        Tab2_btnReturn.Style.Add("display", "block")
        JsScript(" $('.REgUsersBottomMust').show();")

        ForRegUsersTopMobile.Visible = False
        pnlCardMobileCVV.Visible = False

        Tab2_btnReturn.Visible = True
        pnlNewCard.Visible = True
        ForRegUsersTop.Visible = False
        Tab2_RbNewCard.Visible = True
        Tab2_RbSavedCard.Visible = False
        JsScript("ShowHideTabs('none','','none'); $('#Tab2_RB2Div').hide();" & vbCrLf)
    End Sub

    Private Sub GetUserCards()
        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = DBConnection.GetUserCards(UserID)
        Dim dtFinal As DataTable = dt

        dtFinal.Columns.Add("Text", GetType(String), "FriendlyName + ' - xxxxxxxx-xxxx-' + LastFor")
        dtFinal.Columns.Add("Value", GetType(String), "CardID + '_' + CardTypeID")
        Tab2_ddlSavedCard.DataTextField = "Text"
        Tab2_ddlSavedCard.DataValueField = "Value"
        Tab2_ddlSavedCard.DataSource = dtFinal
        Tab2_ddlSavedCard.DataBind()
        Tab2_ddlSavedCard.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=Επιλογή Κάρτας|en-us=Select Card$}"), String.Empty))

        Tab2_ddlSavedCard.Attributes.Add("onchange", String.Concat("CheckValidCard()"))

        If dt.Rows.Count = 0 Then
            ForRegUsersTop.Style.Add("display", "none")
            Tab2_RbNewCardDivCont.Style.Add("display", "none")
            Tab2_RbNewCard.Checked = True
            JsScript(String.Concat("; ChangeRadioChoice('", Tab2_RbNewCard.ClientID, "','", Tab2_RbSavedCard.ClientID, "'); " & vbCrLf))
        End If

    End Sub

    Private Sub SetDefaults()
        Tab2_ChkSaveDetails.Attributes.Add("onclick", "CardForSave(this)")
        Tab1_ReqFldVal_Price.ErrorMessage = ResHelper.LocalizeString("{$=Ποσό|en-us=Amount$}")
        'Tab1_ReqularFldVal_Price.ErrorMessage = ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}")

        Tab2_RbSavedCard.Text = ResHelper.LocalizeString("{$=Επιλογή Αποθηκευμένης Κάρτας|en-us=Choose from your saved Cards$}")
        Tab2_RbNewCard.Text = ResHelper.LocalizeString("{$=Εισάγετε Νέα Κάρτα|en-us=Insert New Credit Card$}")
        Tab2_ReqFldVal_FriendName.ErrorMessage = ResHelper.LocalizeString("{$=Φιλική Ονομασία|en-us=Friendly Name$}")
        If TransPublic AndAlso BrowserHelper.IsMobileDevice() Then
            Tab2_ChlIWantInvoice.Visible = False
        Else
            Tab2_ChlIWantInvoice.Text = ResHelper.LocalizeString("{$=Επιθυμώ να μου αποσταλεί τιμολόγιο|en-us=I wish to have an Invoice issued$}")
        End If

        For i As Integer = 1 To 12
            Dim dtm As Date = New Date(2000, i, 1)
            Tab2_DdlCardMonth.Items.Insert(i - 1, New ListItem(dtm.ToString("MMMM"), i))
        Next

        For i As Integer = 0 To 9
            Dim Years As Integer = Year(Now) + i
            Tab2_DdlCardYear.Items.Insert(i, New ListItem(Years, Years))
        Next

        ddlDoy.DataSource = DBConnection.GetTaxOffices
        ddlDoy.DataTextField = ResHelper.LocalizeString("{$=TitleGr|en-us=TitleEn$}")
        ddlDoy.DataValueField = "ItemID"
        ddlDoy.DataBind()
        ddlDoy.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=Επιλέξτε ΔΟΥ|en-us=Select Tax Office$}"), String.Empty))

    End Sub

    Private Sub CreateCustomFields()
        Tab2_ddlCardType.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=Επιλέξτε Τύπο Κάρτας|en-us=Select Card Type$}"), String.Empty))
        Tab2_ddlCardType.Items.Insert(1, New ListItem("Visa", 0))
        Tab2_ddlCardType.Items.Insert(2, New ListItem("MasterCard", 1))
        Tab2_ddlCardType.Items.Insert(3, New ListItem("EuroLine", 2))
        SetErrorMessages()

        'Response.Write("CMSContext.CurrentDocument.NodeID --> " & CMSContext.CurrentDocument.NodeID)
        'Response.Write("MerchantID --> " & Me.MerchantID)
        'Response.End()

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(_DocumentTreeNode.NodeID)
        Dim dt As DataTable = ds.Tables(0)

        For i As Integer = 0 To dt.Rows.Count - 1

            Dim CustomFieldValue As String = String.Empty
            'Response.Write(CustomFieldValue)

            If Me.TransactionID <> 0 Then
                Dim FieldID As Integer = dt.Rows(i)("ItemID")
                Dim FieldValueDT As DataTable

                FieldValueDT = DBConnection.GetFavoritesCustomFields(Me.TransactionID, FieldID, CMSContext.CurrentUser.UserID)

                If FieldValueDT.Rows.Count > 0 Then
                    CustomFieldValue = FieldValueDT.Rows(0)(0).ToString()
                End If
            End If

            'If Request("nodeid") = CMSContext.CurrentDocument.NodeID Then
            If Request("nodeid") <> "" Or Request("data") = 1 Then

                Select Case i
                    Case 0
                        CustomFieldValue = Request("info0")
                    Case 1
                        CustomFieldValue = Request("info1")
                    Case 2
                        CustomFieldValue = Request("info2")
                    Case 3
                        CustomFieldValue = Request("info3")
                    Case 4
                        CustomFieldValue = Request("info4")
                    Case 5
                        CustomFieldValue = Request("info5")
                    Case 6
                        CustomFieldValue = Request("info6")
                    Case 7
                        CustomFieldValue = Request("info7")
                    Case 8
                        CustomFieldValue = Request("info8")
                    Case 9
                        CustomFieldValue = Request("info9")
                End Select

                'If Request("Amount") <> "" Then
                'Tab1_txtPrice.Text = Request("Amount")
                'End If
            End If


            Dim CustomTitleDiv As New HtmlGenericControl("div")
            CustomTitleDiv.Attributes.Add("class", "RegUsersTab1CustomTitle")
            CustomTitleDiv.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))




            Dim CustomTextBoxDiv As New HtmlGenericControl("div")
            CustomTextBoxDiv.ID = "formUserControl_Div" & i
            CustomTextBoxDiv.Attributes.Add("class", "RegUsersTab2_TxtDiv RegUsersTab2_TxtDiv_text")

            'If (CMSContext.CurrentDocument.NodeClassName = mMerchantPublicSector) Then

            Dim FormControlID As Integer = Convert.ToInt32(dt.Rows(i)("FieldType").ToString())
            Dim pathFormControl As String = LivePayCustomField.GetPathFormControl(FormControlID)

            'If Not IsPostBack Then
            Dim NDate As String = dt.Rows(i)("NDate").ToString
            Dim OrganismCode As Integer = ValidationHelper.GetInteger(dt.Rows(i)("OrganismCode").ToString, 0)
            Dim TypePayment As Integer = ValidationHelper.GetInteger(dt.Rows(i)("TypePayment").ToString, 0)
            Dim MaxLength As Integer = ValidationHelper.GetInteger(dt.Rows(i)("MaxLength").ToString, 0)
            Dim Mandatory As Boolean = ValidationHelper.GetBoolean(dt.Rows(i)("Mandatory").ToString, False)
            Dim RegExp As String = ValidationHelper.GetString(dt.Rows(i)("RegExp").ToString, "")
            Dim NameFieldEn As String = ValidationHelper.GetString(dt.Rows(i)("NameFieldEn").ToString, "")
            Dim NameFieldGr As String = ValidationHelper.GetString(dt.Rows(i)("NameFieldGr").ToString, "")

            CreateCustomFormControl(pathFormControl, CustomTextBoxDiv, i, NameFieldGr, NameFieldEn, RegExp, Mandatory, MaxLength, TypePayment, OrganismCode, NDate, CustomFieldValue)




            'End If



            'Select Case dt.Rows(i)("FieldType").ToString.ToLower()
            '    Case "text"
            '        Dim pathFormControlAFM As String = "~/CMSTemplates/LivePay/CustomFields/USphone.ascx"
            '        CreateCustomFormControl(pathFormControlAFM, CustomTextBoxDiv, i, dt.Rows(i)("NameFieldGr").ToString, dt.Rows(i)("NameFieldEn").ToString)
            '    Case "taxid"
            '        Dim pathFormControlAFM As String = "~/CMSTemplates/LivePay/CustomFields/taxid.ascx"
            '        CreateCustomFormControl(pathFormControlAFM, CustomTextBoxDiv, i, dt.Rows(i)("NameFieldGr").ToString, dt.Rows(i)("NameFieldEn").ToString)
            '    Case "test"
            '        Dim pathFormControlAFM As String = "~/CMSTemplates/LivePay/CustomFields/USphone.ascx"
            '        CreateCustomFormControl(pathFormControlAFM, CustomTextBoxDiv, i, dt.Rows(i)("NameFieldGr").ToString, dt.Rows(i)("NameFieldEn").ToString)
            '    Case Else
            '        Dim CustomCloudDiv As New HtmlGenericControl("div")
            '        CustomCloudDiv.InnerHtml = String.Concat("<a id='CustomCloud_", i, "' href='#'></a>")
            '        CustomTextBoxDiv.Controls.Add(CustomCloudDiv)

            '        Dim txtbx As New TextBox
            '        txtbx.CssClass = "RegUsersTab1CustomTxt"
            '        txtbx.Width = "260"
            '        txtbx.ID = "CustomField_" & i
            '        txtbx.MaxLength = dt.Rows(i)("maxlength").ToString
            '        txtbx.Attributes.Add("onclick", "CountValidations=0;")
            '        If CustomFieldValue <> String.Empty Then
            '            txtbx.Text = CustomFieldValue
            '        End If
            '        CustomTextBoxDiv.Controls.Add(txtbx)

            '        Dim CustomDivValidation As New HtmlGenericControl("div")
            '        CustomDivValidation.Style.Add("display", "none")

            '        Dim CustomValidat As New CustomValidator
            '        CustomValidat.ValidationGroup = "Tab1_PayForm"
            '        CustomValidat.ValidateEmptyText = True
            '        CustomValidat.ControlToValidate = txtbx.ClientID
            '        CustomValidat.ClientValidationFunction = "validateControls"
            '        CustomValidat.ErrorMessage = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
            '        CustomValidat.Attributes.Add("ControlValidated", txtbx.ClientID)
            '        CustomValidat.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", i))
            '        CustomValidat.Attributes.Add("CustomValidatorErrorMessage", ResHelper.LocalizeString(String.Concat("{$=Συμπληρώστε το πεδίο <b>", dt.Rows(i)("NameFieldGr").ToString, "</b>|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}")))

            '        If dt.Rows(i)("Mandatory").ToString = "True" Then
            '            CustomDivValidation.Controls.Add(CustomValidat)
            '        End If

            '        CustomTextBoxDiv.Controls.Add(CustomDivValidation)
            '        Exit Select
            'End Select

            '--- Add Controls
            CustomFields.Controls.Add(CustomTitleDiv)
            CustomFields.Controls.Add(CustomTextBoxDiv)


            'Dim ControlID As String = String.Concat("formUserControl_" & i)
            'Dim MyControl As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID), LivePayFormEngineUserControl)
            'If MyControl IsNot Nothing Then
            '    'Response.Write("|->" & MyControl.FindControl("IDTransaction").ClientID & "<-|")
            '    'Response.Write("|->" & Request.Form.ToString & "<-|")
            '    'Response.Write("|->" & MyControl.FindControl("IDTransaction").ClientID.Replace("%24", "$").Replace("$0", "_0") & "<-|")
            '    Response.Write("|->" & Request.Form(MyControl.FindControl("IDTransaction").ClientID.Replace("_", "$")) & "<-|")         '.Replace("$0", "_0")
            '    'For j As Integer = 0 To Request.Form.Keys.Count - 1
            '    'Response.Write(Request.Form.Keys(j) & "<br />")
            '    'Next

            'End If

            '-----
            Dim MainDivCloud As New HtmlGenericControl("div")
            MainDivCloud.Attributes.Add("class", "RegUsersMainDivCloud")
            'MainDivCloud.Attributes.Add("onmouseover", String.Concat("ShowClound('", CloudID, "',this)"))
            'MainDivCloud.Attributes.Add("onmouseout", String.Concat("HideClound('", CloudID, "')"))

            '--- Start Create Cloud

            Dim NewCustomCloudID As String = "NewCoustomCloud_" & i & "_CloudMain"
            Dim NewCustomCloudContentID As String = "NewCoustomCloud_" & i & "_CloudContent"
            Dim NewCloudContent As String = ResHelper.LocalizeString(String.Concat("{$=", Replace(dt.Rows(i)("DescriptionGR").ToString, "'", String.Empty), "|en-us=", Replace(dt.Rows(i)("DescriptionEn").ToString, "'", String.Empty), "$}"))
            NewCloudContent = Replace(NewCloudContent, vbCrLf, " ")
            Dim LinkOpener As New HtmlAnchor
            MainDivCloud.ID = "Opener_ " & i & "_"



            Dim imgInfo As New HtmlImage
            imgInfo.Src = "/App_Themes/LivePay/InfoBtn.png"
            LinkOpener.Controls.Add(imgInfo)
            MainDivCloud.Controls.Add(LinkOpener)

            Dim DivCloud As New HtmlGenericControl("div")
            DivCloud.Style.Add("position", "absolute")
            DivCloud.Style.Add("padding-bottom", "130px")
            DivCloud.Style.Add("display", "none")
            DivCloud.Attributes.Add("id", NewCustomCloudID)

            DivCloud.Controls.Add(CreatCloud(NewCustomCloudContentID))

            'MainDivCloud.Controls.Add(DivCloud)
            '--- Add Controls
            CustomFields.Controls.Add(MainDivCloud)
            CustomFields.Controls.Add(DivCloud)

            Dim DivClear As New HtmlGenericControl("div")
            DivClear.Attributes.Add("class", "Clear")
            CustomFields.Controls.Add(DivClear)

            Dim DivMobile As New HtmlGenericControl("div")
            '<div id="Inv_DOYMsg_Mob" style="color:#d31f00;font-family:Arial;font-size:25px;display:none"></div>
            DivMobile.Style.Add("color", "#d31f00")
            DivMobile.Style.Add("font-family", "Arial")
            DivMobile.Style.Add("font-size", "25px")
            DivMobile.Style.Add("display", "none")
            DivMobile.Attributes.Add("class", "msg_error_mobile")
            DivMobile.Attributes.Add("id", "CustomCloud_" & i & "_Mob")
            CustomFields.Controls.Add(DivMobile)
            '-----
            'CustomCloud_", i
            CreateCustomFieldsForTab3(dt.Rows(i)("NameFieldGr").ToString, dt.Rows(i)("NameFieldEn").ToString, i)


            If BrowserHelper.IsMobileDevice() Then
                LinkOpener.Attributes.Add("onclick", String.Concat("OpenCustomCloud('", NewCloudContent.Replace("src=""~/", "src=""/"), "','", NewCustomCloudContentID, "','", NewCustomCloudID, "','" & MainDivCloud.ClientID & "',true)"))
                If HttpContext.Current.Request.UserAgent.ToLower().Contains("android") Then
                    NewStyle.Text = "<style type=""text/css""> #ui-datepicker-div{width:300px} .ui-state-default{zoom:2} </style>"
                End If
                If HttpContext.Current.Request.UserAgent.ToLower().Contains("iphone") Then
                    NewStyle.Text = "<style type=""text/css""> #ui-datepicker-div{width:430px}  .ui-state-default{zoom:3}</style>"
                End If
                If Request.Url.ToString.ToLower.Contains("appiphone") Then
                    NewStyle.Text = "<style type=""text/css""> #ui-datepicker-div{width:230px}</style>"
                End If

                LinkOpener.Attributes.Add("onclick", String.Concat("OpenCustomCloudAppMobile('", NewCloudContent.Replace("src=""~/", "src=""/"), "','", NewCustomCloudContentID, "','", NewCustomCloudID, "','" & MainDivCloud.ClientID & "',true)"))

            Else
                LinkOpener.Attributes.Add("onmouseover", String.Concat("OpenCustomCloud('", NewCloudContent.Replace("src=""~/", "src=""/"), "','", NewCustomCloudContentID, "','", NewCustomCloudID, "',this,true)"))
                LinkOpener.Attributes.Add("onmouseout", String.Concat("OpenCustomCloud('", NewCloudContent.Replace("src=""~/", "src=""/"), "','", NewCustomCloudContentID, "','", NewCustomCloudID, "',this,false)"))
            End If


        Next



        Dim CloseCloudsFn As String = "function CloseAllClouds() { "
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardMsgNewCard').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#NewCardFullNameMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#NewCardCVVMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardCVV2Msg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardMsgSec').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_CompanyNameMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_OccupationMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_AddressMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_TKMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_CityMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_AFMMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_DOYMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#CardTypeMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Tab1_PriceMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#NewCardFriendly').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#UnRegUserPhoneMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#UnRegUserEmailMsg').poshytip('hide'); ")
        '---- Custom Clouds
        For i As Integer = 0 To dt.Rows.Count - 1
            CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#CustomCloud_", i, "').poshytip('hide'); ")
        Next
        '---- Custom Clouds
        CloseCloudsFn = String.Concat(CloseCloudsFn, "} ")
        lit_JsScript.Text = "<script> " & CloseCloudsFn & " </script>"

        Tab2_RbSavedCard.TabIndex = 9
        Tab2_ddlSavedCard.TabIndex = 10
        Tab2_txtCVV2Top.TabIndex = 11
        Tab2_RbNewCard.TabIndex = 12
        Tab2_ddlCardType.TabIndex = 13
        Tab2_txtCardNo.TabIndex = 14
        Tab2_txtFullNameOwner.TabIndex = 15
        Tab2_DdlCardMonth.TabIndex = 16
        Tab2_DdlCardYear.TabIndex = 17
        Tab2_txtCVV2.TabIndex = 18
        Tab2_ChkSaveDetails.TabIndex = 19
        Tab2_txtFriendName.TabIndex = 20
        Tab2_txtPhone.TabIndex = 21
        Tab2_txtEmail.TabIndex = 22
        Tab2_btnNext.TabIndex = 23
        Tab2_ChlIWantInvoice.TabIndex = 24
        Tab2Inv_CompanyName.TabIndex = 25
        Tab2Inv_Occupation.TabIndex = 26
        Tab2Inv_Address.TabIndex = 27
        Tab2Inv_City.TabIndex = 28
        Tab2Inv_TK.TabIndex = 29
        Tab2Inv_AFM.TabIndex = 30
        ddlDoy.TabIndex = 31
        Tab2_NextBtnInvoice.TabIndex = 32
    End Sub

    Private Sub CreateCustomFieldsForTab3(ByVal NameGR As String, ByVal NameEN As String, ByVal iCount As Integer)
        'Dim ControlID1 As String = String.Concat("formUserControl_0")
        'Dim MyControl1 As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID1), LivePayFormEngineUserControl)
        'Response.Write(MyControl1.Value)

        Dim TitleDiv As New HtmlGenericControl("div")
        Dim ValueDiv As New HtmlGenericControl("div")
        Dim ClearDiv As New HtmlGenericControl("div")
        TitleDiv.Attributes.Add("class", "RegUserTab3TitleB")
        TitleDiv.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", NameGR, "|en-us=", NameEN, "$}"))
        ValueDiv.Attributes.Add("class", "RegUsersTab2_TxtDiv")

        Dim ControlID As String = String.Concat("formUserControl_" & iCount)
        Dim MyControl As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID), LivePayFormEngineUserControl)

        'MyControl.Value = "123444444"

        ValueDiv.InnerHtml = String.Concat("<span class='RegUSerTab3Control' id='Tab3_Span_CustomField_", iCount, "'></span>")
        ClearDiv.Attributes.Add("class", "Clear")
        With Tab3_CustomFields.Controls
            .Add(TitleDiv)
            .Add(ValueDiv)
            .Add(ClearDiv)
        End With
    End Sub

    Private Function CreatCloud(ByVal NewCustomCloudContentID As String) As HtmlTable
        Dim MainTable As New HtmlTable
        MainTable.CellPadding = 0
        MainTable.CellSpacing = 0
        MainTable.Border = 0



        Dim RowA As New HtmlTableRow
        Dim Cell_LeftTop As New HtmlTableCell
        Dim Cell_Top As New HtmlTableCell
        Dim Cell_RightTop As New HtmlTableCell

        Cell_LeftTop.Attributes.Add("class", "CustCloudTopLeft")
        Cell_Top.Attributes.Add("class", "CustCloudTop")
        Cell_RightTop.Attributes.Add("class", "CustCloudTopRight")

        With RowA.Controls
            .Add(Cell_LeftTop)
            .Add(Cell_Top)
            .Add(Cell_RightTop)
        End With

        Dim RowB As New HtmlTableRow
        Dim Cell_Left As New HtmlTableCell
        Dim Cell_Content As New HtmlTableCell
        Dim Cell_Right As New HtmlTableCell

        Cell_Left.Attributes.Add("class", "CustCloudLeft")
        Cell_Content.Attributes.Add("class", "CustCloudCenter")
        Cell_Right.Attributes.Add("class", "CustCloudRight")

        Cell_Content.Attributes.Add("id", NewCustomCloudContentID)

        With RowB.Controls
            .Add(Cell_Left)
            .Add(Cell_Content)
            .Add(Cell_Right)
        End With

        Dim RowC As New HtmlTableRow
        Dim Cell_LeftBottom As New HtmlTableCell
        Dim Cell_Bottom As New HtmlTableCell
        Dim Cell_RightBottom As New HtmlTableCell

        Cell_LeftBottom.Attributes.Add("class", "CustCloudBottomLeft_")
        Cell_Bottom.Attributes.Add("class", "CustCloudBottom_")
        Cell_RightBottom.Attributes.Add("class", "CustCloudBottomRight_")

        With RowC.Controls
            .Add(Cell_LeftBottom)
            .Add(Cell_Bottom)
            .Add(Cell_RightBottom)
        End With

        With MainTable.Controls
            .Add(RowA)
            .Add(RowB)
            .Add(RowC)
        End With

        Return MainTable
    End Function
#End Region

#Region " Tabs Events "

    Protected Sub Tab1_BtnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab1_BtnNext.Click
        'Page.Validate()
        If Page.IsValid Then
            Dim validationFailed As Boolean = True

            'Dim ControlID As String = String.Concat("formUserControl_0")
            'Dim MyControl As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID), LivePayFormEngineUserControl)
            'Response.Write(MyControl.Value)

            For Each customFT As CustomFieldsType In FieldType

                Dim cntrl As Control = CustomFields.FindControl(customFT.TypeName)

                Dim control As LivePayFormEngineUserControl = TryCast(cntrl, LivePayFormEngineUserControl)
                If control IsNot Nothing Then
                    If Not control.IsValid() Then
                        validationFailed = False
                        JsScript("Tab1_ValidationCloud('#" & customFT.ErrorDivID & "', "" " & control.ValidationError & " "", true, '');" & vbCrLf)
                    End If
                End If
            Next
            If validationFailed Then
                Dim MobileScript As String = String.Empty
		JsScript("ShowHideTabs('none','','none'); payment_setheight();" & vbCrLf)
		'JsScript("ShowHideTabs('none','','none'); var _is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1; var f = document.getElementById('footer'); if (_is_chrome && document.getElementById('Tab2').style.display != 'none'){f.style.top = GetInnerHeight() + 'px';}" & vbCrLf)
                If BrowserHelper.IsMobileDevice() Then
                    Tab2_RbNewCard.Visible = False
                    'If True Then
                    MobileScript = "mobileInit(); SetZoom_(); "
                    'ForRegUsersTopMobile.Visible = True
                    If Not CMSContext.CurrentUser.IsPublic Then
                        GetUserCardsMobile()
                    Else
                        Tab2_RbSavedCard.Checked = False

                        pnlCardMobileCVV.Style.Add("display", "none")
                        Tab2_RbNewCard.Visible = False
                        'JsAlert(pnlCardMobileCVV.Style.Value)
                        pnlCardMobileCVV.Visible = False
                    End If
                    JsScript(MobileScript & vbCrLf)
                Else
                    JsScript("CheckValidCard(); " & vbCrLf)
                End If


                If Tab2_txtCardNo.Text.Length = 16 Then
                    JsScript("AllCardsValidation(); " & vbCrLf)
                End If
                If Tab2_RbNewCard.Checked Then
                    pnlNewCard.Style.Add("display", "block")
                End If

            End If
        Else
            'If BrowserHelper.IsMobileDevice() Then
            '    Dim MobileScript As String = String.Empty
            '    MobileScript = " mobileInit(); SetZoom_(); "
            '    JsScript(MobileScript & vbCrLf)
            'End If

        End If
    End Sub

    Public Function CheckMerchantLimitsPerCard() As Boolean
        Dim valid As Boolean = True
        'Organization Payment And Merchant
        Dim EncryptedCard As String = String.Empty

        Dim CardID As Integer = 0
        If Tab2_RbSavedCard.Checked Then
            If BrowserHelper.IsMobileDevice AndAlso Not CMSContext.CurrentUser.IsPublic Then
                'Apothikevmeni karta apo mobile
                CardID = hid_MobileSelectedSaveCardID.Text
            Else
                CardID = Split(Tab2_ddlSavedCard.SelectedValue, "_")(0)
            End If
            Dim DtCard As DataTable = DBConnection.GetUserCard(CMSContext.CurrentUser.UserID, CardID)
            If DtCard.Rows.Count > 0 Then
                EncryptedCard = DtCard.Rows(0)("CardNumber").ToString
            End If
        Else
            EncryptedCard = Left(Me.Tab2_txtCardNo.Text, 4) & CryptoFunctions.EncryptString(Right(Me.Tab2_txtCardNo.Text, 12))
        End If

        Dim dtMaxMerchantTransPerDayCard As DataTable = DBConnection.GetMerchantTransaction(CMSContext.CurrentUser.UserID, Convert.ToInt32(HiddenMerchantID.Text), 0, EncryptedCard, 2, True)

        If dtMaxMerchantTransPerDayCard.Rows.Count > 0 Then
            Dim TransCountPerMerchantPerCard As Integer = dtMaxMerchantTransPerDayCard.Rows(0)("TransCountPerMerchantPerCard") 'Μέγιστες επιτρεπόμενες συναλλαγές <br />  ανά Ημέρα στην ίδια κάρτα
            'ErrorText.Text = "MerchantID = " & CmsContext.CurrentDocument.NodeID & " CardNo = " & EncryptedCard & " TransCountPerMerchantPerCard = " & TransCountPerMerchantPerCard & " Convert.ToInt32(HiddenMaximumTransactionsPerDayCard.Text) = " & Convert.ToInt32(HiddenMaximumTransactionsPerDayCard.Text)
            If TransCountPerMerchantPerCard >= MaximumTransactionsPerDayCard Then
                'GotToTransErrorPage("MaximumTransError")
                ErrorText.Text = lbl_ErrorTransCountPerMerchantPerCard.Text
                valid = False
            End If
            Dim TransSumAmountPerCardPerMerchant As Decimal = Convert.ToDecimal(dtMaxMerchantTransPerDayCard.Rows(0)("TransSumAmountPerCardPerMerchant")) '.ToString("#0.00").PadLeft(3).Replace(",", ".")

            Dim culture As System.Globalization.CultureInfo
            culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US")

            Dim Price As Decimal = Convert.ToDecimal(Tab1_txtPrice.Text, culture)

            Dim SumAmount As Decimal = 0
            SumAmount = Price + TransSumAmountPerCardPerMerchant

            If SumAmount > Convert.ToDecimal(HiddenMaximumSumTransactionsPerDayInCard.Text) Then 'Μέγιστο επιτρεπόμενο ποσό συναλλαγών <br />  (άθροισμα) ανά Ημέρα στην ίδια κάρτα
                'If SumAmount > MaximumSumTransactionsPerDayInCard Then 'Μέγιστο επιτρεπόμενο ποσό συναλλαγών <br />  (άθροισμα) ανά Ημέρα στην ίδια κάρτα
                'GotToTransErrorPage("MaximumTransError")
                ErrorText.Text = lbl_ErrorTransSumAmountPerCardPerMerchant.Text
                valid = False
            End If
        Else
            valid = False
        End If

        Return valid
    End Function

    Protected Sub Tab2_btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab2_btnNext.Click

        'Dim ControlID As String = String.Concat("formUserControl_0")
        'Dim MyControl As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID), LivePayFormEngineUserControl)
        'Response.Write("tab 2 " & MyControl.Value)
        GetMerchantLimits()
        'If Page.IsValid AndAlso CheckInstallments() AndAlso CheckMerchantLimitsPerCard() Then
        If Page.IsValid AndAlso CheckInstallments() AndAlso CheckMerchantLimitsPerCard() Then
            Tab3_lblPayTo.Text = HiddenCompanyName.Text
            Tab3_lblPrice.Text = Tab1_txtPrice.Text
            Dim CardNo As String = String.Empty
            If Tab2_RbNewCard.Checked Then
                Tab3_lblCard.Text = String.Concat("xxxxxxxx-xxxx-", Right(Tab2_txtCardNo.Text, 4))
                CardNo = Tab2_txtCardNo.Text
                hdn_CVV.Text = Tab2_txtCVV2.Text
                If BrowserHelper.IsMobileDevice Then JsScript(" ChangeIPhoneTitle('" & _DocumentTreeNode.GetValue("DiscreetTitle") & "'); ")
            ElseIf BrowserHelper.IsMobileDevice Then
                Dim DtCard As DataTable = DBConnection.GetUserCard(CMSContext.CurrentUser.UserID, hid_MobileSelectedSaveCardID.Text)
                If DtCard.Rows.Count > 0 Then
                    CardNo = Left(DtCard.Rows(0)("CardNumber").ToString, 4) & CryptoFunctions.DecryptString(Right(DtCard.Rows(0)("CardNumber").ToString, (DtCard.Rows(0)("CardNumber").ToString.Length - 4)))
                End If
                Tab3_lblCard.Text = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                hdn_CVV.Text = txtSaveCardMobileCVV.Text


                JsScript(" ChangeIPhoneTitle('" & _DocumentTreeNode.GetValue("DiscreetTitle") & "'); ")
            Else
                Tab3_lblCard.Text = Split(Tab2_ddlSavedCard.SelectedItem.Text, " - ")(1).ToString
                Dim CardID As String = Split(Tab2_ddlSavedCard.SelectedValue, "_")(0)
                Dim DtCard As DataTable = DBConnection.GetUserCard(CMSContext.CurrentUser.UserID, CardID)
                hdn_CVV.Text = Tab2_txtCVV2Top.Text
                If DtCard.Rows.Count > 0 Then
                    CardNo = Left(DtCard.Rows(0)("CardNumber").ToString, 4) & CryptoFunctions.DecryptString(Right(DtCard.Rows(0)("CardNumber").ToString, (DtCard.Rows(0)("CardNumber").ToString.Length - 4)))
                End If
            End If
            Dim jsCustomFeelFields As String = String.Empty
            'Dim ds As DataSet = DBConnection.GetMerchantCustomFields(Me.MerchantID)
            Dim ds As New DataSet
            If BrowserHelper.IsMobileDevice Then
                ds = DBConnection.GetMerchantCustomFields(HiddenMerchantID.Text)
            Else
                ds = DBConnection.GetMerchantCustomFields(_DocumentTreeNode.NodeID)
            End If
            Dim dt As DataTable = ds.Tables(0)

            Dim MobileScript As String = String.Empty
            If BrowserHelper.IsMobileDevice() Then
                MobileScript = " mobileInit(); SetZoom_(); "
            End If

            JsScript(String.Concat("SetCustomFieldsValues('CustomField_','Tab3_Span_CustomField_','", dt.Rows.Count - 1, "');ShowHideTabs('none','none',''); " & MobileScript & vbCrLf))
            'Response.Write(Convert.ToInt32(Me.hiddenInstallments.Text) > 0)
            'Response.End()

            If Convert.ToInt32(Me.hiddenInstallments.Text) > 0 Then
                Tab3_Installments.Visible = True
                Text_Installments.Text = Me.hiddenInstallments.Text
            End If

            If TransPublic Then
                Dim w As New LivePay_ESBBridge.Bridge
                w.Timeout = TimeoutLivePayBridge
                'Dim ResultGetCommission As String = w.GetCommission(CardNo, Session("NewPayment_Merchant_LivePay_ID"), Tab1_txtPrice.Text).Result
                Me.ResultGetCommission = w.GetCommission(CardNo, Me.LivePayID, Tab1_txtPrice.Text).Result

                'If Me.ResultGetCommission <> "" AndAlso Me.ResultGetCommission <> "0" Then
                If Me.ResultGetCommission <> 0 Then
                    Tab3_Commission.Visible = True
                    Text_Commission.Text = Me.ResultGetCommission.ToString("#0.00").PadLeft(3).Replace(",", ".")
                End If
            End If

        End If
    End Sub

    Protected Sub Tab2_NextBtnInvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) 'Handles Tab2_NextBtnInvoice.Click


        If Page.IsValid AndAlso CheckInstallments() AndAlso CheckMerchantLimitsPerCard() Then
            'If Page.IsValid Then

            Tab3_lblPayTo.Text = HiddenCompanyName.Text
            Tab3_lblPrice.Text = Tab1_txtPrice.Text
            If Tab2_RbNewCard.Checked Then
                Tab3_lblCard.Text = String.Concat("xxxxxxxx-xxxx-", Right(Tab2_txtCardNo.Text, 4))
            Else
                Tab3_lblCard.Text = Split(Tab2_ddlSavedCard.SelectedItem.Text, " - ")(1).ToString
            End If
            Dim jsCustomFeelFields As String = String.Empty
            'Dim ds As DataSet = DBConnection.GetMerchantCustomFields(Me.MerchantID)
            Dim ds As DataSet = DBConnection.GetMerchantCustomFields(_DocumentTreeNode.NodeID)
            Dim dt As DataTable = ds.Tables(0)

            Dim MobileScript As String = String.Empty
            If BrowserHelper.IsMobileDevice() Then
                MobileScript = " mobileInit(); SetZoom_(); "
            End If

            JsScript(String.Concat("SetCustomFieldsValues('CustomField_','Tab3_Span_CustomField_','", dt.Rows.Count - 1, "');ShowHideTabs('none','none',''); " & MobileScript & vbCrLf))
        End If
    End Sub

    Function CheckInstallments() As Boolean

        If (hiddenInstallments.Text = "-1" AndAlso pnlInstallments.Style("display") <> "none") Then
            JsScript("AllCardsValidation(); ShowHideTabs('none','','none');" & vbCrLf)
            Return False
        ElseIf (hiddenInstallments.Text <> 0 AndAlso hiddenInstallments.Text <> "-1") Then

            Dim EncryptedCard As String = Left(Me.Tab2_txtCardNo.Text, 4) & CryptoFunctions.EncryptString(Right(Me.Tab2_txtCardNo.Text, 12))
            If Me.Tab2_txtCardNo.Text = String.Empty Then
                EncryptedCard = String.Empty
            End If

            Dim dtCards As DataTable
            'If BrowserHelper.IsMobileDevice() Then
                'dtCards = DBConnection.GetMerchantCardType(hid_MobileSelectedSaveCardID.Text, String.Empty)
            'Else
                dtCards = DBConnection.GetMerchantCardType(Me.hiddenUserCardID.Text, Me.Tab2_txtCardNo.Text)
            'End If

            Dim CardLimitID As Integer = 0

            If dtCards.Rows.Count > 0 Then
                Dim UserCard As String = Me.Tab2_txtCardNo.Text
                If Me.Tab2_txtCardNo.Text <> String.Empty Then '---- New Card
                    For i As Integer = 0 To dtCards.Rows.Count - 1
                        Dim Limits As String = dtCards.Rows(i)("Limit").ToString

                        If Limits <> String.Empty Then
                            Dim LimitLength As Integer = Split(Limits, ",").Length - 1
                            For iCount As Integer = 0 To LimitLength
                                Dim CardLimitNo As String = Split(Limits, ",")(iCount)
                                If Me.Tab2_txtCardNo.Text.StartsWith(CardLimitNo) Then
                                    CardLimitID = dtCards.Rows(i)("itemid").ToString
                                End If
                            Next
                        End If
                    Next
                Else
                    For i As Integer = 0 To dtCards.Rows.Count - 1

                        Dim Limits As String = dtCards.Rows(i)("Limit").ToString
                        If Limits <> String.Empty Then
                            Dim LimitLength As Integer = Split(Limits, ",").Length - 1
                            For iCount As Integer = 0 To LimitLength
                                Dim CardLimitNo As String = Split(Limits, ",")(iCount)

                                Dim DecryptedCard As String = Left(dtCards.Rows(i)("CardNo").ToString, 4) & CryptoFunctions.DecryptString(Right(dtCards.Rows(i)("CardNo").ToString, dtCards.Rows(i)("CardNo").ToString.Length - 4))

                                If DecryptedCard.StartsWith(CardLimitNo) Then
                                    CardLimitID = dtCards.Rows(i)("itemid").ToString
                                End If
                            Next
                        End If
                    Next
                End If
            End If

            Dim MinInstallments_merchant As Integer = _DocumentTreeNode.GetValue("MinInstallments")
            Dim MaxInstallments_merchant As Integer = _DocumentTreeNode.GetValue("MaxInstallments")

            Dim dsItems As DataSet = GetTypeCard("active = 1 and Itemid = " & CardLimitID, "MinInstallments")

            Dim MinInstallments_cardtype As Integer = Convert.ToInt32(dsItems.Tables(0).Rows(0)("MinInstallments").ToString)
            Dim MaxInstallments_cardtype As Integer = Convert.ToInt32(dsItems.Tables(0).Rows(0)("MaxInstallments").ToString)

            Dim arr_Installments As New ArrayList

            For i As Integer = MinInstallments_merchant To MaxInstallments_merchant
                If (i >= MinInstallments_cardtype AndAlso i <= MaxInstallments_cardtype) Then
                    arr_Installments.Add(i)
                End If
            Next

            Dim status As Boolean = False
            For i As Integer = 0 To arr_Installments.Count - 1
                If CType(arr_Installments.Item(i), Integer) = Convert.ToInt32(hiddenInstallments.Text) Then
                    status = True
                    Exit For
                End If
            Next

            Return status
        Else
            Return True
        End If

    End Function

#End Region


    Protected Sub Tab3_btnComplete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab3_btnComplete.Click
        If Page.IsValid Then
            Dim CatchError As String = String.Empty
            Session("MakePaymentError") = String.Empty
            Dim TransID As Integer = 0
            Dim PublicTransID As String = String.Empty

            Try


                Dim w As New LivePay_ESBBridge.Bridge
                w.Timeout = TimeoutLivePayBridge
                Dim obj As New LivePay_ESBBridge.PaymentObj
                Dim UserID As Integer = 0
                Dim UserEmail As String = Tab2_txtEmail.Text
                Dim USerPhone As String = Tab2_txtPhone.Text
                Dim CustomerName As String = Tab2_txtFullNameOwner.Text
                Dim CardNo As String = Tab2_txtCardNo.Text
                Dim CardCvv2 As String = hdn_CVV.Text 'Tab2_txtCVV2.Text
                Dim CardExpDate As String = String.Empty
                Dim CardDescrID As Integer = 0
                Dim ExpMonth As String = Tab2_DdlCardMonth.SelectedValue
                Dim ExpYear As String = Tab2_DdlCardYear.Text
                Dim CardID As Integer = 0

                Dim Price As Decimal
                Dim twoLettersLanguage As String = CMSContext.CurrentDocumentCulture.CultureCode.ToLower().Substring(0, 2).ToLower()
                If twoLettersLanguage = "en" Then
                    Price = Tab1_txtPrice.Text
                Else
                    Price = Replace(Tab1_txtPrice.Text, ".", ",")
                End If

                If TransPublic Then
                    Me.ResultGetCommission = w.GetCommission(CardNo, Me.LivePayID, Price).Result
                End If

                Dim IsLogedIn As Boolean = CMSContext.CurrentUser.IsPublic()
                If IsLogedIn = False Then
                    UserID = CMSContext.CurrentUser.UserID
                    UserEmail = CMSContext.CurrentUser.Email
                    Try
                        USerPhone = CMSContext.CurrentUser.GetValue("UserPhone").ToString
                    Catch ex As Exception
                        USerPhone = ""
                    End Try
                End If

                If Tab2_RbSavedCard.Checked Then          '----- If Saved Cards is Checked
                    If BrowserHelper.IsMobileDevice AndAlso Not CMSContext.CurrentUser.IsPublic Then
                        'Apothikevmeni karta apo mobile
                        CardID = hid_MobileSelectedSaveCardID.Text
                    Else
                        CardID = Split(Tab2_ddlSavedCard.SelectedValue, "_")(0)
                    End If
                    Dim DtCard As DataTable = DBConnection.GetUserCard(UserID, CardID)
                    If DtCard.Rows.Count > 0 Then
                        CardNo = Left(DtCard.Rows(0)("CardNumber").ToString, 4) & CryptoFunctions.DecryptString(Right(DtCard.Rows(0)("CardNumber").ToString, (DtCard.Rows(0)("CardNumber").ToString.Length - 4)))
                        CardCvv2 = hdn_CVV.Text 'Tab2_txtCVV2Top.Text
                        ExpMonth = DtCard.Rows(0)("MonthExpiration").ToString
                        ExpYear = DtCard.Rows(0)("YearExpiration").ToString
                        CustomerName = DtCard.Rows(0)("FullName").ToString
                        CardDescrID = DtCard.Rows(0)("CardDescrID").ToString
                    End If
                Else
                    CardDescrID = Tab2_ddlCardType.SelectedValue
                End If
                If ExpMonth.Length = 1 Then
                    ExpMonth = String.Concat("0", ExpMonth)
                End If
                CardExpDate = String.Concat(Right(ExpYear, 2), ExpMonth)

                Dim Doy As Integer = 0
                If Tab2_ChlIWantInvoice.Checked Then '--- If With Invoice is checked
                    Doy = ddlDoy.SelectedValue
                End If

                Dim EncryptedCard As String = Left(CardNo, 4) & CryptoFunctions.EncryptString(Right(CardNo, 12))

                '-----------------
                '--Start--- I take the New Trans ID Status = 0 (before check)

                'Dim CustomValue() As String

                'Dim dtTrans As DataTable = DBConnection.InsertMerchantTransaction(Me.MerchantID, UserID, CardID, EncryptedCard, CustomerName, Price, USerPhone, UserEmail, Tab2Inv_CompanyName.Text, _
                '                                                                  Tab2Inv_Occupation.Text, Tab2Inv_Address.Text, Tab2Inv_TK.Text, Tab2Inv_City.Text, Tab2Inv_AFM.Text, Doy, CardDescrID, 0)
                Dim dtTrans As DataTable = DBConnection.InsertMerchantTransaction(_DocumentTreeNode.NodeID, UserID, CardID, EncryptedCard, CustomerName, Price, USerPhone, UserEmail, Tab2Inv_CompanyName.Text, _
                                                                  Tab2Inv_Occupation.Text, Tab2Inv_Address.Text, Tab2Inv_TK.Text, Tab2Inv_City.Text, Tab2Inv_AFM.Text, Doy, CardDescrID, 0, TransPublic, HTTPHelper.GetUserHostAddress(), Request.UserAgent)

                Dim TransGUID As Guid
                If dtTrans.Rows.Count > 0 Then
                    TransID = dtTrans.Rows(0)("TransID")
                    TransGUID = New Guid(dtTrans.Rows(0)("TransGUID").ToString)
                End If
                '--End--- I take the New Trans ID Status = 0 (before check)

                'JsAlert(Session("NewPayment_Merchant_LivePay_ID"))
                'JsAlert(UserID)
                'JsAlert(UserEmail)
                'JsAlert(USerPhone)
                'JsAlert(Tab1_txtNotes.Text)
                'JsAlert(CustomerName)
                'JsAlert(CardCvv2)
                'JsAlert(CardExpDate)

                'JsAlert(CardNo)
                'JsAlert(TransID)
                'JsAlert(Replace(Tab1_txtPrice.Text, ".", ","))
                'JsAlert(CardDescrID)

                'Response.Write(CardExpDate)
                'Response.End()

                obj.Public = TransPublic

                obj.MerchantId = Me.LivePayID 'Session("NewPayment_Merchant_LivePay_ID") ' Me.MerchantID ----- Na Bro To Merchant ID to kanoniko
                obj.CustomerId = UserID
                obj.CustomerEmail = UserEmail
                obj.CustomerTelephone = USerPhone
                obj.CustomerComments = Tab1_txtNotes.Text
                obj.CustomerName = CustomerName
                If String.IsNullOrEmpty(CardCvv2) Then obj.CardCvv2 = 111 Else obj.CardCvv2 = CardCvv2
                obj.CardExpiryDate = CardExpDate
                obj.CardNumber = CardNo
                obj.TransactionId = TransID
                obj.TransactionAmount = Price 'Replace(Tab1_txtPrice.Text, ".", ",") '----
                obj.CardType = CardDescrID
                obj.Installments = Me.hiddenInstallments.Text

                'Response.Write("Installments" & Me.hiddenInstallments.Text)
                'Response.End()

                'Response.Write("Me.hiddenInstallments.Text -> " & Me.hiddenInstallments.Text)

                'Dim ds As DataSet = DBConnection.GetMerchantCustomFields(Me.MerchantID)
                Dim ds As DataSet = DBConnection.GetMerchantCustomFields(_DocumentTreeNode.NodeID)
                Dim dt As DataTable = ds.Tables(0)

                LivePayCustomField.RemoveFieldsCurrentUser(CMSContext.CurrentUser.UserID)

                Dim info1_Transaction As String = String.Empty

                Dim sbMailHandler_CustomFields As New StringBuilder()
                Dim MailHandler_Commission As String = "" & _
                                "<tr> " & _
                                    "<td width=""35%"" style=""border-bottom: #cacaca 1pt inset; border-left: #cacaca 1pt inset; padding-bottom: 3.75pt; background-color: transparent; border-top-color: #f0f0f0; padding-left: 3.75pt; width: 35%; padding-right: 3.75pt; border-right: #cacaca 1pt inset; padding-top: 3.75pt""> " & _
                                    "<div><b><span style=""color: #686868; font-size: 7.5pt"">" & ResHelper.LocalizeString("{$=Προμήθεια (&euro;)|en-us=Commission (&euro;)$}") & " : </span></b></div> " & _
                                    "</td> " & _
                                    "<td style=""border-bottom: #cacaca 1pt inset; padding-bottom: 3.75pt; background-color: transparent; border-top-color: #f0f0f0; padding-left: 3.75pt; padding-right: 3.75pt; border-left-color: #f0f0f0; border-right: #cacaca 1pt inset; padding-top: 3.75pt""> " & _
                                    "<div><b><span style=""color: #686868; font-size: 7.5pt"">{0}</span></b></div> " & _
                                    "</td> " & _
                                "</tr> "

                For i As Integer = 0 To dt.Rows.Count - 1


                    Dim NameFieldEn As String = ValidationHelper.GetString(dt.Rows(i)("NameFieldEn").ToString, "")
                    Dim NameFieldGr As String = ValidationHelper.GetString(dt.Rows(i)("NameFieldGr").ToString, "")
                    Dim MailTemplate_CustomFields As String = "" & _
                                "<tr> " & _
                                    "<td width=""35%"" style=""border-bottom: #cacaca 1pt inset; border-left: #cacaca 1pt inset; padding-bottom: 3.75pt; background-color: transparent; border-top-color: #f0f0f0; padding-left: 3.75pt; width: 35%; padding-right: 3.75pt; border-right: #cacaca 1pt inset; padding-top: 3.75pt""> " & _
                                    "<div><b><span style=""color: #686868; font-size: 7.5pt"">{0} : </span></b></div> " & _
                                    "</td> " & _
                                    "<td style=""border-bottom: #cacaca 1pt inset; padding-bottom: 3.75pt; background-color: transparent; border-top-color: #f0f0f0; padding-left: 3.75pt; padding-right: 3.75pt; border-left-color: #f0f0f0; border-right: #cacaca 1pt inset; padding-top: 3.75pt""> " & _
                                    "<div><b><span style=""color: #686868; font-size: 7.5pt"">{1}</span></b></div> " & _
                                    "</td> " & _
                                "</tr> "
                    Dim ControlID As String = String.Concat("formUserControl_", i)
                    Dim MyControl As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID), LivePayFormEngineUserControl)
                    Dim ItemID As Integer = dt.Rows(i)("ItemID")
                    If Not IsNothing(MyControl) Then
                        Select Case i
                            Case 0
                                info1_Transaction = MyControl.Value
                                obj.Info1 = MyControl.Value
                                sbMailHandler_CustomFields.AppendFormat(MailTemplate_CustomFields, ResHelper.LocalizeString("{$=" & NameFieldGr & "|en-us=" & NameFieldEn & "$}"), MyControl.Value)
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 1
                                obj.Info2 = MyControl.Value
                                sbMailHandler_CustomFields.AppendFormat(MailTemplate_CustomFields, ResHelper.LocalizeString("{$=" & NameFieldGr & "|en-us=" & NameFieldEn & "$}"), MyControl.Value)
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 2
                                obj.Info3 = MyControl.Value
                                sbMailHandler_CustomFields.AppendFormat(MailTemplate_CustomFields, ResHelper.LocalizeString("{$=" & NameFieldGr & "|en-us=" & NameFieldEn & "$}"), MyControl.Value)
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 3
                                obj.Info4 = MyControl.Value
                                sbMailHandler_CustomFields.AppendFormat(MailTemplate_CustomFields, ResHelper.LocalizeString("{$=" & NameFieldGr & "|en-us=" & NameFieldEn & "$}"), MyControl.Value)
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 4
                                obj.Info5 = MyControl.Value
                                sbMailHandler_CustomFields.AppendFormat(MailTemplate_CustomFields, ResHelper.LocalizeString("{$=" & NameFieldGr & "|en-us=" & NameFieldEn & "$}"), MyControl.Value)
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 5
                                obj.Info6 = MyControl.Value
                                sbMailHandler_CustomFields.AppendFormat(MailTemplate_CustomFields, ResHelper.LocalizeString("{$=" & NameFieldGr & "|en-us=" & NameFieldEn & "$}"), MyControl.Value)
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 6
                                obj.Info7 = MyControl.Value
                                sbMailHandler_CustomFields.AppendFormat(MailTemplate_CustomFields, ResHelper.LocalizeString("{$=" & NameFieldGr & "|en-us=" & NameFieldEn & "$}"), MyControl.Value)
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 7
                                obj.Info8 = MyControl.Value
                                sbMailHandler_CustomFields.AppendFormat(MailTemplate_CustomFields, ResHelper.LocalizeString("{$=" & NameFieldGr & "|en-us=" & NameFieldEn & "$}"), MyControl.Value)
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 8
                                obj.Info9 = MyControl.Value
                                sbMailHandler_CustomFields.AppendFormat(MailTemplate_CustomFields, ResHelper.LocalizeString("{$=" & NameFieldGr & "|en-us=" & NameFieldEn & "$}"), MyControl.Value)
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 9
                                obj.Info10 = MyControl.Value
                                sbMailHandler_CustomFields.AppendFormat(MailTemplate_CustomFields, ResHelper.LocalizeString("{$=" & NameFieldGr & "|en-us=" & NameFieldEn & "$}"), MyControl.Value)
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                        End Select
                    End If
                Next
                Dim IsValidCard As Boolean = True
                If CardBelongToOtherUser.Text <> "0" AndAlso Tab2_RbSavedCard.Checked = False Then
                    IsValidCard = False
                End If

                If IsValidCard Then

                    If 1 = 1 Then

                        Dim res As LivePay_ESBBridge.PaymentResponse = w.MakePayment(obj)
                        'Response.Write("ErrorDetails -> " & res.ErrorDetails)
                        'Response.Write("ErrorMessage -> " & res.ErrorMessage)
                        'Response.End()


                        Dim Result As Boolean = res.Result

                        If Result Then '---- Success
                            Dim TransactionsObj As New GetTransactionsObj
                            'Response.Write("Price  --> " & Price)
                            'Response.End()
                            TransactionsObj.CardNumber = CardNo
                            'TransactionsObj.CustomerId = UserID
                            TransactionsObj.AmountFrom = Price
                            TransactionsObj.amountTo = Price
                            TransactionsObj.MerchantId = Me.LivePayID
                            TransactionsObj.TransactionDateFrom = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ssK")
                            TransactionsObj.TransactionDateTo = DateTime.Now.AddDays(4).ToString("yyyy-MM-ddTHH:mm:ssK")
                            TransactionsObj.info1 = info1_Transaction
                            TransactionsObj.PageSize = 10

                            Dim TransactionsResp As New GetTransactionsResponse

                            TransactionsResp = w.GetTransactions(TransactionsObj)
                            If TransPublic Then
                                If TransactionsResp.Transactions IsNot Nothing Then
                                    If TransactionsResp.Transactions.Length > 0 Then
                                        PublicTransID = TransactionsResp.Transactions(0).transactionId()
                                    End If
                                End If
                            End If

                            DBConnection.UpdateTransactionWithStatusMes(TransID, 2, Me.TransactionID, "Success", PublicTransID)
                            'Response.Write("PublicTransID - > " & PublicTransID)
                            'Response.End()
                            Dim MailHandler As New MailContext()

                            MailHandler.UserPhone = USerPhone
                            MailHandler.Transdate = Now
                            If TransPublic Then
                                'MailHandler.Transcode = "---"
                            Else
                                MailHandler.Transcode = "01" & TransID.ToString()
                            End If
                            MailHandler.MerchantName = _DocumentTreeNode.GetValue("DiscreetTitle")
                            MailHandler.MerchantID = Me.LivePayID 'Session("NewPayment_Merchant_LivePay_ID")
                            MailHandler.InvoiceBool = IIf(Tab2_ChlIWantInvoice.Checked, ResHelper.LocalizeString("{$=ΝΑΙ|en-us=YES$}"), ResHelper.LocalizeString("{$=ΟΧΙ|en-us=NO$}"))
                            MailHandler.CustomFields = sbMailHandler_CustomFields.ToString()
                            If TransPublic Then
                                MailHandler.Commission = String.Format(MailHandler_Commission, Me.ResultGetCommission.ToString("#0.00").PadLeft(3).Replace(",", "."))
                            End If

                            MailHandler.Comments = Tab1_txtNotes.Text
                            MailHandler.CardType = IIf(CardDescrID = 0, "Visa", IIf(CardDescrID = 1, "MasterCard", "EuroLine"))
                            MailHandler.CardNumber = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                            MailHandler.CardName = CustomerName
                            MailHandler.CardExpDate = CardExpDate
                            MailHandler.CardEmail = UserEmail
                            MailHandler.Amount = Tab1_txtPrice.Text
                            MailHandler.Installments = IIf(hiddenInstallments.Text > 1, hiddenInstallments.Text, ResHelper.LocalizeString("{$=ΟΧΙ|en-us=NO$}"))

                            MailHandler.SendEmail(IIf(TransPublic, "USERCARDSUCCPUBLIC", "USERCARDSUCC") & ResHelper.LocalizeString("{$=|en-us=_en$}"), "info@livepay.gr", UserEmail)


                            MailHandler.UserPhone = USerPhone
                            MailHandler.Transdate = Now
                            If TransPublic Then
                                'MailHandler.Transcode = "---"
                            Else
                                MailHandler.Transcode = "01" & TransID.ToString()
                            End If
                            MailHandler.MerchantName = _DocumentTreeNode.GetValue("DiscreetTitle")
                            MailHandler.MerchantID = Me.LivePayID 'Session("NewPayment_Merchant_LivePay_ID")
                            Dim boolString As String = "NAI"
                            If Not Tab2_ChlIWantInvoice.Checked Then
                                boolString = "ΟΧΙ"
                            End If
                            MailHandler.InvoiceBool = boolString
                            MailHandler.CustomFields = sbMailHandler_CustomFields.ToString()
                            If TransPublic Then
                                MailHandler.Commission = String.Format(MailHandler_Commission, Me.ResultGetCommission.ToString("#0.00").PadLeft(3).Replace(",", "."))
                            End If

                            MailHandler.Comments = Tab1_txtNotes.Text
                            MailHandler.CardType = IIf(CardDescrID = 0, "Visa", IIf(CardDescrID = 1, "MasterCard", "EuroLine"))
                            MailHandler.CardNumber = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                            MailHandler.CardName = CustomerName
                            MailHandler.CardExpDate = CardExpDate
                            MailHandler.CardEmail = UserEmail
                            MailHandler.Amount = Tab1_txtPrice.Text

                            MailHandler.SendEmail(IIf(TransPublic, "MERCHCARDSUCCPUBLIC", "MERCHCARDSUCC") & ResHelper.LocalizeString("{$=|en-us=_en$}"), "info@livepay.gr", GetMerchantUserEmail())


                            Session("UserTransactions") = Session("UserTransactions") + 1
                            Session("MakePaymentError") = String.Empty

                            '--- Save New Card
                            If Tab2_ChkSaveDetails.Checked AndAlso Tab2_RbNewCard.Checked Then

                                Dim dtNewCard As DataTable = DBConnection.InsertNewCard(UserID, Tab2_txtFriendName.Text, CardDescrID, EncryptedCard, CustomerName, ExpMonth, ExpYear, Right(CardNo, 4), CardNo)
                                If dtNewCard.Rows.Count > 0 Then
                                    CardID = dtNewCard.Rows(0)("NewCardID").ToString
                                End If
                            End If

                            'Response.Write("PublicTransID --> " & PublicTransID)
                            'Response.End()
                            If Request.Url.ToString.ToLower.Contains("appiphone") Then
                                Response.Redirect("~/AppiPhone/TransactionReceipt.aspx?TransID=" & TransGUID.ToString)
                            ElseIf BrowserHelper.IsMobileDevice() Then
                                Response.Redirect("~/Mobile/TransactionReceipt.aspx?TransID=" & TransGUID.ToString)
                            Else
                                Response.Redirect("~/TransactionReceipt.aspx?TransID=" & TransGUID.ToString)
                            End If
                        Else '--- Failed

                            Dim MailHandler As New MailContext()

                            MailHandler.UserPhone = USerPhone
                            MailHandler.Transdate = Now

                            If TransPublic Then
                                MailHandler.Transcode = "---"
                            Else
                                MailHandler.Transcode = "01" & TransID.ToString()
                            End If
                            MailHandler.MerchantName = _DocumentTreeNode.GetValue("DiscreetTitle")
                            MailHandler.MerchantID = Me.LivePayID 'Session("NewPayment_Merchant_LivePay_ID")
                            Dim boolString As String = "NAI"
                            If Not Tab2_ChlIWantInvoice.Checked Then
                                boolString = "ΟΧΙ"
                            End If
                            MailHandler.InvoiceBool = boolString
                            MailHandler.CustomFields = sbMailHandler_CustomFields.ToString()
                            If TransPublic Then
                                MailHandler.Commission = String.Format(MailHandler_Commission, Me.ResultGetCommission.ToString("#0.00").PadLeft(3).Replace(",", "."))
                            End If

                            MailHandler.Comments = Tab1_txtNotes.Text
                            MailHandler.CardType = IIf(CardDescrID = 0, "Visa", IIf(CardDescrID = 1, "MasterCard", "EuroLine"))
                            MailHandler.CardNumber = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                            MailHandler.CardName = CustomerName
                            MailHandler.CardExpDate = CardExpDate
                            MailHandler.CardEmail = UserEmail
                            MailHandler.Amount = Tab1_txtPrice.Text

                            MailHandler.SendEmail(IIf(TransPublic, "USERACCFAILPUBLIC", "USERACCFAIL") & ResHelper.LocalizeString("{$=|en-us=_en$}"), "info@livepay.gr", UserEmail)

                            DBConnection.UpdateTransactionWithStatusMes(TransID, 1, Me.TransactionID, res.ErrorCode & " : " & res.ErrorMessage, PublicTransID)
                            Session("MakePaymentError") = res.ErrorCode

                            If Request.Url.ToString.ToLower.Contains("appiphone") Then
                                Response.Redirect("~/AppiPhone/TransactionReceipt.aspx?Result=ESBerror")
                            ElseIf BrowserHelper.IsMobileDevice() Then
                                Response.Redirect("~/Mobile/TransactionReceipt.aspx?Result=ESBerror")
                            Else
                                Response.Redirect("~/TransactionReceipt.aspx?Result=ESBerror")
                            End If
                        End If
                    End If
                Else
                    Dim MailHandler As New MailContext()

                    MailHandler.UserPhone = USerPhone
                    MailHandler.Transdate = Now

                    If TransPublic Then
                        'MailHandler.Transcode = "---"
                    Else
                        MailHandler.Transcode = "01" & TransID.ToString()
                    End If
                    MailHandler.MerchantName = _DocumentTreeNode.GetValue("DiscreetTitle")
                    MailHandler.MerchantID = Me.LivePayID 'Session("NewPayment_Merchant_LivePay_ID")
                    Dim boolString As String = "NAI"
                    If Not Tab2_ChlIWantInvoice.Checked Then
                        boolString = "ΟΧΙ"
                    End If
                    MailHandler.InvoiceBool = boolString
                    MailHandler.CustomFields = sbMailHandler_CustomFields.ToString()
                    If TransPublic Then
                        MailHandler.Commission = String.Format(MailHandler_Commission, Me.ResultGetCommission.ToString("#0.00").PadLeft(3).Replace(",", "."))
                    End If

                    MailHandler.Comments = Tab1_txtNotes.Text
                    MailHandler.CardType = IIf(CardDescrID = 0, "Visa", IIf(CardDescrID = 1, "MasterCard", "EuroLine"))
                    MailHandler.CardNumber = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                    MailHandler.CardName = CustomerName
                    MailHandler.CardExpDate = CardExpDate
                    MailHandler.CardEmail = UserEmail
                    MailHandler.Amount = Tab1_txtPrice.Text

                    MailHandler.SendEmail(IIf(TransPublic, "USERACCFAILPUBLIC", "USERACCFAIL") & ResHelper.LocalizeString("{$=|en-us=_en$}"), "info@livepay.gr", UserEmail)

                    Dim UserError As String = ResHelper.LocalizeString(String.Concat("{$=Η κάρτα έχει αποθηκευτεί από άλλο χρήστη|en-us=Η κάρτα έχει αποθηκευτεί από άλλο χρήστη$}"))

                    DBConnection.UpdateTransactionWithStatusMes(TransID, 1, Me.TransactionID, UserError, PublicTransID) '------ Update Transaction Set Status = 1 (Failed)
                    Session("MakePaymentError") = UserError
                    If Request.Url.ToString.ToLower.Contains("appiphone") Then
                        Response.Redirect("~/AppiPhone/TransactionReceipt.aspx?Result=CatchError")
                    ElseIf BrowserHelper.IsMobileDevice() Then
                        Response.Redirect("~/Mobile/TransactionReceipt.aspx?Result=CatchError")
                    Else
                        Response.Redirect("~/TransactionReceipt.aspx?Result=CatchError")
                    End If
                End If


            Catch ex As Exception
                CatchError = "PageEx" 'ex.ToString
            End Try

            If CatchError <> String.Empty Then
                DBConnection.UpdateTransactionWithStatusMes(TransID, 1, Me.TransactionID, CatchError, PublicTransID) '------ Update Transaction Set Status = 1 (Failed)
                Session("MakePaymentError") = CatchError

                If Request.Url.ToString.ToLower.Contains("appiphone") Then
                    Response.Redirect("~/AppiPhone/TransactionReceipt.aspx?Result=CatchError")
                ElseIf BrowserHelper.IsMobileDevice() Then
                    Response.Redirect("~/Mobile/TransactionReceipt.aspx?Result=CatchError")
                Else
                    Response.Redirect("~/TransactionReceipt.aspx?Result=CatchError")
                End If
            End If



            'Dim ErrorMessage As String = String.Concat("<font color='red'>", res.ErrorCode, "</font>:", res.ErrorMessage)

        End If
    End Sub

    Private Function GetMerchantUserEmail() As String
        Dim LivePayID As Integer = _DocumentTreeNode.GetValue("LivePayID")

        Dim ds As DataSet = UserInfoProvider.GetUsers("LivePayID = " & LivePayID, "UserCreated", 1, "Email", ConnectionHelper.GetConnection())

        If Not DataHelper.DataSourceIsEmpty(ds) Then
            Return ds.Tables(0).Rows(0)("Email")
        Else
            Return _DocumentTreeNode.GetValue("Email")
        End If

    End Function

    Private Sub JsAlert(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, String.Concat(" alert('", Script, "'); "), True)
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

    Private Function GetTypeCard(ByVal where As String, ByVal orderby As String) As DataSet
        Dim customTableClassName As String = "LivePay.TypeCard"

        ' Get data class using custom table name
        Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass(customTableClassName)

        If customTableClassInfo Is Nothing Then
            Throw New Exception("Given custom table does not exist.")
        End If

        ' Initialize custom table item provider with current user info and general connection
        Dim ctiProvider As New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())

        ' Get custom table items
        Dim dsItems As DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, where, orderby)

        Return dsItems

    End Function

    Private Sub GetInstallments()
        Dim cardType As StringBuilder = New StringBuilder()
        Dim MinInstallments_merchant As Integer = _DocumentTreeNode.GetValue("MinInstallments")
        Dim MaxInstallments_merchant As Integer = _DocumentTreeNode.GetValue("MaxInstallments")


        Dim dsItems As DataSet = GetTypeCard("active = 1", "MinInstallments")

        Dim MinInstallments_cards As Integer = 0

        For Each row As DataRow In dsItems.Tables(0).Rows
            If cardType.Length > 0 Then cardType.Append(", ")

            If Int32.TryParse(ValidationHelper.GetInteger(row("MinInstallments").ToString(), 0), MinInstallments_cards) AndAlso MinInstallments_cards > 1 AndAlso ValidationHelper.GetInteger(row("MaxInstallments").ToString(), 0) >= MinInstallments_cards Then
                cardType.AppendFormat("{{""index"":{0}, min: ""{1}"", max: ""{2}""}}", row("itemid").ToString.ToLower(), row("MinInstallments").ToString, row("MaxInstallments").ToString)
            Else
                cardType.AppendFormat("{{""index"":{0}, min: ""{1}"", max: ""{2}""}}", row("itemid").ToString.ToLower(), "", "")
            End If
        Next



        Dim merchantInstallments As String = String.Empty
        If MaxInstallments_merchant >= MinInstallments_merchant Then
            merchantInstallments = String.Format("{{min: {0}, max: {1}}}", MinInstallments_merchant, MaxInstallments_merchant)
        Else
            merchantInstallments = String.Format("{{min: {0}, max: {1}}}", 0, 0)
        End If
        Dim Installments As String = String.Format("var _installment = {{merchant: {0}, cardtype:{1}}}; ", merchantInstallments, String.Concat("[", cardType.ToString(), "]"))
        'Response.Write(Installments)
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), Guid.NewGuid().ToString(), Installments, True)

        'String.Format("var Installments = {merchant:{min:1, max:5}, cardtype:{visa:{min:1, max:20}, master:{min:20, max:25}}}")

        'Dim MinInstallments_merchant As Integer = _DocumentTreeNode.GetValue("MinInstallments")
        'Dim MaxInstallments_merchant As Integer = _DocumentTreeNode.GetValue("MaxInstallments")

        'Dim customTableClassName As String = "Merchant.LivePay_Merchant"

        '' Get data class using custom table name
        'Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass(customTableClassName)

        'If customTableClassInfo Is Nothing Then
        '    Throw New Exception("Given custom table does not exist.")
        'End If

        '' Initialize custom table item provider with current user info and general connection
        'Dim ctiProvider As New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())

        '' Get custom table items
        'Dim dsItems As DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, "active = 1 and ItemID = " & Tab2_ddlCardType.SelectedIndex, "MinInstallments")



        'Dim MinInstallments_Card As Integer = _DocumentTreeNode.GetValue("MinInstallments")
        'Dim MaxInstallments_Card As Integer = _DocumentTreeNode.GetValue("MaxInstallments")


    End Sub


    ''' <summary>
    ''' Create custom form control
    ''' </summary>
    ''' <param name="pathToControl">Path custom control</param>
    ''' <param name="CustomTextBoxDiv"></param>
    ''' <param name="i">Row Data Table</param>
    ''' <remarks></remarks>
    Private Sub CreateCustomFormControl(ByVal pathToControl As String, _
                                        ByVal CustomTextBoxDiv As Object, _
                                        ByVal i As Integer, _
                                        ByVal ErrorMsgGr As String, _
                                        ByVal ErrorMsgEn As String, _
                                        ByVal RegExp As String, _
                                        ByVal Mandatory As Boolean, _
                                        ByVal MaxLength As Integer, _
                                        ByVal TypePayment As Integer, _
                                        ByVal OrganismCode As Integer, _
                                        ByVal NDate As String, _
                                        ByVal CustomFieldValue As String)


        If File.Exists(HttpContext.Current.Request.MapPath(ResolveUrl(pathToGroupselector))) Then
            Dim CustomFT As New CustomFieldsType()
            Dim ctrl As LivePayFormEngineUserControl = Me.LoadControl(pathToControl)
            If ctrl IsNot Nothing Then
                Dim strError As String = "CustomCloud_"
                ctrl = Me.LoadControl(pathToControl)
                ctrl.ID = "formUserControl_" & i
                ctrl.ValidationError = ResHelper.LocalizeString(String.Concat("{$=Συμπληρώστε το πεδίο <b>", ErrorMsgGr, "</b>|en-us=Complete the field <b>", ErrorMsgEn, "</b>$}"))
                ctrl.RowCustomField = i
                ctrl.RegularExp = RegExp
                ctrl.Mandatory = Mandatory
                ctrl.MaxLength = MaxLength
                ctrl.TypePayment = TypePayment
                ctrl.OrganismCode = OrganismCode
                ctrl.NDate = NDate

                If CustomFieldValue <> String.Empty Then
                    ctrl.Value = CustomFieldValue
                End If


                CustomFT.TypeName = ctrl.ID.ToString()
                CustomFT.ErrorDivID = strError & i

                FieldType.Add(CustomFT)

                CustomTextBoxDiv.Controls.Add(ctrl)
            End If
        End If


    End Sub
End Class


