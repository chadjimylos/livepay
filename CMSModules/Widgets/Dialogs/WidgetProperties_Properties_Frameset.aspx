<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WidgetProperties_Properties_Frameset.aspx.cs"
    Inherits="CMSModules_Widgets_Dialogs_WidgetProperties_Properties_Frameset" 
    EnableEventValidation="false" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Widget Properties</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }
    </style>

    <script type="text/javascript">
        //<![CDATA[
        var wopener = parent.wopener;

        function ChangeWidget(zoneId, widgetId, aliasPath) {
            parent.ChangeWidget(zoneId, widgetId, aliasPath);
        }

        function RefreshPage() {
            wopener.RefreshPage();
        }

        //]]>
    </script>

</head>
<frameset border="0" rows="*,43" runat="server" id="rowsFrameset">
    <frame name="widgetpropertiescontent" frameborder="0" noresize="noresize" scrolling="no" runat="server" id="frameContent" />
    <frame name="widgetpropertiesbuttons" frameborder="0" noresize="noresize" scrolling="no" runat="server" id="frameButtons" />
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
</frameset>
</html>
