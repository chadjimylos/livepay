<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewsLetter.ascx.vb" Inherits="CMSWebParts_AegeanPower_NewsLetter" %>



<asp:UpdatePanel ID="Update" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
<ContentTemplate>
    <div id="NewsLetter">
    <div class="newsletter-content" id="DivNewsletterContent" runat="server" visible="true">
        <div class="NewsLetterText">
            <asp:Label runat="server" ID="lbNewsLetterText"></asp:Label>
			<asp:RequiredFieldValidator id="RequiredFieldValidatoremail" runat="server" ValidationGroup="CustomerData" ControlToValidate="tbEmail"
				ErrorMessage="�������� ����������� �� �����." Display="dynamic" />
			<asp:RegularExpressionValidator id="RegularValidatorEmail" ValidationGroup="CustomerData" 
					ControlToValidate="tbEmail"
					ValidationExpression="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$"
					Display="Dynamic"
					ErrorMessage="�������� �������� ��� ������ ��������� email."
					runat="server"/>
        </div>
        <div class="BoxClick">
            <div class="NewsLetterBox">
                <div class="NewsLetterBoxText">
                    <asp:TextBox ID="tbEmail" runat="server" Text="E-mail" Width="115" CssClass="tb-newsletter-email" onclick="email(this);"></asp:TextBox>
                </div>
                <div class="OKclick">
                    <div class="left"><asp:LinkButton runat="server" Text="ok" id="submit"></asp:LinkButton></div>
                    <div class="okbuttonnewsletter"><img src="../../App_Themes/AegeanPower/Images/NewsLetterArrowclick.gif"alt="" border="0" /></div>
                </div>
            </div>
        </div>
    </div>
    <div class="newsletter-content" id="DivNewsletterContentSubmit" runat="server" visible="false">
        <asp:Label runat="server" ID="lbNewsLetterMsg"></asp:Label>
    </div>
    <div class="newsletter-click" id="newsletter-click" onmouseover="$(this).toggleClass('newsletter-clickSelected');" onmouseout="$(this).toggleClass('newsletter-clickSelected');"><strong>NewsLetter</strong></div>
    </div>
</ContentTemplate>
<Triggers>
	<asp:AsyncPostBackTrigger ControlID="submit" />
</Triggers>
</asp:UpdatePanel>

  <script type="text/javascript">
      function email(el) {
          el.value = '';
      }
      $(document).ready(function () {

          Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SaveNewsLetter)

          function SaveNewsLetter(sender, args) {
              var DivNewsletterContent = '<%= DivNewsletterContent.ClientID %>';
              $("#newsletter-click").click(function () {
                  var DivPos = document.getElementById('NewsLetter').offsetLeft

                  if (DivPos == "-213") {
                      $("#NewsLetter").animate({ left: 0 }, 1000);
                  }
                  else {
                      $("#NewsLetter").animate({ left: -213 }, 1000);
                  }
              });
          }

          $("#newsletter-click").click(function () {
              var DivPos = document.getElementById('NewsLetter').offsetLeft

              if (DivPos == "-213") {
                  $("#NewsLetter").animate({ left: 0 }, 1000);
              }
              else {
                  $("#NewsLetter").animate({ left: -213 }, 1000);
              }
          });
      });
  </script>
  <asp:Literal ID="ltlScript" runat="server" />