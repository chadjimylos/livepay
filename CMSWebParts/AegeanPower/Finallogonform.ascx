﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/AegeanPower/Finallogonform.ascx.cs"
    Inherits="CMSWebParts_AegeanPower_Finallogonform" %>

    <style type="text/css" >
        .DialogPosition{
	        margin-top:10px;
        }
        .LogonDialog{

        }
        .logon-form
        {
            font-size:11px;
            color:#fff;
            float:left;
            width:212px;
            margin-left:15px;
        }
        .logon-form a
        {
            font-size:11px;
            color:#fff;
        }
        .logon-form-header .label
        {
            font-size:12px;
            font-weight:bold;
            padding:8px 10px 0px 10px;
        }
        .logon-form-body
        {            
	        width:212px;
	        float:left;
	        color:Red;
        }
        .logon-form-username
        {
            padding-top:10px;
        }
        .logon-form-username .label
        {
            float:left;
            width:52px;
            line-height:21px;
            font-weight:bold;
        }
        .fullname
        {
            font-weight:bold;
            color:#004990;
            float:left;
            width:170px;
        }
        
        .fullname a{text-decoration:none;color:#004990;}
        .logon-form-password
        {
            padding-top:5px;
        }
        .logon-form-password .label
        {
            float:left;
            width:52px;
            line-height:21px;
            font-weight:bold;
        }
        .logon-form-links
        {
            padding-top:7px;
        }
        .logon-form-time
        {
            padding-top:7px;
            padding-bottom:7px;
        }
        .logon-form-time .label
        {
            float:left;
        }
        .logon-form-time .time
        {
            float:left;
            padding-left:5px;
        }
        .logon-form-submit input
        {
            background-image:url(/App_Themes/AegeanPower/Images/EnterButton.jpg);
            background-position:left;
            background-repeat:no-repeat;
            cursor:pointer;
            width:128px;
            height:27px;
            float:left;
            margin-top:5px;
            margin-left:50px;
            border:0px;
            padding:0px 0px 3px 0px;
            color:white;
        }
        .logon-form-logout input
        {
            width:105px;
            height:23px;
            border:none;
            color:red;
            font-weight:bold;
            cursor:pointer;
        }
        .WelcomeMessage{margin:0px 9px 11px 0px; width:170px; float:left;}
        .WelcomeMessage1{margin:5px 9px 0px 0px; width:170px; color:#515151; font-family:Tahoma; font-size:14px; font-weight:bold; float:left;}
        .WelcomeMessage2{margin:0px 9px 0px 0px; width:170px; color:#515151; font-family:Tahoma; font-size:14px; float:left;}
        .UserName{color:#004990;margin-left:0px;float:left;width:170px;}
        .logon-form-logout .signoutButton{color:Green;width:92px;padding:0px;border:0px;background-color:#f0f0f4;color:#004990; font-family:Tahoma; font-size:11px; float:left;text-decoration:underline;margin:19px 0px 0px 0px;}
        .NewCustomer a {text-decoration:none;color:#0049A1;}   
        #FailureText{color:red;float:left;width:185px;} 
    </style>

<asp:Panel ID="Panel_Logout" runat="server" CssClass="LogoutPageBackground" Visible="false">
    <div class="logon-form">
        <div class="logon-form-body">
        <div class="WelcomeMessage">
            <asp:Label ID="lblWelcomeMessage1" runat="server" CssClass="WelcomeMessage1" EnableViewState="false" />
            <asp:Label ID="lblWelcomeMessage2" runat="server" CssClass="WelcomeMessage2" EnableViewState="false" />
        </div>
        <div class="UserName"><asp:Label ID="lblUserNameText" runat="server" CssClass="UserNamecss" EnableViewState="false" /></div>
                <div class="fullname"><a id="LinkToChange" runat="server" href="~/Home-Page/Customer-Center/Change-your-Personal-Information.aspx"><asp:Label ID="lblFullName" runat="server" CssClass="CurrentUserLabel" EnableViewState="false" /></a></div>
            <div class="logon-form-logout">
               <cms:CMSButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click" CssClass="signoutButton" EnableViewState="false" />
            </div>
        <div class="Clear"></div>
        </div>
    </div>
</asp:Panel>

<asp:Panel ID="Panel1" runat="server" CssClass="LogonPageBackground">
    <div class="logon-form">
        <div class="logon-form-body">
                <asp:Login ID="Login1" runat="server" DestinationPageUrl="~/Default.aspx">
                    <LayoutTemplate>
                        <asp:Panel runat="server" ID="pnlLogin" DefaultButton="LoginButton" Width="212">
                        
                         <div style="margin-top:8px;" class="enter">
                <div class="TextSize"><cms:LocalizedLabel ID="lblUserName" runat="server" AssociatedControlID="UserName" EnableViewState="false" /></div>
                <div class="enterbox"><asp:TextBox ID="UserName" runat="server" EnableViewState="false" CssClass="LogonField" /></div>
                <asp:RequiredFieldValidator ID="rfvUserNameRequired" runat="server" ControlToValidate="UserName"
                                    Display="Dynamic" EnableViewState="false" ValidationGroup="LoginBox">*</asp:RequiredFieldValidator>
                        </div>
                        <div class="enter">
            <div class="TextSize"><cms:LocalizedLabel ID="lblPassword" runat="server" AssociatedControlID="Password" EnableViewState="false" /></div>
            <div class="enterbox"><asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="LogonField" EnableViewState="false" /></div>
            </div><div class="clear"></div>
                        <%--<div id="labelerrormessage">--%>
                        <cms:LocalizedLiteral ID="FailureText" runat="server" EnableViewState="False" />
                        <%--</div>--%>   
                        <div class="clear"></div>
                        <div class="logon-form-submit">
                            <cms:LocalizedButton ID="LoginButton" runat="server" CommandName="Login" ValidationGroup="Login1"
                                EnableViewState="false" />      
                        </div>


                        </asp:Panel>
                    </LayoutTemplate>
                </asp:Login>
        <div>
                <asp:LinkButton ID="lnkPasswdRetrieval" runat="server" EnableViewState="false" ValidationGroup="LoginBox" />
        </div>
        <div class="Questions">
                            <div class="NewCustomer"><a href="~/New-Customer-(1)/New-Customer.aspx"><asp:Label ID="lblNewCustomer" runat="server" EnableViewState="false" /></a></div>
                            <div class="NewCustomer"><a href="~/Home-Page/General-Information/Retrieve-Password.aspx"><asp:Label ID="lblForgotPassword" runat="server" EnableViewState="false" /></a></div>
        </div>

        </div><!-- Class End Body -->
        <div class="logon-form-bottom"></div>

    </div>
</asp:Panel>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
<asp:HiddenField runat="server" ID="hdnPasswDisplayed" />