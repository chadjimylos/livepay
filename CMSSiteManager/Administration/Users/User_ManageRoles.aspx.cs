using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;


public partial class CMSSiteManager_Administration_Users_User_ManageRoles : CMSModalPage
{
    #region "Variables"

    private int userId = 0;
    private int siteId = 0;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check administration
        CheckAdministration();

        // Check permissions
        CurrentUserInfo user = CMSContext.CurrentUser;
        if (user != null)
        {
            if (!user.IsAuthorizedPerUIElement("CMS.Administration", "Users"))
            {
                RedirectToCMSDeskUIElementAccessDenied("CMS.Administration", "Users");
            }

            // Check "read" permission
            if (!user.IsAuthorizedPerResource("CMS.Users", "Read"))
            {
                RedirectToAccessDenied("CMS.Users", "Read");
            }
        }        

        userId = QueryHelper.GetInteger("userId", 0);

        // Check that only global administrator can edit global administrator's accouns
        UserInfo ui = UserInfoProvider.GetUserInfo(userId);
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        if ((!currentUser.IsGlobalAdministrator || (currentUser.IsGlobalAdministrator && currentUser.UserSiteManagerDisabled)) && ui != null && ui.UserIsGlobalAdministrator && (ui.UserID != currentUser.UserID))
        {
            plcTable.Visible = false;
            lblError.Text = ResHelper.GetString("Administration-User_List.ErrorGlobalAdmin");
            lblError.Visible = true;
            return;
        }

        // Only global admin can choose the site
        if ((CMSContext.CurrentUser != null && CMSContext.CurrentUser.IsGlobalAdministrator))
        {
            // Set site selector
            siteSelector.DropDownSingleSelect.AutoPostBack = true;
            siteSelector.AllowAll = false;
            siteSelector.UserId = userId;
            siteSelector.OnlyRunningSites = false;
            siteSelector.UniSelector.OnSelectionChanged += UniSelector_OnSelectionChanged;

            if (RequestHelper.IsPostBack())
            {
                siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
            }
            else
            {
                siteSelector.Value = CMSContext.CurrentSiteID;
            }
        }
        else
        {
            siteSelector.Visible = false;
            siteSelector.StopProcessing = true;
            siteId = CMSContext.CurrentSiteID;
        }

        // Init UI
        itemSelectionElem.LeftColumnLabel.Text = ResHelper.GetString("user.manageroles.availableroles");
        itemSelectionElem.RightColumnLabel.Text = ResHelper.GetString("user.manageroles.userinroles");
        btnClose.Text = ResHelper.GetString("general.close");
        CurrentMaster.Title.TitleText = ResHelper.GetString("user.manageroles.header");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Role/object.png");
        CurrentMaster.DisplayControlsPanel = true;

        // Register the events
        itemSelectionElem.OnMoveLeft += itemSelectionElem_OnMoveLeft;
        itemSelectionElem.OnMoveRight += itemSelectionElem_OnMoveRight;
    }


    protected override void OnPreRender(EventArgs e)
    {
        // Load the roles into ItemSelection Control
        if (!RequestHelper.IsPostBack())
        {
            if (siteSelector.UniSelector.HasData)
            {
                siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
            }

            LoadRoles();
            itemSelectionElem.fill();
        }

        base.OnPreRender(e);
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        LoadRoles();
        itemSelectionElem.fill();
        pnlUpdate.Update();
    }


    #region "ItemSelection events"

    protected void itemSelectionElem_OnMoveRight(object sender, CommandEventArgs e)
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify"))
        {
            RedirectToAccessDenied("CMS.Users", "Modify");
        }
        
        string argument = ValidationHelper.GetString(e.CommandArgument, "") ;
        if (!string.IsNullOrEmpty(argument))
        {
            string[] ids = argument.Split(new char[] {';'}, StringSplitOptions.RemoveEmptyEntries);
            foreach (string id in ids)
            {
                UserInfoProvider.AddUserToRole(userId, ValidationHelper.GetInteger(id, 0));
            }
        }
        
    }


    protected void itemSelectionElem_OnMoveLeft(object sender, CommandEventArgs e)
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify"))
        {
            RedirectToAccessDenied("CMS.Users", "Modify");
        }

        string argument = ValidationHelper.GetString(e.CommandArgument, "") ;
        if (!string.IsNullOrEmpty(argument))
        {
            string[] ids = argument.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string id in ids)
            {
                UserInfoProvider.RemoveUserFromRole(userId, ValidationHelper.GetInteger(id, 0));
            }
        }
    }

    #endregion


    #region "Private helper functions"

    private void LoadRoles()
    {
        itemSelectionElem.LeftItems = GetSelectionControlItems(GetRoleDataset(false));
        itemSelectionElem.RightItems = GetSelectionControlItems(GetRoleDataset(true));
    }


    /// <summary>
    /// Returns the dataset with roles for specified site.
    /// </summary>
    /// <param name="users">Determines whether to return only roles to which the user is assigned</param>
    private DataSet GetRoleDataset(bool users)
    {
        string where = "(RoleID " + (users ? "" : "NOT ") + "IN (SELECT RoleID FROM CMS_UserRole WHERE UserID = " + userId + ")) AND (SiteID=" + siteId + ") AND RoleGroupID IS NULL";

        // Exclude generic roles
        string genericWhere = null;
        ArrayList genericRoles = RoleInfoProvider.GetGenericRoles();
        if (genericRoles.Count != 0)
        {
            foreach (string role in genericRoles)
            {
                genericWhere += "'" + role.Replace("'", "''") + "',";
            }

            if (genericWhere != null)
            {
                genericWhere = genericWhere.TrimEnd(',');
            }
            where += " AND ( RoleName NOT IN (" + genericWhere + ") )";
        }

        return RoleInfoProvider.GetRoles("RoleDisplayName, RoleID", where, "RoleDisplayName", 0, null);
    }


    /// <summary>
    /// Returns the 2 dimensional array which can be used in ItemSelection control.
    /// </summary>
    private static string[,] GetSelectionControlItems(DataSet ds)
    {
        string[,] retval = null;
        if (!DataHelper.DataSourceIsEmpty(ds))
        {
            retval = new string[ds.Tables[0].Rows.Count, 2];
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                DataRow dr = ds.Tables[0].Rows[i];
                retval[i, 1] = Convert.ToString(dr["RoleDisplayName"]);
                retval[i, 0] = Convert.ToString(dr["RoleID"]);
            }
        }
        return retval;
    }

    #endregion
}
