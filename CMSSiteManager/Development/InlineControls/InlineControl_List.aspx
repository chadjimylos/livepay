<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InlineControl_List.aspx.cs"
    Inherits="CMSSiteManager_Development_InlineControls_InlineControl_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Inline controls - list" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:UniGrid runat="server" ID="UniGrid" GridName="InlineControl_List.xml" OrderBy="ControlDisplayName"
        Columns="ControlID, ControlDisplayName, ControlName" IsLiveSite="false" />
</asp:Content>
