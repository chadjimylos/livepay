<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Report_Parameters.aspx.cs"
    Inherits="CMSModules_Reporting_Tools_Report_Parameters" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/FieldEditor.ascx" TagName="FieldEditor"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <div class="PageContent">
        <asp:Label ID="lblInfo" runat="server" EnableViewState="false" CssClass="InfoLabel" />
        <cms:FieldEditor ID="editor" runat="server" />
    </div>
</asp:Content>
