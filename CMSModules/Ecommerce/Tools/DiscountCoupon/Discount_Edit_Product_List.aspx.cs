using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Tools_DiscountCoupon_Discount_Edit_Product_List : CMSEcommercePage
{
    protected int discountCouponId = 0;
    protected string currentValues = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        discountCouponId = QueryHelper.GetInteger("discountid", 0);
        if (discountCouponId > 0)
        {
            // Get the active skus
            string where = "SKUID IN (SELECT SKUID FROM COM_SKUDiscountCoupon WHERE DiscountCouponID = " + discountCouponId + ")";
            DataSet ds = DiscountCouponInfoProvider.GetCouponProducts(discountCouponId);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "SKUID"));
            }

            if (!IsPostBack)
            {
                this.uniSelector.Value = currentValues;
            }
        }

        this.uniSelector.IconPath = GetObjectIconUrl("ecommerce.sku", "object.png");
        this.uniSelector.OnSelectionChanged += uniSelector_OnSelectionChanged;

        this.lblApplies.Text = ResHelper.GetString("Discount_Product.Applies");
        this.radExcept.Text = ResHelper.GetString("Discount_Product.radExcept");
        this.radFollowing.Text = ResHelper.GetString("Discount_Product.radFollowing");

        if (!RequestHelper.IsPostBack())
        {
            DiscountCouponInfo dci = DiscountCouponInfoProvider.GetDiscountCouponInfo(discountCouponId);
            if (dci != null)
            {
                radFollowing.Checked = true;
                radExcept.Checked = false;

                if (dci.DiscountCouponIsExcluded)
                {
                    radFollowing.Checked = false;
                    radExcept.Checked = true;
                }
            }
        }
    }


    protected void uniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        SaveItems();
    }


    protected void SaveItems()
    {
        // Remove old items
        string newValues = ValidationHelper.GetString(uniSelector.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to sku
                foreach (string item in newItems)
                {
                    int skuId = ValidationHelper.GetInteger(item, 0);
                    SKUDiscountCouponInfoProvider.RemoveDiscountCouponFromSKU(skuId, discountCouponId);
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to sku
                foreach (string item in newItems)
                {
                    int skuId = ValidationHelper.GetInteger(item, 0);
                    SKUDiscountCouponInfoProvider.AddOptionCategoryToSKU(skuId, discountCouponId);
                }
            }
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }


    protected void radFollowing_CheckedChanged(object sender, EventArgs e)
    {
        // Check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        DiscountCouponInfo dci = DiscountCouponInfoProvider.GetDiscountCouponInfo(discountCouponId);

        if (dci != null)
        {
            if (radExcept.Checked)
            {
                dci.DiscountCouponIsExcluded = true;
            }
            else
            {
                dci.DiscountCouponIsExcluded = false;
            }

            DiscountCouponInfoProvider.SetDiscountCouponInfo(dci);

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }
    }
}
