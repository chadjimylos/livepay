<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UniMatrix.ascx.cs" Inherits="CMSAdminControls_UI_UniControls_UniMatrix" %>
<asp:Literal runat="server" ID="ltlContentBefore" EnableViewState="false" />
<asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" Visible="false" EnableViewState="false" />
<asp:PlaceHolder runat="server" ID="plcFilter">
    <asp:Literal runat="server" runat="server" ID="ltlBeforeFilter" EnableViewState="false" /><asp:Panel
        runat="server" ID="pnlFilter" DefaultButton="btnFilter" Visible="false">
        <cms:LocalizedLabel ID="lblFilter" runat="server" EnableViewState="false" /><asp:TextBox
            runat="server" ID="txtFilter" CssClass="ShortTextBox" /><cms:LocalizedButton runat="server"
                ID="btnFilter" OnClick="btnFilter_Click" CssClass="ShortButton" />
    </asp:Panel>
    <asp:Literal runat="server" runat="server" ID="ltlAfterFilter" EnableViewState="false" />
</asp:PlaceHolder>
<asp:Literal runat="server" ID="ltlMatrix" EnableViewState="false" />
<asp:PlaceHolder runat="server" ID="plcPager">
    <asp:Literal runat="server" runat="server" ID="ltlPagerBefore" EnableViewState="false" />
    <cms:UniPager runat="server" ID="uniPager" PageCount="10" PageSize="1" PagerMode="PostBack"
        HidePagerForSinglePage="true">
        <PreviousGroupTemplate>
            <a class="UnigridPagerPage" href="<%# Eval("PreviousGroupURL") %>">...</a>
        </PreviousGroupTemplate>
        <PageNumbersTemplate>
            <a class="UnigridPagerPage" href="<%# Eval("PageURL") %>">
                <%# Eval("Page") %></a>
        </PageNumbersTemplate>
        <PageNumbersSeparatorTemplate>
        </PageNumbersSeparatorTemplate>
        <CurrentPageTemplate>
            <span class="UnigridPagerSelectedPage">
                <%# Eval("Page") %></span>
        </CurrentPageTemplate>
        <NextGroupTemplate>
            <a class="UnigridPagerPage" href="<%# Eval("NextGroupURL") %>">...</a>
        </NextGroupTemplate>
        <LayoutTemplate>
            <div class="UnigridPagerPages">
                <asp:PlaceHolder runat="server" ID="plcPreviousGroup"></asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="plcPageNumbers"></asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="plcNextGroup"></asp:PlaceHolder>
            </div>
        </LayoutTemplate>
    </cms:UniPager>
    <asp:Literal runat="server" runat="server" ID="ltlPagerAfter" EnableViewState="false" />
</asp:PlaceHolder>
<asp:Literal runat="server" ID="ltlContentAfter" EnableViewState="false" />