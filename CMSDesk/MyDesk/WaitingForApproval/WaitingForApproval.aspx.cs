using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SiteProvider;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSDesk_MyDesk_WaitingForApproval_WaitingForApproval : CMSMyDeskPage
{
    #region "Private variables"

    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;
    private TimeZoneInfo usedTimeZone = null;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check UIProfile
        if ((CMSContext.CurrentUser == null) || (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.MyDesk", "WaitingDocs")))
        {
            RedirectToUINotAvailable();
        }

        // Setup page title text and image
        CurrentMaster.Title.TitleText = ResHelper.GetString("MyDesk.WaitingForApproval");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_MyDesk/WaitingForApproval/module.png");

        CurrentMaster.Title.HelpTopicName = "waiting_for_my_approval";
        CurrentMaster.Title.HelpName = "helpTopic";

        gridWaitingDocs.ZeroRowsText = ResHelper.GetString("mydesk.ui.nowaitingdocs");
        object[,] parameters = new object[1, 3];
        parameters[0, 0] = "@UserID";

        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        int userId = currentUser.UserID;
        if (currentUser.IsGlobalAdministrator || currentUser.IsAuthorizedPerResource("CMS.Content", "manageworkflow"))
        {
            userId = -1;
        }
        parameters[0, 1] = userId;

        // Initialize unigrid
        gridWaitingDocs.QueryParameters = parameters;
        gridWaitingDocs.WhereCondition = "NodeSiteID = " + CMSContext.CurrentSite.SiteID;
        gridWaitingDocs.OnExternalDataBound += gridWaitingDocs_OnExternalDataBound;
    }


    /// <summary>
    /// External data binding handler
    /// </summary>
    protected object gridWaitingDocs_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        int nodeId = 0;
        string culture = string.Empty;
        DataRowView data = null;
        sourceName = sourceName.ToLower();
        switch (sourceName)
        {
            case "classname":
                data = (DataRowView)parameter;
                nodeId = ValidationHelper.GetInteger(data["NodeID"], 0);
                culture = ValidationHelper.GetString(data["DocumentCulture"], string.Empty);
                string className = ValidationHelper.GetString(data["ClassName"], string.Empty);
                string imageUrl = GetDocumentTypeIconUrl(className);
                string toReturn = "<img src=\"" + imageUrl + "\" border=\"0\"/>";
                // Check permissions
                if (IsUserAuthorizedPerContent())
                {
                    toReturn = "<a href=\"javascript: SelectItem(" + nodeId + ", '" + culture + "');\">" + toReturn + "</a>";
                }

                return toReturn;

            case "documentname":
                data = (DataRowView)parameter;
                string name = ValidationHelper.GetString(data["DocumentName"], string.Empty);
                nodeId = ValidationHelper.GetInteger(data["NodeID"], 0);
                culture = ValidationHelper.GetString(data["DocumentCulture"], string.Empty);

                if (name == string.Empty)
                {
                    name = "-";
                }
                string result = string.Empty;
                if (IsUserAuthorizedPerContent())
                {
                    result = "<a href=\"javascript: SelectItem(" + nodeId + ", '" + culture + "');\">" + HTMLHelper.HTMLEncode(TextHelper.LimitLength(name, 50)) + "</a>";
                    bool isLink = (data["NodeLinkedNodeID"] != DBNull.Value);
                    if (isLink)
                    {
                        result += UIHelper.GetDocumentMarkImage(this, DocumentMarkEnum.Link);
                    }
                }
                else
                {
                    result = "<span>" + HTMLHelper.HTMLEncode(TextHelper.LimitLength(name, 50)) + "</span>";
                }

                return result;

            case "documentnametooltip":
                data = (DataRowView)parameter;
                return UniGridFunctions.DocumentNameTooltip(data);

            case "stepdisplayname":
                string stepName = ValidationHelper.GetString(parameter, string.Empty);
                if (stepName == string.Empty)
                {
                    stepName = "-";
                }
                return stepName;

            case "culture":
                DataRowView drv = (DataRowView)parameter;

                // Add icon
                return UniGridFunctions.DocumentCultureFlag(drv, this.Page);

            case "modifiedwhen":
            case "modifiedwhentooltip":
                if (string.IsNullOrEmpty(parameter.ToString()))
                {
                    return "";
                }
                else
                {
                    DateTime modifiedWhen = ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME);
                    if (currentUserInfo == null)
                    {
                        currentUserInfo = CMSContext.CurrentUser;
                    }
                    if (currentSiteInfo == null)
                    {
                        currentSiteInfo = CMSContext.CurrentSite;
                    }

                    if (sourceName == "modifiedwhen")
                    {
                        return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(modifiedWhen,
                            currentUserInfo, currentSiteInfo, out usedTimeZone);
                    }
                    else
                    {
                        if (TimeZoneHelper.TimeZonesEnabled() && (usedTimeZone == null))
                        {
                            TimeZoneHelper.GetCurrentTimeZoneDateTimeString(modifiedWhen,
                                                        currentUserInfo, currentSiteInfo, out usedTimeZone);
                        }
                        return TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone);
                    }
                }
        }

        return parameter;
    }
}
