<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Discount_List.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_DiscountCoupon_Discount_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Discount coupon - List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="gridElem" GridName="Discount_List.xml" OrderBy="DiscountCouponDisplayName"
        IsLiveSite="false" Columns="DiscountCouponID, DiscountCouponDisplayName, DiscountCouponValidFrom, DiscountCouponValidTo, DiscountCouponValue, DiscountCouponIsFlatValue" />
</asp:Content>
