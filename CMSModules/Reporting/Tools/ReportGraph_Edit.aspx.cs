using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.Reporting;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.UIControls;

using ZedGraph;

public partial class CMSModules_Reporting_Tools_ReportGraph_Edit : CMSReportingModalPage
{
    protected ReportGraphInfo graphInfo = null;
    protected int graphId;
    protected ReportInfo reportInfo = null;

    private bool newReportGraph = true;

    /// <summary>
    /// PreRender event handler
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        this.AddNoCacheTag();
        this.RegisterModalPageScripts();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Title.HelpTopicName = "report_graph_properties";
        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.Title = "ReportGraph Edit";

        int reportId = ValidationHelper.GetInteger(Request.QueryString["reportid"], 0);
        if (reportId > 0)
            reportInfo = ReportInfoProvider.GetReportInfo(reportId);
        if (reportInfo != null) //must be valid reportid parameter
        {
            string graphName = ValidationHelper.GetString(Request.QueryString["itemname"], "");

            // try to load graphname from hidden field (adding new graph & preview)
            if (graphName == String.Empty)
            {
                graphName = txtNewGraphHidden.Value;
            }

            if (ValidationHelper.IsCodeName(graphName))
            {
                graphInfo = ReportGraphInfoProvider.GetReportGraphInfo(graphName);
                
                if (graphInfo != null)
                {
                    graphId = graphInfo.GraphID;

                    // indicates that this is edit, not add new graph action
                    newReportGraph = false;
                }
            }

            rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");
            rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
            rfvQuery.ErrorMessage = ResHelper.GetString("Reporting_ReportGraph_Edit.ErrorQuery");

            // control initializations
            lblQuery.Text = ResHelper.GetString("Reporting_ReportGraph_Edit.Query");
            lblStoredProcedure.Text = ResHelper.GetString("Reporting_ReportGraph_Edit.StoredProcedure");
            lblGraphType.Text = ResHelper.GetString("Reporting_ReportGraph_Edit.GraphType");

            lblGraphHeight.Text = ResHelper.GetString("Reporting_ReportGraph_Edit.GraphHeight");
            lblGraphWidth.Text = ResHelper.GetString("Reporting_ReportGraph_Edit.GraphWidth");
            lblGraphTitle.Text = ResHelper.GetString("Reporting_ReportGraph_Edit.GraphTitle");
            lblGraphXAxisTitle.Text = ResHelper.GetString("Reporting_ReportGraph_Edit.GraphXAxisTitle");
            lblGraphYAxisTitle.Text = ResHelper.GetString("Reporting_ReportGraph_Edit.GraphYAxisTitle");
            lblGraphLegendPosition.Text = ResHelper.GetString("Reporting_ReportGraph_Edit.GraphLegendPosition");

            lblAxisFont.Text = ResHelper.GetString("rep.graph.lblAxisFont");
            lblColors.Text = ResHelper.GetString("rep.graph.lblColors");
            lblGraphGradient.Text = ResHelper.GetString("rep.graph.lblGraphGradient");
            lblChartGradient.Text = ResHelper.GetString("rep.graph.lblChartGradient");
            lblItemGradient.Text = ResHelper.GetString("rep.graph.lblItemGradient");
            lblSymbols.Text = ResHelper.GetString("rep.graph.lblSymbols");
            lblTitleFont.Text = ResHelper.GetString("rep.graph.lblTitleFont");
            lblXAxisAngle.Text = ResHelper.GetString("rep.graph.lblXAxisAngle");
            lblYAxisAngle.Text = ResHelper.GetString("rep.graph.lblYAxisAngle");
            lblScaleMax.Text = ResHelper.GetString("rep.graph.chkScaleMax");
            lblScaleMin.Text = ResHelper.GetString("rep.graph.chkScaleMin");
            
            chkVerticalBars.Text = ResHelper.GetString("rep.graph.VerticalBars");
            chkFillCurves.Text = ResHelper.GetString("rep.graph.chkFillCurves");
            chkReverseYAxis.Text = ResHelper.GetString("rep.graph.chkReverseYAxis");
            chkShowMajorGrid.Text = ResHelper.GetString("rep.graph.chkShowMajorGrid");
            chkSmoothCurves.Text = ResHelper.GetString("rep.graph.chkSmoothCurves");
            chkTenPowers.Text = ResHelper.GetString("rep.graph.chkTenPowers");

            if (!RequestHelper.IsPostBack())
            {
                //Set default font values
                titleFont.Value = "arial;24;True;False";
                axisFont.Value = "arial;14;True;False";
            }

            if (graphInfo != null)
            {
                this.CurrentMaster.Title.TitleText = ResHelper.GetString("Reporting_ReportGraph_Edit.TitleText");
                this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Reporting_ReportGraph/object.png");                
            }
            else //new item
            {
                this.CurrentMaster.Title.TitleText = ResHelper.GetString("Reporting_ReportGraph_Edit.NewItemTitleText");
                this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Reporting_ReportGraph/new.png");
            }

            // set help key            
            this.CurrentMaster.Title.HelpTopicName = "report_graph_properties";

            if (!RequestHelper.IsPostBack())
            {
                LoadData();
            }
        }
        else
        {
            pnlElements.Visible = false;
            btnOk.Visible = false;
            btnCancel.Visible = false;
            btnPreview.Visible = false;
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Reporting_ReportGraph_Edit.InvalidReportId");
        }
        btnOk.Text = ResHelper.GetString("General.OK");
        btnCancel.Text = ResHelper.GetString("General.Cancel");
        btnPreview.Text = ResHelper.GetString("General.SaveAndPreview");
    }


    protected void LoadData()
    {
        // load graph types
        drpGraphType.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.GraphType_Pie"), "pie"));
        drpGraphType.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.GraphType_Bar"), "bar"));
        drpGraphType.Items.Add(new ListItem(ResHelper.GetString("rep.graph.TypeBarOverlay"), "baroverlay"));
        drpGraphType.Items.Add(new ListItem(ResHelper.GetString("rep.graph.TypeBarStacked"), "barstacked"));
        drpGraphType.Items.Add(new ListItem(ResHelper.GetString("rep.graph.TypeBarPercentage"), "barpercentage"));
        drpGraphType.Items.Add(new ListItem(ResHelper.GetString("rep.graph.TypeLine"), "line"));

        // load legend positions
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_NoLegend"), ReportGraph.NO_LEGEND.ToString()));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_Top"), "0"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_Left"), "1"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_Right"), "2"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_Bottom"), "3"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_InsideTopLeft"), "4"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_InsideTopRight"), "5"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_InsideBotLeft"), "6"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_InsideBotRight"), "7"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_Float"), "8"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_TopCenter"), "9"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_BottomCenter"), "10"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_TopFlushLeft"), "11"));
        drpGraphLegendPosition.Items.Add(new ListItem(ResHelper.GetString("Reporting_ReportGraph.LegendPosition_BottomFlushLeft"), "12"));


        if (graphInfo != null)
        {
            txtDisplayName.Text = graphInfo.GraphDisplayName;
            txtCodeName.Text = graphInfo.GraphName;
            txtQuery.Text = graphInfo.GraphQuery;
            chkStoredProcedure.Checked = graphInfo.GraphQueryIsStoredProcedure;
            drpGraphType.SelectedValue = graphInfo.GraphType;
            drpGraphLegendPosition.SelectedValue = graphInfo.GraphLegendPosition.ToString();
            if (ValidationHelper.GetInteger(graphInfo.GraphHeight, 0) == 0)
            {
                txtGraphHeight.Text = "";
            }
            else
            {
                txtGraphHeight.Text = ValidationHelper.GetString(graphInfo.GraphHeight.ToString(), "");
            }
            if (ValidationHelper.GetInteger(graphInfo.GraphWidth, 0) == 0)
            {
                txtGraphWidth.Text = "";
            }
            else
            {
                txtGraphWidth.Text = ValidationHelper.GetString(graphInfo.GraphWidth.ToString(), "");
            }
            txtGraphTitle.Text = graphInfo.GraphTitle;
            txtGraphXAxisTitle.Text = graphInfo.GraphXAxisTitle;
            txtGraphYAxisTitle.Text = graphInfo.GraphYAxisTitle;

           
            CustomData settings = graphInfo.GraphSettings;

            // set fonts only if not empty, otherwise default values will be used
            string font = ValidationHelper.GetString(settings["axisFont"], "");
            if (font != String.Empty) {
                axisFont.Value = font;
            }
            font = ValidationHelper.GetString(settings["titleFont"], "");
            if (font != null) {
                titleFont.Value = font;
            }

            txtColors.Text = ValidationHelper.GetString(settings["colors"], "");
            graphGradient.Value = ValidationHelper.GetString(settings["graphGradient"], "");
            chartGradient.Value = ValidationHelper.GetString(settings["chartGradient"], "");
            itemGradient.Value = ValidationHelper.GetString(settings["itemGradient"], "");
            txtSymbols.Text = ValidationHelper.GetString(settings["symbols"], "");            
            txtXAxisAngle.Text = ValidationHelper.GetString(settings["XAxisAngle"], "");
            txtYAxisAngle.Text = ValidationHelper.GetString(settings["YAxisAngle"], "");
            txtScaleMax.Text = ValidationHelper.GetString(settings["ScaleMax"], "");
            txtScaleMin.Text = ValidationHelper.GetString(settings["ScaleMin"], "");

            chkVerticalBars.Checked = ValidationHelper.GetBoolean(settings["VerticalBars"], true);
            chkFillCurves.Checked = ValidationHelper.GetBoolean(settings["FillCurves"], false);
            chkReverseYAxis.Checked = ValidationHelper.GetBoolean(settings["ReverseYAxis"], false);
            chkShowMajorGrid.Checked = ValidationHelper.GetBoolean(settings["ShowMajorGrid"], false);
            chkSmoothCurves.Checked = ValidationHelper.GetBoolean(settings["SmoothCurves"], false);
            chkTenPowers.Checked = ValidationHelper.GetBoolean(settings["TenPowers"], false); 
            
        }
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (Save())
        {
            ltlScript.Text += ScriptHelper.GetScript("wopener.ReloadPage(); window.close();");
        }
    }


    protected void btnPreview_Click(object sender, EventArgs e)
    {
        if (Save())
        {            
            if (newReportGraph)
            {
                txtNewGraphHidden.Value = graphInfo.GraphName;
            }

            int windowWidth = Math.Max(graphInfo.GraphWidth + 30, 450);
            int windowHeight = Math.Max(graphInfo.GraphHeight + 110, 350);
            ltlScript.Text += ScriptHelper.GetScript("modalDialog('Report_Preview.aspx?type=graph&id=" + graphInfo.GraphID + "','ReportPreview'," + windowWidth + "," + windowHeight + ");");
        }
    }

    
    /// <summary>
    /// Save data
    /// </summary>
    /// <returns>return true if save was successfull</returns>
    protected bool Save()
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
        {
            RedirectToAccessDenied("cms.reporting", "Modify");
        }

        string errorMessage = new Validator().NotEmpty(txtDisplayName.Text, rfvDisplayName.ErrorMessage).NotEmpty(txtCodeName.Text, rfvCodeName.ErrorMessage).NotEmpty(txtQuery.Text, rfvQuery.ErrorMessage).Result;

        if ((errorMessage == "") && (!ValidationHelper.IsIdentificator(txtCodeName.Text.Trim())))
        {
            errorMessage = ResHelper.GetString("general.erroridentificatorformat");
        }

        string fullName = reportInfo.ReportName + "." + txtCodeName.Text.Trim();
        ReportGraphInfo codeNameCheck = ReportGraphInfoProvider.GetReportGraphInfo(fullName);
        if ((errorMessage == "") && (codeNameCheck != null) && (codeNameCheck.GraphID != graphId))
        {
            errorMessage = ResHelper.GetString("Reporting_ReportGraph_Edit.ErrorCodeNameExist");
        }
        if ((errorMessage == "") && (txtGraphWidth.Text.Trim() != String.Empty) && ((!ValidationHelper.IsInteger(txtGraphWidth.Text.Trim())) || (Int32.Parse(txtGraphWidth.Text.Trim()) <= 0)))
        {
            errorMessage = ResHelper.GetString("Reporting_ReportGraph_Edit.ErrorWidthInvalid");
        }
        if ((errorMessage == "") && (txtGraphHeight.Text.Trim() != String.Empty) && ((!ValidationHelper.IsInteger(txtGraphHeight.Text.Trim())) || (Int32.Parse(txtGraphHeight.Text.Trim()) <= 0)))
        {
            errorMessage = ResHelper.GetString("Reporting_ReportGraph_Edit.ErrorHeightInvalid");
        }

        if ((errorMessage == ""))
        {
            if (graphInfo == null) //new graph
                graphInfo = new ReportGraphInfo();

            graphInfo.GraphDisplayName = txtDisplayName.Text;
            graphInfo.GraphName = txtCodeName.Text;
            graphInfo.GraphQuery = txtQuery.Text;
            graphInfo.GraphQueryIsStoredProcedure = chkStoredProcedure.Checked;
            graphInfo.GraphType = drpGraphType.SelectedValue;
            graphInfo.GraphReportID = reportInfo.ReportID;
            graphInfo.GraphHeight = ValidationHelper.GetInteger(txtGraphHeight.Text, 0);
            graphInfo.GraphWidth = ValidationHelper.GetInteger(txtGraphWidth.Text, 0);
            graphInfo.GraphTitle = txtGraphTitle.Text;
            graphInfo.GraphXAxisTitle = txtGraphXAxisTitle.Text;
            graphInfo.GraphYAxisTitle = txtGraphYAxisTitle.Text;
            graphInfo.GraphLegendPosition = ValidationHelper.GetInteger(drpGraphLegendPosition.SelectedValue, 0);           

            CustomData settings = graphInfo.GraphSettings;

            settings["axisFont"] = axisFont.Value;
            settings["colors"] = txtColors.Text;
            settings["graphGradient"] = graphGradient.Value;
            settings["chartGradient"] = chartGradient.Value;
            settings["itemGradient"] = itemGradient.Value;
            settings["symbols"] = txtSymbols.Text;
            settings["titleFont"] = titleFont.Value;
            settings["XAxisAngle"] = txtXAxisAngle.Text;
            settings["YAxisAngle"] = txtYAxisAngle.Text;
            settings["ScaleMax"] = txtScaleMax.Text;
            settings["ScaleMin"] = txtScaleMin.Text;

            settings["VerticalBars"] = chkVerticalBars.Checked.ToString();
            settings["FillCurves"] = chkFillCurves.Checked.ToString();
            settings["ReverseYAxis"] = chkReverseYAxis.Checked.ToString();
            settings["ShowMajorGrid"] = chkShowMajorGrid.Checked.ToString();
            settings["SmoothCurves"] = chkSmoothCurves.Checked.ToString();
            settings["TenPowers"] = chkTenPowers.Checked.ToString();

            ReportGraphInfoProvider.SetReportGraphInfo(graphInfo);
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
            return false;
        }
        return true;
    }
}
