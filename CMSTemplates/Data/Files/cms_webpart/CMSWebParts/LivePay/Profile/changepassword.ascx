<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/LivePay/Profile/changepassword.ascx.cs"
    Inherits="CMSWebParts_LivePay_Profile_ChangePassword" %>
    <style>
        .change-password
        {
            padding-left:20px;
        }
        .change-password .row
        {
            padding-top:15px;
        }
        .change-password .row #result
        {
            float:left;
            padding-left:10px;
            margin-top:5px;
            margin-left:5px;
            line-height:20px;
            width: 164px;
        }        
        .change-password .label
        {
            float:left;
            color:#094595;
            font-size:12px;
            font-weight:bold;
            width:111px;
            padding-right:10px;
            text-align:right;
        }      
        .change-password .textbox
        {
            float:left;
            margin-top:4px;
        }      
        .change-password .textbox input
        {
            border:1px solid #3b6db4;
        }          
        .change-password-bottom
        {
            width:262px;
            text-align:right;
            padding-top:10px;            
        }     
        .change-password-bottom input
        {
            background:transparent url(/App_Themes/LivePay/ChangeUserDetails/bgButton.gif) no-repeat;
            width:121px;
            height:23px;
            border:none;
            color:#fff;
            font-weight:bold;
            cursor:pointer;            
        }
        .change-password .tooshort
        {
            background-color:Gray;
            background:gray url(/App_Themes/LivePay/ChangeUserDetails/bgValidPassword.gif) no-repeat 5px 10px;
            padding-left:10px;
        }
        .change-password .bad
        {
            background-color:Yellow;
        }
        .change-password .good
        {
            background-color:Lime;
        }
        .change-password .strong
        {
            background-color:Black;
        }
        .ErrorLabel{color:Red;padding-left:30px;font-weight:bold}
    </style>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            var idPassword = $('input[type=password][id$=txtNewPassword]:first')
            idPassword.keyup(function () { DivStrenght(passwordStrength(idPassword.val())) })

        });
        function DivStrenght(txt) {
            if (txt == 'Too short') {
                $('#result').css('background-color', '#ffcfcf');
                $('#result-icon').attr("class", "CUDStatus-icon-tooshort");
                $('#result-text').html('<%= ResHelper.LocalizeString("{$=���������� �������|en-us=Too short$}") %>');
            } else if (txt == 'Bad') {
                $('#result').css('background-color', '#ffd2b5');
                $('#result-icon').attr("class", "CUDStatus-icon-bad");
                $('#result-text').html('<%= ResHelper.LocalizeString("{$=��������|en-us=Bad$}") %>');
            } else if (txt == 'Good') {
                $('#result').css('background-color', '#fefdd2');
                $('#result-icon').attr("class", "CUDStatus-icon-good");
                $('#result-text').html('<%= ResHelper.LocalizeString("{$=�������|en-us=Good - ����$}") %>');

            } else if (txt == 'Strong') {
                $('#result').css('background-color', '#caedb8');
                $('#result-icon').attr("class", "CUDStatus-icon-strong");
                $('#result-text').html('<%= ResHelper.LocalizeString("{$=���� �������|en-us=Strong$}") %>');
            }
        }
    </script>

<asp:Panel ID="pnlWebPart" runat="server" DefaultButton="btnOk">
            <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false" Visible="false" />
            <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false" Visible="false" />
                 <div class="change-password">
                    <div class="row">
                        <div class="label"><asp:Label ID="lblOldPassword" AssociatedControlID="txtOldPassword" runat="server" /></div>
                        <div class="textbox"><asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password" /></div>
                        <div class="Clear"></div>
                    </div>
                    <div class="row">
                        <div class="label"><asp:Label ID="lblNewPassword" AssociatedControlID="txtNewPassword" runat="server" /></div>
                        <div class="textbox"><asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" /></div>
                        <div id="result"><div id="result-icon" class="CUDStatus icon-tooshort">&nbsp;</div> <span id="result-text"></span></div>
                        <div class="Clear"></div>
                    </div> 
                    <div class="row" style="padding-top:7px">
                        <div class="label"><asp:Label ID="lblConfirmPassword" AssociatedControlID="txtConfirmPassword" runat="server" /></div>
                        <div class="textbox"><asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" /></div>
                        <div class="Clear"></div>
                    </div>
                </div>
                 <div class="change-password-bottom"><cms:CMSButton ID="btnOk" runat="server" OnClick="btnOk_Click" /></div>
</asp:Panel>