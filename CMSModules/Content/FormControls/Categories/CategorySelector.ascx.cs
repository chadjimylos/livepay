using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.FormControls;
using CMS.UIControls;

public partial class CMSModules_Content_FormControls_Categories_CategorySelector : FormEngineUserControl
{
    #region "Variables"

    private bool mUseCategoryNameForSelection = true;
    private bool mAddNoneItemsRecord = false;
    private bool mUseAutoPostBack = false;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (this.mUseCategoryNameForSelection)
            {
                return this.CategoryName;
            }
            else
            {
                return this.CategoryID;
            }
        }
        set
        {
            if (this.mUseCategoryNameForSelection)
            {
                this.CategoryName = ValidationHelper.GetString(value, "");
            }
            else
            {
                this.CategoryID = ValidationHelper.GetInteger(value, 0);
            }
        }
    }


    /// <summary>
    /// Gets or sets the Category ID.
    /// </summary>
    public int CategoryID
    {
        get
        {
            if (this.mUseCategoryNameForSelection)
            {
                string name = ValidationHelper.GetString(this.uniSelector.Value, "");
                CategoryInfo ngi = CategoryInfoProvider.GetCategoryInfo(name);
                if (ngi != null)
                {
                    return ngi.CategoryID;
                }
                return 0;
            }
            else
            {
                return ValidationHelper.GetInteger(this.uniSelector.Value, 0);
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.mUseCategoryNameForSelection)
            {
                CategoryInfo ngi = CategoryInfoProvider.GetCategoryInfo(value);
                if (ngi != null)
                {
                    this.uniSelector.Value = ngi.CategoryName;
                }
            }
            else
            {
                this.uniSelector.Value = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets the Category code name.
    /// </summary>
    public string CategoryName
    {
        get
        {
            if (this.mUseCategoryNameForSelection)
            {
                return ValidationHelper.GetString(this.uniSelector.Value, "");
            }
            else
            {
                int id = ValidationHelper.GetInteger(this.uniSelector.Value, 0);
                CategoryInfo ngi = CategoryInfoProvider.GetCategoryInfo(id);
                if (ngi != null)
                {
                    return ngi.CategoryName;
                }
                return "";
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.mUseCategoryNameForSelection)
            {
                this.uniSelector.Value = value;
            }
            else
            {
                CategoryInfo ngi = CategoryInfoProvider.GetCategoryInfo(value);
                if (ngi != null)
                {
                    this.uniSelector.Value = ngi.CategoryID;
                }
            }
        }
    }


    /// <summary>
    ///  If true, selected value is CategoryName, if false, selected value is CategoryID
    /// </summary>
    public bool UseCategoryNameForSelection
    {
        get
        {
            return mUseCategoryNameForSelection;
        }
        set
        {
            mUseCategoryNameForSelection = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.ReturnColumnName = (value ? "CategoryName" : "CategoryID");
            }
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add none item record to the dropdownlist.
    /// </summary>
    public bool AddNoneItemsRecord
    {
        get
        {
            return mAddNoneItemsRecord;
        }
        set
        {
            mAddNoneItemsRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether the dropdown should use AutoPostBack.
    /// </summary>
    public bool UseAutoPostBack
    {
        get
        {
            return mUseAutoPostBack;
        }
        set
        {
            mUseAutoPostBack = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.DropDownSingleSelect.AutoPostBack = value;
            }
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.DisplayNameFormat = "{%CategoryDisplayName%}";
        this.uniSelector.SelectionMode = SelectionModeEnum.SingleDropDownList;
        this.uniSelector.ReturnColumnName = (this.UseCategoryNameForSelection ? "CategoryName" : "CategoryID");
        this.uniSelector.ObjectType = "cms.category";
        this.uniSelector.ResourcePrefix = "categoryselector";
        this.uniSelector.DropDownSingleSelect.AutoPostBack = this.UseAutoPostBack;
        this.uniSelector.AllowEmpty = this.AddNoneItemsRecord;
        this.uniSelector.WhereCondition = "(CategoryEnabled = 1)";

        if (this.UseCategoryNameForSelection)
        {
            this.uniSelector.AllRecordValue = "";
            this.uniSelector.NoneRecordValue = "";
        }
    }
}
