﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UcTransConfirmation.ascx.vb" Inherits="CMSTemplates_LivePay_UcTransConfirmation" %>

<script>
    function ExportDetails(url) {
        document.getElementById('<%=exportpopFrame.ClientID %>').src = url;
        return false
    }
</script>
<div style="display:none">
<iframe id="exportpopFrame" runat="server" ></iframe>
</div>
<div class="CUDDarkBlueBGTitle">
    <div class="SvdCardsTopTitle" id="PageTitle" runat="server"></div>
    <div class="CUDContentBG PB20">
        <div class="PT10PL10">
            <div>&nbsp;</div>
            <div style="background-image:url('/app_themes/LivePay/MessageTitleBG.png');background-repeat:no-repeat;height:30px">
                <div class="TransRecTblTickSec"><img id="imgResult" runat="server" src="/app_themes/LivePay/GreenTick.gif"></div>
	            <div class="TransRecTblGreenTitleSec" id="MessageTitle" runat="server"></div>
	            <div class="Clear"></div>
            </div>
        </div>
         <div style="line-height:22px;font-size:12px;color:#43474a;padding-top:20px" id="MainDivForm" runat="server">
            <div class="TransConfTitle"><%= ResHelper.LocalizeString("{$=Αριθμός Πακέτου|en-us=Αριθμός Πακέτου$}")%></div>
            <div class="TransConfValue" id="tdBoxNo" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%= ResHelper.LocalizeString("{$=Ημερομηνία Κλεισίματος Πακέτου|en-us=Ημερομηνία Κλεισίματος Πακέτου$}")%></div>
            <div class="TransConfValue" id="tdBoxCloseDtm" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=Κατάσταση Πακέτου|en-us=Κατάσταση Πακέτου$}") %></div>
            <div class="TransConfValue" id="tdBoxStatus" runat="server"> </div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=Κωδικός Συναλλαγής|en-us=Κωδικός Συναλλαγής$}") %></div>
            <div class="TransConfValue" id="tdTransCode" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=Ημερομηνία Συναλλαγής|en-us=Ημερομηνία Συναλλαγής$}") %></div>
            <div class="TransConfValue" id="tdTransDtm" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=Ημερομηνία Ακύρωσης Συναλλαγής|en-us=Ημερομηνία Ακύρωσης Συναλλαγής$}") %></div>
            <div class="TransConfValue" id="tdCancelDTM" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=Ονοματεπώνυμο Πελάτη|en-us=Ονοματεπώνυμο Πελάτη$}") %></div>
            <div class="TransConfValue" id="tdCustFullName" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}") %></div>
            <div class="TransConfValue" ID="tdPrice" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%= ResHelper.LocalizeString("{$=Τύπος Συναλλαγής|en-us=Τύπος Συναλλαγής$}")%></div>
            <div class="TransConfValue" id="tdTransType" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=Κατάσταση Πληρωμής|en-us=Κατάσταση Πληρωμής$}") %></div>
            <div class="TransConfValue" id="tdTransStatus" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=Τύπος Κάρτας|en-us=Τύπος Κάρτας$}") %></div>
            <div class="TransConfValue" id="tdCardType" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=Αριθμός Κάρτας|en-us=Αριθμός Κάρτας$}") %></div>
            <div class="TransConfValue" id="tdCardNo" runat="server"></div>
            <div class="Clear"></div>
             <div id="CustomFieldsPanel" runat="server">
             </div>
            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=E-mail|en-us=E-mail$}") %></div>
            <div class="TransConfValue" id="tdEmail" runat="server"></div>
            <div class="Clear"></div>

            <div class="TransConfTitle"><%=ResHelper.LocalizeString("{$=Τηλέφωνο Επικοινωνίας|en-us=Τηλέφωνο Επικοινωνίας$}") %></div>
            <div class="TransConfValue" id="tdContactPhone" runat="server"></div>
            <div class="Clear"></div>
            <div class="TransConBTN"><img class="Chand"  onclick="ExportDetails('/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=MerchantTransDetails')" src="/app_themes/LivePay/btnSavePDF.gif" /></div>
            <div class="TransConPrint"><img  class="Chand" OnClick="window.print();" src="/app_themes/LivePay/TransConfirmation/btnPrintReceipt.png" /></div>
            <div class="Clear"></div>
            
         </div>
         <div style="line-height:22px;font-size:12px;color:#43474a;padding-top:20px;display:none;padding-left:10px" id="MainDivErrorContainer" runat="server">
         </div> 
    </div> 
    <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
</div> 