<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Membership/Registration/RegistrationConfirmation.ascx.cs" Inherits="CMSWebParts_Membership_Registration_RegistrationConfirmation" %>

<%@ Register Src="~/CMSModules/Membership/Controls/RegistrationApproval.ascx" TagName="RegistrationApproval" TagPrefix="cms" %>

<cms:RegistrationApproval ID="registrationApproval" runat="server" />
