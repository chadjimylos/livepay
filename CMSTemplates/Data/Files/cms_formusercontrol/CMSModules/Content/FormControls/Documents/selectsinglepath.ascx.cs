using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.CMSHelper;
using CMS.SiteProvider;

public partial class CMSModules_Content_FormControls_Documents_SelectSinglePath : CMS.FormControls.FormEngineUserControl
{
    #region "Variables"

    private int mSiteId = 0;
    private DialogConfiguration mConfig = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Gets the configuration for Copy and Move dialog.
    /// </summary>
    private DialogConfiguration Config
    {
        get
        {
            if (this.mConfig == null)
            {
                this.mConfig = new DialogConfiguration();
                this.mConfig.HideLibraries = true;
                this.mConfig.ContentSelectedSite = CMSContext.CurrentSiteName;
                this.mConfig.HideAnchor = true;
                this.mConfig.HideAttachments = true;
                this.mConfig.HideContent = false;
                this.mConfig.HideEmail = true;
                this.mConfig.HideLibraries = true;
                this.mConfig.HideWeb = true;
                this.mConfig.EditorClientID = this.txtPath.ClientID;
                this.mConfig.OutputFormat = OutputFormatEnum.Custom;
                this.mConfig.CustomFormatCode = "selectpath";
                this.mConfig.SelectableContent = SelectableContentEnum.AllContent;
            }
            return this.mConfig;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.txtPath.Enabled = value;
            this.btnSelectPath.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.txtPath.Text;
        }
        set
        {
            this.txtPath.Text = (string)value;
        }
    }


    /// <summary>
    /// Gets ClientID of the textbox with path
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.txtPath.ClientID;
        }
    }


    /// <summary>
    /// Gets or sets the ID of the site from which the path is selected.
    /// </summary>
    public int SiteID
    {
        get
        {
            return this.mSiteId;
        }
        set
        {
            this.mSiteId = value;
            if (value > 0)
            {
                this.Config.ContentSites = AvailableSitesEnum.OnlySingleSite;
                SiteInfo si = SiteInfoProvider.GetSiteInfo(value);
                if (si != null)
                {
                    this.Config.ContentSelectedSite = si.SiteName;
                }
            }
            else
            {
                this.Config.ContentSites = AvailableSitesEnum.All;
            }
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        this.btnSelectPath.Text = ResHelper.GetString("general.select");
        this.btnSelectPath.OnClientClick = "modalDialog('" + GetDialogUrl() + "','PathSelection', '90%', '85%'); return false;";
    }


    /// <summary>
    /// Returns Correct URL of the copy or move dialog.
    /// </summary>
    private string GetDialogUrl()
    {
        string url = CMSDialogHelper.GetDialogUrl(this.Config, this.IsLiveSite, false);
        url = url.Replace(UrlHelper.GetFullDomain(), "").Replace("https://", "").Replace("http://", "");
        url = UrlHelper.RemoveParameterFromUrl(url, "hash");
        url = UrlHelper.AddParameterToUrl(url, "selectionmode", "single");
        url = UrlHelper.AddParameterToUrl(url, "hash", QueryHelper.GetHash(url));
        url = UrlHelper.EncodeQueryString(url);
        return url;
    }
}
