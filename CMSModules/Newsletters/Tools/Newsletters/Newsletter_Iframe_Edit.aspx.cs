using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using System.Text;

using CMS.Newsletter;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Controls;
using CMS.CMSHelper;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Iframe_Edit : CMSNewsletterPage
{
    private int mIssueID = 0;
    private int mNewsletterID = 0;
    private int templateId = 0;
    private ArrayList regionList = new ArrayList();
    private Hashtable regionsContents = new Hashtable();
    Issue issue = null;

    // True indicates that new issue is created otherwise existing one is edited
    private bool mIsNewIssue = false;


    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "Reload", ScriptHelper.GetScript("" +
           "function SaveContent(callNext, newsSubject, newsShowInArchive) { \n" +
           " document.getElementById('" + hdnNewsletterSubject.ClientID + "').value = newsSubject; \n" +
           " document.getElementById('" + hdnNewsletterShowInArchive.ClientID + "').value = newsShowInArchive; \n" +
           " if (callNext) { document.getElementById('" + this.hdnNext.ClientID + "').value = 1; } \n" +
           this.Page.ClientScript.GetPostBackEventReference(btnHidden, null) +
           "} \n" +
           "function SaveDocument() { window.parent.ClearToolbar(); window.parent.SaveDocument(); } \n"));

        if (CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "authorissues"))
        {
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>");
        }

        // Get issue ID
        mIssueID = QueryHelper.GetInteger("issueid", 0);
        if (mIssueID == 0)
        {
            mIssueID = QueryHelper.GetInteger(hdnIssueId.Value, 0);
        }

        // Get newsletter ID
        mNewsletterID = QueryHelper.GetInteger("newsletterid", 0);
        // Get info if new issue is created
        mIsNewIssue = QueryHelper.GetBoolean("new", false);

        if (mIssueID > 0)
        {
            issue = IssueProvider.GetIssue(mIssueID);
            if (issue != null)
            {
                templateId = issue.IssueTemplateID;
            }
        }
        else if (mNewsletterID > 0)
        {
            Newsletter newsletter = NewsletterProvider.GetNewsletter(mNewsletterID);
            if (newsletter != null)
            {
                templateId = newsletter.NewsletterTemplateID;
            }
        }

        if (templateId > 0)
        {
            // Load content from the template
            LoadContent();
        }

        if (!RequestHelper.IsPostBack())
        {
            if ((issue != null))
            {
                bool saved = QueryHelper.GetBoolean("saved", false);
                if (saved)
                {
                    ltlScript2.Text = ScriptHelper.GetScript("if (parent.MsgInfo) { parent.MsgInfo(0); } ");
                }
            }
        }

        LoadRegionList();
    }


    /// <summary>
    /// Loads content from specific newsletter template.
    /// </summary>
    private void LoadContent()
    {
        EmailTemplate emailTemplate = EmailTemplateProvider.GetEmailTemplate(templateId);
        if (emailTemplate == null)
        {
            return;
        }
        string templateText = emailTemplate.TemplateBody;
        LiteralControl before = new LiteralControl();
        CMSEditableRegion editableRegion = new CMSEditableRegion();
        editableRegion.ViewMode = ViewModeEnum.Edit;

        if (!RequestHelper.IsPostBack() && (issue != null))
        {
            // Load content of editable regions
            LoadRegionsContents();
        }

        int count = 0;
        int textStart = 0;
        int editRegStart = templateText.IndexOf("$$", textStart);


        // Apply CSS e-mail template style
        this.Page.Header.Controls.Add(new LiteralControl("<link href=\"" + EmailTemplateProvider.GetStylesheetUrl(emailTemplate.TemplateName) + "\" type=\"text/css\" rel=\"stylesheet\" />"));

        while (editRegStart >= 0)
        {
            count++;

            before = new LiteralControl();
            before.Text = UrlHelper.MakeLinksAbsolute(templateText.Substring(textStart, (editRegStart - textStart)));
            plcContent.Controls.Add(before);

            // End of region
            editRegStart += 2;
            textStart = editRegStart;
            if (editRegStart < templateText.Length - 1)
            {
                int editRegEnd = templateText.IndexOf("$$", editRegStart);
                if (editRegEnd >= 0)
                {
                    string region = templateText.Substring(editRegStart, editRegEnd - editRegStart);
                    string[] parts = (region + ":" + ":").Split(':');

                    try
                    {
                        string name = parts[0];
                        if (!string.IsNullOrEmpty(name.Trim()))
                        {
                            int width = ValidationHelper.GetInteger(parts[1], 0);
                            int height = ValidationHelper.GetInteger(parts[2], 0);

                            editableRegion = new CMSEditableRegion();
                            editableRegion.ID = name;
                            editableRegion.RegionType = CMSEditableRegionTypeEnum.HtmlEditor;
                            editableRegion.ViewMode = ViewModeEnum.Edit;

                            editableRegion.DialogHeight = height;
                            editableRegion.DialogWidth = width;

                            editableRegion.WordWrap = false;
                            editableRegion.HtmlAreaToolbarLocation = "Out:parent(FCKEditorToolbar)";
                            editableRegion.RegionTitle = name;
                            editableRegion.UseStylesheet = false;
                            editableRegion.HTMLEditorCssStylesheet = EmailTemplateProvider.GetStylesheetUrl(emailTemplate.TemplateName);
                            editableRegion.HtmlAreaToolbar = "Newsletter";
                            editableRegion.HtmlEditor.MediaDialogConfig.UseFullURL = true;
                            editableRegion.HtmlEditor.LinkDialogConfig.UseFullURL = true;
                            editableRegion.HtmlEditor.QuickInsertConfig.UseFullURL = true;

                            editableRegion.LoadContent(ValidationHelper.GetString(regionsContents[name.ToLower()], ""));

                            plcContent.Controls.Add(editableRegion);

                            textStart = editRegEnd + 2;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            editRegStart = templateText.IndexOf("$$", textStart);
        }

        before = new LiteralControl();
        before.Text = UrlHelper.MakeLinksAbsolute(templateText.Substring(textStart));

        plcContent.Controls.Add(before);
    }


    /// <summary>
    /// Loads content of editable regions.
    /// </summary>
    protected void LoadRegionsContents()
    {
        regionsContents.Clear();
        string contentXml = issue.IssueText;
        if (!string.IsNullOrEmpty(contentXml))
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(contentXml);
            XmlNode docElem = xml.DocumentElement;

            // Load editable regions
            XmlNodeList regions = docElem.SelectNodes("region");
            if (regions != null)
            {
                foreach (XmlNode region in regions)
                {
                    string id = region.Attributes["id"].Value;
                    string value = region.InnerText;
                    regionsContents[id.ToLower()] = value;
                }
            }
        }
    }


    /// <summary>
    /// Saves the content to the specified Issue
    /// </summary>
    /// <param name="issue">Issue data</param>
    public string SaveContent(Issue issue)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Newsletter", "AuthorIssues"))
        {
            RedirectToCMSDeskAccessDenied("CMS.Newsletter", "AuthorIssues");
        }

        CMSEditableRegion editReg;
        XmlWriterSettings settings = new XmlWriterSettings();
        settings.Indent = true;
        settings.NewLineOnAttributes = false;
        //string xmlOutput = "";
        StringBuilder sb = new StringBuilder();
        using (XmlWriter writer = XmlWriter.Create(sb, settings))
        {
            writer.WriteStartDocument();
            writer.WriteStartElement("content");

            for (int i = 0; i < regionList.Count; i++)
            {
                editReg = (CMSEditableRegion)regionList[i];
                writer.WriteStartElement("region");
                writer.WriteStartAttribute("id");
                writer.WriteValue(editReg.ID);
                writer.WriteEndAttribute();
                writer.WriteString(editReg.GetContent());
                writer.WriteEndElement(); //region
            }

            writer.WriteEndElement(); //content
            writer.WriteEndDocument();
        }

        issue.IssueText = sb.ToString();
        return "";
    }


    protected void LoadRegionList()
    {
        regionList = CMSPageManager.CollectEditableControls(plcContent);

        // Create array of regions IDs in javascript. We will use it to find out the focused region
        string script = "var focusedRegionID = ''; \n var regions = new Array(" + regionList.Count.ToString() + "); \n ";
        for (int i = 0; i < regionList.Count; i++)
        {
            script += "regions[" + i.ToString() + "] = '" + ((CMSEditableRegion)regionList[i]).ClientID + "_HtmlEditor'; \n ";
        }
        ltlScript.Text = ScriptHelper.GetScript(script);
    }


    /// <summary>
    /// Save click handler
    /// </summary>
    protected void btnHidden_Click(object sender, EventArgs e)
    {
        string result = new Validator().NotEmpty(hdnNewsletterSubject.Value.Trim(), ResHelper.GetString("NewsletterContentEditor.SubjectRequired")).Result;
        if (result != String.Empty)
        {
            ltlScript2.Text = ScriptHelper.GetScript("if (parent.MsgInfo) { parent.MsgInfo(1); }");
            return;
        }

        if (this.issue == null)
        {
            this.issue = new Issue();
            this.issue.IssueUnsubscribed = 0;
            this.issue.IssueSentEmails = 0;
            this.issue.IssueTemplateID = templateId;
            this.issue.IssueNewsletterID = mNewsletterID;
            this.issue.IssueSiteID = CMSContext.CurrentSiteID;
        }

        SaveContent(issue);

        issue.IssueSubject = hdnNewsletterSubject.Value.Trim();
        this.issue.IssueShowInNewsletterArchive = ValidationHelper.GetBoolean(hdnNewsletterShowInArchive.Value, false);
        IssueProvider.SetIssue(issue);

        hdnIssueId.Value = issue.IssueID.ToString();

        if (mIsNewIssue)
        {
            // Hide content if redirecting
            plcContent.Visible = false;
        }

        if (hdnNext.Value == "1")
        {
            ltlScript2.Text = ScriptHelper.GetScript("parent.NextClick('" + issue.IssueID.ToString() + "');");
        }
        else
        {
            ltlScript2.Text = ScriptHelper.GetScript("if (parent.SaveClick) {parent.SaveClick('" + issue.IssueID.ToString() + "');} ");
        }
    }
}
