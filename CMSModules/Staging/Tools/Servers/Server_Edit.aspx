<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Server_Edit.aspx.cs" Inherits="CMSModules_Staging_Tools_Servers_Server_Edit"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Server edit" Theme="Default" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <cms:LocalizedLabel runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top;">
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblServerDisplayName" EnableViewState="false"
                    ResourceString="Server_Edit.ServerDisplayNameLabel" />
            </td>
            <td>
                <asp:TextBox ID="txtServerDisplayName" runat="server" CssClass="TextBoxField" MaxLength="440" />
                <asp:RequiredFieldValidator ID="rfvServerDisplayName" runat="server" ControlToValidate="txtServerDisplayName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblServerName" EnableViewState="false" ResourceString="Server_Edit.ServerNameLabel" />
            </td>
            <td>
                <asp:TextBox ID="txtServerName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvServerName" runat="server" ControlToValidate="txtServerName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblServerURL" EnableViewState="false" ResourceString="Server_Edit.ServerURLLabel" />
            </td>
            <td>
                <asp:TextBox ID="txtServerURL" runat="server" CssClass="TextBoxField" MaxLength="450" />
                <asp:RequiredFieldValidator ID="rfvServerURL" runat="server" ControlToValidate="txtServerURL"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblServerEnabled" EnableViewState="false"
                    ResourceString="general.enabled" DisplayColon="true" />
            </td>
            <td>
                <asp:CheckBox ID="chkServerEnabled" runat="server" CssClass="CheckBoxMovedLeft" Checked="true" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" colspan="2">
                <cms:LocalizedLabel runat="server" ID="lblServerAuthentication" EnableViewState="false"
                    ResourceString="Server_Edit.ServerAuthenticationLabel" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <cms:LocalizedRadioButton ID="radUserName" runat="server" GroupName="ServerAuthentication"
                    AutoPostBack="true" Checked="true" ResourceString="Server_Edit.Authentication_UserName" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <cms:LocalizedLabel runat="server" ID="lblServerUsername" EnableViewState="false"
                    ResourceString="general.username" DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtServerUsername" runat="server" CssClass="TextBoxField" MaxLength="100" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <cms:LocalizedLabel runat="server" ID="lblServerPassword" EnableViewState="false"
                    ResourceString="Server_Edit.ServerPasswordLabel" />
            </td>
            <td>
                <asp:TextBox ID="txtServerPassword" runat="server" CssClass="TextBoxField" MaxLength="100" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <cms:LocalizedRadioButton ID="radX509" runat="server" GroupName="ServerAuthentication"
                    AutoPostBack="true" ResourceString="Server_Edit.Authentication_X509" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <cms:LocalizedLabel runat="server" ID="lblServerX509ClientKeyID" EnableViewState="false"
                    ResourceString="Server_Edit.ServerX509ClientKeyIDLabel" />
            </td>
            <td>
                <asp:TextBox ID="txtServerX509ClientKeyID" runat="server" CssClass="TextBoxField"
                    MaxLength="200" Enabled="false" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <cms:LocalizedLabel runat="server" ID="lblServerX509ServerKeyID" EnableViewState="false"
                    ResourceString="Server_Edit.ServerX509ServerKeyIDLabel" />
            </td>
            <td>
                <asp:TextBox ID="txtServerX509ServerKeyID" runat="server" CssClass="TextBoxField"
                    MaxLength="200" Enabled="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:LocalizedButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" ResourceString="General.OK" />
            </td>
        </tr>
    </table>
</asp:Content>
