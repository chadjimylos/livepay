using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.SiteProvider;

public partial class CMSModules_Ecommerce_FormControls_DepartmentSelector : CMS.FormControls.FormEngineUserControl
{
    #region "Variables"

    private bool mUseDepartmentNameForSelection = true;
    private bool mAddNoneRecord = true;
    private bool mAddAllItemsRecord = true;
    private bool mAddAllMyRecord = false;
    private int mUserId = 0;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the ID of the user the departments of which should be displayed. 0 means all departments are displayed.
    /// </summary>
    public int UserID
    {
        get
        {
            return this.mUserId;
        }
        set
        {
            this.mUserId = value;
        }
    }


    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (this.mUseDepartmentNameForSelection)
            {
                return this.DepartmentName;
            }
            else
            {
                return this.DepartmentID;
            }
        }
        set
        {
            if (this.mUseDepartmentNameForSelection)
            {
                this.DepartmentName = ValidationHelper.GetString(value, "");
            }
            else
            {
                this.DepartmentID = ValidationHelper.GetInteger(value, 0);
            }
        }
    }


    /// <summary>
    /// Gets or sets the Department ID.
    /// </summary>
    public int DepartmentID
    {
        get
        {
            if (this.mUseDepartmentNameForSelection)
            {
                string name = ValidationHelper.GetString(uniSelector.Value, "");
                DepartmentInfo tgi = DepartmentInfoProvider.GetDepartmentInfo(name);
                if (tgi != null)
                {
                    return tgi.DepartmentID;
                }
                return 0;
            }
            else
            {
                return ValidationHelper.GetInteger(uniSelector.Value, 0);
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.mUseDepartmentNameForSelection)
            {
                DepartmentInfo tgi = DepartmentInfoProvider.GetDepartmentInfo(value);
                if (tgi != null)
                {
                    this.uniSelector.Value = tgi.DepartmentID;
                }
            }
            else
            {
                this.uniSelector.Value = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets the Department code name.
    /// </summary>
    public string DepartmentName
    {
        get
        {
            if (this.mUseDepartmentNameForSelection)
            {
                return ValidationHelper.GetString(this.uniSelector.Value, "");
            }
            else
            {
                int id = ValidationHelper.GetInteger(this.uniSelector.Value, 0);
                DepartmentInfo tgi = DepartmentInfoProvider.GetDepartmentInfo(id);
                if (tgi != null)
                {
                    return tgi.DepartmentName;
                }
                return "";
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.mUseDepartmentNameForSelection)
            {
                this.uniSelector.Value = value;
            }
            else
            {
                DepartmentInfo tgi = DepartmentInfoProvider.GetDepartmentInfo(value);
                if (tgi != null)
                {
                    this.uniSelector.Value = tgi.DepartmentName;
                }
            }
        }
    }


    /// <summary>
    ///  If true, selected value is DepartmentName, if false, selected value is DepartmentID
    /// </summary>
    public bool UseDepartmentNameForSelection
    {
        get
        {
            return mUseDepartmentNameForSelection;
        }
        set
        {
            mUseDepartmentNameForSelection = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add none item record to the dropdownlist.
    /// </summary>
    public bool AddNoneRecord
    {
        get
        {
            return mAddNoneRecord;
        }
        set
        {
            mAddNoneRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add all item record to the dropdownlist.
    /// </summary>
    public bool AddAllItemsRecord
    {
        get
        {
            return mAddAllItemsRecord;
        }
        set
        {
            mAddAllItemsRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add all my departments item record to the dropdownlist.
    /// </summary>
    public bool AddAllMyRecord
    {
        get
        {
            return mAddAllMyRecord;
        }
        set
        {
            mAddAllMyRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Returns ClientID of the dropdownlist.
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect.ClientID;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.AllowEmpty = this.AddNoneRecord;
        this.uniSelector.AllowAll = this.AddAllItemsRecord;
        this.uniSelector.ReturnColumnName = (this.UseDepartmentNameForSelection ? "DepartmentName" : "DepartmentID");

        if (this.UseDepartmentNameForSelection)
        {
            this.uniSelector.AllRecordValue = "";
            this.uniSelector.NoneRecordValue = "";
        }

        if (this.AddAllMyRecord)
        {
            this.uniSelector.SpecialFields = new string[,] { { ResHelper.GetString("product_list.allmydepartments"), "" } };
        }

        if (this.UserID > 0)
        {
            this.uniSelector.WhereCondition = "DepartmentID IN (SELECT DepartmentID FROM COM_UserDepartment WHERE UserID = " + this.UserID + ")";
        }
    }
}
