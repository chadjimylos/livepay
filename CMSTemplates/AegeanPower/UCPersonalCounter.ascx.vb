﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.Controls

Imports CMS.FormEngine
Imports CMS.SettingsProvider
Imports CMS.DataEngine


Partial Class CMSTemplates_AegeanPower_UCPersonalCounter
    Inherits CMSUserControl

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If

        If Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub


    Public Overloads Sub ReloadData()
        CheckAfm()
        FeelUserCounters()
        lblSelectCounter.Text = ResHelper.LocalizeString("{$=Επιλογή μετρητή:|en-us=Select counter:$}")
        lblIndication.Text = ResHelper.LocalizeString("{$=Ένδειξη (kWh):|en-us=Indication (kWh):$}")
        lblNightlifeMeterReading.Text = ResHelper.LocalizeString("{$=Νυχτερινή ένδειξη Μετρητή (kWh):|en-us=Nightlife meter reading (kWh):$}")
        lblReactiveMeterReading.Text = ResHelper.LocalizeString("{$=Άεργος ένδειξη μετρητή (kWh):|en-us=Reactive meter reading (kWh):$}")
        lblMaximumDemand.Text = ResHelper.LocalizeString("{$=Μέγιστη ζήτηση (kW):|en-us=Maximum demand (kW):$}")

    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
        CheckAfm()

    End Sub

    Private Sub FeelUserCounters()
        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = AegeanPower_DBConnection.GetUserProviders(UserID)

        If dt.Rows.Count = 0 Then
            DivNoActiveAFM.Visible = True
        Else
            DivNoActiveAFM.Visible = False

            ddlSelectCounter.DataTextField = "Provider_Number"
            ddlSelectCounter.DataValueField = "Provider_Number"
            ddlSelectCounter.DataSource = dt
            ddlSelectCounter.DataBind()
            ddlSelectCounter.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=Επιλέξτε|en-us=Select$}"), 0))
        End If
    End Sub


    Protected Sub ddlSelectCounter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSelectCounter.SelectedIndexChanged
        CheckAfm()
    End Sub

    Private Sub CheckAfm()
        Dim providerno As String = ddlSelectCounter.SelectedValue
        If providerno <> String.Empty Then
            Dim dt As DataTable = AegeanPower_DBConnection.GetProviderDetails(providerno)
            If dt.Rows.Count > 0 Then
                Dim ActPO As Boolean = dt.Rows(0)("ACTIVE_POWER")
                Dim AerEn As Boolean = dt.Rows(0)("AERGOS_ENERGY")
                Dim NigEn As Boolean = dt.Rows(0)("NIGHT_ENERGY")
                Dim EnPo As Boolean = dt.Rows(0)("ENERGY_POWER")

                rfvIndication.Enabled = ActPO
                rfvIndication.ErrorMessage = ResHelper.LocalizeString("{$=Το πεδίο είναι υποχρεωτικό|en-us=The field is obligatory$}")
                cvIndication.Enabled = ActPO
                cvIndication.ErrorMessage = ResHelper.LocalizeString("{$=Εισάγετε κάποιο νούμερο|en-us=Ender a number$}")

                Star3.Visible = NigEn
                rfvNightlifeMeterReading.Enabled = NigEn
                rfvNightlifeMeterReading.ErrorMessage = ResHelper.LocalizeString("{$=Το πεδίο είναι υποχρεωτικό|en-us=The field is obligatory$}")
                cvNightMeterReading.Enabled = NigEn
                cvNightMeterReading.ErrorMessage = ResHelper.LocalizeString("{$=Εισάγετε κάποιο νούμερο|en-us=Ender a number$}")

                Star4.Visible = AerEn
                rfvReactiveMeterReading.Enabled = AerEn
                rfvReactiveMeterReading.ErrorMessage = ResHelper.LocalizeString("{$=Το πεδίο είναι υποχρεωτικό|en-us=The field is obligatory$}")
                cvReactiveMeter.Enabled = AerEn
                cvReactiveMeter.ErrorMessage = ResHelper.LocalizeString("{$=Εισάγετε κάποιο νούμερο|en-us=Ender a number$}")

                Star5.Visible = EnPo
                rfvMaximumDemand.Enabled = EnPo
                rfvMaximumDemand.ErrorMessage = ResHelper.LocalizeString("{$=Το πεδίο είναι υποχρεωτικό|en-us=The field is obligatory$}")
                cvtbMaximumDemand.Enabled = EnPo
                cvtbMaximumDemand.ErrorMessage = ResHelper.LocalizeString("{$=Εισάγετε κάποιο νούμερο|en-us=Ender a number$}")

            End If
        End If
    End Sub

    Protected Sub btnInputData_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnInputData.Click

        Dim bizFormName As String = "AegeanPowerRegistrationparticulars"
        Dim siteName As String = "AegeanPower"

        'Read BizFrom definition
        Dim bfi As BizFormInfo = BizFormInfoProvider.GetBizFormInfo(bizFormName, siteName)
        If Not IsNothing(bfi) Then

            'Read data type definition
            Dim dci As DataClassInfo = DataClassInfoProvider.GetDataClass(bfi.FormClassID)
            If Not IsNothing(dci) Then
                Dim genConn As GeneralConnection = ConnectionHelper.GetConnection()
                Dim formRecord As DataClass = New DataClass(dci.ClassName, genConn)
                'Insert some data
                formRecord.SetValue("FormInserted", DateAndTime.Now.ToString())
                formRecord.SetValue("FormUpdated", DateAndTime.Now.ToString())
                formRecord.SetValue("SelectCounter", ddlSelectCounter.SelectedValue.ToString())
                formRecord.SetValue("Indication", tbIndication.Text)
                formRecord.SetValue("NightlifeMeterReading", tbNightlifeMeterReading.Text)
                formRecord.SetValue("ReactiveMeterReading", tbReactiveMeterReading.Text)
                formRecord.SetValue("MaximumDemand", tbMaximumDemand.Text)

                'Insert the new record in the database
                formRecord.Insert()

                'Update number of entries in BizFormInfo
                bfi.FormItems += 1
                BizFormInfoProvider.SetBizFormInfo(bfi)
            End If
            Response.Redirect(url:="/Home-Page/Customer-Center/Count-alone/Καταχωρηση-ενδειξεων/Οι-μετρησεις-σας-εχουν-καταχωρηθει.aspx")
        End If
    End Sub
End Class

