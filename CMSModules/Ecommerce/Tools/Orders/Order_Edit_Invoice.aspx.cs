using System;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_Invoice : CMSEcommercePage
{
    int orderId;
    OrderInfo order;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.NEWWINDOW_SCRIPT_KEY, ScriptHelper.NewWindowScript);

        lblInvoiceNumber.Text = ResHelper.GetString("order_invoice.lblInvoiceNumber");
        btnGenerate.Text = ResHelper.GetString("order_invoice.btnGenerate");
        btnPrintPreview.Text = ResHelper.GetString("order_invoice.btnPrintPreview");

        if (ValidationHelper.GetInteger(Request.QueryString["orderid"], 0) != 0)
        {
            orderId = ValidationHelper.GetInteger(Request.QueryString["orderid"], 0);
        }
        order = OrderInfoProvider.GetOrderInfo(orderId);
        
        if (order == null)
        {
            btnGenerate.Enabled = false;
            btnPrintPreview.Enabled = false;
            return;
        }

        ltlScript.Text = ScriptHelper.GetScript("function showPrintPreview() { NewWindow('Order_Edit_InvoicePrint.aspx?orderid=" + orderId + "', 'InvoicePrint', 650, 700);}");

        if (!RequestHelper.IsPostBack())
        {
            txtInvoiceNumber.Text = order.OrderInvoiceNumber;
            lblInvoice.Text = UrlHelper.MakeLinksAbsolute(order.OrderInvoice);
        }
    }


    protected void btnGenerate_Click1(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        // Save updated order invoice number
        order.OrderInvoiceNumber = txtInvoiceNumber.Text;
        OrderInfoProvider.SetOrderInfo(order);

        // Generate and display new invoice
        string invoice = OrderInfoProvider.GetInvoice(orderId, order.ObjectSiteID);
        lblInvoice.Text = UrlHelper.MakeLinksAbsolute(invoice);
        
        // Save new invoice
        order.OrderInvoice = invoice;
        OrderInfoProvider.SetOrderInfo(order);

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");        
    }
}
