using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.FormEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.PortalControls;

public partial class CMSModules_Widgets_Dialogs_WidgetProperties_Header : CMSDeskPage
{
    string widgetId = "";
    string aliasPath = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        SetBrowserClass();

        widgetId = QueryHelper.GetString("widgetid", "");
        aliasPath = QueryHelper.GetString("aliasPath", "");


        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.NEWWINDOW_SCRIPT_KEY, ScriptHelper.NewWindowScript);

        // initialize page title
        PageTitle.TitleText = ResHelper.GetString("Widgets.Properties.Title");
        PageTitle.TitleImage = GetImageUrl("CMSModules/CMS_PortalEngine/Widgetproperties.png");

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }
    }


    /// <summary>
    /// Initializes menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string zoneId = QueryHelper.GetString("zoneid", "");
        Guid instanceGuid = QueryHelper.GetGuid("instanceguid", Guid.Empty);
        bool isNewWidget = QueryHelper.GetBoolean("isnew", false);

        if ((widgetId != "") && (aliasPath != ""))
        {
            WidgetInfo wi = null;

            if (!isNewWidget)
            {
                // get pageinfo
                PageInfo pi = null;
                try
                {
                    pi = PageInfoProvider.GetPageInfo(CMSContext.CurrentSiteName, aliasPath, CMSContext.PreferredCultureCode, null, CMSContext.CurrentSite.CombineWithDefaultCulture, null);
                }
                catch (PageNotFoundException)
                {
                    // Do not throw exception if page info not found (e.g. bad alias path)
                }

                if (pi == null)
                {
                    this.Visible = false;
                    return;
                }


                // Get template instance
                PageTemplateInstance templateInstance = CMSPortalManager.GetTemplateInstanceForEditing(pi);

                if (templateInstance != null)
                {
                    // Get web part
                    WebPartInstance widget = templateInstance.GetWebPart(instanceGuid, widgetId);

                    if ((widget != null) && widget.IsWidget)
                    {
                        // WebPartType = codename, get widget by codename 
                        wi = WidgetInfoProvider.GetWidgetInfo(widget.WebPartType);
                    }
                }
            }
            // New widget
            else
            {
                int id = ValidationHelper.GetInteger(widgetId, 0);
                wi = WidgetInfoProvider.GetWidgetInfo(id);
            }

            if (wi != null)
            {
                PageTitle.TitleText = ResHelper.GetString("Widgets.Properties.Title") + " (" + HTMLHelper.HTMLEncode(wi.WidgetDisplayName) + ")";
            }
        }
    }
}
