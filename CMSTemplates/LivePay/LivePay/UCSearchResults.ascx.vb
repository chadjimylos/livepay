﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.SiteProvider
Imports CMS.SettingsProvider
Imports CMS.DataEngine

Imports CMS.Controls.CMSAbstractTransformation


Partial Class CMSTemplates_LivePay_UCSearchResults
    Inherits CMSUserControl

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        Bind()
    End Sub

    Private Sub Bind()
        GetItemsCustomTable("LivePay.Merchant", "MerchantID", "CompanyName")
    End Sub

    Private Sub GetItemsCustomTable(ByVal CustomTable As String, ByVal ColumeValue As String, ByVal ColumeText As String)
        Dim srckey As String = Request.QueryString("searchtext")
        Dim key As String = Replace(srckey, "'", String.Empty)
        key = Replace(key, """", String.Empty)
        If key <> String.Empty Then


            'Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass(CustomTable)
            'If customTableClassInfo Is Nothing Then
            '    Throw New Exception("Given custom table does not exist.")
            'End If
            'Dim ctiProvider As New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())
            'Dim where As String = String.Concat("CompanyName", " like '", key, "%'")

            'Dim dsItems As DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, where, ColumeText)

            Dim dsItems As DataSet = DBConnection.SearchMerchant("Proc_LivePay_SearchMerchant", key, CMSContext.CurrentDocument.DocumentCulture)


            Session("GetGVMerchantsDataTable") = dsItems
            GVMerchants.DataSource = dsItems
            GVMerchants.DataBind()
        End If
    End Sub

#Region " Paging "

    Protected Sub GvNews_PageIndexChanged(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles GVMerchants.PageIndexChanging
        GVMerchants.PageIndex = e.NewPageIndex
        Bind()
    End Sub

    Protected Sub GvNews_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVMerchants.DataBound
        Dim PageLimit As Integer = 11
        Dim PageSeperator As Integer = (PageLimit - 1) / 2
        Dim pagerRow As GridViewRow = GVMerchants.BottomPagerRow
        If GVMerchants.Rows.Count > 0 Then
            Dim GetPageNow As Integer = GVMerchants.PageIndex + 1
            Dim GetPageCount As Integer = GVMerchants.PageCount
            Dim GetPageLinksIndex As HtmlGenericControl = DirectCast(pagerRow.Cells(0).FindControl("lblPageIndex"), HtmlGenericControl)
            If pagerRow.Visible = True Then
                If GetPageCount <= PageLimit Then
                    CreateGridPagerLinks(1, GVMerchants.PageCount, GetPageNow, GetPageLinksIndex)
                Else
                    Dim GetLeft As Integer = 1
                    Dim GetRight As Integer = PageLimit
                    If GetPageNow > (PageSeperator + 1) Then
                        GetLeft = GetPageNow - PageSeperator
                        GetRight = GetPageNow + PageSeperator
                        If GetRight > GetPageCount Then
                            GetLeft = GetLeft - (GetRight - GetPageCount)
                            If GetLeft < 1 Then
                                GetLeft = 1
                            End If
                            GetRight = GetPageCount
                        End If
                        CreateGridPagerLinks(GetLeft, GetRight, GetPageNow, GetPageLinksIndex)
                    Else
                        CreateGridPagerLinks(GetLeft, GetRight, GetPageNow, GetPageLinksIndex)
                    End If
                End If
            End If
            pagerRow.Cells(0).FindControl("lnkPrev").Visible = (GVMerchants.PageIndex > 0)
            pagerRow.Cells(0).FindControl("lnkNext").Visible = (GVMerchants.PageCount - 1 > GVMerchants.PageIndex)

            '-- Code Begin -- Extra Buttons First Page & Las Page
            pagerRow.Cells(0).FindControl("lnkFirstPage").Visible = (GVMerchants.PageIndex > 0)
            pagerRow.Cells(0).FindControl("lnkLastPage").Visible = (GVMerchants.PageCount - 1 > GVMerchants.PageIndex)
            '-- Code End -- Extra Buttons First Page & Las Page

            Dim btnSaveToPDF As ImageButton = DirectCast(pagerRow.Cells(0).FindControl("btnSaveToPDF"), ImageButton)
            Dim btnSaveToExcel As ImageButton = DirectCast(pagerRow.Cells(0).FindControl("btnSaveToExcel"), ImageButton)

            If pagerRow.Visible = False Then
                pagerRow.Visible = True
            End If
        End If
    End Sub

    Private Sub CreateGridPagerLinks(ByVal StartNo As Integer, ByVal EndNo As Integer, ByVal GetPageNow As Integer, ByVal GetPageLinksIndex As HtmlGenericControl)
        For i As Integer = StartNo To EndNo
            Dim NewDiv As New HtmlGenericControl("div")
            Dim NewLink As New LinkButton
            NewLink.ID = "PagerLink_" & i
            NewLink.OnClientClick = String.Concat("GoToPage('", i, "');return false")
            If i = GetPageNow Then
                NewDiv.Attributes.Add("class", "GridPagerNoSel")
                NewLink.CssClass = "GridPagerNolnkSel"
            Else
                NewDiv.Attributes.Add("class", "GridPagerNo")
                NewLink.CssClass = "GridPagerNolnk"
                NewLink.Style.Add("color", "#4f85d1")
            End If
            NewLink.Text = i
            NewDiv.Controls.Add(NewLink)
            GetPageLinksIndex.Controls.Add(NewDiv)
        Next
    End Sub

#Region " Extra Buttons First Page and Las Page "

    Sub ChangePageByLinkNumber_First(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageList As LinkButton = CType(sender, LinkButton)
        GVMerchants.PageIndex = 0
        GVMerchants.DataSource = Session("GetGVMerchantsDataTable")
        GVMerchants.DataBind()
    End Sub

    Sub ChangePageByLinkNumber_Last(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageList As LinkButton = CType(sender, LinkButton)
        GVMerchants.PageIndex = GVMerchants.PageCount - 1
        GVMerchants.DataSource = Session("GetGVMerchantsDataTable")
        GVMerchants.DataBind()
    End Sub

#End Region

#Region " Buttons Next And Prev "

    Sub ChangePageByLinkNumber_Before(ByVal sender As Object, ByVal e As EventArgs)
        If GVMerchants.PageIndex > 0 Then
            Dim pageList As LinkButton = CType(sender, LinkButton)
            GVMerchants.PageIndex = GVMerchants.PageIndex - 1
            GVMerchants.DataSource = Session("GetGVMerchantsDataTable")
            GVMerchants.DataBind()
        End If
    End Sub

    Sub ChangePageByLinkNumber_Next(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageList As LinkButton = CType(sender, LinkButton)
        GVMerchants.PageIndex = GVMerchants.PageIndex + 1
        GVMerchants.DataSource = Session("GetGVMerchantsDataTable")
        GVMerchants.DataBind()
    End Sub

#End Region

    Protected Sub HiddenPageBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HiddenPageBtn.Click
        GVMerchants.PageIndex = CInt(HiddenPage.Text) - 1
        GVMerchants.DataSource = Session("GetGVMerchantsDataTable")
        GVMerchants.DataBind()
    End Sub
#End Region

End Class
