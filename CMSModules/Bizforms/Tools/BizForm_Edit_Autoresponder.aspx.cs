using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.FormEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.ExtendedControls;
using CMS.FileManager;
using CMS.UIControls;

public partial class CMSModules_BizForms_Tools_BizForm_Edit_Autoresponder : CMSBizFormPage
{
    private int formId = 0;
    private DataClassInfo formClassObj = null;
    private CurrentUserInfo currentUser = null;
    private CurrentSiteInfo currentSite = null;
    protected string mSave = null;

    #region "Private properties"

    /// <summary>
    /// Indicates whether custom form layout is set or not
    /// </summary>
    private bool IsLayoutSet
    {
        get
        {
            object obj = ViewState["IsLayoutSet"];
            return (obj == null) ? false : (bool)obj;
        }

        set
        {
            ViewState["IsLayoutSet"] = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'ReadForm' and 'EditData' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "ReadForm"))
        {
            RedirectToCMSDeskAccessDenied("cms.form", "ReadForm");
        }

        currentUser = CMSContext.CurrentUser;
        currentSite = CMSContext.CurrentSite;

        // get form id from url
        formId = QueryHelper.GetInteger("formid", 0);
        BizFormInfo bfi = BizFormInfoProvider.GetBizFormInfo(formId);
        if (bfi != null)
        {
            this.imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
            mSave = ResHelper.GetString("general.save");

            AttachmentTitle.TitleText = ResHelper.GetString("general.attachments");

            // Control initialization
            lblAvailableFields.Text = ResHelper.GetString("Bizform_Edit_Autoresponder.AvailableFields");
            btnGenerateLayout.Text = ResHelper.GetString("Bizform_Edit_Autoresponder.btnGenerateLayout");
            btnInsertLabel.Text = ResHelper.GetString("Bizform_Edit_Autoresponder.btnInsertLabel");
            btnInsertInput.Text = ResHelper.GetString("Bizform_Edit_Autoresponder.btnInsertInput");

            ltlConfirmDelete.Text = "<input type=\"hidden\" id=\"confirmdelete\" value=\"" + ResHelper.GetString("Bizform_Edit_Autoresponder.ConfirmDelete") + "\">";

            lblEmailField.Text = ResHelper.GetString("bizform_edit_autoresponder.lblemailfield");
            lblEmailFrom.Text = ResHelper.GetString("bizform_edit_autoresponder.lblEmailFrom");

            drpEmailField.SelectedIndexChanged += new EventHandler(drpEmailField_SelectedIndexChanged);

            // Init attachment storage
            AttachmentList.SiteID = CMSContext.CurrentSiteID;
            AttachmentList.AllowPasteAttachments = true;
            AttachmentList.ObjectID = bfi.FormID;
            AttachmentList.ObjectType = FormObjectType.BIZFORM;
            AttachmentList.Category = MetaFileInfoProvider.OBJECT_CATEGORY_FORM_LAYOUT;
            AttachmentList.AllowEdit = currentUser.IsAuthorizedPerResource("cms.form", "EditForm");

            // Initialize HTML editor
            InitHTMLEditor();

            if (!RequestHelper.IsPostBack())
            {
                // Get bizform class object
                formClassObj = DataClassInfoProvider.GetDataClass(bfi.FormClassID);
                if (formClassObj != null)
                {
                    // Enable or disable form
                    EnableDisableForm(bfi.FormConfirmationTemplate);

                    // Fill list of available fields                    
                    FillFieldsList();

                    // Load dropdownlist with form text fields   
                    FormInfo fi = new FormInfo(formClassObj.ClassFormDefinition);
                    drpEmailField.DataSource = fi.GetFields(FormFieldDataTypeEnum.Text);
                    drpEmailField.DataBind();
                    drpEmailField.Items.Insert(0, new ListItem(ResHelper.GetString("bizform_edit_autoresponder.emptyemailfield"), ""));

                    // Try to select specified field
                    ListItem li = drpEmailField.Items.FindByValue(bfi.FormConfirmationEmailField);
                    if (li != null)
                    {
                        li.Selected = true;
                    }

                    // Load email subject and emailfrom address
                    txtEmailFrom.Text = bfi.FormConfirmationSendFromEmail;
                    txtEmailSubject.Text = bfi.FormConfirmationEmailSubject;
                }
                else
                {
                    // Disable form by default
                    EnableDisableForm(null);
                }
            }
        }
    }


    protected void Page_PreRender(Object sender, EventArgs e)
    {
        if (!IsClientScriptRegistered())
        {
            if (!pnlCustomLayout.Visible && IsLayoutSet)
            {
                RegisterSaveDocumentWithDeleteConfirmation();
            }
            else
            {
                RegisterSaveDocument();
            }
        }
    }


    /// <summary>
    /// Register client script block for document saving via 'Ctrl+S'.
    /// </summary>
    protected void RegisterSaveDocument()
    {
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>" +
            ScriptHelper.GetScript("function SaveDocument() { " + this.ClientScript.GetPostBackEventReference(this.lnkSave, null) + " } \n"
        ));
    }


    /// <summary>
    /// Register client script block for document saving via 'Ctrl+S' with layout delete confirmation.
    /// </summary>
    protected void RegisterSaveDocumentWithDeleteConfirmation()
    {
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>" +
            ScriptHelper.GetScript("function SaveDocument() { if (ConfirmDelete()) { " + this.ClientScript.GetPostBackEventReference(this.lnkSave, null) + " } } \n"
        ));
    }


    /// <summary>
    /// Return true if "EditShortcuts" client script block is registered.
    /// </summary>
    protected bool IsClientScriptRegistered()
    {
        return ScriptHelper.IsClientScriptBlockRegistered(this, "EditShortcuts");
    }


    void drpEmailField_SelectedIndexChanged(object sender, EventArgs e)
    {
        pnlCustomLayout.Visible = (drpEmailField.SelectedValue != "");
        AttachmentTitle.Visible = pnlCustomLayout.Visible;
        AttachmentList.Visible = pnlCustomLayout.Visible;

        // Remove content checking and add delete confirmation
        if (!pnlCustomLayout.Visible && IsLayoutSet)
        {
            lnkSave.OnClientClick = "return ConfirmDelete();";
        }
        // Remove delete confirmation and add content checking
        else if (pnlCustomLayout.Visible)
        {
            lnkSave.OnClientClick = "";

            // Reload HTML editor content
            BizFormInfo bfi = BizFormInfoProvider.GetBizFormInfo(formId);
            if (bfi != null && bfi.FormConfirmationTemplate != null)
            {
                htmlEditor.ResolvedValue = bfi.FormConfirmationTemplate;
            }
        }
    }


    /// <summary>
    /// Fills list of available fields
    /// </summary>
    private void FillFieldsList()
    {
        FormInfo fi = new FormInfo();
        FormFieldInfo[] fields = null;

        if (formClassObj != null)
        {
            // load form definition and get visible fields
            fi.LoadXmlDefinition(formClassObj.ClassFormDefinition);
            fields = fi.GetFields(true, true);

            lstAvailableFields.Items.Clear();

            if (fields != null)
            {
                // add visible fields to the list
                foreach (FormFieldInfo ffi in fields)
                {
                    lstAvailableFields.Items.Add(new ListItem(ffi.Name, ffi.Name));
                }
            }
            lstAvailableFields.SelectedIndex = 0;
        }
    }


    /// <summary>
    /// Enables or disables form according to the confirmation email template text is defined or not
    /// </summary>
    protected void EnableDisableForm(string formLayout)
    {

        if (formLayout != null)
        {
            //Enable layout editing                                
            pnlCustomLayout.Visible = true;
            AttachmentList.Visible = true;
            AttachmentTitle.Visible = true;

            // set confirmation email template text to the editable window of the HTML editor
            htmlEditor.ResolvedValue = formLayout;

            // Save info to viewstate 
            IsLayoutSet = true;
        }
        else
        {
            // Layout editing is not enabled by default        
            pnlCustomLayout.Visible = false;
            AttachmentList.Visible = false;
            AttachmentTitle.Visible = false;

            htmlEditor.ResolvedValue = "";

            // Save info to viewstate
            IsLayoutSet = false;

            lnkSave.OnClientClick = "";
        }
    }


    /// <summary>
    /// Displays specified info message and hide error message.
    /// </summary>
    /// <param name="infoMessage">Info message to display.</param>
    protected void DisplayInfoMessage(string infoMessage)
    {
        lblError.Visible = false;
        lblInfo.Visible = true;
        lblInfo.Text = infoMessage;
    }


    /// <summary>
    /// Displays specified error message and hide info message.
    /// </summary>
    /// <param name="errorMessage">Error message to display.</param>
    protected void DisplayErrorMessage(string errorMessage)
    {
        lblInfo.Visible = false;
        lblError.Visible = true;
        lblError.Text = errorMessage;
    }


    /// <summary>
    /// Initializes HTML editor's settings
    /// </summary>
    protected void InitHTMLEditor()
    {
        htmlEditor.AutoDetectLanguage = false;
        htmlEditor.DefaultLanguage = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        htmlEditor.MediaDialogConfig.UseFullURL = true;
        htmlEditor.LinkDialogConfig.UseFullURL = true;
        htmlEditor.QuickInsertConfig.UseFullURL = true;
    }


    /// <summary>
    /// Gets uploaded file binary.
    /// </summary>
    /// <param name="postedFile">Posted file.</param>
    protected byte[] GetAttachmentBinary(HttpPostedFile postedFile)
    {
        byte[] fileBinary = null;

        if (postedFile != null)
        {
            // Get file size and it's binary.
            int size = postedFile.ContentLength;
            fileBinary = new byte[size];
            postedFile.InputStream.Read(fileBinary, 0, size);
        }

        return fileBinary;
    }


    /// <summary>
    /// Gets name of uploaded file.
    /// </summary>
    /// <param name="postedFile">Posted file.</param>
    protected string GetAttachmentName(HttpPostedFile postedFile)
    {
        string fileName = "";

        if (postedFile != null)
        {
            fileName = postedFile.FileName;
        }

        return fileName;
    }


    /// <summary>
    /// Gets MIME type of uploaded file.
    /// </summary>
    /// <param name="postedFile">Posted file.</param>
    protected string GetAttachmentMIMEType(HttpPostedFile postedFile)
    {
        string mimeType = "";

        if (postedFile != null)
        {
            mimeType = postedFile.ContentType;
        }

        return mimeType;
    }


    /// <summary>
    /// Save button is clicked.
    /// </summary>
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        string errorMessage = "";

        // check 'ReadForm' permission
        if (!currentUser.IsAuthorizedPerResource("cms.form", "EditForm"))
        {
            RedirectToCMSDeskAccessDenied("cms.form", "EditForm");
        }

        // Validate form
        errorMessage = new Validator().NotEmpty(txtEmailFrom.Text.Trim(), ResHelper.GetString("bizform_edit_autoresponder.emptyemail")).NotEmpty(txtEmailSubject.Text.Trim(), ResHelper.GetString("bizform_edit_autoresponder.emptysubject")).Result;

        // Check if from e-mail contains macro expression or e-mails separated by semicolon
        if (string.IsNullOrEmpty(errorMessage) && !MacroResolver.ContainsMacro(txtEmailFrom.Text.Trim()) && !ValidationHelper.IsEmail(txtEmailFrom.Text.Trim()))
        {
            errorMessage = ResHelper.GetString("bizform_edit_autoresponder.emptyemail");
        }

        if ((string.IsNullOrEmpty(errorMessage)) || (!pnlCustomLayout.Visible))
        {
            errorMessage = String.Empty;
            BizFormInfo bfi = BizFormInfoProvider.GetBizFormInfo(formId);
            if (bfi != null)
            {
                // Save custom layout
                if (drpEmailField.SelectedValue != "")
                {
                    bfi.FormConfirmationTemplate = htmlEditor.ResolvedValue.Trim();
                    bfi.FormConfirmationEmailField = drpEmailField.SelectedValue;
                    bfi.FormConfirmationEmailSubject = txtEmailSubject.Text.Trim();
                    bfi.FormConfirmationSendFromEmail = txtEmailFrom.Text.Trim();

                    try
                    {
                        BizFormInfoProvider.SetBizFormInfo(bfi);
                        DisplayInfoMessage(ResHelper.GetString("General.ChangesSaved"));
                        EnableDisableForm(bfi.FormConfirmationTemplate);
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.Message;
                    }
                }
                // Delete custom layout if exists
                else if (IsLayoutSet)
                {
                    bfi.FormConfirmationTemplate = null;
                    bfi.FormConfirmationEmailField = drpEmailField.SelectedValue;
                    bfi.FormConfirmationEmailSubject = "";
                    bfi.FormConfirmationSendFromEmail = "";

                    // Delete all attachments
                    MetaFileInfoProvider.DeleteFiles(bfi.FormID, FormObjectType.BIZFORM, MetaFileInfoProvider.OBJECT_CATEGORY_FORM_LAYOUT);

                    try
                    {
                        BizFormInfoProvider.SetBizFormInfo(bfi);
                        DisplayInfoMessage(ResHelper.GetString("Bizform_Edit_Autoresponder.LayoutDeleted"));
                        EnableDisableForm(bfi.FormConfirmationTemplate);
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.Message;
                    }
                }
            }
        }

        if (errorMessage != "")
        {
            DisplayErrorMessage(errorMessage);
        }
    }
}


