using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Categories_Category_Edit_General : SiteManagerPage
{
    #region "Private properties"

    /// <summary>
    /// Current category ID
    /// </summary>
    private int CategoryID
    {
        get
        {
            return QueryHelper.GetInteger("categoryId", 0);
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        // Intialize the control
        SetupControl();
    }

    #region "Private methods"

    /// <summary>
    /// Initializes the controls
    /// </summary>
    private void SetupControl()
    {
        this.catEdit.CategoryID = this.CategoryID;
        this.catEdit.RefreshPageURL = "~/CMSSiteManager/Development/Categories/Category_Edit_General.aspx";
    }

    #endregion
}
