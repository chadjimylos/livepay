CREATE TABLE [COM_Customer] (
		[CustomerID]                            int NOT NULL IDENTITY(1, 1),
		[CustomerFirstName]                     nvarchar(200) NOT NULL,
		[CustomerLastName]                      nvarchar(200) NOT NULL,
		[CustomerEmail]                         nvarchar(200) NULL,
		[CustomerPhone]                         nvarchar(50) NULL,
		[CustomerFax]                           nvarchar(50) NULL,
		[CustomerCompany]                       nvarchar(200) NULL,
		[CustomerUserID]                        int NULL,
		[CustomerPreferredCurrencyID]           int NULL,
		[CustomerPreferredShippingOptionID]     int NULL,
		[CustomerCountryID]                     int NULL,
		[CustomerEnabled]                       bit NOT NULL,
		[CustomerPrefferedPaymentOptionID]      int NULL,
		[CustomerStateID]                       int NULL,
		[CustomerGUID]                          uniqueidentifier NOT NULL,
		[CustomerTaxRegistrationID]             nvarchar(50) NULL,
		[CustomerOrganizationID]                nvarchar(50) NULL,
		[CustomerDiscountLevelID]               int NULL,
		[CustomerCreated]                       datetime NULL,
		[CustomerLastModified]                  datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_Customer]
	ADD
	CONSTRAINT [PK_COM_Customer]
	PRIMARY KEY
	NONCLUSTERED
	([CustomerID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Country]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Customer_CustomerCountryID_CMS_Country]') IS NULL
BEGIN
		ALTER TABLE [COM_Customer]
			ADD CONSTRAINT [FK_COM_Customer_CustomerCountryID_CMS_Country]
			FOREIGN KEY ([CustomerCountryID]) REFERENCES [CMS_Country] ([CountryID])
END
IF OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[COM_DiscountLevel]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Customer_CustomerDiscountLevelID_COM_DiscountLevel]') IS NULL
BEGIN
		ALTER TABLE [COM_Customer]
			ADD CONSTRAINT [FK_COM_Customer_CustomerDiscountLevelID_COM_DiscountLevel]
			FOREIGN KEY ([CustomerDiscountLevelID]) REFERENCES [COM_DiscountLevel] ([DiscountLevelID])
END
IF OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Currency]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Customer_CustomerPreferredCurrencyID_COM_Currency]') IS NULL
BEGIN
		ALTER TABLE [COM_Customer]
			ADD CONSTRAINT [FK_COM_Customer_CustomerPreferredCurrencyID_COM_Currency]
			FOREIGN KEY ([CustomerPreferredCurrencyID]) REFERENCES [COM_Currency] ([CurrencyID])
END
IF OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[COM_ShippingOption]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Customer_CustomerPreferredShippingOptionID_COM_ShippingOption]') IS NULL
BEGIN
		ALTER TABLE [COM_Customer]
			ADD CONSTRAINT [FK_COM_Customer_CustomerPreferredShippingOptionID_COM_ShippingOption]
			FOREIGN KEY ([CustomerPreferredShippingOptionID]) REFERENCES [COM_ShippingOption] ([ShippingOptionID])
END
IF OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[COM_PaymentOption]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Customer_CustomerPrefferedPaymentOptionID_COM_PaymentOption]') IS NULL
BEGIN
		ALTER TABLE [COM_Customer]
			ADD CONSTRAINT [FK_COM_Customer_CustomerPrefferedPaymentOptionID_COM_PaymentOption]
			FOREIGN KEY ([CustomerPrefferedPaymentOptionID]) REFERENCES [COM_PaymentOption] ([PaymentOptionID])
END
IF OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_State]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Customer_CustomerStateID_CMS_State]') IS NULL
BEGIN
		ALTER TABLE [COM_Customer]
			ADD CONSTRAINT [FK_COM_Customer_CustomerStateID_CMS_State]
			FOREIGN KEY ([CustomerStateID]) REFERENCES [CMS_State] ([StateID])
END
IF OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Customer_CustomerUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [COM_Customer]
			ADD CONSTRAINT [FK_COM_Customer_CustomerUserID_CMS_User]
			FOREIGN KEY ([CustomerUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_COM_Customer_CustomerCompany]
	ON [COM_Customer] ([CustomerCompany])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_Customer_CustomerCountryID]
	ON [COM_Customer] ([CustomerCountryID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Customer_CustomerDiscountLevelID]
	ON [COM_Customer] ([CustomerDiscountLevelID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Customer_CustomerEmail]
	ON [COM_Customer] ([CustomerEmail])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_Customer_CustomerLastName_CustomerFirstName_CustomerEnabled]
	ON [COM_Customer] ([CustomerLastName], [CustomerFirstName], [CustomerEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_Customer_CustomerPreferredCurrencyID]
	ON [COM_Customer] ([CustomerPreferredCurrencyID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Customer_CustomerPreferredShippingOptionID]
	ON [COM_Customer] ([CustomerPreferredShippingOptionID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Customer_CustomerPrefferedPaymentOptionID]
	ON [COM_Customer] ([CustomerPrefferedPaymentOptionID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Customer_CustomerStateID]
	ON [COM_Customer] ([CustomerStateID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Customer_CustomerUserID]
	ON [COM_Customer] ([CustomerUserID])
	ON [PRIMARY]
