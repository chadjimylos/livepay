<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductFilter.ascx.cs"
    Inherits="CMSModules_Ecommerce_Controls_Filters_ProductFilter" %>
<%@ Register Src="~/CMSModules/ECommerce/FormControls/PublicStatusSelector.ascx"
    TagName="PublicStatusSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/ECommerce/FormControls/ManufacturerSelector.ascx"
    TagName="ManufacturerSelector" TagPrefix="cms" %>
<table width="100%">
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblStatus" runat="server" EnableViewState="false" />
        </td>
        <td>
            <cms:PublicStatusSelector ID="statusSelector" runat="server" UseStatusNameForSelection="false"
                AddAllItemsRecord="true" />
        </td>
        <td>
            <cms:LocalizedLabel ID="lblManufacturer" runat="server" EnableViewState="false" />
        </td>
        <td>
            <cms:ManufacturerSelector ID="manufacturerSelector" runat="server" AddAllItemsRecord="true" />
        </td>
        <td>
            <cms:LocalizedCheckBox ID="chkStock" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblPaging" AssociatedControlID="drpPaging" EnableViewState="false" runat="server" />
        </td>
        <td>
            <asp:DropDownList ID="drpPaging" runat="server" CssClass="DropDownList" />
        </td>
        <td>
            <asp:Label ID="lblSort" AssociatedControlID="drpSort" EnableViewState="false" runat="server" />
        </td>
        <td>
            <asp:DropDownList ID="drpSort" runat="server" CssClass="DropDownList" />
        </td>
        <td>
            <cms:CMSButton ID="btnFilter" runat="server" CssClass="ContentButton" EnableViewState="false" OnClick="btnFilter_Click" />
        </td>
    </tr>
</table>
