<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DiscountLevel_List.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_DiscountLevel_DiscountLevel_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Discount levels" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="gridElem" GridName="DiscountLevel_List.xml" OrderBy="DiscountLevelDisplayName"
        IsLiveSite="false" Columns="DiscountLevelID, DiscountLevelDisplayName, DiscountLevelValue, DiscountLevelEnabled, DiscountLevelValidFrom, DiscountLevelValidTo" />
</asp:Content>
