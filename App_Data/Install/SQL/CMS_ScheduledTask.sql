CREATE TABLE [CMS_ScheduledTask] (
		[TaskID]                     int NOT NULL IDENTITY(1, 1),
		[TaskName]                   nvarchar(200) NOT NULL,
		[TaskDisplayName]            nvarchar(200) NOT NULL,
		[TaskAssemblyName]           nvarchar(200) NOT NULL,
		[TaskClass]                  nvarchar(200) NULL,
		[TaskInterval]               nvarchar(1000) NOT NULL,
		[TaskData]                   nvarchar(max) NOT NULL,
		[TaskLastRunTime]            datetime NULL,
		[TaskNextRunTime]            datetime NULL,
		[TaskProgress]               int NULL,
		[TaskLastResult]             nvarchar(max) NULL,
		[TaskEnabled]                bit NOT NULL,
		[TaskSiteID]                 int NULL,
		[TaskDeleteAfterLastRun]     bit NULL,
		[TaskServerName]             nvarchar(100) NULL,
		[TaskGUID]                   uniqueidentifier NOT NULL,
		[TaskLastModified]           datetime NOT NULL,
		[TaskExecutions]             int NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_ScheduledTask]
	ADD
	CONSTRAINT [PK_CMS_ScheduledTask]
	PRIMARY KEY
	NONCLUSTERED
	([TaskID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_ScheduledTask]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_ScheduledTask_TaskSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_ScheduledTask]
			ADD CONSTRAINT [FK_CMS_ScheduledTask_TaskSiteID_CMS_Site]
			FOREIGN KEY ([TaskSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_CMS_ScheduledTask_TaskNextRunTime_TaskEnabled_TaskServerName]
	ON [CMS_ScheduledTask] ([TaskNextRunTime], [TaskEnabled], [TaskServerName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_ScheduledTask_TaskSiteID_TaskDisplayName]
	ON [CMS_ScheduledTask] ([TaskSiteID], [TaskDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
