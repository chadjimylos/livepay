using System;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSModules_Staging_Tools_Header : CMSStagingPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentMaster.Title.TitleText = ResHelper.GetString("Staging.HeaderCaption");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Staging/object.png");

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }
    }


    /// <summary>
    /// Initializes menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string[,] tabs = new string[5, 4];

        int index = 0;

        CurrentUserInfo currentUser = CMSContext.CurrentUser;

        // All tasks
        if (currentUser.IsAuthorizedPerResource("cms.staging", "ManageAllTasks"))
        {
            tabs[index, 0] = ResHelper.GetString("Staging.AllTasks");
            tabs[index, 2] = "AllTasks/TaskSeparator.aspx";
            index++;
        }

        // Document tasks
        if (currentUser.IsAuthorizedPerResource("cms.staging", "ManageTasks"))
        {
            tabs[index, 0] = ResHelper.GetString("Staging.Tasks");
            tabs[index, 2] = "Tasks/TaskSeparator.aspx";
            index++;
        }

        // Data tasks
        if (currentUser.IsAuthorizedPerResource("cms.staging", "ManageDataTasks"))
        {
            tabs[index, 0] = ResHelper.GetString("Staging.Data");
            tabs[index, 2] = "Data/TaskSeparator.aspx";
            index++;
        }

        // Object tasks
        if (currentUser.IsAuthorizedPerResource("cms.staging", "ManageObjectTasks"))
        {
            tabs[index, 0] = ResHelper.GetString("Staging.Objects");
            tabs[index, 2] = "Objects/TaskSeparator.aspx";
            index++;
        }

        // Servers
        if (currentUser.IsAuthorizedPerResource("cms.staging", "ManageServers"))
        {
            tabs[index, 0] = ResHelper.GetString("Staging.Servers");
            tabs[index, 2] = "Servers/List.aspx";
        }

        CurrentMaster.Tabs.Tabs = tabs;
        CurrentMaster.Tabs.UrlTarget = "stagingContent";
    }
}
