using System;
using System.Data;
using System.Threading;
using System.Collections;
using System.Security.Principal;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.ExtendedControls;

using TreeNode = CMS.TreeEngine.TreeNode;
using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSSiteManager_Administration_RecycleBin_Controls_RecycleBin : CMSUserControl
{
    #region "Private variables"

    private bool mIsCMSDesk = false;
    private int mSiteID = 0;
    private string currentCulture = CultureHelper.DefaultUICulture;
    private CurrentUserInfo currentUser = null;
    private SiteInfo currentSite = null;
    private TimeZoneInfo usedTimeZone = null;
    private SiteInfo si = null;
    private static readonly Hashtable mLogs = new Hashtable();
    private static readonly Hashtable mErrors = new Hashtable();
    private static readonly Hashtable mInfos = new Hashtable();

    #endregion


    #region "Properties"

    public bool IsCMSDesk
    {
        get
        {
            return mIsCMSDesk;
        }
        set
        {
            mIsCMSDesk = value;
        }
    }


    /// <summary>
    /// ID of site
    /// </summary>
    public int SiteID
    {
        get
        {
            return mSiteID;
        }
        set
        {
            mSiteID = value;
        }
    }


    /// <summary>
    /// Current log
    /// </summary>
    public string CurrentLog
    {
        get
        {
            return ValidationHelper.GetString(mLogs["RestoreLog_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mLogs["RestoreLog_" + ctlAsync.ProcessGUID] = value;
        }
    }


    /// <summary>
    /// Current Error
    /// </summary>
    public string CurrentError
    {
        get
        {
            return ValidationHelper.GetString(mErrors["RestoreError_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mErrors["RestoreError_" + ctlAsync.ProcessGUID] = value;
        }
    }


    /// <summary>
    /// Current Info
    /// </summary>
    public string CurrentInfo
    {
        get
        {
            return ValidationHelper.GetString(mInfos["RestoreInfo_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mInfos["RestoreInfo_" + ctlAsync.ProcessGUID] = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register script for pendingCallbacks repair
        ScriptHelper.FixPendingCallbacks(this.Page);

        // Set current UI culture
        currentCulture = CultureHelper.PreferredUICulture;

        // Get current user info
        currentUser = CMSContext.CurrentUser;

        // Set the full postabck because async control cannot be in update panel
        ControlsHelper.RegisterPostbackControl(lnkRestoreSelected);
        ControlsHelper.RegisterPostbackControl(lnkEmptyRecBin);
        ControlsHelper.RegisterPostbackControl(lnkRestoreAll);

        if (!RequestHelper.IsCallback())
        {
            // Initialize query parameters
            object[,] parameters = new object[1 + Convert.ToInt32(IsCMSDesk), 3];
            parameters[0, 0] = "@SiteID";
            parameters[0, 1] = SiteID;

            // Initialize UniGrid
            if (IsCMSDesk)
            {
                parameters[1, 0] = "@UserID";
                parameters[1, 1] = ValidationHelper.GetInteger(currentUser.UserID, 0);
            }
            ugRecycleBin.OrderBy = "DocumentNamePath";
            ugRecycleBin.QueryParameters = parameters;
            ugRecycleBin.Query = IsCMSDesk ? "cms.versionhistory.selectmyrecyclebin" : "cms.versionhistory.selectallrecyclebin";
            ugRecycleBin.Columns = "VersionHistoryID, DocumentID, NodeSiteID, DocumentNamePath, ModifiedByUserID, ModifiedWhen, VersionNumber, VersionComment, ToBePublished, PublishFrom, PublishTo, WasPublishedFrom, WasPublishedTo";
            ugRecycleBin.OnExternalDataBound += ugRecycleBin_OnExternalDataBound;
            ugRecycleBin.OnAction += ugRecycleBin_OnAction;
            ugRecycleBin.ZeroRowsText = IsCMSDesk ? ResHelper.GetString("RecycleBin.NoDocuments") : ResHelper.GetString("RecycleBin.Empty");
            ugRecycleBin.HideControlForZeroRows = false;
            ugRecycleBin.OnAfterDataReload += new OnAfterDataReload(ugRecycleBin_OnAfterDataReload);

            // Register the dialog script
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

            // Register script for viewing versions
            string viewVersionScript = "function ViewVersion(versionHistoryId) {modalDialog('" +
                                          ResolveUrl("~/CMSSiteManager/Administration/RecycleBin/ViewVersion.aspx") +
                                                     "?noCompare=1&versionHistoryId=' + versionHistoryId, 'contentversion', 900, 600);}";

            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "viewVersionScript", ScriptHelper.GetScript(viewVersionScript));

            imgRestoreAll.ImageUrl = GetImageUrl("CMSModules/CMS_RecycleBin/restoreall.png");
            imgRestoreSelected.ImageUrl = GetImageUrl("CMSModules/CMS_RecycleBin/restoreselected.png");
            imgEmptyRecBin.ImageUrl = GetImageUrl("CMSModules/CMS_RecycleBin/emptybin.png");

            // Initialize buttons
            btnCancel.Attributes.Add("onclick", "CancelAction(); return false;");

            // Emptying bin confirmation
            lnkEmptyRecBin.Attributes.Add("onclick", "if(!confirm(" + ScriptHelper.GetString(ResHelper.GetString("recyclebin.confirmemptyrecbin")) + ")){return false;}");


            string error = QueryHelper.GetString("displayerror", String.Empty);
            if (error != String.Empty)
            {
                lblError.Text = ResHelper.GetString("recyclebin.errorsomenotdestroyed");
            }


            // Set visibility of panels
            pnlContent.Visible = true;
            pnlLog.Visible = false;
        }
        else
        {
            ugRecycleBin.StopProcessing = true;

        }

        // Initialize events
        ctlAsync.OnFinished += ctlAsync_OnFinished;
        ctlAsync.OnError += ctlAsync_OnError;
        ctlAsync.OnRequestLog += ctlAsync_OnRequestLog;
        ctlAsync.OnCancel += ctlAsync_OnCancel;
    }

    void ugRecycleBin_OnAfterDataReload()
    {
        // If unigrid is empty after data reload and current page is > 1
        if (DataHelper.DataSourceIsEmpty(ugRecycleBin.GridView.DataSource) && (ugRecycleBin.Pager.CurrentPage > 1))
        {
            // Reload unigrid for first page
            ugRecycleBin.Pager.UniPager.CurrentPage = 1;
            ugRecycleBin.ReloadData();
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        // Show labels
        lblError.Visible = (lblError.Text != string.Empty);
        lblInfo.Visible = (lblInfo.Text != string.Empty);

        // Hide action buttons if grid is empty
        pnlActions.Visible = ugRecycleBin.GridView.Rows.Count > 0;

        ugRecycleBin.GridView.Columns[3].Visible = (SiteID == 0) && !IsCMSDesk;
        base.OnPreRender(e);
    }

    #endregion


    #region "Restoring & destroying methods"

    /// <summary>
    /// Restores all documents
    /// </summary>
    public void RestoreAll(object parameter)
    {
        try
        {
            CurrentUserInfo currentUserInfo = (CurrentUserInfo)parameter;
            // Result flag
            bool resultOK = true;

            // Begin log
            AddLog(ResHelper.GetString("Recyclebin.RestoringDocuments", currentCulture));

            TreeProvider tree = new TreeProvider(currentUserInfo);
            tree.AllowAsyncActions = false;
            VersionManager verMan = new VersionManager(tree);

            if (!DataHelper.DataSourceIsEmpty(ugRecycleBin.GridView.DataSource))
            {
                DataTable dt = ((DataSet)ugRecycleBin.GridView.DataSource).Tables[0];
                // Sort table and set primary key (to ensure removing order)
                dt.DefaultView.Sort = "DocumentNamePath ASC";
                dt.PrimaryKey = new DataColumn[] { dt.Columns["VersionHistoryID"] };

                // Restore selected documents
                for (int i = 0; i < dt.DefaultView.Table.Rows.Count; i++)
                {
                    int versionId = Convert.ToInt32(dt.DefaultView[i]["VersionHistoryID"]);

                    // Log actual event
                    DataRow dr = dt.DefaultView.Table.Rows.Find(versionId);
                    string taskTitle = HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["DocumentNamePath"], string.Empty));
                    AddLog(taskTitle);

                    // Restore document
                    if (versionId > 0)
                    {
                        TreeNode tn = verMan.RestoreDocument(Convert.ToInt32(dr["VersionHistoryID"]));
                        // Set result flag
                        if (resultOK)
                        {
                            resultOK = (tn != null);
                        }
                    }
                }
            }

            if (!resultOK)
            {
                CurrentError = ResHelper.GetString("Recyclebin.RestorationFailed", currentCulture);
                AddLog(CurrentError);
            }
            else
            {
                CurrentInfo = ResHelper.GetString("Recyclebin.RestorationOK", currentCulture);
                AddLog(CurrentInfo);
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                // When canceled
                CurrentInfo = ResHelper.GetString("Recyclebin.RestorationCanceled", currentCulture);
                AddLog(CurrentInfo);
            }
            else
            {
                // Log error
                CurrentError = ResHelper.GetString("Recyclebin.RestorationFailed", currentCulture) + ": " + ex.Message;
                AddLog(CurrentError);
            }
        }
        catch (Exception ex)
        {
            // Log error
            CurrentError = ResHelper.GetString("Recyclebin.RestorationFailed", currentCulture) + ": " + ex.Message;
            AddLog(CurrentError);
        }
    }


    /// <summary>
    /// Restores documents selected in UniGrid
    /// </summary>
    private void RestoreSelected(object parameter)
    {
        try
        {
            CurrentUserInfo currentUserInfo = (CurrentUserInfo)parameter;
            // Result flag
            bool resultOK = true;

            // Begin log
            AddLog(ResHelper.GetString("Recyclebin.RestoringDocuments", currentCulture));

            TreeProvider tree = new TreeProvider(currentUserInfo);
            //tree.AllowAsyncActions = false;
            VersionManager verMan = new VersionManager(tree);
            // ArrayList with IDs to restore
            ArrayList toRestore = ugRecycleBin.SelectedItems;

            // Restore selected documents
            if (toRestore.Count > 0)
            {
                foreach (object o in toRestore)
                {
                    int versionId = Convert.ToInt32(o);

                    // Restore document
                    if (versionId > 0)
                    {
                        TreeNode tn = verMan.RestoreDocument(versionId);

                        // Log task
                        if (tn != null)
                        {
                            string taskTitle = HTMLHelper.HTMLEncode(ValidationHelper.GetString(tn.DocumentNamePath, string.Empty));
                            AddLog(taskTitle);
                        }

                        // Set result flag
                        if (resultOK)
                        {
                            resultOK = (tn != null);
                        }
                    }
                }
            }

            if (resultOK)
            {
                CurrentInfo = ResHelper.GetString("Recyclebin.RestorationOK", currentCulture);
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Recyclebin.RestorationFailed", currentCulture);
                AddLog(CurrentError);
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                // When canceled
                CurrentInfo = ResHelper.GetString("Recyclebin.RestorationCanceled", currentCulture);
                AddLog(CurrentInfo);
            }
            else
            {
                // Log error
                CurrentError = ResHelper.GetString("Recyclebin.RestorationFailed", currentCulture) + ": " + ex.Message;
                AddLog(CurrentError);
            }
        }
        catch (Exception ex)
        {
            // Log error
            CurrentError = ResHelper.GetString("Recyclebin.RestorationFailed", currentCulture) + ": " + ex.Message;
            AddLog(CurrentError);
        }
    }


    /// <summary>
    /// Empties recycle bin.
    /// </summary>
    private void EmptyBin(object parameter)
    {
        // Begin log
        AddLog(ResHelper.GetString("Recyclebin.EmptyingBin", currentCulture));
        CurrentUserInfo currentUserInfo = (CurrentUserInfo)parameter;

        TreeProvider tree = new TreeProvider(currentUserInfo);
        tree.AllowAsyncActions = false;
        VersionManager verMan = new VersionManager(tree);
        TreeNode deletedNode = null;
        bool userHasGlobalDestroyPerm = currentUserInfo.IsAuthorizedPerResource("CMS.Content", "Destroy");

        DataSet ds = IsCMSDesk ? VersionHistoryInfoProvider.GetRecycleBin(SiteID, currentUserInfo.UserID) : VersionHistoryInfoProvider.GetRecycleBin(SiteID);

        try
        {
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                DataTable dt = (ds).Tables[0];
                // Sort table and set primary key (to ensure removing order)
                dt.DefaultView.Sort = "DocumentNamePath ASC";
                dt.PrimaryKey = new DataColumn[] { dt.Columns["VersionHistoryID"] };
                foreach (DataRow dr in dt.DefaultView.ToTable().Rows)
                {
                    int versionHistoryId = Convert.ToInt32(dr["VersionHistoryID"]);
                    string className = null;
                    int aclId = 0;

                    try
                    {
                        // Get the values form deleted node
                        deletedNode = verMan.GetVersion(versionHistoryId, null);
                        aclId = Convert.ToInt32(deletedNode.GetValue("NodeACLID"));
                        className = deletedNode.NodeClassName;
                    }
                    catch (ThreadAbortException)
                    {
                        throw;
                    }
                    catch (Exception ex)
                    {
                        CurrentError = "Error occurred: " + ex.Message;
                        AddLog(CurrentError);
                    }

                    // Check permissions
                    if (!((userHasGlobalDestroyPerm || currentUserInfo.IsAuthorizedPerClassName(className, "Destroy") ||
                           (TreeSecurityProvider.IsAuthorizedPerACL(aclId, NodePermissionsEnum.Destroy, currentUserInfo, CMSContext.CurrentSiteID) ==
                            AuthorizationResultEnum.Allowed))))
                    {
                        CurrentError = ResHelper.GetString("recyclebin.errorsomenotdestroyed", currentCulture);
                        AddLog(CurrentError);
                    }
                    else
                    {
                        AddLog(HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["DocumentNamePath"], string.Empty)));
                        // Destroy the version
                        verMan.DestroyDocumentHistory(ValidationHelper.GetInteger(dr["DocumentID"], 0));
                    }
                }
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state != CMSThread.ABORT_REASON_STOP)
            {
                // Log error
                CurrentError = "Error occurred: " + ex.Message;
                AddLog(CurrentError);
            }
        }
        catch (Exception ex)
        {
            // Log error
            CurrentError = "Error occurred: " + ex.Message;
            AddLog(CurrentError);
        }
    }

    #endregion


    #region "Handling async thread"

    private void ctlAsync_OnCancel(object sender, EventArgs e)
    {
        HandlePossibleErrors();
    }


    private void ctlAsync_OnRequestLog(object sender, EventArgs e)
    {
        ctlAsync.Log = CurrentLog;
    }


    private void ctlAsync_OnError(object sender, EventArgs e)
    {
        HandlePossibleErrors();
    }


    private void ctlAsync_OnFinished(object sender, EventArgs e)
    {
        HandlePossibleErrors();
    }


    /// <summary>
    /// Adds the log information
    /// </summary>
    /// <param name="newLog">New log information</param>
    protected void AddLog(string newLog)
    {
        CurrentLog = (newLog + "<br />" + CurrentLog);
    }


    private void HandlePossibleErrors()
    {
        TerminateCallbacks();
        lblError.Text = CurrentError;
        lblInfo.Text = CurrentInfo;
        ugRecycleBin.ResetSelection();
    }


    private void TerminateCallbacks()
    {
        string terminatingScript = ScriptHelper.GetScript("CancelWithoutPostback();var __pendingCallbacks = new Array();");
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "terminatePendingCallbacks", terminatingScript);
    }


    /// <summary>
    /// Runs async thread
    /// </summary>
    /// <param name="action">Method to run</param>
    protected void RunAsync(AsyncAction action)
    {
        pnlLog.Visible = true;
        pnlContent.Visible = false;

        CurrentLog = string.Empty;
        CurrentError = string.Empty;
        CurrentInfo = string.Empty;

        ctlAsync.RunAsync(action, WindowsIdentity.GetCurrent());
    }

    #endregion


    #region "Button handling"

    protected void btnEmptyRecBin_Click(Object sender, EventArgs e)
    {
        titleElemAsync.TitleText = ResHelper.GetString("recyclebin.emptyingbin");
        titleElemAsync.TitleImage = GetImageUrl("CMSModules/CMS_RecycleBin/emptybin.png");
        ctlAsync.Parameter = currentUser;
        RunAsync(EmptyBin);
    }


    protected void btnRestoreAll_Click(object sender, EventArgs e)
    {
        titleElemAsync.TitleText = ResHelper.GetString("Recyclebin.RestoringDocuments");
        titleElemAsync.TitleImage = GetImageUrl("CMSModules/CMS_RecycleBin/restoreall.png");
        ctlAsync.Parameter = currentUser;
        RunAsync(RestoreAll);
    }


    protected void btnRestoreSelected_Click(object sender, EventArgs e)
    {
        titleElemAsync.TitleText = ResHelper.GetString("Recyclebin.RestoringDocuments");
        titleElemAsync.TitleImage = GetImageUrl("CMSModules/CMS_RecycleBin/restoreselected.png");
        if (ugRecycleBin.SelectedItems.Count > 0)
        {
            ctlAsync.Parameter = currentUser;
            RunAsync(RestoreSelected);
        }
    }

    #endregion


    #region "Grid events"

    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }


    protected object ugRecycleBin_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        sourceName = sourceName.ToLower();
        switch (sourceName)
        {
            case "view":
                ImageButton btnView = (ImageButton)sender;
                GridViewRow row = (GridViewRow)parameter;
                object siteId = DataHelper.GetDataRowViewValue((DataRowView)row.DataItem, "NodeSiteID");
                si = SiteInfoProvider.GetSiteInfo(ValidationHelper.GetInteger(siteId, 0));
                if (si != null)
                {
                    if (si.Status == SiteStatusEnum.Stopped)
                    {
                        btnView.Enabled = false;
                        btnView.Style.Add("cursor", "default");
                        btnView.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Viewdisabled.png");
                    }
                }
                break;

            case "nodesiteid":
                if (si != null)
                {
                    return si.DisplayName;
                }
                else
                {
                    return string.Empty;
                }

            case "modifiedwhen":
            case "modifiedwhentooltip":
                if (string.IsNullOrEmpty(parameter.ToString()))
                {
                    return string.Empty;
                }
                else
                {
                    DateTime modifiedWhen = ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME);
                    if (currentUser == null)
                    {
                        currentUser = CMSContext.CurrentUser;
                    }
                    if (currentSite == null)
                    {
                        currentSite = CMSContext.CurrentSite;
                    }

                    if (sourceName == "modifiedwhen")
                    {
                        return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(modifiedWhen, currentUser, currentSite, out usedTimeZone);
                    }
                    else
                    {
                        if (TimeZoneHelper.TimeZonesEnabled() && (usedTimeZone == null))
                        {
                            TimeZoneHelper.GetCurrentTimeZoneDateTimeString(modifiedWhen, currentUser, currentSite, out usedTimeZone);
                        }
                        return TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone);
                    }
                }
        }

        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void ugRecycleBin_OnAction(string actionName, object actionArgument)
    {
        SiteInfo info = SiteInfoProvider.GetSiteInfo(SiteID);
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        VersionManager verMan = new VersionManager(tree);
        if (actionName == "restore")
        {
            int versionHistoryId = ValidationHelper.GetInteger(actionArgument, 0);
            try
            {
                verMan.RestoreDocument(versionHistoryId);
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("recyclebin.errorrestoringdocument") + " " + ex.Message;
            }
        }
        else if (actionName == "destroy")
        {
            int documentId = ValidationHelper.GetInteger(actionArgument, 0);
            verMan.DestroyDocumentHistory(documentId);
        }
        ugRecycleBin.ReloadData();
        ugRecycleBin.ResetSelection();
    }

    #endregion
}
