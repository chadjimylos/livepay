<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CustomerSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_FormControls_CustomerSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%CustomerFirstName%}, {%CustomerLastName%}, {%CustomerEmail%}"
            ObjectType="ecommerce.customer" ResourcePrefix="customerselector" ReturnColumnName="CustomerID"
            SelectionMode="SingleTextBox" AllowEditTextBox="false" AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
