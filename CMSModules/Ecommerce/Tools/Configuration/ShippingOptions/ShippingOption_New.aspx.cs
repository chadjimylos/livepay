using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_ShippingOptions_ShippingOption_New : CMSEcommerceConfigurationPage
{
    protected int shippingOptionID = 0;


	protected void Page_Load(object sender, EventArgs e)
	{
        // initializes page title and BreadCrumbs
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("ShippingOption_EditHeader.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/ShippingOptions/ShippingOption_List.aspx";
        pageTitleTabs[0, 2] = "configEdit";
        pageTitleTabs[1, 0] = ResHelper.GetString("com_shippingoption_edit.newitemcaption");
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("com_shippingoption_edit.newheadercaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_ShippingOption/new.png");
        this.CurrentMaster.Title.HelpTopicName = "newgeneral_tab2";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Required field validator error messages initialization
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvName.ErrorMessage = ResHelper.GetString("COM_ShippingOption_Edit.NameError");
        txtShippingOptionCharge.EmptyErrorMessage = ResHelper.GetString("COM_ShippingOption_Edit.ChargeError");
        txtShippingOptionCharge.ValidationErrorMessage = ResHelper.GetString("COM_ShippingOption_Edit.ChargePositive");
				
		// control initializations						
		lblShippingOptionDisplayName.Text = ResHelper.GetString("COM_ShippingOption_Edit.ShippingOptionDisplayNameLabel");
		lblShippingOptionCharge.Text = ResHelper.GetString("COM_ShippingOption_Edit.ShippingOptionChargeLabel");
		lblShippingOptionName.Text = ResHelper.GetString("COM_ShippingOption_Edit.ShippingOptionNameLabel");

		btnOk.Text = ResHelper.GetString("General.OK");	

		string currentShippingOption = ResHelper.GetString("COM_ShippingOption_Edit.NewItemCaption");
		// get shippingOption id from querystring		
        shippingOptionID = ValidationHelper.GetInteger(Request.QueryString["ShippingOptionID"], 0);
        if (shippingOptionID > 0)
        {
            ShippingOptionInfo shippingOptionObj = ShippingOptionInfoProvider.GetShippingOptionInfo(shippingOptionID);
            if (shippingOptionObj != null)
            {
                currentShippingOption = shippingOptionObj.ShippingOptionDisplayName;
            }
        }
    }


	/// <summary>
	/// Sets data to database.
	/// </summary>
	protected void btnOK_Click(object sender, EventArgs e)
	{
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        string errorMessage = new Validator()
            .NotEmpty(txtShippingOptionDisplayName.Text, rfvDisplayName.ErrorMessage)
            .NotEmpty(txtShippingOptionName.Text, rfvName.ErrorMessage).Result;

        if (!ValidationHelper.IsCodeName(txtShippingOptionName.Text))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

		if (errorMessage == "")
		{
			
			// shippingOptionName must to be unique
			ShippingOptionInfo shippingOptionObj = ShippingOptionInfoProvider.GetShippingOptionInfo(txtShippingOptionName.Text.Trim(), CMSContext.CurrentSiteName);

			// if shippingOptionName value is unique														
			if ((shippingOptionObj == null) || (shippingOptionObj.ShippingOptionID == shippingOptionID))			
			{
				// if shippingOptionName value is unique -> determine whether it is update or insert 
				if ((shippingOptionObj == null))
				{
					// get ShippingOptionInfo object by primary key
					shippingOptionObj = ShippingOptionInfoProvider.GetShippingOptionInfo(shippingOptionID);
					if (shippingOptionObj == null)
					{
						// create new item -> insert
						shippingOptionObj = new ShippingOptionInfo();                        
					}			        									
				}

                if (CMSContext.CurrentSite != null)
                {
                    shippingOptionObj.ShippingOptionSiteID = CMSContext.CurrentSite.SiteID;
                }
				shippingOptionObj.ShippingOptionDisplayName = txtShippingOptionDisplayName.Text.Trim();
                shippingOptionObj.ShippingOptionCharge = txtShippingOptionCharge.Value;
				shippingOptionObj.ShippingOptionName = txtShippingOptionName.Text.Trim();
				shippingOptionObj.ShippingOptionEnabled = chkShippingOptionEnabled.Checked;
                shippingOptionObj.ShippingOptionSiteID = CMSContext.CurrentSite.SiteID;
				
				ShippingOptionInfoProvider.SetShippingOptionInfo(shippingOptionObj);

				UrlHelper.Redirect("ShippingOption_Edit_Frameset.aspx?ShippingOptionID=" + Convert.ToString(shippingOptionObj.ShippingOptionID) + "&saved=1");
			}
			else
			{
				lblError.Visible = true;
				lblError.Text = ResHelper.GetString("ShippingOption_Edit.ShippingOptionNameExists");
			}
		}
		else
		{
			lblError.Visible = true;
			lblError.Text = errorMessage;
		}
	}
}
