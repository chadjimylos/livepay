// Register script displaying safe modal page dialog
function mediaModalDialog(url, name, width, height, otherParams) {
    win = window;
    var dHeight = height;
    var dWidth = width;
    if (width.toString().indexOf('%') != -1) {
        dWidth = Math.round(screen.width * parseInt(width, 10) / 100);
    }
    if (height.toString().indexOf('%') != -1) {
        dHeight = Math.round(screen.height * parseInt(height, 10) / 100);
    }
    if ((document.all) && (navigator.appName != 'Opera')) {
        if (otherParams == undefined) {
            otherParams = 'resizable:yes;scroll:no';
        }
        var vopenwork = true;
        try {
            win = wopener.window;
        }
        catch (e) {
            vopenwork = false;
        }

        if (parseInt(navigator.appVersion.substr(22, 1)) < 7) {
            dWidth += 4;
            dHeight += 58;
        }

        try {
            dialog = win.showModalDialog(url, this, 'dialogWidth:' + dWidth + 'px;dialogHeight:' + dHeight + 'px;' + otherParams);
        }
        catch (e) {
            if (vopenwork) {
                window.showModalDialog(url, this, 'dialogWidth:' + dWidth + 'px;dialogHeight:' + dHeight + 'px;' + otherParams);
            }
        }
    }
    else {
        if (otherParams == undefined) {
            otherParams = 'toolbar=no,directories=no,menubar=no,modal=yes,dependent=yes,resizable=yes';
        }
        oWindow = win.open(url, name, 'width=' + dWidth + ',height=' + dHeight + ',' + otherParams);
        oWindow.opener = this;
        oWindow.focus();
    }
}


// Create and define new FCKEditor button command openning the popup window
var InsertImageOrMediaCommand = function() {
}

InsertImageOrMediaCommand.GetState = function() {
    if (FCK.EditMode != FCK_EDITMODE_WYSIWYG)
        return FCK_TRISTATE_DISABLED;
    else {
        return FCK_TRISTATE_OFF;
    }
}

InsertImageOrMediaCommand.Execute = function() {

    window.FCK_ACTIVE = window.FCK.Name;

    var url = FCKConfig["MediaDialogURL"];
    var width = FCKConfig["MediaDialogWidth"];
    var height = FCKConfig["MediaDialogHeight"];

    mediaModalDialog(FCKPlugins.Items['InsertImageOrMedia'].Path + url, FCKLang.InsertMediaOrImageTitle, width, height, null);
}

// Register the Image or Media command.
FCKCommands.RegisterCommand('InsertImageOrMedia', InsertImageOrMediaCommand);

var oInsertImageOrMedia = new FCKToolbarButton('InsertImageOrMedia', FCKLang.InsertMediaOrImageButton);
oInsertImageOrMedia.IconPath = FCKPlugins.Items['InsertImageOrMedia'].Path + 'fckImageOrMedia.gif';
FCKToolbarItems.RegisterItem('InsertImageOrMedia', oInsertImageOrMedia);

FCK.ContextMenu.RegisterListener({
    AddItems: function(contextMenu, tag, tagName) {
        if (tagName == 'IMG' || tagName == 'img') {
            if ((!tag.getAttribute('_fckfakelement')) || ((tag.getAttribute('_fckmedia')) && (!tag.getAttribute('_fckyoutube')))) {
                // Clear existing context menu and create new
                contextMenu.RemoveAllItems();
                contextMenu.AddItem('Cut', FCKLang.Cut, 7, FCKCommands.GetCommand('Cut').GetState() == FCK_TRISTATE_DISABLED);
                contextMenu.AddItem('Copy', FCKLang.Copy, 8, FCKCommands.GetCommand('Copy').GetState() == FCK_TRISTATE_DISABLED);
                contextMenu.AddItem('Paste', FCKLang.Paste, 9, FCKCommands.GetCommand('Paste').GetState() == FCK_TRISTATE_DISABLED);
                contextMenu.AddSeparator();
                contextMenu.AddItem('InsertImageOrMedia', FCKLang.InsertMediaOrImageEdit, FCKPlugins.Items['InsertImageOrMedia'].Path + 'fckImageOrMedia.gif');
            }
        }
    }
});

//==================================================================================================
//                                     FCK Media object
//==================================================================================================

var FCKMedia = new Object();

FCKMedia.oFakeImage = null;
FCKMedia.oInline = null;

FCKMedia.OnDoubleClick = function(img) {
    if (!FCK.Disabled) {
        if (img.tagName == 'IMG' || img.tagName == 'img') {
            if (img.getAttribute('_fckmedia')) {
                FCKCommands.GetCommand('InsertImageOrMedia').Execute();
            }
            else {
                var origElem = FCK.GetRealElement(img);
                if (!origElem) {
                    FCKCommands.GetCommand('InsertImageOrMedia').Execute();
                }
            }
        }
    }
}

FCK.RegisterDoubleClickHandler(FCKMedia.OnDoubleClick, 'IMG');

FCKMedia.Add = function(obj) {
    if (((obj.img_url) && (obj.img_url != '')) ||
    ((obj.flash_url) && (obj.flash_url != '')) ||
    ((obj.av_url) && (obj.av_url != ''))) {
        this.oFakeImage = FCK.Selection.GetSelectedElement();
        this.oInline = null;
        FCKMedia._Create(obj)
    }
}

FCKMedia._Create = function(obj) {
    if (this.oFakeImage) {
        // Get inline element if exist
        if (this.oFakeImage.tagName == 'IMG' && this.oFakeImage.getAttribute('_fckmedia')) {
            this.oInline = FCK.GetRealElement(this.oFakeImage);
        }
        else {
            this.oFakeImage = null;
        }
    }
    if (!this.oInline) {
        // Create new inline element
        this.oInline = FCK.EditorDocument.createTextNode(' ');
        this.oFakeImage = null;
    }

    FCKMedia._UpdateInline(this.oInline, this.oFakeImage, obj, true);

    if (!this.oFakeImage) {
        // Create fake img
        this.oFakeImage = FCKDocumentProcessor_CreateFakeImage('FCK__Media', this.oInline);

        this.oFakeImage.setAttribute('_fckmedia', 'true', 0);
        this.oFakeImage = FCK.InsertElement(this.oFakeImage);
    }
    FCKMedia._SetClass(obj);
    FCKMedia._RefreshView();
    FCKMedia._UpdateLink(obj);
}

FCKMedia.Redraw = function() {
    if (FCK.EditMode != FCK_EDITMODE_WYSIWYG) {
        var arr = new Array();
        var fBody = FCK.EditorDocument.body;
        FCKMedia._BuildNodes(fBody, arr);
        alert(arr.length);
        return;
    }

    var arr = new Array();
    var fBody = FCK.EditorDocument.body;
    FCKMedia._BuildNodes(fBody, arr);

    for (var i = 0; i < arr.length; i++) {
        if (/{\^(?:Media|Image)(?:[^}]*)}/i.test(arr[i].data)) {
            // text node value
            var nodeValue = arr[i].data;
            // text node parrent node
            var node = arr[i].parentNode;
            FCKMedia._CreateNodes(node, arr[i], nodeValue);
            // remove original text node
            node.removeChild(arr[i]);
        }
    }
}

FCKMedia.Parse = function(nodeValue) {
    var match = nodeValue.match(/\([^\(\)]+\)|[^|\^{}]+/g);
    // Get all arrguments
    var arr = new Array();
    if (match) {
        for (var i = 1; i < match.length; i = i + 2) {
            arr[match[i].toLowerCase().replace(/^\(+|\)+$/g, '')] = match[i + 1];
        }
    }

    var obj = new Object();
    if (arr['behavior'] == 'hover') {
        // Create image object
        obj.img_ext = (arr['type'] ? arr['type'] : '');
        obj.img_url = (arr['url'] ? arr['url'] : '');
        obj.img_width = (arr['width'] ? arr['width'] : '');
        obj.img_height = (arr['height'] ? arr['height'] : '');
        obj.img_alt = (arr['alt'] ? arr['alt'] : '');
        obj.img_borderwidth = (arr['borderwidth'] ? parseInt(arr['borderwidth'], 10) >= 0 ? arr['borderwidth'] : '' : '');
        obj.img_bordercolor = (arr['bordercolor'] ? arr['bordercolor'] : '');
        obj.img_hspace = (arr['hspace'] ? parseInt(arr['hspace'], 10) >= 0 ? arr['hspace'] : '' : '');
        obj.img_vspace = (arr['vspace'] ? parseInt(arr['vspace'], 10) >= 0 ? arr['vspace'] : '' : '');
        obj.img_align = (arr['align'] ? arr['align'] : '');
        obj.img_id = (arr['id'] ? arr['id'] : '');
        obj.img_tooltip = (arr['tooltip'] ? arr['tooltip'] : '');
        obj.img_class = (arr['class'] ? arr['class'] : '');
        obj.img_style = (arr['style'] ? arr['style'] : '');
        obj.img_behavior = (arr['behavior'] ? arr['behavior'] : '');
        obj.img_mouseoverwidth = (arr['mouseoverwidth'] ? arr['mouseoverwidth'] : '');
        obj.img_mouseoverheight = (arr['mouseoverheight'] ? arr['mouseoverheight'] : '');
    }
    else
        if (arr['type'] == 'swf') {
        // Create flash object
        obj.flash_ext = (arr['type'] ? arr['type'] : '');
        obj.flash_url = (arr['url'] ? arr['url'] : '');
        obj.flash_width = (arr['width'] ? arr['width'] : '');
        obj.flash_height = (arr['height'] ? arr['height'] : '');
        obj.flash_autoplay = ((arr['autoplay'] != null) && (arr['autoplay'] == '1') ? true : false);
        obj.flash_loop = ((arr['loop'] != null) && (arr['loop'] == '1') ? true : false);
        obj.flash_menu = ((arr['menu'] != null) && (arr['menu'] == '1') ? true : false);
        obj.flash_scale = (arr['scale'] ? arr['scale'] : '');
        obj.flash_id = (arr['id'] ? arr['id'] : '');
        obj.flash_title = (arr['title'] ? arr['title'] : '');
        obj.flash_class = (arr['class'] ? arr['class'] : '');
        obj.flash_style = (arr['style'] ? arr['style'] : '');
    }
    else
        if (arr['type']) {
        // Create video object
        obj.av_ext = (arr['type'] ? arr['type'] : '');
        obj.av_url = (arr['url'] ? arr['url'] : '');
        obj.av_width = (arr['width'] ? arr['width'] : '');
        obj.av_height = (arr['height'] ? arr['height'] : '');
        obj.av_autoplay = ((arr['autoplay'] != null) && (arr['autoplay'] == '1') ? true : false);
        obj.av_loop = ((arr['loop'] != null) && (arr['loop'] == '1') ? true : false);
        obj.av_controls = ((arr['controls'] != null) && (arr['controls'] == '1') ? true : false);
    }
    else
        if ((arr['fileguid']) && (arr['fileguid'] != '')) {
        obj.url_guid = arr['fileguid'];
        obj.url_width = (arr['width'] ? arr['width'] : '');
        obj.url_height = (arr['height'] ? arr['height'] : '');
        obj.url_maxsidesize = (arr['maxsidesize'] ? arr['maxsidesize'] : '');
        obj.url_sitename = (arr['sitename'] ? arr['sitename'] : '');
        obj.url_url = '~/CMSPages/GetMediaFile.aspx?fileguid=' + arr['fileguid'];
        obj.url_oldformat = true;
    }

    return FCKMedia._UnescapeChars(obj);
}

FCKMedia._RefreshView = function() {
    var obj = FCKMedia.Parse(this.oInline.data);
    var width = 0;
    var height = 0;
    // Get width/height from parsed object
    if (obj.img_width) {
        width = parseInt(obj.img_width, 10);
        height = parseInt(obj.img_height, 10);
    }
    else
        if (obj.flash_width) {
        width = parseInt(obj.flash_width, 10);
        height = parseInt(obj.flash_height, 10);
    }
    else
        if (obj.av_width) {
        width = parseInt(obj.av_width, 10);
        height = parseInt(obj.av_height, 10);
    }
    else
        if (obj.url_width) {
        width = parseInt(obj.url_width, 10);
        height = parseInt(obj.url_height, 10);
    }
    else
        if (obj.url_maxsidesize) {
        width = height = parseInt(obj.url_maxsidesize, 10);
    }
    // Update width/height in fake img
    if (width > 0)
        this.oFakeImage.style.width = width + 'px';

    if (height > 0)
        this.oFakeImage.style.height = height + 'px';
}

FCKMedia._UpdateInline = function(inline, fakeImg, obj, creating) {
    var inlineTxt = '';
    if ((obj.img_url != null) && (obj.img_url != "")) {
        inlineTxt = '{^Image|(behavior)' + obj.img_behavior + '|(url)' + obj.img_url;
        if ((!creating) && (fakeImg)) {
            if (parseInt(fakeImg.style.width, 10) >= 0) {
                inlineTxt += '|(width)' + fakeImg.style.width;
            }
            if (parseInt(fakeImg.style.width, 10) >= 0) {
                inlineTxt += '|(height)' + fakeImg.style.height;
            }
        }
        else {
            if (parseInt(obj.img_width, 10) >= 0) {
                inlineTxt += '|(width)' + obj.img_width;
            }
            if (parseInt(obj.img_height, 10) >= 0) {
                inlineTxt += '|(height)' + obj.img_height;
            }
        }
        if (obj.img_alt) {
            inlineTxt += '|(alt)' + escape(obj.img_alt);
        }
        if ((obj.img_borderwidth) && (parseInt(obj.img_borderwidth, 10) >= 0)) {
            inlineTxt += '|(borderwidth)' + obj.img_borderwidth;
        }
        if (obj.img_bordercolor) {
            inlineTxt += '|(bordercolor)' + obj.img_bordercolor;
        }
        if ((obj.img_hspace) && (parseInt(obj.img_hspace, 10) >= 0)) {
            inlineTxt += '|(hspace)' + obj.img_hspace;
        }
        if ((obj.img_vspace) && (parseInt(obj.img_vspace, 10) >= 0)) {
            inlineTxt += '|(vspace)' + obj.img_vspace;
        }
        if (obj.img_align) {
            inlineTxt += '|(align)' + obj.img_align;
        }
        if (obj.img_id) {
            inlineTxt += '|(id)' + escape(obj.img_id);
        }
        if (obj.img_tooltip) {
            inlineTxt += '|(tooltip)' + escape(obj.img_tooltip);
        }
        if (obj.img_class) {
            inlineTxt += '|(class)' + escape(obj.img_class);
        }
        if (obj.img_style) {
            inlineTxt += '|(style)' + escape(obj.img_style);
        }
        if (obj.img_mouseoverwidth) {
            inlineTxt += '|(mouseoverwidth)' + obj.img_mouseoverwidth;
        }
        if (obj.img_mouseoverheight) {
            inlineTxt += '|(mouseoverheight)' + obj.img_mouseoverheight;
        }
        inlineTxt += '^}';
    }
    if ((obj.av_url != null) && (obj.av_url != "")) {
        inlineTxt = '{^Media|(type)' + obj.av_ext.replace('.', '') + '|(url)' + obj.av_url + '|(width)' + obj.av_width + '|(height)' + obj.av_height;

        if ((obj.av_autoplay != null) && (obj.av_autoplay.toLowerCase() == "true")) {
            inlineTxt += '|(autoplay)1';
        }
        if ((obj.av_loop != null) && (obj.av_loop.toLowerCase() == "true")) {
            inlineTxt += '|(loop)1';
        }
        if ((obj.av_controls != null) && (obj.av_controls.toLowerCase() == "true")) {
            inlineTxt += '|(controls)1';
        }
        inlineTxt += '^}';
    }
    if ((obj.flash_url != null) && (obj.flash_url != "")) {
        inlineTxt = '{^Media|(type)' + obj.flash_ext.replace('.', '') + '|(url)' + obj.flash_url + '|(width)' + obj.flash_width + '|(height)' + obj.flash_height;
        if ((obj.flash_autoplay != null) && (obj.flash_autoplay.toLowerCase() == "true")) {
            inlineTxt += '|(autoplay)1';
        }
        if ((obj.flash_loop != null) && (obj.flash_loop.toLowerCase() == "true")) {
            inlineTxt += '|(loop)1';
        }
        if ((obj.flash_menu != null) && (obj.flash_menu.toLowerCase() == "true")) {
            inlineTxt += '|(menu)1';
        }
        if (obj.flash_scale) {
            inlineTxt += '|(scale)' + obj.flash_scale;
        }
        if (obj.flash_id) {
            inlineTxt += '|(id)' + escape(obj.flash_id);
        }
        if (obj.flash_title) {
            inlineTxt += '|(title)' + escape(obj.flash_title);
        }
        if (obj.flash_class) {
            inlineTxt += '|(class)' + escape(obj.flash_class);
        }
        if (obj.flash_style) {
            inlineTxt += '|(style)' + escape(obj.flash_style);
        }
        inlineTxt += '^}';
    }

    inline.nodeValue = inlineTxt;
}

FCKMedia._UpdateImageStyle = function(obj, fakeImg) {
    if (obj.img_id) {
        fakeImg.id = obj.img_id;
    }
    if (obj.img_alt) {
        fakeImg.alt = obj.img_alt;
    }
    if (obj.img_tooltip) {
        fakeImg.title = obj.img_tooltip;
    }
    if (obj.img_class) {
        fakeImg.className = obj.img_class;
    }
    if (obj.img_style) {
        fakeImg.setAttribute('style', obj.img_style);
        if (fakeImg.style.cssStyle != undefined) {
            fakeImg.style.cssStyle = obj.img_style;
        }
        if (fakeImg.style.cssText != undefined) {
            fakeImg.style.cssText = obj.img_style;
        }
    }
    if ((obj.img_width) && (parseInt(obj.img_width, 10) >= 0)) {
        fakeImg.style.width = parseInt(obj.img_width, 10) + 'px';
    }
    if ((obj.img_height) && (parseInt(obj.img_height, 10) >= 0)) {
        fakeImg.style.height = parseInt(obj.img_height, 10) + 'px';
    }
    if ((obj.img_borderwidth) && (parseInt(obj.img_borderwidth, 10) >= 0)) {
        fakeImg.style.borderWidth = obj.img_borderwidth + 'px';
        fakeImg.style.borderStyle = 'solid';
    }
    if (obj.img_bordercolor) {
        try {
            fakeImg.style.borderColor = obj.img_bordercolor;
        }
        catch (e) {
            fakeImg.style.borderColor = '';
        }
    }
    if (obj.img_align) {
        if ((obj.img_align == 'left') || (obj.img_align == 'right')) {
            if (fakeImg.style.cssFloat != undefined) {
                fakeImg.style.cssFloat = obj.img_align;
            }
            if (fakeImg.style.styleFloat != undefined) {
                fakeImg.style.styleFloat = obj.img_align;
            }
        }
        else {
            if (fakeImg.style.verticalAlign != undefined) {
                fakeImg.style.verticalAlign = obj.img_align;
            }
            if (fakeImg.style['vertical-align'] != undefined) {
                fakeImg.style['vertical-align'] = obj.img_align;
            }
        }
    }

    if ((obj.img_vspace) || (obj.img_hspace)) {
        var vspace = parseInt(obj.img_vspace, 10);
        var hspace = parseInt(obj.img_hspace, 10);
        if ((vspace == hspace) && (vspace >= 0)) {
            fakeImg.style.margin = hspace + 'px';
        }
        else
            if ((vspace == hspace) && (vspace == -1)) {
            // No margin will be inserted
        }
        else {
            fakeImg.style.margin = (vspace >= 0 ? vspace + 'px ' : 'auto ') + (hspace >= 0 ? hspace + 'px' : 'auto');
        }
    }
}

FCKMedia._BuildNodes = function(node, array) {
    for (var i = 0; i < node.childNodes.length; i++) {
        var nodeName = null;
        try {
            nodeName = node.childNodes[i].nodeName;
        }
        catch (e) {
        }

        if (nodeName != null) {
            nodeName = nodeName.toLowerCase();
        }
        if (nodeName == '#text') {
            array.push(node.childNodes[i]);
        }
        else {
            var flash = null;
            // In Firefox is flash as embed object
            if ((nodeName == 'embed') &&
            (node.childNodes[i].getAttribute('type') != null) &&
            (node.childNodes[i].getAttribute('type').toLowerCase() === 'application/x-shockwave-flash')) {
                flash = FCKMedia._BuildFlashInline(node.childNodes[i]);
            }
            else
                if ((nodeName == 'object') &&
                (node.childNodes[i].getAttribute('classid') != null) &&
                (node.childNodes[i].getAttribute('classid').toLowerCase() === 'clsid:d27cdb6e-ae6d-11cf-96b8-444553540000')) {
                flash = FCKMedia._BuildFlashInline(node.childNodes[i]);
            }
            // In IE is flash as fake image
            else
                if (nodeName == 'img') {
                var origElem = FCK.GetRealElement(node.childNodes[i]);
                if ((origElem != null) && (origElem.getAttribute != undefined) &&
                        (((origElem.getAttribute('type') != null) && (origElem.getAttribute('type').toLowerCase() === 'application/x-shockwave-flash')) ||
                        ((origElem.getAttribute('classid') != null) && (origElem.getAttribute('classid').toLowerCase() === 'clsid:d27cdb6e-ae6d-11cf-96b8-444553540000')))) {
                    flash = FCKMedia._BuildFlashInline(origElem);
                }
            }
            if (flash != null) {
                // Create inline text node
                node.insertBefore(FCK.EditorDocument.createTextNode(flash), node.childNodes[i]);
                // Remove embed node
                node.removeChild(node.childNodes[i + 1]);
                array.push(node.childNodes[i]);
            }
            else {
                if (nodeName == 'img') {
                    // Add the append part
                    var append = FCKConfig.AppendToImagePath;
                    if ((append != null) && (append != "")) {
                        var url = node.childNodes[i].getAttribute('src');
                        if (FCKMedia._SizeToURL(url)) {
                            var re = new RegExp('[&?]' + append, 'i');
                            if (!re.test(url)) {
                                if (/\?/.test(url)) {
                                    url += '&' + append;
                                }
                                else {
                                    url += '?' + append;
                                }
                            }

                            node.childNodes[i].setAttribute('src', url);
                        }
                    }
                }
            }

        }
        if (node.childNodes[i].childNodes.length > 0) {
            FCKMedia._BuildNodes(node.childNodes[i], array);
        }
    }
}

FCKMedia._BuildFlashInline = function(node) {
    if (node.nodeName.toLowerCase() === 'object') {
        for (var i = 0; i < node.childNodes.length; i++) {
            if (node.childNodes[i].nodeName.toLowerCase() === 'embed') {
                return (FCKMedia._BuildFlashInline(node.childNodes[i]));
            }
        }
    }

    var width = node.getAttribute('width');
    var height = node.getAttribute('height');
    var menu = node.getAttribute('menu');
    var loop = node.getAttribute('loop');
    var play = node.getAttribute('play');
    var src = node.getAttribute('src');
    var data = node.getAttribute('data');
    var id = node.getAttribute('id');
    var scale = node.getAttribute('scale');
    var title = node.getAttribute('title');
    var cssClass = node.getAttribute('class');
    if (cssClass == null) {
        cssClass = node.className;
    }
    var style = null;
    if (node.style.cssStyle != undefined) {
        style = node.style.cssStyle;
    }
    if (node.style.cssText != undefined) {
        style = node.style.cssText;
    }

    var out = '{^Media|(type)swf';
    if (src) {
        out += '|(url)' + src;
    }
    else
        if (data) {
        out += '|(url)' + data;
    }
    if (width)
        out += '|(width)' + width;
    if (height)
        out += '|(height)' + height;
    if (play)
        if (play.toLowerCase() == "true") {
        out += '|(autoplay)1';
    }
    else {
        out += '|(autoplay)0';
    }
    if (menu)
        if (menu.toLowerCase() == "true") {
        out += '|(menu)1';
    }
    else {
        out += '|(menu)0';
    }
    if (loop)
        if (loop.toLowerCase() == "true") {
        out += '|(loop)1';
    }
    else {
        out += '|(loop)0';
    }
    if (id)
        out += '|(id)' + id;
    if (scale)
        out += '|(scale)' + scale;
    if (title)
        out += '|(title)' + title;
    if (cssClass)
        out += '|(class)' + cssClass;
    if ((style) && (style != ';'))
        out += '|(style)' + style;
    out += '^}';

    return out;
}

FCKMedia._CreateNodes = function(node, origNode, val) {
    var index = val.search(/{\^(?:Media|Image)(?:[^}]*)}/i);
    if (index != -1) {
        var leftCont = RegExp.leftContext;
        var searchCont = RegExp.lastMatch;
        var rightCont = RegExp.rightContext;
        if (leftCont != '') {
            // Create left textnode if some
            node.insertBefore(FCK.EditorDocument.createTextNode(leftCont), origNode);
        }
        if (searchCont != '') {
            // Create inline text node
            node.insertBefore(FCK.EditorDocument.createTextNode(searchCont), origNode);
        }
        if (rightCont != '') {
            // Recursive _CreateNodes id right content exist
            FCKMedia._CreateNodes(node, origNode, rightCont);
        }
    }
    else {
        // If not find then create textnode from val
        node.insertBefore(FCK.EditorDocument.createTextNode(val), origNode);
    }
}

FCKMedia._SetClass = function(obj) {
    if (!obj) {
        obj = FCKMedia.Parse(this.oInline.data);
    }

    if (obj.img_url) {
        this.oFakeImage.className = 'FCK__Media';
        // Resolve URL
        var url = obj.img_url.replace(/^~\//, FCKConfig["ApplicationPath"]);
        // Update URL of fake image
        this.oFakeImage.src = url;
        FCKMedia._UpdateImageStyle(obj, this.oFakeImage);
    }
    else
        if (obj.flash_url) {
        this.oFakeImage.className = 'FCK__Media FCK__Flash_Object';
    }
    else
        if (obj.av_url) {
        this.oFakeImage.className = 'FCK__Media FCK__AudioVideo_Object';
    }
    else
        if (obj.url_url) {
        this.oFakeImage.className = 'FCK__Media FCK__OldMedia_Object';
    }
}

FCKMedia._UpdateLink = function(obj) {
    var oLink = FCKMedia._GetLink(this.oFakeImage);
    if ((oLink == null) && (obj.img_link != null) && (obj.img_link != '')) {
        FCK.Selection.SelectNode(this.oFakeImage);
        oLink = FCK.CreateLink(obj.img_link, true);
        if (oLink.length > 0) {
            oLink = oLink[0];
        }
        else {
            oLink = null;
        }
    }
    if (oLink != null) {
        if ((obj.img_link != null) && (obj.img_link != '')) {
            oLink.href = obj.img_link;
            oLink.setAttribute('_fcksavedurl', obj.img_link);
            if ((obj.img_target != null) && (obj.img_target != '')) {
                oLink.setAttribute('target', obj.img_target);
            }
            else
                if ((obj.img_behavior != null) && (obj.img_behavior != '') && (obj.img_behavior != 'hover')) {
                oLink.setAttribute('target', obj.img_behavior);
            }
            else {
                oLink.removeAttribute('target');
            }
        }
        else {
            FCK.ExecuteNamedCommand('Unlink');
        }
    }
}

FCKMedia._HasLink = function(node) {
    if (node.nodeName.toLowerCase() == 'body')
        return false;
    if (node.nodeName.toLowerCase() == 'a')
        return true;
    if (node.parentNode == null)
        return false;
    return FCKMedia._HasLink(node.parentNode);
}

FCKMedia._GetLink = function(node) {
    if (node.nodeName.toLowerCase() == 'body')
        return null;
    if (node.nodeName.toLowerCase() == 'a')
        return node;
    if (node.parentNode == null)
        return null;
    return FCKMedia._GetLink(node.parentNode);
}

FCKMedia._UnescapeChars = function(obj) {
    for (var i in obj) {
        obj[i] = unescape(obj[i]);
    }
    return obj;
}

FCKMedia._IsImage = function(ext) {
    ext = ext.replace('.', '');
    switch (ext) {
        case 'jpg':
        case 'jpeg':
        case 'gif':
        case 'png':
        case 'tif':
        case 'tiff':
            return true;
    }
    return false;
}

FCKMedia._SizeToURL = function(url) {
    if ((url != null) && (url != '')) {
        var resizeURL = new RegExp('\/getmedia\/|\/getmediafile.aspx?|\/getfile\/|\/getfile.aspx?|\/getdoc\/|\/getattachment\/', 'i').test(url);
        if (resizeURL) {
            return true;
        }
        var hasQueryString = /\?/.test(url);
        if (hasQueryString) {
            return true;
        }
        var lastDot = url.lastIndexOf('.');
        var isImage = true;
        if (lastDot > 0) {
            var ext = url.substring(lastDot);
            isImage = FCKMedia._IsImage(ext);
        }
        if (!isImage) {
            return true;
        }
    }
    return false;
}

var FCKMediaObjectProcessor = FCKDocumentProcessor.AppendNew();
FCKMediaObjectProcessor.ProcessDocument = function(document) {
    var arr = new Array();
    FCKMedia._BuildNodes(document.body, arr);

    // Create all inline text nodes
    for (var i = 0; i < arr.length; i++) {
        if (/{\^(?:Media|Image)(?:[^}]*)}/i.test(arr[i].data)) {
            // text node value
            var nodeValue = arr[i].data;
            // text node inline
            var node = arr[i].parentNode;
            FCKMedia._CreateNodes(node, arr[i], nodeValue);
            // remove original text node
            node.removeChild(arr[i]);
        }
    }

    // Clear text nodes array
    arr = new Array();

    // Get all new text nodes
    FCKMedia._BuildNodes(document.body, arr);
    for (var i = 0; i < arr.length; i++) {
        if (/{\^(?:Media|Image)(?:[^}]*)}/i.test(arr[i].data)) {

            var obj = FCKMedia.Parse(arr[i].data);
            FCKMedia.oInline = arr[i].cloneNode(false);
            FCKMedia.oFakeImage = FCKDocumentProcessor_CreateFakeImage('FCK__Media', FCKMedia.oInline);

            FCKMedia.oFakeImage.setAttribute('_fckmedia', 'true', 0);

            FCKMedia._SetClass();
            FCKMedia._RefreshView();

            arr[i].parentNode.insertBefore(FCKMedia.oFakeImage, arr[i]);
            arr[i].parentNode.removeChild(arr[i]);
        }
    }
}

FCKMediaObjectProcessor.RefreshInline = function(fakeImage, originalInline) {
    if ((fakeImage.style.width != '') && (fakeImage.style.height != '')) {
        var inlineData = originalInline.data.toString();
        var width = parseInt(fakeImage.style.width, 10);
        var height = parseInt(fakeImage.style.height, 10);
        // If no width in style try img width
        if (width.toString() == "NaN") {
            width = parseInt(fakeImage.width.toString(), 10);
        }
        // If no height in style try img height
        if (height.toString() == "NaN") {
            height = parseInt(fakeImage.height.toString(), 10);
        }
        var isWidth = /\|\(width\)[^\|\^]*/i.test(inlineData);
        var isHeight = /\|\(height\)[^\|\^]*/i.test(inlineData);

        if (isWidth) {
            // If width in inline control update it
            inlineData = inlineData.replace(/\|\(width\)[^\|\^]*/i, '|(width)' + width);
        }
        else {
            // Else create width prameter in inline control
            inlineData = inlineData.replace(/\^}/i, '') + '|(width)' + width + '^}';
        }
        if (isHeight) {
            // If height in inline control update it
            inlineData = inlineData.replace(/\|\(height\)[^\|\^]*/i, '|(height)' + height);
        }
        else {
            // Else create height prameter in inline control
            inlineData = inlineData.replace(/\^}/i, '') + '|(height)' + height + '^}';
        }
        // Update url for Image inline control
        if (/{\^Image\|/i.test(inlineData)) {
            var url = inlineData.match(/\(url\)[^\|]+/i);
            if (url[0] != null) {
                url = url[0];
            }
            var sizeToUrl = FCKMedia._SizeToURL(url);
            if (sizeToUrl) {
                var widthInUrl = /width=\d+/i.test(url);
                var heightInUrl = /height=\d+/i.test(url);
                var queryInUrl = /\?/i.test(url);
                var urlUpdated = false;

                if (widthInUrl) {
                    inlineData = inlineData.replace(/width=\d+/i, 'width=' + width);
                }
                else {
                    if (queryInUrl) {
                        url += '&width=' + width;
                    }
                    else {
                        url += '?width=' + width;
                        queryInUrl = true;
                    }
                    urlUpdated = true;
                }
                if (heightInUrl) {
                    inlineData = inlineData.replace(/height=\d+/i, 'height=' + height);
                }
                else {
                    if (queryInUrl) {
                        url += '&height=' + height;
                    }
                    else {
                        url += '?height=' + height;
                        queryInUrl = true;
                    }
                    urlUpdated = true;
                }
                if (urlUpdated) {
                    inlineData = inlineData.replace(/\(url\)[^\|]+/i, url);
                }
            }
        }
        originalInline.data = inlineData;
    }
}

FCK.GetRealElement = function(fakeElement) {
    var e = FCKTempBin.Elements[fakeElement.getAttribute('_fckrealelement')];

    if (fakeElement.getAttribute('_fckflash')) {
        if (fakeElement.style.width.length > 0)
            e.width = FCKTools.ConvertStyleSizeToHtml(fakeElement.style.width);

        if (fakeElement.style.height.length > 0)
            e.height = FCKTools.ConvertStyleSizeToHtml(fakeElement.style.height);
    }
    else
        if (fakeElement.getAttribute('_fckmedia') && FCKMediaObjectProcessor) {
        FCKMediaObjectProcessor.RefreshInline(fakeElement, e);
    }
    else
        if (fakeElement.getAttribute('_fckyoutube') && FCKYouTubeObjectProcessor) {
        FCKYouTubeObjectProcessor.RefreshInline(fakeElement, e);
    }

    return e;
}

FCKXHtml.TagProcessors['img'] = function(node, htmlNode) {
    if (!node.attributes.getNamedItem('alt')) {
        FCKXHtml._AppendAttribute(node, 'alt', '');
    }

    var url = htmlNode.getAttribute('_fcksavedurl');
    if (url == null) {
        url = htmlNode.getAttribute('src');
    }

    var oWidth = htmlNode.getAttribute('_fcksavedwidth');
    var oHeight = htmlNode.getAttribute('_fcksavedheight');
    var width = parseInt(htmlNode.style.width, 10);
    var height = parseInt(htmlNode.style.height, 10);

    // If no width in style try img width
    if (width.toString() == "NaN") {
        width = parseInt(htmlNode.width.toString(), 10);
    }

    // If no height in style try img height
    if (height.toString() == "NaN") {
        height = parseInt(htmlNode.height.toString(), 10);
    }

    if ((width.toString() != "NaN") && (height.toString() != "NaN") && (FCKMedia._SizeToURL(url))) {
        if ((width != oWidth) || (height != oHeight)) {
            if ((/width=/i.test(url)) || (/height=/i.test(url))) {
                url = url.replace(/width=[\d]+/i, 'width=' + width);
                url = url.replace(/height=[\d]+/i, 'height=' + height);
            }
            else {
                if (/\?/.test(url)) {
                    url += '&width=' + width;
                }
                else {
                    url += '?width=' + width;
                }
                url += '&height=' + height;
            }
        }
    }

    // Remove the append part
    var append = FCKConfig.AppendToImagePath;
    if ((append != null) && (append != "")) {
        var re = new RegExp('[&?]' + append, 'i');
        url = url.replace(re, '');
    }

    FCKXHtml._AppendAttribute(node, 'src', url);

    if (htmlNode.style.width) {
        node.removeAttribute('width');
    }
    if (htmlNode.style.height) {
        node.removeAttribute('height');
    }

    return node;
}


FCK.ProtectUrls = function(html) {
    // <A> href
    html = html.replace(FCKRegexLib.ProtectUrlsA, '$& _fcksavedurl=$1');

    // <IMG> src
    html = html.replace(FCKRegexLib.ProtectUrlsImg, '$& _fcksavedurl=$1');

    // <AREA> href
    html = html.replace(FCKRegexLib.ProtectUrlsArea, '$& _fcksavedurl=$1');

    // <IMG> size
    html = html.replace(/<img(?=\s).*?\sstyle=(?:["|'])(?:[^"']*)width\s*:\s*([\d]*)(?:[^"']*)(?:["|'])/gi, '$& _fcksavedwidth=$1');
    html = html.replace(/<img(?=\s).*?\sstyle=(?:["|'])(?:[^"']*)height\s*:\s*([\d]*)(?:[^"']*)(?:["|'])/gi, '$& _fcksavedheight=$1');

    return html;
}

