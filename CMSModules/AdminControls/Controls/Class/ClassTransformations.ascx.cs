using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_AdminControls_Controls_Class_ClassTransformations : CMSUserControl
{
    #region "Private fields"

    private int mClassID = 0;
    private string mEditPageUrl = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the class id
    /// </summary>
    public int ClassID
    {
        get
        {
            return this.mClassID;
        }
        set
        {
            this.mClassID = value;
        }
    }


    /// <summary>
    /// URL of the page holding the editing tasks
    /// </summary>
    public string EditPageUrl
    {
        get
        {
            return this.mEditPageUrl;
        }
        set
        {
            this.mEditPageUrl = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!SettingsKeyProvider.UsingVirtualPathProvider)
        {
            lblError.Text = ResHelper.GetString("Transformations.VirtualPathProviderNotRunning") + "<br /><br />";
            lblError.Visible = true;
        }

        // Initialize the controls
        SetupControl();
    }


    #region "GridView handling"

    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName.ToLower() == "edit")
        {
            string actionArg = ValidationHelper.GetString(actionArgument, "");
            if (actionArg != "")
            {
                UrlHelper.Redirect(this.EditPageUrl + "?transformationid=" + actionArg + "&classid=" + this.ClassID.ToString());
            }
        }
        else if (actionName.ToLower() == "delete")
        {
            int transformationId = ValidationHelper.GetInteger(actionArgument, -1);
            if (transformationId > 0)
            {
                TransformationInfoProvider.DeleteTransformation(transformationId);
            }
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes the controls on the page
    /// </summary>
    private void SetupControl()
    {
        this.uniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        this.uniGrid.GridName = "~/CMSModules/AdminControls/Controls/Class/ClassTransformations.xml";
        this.uniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        // If the ClassID was specified
        if (this.ClassID > 0)
        {
            this.uniGrid.WhereCondition = "CMS_Class.ClassID=" + this.ClassID;
        }
        else
        {
            // Otherwise hide the UniGrid to avoid unexpected behaviour
            this.uniGrid.Visible = false;
        }
    }

    #endregion
}
