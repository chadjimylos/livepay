using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.ExtendedControls.Dialogs;

public partial class CMSSiteManager_Development_FormControls_FormUserControl_Edit : SiteManagerPage
{
    protected int controlid = 0;
    string[,] pageTitleTabs = new string[2, 3];

    protected void Page_Load(object sender, EventArgs e)
    {
        rfvCodeName.ErrorMessage = ResHelper.GetString("Development_FormUserControl_Edit.rfvCodeName");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("Development_FormUserControl_Edit.rfvDisplayName");

        controlid = QueryHelper.GetInteger("controlid", 0);

        if (!RequestHelper.IsPostBack())
        {
            LoadData();
        }

        drpDataType_SelectedIndexChanged(sender, e);
        chkForBizForms_CheckedChanged(sender, e);

        if (QueryHelper.GetString("saved", String.Empty) != String.Empty)
        {
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }

        string controls = ResHelper.GetString("Development_FormUserControl_Edit.Controls");
        string title = ResHelper.GetString("Development_FormUserControl_Edit.Title");
        string image = GetImageUrl("Objects/CMS_FormControl/new.png");

        string currentControl = ResHelper.GetString("Development_FormUserControl_Edit.New");
        if (controlid != 0)
        {
            FormUserControlInfo fi = FormUserControlInfoProvider.GetFormUserControlInfo(controlid);
            currentControl = fi.UserControlDisplayName;
            title = ResHelper.GetString("Development_FormUserControl_Edit.Edit");
            image = GetImageUrl("Objects/CMS_FormControl/object.png");
        }

        pageTitleTabs[0, 0] = controls;
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/FormControls/FormUserControl_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentControl;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = title;
        this.CurrentMaster.Title.TitleImage = image;
        this.CurrentMaster.Title.HelpTopicName = "newedit_form_control";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        FileSystemDialogConfiguration config = new FileSystemDialogConfiguration();
        config.DefaultPath = "CMSFormControls";
        config.AllowedExtensions = "ascx";
        config.ShowFolders = false;
        tbFileName.DialogConfig = config;
        tbFileName.AllowEmptyValue = false;
        tbFileName.SelectedPathPrefix = "~/CMSFormControls/";
        tbFileName.ValidationError = ResHelper.GetString("Development_FormUserControl_Edit.rfvFileName");
    }


    /// <summary>
    /// Loads data of edited form user control
    /// </summary>
    protected void LoadData()
    {
        FormUserControlInfo fi = FormUserControlInfoProvider.GetFormUserControlInfo(controlid);
        if (fi != null)
        {
            tbCodeName.Text = fi.UserControlCodeName;
            tbDisplayName.Text = fi.UserControlDisplayName;
            tbFileName.Value = fi.UserControlFileName;
            chkForBizForms.Checked = fi.UserControlShowInBizForms;
            chkForBoolean.Checked = fi.UserControlForBoolean;
            chkForDateTime.Checked = fi.UserControlForDateTime;
            chkForDecimal.Checked = fi.UserControlForDecimal;
            chkForFile.Checked = fi.UserControlForFile;
            chkForInteger.Checked = fi.UserControlForInteger;
            chkForLongText.Checked = fi.UserControlForLongText;
            chkForText.Checked = fi.UserControlForText;
            chkForGuid.Checked = fi.UserControlForGUID;
            chkForVisibility.Checked = fi.UserControlForVisibility;

            chkShowInDocumentTypes.Checked = fi.UserControlShowInDocumentTypes;
            chkShowInSystemTables.Checked = fi.UserControlShowInSystemTables;
            chkShowInControls.Checked = fi.UserControlShowInWebParts;
            chkShowInReports.Checked = fi.UserControlShowInReports;
            chkShowInCustomTables.Checked = fi.UserControlShowInCustomTables;

            tbColumnSize.Text = fi.UserControlDefaultDataTypeSize.ToString();
            drpDataType.SelectedIndex = drpDataType.Items.IndexOf(drpDataType.Items.FindByText(fi.UserControlDefaultDataType));
        }
    }


    /// <summary>
    /// Handles btnOK's OnClick event - Update FormUserControl info
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // finds whether required fields are not empty
        string result = new Validator().NotEmpty(tbCodeName.Text, rfvCodeName.ErrorMessage).NotEmpty(tbDisplayName, rfvDisplayName.ErrorMessage).NotEmpty(tbFileName.Value, tbFileName.ValidationError)
            .IsCodeName(tbCodeName.Text, ResHelper.GetString("general.invalidcodename"))
            .Result;

        if (result == "")
        {
            // Get object
            FormUserControlInfo fi = FormUserControlInfoProvider.GetFormUserControlInfo(controlid);
            if (fi == null)
            {
                fi = new FormUserControlInfo();
            }

            fi.UserControlCodeName = tbCodeName.Text;
            fi.UserControlDefaultDataType = drpDataType.SelectedValue;
            if (tbColumnSize.Visible && chkForBizForms.Checked)
            {
                try
                {
                    if (Convert.ToInt32(tbColumnSize.Text) <= 0)
                    {
                        lblErrorSize.Visible = true;
                        lblInfo.Visible = false;
                        return;
                    }
                    else
                    {
                        fi.UserControlDefaultDataTypeSize = Convert.ToInt32(tbColumnSize.Text);
                        lblErrorSize.Visible = false;
                    }
                }
                catch
                {
                    lblError.Visible = true;
                    lblInfo.Visible = false;
                    lblError.Text = ResHelper.GetString("Development_FormUserControl_Edit.NotInteger").Replace("%%value%%", tbColumnSize.Text);
                    return;
                }
            }

            fi.UserControlDisplayName = tbDisplayName.Text;
            fi.UserControlFileName = tbFileName.Value.ToString();

            fi.UserControlForBoolean = chkForBoolean.Checked;
            fi.UserControlForDateTime = chkForDateTime.Checked;
            fi.UserControlForDecimal = chkForDecimal.Checked;
            fi.UserControlForFile = chkForFile.Checked;
            fi.UserControlForInteger = chkForInteger.Checked;
            fi.UserControlForLongText = chkForLongText.Checked;
            fi.UserControlForText = chkForText.Checked;
            fi.UserControlShowInBizForms = chkForBizForms.Checked;
            fi.UserControlForGUID = chkForGuid.Checked;
            fi.UserControlForVisibility = chkForVisibility.Checked;

            fi.UserControlShowInDocumentTypes = chkShowInDocumentTypes.Checked;
            fi.UserControlShowInSystemTables = chkShowInSystemTables.Checked;
            fi.UserControlShowInWebParts = chkShowInControls.Checked;
            fi.UserControlShowInReports = chkShowInReports.Checked;
            fi.UserControlShowInCustomTables = chkShowInCustomTables.Checked;

            try
            {
                FormUserControlInfoProvider.SetFormUserControlInfo(fi);

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                pageTitleTabs[1, 0] = fi.UserControlDisplayName;
                if (controlid == 0)
                {
                    UrlHelper.Redirect("FormUserControl_Edit.aspx?controlid=" + fi.UserControlID + "&saved=1");
                }
            }
            catch (Exception ex)
            {
                lblInfo.Visible = false;
                lblError.Visible = true;
                lblError.Text = ex.Message.Replace("%%name%%", fi.UserControlCodeName);
                //lblError.Text = ResHelper.GetString("Development_FormUserControl_Edit.CodeNameAlreadyExist").Replace("%%name%%", fi.UserControlCodeName);
            }
        }
        else
        {
            lblInfo.Visible = false;
            lblError.Visible = true;
            lblError.Text = result;
        }
    }


    protected void drpDataType_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblErrorSize.Visible = false;
        lblInfo.Visible = false;
        if (drpDataType.SelectedValue == "Text")
        {
            lblColumnSize.Visible = true;
            tbColumnSize.Visible = true;
        }
        else
        {
            lblColumnSize.Visible = false;
            tbColumnSize.Visible = false;
            tbColumnSize.Text = string.Empty;
        }
    }


    protected void chkForBizForms_CheckedChanged(object sender, EventArgs e)
    {
        lblErrorSize.Visible = false;
        lblInfo.Visible = false;
        if (chkForBizForms.Checked)
        {
            drpDataType.Enabled = true;
            lblColumnSize.Enabled = true;
            tbColumnSize.Enabled = true;
        }
        else
        {
            tbColumnSize.Enabled = false;
            drpDataType.Enabled = false;
        }
    }
}
