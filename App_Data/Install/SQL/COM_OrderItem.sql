CREATE TABLE [COM_OrderItem] (
		[OrderItemID]               int NOT NULL IDENTITY(1, 1),
		[OrderItemOrderID]          int NOT NULL,
		[OrderItemSKUID]            int NOT NULL,
		[OrderItemSKUName]          nvarchar(450) NOT NULL,
		[OrderItemUnitPrice]        float NOT NULL,
		[OrderItemUnitCount]        int NOT NULL,
		[OrderItemCustomData]       nvarchar(max) NULL,
		[OrderItemGuid]             uniqueidentifier NOT NULL,
		[OrderItemParentGuid]       uniqueidentifier NULL,
		[OrderItemLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_OrderItem]
	ADD
	CONSTRAINT [PK_COM_OrderItem]
	PRIMARY KEY
	([OrderItemID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_OrderItem]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_OrderItem_OrderItemOrderID_COM_Order]') IS NULL
BEGIN
		ALTER TABLE [COM_OrderItem]
			ADD CONSTRAINT [FK_COM_OrderItem_OrderItemOrderID_COM_Order]
			FOREIGN KEY ([OrderItemOrderID]) REFERENCES [COM_Order] ([OrderID])
END
IF OBJECT_ID(N'[COM_OrderItem]') IS NOT NULL
	AND OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_OrderItem_OrderItemSKUID_COM_SKU]') IS NULL
BEGIN
		ALTER TABLE [COM_OrderItem]
			ADD CONSTRAINT [FK_COM_OrderItem_OrderItemSKUID_COM_SKU]
			FOREIGN KEY ([OrderItemSKUID]) REFERENCES [COM_SKU] ([SKUID])
END
CREATE INDEX [IX_COM_OrderItem_OrderItemOrderID]
	ON [COM_OrderItem] ([OrderItemOrderID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_OrderItem_OrderItemSKUID]
	ON [COM_OrderItem] ([OrderItemSKUID])
	ON [PRIMARY]
