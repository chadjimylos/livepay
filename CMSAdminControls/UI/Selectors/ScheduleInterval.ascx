<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ScheduleInterval.ascx.cs"
    Inherits="CMSAdminControls_UI_Selectors_ScheduleInterval" %>

<asp:Panel runat="server" ID="pnlBody" CssClass="EditingFormControl">
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" />
    <%--Period--%>
    <table cellpadding="0" cellspacing="3">
        <asp:PlaceHolder runat="server" ID="pnlPeriod">
            <tr>
                <td>
                    <asp:Label ID="lblPeriod" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:DropDownList ID="drpPeriod" runat="server" OnSelectedIndexChanged="DrpPeriod_OnSelectedIndexChanged"
                        AutoPostBack="true">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblStart" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <cms:DateTimePicker ID="dateTimePicker" runat="server" SupportFolder="~/CMSAdminControls/Calendar"
                        EditTime="true" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
        </asp:PlaceHolder>
        <%--Every--%>
        <asp:PlaceHolder runat="server" ID="pnlEvery">
            <tr>
                <td>
                    <asp:Label ID="lblEvery" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtEvery" runat="server" Style="width: 40px" MaxLength="5" />&nbsp;
                    <asp:Label ID="lblEveryPeriod" runat="server" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:RequiredFieldValidator ID="rfvEvery" runat="server" ControlToValidate="txtEvery"
                        Display="dynamic" />
                    <asp:RangeValidator ID="rvEvery" runat="server" ControlToValidate="txtEvery" Type="integer"
                        Display="dynamic" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <%--Between--%>
        <asp:PlaceHolder runat="server" ID="pnlBetween">
            <tr>
                <td>
                    <asp:Label ID="lblBetween" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtFromHours" runat="server" Style="width: 20px" MaxLength="2" />
                    :
                    <asp:TextBox ID="txtFromMinutes" runat="server" Style="width: 20px" MaxLength="2" />
                    &nbsp;<asp:Label ID="lblAnd" runat="server" EnableViewState="false" />&nbsp;
                    <asp:TextBox ID="txtToHours" runat="server" Style="width: 20px" MaxLength="2" />
                    :
                    <asp:TextBox ID="txtToMinutes" runat="server" Style="width: 20px" MaxLength="2" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:RequiredFieldValidator ID="rfvFromHours" runat="server" ControlToValidate="txtFromHours"
                        Display="dynamic" />
                    <asp:RangeValidator ID="rvFromHours" runat="server" ControlToValidate="txtFromHours"
                        Type="integer" Display="dynamic" />
                    <asp:RequiredFieldValidator ID="rfvFromMinutes" runat="server" ControlToValidate="txtFromMinutes"
                        Display="dynamic" />
                    <asp:RangeValidator ID="rvFromMinutes" runat="server" ControlToValidate="txtFromMinutes"
                        Type="integer" Display="dynamic" />
                    <asp:RequiredFieldValidator ID="rfvToHours" runat="server" ControlToValidate="txtToHours"
                        Display="dynamic" />
                    <asp:RangeValidator ID="rvToHours" runat="server" ControlToValidate="txtToHours"
                        Type="integer" Display="dynamic" />
                    <asp:RequiredFieldValidator ID="rfvToMinutes" runat="server" ControlToValidate="txtToMinutes"
                        Display="dynamic" />
                    <asp:RangeValidator ID="rvToMinutes" runat="server" ControlToValidate="txtToMinutes"
                        Type="integer" Display="dynamic" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <%--Days--%>
        <asp:PlaceHolder runat="server" ID="pnlDays">
            <tr>
                <td style="vertical-align: top;">
                    <asp:Label ID="lblDays" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <table cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:CheckBoxList ID="chkWeek" runat="server" />
                            </td>
                            <td style="vertical-align: text-top">
                                <asp:CheckBoxList ID="chkWeekEnd" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </asp:PlaceHolder>
        <%--Month--%>
        <asp:PlaceHolder runat="server" ID="pnlMonth" Visible="false">
            <tr>
                <td>
                    <asp:RadioButton ID="radMonth1" runat="server" AutoPostBack="true" OnCheckedChanged="radMonth1_CheckedChanged"
                        Checked="True" />
                </td>
                <td>
                    <asp:DropDownList ID="drpMonth1" runat="server" />&nbsp;
                    <asp:Label ID="lblMonth1" runat="server" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:RadioButton ID="radMonth2" runat="server" AutoPostBack="true" OnCheckedChanged="radMonth2_CheckedChanged" />
                </td>
                <td>
                    <asp:DropDownList ID="drpMonth2" runat="server" />
                    &nbsp;
                    <asp:DropDownList ID="drpMonth3" runat="server" />&nbsp;
                    <asp:Label ID="lblMonth2" runat="server" EnableViewState="false" />
                </td>
            </tr>
        </asp:PlaceHolder>
    </table>
</asp:Panel>
