using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.DataEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_Permissions_Permissions_Matrix : CMSPermissionsPage
{
    #region "Protected variables"

    protected int id = 0;
    protected string type = string.Empty;

    #endregion


    #region "Page events"

    protected override void OnPreInit(EventArgs e)
    {
        ((Panel)this.CurrentMaster.PanelBody.FindControl("pnlContent")).CssClass = "";
        base.OnPreInit(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get the settings
        id = QueryHelper.GetInteger("id", 0);
        if (id > 0)
        {
            type = QueryHelper.GetString("type", "");
        }

        int siteId = GetSiteID(QueryHelper.GetString("siteid", null));

        // Retrive permission matrix data
        object[,] parameters = new object[4, 3];
        parameters[0, 0] = "@ID";
        parameters[0, 1] = id;
        parameters[1, 0] = "@SiteID";
        parameters[1, 1] = siteId;
        parameters[2, 0] = "@DisplayInMatrix";
        parameters[2, 1] = true;

        if (type == "r")
        {
            gridMatrix.QueryName = "cms.permission.getResourcePermissionMatrix";
        }
        else
        {
            gridMatrix.QueryName = "cms.permission.getClassPermissionMatrix";
        }

        gridMatrix.OrderBy = "Matrix.RoleDisplayName, Matrix.RoleID, Matrix.PermissionDisplayName";
        gridMatrix.QueryParameters = parameters;
        gridMatrix.WhereCondition = "RoleGroupID IS NULL";
        gridMatrix.ContentBefore = "<table class=\"PermissionMatrix\" cellspacing=\"0\" cellpadding=\"0\" rules=\"rows\" border=\"1\" style=\"border-collapse:collapse;\">";
        gridMatrix.OnItemChanged += new UniMatrix.ItemChangedEventHandler(gridMatrix_OnItemChanged);

        // Check "Manage" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Permissions", "Manage"))
        {
            gridMatrix.Enabled = false;
            lblInfo.Text = String.Format(ResHelper.GetString("CMSSiteManager.AccessDeniedOnPermissionName"), "Manage");
        }
    }


    void gridMatrix_OnItemChanged(object sender, int roleId, int permissionId, bool allow)
    {
        // Check "Manage" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Permissions", "Manage"))
        {
            throw new Exception("Not authorized");
        }

        if (allow)
        {
            RolePermissionInfoProvider.SetRolePermissionInfo(roleId, permissionId);
        }
        else
        {
            RolePermissionInfoProvider.DeleteRolePermissionInfo(roleId, permissionId);
        }

        // Invalidate all users
        //UserInfo.TYPEINFO.InvalidateAllObjects();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!gridMatrix.HasData)
        {
            lblInfo.Text = ResHelper.GetString("Administration-Permissions_Matrix.EmptyMatrix");
        }
        else if (String.IsNullOrEmpty(lblInfo.Text))
        {
            lblInfo.Visible = false;
        }
    }

    #endregion
}
