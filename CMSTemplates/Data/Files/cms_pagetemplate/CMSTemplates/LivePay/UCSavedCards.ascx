﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSavedCards.ascx.vb" Inherits="CMSTemplates_LivePay_UCSavedCards" %>
<asp:UpdatePanel ID="uptpanel" runat="server" UpdateMode="Always" >
<ContentTemplate>


 <div class="SvdCardsDarkBlueBGTitle">
    <div class="SvdCardsTopTitle"><%=ResHelper.LocalizeString("{$=Αποθηκευμένες Κάρτες|en-us=Αποθηκευμένες Κάρτες$}") %></div>
    <div class="SvdCardsContentBG">
        <div class="PL15 SvdCardsTopDescr"><%=ResHelper.LocalizeString("{$=Στη σελίδα αυτή μπορείτε να διαχειριστείτε τις αποθηκευμένες κάρτες σας. Πρόσθετο κείμενο που εξηγεί την πολιτική ασφάλειας της Τράπεζας για αποθηκευμένες κάρτες του χρήστη|en-us=Στη σελίδα αυτή μπορείτε να διαχειριστείτε τις αποθηκευμένες κάρτες σας. Πρόσθετο κείμενο που εξηγεί την πολιτική ασφάλειας της Τράπεζας για αποθηκευμένες κάρτες του χρήστη$}") %></div>
        <div class="PL5">
             <div class="SvdCardsMainGrid">
            <div class="SvdCardsGridBGTop" id="divCards" runat="server">
                <div class="SvdCardsGridTopTitle"><%=ResHelper.LocalizeString("{$=Αποθηκευμένες κάρτες|en-us=Αποθηκευμένες κάρτες$}") %></div>
                <div class="SvdCardsGridDiv">
                    <asp:GridView ID="GridViewPayments" EmptyDataTemplate-CssClass="SvdCardsPagerEmpty"  GridLines="None" runat="server"  AutoGenerateColumns="false"  Width="642px">
                    <HeaderStyle BackColor="#ffffff" ForeColor="#000000" Font-Size="12px" Height="35px" />
                    <AlternatingRowStyle Height="25px" BackColor="#ffffff" ForeColor="#58595b" Font-Size="11px" Font-Names="tahoma" />
                    <RowStyle Height="25px" BackColor="#ffffff" ForeColor="#58595b" Font-Size="11px" Font-Names="tahoma" />
                        <Columns >
                            <asp:TemplateField HeaderText="Φιλική Ονομασία Κάρτας" HeaderStyle-CssClass="SvdCardsGridHeader" ItemStyle-CssClass="SvdCardsGridItem"  >
                                <ItemTemplate>
                                    <asp:CheckBox ID="cbxCard" runat="server" Text='<%#Eval("FriendlyName") %>' ></asp:CheckBox>
                                    <asp:Label ID="lblID" style="display:none" runat="server" Text='<%#Eval("CardID") %>' ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Τύπος"   >
                                <ItemTemplate>
                                    <asp:label ID="lbltype" runat="server" Text='<%#Eval("CardDescr") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Αριθμός Κάρτας"    >
                                <ItemTemplate>
                                    <asp:label ID="lblcardno" runat="server" Text='<%#"xxxxxxxx-xxxx-" & Eval("LastFor") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ημερομηνία" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" HeaderStyle-CssClass="SvdCardsGridHeaderLast" ItemStyle-CssClass="SvdCardsGridItemLast" >
                                <ItemTemplate>
                                     <asp:label ID="lbldtm" runat="server" Text='<%#Eval("InsDate") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        <EmptyDataTemplate><div class="SvdCardsPagerEmpty">
                                <div style="padding-top:5px;padding-left:10px"><%=ResHelper.LocalizeString("{$=NoRecords|en-us=NoRecords$}") %></div>
                            </div>
                        </EmptyDataTemplate>
                       
                    </asp:GridView>
                </div>
                <asp:TextBox ID="HiddenCbxList" style="display:none" runat="server" Text="0"></asp:TextBox>
               
                
                <div class="SvdCardsbtnDeleteCard"><asp:ImageButton ID="btnDeleteCard"   runat="server" ImageUrl="/app_themes/LivePay/SavedCards/btnDeleteCard.png" /></div>
                <div class="SvdCardsBottomDescr"><font style="font-size:10px">></font> <%=ResHelper.LocalizeString("{$=Για να προσθέσετε νέα κάρτα θα πρέπει να επιλέξετε Αποθήκευση Κάρτας στο Προφίλ μου κατά την πραγματοποίηση μιας Πληρωμής μέσα στο Live Pay|en-us=Για να προσθέσετε νέα κάρτα θα πρέπει να επιλέξετε Αποθήκευση Κάρτας στο Προφίλ μου κατά την πραγματοποίηση μιας Πληρωμής μέσα στο Live Pay$}") %></div>
                <div id="BottomDiv" style="display:" runat="server" class="SrcHisPayGridBGBottom"></div> 
            </div>
            
       </div>
        </div>
    </div>
    <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
</div> 
</ContentTemplate>
</asp:UpdatePanel>
<script language ="javascript" type="text/javascript" >

    function DeleteCards() {
       
            var HiddenCbxList = document.getElementById('<%=HiddenCbxList.ClientID %>')

            if (HiddenCbxList.value != '0') {

                if (confirm('<%=ResHelper.LocalizeString("{$=Επιθυμείτε την διαγραφή των επιλεγμένων καρτών?|en-us=Παρακαλώ επιλέξτε Κάρτα$}") %>')) {
                    return true
                } else {
                    return false
                }

            } else {
                alert('<%=ResHelper.LocalizeString("{$=Παρακαλώ επιλέξτε Κάρτα|en-us=Παρακαλώ επιλέξτε Κάρτα$}") %>')
                return false
            }
    }


    function SendChbxValue(obj, val) {
        var GetCheckBox = obj
        var HiddenCbxList = document.getElementById('<%=HiddenCbxList.ClientID %>')
        var MyVal = HiddenCbxList.value
        if (GetCheckBox.checked) {
            MyVal = MyVal + ',' + val + '_'
        } else {
            
            MyVal = MyVal.replace("," + val + "_", "")
        }
        HiddenCbxList.value = MyVal
    }

</script>