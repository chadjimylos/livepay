CREATE PROCEDURE [Proc_CMS_SettingsKey_SelectAllSettings]
AS
SELECT SiteName, KeyName, KeyValue FROM CMS_SettingsKey LEFT OUTER JOIN CMS_Site ON
	CMS_SettingsKey.SiteID = CMS_Site.SiteID;
