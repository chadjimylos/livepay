<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentType_Edit_Sites.aspx.cs"
    Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_Sites" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Document Type Edit - Sites" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/ClassSites.ascx" TagName="ClassSites"
    TagPrefix="cms" %>
    
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:ClassSites ID="classSites" runat="server" />
</asp:Content>

