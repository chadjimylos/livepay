<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjectAttachmentSelector.ascx.cs"
    Inherits="CMSModules_ImportExport_Controls_Global_ObjectAttachmentSelector" %>
<asp:HiddenField ID="hdnLastSelected" runat="server" />
<asp:Literal ID="ltlScript" EnableViewState="false" runat="server" />

<script type="text/javascript">

    function SelectItem(id) {
        if (id != '') {
            if (hdnLastSelected.value != '') {
                var lastElem = document.getElementById(hdnLastSelected.value);
                if (lastElem != null) {
                    lastElem.className = 'GlobalItem';
                }
            }

            var elem = document.getElementById(id);
            if (elem != null) {
                elem.className = 'GlobalSelectedItem';
            }
            hdnLastSelected.value = id;
        }
    }
</script>

<asp:Panel ID="pnlTemplate" runat="Server">
    <div style="overflow: auto; height: <%= mPanelHeight %>px; border: 1px solid #b9d3e6;
        margin: 15px 3px 15px 3px;">
        <asp:Repeater ID="rptItems" runat="server">
            <ItemTemplate>
                <div class="GlobalItem" id="<%# Eval(mIDColumn) %>" onclick="SelectItem('<%# Eval(mIDColumn) %>')">
                    <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td style="vertical-align: middle;">
                                <img style="margin: 3px;" src="<%# GetPreviewImage(Eval(mIDColumn)) %>" width="<%= mPreviewWidth %>"
                                    height="<%= mPreviewHeight %>" alt="Preview" />
                            </td>
                            <td style="vertical-align: top;">
                                <div style="margin: 3px;">
                                    <div>
                                        <strong>
                                            <%# Eval(mDisplayNameColumn)%>
                                        </strong>
                                    </div>
                                    <br />
                                    <div>
                                        <%# Eval(mDescriptionColumn)%>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
<asp:Literal ID="ltlScriptAfter" EnableViewState="false" runat="server" />