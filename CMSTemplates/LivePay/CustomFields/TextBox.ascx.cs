using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;

public partial class CMSFormControls_TextBox : LivePayFormEngineUserControl
{
    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.TextBox.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return TextBox.Text.Trim();
        }
        set
        {
            TextBox.Text = (string)value;
        }
    }


    /// <summary>
    /// Clears current value
    /// </summary>
    public void Clear()
    {
        TextBox.Text = "";
    }


    /// <summary>
    /// Returns true if the number is empty
    /// </summary>
    public bool IsEmpty()
    {
        return (DataHelper.IsEmpty(TextBox.Text));
    }


    /// <summary>
    /// Returns true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        if (IsEmpty() && this.Mandatory)
        {
            return false;
        }
        if (this.RegularExp != "")
        {
            Validator val = new Validator();
            string result = val.IsRegularExp(TextBox.Text, ValidationHelper.GetString(string.Format("{0}", @"" + this.RegularExp), ""), "error").Result;
            if (result != "")
            {
                return false;
            }
        }

        return true;
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        CustomCloud_div.InnerHtml = string.Concat("<a id='CustomCloud_", this.RowCustomField, "' href='#'></a>");

        TextBox.MaxLength = this.MaxLength;

        if (this.RegularExp != "")
        {
            regexp_TextBox.Visible = true;
            regexp_TextBox.ValidationExpression = ValidationHelper.GetString(string.Format("{0}", @"" + this.RegularExp), "");
            regexp_TextBox.Attributes.Add("ControlValidated", TextBox.ClientID);
            regexp_TextBox.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", this.RowCustomField));
            regexp_TextBox.Attributes.Add("CustomValidatorErrorMessage", this.ValidationError);
            RegularExpressionValidator();
        }
        if (this.Mandatory) {
            rfv_TextBox.Visible = true;
            rfv_TextBox.Attributes.Add("ControlValidated", TextBox.ClientID);
            rfv_TextBox.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", this.RowCustomField));
            rfv_TextBox.Attributes.Add("CustomValidatorErrorMessage", this.ValidationError);
            RequiredFieldValidator();
        }
        //TextBox.Attributes.Add("MaxLength", "1");

    }

    public void RegularExpressionValidator()
    {
        bool Mandatory = this.Mandatory;

        string MandatoryScript = "";
        if (!Mandatory)
        {
            MandatoryScript = "if (ValidatorTrim(value).length == 0) \n" +
                              "   return true; \n";
        }

        string ValidatorScript = string.Empty;

        ValidatorScript = "\n" +
            "function RegularExpressionValidatorEvaluateIsValid(sender, args) { \n" +
                "var bIsValid = false; \n" +
                "var value = ValidatorGetValue(sender.controltovalidate);\n" +
                MandatoryScript +
                "var rx = new RegExp(sender.validationexpression);\n" +
                "var matches = rx.exec(value);\n" +
                "\n" +
                "bIsValid = (matches != null && value == matches[0]);\n" +
                "\n" +
                "var IsCutomValid = sender.getAttribute('IsCustomValidator'); \n" +
                "var CustomErrorMess = sender.getAttribute('CustomValidatorErrorMessage'); \n" +
                "if (IsCutomValid != null) { \n" +
                "   if (IsCutomValid.indexOf('CustomCloud_') != -1) { \n" +
                "       if (!bIsValid) { \n" +
                "           Tab1_ValidationCloud('#' + IsCutomValid, CustomErrorMess, true, ''); \n" +
                "       } else { \n" +
                "           Tab1_ValidationCloud('#' + IsCutomValid, '', false, ''); \n" +
                "       } \n" +
                "   } \n" +
                "} \n" +
                "return bIsValid; \n" +
            "}\n";

        ScriptManager.RegisterStartupScript(this.Page, typeof(String), "ValidatorScript_" + Guid.NewGuid().ToString(), ValidatorScript, true);
    }

    public void RequiredFieldValidator() {
        string ValidatorScript = string.Empty;
        ValidatorScript = "\n" +
        "function RequiredFieldValidatorEvaluateIsValid(sender) { \n" +
            "var bIsValid = false; \n" +
            "var value = ValidatorGetValue(sender.controltovalidate);\n" +
            "\n" +
            "if (ValidatorTrim(value).length > 0)\n" +
            "bIsValid = true;\n" +
            "\n" +
            "var IsCutomValid = sender.getAttribute('IsCustomValidator'); \n" +
            "var CustomErrorMess = sender.getAttribute('CustomValidatorErrorMessage'); \n" +
            "if (IsCutomValid != null) { \n" +
            "   if (IsCutomValid.indexOf('CustomCloud_') != -1) { \n" +
            "       if (!bIsValid) { \n" +
            "           Tab1_ValidationCloud('#' + IsCutomValid, CustomErrorMess, true, ''); \n" +
            "       } else { \n" +
            "           Tab1_ValidationCloud('#' + IsCutomValid, '', false, ''); \n" +
            "       } \n" +
            "   } \n" +
            "} \n" +
            "return bIsValid; \n" +
        "}\n";

        ScriptManager.RegisterStartupScript(this.Page, typeof(String), "ValidatorScript_rfv_" + Guid.NewGuid().ToString(), ValidatorScript, true);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }
}

