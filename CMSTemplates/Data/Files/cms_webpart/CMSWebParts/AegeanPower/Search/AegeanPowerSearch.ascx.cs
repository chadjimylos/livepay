using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.ISearchEngine;
using CMS.Controls;

public partial class CMSWebParts_AegeanPower_Search_AegeanPowerSearch : CMSAbstractWebPart
{
    // Result page url
    protected string mResultPageUrl = UrlHelper.CurrentURL;

    #region "Properties"

    public string TextSearch
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("TextSearch"), "");
        }
        set
        {
            this.SetValue("TextSearch", value);
        }
    }

    /// <summary>
    /// Gets or sets the search results page URL
    /// </summary>
    public string SearchResultsPageUrl
    {
        get
        {
            return  DataHelper.GetNotEmpty(GetValue("SearchResultsPageUrl"), mResultPageUrl);
        }
        set
        {
            SetValue("SearchResultsPageUrl", value);
            mResultPageUrl = value;
        }
    }


    /// <summary>
    ///  Gets or sets the Search mode
    /// </summary>
    public SearchModeEnum SearchMode
    {
        get
        {
            return CMSSearchDialog.GetSearchMode(ValidationHelper.GetString(GetValue("SearchMode"), ""));
        }
        set
        {
            SetValue("SearchMode", value.ToString());
        }
    }

    #endregion

    #region "Methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            // Set WebPart properties
            // this.MyControl.MyProperty = this.MyProperty;
            this.AegeanPowerSearchControl.TextSearch = this.TextSearch;
            this.AegeanPowerSearchControl.SearchResultsPageUrl = this.SearchResultsPageUrl;
            this.AegeanPowerSearchControl.SearchMode = this.SearchMode;
        }
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        // Reload data in all controls if needed
        // this.MyControl.ReloadData();
    }

    #endregion
}
