using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Staging_Tools_Data_View : CMSStagingPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'Manage tasks' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.staging", "ManageDataTasks"))
        {
            RedirectToAccessDenied("cms.staging", "ManageDataTasks");
        }

        CurrentMaster.Title.TitleText = ResHelper.GetString("Task.ViewHeader");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Staging/tasks.png");

        int taskId = QueryHelper.GetInteger("taskid", 0);
        ucViewTask.TaskId = taskId;
    }
}