using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.DirectoryUtilities;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.WorkflowEngine;
using CMS.SiteProvider;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.ExtendedControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_FileImport_Tools_Default : CMSToolsPage
{
    #region "Variables"

    private List<string> filesList = new List<string>();
    protected string targetAliasPath = "";
    protected long filesCount = 0;
    protected long allowedFilesCount = 0;
    protected string rootPath = "~/cmsimportfiles/";
    private ArrayList resultListIndex = null;
    private ArrayList resultListValues = null;
    private ArrayList errorFiles = null;
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), "") != "")
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.FileImport);
        }

        CurrentUserInfo user = CMSContext.CurrentUser;

        // Check permissions for CMS Desk -> Tools -> File Import
        if (!user.IsAuthorizedPerUIElement("CMS.Tools", "FileImport"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Tools", "FileImport");
        }

        if (!user.IsAuthorizedPerResource("CMS.FileImport", "ImportFiles"))
        {
            RedirectToCMSDeskAccessDenied("CMS.FileImport", "ImportFiles");
        }

        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Tools.FileImport.FileImport");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_FileImport/module.png");
        this.CurrentMaster.Title.HelpTopicName = "file_import";

        // Initialize culture selector
        this.cultureSelector.SiteID = CMSContext.CurrentSiteID;
        this.pathElem.SiteID = CMSContext.CurrentSiteID;

        // Prepare unigrid
        GetPath();
        gridImport.DataSource = GetFileSystemDataSource(gridImport.WhereClause);
        gridImport.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridImport_OnExternalDataBound);
        gridImport.SelectionJavascript = "UpdateCount";
        gridImport.ZeroRowsText = ResHelper.GetString("Tools.FileImport.NoFiles");
        gridImport.OnShowButtonClick += new EventHandler(gridImport_OnShowButtonClick);

        if (!Directory.Exists(rootPath))
        {
            lblError.Visible = true;
            lblError.Text = String.Format(ResHelper.GetString("Tools.FileImport.DirectoryDoesNotExist"), rootPath);
            plcImportContent.Visible = false;
            pnlTitle.Visible = false;
        }
        else
        {
            lblError.Visible = false;
            plcImportContent.Visible = true;
            pnlTitle.Visible = true;
        }

        lblTitle.Text = ResHelper.GetString("Tools.FileImport.ImportedFiles") + " " + rootPath + ": ";
        lblSelected.Text = string.Format(ResHelper.GetString("Tools.FileImport.SelectedCount"), filesCount);

        if (!RequestHelper.IsPostBack())
        {
            this.cultureSelector.Value = CMSContext.PreferredCultureCode;
        }

        ltlScript.Text += ScriptHelper.GetScript(
            " function UpdateCount(id, checked) {\n" +
            " var hidden = document.getElementById(\"" + hdnSelected.ClientID + "\")\n" +
            " var label =  document.getElementById(\"" + lblSelectedValue.ClientID + "\")\n" +
            " if (hidden.value.indexOf('|'+id+'|') != -1) {  \n" +
            "   if (checked == false) { \n"+
            "     hidden.value=hidden.value.replace('|'+id+'|', '');  \n" +
            "   } \n" +
            " } else { \n" +
            "   if (checked == true) { \n"+
            "     hidden.value = hidden.value + '|'+id+'|';  \n" +
            "   } \n" +   
            " } \n" +
            "  label.innerHTML = (hidden.value.split('|').length - 1) / 2; \n" +
            " }\n");
    }


    void gridImport_OnShowButtonClick(object sender, EventArgs e)
    {
        gridImport.ClearSelectedItems();
        hdnSelected.Value = "";
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (errorFiles != null)
        {
            gridImport.SelectedItems = errorFiles;
        }
        gridImport.DataSource = GetFileSystemDataSource(gridImport.WhereClause);
        gridImport.ReloadData();

        // Disable controls if no files found
        if (DataHelper.DataSourceIsEmpty(gridImport.DataSource))
        {
            btnStartImport.Enabled = false;
            pathElem.Enabled = false;
            pnlImportControls.Enabled = false;
        }
        else
        {
            btnStartImport.Enabled = true;
            pathElem.Enabled = true;
            pnlImportControls.Enabled = true;
            filesCount = ((DataSet)gridImport.DataSource).Tables[0].Rows.Count;
        }

        // Set labels
        string count = gridImport.SelectedItems.Count.ToString();
        hdnValue.Value = count;
        lblSelectedValue.Text = count;
        lblTotal.Text = string.Format(ResHelper.GetString("Tools.FileImport.TotalCount"), filesCount);
    }


    /// <summary>
    /// Gets path from current application settings.
    /// </summary>
    private string GetPath()
    {
        // If import folder for current site is not specified in settings set its path as rootpath
        if (!String.IsNullOrEmpty(CMSContext.CurrentSiteName))
        {
            // Get import folder path from settings
            string path = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + "." + "CMSFileImportFolder").Trim();
            if (!string.IsNullOrEmpty(path))
            {
                rootPath = EnsureValidPath(path);
            }
        }

        // Path starting with local driver letter
        if ((char.IsLetter(rootPath.ToLower(), 0)) && (rootPath[1] == ':'))
        {
            if (rootPath[2] != '\\')
            {
                rootPath = rootPath[0] + ":\\" + rootPath.Substring(2, rootPath.Length - 2);
            }
        }
        // Relative path
        else if (!((rootPath[0] == '\\') && (rootPath[1] == '\\')))
        {
            try
            {
                rootPath = HttpContext.Current.Server.MapPath(rootPath);
            }
            catch
            {
                plcImportContent.Visible = false;
                btnStartImport.Enabled = false;
                lblError.Visible = true;
                lblError.Text = String.Format(ResHelper.GetString("Tools.FileImport.InvalidFolder"), rootPath);
                return null;
            }
        }

        if (!String.IsNullOrEmpty(rootPath))
        {
            rootPath = rootPath.TrimEnd('\\') + "\\";
        }

        return rootPath;
    }


    /// <summary>
    /// Ensures that the given path is valid for the later use
    /// </summary>
    /// <param name="rootPath">Root path to valid</param>    
    private string EnsureValidPath(string rootPath)
    {
        return rootPath.Replace('/', '\\').TrimEnd('\\');
    }


    /// <summary>
    /// Removes all N (at the beginning of expression) from where condition
    /// (e.g. "column LIKE N'word'" => "column LIKE 'word'").
    /// </summary>
    /// <param name="where">WHERE condition</param>
    private string RemoveNFromWhereCondition(string where)
    {
        if (!String.IsNullOrEmpty(where))
        {
            // Remove all N (at the beginning of expression) from where condition (e.g. "column LIKE N'word'" => "column LIKE 'word'")
            bool inString = false;
            char prev = ' ';
            for (int i = 0; i < where.Length; i++)
            {
                if (where[i] == '\'')
                {
                    if (!inString && (prev == 'N'))
                    {
                        where = where.Remove(i - 1, 1);
                        where = where.Insert(i - 1, " ");
                    }
                    inString = !inString;
                }
                prev = where[i];
            }
        }

        return where;
    }


    /// <summary>
    /// Renames columns in WHERE condition.
    /// </summary>
    /// <param name="where">WHERE condition</param>
    /// <param name="oldColName">Old col name</param>
    /// <param name="newColName">New col name</param>
    private string RenameColumn(string where, string oldColName, string newColName)
    {
        if (!String.IsNullOrEmpty(where))
        {
            string[] strs = where.Split('\'');
            bool inString = where.StartsWith("'");
            where = "";
            for (int i = 0; i < strs.Length; i++)
            {
                // Rename/replace column name (avoid string literals)
                if (!inString && !String.IsNullOrEmpty(strs[i]))
                {
                    strs[i] = strs[i].Replace(oldColName, newColName);
                }

                // Create new WHERE condition
                where += (inString ? "'" : "") + strs[i] + (inString ? "'" : "");

                inString = !inString;
            }
        }

        return where;
    }


    /// <summary>
    /// Returns set of files in the file system.
    /// </summary>
    private DataSet GetFileSystemDataSource(string where)
    {
        string whereCond = RemoveNFromWhereCondition(where);
        whereCond = RenameColumn(whereCond, "[FilePath]", "[FileName]");
        this.fileSystemDataSource.WhereCondition = whereCond;
        this.fileSystemDataSource.Path = rootPath;

        try
        {
            return (DataSet)this.fileSystemDataSource.GetDataSource();
        }
        catch (Exception e)
        {
            lblError.Visible = true;
            lblError.Text = e.Message;
            return null;
        }
    }


    /// <summary>
    /// File list external databound handler.
    /// </summary>
    object gridImport_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        int index = -1;
        string filePath = ValidationHelper.GetString(parameter, "");
        switch (sourceName.ToLower())
        {
            case "filename":
                if (filePath.StartsWith(rootPath))
                {
                    return filePath.Substring(rootPath.Length);
                }
                break;
            case "result":
                    string result = "";;
                    if (resultListIndex != null)
                    {
                        index = resultListIndex.IndexOf(filePath);
                        if (index > -1)
                        {
                            object[] value = (object[])resultListValues[index];
                            if (ValidationHelper.GetBoolean(value[2], false))
                            {
                                result = UniGridFunctions.ColoredSpanMsg(ResHelper.GetString("Tools.FileImport.Imported"), true);
                            }
                            else
                            {
                                result = UniGridFunctions.ColoredSpanMsg((string)value[1], false);
                            }
                        }
                        else
                        {
                            result = ResHelper.GetString("Tools.FileImport.Skipped");
                        }
                    }
                return result;
        }
        return parameter.ToString();
    }



    /// <summary>
    /// btnStartImport click event handler.
    /// </summary>
    protected void btnStartImport_Click(System.Object sender, System.EventArgs e)
    {
        // Check license limitations
        if (!CheckFilesCount(gridImport.SelectedItems.Count))
        {
            lblError.Visible = true;
            lblError.Text = string.Format(ResHelper.GetString("Tools.FileImport.MaximumCountExceeded"), allowedFilesCount);
        }
        else
        {
            hdnValue.Value = null;
            hdnSelected.Value = null;
            string siteName = CMSContext.CurrentSiteName;
            string targetAliasPath = this.pathElem.Value.ToString().Trim();

            bool imported = false; // Flag - true if one file was imported at least
            bool importError = false; // Flag - true when import failed

            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
            TreeNode tn = tree.SelectSingleNode(siteName, targetAliasPath, TreeProvider.ALL_SITES);
            if (tn != null)
            {
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(tn.NodeClassName);

                if (dci != null)
                {
                    // Check if "file" and "folder" are allowed as a child document under selected document type
                    bool fileAllowed = false;
                    bool folderAllowed = false;
                    DataClassInfo folderClassInfo = DataClassInfoProvider.GetDataClass("CMS.Folder");
                    DataClassInfo fileClassInfo = DataClassInfoProvider.GetDataClass("CMS.File");
                    if ((fileClassInfo != null) || (folderClassInfo != null))
                    {
                        foreach (string fullFileName in gridImport.SelectedItems)
                        {
                            string[] paths = fullFileName.Substring(rootPath.Length).Split('\\');
                            if (paths.Length == 1)         // Check file
                            {
                                if (!fileAllowed && (fileClassInfo != null) && !DataClassInfoProvider.IsChildClassAllowed(dci.ClassID, fileClassInfo.ClassID))
                                {
                                    lblError.Visible = true;
                                    lblError.Text = ResHelper.GetString("Tools.FileImport.NotAllowedChildClass");
                                    return;
                                }
                                else
                                {
                                    fileAllowed = true;
                                }
                            }

                            if (paths.Length > 1)          // Check folder
                            {
                                if (!folderAllowed && (folderClassInfo != null) && !DataClassInfoProvider.IsChildClassAllowed(dci.ClassID, folderClassInfo.ClassID))
                                {
                                    lblError.Visible = true;
                                    lblError.Text = ResHelper.GetString("Tools.FileImport.FolderNotAllowedChildClass");
                                    return;
                                }
                                else
                                {
                                    folderAllowed = true;
                                }
                            }

                            if (fileAllowed && folderAllowed)
                            {
                                break;
                            }
                        }
                    }
                }

                int userId = CMSContext.CurrentUser.UserID;
                string cultureCode = ValidationHelper.GetString(this.cultureSelector.Value, "");
                string[] fileList = new string[1];
                string[] relativePathList = new string[1];
                resultListIndex = new ArrayList();
                resultListValues = new ArrayList();
                // Initialize error files list
                errorFiles = new ArrayList();

                // Insert files selected in datagrid to list of files to import
                foreach (string fullFileName in gridImport.SelectedItems)
                {
                    // Import selected files only
                    fileList[0] = fullFileName;
                    relativePathList[0] = fullFileName.Substring(rootPath.Length);

                    // Remove extension if needed
                    if (!chkIncludeExtension.Checked)
                    {
                        relativePathList[0] = Regex.Replace(relativePathList[0], "(.*)\\..*", "$1");
                    }

                    try
                    {
                        FileImport.ImportFiles(siteName, targetAliasPath, cultureCode, fileList, relativePathList, userId, chkDeleteImported.Checked);
                        resultListIndex.Add(fullFileName);
                        resultListValues.Add(new string[] { fullFileName, ResHelper.GetString("Tools.FileImport.Imported"), true.ToString() });
                        imported = true; // One file was imported
                    }
                    catch (Exception ex)
                    {
                        errorFiles.Add(fullFileName);
                        importError = true;
                        resultListIndex.Add(fullFileName);
                        resultListValues.Add(new string[] {fullFileName, ResHelper.GetString("Tools.FileImport.Failed") +
                            " (" + HTMLHelper.HTMLEncode(ex.Message) + ")", false.ToString()});
                    }
                }
            }
            else
            {
                // Specified alias path not found
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Tools.FileImport.AliasPathNotFound");
            }

            if (filesList.Count > 0)
            {
                if (!importError)
                {
                    if (imported)
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("Tools.FileImport.FilesWereImported");
                    }
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Tools.FileImport.FilesNotImported");
                }
            }
        }
    }


    /// <summary>
    /// Checks the total count of files to not exceed the license limitations
    /// </summary>
    /// <param name="selectedFilesCount">Selected files count</param>
    private bool CheckFilesCount(long selectedFilesCount)
    {
        long currentDocumentsCount = 0;
        DataSet dsDocuments = TreeHelper.GetDocuments(CMSContext.CurrentSiteName, "/%", TreeProvider.ALL_CULTURES, true, null, null, null, TreeProvider.ALL_LEVELS, false, -1, TreeProvider.SELECTNODES_REQUIRED_COLUMNS);
        if (!DataHelper.DataSourceIsEmpty(dsDocuments))
        {
            foreach (DataTable table in dsDocuments.Tables)
            {
                currentDocumentsCount += table.Rows.Count;
            }
        }

        int versionLimitations = LicenseKeyInfoProvider.VersionLimitations(LicenseHelper.CurrentLicenseInfo, FeatureEnum.Documents);
        allowedFilesCount = (versionLimitations - currentDocumentsCount);
        return !((versionLimitations != 0) && (versionLimitations < (currentDocumentsCount + selectedFilesCount)));
    }
}
