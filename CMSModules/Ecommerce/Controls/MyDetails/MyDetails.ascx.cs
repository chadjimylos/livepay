using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.URLRewritingEngine;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Controls_MyDetails_MyDetails : CMSAdminControl
{
    /// <summary>
    /// Inform on new customer creation
    /// </summary>
    /// <param name="url">Url of the page where control currently sits</param>
    public delegate void CustomerCreated();
    
    /// <summary>
    /// Fired when new customer is created
    /// </summary>
    public event CustomerCreated OnCustomerCrated;

    private CustomerInfo mCustomer = null;

    /// <summary>
    /// Customer info object.
    /// </summary>
    public CustomerInfo Customer
    {
        get
        {
        	 return mCustomer; 
        }
        set
        {
        	 mCustomer = value; 
        }
    }
    

    /// <summary>
    /// If true, control does not process the data
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["StopProcessing"], false);
        }
        set
        {
            ViewState["StopProcessing"] = value;
            this.drpCountry.StopProcessing = value;
            this.drpCurrency.StopProcessing = value;
            this.drpPayment.StopProcessing = value;
            this.drpShipping.StopProcessing = value;
        }
    }


    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!StopProcessing)
        {
            if (CMSContext.CurrentUser.IsAuthenticated())
            {
                btnOk.Text = ResHelper.GetString("General.OK");
                lblCustomerLastName.Text = ResHelper.GetString("Customers_Edit.CustomerLastNameLabel");
                lblCustomerFirstName.Text = ResHelper.GetString("Customers_Edit.CustomerFirstNameLabel");
                lblCustomerCompany.Text = ResHelper.GetString("Customers_Edit.CustomerCompanyLabel");
                lblCustomerFax.Text = ResHelper.GetString("customers_edit.CustomerFax");
                lblCustomerPhone.Text = ResHelper.GetString("customers_edit.CustomerPhone");
                lblCustomerPreferredCurrency.Text = ResHelper.GetString("customers_edit.CustomerCurrency");
                lblCustomerPreferredShippingOption.Text = ResHelper.GetString("customers_edit.CustomerShipping");
                lblCustomerPrefferedPaymentOption.Text = ResHelper.GetString("customers_edit.CustomerPayment");
                lblCustomerEmail.Text = ResHelper.GetString("general.email");
                lblCustomerCountry.Text = ResHelper.GetString("Customers_Edit.CustomerCountry");
                lblCompanyAccount.Text = ResHelper.GetString("Customers_Edit.lblCompanyAccount");                

                // WAI validation
                lblCustomerPreferredCurrency.AssociatedControlClientID = drpCurrency.InputClientID;
                lblCustomerPreferredShippingOption.AssociatedControlClientID = drpShipping.InputClientID;
                lblCustomerPrefferedPaymentOption.AssociatedControlClientID = drpPayment.InputClientID;
                lblCustomerCountry.AssociatedControlClientID = drpCountry.InputClientID;

                if (CMSContext.CurrentSite.SiteShowTaxRegistrationID)
                {
                    lblTaxRegistrationID.Text = ResHelper.GetString("Customers_Edit.lblTaxRegistrationID");
                }
                else
                {
                    plhTaxRegistrationID.Visible = false;
                }
                if (CMSContext.CurrentSite.SiteShowOrganizationID)
                {
                    lblOrganizationID.Text = ResHelper.GetString("Customers_Edit.lblOrganizationID");
                }
                else
                {
                    plhOrganizationID.Visible = false;
                }

                if (mCustomer != null)
                {
                    // Fill editing form
                    if (!RequestHelper.IsPostBack())
                    {
                        LoadData();

                        // Show that the customer was created or updated successfully
                        if (QueryHelper.GetString("saved", String.Empty) == "1")
                        {
                            lblInfo.Visible = true;
                            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                        }
                    }
                }
                else
                {
                    if (!RequestHelper.IsPostBack())
                    {
                        txtCustomerEmail.Text = CMSContext.CurrentUser.Email;
                    }
                    
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("MyAccount.MyDetails.CreateNewCustomer");
                }
            }
            else
            {
                // Hide if user is not authenticated
                this.Visible = false;
            }
        }
    }


    /// <summary>
    /// Load form data.
    /// </summary>
    public void LoadData()
    {
        txtCustomerCompany.Text = mCustomer.CustomerCompany;
        txtCustomerEmail.Text = mCustomer.CustomerEmail;
        txtCustomerFax.Text = mCustomer.CustomerFax;
        txtCustomerFirstName.Text = mCustomer.CustomerFirstName;
        txtCustomerLastName.Text = mCustomer.CustomerLastName;
        txtCustomerPhone.Text = mCustomer.CustomerPhone;
        txtOraganizationID.Text = mCustomer.CustomerOrganizationID;
        txtTaxRegistrationID.Text = mCustomer.CustomerTaxRegistrationID;

        if (mCustomer.CustomerCountryID > 0)
        {
            drpCountry.CountryID = mCustomer.CustomerCountryID;
        }
        if (mCustomer.CustomerStateID > 0)
        {
            drpCountry.StateID = mCustomer.CustomerStateID;
        }
        if (mCustomer.CustomerPreferredCurrencyID > 0)
        {
            drpCurrency.CurrencyID = mCustomer.CustomerPreferredCurrencyID;
        }
        if (mCustomer.CustomerPrefferedPaymentOptionID > 0)
        {
            drpPayment.PaymentID = mCustomer.CustomerPrefferedPaymentOptionID;
        }
        if (mCustomer.CustomerPreferredShippingOptionID > 0)
        {
            drpShipping.ShippingID = mCustomer.CustomerPreferredShippingOptionID;
        }

        if (!DataHelper.IsEmpty(txtCustomerCompany.Text) || !DataHelper.IsEmpty(txtOraganizationID.Text) || !DataHelper.IsEmpty(txtTaxRegistrationID.Text))
        {
            chkCompanyAccount.Checked = true;
            pnlCompanyInfo.Visible = true;
        }        
    }


    /// <summary>
    /// On chkCompanyAccount check box check changed event handler.
    /// </summary>
    protected void chkCompanyAccount_CheckChanged(object sender, EventArgs e)
    {
        // Displays/hides company info region
        if (chkCompanyAccount.Checked)
        {
            pnlCompanyInfo.Visible = true;
        }
        else
        {
            pnlCompanyInfo.Visible = false;
        }
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        string errorMessage = "";

        if ((txtCustomerCompany.Text.Trim() == "" || !chkCompanyAccount.Checked) &&
            ((txtCustomerFirstName.Text.Trim() == "") || (txtCustomerLastName.Text.Trim() == "")))
        {
            errorMessage = ResHelper.GetString("Customers_Edit.errorInsert");
        }
        // Check the following items if complete company info is required for company account
        if (errorMessage == "" && CMSContext.CurrentSite.SiteRequireOrgTaxRegIDs && chkCompanyAccount.Checked)
        {
            errorMessage = new Validator().NotEmpty(txtCustomerCompany.Text, ResHelper.GetString("customers_edit.errorCompany"))
                .NotEmpty(txtOraganizationID.Text, ResHelper.GetString("customers_edit.errorOrganizationID"))
                .NotEmpty(txtTaxRegistrationID.Text, ResHelper.GetString("customers_edit.errorTaxRegID")).Result;
        }

        if (errorMessage == "")
        {
            errorMessage = new Validator().IsEmail(txtCustomerEmail.Text.Trim(), ResHelper.GetString("customers_edit.erroremailformat")).Result;
        }

        if (errorMessage == "")
        {
            //// Check if user exists             
            //UserInfo ui = UserInfoProvider.GetUserInfo(txtCustomerEmail.Text.Trim());
            //if ((ui != null) && (ui.UserID != CMSContext.CurrentUser.UserID))
            //{
            //    lblError.Visible = true;
            //    lblError.Text = ResHelper.GetString("ShoppingCartUserRegistration.ErrorUserExists");
            //    return;
            //}


            // If customer doesn't already exist, create new one
            if (mCustomer == null)
            {
                mCustomer = new CustomerInfo();
                mCustomer.CustomerEnabled = true;
                mCustomer.CustomerUserID = CMSContext.CurrentUser.UserID;
            }

            mCustomer.CustomerPreferredCurrencyID = drpCurrency.CurrencyID;
            mCustomer.CustomerEmail = txtCustomerEmail.Text.Trim();
            mCustomer.CustomerPrefferedPaymentOptionID = drpPayment.PaymentID;
            mCustomer.CustomerFax = txtCustomerFax.Text.Trim();
            mCustomer.CustomerPreferredShippingOptionID = drpShipping.ShippingID;
            mCustomer.CustomerLastName = txtCustomerLastName.Text.Trim();
            mCustomer.CustomerPhone = txtCustomerPhone.Text.Trim();
            mCustomer.CustomerFirstName = txtCustomerFirstName.Text.Trim();
            mCustomer.CustomerCountryID = drpCountry.CountryID;
            mCustomer.CustomerStateID = drpCountry.StateID;
            mCustomer.CustomerCreated = DateTime.Now;

            if (chkCompanyAccount.Checked)
            {
                mCustomer.CustomerCompany = txtCustomerCompany.Text.Trim();
                if (CMSContext.CurrentSite.SiteShowOrganizationID)
                {
                    mCustomer.CustomerOrganizationID = txtOraganizationID.Text.Trim();
                }
                if (CMSContext.CurrentSite.SiteShowTaxRegistrationID)
                {
                    mCustomer.CustomerTaxRegistrationID = txtTaxRegistrationID.Text.Trim();
                }
            }
            else
            {
                mCustomer.CustomerCompany = "";
                mCustomer.CustomerOrganizationID = "";
                mCustomer.CustomerTaxRegistrationID = "";
            }

            // Update customer data
            CustomerInfoProvider.SetCustomerInfo(mCustomer);

            // Update corresponding user email
            UserInfo user = UserInfoProvider.GetUserInfo(mCustomer.CustomerUserID);
            if (user != null)
            {
                user.Email = mCustomer.CustomerEmail;                
                UserInfoProvider.SetUserInfo(user);
            }

            // Let others now that customer was created
            if (OnCustomerCrated != null)
            {
                OnCustomerCrated();

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
            else 
            {
                UrlHelper.Redirect(UrlHelper.AddParameterToUrl(URLRewriter.CurrentURL, "saved", "1"));
            }            
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    /// <summary>
    /// Raises OnShippingChange event
    /// </summary>
    protected void drpShipping_ShippingChange(object sender, EventArgs e)
    {
        this.drpPayment.ShippingOptionID = this.drpShipping.ShippingID;
        this.drpPayment.PaymentID = 0;
    }


    /// <summary>
    /// Overriden SetValue - because of MyAccount webpart.
    /// </summary>
    /// <param name="propertyName">Name of the property to set</param>
    /// <param name="value">Value to set</param>
    public override void SetValue(string propertyName, object value)
    {
        base.SetValue(propertyName, value);

        switch (propertyName.ToLower())
        {
            case "customer":
                this.Customer = value as CustomerInfo;
                break;
        }
    }
} 
