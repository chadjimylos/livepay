<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NameForm.ascx.cs" Inherits="CMSAdminControls_UI_NameForm" %>
<table>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblDisplayName" runat="server" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="100" />
            <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ErrorMessage="" ControlToValidate="txtDisplayName"
                EnableViewState="false"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblCodeName" runat="server" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="100" />
            <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ErrorMessage="" ControlToValidate="txtCodeName"
                EnableViewState="false"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <cms:CMSButton ID="btnOk" runat="server" Visible="false" CssClass="SubmitButton" EnableViewState="false" />
        </td>
    </tr>
</table>
