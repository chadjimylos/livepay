<%@ Page Language="C#" AutoEventWireup="true" CodeFile="System_Frameset.aspx.cs" Inherits="CMSSiteManager_Administration_System_System_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Administration - System</title>
</head>
<frameset border="0" rows="72, *" id="rowsFrameset">
    <frame name="systemMenu" src="System_Header.aspx" frameborder="0" scrolling="no" noresize="noresize" />
    <frame name="systemContent" src="System.aspx" frameborder="0" />
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
</frameset>
</html>
