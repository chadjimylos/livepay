using System;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Staging;
using CMS.UIControls;

public partial class CMSModules_Staging_Tools_Tasks_Log : CMSStagingPage
{
    #region "Private variables"

    private int serverId = 0;
    private int taskId = 0;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register modal dialog scripts
        RegisterModalPageScripts();

        // Check 'Manage servers' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.staging", "ManageTasks"))
        {
            RedirectToAccessDenied("cms.staging", "ManageTasks");
        }

        serverId = QueryHelper.GetInteger("serverid", 0);
        taskId = QueryHelper.GetInteger("taskid", 0);

        object[,] parameters = new object[2, 3];
        parameters[0, 0] = "@TaskID";
        parameters[0, 1] = taskId;
        parameters[1, 0] = "@ServerID";
        parameters[1, 1] = serverId;

        gridLog.QueryParameters = parameters;
        gridLog.OnAction += gridLog_OnAction;
        gridLog.ZeroRowsText = ResHelper.GetString("Task.LogNoEvents");

        CurrentMaster.Title.TitleText = ResHelper.GetString("Task.LogHeader");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Staging_Task/object.png");
        CurrentMaster.DisplayControlsPanel = true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        // Get task data
        if (taskId > 0)
        {
            gridLog.GridView.Columns[1].Visible = false;
            TaskInfo ti = TaskInfoProvider.GetTaskInfo(taskId);
            if (ti != null)
            {
                lblInfo.Text += String.Format(ResHelper.GetString("Task.LogTaskInfo"), Server.HtmlEncode(ti.TaskTitle));
            }
        }
        // Get server data
        if (serverId > 0)
        {
            gridLog.GridView.Columns[3].Visible = false;
            ServerInfo si = ServerInfoProvider.GetServerInfo(serverId);
            if (si != null)
            {
                if (lblInfo.Text != "")
                {
                    lblInfo.Text += "<br /><br />";
                }
                lblInfo.Text += String.Format(ResHelper.GetString("Task.LogServerInfo"), si.ServerDisplayName);
            }
        }
        lblInfo.Visible = (lblInfo.Text != "");
        base.OnPreRender(e);
    }

    #endregion


    #region "Control events"

    /// <summary>
    /// UniGrid action event handler
    /// </summary>
    protected void gridLog_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "delete":
                int logid = ValidationHelper.GetInteger(actionArgument, 0);
                if (logid > 0)
                {
                    SyncLogInfoProvider.DeleteSyncLogInfo(logid);
                }
                break;
        }
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        SyncLogInfoProvider.DeleteSyncLogInfo(taskId, serverId);
        gridLog.ReloadData();
    }

    #endregion
}