<%@ Control Language="C#" Inherits="CMSModules_AdminControls_Controls_Class_FieldEditor"
    CodeFile="FieldEditor.ascx.cs" %>
<%@ Register Src="~/CMSFormControls/Inputs/LargeTextArea.ascx" TagName="LargeTextArea" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/TimeZones/TimeZoneSelector.ascx" TagName="TimeZone" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/TimeZones/TimeZoneTypeSelector.ascx" TagName="TimeZoneType"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/AutoResizeConfiguration.ascx" TagName="AutoResize"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Attachments/DocumentattachmentsConfiguration.ascx"
    TagName="DocAttachments" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/ExtensionsConfiguration.ascx"
    TagName="FileExtensions" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/Configuration/DialogStartConfiguration.ascx"
    TagName="DialogConfig" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/SelectCssStylesheet.ascx" TagName="SelectCssStylesheet"
    TagPrefix="cms" %>
<%@ Register Namespace="CMS.FormControls" Assembly="CMS.FormControls" TagPrefix="cms" %>
<%@ Register Namespace="CMS.ExtendedControls" Assembly="CMS.ExtendedControls" TagPrefix="cms" %>
<asp:Panel ID="pnlFieldEditorContentCss" runat="server">
    <asp:Label ID="lblError" runat="server" CssClass="FieldEditorErrorLabel" EnableViewState="false" /><asp:Label
        ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />
    <table border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td style="vertical-align: top;" id="AttributeListBoxPadding">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:ListBox ID="lstAttributes" runat="server" AutoPostBack="True" OnSelectedIndexChanged="lstAttributes_SelectedIndexChanged"
                                EnableViewState="true" CssClass="AttributesList" />
                            <asp:Panel ID="pnlSourceField" runat="server">
                                <asp:Label ID="lblSourceField" runat="server" EnableViewState="false" /><br />
                                <asp:DropDownList ID="drpSourceField" runat="server" CssClass="SourceFieldDropDown"
                                    AutoPostBack="true" OnSelectedIndexChanged="drpSourceField_SelectedIndexChanged">
                                </asp:DropDownList>
                                <br />
                                <asp:Label ID="lblSourceAliasField" runat="server" EnableViewState="false" /><br />
                                <asp:DropDownList ID="drpSourceAliasField" runat="server" CssClass="SourceFieldDropDown"
                                    AutoPostBack="true" OnSelectedIndexChanged="drpSourceField_SelectedIndexChanged">
                                </asp:DropDownList>
                            </asp:Panel>
                        </td>
                        <td style="padding: 50px 6px 0px 6px; vertical-align: top;">
                            <asp:PlaceHolder ID="plcActions" runat="server">
                                <asp:ImageButton ID="btnUpAttribute" runat="server" Width="24" Height="24" OnClick="btnUpAttribute_Click" /><br />
                                <br />
                                <asp:ImageButton ID="btnDownAttribute" runat="server" Width="24" Height="24" OnClick="btnDownAttribute_Click" /><br />
                                <br />
                                <br />
                                <br />
                                <asp:ImageButton ID="btnNewCategory" runat="server" Width="24" Height="24" OnClick="btnNewCategory_Click" /><br />
                                <br />
                                <asp:ImageButton ID="btnNewSysAttribute" runat="server" Width="24" Height="24" OnClick="btnNewSysAttribute_Click" /><br />
                                <br />
                                <br />
                                <br />
                                <asp:ImageButton ID="btnNewAttribute" runat="server" Width="24" Height="24" OnClick="btnNewAttribute_Click" /><br />
                                <br />
                                <asp:ImageButton ID="btnDeleteItem" runat="server" Width="24" Height="24" OnClick="btnDeleteItem_Click" />
                            </asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 500px; vertical-align: top;">
                <asp:Panel ID="pnlFieldEditorContent" runat="server" CssClass="FieldEditorContent">
                    <%-- Advanced mode BEGIN --%>
                    <asp:Panel ID="pnlAdvancedAttributeEdit" runat="server">
                        <%-- Database section --%>
                        <asp:Panel ID="pnlDatabaseSection" runat="server">
                            <asp:Label ID="lblSectionDatabase" runat="server" CssClass="SectionTitle" />
                            <table border="0" cellpadding="2" cellspacing="2">
                                <asp:PlaceHolder ID="plcGuid" runat="server" Visible="false">
                                    <tr>
                                        <td class="TextColumn">
                                            <asp:Label ID="lblGuid" runat="server" EnableViewState="false" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGuidValue" runat="server" EnableViewState="false" />
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="plcGroup" runat="server">
                                    <tr>
                                        <td class="TextColumn">
                                            <asp:Label ID="lblGroup" runat="server" EnableViewState="false" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpGroup" runat="server" CssClass="DropDownField" OnSelectedIndexChanged="drpGroup_SelectedIndexChanged"
                                                AutoPostBack="true" />
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblAdvancedAttributeName" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpSystemFields" runat="server" CssClass="DropDownField" OnSelectedIndexChanged="drpSystemFields_SelectedIndexChanged"
                                            AutoPostBack="true" DataValueField="column_name" DataTextField="column_name" />
                                        <asp:TextBox ID="txtAdvancedAttributeName" runat="server" CssClass="TextBoxField"
                                            MaxLength="100" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblAttributeType" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="lstAttributeType" runat="server" DataTextField="Text" DataValueField="Value"
                                            CssClass="DropDownField" AutoPostBack="True" OnSelectedIndexChanged="lstAttributeType_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblAttributeSize" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtAttributeSize" runat="server" CssClass="TextBoxField" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblAdvancedAllowEmpty" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkAdvancedAllowEmpty" runat="server" CssClass="CheckBoxMovedLeft" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblAdvancedDefaultValue" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <%-- default value --%>
                                        <asp:TextBox ID="txtAdvancedDefaultValue" runat="server" CssClass="TextBoxField" />
                                        <cms:LargeTextArea ID="txtAdvancedLargeDefaultValue" runat="server" />
                                        <asp:CheckBox ID="chkAdvancedDefaultValue" runat="server" CssClass="CheckBoxMovedLeft" />
                                        <cms:DateTimePicker ID="datetimeAdvancedDefatulValue" runat="server" />
                                    </td>
                                </tr>
                                <asp:PlaceHolder ID="plcAdvancedIsSystem" runat="server">
                                    <tr>
                                        <td class="TextColumn">
                                            <asp:Label ID="lblAdvancedIsSystem" runat="server" EnableViewState="false" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkAdvancedIsSystem" runat="server" CssClass="CheckBoxMovedLeft" />
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>
                            </table>
                        </asp:Panel>
                        <br />
                        <asp:PlaceHolder ID="plcAdvancedTimeOptions" runat="server" Visible="false">
                            <asp:Label ID="lblTimeZoneSection" runat="server" CssClass="SectionTitle" />
                            <table border="0" cellpadding="2" cellspacing="2">
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblAdvancedTimeZoneType" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <cms:TimeZoneType ID="advancedtimeZoneType" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblAdvancedTimeZone" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:Panel ID="pnlTimeZone" runat="server">
                                            <cms:TimeZone ID="advancedTimeZone" runat="server" UseZoneNameForSelection="true" />
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:PlaceHolder>
                        <br />
                        <asp:CheckBox ID="chkDisplayInForm" runat="server" AutoPostBack="True" OnCheckedChanged="chkDisplayInForm_CheckedChanged"
                            CssClass="CheckBoxMovedLeft"></asp:CheckBox>
                        <%-- Display in form: BEGIN--%>
                        <asp:Panel ID="pnlDisplayInForm" runat="server">
                            <br />
                            <asp:Label ID="lblSectionField" runat="server" CssClass="SectionTitle" />
                            <table border="0" cellpadding="2" cellspacing="2">
                                <asp:PlaceHolder ID="pnlSimplifiedModeFieldSettings" runat="server">
                                    <asp:PlaceHolder ID="pnlAdvancedPublicField" runat="server">
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblAdvancedPublicField" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkAdvancedPublicField" runat="server" CssClass="CheckBoxMovedLeft" />
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="plcVisibility" runat="server" Visible="false">
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblVisibility" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <cms:VisibilityControl ID="ctrlVisibility" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblVisibilityControl" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="drpVisibilityControl" runat="server" DataValueField="UserControlCodeName"
                                                    DataTextField="UserControlDisplayName" CssClass="DropDownField" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblChangeVisibility" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <asp:CheckBox ID="chkChangeVisibility" runat="server" CssClass="CheckBoxMovedLeft" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <tr>
                                        <td class="TextColumn">
                                            <asp:Label ID="lblAdvancedFieldCaption" runat="server" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAdvancedFieldCaption" runat="server" CssClass="TextBoxField" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TextColumn">
                                            <asp:Label ID="lblAdvancedFieldType" runat="server" />
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="lstAdvancedFieldType" runat="server" AutoPostBack="True" DataTextField="Text"
                                                DataValueField="Value" CssClass="DropDownField" OnSelectedIndexChanged="lstFieldType_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="TextColumn">
                                        </td>
                                        <td>
                                            <asp:Panel ID="pnlAdvacnedUploadFile" runat="server">
                                                <strong>
                                                    <asp:Label ID="lblAdvancedUploadFile" runat="server" EnableViewState="false" /><br />
                                                </strong>
                                                <div style="margin: -3px">
                                                    <cms:AutoResize ID="autoResizeAdvanced" runat="server" />
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAdvancedTextAreaSettings" runat="server">
                                                <table>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedTextAreaCols" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedTextAreaCols" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedTextAreaRows" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedTextAreaRows" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedTextAreaSize" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedTextAreaSize" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAdvancedBBEditorSettings" runat="server">
                                                <table>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedBBEditorCols" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedBBEditorCols" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedBBEditorRows" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedBBEditorRows" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedBBEditorSize" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedBBEditorSize" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblBBEnableImage" runat="server" EnableViewState="false" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <cms:LocalizedRadioButton ID="radBBUrlNo" runat="server" ResourceString="general.no"
                                                                GroupName="EnableImage" />&nbsp;
                                                            <cms:LocalizedRadioButton ID="radBBUrlSimple" runat="server" ResourceString="forum.settings.simpledialog"
                                                                GroupName="EnableImage" />&nbsp;
                                                            <cms:LocalizedRadioButton ID="radBBUrlAdvanced" runat="server" ResourceString="forum.settings.advanceddialog"
                                                                GroupName="EnableImage" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Label ID="lblBBEnableLink" runat="server" EnableViewState="false" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <cms:LocalizedRadioButton ID="radBBImageNo" runat="server" ResourceString="general.no"
                                                                GroupName="EnableURL" />&nbsp;
                                                            <cms:LocalizedRadioButton ID="radBBImageSimple" runat="server" ResourceString="forum.settings.simpledialog"
                                                                GroupName="EnableURL" />&nbsp;
                                                            <cms:LocalizedRadioButton ID="radBBImageAdvanced" runat="server" ResourceString="forum.settings.advanceddialog"
                                                                GroupName="EnableURL" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkAdvancedBBEditorShowQuote" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkAdvancedBBEditorShowCode" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkAdvancedBBEditorShowBold" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkAdvancedBBEditorShowItalic" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkAdvancedBBEditorShowUnderline" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkAdvancedBBEditorShowStrike" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkAdvancedBBEditorShowColor" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkAdvancedBBEditorUsePromptDialog" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAdvancedDocAttachmentsSettings" runat="server">
                                                <div style="margin: -3px">
                                                    <cms:DocAttachments ID="ucAdvancedDocAttachments" runat="server" />
                                                </div>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAdvancedHTMLAreaSettings" runat="server">
                                                <table>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedHTMLAreaWidth" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedHTMLAreaWidth" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedHTMLAreaHeight" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedHTMLAreaHeight" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedHTMLAreaSize" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedHTMLAreaSize" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedCssStyleSheet" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <cms:SelectCssStylesheet ID="ctrlAdvancedStyleSheet" runat="server" StopProcessing="true" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedToolbarSet" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedToolbarSet" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedStartingPath" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedStartingPath" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAdvancedDropDownListSettings" runat="server">
                                                <asp:PlaceHolder ID="plcAdvancedRadioButtonsDirection" runat="server">
                                                    <table>
                                                        <tr>
                                                            <td class="TextColumnSettings">
                                                                <asp:Label ID="lblAdvancedRadioButtonsDirection" runat="server" EnableViewState="false" />
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="drpAdvancedRadioButtonsDirection" runat="server" CssClass="SmallDropDown" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:PlaceHolder>
                                                <div><asp:RadioButton ID="radAdvancedDropDownListOptions" runat="server" Checked="True"
                                                    GroupName="dropdownlistsettings"></asp:RadioButton>
                                                </div>
                                                <div>
                                                <asp:RadioButton ID="radAdvancedDropDownListSQL" runat="server" GroupName="dropdownlistsettings" />
                                                </div>
                                                <asp:TextBox ID="txtAdvancedDropDownListOptions" runat="server" CssClass="TextAreaField"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAdvancedCalendarSettings" runat="server">
                                                <div><asp:CheckBox ID="chkAdvancedCalendarSettingsEditTime" runat="server" Checked="True"
                                                    CssClass="CheckBoxMovedLeft"></asp:CheckBox></div>
                                                <asp:CheckBox ID="chkAdvancedCalendarSettingsShowNowLink" runat="server" Checked="True"
                                                    CssClass="CheckBoxMovedLeft"></asp:CheckBox>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAdvancedMultipleChoice" runat="server">
                                                <table>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedMultipleChoiceDirection" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="drpAdvancedMultipleChoiceDirection" runat="server" CssClass="SmallDropDown" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                <asp:RadioButton ID="radAdvancedMultipleChoiceOptions" runat="server" Checked="True"
                                                    GroupName="multiplechoicesettings"></asp:RadioButton>
                                                </div>
                                                <div>
                                                <asp:RadioButton ID="radAdvancedMultipleChoiceSQL" runat="server" GroupName="multiplechoicesettings" />
                                                </div>
                                                <asp:TextBox ID="txtAdvancedMultipleChoiceOptions" runat="server" Rows="8" Columns="32"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAdvancedListBox" runat="server">
                                                <table>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedListBoxMultipleChoices" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkAdvancedListBoxMultipleChoices" runat="server" Checked="true" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                <asp:RadioButton ID="radAdvancedListBoxOptions" runat="server" Checked="True" GroupName="multiplechoicesettings" />
                                                </div>
                                                <div>
                                                <asp:RadioButton ID="radAdvancedListBoxSQL" runat="server" GroupName="multiplechoicesettings" />
                                                </div>
                                                <asp:TextBox ID="txtAdvancedListBoxOptions" runat="server" Rows="8" Columns="32"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAdvancedImageSelection" runat="server">
                                                <strong>
                                                    <cms:LocalizedLabel ID="lblIAdvancedmagePreview" runat="server" EnableViewState="false"
                                                        ResourceString="TemplateDesigner.ImagePreview" />
                                                </strong>
                                                <table>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedImageWidth" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedImageWidth" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedImageHeight" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedImageHeight" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="TextColumnSettings">
                                                            <asp:Label ID="lblAdvancedImageMaxSideSize" runat="server" EnableViewState="false" />
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtAdvancedImageMaxSideSize" runat="server" CssClass="SmallTextBox" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlAutoResize" runat="server">
                                                <strong>
                                                    <asp:Label ID="lblAutoResize" runat="server" EnableViewState="false" /><br />
                                                </strong>
                                                <div style="margin: -3px">
                                                    <cms:AutoResize ID="autoResizeElem" runat="server" />
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>
                                <asp:PlaceHolder ID="plcAdvacedFieldSettings" runat="server" Visible="false">
                                    <tr>
                                        <td class="TextColumn">
                                            <asp:Label ID="lblAdvacedFieldSettings" runat="server" EnableViewState="false"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkAdvacedFieldSettings" runat="server"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="padding-left: 3px;">
                                            <asp:Panel ID="pnlAdvancedFieldSettings" runat="server">
                                                <cms:DialogConfig ID="elemAdvancedDialogConfig" runat="server" />
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblFieldDescription" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDescription" runat="server" CssClass="TextAreaField" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <asp:PlaceHolder runat="server" ID="plcSpellCheck">
                                    <tr>
                                        <td class="TextColumn">
                                            <asp:Label ID="lblSpellCheck" runat="server" EnableViewState="false" />
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkSpellCheck" runat="server" />
                                        </td>
                                    </tr>
                                </asp:PlaceHolder>
                            </table>
                            <br />
                            <%-- Validation section --%>
                            <asp:Panel ID="pnlSectionValidation" runat="server">
                                <asp:Label ID="lblSectionValidation" runat="server" CssClass="SectionTitle" />
                                <table border="0" cellpadding="2" cellspacing="2">
                                    <asp:PlaceHolder ID="pnlTextValidation" runat="server">
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblRegExpr" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtRegExpr" runat="server" CssClass="TextBoxField" />
                                            </td>
                                        </tr>
                                        <asp:PlaceHolder runat="server" ID="pnlMinMaxLengthValidation">
                                            <tr>
                                                <td class="TextColumn">
                                                    <asp:Label ID="lblMinLength" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMinLength" runat="server" CssClass="TextBoxField" MaxLength="9" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumn">
                                                    <asp:Label ID="lblMaxLength" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMaxLength" runat="server" CssClass="TextBoxField" MaxLength="9" />
                                                </td>
                                            </tr>
                                        </asp:PlaceHolder>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="pnlNumberValidation" runat="server">
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblMinValue" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMinValue" runat="server" CssClass="TextBoxField" MaxLength="20" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblMaxValue" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMaxValue" runat="server" CssClass="TextBoxField" MaxLength="20" />
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="pnlDateTimeValidation" runat="server">
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblFrom" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <cms:DateTimePicker ID="dateFrom" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblTo" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <cms:DateTimePicker ID="dateTo" runat="server" />
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="pnlFileValidation" runat="server">
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblFileExtensions" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <cms:FileExtensions ID="fileExt" runat="server" />
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="pnlErrorMessageValidation" runat="server">
                                        <tr>
                                            <td class="TextColumn">
                                                <asp:Label ID="lblErrorMessage" runat="server" EnableViewState="false" />
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtErrorMessage" runat="server" CssClass="TextBoxField" />
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                </table>
                                <br />
                            </asp:Panel>
                            <%-- Design section --%>
                            <asp:Label ID="lblSectionDesign" runat="server" CssClass="SectionTitle" />
                            <table border="0" cellpadding="2" cellspacing="2">
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblCaptionStyle" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCaptionStyle" runat="server" CssClass="TextBoxField" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblInputStyle" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtInputStyle" runat="server" CssClass="TextBoxField" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblControlCssClass" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtControlCssClass" runat="server" CssClass="TextBoxField" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <%-- Display in form: END --%>
                    </asp:Panel>
                    <%-- Advanced mode END --%>
                    <%-- Category --%>
                    <asp:Panel ID="pnlCategoryEdit" runat="server">
                        <asp:Label ID="lblSectionCategory" runat="server" CssClass="SectionTitle" />
                        <table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                                <td class="TextColumn">
                                    <asp:Label ID="lblCategoryName" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCategoryName" runat="server" CssClass="TextBoxField" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <%-- Simplified mode BEGIN --%>
                    <asp:Panel ID="pnlSimpleAttributeEdit" runat="server">
                        <table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                                <td class="TextColumn">
                                    <asp:Label ID="lblSimpleColumnName" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSimpleColumnName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                                </td>
                            </tr>
                            <asp:PlaceHolder ID="pnlSimplePublicField" runat="server">
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblSimplePublicField" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSimplePublicField" runat="server" CssClass="CheckBoxMovedLeft" />
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                            <tr>
                                <td class="TextColumn">
                                    <asp:Label ID="lblSimpleFieldCaption" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSimpleFieldCaption" runat="server" CssClass="TextBoxField" />
                                </td>
                            </tr>
                            <tr>
                                <td class="TextColumn">
                                    <asp:Label ID="lblSimpleFieldType" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:DropDownList ID="lstSimpleFieldType" runat="server" AutoPostBack="True" DataTextField="Text"
                                        DataValueField="Value" CssClass="DropDownField" OnSelectedIndexChanged="lstFieldType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="TextColumn">
                                </td>
                                <td>
                                    <asp:Panel ID="pnlSimpleUploadFile" runat="server">
                                        <strong>
                                            <asp:Label ID="lblSimpleUploadFile" runat="server" EnableViewState="false" /><br />
                                        </strong>
                                        <div style="margin: -3px">
                                            <cms:AutoResize ID="autoResizeSimple" runat="server" />
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSimpleTextAreaSettings" runat="server">
                                        <table>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleTextAreaCols" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleTextAreaCols" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleTextAreaRows" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleTextAreaRows" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleTextAreaSize" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleTextAreaSize" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSimpleBBEditorSettings" runat="server">
                                        <table>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleBBEditorCols" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleBBEditorCols" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleBBEditorRows" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleBBEditorRows" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleBBEditorSize" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleBBEditorSize" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSimpleHTMLAreaSettings" runat="server">
                                        <table>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleHTMLAreaWidth" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleHTMLAreaWidth" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleHTMLAreaHeight" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleHTMLAreaHeight" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleHTMLAreaSize" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleHTMLAreaSize" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleCssStyleSheet" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <cms:SelectCssStylesheet ID="ctrlSimpleStyleSheet" runat="server" StopProcessing="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleToolbarSet" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleToolbarSet" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleStartingPath" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleStartingPath" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSimpleDropDownListSettings" runat="server">
                                        <asp:PlaceHolder ID="plcSimpleRadioButtonsDirection" runat="server">
                                            <table>
                                                <tr>
                                                    <td class="TextColumnSettings">
                                                        <asp:Label ID="lblSimpleRadioButtonsDirection" runat="server" EnableViewState="false" />
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="drpSimpleRadioButtonsDirection" runat="server" CssClass="SmallDropDown" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:PlaceHolder>
                                        <asp:RadioButton ID="radSimpleDropDownListOptions" runat="server" Checked="True"
                                            GroupName="dropdownlistsettings"></asp:RadioButton>
                                        <br />
                                        <asp:RadioButton ID="radSimpleDropDownListSQL" runat="server" GroupName="dropdownlistsettings" />
                                        <br />
                                        <asp:TextBox ID="txtSimpleDropDownListOptions" runat="server" CssClass="TextAreaField"
                                            TextMode="MultiLine"></asp:TextBox>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSimpleCalendarSettings" runat="server">
                                        <div><asp:CheckBox ID="chkSimpleCalendarSettingsEditTime" runat="server" Checked="True"
                                            CssClass="CheckBoxMovedLeft"></asp:CheckBox></div>
                                        <div><asp:CheckBox ID="chkSimpleCalendarSettingsShowNowLink" runat="server" Checked="True"
                                            CssClass="CheckBoxMovedLeft"></asp:CheckBox></div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSimpleMultipleChoice" runat="server">
                                        <table>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleMultipleChoiceDirection" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="drpSimpleMultipleChoiceDirection" runat="server" CssClass="SmallDropDown" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:RadioButton ID="radSimpleMultipleChoiceOptions" runat="server" Checked="True"
                                            GroupName="multiplechoicesettings"></asp:RadioButton>
                                        <br />
                                        <asp:RadioButton ID="radSimpleMultipleChoiceSQL" runat="server" GroupName="multiplechoicesettings" />
                                        <br />
                                        <asp:TextBox ID="txtSimpleMultipleChoiceOptions" runat="server" Rows="8" Columns="32"
                                            TextMode="MultiLine"></asp:TextBox>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSimpleListBox" runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblSimpleListBoxMultipleChoices" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkSimpleListBoxMultipleChoices" runat="server" Checked="true" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:RadioButton ID="radSimpleListBoxOptions" runat="server" Checked="True" GroupName="multiplechoicesettings" />
                                        <br />
                                        <asp:RadioButton ID="radSimpleListBoxSQL" runat="server" GroupName="multiplechoicesettings" />
                                        <br />
                                        <asp:TextBox ID="txtSimpleListBoxOptions" runat="server" Rows="8" Columns="32" TextMode="MultiLine" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSimpleTextBox" runat="server">
                                        <table>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleTextBoxMaxLength" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleTextBoxMaxLength" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSimpleImageSelection" runat="server">
                                        <strong>
                                            <cms:LocalizedLabel ID="lblSimpleImagePreview" runat="server" EnableViewState="false"
                                                ResourceString="TemplateDesigner.ImagePreview" />
                                        </strong>
                                        <table>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleImageWidth" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleImageWidth" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleImageHeight" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleImageHeight" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="TextColumnSettings">
                                                    <asp:Label ID="lblSimpleImageMaxSideSize" runat="server" EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSimpleImageMaxSideSize" runat="server" CssClass="SmallTextBox" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td class="TextColumn">
                                    <asp:Label ID="lblSimpleAllowEmpty" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkSimpleAllowEmpty" runat="server" CssClass="CheckBoxMovedLeft" />
                                </td>
                            </tr>
                            <tr>
                                <td class="TextColumn">
                                    <asp:Label ID="lblSimpleDefaultValue" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <%-- default value --%>
                                    <asp:TextBox ID="txtSimpleDefault" runat="server" CssClass="TextBoxField" />
                                    <cms:LargeTextArea ID="txtSimpleLargeDefault" runat="server" />
                                    <asp:CheckBox ID="chkSimpleDefault" runat="server" CssClass="CheckBoxMovedLeft" />
                                    <cms:DateTimePicker ID="datetimeSimpleDefault" runat="server" />
                                </td>
                            </tr>
                            <asp:PlaceHolder ID="plcSimpleTimeOptions" runat="server">
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblSimpleTimeZoneType" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <cms:TimeZoneType ID="SimpletimeZoneType" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblSimpleTimeZone" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <cms:TimeZone ID="SimpleTimeZone" runat="server" UseZoneNameForSelection="true" />
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="plcSimpleIsSystem" runat="server">
                                <tr>
                                    <td class="TextColumn">
                                        <asp:Label ID="lblSimpleIsSystem" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkSimpleIsSystem" runat="server" CssClass="CheckBoxMovedLeft" />
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                        </table>
                    </asp:Panel>
                    <%-- Simplified mode END --%>
                </asp:Panel>
                <%-- FieldEditorContent END --%>
                <asp:Panel ID="pnlFieldEditorFooterClass" runat="server" CssClass="FieldEditorFooter">
                    <div class="FloatLeft">
                        <cms:LocalizedLinkButton ID="btnSimplified" runat="server" ResourceString="fieldeditor.tabs.simplifiedmode"
                            Visible="false" OnCommand="btnSimplified_Command" />
                        <cms:LocalizedLinkButton ID="btnAdvanced" runat="server" ResourceString="fieldeditor.tabs.advancedmode"
                            Visible="false" OnCommand="btnAdvanced_Command" />
                    </div>
                    <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Literal ID="ltlConfirmText" runat="server" EnableViewState="false" />
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />

<script type="text/javascript">
            //<![CDATA[
            function confirmDelete() {
                return confirm(document.getElementById('confirmdelete').value);
            }
            //]]>
</script>

