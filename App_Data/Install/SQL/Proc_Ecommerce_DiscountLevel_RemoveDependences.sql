-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Ecommerce_DiscountLevel_RemoveDependences]
	@DiscountLevelID int
AS
BEGIN
	DELETE FROM COM_DiscountLevelDepartment WHERE DiscountLevelID=@DiscountLevelID;
	UPDATE COM_Customer SET CustomerDiscountLevelID = NULL WHERE CustomerDiscountLevelID = @DiscountLevelID;
END
