using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.FormControls;
using CMS.UIControls;
using CMS.Ecommerce;

public partial class CMSModules_Ecommerce_Tools_Customers_Customer_Edit_CustomFields : CMSEcommercePage
{
    protected int customerId = 0;


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // set edit mode
        customerId = ValidationHelper.GetInteger(Request.QueryString["customerId"], 0);
        if (customerId > 0)
        {
            formCustomerCustomFields.Info = CustomerInfoProvider.GetCustomerInfo(customerId);
            formCustomerCustomFields.OnBeforeSave += new DataForm.OnBeforeSaveEventHandler(formCustomerCustomFields_OnBeforeSave);
            formCustomerCustomFields.OnAfterSave += new DataForm.OnAfterSaveEventHandler(formCustomerCustomFields_OnAfterSave);
        }
        else
        {
            formCustomerCustomFields.Enabled = false;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (formCustomerCustomFields.BasicForm != null)
        {
            if (formCustomerCustomFields.BasicForm.FieldControls.Count <= 0)
            {
                // Hide submit button if no field are present
                formCustomerCustomFields.BasicForm.SubmitButton.Visible = false;
            }
            else
            {
                // Set submit button's css class
                formCustomerCustomFields.BasicForm.SubmitButton.CssClass = "ContentButton";
            }
        }
    }


    void formCustomerCustomFields_OnBeforeSave()
    {
        // Check 'EcommerceModify' permission
        if ((CMSContext.CurrentSite != null) && (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify")))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }
    }


    void formCustomerCustomFields_OnAfterSave()
    {
        // Display 'changes saved' information
        this.lblInfo.Visible = true;
        this.lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}
