using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Xml;

using CMS.CMSHelper;
using CMS.UIControls;
using CMS.GlobalHelper;

namespace CMSControlsExamples
{
    public partial class Menu : ControlsExamplesPage
    {
        protected void Page_Load(Object sender, EventArgs e)
        {
            if (!RequestHelper.IsPostBack())
            {
                string applicationPath = HttpContext.Current.Request.ApplicationPath;

                StreamReader sr = File.OpenText(Server.MapPath("menu.xml"));
                string xmlDoc = sr.ReadToEnd();

                sr.Close();

                if (applicationPath == "/")
                {
                    xmlDoc = xmlDoc.Replace(">~/", ">/");
                }
                else
                {
                    xmlDoc = xmlDoc.Replace(">~/", ">" + applicationPath + "/");
                }

                Stream stream = new MemoryStream(System.Text.ASCIIEncoding.Default.GetBytes(xmlDoc));
                XmlDocument xml = new XmlDocument();
                xml.Load(stream);

                Menu1.DataSource = xml;
                Menu1.CssClass = "menustyle";
                Menu1.DefaultMouseOverCssClass = "menustyleover";
                Menu1.DefaultTarget = "desktop";
                Menu1.HighlightTopMenu = true;
                Menu1.DataBind();
                Menu1.Cursor = CMS.skmMenuControl.MouseCursor.Pointer;

                stream.Close();
            }
        }
    }
}
