<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_Edit_CustomFields.aspx.cs" Inherits="CMSModules_Ecommerce_Content_Product_Product_Edit_CustomFields" Theme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Product Edit - Custom fields</title>
    <style type="text/css">
    body
    {
        margin: 0px;
        padding: 0px;
        height:100%; 
    }
    </style>
</head>
<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="pnlHeader" CssClass="PageHeaderLine">
            <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false" />
            <asp:CheckBox CssClass="CheckBoxProd" ID="chkMarkDocAsProd" runat="server" OnCheckedChanged="chkMarkDocAsProd_CheckedChanged"
                AutoPostBack="true" Checked="true" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlLeft" CssClass="FullTabsLeft" EnableViewState="false">
            &nbsp;
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlPropTabs" CssClass="TabsTabs">
            <asp:Panel runat="server" ID="pnlWhite" CssClass="TabsWhite">
                <cms:BasicTabControl ID="BasicTabControlMenu" runat="server" />
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlRight" CssClass="FullTabsRight" EnableViewState="false">
            &nbsp;
        </asp:Panel>
        <asp:Panel ID="pnlBody" runat="server" CssClass="PageBody">
            <asp:Panel ID="pblContent" runat="server" CssClass="PageContent">
                <asp:Label runat="server" ID="lblInfo2" CssClass="InfoLabel" EnableViewState="false" />
                <cms:DataForm ID="formProductCustomFields"  runat="server" ClassName="Ecommerce.SKU" IsLiveSite="false" />
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
