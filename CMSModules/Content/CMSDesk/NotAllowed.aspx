<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NotAllowed.aspx.cs" Inherits="CMSModules_Content_CMSDesk_NotAllowed"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Content - Not allowed" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <asp:Label ID="lblError" runat="server" ForeColor="Red" EnableViewState="false" />
</asp:Content>