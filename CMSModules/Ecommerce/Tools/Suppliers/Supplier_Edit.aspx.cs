using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Suppliers_Supplier_Edit : CMSEcommercePage
{
    protected int suplierid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        rfvDisplayName.ErrorMessage = ResHelper.GetString("supplier_Edit.errorEmptyDisplayName");

        // control initializations
        lblSupplierFax.Text = ResHelper.GetString("supplier_Edit.SupplierFaxLabel");
        lblSupplierEmail.Text = ResHelper.GetString("supplier_Edit.SupplierEmailLabel");
        lblSupplierPhone.Text = ResHelper.GetString("supplier_Edit.SupplierPhoneLabel");
        lblSupplierDisplayName.Text = ResHelper.GetString("supplier_Edit.SupplierDisplayNameLabel");

        btnOk.Text = ResHelper.GetString("General.OK");

        string currentSupplier = ResHelper.GetString("supplier_Edit.NewItemCaption");

        // get supplier id from querystring
        if ((Request.QueryString["suplierid"] != null) && (Request.QueryString["suplierid"] != ""))
        {
            suplierid = ValidationHelper.GetInteger(Request.QueryString["suplierid"], 0);
            SupplierInfo supplierObj = SupplierInfoProvider.GetSupplierInfo(suplierid);
            currentSupplier = supplierObj.SupplierDisplayName;

            // fill editing form
            if (!RequestHelper.IsPostBack())
            {
                LoadData(supplierObj);

                // show that the supplier was created or updated successfully
                if (Request.QueryString["saved"] == "1")
                {
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
            }
        }

        this.CurrentMaster.Title.HelpTopicName = "newedit_supplier";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Initializes page title breadcrumbs control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("supplier_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Suppliers/supplier_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentSupplier;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// Load data of editing supplier.
    /// </summary>
    /// <param name="supplierObj">Supplier object.</param>
    protected void LoadData(SupplierInfo supplierObj)
    {
        txtSupplierFax.Text = supplierObj.SupplierFax;
        txtSupplierEmail.Text = supplierObj.SupplierEmail;
        txtSupplierPhone.Text = supplierObj.SupplierPhone;
        txtSupplierDisplayName.Text = supplierObj.SupplierDisplayName;
        chkSupplierEnabled.Checked = supplierObj.SupplierEnabled;
    }


    /// <summary>
    /// Sets the data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        string errorMessage = new Validator().NotEmpty(txtSupplierDisplayName.Text, ResHelper.GetString("supplier_Edit.errorEmptyDisplayName")).Result;


        // Validate email format if not empty
        if (errorMessage == "")
        {
            if ((txtSupplierEmail.Text.Trim() != "") && (!ValidationHelper.IsEmail(txtSupplierEmail.Text.Trim())))
            {
                errorMessage = ResHelper.GetString("supplier_Edit.errorEmailFormat");
            }
        }

        if (errorMessage == "")
        {
            // Check unique name
            SupplierInfo supplierObj = SupplierInfoProvider.GetSupplierInfo(txtSupplierDisplayName.Text.Trim());
            if ((supplierObj == null) || (supplierObj.SupplierID == suplierid))
            {
                // Get object
                if ((supplierObj == null))
                {
                    supplierObj = SupplierInfoProvider.GetSupplierInfo(suplierid);
                    if (supplierObj == null)
                    {
                        supplierObj = new SupplierInfo();
                    }
                }

                supplierObj.SupplierFax = txtSupplierFax.Text.Trim();
                supplierObj.SupplierEmail = txtSupplierEmail.Text.Trim();
                supplierObj.SupplierPhone = txtSupplierPhone.Text.Trim();
                supplierObj.SupplierDisplayName = txtSupplierDisplayName.Text.Trim();
                supplierObj.SupplierEnabled = chkSupplierEnabled.Checked;

                SupplierInfoProvider.SetSupplierInfo(supplierObj);

                UrlHelper.Redirect("supplier_Edit.aspx?suplierid=" + Convert.ToString(supplierObj.SupplierID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("supplier_Edit.SupplierNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
