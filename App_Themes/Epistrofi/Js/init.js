function checkAnauthorizedPage() {
    var url = document.location.href.toLowerCase()
    if (url.indexOf('anauthorized=1') != -1) {
        var urlFinal = url.replace("?anauthorized=1", "");
        document.location.href = urlFinal;
    }
}

// page init
jQuery(function () {
    initCustomForms();

    //popups
    
    initPopups();
});




//function for click to call
function OpenClickToCall(pageURL, title, w, h) {
    w =580;
    h = 410;
    var left = (screen.width / 2.5) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    var targetWin = window.open(pageURL, title, 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

//function for click to chat
function OpenClickToChat(pageURL, title, w, h) {
    w = 484;
    h = 361;
    var left = (screen.width / 2.5) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    var targetWin = window.open(pageURL, title, 'width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
} 

// function for the FAQs

function _hideBeforeExpand(id) {
    var c = jQuery(id).attr('className');
    var visible = jQuery('div.' + c).filter(function () { return jQuery(this).is(':visible') });
    var onCompleted = function () {

        if (jQuery(id).is(':visible')) {            
            jQuery(id).slideUp('normal');
            jQuery('#boxOfInsideTitle').slideDown('normal');
            jQuery(id + 'Link').removeClass('sel');
        }
        else {
            jQuery(id).slideDown('normal');
            jQuery('#boxOfInsideTitle').slideUp('normal');
            jQuery(id + 'Link').addClass('sel');
        }
    };

    if (visible.size() > 0)
        visible.slideUp('normal');

    onCompleted();
}

// function for the lightbox

function show_lightbox(DivID,Width,Height,Left)
{
	// get popup
    var popup = jQuery('div#' + DivID);
	Left = (document.body.offsetWidth / 2) - (Width / 2) + 'px'; 
	// show popup
	jQuery.blockUI(
	{
		message: popup
		,css:
		{
			border: 0,
			top:'20%',
			left:Left,
			width:Width,
			height:Height,
			backgroundColor: 'transparent'
		}
		,overlayCSS:
		{
			backgroundColor: '#000'
		}
	});
}




// popups function
function initPopups() {

  if (document.location.href.toLowerCase().indexOf('cmsp') != -1) {
        return false
    }
    var _zIndex = 1000;
    var _fadeSpeed = 350;
    var _faderOpacity = 0.75;
    var _faderBackground = '#000';
    var _faderId = 'lightbox-overlay';
    var _closeLink = 'a.close';
    var _fader;
    var _lightbox = jQuery('<div class="lightbox"><div class="t"></div><div class="m"><div class="inside"><a href="#" class="close" id="aClosePopUp"></a><div class="holder"></div></div></div><div class="b"></div></div>');
   
    var _iframeHolder = _lightbox.find('.holder');
    var _btnClose = _lightbox.find('a.close');
    
    var _openers = jQuery('a.iframe-popup');
    var _page = jQuery(document);
    var _minWidth = parseInt(jQuery('body').css('minWidth'));
    var _scroll = false;

    

    // init popup fader
    _fader = jQuery('#' + _faderId);
    if (!_fader.length) {
        _fader = jQuery('<div />');
        _fader.attr('id', _faderId);
        jQuery('body').append(_fader).append(_lightbox.hide());
    }
    _fader.css({
        opacity: _faderOpacity,
        backgroundColor: _faderBackground,
        position: 'absolute',
        overflow: 'hidden',
        display: 'none',
        top: 0,
        left: 0,
        zIndex: _zIndex
    });

    // IE6 iframe fix
    if (jQuery.browser.msie && jQuery.browser.version < 7) {
        if (!_fader.children().length) {
            var _frame = jQuery('<iframe  src="javascript:false" frameborder="0" scrolling="no" />');
            _frame.css({
                opacity: 0,
                width: '100%',
                height: '100%'
            });
            var _frameOverlay = jQuery('<div>');
            _frameOverlay.css({
                top: 0,
                left: 0,
                zIndex: 1,
                opacity: 0,
                background: '#000',
                position: 'absolute',
                width: '100%',
                height: '100%'
            });
            _fader.empty().append(_frame).append(_frameOverlay);
        }
    }

    // lightbox positioning function
    function positionLightbox() {
        if (_lightbox) {
            var _windowHeight = jQuery(window).height();
            var _windowWidth = jQuery(window).width();
            var _lightboxWidth = _lightbox.outerWidth();
            var _lightboxHeight = _lightbox.outerHeight();
            var _pageHeight = _page.height();

            if (_windowWidth < _minWidth) _fader.css('width', _minWidth);
            else _fader.css('width', '100%');
            if (_windowHeight < _pageHeight) _fader.css('height', _pageHeight);
            else _fader.css('height', _windowHeight);

            _lightbox.css({
                position: 'absolute',
                zIndex: (_zIndex + 1)
            });

            // vertical position
            if (_windowHeight > _lightboxHeight) {
                if (jQuery.browser.msie && jQuery.browser.version < 7) {
                    _lightbox.css({
                        position: 'absolute',
                        top: parseInt(jQuery(window).scrollTop()) + (_windowHeight - _lightboxHeight) / 2
                    });
                } else {
                    _lightbox.css({
                        position: 'fixed',
                        top: (_windowHeight - _lightboxHeight) / 2
                    });
                }
            } else {
                var _faderHeight = _fader.height();
                if (_faderHeight < _lightboxHeight) _fader.css('height', _lightboxHeight);
                if (!_scroll) {
                    if (_faderHeight - _lightboxHeight > parseInt(jQuery(window).scrollTop())) {
                        _faderHeight = parseInt(jQuery(window).scrollTop())
                        _scroll = _faderHeight;
                    } else {
                        _scroll = _faderHeight - _lightboxHeight;
                    }
                }
                _lightbox.css({
                    position: 'absolute',
                    top: _scroll
                });
            }

            // horizontal position
            if (_lightbox.outerWidth() < jQuery(window).width()) {
                _lightbox.css({ left: (jQuery(window).width() - _lightbox.outerWidth()) / 2 });
            } else {
                if (_fader.width() > _lightbox.outerWidth()) _lightbox.css({ left: (_fader.width() - _lightbox.outerWidth()) / 2 });
                else _lightbox.css({ left: 0 });
            }
        }
    }

    // show/hide lightbox
    function toggleState(_state) {
        if (!_lightbox) return;

        if (_state) {
            _fader.fadeIn(_fadeSpeed, function () {
                _lightbox.fadeIn(_fadeSpeed);
            });
            _scroll = false;
            positionLightbox();
        } else {
            _lightbox.fadeOut(_fadeSpeed, function () {
			
                _fader.fadeOut(_fadeSpeed);
                _scroll = false;
				jQuery('.holder iframe').attr('src', '/');
				
				
            });
            checkAnauthorizedPage()
        }        
    }
   
    // close lightbox button
    _btnClose.click(function () {
        toggleState(false);
        return false;
    })

  
    // lightbox openers
    _openers.each(function () {
        var _opener = jQuery(this);
        var _meta = _opener.attr('rel').split('|');
        var _target = _opener.attr('href');

        _opener.click(function () {
            var _iframe = jQuery('<iframe name="test" allowtransparency="true" frameborder="0" scrolling="no"></iframe>');
            _iframe.attr('width', _meta[0]);
            _iframe.attr('height', _meta[1]);
            _iframe.attr('src', _target);
            _iframeHolder.empty().append(_iframe);
            toggleState(true);
            return false;
        });
    });

    // event handlers
    jQuery(window).resize(positionLightbox);
    jQuery(window).scroll(positionLightbox);
    jQuery(document).keydown(function (e) {
        if (e.keyCode == 27) toggleState(false);
    });
    _fader.click(function () {
        if (!_fader.is(':animated')) toggleState(false);
        return false;
    });

}

// page init
function initCustomForms() {
    jQuery('input:radio').customRadio();
}


// custom radios module
jQuery.fn.customRadio = function (_options) {
    var _options = jQuery.extend({
        radioStructure: '<div></div>',
        radioDisabled: 'disabled',
        radioDefault: 'radioArea',
        radioChecked: 'radioAreaChecked'
    }, _options);
    return this.each(function () {
        var radio = jQuery(this);
        if (!radio.hasClass('outtaHere') && radio.is(':radio')) {
            var replaced = jQuery(_options.radioStructure);
            this._replaced = replaced;
            if (radio.is(':disabled')) replaced.addClass(_options.radioDisabled);
            else if (radio.is(':checked')) replaced.addClass(_options.radioChecked);
            else replaced.addClass(_options.radioDefault);
            replaced.click(function () {
                if (jQuery(this).hasClass(_options.radioDefault)) {
                    radio.attr('checked', 'checked');
                    changeRadio(radio.get(0));
                }
            });
            radio.click(function () {
                changeRadio(this);
            });
            replaced.insertBefore(radio);
            radio.addClass('outtaHere');
        }
    });
    function changeRadio(_this) {
        jQuery(_this).change();
        jQuery('input:radio[name=' + jQuery(_this).attr("name") + ']').not(_this).each(function () {
            if (this._replaced && !jQuery(this).is(':disabled')) this._replaced.removeClass().addClass(_options.radioDefault);
        });
        _this._replaced.removeClass().addClass(_options.radioChecked);
    }

}



