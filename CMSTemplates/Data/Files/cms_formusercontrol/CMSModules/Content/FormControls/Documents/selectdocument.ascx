<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectDocument.ascx.cs"
    Inherits="CMSModules_Content_FormControls_Documents_SelectDocument" %>
<asp:TextBox ID="txtName" runat="server" MaxLength="800" CssClass="SelectorTextBox" /><cms:CMSButton
    ID="btnSelect" runat="server" CssClass="ContentButton" /><cms:CMSButton ID="btnClear"
        runat="server" CssClass="ContentButton" />
