using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSAdminControls_UI_System_RequireScript : CMSUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.ScriptTitle.TitleText = ResHelper.GetString("RequireScript.Title");
        this.lblInfo.Text = ResHelper.GetString("RequireScript.Information");
        this.ScriptTitle.TitleImage = GetImageUrl("Design/Controls/RequireScript/javascript.png");
        this.btnContinue.Text = ResHelper.GetString("RequireScript.Continue");
    }
}
