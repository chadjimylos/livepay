using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SiteProvider;

using TreeNode = CMS.TreeEngine.TreeNode;
using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_Content_CMSDesk_Properties_Workflow : CMSPropertiesPage
{
    #region "Private variables"

    // Current step ID
    private int currentStepId = 0;

    // Current Node
    private TreeNode mNode = null;
    private TreeProvider mTree = null;
    private WorkflowManager mWorkflowManager = null;

    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;
    private TimeZoneInfo usedTimeZone = null;
    private WorkflowInfo mWorkflowInfo = null;
    private bool wfInfoBinded = false;

    #endregion


    #region "Properties"

    private TreeProvider Tree
    {
        get
        {
            return mTree ?? (mTree = new TreeProvider(CMSContext.CurrentUser));
        }
    }


    private WorkflowManager WorkflowManager
    {
        get
        {
            return mWorkflowManager ?? (mWorkflowManager = new WorkflowManager(Tree));
        }
    }


    private TreeNode Node
    {
        get
        {
            return mNode ?? (mNode = DocumentHelper.GetDocument(NodeID, CMSContext.PreferredCultureCode, Tree));
        }
    }


    private static int NodeID
    {
        get
        {
            // Current Node ID        
            return QueryHelper.GetInteger("nodeid", 0);
        }
    }


    private WorkflowInfo WorkflowInfo
    {
        get
        {
            if ((mWorkflowInfo == null) && !wfInfoBinded)
            {
                mWorkflowInfo = WorkflowManager.GetNodeWorkflow(Node);
                wfInfoBinded = true;
            }
            return mWorkflowInfo;
        }
    }


    /// <summary>
    /// Determines whether to send workflow emails
    /// </summary>
    private static bool SendWorkflowEmails
    {
        get
        {
            return SettingsKeyProvider.GetBoolValue("CMSSendWorkflowEmails");
        }
    }

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.Workflow"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.Workflow");
        }

        // Hide custom steps if license doesn't allow them
        if (!WorkflowInfoProvider.IsCustomStepAllowed())
        {
            gridSteps.WhereCondition = "((StepName = 'edit') OR (StepName = 'published') OR (StepName = 'archived'))";
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the scripts
        ScriptHelper.RegisterProgress(Page);

        UIContext.PropertyTab = PropertyTabEnum.Workflow;
        // Turn sorting off
        gridSteps.GridView.AllowSorting = false;

        // Prepare the query parameters
        object[,] parameters = new object[1, 3];
        parameters[0, 0] = "@DocumentID";
        parameters[0, 1] = 0;

        // Prepare the steps query parameters
        object[,] stepsParameters = new object[1, 3];
        stepsParameters[0, 0] = "@StepWorkflowID";
        stepsParameters[0, 1] = 0;

        if (Node != null)
        {
            // Check read permissions
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(Node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), Node.NodeAliasPath));
            }

            // Prepare parameters
            if (Node != null)
            {
                parameters[0, 1] = Node.DocumentID;
                currentStepId = ValidationHelper.GetInteger(Node.GetValue("DocumentWorkflowStepID"), 0);
            }

            if (WorkflowInfo != null)
            {
                stepsParameters[0, 1] = WorkflowInfo.WorkflowID;
                lblWorkflowName.Text = string.Format(ResHelper.GetString("WorfklowProperties.lblWorkflowName"), WorkflowInfo.WorkflowDisplayName);
            }

            // Initialize unigrids
            gridHistory.OnExternalDataBound += gridHistory_OnExternalDataBound;
            gridHistory.OnPageChanged += gridHistory_OnPageChanged;
            gridHistory.OnBeforeSorting += gridHistory_OnBeforeSorting;
            gridSteps.OnExternalDataBound += gridSteps_OnExternalDataBound;
            gridSteps.OnPageChanged += gridSteps_OnPageChanged;

            // Setup form appearance
            SetupForm();
        }
        else
        {
            pnlContent.Visible = false;
        }
        gridSteps.QueryParameters = stepsParameters;
        gridHistory.QueryParameters = parameters;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!RequestHelper.IsPostBack() && (WorkflowInfo != null))
        {
            ReloadData();
        }

        // Check if document has any version history record
        if (Node != null)
        {
            if (Node.DocumentCheckedOutVersionHistoryID == 0)
            {
                lblInfo.Text = ResHelper.GetString("WorfklowProperties.DocumentHasNoHistory");
            }
        }
        lblEmptyHistory.Visible = (gridHistory.GridView.Rows.Count == 0);
    }

    #endregion


    #region "Grid events"

    protected void gridSteps_OnPageChanged(object sender, EventArgs e)
    {
        ReloadData(false, true, false);
    }


    protected void gridHistory_OnBeforeSorting(object sender, EventArgs e)
    {
        ReloadData(false, true, true);
    }


    protected void gridHistory_OnPageChanged(object sender, EventArgs e)
    {
        ReloadData(false, false, true);
    }


    /// <summary>
    /// External step binding
    /// </summary>
    protected object gridSteps_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "currentstepinfo":
                DataRowView data = (DataRowView)parameter;
                if (currentStepId <= 0)
                {
                    string stepName = ValidationHelper.GetString(data["StepName"], string.Empty);
                    if (stepName.ToLower() == "edit")
                    {
                        return "<img src=\"" + GetImageUrl("CMSModules/CMS_Content/Properties/currentstep.png") + "\" alt=\"\" />";
                    }
                }
                else
                {
                    int stepId = ValidationHelper.GetInteger(data["StepID"], 0);
                    if (stepId == currentStepId)
                    {
                        return "<img src=\"" + GetImageUrl("CMSModules/CMS_Content/Properties/currentstep.png") + "\" alt=\"\" />";
                    }
                }
                return string.Empty;

            case "steporder":
                // Get grid row
                GridViewRow row = (GridViewRow)((DataControlFieldCell)sender).Parent;
                int pageOffset = (gridSteps.Pager.CurrentPage - 1) * gridSteps.Pager.CurrentPageSize;
                // Return row index
                return (pageOffset + row.RowIndex + 1).ToString();

        }
        return parameter;
    }


    /// <summary>
    /// External history binding
    /// </summary>
    protected object gridHistory_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "action":
                bool wasrejected = ValidationHelper.GetBoolean(parameter, false);
                return wasrejected ? ResHelper.GetString("WorfklowProperties.Rejected") : ResHelper.GetString("WorfklowProperties.Approved");

            case "approvedwhen":
            case "approvedwhentooltip":
                if (string.IsNullOrEmpty(parameter.ToString()))
                {
                    return string.Empty;
                }
                else
                {
                    if (currentUserInfo == null)
                    {
                        currentUserInfo = CMSContext.CurrentUser;
                    }

                    if (currentSiteInfo == null)
                    {
                        currentSiteInfo = CMSContext.CurrentSite;
                    }

                    if (sourceName == "approvedwhen")
                    {
                        return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME),
                            currentUserInfo, currentSiteInfo, out usedTimeZone);
                    }
                    else
                    {
                        if (TimeZoneHelper.TimeZonesEnabled() && (usedTimeZone == null))
                        {
                            TimeZoneHelper.GetCurrentTimeZoneDateTimeString(ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME),
                                currentUserInfo, currentSiteInfo, out usedTimeZone);
                        }
                        return TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone);
                    }
                }

            case "formattedusername":
                DataRowView drv = (DataRowView)parameter;
                string userName = ValidationHelper.GetString(drv["UserName"], String.Empty);
                string fullName = ValidationHelper.GetString(drv["FullName"], String.Empty);

                return HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(userName, fullName));

        }
        return parameter;
    }

    #endregion


    #region "Protected methods"

    /// <summary>
    /// Reloads the page data
    /// </summary>
    protected void ReloadData()
    {
        ReloadData(true, true, true);
    }


    /// <summary>
    /// Reloads the page data
    /// </summary>
    protected void ReloadData(bool reloadForm, bool reloadHistory, bool reloadSteps)
    {
        if (reloadForm)
        {
            SetupForm();
        }
        if (reloadHistory)
        {
            gridHistory.ReloadData();
        }
        if (reloadSteps)
        {
            gridSteps.ReloadData();
        }
    }


    protected void DisableForm(bool allowApprove)
    {
        pnlForm.Enabled = allowApprove;
        btnApprove.Enabled = false;
        btnReject.Enabled = false;
    }


    /// <summary>
    /// Reloads the form status
    /// </summary>
    protected void SetupForm()
    {
        // Check modify permissions
        if (CMSContext.CurrentUser.IsAuthorizedPerDocument(Node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
        {
            DisableForm(false);
            btnArchive.Visible = false;
            lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), Node.NodeAliasPath);
        }
        else
        {
            pnlForm.Enabled = true;
            pnlSendMail.Visible = SendWorkflowEmails;

            btnApprove.Visible = true;
            btnApprove.Enabled = true;

            btnReject.Visible = false;
            btnReject.Enabled = true;

            btnArchive.Visible = false;
            if (currentStepId > 0)
            {
                // Setup the form
                WorkflowStepInfo si = WorkflowStepInfoProvider.GetWorkflowStepInfo(currentStepId);
                if (si != null)
                {
                    switch (si.StepName.ToLower())
                    {
                        case "published":
                            btnApprove.Visible = false;
                            btnApprove.Enabled = false;

                            btnArchive.Visible = true;
                            btnReject.Visible = (Node.DocumentCheckedOutVersionHistoryID > 0);
                            break;

                        case "archived":
                            btnReject.Visible = false;
                            btnReject.Enabled = false;

                            btnArchive.Visible = false;
                            btnArchive.Enabled = false;

                            pnlSendMail.Visible = false;
                            DisableForm(false);
                            break;

                        case "edit":
                            btnReject.Visible = false;
                            btnReject.Enabled = false;
                            break;

                        default:
                            btnReject.Visible = true;
                            if (!WorkflowManager.CanUserApprove(Node, CMSContext.CurrentUser))
                            {
                                btnArchive.Visible = false;
                                btnArchive.Enabled = false;
                                DisableForm(false);
                            }
                            break;
                    }
                }

                // If user cannot approve then cannot also reject
                if (!WorkflowManager.CanUserApprove(Node, CMSContext.CurrentUser))
                {
                    btnApprove.Visible = false;
                    btnApprove.Enabled = false;

                    btnReject.Visible = false;
                    btnReject.Enabled = false;

                    btnArchive.Visible = false;
                    btnArchive.Enabled = false;

                    pnlSendMail.Visible = false;
                    lblSendMail.Visible = false;
                    txtComment.Enabled = false;
                }
            }
        }

        // Check if document isn't checked out
        if (Node != null)
        {
            if (Node.GetValue("DocumentCheckedOutByUserID") != null)
            {
                lblInfo.Text = ResHelper.GetString("WorfklowProperties.DocumentIsCheckedOut");
                DisableForm(false);
            }
        }

        // If no workflow scope set for node, hide the data  
        if (WorkflowInfo == null)
        {
            lblInfo.Text = ResHelper.GetString("WorfklowProperties.ScopeNotSet");
            DisableForm(false);
            pnlWorkflow.Visible = false;
        }

        if (btnApprove.Enabled || btnReject.Enabled || btnArchive.Enabled)
        {
            txtComment.Enabled = true;
            pnlSendMail.Visible = true;
        }
    }

    #endregion


    #region "Button handling"

    /// <summary>
    /// Approve event handler
    /// </summary>
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        // Get original step
        WorkflowStepInfo originalStep = WorkflowManager.GetStepInfo(Node);

        // Approve document
        WorkflowStepInfo nextStep = WorkflowManager.MoveToNextStep(Node, txtComment.Text);

        // Send workflow e-mails
        if (chkSendMail.Checked && SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSSendWorkflowEmails"))
        {
            if ((nextStep == null) || (nextStep.StepName.ToLower() == "published"))
            {
                // Publish e-mails
                WorkflowManager.SendWorkflowEmails(Node, CMSContext.CurrentUser, originalStep, nextStep, WorkflowActionEnum.Published, txtComment.Text);
            }
            else
            {
                // Approve e-mails
                WorkflowManager.SendWorkflowEmails(Node, CMSContext.CurrentUser, originalStep, nextStep, WorkflowActionEnum.Approved, txtComment.Text);
            }
        }

        // If no next step (workflow cancelled), hide the form
        if (nextStep == null)
        {
            lblInfo.Text = ResHelper.GetString("WorfklowProperties.WorkflowFinished");
            DisableForm(false);
            pnlWorkflow.Visible = false;
            return;
        }
        else
        {
            currentStepId = nextStep.StepID;
        }

        txtComment.Text = string.Empty;

        ReloadData();
    }


    /// <summary>
    /// Reject event handler
    /// </summary>
    protected void btnReject_Click(object sender, EventArgs e)
    {
        // Get original step
        WorkflowStepInfo originalStep = WorkflowManager.GetStepInfo(Node);

        // Reject document
        WorkflowStepInfo previousStep = WorkflowManager.MoveToPreviousStep(Node, txtComment.Text);
        currentStepId = previousStep.StepID;

        // Send workflow e-mails
        if (chkSendMail.Checked && SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSSendWorkflowEmails"))
        {
            WorkflowManager.SendWorkflowEmails(Node, CMSContext.CurrentUser, originalStep, previousStep, WorkflowActionEnum.Rejected, txtComment.Text);
        }

        txtComment.Text = string.Empty;

        ReloadData();
    }


    /// <summary>
    /// Archive event handler
    /// </summary>
    protected void btnArchive_Click(object sender, EventArgs e)
    {
        // Get original step
        WorkflowStepInfo originalStep = WorkflowManager.GetStepInfo(Node);

        // Archive document
        WorkflowStepInfo nextStep = WorkflowManager.ArchiveDocument(Node, txtComment.Text);
        currentStepId = nextStep.StepID;

        // Send workflow e-mails
        if (chkSendMail.Checked && SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSSendWorkflowEmails"))
        {
            WorkflowManager.SendWorkflowEmails(Node, CMSContext.CurrentUser, originalStep, nextStep, WorkflowActionEnum.Archived, txtComment.Text);
        }

        ltlScript.Text += ScriptHelper.GetScript("RefreshTree(" + Node.NodeParentID + ", " + Node.NodeID + ");");
        txtComment.Text = string.Empty;

        ReloadData();
    }

    #endregion
}
