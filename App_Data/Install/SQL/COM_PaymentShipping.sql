CREATE TABLE [COM_PaymentShipping] (
		[PaymentOptionID]      int NOT NULL,
		[ShippingOptionID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_PaymentShipping]
	ADD
	CONSTRAINT [PK_COM_PaymentShipping]
	PRIMARY KEY
	([PaymentOptionID], [ShippingOptionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_PaymentShipping]') IS NOT NULL
	AND OBJECT_ID(N'[COM_PaymentOption]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_PaymentShipping_PaymentOptionID_COM_PaymentOption]') IS NULL
BEGIN
		ALTER TABLE [COM_PaymentShipping]
			ADD CONSTRAINT [FK_COM_PaymentShipping_PaymentOptionID_COM_PaymentOption]
			FOREIGN KEY ([PaymentOptionID]) REFERENCES [COM_PaymentOption] ([PaymentOptionID])
END
IF OBJECT_ID(N'[COM_PaymentShipping]') IS NOT NULL
	AND OBJECT_ID(N'[COM_ShippingOption]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_PaymentShipping_ShippingOptionID_COM_ShippingOption]') IS NULL
BEGIN
		ALTER TABLE [COM_PaymentShipping]
			ADD CONSTRAINT [FK_COM_PaymentShipping_ShippingOptionID_COM_ShippingOption]
			FOREIGN KEY ([ShippingOptionID]) REFERENCES [COM_ShippingOption] ([ShippingOptionID])
END
