﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/AegeanPower/PasswdRetrieve.ascx.cs"
    Inherits="CMSWebParts_AegeanPower_PasswdRetrieve" %>

<style type="text/css">
.passwdret-rieval{width:500px;}
.passwdret-rieval .label{float:left;width:450px;line-height:22px;font-family:Tahoma;font-size:11px;margin-bottom:10px;}
.passwdret-rieval .text{float:left;width:}
.text .button {vertical-align:middle;margin-top:expression(0);}
.text .TextAreaBox {vertical-align:middle;}
.button
{
    background:transparent url(/App_Themes/AegeanPower/Images/submitbutton.png) no-repeat;
    width:102px;
    height:20px;
    border:0px;
    cursor:hand;
    border-top-width:0px;
    border-bottom-width:0px;
    color:White;
    font-family:Tahoma;
    font-size:14px;
    vertical-align:text-top;
}
</style>

<asp:Panel ID="Panel1" runat="server">
    <div class="logon-form" style="width:490px;padding-top:0px;font-size:12px;min-height:130px">
        <div style="padding:15px 0px 10px 15px">
            <asp:Panel ID="pnlPasswdRetrieval" runat="server" DefaultButton="btnPasswdRetrieval">
                <div class="passwdret-rieval">
                    <div class="label">
                        <asp:Label ID="lblPasswdRetrieval" runat="server" EnableViewState="false" AssociatedControlID="txtPasswordRetrieval"/>
                    </div>
                    <div class="text" id="EmailDiv" runat="server">
                        <asp:TextBox CssClass="TextAreaBox" ID="txtPasswordRetrieval" runat="server" />
                        <cms:CMSButton ID="btnPasswdRetrieval" runat="server" CssClass="button" ValidationGroup="PsswdRetrieval" EnableViewState="false"/><br />
                        <asp:RequiredFieldValidator ID="rqValue" runat="server"  ControlToValidate="txtPasswordRetrieval" ValidationGroup="PsswdRetrieval" EnableViewState="false" />
                    </div>
                    <div class="Clear"></div>
                </div>
            <asp:Label ID="lblResult" runat="server" Visible="false" EnableViewState="false" />
            </asp:Panel>
        </div>
    </div> 
</asp:Panel>
<asp:HiddenField runat="server" ID="hdnPasswDisplayed" />
