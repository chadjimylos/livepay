CREATE TABLE [Newsletter_NewsletterIssue] (
		[IssueID]                          int NOT NULL IDENTITY(1, 1),
		[IssueSubject]                     nvarchar(450) NOT NULL,
		[IssueText]                        nvarchar(max) NOT NULL,
		[IssueUnsubscribed]                int NOT NULL,
		[IssueNewsletterID]                int NOT NULL,
		[IssueTemplateID]                  int NULL,
		[IssueSentEmails]                  int NOT NULL,
		[IssueMailoutTime]                 datetime NULL,
		[IssueShowInNewsletterArchive]     bit NULL,
		[IssueGUID]                        uniqueidentifier NOT NULL,
		[IssueLastModified]                datetime NOT NULL,
		[IssueSiteID]                      int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Newsletter_NewsletterIssue]
	ADD
	CONSTRAINT [PK_Newsletter_NewsletterIssue]
	PRIMARY KEY
	([IssueID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Newsletter_NewsletterIssue]
	ADD
	CONSTRAINT [DEFAULT_Newsletter_NewsletterIssue_IssueSiteID]
	DEFAULT ((0)) FOR [IssueSiteID]
ALTER TABLE [Newsletter_NewsletterIssue]
	ADD
	CONSTRAINT [DEFAULT_Newsletter_NewsletterIssue_IssueSubject]
	DEFAULT ('') FOR [IssueSubject]
IF OBJECT_ID(N'[Newsletter_NewsletterIssue]') IS NOT NULL
	AND OBJECT_ID(N'[Newsletter_Newsletter]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_NewsletterIssue_IssueNewsletterID_Newsletter_Newsletter]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_NewsletterIssue]
			ADD CONSTRAINT [FK_Newsletter_NewsletterIssue_IssueNewsletterID_Newsletter_Newsletter]
			FOREIGN KEY ([IssueNewsletterID]) REFERENCES [Newsletter_Newsletter] ([NewsletterID])
END
IF OBJECT_ID(N'[Newsletter_NewsletterIssue]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_NewsletterIssue_IssueSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_NewsletterIssue]
			ADD CONSTRAINT [FK_Newsletter_NewsletterIssue_IssueSiteID_CMS_Site]
			FOREIGN KEY ([IssueSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[Newsletter_NewsletterIssue]') IS NOT NULL
	AND OBJECT_ID(N'[Newsletter_EmailTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_NewsletterIssue_IssueTemplateID_Newsletter_EmailTemplate]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_NewsletterIssue]
			ADD CONSTRAINT [FK_Newsletter_NewsletterIssue_IssueTemplateID_Newsletter_EmailTemplate]
			FOREIGN KEY ([IssueTemplateID]) REFERENCES [Newsletter_EmailTemplate] ([TemplateID])
END
CREATE INDEX [IX_Newsletter_NewsletterIssue_IssueNewsletterID]
	ON [Newsletter_NewsletterIssue] ([IssueNewsletterID])
	ON [PRIMARY]
CREATE INDEX [IX_Newsletter_NewsletterIssue_IssueSiteID]
	ON [Newsletter_NewsletterIssue] ([IssueSiteID])
	ON [PRIMARY]
CREATE INDEX [IX_Newsletter_NewsletterIssue_IssueTemplateID]
	ON [Newsletter_NewsletterIssue] ([IssueTemplateID])
	ON [PRIMARY]
CREATE INDEX [IX_Newslettes_NewsletterIssue_IssueShowInNewsletterArchive]
	ON [Newsletter_NewsletterIssue] ([IssueShowInNewsletterArchive])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
