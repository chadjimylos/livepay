<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Report_new.aspx.cs" Inherits="CMSModules_Reporting_Tools_Report_new"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblReportDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtReportDisplayName" runat="server" CssClass="TextBoxField" MaxLength="440"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvReportDisplayName" runat="server" ErrorMessage=""
                    ControlToValidate="txtReportDisplayName" EnableViewState="false"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblReportName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtReportName" runat="server" CssClass="TextBoxField" MaxLength="100"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvReportName" runat="server" ErrorMessage="" ControlToValidate="txtReportName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblReportAccess" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox ID="chkReportAccess" runat="server" Checked="True" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
