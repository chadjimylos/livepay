<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Group_List.aspx.cs" Inherits="CMSModules_Forums_Tools_Groups_Group_List"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="Default" %>
<%@ Register Src="~/CMSModules/Forums/Controls/Forums/ForumGroupList.ascx" TagName="ForumGroupList" TagPrefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:ForumGroupList ID="forumGroupList" runat="server" />
</asp:Content>