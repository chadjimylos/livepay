using System;
using System.Data;
using System.Collections;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.TreeEngine;
using CMS.WorkflowEngine;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_PublishArchive : CMSContentPage
{
    #region "Private variables & enums"

    private readonly List<int> nodeIds = new List<int>();
    private static readonly Hashtable mErrors = new Hashtable();
    private readonly Dictionary<int, string> list = new Dictionary<int, string>();

    private int cancelNodeId = 0;
    private CurrentUserInfo currentUser = null;
    private SiteInfo currentSite = null;
    private string currentCulture = CultureHelper.DefaultUICulture;

    private Action mCurrentAction = Action.None;

    protected enum Action
    {
        None = 0,
        Publish = 1,
        Archive = 2
    }

    #endregion


    #region "Properties"

    /// <summary>
    /// Current log context
    /// </summary>
    public LogContext CurrentLog
    {
        get
        {
            return EnsureLog();
        }
    }


    /// <summary>
    /// Current Error
    /// </summary>
    public string CurrentError
    {
        get
        {
            return ValidationHelper.GetString(mErrors["PublishError_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mErrors["PublishError_" + ctlAsync.ProcessGUID] = value;
        }
    }

    /// <summary>
    /// Determines current action
    /// </summary>
    private Action CurrentAction
    {
        get
        {
            if (mCurrentAction == Action.None)
            {
                string actionString = QueryHelper.GetString("action", Action.None.ToString());
                mCurrentAction = (Action)Enum.Parse(typeof(Action), actionString);
            }

            return mCurrentAction;
        }
    }


    /// <summary>
    /// Where condition used for multiple actions
    /// </summary>
    private static string WhereCondition
    {
        get
        {
            return HttpUtility.UrlDecode(QueryHelper.GetString("where", string.Empty)).Replace("%26", "&").Replace("%23", "#");
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize current user
        currentUser = CMSContext.CurrentUser;
        // Check permissions
        if (!currentUser.IsGlobalAdministrator && !currentUser.IsAuthorizedPerResource("CMS.Content", "manageworkflow"))
        {
            RedirectToAccessDenied("CMS.Content", "manageworkflow");
        }
        // Set current UI culture
        currentCulture = CultureHelper.PreferredUICulture;
        // Initialize current site
        currentSite = CMSContext.CurrentSite;

        // Initialize events
        ctlAsync.OnFinished += ctlAsync_OnFinished;
        ctlAsync.OnError += ctlAsync_OnError;
        ctlAsync.OnRequestLog += ctlAsync_OnRequestLog;
        ctlAsync.OnCancel += ctlAsync_OnCancel;

        if (!IsCallback)
        {
            DataSet allDocs = null;
            TreeProvider tree = new TreeProvider(currentUser);

            // Current Node ID to delete
            string parentAliasPath = QueryHelper.GetString("parentaliaspath", string.Empty);
            if (string.IsNullOrEmpty(parentAliasPath))
            {
                // Get IDs of nodes
                string nodeIdsString = QueryHelper.GetString("nodeids", string.Empty);
                string[] nodeIdsArr = nodeIdsString.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string nodeId in nodeIdsArr)
                {
                    int id = ValidationHelper.GetInteger(nodeId, 0);
                    if (id != 0)
                    {
                        nodeIds.Add(id);
                    }
                }
            }
            else
            {
                string where = "ClassName <> 'CMS.Root'";
                if (!string.IsNullOrEmpty(WhereCondition))
                {
                    where = SqlHelperClass.AddWhereCondition(where, WhereCondition);
                }
                string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeParentID, DocumentName,DocumentCheckedOutByUserID");
                allDocs = tree.SelectNodes(currentSite.SiteName, parentAliasPath.TrimEnd('/') + "/%", TreeProvider.ALL_CULTURES, true, null, where, "DocumentName", 1, false, 0, columns);

                if (!DataHelper.DataSourceIsEmpty(allDocs))
                {
                    foreach (DataRow row in allDocs.Tables[0].Rows)
                    {
                        nodeIds.Add(ValidationHelper.GetInteger(row["NodeID"], 0));
                    }
                }
            }

            // Initialize strings based on current action
            switch (CurrentAction)
            {
                case Action.Archive:
                    lblQuestion.ResourceString = "content.archivequestion";
                    chkAllCultures.ResourceString = "content.archiveallcultures";
                    chkUnderlying.ResourceString = "content.archiveunderlying";

                    // Setup title of log
                    titleElemAsync.TitleText = ResHelper.GetString("content.archivingdocuments");
                    titleElemAsync.TitleImage = GetImageUrl("CMSModules/CMS_Content/Dialogs/archive.png");

                    // Setup page title text and image
                    CurrentMaster.Title.TitleText = ResHelper.GetString("Content.ArchiveTitle");
                    CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Content/Dialogs/archive.png");
                    break;

                case Action.Publish:
                    lblQuestion.ResourceString = "content.publishquestion";
                    chkAllCultures.ResourceString = "content.publishallcultures";
                    chkUnderlying.ResourceString = "content.publishunderlying";

                    // Setup title of log
                    titleElemAsync.TitleText = ResHelper.GetString("content.publishingdocuments");
                    titleElemAsync.TitleImage = GetImageUrl("CMSModules/CMS_Content/Dialogs/publish.png");

                    // Setup page title text and image
                    CurrentMaster.Title.TitleText = ResHelper.GetString("Content.PublishTitle");
                    CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Content/Dialogs/publish.png");
                    break;
            }
            if (nodeIds.Count == 0)
            {
                // Hide if no node was specified
                pnlContent.Visible = false;
                return;
            }

            btnCancel.Attributes.Add("onclick", "CancelAction(); return false;");

            // Set visiblity of undo checkout checkbox
            bool userCheckinCheckout = VersionManager.UseCheckInCheckOut(currentSite.SiteName);
            plcUndoCheckOut.Visible = userCheckinCheckout;

            // Register the dialog script
            ScriptHelper.RegisterDialogScript(this);

            // Set visibility of panels
            pnlContent.Visible = true;
            pnlLog.Visible = false;

            // Set all cultures checkbox
            DataSet culturesDS = CultureInfoProvider.GetSiteCultures(currentSite.SiteName);
            if ((DataHelper.DataSourceIsEmpty(culturesDS)) || (culturesDS.Tables[0].Rows.Count <= 1))
            {
                chkAllCultures.Checked = true;
                plcAllCultures.Visible = false;
            }

            if (nodeIds.Count > 0)
            {
                pnlDocList.Visible = true;

                // Create where condition
                string where = SqlHelperClass.GetWhereCondition("NodeID", nodeIds.ToArray());
                string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeParentID, DocumentName,DocumentCheckedOutByUserID");

                // Select nodes
                DataSet ds = allDocs ?? tree.SelectNodes(currentSite.SiteName, "/%", TreeProvider.ALL_CULTURES, true, null, where, "DocumentName", TreeProvider.ALL_LEVELS, false, 0, columns);

                // Enumerate selected documents
                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    cancelNodeId = ValidationHelper.GetInteger(DataHelper.GetDataRowValue(ds.Tables[0].Rows[0], "NodeParentID"), 0);

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        AddToList(dr, userCheckinCheckout);
                    }

                    // Display enumeration of documents
                    foreach (KeyValuePair<int, string> line in list)
                    {
                        lblDocuments.Text += line.Value;
                    }
                }
            }
        }
    }


    protected override void OnPreInit(EventArgs e)
    {
        ((Panel)CurrentMaster.PanelBody.FindControl("pnlContent")).CssClass = string.Empty;
        base.OnPreInit(e);
    }


    protected override void OnPreRender(EventArgs e)
    {
        lblError.Visible = (lblError.Text != string.Empty);
        base.OnPreRender(e);
    }

    #endregion


    #region "Button actions"

    protected void btnNo_Click(object sender, EventArgs e)
    {
        // Go back to listing
        AddScript("DisplayDocument(" + cancelNodeId + ");");
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        pnlLog.Visible = true;
        pnlContent.Visible = false;

        EnsureLog();
        CurrentError = string.Empty;

        ctlAsync.Parameter = currentUser.PreferredCultureCode + ";" + CMSContext.CurrentSiteName;
        switch (CurrentAction)
        {
            case Action.Publish:
                ctlAsync.RunAsync(PublishAll, WindowsIdentity.GetCurrent());
                break;

            case Action.Archive:
                ctlAsync.RunAsync(ArchiveAll, WindowsIdentity.GetCurrent());
                break;
        }
    }

    #endregion


    #region "Async methods"

    /// <summary>
    /// Archives document(s)
    /// </summary>
    private void ArchiveAll(object parameter)
    {
        if (parameter == null)
        {
            return;
        }

        TreeProvider tree = new TreeProvider(currentUser);
        tree.AllowAsyncActions = false;

        try
        {
            // Begin log
            AddLog(ResHelper.GetString("content.archivingdocuments", currentCulture));

            string[] parameters = ((string)parameter).Split(';');

            string siteName = parameters[1];

            // Get identifiers
            int[] workNodes = nodeIds.ToArray();

            // Prepare the where condition
            string where = SqlHelperClass.GetWhereCondition("NodeID", workNodes);
            string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeAliasPath, ClassName, DocumentCulture");

            // Get cultures
            string cultureCode = chkAllCultures.Checked ? TreeProvider.ALL_CULTURES : parameters[0];

            // Get the documents
            DataSet documents = tree.SelectNodes(siteName, "/%", cultureCode, false, null, where, "NodeAliasPath DESC", TreeProvider.ALL_LEVELS, false, 0, columns);

            // Create instance of workflow manager class
            WorkflowManager wm = new WorkflowManager(tree);

            if (!DataHelper.DataSourceIsEmpty(documents))
            {
                foreach (DataRow nodeRow in documents.Tables[0].Rows)
                {
                    // Get the current document
                    string className = nodeRow["ClassName"].ToString();
                    string aliasPath = nodeRow["NodeAliasPath"].ToString();
                    string docCulture = nodeRow["DocumentCulture"].ToString();
                    TreeNode node = DocumentHelper.GetDocument(siteName, aliasPath, docCulture, false, className, null, null, -1, false, null, tree);

                    if (node == null)
                    {
                        AddLog(string.Format(ResHelper.GetString("ContentRequest.DocumentNoLongerExists", currentCulture), HTMLHelper.HTMLEncode(aliasPath)));
                        continue;
                    }

                    if (!UndoPossibleCheckOut(tree, node))
                    {
                        ctlAsync.RaiseError(null, null);
                        return;
                    }

                    try
                    {
                        // Try to get workflow scope
                        wm.GetStepInfo(node);

                        // Add log record
                        AddLog(HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")"));

                        // Archive document
                        wm.ArchiveDocument(node, string.Empty);
                    }
                    catch
                    {
                        AddLog(string.Format(ResHelper.GetString("content.archivenowf"), HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")")));
                        continue;
                    }

                    // Process underlying documents
                    if (chkUnderlying.Checked && (node.NodeChildNodesCount > 0))
                    {
                        ArchiveSubDocuments(node, tree, wm, cultureCode, siteName);
                    }
                }
            }
            else
            {
                ctlAsync.RaiseError(null, null);
                AddError(ResHelper.GetString("content.nothingtoarchive", currentCulture));
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                // When canceled
                AddLog(ResHelper.GetString("content.archivecanceled", currentCulture));
                ctlAsync.RaiseError(null, null);
            }
            else
            {
                // Log error
                LogExceptionToEventLog(ResHelper.GetString("content.archivefailed", currentCulture), tree.Connection, ex);
            }
        }
        catch (Exception ex)
        {
            // Log error
            LogExceptionToEventLog(ResHelper.GetString("content.archivefailed", currentCulture), tree.Connection, ex);
            ctlAsync.RaiseError(null, null);
        }
    }


    /// <summary>
    /// Archives sub documents of given node
    /// </summary>
    /// <param name="parentNode">Parent node</param>
    /// <param name="tree">Tree provider</param>
    /// <param name="wm">Workflow manager</param>
    /// <param name="cultureCode">Culture code</param>
    /// <param name="siteName">Site name</param>
    private void ArchiveSubDocuments(TreeNode parentNode, TreeProvider tree, WorkflowManager wm, string cultureCode, string siteName)
    {
        string where = "NodeParentID=" + parentNode.NodeID;
        string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeAliasPath, ClassName, DocumentCulture");

        // Get subdocuments
        DataSet subDocuments = tree.SelectNodes(siteName, "/%", cultureCode, false, null, where, null, TreeProvider.ALL_LEVELS, false, 0, columns);
        if (!DataHelper.DataSourceIsEmpty(subDocuments))
        {
            foreach (DataRow nodeRow in subDocuments.Tables[0].Rows)
            {
                // Get the current document
                string className = nodeRow["ClassName"].ToString();
                string aliasPath = nodeRow["NodeAliasPath"].ToString();
                string docCulture = nodeRow["DocumentCulture"].ToString();
                TreeNode node = DocumentHelper.GetDocument(siteName, aliasPath, docCulture, false, className, null, null, TreeProvider.ALL_LEVELS, false, null, tree);

                if (node == null)
                {
                    AddLog(string.Format(ResHelper.GetString("ContentRequest.DocumentNoLongerExists", currentCulture), HTMLHelper.HTMLEncode(aliasPath)));
                    continue;
                }

                if (!UndoPossibleCheckOut(tree, node))
                {
                    ctlAsync.RaiseError(null, null);
                    return;
                }

                try
                {
                    // Try to get workflow scope
                    wm.GetStepInfo(node);

                    // Add log record
                    AddLog(HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")"));

                    // Archive document
                    wm.ArchiveDocument(node, string.Empty);
                }
                catch
                {
                    AddLog(string.Format(ResHelper.GetString("content.archivenowf"), HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")")));
                    continue;
                }
            }
        }
    }


    /// <summary>
    /// Publishes document(s)
    /// </summary>
    private void PublishAll(object parameter)
    {
        if (parameter == null)
        {
            return;
        }

        TreeProvider tree = new TreeProvider(currentUser);
        tree.AllowAsyncActions = false;

        try
        {
            // Begin log
            AddLog(ResHelper.GetString("content.publishingdocuments", currentCulture));

            string[] parameters = ((string)parameter).Split(';');

            string siteName = parameters[1];

            // Get identifiers
            int[] workNodes = nodeIds.ToArray();

            // Prepare the where condition
            string where = SqlHelperClass.GetWhereCondition("NodeID", workNodes);
            string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeAliasPath, ClassName, DocumentCulture");

            // Get cultures
            string cultureCode = chkAllCultures.Checked ? TreeProvider.ALL_CULTURES : parameters[0];

            // Get the documents
            DataSet documents = tree.SelectNodes(siteName, "/%", cultureCode, false, null, where, "NodeAliasPath DESC", TreeProvider.ALL_LEVELS, false, 0, columns);

            // Create instance of workflow manager class
            WorkflowManager wm = new WorkflowManager(tree);

            if (!DataHelper.DataSourceIsEmpty(documents))
            {
                foreach (DataRow nodeRow in documents.Tables[0].Rows)
                {
                    // Get the current document
                    string className = nodeRow["ClassName"].ToString();
                    string aliasPath = nodeRow["NodeAliasPath"].ToString();
                    string docCulture = nodeRow["DocumentCulture"].ToString();
                    TreeNode node = DocumentHelper.GetDocument(siteName, aliasPath, docCulture, false, className, null, null, TreeProvider.ALL_LEVELS, false, null, tree);

                    if (node == null)
                    {
                        AddLog(string.Format(ResHelper.GetString("ContentRequest.DocumentNoLongerExists", currentCulture), HTMLHelper.HTMLEncode(aliasPath)));
                        continue;
                    }

                    // Perform undo checkout
                    if (!UndoPossibleCheckOut(tree, node))
                    {
                        ctlAsync.RaiseError(null, null);
                        return;
                    }

                    WorkflowStepInfo currentStep = null;
                    try
                    {
                        // Try to get workflow scope
                        currentStep = wm.GetStepInfo(node);
                    }
                    catch
                    {
                        AddLog(string.Format(ResHelper.GetString("content.publishnowf"), HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")")));
                        continue;
                    }

                    if (currentStep != null)
                    {
                        // Publish document
                        if (!Publish(node, wm, currentStep))
                        {
                            // Add log record
                            AddLog(HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")"));
                        }
                        else
                        {
                            AddLog(string.Format(ResHelper.GetString("content.publishedalready"), HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")")));
                        }
                    }

                    // Process underlying documents
                    if (chkUnderlying.Checked && (node.NodeChildNodesCount > 0))
                    {
                        PublishSubDocuments(node, tree, wm, cultureCode, siteName);
                    }
                }
            }
            else
            {
                AddError(ResHelper.GetString("content.nothingtopublish", currentCulture));
                ctlAsync.RaiseError(null, null);
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                // When canceled
                AddLog(ResHelper.GetString("content.publishcanceled", currentCulture));
                ctlAsync.RaiseError(null, null);
            }
            else
            {
                // Log error
                LogExceptionToEventLog(ResHelper.GetString("content.publishfailed", currentCulture), tree.Connection, ex);
            }
        }
        catch (Exception ex)
        {
            // Log error
            LogExceptionToEventLog(ResHelper.GetString("content.publishfailed", currentCulture), tree.Connection, ex);
            ctlAsync.RaiseError(null, null);
        }
    }


    /// <summary>
    /// Publishes sub documents of given node
    /// </summary>
    /// <param name="parentNode">Parent node</param>
    /// <param name="tree">Tree provider</param>
    /// <param name="wm">Workflow manager</param>
    /// <param name="cultureCode">Culture code</param>
    /// <param name="siteName">Site name</param>
    private void PublishSubDocuments(TreeNode parentNode, TreeProvider tree, WorkflowManager wm, string cultureCode, string siteName)
    {
        string where = "NodeParentID=" + parentNode.NodeID;
        string columns = SqlHelperClass.MergeColumns(TreeProvider.SELECTNODES_REQUIRED_COLUMNS, "NodeAliasPath, ClassName, DocumentCulture");

        // Get sub documents
        DataSet subDocuments = tree.SelectNodes(siteName, "/%", cultureCode, false, null, where, null, TreeProvider.ALL_LEVELS, false, 0, columns);
        if (!DataHelper.DataSourceIsEmpty(subDocuments))
        {
            foreach (DataRow nodeRow in subDocuments.Tables[0].Rows)
            {
                // Get the current document
                string className = nodeRow["ClassName"].ToString();
                string aliasPath = nodeRow["NodeAliasPath"].ToString();
                string docCulture = nodeRow["DocumentCulture"].ToString();
                TreeNode node = DocumentHelper.GetDocument(siteName, aliasPath, docCulture, false, className, null, null, TreeProvider.ALL_LEVELS, false, null, tree);

                if (node == null)
                {
                    AddLog(string.Format(ResHelper.GetString("ContentRequest.DocumentNoLongerExists", currentCulture), HTMLHelper.HTMLEncode(aliasPath)));
                    continue;
                }

                if (!UndoPossibleCheckOut(tree, node))
                {
                    ctlAsync.RaiseError(null, null);
                    return;
                }

                WorkflowStepInfo currentStep = null;
                try
                {
                    // Try to get workflow scope
                    currentStep = wm.GetStepInfo(node);
                }
                catch
                {
                    AddLog(string.Format(ResHelper.GetString("content.publishnowf"), HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")")));
                    continue;
                }

                if (currentStep != null)
                {
                    // Publish document
                    if (!Publish(node, wm, currentStep))
                    {
                        // Add log record
                        AddLog(HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")"));
                    }
                    else
                    {
                        AddLog(string.Format(ResHelper.GetString("content.publishedalready"), HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")")));
                    }
                }
            }
        }
    }


    /// <summary>
    /// Publishes document
    /// </summary>
    /// <param name="node">Node to publish</param>
    /// <param name="wm">Workflow manager</param>
    /// <param name="currentStep">Current workflow step</param>
    /// <returns>Whether node is already published</returns>
    private static bool Publish(TreeNode node, WorkflowManager wm, WorkflowStepInfo currentStep)
    {
        bool toReturn = true;
        if (currentStep != null)
        {
            string currentStepName = currentStep.StepName.ToLower();
            // For archive step start new version
            if (currentStepName == "archived")
            {
                VersionManager vm = new VersionManager(node.TreeProvider);
                currentStep = vm.CheckOut(node);
                vm.CheckIn(node, null, null);
            }

            // Approve until the step is publish
            while (currentStep.StepName.ToLower() != "published")
            {
                currentStep = wm.MoveToNextStep(node, string.Empty);
                toReturn = false;
            }
        }
        return toReturn;
    }


    /// <summary>
    /// Undoes checkout for given node
    /// </summary>
    /// <param name="tree">Tree provider</param>
    /// <param name="node">Node to undo checkout</param>
    /// <returns>FALSE when document is checked out and checkbox for undoing checkout is not checked</returns>
    private bool UndoPossibleCheckOut(TreeProvider tree, TreeNode node)
    {
        if (VersionManager.UseCheckInCheckOut(currentSite.SiteName))
        {
            if (ValidationHelper.GetInteger(node.GetValue("DocumentCheckedOutByUserID"), 0) != 0)
            {
                if (chkUndoCheckOut.Checked)
                {
                    VersionManager vm = new VersionManager(tree);
                    vm.UndoCheckOut(node);
                }
                else
                {
                    AddError(string.Format(ResHelper.GetString("content.checkedoutdocument"), HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")")));
                    return false;
                }
            }
        }
        return true;
    }

    #endregion


    #region "Help methods"

    /// <summary>
    /// When exception occures, log it to event log
    /// </summary>
    /// <param name="messageTitle">Title message</param>
    /// <param name="connection">Connection to use</param>
    /// <param name="ex">Exception to log</param>
    private void LogExceptionToEventLog(string messageTitle, GeneralConnection connection, Exception ex)
    {
        AddError(messageTitle + ": " + ex.Message);
        EventLogProvider log = new EventLogProvider(connection);
        log.LogEvent("E", DateTime.Now, "Content", "PUBLISHDOC", currentUser.UserID,
                     currentUser.UserName, 0, null,
                     HTTPHelper.GetUserHostAddress(), EventLogProvider.GetExceptionLogMessage(ex),
                     currentSite.SiteID, HTTPHelper.GetAbsoluteUri());
    }


    /// <summary>
    /// Adds the script to the output request window
    /// </summary>
    /// <param name="script">Script to add</param>
    public override void AddScript(string script)
    {
        ltlScript.Text += ScriptHelper.GetScript(script);
    }


    /// <summary>
    /// Adds document to list
    /// </summary>
    /// <param name="dr">Data row with document</param>
    /// <param name="useCheckinCheckout">Whether check-in and check-out is used</param>
    private void AddToList(DataRow dr, bool useCheckinCheckout)
    {
        int nodeId = ValidationHelper.GetInteger(dr["NodeID"], 0);
        int linkedNodeId = ValidationHelper.GetInteger(dr["NodeLinkedNodeID"], 0);
        int documentCheckedOutByUserID = ValidationHelper.GetInteger(dr["DocumentCheckedOutByUserID"], 0);
        string name = HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["DocumentName"], string.Empty));
        if ((documentCheckedOutByUserID != 0) && useCheckinCheckout)
        {
            name += " " + UIHelper.GetDocumentMarkImage(Page, DocumentMarkEnum.CheckedOut);
        }
        if (linkedNodeId != 0)
        {
            name += " " + UIHelper.GetDocumentMarkImage(Page, DocumentMarkEnum.Link);
        }
        name += "<br />";
        list[nodeId] = name;
        // Add documents to list
        //if (list.ContainsKey(nodeId))
        //{
        //    // Update record if original document is present
        //    if (linkedNodeId == 0)
        //    {
        //        list[nodeId] = name;
        //    }
        //}
        //else
        //{
        //    // Original or linked document
        //    if (linkedNodeId == 0)
        //    {
        //        list[nodeId] = name;
        //    }
        //    else
        //    {
        //        list[linkedNodeId] = name;
        //    }
        //}
    }

    #endregion


    #region "Handling async thread"

    private void ctlAsync_OnCancel(object sender, EventArgs e)
    {
        if (ctlAsync.Status == AsyncWorkerStatusEnum.Running)
        {
            ctlAsync.Stop();
        }

        AddScript("var __pendingCallbacks = new Array();");
        RefreshTree();
        lblError.Text = CurrentError;
        CurrentLog.Close();
    }


    private void ctlAsync_OnRequestLog(object sender, EventArgs e)
    {
        ctlAsync.Log = CurrentLog.Log;
    }


    private void ctlAsync_OnError(object sender, EventArgs e)
    {
        RefreshTree();
        lblError.Text = CurrentError;
        CurrentLog.Close();
    }


    private void ctlAsync_OnFinished(object sender, EventArgs e)
    {
        lblError.Text = CurrentError;
        CurrentLog.Close();
        if (string.IsNullOrEmpty(CurrentError))
        {
            AddScript("DisplayDocument(" + cancelNodeId + ");");
        }
    }


    /// <summary>
    /// Refreshes content tree
    /// </summary>
    private void RefreshTree()
    {
        if (cancelNodeId != 0)
        {
            AddScript("RefreshTree(" + cancelNodeId + ");");
        }
    }


    /// <summary>
    /// Ensures the logging context
    /// </summary>
    protected LogContext EnsureLog()
    {
        LogContext log = LogContext.EnsureLog(ctlAsync.ProcessGUID);
        log.Reversed = true;
        log.LineSeparator = "<br />";
        return log;
    }


    /// <summary>
    /// Adds the log information
    /// </summary>
    /// <param name="newLog">New log information</param>
    protected void AddLog(string newLog)
    {
        EnsureLog();
        LogContext.AppendLine(newLog);
    }


    /// <summary>
    /// Adds the error to collection of errors
    /// </summary>
    /// <param name="error">Error message</param>
    protected void AddError(string error)
    {
        AddLog(error);
        CurrentError = (error + "<br />" + CurrentError);
    }

    #endregion
}
