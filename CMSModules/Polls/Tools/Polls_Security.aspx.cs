using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.Polls;
using CMS.UIControls;

public partial class CMSModules_Polls_Tools_Polls_Security : CMSPollsPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PollSecurity.ItemID = QueryHelper.GetInteger("pollid", 0);
        PollSecurity.IsLiveSite = false;
    }
}
