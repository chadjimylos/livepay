using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Reporting;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Reporting_Tools_Report_Header : CMSReportingPage
{
    protected int reportId = 0;
    protected int categoryId = 0;
    protected int saved = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        reportId = ValidationHelper.GetInteger(Request.QueryString["reportid"], 0);
        saved = ValidationHelper.GetInteger(Request.QueryString["saved"], 0);
        
        string currentReport = "";

        ReportInfo ri = ReportInfoProvider.GetReportInfo(reportId);

        categoryId = ValidationHelper.GetInteger(Request.QueryString["categoryId"],0);
        string reportListLink = "~/CMSModules/Reporting/Tools/Report_List.aspx";
        if (ri != null)
        {
            currentReport = ri.ReportDisplayName;
            reportListLink +="?categoryId="+categoryId;
        }

        this.InitializeMasterPage(currentReport, reportListLink);
        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }
    }


    /// <summary>
    /// Initilizes master page.
    /// </summary>
    protected void InitializeMasterPage(string currentReport, string reportListLink)
    {
        // Initialize title and help
        this.Title = "Report header";
        this.CurrentMaster.Title.HelpTopicName = "view_tab2";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // initializes page title control		
        string[,] tabs = new string[2, 3];
        tabs[0, 0] = ResHelper.GetString("Report_Header.ReportList");
        tabs[0, 1] = reportListLink;
        tabs[0, 2] = "categoryContent";
        tabs[1, 0] = currentReport;
        tabs[1, 1] = "";
        tabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = tabs;

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SetSavedTab",
            ScriptHelper.GetScript("function SetSavedTab() { selTabControlItem(3); SetHelpTopic('helpTopic', 'saved_reports_tab'); } \n"
        ));
    }


    /// <summary>
    /// Initializes user edit menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string[,] tabs = new string[4, 4];
        tabs[0, 0] = ResHelper.GetString("general.view");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'view_tab2');";
        tabs[0, 2] = "Report_View.aspx?reportid=" + reportId.ToString();
        tabs[1, 0] = ResHelper.GetString("general.general");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab7');";
        tabs[1, 2] = "Report_General.aspx?reportid=" + reportId.ToString();
        tabs[2, 0] = ResHelper.GetString("Report_Header.Parameters");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'parameters_tab');";
        tabs[2, 2] = "Report_Parameters.aspx?reportid=" + reportId.ToString();
        tabs[3, 0] = ResHelper.GetString("Report_Header.SavedReports");
        tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'saved_reports_tab');";
        tabs[3, 2] = "./SavedReports/SavedReports_List.aspx?reportid=" + reportId.ToString();
        
        this.CurrentMaster.Tabs.UrlTarget = "reportContent";
        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.SelectedTab = saved;
    }
}
