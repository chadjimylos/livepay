using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_MessageBoards_Tools_Boards_Header : CMSMessageBoardPage
{
    private int mGroupId = 0;

    protected override void OnPreInit(EventArgs e)
    {
        this.mGroupId = QueryHelper.GetInteger("groupid", 0);
        base.OnPreInit(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Intialize the control
        SetupControl();
    }


    #region "Private methods"

    /// <summary>
    /// Initializes the controls
    /// </summary>
    private void SetupControl()
    {
        // Set the page title when existing category is being edited
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("board.header.messageboards");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Board_Board/object.png");

        InitalizeMenu();
    }


    /// <summary>
    /// Initialize the tab control on the master page
    /// </summary>
    private void InitalizeMenu()
    {
        // Collect tabs data
        string[,] tabs = new string[2, 4];
        tabs[0, 0] = ResHelper.GetString("board.header.messages");
        tabs[0, 2] = "Messages/Message_List.aspx" + ((this.mGroupId > 0) ? "?groupid=" + this.mGroupId : "");

        tabs[1, 0] = ResHelper.GetString("board.header.boards");
        tabs[1, 2] = "Boards/Board_List.aspx" + ((this.mGroupId > 0) ? "?groupid=" + this.mGroupId : "");

        // Set the target iFrame
        this.CurrentMaster.Tabs.UrlTarget = "boardsContent";

        // Assign tabs data
        this.CurrentMaster.Tabs.Tabs = tabs;
    }

    #endregion
}
