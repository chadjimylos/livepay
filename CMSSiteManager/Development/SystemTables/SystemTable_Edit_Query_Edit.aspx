<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SystemTable_Edit_Query_Edit.aspx.cs"
    Inherits="CMSSiteManager_Development_SystemTables_SystemTable_Edit_Query_Edit"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="System tables - Edit query"
    Theme="Default"
    %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/QueryEdit.ascx" TagName="QueryEdit" TagPrefix="cms" %>
    
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:QueryEdit ID="queryEdit" Visible="true" runat="server" />
</asp:Content>