using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using System.Text.RegularExpressions;

public partial class CMSWebParts_LivePay_Profile_ChangePassword : CMSAbstractWebPart
{
    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether this webpart is displayed only when user is authenticated
    /// </summary>
    public bool ShowOnlyWhenAuthenticated
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("ShowOnlyWhenAuthenticated"), true);
        }
        set
        {
            SetValue("ShowOnlyWhenAuthenticated", value);
            Visible = (!value || CMSContext.CurrentUser.IsAuthenticated());
        }
    }


    /// <summary>
    /// Gets or sets the maximal new password length
    /// </summary>
    public int MaximalPasswordLength
    {
        get
        {
            return ValidationHelper.GetInteger(GetValue("MaximalPasswordLength"), 0);
        }
        set
        {
            SetValue("MaximalPasswordLength", value);
            txtNewPassword.MaxLength = value;
            txtConfirmPassword.MaxLength = value;
        }
    }

    /// <summary>
    /// Gets or sets the Existing Password
    /// </summary>
    public string TextErrorNewPassword
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("TextErrorNewPassword"), ResHelper.LocalizeString("{$=������� ������� ���������:|en-us=Existing Password :$}"));
        }
        set
        {
            this.SetValue("TextErrorNewPassword", value);
        }
    }


    /// <summary>
    /// Gets or sets the Existing Password
    /// </summary>
    public string TextErrorOldPassword
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("TextErrorOldPassword"), ResHelper.LocalizeString("{$=������� ������� ���������:|en-us=Existing Password :$}"));
        }
        set
        {
            this.SetValue("TextErrorOldPassword", value);
        }
    }

    /// <summary>
    /// Gets or sets the Existing Password
    /// </summary>
    public string TextErrorRequirePasswords
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("TextRequirePasswords"), ResHelper.LocalizeString("{$=�	Require complex passwords, consisting of at least both numeric and alphabetic characters|en-us=�	Require complex passwords, consisting of at least both numeric and alphabetic characters$}"));
        }
        set
        {
            this.SetValue("TextRequirePasswords", value);
        }
    }

    /// <summary>
    /// Gets or sets the Existing Password
    /// </summary>
    public string TextChangesSaved
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("TextChangesSaved"), ResHelper.LocalizeString("{$=������� ������� ���������:|en-us=Existing Password :$}"));
        }
        set
        {
            this.SetValue("TextChangesSaved", value);
        }
    }

    public string TextEqualsOldNewPassword
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("TextEqualsOldNewPassword"), ResHelper.LocalizeString("{$=� ������� ������� ��� ����� ������|en-us=You cannot use the same password again! $}"));
        }
        set
        {
            this.SetValue("TextEqualsOldNewPassword", value);
        }
    }

    /// <summary>
    /// Gets or sets the Existing Password
    /// </summary>
    public string OldPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("OldPasswordText"), ResHelper.LocalizeString("{$=������� ������� ���������:|en-us=Existing Password :$}"));
        }
        set
        {
            this.SetValue("OldPasswordText", value);
        }
    }


    /// <summary>
    /// Gets or sets the new password
    /// </summary>
    public string NewPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("NewPasswordText"), ResHelper.LocalizeString("{$=���� ������� ��������� :|en-us=new password :$}"));
        }
        set
        {
            this.SetValue("NewPasswordText", value);
        }
    }


    /// <summary>
    /// Gets or sets the Confirm password
    /// </summary>
    public string ConfirmPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ConfirmPasswordText"), ResHelper.LocalizeString("{$=���������� ���� ������� :|en-us=Confirm password :$}"));
        }
        set
        {
            this.SetValue("ConfirmPasswordText", value);
        }
    }


    /// <summary>
    /// Gets or sets the Button Submit
    /// </summary>
    public string ButtonSubmitText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ButtonSubmitText"), ResHelper.LocalizeString("{$=������ �������|en-us=Password Change$}"));
        }
        set
        {
            this.SetValue("ButtonSubmitText", value);
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do not process
            SetErrorMessages();
        }
        else
        {
            Visible = (!ShowOnlyWhenAuthenticated || CMSContext.CurrentUser.IsAuthenticated() || (Session["userName_RequirePasswords"] != null) );
            txtNewPassword.MaxLength = MaximalPasswordLength;
            txtConfirmPassword.MaxLength = MaximalPasswordLength;

            // Set labels text
            lblOldPassword.Text = this.OldPasswordText; //ResHelper.GetString("ChangePassword.lblOldPassword") + "1";
            lblNewPassword.Text = this.NewPasswordText; //ResHelper.GetString("ChangePassword.lblNewPassword") + "2";
            lblConfirmPassword.Text = this.ConfirmPasswordText; //ResHelper.GetString("ChangePassword.lblConfirmPassword") + "3";
            btnOk.Text = this.ButtonSubmitText; // ResHelper.GetString("ChangePassword.btnOK");
            SetErrorMessages();
        }
    }

    private void SetErrorMessages()
    {
        DataTable dt = DBConnection.GetErrorMessages(2);
        string JsStringMessages = "<script>  ";
        for (int i = 0; i <= dt.Rows.Count - 1; i++)
        {
            string ErrorMessageStr = ResHelper.LocalizeString(string.Concat("{$=", dt.Rows[i]["ErrorMessageGR"].ToString(), "|en-us=", dt.Rows[i]["ErrorMessageEN"].ToString(), "$}"));
            ErrorMessageStr = Regex.Replace(ErrorMessageStr, "'", "\\'");
            ErrorMessageStr = ErrorMessageStr.Replace(System.Environment.NewLine, "");

            JsStringMessages = string.Concat(JsStringMessages, " ", dt.Rows[i]["CodeName"].ToString(), " = '", ErrorMessageStr, "'; ");

        }
        lit_JsScript.Text = JsStringMessages + " </script>";
    }

    /// <summary>
    /// OnClick handler (Set password)
    /// </summary>
    protected void btnOk_Click(object sender, EventArgs e)
    {
        // Get current user info object
        //UserInfo ui;
        UserInfo ui;
        if (Session["userName_RequirePasswords"] != null)
        {
            string userName = Session["userName_RequirePasswords"].ToString();
            ui = UserInfoProvider.GetUserInfo(userName);
        }
        else {
            ui = CMSContext.CurrentUser;
        }
        
        // Get current site info object
        CurrentSiteInfo si = CMSContext.CurrentSite;

        if (StripHTMLFunctions.StripTags(txtOldPassword.Text).Trim() == StripHTMLFunctions.StripTags(txtNewPassword.Text).Trim())
        {
            lblInfo.Visible = true;
            lblInfo.Text = TextEqualsOldNewPassword;
            lblInfo.Style.Add("color", "red");
        }
        else if ((ui != null) && (si != null))
        {
            string userName = ui.UserName;
            string siteName = si.SiteName;
            bool valid = false;

            // new password correctly filled
            if (StripHTMLFunctions.StripTags(txtConfirmPassword.Text) == StripHTMLFunctions.StripTags(txtNewPassword.Text))
            {
                if (!CMSFunctions.CheckPassword(StripHTMLFunctions.StripTags(txtNewPassword.Text))) {
                    lblError.Visible = true;
                    lblError.Text = TextErrorRequirePasswords;
                }
                // Old password match
                else if (UserInfoProvider.AuthenticateUser(userName, StripHTMLFunctions.StripTags(txtOldPassword.Text).Trim(), siteName) != null)
                {
                    //UserInfoProvider.SetPassword(userName, txtNewPassword.Text.Trim());
                    
                    valid = true;
                    lblInfo.Visible = true;
                    lblInfo.Text = TextChangesSaved;//ResHelper.GetString("ChangePassword.ChangesSaved");
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = TextErrorOldPassword;//ResHelper.GetString("ChangePassword.ErrorOldPassword");
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = TextErrorNewPassword;//ResHelper.GetString("ChangePassword.ErrorNewPassword");
            }            

            if (valid) {
                ui.SetValue("UserLastPasswordModified", DateTime.Now);
                CMS.SiteProvider.UserInfoProvider.SetUserInfo(ui);

                UserInfoProvider.SetPassword(userName, StripHTMLFunctions.StripTags(txtNewPassword.Text).Trim());
                if (Session["userName_RequirePasswords"] != null)
                {
                    // Set authentication cookie
                    FormsAuthentication.SetAuthCookie(ui.UserName, false);

                    Session.Add("userName_RequirePasswords", null);
                    //UrlHelper.Redirect(ResolveUrl(UrlHelper.CurrentURL));
                    UrlHelper.Redirect("~/");
                }
            }
        }
    }
}