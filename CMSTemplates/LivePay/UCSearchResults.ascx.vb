﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.SiteProvider
Imports CMS.SettingsProvider
Imports CMS.DataEngine

Imports CMS.Controls.CMSAbstractTransformation


Partial Class CMSTemplates_LivePay_UCSearchResults
    Inherits CMSUserControl

    Protected ReadOnly Property NodeID() As Integer
        Get
            Dim iNodeID As Integer = 0
            If Not String.IsNullOrEmpty(Request("NodeID")) Then
                iNodeID = Request("NodeID")
            End If
            Return iNodeID
        End Get
    End Property

    Protected ReadOnly Property nomos() As Integer
        Get
            Dim inomos As Integer = 0
            If Not String.IsNullOrEmpty(Request("nomos")) Then
                inomos = Request("nomos")
            End If
            Return inomos
        End Get
    End Property

    Protected ReadOnly Property area() As Integer
        Get
            Dim iarea As Integer = 0
            If Not String.IsNullOrEmpty(Request("area")) Then
                iarea = Request("area")
            End If
            Return iarea
        End Get
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        Bind()
    End Sub

    Private Sub Bind()
        GetItemsCustomTable("LivePay.Merchant", "MerchantID", "CompanyName")
    End Sub

    Private Sub GetItemsCustomTable(ByVal CustomTable As String, ByVal ColumeValue As String, ByVal ColumeText As String)
        Dim srckey As String = Request.QueryString("searchtext")
        'If (Not String.IsNullOrEmpty(srckey)) AndAlso srckey.Contains("<") Then Response.Redirect(String.Concat("~/Search.aspx?searchtext=", srckey.Replace("<", "&lt;").Replace(">", "&gt;"), ""))
        Dim key As String = Replace(srckey, "'", String.Empty)
        key = Replace(key, """", String.Empty)



        Dim nodePath As String = String.Empty
        If Me.NodeID > 0 Then

            Dim tp As CMS.TreeEngine.TreeProvider = New CMS.TreeEngine.TreeProvider(CMS.CMSHelper.CMSContext.CurrentUser)
            Dim node As CMS.TreeEngine.TreeNode = tp.SelectSingleNode(Me.NodeID)
            'repSearch.Path = node.NodeAliasPath & "/%"
            nodePath = node.NodeAliasPath & "/%"
        End If

        Dim dsItems As DataSet = DBConnection.SearchMerchant("Proc_LivePay_SearchMerchant", key, nodePath, nomos, area, CMSContext.CurrentDocument.DocumentCulture)



        Session("GetGVMerchantsDataTable") = dsItems
        GVMerchants.DataSource = dsItems
        GVMerchants.DataBind()
        If dsItems.Tables(0).Rows.Count > 0 Then
            divResults.Visible = True
            lblResults.Text = dsItems.Tables(0).Rows.Count
        Else
            divResults.Visible = False
        End If
    End Sub

#Region " Paging "

    Protected Sub GvNews_PageIndexChanged(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles GVMerchants.PageIndexChanging
        GVMerchants.PageIndex = e.NewPageIndex
        Bind()
    End Sub

    Protected Sub GvNews_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GVMerchants.DataBound
        Dim PageLimit As Integer = 11
        Dim PageSeperator As Integer = (PageLimit - 1) / 2
        Dim pagerRow As GridViewRow = GVMerchants.BottomPagerRow
        If GVMerchants.Rows.Count > 0 Then
            Dim GetPageNow As Integer = GVMerchants.PageIndex + 1
            Dim GetPageCount As Integer = GVMerchants.PageCount
            Dim GetPageLinksIndex As HtmlGenericControl = DirectCast(pagerRow.Cells(0).FindControl("lblPageIndex"), HtmlGenericControl)
            If pagerRow.Visible = True Then
                If GetPageCount <= PageLimit Then
                    CreateGridPagerLinks(1, GVMerchants.PageCount, GetPageNow, GetPageLinksIndex)
                Else
                    Dim GetLeft As Integer = 1
                    Dim GetRight As Integer = PageLimit
                    If GetPageNow > (PageSeperator + 1) Then
                        GetLeft = GetPageNow - PageSeperator
                        GetRight = GetPageNow + PageSeperator
                        If GetRight > GetPageCount Then
                            GetLeft = GetLeft - (GetRight - GetPageCount)
                            If GetLeft < 1 Then
                                GetLeft = 1
                            End If
                            GetRight = GetPageCount
                        End If
                        CreateGridPagerLinks(GetLeft, GetRight, GetPageNow, GetPageLinksIndex)
                    Else
                        CreateGridPagerLinks(GetLeft, GetRight, GetPageNow, GetPageLinksIndex)
                    End If
                End If
            End If


            Dim lnkPrev As LinkButton = pagerRow.Cells(0).FindControl("lnkPrev")
            Dim lnkNext As LinkButton = pagerRow.Cells(0).FindControl("lnkNext")

            Dim lnkFirstPage As LinkButton = pagerRow.Cells(0).FindControl("lnkFirstPage")
            Dim lnkLastPage As LinkButton = pagerRow.Cells(0).FindControl("lnkLastPage")

            If (GVMerchants.PageIndex > 0) Then
                lnkPrev.Style.Add("color", "#242424")
                lnkPrev.Enabled = True

                lnkFirstPage.Style.Add("color", "#242424")
                lnkFirstPage.Enabled = True
            Else
                lnkPrev.Style.Add("color", "#adadad")
                lnkPrev.Enabled = False

                lnkFirstPage.Style.Add("color", "#adadad")
                lnkFirstPage.Enabled = False
            End If

            If (GVMerchants.PageCount - 1 > GVMerchants.PageIndex) Then
                lnkNext.Style.Add("color", "#242424")
                lnkNext.Enabled = True

                lnkLastPage.Style.Add("color", "#242424")
                lnkLastPage.Enabled = True
            Else
                lnkNext.Enabled = False
                lnkNext.Style.Add("color", "#adadad")

                lnkLastPage.Enabled = False
                lnkLastPage.Style.Add("color", "#adadad")
            End If



            Dim btnSaveToPDF As ImageButton = DirectCast(pagerRow.Cells(0).FindControl("btnSaveToPDF"), ImageButton)
            Dim btnSaveToExcel As ImageButton = DirectCast(pagerRow.Cells(0).FindControl("btnSaveToExcel"), ImageButton)

            If pagerRow.Visible = False Then
                pagerRow.Visible = True
            End If
        End If
    End Sub

    Private Sub CreateGridPagerLinks(ByVal StartNo As Integer, ByVal EndNo As Integer, ByVal GetPageNow As Integer, ByVal GetPageLinksIndex As HtmlGenericControl)
        For i As Integer = StartNo To EndNo
            Dim NewDiv As New HtmlGenericControl("div")
            Dim NewLink As New LinkButton
            NewLink.ID = "PagerLink_" & i
            NewLink.OnClientClick = String.Concat("GoToPage('", i, "');return false")
            If i = GetPageNow Then
                NewDiv.Attributes.Add("class", "GridPagerNoSel")
                NewLink.CssClass = "GridPagerNolnkSel"
            Else
                NewDiv.Attributes.Add("class", "GridPagerNo")
                NewLink.CssClass = "GridPagerNolnk"
                NewLink.Style.Add("color", "#5a5a5a")
            End If
            NewLink.Text = i
            NewDiv.Controls.Add(NewLink)
            GetPageLinksIndex.Controls.Add(NewDiv)
        Next
    End Sub

#Region " Extra Buttons First Page and Las Page "

    Sub ChangePageByLinkNumber_First(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageList As LinkButton = CType(sender, LinkButton)
        GVMerchants.PageIndex = 0
        GVMerchants.DataSource = Session("GetGVMerchantsDataTable")
        GVMerchants.DataBind()
    End Sub

    Sub ChangePageByLinkNumber_Last(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageList As LinkButton = CType(sender, LinkButton)
        GVMerchants.PageIndex = GVMerchants.PageCount - 1
        GVMerchants.DataSource = Session("GetGVMerchantsDataTable")
        GVMerchants.DataBind()
    End Sub

#End Region

#Region " Buttons Next And Prev "

    Sub ChangePageByLinkNumber_Before(ByVal sender As Object, ByVal e As EventArgs)
        If GVMerchants.PageIndex > 0 Then
            Dim pageList As LinkButton = CType(sender, LinkButton)
            GVMerchants.PageIndex = GVMerchants.PageIndex - 1
            GVMerchants.DataSource = Session("GetGVMerchantsDataTable")
            GVMerchants.DataBind()
        End If
    End Sub

    Sub ChangePageByLinkNumber_Next(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageList As LinkButton = CType(sender, LinkButton)
        GVMerchants.PageIndex = GVMerchants.PageIndex + 1
        GVMerchants.DataSource = Session("GetGVMerchantsDataTable")
        GVMerchants.DataBind()
    End Sub

#End Region

    Protected Sub HiddenPageBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HiddenPageBtn.Click
        GVMerchants.PageIndex = CInt(HiddenPage.Text) - 1
        GVMerchants.DataSource = Session("GetGVMerchantsDataTable")
        GVMerchants.DataBind()
    End Sub
#End Region

    Dim CountRec As Integer = 0
    Protected Sub GVMerchants_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GVMerchants.RowDataBound
        'GVMerchants
        If e.Row.RowType = DataControlRowType.DataRow Then
            If CountRec Mod 2 = 0 Then
                DirectCast(e.Row.FindControl("GvRow"), HtmlGenericControl).Style.Add("background-color", "#f0f0f0")
            End If
            CountRec = CountRec + 1
            '
        End If
    End Sub
End Class
