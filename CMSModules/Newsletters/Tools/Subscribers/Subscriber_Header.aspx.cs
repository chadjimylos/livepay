using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Newsletter;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Subscribers_Subscriber_Header : CMSNewsletterPage
{
    protected int subscriberId = 0;
    protected int siteId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        string currentSubscriber = ResHelper.GetString("Subscriber_Edit.NewItemCaption");

        // Get subscriber id from querystring
        siteId = QueryHelper.GetInteger("siteid", 0);
        subscriberId = QueryHelper.GetInteger("subscriberid", 0);

        if (subscriberId > 0)
        {
            Subscriber subscriberObj = SubscriberProvider.GetSubscriber(subscriberId);
            if (subscriberObj != null)
            {
                if (!DataHelper.IsEmpty(subscriberObj.SubscriberEmail))
                {
                    currentSubscriber = subscriberObj.SubscriberEmail;
                }
                else
                {
                    currentSubscriber = subscriberObj.SubscriberFirstName + " " + subscriberObj.SubscriberLastName;
                }
            }
        }

        // Initializes page title
        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("Subscriber_Edit.ItemListLink");
        breadcrumbs[0, 1] = "~/CMSModules/Newsletters/Tools/Subscribers/Subscriber_List.aspx?siteid=" + siteId;
        breadcrumbs[0, 2] = "newslettersContent";
        breadcrumbs[1, 0] = currentSubscriber;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        this.CurrentMaster.Title.HelpTopicName = "general_tab2";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }
    }


    /// <summary>
    /// Initializes user edit menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string generalString = ResHelper.GetString("general.general");
        string subscriptionsString = ResHelper.GetString("Subscriber_Edit.Subscription");

        string[,] tabs = new string[2, 4];
        tabs[0, 0] = generalString;
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab2');";
        tabs[0, 2] = "Subscriber_General.aspx?subscriberid=" + subscriberId + "&siteid=" + siteId;
        tabs[1, 0] = subscriptionsString;
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'subscriptions_tab');";
        tabs[1, 2] = "Subscriber_Subscriptions.aspx?subscriberid=" + subscriberId + "&siteid=" + siteId;

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "subscriberContent";
    }
}
