CREATE TABLE [CMS_WebPart] (
		[WebPartID]                 int NOT NULL IDENTITY(1, 1),
		[WebPartName]               nvarchar(100) NOT NULL,
		[WebPartDisplayName]        nvarchar(100) NOT NULL,
		[WebPartDescription]        nvarchar(max) NOT NULL,
		[WebPartFileName]           nvarchar(100) NOT NULL,
		[WebPartProperties]         nvarchar(max) NOT NULL,
		[WebPartCategoryID]         int NOT NULL,
		[WebPartParentID]           int NULL,
		[WebPartDocumentation]      nvarchar(max) NULL,
		[WebPartGUID]               uniqueidentifier NOT NULL,
		[WebPartLastModified]       datetime NOT NULL,
		[WebPartType]               int NULL,
		[WebPartLoadGeneration]     int NOT NULL,
		[WebPartLastSelection]      datetime NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WebPart]
	ADD
	CONSTRAINT [PK_CMS_WebPart]
	PRIMARY KEY
	NONCLUSTERED
	([WebPartID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_WebPart]
	ADD
	CONSTRAINT [DEFAULT_CMS_WebPart_WebPartLoadGeneration]
	DEFAULT ((0)) FOR [WebPartLoadGeneration]
IF OBJECT_ID(N'[CMS_WebPart]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WebPartCategory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WebPart_WebPartCategoryID_CMS_WebPartCategory]') IS NULL
BEGIN
		ALTER TABLE [CMS_WebPart]
			ADD CONSTRAINT [FK_CMS_WebPart_WebPartCategoryID_CMS_WebPartCategory]
			FOREIGN KEY ([WebPartCategoryID]) REFERENCES [CMS_WebPartCategory] ([CategoryID])
END
IF OBJECT_ID(N'[CMS_WebPart]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WebPart]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WebPart_WebPartParentID_CMS_WebPart]') IS NULL
BEGIN
		ALTER TABLE [CMS_WebPart]
			ADD CONSTRAINT [FK_CMS_WebPart_WebPartParentID_CMS_WebPart]
			FOREIGN KEY ([WebPartParentID]) REFERENCES [CMS_WebPart] ([WebPartID])
END
CREATE INDEX [IX_CMS_WebPart_WebPartCategoryID]
	ON [CMS_WebPart] ([WebPartCategoryID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WebPart_WebPartLastSelection]
	ON [CMS_WebPart] ([WebPartLastSelection])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_WebPart_WebPartLoadGeneration]
	ON [CMS_WebPart] ([WebPartLoadGeneration])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WebPart_WebPartName]
	ON [CMS_WebPart] ([WebPartName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_WebPart_WebPartParentID]
	ON [CMS_WebPart] ([WebPartParentID])
	ON [PRIMARY]
