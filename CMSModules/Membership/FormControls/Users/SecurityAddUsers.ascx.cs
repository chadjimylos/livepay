using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.FormEngine;
using CMS.SiteProvider;
using CMS.EventLog;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Membership_FormControls_Users_SecurityAddUsers : FormEngineUserControl
{

    #region "Private variables"

    private int mNodeID = 0;
    private TreeNode mNode = null;
    private int mBoardID = 0;
    private int mForumID = 0;
    private string mCurrentValues = String.Empty;
    private TreeProvider mTree = null;
    private EventLogProvider mEventLog = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets node id.
    /// </summary>
    public int NodeID
    {
        get
        {
            return mNodeID;
        }
        set
        {
            // Clear TreeNode on id change
            if (mNodeID != value)
            {
                mNode = null;
            }

            mNodeID = value;
        }
    }


    /// <summary>
    /// Gets or sets the TreeNode
    /// </summary>
    public TreeNode Node
    {
        get
        {
            if ((mNode == null) && (this.NodeID > 0))
            {
                // Get node
                mNode = Tree.SelectSingleNode(this.NodeID);
            }
            return mNode;
        }
        set
        {
            mNode = value;
            // Update NodeID
            if (mNode != null)
            {
                this.NodeID = mNode.NodeID;
            }
        }
    }


    /// <summary>
    /// Gets or sets board id.
    /// </summary>
    public int BoardID
    {
        get
        {
            return mBoardID;
        }
        set
        {
            mBoardID = value;
        }
    }


    /// <summary>
    /// Gets or sets forum id.
    /// </summary>
    public int ForumID
    {
        get
        {
            return mForumID;
        }
        set
        {
            mForumID = value;
        }
    }


    /// <summary>
    /// Gets or sets group id.
    /// </summary>
    public int GroupID
    {
        get
        {
            return usUsers.GroupID;
        }
        set
        {
            usUsers.GroupID = value;
        }
    }


    /// <summary>
    /// Returns current uniselector.
    /// </summary>
    public UniSelector CurrentSelector
    {
        get
        {
            return usUsers.UniSelector;
        }
    }

    /// <summary>
    /// Gets or sets subscriber.
    /// </summary>
    public string CurrentValues
    {
        get
        {
            return mCurrentValues;
        }
        set
        {
            mCurrentValues = value;
        }
    }


    /// <summary>
    /// Enables or disables the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            usUsers.Enabled = value;
        }
    }


    /// <summary>
    /// Enables or disables site filter in uni selector
    /// </summary>
    public bool ShowSiteFilter
    {
        get
        {
            return usUsers.ShowSiteFilter;
        }
        set
        {
            usUsers.ShowSiteFilter = value;
        }
    }


    /// <summary>
    /// Gets or sets ID of the site. Only users of this site are shown in selector.
    /// Note. SiteID is not used if site filter is enabled
    /// </summary>
    public int SiteID
    {
        get
        {
            return usUsers.SiteID;
        }
        set
        {
            usUsers.SiteID = value;
        }
    }


    /// <summary>
    /// Indicates if control is used on live site
    /// </summary>
    public override bool IsLiveSite
    {
        get
        {
            return base.IsLiveSite;
        }
        set
        {
            base.IsLiveSite = value;
            usUsers.IsLiveSite = value;
        }
    }

    #endregion


    #region "Protected properties"

    /// <summary>
    /// Tree provider
    /// </summary>
    protected TreeProvider Tree
    {
        get
        {
            if (mTree == null)
            {
                mTree = new TreeProvider(CMSContext.CurrentUser);
            }
            return mTree;
        }
    }


    /// <summary>
    /// Event log provider
    /// </summary>
    protected EventLogProvider EventLog
    {
        get
        {
            if (mEventLog == null)
            {
                mEventLog = new EventLogProvider(Tree.Connection);
            }
            return mEventLog;
        }
    }

    #endregion


    #region "Events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // Add sites filter        
        usUsers.UniSelector.SetValue("FilterMode", "user");
        usUsers.ResourcePrefix = "addusers";
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check node permissions
        if (this.Node != null)
        {
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(this.Node, NodePermissionsEnum.ModifyPermissions) != AuthorizationResultEnum.Allowed)
            {
                usUsers.Enabled = false;
                return;
            }
        }

        // Check message board permission
        if (this.BoardID > 0)
        {
            IInfoObject boardObj = ModuleCommands.MessageBoardGetMessageBoardInfo(this.BoardID);
            if (boardObj != null)
            {
                int boardGroupId = ValidationHelper.GetInteger(boardObj.GetValue("BoardGroupID"), 0);
                if (boardGroupId > 0)
                {
                    if (!CMSContext.CurrentUser.IsGroupAdministrator(boardGroupId))
                    {
                        usUsers.Enabled = false;
                        return;
                    }
                }
                else if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.MessageBoards", "Modify"))
                {
                    usUsers.Enabled = false;
                    return;
                }
            }
        }

        // Check forum permission
        if (this.ForumID > 0)
        {
            if (this.GroupID > 0)
            {
                if (!CMSContext.CurrentUser.IsGroupAdministrator(this.GroupID))
                {
                    usUsers.Enabled = false;
                    return;
                }
            }
            else if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.forums", CMSAdminControl.PERMISSION_MODIFY))
            {
                usUsers.Enabled = false;
                return;
            }
        }

        // Add event handling
        usUsers.UniSelector.OnItemsSelected += new EventHandler(usUsers_OnItemsSelected);
        usUsers.UniSelector.OnSelectionChanged += new EventHandler(usUsers_OnItemsSelected);
    }


    protected void usUsers_OnItemsSelected(object sender, EventArgs e)
    {
        AclProvider aclProv = null;

        // Create Acl provider to current treenode
        if (this.Node != null)
        {
            aclProv = new AclProvider(Tree);
        }

        // Remove old items
        string newValues = ValidationHelper.GetString(usUsers.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, CurrentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int userID = ValidationHelper.GetInteger(item, 0);

                    if (BoardID > 0)
                    {
                        // Remove message board from board
                        ModuleCommands.MessageBoardRemoveModeratorFromBoard(userID, BoardID);
                    }
                    else if (this.Node != null)
                    {
                        if (aclProv != null)
                        {
                            UserInfo ui = UserInfoProvider.GetUserInfo(userID);
                            if (ui != null)
                            {
                                // Remove user from treenode
                                aclProv.RemoveUser(this.NodeID, ui);
                            }
                        }
                    }
                    else if (ForumID > 0)
                    {
                        // Remove user from forum moderators
                        ModuleCommands.ForumsRemoveForumModerator(userID, ForumID);
                    }
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(CurrentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int userID = ValidationHelper.GetInteger(item, 0);

                    if (BoardID > 0)
                    {
                        // Add user to the message board
                        ModuleCommands.MessageBoardAddModeratorToBoard(userID, BoardID);
                    }
                    else if (this.Node != null)
                    {
                        // Add user to treenode
                        if (aclProv != null)
                        {
                            UserInfo ui = UserInfoProvider.GetUserInfo(userID);
                            if (ui != null)
                            {
                                // Remove user from treenode
                                aclProv.SetUserPermissions(this.Node, 0, 0, ui);
                            }
                        }
                    }
                    else if (ForumID > 0)
                    {
                        // Add user to the forum moderators
                        ModuleCommands.ForumsAddForumModerator(userID, ForumID);
                    }
                }
            }
        }

        this.RaiseOnChanged();
    }

    #endregion


    /// <summary>
    /// Reloads the data of the UniSelector.
    /// </summary>
    public void ReloadData()
    {
        this.usUsers.Value = this.CurrentValues;
        this.usUsers.ReloadData();
    }
}
