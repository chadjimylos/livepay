<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImageEditor.aspx.cs" Inherits="CMSFormControls_LiveSelectors_ImageEditor"
    Theme="Default" MasterPageFile="~/CMSMasterPages/LiveSite/Dialogs/ModalDialogPage.master"
    Title="Edit image" %>

<%@ Register Src="~/CMSAdminControls/ImageEditor/ImageEditor.ascx" TagName="ImageEditor"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <div class="PageContent">
        <cms:ImageEditor ID="imageEditor" runat="server" IsLiveSite="true" EnableViewState="true" />
    </div>
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:LocalizedButton ID="btnClose" runat="server" OnClientClick="Close(); return false;"
            CssClass="SubmitButton" ResourceString="general.close" />
    </div>
</asp:Content>
