using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Newsletter;

public partial class CMSModules_Newsletters_FormControls_NewsletterSelector : CMS.FormControls.FormEngineUserControl
{
    private string mValue = "";

    #region "Properties"

    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            this.EnsureChildControls();
            base.Enabled = value;
            this.usNewsletters.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return usNewsletters.Value;
        }
        set
        {
            this.EnsureChildControls();
            mValue = ValidationHelper.GetString(value, "");
            try
            {
                usNewsletters.Value = mValue;
            }
            catch
            {
            }
        }
    }


    /// <summary>
    /// Gets ClientID of the dropdownlist with newsletters
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return usNewsletters.ClientID;
        }
    }

    #endregion


    #region "Methods"

    protected override void CreateChildControls()
    {
        if (usNewsletters == null)
        {
            pnlUpdate.LoadContainer();
        }
        base.CreateChildControls();
        if (!StopProcessing)
        {
            ReloadData();
        }
    }


    protected void ReloadData()
    {
        usNewsletters.WhereCondition = "NewsletterSiteID = " + CMSContext.CurrentSiteID;
        usNewsletters.ButtonRemoveSelected.CssClass = "XLongButton";
        usNewsletters.ButtonAddItems.CssClass = "XLongButton";
        usNewsletters.SpecialFields = new string[,] { { ResHelper.GetString("NewsletterSelect.LetUserChoose"), "NWSLetUserChoose" } };
        usNewsletters.ReturnColumnName = "NewsletterName";

        try
        {
            usNewsletters.Value = mValue;
        }
        catch
        {
        }
    }

    #endregion
}
