using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_TaxClasses_TaxClass_General : CMSEcommerceConfigurationPage
{
    protected int taxclassid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        rfvTaxClassDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvTaxClassName.ErrorMessage = ResHelper.GetString("general.requirescodename");

        // control initializations				
        lblTaxClassDisplayName.Text = ResHelper.GetString("TaxClass_Edit.TaxClassDisplayNameLabel");
        lblTaxClassName.Text = ResHelper.GetString("TaxClass_Edit.TaxClassNameLabel");
        lblTaxZero.Text = ResHelper.GetString("TaxClass_Edit.lblTaxZero");

        btnOk.Text = ResHelper.GetString("General.OK");

        string currentTaxClass = "";

        // get taxClass id from querystring		
        taxclassid = ValidationHelper.GetInteger(Request.QueryString["taxclassid"], 0);
        if (taxclassid > 0)
        {
            TaxClassInfo taxClassObj = TaxClassInfoProvider.GetTaxClassInfo(taxclassid);
            if (taxClassObj != null)
            {
                currentTaxClass = taxClassObj.TaxClassDisplayName;

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(taxClassObj);

                    // show that the taxClass was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
            else
            {
                btnOk.Enabled = false;
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("TaxClass_Edit.TaxClassDoesNotExists");
                return;
            }

        }
    }


    /// <summary>
    /// Load data of editing taxClass.
    /// </summary>
    /// <param name="taxClassObj">TaxClass object.</param>
    protected void LoadData(TaxClassInfo taxClassObj)
    {
        txtTaxClassDisplayName.Text = taxClassObj.TaxClassDisplayName;
        txtTaxClassName.Text = taxClassObj.TaxClassName;
        chkTaxZero.Checked = taxClassObj.TaxClassZeroIfIDSupplied;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        string errorMessage = new Validator()
            .NotEmpty(txtTaxClassDisplayName.Text.Trim(), ResHelper.GetString("general.requiresdisplayname"))
            .NotEmpty(txtTaxClassName.Text.Trim(), ResHelper.GetString("general.requirescodename")).Result;

        if (!ValidationHelper.IsCodeName(txtTaxClassName.Text.Trim()))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

        if (errorMessage == "")
        {
            // taxClassName must to be unique
            TaxClassInfo taxClassObj = TaxClassInfoProvider.GetTaxClassInfo(txtTaxClassName.Text.Trim());

            // if taxClassName value is unique														
            if ((taxClassObj == null) || (taxClassObj.TaxClassID == taxclassid))
            {
                // if taxClassName value is unique -> determine whether it is update or insert 
                if (taxClassObj == null)
                {
                    // get TaxClassInfo object by primary key
                    taxClassObj = TaxClassInfoProvider.GetTaxClassInfo(taxclassid);
                }
                if (taxClassObj != null)
                {
                    taxClassObj.TaxClassDisplayName = txtTaxClassDisplayName.Text.Trim();
                    taxClassObj.TaxClassName = txtTaxClassName.Text.Trim();
                    taxClassObj.TaxClassZeroIfIDSupplied = chkTaxZero.Checked;

                    TaxClassInfoProvider.SetTaxClassInfo(taxClassObj);

                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("TaxClass_Edit.TaxClassNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
