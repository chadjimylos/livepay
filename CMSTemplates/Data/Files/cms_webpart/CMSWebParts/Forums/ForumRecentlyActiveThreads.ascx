<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Forums/ForumRecentlyActiveThreads.ascx.cs"
    Inherits="CMSWebParts_Forums_ForumRecentlyActiveThreads" %>
<%@ Register TagPrefix="cms" Namespace="CMS.Forums" Assembly="CMS.Forums" %>
<cms:BasicRepeater runat="server" ID="repLatestPosts" />
<cms:ForumPostsDataSource runat="server" ID="forumDataSource" />
