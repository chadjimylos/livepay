using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.SiteProvider;

public partial class CMSWebParts_Community_Friends_FriendsDataSource : CMSAbstractWebPart
{
    #region "Stop processing"

    /// <summary>
    /// Returns true if the control processing should be stopped
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return base.StopProcessing;
        }
        set
        {
            base.StopProcessing = value;
            srcFriends.StopProcessing = value;
        }
    }

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets WHERE condition.
    /// </summary>
    public string WhereCondition
    {
        get
        {
            return ValidationHelper.GetString(GetValue("WhereCondition"), "");
        }
        set
        {
            SetValue("WhereCondition", value);
            srcFriends.WhereCondition = value;
        }
    }


    /// <summary>
    /// Gets or sets UserId.
    /// </summary>
    public int UserID
    {
        get
        {
            int userId = ValidationHelper.GetInteger(GetValue("UserID"), 0);
            if ((userId == 0) && (SiteContext.CurrentUser != null))
            {
                return SiteContext.CurrentUser.UserID;
            }
            return userId;
        }
        set
        {
            SetValue("UserID", value);
            srcFriends.UserID = value;
        }
    }


    /// <summary>
    /// Gets or sets ORDER BY condition.
    /// </summary>
    public string OrderBy
    {
        get
        {
            return ValidationHelper.GetString(GetValue("OrderBy"), string.Empty);
        }
        set
        {
            SetValue("OrderBy", value);
            srcFriends.OrderBy = value;
        }
    }


    /// <summary>
    /// Gets or sets status of friends to be selected.
    /// </summary>
    public FriendshipStatusEnum FriendStatus
    {
        get
        {
            return (FriendshipStatusEnum)ValidationHelper.GetInteger(GetValue("FriendStatus"), 0);
        }
        set
        {
            SetValue("FriendStatus", (int)value);
            srcFriends.FriendStatus = value;
        }
    }


    /// <summary>
    /// Gets or sets the source filter name
    /// </summary>
    public string FilterName
    {
        get
        {
            return ValidationHelper.GetString(GetValue("FilterName"), "");
        }
        set
        {
            SetValue("FilterName", value);
            srcFriends.SourceFilterName = value;
        }
    }


    /// <summary>
    /// Gets or sets the cache item name
    /// </summary>
    public override string CacheItemName
    {
        get
        {
            return base.CacheItemName;
        }
        set
        {
            base.CacheItemName = value;
            this.srcFriends.CacheItemName = value;
        }
    }


    /// <summary>
    /// Cache dependencies, each cache dependency on a new line
    /// </summary>
    public override string CacheDependencies
    {
        get
        {
            return ValidationHelper.GetString(base.CacheDependencies, this.srcFriends.CacheDependencies);
        }
        set
        {
            base.CacheDependencies = value;
            this.srcFriends.CacheDependencies = value;

        }
    }


    /// <summary>
    /// Gets or sets the cache minutes
    /// </summary>
    public override int CacheMinutes
    {
        get
        {
            return base.CacheMinutes;
        }
        set
        {
            base.CacheMinutes = value;
            this.srcFriends.CacheMinutes = value;
        }
    }

    /// <summary>
    /// Gest or sets selected columns
    /// </summary>
    public string Columns
    {
        get
        {
            return ValidationHelper.GetString(GetValue("Columns"), "");
        }
        set
        {
            SetValue("Columns", value);
            srcFriends.SelectedColumns = value;
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            srcFriends.WhereCondition = WhereCondition;
            srcFriends.OrderBy = OrderBy;
            srcFriends.UserID = UserID;
            srcFriends.FilterName = ID;
            srcFriends.SourceFilterName = FilterName;
            srcFriends.CacheItemName = CacheItemName;
            srcFriends.CacheDependencies = this.CacheDependencies;
            srcFriends.CacheMinutes = CacheMinutes;
            srcFriends.FriendStatus = FriendStatus;
            srcFriends.SelectedColumns = Columns;
        }
    }


    /// <summary>
    /// Clears cache.
    /// </summary>
    public override void ClearCache()
    {
        this.srcFriends.ClearCache();
    }

    #endregion
}
