<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupProfile.ascx.cs"
    Inherits="CMSModules_Groups_Controls_GroupProfile" %>
<asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<div class="TabContainer">
    <cms:BasicTabControl ID="tabMenu" runat="server" Visible="true" />
</div>
<asp:Panel ID="pnlContent" runat="server" CssClass="TabBody">
</asp:Panel>
