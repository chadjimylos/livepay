<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyDocuments.aspx.cs" Inherits="CMSDesk_MyDesk_MyDocuments_MyDocuments"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="MyDesk - My documents" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <cms:UniGrid ID="ugDocs" runat="server" GridName="mydocuments.xml" OrderBy="DocumentName"
        IsLiveSite="false" />

    <script type="text/javascript">
        //<![CDATA[
        // Select item action
        function SelectItem(nodeId, culture) {
            if (nodeId != 0) {
                parent.parent.location.href = "../../default.aspx?section=content&action=edit&nodeid=" + nodeId + "&culture=" + culture;
            }
        }
        //]]>
    </script>

</asp:Content>
