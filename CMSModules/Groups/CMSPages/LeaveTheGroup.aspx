<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LeaveTheGroup.aspx.cs" 
Inherits="CMSModules_Groups_CMSPages_LeaveTheGroup" MasterPageFile="~/CMSMasterPages/LiveSite/SimplePage.master" Theme="Default"  %>

<%@ Register Src="~/CMSModules/Groups/Controls/GroupLeave.ascx" TagName="GroupLeave"
    TagPrefix="uc1" %>
<asp:Content ID="cntContent" runat="server" ContentPlaceHolderID="plcContent">
    <div class="CommunityJoinTheGroup">
        <uc1:GroupLeave ID="GroupLeave1" runat="server"></uc1:GroupLeave>
    </div>
</asp:Content>
