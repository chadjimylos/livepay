<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FormUserControl_Edit.aspx.cs"
    Inherits="CMSSiteManager_Development_FormControls_FormUserControl_Edit" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Form User Control Edit" %>
<%@ Register Src="~/CMSFormControls/System/UserControlSelector.ascx" TagPrefix="cms"
    TagName="FileSystemSelector" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblInfo" runat="server" /><asp:Label runat="server" ID="lblError"
        ForeColor="red" EnableViewState="false" Visible="false" />
    <table>
        <tr>
            <td>
                <cms:LocalizedLabel ID="lblDisplayName" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblDisplayName" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:TextBox ID="tbDisplayName" runat="server" CssClass="TextBoxField" MaxLength="100" /><br />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="tbDisplayName"
                    Display="Dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblCodeName" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lbCodeName" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:TextBox ID="tbCodeName" runat="server" CssClass="TextBoxField" MaxLength="100" /><br />
                <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" EnableViewState="false"
                    ControlToValidate="tbCodeName" Display="dynamic"></asp:RequiredFieldValidator>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblFileName" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblFileName" DisplayColon="true" />
            </td>
            <td colspan="2">
                <cms:FileSystemSelector ID="tbFileName" runat="server" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblForText" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblForText" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkForText" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblForLongText" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblForLongText" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkForLongText" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblForInteger" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblForInteger" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkForInteger" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblForDecimal" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblForDecimal" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkForDecimal" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblForDateTime" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblForDateTime" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkForDateTime" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblForBoolean" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblForBoolean" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkForBoolean" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblForFile" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblForFile" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkForFile" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblForGuid" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblForGuid" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkForGuid" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblForVisibility" runat="server" CssClass="ContentLabel"
                    EnableViewState="false" ResourceString="Development_FormUserControl_Edit.lblForVisibility"
                    DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkForVisibility" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblForBizForms" runat="server" CssClass="ContentLabel" EnableViewState="false"
                    ResourceString="Development_FormUserControl_Edit.lblForBizForms" DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkForBizForms" runat="server" CssClass="ContentCheckBox" AutoPostBack="True"
                    OnCheckedChanged="chkForBizForms_CheckedChanged" />
                <br />
                <table>
                    <tr>
                        <td>
                            <cms:LocalizedLabel ID="lblDataType" runat="server" EnableViewState="false" ResourceString="Development_FormUserControl_Edit.lblDataType"
                                DisplayColon="true" />
                        </td>
                        <td class="FieldLabel">
                            <asp:DropDownList ID="drpDataType" runat="server" CssClass="SmallDropDown" AutoPostBack="True"
                                OnSelectedIndexChanged="drpDataType_SelectedIndexChanged">
                                <asp:ListItem Selected="True">Text</asp:ListItem>
                                <asp:ListItem>Long text</asp:ListItem>
                                <asp:ListItem>Integer</asp:ListItem>
                                <asp:ListItem>Decimal</asp:ListItem>
                                <asp:ListItem>DateTime</asp:ListItem>
                                <asp:ListItem>Boolean</asp:ListItem>
                                <asp:ListItem>File</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldLabel">
                            <cms:LocalizedLabel ID="lblColumnSize" runat="server" EnableViewState="false" ResourceString="Development_FormUserControl_Edit.lblColumnSize"
                                DisplayColon="true" />
                        </td>
                        <td style="width: 158px">
                            <asp:TextBox ID="tbColumnSize" runat="server" CssClass="SmallTextBox">0</asp:TextBox>
                            <cms:LocalizedLabel ID="lblErrorSize" runat="server" ForeColor="Red" Visible="False"
                                EnableViewState="false" ResourceString="Development_FormUserControl_Edit.lblErrorSize"
                                DisplayColon="true" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblShowInDocumentTypes" runat="server" CssClass="ContentLabel"
                    EnableViewState="false" ResourceString="Development_FormUserControl_Edit.lblShowInDocumentTypes"
                    DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkShowInDocumentTypes" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblShowInSystemTables" runat="server" CssClass="ContentLabel"
                    EnableViewState="false" ResourceString="Development_FormUserControl_Edit.lblShowInSystemTables"
                    DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkShowInSystemTables" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblShowInCustomTables" runat="server" CssClass="ContentLabel"
                    EnableViewState="false" ResourceString="Development_FormUserControl_Edit.lblShowInCustomTables"
                    DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkShowInCustomTables" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblShowInControls" runat="server" CssClass="ContentLabel"
                    EnableViewState="false" ResourceString="Development_FormUserControl_Edit.lblShowInControls"
                    DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkShowInControls" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top; height: 21px;">
                <cms:LocalizedLabel ID="lblWebPartsAndControls" runat="server" CssClass="ContentLabel"
                    EnableViewState="false" ResourceString="Development_FormUserControl_Edit.lblWebPartsAndControls" />
            </td>
            <td colspan="2">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblShowInReports" runat="server" CssClass="ContentLabel"
                    EnableViewState="false" ResourceString="Development_FormUserControl_Edit.lblShowInReports"
                    DisplayColon="true" />
            </td>
            <td colspan="2">
                <asp:CheckBox ID="chkShowInReports" runat="server" CssClass="ContentCheckBox" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:LocalizedButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton"
                    EnableViewState="false" ResourceString="general.ok" />
            </td>
        </tr>
    </table>
</asp:Content>
