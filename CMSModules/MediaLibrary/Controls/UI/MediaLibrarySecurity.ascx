<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MediaLibrarySecurity.ascx.cs"
    Inherits="CMSModules_MediaLibrary_Controls_UI_MediaLibrarySecurity" %>
<%@ Register Src="~/CMSAdminControls/UI/UniControls/UniMatrix.ascx" TagName="UniMatrix" TagPrefix="cms" %>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
<cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<asp:Table runat="server" ID="tblMatrix" CssClass="PermissionMatrix" CellPadding="3"
    CellSpacing="0" EnableViewState="false" GridLines="Horizontal">
</asp:Table>
<br />
<cms:UniMatrix ID="gridMatrix" runat="server" QueryName="media.library.getpermissionmatrix"
    RowItemIDColumn="RoleID" ColumnItemIDColumn="PermissionID" RowItemDisplayNameColumn="RoleDisplayName"
    ColumnItemDisplayNameColumn="PermissionDisplayName" RowTooltipColumn="RowDisplayName"
    ColumnTooltipColumn="PermissionDescription" ItemTooltipColumn="PermissionDescription"
    FirstColumnsWidth="28" FixedWidth="12" UsePercentage="true" />
