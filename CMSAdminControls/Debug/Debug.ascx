﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Debug.ascx.cs" Inherits="CMSAdminControls_Debug_Debug" %>
<%@ Register Src="~/CMSAdminControls/Debug/QueryLog.ascx" TagName="QueryLog" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/Debug/CacheLog.ascx" TagName="CacheLog" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/Debug/ViewState.ascx" TagName="ViewState" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/Debug/SecurityLog.ascx" TagName="SecurityLog" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/Debug/MacroLog.ascx" TagName="MacroLog" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/Debug/RequestLog.ascx" TagName="RequestLog" TagPrefix="cms" %>
<div>
    <cms:QueryLog ID="logSQL" runat="server" />
    <cms:CacheLog ID="logCache" runat="server" />
    <cms:ViewState ID="logState" runat="server" />
    <cms:SecurityLog ID="logSec" runat="server" />
    <cms:MacroLog ID="logMac" runat="server" />
    <cms:RequestLog ID="logRequest" runat="server" />
</div>
