﻿var toolbarElem = document.getElementById('FCKToolbar');

function ClearToolbar() {
    toolbarElem.innerHTML = '';
    toolbarElem.__FCKToolbarSet = null;
    toolbarElem.style.height = '';
    ResizeFrame();
}

function SaveDocument(nodeId, createAnother) {
    ClearToolbar();
    try {
        if (window.frames['pageview'].SaveDocument) {
            window.frames['pageview'].SaveDocument(nodeId, createAnother);
        }
        else {
            alert(notAllowedAction);
        }
    }
    catch (err) {
        alert(notAllowedAction);
    }
}

function Approve(nodeId) {
    ClearToolbar();
    try {
        if (window.frames['pageview'].Approve) {
            window.frames['pageview'].Approve(nodeId);
        }
        else {
            alert(notAllowedAction);
        }
    }
    catch (err) {
        alert(notAllowedAction);
    }
}

function Reject(nodeId) {
    ClearToolbar();

    if (window.frames['pageview'].Reject) {
        window.frames['pageview'].Reject(nodeId);
    }
}

function CheckIn(nodeId) {
    ClearToolbar();
    try {
        if (window.frames['pageview'].CheckIn) {
            window.frames['pageview'].CheckIn(nodeId);
        }
        else {
            alert(notAllowedAction);
        }
    }
    catch (err) {
        alert(notAllowedAction);
    }
}

function SetToolbarHeight() {
    var toolbarHeight = 0;
    if (toolbarElem.childNodes.length > 0) {
        var toolbarFrames = toolbarElem.getElementsByTagName('iframe');
        var toolbarFrame = null;
        if ((toolbarFrames != null) && (toolbarFrames.length > 0)) {
            toolbarFrame = toolbarFrames[0];
        }
        if (toolbarFrame != null) {
            toolbarHeight = toolbarFrame.height;
            var h = toolbarHeight + 'px';
            if (toolbarElem.style.height != h) {
                toolbarElem.style.height = h;
            }
        }
    }

    return toolbarHeight;
}

function ResizeFrame() {
    var pageElem = document.getElementById('pageview');
    if (pageElem != null) {
        var toolbarHeight = SetToolbarHeight();
        var h = document.body.offsetHeight - toolbarHeight - 1;
        if (pageElem.height != h) {
            pageElem.height = h;
        }
    }
}

function ShowToolbar() {
    SetToolbarHeight();
    ResizeFrame();
}

function PassiveRefresh(nodeId, parentNodeId) {
    if (parent.PassiveRefresh) {
        parent.PassiveRefresh(nodeId, parentNodeId);
    }
}

function RefreshTree(nodeId, selectNodeId) {
    if (parent.RefreshTree) {
        return parent.RefreshTree(nodeId, selectNodeId);
    }
    return false;
}

function RefreshNode(nodeId, selectNodeId) {
    if (parent.RefreshNode) {
        return parent.RefreshNode(nodeId, selectNodeId);
    }
    return false;
}

function SelectNode(nodeId) {
    if (parent.SelectNode) {
        return parent.SelectNode(nodeId);
    }
}

function CreateAnother() {
    if (parent.CreateAnother) {
        parent.CreateAnother();
    }
}

// Edit mode actions
function NotAllowed(baseUrl, action) {
    if (parent.NotAllowed) {
        parent.NotAllowed(baseUrl, action);
    }
}

function NewDocument(parentNodeId, className) {
    if (parent.NewDocument) {
        parent.NewDocument(parentNodeId, className);
    }
}

function DeleteDocument(nodeId) {
    if (parent.DeleteDocument) {
        parent.DeleteDocument(nodeId);
    }
}

function EditDocument(nodeId) {
    if (parent.EditDocument) {
        parent.EditDocument(nodeId);
    }
}

function SpellCheck(spellURL) {
    try {
        if (window.frames['pageview'].SpellCheck) {
            window.frames['pageview'].SpellCheck(spellURL);
        }
        else {
            alert(notAllowedAction);
        }
    }
    catch (err) {
        alert(notAllowedAction);
    }
}

// File created
function FileCreated(nodeId, parentNodeId, closeWindow) {
    if (parent.FileCreated) {
        parent.FileCreated(nodeId, parentNodeId, closeWindow);
    }
}

function CheckChanges() {
    if (window.frames['pageview'].CheckChanges) {
        return window.frames['pageview'].CheckChanges();
    }
    return true;
}

function FocusFrame() {
    var fr = document.getElementById("pageview");
    try {
        if (document.all)
        //IE
        {
            fr.document.body.focus();
        }
        else
        //Firefox
        {
            fr.contentDocument.body.focus();
        }
    }
    catch (err)
    { }
}

var IsCMSDesk = true;
var isMainWindow = true;
