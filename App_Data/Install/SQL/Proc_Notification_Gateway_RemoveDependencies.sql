-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Notification_Gateway_RemoveDependencies]
	@GatewayID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from	
	SET NOCOUNT ON;
    DELETE FROM [Notification_Subscription] WHERE SubscriptionGatewayID=@GatewayID;
	DELETE FROM [Notification_TemplateText] WHERE GatewayID=@GatewayID;
END
