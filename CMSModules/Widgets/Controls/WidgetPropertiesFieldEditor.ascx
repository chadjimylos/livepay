<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetPropertiesFieldEditor.ascx.cs"
    Inherits="CMSModules_Widgets_Controls_WidgetPropertiesFieldEditor" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/FieldEditor.ascx" TagName="FieldEditor" TagPrefix="cms" %>    

<asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" />
<cms:FieldEditor ID="fieldEditor" runat="server" EnableMacrosForDefaultValue="true" />
