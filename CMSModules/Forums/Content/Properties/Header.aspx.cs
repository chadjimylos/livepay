using System;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSModules_Forums_Content_Properties_Header : CMSToolsModalPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check permissions for CMS Desk -> Tools -> Forums
        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Tools", "Forums"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Tools", "Forums");
        }

        this.CurrentMaster.Title.TitleText = ResHelper.GetString("forum.header.forums");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Forums_Forum/object.png");
        this.CurrentMaster.FrameResizer.Visible = false;
    }
}
