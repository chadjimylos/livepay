﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.IO;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.MediaLibrary;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.Controls;
using CMS.ExtendedControls;
using CMS.TreeEngine;
using System.Text;


public partial class CMSModules_MediaLibrary_Controls_Dialogs_MediaView : MediaView
{
    #region "Events & delegates"

    /// <summary>
    /// Delegate for the event occuring when information on file import status is required.
    /// </summary>
    /// <param name="type">Type of the required information.</param>
    /// <param name="parameter">Parameter related.</param>
    public delegate object OnGetInformation(string type, object parameter);


    /// <summary>
    /// Event occuring when information on file import status is required.
    /// </summary>
    public event OnGetInformation GetInformation;

    #endregion


    #region "Private variables"

    // Media library variables
    private int mLibraryId = 0;
    private MediaLibraryInfo mLibraryInfo = null;
    private SiteInfo mLibrarySite = null;

    private bool mIsCopyMoveLinkDialog = false;
    protected string mSaveText = null;
    private bool? mHasModify = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets an ID of the media library.
    /// </summary>
    public int LibraryID
    {
        get
        {
            return mLibraryId;
        }
        set
        {
            mLibraryId = value;
        }
    }


    /// <summary>
    /// Current media library information.
    /// </summary>
    public MediaLibraryInfo LibraryInfo
    {
        get
        {
            if (mLibraryInfo == null)
            {
                mLibraryInfo = MediaLibraryInfoProvider.GetMediaLibraryInfo(LibraryID);
            }
            return mLibraryInfo;
        }
        set
        {
            mLibraryInfo = value;
        }
    }


    /// <summary>
    /// Indicates whether control processing should be stopped.
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return base.StopProcessing;
        }
        set
        {
            innermedia.StopProcessing = value;
            base.StopProcessing = value;
        }
    }


    /// <summary>
    /// Gets currently selected page size.
    /// </summary>
    public int CurrentTopN
    {
        get
        {
            return innermedia.CurrentTopN;
        }
    }


    /// <summary>
    /// Gets or sets the OutputFormat (needed for correct dialog type reckognition)
    /// </summary>
    public OutputFormatEnum OutputFormat
    {
        get
        {
            return innermedia.OutputFormat;
        }
        set
        {
            innermedia.OutputFormat = value;
        }
    }


    /// <summary>
    /// Gets a UniGrid control used to display files in LIST view mode.
    /// </summary>
    public UniGrid ListViewControl
    {
        get
        {
            return innermedia.ListViewControl;
        }
    }


    /// <summary>
    /// Indicates if full listing mode is enabled. This mode enables navigation to child and parent folders/documents from current view.
    /// </summary>
    public bool IsFullListingMode
    {
        get
        {
            return innermedia.IsFullListingMode;
        }
        set
        {
            innermedia.IsFullListingMode = value;
        }
    }


    /// <summary>
    /// Indicates whether the control is displayed as part of the copy/move dialog.
    /// </summary>
    public bool IsCopyMoveLinkDialog
    {
        get
        {
            return mIsCopyMoveLinkDialog;
        }
        set
        {
            mIsCopyMoveLinkDialog = value;
            innermedia.IsCopyMoveLinkDialog = value;
        }
    }


    /// <summary>
    /// Gets list of names of selected files.
    /// </summary>
    public ArrayList SelectedItems
    {
        get
        {
            return innermedia.SelectedItems;
        }
    }


    /// <summary>
    /// Gets or sets the value which determineds whtether to show the Parent button or not.
    /// </summary>
    public bool ShowParentButton
    {
        get
        {
            return plcParentButton.Visible;
        }
        set
        {
            plcParentButton.Visible = value;
        }
    }

    #endregion


    #region "Private properties"

    /// <summary>
    /// Gets site info current library is related to.
    /// </summary>
    private SiteInfo LibrarySite
    {
        get
        {
            if ((mLibrarySite == null) && (LibraryInfo != null))
            {
                mLibrarySite = SiteInfoProvider.GetSiteInfo(LibraryInfo.LibrarySiteID);
            }
            return mLibrarySite;
        }
    }

    #endregion


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Parent directory button
        if (ShowParentButton)
        {
            plcParentButton.Visible = true;
            imgParent.ImageUrl = ResolveUrl(GetImageUrl("CMSModules/CMS_Content/Dialogs/parent.png", IsLiveSite));
            mSaveText = ResHelper.GetString("dialogs.mediaview.parentfolder");
            btnParent.Attributes["onclick"] = "SetAction('parentselect', ''); RaiseHiddenPostBack(); return false;";
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        Visible = !StopProcessing;

        // If processing the request should not continue
        if (!StopProcessing)
        {
            // Initialize controls
            SetupControls();
        }

        innermedia.ViewMode = ViewMode;
        innermedia.IsLiveSite = IsLiveSite;
    }


    /// <summary>
    /// Loads control's content.
    /// </summary>
    /// <param name="forceSetup">Indicates whether the inner controls should be re-setuped.</param>
    public void Reload(bool forceSetup)
    {
        Visible = !StopProcessing;
        if (Visible)
        {
            // Initialize controls
            SetupControls();

            ReloadData(forceSetup);
        }
    }


    /// <summary>
    /// Loads control's content.
    /// </summary>    
    public void Reload()
    {
        Reload(false);
    }



    /// <summary>
    /// Ensures no item is selected.
    /// </summary>
    public void ResetListSelection()
    {
        innermedia.ResetListSelection();
    }


    /// <summary>
    /// Ensures first page is displayed in the control displaying the content.
    /// </summary>
    public void ResetPageIndex()
    {
        this.innermedia.ResetPageIndex();
    }


    #region "Private methods"

    /// <summary>
    /// Initializes all nested controls.
    /// </summary>
    private void SetupControls()
    {
        InitializeControlScripts();

        // Initialize inner view control
        innermedia.ViewMode = ViewMode;
        innermedia.DataSource = DataSource;
        innermedia.SelectableContent = SelectableContent;

        string gridName = "~/CMSModules/MediaLibrary/Controls/Dialogs/MediaListView.xml";
        if (!IsCopyMoveLinkDialog && (DisplayMode == ControlDisplayModeEnum.Simple))
        {
            innermedia.DisplayMode = DisplayMode;
            gridName = "~/CMSModules/MediaLibrary/Controls/Dialogs/MediaListView_UI.xml";
        }
        else if (IsCopyMoveLinkDialog && (DisplayMode == ControlDisplayModeEnum.Simple))
        {
            innermedia.DisplayMode = DisplayMode;
            gridName = "~/CMSModules/MediaLibrary/Controls/Dialogs/MediaListView_CopyMove.xml";
        }

        innermedia.ListViewControl.GridName = gridName;
        ((CMSAdminControls_UI_UniGrid_UniGrid)(innermedia.ListViewControl)).OnPageChanged += new EventHandler<EventArgs>(ListViewControl_OnPageChanged);     
        
        innermedia.SourceType = SourceType;

        innermedia.ResizeToHeight = ResizeToHeight;
        innermedia.ResizeToMaxSideSize = ResizeToMaxSideSize;
        innermedia.ResizeToWidth = ResizeToWidth;

        // Set inner control binding columns
        innermedia.FileIdColumn = "FileGUID";
        innermedia.FileNameColumn = "FileName";
        innermedia.FileExtensionColumn = (DisplayMode == ControlDisplayModeEnum.Simple) ? "Extension" : "FileExtension";
        innermedia.FileSizeColumn = "FileSize";
        innermedia.FileWidthColumn = "FileImageWidth";
        innermedia.FileHeightColumn = "FileImageHeight";

        // Register for inner media events
        innermedia.GetArgumentSet += new CMSModules_Content_Controls_Dialogs_LinkMediaSelector_InnerMediaView.OnGetArgumentSet(innermedia_GetArgumentSet);
        innermedia.GetListItemUrl += new CMSModules_Content_Controls_Dialogs_LinkMediaSelector_InnerMediaView.OnGetListItemUrl(innermedia_GetListItemUrl);
        innermedia.GetTilesThumbsItemUrl += new CMSModules_Content_Controls_Dialogs_LinkMediaSelector_InnerMediaView.OnGetTilesThumbsItemUrl(innermedia_GetTilesThumbsItemUrl);
        innermedia.GetInformation += new CMSModules_Content_Controls_Dialogs_LinkMediaSelector_InnerMediaView.OnGetInformation(innermedia_GetInformation);
        innermedia.GetModifyPermission += new CMSModules_Content_Controls_Dialogs_LinkMediaSelector_InnerMediaView.OnGetModifyPermission(innermedia_GetModifyPermission);
    }


    private bool innermedia_GetModifyPermission(DataRow dr)
    {
        if (mHasModify == null)
        {
            if (LibraryInfo != null)
            {
                mHasModify = MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "filemodify");
            }
            else
            {
                mHasModify = false;
            }
        }
        return (mHasModify == true ? true : false);
    }


    private object innermedia_GetInformation(string type, object parameter)
    {
        return GetInformationInternal(type, parameter);
    }


    private object GetInformationInternal(string type, object parameter)
    {
        if (GetInformation != null)
        {
            return GetInformation(type, parameter);
        }

        return null;
    }


    private void ListViewControl_OnPageChanged(object sender, EventArgs e)
    {
        RaiseListReloadRequired();
    }


    /// <summary>
    /// Initializes scrips used by the control.
    /// </summary>
    private void InitializeControlScripts()
    {
        string script = @" function SetSelectAction(argument) {
                                // Raise select action
                                SetAction('select', argument);
                                RaiseHiddenPostBack();
                            }";

        ScriptHelper.RegisterStartupScript(this, GetType(), "DialogsSelectAction", ScriptHelper.GetScript(script));
    }


    /// <summary>
    /// Loads data from data source property.
    /// </summary>
    /// <param name="forceSetup">Indicates whether the inner controls should be re-setuped.</param>
    private void ReloadData(bool forceSetup)
    {
        innermedia.Reload(forceSetup);
    }


    /// <summary>
    /// Displays listing info message.
    /// </summary>
    /// <param name="infoMsg">Info message to display.</param>
    public void DisplayListingInfo(string infoMsg)
    {
        if (!string.IsNullOrEmpty(infoMsg))
        {
            plcListingInfo.Visible = true;
            lblListingInfo.Text = infoMsg;
        }
    }

    #endregion


    #region "Inner media view event handlers"

    /// <summary>
    /// Returns argument set according passed DataRow and flag indicating whether the set is obtained for selected item.
    /// </summary>
    /// <param name="dr">DataRow with all the item data.</param>
    /// <param name="isSelected">Indicates whether the set is required for an selected item.</param>
    private string innermedia_GetArgumentSet(DataRow dr)
    {
        // Return required argument set
        return GetArgumentSet(dr);
    }


    /// <summary>
    /// Returns URL of item according specified conditions.
    /// </summary>
    /// <param name="dr">Data row holding information on item.</param>
    /// <param name="isPreview">Indicates whether the URL is requested for item preview.</param>
    private string innermedia_GetListItemUrl(DataRow dr, bool isPreview, bool notAttachment)
    {
        // Get set of important information
        string arg = GetArgumentSet(dr);
        // Get item url
        return GetItemUrlInternal(arg, dr, isPreview, 0, 0, 0, notAttachment);
    }


    /// <summary>
    /// Returns URL of item according specified conditions.
    /// </summary>
    /// <param name="dr">Data row holding information on item.</param>
    /// <param name="isPreview">Indicates whether the URL is requested for item preview.</param>
    /// <param name="maxSideSize">Specifies maximum size of the image.</param>
    private string innermedia_GetTilesThumbsItemUrl(DataRow dr, bool isPreview, int height, int width, int maxSideSize, bool notAttachment)
    {
        if (LibraryInfo != null)
        {
            // get argument set
            string arg = GetArgumentSet(dr);

            // Get extension
            string ext = "";
            if (dr.Table.Columns.Contains("FileExtension"))
            {
                ext = dr["FileExtension"].ToString();
            }
            else
            {
                ext = dr["Extension"].ToString();
            }

            // If image is requested for preview
            if (isPreview)
            {
                if (ext.ToLower() == "<dir>")
                {
                    return GetDocumentTypeIconUrl("CMS.Folder", "48x48");
                }
                else
                {
                    // Check if file has a preview
                    if (!ImageHelper.IsImage(ext))
                    {
                        // File isn't image and no preview exists - get the default file icon
                        return GetFileIconUrl(ext, "");
                    }
                    else
                    {
                        // Files are obtained from the FS
                        if (!dr.Table.Columns.Contains("FileURL"))
                        {
                            // Try to get preview or image itself
                            return GetItemUrl(arg, isPreview, height, width, maxSideSize);
                        }
                        else
                        {
                            // Information comming from FileSystem (FS)
                            return GetFileIconUrl(ext, "");
                        }
                    }
                }
            }
            else
            {
                // Get item url
                return GetItemUrlInternal(arg, dr, isPreview, height, width, maxSideSize, notAttachment);
            }
        }
        return null;
    }

    #endregion


    #region "Helper methods"

    /// <summary>
    /// Returns argument set for the passed file data row.
    /// </summary>
    /// <param name="dr">Data row object holding all the data on the current file.</param>
    /// <param name="isSelectedItem">Indicates whether the argument is required for selected item initialization.</param>
    public string GetArgumentSet(DataRow dr)
    {
        if (dr.Table.Columns.Contains("FileGUID"))
        {
            return dr["FileGUID"].ToString() + "|" + dr["FileName"].ToString() + "|" + dr["FilePath"].ToString() + "|" +
                dr["FileExtension"] + "|" + dr["FileImageWidth"].ToString() + "|" + dr["FileImageHeight"].ToString() + "|" +
                dr["FileTitle"].ToString() + "|" + dr["FileSize"].ToString() + "|" + dr["FileID"].ToString();
        }
        else
        {
            return dr["FileName"].ToString() + "|" + dr["Extension"].ToString() + "|" + dr["FileURL"] + "|" + dr["Size"].ToString();
        }
    }


    /// <summary>
    /// Returns URL of the media item according site settings.
    /// </summary>
    /// <param name="fileInfo">Media file information.</param>
    /// <param name="isPreview">Indicates whether the file has a preview file or not.</param>
    /// <param name="height">Height of the requested image.</param>
    /// <param name="width">Width of the requested image.</param>
    /// <param name="maxSideSize">Maximum dimension for images displayed for tile and thumbnails view.</param>
    public string GetItemUrl(MediaFileInfo fileInfo, bool isPreview, int height, int width, int maxSideSize)
    {
        if (fileInfo!=null)
        {
            return GetItemUrl(fileInfo, LibrarySite, fileInfo.FileGUID, fileInfo.FileName, fileInfo.FileExtension, fileInfo.FilePath, isPreview, height, width, maxSideSize);
        }

        return "";
    }


    /// <summary>
    /// Returns URL of the media item according site settings.
    /// </summary>
    /// <param name="argument">Argument containing information on current media item.</param>
    /// <param name="isPreview">Indicates whether the file has a preview file or not.</param>
    /// <param name="height">Height of the requested image.</param>
    /// <param name="width">Width of the requested image.</param>
    /// <param name="maxSideSize">Maximum dimension for images displayed for tile and thumbnails view.</param>
    public string GetItemUrl(string argument, bool isPreview, int height, int width, int maxSideSize)
    {
        string[] argArr = argument.Split('|');
        if (argArr.Length >= 2)
        {
            // Get information from argument
            Guid fileGuid = ValidationHelper.GetGuid(argArr[0], Guid.Empty);
            string fileName = argArr[1];
            string fileExtension = argArr[3];
            string filePath = argArr[2];

            return GetItemUrl(null, LibrarySite, fileGuid, fileName, fileExtension, filePath, isPreview, height, width, maxSideSize);
        }

        return "";
    }


    /// <summary>
    /// Returns URL of the media item according site settings.
    /// </summary>
    /// <param name="argument">Argument containing information on current media item</param>
    /// <param name="dr">Data row object holding all the data on the current file</param>
    /// <param name="isPreview">Indicates whether the file has a preview file or not</param>
    /// <param name="height">Specifies height of the image</param>
    /// <param name="width">Specifies width of the image</param>
    /// <param name="maxSideSize">Specifies maximum size of the image</param>
    /// <param name="notAttachment">Indicates whether the file is attachment</param>
    private string GetItemUrlInternal(string argument, DataRow dr, bool isPreview, int height, int width, int maxSideSize, bool notAttachment)
    {
        MediaFileInfo mfi = null;

        // Get filename with extension
        string fileName = null;
        if (dr.Table.Columns.Contains("FileExtension"))
        {
            string ext = ValidationHelper.GetString(dr["FileExtension"], "");
            // In format 'name'
            fileName = ValidationHelper.GetString(dr["FileName"], "");
            
            fileName = AttachmentHelper.GetFullFileName(fileName, ext);
        }
        else
        {
            // In format 'name.ext'
            fileName = dr["FileName"].ToString();
        }

        // Try to get imported data row
        DataRow importedRow = GetInformationInternal("fileisnotindatabase", fileName) as DataRow;
        if (importedRow != null)
        {
            mfi = new MediaFileInfo(importedRow);
        }

        // If data row is not from DB check external library
        if (!dr.Table.Columns.Contains("FileGUID"))
        {
            bool isExternal = false;
            // Check if is external media library
            if (dr.Table.Columns.Contains("FilePath"))
            {
                // Get file path
                string filePath = ValidationHelper.GetString(dr["FilePath"], String.Empty);
                if (!String.IsNullOrEmpty(filePath))
                {
                    // Test if file path is inside system root folder
                    string rootPath = Server.MapPath("~/");
                    if (!filePath.StartsWith(rootPath))
                    {
                        isExternal = true;
                    }
                }
            }

            if (isExternal && dr.Table.Columns.Contains("FileName"))
            {
                if (mfi != null)
                {
                    return GetItemUrl(mfi, isPreview, 0, 0, 0);
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        // Files are obtained from the FS
        if (dr.Table.Columns.Contains("FileURL"))
        {
            // Information comming from FileSystem (FS)
            return UrlHelper.GetAbsoluteUrl(dr["FileURL"].ToString());
        }
        else
        {
            if (mfi != null)
            {
                // Get URL of the list item image
                return GetItemUrl(mfi, isPreview, 0, 0, 0);
            }
            else 
            {
                return GetItemUrl(argument, isPreview, 0, 0, 0);
            }
        }
    }

    
    /// <summary>
    /// Returnd URL of the media item according site settings.
    /// </summary>
    /// <param name="site">Information on site file belongs to.</param>
    /// <param name="fileGuid">GUID of the file URL is generated for.</param>
    /// <param name="fileName">Name of the file URL is generated for.</param>
    /// <param name="filePath">Media file path of the file URL is generated for.</param>
    /// <param name="isPreview">Indicates whether the file URL is generated for preview.</param>
    /// <param name="maxSideSize">Maximum dimension for images displayed for tile and thumbnails view.</param>
    public string GetItemUrl(SiteInfo site, Guid fileGuid, string fileName, string fileExtension, string filePath, bool isPreview, int height, int width, int maxSideSize)
    {
        return GetItemUrl(null, site, fileGuid, fileName, fileExtension, filePath, isPreview, height, width, maxSideSize);
    }


    /// <summary>
    /// Returnd URL of the media item according site settings.
    /// </summary>
    /// <param name="site">Information on site file belongs to.</param>
    /// <param name="fileGuid">GUID of the file URL is generated for.</param>
    /// <param name="fileName">Name of the file URL is generated for.</param>
    /// <param name="filePath">Media file path of the file URL is generated for.</param>
    /// <param name="isPreview">Indicates whether the file URL is generated for preview.</param>
    /// <param name="maxSideSize">Maximum dimension for images displayed for tile and thumbnails view.</param>
    public string GetItemUrl(MediaFileInfo fileInfo, SiteInfo site, Guid fileGuid, string fileName, string fileExtension, string filePath, bool isPreview, int height, int width, int maxSideSize)
    {
        string result = "";
        bool resize = (maxSideSize > 0);

        fileName = UsePermanentUrls ? AttachmentHelper.GetFullFileName(fileName, fileExtension) : fileName;

        if ((Config != null) && (Config.UseFullURL || (CMSContext.CurrentSiteID != LibraryInfo.LibrarySiteID) || (CMSContext.CurrentSiteID != base.GetCurrentSiteId())))
        {
            // If permanent URLs should be generated
            if (UsePermanentUrls || resize || isPreview)
            {
                // URL in format 'http://domainame/getmedia/123456-25245-45454-45455-5455555545/testfile.gif'
                result = MediaFileURLProvider.GetMediaFileAbsoluteUrl(site.SiteName, fileGuid, fileName);
            }
            else
            {
                // URL in format 'http://domainame/cms/EcommerceSite/media/testlibrary/folder1/testfile.gif'
                result = MediaFileURLProvider.GetMediaFileUrl(site.SiteName, LibraryInfo.LibraryFolder, filePath);
                result = UrlHelper.GetAbsoluteUrl(result, site.DomainName, UrlHelper.GetApplicationUrl(site.DomainName), HttpContext.Current.Request.Url.AbsolutePath);
            }
        }
        else
        {
            if (UsePermanentUrls || resize || isPreview)
            {
                // URL in format '/cms/getmedia/123456-25245-45454-45455-5455555545/testfile.gif'
                result = MediaFileURLProvider.GetMediaFileUrl(fileGuid, fileName);
            }
            else
            {
                if (fileInfo != null)
                {
                    result = MediaFileURLProvider.GetMediaFileUrl(fileInfo, CMSContext.CurrentSiteName, LibraryInfo.LibraryFolder);
                }
                else
                {
                    // URL in format '/cms/EcommerceSite/media/testlibrary/folder1/testfile.gif'
                    result = MediaFileURLProvider.GetMediaFileUrl(CMSContext.CurrentSiteName, LibraryInfo.LibraryFolder, filePath);
                }
            }
        }

        // If image dimensions are specified
        if (resize)
        {
            result = UrlHelper.AddParameterToUrl(result, "maxsidesize", maxSideSize.ToString());
        }
        if (height > 0)
        {
            result = UrlHelper.AddParameterToUrl(result, "height", height.ToString());
        }
        if (width > 0)
        {
            result = UrlHelper.AddParameterToUrl(result, "width", width.ToString());
        }

        // Media selctor should returns non-resolved URL in all cases
        bool isMediaSelector = (OutputFormat == OutputFormatEnum.URL) && (SelectableContent == SelectableContentEnum.OnlyMedia);

        return (isMediaSelector ? result : UrlHelper.ResolveUrl(result));
    }


    /// <summary>
    /// Returns updated IFrame URL when required.
    /// </summary>
    /// <param name="dr">Data row holding information on imported media file.</param>
    public string GetUpdateIFrameUrl(DataRow dr)
    {
        string result = null;

        // Uploader is displayed only for imported files - have FileIDs
        if ((dr != null) && dr.Table.Columns.Contains("FileID"))
        {
            DirectFileUploader dfuElem = (Page.LoadControl("~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploader.ascx") as DirectFileUploader);

            innermedia.GetLibraryUpdateControl(ref dfuElem, dr);

            result = dfuElem.IFrameUrl;
        }

        return result;
    }


    /// <summary>
    /// Gets HTML code of rendered DirectFileUploader control.
    /// </summary>
    /// <param name="mfi">Media file information.</param>
    public string GetDirectFileUploaderHTML(MediaFileInfo mfi)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter tw = new StringWriter(sb);
        HtmlTextWriter hw = new HtmlTextWriter(tw);

        DirectFileUploader dfuElem = Page.LoadControl("~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploader.ascx") as DirectFileUploader;
        if (dfuElem != null)
        {
            Panel updatePanel = new Panel();

            // Initialize update control
            innermedia.GetLibraryUpdateControl(ref dfuElem, mfi.DataClass.DataRow);
            dfuElem.ReloadData();

            updatePanel.Attributes["style"] = "text-align:center;";
            updatePanel.Controls.Add(dfuElem);
            updatePanel.RenderControl(hw);
        }

        return sb.ToString();
    }


    /// <summary>
    /// Ensures no item is selected.
    /// </summary>
    public void ResetSearch()
    {
        dialogSearch.ResetSearch();
    }

    #endregion
}
