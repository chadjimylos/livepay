﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.FormControls
Imports CMS.Controls
Imports CMS.UIControls

Partial Class CMSWebParts_LivePay_Filter_CustomFieldsMerchants
    Inherits CMSUserControl

#Region "Methods"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load


    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        btnFilter.ImageUrl = ResHelper.LocalizeString(String.Concat("{$=/app_themes/LivePay/btnSearch.png|en-us=/app_themes/LivePay/btnSearch_en-us.png$}"))
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            'ReloadData()
        End If
        ReloadData()
    End Sub

    Public Overloads Sub ReloadData()
        Bind()
    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnFilter.Click
        Bind()
    End Sub



    Private Sub Bind()
        'Dim dt As New DataTable("HistoryPayments")
        Dim ds As DataSet = DBConnection.GetCustomFieldsMerchant(StripHTMLFunctions.StripTags(txtMerchantsName.Text), CMSContext.CurrentDocument.DocumentCulture)

        Session("Merchants_GetSrcHisPayDataTable1") = ds
        GridViewPayments.DataSource = ds
        GridViewPayments.DataBind()
    End Sub

#Region " Paging "

    Protected Sub GvNews_PageIndexChanged(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles GridViewPayments.PageIndexChanging
        GridViewPayments.PageIndex = e.NewPageIndex
        Bind()
    End Sub

    Protected Sub GvNews_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridViewPayments.DataBound
        Dim pagerRow As GridViewRow = GridViewPayments.BottomPagerRow
        If GridViewPayments.Rows.Count > 0 Then
            Dim GetPageNow As Integer = GridViewPayments.PageIndex + 1
            Dim GetPageCount As Integer = GridViewPayments.PageCount
            Dim GetPageLinksIndex As HtmlGenericControl = DirectCast(pagerRow.Cells(0).FindControl("lblPageIndex"), HtmlGenericControl)
            If pagerRow.Visible = True Then
                If GetPageCount <= 5 Then
                    CreateGridPagerLinks(1, GridViewPayments.PageCount, GetPageNow, GetPageLinksIndex)
                Else
                    Dim GetLeft As Integer = 1
                    Dim GetRight As Integer = 5
                    If GetPageNow > 3 Then
                        GetLeft = GetPageNow - 2
                        GetRight = GetPageNow + 2
                        If GetRight > GetPageCount Then
                            GetLeft = GetLeft - (GetRight - GetPageCount)
                            If GetLeft < 1 Then
                                GetLeft = 1
                            End If
                            GetRight = GetPageCount
                        End If
                        CreateGridPagerLinks(GetLeft, GetRight, GetPageNow, GetPageLinksIndex)
                    Else
                        CreateGridPagerLinks(GetLeft, GetRight, GetPageNow, GetPageLinksIndex)
                    End If
                End If
            End If
            pagerRow.Cells(0).FindControl("lnkPrev").Visible = (GridViewPayments.PageIndex > 0)
            pagerRow.Cells(0).FindControl("lnkNext").Visible = (GridViewPayments.PageCount - 1 > GridViewPayments.PageIndex)
            'Dim btnSaveToPDF As ImageButton = DirectCast(pagerRow.Cells(0).FindControl("btnSaveToPDF"), ImageButton)
            'Dim btnSaveToExcel As ImageButton = DirectCast(pagerRow.Cells(0).FindControl("btnSaveToExcel"), ImageButton)


            If pagerRow.Visible = False Then
                pagerRow.Visible = True
            End If
        End If
    End Sub

    Private Sub CreateGridPagerLinks(ByVal StartNo As Integer, ByVal EndNo As Integer, ByVal GetPageNow As Integer, ByVal GetPageLinksIndex As HtmlGenericControl)
        For i As Integer = StartNo To EndNo
            Dim NewDiv As New HtmlGenericControl("div")
            Dim NewLink As New LinkButton
            NewLink.ID = "PagerLink_" & i
            NewLink.OnClientClick = String.Concat("GoToPage('", i, "');return false")
            If i = GetPageNow Then
                NewDiv.Attributes.Add("class", "GridPagerNoSel")
                NewLink.CssClass = "GridPagerNolnkSel"
            Else
                NewDiv.Attributes.Add("class", "GridPagerNo")
                NewLink.CssClass = "GridPagerNolnk"
                NewLink.Style.Add("color", "#4f85d1")
            End If
            NewLink.Text = i
            NewDiv.Controls.Add(NewLink)
            GetPageLinksIndex.Controls.Add(NewDiv)
        Next
    End Sub

    Sub ChangePageByLinkNumber_Before(ByVal sender As Object, ByVal e As EventArgs)
        If GridViewPayments.PageIndex > 0 Then
            Dim pageList As LinkButton = CType(sender, LinkButton)
            GridViewPayments.PageIndex = GridViewPayments.PageIndex - 1
            GridViewPayments.DataSource = Session("Merchants_GetSrcHisPayDataTable1")
            GridViewPayments.DataBind()
        End If
    End Sub

    Sub ChangePageByLinkNumber_Next(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageList As LinkButton = CType(sender, LinkButton)
        GridViewPayments.PageIndex = GridViewPayments.PageIndex + 1
        GridViewPayments.DataSource = Session("Merchants_GetSrcHisPayDataTable1")
        GridViewPayments.DataBind()
    End Sub

    Protected Sub HiddenPageBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HiddenPageBtn.Click
        GridViewPayments.PageIndex = CInt(StripHTMLFunctions.StripTags(HiddenPage.Text)) - 1
        GridViewPayments.DataSource = Session("Merchants_GetSrcHisPayDataTable1")
        GridViewPayments.DataBind()
    End Sub
#End Region

#End Region

End Class
