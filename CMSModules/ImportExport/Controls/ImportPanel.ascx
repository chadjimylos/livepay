<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImportPanel.ascx.cs" Inherits="CMSModules_ImportExport_Controls_ImportPanel" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>
<%@ Register Src="ImportGridView.ascx" TagName="ImportGridView" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/Trees/ObjectTree.ascx" TagName="ObjectTree" TagPrefix="cms" %>

<script type="text/javascript">
    //<![CDATA[
    function imSetDivPosition() {
        var intY = document.getElementById("imTreeDiv").scrollTop;
        document.getElementById("hdnDivScrollBar").value = intY;
    }

    function imGetDivPosition() {
        var intY = document.getElementById('hdnDivScrollBar').value;
        document.getElementById("imTreeDiv").scrollTop = intY;
    }

    // Hook onload handler
    if (window.onload != null) {
        var oldOnload = window.onload;
        window.onload = function(e) { oldOnload(e); imGetDivPosition(); }
    }
    else {
        window.onload = function(e) { imGetDivPosition(); }
    }
    //]]>
</script>

<asp:Literal ID="ltlDefaultValues" runat="server" />
<asp:Panel ID="pnlGrid" runat="Server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 250px; vertical-align: top;">
                <div id="imTreeDiv" style="width: 250px; height: 387px; padding: 5px; overflow: auto;
                    border-right: solid 1px #dddddd; border-left: solid 1px #dddddd;" onclick="imSetDivPosition();" onunload="imSetDivPosition();">
                    <cms:ObjectTree ID="objectTree" runat="server" UsePostback="true" UseImages="true"
                        EnableViewState="true" />
                </div>
                <input type="hidden" id="hdnDivScrollBar" name="hdnDivScrollBar" value="<%= mScrollPosition %>" />
            </td>
            <td style="vertical-align: top;">
                <div style="height: 387px; padding: 5px 15px 5px 15px; overflow: auto;">
                    <div style="width: 97%;">
                        <cms:PageTitle ID="titleElem" runat="server" />
                        <asp:Panel runat="server" ID="pnlError" CssClass="WizardHeaderLine" Visible="false"
                            BackColor="transparent">
                            <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" />
                        </asp:Panel>
                        <asp:PlaceHolder ID="plcPanel" runat="server">
                            <asp:PlaceHolder ID="plcControl" runat="Server" />
                            <br />
                            <cms:ImportGridView ID="gvObjects" runat="server" />
                        </asp:PlaceHolder>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
