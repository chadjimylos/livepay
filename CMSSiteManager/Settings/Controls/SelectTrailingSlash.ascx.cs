using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.FormEngine;
using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.Staging;

public partial class CMSSiteManager_Settings_Controls_SelectTrailingSlash : FormEngineUserControl
{
    private string slash = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        ReloadData();
    }


    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.drpSlash.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return ValidationHelper.GetString(drpSlash.SelectedValue, "");
        }
        set
        {
            slash = ValidationHelper.GetString(value, "");
            ReloadData();
        }
    }


    /// <summary>
    /// Return true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        return true;
    }


    /// <summary>
    /// Loads drop down list with data.
    /// </summary>
    private void ReloadData()
    {
        if (this.drpSlash.Items.Count == 0)
        {
            this.drpSlash.Items.Add(new ListItem(ResHelper.GetString("trailingslashcheck.dontcare"), "DONTCARE"));
            this.drpSlash.Items.Add(new ListItem(ResHelper.GetString("trailingslashcheck.always"), "ALWAYS"));
            this.drpSlash.Items.Add(new ListItem(ResHelper.GetString("trailingslashcheck.never"), "NEVER"));
        }

        // Preselect value
        ListItem selectedItem = drpSlash.Items.FindByValue(slash);
        if (selectedItem != null)
        {
            drpSlash.ClearSelection();
            selectedItem.Selected = true;
        }
        else
        {
            this.drpSlash.SelectedIndex = 0;
        }
    }
}