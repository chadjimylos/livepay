CREATE TABLE [CMS_Category] (
		[CategoryID]               int NOT NULL IDENTITY(1, 1),
		[CategoryDisplayName]      nvarchar(250) NULL,
		[CategoryName]             nvarchar(250) NULL,
		[CategoryDescription]      nvarchar(max) NOT NULL,
		[CategoryCount]            int NOT NULL,
		[CategoryEnabled]          bit NOT NULL,
		[CategoryUserID]           int NULL,
		[CategoryGUID]             uniqueidentifier NOT NULL,
		[CategoryLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Category]
	ADD
	CONSTRAINT [PK_CMS_Category]
	PRIMARY KEY
	NONCLUSTERED
	([CategoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_Category]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Category_CategoryUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_Category]
			ADD CONSTRAINT [FK_CMS_Category_CategoryUserID_CMS_User]
			FOREIGN KEY ([CategoryUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE CLUSTERED INDEX [IX_CMS_Category_CategoryDisplayName_CategoryEnabled]
	ON [CMS_Category] ([CategoryDisplayName], [CategoryEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Category_CategoryUserID]
	ON [CMS_Category] ([CategoryUserID])
	ON [PRIMARY]
