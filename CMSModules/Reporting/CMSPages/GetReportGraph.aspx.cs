using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Collections.Generic;
using System.Threading;

using ZedGraph;
using ZedGraph.Web;

using CMS.CMSHelper;
using CMS.Reporting;
using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SettingsProvider;
using CMS.EventLog;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Reporting_CMSPages_GetReportGraph : CMSPage
{
    protected Guid sGraphGuid;
    protected ReportGraphInfo reportGraph;
    protected DataSet dsGraphData;
    protected string mCulture = "en-us";


    /// <summary>
    /// Constructor
    /// </summary>
    public CMSModules_Reporting_CMSPages_GetReportGraph()
    {
        mCulture = Page.UICulture;

        if ((HttpContext.Current != null) && (HttpContext.Current.Request != null) && (QueryHelper.GetString("UILang", String.Empty) != String.Empty))
        {
            this.UICulture = QueryHelper.GetString("UILang", String.Empty);
            mCulture = QueryHelper.GetString("UILang", String.Empty);
        }
        else
        {
            if (CMSContext.CurrentDocument != null)
            {
                mCulture = CMSContext.CurrentDocument.DocumentCulture;
            }
            else
            {
                mCulture = System.Globalization.CultureInfo.CurrentUICulture.IetfLanguageTag;
            }
        }


        Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(mCulture);
        Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(mCulture);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        //check if it is request for saved graph - by graph guid
        sGraphGuid = QueryHelper.GetGuid("graphguid", Guid.Empty);
        if (sGraphGuid != Guid.Empty)
        {
            SavedGraphInfo sGraphInfo = SavedGraphInfoProvider.GetSavedGraphInfo(sGraphGuid);
            if (sGraphInfo != null)
            {
                SavedReportInfo savedReport = SavedReportInfoProvider.GetSavedReportInfo(sGraphInfo.SavedGraphSavedReportID);
                ReportInfo report = ReportInfoProvider.GetReportInfo(savedReport.SavedReportReportID);

                //check graph security settings
                if (report.ReportAccess != ReportAccessEnum.All)
                {
                    if (!CMSContext.CurrentUser.IsAuthenticated())
                    {
                        Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                        return;
                    }
                    else
                    {
                        // Check 'Read' permission
                        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Read"))
                        {
                            UrlHelper.Redirect("~/CMSSiteManager/accessdenied.aspx?resource=cms.reporting&permission=Read");
                        }
                    }
                }

                //send response with image data
                SendGraph(sGraphInfo);
                return;
            }
        }
        else //generate graph on the fly from report data
        {
            int graphId = QueryHelper.GetInteger("graphid", 0);
            string[] httpParameters = QueryHelper.GetString("parameters", String.Empty).Split(';');
            reportGraph = ReportGraphInfoProvider.GetReportGraphInfo(graphId);

            if (reportGraph != null)
            {
                ReportInfo report = ReportInfoProvider.GetReportInfo(reportGraph.GraphReportID);
                //check graph security settings
                if (report.ReportAccess != ReportAccessEnum.All)
                {
                    if (!CMSContext.CurrentUser.IsAuthenticated())
                    {
                        Response.StatusCode = (int)System.Net.HttpStatusCode.Unauthorized;
                        return;
                    }
                    else
                    {
                        // Check 'Read' permission
                        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Read"))
                        {
                            UrlHelper.Redirect("~/CMSSiteManager/accessdenied.aspx?resource=cms.reporting&permission=Read");
                        }
                    }
                }

                string aliasPath = "";

                object[,] parameters = null;

                // must have 2 parameters
                if ((httpParameters.Length > 0) && ((httpParameters.Length % 2) == 0))
                {
                    int paramCount = httpParameters.Length / 2;

                    // User parameters
                    parameters = new object[paramCount, 3];
                    for (int i = 0; i < paramCount; i++)
                    {
                        parameters[i, 0] = "@" + ValidationHelper.GetIdentificator(httpParameters[i * 2]);
                        parameters[i, 1] = httpParameters[i * 2 + 1];
                        if (httpParameters[i * 2].ToLower() == "aliaspathgraph")
                        {
                            aliasPath = httpParameters[i * 2 + 1];
                        }
                    }

                    TreeNode node = null;
                    if (aliasPath != "")
                    {
                        node = TreeHelper.SelectSingleNode(aliasPath);
                    }

                    // Context parameters (only when not stored procedure)
                    object[,] contextParams = null;
                    int contextParamsCount = 0;
                    if (!reportGraph.GraphQueryIsStoredProcedure)
                    {
                        contextParams = SpecialFunctions.ConvertDataRowToParams(CMSContext.GetContextParametersDataRow(), null);
                        contextParamsCount = contextParams.GetUpperBound(0) + 1;
                        paramCount += contextParamsCount;
                    }

                    object[,] allParams = null;

                    if (contextParams == null)
                    {
                        // Use just the user parameters
                        allParams = parameters;
                    }
                    else
                    {
                        allParams = new object[paramCount, 3];

                        // Context parameters
                        for (int j = 0; j < contextParamsCount; j++)
                        {
                            allParams[j, 0] = contextParams[j, 0];
                            allParams[j, 1] = contextParams[j, 1];
                            allParams[j, 2] = contextParams[j, 2];

                            if (node != null)
                            {
                                switch (contextParams[j, 0].ToString().ToLower())
                                {
                                    case "@cmscontextcurrentaliaspath":
                                        allParams[j, 1] = aliasPath;
                                        break;

                                    case "@cmscontextcurrentdocumentname":
                                        allParams[j, 1] = node.DocumentName;
                                        break;

                                    case "@cmscontextcurrentnodeid":
                                        allParams[j, 1] = node.NodeID;
                                        break;

                                    case "@cmscontextcurrentdocumentid":
                                        allParams[j, 1] = node.DocumentID;
                                        break;

                                    case "@cmscontextcurrentdocumentnamepath":
                                        allParams[j, 1] = node.DocumentNamePath;
                                        break;
                                }
                            }
                        }

                        // User parameters
                        for (int j = 0; j <= parameters.GetUpperBound(0); j++)
                        {
                            allParams[j + contextParamsCount, 0] = parameters[j, 0];
                            allParams[j + contextParamsCount, 1] = parameters[j, 1];
                            allParams[j + contextParamsCount, 2] = parameters[j, 2];
                        }
                    }

                    ContextResolver resolver = CMSContext.CurrentResolver.CreateChild();

                    try
                    {
                        resolver.SourceParameters = allParams;

                        reportGraph.GraphQuery = resolver.ResolveMacros(reportGraph.GraphQuery);
                        dsGraphData = ReportGraphInfoProvider.GetGraphData(reportGraph, allParams);
                    }
                    catch (Exception ex)
                    {
                        // Log the error
                        EventLogProvider ev = new EventLogProvider();
                        ev.LogEvent("Get report graph", "E", ex);

                        // Create invalid graph as a result
                        ReportGraph gr = new ReportGraph();
                        byte[] invalidGraph = gr.CreateInvalidGraph(reportGraph);

                        if (invalidGraph != null)
                        {
                            SendGraph("image/png", invalidGraph);
                        }
                        return;
                    }

                    // Create the graph
                    ReportGraph graph = new ReportGraph();
                    reportGraph.GraphTitle = resolver.ResolveMacros(reportGraph.GraphTitle);
                    reportGraph.GraphXAxisTitle = resolver.ResolveMacros(reportGraph.GraphXAxisTitle);
                    reportGraph.GraphYAxisTitle = resolver.ResolveMacros(reportGraph.GraphYAxisTitle);

                    // Create graph image
                    byte[] createdGraph = graph.CreateGraph(reportGraph, dsGraphData);

                    // If ok, send response with image data
                    if (createdGraph != null)
                    {
                        SendGraph("image/png", createdGraph);
                    }
                    return;
                }
            }
        }

        // Bad parameters, guid ... -> not found
        RequestHelper.Respond404();
    }


    /// <summary>
    /// Sends the graph
    /// </summary>
    /// <param name="graphObj">Graph obj containing graph</param>
    protected void SendGraph(SavedGraphInfo graphObj)
    {
        if (graphObj != null)
        {
            SendGraph(graphObj.SavedGraphMimeType, graphObj.SavedGraphBinary);
        }
    }


    /// <summary>
    /// Sends the graph
    /// </summary>
    /// <param name="mimeType">response mime type</param>
    /// <param name="graph">raw data to be sent</param>
    protected void SendGraph(string mimeType, byte[] graph)
    {
        // Clear response.
        Response.Cookies.Clear();
        Response.Clear();

        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);

        // Prepare response
        Response.ContentType = mimeType;
        Response.OutputStream.Write(graph, 0, graph.Length);

        //RequestHelper.CompleteRequest();
        RequestHelper.EndResponse();
    }
}
