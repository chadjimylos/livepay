﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports LivePay_ESBBridge
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Partial Class CMSTemplates_LivePay_UCRegisteredUsers
    Inherits CMSUserControl

    Private LivePayMerchant As String

#Region " Properties "
    Protected ReadOnly Property MerchantID() As Integer
        Get
            Dim iMerchantID As Integer = 1
            iMerchantID = CMSContext.CurrentDocument.GetValue("MerchantID")
            Return iMerchantID
        End Get
    End Property

    Protected ReadOnly Property SelectedPrice() As String
        Get
            Dim sPrice As String = "0"
            If Not String.IsNullOrEmpty(Request("SelPrice")) Then
                sPrice = Request("SelPrice")
            End If
            Return sPrice
        End Get
    End Property

    Private MinimumTransactionAmount As Integer = 0
    Private MaximumTransactionAmount As Integer = 0
    Private MaximumTransactionsPerDayCard As Integer = 0
    Private MaximumTransactionsPerDayGlobally As Integer = 0
    Private MaximumSumTransactionsPerDayInCard As Integer = 0
    Private MaxTransactionsPerSessionPerCard As Integer = 0
    Private DTMerchantCardTypes As DataTable = Nothing
    Private CompanyName As String = String.Empty
    Private CompanyLogo As String = String.Empty
    Private MerchantValidCards As String = String.Empty
#End Region

#Region "Methods"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        LivePayMerchant = CMSContext.CurrentDocument.NodeID

        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        ElseIf Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
        CreateCustomFields()

    End Sub

    Public Overloads Sub ReloadData()
        ' Response.Redirect("/CMSTemplates/LivePay/xmlHttpMerchants.aspx")
        If IsNothing(Session("UserTransactions")) Then
            Session("UserTransactions") = 0
        End If
        Tab2_RbSavedCard.Attributes.Add("onclick", String.Concat("ChangeRadioChoice('", Tab2_RbSavedCard.ClientID, "','", Tab2_RbNewCard.ClientID, "')"))
        Tab2_RbNewCard.Attributes.Add("onclick", String.Concat("ChangeRadioChoice('", Tab2_RbNewCard.ClientID, "','", Tab2_RbSavedCard.ClientID, "')"))
        CreateCustomFields()
        SetDefaults()
        GetMerchantLimits()
        GetUserCards()
        If Me.SelectedPrice <> "0" Then
            Tab1_txtPrice.Text = SelectedPrice
        End If
        Dim IsLogedIn As Boolean = CMSContext.CurrentUser.IsPublic()
        If IsLogedIn Then
            ForRegUsersTop.Style.Add("display", "none")
            Tab2_RbNewCardDivCont.Style.Add("display", "none")
            Tab2_RbNewCard.Checked = True
            ForUnRegUsers_PhoneMail.Style.Add("display", "")
            ForRegUsersChoiceToSave.Style.Add("display", "none")
            JsScript(String.Concat("; ChangeRadioChoice('", Tab2_RbNewCard.ClientID, "','", Tab2_RbSavedCard.ClientID, "'); "))
        End If
    End Sub

    Private Sub SetErrorMessages()
        Dim dt As DataTable = DBConnection.GetErrorMessages(1)
        Dim JsStringMessages As String = String.Empty
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim ErrorMessageStr As String = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("ErrorMessageGR").ToString, "|en-us=", dt.Rows(i)("ErrorMessageEN").ToString, "$}"))
            ErrorMessageStr = Replace(ErrorMessageStr, "'", "\'")
            JsStringMessages = String.Concat(JsStringMessages, " ", dt.Rows(i)("CodeName").ToString, " = '", ErrorMessageStr, "'; ")
        Next
        JsStringMessages = String.Concat(JsStringMessages, " document.getElementById('", Tab3_lblTerms.ClientID, "').value = TermsOfUse;  ")
        JsScript(JsStringMessages)
    End Sub

    Private Sub GetMerchantLimits()
        SetErrorMessages()
        Dim ds As DataSet = DBConnection.GetMerchantLimits(Me.MerchantID)

        Dim dt As DataTable = ds.Tables(0)
        If dt.Rows.Count > 0 Then
            Dim row As DataRow = dt.Rows(0)
            MinimumTransactionAmount = row("MinimumTransactionAmount")
            MaximumTransactionAmount = row("MaximumTransactionAmount")
            MaximumTransactionsPerDayCard = row("MaximumTransactionsPerDayCard")
            MaximumTransactionsPerDayGlobally = row("MaximumTransactionsPerDayGlobally")
            MaximumSumTransactionsPerDayInCard = row("MaximumSumTransactionsPerDayInCard")
            CompanyName = row("CompanyName").ToString
            CompanyLogo = row("Logo").ToString
            HiddenMinimumTransactionAmount.Text = row("MinimumTransactionAmount").ToString
            HiddenMaximumTransactionAmount.Text = row("MaximumTransactionAmount").ToString
            HiddenMaximumTransactionsPerDayCard.Text = row("MaximumTransactionsPerDayCard").ToString
            HiddenMaximumTransactionsPerDayGlobally.Text = row("MaximumTransactionsPerDayGlobally").ToString
            HiddenMaximumSumTransactionsPerDayInCard.Text = row("MaximumSumTransactionsPerDayInCard").ToString
            HiddenCompanyName.Text = row("CompanyName").ToString
            Session("NewPayment_Merchant_LivePay_ID") = row("LivePayID").ToString
            HiddenMerchantID.Text = MerchantID

            '--- Per Session
            Dim SessionLimits As Integer = 10
            HiddenMaxTransactionsPerSessionPerCard.Text = SessionLimits
            If Session("UserTransactions") > SessionLimits Then

                GotToTransErrorPage("SessionError")
            End If

            Dim UserID As Integer = CMSContext.CurrentUser.UserID
            Dim dtMaxMerchantTransLimit As DataTable = DBConnection.GetMerchantTransaction(UserID, Me.MerchantID, 0, String.Empty, 2)
            If dtMaxMerchantTransLimit.Rows.Count > 0 Then
                Dim MerchantsTrans As Integer = dtMaxMerchantTransLimit.Rows(0)("TransCountPerMerchant")
                If MerchantsTrans > MaximumTransactionsPerDayGlobally Then

                    GotToTransErrorPage("MaximumTransError")
                End If
            End If

            '--- Accepted Card Types
            DTMerchantCardTypes = ds.Tables(1)
            For i As Integer = 0 To DTMerchantCardTypes.Rows.Count - 1
                If MerchantValidCards = String.Empty Then
                    MerchantValidCards = String.Concat("[", DTMerchantCardTypes.Rows(i)("itemID").ToString, "]")
                Else
                    MerchantValidCards = String.Concat(MerchantValidCards, ",[", DTMerchantCardTypes.Rows(i)("itemID").ToString, "]")
                End If
                '---- Card Types DropDown

            Next
            HiddenMerchantValidCards.Text = MerchantValidCards

            Tab2_ddlCardType.Items.Insert(0, New ListItem("Επιλέξτε Τύπο Κάρτας", String.Empty))
            Tab2_ddlCardType.Items.Insert(1, New ListItem("Visa", 0))
            Tab2_ddlCardType.Items.Insert(2, New ListItem("MasterCard", 1))

        End If
    End Sub

    Private Sub GotToTransErrorPage(ByVal ErrorName As String)
        Select Case ErrorName
            Case "SessionError"
                JsScript("alert('Session limit!');location.href='';")
            Case "MaximumTransError"
                JsScript("alert('Maximum Transactions PerDay!');location.href='';")
        End Select


    End Sub

    Private Sub GetUserCards()
        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = DBConnection.GetUserCards(UserID)
        Dim dtFinal As DataTable = dt
        dtFinal.Columns.Add("Text", GetType(String), "FriendlyName + ' - xxxxxxxx-xxxx-' + LastFor")
        dtFinal.Columns.Add("Value", GetType(String), "CardID + '_' + CardTypeID")
        Tab2_ddlSavedCard.DataTextField = "Text"
        Tab2_ddlSavedCard.DataValueField = "Value"
        Tab2_ddlSavedCard.DataSource = dtFinal
        Tab2_ddlSavedCard.DataBind()
        Tab2_ddlSavedCard.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=Επιλογή Κάρτας|en-us=Select Card$}"), String.Empty))

        Tab2_ddlSavedCard.Attributes.Add("onchange", String.Concat("CheckValidCard()"))

        If dt.Rows.Count = 0 Then

            ForRegUsersTop.Style.Add("display", "none")
            Tab2_RbNewCardDivCont.Style.Add("display", "none")
            Tab2_RbNewCard.Checked = True

            JsScript(String.Concat("; ChangeRadioChoice('", Tab2_RbNewCard.ClientID, "','", Tab2_RbSavedCard.ClientID, "'); "))

        End If

    End Sub

    Private Sub SetDefaults()
        Tab2_ChkSaveDetails.Attributes.Add("onclick", "CardForSave(this)")
        Tab1_ReqFldVal_Price.ErrorMessage = ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}")
        'Tab1_ReqularFldVal_Price.ErrorMessage = ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}")

        Tab2_RbSavedCard.Text = ResHelper.LocalizeString("{$=Επιλογή Αποθηκευμένης Κάρτας|en-us=Επιλογή Αποθηκευμένης Κάρτας$}")
        Tab2_RbNewCard.Text = ResHelper.LocalizeString("{$=Εισάγετε Νέα Κάρτα|en-us=Εισάγετε Νέα Κάρτα$}")
        Tab2_ReqFldVal_FriendName.ErrorMessage = ResHelper.LocalizeString("{$=Φιλική Ονομασία|en-us=Φιλική Ονομασία$}")
        Tab2_ChlIWantInvoice.Text = ResHelper.LocalizeString("{$=Επιθυμώ να μου αποσταλεί τιμολόγιο|en-us=Επιθυμώ να μου αποσταλεί τιμολόγιο$}")

        For i As Integer = 1 To 12
            Dim dtm As Date = String.Concat("1/", i, "/2000")
            Tab2_DdlCardMonth.Items.Insert(i - 1, New ListItem(dtm.ToString("MMMM"), i))
        Next

        For i As Integer = 0 To 3
            Dim Years As Integer = Year(Now) + i
            Tab2_DdlCardYear.Items.Insert(i, New ListItem(Years, Years))
        Next

        ddlDoy.DataSource = DBConnection.GetTaxOffices
        ddlDoy.DataTextField = ResHelper.LocalizeString("{$=TitleGr|en-us=TitleEn$}")
        ddlDoy.DataValueField = "ItemID"
        ddlDoy.DataBind()
        ddlDoy.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=Επιλέξτε ΔΟΥ|en-us=Select TaxOffice$}"), String.Empty))

    End Sub

    Private Sub CreateCustomFields()
        SetErrorMessages()
        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(Me.MerchantID)
        Dim dt As DataTable = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim CustomTitleDiv As New HtmlGenericControl("div")
            CustomTitleDiv.Attributes.Add("class", "RegUsersTab1CustomTitle")
            CustomTitleDiv.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))

            Dim CustomTextBoxDiv As New HtmlGenericControl("div")
            CustomTextBoxDiv.Attributes.Add("class", "RegUsersTab2_TxtDiv")

            Dim CustomCloudDiv As New HtmlGenericControl("div")
            CustomCloudDiv.InnerHtml = String.Concat("<a id='CustomCloud_", i, "' href='#'></a>")
            CustomTextBoxDiv.Controls.Add(CustomCloudDiv)
            Dim txtbx As New TextBox
            txtbx.CssClass = "RegUsersTab1CustomTxt"
            txtbx.Width = "260"
            txtbx.ID = "CustomField_" & i
            txtbx.MaxLength = dt.Rows(i)("maxlength").ToString
            txtbx.Attributes.Add("onclick", "CountValidations=0;")
            CustomTextBoxDiv.Controls.Add(txtbx)

            Dim CustomDivValidation As New HtmlGenericControl("div")
            CustomDivValidation.Style.Add("display", "none")

            Dim CustomValidat As New CustomValidator
            CustomValidat.ValidationGroup = "Tab1_PayForm"
            CustomValidat.ValidateEmptyText = True
            CustomValidat.ControlToValidate = txtbx.ClientID
            CustomValidat.ClientValidationFunction = "validateControls"
            CustomValidat.ErrorMessage = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
            CustomValidat.Attributes.Add("ControlValidated", txtbx.ClientID)
            CustomValidat.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", i))
            CustomValidat.Attributes.Add("CustomValidatorErrorMessage", ResHelper.LocalizeString(String.Concat("{$=Συμπληρώστε το πεδίο <b>", dt.Rows(i)("NameFieldGr").ToString, "</b>|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}")))
            CustomDivValidation.Controls.Add(CustomValidat)
            CustomTextBoxDiv.Controls.Add(CustomDivValidation)
            '--- Add Controls
            CustomFields.Controls.Add(CustomTitleDiv)
            CustomFields.Controls.Add(CustomTextBoxDiv)
            '-----
            Dim MainDivCloud As New HtmlGenericControl("div")
            Dim CloudID As String = "CustomField_" & i & "_Cloud"
            MainDivCloud.Attributes.Add("class", "RegUsersMainDivCloud")
            MainDivCloud.Attributes.Add("onmouseover", String.Concat("ShowClound('", CloudID, "',this)"))
            MainDivCloud.Attributes.Add("onmouseout", String.Concat("HideClound('", CloudID, "')"))

            Dim imgInfo As New HtmlImage
            imgInfo.Src = "/App_Themes/LivePay/InfoBtn.png"
            MainDivCloud.Controls.Add(imgInfo)

            Dim DivCloud As New HtmlGenericControl("div")
            DivCloud.Attributes.Add("class", "RegUsersMainClouds")
            DivCloud.Style.Add("display", "none")
            DivCloud.Attributes.Add("id", CloudID)

            DivCloud.Controls.Add(CreatCloud(ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("DescriptionGR").ToString, "|en-us=", dt.Rows(i)("DescriptionEn").ToString, "$}"))))

            MainDivCloud.Controls.Add(DivCloud)
            '--- Add Controls
            CustomFields.Controls.Add(MainDivCloud)

            Dim DivClear As New HtmlGenericControl("div")
            DivClear.Attributes.Add("class", "Clear")
            CustomFields.Controls.Add(DivClear)
            '-----
            'CustomCloud_", i
            CreateCustomFieldsForTab3(dt.Rows(i)("NameFieldGr").ToString, dt.Rows(i)("NameFieldEn").ToString, i)
        Next


        Dim CloseCloudsFn As String = "function CloseAllClouds() { "
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardMsgNewCard').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#NewCardFullNameMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#NewCardCVVMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardCVV2Msg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardMsgSec').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_CompanyNameMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_OccupationMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_AddressMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_TKMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_CityMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_AFMMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_DOYMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#CardTypeMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Tab1_PriceMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#NewCardFriendly').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#UnRegUserPhoneMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#UnRegUserEmailMsg').poshytip('hide'); ")
        '---- Custom Clouds
        For i As Integer = 0 To dt.Rows.Count - 1
            CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#CustomCloud_", i, "').poshytip('hide'); ")
        Next
        '---- Custom Clouds
        CloseCloudsFn = String.Concat(CloseCloudsFn, "} ")
        lit_JsScript.Text = "<script> " & CloseCloudsFn & " </script>"
    End Sub

    Private Sub CreateCustomFieldsForTab3(ByVal NameGR As String, ByVal NameEN As String, ByVal iCount As Integer)
        Dim TitleDiv As New HtmlGenericControl("div")
        Dim ValueDiv As New HtmlGenericControl("div")
        Dim ClearDiv As New HtmlGenericControl("div")
        TitleDiv.Attributes.Add("class", "RegUserTab3TitleB")
        TitleDiv.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", NameGR, "|en-us=", NameEN, "$}"))
        ValueDiv.Attributes.Add("class", "RegUsersTab2_TxtDiv")
        ValueDiv.InnerHtml = String.Concat("<span class='RegUSerTab3Control' id='Tab3_Span_CustomField_", iCount, "'></span>")
        ClearDiv.Attributes.Add("class", "Clear")
        With Tab3_CustomFields.Controls
            .Add(TitleDiv)
            .Add(ValueDiv)
            .Add(ClearDiv)
        End With
    End Sub

    Private Function CreatCloud(ByVal txt As String) As HtmlGenericControl
        Dim MainDin As New HtmlGenericControl("div")
        Dim TopCloud As New HtmlGenericControl("div")
        TopCloud.Attributes.Add("class", "CloudTop")

        Dim RptCloud As New HtmlGenericControl("div")
        RptCloud.Attributes.Add("class", "RptCloud")

        Dim TextCloud As New HtmlGenericControl("div")
        TextCloud.Attributes.Add("class", "TextCloud")
        TextCloud.InnerHtml = txt
        RptCloud.Controls.Add(TextCloud)
        Dim BottomCloud As New HtmlGenericControl("div")
        BottomCloud.Attributes.Add("class", "BottomCloud")

        With MainDin.Controls
            .Add(TopCloud)
            .Add(RptCloud)
            .Add(BottomCloud)
        End With
        Return MainDin
    End Function
#End Region

#Region " Tabs Events "

    Protected Sub Tab1_BtnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab1_BtnNext.Click
        If Page.IsValid Then
            JsScript("ShowHideTabs('none','','none');")
        End If
    End Sub

    Protected Sub Tab2_btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab2_btnNext.Click
        If Page.IsValid Then
            Tab3_lblPayTo.Text = HiddenCompanyName.Text
            Tab3_lblPrice.Text = Tab1_txtPrice.Text
            If Tab2_RbNewCard.Checked Then
                Tab3_lblCard.Text = String.Concat("xxxxxxxx-xxxx-", Right(Tab2_txtCardNo.Text, 4))
            Else
                Tab3_lblCard.Text = Split(Tab2_ddlSavedCard.SelectedItem.Text, " - ")(1).ToString
            End If
            Dim jsCustomFeelFields As String = String.Empty
            Dim ds As DataSet = DBConnection.GetMerchantCustomFields(Me.MerchantID)
            Dim dt As DataTable = ds.Tables(0)
            JsScript(String.Concat("SetCustomFieldsValues('CustomField_','Tab3_Span_CustomField_','", dt.Rows.Count - 1, "');ShowHideTabs('none','none','');"))
        End If
    End Sub

    Protected Sub Tab2_NextBtnInvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab2_NextBtnInvoice.Click
        If Page.IsValid Then
            Tab3_lblPayTo.Text = HiddenCompanyName.Text
            Tab3_lblPrice.Text = Tab1_txtPrice.Text
            If Tab2_RbNewCard.Checked Then
                Tab3_lblCard.Text = String.Concat("xxxxxxxx-xxxx-", Right(Tab2_txtCardNo.Text, 4))
            Else
                Tab3_lblCard.Text = Split(Tab2_ddlSavedCard.SelectedItem.Text, " - ")(1).ToString
            End If
            Dim jsCustomFeelFields As String = String.Empty
            Dim ds As DataSet = DBConnection.GetMerchantCustomFields(Me.MerchantID)
            Dim dt As DataTable = ds.Tables(0)
            JsScript(String.Concat("SetCustomFieldsValues('CustomField_','Tab3_Span_CustomField_','", dt.Rows.Count - 1, "');ShowHideTabs('none','none','');"))
        End If
    End Sub

#End Region


    Protected Sub Tab3_btnComplete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab3_btnComplete.Click
        If Page.IsValid Then
            Dim CatchError As String = String.Empty
            Session("MakePaymentError") = String.Empty
            Dim TransID As Integer = 0
            Try
                Dim w As New LivePay_ESBBridge.Bridge
                Dim obj As New LivePay_ESBBridge.PaymentObj
                Dim UserID As Integer = 0
                Dim UserEmail As String = Tab2_txtEmail.Text
                Dim USerPhone As String = Tab2_txtPhone.Text
                Dim CustomerName As String = Tab2_txtFullNameOwner.Text
                Dim CardNo As String = Tab2_txtCardNo.Text
                Dim CardCvv2 As String = Tab2_txtCVV2.Text
                Dim CardExpDate As String = String.Empty
                Dim CardDescrID As Integer = 0
                Dim ExpMonth As String = Tab2_DdlCardMonth.SelectedValue
                Dim ExpYear As String = Tab2_DdlCardYear.Text
                Dim CardID As Integer = 0
                Dim IsLogedIn As Boolean = CMSContext.CurrentUser.IsPublic()
                If IsLogedIn = False Then
                    UserID = CMSContext.CurrentUser.UserID
                    UserEmail = CMSContext.CurrentUser.Email
                    USerPhone = CMSContext.CurrentUser.GetValue("UserPhone").ToString
                End If

                If Tab2_RbSavedCard.Checked Then          '----- If Saved Cards is Checked
                    CardID = Split(Tab2_ddlSavedCard.SelectedValue, "_")(0)
                    Dim DtCard As DataTable = DBConnection.GetUserCard(UserID, CardID)
                    If DtCard.Rows.Count > 0 Then
                        CardNo = DtCard.Rows(0)("CardNumber").ToString
                        CardCvv2 = Tab2_txtCVV2Top.Text
                        ExpMonth = DtCard.Rows(0)("MonthExpiration").ToString
                        ExpYear = DtCard.Rows(0)("YearExpiration").ToString
                        CustomerName = DtCard.Rows(0)("FullName").ToString
                        CardDescrID = DtCard.Rows(0)("CardDescrID").ToString
                    End If
                Else
                    CardDescrID = Tab2_ddlCardType.SelectedValue
                End If
                If ExpMonth.Length = 1 Then
                    ExpMonth = String.Concat("0", ExpMonth)
                End If
                CardExpDate = String.Concat(Right(ExpYear, 2), ExpMonth)
                Dim Price As Decimal = Replace(Tab1_txtPrice.Text, ".", ",")
                Dim Doy As Integer = 0
                If Tab2_ChlIWantInvoice.Checked Then '--- If With Invoice is checked
                    Doy = ddlDoy.SelectedValue
                End If
                '--- Save New Card
                If Tab2_ChkSaveDetails.Checked AndAlso Tab2_RbNewCard.Checked Then
                    Dim dtNewCard As DataTable = DBConnection.InsertNewCard(UserID, Tab2_txtFriendName.Text, CardDescrID, CardNo, CustomerName, ExpMonth, ExpYear)
                    If dtNewCard.Rows.Count > 0 Then
                        CardID = dtNewCard.Rows(0)("NewCardID").ToString
                    End If
                End If
                '-----------------
                '--Start--- I take the New Trans ID Status = 0 (before check)
                Dim dtTrans As DataTable = DBConnection.InsertMerchantTransaction(Me.MerchantID, UserID, CardID, CardNo, CustomerName, Price, USerPhone, UserEmail, Tab2Inv_CompanyName.Text, _
                                                                                  Tab2Inv_Occupation.Text, Tab2Inv_Address.Text, Tab2Inv_TK.Text, Tab2Inv_City.Text, Tab2Inv_AFM.Text, Doy, CardDescrID, 0)

                Dim TransGUID As Guid
                If dtTrans.Rows.Count > 0 Then
                    TransID = dtTrans.Rows(0)("TransID")
                    TransGUID = New Guid(dtTrans.Rows(0)("TransGUID").ToString)
                End If
                '--End--- I take the New Trans ID Status = 0 (before check)

                obj.MerchantId = Session("NewPayment_Merchant_LivePay_ID") ' Me.MerchantID ----- Na Bro To Merchant ID to kanoniko
                obj.CustomerId = UserID
                obj.CustomerEmail = UserEmail
                obj.CustomerTelephone = USerPhone
                obj.CustomerComments = Tab1_txtNotes.Text
                obj.CustomerName = CustomerName
                obj.CardCvv2 = CardCvv2
                obj.CardExpiryDate = CardExpDate
                obj.CardNumber = CardNo
                obj.TransactionId = TransID
                obj.TransactionAmount = Tab1_txtPrice.Text '----
                obj.CardType = CardDescrID
                Dim ds As DataSet = DBConnection.GetMerchantCustomFields(Me.MerchantID)
                Dim dt As DataTable = ds.Tables(0)
                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim ControlID As String = String.Concat("CustomField_", i)
                    Dim MyControl As TextBox = DirectCast(Me.FindControl(ControlID), TextBox)
                    If Not IsNothing(MyControl) Then
                        Select Case i
                            Case 0
                                obj.Info1 = MyControl.Text
                            Case 1
                                obj.Info2 = MyControl.Text
                            Case 2
                                obj.Info3 = MyControl.Text
                            Case 3
                                obj.Info4 = MyControl.Text
                            Case 4
                                obj.Info5 = MyControl.Text
                            Case 5
                                obj.Info6 = MyControl.Text
                            Case 6
                                obj.Info7 = MyControl.Text
                            Case 7
                                obj.Info8 = MyControl.Text
                            Case 8
                                obj.Info9 = MyControl.Text
                            Case 9
                                obj.Info10 = MyControl.Text
                        End Select
                    End If
                Next

                Dim res As LivePay_ESBBridge.PaymentResponse = w.MakePayment(obj)
                Dim Result As Boolean = res.Result


                If Result Then '---- Success
                    DBConnection.UpdateTransactionWithStatusMes(TransID, 2, 0, "Success")
                    Dim MailHandler As New MailContext()



                    MailHandler.UserPhone = USerPhone
                    MailHandler.Transdate = Now.ToShortDateString
                    MailHandler.Transcode = TransID
                    MailHandler.MerchantName = ""
                    MailHandler.MerchantID = Session("NewPayment_Merchant_LivePay_ID")
                    MailHandler.InvoiceBool = Tab2_ChlIWantInvoice.Checked
                    MailHandler.Comments = Tab1_txtNotes.Text
                    MailHandler.CardType = IIf(CardDescrID = 0, "Visa", "MasterCard")
                    MailHandler.CardNumber = CardNo
                    MailHandler.CardName = CustomerName
                    MailHandler.CardLastName = CustomerName
                    MailHandler.CardExpDate = CardExpDate
                    MailHandler.CardEmail = UserEmail
                    MailHandler.Amount = Tab1_txtPrice.Text
                    MailHandler.SendEmail("USERCARDSUCC", "nka1l@realize.gr", "kornebi1f@hotmail.com")


                    MailHandler.UserPhone = USerPhone
                    MailHandler.Transdate = Now.ToShortDateString
                    MailHandler.Transcode = TransID
                    MailHandler.MerchantName = ""
                    MailHandler.MerchantID = Session("NewPayment_Merchant_LivePay_ID")
                    MailHandler.InvoiceBool = Tab2_ChlIWantInvoice.Checked
                    MailHandler.Comments = Tab1_txtNotes.Text
                    MailHandler.CardType = IIf(CardDescrID = 0, "Visa", "MasterCard")
                    MailHandler.CardNumber = CardNo
                    MailHandler.CardName = CustomerName
                    MailHandler.CardLastName = CustomerName
                    MailHandler.CardExpDate = CardExpDate
                    MailHandler.CardEmail = UserEmail
                    MailHandler.Amount = Tab1_txtPrice.Text
                    MailHandler.SendEmail("MERCHCARDSUCC", "nka1l@realize.gr", "kornebi1f@hotmail.com")


                    Session("UserTransactions") = Session("UserTransactions") + 1
                    Session("MakePaymentError") = String.Empty
                    Response.Redirect("~/TransactionReceipt.aspx?TransID=" & TransGUID.ToString)
                Else '--- Failed

                    DBConnection.UpdateTransactionWithStatusMes(TransID, 1, 0, res.ErrorCode & " : " & res.ErrorMessage)
                    Session("MakePaymentError") = res.ErrorCode & " : " & res.ErrorMessage
                    Response.Redirect("~/TransactionReceipt.aspx?Result=ESBerror")
                End If

            Catch ex As Exception
                CatchError = ex.ToString
            End Try

            If CatchError <> String.Empty Then
                DBConnection.UpdateTransactionWithStatusMes(TransID, 1, 0, CatchError) '------ Update Transaction Set Status = 1 (Failed)
                Session("MakePaymentError") = CatchError
                Response.Redirect("~/TransactionReceipt.aspx?Result=CatchError")
            End If



            'Dim ErrorMessage As String = String.Concat("<font color='red'>", res.ErrorCode, "</font>:", res.ErrorMessage)
        End If
    End Sub


    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub
End Class
