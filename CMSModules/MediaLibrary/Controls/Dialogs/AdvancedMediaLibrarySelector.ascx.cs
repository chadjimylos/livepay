﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;

using CMS.UIControls;
using CMS.MediaLibrary;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.ExtendedControls;
using CMS.UIControls.AdminControls;
using CMS.FormControls;


public partial class CMSModules_MediaLibrary_Controls_Dialogs_AdvancedMediaLibrarySelector : CMSUserControl
{
    #region "Events & delegates"

    /// <summary>
    /// Event fired when library selection changed.
    /// </summary>
    public event EventHandler LibraryChanged;

    #endregion


    #region "Private variables"

    private AvailableGroupsEnum mGroups = AvailableGroupsEnum.None;
    private AvailableLibrariesEnum mGlobalLibraries = AvailableLibrariesEnum.None;
    private AvailableLibrariesEnum mGroupLibraries = AvailableLibrariesEnum.None;
    private AvailableSitesEnum mSites = AvailableSitesEnum.All;

    private string mGlobalLibaryName = "";
    private int mSelectGroupID = 0;
    private int mSelectedLibraryID = 0;
    private string mGroupName = "";
    private string mGroupLibraryName = "";
    private string mSiteToSelect = "";

    private string mLibraryName = "";

    // Group selector
    private string controlPath = "~/CMSModules/Groups/FormControls/CommunityGroupSelector.ascx";
    private FormEngineUserControl groupsSelector = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Indicates what groups should be available.
    /// </summary>
    public AvailableGroupsEnum Groups
    {
        get
        {
            return this.mGroups;
        }
        set
        {
            this.mGroups = value;
        }
    }


    /// <summary>
    /// Indicates what libraries should be available.
    /// </summary>
    public AvailableLibrariesEnum GlobalLibraries
    {
        get
        {
            return this.mGlobalLibraries;
        }
        set
        {
            this.mGlobalLibraries = value;
        }
    }


    /// <summary>
    /// Indicates what group libraries should be available.
    /// </summary>
    public AvailableLibrariesEnum GroupLibraries
    {
        get
        {
            return this.mGroupLibraries;
        }
        set
        {
            this.mGroupLibraries = value;
        }
    }


    /// <summary>
    /// Available sites.
    /// </summary>
    public AvailableSitesEnum Sites
    {
        get
        {
            return this.mSites;
        }
        set
        {
            this.mSites = value;
        }
    }


    /// <summary>
    /// Name of the global library.
    /// </summary>
    public string GlobalLibaryName
    {
        get
        {
            return this.mGlobalLibaryName;
        }
        set
        {
            this.mGlobalLibaryName = value;
            this.mLibraryName = value;
        }
    }


    /// <summary>
    /// Name of the group.
    /// </summary>
    public string GroupName
    {
        get
        {
            return this.mGroupName;
        }
        set
        {
            this.mGroupName = value;
        }
    }


    /// <summary>
    /// ID of the group to select.
    /// </summary>
    public int SelectedGroupID
    {
        get
        {
            return this.mSelectGroupID;
        }
        set
        {
            this.mSelectGroupID = value;
        }
    }


    /// <summary>
    /// ID of the library to select.
    /// </summary>
    public int SelectedLibraryID
    {
        get
        {
            return this.mSelectedLibraryID;
        }
        set
        {
            this.mSelectedLibraryID = value;
        }
    }


    /// <summary>
    /// Name of the group library.
    /// </summary>
    public string GroupLibraryName
    {
        get
        {
            return this.mGroupLibraryName;
        }
        set
        {
            this.mGroupLibraryName = value;
            this.mLibraryName = value;
        }
    }


    /// <summary>
    /// Current library ID.
    /// </summary>
    public int LibraryID
    {
        get
        {
            return this.librarySelector.MediaLibraryID;
        }
        set
        {
            this.librarySelector.MediaLibraryID = value;
        }
    }


    /// <summary>
    /// Name of the library to pre-select.
    /// </summary>
    public string LibraryName
    {
        get
        {
            return this.librarySelector.MediaLibraryName;
        }
        set
        {
            this.librarySelector.MediaLibraryName = value;
        }
    }


    /// <summary>
    /// Currently selected site name.
    /// </summary>
    public string SelectedSiteName
    {
        get
        {
            if (this.siteSelector.SiteName == "")
            {
                return this.mSiteToSelect;
            }
            return this.siteSelector.SiteName;
        }
        set
        {
            this.siteSelector.SiteName = value;
            this.mSiteToSelect = value;
        }
    }


    /// <summary>
    /// ID of the currently selected site.
    /// </summary>
    public int SiteID
    {
        get
        {
            return ValidationHelper.GetInteger(this.siteSelector.DropDownSingleSelect.SelectedValue, 0);
        }
        set
        {
            this.siteSelector.SiteID = value;
        }
    }

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.siteSelector.StopProcessing = false;
        this.siteSelector.UniSelector.AllowEmpty = false;

        InitializeGroupSelector();
    }


    protected override void OnPreRender(EventArgs e)
    {
        if (!RequestHelper.IsPostBack())
        {
            // Display group selector only when group module is present
            if (ModuleEntry.IsModuleRegistered(ModuleEntry.COMMUNITY) && ModuleEntry.IsModuleLoaded(ModuleEntry.COMMUNITY) && (this.groupsSelector != null))
            {
                HandleGroupsSelection();
            }
            else
            {
                // Reload libraries
                LoadLibrarySelection();

                // Pre-select library
                PreselectLibrary();

                RaiseOnLibraryChanged();
            }
        }

        if (this.librarySelector.MediaLibraryID == 0)
        {
            SetLibrariesEmpty();
        }
        else
        {
            SetLibraries();
        }

        base.OnPreRender(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.siteSelector.StopProcessing = true;
            this.librarySelector.StopProcessing = true;
            this.Visible = false;
        }
        this.siteSelector.UniSelector.OnSelectionChanged += new EventHandler(UniSelector_OnSelectionChanged);
    }


    /// <summary>
    /// Loads selector data.
    /// </summary>
    public void LoadData()
    {
        // Initialize controls
        SetupControls();
    }


    /// <summary>
    /// Reloads content of site selector.
    /// </summary>
    public void ReloadSites()
    {
        this.siteSelector.Reload(true);
    }


    #region "Private methods"

    /// <summary>
    /// Initializes all the inner controls.
    /// </summary>
    private void SetupControls()
    {
        // Initialize site selector
        this.siteSelector.IsLiveSite = this.IsLiveSite;
        this.siteSelector.DropDownSingleSelect.AutoPostBack = true;
        this.siteSelector.UniSelector.WhereCondition = GetSitesWhere();
        this.siteSelector.Reload(true);

        this.librarySelector.IsLiveSite = this.IsLiveSite;
    }


    /// <summary>
    /// Gets WHERE condition to retrieve available sites according specified type.
    /// </summary>
    private string GetSitesWhere()
    {
        string where = "SiteStatus = 'RUNNING'";

        // If not global admin display only related sites
        if (!CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            where = SqlHelperClass.AddWhereCondition(where, "SiteID IN (SELECT SiteID FROM CMS_UserSite WHERE UserID=" + CMSContext.CurrentUser.UserID + ")");
        }

        switch (this.Sites)
        {
            case AvailableSitesEnum.OnlySingleSite:
                if (!string.IsNullOrEmpty(this.SelectedSiteName))
                {
                    where = SqlHelperClass.AddWhereCondition(where, "SiteName LIKE N'" + this.SelectedSiteName + "'");
                }
                break;

            case AvailableSitesEnum.OnlyCurrentSite:
                where = SqlHelperClass.AddWhereCondition(where, "SiteName LIKE N'" + CMSContext.CurrentSiteName + "'");
                break;

            case AvailableSitesEnum.All:
            default:
                break;
        }

        return where;
    }


    /// <summary>
    /// Disables libraries drop-down list when empty.
    /// </summary>
    private void SetLibrariesEmpty()
    {
        this.LibraryID = 0;
        this.librarySelector.Enabled = false;

        ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "DialogsDisableMenuActions", ScriptHelper.GetScript("if(DisableNewFileBtn){ DisableNewFileBtn(); } if(DisableNewFolderBtn){ DisableNewFolderBtn(); }"));
    }


    /// <summary>
    /// Enables libraries drop-down list.
    /// </summary>
    private void SetLibraries()
    {
        this.librarySelector.Enabled = true;
    }

    #endregion


    #region "Selection handler methods"

    /// <summary>
    /// Initializes group selector.
    /// </summary>
    private void HandleGroupsSelection()
    {
        // Display group selector only when group module is present
        if (ModuleEntry.IsModuleRegistered(ModuleEntry.COMMUNITY) && ModuleEntry.IsModuleLoaded(ModuleEntry.COMMUNITY) && (this.groupsSelector != null))
        {
            // Global libraries item into group selector
            if (this.GlobalLibraries != AvailableLibrariesEnum.None)
            {
                // Add special item
                this.groupsSelector.SetValue("UseDisplayNames", true);
                this.groupsSelector.SetValue("DisplayGlobalValue", true);
                this.groupsSelector.SetValue("SiteID", SiteID);
                this.groupsSelector.SetValue("GlobalValueText", ResHelper.GetString("dialogs.media.globallibraries"));
            }
            else
            {
                this.groupsSelector.SetValue("DisplayGlobalValue", false);
            }

            // If all groups should be displayed
            switch (this.Groups)
            {
                case AvailableGroupsEnum.All:
                    // Set condition to load only groups realted to the current site
                    this.groupsSelector.SetValue("WhereCondition", "(GroupSiteID=" + this.SiteID + ")");
                    break;

                case AvailableGroupsEnum.OnlyCurrentGroup:
                    // Load only current group and disable control
                    int groupId = ModuleCommands.CommunityGetCurrentGroupID();
                    this.groupsSelector.SetValue("WhereCondition", "(GroupID=" + groupId + ")");
                    break;

                case AvailableGroupsEnum.OnlySingleGroup:
                    if (!string.IsNullOrEmpty(this.GroupName))
                    {
                        this.groupsSelector.SetValue("WhereCondition", "(GroupName = N'" + this.GroupName.Replace("'", "''") + "' AND GroupSiteID=" +
                            this.SiteID + ")");
                    }
                    break;

                case AvailableGroupsEnum.None:
                    // Just '(none)' displayed in the selection
                    if (this.GlobalLibraries == AvailableLibrariesEnum.None)
                    {
                        this.groupsSelector.SetValue("DisplayNoneWhenEmpty", true);
                        this.groupsSelector.SetValue("EnabledGroups", false);
                    }
                    this.groupsSelector.SetValue("WhereCondition", "(" + SqlHelperClass.NO_DATA_WHERE + ")");
                    break;
            }

            // Reload group selector based on recently passed settings
            ((IDataUserControl)this.groupsSelector).ReloadData(true);
        }
        else
        {
            this.plcGroupSelector.Visible = false;
        }
    }


    /// <summary>
    /// Initializes group selector.
    /// </summary>
    private void InitializeGroupSelector()
    {
        if (groupsSelector == null)
        {
            if (File.Exists(MapPath(controlPath)))
            {
                groupsSelector = this.LoadControl(controlPath) as FormEngineUserControl;
                if (groupsSelector != null)
                {
                    // Set up selector
                    groupsSelector.ID = "dialogsGroupSelector";
                    groupsSelector.SetValue("DisplayCurrentGroup", false);
                    groupsSelector.SetValue("SiteID", 0);
                    groupsSelector.Changed += new FormEngineUserControl.OnChanged(groupSelector_Changed);

                    // Get uniselector and set it up
                    UniSelector us = groupsSelector.GetValue("CurrentSelector") as UniSelector;
                    if (us != null)
                    {
                        us.ReturnColumnName = "GroupID";
                    }

                    // Get dropdownlist and set it up
                    DropDownList drpGroups = groupsSelector.GetValue("CurrentDropDown") as DropDownList;
                    if (drpGroups != null)
                    {
                        drpGroups.AutoPostBack = true;
                    }

                    // Add control to panel
                    this.pnlGroupSelector.Controls.Add(groupsSelector);
                }
            }
            else
            {
                this.plcGroupSelector.Visible = false;
            }
        }
    }


    /// <summary>
    /// Initializes library selector.
    /// </summary>
    public void LoadLibrarySelection()
    {
        string where = string.Empty;

        int groupId = 0;
        if (this.groupsSelector != null)
        {
            groupId = ValidationHelper.GetInteger(this.groupsSelector.GetValue("GroupID"), -1);
        }
        // Decide what type of libraries to display
        if (groupId == -1)
        {
            this.librarySelector.Where = "(" + SqlHelperClass.NO_DATA_WHERE + ")";
            SetLibrariesEmpty();
        }
        else if (groupId == 0)
        {
            this.librarySelector.Where = GetGlobalLibrariesCondition();
        }
        else
        {
            this.librarySelector.Where = GetGroupLibrariesCondition();
        }

        this.librarySelector.SiteID = SiteID;
        this.librarySelector.ReloadData();
    }


    /// <summary>
    /// Returns WHERE condition when group libraries are being displayed.
    /// </summary>
    private string GetGroupLibrariesCondition()
    {
        string result = "";

        // Get currently selected group ID
        int groupId = ValidationHelper.GetInteger(this.groupsSelector.GetValue("GroupID"), 0);
        if (groupId > 0)
        {
            this.librarySelector.GroupID = groupId;

            switch (this.GroupLibraries)
            {
                case AvailableLibrariesEnum.OnlySingleLibrary:
                    this.librarySelector.SiteID = this.SiteID;
                    result = "(LibraryName= N'" + this.GroupLibraryName.Replace("'", "''") + "')";
                    break;

                case AvailableLibrariesEnum.OnlyCurrentLibrary:
                    result = "(LibraryID=" + ((MediaLibraryContext.CurrentMediaLibrary != null) ? MediaLibraryContext.CurrentMediaLibrary.LibraryID : 0) + ")";
                    break;

                case AvailableLibrariesEnum.All:
                default:
                    this.librarySelector.SiteID = this.SiteID;
                    break;
            }
        }

        return result;
    }


    /// <summary>
    /// Returns WHERE condition when global libraries are being displayed.
    /// </summary>
    private string GetGlobalLibrariesCondition()
    {
        // Create WHERE condition according global libraries
        switch (this.GlobalLibraries)
        {
            case AvailableLibrariesEnum.OnlyCurrentLibrary:
                int libraryId = ((MediaLibraryContext.CurrentMediaLibrary != null) ? MediaLibraryContext.CurrentMediaLibrary.LibraryID : 0);

                return "(LibraryID=" + libraryId + ")";

            case AvailableLibrariesEnum.OnlySingleLibrary:
                this.librarySelector.SiteID = this.SiteID;
                return "(LibraryName= N'" + this.GlobalLibaryName.Replace("'", "''") + "')";

            case AvailableLibrariesEnum.All:
            default:
                this.librarySelector.SiteID = this.SiteID;
                return "";
        }
    }


    /// <summary>
    /// Ensures right library is selected in the list.
    /// </summary>
    private void PreselectLibrary()
    {
        if (this.SelectedLibraryID > 0)
        {
            this.librarySelector.MediaLibraryID = this.SelectedLibraryID;
        }
    }


    /// <summary>
    /// Ensures right group is selected in the list.
    /// </summary>
    private void PreselectGroup()
    {
        if (this.SelectedGroupID >= 0)
        {
            // Get dropdownlist and set it up
            DropDownList drpGroups = groupsSelector.GetValue("CurrentDropDown") as DropDownList;
            if (drpGroups != null)
            {
                try
                {
                    drpGroups.SelectedValue = this.SelectedGroupID.ToString();
                }
                catch { }
            }
        }
    }

    #endregion


    #region "Event handlers"

    /// <summary>
    /// Handler of the event occuring when the site in selector has changed.
    /// </summary>
    void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        this.SiteID = (int)this.siteSelector.Value;

        // Reload groups selector
        HandleGroupsSelection();

        // Pre-select group
        PreselectGroup();

        // Pre-select library
        PreselectLibrary();

        // Reload libraries
        LoadLibrarySelection();

        RaiseOnLibraryChanged();
    }


    /// <summary>
    /// Handler of the event occuring when the group selector has changed.
    /// </summary>
    protected void groupSelector_Changed()
    {
        if (!UrlHelper.IsPostback())
        {
            // Pre-select group
            PreselectGroup();
        }

        if (!UrlHelper.IsPostback())
        {
            // Pre-select library
            PreselectLibrary();
        }

        // Reload libraries
        LoadLibrarySelection();

        RaiseOnLibraryChanged();
    }


    protected void librarySelector_SelectedLibraryChanged()
    {
        // Let the parent now about library selection change
        RaiseOnLibraryChanged();
    }


    /// <summary>
    /// Fires event when library changed.
    /// </summary>
    private void RaiseOnLibraryChanged()
    {
        // Fire event
        if (this.LibraryChanged != null)
        {
            this.LibraryChanged(this, null);
        }
    }

    #endregion
}
