using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;


public partial class CMSModules_PortalEngine_FormControls_SelectContainer : CMS.FormControls.FormEngineUserControl
{
    protected override void CreateChildControls()
    {
        base.CreateChildControls();
        
        // Where condition
        selectContainer.WhereCondition = "ContainerID IN (SELECT ContainerID FROM CMS_WebPartContainerSite WHERE SiteID = " + CMSContext.CurrentSiteID + ")";

        // Add none value
        string[,] noneValue = new string[1, 2];
        noneValue[0, 0] = ResHelper.GetString("general.empty");
        noneValue[0, 1] = String.Empty;
        selectContainer.SpecialFields = noneValue;
    }


    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.selectContainer.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return selectContainer.Value;
        }
        set
        {
            EnsureChildControls();
            selectContainer.Value = value;            
        }
    }


    /// <summary>
    /// Gets ClientID of the dropdownlist with containers
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return selectContainer.ClientID;
        }
    }


    /// <summary>
    /// Is live site.
    /// </summary>
    public override bool IsLiveSite
    {
        get
        {
            return selectContainer.IsLiveSite;
        }
        set
        {
            selectContainer.IsLiveSite = value;
        }
    }
}
