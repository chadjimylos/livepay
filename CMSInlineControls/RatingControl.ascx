<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RatingControl.ascx.cs" Inherits="CMSInlineControls_RatingControl" %>
<%@ Register Src="~/CMSAdminControls/ContentRating/RatingControl.ascx" TagName="RatingControl"
    TagPrefix="cms" %>
<div>
    <cms:RatingControl ID="elemRating" runat="server" />
</div>
