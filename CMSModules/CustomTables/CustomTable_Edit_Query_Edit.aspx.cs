using System;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_CustomTables_CustomTable_Edit_Query_Edit : CMSCustomTablesPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "Custom table edit - Query";

        // Get the current class ID
        int classId = QueryHelper.GetInteger("customtableid", 0);

        // If the existing query is being edited
        int queryId = QueryHelper.GetInteger("queryid", 0);
        if (queryId > 0)
        {
            queryEdit.QueryID = queryId;

            Query query = QueryProvider.GetQuery(queryId);
            InitializeBreadcrumb(query.QueryName, query.QueryClassId);
        }
        else
        {
            queryEdit.QueryID = 0;
            InitializeBreadcrumb("customtable.edit.newquery", classId);
        }

        // If the new query is being created        
        if (classId > 0)
        {
            queryEdit.ClassID = classId;
        }

        queryEdit.RefreshPageURL = "~/CMSModules/CustomTables/CustomTable_Edit_Query_Edit.aspx";
    }


    #region "Private methods"

    /// <summary>
    /// Sets the page breadcrumb control
    /// </summary>
    /// <param name="caption">Caption of the breadcrumb item displayed in the page title as resource string key</param>
    /// <param name="classId">ID of a class</param>
    private void InitializeBreadcrumb(string caption, int classId)
    {
        // Initialize the breadcrumb settings
        string[,] breadCrumbInfo = new string[2, 3];

        // Add the base item
        breadCrumbInfo[0, 0] = ResHelper.GetString("general.queries");

        string backUrl = "~/CMSModules/CustomTables/CustomTable_Edit_Query_List.aspx";

        if (classId > 0)
        {
            backUrl += "?customtableid=" + classId;
        }

        breadCrumbInfo[0, 1] = backUrl;
        breadCrumbInfo[0, 2] = "";

        // Add the custom item
        breadCrumbInfo[1, 0] = ResHelper.GetString(caption);
        breadCrumbInfo[1, 1] = "";
        breadCrumbInfo[1, 2] = "";

        // Send the breadcrumb settings to the master page
        if (CurrentMaster != null)
        {
            CurrentMaster.Title.Breadcrumbs = breadCrumbInfo;
        }
    }

    #endregion
}
