using System;
using System.Web;
using System.Web.UI;

using CMS.CMSHelper;
using CMS.Community;
using CMS.EmailEngine;
using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;

public partial class CMSWebParts_Community_Membership_GroupInvitation : CMSAbstractWebPart
{
    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether to send default group e-mails.
    /// </summary>
    public bool SendDefaultGroupEmails
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("SendDefaultGroupEmails"), false);
        }
        set
        {
            SetValue("SendDefaultGroupEmails", value);
        }
    }


    ///// <summary>
    ///// Gets or sets the value that indicates whether to send e-mail to inviter.
    ///// </summary>
    public bool SendEmailToInviter
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("SendEmailToInviter"), false);
        }
        set
        {
            SetValue("SendEmailToInviter", value);
        }
    }


    /// <summary>
    /// Gets or sets logon page URL.
    /// </summary>
    public string LoginURL
    {
        get
        {
            return ValidationHelper.GetString(GetValue("LoginURL"), string.Empty);
        }
        set
        {
            SetValue("LoginURL", value);
        }
    }

    #region "Caption properties"

    public string InvitationNoLongerExists
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("InvitationNoLongerExists"), ResHelper.GetString("group.invitationnolongerexists"));
        }
    }


    public string InvitationIsNotValid
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("InvitationIsNotValid"), ResHelper.GetString("group.invitationisnotvalid"));
        }
    }


    public string GroupNoLongerExists
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("GroupNoLongerExists"), ResHelper.GetString("group.nolongerexists"));
        }
    }


    public string MemberJoined
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("MemberJoined"), ResHelper.GetString("groups.memberjoined"));
        }
    }


    public string MemberWaiting
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("MemberWaiting"), ResHelper.GetString("groups.memberjoinedwaiting"));
        }
    }


    public string UserIsAlreadyMember
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("UserIsAlreadyMember"), ResHelper.GetString("groups.userisalreadymember"));
        }
    }

    #endregion


    #endregion

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            if (!RequestHelper.IsPostBack())
            {
                // If user is public
                if (CMSContext.CurrentUser.IsPublic())
                {
                    // Get logon url
                    string logonUrl = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSSecuredAreasLogonPage");
                    logonUrl = DataHelper.GetNotEmpty(LoginURL, logonUrl);

                    // Create redirect url
                    logonUrl = UrlHelper.ResolveUrl(logonUrl) + "?ReturnURL=" + HttpUtility.UrlEncode(UrlHelper.CurrentURL);
                    UrlHelper.Redirect(logonUrl);
                }
                else
                {
                    // Get invitation by GUID
                    Guid invitationGuid = QueryHelper.GetGuid("invitationguid", Guid.Empty);
                    if (invitationGuid != Guid.Empty)
                    {
                        InvitationInfo invitation = InvitationInfoProvider.GetInvitationInfo(invitationGuid);
                        if (invitation != null)
                        {
                            // Check if invitation is valid
                            if ((invitation.InvitationValidTo == DateTimeHelper.ZERO_TIME) ||
                                (invitation.InvitationValidTo >= DateTime.Now))
                            {
                                GroupInfo group = GroupInfoProvider.GetGroupInfo(invitation.InvitationGroupID);

                                if (group != null)
                                {
                                    // Check whether current user is the user who should be invited
                                    if ((invitation.InvitedUserID > 0) && (invitation.InvitedUserID != CMSContext.CurrentUser.UserID))
                                    {
                                        lblInfo.CssClass = "InvitationErrorLabel";
                                        lblInfo.Text = InvitationIsNotValid;
                                        lblInfo.Visible = true;
                                        return;
                                    }

                                    // If user was invited by e-mail
                                    if (invitation.InvitedUserID == 0)
                                    {
                                        invitation.InvitedUserID = CMSContext.CurrentUser.UserID;
                                    }

                                    if (!GroupMemberInfoProvider.IsMemberOfGroup(invitation.InvitedUserID, invitation.InvitationGroupID))
                                    {
                                        // Create group member info object
                                        GroupMemberInfo groupMember = new GroupMemberInfo();
                                        groupMember.MemberInvitedByUserID = invitation.InvitedByUserID;
                                        groupMember.MemberUserID = CMSContext.CurrentUser.UserID;
                                        groupMember.MemberGroupID = invitation.InvitationGroupID;
                                        groupMember.MemberJoined = DateTime.Now;

                                        // Set proper status depending on grouo settings
                                        switch (group.GroupApproveMembers)
                                        {
                                            // Only approved members can join
                                            case GroupApproveMembersEnum.ApprovedCanJoin:
                                                groupMember.MemberStatus = GroupMemberStatus.WaitingForApproval;
                                                lblInfo.Text = MemberWaiting.Replace("##GROUPNAME##", HTMLHelper.HTMLEncode(group.GroupDisplayName));
                                                break;
                                            // Only invited members
                                            case GroupApproveMembersEnum.InvitedWithoutApproval:
                                            // Any site members can join
                                            case GroupApproveMembersEnum.AnyoneCanJoin:
                                                groupMember.MemberApprovedWhen = DateTime.Now;
                                                groupMember.MemberStatus = GroupMemberStatus.Approved;
                                                lblInfo.Text = MemberJoined.Replace("##GROUPNAME##", HTMLHelper.HTMLEncode(group.GroupDisplayName));
                                                break;
                                        }
                                        // Store info object to database
                                        GroupMemberInfoProvider.SetGroupMemberInfo(groupMember);

                                        // Handle sending e-mails
                                        if (SendEmailToInviter || SendDefaultGroupEmails)
                                        {
                                            UserInfo sender = UserInfoProvider.GetFullUserInfo(groupMember.MemberUserID);
                                            UserInfo recipient = UserInfoProvider.GetFullUserInfo(groupMember.MemberInvitedByUserID);

                                            if (SendEmailToInviter)
                                            {
                                                EmailTemplateInfo template = EmailTemplateProvider.GetEmailTemplate("Groups.MemberAcceptedInvitation", CMSContext.CurrentSiteName);

                                                // Resolve macros
                                                MacroResolver resolver = CMSContext.CurrentResolver;
                                                resolver.SourceData = new object[] { sender, recipient, group, groupMember };
                                                resolver.SetNamedSourceData("Sender", sender.DataClass.DataRow);
                                                resolver.SetNamedSourceData("Recipient", recipient.DataClass.DataRow);
                                                resolver.SetNamedSourceData("Group", group.DataClass.DataRow);
                                                resolver.SetNamedSourceData("GroupMember", groupMember.DataClass.DataRow);

                                                if (!String.IsNullOrEmpty(recipient.Email) && !String.IsNullOrEmpty(sender.Email))
                                                {
                                                    // Send e-mail
                                                    EmailMessage message = new EmailMessage();
                                                    message.Recipients = recipient.Email;
                                                    message.From = EmailHelper.GetSender(template, SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSNoreplyEmailAddress"));
                                                    message.Subject = resolver.ResolveMacros(template.TemplateSubject);
                                                    message.PlainTextBody = resolver.ResolveMacros(template.TemplatePlainText);
                                                    // Enable macro encoding for body
                                                    resolver.EncodeResolvedValues = true;
                                                    message.Body = resolver.ResolveMacros(template.TemplateText);
                                                    message.CcRecipients = template.TemplateCc;
                                                    message.BccRecipients = template.TemplateBcc;
                                                    message.EmailFormat = EmailFormatEnum.Default;

                                                    MetaFileInfoProvider.ResolveMetaFileImages(message, template.TemplateID, EmailObjectType.EMAILTEMPLATE, MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE);
                                                    EmailSender.SendEmail(CMSContext.CurrentSiteName, message);
                                                }
                                            }

                                            if (SendDefaultGroupEmails)
                                            {
                                                if (group.GroupSendJoinLeaveNotification &&
                                                    (groupMember.MemberStatus == GroupMemberStatus.Approved))
                                                {
                                                    GroupMemberInfoProvider.SendNotificationMail("Groups.MemberJoin", CMSContext.CurrentSiteName, groupMember, true);
                                                    GroupMemberInfoProvider.SendNotificationMail("Groups.MemberJoinedConfirmation", CMSContext.CurrentSiteName, groupMember, false);
                                                }
                                                if (group.GroupSendWaitingForApprovalNotification && (groupMember.MemberStatus == GroupMemberStatus.WaitingForApproval))
                                                {
                                                    GroupMemberInfoProvider.SendNotificationMail("Groups.MemberWaitingForApproval", CMSContext.CurrentSiteName, groupMember, true);
                                                    GroupMemberInfoProvider.SendNotificationMail("Groups.MemberJoinedWaitingForApproval", CMSContext.CurrentSiteName, groupMember, false);
                                                }
                                            }
                                        }

                                        // Delete all invitations to specified group for specified user (based on e-mail or userId)
                                        string whereCondition = "InvitationGroupID = " + invitation.InvitationGroupID + " AND (InvitedUserID=" + CMSContext.CurrentUser.UserID + " OR InvitationUserEmail = N'" + CMSContext.CurrentUser.Email.Replace("'", "''") + "')";
                                        InvitationInfoProvider.DeleteInvitations(whereCondition);
                                    }
                                    else
                                    {
                                        lblInfo.Text = UserIsAlreadyMember.Replace("##GROUPNAME##", HTMLHelper.HTMLEncode(group.GroupDisplayName));
                                        lblInfo.CssClass = "InvitationErrorLabel";

                                        // Delete this invitation
                                        InvitationInfoProvider.DeleteInvitationInfo(invitation);
                                    }
                                }
                                else
                                {
                                    lblInfo.Text = GroupNoLongerExists;
                                    lblInfo.CssClass = "InvitationErrorLabel";
                                    // Delete this invitation
                                    InvitationInfoProvider.DeleteInvitationInfo(invitation);
                                }
                            }
                            else
                            {
                                lblInfo.Text = InvitationIsNotValid;
                                lblInfo.CssClass = "InvitationErrorLabel";
                                // Delete this invitation
                                InvitationInfoProvider.DeleteInvitationInfo(invitation);
                            }
                        }
                        else
                        {
                            lblInfo.Text = InvitationNoLongerExists;
                            lblInfo.CssClass = "InvitationErrorLabel";
                        }
                        lblInfo.Visible = true;
                    }
                    else
                    {
                        Visible = false;
                    }
                }
            }
        }
    }
}
