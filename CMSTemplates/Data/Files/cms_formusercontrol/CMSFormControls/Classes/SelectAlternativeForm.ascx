<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectAlternativeForm.ascx.cs"
    Inherits="CMSFormControls_Classes_SelectAlternativeForm" %>
<asp:TextBox ID="txtName" runat="server" MaxLength="200" CssClass="SelectorTextBox" /><cms:CMSButton 
    ID="btnSelect" runat="server" CssClass="ContentButton" />
<asp:Label ID="lblStatus" runat="server" CssClass="SelectorError" EnableViewState="False" />
