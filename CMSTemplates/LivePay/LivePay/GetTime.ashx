﻿<%@ WebHandler Language="VB" Class="GetTime" %>

Imports System
Imports System.Web
Imports System.Web.Services

Public Class GetTime : Implements IHttpHandler
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property
    
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        context.Response.Write(GetTime())
    End Sub
    
    Public Shared Function GetTime() As [String]
        Dim dt As New DateTime()
        dt = Convert.ToDateTime("April 9, 2010 22:38:10")
        Return dt.ToString("dddd, dd MMMM yyyy HH:mm:ss")
    End Function



End Class