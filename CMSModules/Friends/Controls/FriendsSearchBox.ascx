<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FriendsSearchBox.ascx.cs"
    Inherits="CMSModules_Friends_Controls_FriendsSearchBox" %>
<asp:Panel ID="pnlFilter" runat="server" EnableViewState="false">
    <asp:TextBox ID="txtSearch" runat="server" EnableViewState="false" CssClass="TextBoxField" />
    <cms:LocalizedButton ID="btnSearch" runat="server" ResourceString="general.search"
        EnableViewState="false" CssClass="ContentButton" OnClick="btnSearch_OnClick" />
    <br />
    <br />
</asp:Panel>
