using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Reporting;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Reporting_Tools_Report_Print : CMSReportingModalPage
{
    private int reportId = 0;

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        SetCulture();

        IFormatProvider culture = DateTimeHelper.DefaultIFormatProvider;
        IFormatProvider currentCulture = new System.Globalization.CultureInfo(System.Threading.Thread.CurrentThread.CurrentUICulture.IetfLanguageTag);

        reportId = ValidationHelper.GetInteger(Request.QueryString["reportid"], 0);
        ReportInfo ri = ReportInfoProvider.GetReportInfo(reportId);
        if (ri != null)
        {
            string[] httpParameters = ValidationHelper.GetString(Request.QueryString["parameters"], "").Split(";".ToCharArray());

            if (httpParameters.Length > 1)
            {
                string[] parameters = new string[httpParameters.Length / 2];

                DataTable dtp = new DataTable();

                // Create correct columns and put values in it
                for (int i = 0; i < httpParameters.Length; i = i + 2)
                {
                    if (ValidationHelper.GetDateTime(httpParameters[i+1], DataHelper.DATETIME_NOT_SELECTED, culture) == DataHelper.DATETIME_NOT_SELECTED)
                    {
                        dtp.Columns.Add(httpParameters[i]);
                        parameters[i / 2] = httpParameters[i + 1];
                    }
                    else
                    {
                        dtp.Columns.Add(httpParameters[i], typeof(DateTime));
                        parameters[i / 2] =  Convert.ToDateTime(httpParameters[i + 1], culture).ToString(currentCulture);
                    }
                }


                dtp.Rows.Add(parameters);
                dtp.AcceptChanges();

                DisplayReport1.LoadFormParameters = false;
                DisplayReport1.ReportName = ri.ReportName;
                DisplayReport1.DisplayFilter = false;
                DisplayReport1.ReportParameters = dtp.Rows[0];
            }
            else
            {
                DisplayReport1.ReportName = ri.ReportName;
                DisplayReport1.DisplayFilter = false;
            }
            this.Page.Title = ResHelper.GetString("Report_Print.lblPrintReport") + " " + ri.ReportDisplayName;
        }
    }    
}