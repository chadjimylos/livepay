<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Module_Edit_PermissionName_Edit.aspx.cs"
    Inherits="CMSSiteManager_Development_Modules_Module_Edit_PermissionName_Edit" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Module Edit - Permission Name Edit" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent" EnableViewState="false">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lbPermissionDisplayName" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="tbPermissionDisplayName" runat="server" CssClass="TextBoxField"
                    MaxLength="100" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvPermissionDisplayName" runat="server" ControlToValidate="tbPermissionDisplayName"
                    Display="dynamic" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lbPermissionCodeName" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="tbPermissionCodeName" runat="server" CssClass="TextBoxField" MaxLength="100"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvPermissionCodeName" runat="server" ControlToValidate="tbPermissionCodeName"
                    Display="dynamic" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <asp:Label ID="lblPermissionDescription" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtPermissionDescription" runat="server" EnableViewState="false"
                    TextMode="MultiLine" CssClass="TextAreaField" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top">
                <cms:LocalizedLabel ID="lblPermissionDisplayInMatrix" runat="server" DisplayColon="true"
                    ResourceString="Administration-Module_Edit_PermissionName_Edit.DisplayInMatrix"
                    EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox ID="chkPermissionDisplayInMatrix" runat="server" CssClass="CheckBoxMovedLeft"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton"
                    EnableViewState="false" />
            </td>
        </tr>
    </table>
</asp:Content>
