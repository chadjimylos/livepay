using System;
using System.Web.UI.WebControls;
using CMS.GlobalHelper;
using CMS.Controls;
using CMS.UIControls;

public partial class CMSAdminControls_UI_UniGrid_Filters_ClassNameFilter : CMSAbstractBaseFilterControl
{
    #region "Variables"

    private CMSUserControl filteredControl;
    private string mFilterMode = null;
    protected bool mDisplayLabel = false;

    #endregion


    #region "Properties"

    /// <summary>
    /// Current filter mode.
    /// </summary>
    public string FilterMode
    {
        get
        {
            if (mFilterMode == null)
            {
                mFilterMode = ValidationHelper.GetString(filteredControl.GetValue("FilterMode"), string.Empty).ToLower();
            }
            return mFilterMode;
        }
        set
        {
            mFilterMode = value;
        }
    }


    /// <summary>
    /// Where condition.
    /// </summary>
    public override string WhereCondition
    {
        get
        {
            base.WhereCondition = GenerateWhereCondition();
            return base.WhereCondition;
        }
        set
        {
            base.WhereCondition = value;
        }
    }


    /// <summary>
    /// Filter value.
    /// </summary>
    public override object Value
    {
        get
        {
            return ValidationHelper.GetString(txtClassName.Text, string.Empty);
        }
        set
        {
            txtClassName.Text = ValidationHelper.GetString(value, string.Empty);
        }
    }


    /// <summary>
    /// Gets or sets whether to display label
    /// </summary>
    public bool DisplayLabel
    {
        get
        {
            return mDisplayLabel;
        }
        set
        {
            mDisplayLabel = value;
            plcLabel.Visible = value;
        }
    }


    /// <summary>
    /// Gets class label
    /// </summary>
    public Label ClassLabel
    {
        get
        {
            return lblClass;
        }
    }


    /// <summary>
    /// Determines whether filter is set
    /// </summary>
    public bool FilterIsSet
    {
        get
        {
            return !string.IsNullOrEmpty(txtClassName.Text);
        }
    }

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        filteredControl = FilteredControl as CMSUserControl;
    }


    /// <summary>
    /// OnLoad override - check wheter filter is set
    /// </summary>
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // Init operands
        if (drpOperator.Items.Count == 0)
        {
            drpOperator.Items.Add(new ListItem("LIKE", "LIKE"));
            drpOperator.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
            drpOperator.Items.Add(new ListItem("=", "="));
            drpOperator.Items.Add(new ListItem("<>", "<>"));
        }


        plcLabel.Visible = DisplayLabel;

        // Generate current where condition 
        WhereCondition = GenerateWhereCondition();
    }

    #endregion


    #region "Other methods"

    /// <summary>
    /// Generates where condition.
    /// </summary>
    protected string GenerateWhereCondition()
    {
        string where = "NodeClassID IN (SELECT ClassID FROM CMS_Class WHERE {0})";
        string phrase = txtClassName.Text.Trim().Replace("'", "''");
        if (phrase != string.Empty)
        {
            phrase = TextHelper.LimitLength(phrase, 100);
            switch (drpOperator.SelectedValue)
            {
                case "NOT LIKE":
                case "LIKE":
                    where = string.Format(where, "ClassDisplayName " + drpOperator.SelectedValue + " N'%" + phrase + "%'");
                    break;

                default:
                    where = string.Format(where, "ClassDisplayName " + drpOperator.SelectedValue + " N'" + phrase + "'");
                    break;
            }
            return where;
        }
        else
        {
            return string.Empty;
        }
    }

    #endregion
}
