using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.Community;

public partial class CMSModules_Groups_Tools_Security_Security : CMSGroupPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        groupSecurity.GroupID = QueryHelper.GetInteger("groupid", 0);
        groupSecurity.OnCheckPermissions += new CMSAdminControl.CheckPermissionsEventHandler(groupSecurity_OnCheckPermissions);
    }


    void groupSecurity_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        CheckPermissions(groupSecurity.GroupID, CMSAdminControl.PERMISSION_MANAGE);
    }
}
