CREATE TABLE [CMS_Workflow] (
		[WorkflowID]               int NOT NULL IDENTITY(1, 1),
		[WorkflowDisplayName]      nvarchar(450) NOT NULL,
		[WorkflowName]             nvarchar(450) NOT NULL,
		[WorkflowGUID]             uniqueidentifier NOT NULL,
		[WorkflowLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Workflow]
	ADD
	CONSTRAINT [PK_CMS_Workflow]
	PRIMARY KEY
	NONCLUSTERED
	([WorkflowID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_Workflow_WorkflowDisplayName]
	ON [CMS_Workflow] ([WorkflowDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
