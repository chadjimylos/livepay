<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TimeZone_Edit.aspx.cs" Inherits="CMSSiteManager_Development_TimeZones_TimeZone_Edit"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>

<%@ Register Src="~/CMSSiteManager/Development/TimeZones/TimeZoneRuleEdior.ascx"
    TagName="RuleEditor" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:ScriptManager ID="scrManager" runat="server" />
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblTimeZoneDisplayName" EnableViewState="false"
                    ResourceString="TimeZ.Edit.TimeZoneDisplayName" DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtTimeZoneDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtTimeZoneDisplayName"
                    Display="Dynamic" ValidationGroup="vgTimeZone"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblTimeZoneName" EnableViewState="false" ResourceString="General.CodeName"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtTimeZoneName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtTimeZoneName"
                    Display="Dynamic" ValidationGroup="vgTimeZone"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblTimeZoneGMT" EnableViewState="false" ResourceString="TimeZ.Edit.TimeZoneGMT" />
            </td>
            <td>
                <asp:TextBox ID="txtTimeZoneGMT" runat="server" CssClass="TextBoxField" />
                <asp:RequiredFieldValidator ID="rfvGMT" runat="server" ControlToValidate="txtTimeZoneGMT"
                    Display="Dynamic" ValidationGroup="vgTimeZone"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="rvGMTDouble" runat="server" ControlToValidate="txtTimeZoneGMT"
                    Display="Dynamic" Type="Double" ValidationGroup="vgTimeZone"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblTimeZoneDaylight" EnableViewState="false"
                    ResourceString="TimeZ.Edit.TimeZoneDaylight" />
            </td>
            <td>
                <asp:CheckBox ID="chkTimeZoneDaylight" AutoPostBack="true" runat="server" CssClass="CheckBoxMovedLeft" />
            </td>
        </tr>
        <asp:PlaceHolder runat="server" ID="plcDSTInfo">
            <tr>
                <td>
                    <cms:LocalizedLabel runat="server" ID="lblTimeZoneRuleStartIn" EnableViewState="false"
                        ResourceString="TimeZ.Edit.TimeZoneRuleStartIn" />
                </td>
                <td>
                    <%--<asp:TextBox ID="txtTimeZoneRuleStart" runat="server" CssClass="TextBoxField" ReadOnly="true" />--%>
                    <asp:Label ID="lblTimeZoneRuleStart" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <cms:LocalizedLabel runat="server" ID="lblTimeZoneRuleEndIn" EnableViewState="false"
                        ResourceString="TimeZ.Edit.TimeZoneRuleEndIn" />
                </td>
                <td>
                    <%--<asp:TextBox ID="txtTimeZoneRuleEnd" runat="server" CssClass="TextBoxField" ReadOnly="true" />--%>
                    <asp:Label ID="lblTimeZoneRuleEnd" runat="server" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <tr>
            <td>
                <cms:LocalizedLabel runat="server" ID="lblTimeZoneRuleStartRule" EnableViewState="false"
                    ResourceString="TimeZ.Edit.TimeZoneRuleStartRule" />
            </td>
            <td>
                <cms:RuleEditor ID="startRuleEditor" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:LocalizedLabel runat="server" ID="lblTimeZoneRuleEndRule" EnableViewState="false"
                    ResourceString="TimeZ.Edit.TimeZoneRuleEndRule" />
            </td>
            <td>
                <cms:RuleEditor ID="endRuleEditor" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:LocalizedButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" ValidationGroup="vgTimeZone" ResourceString="general.ok" />
            </td>
        </tr>
    </table>
</asp:Content>
