<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fck_InsertRating.aspx.cs"
    Inherits="CMSAdminControls_FCKeditor_editor_plugins_InsertRating_fck_InsertRating"
    Theme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Insert Rating</title>

    <script type="text/javascript">
        //<![CDATA[
        var oEditor = window.parent.InnerDialogLoaded();

        function insertRating(charValue) {
            oEditor.FCK.InsertHtml(charValue || "");
            window.parent.Cancel();
        }
        //]]>
    </script>

</head>
<body style="background-color: White; height: 94%;" runat="server" id="bodyElem">
    <form id="form1" runat="server">
    <asp:Panel ID="pnlGrid" runat="server" CssClass="UniGridBody">
        <asp:Panel ID="pnlContent" runat="server" CssClass="UniGridContent">
            <cms:BasicDataGrid ID="gridForms" runat="server" CssClass="UniGridGrid" CellSpacing="0" CellPadding="3" AutoGenerateColumns="false"
                GridLines="Horizontal" Width="100%">
                <HeaderStyle CssClass="UniGridHead" />
                <AlternatingItemStyle CssClass="OddRow" />
                <ItemStyle CssClass="EvenRow" />
                <Columns>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <a href="#" onclick='javascript:insertRating("%%control:RatingControl?<%# DataBinder.Eval(Container.DataItem, "UserControlCodeName") %>%%");'>
                                <%# DataBinder.Eval(Container.DataItem, "UserControlDisplayName") %>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </cms:BasicDataGrid>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
