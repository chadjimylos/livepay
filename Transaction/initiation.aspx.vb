Imports System.Data
Imports System.Data.SqlClient
Partial Class TRANSACTION_INITIATION
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnSubmit.Attributes("onClick") = "return payClicked();"
        merchantID.Value = System.Configuration.ConfigurationManager.AppSettings("ApacsUserID")
        APACSURL.Text = System.Configuration.ConfigurationManager.AppSettings("APACSURL")

        Dim remoteAddr As String = Request.UserHostAddress

        Try
            Dim AmountEncrypt As String, pcEncrypt As String
            If Request.QueryString("Am") <> "" Then
                AmountEncrypt = CryptoFunctions.DecryptString(Request("Am"))
                pcEncrypt = CryptoFunctions.DecryptString(Request("pc"))
            Else
                AmountEncrypt = Request("Amount")
                pcEncrypt = Request("PaymentCode")
            End If

            amount.Value = AmountEncrypt 'GET THE CORRECT AMOUNT HERE
            Var1.Value = pcEncrypt 'GET THE TRANSACTIONCODE
            lang.Value = Request("lang") '/EN GET THE CORRECT LANGUAGE
	    lang.Value = lang.Value.toUpper()
	    if lang.Value <> "GR" then lang.value = "EN"
            merchantRef.Value = GetMerchantRef(amount.Value, Var1.Value)
        Catch ex As Exception
            'Response.Redirect("OnlineCheckOut.aspx")
        End Try
    End Sub

    Private Function GetMerchantRef(ByVal Amount As String, ByVal PaymentCode As String) As String
        Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()

        Dim objConnection As New SqlConnection(strConnection)
        objConnection.Open()

        Dim objCommand As New SqlCommand("AegeanPower_GetMerchantRef", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@PaymentCode", PaymentCode)
        objCommand.Parameters.AddWithValue("@Amount", Amount)

        Dim merchantRef As String = objCommand.ExecuteScalar()

        objConnection.Close()
        Return merchantRef
    End Function
End Class
