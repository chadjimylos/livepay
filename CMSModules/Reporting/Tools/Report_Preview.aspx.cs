using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Reporting;
using CMS.FormEngine;
using CMS.CMSHelper;
using CMS.FileManager;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Reporting_Tools_Report_Preview : CMSReportingModalPage
{
    string type = QueryHelper.GetString("type", "").ToLower();

    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("General.Preview");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Reporting/preview.png");

        int itemId = QueryHelper.GetInteger("id", -1);
        if (itemId <= 0)
        {
            return;
        }


        switch (type)
        {
            case "graph":
                ReportGraphInfo graph = ReportGraphInfoProvider.GetReportGraphInfo(itemId);
                if (graph != null)
                {
                    ReportInfo ri = ReportInfoProvider.GetReportInfo(graph.GraphReportID);

                    if (ri != null)
                    {
                        // Set the form
                        FormInfo fi = new FormInfo();
                        fi.LoadXmlDefinition(ri.ReportParameters);
                        // Get datarow with required columns
                        DataRow dr = fi.GetDataRow();

                        fi.LoadDefaultValues(dr);

                        //reportGraph.ContextParameters 
                        reportGraph.ReportParameters = dr;

                        reportGraph.Visible = true;

                        // prepare fully qualified graph name = with reportname
                        string fullReportGraphName = ri.ReportName + "." + graph.GraphName;
                        reportGraph.Parameter = fullReportGraphName;

                        reportGraph.ReloadData(true);
                    }
                }
                break;

            case "table":

                ReportTableInfo table = ReportTableInfoProvider.GetReportTableInfo(itemId);
                if (table != null)
                {
                    ReportInfo ri = ReportInfoProvider.GetReportInfo(table.TableReportID);

                    if (ri != null)
                    {
                        // Set the form
                        FormInfo fi = new FormInfo();
                        fi.LoadXmlDefinition(ri.ReportParameters);
                        // Get datarow with required columns
                        DataRow dr = fi.GetDataRow();

                        fi.LoadDefaultValues(dr);
                        reportTable.ReportParameters = dr;

                        reportTable.Visible = true;

                        // prepare fully qualified table name = with reportname
                        string fullReportTableName = ri.ReportName + "." + table.TableName;
                        reportTable.Parameter = fullReportTableName;
                    }
                }
                break;
        }

        btnClose.Text = ResHelper.GetString("General.Close");
    }

    protected override void OnPreRender(EventArgs e)
    {
        // Table cannot be reload earlier because of table paging
        if (type == "table")
        {
            reportTable.ReloadData(true);
        }
        base.OnPreRender(e);
    }
}
