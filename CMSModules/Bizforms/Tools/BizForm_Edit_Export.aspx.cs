using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.FormEngine;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.DirectoryUtilities;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_BizForms_Tools_BizForm_Edit_Export : CMSBizFormPage
{
    protected bool office2007 = false;
    protected int formId = 0;

    // XLST template file path
    private const string TEMPLATEFILEPATH = "~/CMSModules/BizForms/Tools/template.zip";


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'ReadData' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "ReadData"))
        {
            RedirectToCMSDeskAccessDenied("cms.form", "ReadData");
        }

        // Get form ID from url
        formId = QueryHelper.GetInteger("formid", 0);
        // Check which office version the file should be exported for
        if (QueryHelper.GetString("office", String.Empty) != String.Empty)
        {
            office2007 = true;
        }

        // Get BizFormInfo object
        BizFormInfo bfi = BizFormInfoProvider.GetBizFormInfo(formId);
        if (bfi != null)
        {
            FormFieldInfo ffi = null;
            DataSet dsFormData = null;
            BoundField column = null;
            string className = String.Empty;
            string classFormDefinition = String.Empty;

            // Set form name
            string formName = bfi.FormName;

            DataClassInfo dci = DataClassInfoProvider.GetDataClass(bfi.FormClassID);
            if (dci != null)
            {
                // Set form class name
                className = dci.ClassName;
                // Set class form definition
                classFormDefinition = dci.ClassFormDefinition;
            }

            // Get all form data
            GeneralConnection genConn = ConnectionHelper.GetConnection();
            dsFormData = genConn.ExecuteQuery(className + ".selectall", null, null, null);

            // Load form definition
            FormInfo fi = new FormInfo();
            fi.LoadXmlDefinition(classFormDefinition);

            // Get all column names
            string[] columnNames = fi.GetColumnNames();

            string reportFields = null;
            ArrayList headerCaptions = new ArrayList();

            // Set displayed columns
            if (columnNames != null && dsFormData.Tables.Count > 0)
            {
                // Add displayed columns
                if (!string.IsNullOrEmpty(bfi.FormReportFields))
                {
                    reportFields = ";" + bfi.FormReportFields.ToLower().Trim(';') + ";";

                    int index = 0;
                    // Add selected columns to the datagrid
                    foreach (string columnName in columnNames)
                    {
                        if (dsFormData.Tables[0].Columns.Contains(columnName))
                        {
                            if (reportFields.Contains(";" + columnName.ToLower() + ";"))
                            {
                                // Get form field info
                                ffi = fi.GetFormField(columnName);

                                // Set column datafield and caption
                                column = new BoundField();
                                column.DataField = columnName;
                                column.HeaderText = GetFieldCaption(ffi, columnName);
                                // Store header caption (for Excel2007)
                                headerCaptions.Add(column.HeaderText);

                                // Add column to gridview
                                gridView.Columns.Add(column);

                                // Change column's position to proper one (for Excel2007)
                                dsFormData.Tables[0].Columns[columnName].SetOrdinal(index);
                                index++;
                            }
                            else
                            {
                                // Remove column from datatable (for Excel2007)
                                dsFormData.Tables[0].Columns.Remove(columnName);
                            }
                        }
                    }
                }
                // Add all columns by default
                else
                {
                    int index = 0;
                    foreach (string columnName in columnNames)
                    {
                        // Get form field info
                        ffi = fi.GetFormField(columnName);

                        // Set column datafield and caption
                        column = new BoundField();
                        column.DataField = columnName;
                        column.HeaderText = GetFieldCaption(ffi, columnName);
                        // Store header caption (for Excel2007)
                        headerCaptions.Add(column.HeaderText);

                        // Add column to gridview
                        gridView.Columns.Add(column);

                        // Change column's position to proper one (for Excel2007)
                        dsFormData.Tables[0].Columns[columnName].SetOrdinal(index);
                        index++;
                    }
                }
            }

            gridView.DataSource = dsFormData;
            gridView.DataBind();

            if (office2007)
            {
                // Export to new Excel2007 format
                if (dsFormData != null)
                {
                    // Get the output file name
                    string fileName = formName + "_data.xlsx";

                    // Create new excel file on specified place and get path to the file
                    string newFilePath = DataHelper.CreateExcelFile(fileName, TEMPLATEFILEPATH, headerCaptions, dsFormData.Tables[0]);

                    if (!string.IsNullOrEmpty(newFilePath))
                    {
                        // Send created XLSX file
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                        Response.ContentEncoding = System.Text.Encoding.UTF8;
                        Response.ContentType = "application/vnd.xlsx";
                        Response.Charset = "";
                        this.EnableViewState = false;

                        FileStream jfs = new FileStream(newFilePath, FileMode.Open);
                        if (jfs != null)
                        {
                            StreamReader sr = new StreamReader(jfs);
                            if (jfs.Length > 0)
                            {
                                byte[] jdata = new byte[jfs.Length];
                                jfs.Read(jdata, 0, jdata.Length);
                                Response.ClearContent();
                                // Add the file data
                                Response.OutputStream.Write(jdata, 0, jdata.Length);
                                Response.OutputStream.Close();
                            }

                            sr.Dispose();
                            sr.Close();
                            jfs.Dispose();
                            jfs.Close();
                        }

                        // Delete file content of the temporary directory (without sub-directories)
                        DirectoryHelper.DeleteDirectoryStructure(newFilePath.Replace(fileName, ""));

                        RequestHelper.EndResponse();
                    }
                }
            }
            else
            {
                // Export to old Excel format
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}_data.xls", formName));
                Response.ContentType = "application/vnd.ms-excel";
                Response.ContentEncoding = System.Text.Encoding.UTF7;
                Response.Charset = "";
                this.EnableViewState = false;

                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        // Create a table to contain the grid
                        Table table = new Table();

                        // Include the gridline settings
                        table.GridLines = gridView.GridLines;

                        // Add the header row to the table
                        if (gridView.HeaderRow != null)
                        {
                            table.Rows.Add(gridView.HeaderRow);
                        }

                        // Add each of the data rows to the table
                        foreach (GridViewRow row in gridView.Rows)
                        {
                            table.Rows.Add(row);
                        }

                        // Render the table into the htmlwriter
                        table.RenderControl(htw);

                        // Render the htmlwriter into the response
                        Response.Write(sw.ToString());

                        RequestHelper.EndResponse();
                    }
                }
            }
        }
    }


    /// <summary>
    /// Returtns field caption of the specified column.
    /// </summary>
    /// <param name="ffi">Form field info.</param>
    /// <param name="columnName">Column name.</param>
    protected string GetFieldCaption(FormFieldInfo ffi, string columnName)
    {
        string fieldCaption = "";

        // Get field caption        
        if ((ffi == null) || (ffi.Caption == ""))
        {
            fieldCaption = columnName;
        }
        else
        {
            if (ffi.Caption == "")
            {
                fieldCaption = columnName;
            }
            else
            {
                fieldCaption = ffi.Caption;
            }
        }

        return fieldCaption;
    }
}
