CREATE TABLE [COM_CustomerCreditHistory] (
		[EventID]                     int NOT NULL IDENTITY(1, 1),
		[EventName]                   nvarchar(200) NOT NULL,
		[EventCreditChange]           float NOT NULL,
		[EventDate]                   datetime NOT NULL,
		[EventDescription]            nvarchar(max) NOT NULL,
		[EventCustomerID]             int NOT NULL,
		[EventCreditGUID]             uniqueidentifier NOT NULL,
		[EventCreditLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_CustomerCreditHistory]
	ADD
	CONSTRAINT [PK_COM_CustomerCreditHistory]
	PRIMARY KEY
	NONCLUSTERED
	([EventID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_CustomerCreditHistory]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_CustomerCreditHistory_EventCustomerID_COM_Customer]') IS NULL
BEGIN
		ALTER TABLE [COM_CustomerCreditHistory]
			ADD CONSTRAINT [FK_COM_CustomerCreditHistory_EventCustomerID_COM_Customer]
			FOREIGN KEY ([EventCustomerID]) REFERENCES [COM_Customer] ([CustomerID])
END
CREATE CLUSTERED INDEX [IX_COM_CustomerCreditHistory_EventCustomerID_EventDate]
	ON [COM_CustomerCreditHistory] ([EventCustomerID], [EventDate] DESC)
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
