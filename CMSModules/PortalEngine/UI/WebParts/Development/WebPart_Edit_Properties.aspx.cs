using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.URLRewritingEngine;
using CMS.PortalEngine;
using CMS.UIControls;
using CMS.FormEngine;
using CMS.DataEngine;

public partial class CMSModules_PortalEngine_UI_WebParts_Development_WebPart_Edit_Properties : SiteManagerPage
{
    private int webPartId = 0;
    private WebPartInfo wi = null;    

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = "WebPart Edit - Properties";

        // get webpart ID
        webPartId = QueryHelper.GetInteger("webpartid", 0);
        FieldEditor.Visible = false;

        // If saved is found in query string
        if ((!RequestHelper.IsPostBack()) && (QueryHelper.GetInteger("saved", 0) == 1))
        {
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }

        if (webPartId > 0)
        {
            // Get web part info
            wi = WebPartInfoProvider.GetWebPartInfo(webPartId);            

            // Check if info object exists
            if (wi != null)
            {
                // For inherited webpart display DefaultValue Editor
                if (wi.WebPartParentID > 0)
                {
                    FieldEditor.Visible = false;
                    DefaultValueEditor1.Visible = true;
                    DefaultValueEditor1.ParentWebPartID = wi.WebPartParentID;
                    DefaultValueEditor1.DefaultValueXMLDefinition = wi.WebPartProperties;
                    // Add handler to 
                    DefaultValueEditor1.XMLCreated += new EventHandler(DefaultValueEditor1_XMLCreated);
                }
                else
                {
                    // set fieldeditor                
                    FieldEditor.WebPartId = webPartId;
                    FieldEditor.Mode = FieldEditorModeEnum.WebPartProperties;
                    FieldEditor.Visible = true;

                    // Check newly created field for widgets update
                    FieldEditor.OnFieldCreated += new OnFieldCreated(UpdateWidgetsDefinition);
                }
            }
        }
    }


    /// <summary>
    /// Handles OnFieldCreated action and updates form definition of all widgets based on current webpart.
    /// Newly created field is set to be disabled in widget definition for security reasons.
    /// </summary>
    /// <param name="newField">Newly created field</param>
    protected void UpdateWidgetsDefinition(FormFieldInfo newField)
    {
        if ((wi != null) && (newField != null))
        {
            // Get widgets based on this webpart
            DataSet ds = WidgetInfoProvider.GetWidgets("WidgetWebPartID = " + webPartId, null, 0, "WidgetID");

            // Continue only if there are some widgets
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    int widgetId = ValidationHelper.GetInteger(dr["WidgetID"], 0);
                    WidgetInfo widget = WidgetInfoProvider.GetWidgetInfo(widgetId);
                    if (widget != null)
                    {
                        // Prepare disabled field definition
                        string disabledField = String.Format("<form><field column=\"{0}\" visible=\"false\" /></form>", newField.Name);

                        // Incorporate disabled field into original definition of widget
                        widget.WidgetProperties = FormHelper.CombineFormDefinitions(widget.WidgetProperties, disabledField);

                        // Update widget
                        WidgetInfoProvider.SetWidgetInfo(widget);
                    }
                }
            }
        }
    }


    /// <summary>
    /// XML created, save it
    /// </summary>
    protected void DefaultValueEditor1_XMLCreated(object sender, EventArgs e)
    {
        if (wi != null)
        {
            // Load xml definition
            wi.WebPartProperties = this.DefaultValueEditor1.DefaultValueXMLDefinition;


            // Sav web part info
            WebPartInfoProvider.SetWebPartInfo(wi);

            // Redirect with saved assign
            string url = UrlHelper.RemoveParameterFromUrl(URLRewriter.CurrentURL, "saved");
            url = UrlHelper.AddParameterToUrl(url, "saved", "1");
            UrlHelper.Redirect(url);
        }
    }
}

