CREATE PROCEDURE [Proc_CMS_Site_RemoveDependences] 
    @SiteID int
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    -- removes VersionHistory dependences from VersionAttachment
    DELETE FROM [CMS_VersionAttachment] WHERE VersionHistoryID IN (
        SELECT VersionHistoryID FROM [CMS_VersionHistory] WHERE NodeSiteID = @SiteID
    );
    -- removes VersionHistory dependences from WorkflowHistory
    DELETE FROM [CMS_WorkflowHistory] WHERE VersionHistoryID IN (
        SELECT VersionHistoryID FROM [CMS_VersionHistory] WHERE NodeSiteID = @SiteID
    );
    -- deletes user cultures
    DELETE FROM [CMS_UserCulture] WHERE SiteID=@SiteID;
    -- deletes site's VersionHistory
    DELETE FROM [CMS_VersionHistory] WHERE NodeSiteID=@SiteID;
    -- deletes site's SettingKeys
    DELETE FROM [CMS_SettingsKey] WHERE SiteID=@SiteID;
    -- deletes site's EmailTemplates
    DELETE FROM [CMS_EmailTemplate] WHERE EmailTemplateSiteID=@SiteID;
    -- deletes site's DomainAliases
    DELETE FROM [CMS_SiteDomainAlias] WHERE SiteID=@SiteID;
    -- deletes site's cultures
    DELETE FROM [CMS_SiteCulture] WHERE SiteID=@SiteID;
    -- deletes site's PageTemplates
    -- declare temporary templates table
	DECLARE @templateTable TABLE (
		PageTemplateID int NOT NULL
	);	
	-- get the all templates to be deleted 
	INSERT INTO @templateTable SELECT PageTemplateID FROM CMS_PageTemplate WHERE (PageTemplateID IN (SELECT PageTemplateID FROM CMS_PageTemplateSite WHERE SiteID = @SiteID ) OR PageTemplateSiteID = @SiteID) GROUP BY PageTemplateID HAVING COUNT(PageTemplateID) = 1
	
	-- delete pagetemplate <-> site bindings
    DELETE FROM [CMS_PageTemplateSite] WHERE SiteID=@SiteID;
    -- delete page templates that are not used anywhere else
    DELETE FROM [CMS_PageTemplate] WHERE PageTemplateSiteID=@SiteID AND PageTemplateID IN (SELECT * FROM @templateTable);
    -- deletes site's Users
    DELETE FROM [CMS_UserSite] WHERE SiteID=@SiteID;
    -- removes Role's dependences when site's Roles are deleted
    DELETE FROM [Forums_ForumRoles ] WHERE RoleID IN (
        SELECT RoleID FROM [CMS_Role] WHERE SiteID = @SiteID
    );
    DELETE FROM [CMS_UserRole] WHERE RoleID IN (
        SELECT RoleID FROM [CMS_Role] WHERE SiteID=@SiteID
    );
    DELETE FROM [CMS_RolePermission] WHERE RoleID IN (
        SELECT RoleID FROM [CMS_Role] WHERE SiteID=@SiteID
    );
    DELETE FROM [CMS_RoleUIElement] WHERE RoleID IN (
        SELECT RoleID FROM [CMS_Role] WHERE SiteID=@SiteID
    );
    DELETE FROM [CMS_WorkflowStepRoles] WHERE RoleID IN (
        SELECT RoleID FROM [CMS_Role] WHERE SiteID=@SiteID
    );
    DELETE FROM [Polls_PollRoles] WHERE RoleID IN (
        SELECT RoleID FROM [CMS_Role] WHERE SiteID = @SiteID
    );
    DELETE FROM [CMS_FormRole] WHERE RoleID IN (
        SELECT RoleID FROM [CMS_Role] WHERE SiteID=@SiteID
    );
    DELETE FROM [Community_GroupRolePermission] WHERE RoleID IN (
        SELECT RoleID FROM [CMS_Role] WHERE SiteID = @SiteID
    );
	DELETE FROM [Media_LibraryRolePermission] WHERE RoleID IN (
        SELECT RoleID FROM [CMS_Role] WHERE SiteID = @SiteID
    );
    
    DELETE FROM [CMS_WidgetRole] WHERE RoleID IN (
        SELECT RoleID FROM [CMS_Role] WHERE SiteID = @SiteID
    );
    
    -- deletes site's Roles
    DELETE FROM [CMS_Role] WHERE SiteID=@SiteID;
    -- deletes site's Resources
    DELETE FROM [CMS_ResourceSite] WHERE SiteID=@SiteID;
    -- deletes site's Classes
    DELETE FROM [CMS_ClassSite] WHERE SiteID=@SiteID;
    -- deletes Events of the site
    DELETE FROM [CMS_EventLog] WHERE SiteID=@SiteID;
    -- deletes site's RelationshipNames
    DELETE FROM [CMS_RelationshipNameSite] WHERE SiteID=@SiteID;
    -- deletes site's WorkflowScopes
    DELETE FROM [CMS_WorkflowScope] WHERE ScopeSiteID=@SiteID;
    -- deletes site's CSSStylesheets
    DELETE FROM CMS_CssStylesheetSite WHERE SiteID=@SiteID;
    -- deletes site's InlineControls
    DELETE FROM [CMS_InlineControlSite] WHERE SiteID=@SiteID;
    -- deletes site's ScheduledTasks
    DELETE FROM [CMS_ScheduledTask] WHERE TaskSiteID=@SiteID;
    -- deletes site's WebPart containers
    DELETE FROM CMS_WebPartContainerSite WHERE SiteID=@SiteID;
    -- deletes site Polls and site's assignment to Polls
    DELETE FROM [Polls_PollRoles] WHERE PollID IN (
            SELECT PollID FROM [Polls_Poll] WHERE PollSiteID = @SiteID
        );
    DELETE FROM [Polls_PollSite] WHERE SiteID = @SiteID;
    DELETE FROM [Polls_PollAnswer] WHERE AnswerPollID IN (
            SELECT PollID FROM [Polls_Poll] WHERE PollSiteID = @SiteID
        );
    DELETE FROM [Polls_Poll] WHERE PollSiteID = @SiteID;
    
    -- removes Newsletter_Subscriber's dependences when site's Newsletter_Subscribers are deleted
    DELETE FROM [Newsletter_Emails] WHERE EmailSubscriberID IN (
        SELECT SubscriberID FROM [Newsletter_Subscriber] WHERE SubscriberSiteID=@SiteID
    );
    DELETE FROM [Newsletter_SubscriberNewsletter] WHERE SubscriberID IN (
        SELECT SubscriberID FROM [Newsletter_Subscriber] WHERE SubscriberSiteID=@SiteID
    );
    -- deletes site's Newsletter_Subscribers
    DELETE FROM [Newsletter_Subscriber] WHERE SubscriberSiteID=@SiteID;
    -- removes Newsletter_Newsletter's dependences when site's Newsletter_Newsletters are deleted
    DELETE FROM [Newsletter_Emails] WHERE EmailNewsletterIssueID IN (
        SELECT IssueID FROM [Newsletter_NewsletterIssue] WHERE IssueNewsletterID IN (
            SELECT NewsletterID FROM [Newsletter_Newsletter] WHERE NewsletterSiteID=@SiteID
        )
    );
    DELETE FROM [Newsletter_NewsletterIssue] WHERE IssueNewsletterID IN (
        SELECT NewsletterID FROM [Newsletter_Newsletter] WHERE NewsletterSiteID=@SiteID
    );
    DELETE FROM [Newsletter_SubscriberNewsletter] WHERE NewsletterID IN (
        SELECT NewsletterID FROM [Newsletter_Newsletter] WHERE NewsletterSiteID=@SiteID
    );
    -- deletes site's Newsletter_Newsletters
    DELETE FROM [Newsletter_Newsletter] WHERE NewsletterSiteID=@SiteID;
    -- removes Newsletter_EmailTemplate's dependences when site's Newsletter_EmailTemplates are deleted
    DELETE FROM [Newsletter_Emails] WHERE EmailNewsletterIssueID IN (
        SELECT IssueID FROM [Newsletter_NewsletterIssue] WHERE IssueTemplateID IN (
            SELECT TemplateID FROM [Newsletter_EmailTemplate] WHERE TemplateSiteID=@SiteID
        )
    );
    DELETE FROM [Newsletter_NewsletterIssue] WHERE IssueTemplateID IN (
            SELECT TemplateID FROM [Newsletter_EmailTemplate] WHERE TemplateSiteID=@SiteID
    );
    -- deletes site's Newsletter_EmailTemplates
    DELETE FROM [Newsletter_EmailTemplate] WHERE TemplateSiteID=@SiteID;
    -- removes CMS_Class's dependences when BizForms are deleted
    DELETE FROM [CMS_Query] WHERE ClassID IN (
        SELECT FormClassID FROM [CMS_Form] WHERE FormSiteID=@SiteID
    );
    -- insert ClassIDs of BizForms into the temporaty table
    DECLARE @classIdTable TABLE (
        FormClassID int
    );
    -- get the classes
    INSERT INTO @classIdTable SELECT FormClassID FROM [CMS_Form] WHERE FormSiteID=@SiteID;
    -- deletes site's BizForms' roles from CMS_Form
    DELETE FROM [CMS_FormRole] WHERE FormID IN (SELECT FormID FROM [CMS_Form] WHERE FormSiteID=@SiteID);
    -- deletes site's BizForms' alt.forms from CMS_AlternativeForm
    DELETE FROM [CMS_AlternativeForm] WHERE FormClassID IN (SELECT FormClassID FROM @classIdTable)
        OR FormCoupledClassID IN (SELECT FormClassID FROM @classIdTable);
    -- deletes site's BizForms from CMS_Form
    DELETE FROM [CMS_Form] WHERE FormSiteID=@SiteID;
    -- deletes BizForms' records in CMS_Class
    DELETE FROM [CMS_Class] WHERE ClassID IN (SELECT FormClassID FROM @classIdTable);
    
    -- FORUM
    -- Forum user favorites
    DELETE FROM [Forums_UserFavorites] WHERE SiteID = @SiteID;
    -- Forum attachment
    DELETE FROM [Forums_Attachment] WHERE AttachmentSiteID = @SiteID;
    
    UPDATE [Forums_ForumPost]  SET PostParentID=NULL WHERE PostForumId IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
        SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupSiteID = @SiteID
        ));
    DELETE FROM [Forums_ForumSubscription] WHERE SubscriptionForumId IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID  IN (
        SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupSiteID = @SiteID)
    );
    DELETE FROM [Forums_ForumPost] WHERE PostForumId IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
        SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupSiteID = @SiteID)
    );
    DELETE FROM [Forums_ForumRoles ] WHERE ForumId IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
        SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupSiteID = @SiteID)
    );
    DELETE FROM [Forums_ForumModerators] WHERE ForumID IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID  IN (
        SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupSiteID = @SiteID)
    );
    
    DELETE FROM [Forums_Forum] WHERE ForumGroupId  IN (
        SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupSiteID = @SiteID);    
    DELETE FROM [Forums_ForumGroup] WHERE GroupSiteID = @SiteID;
    -- delete Staging dependences
    DELETE FROM [Staging_Synchronization] WHERE 
        SynchronizationTaskID IN (SELECT TaskID FROM [Staging_Task] WHERE TaskSiteID = @SiteID)
        OR
        SynchronizationServerID IN (SELECT ServerID FROM [Staging_Server] WHERE ServerSiteID = @SiteID)
    ;
    DELETE FROM [Staging_SyncLog] WHERE 
        SyncLogTaskID IN (SELECT TaskID FROM [Staging_Task] WHERE TaskSiteID = @SiteID)
        OR 
        SyncLogServerID IN (SELECT ServerID FROM [Staging_Server] WHERE ServerSiteID = @SiteID)
    ;
    DELETE FROM [Staging_Server] WHERE ServerSiteID = @SiteID;
    -- delete StagingTasks
    DELETE FROM [Staging_Task] WHERE TaskSiteID = @SiteID;
    -- ECOMMERCE
    -- delete COM_ShoppingCart dependences
    DELETE FROM [COM_ShoppingCartSKU] WHERE ShoppingCartID IN (
        SELECT ShoppingCartID FROM [COM_ShoppingCart] WHERE ShoppingCartSiteID = @SiteID
    );
    -- delete COM_ShoppingCart
    DELETE FROM [COM_ShoppingCart] WHERE ShoppingCartSiteID = @SiteID;
    -- delete COM_Order dependences
    DELETE FROM [COM_OrderStatusUser] WHERE OrderID IN (
        SELECT OrderID FROM [COM_Order] WHERE OrderSiteID = @SiteID
    );
    DELETE FROM [COM_OrderItem] WHERE OrderItemOrderID IN (
        SELECT OrderID FROM [COM_Order] WHERE OrderSiteID = @SiteID
    );
    -- delete COM_Order
    DELETE FROM [COM_Order] WHERE OrderSiteID = @SiteID;
    -- delete COM_ShippingOption dependences
    DELETE FROM [COM_ShoppingCart] WHERE ShoppingCartShippingOptionID IN (
        SELECT ShippingOptionID FROM [COM_ShippingOption] WHERE ShippingOptionSiteID = @SiteID
    );
    UPDATE [COM_Customer] SET CustomerPreferredShippingOptionID=NULL WHERE CustomerPreferredShippingOptionID IN (
        SELECT ShippingOptionID FROM [COM_ShippingOption] WHERE ShippingOptionSiteID = @SiteID
    );
    DELETE FROM [COM_PaymentShipping] WHERE ShippingOptionID IN (
        SELECT ShippingOptionID FROM [COM_ShippingOption] WHERE ShippingOptionSiteID = @SiteID
    );
    -- delete COM_ShippingOption
    DELETE FROM [COM_ShippingOption] WHERE ShippingOptionSiteID = @SiteID;
    -- delete COM_PaymentOption dependences
    DELETE FROM [COM_ShoppingCart] WHERE ShoppingCartPaymentOptionID IN (
        SELECT PaymentOptionID FROM [COM_PaymentOption] WHERE PaymentOptionSiteID = @SiteID
    );
    UPDATE [COM_Customer] SET CustomerPrefferedPaymentOptionID=NULL WHERE CustomerPrefferedPaymentOptionID IN (
        SELECT PaymentOptionID FROM [COM_PaymentOption] WHERE PaymentOptionSiteID = @SiteID
    );
    DELETE FROM [COM_PaymentShipping] WHERE PaymentOptionID IN (
        SELECT PaymentOptionID FROM [COM_PaymentOption] WHERE PaymentOptionSiteID = @SiteID
    );
    -- delete COM_PaymentOption
    DELETE FROM [COM_PaymentOption] WHERE PaymentOptionSiteID = @SiteID;
    -- delete items from COM_Wishlist
    DELETE FROM [COM_Wishlist] WHERE SiteID = @SiteID;
    -- WEB ANALYTICS
    -- delete Analytics_Statistics dependences
    DELETE FROM Analytics_DayHits WHERE HitsStatisticsID IN (
        SELECT StatisticsID FROM Analytics_Statistics WHERE StatisticsSiteID=@SiteID);
    DELETE FROM Analytics_HourHits WHERE HitsStatisticsID IN (
        SELECT StatisticsID FROM Analytics_Statistics WHERE StatisticsSiteID=@SiteID);
    DELETE FROM Analytics_MonthHits WHERE HitsStatisticsID IN (
        SELECT StatisticsID FROM Analytics_Statistics WHERE StatisticsSiteID=@SiteID);
    DELETE FROM Analytics_WeekHits WHERE HitsStatisticsID IN (
        SELECT StatisticsID FROM Analytics_Statistics WHERE StatisticsSiteID=@SiteID);
    DELETE FROM Analytics_YearHits WHERE HitsStatisticsID IN (
        SELECT StatisticsID FROM Analytics_Statistics WHERE StatisticsSiteID=@SiteID);
    -- delete Analytics_Statistics
    DELETE FROM Analytics_Statistics WHERE StatisticsSiteID = @SiteID;
    -- POLLS
    DELETE FROM Polls_PollSite WHERE SiteID = @SiteID;
    -- Metafiles
    DELETE FROM CMS_MetaFile WHERE MetaFileSiteID = @SiteID;
    -- Export history
    DELETE FROM Export_History WHERE ExportSiteID = @SiteID;
    DELETE FROM Export_Task WHERE TaskSiteID = @SiteID;
    -- Notifications
    DELETE FROM Notification_Subscription WHERE SubscriptionTemplateID IN (SELECT TemplateID FROM Notification_Template WHERE TemplateSiteID = @SiteID);
    DELETE FROM Notification_TemplateText WHERE Notification_TemplateText.TemplateID IN (SELECT TemplateID FROM Notification_Template WHERE TemplateSiteID = @SiteID);
    DELETE FROM Notification_Template WHERE TemplateSiteID = @SiteID;
    DELETE FROM Notification_Subscription WHERE SubscriptionSiteID = @SiteID;
    -- Media libraries
    DELETE FROM Media_LibraryRolePermission WHERE LibraryID IN (SELECT Media_Library.LibraryID FROM Media_Library WHERE LibrarySiteID = @SiteID);
    DELETE FROM Media_File WHERE FileLibraryID IN (SELECT Media_Library.LibraryID FROM Media_Library WHERE LibrarySiteID = @SiteID);
    DELETE FROM Media_Library WHERE LibrarySiteID = @SiteID;
    -- Message boards
    DELETE FROM [Board_Message] WHERE MessageBoardID IN (SELECT BoardID FROM [Board_Board] WHERE BoardSiteID = @SiteID);
	DELETE FROM [Board_Moderator] WHERE BoardID IN (SELECT BoardID FROM [Board_Board] WHERE BoardSiteID = @SiteID);
    DELETE FROM [Board_Role] WHERE BoardID IN (SELECT BoardID FROM [Board_Board] WHERE BoardSiteID = @SiteID);
    DELETE FROM [Board_Subscription] WHERE SubscriptionBoardID IN (SELECT BoardID FROM [Board_Board] WHERE BoardSiteID = @SiteID);
    DELETE FROM [Board_Board] WHERE BoardSiteID = @SiteID;
    -- Delete Tags
    DELETE FROM [CMS_Tag] WHERE TagGroupID  IN (
        SELECT TagGroupID FROM [CMS_TagGroup] WHERE TagGroupSiteID = @SiteID);    
    DELETE FROM [CMS_TagGroup] WHERE TagGroupSiteID = @SiteID;
    -- Groups
    DELETE FROM [Community_GroupMember] WHERE MemberGroupID IN (
        SELECT GroupID FROM [Community_Group] WHERE GroupSiteID = @SiteID
    );
	DELETE FROM [Community_Invitation] WHERE InvitationGroupID IN (
        SELECT GroupID FROM [Community_Group] WHERE GroupSiteID = @SiteID
    );
    DELETE FROM [Community_Group] WHERE GroupSiteID = @SiteID;
    -- Report abuse
    DELETE FROM [CMS_AbuseReport] WHERE ReportSiteID = @SiteID;
    -- Banned IPs
    DELETE FROM [CMS_BannedIP] WHERE IPAddressSiteID = @SiteID;
    -- User sessions
    DELETE FROM [CMS_Session] WHERE SessionSiteID = @SiteID;
    -- search index
    DELETE FROM [CMS_SearchIndexSite] WHERE IndexSiteID = @SiteID;
    
    -- CMS Page template scopes
    DELETE FROM [CMS_PageTemplateScope] WHERE PageTemplateScopeSiteID = @SiteID;
    
END
