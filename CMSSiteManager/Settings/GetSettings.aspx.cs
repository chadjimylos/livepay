using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Settings_GetSettings : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int siteId = QueryHelper.GetInteger("siteid", 0);
        int categoryId = QueryHelper.GetInteger("categoryid", 0);

        // Get the category
        SettingsCategoryInfo ci = SettingsCategoryInfoProvider.GetSettingsCategoryInfo(categoryId);
        if (ci != null)
        {
            StringBuilder sb = new StringBuilder();

            // Prepare the header of the file
            SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);
            if (si == null)
            {
                sb.Append("Global settings, category \"");
            }
            else
            {
                sb.Append("Settings for site \"" + si.DisplayName + "\", category \"");
            }
            sb.Append(ResHelper.LocalizeString(ci.CategoryDisplayName) + "\"" + Environment.NewLine + Environment.NewLine);

            // Get all settings in specified category
            DataSet ds = SettingsKeyProvider.GetSettingsKeysOrdered(siteId, categoryId);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                // Print SettingsKey names and values
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string name = ResHelper.LocalizeString(dr["KeyDisplayName"].ToString());
                    string codeName = dr["KeyName"].ToString();
                    bool isBool = dr["KeyType"].ToString() == "boolean";
                    object value = dr["KeyValue"];

                    sb.Append(name + (name.EndsWith(":") ? " " : ": "));
                    if ((value == DBNull.Value) && (siteId > 0))
                    {
                        if (isBool)
                        {
                            sb.Append(SettingsKeyProvider.GetBoolValue(codeName) + " (inherited)");
                        }
                        else
                        {
                            sb.Append(SettingsKeyProvider.GetStringValue(codeName) + " (inherited)");
                        }
                    }
                    else
                    {
                        if (isBool)
                        {
                            sb.Append(ValidationHelper.GetBoolean(value, false));
                        }
                        else
                        {
                            sb.Append(value.ToString());
                        }
                    }
                    sb.Append(Environment.NewLine);
                    sb.Append(Environment.NewLine);
                }
            }

            // Send the file to the user
            string siteName = (siteId > 0 ? si.SiteName : "Global");

            Response.AddHeader("Content-disposition", "attachment; filename=" + siteName + "_" + ValidationHelper.GetIdentificator(ci.CategoryName, "_") + ".txt");
            Response.ContentType = "text/plain";
            Response.Write(sb.ToString());
            
            RequestHelper.EndResponse();
        }
    }
}