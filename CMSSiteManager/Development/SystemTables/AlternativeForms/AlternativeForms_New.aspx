<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AlternativeForms_New.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="ALternative Forms - New"
    Inherits="CMSSiteManager_Development_SystemTables_AlternativeForms_AlternativeForms_New"
    Theme="default" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" Visible="false" EnableViewState="false" />
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblDisplayName" runat="server" EnableViewState="false" DisplayColon="true" ResourceString="general.displayname" />
            </td>
            <td>
                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ErrorMessage="" ControlToValidate="txtDisplayName" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblCodeName" runat="server" EnableViewState="false" DisplayColon="true" ResourceString="general.codename" />
            </td>
            <td>
                <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ErrorMessage="" ControlToValidate="txtCodeName" />
            </td>
        </tr>
        <asp:Panel ID="pnlCombineUserSettings" runat="server" Visible="false">
            <tr>
                <td>
                    <cms:LocalizedLabel ID="lblCombineUserSettings" runat="server" EnableViewState="false" DisplayColon="true" ResourceString="altform.combineusersettings" />
                </td>
                <td>
                    <asp:CheckBox ID="chkCombineUserSettings" runat="server" EnableViewState="false" />
                </td>
            </tr>
        </asp:Panel>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
