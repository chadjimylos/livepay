using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_RecycleBin_default : SiteManagerPage
{
    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsCallback)
        {
            // Setup page title text and image
            CurrentMaster.Title.TitleText = ResHelper.GetString("Administration-RecycleBin.Header");
            CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_RecycleBin/module.png");
            CurrentMaster.Title.HelpTopicName = "recycle_bin";
            CurrentMaster.Title.HelpName = "helpTopic";
        }

        // Get site ID       
        int selectedSite = QueryHelper.GetInteger("toSite", 0);
        if (selectedSite == 0)
        {

            // Set site selector
            siteSelector.DropDownSingleSelect.AutoPostBack = true;
            siteSelector.OnlyRunningSites = false;
            siteSelector.AllowAll = false;
            siteSelector.UniSelector.SpecialFields = new string[1, 2] { { ResHelper.GetString("RecycleBin.AllSites"), "0" } };
            siteSelector.UniSelector.OnSelectionChanged += UniSelector_OnSelectionChanged;

            if (!RequestHelper.IsPostBack())
            {
                selectedSite = CMSContext.CurrentSiteID;
                siteSelector.Value = selectedSite;
            }
        }
        if (!IsCallback)
        {

            lblSite.Text = ResHelper.GetString("Administration-RecycleBin.Site");
        }
        recycleBin.SiteID = ValidationHelper.GetInteger(siteSelector.Value, 0);
    }


    protected override void OnPreRender(EventArgs e)
    {
        // Hide site selector if there are no sites
        if (!siteSelector.UniSelector.HasData)
        {
            pnlSiteSelector.Visible = false;
        }
        base.OnPreRender(e);
    }

    #endregion


    #region "Control events"

    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        pnlUpdate.Update();
    }

    #endregion
}
