using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSPages_unsubscribe : CMSPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Forums
        Guid subGuid = QueryHelper.GetGuid("subGuid", Guid.Empty);
        int forumId = QueryHelper.GetInteger("forumid", 0);

        if (subGuid != Guid.Empty)
        {
            Server.Transfer(ResolveUrl("~/CMSModules/Forums/CMSPages/Unsubscribe.aspx?&subGuid=") + subGuid.ToString() + "&forumid=" + forumId);    
        }

        // Newsletters
        Guid subscriberGuid = QueryHelper.GetGuid("subscriberguid", Guid.Empty);
        Guid newsletterGuid = QueryHelper.GetGuid("newsletterguid", Guid.Empty);
        int issueID = QueryHelper.GetInteger("issueid", 0);

        if ((subscriberGuid != Guid.Empty) && (newsletterGuid != Guid.Empty))
        {
            Server.Transfer(ResolveUrl("~/CMSModules/Newsletters/CMSPages/Unsubscribe.aspx?&subscriberguid=") + subscriberGuid.ToString() + "&newsletterGuid=" + newsletterGuid + "&issueid=" + issueID);
        }

    }
}
