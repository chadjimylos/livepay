﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UcHomePage.ascx.vb" Inherits="CMSTemplates_LivePay_Merchants_UcHomePage" %>
<div class="CUDDarkBlueBGTitle PB10">
    <div class="SvdCardsTopTitle"><%=ResHelper.LocalizeString("{$=Γιατί να επιλέξω Live-pay|en-us=Γιατί να επιλέξω Live-pay$}") %></div>
    <div class="CUDContentBG PB10">
       <div style="padding-left:17px;color:#43474a;font-size:12px;padding-top:10px;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ullamcorper cursus tellus nec pulvinar.       Nulla sit amet augue nec nunc vehicula ullamcorper et sit amet lectus.       Curabitur sollicitudin elit vitae orci mattis ut auctor ipsum interdum.        Nulla vel nunc sit amet turpis condimentum commodo. In sed leo lorem.        Nulla facilisi. Morbi quis elementum ipsum. Fusce rhoncus auctor suscipit.        Praesent orci orci, hendrerit ac facilisis nec, gravida vitae eros.       Etiam vestibulum lorem nec ligula interdum vitae bibendum leo lobortis. 
       Aliquam at purus lorem. Nunc tristique sollicitudin nulla sit amet pulvinar. 
       Integer lobortis pretium mi, sed mattis odio tempus eu. Donec sollicitudin, turpis at tempus aliquam, metus est egestas tortor,
       </div>
    </div>
    <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
</div>
 <div class="CUDDarkBlueBGTitle">
        <div class="SvdCardsTopTitle"><%=ResHelper.LocalizeString("{$=Στοιχεία Χρήστη|en-us=User Information$}") %></div>
        <div class="CUDContentBG">
           <div style="color:#43474a;font-size:12px;padding-top:10px">              <div style="padding-left:17px;height:35px;line-height:35px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:165px;padding-right:10px;text-align:right"><%=ResHelper.LocalizeString("{$=Επωνυμία Επιχείρησης|en-us=Company Name$}") %>:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtFullName" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  runat="server" ControlToValidate="txtFullName" text="*" ValidationGroup="MerchantsForm"/>
                </div> 
             </div>
             <div style="padding-top:10px"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
             <div style="padding-left:17px;height:40px;line-height:40px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:165px;padding-right:10px;text-align:right"><%=ResHelper.LocalizeString("{$=Υπεύθυνος Επικοινωνίας|en-us=Contact Person$}") %>:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtContactManager" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"  runat="server" ControlToValidate="txtContactManager" text="*" ValidationGroup="MerchantsForm"/>
                </div> 
             </div>
             <div style="padding-top:5px"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
             <div style="padding-left:17px;height:40px;line-height:40px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:165px;padding-right:10px;text-align:right"><%=ResHelper.LocalizeString("{$=Τηλεφωνο Επικοινωνίας|en-us=Phone Number$}") %>:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtPhone" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  runat="server" ControlToValidate="txtPhone" text="*" ValidationGroup="MerchantsForm"/>
                </div> 
             </div>
             <div style="padding-top:5px"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
             <div style="padding-left:17px;height:40px;line-height:40px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:165px;padding-right:10px;text-align:right">E-mail:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtEmail" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ErrorMessage="Email" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEmail" text="*" ValidationGroup="MerchantsForm"/>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" 
                    ValidationExpression="^([a-zA-Z0-9_\-])+(\.([a-zA-Z0-9_\-])+)*@((\[(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5]))\]))|((([a-zA-Z0-9])+(([\-])+([a-zA-Z0-9])+)*\.)+([a-zA-Z])+(([\-])+([a-zA-Z0-9])+)*))$" 
                    Display="Dynamic"  Text="*" ValidationGroup="MerchantsForm"/>
                </div> 
             </div>
            <div style="padding-top:5px"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
             <div style="padding-left:17px;height:40px;line-height:40px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:165px;padding-right:10px;text-align:right"><%=ResHelper.LocalizeString("{$=Α.Φ.Μ.|en-us=Vat Number$}") %>:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtAFM" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5"  runat="server" ControlToValidate="txtAFM" text="*" ValidationGroup="MerchantsForm"/>
                </div> 
              </div>
            
             <div style="padding-top:5px"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
              <div style="padding-left:17px;line-height:40px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:165px;padding-right:10px;text-align:right"><%=ResHelper.LocalizeString("{$=Σχόλια|en-us=Comments$}") %>:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox TextMode="MultiLine" Rows="3" ID="txtComments" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"  runat="server" ControlToValidate="txtFullName" text="*" ValidationGroup="MerchantsForm"/>
                </div> 
             </div>
             <div style="padding-left:17px;padding-top:5px;padding-bottom:10px">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:165px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px">
                   <asp:ImageButton id="BtnSend" runat="server" ValidationGroup="MerchantsForm" />
                </div>
                <div class="Clear"></div>
            </div>
           </div>
        </div> 
        <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
</div> 
<asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList"  ShowMessageBox="True" ShowSummary="false" ErrorMessage="" ValidationGroup="MerchantsForm"/>