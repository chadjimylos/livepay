<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyMessages_IgnoreList.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Messaging_MyMessages_MyMessages_IgnoreList"
    Title="My messages - Inbox" ValidateRequest="false" Theme="Default" %>

<%@ Register Src="~/CMSModules/Messaging/Controls/IgnoreList.ascx" TagName="IgnoreList"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:IgnoreList ID="ignoreListElem" runat="server" IsLiveSite="false" />
</asp:Content>
