﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Widget_Edit_Properties.aspx.cs"
    Inherits="CMSModules_Widgets_UI_Widget_Edit_Properties" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Widget Edit - Properties" Theme="Default" %>

<%@ Register Src="~/CMSModules/Widgets/Controls/WidgetPropertiesEdit.ascx" TagName="WidgetPropertiesEdit"
    TagPrefix="cms" %>
    
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:WidgetPropertiesEdit ID="widgetProperties" runat="server" />
</asp:Content>
