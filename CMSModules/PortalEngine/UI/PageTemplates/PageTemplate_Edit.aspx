<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageTemplate_Edit.aspx.cs" Inherits="CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_Edit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Page Templates</title>
</head>
	<frameset border="0" rows="102, *" id="rowsFrameset">
		<frame name="pt_edit_menu" src="PageTemplate_Header.aspx?templateid=<%=Request.QueryString["templateid"] %>&nobreadcrumbs=<%=Request.QueryString["nobreadcrumbs"] %>" scrolling="no" frameborder="0" noresize="noresize" />		
	    <frame name="pt_edit_content" src="PageTemplate_General.aspx?templateid=<%=Request.QueryString["templateid"] %>&nobreadcrumbs=<%=Request.QueryString["nobreadcrumbs"] %>" frameborder="0" />
	<noframes>
		<body>
		 <p id="p1">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		 </p>
		</body>
	</noframes>
</frameset>
</html>
