using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.EventManager;
using CMS.UIControls;

public partial class CMSModules_EventManager_Tools_Events_Attendee_Edit : CMSEventManagerPage
{
    protected int attendeeId = 0;
    protected int eventNodeId = 0;
    protected EventAttendeeInfo eai = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        InitGUI();

        eventNodeId = QueryHelper.GetInteger("eventNodeId", 0);
        attendeeId = QueryHelper.GetInteger("attendeeId", 0);

        if (attendeeId > 0)
        {
            eai = EventAttendeeInfoProvider.GetEventAttendeeInfo(attendeeId);
        }

        string attEmail = ResHelper.GetString("Events_Attendee_Edit.NewItemCaption");
        if (eai != null)
        {
            if (!RequestHelper.IsPostBack())
            {
                txtFirstName.Text = eai.AttendeeFirstName;
                txtLastName.Text = eai.AttendeeLastName;
                txtEmail.Text = eai.AttendeeEmail;
                txtPhone.Text = eai.AttendeePhone;

                // Show that the attendee was created or updated successfully
                if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                {
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }

                // Show warning if duplicit email was used
                if (ValidationHelper.GetString(Request.QueryString["duplicit"], "") == "1")
                {
                    lblInfo.Visible = true;
                    lblInfo.Text = lblInfo.Text + "<br />" + ResHelper.GetString("eventmanager.attendeeregisteredwarning");
                }
            }
            attEmail = eai.AttendeeEmail;
        }

        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("Events_Attendee_Edit.itemlistlink");
        breadcrumbs[0, 1] = "~/CMSModules/EventManager/Tools/Events_Attendee_List.aspx?eventid=" + eventNodeId;
        breadcrumbs[0, 2] = "eventsContent";
        breadcrumbs[1, 0] = attEmail;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.Title.HelpTopicName = "attendees_edit";
    }


    /// <summary>
    /// Init GUI
    /// </summary>
    protected void InitGUI()
    {
        lblFirstName.Text = ResHelper.GetString("Event_Attendee_Edit.lblFirstName");
        lblLastName.Text = ResHelper.GetString("Event_Attendee_Edit.lblLastName");
        lblPhone.Text = ResHelper.GetString("Event_Attendee_Edit.lblPhone");
        btnOk.Text = ResHelper.GetString("general.ok");

        rfvEmail.Text = ResHelper.GetString("Event_Attendee_Edit.rfvEmail");
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.eventmanager", "Modify"))
        {
            RedirectToCMSDeskAccessDenied("cms.eventmanager", "Modify");
        }

        txtEmail.Text = txtEmail.Text.Trim();

        // Validate fields
        string errorMessage = new Validator()
            .NotEmpty(txtEmail.Text, rfvEmail.ErrorMessage)
            .IsEmail(txtEmail.Text, ResHelper.GetString("Event_Attendee_Edit.IncorectEmailFormat"))
            .Result;

        // Update database
        if (errorMessage == "")
        {
            // Indicates new attendee
            bool isNew = false;

            // Indicates duplicit attendee/email
            bool isDuplicit = false;

            if (attendeeId == 0)
            {
                eai = new EventAttendeeInfo();
                eai.AttendeeEventNodeID = eventNodeId;
                isNew = true;
                isDuplicit = EventAttendeeInfoProvider.GetEventAttendeeInfo(eventNodeId, txtEmail.Text) != null;
            }

            if (eai != null)
            {
                eai.AttendeeFirstName = txtFirstName.Text;
                eai.AttendeeLastName = txtLastName.Text;
                eai.AttendeeEmail = txtEmail.Text;
                eai.AttendeePhone = txtPhone.Text;
                EventAttendeeInfoProvider.SetEventAttendeeInfo(eai);

                if (!isNew)
                {
                    UrlHelper.Redirect("Events_Attendee_Edit.aspx?eventnodeid=" + Convert.ToString(eventNodeId) + "&attendeeId=" + Convert.ToString(eai.AttendeeID) + "&saved=1");
                }
                else
                {
                    ltlScript.Text = ScriptHelper.GetScript("RefreshHeader(" + Convert.ToString(eai.AttendeeID) + ", " + (isDuplicit ? "1" : "0") + ");");
                }
            }
            else
            {
                errorMessage = ResHelper.GetString("general.invalidid");
            }
        }

        if (errorMessage != "")
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
