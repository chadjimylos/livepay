﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UcUserRegistration.ascx.vb" Inherits="CMSTemplates_LivePay_UcUserRegistration" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc2" %>

<script language="javascript" type="text/javascript" >

    function MyButtonOnClientClick() {
        Page_ClientValidate();
        if (Page_IsValid) {
            CheckConfirm()
            if (Page_IsValid) {
                CheckAcceptTerms()
            }
            return Page_IsValid;
        }
        else {
            return false;
        }
    }

    function CheckConfirm() {
        var txtConfirm = document.getElementById('<%=txtConfirmPass.ClientID %>').value
        var txtPass = document.getElementById('<%=txtPassword.ClientID %>').value
        var ChbAcceptTerms = document.getElementById('<%=chbAcceptTerms.ClientID %>')
        var ErrorMessage = '';

        if (txtPass.length < 5) {  /// --------- Check to leng tou password na min einai kato apo 5 gramata
            Page_IsValid = false
            ErrorMessage = '- Ο Κωδικός Πρόσβασης πρέπει να είναι τουλάχιστον 5 χαρακτήρες.'
        }


        if (txtConfirm != txtPass) {/// --------- Check to password na einai idio me to Confirm

            if (ErrorMessage != '') {
                ErrorMessage = ErrorMessage + '\r- Τα πεδία Κωδικός Πρόσβασης και Επιβεβαίωση Κωδικού πρέπει να έχουν ίδια τιμή.'
            } else {
                ErrorMessage = '- Τα πεδία Κωδικός Πρόσβασης και Επιβεβαίωση Κωδικού πρέπει να έχουν ίδια τιμή.'
            }
            Page_IsValid = false;
        }

        
        if (ChbAcceptTerms.checked == false) {/// --------- Check to Checkbox tis apodoxis ton oron na einai chekarismeno
            if (ErrorMessage != '') {
                ErrorMessage = ErrorMessage + '\r- Δεν έχετε τσεκάρει την αποδοχή των όρων χρήσεις.'
            } else {
                ErrorMessage = '- Δεν έχετε τσεκάρει την αποδοχή των όρων χρήσεις.'
            }
            Page_IsValid = false;
        }

        if (Page_IsValid == false) {
            alert(ErrorMessage);
        }
    }



</script>
 <div class="CUDDarkBlueBGTitle">
    <div class="SvdCardsTopTitle">Εγγραφή Χρήστη</div>
    <div class="CUDContentBG">
        <div style="font-family:Tahoma;font-size:12px;color:#000000;line-height:55px;vertical-align:middle;padding-left:15px ">Γίνετε μέλος σήμερα και αποκτήστε προσωποποιημένες υπηρεσίες συμπληρώνοντας την παρακάτω φόρμα</div>
        <div>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">E-mail:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtEmail" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ErrorMessage="Email" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" text="*" ValidationGroup="RegisterForm"/>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" 
                    ValidationExpression="^([a-zA-Z0-9_\-])+(\.([a-zA-Z0-9_\-])+)*@((\[(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5]))\]))|((([a-zA-Z0-9])+(([\-])+([a-zA-Z0-9])+)*\.)+([a-zA-Z])+(([\-])+([a-zA-Z0-9])+)*))$" 
                    Display="Dynamic" ErrorMessage="Συμπληρώστε έγκυρη E-mail διεύθυνση" Text="*" ValidationGroup="RegisterForm"/>
                </div>
            </div>
            <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">Ονοματεπώνυμο:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtFullName" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Ονοματεπώνυμο" runat="server" ControlToValidate="txtFullName" text="*" ValidationGroup="RegisterForm"/>
                </div> 
            </div>
            <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">Κωδικός Πρόσβασης:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtPassword" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                 <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Κωδικός Πρόσβασης" runat="server" ControlToValidate="txtPassword" text="*" ValidationGroup="RegisterForm"/>
                </div> 
            </div>


             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">Επιβεβαίωση Κωδικού:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtConfirmPass" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Επιβεβαίωση Κωδικού" runat="server" ControlToValidate="txtConfirmPass" text="*" ValidationGroup="RegisterForm"/>
                </div> 
            </div>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">Τηλέφωνο:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="TextBox4" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div class="Clear"></div>
            </div>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">Ερώτηση Ασφαλέιας:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtQuestion" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ErrorMessage="Ερώτηση Ασφαλέιας" runat="server" ControlToValidate="txtQuestion" text="*" ValidationGroup="RegisterForm"/>
                </div> 
            </div>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">Απάντηση:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtAnswer" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                 <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ErrorMessage="Απάντηση" runat="server" ControlToValidate="txtAnswer" text="*" ValidationGroup="RegisterForm"/>
                </div> 
            </div>
            
            <div >
                <div style="float:left;width:155px;">&nbsp;</div>
                <div style="float:left" >
                    <cc2:CaptchaControl ID="Captchas" runat="server" BorderWidth="1px" BorderColor="#3b6db4" BorderStyle="Solid"
	                    CaptchaBackgroundNoise="None"
	                    CaptchaLength="5" 
	                    CaptchaHeight="58"
	                    CaptchaWidth="236"
	                    Width="236"
	                    CaptchaLineNoise="Extreme" 
	                    CacheStrategy="HttpRuntime"
	                    CaptchaMaxTimeout="240" 
                    />
                </div>
                <div class="Clear"></div>
            </div>
             <div style="height:30px;line-height:30px;vertical-align:middle">
                <div style="float:left;width:155px;">&nbsp;</div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;">Πληκτρολογήστε το κείμενο που εμφανίζεται στην παραπάνω εικόνα</div>
                <div class="Clear"></div>

            </div>

             <div style="">
                <div style="float:left;width:155px;">&nbsp;</div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;"><asp:TextBox ID="txtCaptcha" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ErrorMessage="Πληκτρολογήστε το κείμενο που εμφανίζεται στην παραπάνω εικόνα" runat="server" ControlToValidate="txtCaptcha" text="*" ValidationGroup="RegisterForm"/>
                </div> 
            </div>
             <div style="">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right;padding-top:12px">Όροι Χρήσης: </div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="TextBox7" TextMode="MultiLine" Rows="3" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="420px"/></div>
                <div class="Clear"></div>
            </div>
            <div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:6px">
                    <asp:CheckBox ID="chbAcceptTerms" runat="server" style="color:#43474a;font-size:11px" Text="Αποδέχομαι τους Όρους Χρήσης" />
                </div>
                <div class="Clear"></div>
            </div>
             <div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px">
                    <asp:CheckBox ID="chbAcceptSendNews" runat="server" style="color:#43474a;font-size:11px" Text="Αποδέχομαι την Αποστολή Ενημερωτικού υλικού απο την Eurobank" />
                </div>
                <div class="Clear"></div>
            </div>
            <div style="padding-top:5px;padding-bottom:10px">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px">
                   <asp:ImageButton ImageUrl="/App_Themes/LivePay/UserRegistration/BtnRegister.png" OnClientClick="return MyButtonOnClientClick()" id="BtnInsert" runat="server"  />
                </div>
                <div class="Clear"></div>
            </div>
             <div style="padding-top:5px;padding-bottom:10px">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px;font-size:10px;color:#094595">Τα πεδία με τον αστερίσκο (*) είναι υποχρεωτικά
                </div>
                <div class="Clear"></div>
            </div>
        </div>
    </div>
    <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
</div>

<asp:ValidationSummary HeaderText="Παρακαλώ συμπληρώστε τα ακόλουθα πεδία:" ID="ValidationSummary1" runat="server" DisplayMode="BulletList"  ShowMessageBox="True" ShowSummary="false" ErrorMessage="" ValidationGroup="RegisterForm"/>