CREATE PROCEDURE [Proc_Forums_ForumGroup_RemoveDependencies] 
    @ID int
AS
BEGIN
UPDATE [Forums_ForumPost] SET PostParentID=NULL WHERE PostForumId IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID = @ID
    );
DELETE FROM [Forums_ForumSubscription] WHERE SubscriptionPostID IN (
        SELECT PostID FROM Forums_ForumPost WHERE (PostForumId  IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID = @ID
    )));
DELETE FROM [Forums_Attachment] WHERE AttachmentPostID IN (
        SELECT PostID FROM Forums_ForumPost WHERE (PostForumId  IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID = @ID
    )));
DELETE FROM [Forums_UserFavorites] WHERE PostID IN (
        SELECT PostID FROM Forums_ForumPost WHERE (PostForumId  IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID = @ID
    )));
DELETE FROM [Forums_UserFavorites] WHERE ForumID IN (
    SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID = @ID
    );
DELETE FROM [Forums_ForumPost] WHERE PostForumId IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID = @ID
    );
DELETE FROM [Forums_ForumRoles ] WHERE ForumId IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID = @ID
    );
DELETE FROM [Forums_ForumModerators] WHERE ForumID IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID = @ID
    );
DELETE FROM [Forums_ForumSubscription] WHERE SubscriptionForumId IN (
        SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID = @ID
    );
DELETE FROM [Forums_Forum] WHERE ForumGroupId = @ID;
END
