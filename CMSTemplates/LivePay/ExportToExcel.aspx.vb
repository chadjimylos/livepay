﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.IO
Imports System.Threading
Imports System.Globalization
Imports LivePay_ESBBridge

Partial Class CMSTemplates_LivePay_ExportToExcel
    Inherits System.Web.UI.Page


#Region " Properties "

    Protected ReadOnly Property Lang() As String
        Get
            Dim sLang As String = "el-gr"
            If Not String.IsNullOrEmpty(Request("lang")) Then
                sLang = Request("lang")
            End If
            Return sLang
        End Get
    End Property

    Protected ReadOnly Property ExportType() As String
        Get
            Dim sCode As String = String.Empty
            If Not String.IsNullOrEmpty(Request("ExportType")) Then
                sCode = Request("ExportType")
            End If
            Return sCode
        End Get
    End Property

    Protected ReadOnly Property TransactionID() As String
        Get
            Dim mTransactionID As String = "0"
            If Not String.IsNullOrEmpty(Request("TransactionID")) Then
                mTransactionID = Request("TransactionID")
            End If
            Return mTransactionID
        End Get
    End Property

    Protected ReadOnly Property NodeID() As Integer
        Get
            Dim mNodeID As Integer = 0
            If Not String.IsNullOrEmpty(Request("NodeID")) Then
                mNodeID = Request("NodeID")
            End If
            Return mNodeID
        End Get
    End Property

    Private FileName As String = String.Empty
#End Region




    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Me.Lang)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(Me.Lang)
        MyBase.InitializeCulture()

        If Me.ExportType = "SrcHisPay" Then
            GridViewPayments.Columns(0).HeaderText = ResHelper.LocalizeString("{$=Ημερομηνία Συναλλαγής|en-us=Transaction Date$}")
            GridViewPayments.Columns(1).HeaderText = ResHelper.LocalizeString("{$=Πληρωμή προς|en-us=Payment to$}")
            GridViewPayments.Columns(2).HeaderText = ResHelper.LocalizeString("{$=Κάρτα|en-us=Card$}")
            GridViewPayments.Columns(3).HeaderText = ResHelper.LocalizeString("{$=Ποσό(€)|en-us=Amount(€)$}")
            GridViewPayments.Columns(4).HeaderText = ResHelper.LocalizeString("{$=Αποτέλεσμα|en-us=Results$}")
            GridViewPayments.Columns(5).HeaderText = ResHelper.LocalizeString("{$=Τύπος|en-us=Type$}")
            divRep1.Visible = True
            GridViewPayments.DataSource = Session("GetSrcHisPayDataTable")
            GridViewPayments.DataBind()
            Dim strFileName As String = "Istoriko_Pliromon -" & Year(Now) & "-" & Month(Now) & "-" & Day(Now)
            Dim tw As New StringWriter()
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            Dim frm As HtmlForm = New HtmlForm()
            Page.Response.ContentType = "application/vnd.ms-excel"
            Page.Response.AddHeader("content-disposition", "filename=" & strFileName & ".xls")
            Page.Response.HeaderEncoding = Encoding.UTF8
            Page.Response.Charset = ""
            Page.EnableViewState = False
            frm.Attributes("runat") = "server"
            Controls.Add(frm)
            frm.Controls.Add(GridViewPayments)
            frm.RenderControl(hw)
            Response.Write(tw.ToString())
            Response.End()
            GridViewPayments.DataBind()
        End If


        If Me.ExportType = "MerchantsSrcHisPay" Then
            GridViewMerchantsPayments.Columns(0).HeaderText = ResHelper.LocalizeString("{$=Αριθμός Πακέτου|en-us=Batch Number$}")
            GridViewMerchantsPayments.Columns(1).HeaderText = ResHelper.LocalizeString("{$=Ημ/νια Κλεισιμάτος<br>Πακέτου|en-us=Batch<br>Submission<br>Date$}")
            GridViewMerchantsPayments.Columns(2).HeaderText = ResHelper.LocalizeString("{$=Κατάσταση Πακέτου|en-us=Batch Status$}")
            GridViewMerchantsPayments.Columns(3).HeaderText = ResHelper.LocalizeString("{$=Κωδικός Συναλλαγής|en-us=Transaction Code$}")
            GridViewMerchantsPayments.Columns(4).HeaderText = ResHelper.LocalizeString("{$=Ημ/νια Συναλλαγής|en-us=Transaction<br>Date$}")
            GridViewMerchantsPayments.Columns(5).HeaderText = ResHelper.LocalizeString("{$=Ονομ/επώνυμο Πελάτη|en-us=Customer<br>Full Name$}")
            GridViewMerchantsPayments.Columns(6).HeaderText = ResHelper.LocalizeString("{$=Ποσό(€)|en-us=Amount(€)$}")
            GridViewMerchantsPayments.Columns(7).HeaderText = ResHelper.LocalizeString("{$=Τύπος Συν/γής|en-us=Transaction<br>Type$}")
            GridViewMerchantsPayments.Columns(8).HeaderText = ResHelper.LocalizeString("{$=Δόσεις |en-us=installments $}")
            GridViewMerchantsPayments.Columns(9).HeaderText = ResHelper.LocalizeString("{$=Κατάσταση Συναλλαγής|en-us=Transaction Status$}")

            'Response.Write("LivePayID --> " & CMSContext.CurrentUser.GetValue("LivePayID"))
            'Response.End()

            'Dim dtNodeID As DataTable = DBConnection.GetMerchantNodeID(CMSContext.CurrentUser.GetValue("LivePayID"))
            'Dim NodeID As Integer = 0

            'If Not DataHelper.DataSourceIsEmpty(dtNodeID) Then
            '    NodeID = Convert.ToInt32(dtNodeID.Columns("nodeID").ToString)
            'End If

            Dim ds As DataSet = DBConnection.GetMerchantCustomFields(NodeID)
            Dim dt As DataTable = ds.Tables(0)

            For i As Integer = 0 To dt.Rows.Count - 1
                Dim CustomFieldValue As String = String.Empty
                If Me.TransactionID <> 0 Then
                    Dim FieldID As Integer = dt.Rows(i)("ItemID")

                    Dim FieldValueDT As DataTable = DBConnection.GetFavoritesCustomFields(Me.TransactionID, FieldID, CMSContext.CurrentUser.UserID)
                    If FieldValueDT.Rows.Count > 0 Then
                        CustomFieldValue = FieldValueDT.Rows(0)(0).ToString()
                    End If
                End If

                GridViewMerchantsPayments.Columns(10 + i).HeaderText = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
                GridViewMerchantsPayments.Columns(10 + i).Visible = True
            Next

            Dim DataSession As LivePay_ESBBridge.TransactionInfo()

            If Session("Merchants_GetSrcHisPayDataTable") IsNot Nothing OrElse String.Empty.Equals(Session("Merchants_GetSrcHisPayDataTable")) Then
                Select Case Session("Merchants_GetSrcHisPayDataTable").GetType().ToString()
                    Case "LivePay_ESBBridge.TransactionInfo", "LivePay_ESBBridge.TransactionInfo[]"
                        DataSession = CType(Session("Merchants_GetSrcHisPayDataTable"), LivePay_ESBBridge.TransactionInfo())
                        If DataSession IsNot Nothing Then
                            For Each row As TransactionInfo In DataSession
                                row.info1 = "&nbsp;" & row.info1
                                row.info2 = "&nbsp;" & row.info2
                                row.info3 = "&nbsp;" & row.info3
                                row.info4 = "&nbsp;" & row.info4
                                row.info5 = "&nbsp;" & row.info5
                                row.info6 = "&nbsp;" & row.info6
                                row.info7 = "&nbsp;" & row.info7
                                row.info8 = "&nbsp;" & row.info8
                                row.info9 = "&nbsp;" & row.info9
                                row.info10 = "&nbsp;" & row.info10
                            Next
                        End If
                End Select
            End If

            divRep2.Visible = True
            GridViewMerchantsPayments.DataSource = Session("Merchants_GetSrcHisPayDataTable")
            GridViewMerchantsPayments.DataBind()
            Dim strFileName As String = "Istoriko_Pliromon_Emporou -" & Year(Now) & "-" & Month(Now) & "-" & Day(Now)
            Dim tw As New StringWriter()
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            Dim frm As HtmlForm = New HtmlForm()
            Page.Response.ContentType = "application/vnd.ms-excel"
            Page.Response.HeaderEncoding = Encoding.UTF8
            Page.Response.AddHeader("content-disposition", "filename=" & strFileName & ".xls")
            Page.Response.Charset = ""
            Page.EnableViewState = False
            frm.Attributes("runat") = "server"
            Controls.Add(frm)
            frm.Controls.Add(GridViewMerchantsPayments)
            frm.RenderControl(hw)
            Response.Write(tw.ToString())
            Response.End()
            GridViewMerchantsPayments.DataBind()
        End If
    End Sub


#Region " GridViewMerchantsPayments "

    Function GetTransactionStatusDescr(ByVal status As LivePay_ESBBridge.TxnResultEnum) As String
        'Failed  1, Reversal 2, Successful 0, Successful_Uploaded 4, Voided 3, Voided_Uploaded 5
        Select Case status
            Case 0 : Return "Επιτυχημένη"
            Case 1 : Return "Αποτυχημένη"
            Case 2 : Return "Αντιλογισμός"
            Case 3 : Return "Ακυρωμένη"
            Case 4 : Return "Ολοκληρωμένη Συναλλαγή"
            Case 5 : Return "Ακυρωμένη Συναλλαγή"
        End Select
        Return status
    End Function

    Sub GridViewMerchantsPayments_DataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridViewMerchantsPayments.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblTransCode As Label = DirectCast(e.Row.FindControl("lblTransCode"), Label)
            Dim myTransactionInfo As TransactionInfo = e.Row.DataItem

            Dim Merchants As Boolean = IIf(myTransactionInfo.system = LivePay_ESBBridge.SystemEnum.Merchants, True, False)

            If Merchants Then
                lblTransCode.Text = myTransactionInfo.transactionId
            Else
                lblTransCode.Text = myTransactionInfo.info1
            End If


            Dim amount As String = CType(e.Row.FindControl("lblAmount"), Label).Text
            amount = Replace(amount, ".", ",")

            If amount.Split(",").Length > 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
            ElseIf amount.Split(",").Length = 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & ",00"
            Else
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount
            End If
        End If
    End Sub

#End Region

    Protected Sub GridViewPayments_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewPayments.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim amount As String = CType(e.Row.FindControl("lblAmount"), Label).Text
            Dim lblMerchantID As Label = DirectCast(e.Row.FindControl("lblMerchantID"), Label)
            amount = Replace(amount, ".", ",")
            If amount.Split(",").Length > 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
            ElseIf amount.Split(",").Length = 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & ",00"
            Else
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount
            End If


            Dim MerachantID As Integer = lblMerchantID.Text

            Dim dt As DataTable = DBConnection.GetMerchantByESBID(lblMerchantID.Text, CultureInfo.CurrentCulture.Name)
            If dt.Rows.Count > 0 Then
                lblMerchantID.Text = dt.Rows(0)("DiscreetTitle").ToString
            End If
        End If
    End Sub


    Function GetTransactionStatusDescrPay(ByVal status As LivePay_ESBBridge.TxnResultEnum) As String
        'Failed  1, Reversal 2, Successful 0, Successful_Uploaded 4, Voided 3, Voided_Uploaded 5
        Select Case status
            Case 0 : Return "Επιτυχημένη"
            Case 1 : Return "Αποτυχημένη"
            Case 2 : Return "Αντιλογισμός"
            Case 3 : Return "Ακυρωμένη"
            Case 4 : Return "Επιτυχημένη Συναλλαγή"
            Case 5 : Return "Επιτυχημένη Συναλλαγή"
        End Select
        Return status
    End Function
End Class
