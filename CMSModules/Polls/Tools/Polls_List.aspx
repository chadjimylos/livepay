<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Polls_List.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Polls"
    Inherits="CMSModules_Polls_Tools_Polls_List" Theme="Default" %>
<%@ Register Src="~/CMSModules/Polls/Controls/PollsList.ascx" TagName="PollsList"
    TagPrefix="cms" %>
        
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:PollsList ID="PollsList" runat="server" Visible="true" DelayedReload="false" />
</asp:Content>
