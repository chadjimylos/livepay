<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShoppingCartSKUPriceDetail.ascx.cs"
    Inherits="CMSModules_Ecommerce_Controls_ShoppingCart_ShoppingCartSKUPriceDetail_Control" %>
<%--Product--%>
<table border="0" cellspacing="0" cellpadding="3" class="UniGridGrid">
    <tr class="UniGridHead PriceDetailHeader">
        <td colspan="2">
            <asp:Label ID="lblProductName" runat="server" EnableViewState="false" />
        </td>
    </tr>
    <tr class="EvenRow PriceDetailSubtotal">
        <td>
            <asp:Label ID="lblPriceWithoutTax" runat="server" EnableViewState="false" />
        </td>
        <td class="TextRight">
            <asp:Label ID="lblPriceWithoutTaxValue" runat="server" EnableViewState="false" />
        </td>
    </tr>
</table>
<br />
<%--Discounts--%>
<asp:GridView ID="gridDiscounts" runat="server" AutoGenerateColumns="false" CssClass="UniGridGrid PriceDetailSummaryTable"
    CellPadding="3" CellSpacing="0">
    <HeaderStyle CssClass="UniGridHead" />
    <AlternatingRowStyle CssClass="OddRow" />
    <RowStyle CssClass="EvenRow" />
    <Columns>
        <asp:TemplateField>
            <HeaderStyle CssClass="TextLeft" />
            <ItemTemplate>
                <%# HTMLHelper.HTMLEncode(GetFormattedName( Eval("DiscountName"))) %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderStyle Width="80" />
            <ItemStyle CssClass="TextRight" Width="80" />
            <ItemTemplate>
                <%# GetFormattedValue( Eval("DiscountValue"), Eval("DiscountIsFlat"), true, true) %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderStyle Width="80" />
            <ItemStyle CssClass="TextRight" Width="80" />
            <ItemTemplate>
                <%# GetFormattedValue(Eval("DiscountRealValue"), true, true, false)%>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>
<%--Total discount--%>
<table border="0" class="UniGridGrid PriceDetailSubtotalTable" cellpadding="3" cellspacing="0">
    <asp:PlaceHolder ID="plcDiscounts" runat="server">
        <tr class="UniGridHead">
            <th colspan="2" class="TextLeft">
                <asp:Label ID="lblDiscounts" runat="server" EnableViewState="false" />
            </th>
        </tr>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcTotalDiscount" runat="server">
        <tr class="EvenRow">
            <td>
                <asp:Label ID="lblTotalDiscount" runat="server" EnableViewState="false" />
            </td>
            <td class="TextRight">
                <asp:Label ID="lblTotalDiscountValue" runat="server" EnableViewState="false" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr class="OddRow PriceDetailSubtotal">
        <td>
            <asp:Label ID="lblPriceAfterDiscount" runat="server" EnableViewState="false" />
        </td>
        <td class="TextRight">
            <asp:Label ID="lblPriceAfterDiscountValue" runat="server" EnableViewState="false" />
        </td>
    </tr>
</table>
<br />
<%--Taxes--%>
<asp:GridView ID="gridTaxes" runat="server" AutoGenerateColumns="false" CssClass="UniGridGrid PriceDetailSummaryTable"
    CellPadding="3" CellSpacing="0">
    <HeaderStyle CssClass="UniGridHead" />
    <AlternatingRowStyle CssClass="OddRow" />
    <RowStyle CssClass="EvenRow" />
    <Columns>
        <asp:TemplateField>
            <HeaderStyle CssClass="TextLeft" />
            <ItemTemplate>
                <%# HTMLHelper.HTMLEncode(GetFormattedName( Eval("TaxClassDisplayName"))) %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderStyle Width="80" />
            <ItemStyle CssClass="TextRight" Width="80" />
            <ItemTemplate>
                <%# GetFormattedValue(Eval("TaxValue"), Eval("TaxIsFlat"), false, true)%>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderStyle Width="80" />
            <ItemStyle CssClass="TextRight" Width="80" />
            <ItemTemplate>
                <%# GetFormattedValue(Eval("TaxRealValue"), true, false, false)%>
            </ItemTemplate>
        </asp:TemplateField>
        <%--<asp:BoundField DataField="TaxClassID" Visible="false" />--%>
    </Columns>
</asp:GridView>
<br />
<%--Total tax---%>
<table border="0" class="UniGridGrid PriceDetailSubtotalTable" cellpadding="3" cellspacing="0">
    <asp:PlaceHolder ID="plcTaxes" runat="server">
        <tr class="UniGridHead">
            <th colspan="2" class="TextLeft">
                <asp:Label ID="lblTaxes" runat="server" EnableViewState="false" />
            </th>
        </tr>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcTotalTax" runat="server">
        <tr class="EvenRow">
            <td>
                <asp:Label ID="lblTotalTax" runat="server" EnableViewState="false" />
            </td>
            <td class="TextRight">
                <asp:Label ID="lblTotalTaxValue" runat="server" EnableViewState="false" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr class="OddRow PriceDetailSubtotal">
        <td>
            <asp:Label ID="lblPriceWithTax" runat="server" EnableViewState="false" />
        </td>
        <td class="TextRight">
            <asp:Label ID="lblPriceWithTaxValue" runat="server" EnableViewState="false" />
        </td>
    </tr>
</table>
<br />
<%--Totals--%>
<table border="0" width="100%" cellspacing="0" class="UniGridGrid" cellpadding="3">
    <tr class="UniGridHead">
        <th colspan="2">
            <cms:LocalizedLabel ID="lblTotal" ResourceString="ProductPriceDetail.Total" runat="server"
                EnableViewState="false" />
        </th>
    </tr>
    <tr class="EvenRow PriceDetailSubtotal">
        <td>
            <asp:Label ID="lblProductUnits" runat="server" EnableViewState="false" />
        </td>
        <td class="TextRight">
            <asp:Label ID="lblProductUnitsValue" runat="server" EnableViewState="false" />
        </td>
    </tr>
    <tr class="OddRow PriceDetailHeader">
        <td>
            <asp:Label ID="lblTotalPrice" runat="server" EnableViewState="false" />
        </td>
        <td class="TextRight">
            <asp:Label ID="lblTotalPriceValue" runat="server" EnableViewState="false" />
        </td>
    </tr>
</table>
