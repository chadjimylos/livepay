using System;
using System.Data;
using System.Collections;
using System.Web;
using System.Web.UI.WebControls;
using System.Text;

using CMS.FormEngine;
using CMS.FormControls;
using CMS.WorkflowEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.ExtendedControls;
using CMS.PortalEngine;
using CMS.EventLog;
using CMS.DataEngine;
using CMS.URLRewritingEngine;
using CMS.Controls;
using CMS.UIControls;
using CMS.LicenseProvider;

using TreeNode = CMS.TreeEngine.TreeNode;


public partial class CMSModules_Content_Controls_UserContributions_EditForm : CMSUserControl
{
    #region "Variables"

    /// <summary>
    /// On after aprove event
    /// </summary>
    public event EventHandler OnAfterApprove = null;

    /// <summary>
    /// On after reject event
    /// </summary>
    public event EventHandler OnAfterReject = null;


    /// <summary>
    /// On after delete event
    /// </summary>
    public event EventHandler OnAfterDelete = null;


    private bool confirmChanges = false;
    private TreeNode node = null;
    private TreeProvider tree = null;
    private DataClassInfo ci = null;

    /// <summary>
    /// Data properties variable
    /// </summary>
    private readonly CMSDataProperties mDataProperties = new CMSDataProperties();

    #endregion


    #region "Document properties"

    /// <summary>
    /// Culture code
    /// </summary>
    public string CultureCode
    {
        get
        {
            return mDataProperties.CultureCode;
        }
        set
        {
            mDataProperties.CultureCode = value;
        }
    }


    /// <summary>
    /// Site name
    /// </summary>
    public string SiteName
    {
        get
        {
            return mDataProperties.SiteName;
        }
        set
        {
            mDataProperties.SiteName = value;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Returns true if new document mode
    /// </summary>
    public bool NewDocument
    {
        get
        {
            return (Action.ToLower() == "new");
        }
    }


    /// <summary>
    /// Returns true if new culture mode
    /// </summary>
    public bool NewCulture
    {
        get
        {
            return (Action.ToLower() == "newculture");
        }
    }


    /// <summary>
    /// Returns true in delete mode
    /// </summary>
    public bool Delete
    {
        get
        {
            return (Action.ToLower() == "delete");
        }
    }


    /// <summary>
    /// Node ID
    /// </summary>
    public int NodeID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["NodeID"], 0);
        }
        set
        {
            ViewState["NodeID"] = value;
            menuElem.NodeID = value;
        }
    }


    /// <summary>
    /// Form Action (mode)
    /// </summary>
    public string Action
    {
        get
        {
            return ValidationHelper.GetString(ViewState["Action"], "");
        }
        set
        {
            ViewState["Action"] = value;
        }
    }


    /// <summary>
    /// Class ID
    /// </summary>
    public int ClassID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["ClassID"], 0);
        }
        set
        {
            ViewState["ClassID"] = value;
        }
    }


    /// <summary>
    /// Alternative form name
    /// </summary>
    public string AlternativeFormName
    {
        get
        {
            return ValidationHelper.GetString(ViewState["AlternativeFormName"], null);
        }
        set
        {
            ViewState["AlternativeFormName"] = value;
        }
    }


    /// <summary>
    /// Form validation error message
    /// </summary>
    public string ValidationErrorMessage
    {
        get
        {
            return ValidationHelper.GetString(ViewState["ValidationErrorMessage"], null);
        }
        set
        {
            ViewState["ValidationErrorMessage"] = value;
        }
    }


    /// <summary>
    /// Template ID
    /// </summary>
    public int TemplateID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["TemplateID"], 0);
        }
        set
        {
            ViewState["TemplateID"] = value;
        }
    }


    /// <summary>
    /// Owner ID
    /// </summary>
    public int OwnerID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["OwnerID"], 0);
        }
        set
        {
            ViewState["OwnerID"] = value;
        }
    }


    /// <summary>
    /// New item page template code name
    /// </summary>
    public string NewItemPageTemplate
    {
        get
        {
            return ValidationHelper.GetString(ViewState["NewItemPageTemplate"], "");
        }
        set
        {
            ViewState["NewItemPageTemplate"] = value;

            // Get template and set template ID
            PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(value);
            if (pti != null)
            {
                TemplateID = pti.PageTemplateId;
            }
        }
    }


    /// <summary>
    /// Returns true if the changes should be saved
    /// </summary>
    public bool SaveChanges
    {
        get
        {
            return ValidationHelper.GetBoolean(HttpContext.Current.Request.Params["saveChanges"], false);
        }
    }


    /// <summary>
    /// List of allowed child classes separated by semicolon
    /// </summary>
    public string AllowedChildClasses
    {
        get
        {
            return ValidationHelper.GetString(ViewState["AllowedChildClasses"], "");
        }
        set
        {
            ViewState["AllowedChildClasses"] = value;
        }
    }


    /// <summary>
    /// Document ID to use for default data of new culture version
    /// </summary>
    public int CopyDefaultDataFromDocumentID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["CopyDefaultDataFromDocumentID"], 0);
        }
        set
        {
            ViewState["CopyDefaultDataFromDocumentID"] = value;
        }
    }


    /// <summary>
    /// If true, form allows deleting the document
    /// </summary>
    public bool AllowDelete
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["AllowDelete"], true);
        }
        set
        {
            ViewState["AllowDelete"] = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether document permissions are checked
    /// </summary>
    public bool CheckPermissions
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["CheckPermissions"], false);
        }
        set
        {
            ViewState["CheckPermissions"] = value;
            menuElem.CheckPermissions = value;
        }
    }


    /// <summary>
    /// Prefix of the action functions
    /// </summary>
    public string FunctionsPrefix
    {
        get
        {
            return ValidationHelper.GetString(ViewState["FunctionsPrefix"], "");
        }
        set
        {
            ViewState["FunctionsPrefix"] = value;
            menuElem.FunctionsPrefix = value;
        }
    }


    /// <summary>
    /// Editing form
    /// </summary>
    public CMSForm CMSForm
    {
        get
        {
            return formElem;
        }
    }

    #endregion


    #region "Methods"

    protected override void CreateChildControls()
    {
        LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.UserContributions);

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        confirmChanges = SettingsKeyProvider.GetBoolValue(SiteName + ".CMSConfirmChanges");

        tree = new TreeProvider(CMSContext.CurrentUser);
        formElem.TreeProvider = tree;

        ReloadData(false);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        gridClass.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridClass_OnExternalDataBound);
    }


    /// <summary>
    /// Reloads control.
    /// </summary>
    /// <param name="forceReload">Forces nested CMSForm reload if true</param>
    public void ReloadData(bool forceReload)
    {
        if (this.StopProcessing)
        {
            formElem.StopProcessing = true;
        }
        else
        {
            titleElem.TitleImage = "";
            titleElem.TitleText = "";

            if (!confirmChanges)
            {
                ltlScript.Text += ScriptHelper.GetScript("confirmChanges = false;");
            }

            //ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SaveChanges", ScriptHelper.GetIncludeScript("~/CMSScripts/savechanges.js"));

            // Get the node
            if (NewCulture || NewDocument)
            {
                node = DocumentHelper.GetDocument(NodeID, null, tree);
                if (NewCulture)
                {
                    DocumentHelper.ClearWorkflowInformation(node);
                }
            }
            else
            {
                node = DocumentHelper.GetDocument(NodeID, CultureCode, tree);
            }

            pnlSelectClass.Visible = false;
            pnlEdit.Visible = false;
            pnlInfo.Visible = false;
            pnlNewCulture.Visible = false;
            pnlDelete.Visible = false;

            // If node found, init the form
            if (node != null)
            {
                if (Delete)
                {
                    // Delete document
                    pnlDelete.Visible = true;

                    titleElem.TitleText = ResHelper.GetString("Content.DeleteTitle");
                    titleElem.TitleImage = GetImageUrl("CMSModules/CMS_Content/Menu/delete.png");

                    chkAllCultures.Text = ResHelper.GetString("ContentDelete.AllCultures");
                    chkDestroy.Text = ResHelper.GetString("ContentDelete.Destroy");

                    lblQuestion.Text = ResHelper.GetString("ContentDelete.Question");
                    btnYes.Text = ResHelper.GetString("general.yes");
                    btnNo.Text = ResHelper.GetString("general.no");

                    DataSet culturesDS = CultureInfoProvider.GetSiteCultures(SiteName);
                    if ((DataHelper.DataSourceIsEmpty(culturesDS)) || (culturesDS.Tables[0].Rows.Count <= 1))
                    {
                        chkAllCultures.Visible = false;
                        chkAllCultures.Checked = true;
                    }

                    if (node.IsLink)
                    {
                        titleElem.TitleText = ResHelper.GetString("Content.DeleteTitleLink") + " \"" + HTMLHelper.HTMLEncode(node.NodeName) + "\"";
                        lblQuestion.Text = ResHelper.GetString("ContentDelete.QuestionLink");
                        chkAllCultures.Checked = true;
                        plcCheck.Visible = false;
                    }
                    else
                    {
                        titleElem.TitleText = ResHelper.GetString("Content.DeleteTitle") + " \"" + HTMLHelper.HTMLEncode(node.NodeName) + "\"";
                    }
                }
                else
                {
                    if (NewDocument)
                    {
                        titleElem.TitleImage = GetImageUrl("CMSModules/CMS_Content/Menu/new.png");
                        titleElem.TitleText = ResHelper.GetString("Content.NewTitle");
                    }

                    if (NewDocument && (ClassID <= 0))
                    {
                        // Select document type
                        pnlSelectClass.Visible = true;

                        // Get the allowed child classes
                        DataSet ds = DataClassInfoProvider.GetAllowedChildClasses(ValidationHelper.GetInteger(node.GetValue("NodeClassID"), 0), ValidationHelper.GetInteger(SiteInfoProvider.GetSiteInfo(SiteName).SiteID, 0), "ClassName, ClassDisplayName, ClassID", -1);

                        ArrayList deleteRows = new ArrayList();

                        if (!DataHelper.DataSourceIsEmpty(ds))
                        {
                            // Get the unwanted classes
                            string allowed = AllowedChildClasses.Trim().ToLower();
                            if (allowed != "")
                            {
                                allowed = ";" + allowed + ";";
                            }

                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                string className = ValidationHelper.GetString(DataHelper.GetDataRowValue(dr, "ClassName"), "").ToLower();
                                // Document type is not allowed or user hasn't got permission, remove it from the data set
                                if (((allowed != "") && (!allowed.Contains(";" + className + ";"))) ||
                                    (CheckPermissions && (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Content", "Create") && !CMSContext.CurrentUser.IsAuthorizedPerClassName(className, "Create"))))
                                {
                                    deleteRows.Add(dr);
                                }
                            }

                            // Remove the rows
                            foreach (DataRow dr in deleteRows)
                            {
                                ds.Tables[0].Rows.Remove(dr);
                            }
                        }

                        // Check if some classes are available
                        if (!DataHelper.DataSourceIsEmpty(ds))
                        {
                            // If number of classes is more than 1 display them in grid
                            if (ds.Tables[0].Rows.Count > 1)
                            {
                                ds.Tables[0].DefaultView.Sort = "ClassDisplayName";
                                lblError.Visible = false;
                                lblInfo.Visible = true;
                            }
                            // else show form of the only class
                            else
                            {
                                ClassID = ValidationHelper.GetInteger(DataHelper.GetDataRowValue(ds.Tables[0].Rows[0], "ClassID"), 0);
                                ReloadData(true);
                                return;
                            }
                        }
                        else
                        {
                            lblError.Visible = true;
                            lblError.Text = ResHelper.GetString("Content.NoAllowedChildDocuments");
                            lblInfo.Visible = false;
                        }

                        DataSet sortedResult = new DataSet();
                        sortedResult.Tables.Add(ds.Tables[0].DefaultView.ToTable());
                        gridClass.DataSource = sortedResult;
                        gridClass.ReloadData();

                        lblInfo.Text = ResHelper.GetString("Content.NewInfo");
                    }
                    else
                    {
                        // Display the form
                        pnlEdit.Visible = true;

                        // Try to get GroupID if group context exists
                        int currentGroupId = ModuleCommands.CommunityGetCurrentGroupID();

                        btnApprove.Attributes.Add("style", "display: none;");
                        btnCheckIn.Attributes.Add("style", "display: none;");
                        btnCheckOut.Attributes.Add("style", "display: none;");
                        btnReject.Attributes.Add("style", "display: none;");
                        btnSave.Attributes.Add("style", "display: none;");
                        btnUndoCheckOut.Attributes.Add("style", "display: none;");
                        btnDelete.Attributes.Add("style", "display: none;");

                        // CMSForm initialization
                        formElem.NodeId = node.NodeID;
                        formElem.SiteName = SiteName;
                        formElem.CultureCode = CultureCode;
                        formElem.ValidationErrorMessage = ValidationErrorMessage;
                        formElem.IsLiveSite = IsLiveSite;
                        // Set group ID if group context exists
                        formElem.GroupID = currentGroupId;

                        // Set the form mode
                        if (NewDocument)
                        {
                            ci = DataClassInfoProvider.GetDataClass(ClassID);
                            if (ci == null)
                            {
                                throw new Exception("[CMSAdminControls/EditForm.aspx]: Class ID '" + ClassID + "' not found.");
                            }

                            titleElem.TitleText = ResHelper.GetString("Content.NewTitle") + ": " + ci.ClassDisplayName;

                            // Set default template ID
                            formElem.DefaultPageTemplateID = TemplateID > 0 ? TemplateID : ci.ClassDefaultPageTemplateID;

                            // Set document owner
                            formElem.OwnerID = OwnerID;
                            formElem.FormMode = FormModeEnum.Insert;
                            string newClassName = ci.ClassName;
                            string newFormName = newClassName + ".default";
                            if (!String.IsNullOrEmpty(AlternativeFormName))
                            {
                                // Set the alternative form full name
                                formElem.AlternativeFormFullName = GetAltFormFullName(ci.ClassName);
                            }
                            if (newFormName.ToLower() != formElem.FormName.ToLower())
                            {
                                formElem.FormName = newFormName;
                            }
                        }
                        else if (NewCulture)
                        {
                            formElem.FormMode = FormModeEnum.InsertNewCultureVersion;
                            // Default data document ID
                            formElem.CopyDefaultDataFromDocumentId = CopyDefaultDataFromDocumentID;

                            ci = DataClassInfoProvider.GetDataClass(node.NodeClassName);
                            formElem.FormName = node.NodeClassName + ".default";
                            if (!String.IsNullOrEmpty(AlternativeFormName))
                            {
                                // Set the alternative form full name
                                formElem.AlternativeFormFullName = GetAltFormFullName(ci.ClassName);
                            }
                        }
                        else
                        {
                            formElem.FormMode = FormModeEnum.Update;
                            ci = DataClassInfoProvider.GetDataClass(node.NodeClassName);
                            formElem.FormName = "";
                            if (!String.IsNullOrEmpty(AlternativeFormName))
                            {
                                // Set the alternative form full name
                                formElem.AlternativeFormFullName = GetAltFormFullName(ci.ClassName);
                            }

                            formElem.LoadForm();
                        }

                        formElem.Visible = true;

                        ReloadForm();
                    }
                }
            }
            else
            {
                // Try to get any culture of the document
                node = DocumentHelper.GetDocument(NodeID, TreeProvider.ALL_CULTURES, tree);
                if (node != null)
                {
                    // Offer a new culture creation
                    pnlNewCulture.Visible = true;

                    titleElem.TitleText = ResHelper.GetString("Content.NewCultureVersionTitle") + " (" + HTMLHelper.HTMLEncode(CMSContext.CurrentUser.PreferredCultureCode) + ")";
                    titleElem.TitleImage = GetImageUrl("CMSModules/CMS_Content/Menu/new.png");

                    lblNewCultureInfo.Text = ResHelper.GetString("ContentNewCultureVersion.Info");
                    radCopy.Text = ResHelper.GetString("ContentNewCultureVersion.Copy");
                    radEmpty.Text = ResHelper.GetString("ContentNewCultureVersion.Empty");

                    radCopy.Attributes.Add("onclick", "ShowSelection();");
                    radEmpty.Attributes.Add("onclick", "ShowSelection()");

                    ltlScript.Text += ScriptHelper.GetScript(
                        "function ShowSelection() { \n" +
                        "   if (document.getElementById('" + radCopy.ClientID + "').checked) { document.getElementById('divCultures').style.display = 'block'; } \n" +
                        "   else { document.getElementById('divCultures').style.display = 'none'; } \n" +
                        "} \n"
                    );

                    btnOk.Text = ResHelper.GetString("ContentNewCultureVersion.Create");

                    // Load culture versions
                    SiteInfo si = SiteInfoProvider.GetSiteInfo(node.NodeSiteID);
                    if (si != null)
                    {
                        lstCultures.Items.Clear();

                        DataSet nodes = tree.SelectNodes(si.SiteName, node.NodeAliasPath, TreeProvider.ALL_CULTURES, false, null, null, null, 1, false);
                        foreach (DataRow nodeCulture in nodes.Tables[0].Rows)
                        {
                            ListItem li = new ListItem();
                            li.Text = CultureInfoProvider.GetCultureInfo(nodeCulture["DocumentCulture"].ToString()).CultureName;
                            li.Value = nodeCulture["DocumentID"].ToString();
                            lstCultures.Items.Add(li);
                        }
                        if (lstCultures.Items.Count > 0)
                        {
                            lstCultures.SelectedIndex = 0;
                        }
                    }
                }
                else
                {
                    pnlInfo.Visible = true;
                    lblFormInfo.Text = ResHelper.GetString("EditForm.DocumentNotFound");
                    formElem.StopProcessing = true;
                }
            }
        }
    }


    /// <summary>
    /// Unigrid external databound.
    /// </summary>
    object gridClass_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            // Display link to class type
            case "classdisplayname":
                {
                    DataRowView row = (DataRowView)parameter;

                    LinkButton btn = new LinkButton();
                    btn.CssClass = "UserContributionNewClass";
                    btn.CommandArgument = ValidationHelper.GetString(row["ClassID"], "0");
                    btn.Command += new CommandEventHandler(btnClass_Command);

                    Image img = new Image();
                    img.ImageUrl = GetDocumentTypeIconUrl(Convert.ToString(row["ClassName"]));

                    Label lbl = new Label();
                    lbl.Text = Convert.ToString(row["ClassName"]);

                    btn.Controls.Add(img);
                    btn.Controls.Add(lbl);
                    return btn;
                }
        }

        return null;
    }


    private void ReloadForm()
    {
        lblWorkflowInfo.Text = "";

        formElem.Enabled = true;

        if ((node != null) && !NewDocument && !NewCulture)
        {
            bool authorized = true;

            // Check the permissions
            if (CheckPermissions)
            {
                // Check read permissions
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                {
                    RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
                }
                // Check modify permissions
                else if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                {
                    formElem.Enabled = false;
                    lblWorkflowInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                    authorized = false;
                }
            }

            // If authorized
            if (authorized)
            {
                // Setup the workflow information
                WorkflowManager wm = new WorkflowManager(tree);
                WorkflowInfo wi = wm.GetNodeWorkflow(node);
                if ((wi != null) && (!NewCulture))
                {
                    // Get current step info, do not update document
                    WorkflowStepInfo si = wm.GetStepInfo(node, false) ??
                                          wm.GetFirstWorkflowStep(node);

                    bool allowApproval = true;
                    bool canApprove = wm.CanUserApprove(node, CMSContext.CurrentUser);
                    string stepName = si.StepName.ToLower();
                    if (!(canApprove || (stepName == "edit") || (stepName == "published") || (stepName == "archived")))
                    {
                        formElem.Enabled = false;
                    }

                    // Check-in, Check-out
                    if (VersionManager.UseCheckInCheckOut(SiteName))
                    {
                        if (node.GetValue("DocumentCheckedOutByUserID") == null)
                        {
                            // If not checked out, add the check-out information
                            if (canApprove || (stepName == "edit") || (stepName == "published") || (stepName == "archived"))
                            {
                                lblWorkflowInfo.Text = ResHelper.GetString("EditContent.DocumentCheckedIn");
                            }
                            formElem.Enabled = NewCulture;
                        }
                        else
                        {
                            // If checked out by current user, add the check-in button
                            int checkedOutBy = ValidationHelper.GetInteger(node.GetValue("DocumentCheckedOutByUserID"), 0);
                            if (checkedOutBy == CMSContext.CurrentUser.UserID)
                            {
                                // Document is checked out
                                lblWorkflowInfo.Text = ResHelper.GetString("EditContent.DocumentCheckedOut");
                            }
                            else
                            {
                                // Checked out by somebody else
                                lblWorkflowInfo.Text = ResHelper.GetString("EditContent.DocumentCheckedOutByAnother");
                                formElem.Enabled = NewCulture;
                            }
                            allowApproval = false;
                        }
                    }

                    // Document approval
                    if (allowApproval)
                    {
                        switch (si.StepName.ToLower())
                        {
                            case "edit":
                            case "published":
                            case "archived":
                                break;

                            default:
                                // If the user is authorized to perform the step, display the approve and reject buttons
                                if (!canApprove)
                                {
                                    lblWorkflowInfo.Text += " " + ResHelper.GetString("EditContent.NotAuthorizedToApprove");
                                }
                                break;
                        }
                        lblWorkflowInfo.Text += " " + String.Format(ResHelper.GetString("EditContent.CurrentStepInfo"), si.StepDisplayName);
                    }
                }
            }
        }

        pnlWorkflowInfo.Visible = (lblWorkflowInfo.Text != "");

        menuElem.ShowDelete = AllowDelete && (Action.ToLower() == "edit");
        menuElem.NodeID = NodeID;
        menuElem.Action = Action.ToLower();

        CheckPermissions = CheckPermissions;

        menuElem.ReloadMenu();
    }


    /// <summary>
    /// Adds the alert message to the output request window
    /// </summary>
    /// <param name="message">Message to display</param>
    private void AddAlert(string message)
    {
        ltlScript.Text += ScriptHelper.GetAlertScript(message);
    }


    /// <summary>
    /// Adds the script to the output request window
    /// </summary>
    /// <param name="script">Script to add</param>
    private void AddScript(string script)
    {
        ltlScript.Text += ScriptHelper.GetScript(script);
    }


    /// <summary>
    /// Save button click event handler.
    /// </summary>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        SaveDocument();
    }


    /// <summary>
    /// Saves new or existing document.
    /// </summary>
    private void SaveDocument()
    {
        if (IsBannedIP())
        {
            return;
        }

        if (NodeID > 0)
        {
            // Validate the form first
            if (formElem.BasicForm.ValidateData())
            {
                bool createAnother = NewDocument && ValidationHelper.GetBoolean(Request.Params["hidAnother"], false);
                bool autoCheck = false;
                bool useWorkflow = false;
                VersionManager vm = null;
                if (!NewDocument && !NewCulture)
                {
                    autoCheck = !VersionManager.UseCheckInCheckOut(SiteName);
                    WorkflowManager workflowMan = new WorkflowManager(tree);

                    // Get the document
                    node = DocumentHelper.GetDocument(NodeID, CultureCode, tree);

                    //test if the document uses workflow
                    if (workflowMan.GetNodeWorkflow(node) != null)
                    {
                        useWorkflow = true;
                    }
                }

                try
                {
                    // If not using check-in/check-out, check out automatically
                    if (autoCheck & useWorkflow)
                    {
                        if (node != null)
                        {
                            // Check out
                            vm = new VersionManager(tree);
                            vm.CheckOut(node);
                        }
                    }

                    // Tree node is returned from CMSForm.Save method
                    if ((node != null) && !NewDocument && !NewCulture)
                    {
                        formElem.Save(node);
                    }
                    else
                    {
                        node = formElem.Save();

                        // Clear cache if current document is blogpost or blog
                        if (ci != null)
                        {
                            if ((ci.ClassName.ToLower() == "cms.blogpost") || (ci.ClassName.ToLower() == "cms.blog"))
                            {
                                // Clear cache
                                if (CMSControlsHelper.CurrentPageManager != null)
                                {
                                    CMSControlsHelper.CurrentPageManager.ClearCache();
                                }
                            }
                        }

                        // Set the edit mode
                        if (node != null)
                        {
                            NodeID = node.NodeID;
                            Action = "edit";
                            ReloadData(true);
                            CMSForm.lblInfo.Text = ResHelper.GetString("general.changessaved");
                        }
                    }

                    if (node != null)
                    {
                        // Check in the document
                        if (autoCheck & useWorkflow)
                        {
                            if (vm != null)
                            {
                                vm.CheckIn(node, null, null);
                            }
                        }
                    }

                    ReloadForm();
                }
                catch (Exception ex)
                {
                    AddAlert(ResHelper.GetString("ContentRequest.SaveFailed") + " : " + ex.Message);
                }
            }
        }
    }


    /// <summary>
    /// Approve button click event handler.
    /// </summary>
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        if (IsBannedIP())
        {
            return;
        }

        if (node != null)
        {
            // Validate the form first
            if (formElem.BasicForm.ValidateData())
            {
                // Save the changes first
                bool autoCheck = !VersionManager.UseCheckInCheckOut(SiteName);
                if (autoCheck)
                {
                    if (SaveChanges)
                    {
                        SaveDocument();
                    }
                    else
                    {
                        formElem.BasicForm.LoadControlValues();
                    }
                }

                // Approve the document - Go to next workflow step
                WorkflowManager wm = new WorkflowManager(tree);

                // Get original step
                WorkflowStepInfo originalStep = wm.GetStepInfo(node);

                // Approve the document
                WorkflowStepInfo nextStep = wm.MoveToNextStep(node, "");
                if (nextStep != null)
                {
                    string nextStepName = nextStep.StepName.ToLower();
                    string originalStepName = originalStep.StepName.ToLower();

                    // Send workflow e-mails
                    if (SettingsKeyProvider.GetBoolValue(SiteName + ".CMSSendWorkflowEmails"))
                    {
                        if (nextStepName == "published")
                        {
                            // Publish e-mails
                            wm.SendWorkflowEmails(node, CMSContext.CurrentUser, originalStep, nextStep, WorkflowActionEnum.Published, "");
                        }
                        else
                        {
                            // Approve e-mails
                            wm.SendWorkflowEmails(node, CMSContext.CurrentUser, originalStep, nextStep, WorkflowActionEnum.Approved, "");
                        }
                    }
                    ltlScript.Text += ScriptHelper.GetScript("PassiveRefresh(" + NodeID + ", " + node.NodeParentID + ");");

                    // Ensure correct message is displayed
                    if (nextStepName == "published")
                    {
                        formElem.lblInfo.Text += " " + ResHelper.GetString("workflowstep.customtopublished");
                    }
                    else if (originalStepName == "edit" && nextStepName != "published")
                    {
                        formElem.lblInfo.Text += " " + ResHelper.GetString("workflowstep.edittocustom");
                    }
                    else if ((originalStepName != "edit") && (nextStepName != "published") && (nextStepName != "archived"))
                    {
                        formElem.lblInfo.Text += " " + ResHelper.GetString("workflowstep.customtocustom");
                    }
                    else
                    {
                        formElem.lblInfo.Text += " " + ResHelper.GetString("ContentEdit.WasApproved");
                    }
                }
                else
                {
                    // Workflow has been removed
                    AddScript("     SelectNode(" + NodeID + ");");
                    formElem.lblInfo.Text += " " + ResHelper.GetString("ContentEdit.WasApproved");
                }
            }

            ReloadForm();

            RaiseOnAfterApprove();
        }
    }


    /// <summary>
    /// Reject button click event handler.
    /// </summary>
    protected void btnReject_Click(object sender, EventArgs e)
    {
        if (IsBannedIP())
        {
            return;
        }

        // Reject the document - Go to previous workflow step
        if (node != null)
        {
            // Validate the form first
            if (formElem.BasicForm.ValidateData())
            {
                // Save the document first
                bool autoCheck = !VersionManager.UseCheckInCheckOut(SiteName);
                if (autoCheck)
                {
                    if (SaveChanges)
                    {
                        SaveDocument();
                    }
                    else
                    {
                        formElem.BasicForm.LoadControlValues();
                    }
                }

                WorkflowManager wm = new WorkflowManager(tree);

                // Get original step
                WorkflowStepInfo originalStep = wm.GetStepInfo(node);

                // Reject the document
                WorkflowStepInfo previousStep = wm.MoveToPreviousStep(node, "");

                // Send workflow e-mails
                if (SettingsKeyProvider.GetBoolValue(SiteName + ".CMSSendWorkflowEmails"))
                {
                    wm.SendWorkflowEmails(node, CMSContext.CurrentUser, originalStep, previousStep, WorkflowActionEnum.Rejected, "");
                }

                formElem.lblInfo.Text += " " + ResHelper.GetString("ContentEdit.WasRejected");
            }

            ReloadForm();

            RaiseOnAfterReject();
        }
    }


    /// <summary>
    /// CheckIn button click event handler.
    /// </summary>
    protected void btnCheckIn_Click(object sender, EventArgs e)
    {
        if (IsBannedIP())
        {
            return;
        }

        if (node != null)
        {
            // Validate the form first
            if (formElem.BasicForm.ValidateData())
            {
                // Save the document first
                if (SaveChanges || !confirmChanges)
                {
                    SaveDocument();
                }
                else
                {
                    formElem.BasicForm.LoadControlValues();
                }

                // Check in the document
                VersionManager verMan = new VersionManager(tree);

                // Check in the document        
                verMan.CheckIn(node, null, null);

                formElem.lblInfo.Text += " " + ResHelper.GetString("ContentEdit.WasCheckedIn");
            }

            ReloadForm();
        }
    }


    /// <summary>
    /// CheckOut button click event handler.
    /// </summary>
    protected void btnCheckOut_Click(object sender, EventArgs e)
    {
        if (IsBannedIP())
        {
            return;
        }

        // Check out the document
        if (node != null)
        {
            TreeProvider treeProvider = new TreeProvider(CMSContext.CurrentUser);
            VersionManager verMan = new VersionManager(treeProvider);

            verMan.EnsureVersion(node, node.IsPublished);

            // Check out the document
            verMan.CheckOut(node);
        }

        ReloadForm();
    }


    /// <summary>
    /// UndoCheckOut click event handler.
    /// </summary>
    protected void btnUndoCheckOut_Click(object sender, EventArgs e)
    {
        if (IsBannedIP())
        {
            return;
        }

        // Check out the document
        if (node != null)
        {
            TreeProvider treeProvider = new TreeProvider(CMSContext.CurrentUser);
            VersionManager verMan = new VersionManager(treeProvider);

            // Undo check out
            verMan.UndoCheckOut(node);
        }

        ReloadForm();
        formElem.LoadForm();

        // Reload the values if the form
        formElem.BasicForm.LoadControlValues();
    }


    /// <summary>
    /// New class selection click event handler.
    /// </summary>
    protected void btnClass_Command(object sender, CommandEventArgs e)
    {
        int newClassId = ValidationHelper.GetInteger(e.CommandArgument, 0);
        if (newClassId > 0)
        {
            ClassID = newClassId;
            ReloadData(true);
        }
    }


    /// <summary>
    /// OK button click event handler.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (IsBannedIP())
        {
            return;
        }

        Action = "newculture";
        CopyDefaultDataFromDocumentID = radCopy.Checked ? ValidationHelper.GetInteger(lstCultures.SelectedValue, 0) : 0;

        ReloadData(true);
    }


    /// <summary>
    /// Yes button click event handler.
    /// </summary>
    protected void btnYes_Click(object sender, EventArgs e)
    {
        if (IsBannedIP())
        {
            return;
        }

        // Prepare the where condition
        string where = "NodeID = " + NodeID;

        // Get the documents
        TreeProvider treeProvider = new TreeProvider(CMSContext.CurrentUser);
        DataSet ds = null;
        if (chkAllCultures.Checked)
        {
            ds = treeProvider.SelectNodes(SiteName, "/%", TreeProvider.ALL_CULTURES, true, null, where, null, -1, false);
        }
        else
        {
            ds = treeProvider.SelectNodes(SiteName, "/%", CultureCode, false, null, where, null, -1, false);
        }


        if (!DataHelper.DataSourceIsEmpty(ds))
        {
            // Get node alias
            string nodeAlias = ValidationHelper.GetString(DataHelper.GetDataRowValue(ds.Tables[0].Rows[0], "NodeAlias"), "");
            // Get parent alias path
            string parentAliasPath = TreePathUtils.GetParentPath(ValidationHelper.GetString(DataHelper.GetDataRowValue(ds.Tables[0].Rows[0], "NodeAliasPath"), ""));

            // Delete the documents
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string aliasPath = ValidationHelper.GetString(dr["NodeAliasPath"], "");
                string culture = ValidationHelper.GetString(dr["DocumentCulture"], "");
                string className = ValidationHelper.GetString(dr["ClassName"], "");

                // Get the node
                TreeNode node = treeProvider.SelectSingleNode(SiteName, aliasPath, culture, false, className, false);

                if (node != null)
                {
                    // Check delete permissions
                    bool hasUserDeletePermission = !CheckPermissions || IsUserAuthorizedToDeleteDocument(node, chkDestroy.Checked);

                    if (hasUserDeletePermission)
                    {
                        // Delete the document
                        try
                        {
                            int parentId = node.NodeParentID;
                            if (parentId <= 0)
                            {
                                parentId = node.NodeID;
                            }
                            if (!DocumentHelper.DeleteDocument(node, treeProvider, chkAllCultures.Checked, chkDestroy.Checked, true))
                            {
                                parentId = node.NodeID;
                            }

                            Action = "edit";
                            ReloadData(true);
                        }
                        catch (Exception ex)
                        {
                            EventLogProvider log = new EventLogProvider(treeProvider.Connection);
                            log.LogEvent("E", DateTime.Now, "Content", "DELETEDOC", CMSContext.CurrentUser.UserID, CMSContext.CurrentUser.UserName, node.NodeID, node.DocumentName, HTTPHelper.GetUserHostAddress(), EventLogProvider.GetExceptionLogMessage(ex), CMSContext.CurrentSite.SiteID, HTTPHelper.GetAbsoluteUri());
                            AddAlert(ResHelper.GetString("ContentRequest.DeleteFailed") + " : " + ex.Message);
                            return;
                        }
                    }
                    // Access denied - not authorized to delete the document
                    else
                    {
                        AddAlert(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtodeletedocument"), node.NodeAliasPath));
                        return;
                    }
                }
                else
                {
                    AddAlert(ResHelper.GetString("ContentRequest.ErrorMissingSource"));
                    return;
                }
            }

            RaiseOnAfterDelete();

            string rawUrl = URLRewriter.RawUrl;
            if ((nodeAlias != "") && (rawUrl.Substring(rawUrl.LastIndexOf('/')).Contains(nodeAlias)))
            {
                // Redirect to the parent url when current url belongs to deleted document
                UrlHelper.Redirect(CMSContext.GetUrl(parentAliasPath));
            }
            else
            {
                // Redirect to current url
                UrlHelper.Redirect(rawUrl);
            }
        }
        else
        {
            AddAlert(ResHelper.GetString("DeleteDocument.CultureNotExists"));
            return;
        }
    }


    /// <summary>
    /// No button click event handler.
    /// </summary>
    protected void btnNo_Click(object sender, EventArgs e)
    {
        if (IsBannedIP())
        {
            return;
        }

        Action = "edit";
        ReloadData(false);
    }


    /// <summary>
    /// Delete button click event handler.
    /// </summary>
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (IsBannedIP())
        {
            return;
        }

        Action = "delete";
        ReloadData(true);
    }


    protected override void OnPreRender(EventArgs e)
    {
        if (Visible)
        {
            // Render the styles link if live site mode
            if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
            {
                CSSHelper.RegisterDesignMode(Page);
            }

            if (pnlEdit.Visible)
            {
                // Register other scripts which are necessary in edit mode
                ScriptHelper.RegisterProgress(Page);
                ScriptHelper.RegisterSpellChecker(Page);
                ScriptHelper.RegisterShortcuts(Page);
                ScriptHelper.RegisterSaveChanges(Page);

                // Register full postback if an uploader is used
                if (formElem.BasicForm.UploaderInUse)
                {
                    ControlsHelper.RegisterPostbackControl(btnApprove);
                    ControlsHelper.RegisterPostbackControl(btnSave);
                    ControlsHelper.RegisterPostbackControl(btnCheckIn);
                }

                // Register script
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("function PassiveRefresh(nodeId, selectNodeId) {} \n");
                if (btnSave.Enabled && menuElem.AllowSave)
                {
                    sb.AppendLine("function " + FunctionsPrefix + "SaveDocument(NodeID) { if (window.AllowSubmit) { AllowSubmit(); } " + Page.ClientScript.GetPostBackEventReference(btnSave, null) + "; } \n");
                }
                else
                {
                    sb.AppendLine("function " + FunctionsPrefix + "SaveDocument(NodeID) {}");
                }
                sb.AppendLine("function " + FunctionsPrefix + "Approve(NodeID) { if (window.SubmitAction) { SubmitAction(); } " + Page.ClientScript.GetPostBackEventReference(btnApprove, null) + "; } \n");
                sb.AppendLine("function " + FunctionsPrefix + "Reject(NodeID) { if (window.SubmitAction) { SubmitAction(); } " + Page.ClientScript.GetPostBackEventReference(btnReject, null) + "; } \n");
                sb.AppendLine("function " + FunctionsPrefix + "CheckIn(NodeID) { " + Page.ClientScript.GetPostBackEventReference(btnCheckIn, null) + "; } \n");
                sb.AppendLine("var confirmSave='" + ResHelper.GetString("Content.ConfirmSave") + "'; var confirmLeave='" + ResHelper.GetString("Content.ConfirmLeave") + "'; \n");
                sb.AppendLine("function " + FunctionsPrefix + "CheckOut(NodeID) { " + Page.ClientScript.GetPostBackEventReference(btnCheckOut, null) + "; } \n");
                sb.AppendLine("function " + FunctionsPrefix + "UndoCheckOut(NodeID) { if(!confirm(" + ScriptHelper.GetString(ResHelper.GetString("EditMenu.UndoCheckOutConfirmation")) + ")) return false; " + Page.ClientScript.GetPostBackEventReference(btnUndoCheckOut, null) + "; } \n");
                sb.AppendLine("function " + FunctionsPrefix + "Delete(NodeID) { " + Page.ClientScript.GetPostBackEventReference(btnDelete, null) + "; } \n");
                if (!string.IsNullOrEmpty(FunctionsPrefix))
                {
                    sb.AppendLine("function SaveDocument(NodeID) { " + FunctionsPrefix + "SaveDocument(NodeID); } \n");
                    sb.AppendLine("\n function " + FunctionsPrefix + "SpellCheck() { checkSpelling(spellURL); } \n");
                }
                sb.Append("\n var spellURL = '");

                if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
                {
                    sb.Append(CMSContext.ResolveDialogUrl("~/CMSFormControls/LiveSelectors/SpellCheck.aspx"));
                }
                else
                {
                    sb.Append(CMSContext.ResolveDialogUrl("~/CMSModules/Content/CMSDesk/Edit/SpellCheck.aspx"));
                }
                sb.AppendLine("'; \n");

                ltlScript.Text += ScriptHelper.GetScript(sb.ToString());
                // Register the scripts
                ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditForm_" + ClientID, ltlScript.Text);
            }
        }

        base.OnPreRender(e);
    }


    /// <summary>
    /// Checks whether the user is authorized to delete document.
    /// </summary>
    /// <param name="treeNode">Document node</param>
    /// <param name="deleteDocHistory">Delete document history?</param>
    protected bool IsUserAuthorizedToDeleteDocument(TreeNode treeNode, bool deleteDocHistory)
    {
        bool isAuthorized = true;

        CurrentUserInfo currentUser = CMSContext.CurrentUser;

        // Check delete permission
        if (currentUser.IsAuthorizedPerDocument(treeNode, NodePermissionsEnum.Delete) == AuthorizationResultEnum.Allowed)
        {
            if (deleteDocHistory)
            {
                // Check destroy permisson
                if (!(currentUser.IsAuthorizedPerDocument(treeNode, NodePermissionsEnum.Destroy) == AuthorizationResultEnum.Allowed))
                {
                    isAuthorized = false;
                }
            }
        }
        else
        {
            isAuthorized = false;
        }

        return isAuthorized;
    }


    /// <summary>
    /// Check if user IP is banned.
    /// </summary>
    private bool IsBannedIP()
    {
        // Check banned ip
        if (!BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.AllNonComplete))
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("General.BannedIP");
            return true;
        }
        return false;
    }


    /// <summary>
    /// Returns alternative form name in full version - 'ClassName.AltFormCodeName'
    /// </summary>
    /// <param name="className">Class name</param>
    private string GetAltFormFullName(string className)
    {
        if (!string.IsNullOrEmpty(AlternativeFormName) && !string.IsNullOrEmpty(className) && !AlternativeFormName.StartsWith(className))
        {
            if (AlternativeFormName.Contains("."))
            {
                // Remove class name if it is different from class name in parameter
                AlternativeFormName = AlternativeFormName.Remove(0, AlternativeFormName.LastIndexOf(".") + 1);
            }
            return className + "." + AlternativeFormName;
        }
        else
        {
            return AlternativeFormName;
        }
    }


    /// <summary>
    /// Raises the OnAfterApprove event
    /// </summary>
    private void RaiseOnAfterApprove()
    {
        if (OnAfterApprove != null)
        {
            OnAfterApprove(this, null);
        }
    }


    /// <summary>
    /// Raises the OnAfterReject event
    /// </summary>
    private void RaiseOnAfterReject()
    {
        if (OnAfterReject != null)
        {
            OnAfterReject(this, null);
        }
    }


    /// <summary>
    /// Raises the OnAfterDelete event
    /// </summary>
    private void RaiseOnAfterDelete()
    {
        if (OnAfterDelete != null)
        {
            OnAfterDelete(this, null);
        }
    }

    #endregion
}
