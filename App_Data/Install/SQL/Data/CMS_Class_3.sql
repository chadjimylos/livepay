set identity_insert [cms_class] on
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Supplier', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="SupplierID" fieldcaption="SupplierID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="7197f910-b4b5-4c81-be4c-b24f7c38e87c" /><field column="SupplierDisplayName" fieldcaption="SupplierDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fe7d0925-6fda-47bc-9783-d8f27867590a" /><field column="SupplierPhone" fieldcaption="SupplierPhone" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f321f915-0958-4c10-9bc3-b8ab505b1f2a" /><field column="SupplierEmail" fieldcaption="SupplierEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="01565a13-6ee4-482d-b9f6-d6a9d03c2fca" /><field column="SupplierFax" fieldcaption="SupplierFax" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8bc06426-f54b-4cd7-9b59-64fd0d84b1c3" /><field column="SupplierEnabled" fieldcaption="SupplierEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fb64cad7-bec2-4e50-8c90-502de0f21738" /><field column="SupplierGUID" fieldcaption="SupplierGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="27e25a04-f570-44cf-9358-89869d3a33e1" /><field column="SupplierLastModified" fieldcaption="SupplierLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d0e00d08-4f82-4a61-a2fd-b6a796758c0d" /></form>', '20081229 09:07:03', 1141, N'ecommerce.supplier', 0, 0, 0, 'd30e1123-ea76-42db-8f74-a20da0c54d03', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_Supplier', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_Supplier">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SupplierID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SupplierDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SupplierPhone">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SupplierEmail">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SupplierFax">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SupplierEnabled" type="xs:boolean" />
              <xs:element name="SupplierGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="SupplierLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_Supplier" />
      <xs:field xpath="SupplierID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Manufacturer', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="ManufacturerID" fieldcaption="ManufacturerID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="adc81423-9119-402b-91a2-8b9f74d4c99d" /><field column="ManufacturerDisplayName" fieldcaption="ManufacturerDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6684af1f-31d5-46c4-8aec-e9f1cc47320b" /><field column="ManufactureHomepage" fieldcaption="ManufactureHomepage" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4082aedd-2dfa-4745-b1c1-78d8b4ede42a" /><field column="ManufacturerEnabled" fieldcaption="ManufacturerEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c7b38aac-2f3a-4d2b-bbcd-4a1d38dad212" /><field column="ManufacturerGUID" fieldcaption="ManufacturerGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7da75487-9b2c-4270-ad83-80bc129f8d0d" /><field column="ManufacturerLastModified" fieldcaption="ManufacturerLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f0c58eb3-4eff-4382-a919-a8bbc66086bf" /></form>', '20081229 09:07:01', 1142, N'ecommerce.manufacturer', 0, 0, 0, 'dcbcbc29-fa72-404c-bcaf-7eea0dac144e', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_Manufacturer', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_Manufacturer">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ManufacturerID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ManufacturerDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ManufactureHomepage" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ManufacturerEnabled" type="xs:boolean" />
              <xs:element name="ManufacturerGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="ManufacturerLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_Manufacturer" />
      <xs:field xpath="ManufacturerID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Currency', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="CurrencyID" fieldcaption="CurrencyID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e5c6c212-da7d-441e-a7ea-0a4ad9878485" /><field column="CurrencyName" fieldcaption="CurrencyName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="90342e29-3669-4e5e-a523-cf604230d80d" /><field column="CurrencyDisplayName" fieldcaption="CurrencyDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="194beb72-436a-452c-88b5-ed2dcaa4794d" /><field column="CurrencyCode" fieldcaption="CurrencyCode" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="088b32fc-7f67-4730-9419-b81aa71c1e48" /><field column="CurrencyRoundTo" fieldcaption="CurrencyRoundTo" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3ed60614-90b5-43ba-be0d-e774e8868f41" /><field column="CurrencyEnabled" fieldcaption="CurrencyEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="305137cc-6b7a-48b9-9c61-1c56cd21ff0b" /><field column="CurrencyFormatString" fieldcaption="CurrencyFormatString" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="32757520-9db9-4153-937d-bc8e3b0ee5b8" /><field column="CurrencyIsMain" fieldcaption="CurrencyIsMain" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="727b2700-d8e8-47e1-be66-b6a9977db89c" /><field column="CurrencyGUID" fieldcaption="CurrencyGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="edfd61fa-607c-4bb0-9166-2a7a5c2c2dd3" /><field column="CurrencyLastModified" fieldcaption="CurrencyLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="13efe629-afaf-4943-b3b7-0a3e0d0ec1d9" /></form>', '20081229 09:07:00', 1143, N'ecommerce.currency', 0, 0, 0, '456a1f44-3c71-446f-8a24-509b74037abd', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_Currency', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_Currency">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CurrencyID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="CurrencyName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CurrencyDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CurrencyCode">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CurrencyRoundTo" type="xs:int" minOccurs="0" />
              <xs:element name="CurrencyEnabled" type="xs:boolean" />
              <xs:element name="CurrencyFormatString">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CurrencyIsMain" type="xs:boolean" />
              <xs:element name="CurrencyGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="CurrencyLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_Currency" />
      <xs:field xpath="CurrencyID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Payment option', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="PaymentOptionID" fieldcaption="PaymentOptionID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="0c8db08f-1219-47e6-9ec0-2c7347e6dfb4" /><field column="PaymentOptionName" fieldcaption="PaymentOptionName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b941eea2-edcd-44af-8370-63df85298d7a" /><field column="PaymentOptionDisplayName" fieldcaption="PaymentOptionDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c81b117e-07e6-4665-8852-e07ee74e30ee" /><field column="PaymentOptionEnabled" fieldcaption="PaymentOptionEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6dcaf75a-f970-4cc9-8119-ba5d68b6a41f" /><field column="PaymentOptionSiteID" fieldcaption="PaymentOptionSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="012bd445-e8a8-4c27-8d04-9e3867e7c3a7" /><field column="PaymentOptionPaymentGateUrl" fieldcaption="PaymentOptionPaymentGateUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e9508d42-44c5-4241-a83f-e74ec498eeb9" /><field column="PaymentOptionAssemblyName" fieldcaption="PaymentOptionAssemblyName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6c32782d-b052-4ff6-a7ba-0f8936788f8b" /><field column="PaymentOptionClassName" fieldcaption="PaymentOptionClassName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="abff12f4-f681-4245-88c3-61b27d1a9794" /><field column="PaymentOptionSucceededOrderStatusID" fieldcaption="PaymentOptionSucceededOrderStatusID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f183ff2d-e653-4d8f-b167-4ea7468efb7c" /><field column="PaymentOptionFailedOrderStatusID" fieldcaption="PaymentOptionFailedOrderStatusID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2b6c2060-7184-410e-9c14-60636824d725" /><field column="PaymentOptionGUID" fieldcaption="PaymentOptionGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2683e510-dc55-4bf1-8367-845ec45dbb05" /><field column="PaymentOptionLastModified" fieldcaption="PaymentOptionLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4e2fdbb1-b20c-4165-84b3-d1b4e55c90c0" /></form>', '20081229 09:07:02', 1144, N'ecommerce.paymentoption', 0, 0, 0, 'f4d1e038-f33e-4ff6-99e8-a075a94a964b', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_PaymentOption', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_PaymentOption">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="PaymentOptionID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="PaymentOptionName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PaymentOptionDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PaymentOptionEnabled" type="xs:boolean" />
              <xs:element name="PaymentOptionSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="PaymentOptionPaymentGateUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="500" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PaymentOptionAssemblyName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PaymentOptionClassName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PaymentOptionSucceededOrderStatusID" type="xs:int" minOccurs="0" />
              <xs:element name="PaymentOptionFailedOrderStatusID" type="xs:int" minOccurs="0" />
              <xs:element name="PaymentOptionGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="PaymentOptionLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_PaymentOption" />
      <xs:field xpath="PaymentOptionID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Public status', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="PublicStatusID" fieldcaption="PublicStatusID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="18356b40-9b5e-448b-8e05-944cacae39ca" /><field column="PublicStatusName" fieldcaption="PublicStatusName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ad426156-5ac2-4260-99bb-4cba01b0eaee" /><field column="PublicStatusDisplayName" fieldcaption="PublicStatusDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b12464a8-94d8-437d-95a5-76e1a3b7c22f" /><field column="PublicStatusEnabled" fieldcaption="PublicStatusEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43ca7794-8f4d-4384-8b99-6e168baec910" /><field column="PublicStatusGUID" fieldcaption="PublicStatusGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4ac4a4f9-5e50-4de6-a042-4d2aa3afdb53" /><field column="PublicStatusLastModified" fieldcaption="PublicStatusLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="94bf7a36-2b43-4b77-97b7-37448a4a80be" /></form>', '20081229 09:07:02', 1145, N'ecommerce.publicstatus', 0, 0, 0, 'ae8706a2-9a38-47be-ad1b-251862821fad', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_PublicStatus', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_PublicStatus">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="PublicStatusID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="PublicStatusName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PublicStatusDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PublicStatusEnabled" type="xs:boolean" />
              <xs:element name="PublicStatusGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="PublicStatusLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_PublicStatus" />
      <xs:field xpath="PublicStatusID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Order Status', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="StatusID" fieldcaption="StatusID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="0054dfd5-d032-4146-b5a1-b611202c552c" /><field column="StatusName" fieldcaption="StatusName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="44fa0a6c-5818-45ae-b066-e2526e5754e7" /><field column="StatusDisplayName" fieldcaption="StatusDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fef2499a-8e05-4f98-aedc-4b6103d46383" /><field column="StatusOrder" fieldcaption="StatusOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ecb76c5e-b8a5-488a-a57b-ee00951c578e" /><field column="StatusEnabled" fieldcaption="StatusEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3eddb801-ea97-430d-a5f8-97a5e06cef87" /><field column="StatusColor" fieldcaption="StatusColor" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a23641f6-08d0-40df-aa21-4dad99eff0ff" /><field column="StatusGUID" fieldcaption="StatusGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="03ecad4a-071c-4810-85d9-a42e792c12b0" /><field column="StatusLastModified" fieldcaption="StatusLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6c01407a-db06-418a-baf0-16a1c398ef57" /><field column="StatusSendNotification" fieldcaption="StatusSendNotification" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9bc726eb-df8d-409e-82ac-7038abc35d6b" /></form>', '20081229 09:07:02', 1146, N'ecommerce.orderstatus', 0, 0, 0, 'da3a2138-096a-4554-9a64-afd4ef3e0b30', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_OrderStatus', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_OrderStatus">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="StatusID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="StatusName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StatusDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StatusOrder" type="xs:int" minOccurs="0" />
              <xs:element name="StatusEnabled" type="xs:boolean" />
              <xs:element name="StatusColor" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="7" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StatusGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="StatusLastModified" type="xs:dateTime" minOccurs="0" />
              <xs:element name="StatusSendNotification" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_OrderStatus" />
      <xs:field xpath="StatusID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Internal status', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="InternalStatusID" fieldcaption="InternalStatusID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="4d9bc5f0-be40-458a-b037-cb2f25c812b8" /><field column="InternalStatusName" fieldcaption="InternalStatusName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d9e2dd64-1886-40af-bbea-f0d722372dde" /><field column="InternalStatusDisplayName" fieldcaption="InternalStatusDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6c1a9a41-562d-4d26-a189-7356ef7608d0" /><field column="InternalStatusEnabled" fieldcaption="InternalStatusEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f45be238-8cf2-4ddc-8bea-9be2914679a7" /><field column="InternalStatusGUID" fieldcaption="InternalStatusGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d73f5d80-96da-4fa4-a642-220c585a6e15" /><field column="InternalStatusLastModified" fieldcaption="InternalStatusLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ad337396-1b43-44d4-ac68-cb6114e158d6" /></form>', '20081229 09:07:00', 1147, N'ecommerce.internalstatus', 0, 0, 0, '65ac5e46-2fbe-4c21-b123-687d3b54ac67', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_InternalStatus', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_InternalStatus">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="InternalStatusID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="InternalStatusName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="InternalStatusDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="InternalStatusEnabled" type="xs:boolean" />
              <xs:element name="InternalStatusGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="InternalStatusLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_InternalStatus" />
      <xs:field xpath="InternalStatusID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Department', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="DepartmentID" fieldcaption="DepartmentID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="3d7b9c80-4c03-4312-a523-d86e3c03102d" /><field column="DepartmentName" fieldcaption="DepartmentName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b5e1a6b9-cfcd-48ef-bcf8-0a7fa4a5e1b8" /><field column="DepartmentDisplayName" fieldcaption="DepartmentDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1a96bf6c-baaa-49c4-8a1f-8d34f80cbbf6" /><field column="DepartmentDefaultTaxClassID" fieldcaption="DepartmentDefaultTaxClassID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="addb3f11-2bfa-4747-8e86-3aaa8905658e" /><field column="DepartmentGUID" fieldcaption="DepartmentGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b7726ed9-96c7-4b3d-b36e-71200f682a89" /><field column="DepartmentLastModified" fieldcaption="DepartmentLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="029ca9e8-e182-43bd-b944-d3bd4f8d5bc9" /></form>', '20091103 09:02:25', 1148, N'ecommerce.department', 0, 0, 0, '1c05d8cc-e5b6-4477-b04c-2bcdf7f2ac84', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_Department', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_Department">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="DepartmentID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="DepartmentName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DepartmentDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DepartmentDefaultTaxClassID" type="xs:int" minOccurs="0" />
              <xs:element name="DepartmentGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="DepartmentLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_Department" />
      <xs:field xpath="DepartmentID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Discount coupon', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="DiscountCouponID" fieldcaption="DiscountCouponID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="f325b7a0-e6e0-427e-ba5a-6e7e1197a7ba" /><field column="DiscountCouponDisplayName" fieldcaption="DiscountCouponDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="30072228-a0df-40dc-8e75-2df26d8b4fc1" /><field column="DiscountCouponIsExcluded" fieldcaption="DiscountCouponIsExcluded" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d2d7e4b6-2225-4bac-9b51-b681ecc71f6b" /><field column="DiscountCouponValidFrom" fieldcaption="DiscountCouponValidFrom" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="869851d3-7ba1-4373-97bf-edd987dcb27a" /><field column="DiscountCouponValidTo" fieldcaption="DiscountCouponValidTo" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2d107d85-ff21-46f7-95f5-600c5f07924b" /><field column="DiscountCouponValue" fieldcaption="DiscountCouponValue" visible="true" columntype="double" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="76a9cd38-0d59-43b7-bdcc-5086f8856f7e" /><field column="DiscountCouponIsFlatValue" fieldcaption="DiscountCouponIsFlatValue" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2dfbc1f5-331a-4c71-b85b-c89ab250c55b" /><field column="DiscountCouponCode" fieldcaption="DiscountCouponCode" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="52d454b1-53da-425c-b24b-93e114ffc1d9" /><field column="DiscountCouponGUID" fieldcaption="DiscountCouponGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2e7733de-f0a1-4b82-ad47-c7f93ca0bc0a" /><field column="DiscountCouponLastModified" fieldcaption="DiscountCouponLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b737ffd1-10c2-4059-a59f-31eae0aac802" /></form>', '20081229 09:07:00', 1149, N'ecommerce.discountcoupon', 0, 0, 0, 'ae9dba35-b6b5-45f4-bb1c-5125d15b3bb4', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_DiscountCoupon', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_DiscountCoupon">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="DiscountCouponID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="DiscountCouponDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DiscountCouponIsExcluded" type="xs:boolean" />
              <xs:element name="DiscountCouponValidFrom" type="xs:dateTime" minOccurs="0" />
              <xs:element name="DiscountCouponValidTo" type="xs:dateTime" minOccurs="0" />
              <xs:element name="DiscountCouponValue" type="xs:double" minOccurs="0" />
              <xs:element name="DiscountCouponIsFlatValue" type="xs:boolean" />
              <xs:element name="DiscountCouponCode">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="DiscountCouponGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="DiscountCouponLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_DiscountCoupon" />
      <xs:field xpath="DiscountCouponID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Shipping option', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="ShippingOptionID" fieldcaption="ShippingOptionID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="dc7a8f97-f1cb-4517-8031-489213a2445d" /><field column="ShippingOptionName" fieldcaption="ShippingOptionName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9804163e-683a-4f71-91ed-d2b81643f483" /><field column="ShippingOptionDisplayName" fieldcaption="ShippingOptionDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="765b2b6e-52b1-41c9-ab12-ec94b3ef6ad7" /><field column="ShippingOptionCharge" fieldcaption="ShippingOptionCharge" visible="true" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a22dba60-33bc-4d6a-8ad3-88c1f3459f49" /><field column="ShippingOptionEnabled" fieldcaption="ShippingOptionEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0fe5eaa7-6d54-4c76-90c5-376b35a11f9d" /><field column="ShippingOptionSiteID" fieldcaption="ShippingOptionSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fd1414cc-26d1-4d35-8dfe-6d6c0edf3a92" /><field column="ShippingOptionGUID" fieldcaption="ShippingOptionGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="18dd26bf-7cc3-4d53-9cd4-b3cb5df860da" /><field column="ShippingOptionLastModified" fieldcaption="ShippingOptionLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="949f7f46-fd67-4cc3-8942-77064b1924cf" /></form>', '20081229 09:07:02', 1150, N'Ecommerce.ShippingOption', 0, 0, 0, 'b556c066-57d3-4400-b601-78bb4f527447', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_ShippingOption', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_ShippingOption">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ShippingOptionID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ShippingOptionName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ShippingOptionDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ShippingOptionCharge" type="xs:double" />
              <xs:element name="ShippingOptionEnabled" type="xs:boolean" />
              <xs:element name="ShippingOptionSiteID" type="xs:int" />
              <xs:element name="ShippingOptionGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="ShippingOptionLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_ShippingOption" />
      <xs:field xpath="ShippingOptionID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Exchange table', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ExchangeTableID" fieldcaption="ExchangeTableID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="0598477f-1a96-4453-a8bc-5d4a4d374a47" /><field column="ExchangeTableDisplayName" fieldcaption="ExchangeTableDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b9524920-c9cd-4861-b338-b6a6c91cf70e" /><field column="ExchangeTableValidFrom" fieldcaption="ExchangeTableValidFrom" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ad6205d5-899a-49f8-8760-8ac47c5c48c3" /><field column="ExchangeTableValidTo" fieldcaption="ExchangeTableValidTo" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bff8a62c-8e74-413e-b319-d22e2490a4e6" /><field column="ExchangeTableGUID" fieldcaption="ExchangeTableGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1a96357c-6911-4b5b-bb22-ec5cc29a7476" /><field column="ExchangeTableLastModified" fieldcaption="ExchangeTableLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1c59db57-7b91-4cc5-aac3-8d6a69dd3e58" /></form>', '20100414 15:51:27', 1151, N'ecommerce.exchangetable', 0, 0, 0, 'f13d168a-10fd-4202-a9f6-cc0c2c61b4ce', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_ExchangeTable', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_ExchangeTable">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ExchangeTableID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ExchangeTableDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ExchangeTableValidFrom" type="xs:dateTime" minOccurs="0" />
              <xs:element name="ExchangeTableValidTo" type="xs:dateTime" minOccurs="0" />
              <xs:element name="ExchangeTableGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="ExchangeTableLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_ExchangeTable" />
      <xs:field xpath="ExchangeTableID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Tax class', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TaxClassID" fieldcaption="TaxClassID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="120dba86-26f2-4062-87b9-c2df858fea8d" /><field column="TaxClassName" fieldcaption="TaxClassName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3a64b5fd-d188-4bb7-83f5-8644669e296a" /><field column="TaxClassDisplayName" fieldcaption="TaxClassDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="170b4fb7-d206-4bb8-87fa-c132a3dd3193" /><field column="TaxClassZeroIfIDSupplied" fieldcaption="TaxClassZeroIfIDSupplied" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3d8003fd-ab1c-4f33-a7f8-5b060cecfad8" /><field column="TaxClassGUID" fieldcaption="TaxClassGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3627e0db-1184-4d5d-a27f-1e11eff3ffe3" /><field column="TaxClassLastModified" fieldcaption="TaxClassLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e5339651-4379-490c-9fea-c380ed074213" /></form>', '20081229 09:07:03', 1152, N'ecommerce.taxclass', 0, 0, 0, 'b4fa4dd9-d561-463e-8aa9-1c3cc4550ac2', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_TaxClass', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_TaxClass">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TaxClassID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="TaxClassName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaxClassDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaxClassZeroIfIDSupplied" type="xs:boolean" minOccurs="0" />
              <xs:element name="TaxClassGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="TaxClassLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_TaxClass" />
      <xs:field xpath="TaxClassID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'TaxClassCountry', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TaxClassID" fieldcaption="TaxClassID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="9867db8a-cff3-4795-a695-3cb52f1bff4e" /><field column="CountryID" fieldcaption="CountryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6304fe0c-face-498e-a72e-4a1bc2eccd24" /><field column="TaxValue" fieldcaption="TaxValue" visible="true" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7671e12b-8ab1-4438-b1f9-947bba3cfb48" /><field column="IsFlatValue" fieldcaption="IsFlatValue" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="dcd43c73-4dc1-47cd-9391-11349a5aa35c" /></form>', '20081229 09:07:24', 1154, N'ECommerce.TaxClassCountry', 0, 0, 0, '87c1d77d-21ea-4629-ad2a-eaab556602f7', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_TaxClassCountry', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_TaxClassCountry">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TaxClassID" type="xs:int" />
              <xs:element name="CountryID" type="xs:int" />
              <xs:element name="TaxValue" type="xs:double" />
              <xs:element name="IsFlatValue" type="xs:boolean" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_TaxClassCountry" />
      <xs:field xpath="TaxClassID" />
      <xs:field xpath="CountryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Address', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="AddressID" fieldcaption="AddressID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="45acf5f0-2a7d-407b-996b-438274529c9a" /><field column="AddressName" fieldcaption="AddressName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5e42b159-00f3-4933-85ab-73f3de18a8ee" /><field column="AddressLine1" fieldcaption="AddressLine1" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="650bc95e-0923-4cbe-b83c-4a250bfe5505" /><field column="AddressLine2" fieldcaption="AddressLine2" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6e25541d-4592-426d-9ace-eb8c4f9903aa" /><field column="AddressCity" fieldcaption="AddressCity" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0633aba6-62bd-4184-ae5f-5ad85e562358" /><field column="AddressZip" fieldcaption="AddressZip" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d036387c-702f-437f-9785-218dac48ad3c" /><field column="AddressPhone" fieldcaption="AddressPhone" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c5e86531-d896-4784-a7ce-46e4e7563fa2" /><field column="AddressCustomerID" fieldcaption="AddressCustomerID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="26fe2b8e-a2c2-40de-8fe6-a2f97dd5e171" /><field column="AddressCountryID" fieldcaption="AddressCountryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8a967728-36e1-4eb9-964c-a91c8a557594" /><field column="AddressStateID" fieldcaption="AddressStateID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ac508421-fa90-4e76-9387-06b5af8df17b" /><field column="AddressIsBilling" fieldcaption="AddressIsBilling" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b4dbab58-4ce0-46cd-936e-889a5dc45540" /><field column="AddressEnabled" fieldcaption="AddressEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4dcca810-6afe-46ca-8d38-9633fcf374b4" /><field column="AddressPersonalName" fieldcaption="AddressPersonalName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8879c6fb-f4ae-4802-9d82-82fd3d074124" /><field column="AddressIsShipping" fieldcaption="AddressIsShipping" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cfe91c20-eda4-4e6a-86b3-6579d973ef2b" /><field column="AddressIsCompany" fieldcaption="AddressIsCompany" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="558c8a0d-7328-4cd8-bed2-82a76514c24f" /><field column="AddressGUID" fieldcaption="AddressGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f038c0a7-59ae-477e-b9c6-5dfc9daedf3e" /><field column="AddressLastModified" fieldcaption="AddressLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="33264671-3b18-4eb9-b7fc-d96c007adb37" /></form>', '20081229 09:07:00', 1156, N'ecommerce.address', 0, 0, 0, '3cc96754-268c-4eeb-94cf-44204a27431a', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_Address', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_Address">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="AddressID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="AddressName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AddressLine1">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AddressLine2">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AddressCity">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AddressZip">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="20" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AddressPhone" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AddressCustomerID" type="xs:int" />
              <xs:element name="AddressCountryID" type="xs:int" />
              <xs:element name="AddressStateID" type="xs:int" minOccurs="0" />
              <xs:element name="AddressIsBilling" type="xs:boolean" />
              <xs:element name="AddressEnabled" type="xs:boolean" />
              <xs:element name="AddressPersonalName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AddressIsShipping" type="xs:boolean" />
              <xs:element name="AddressIsCompany" type="xs:boolean" minOccurs="0" />
              <xs:element name="AddressGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="AddressLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_Address" />
      <xs:field xpath="AddressID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Customer', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="CustomerID" fieldcaption="CustomerID" visible="false" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="96c3731e-c8dc-4bb7-b692-120b5e92e67d" /><field column="CustomerFirstName" fieldcaption="CustomerFirstName" visible="false" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="200" guid="b2b44dc5-ae13-4180-9fad-7a47431b9b36" /><field column="CustomerLastName" fieldcaption="CustomerLastName" visible="false" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="200" guid="f1470954-5eed-49a9-91ae-de257fc8cc6b" /><field column="CustomerEmail" fieldcaption="CustomerEmail" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="200" guid="ed57af72-8df1-456f-b369-041c7faf0085" /><field column="CustomerPhone" fieldcaption="CustomerPhone" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="50" guid="ec61f54e-1202-4e42-8646-30f067f38eb2" /><field column="CustomerFax" fieldcaption="CustomerFax" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="50" guid="044422b8-46d8-4222-b0f7-9466783c2274" /><field column="CustomerCompany" fieldcaption="CustomerCompany" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="200" guid="0b263893-c00f-44f7-a36d-ca0449ac9f0a" /><field column="CustomerUserID" fieldcaption="CustomerUserID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="54062fca-65ff-4995-9b7a-404ca2e04f58" /><field column="CustomerPreferredCurrencyID" fieldcaption="CustomerPreferredCurrencyID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3142d83c-af78-484f-9867-946339d66fba" /><field column="CustomerPreferredShippingOptionID" fieldcaption="CustomerPreferredShippingOptionID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8217b461-c402-41b2-8923-1364b6468ba2" /><field column="CustomerCountryID" fieldcaption="CustomerCountryID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="80182aa0-ae56-432a-8f66-51ea1ec857ac" /><field column="CustomerEnabled" fieldcaption="CustomerEnabled" visible="false" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" defaultvalue="false" guid="86754dcb-f60a-490c-916e-c3e51d63cc38" /><field column="CustomerPrefferedPaymentOptionID" fieldcaption="CustomerPrefferedPaymentOptionID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0bc8e195-ea84-4474-bede-ed7bbdd1ad04" /><field column="CustomerStateID" fieldcaption="CustomerStateID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e2feb737-5110-40db-b535-12079051379e" /><field column="CustomerGUID" fieldcaption="CustomerGUID" visible="false" columntype="text" fieldtype="upload" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="500" guid="56206b17-fdf9-4953-9136-6e91e18d6d4d" /><field column="CustomerTaxRegistrationID" fieldcaption="CustomerTaxRegistrationID" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="50" guid="873dce9d-ee95-4793-a1ed-963a3feeee42" /><field column="CustomerOrganizationID" fieldcaption="CustomerOrganizationID" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="50" guid="e7df891f-ea59-4add-876b-69d4a619ac12" /><field column="CustomerDiscountLevelID" fieldcaption="CustomerDiscountLevelID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7fbca01a-61af-4362-a273-e2aa8a3a454c" /><field column="CustomerLastModified" fieldcaption="CustomerLastModified" visible="false" columntype="datetime" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="false" guid="87a1a570-2285-457b-9508-d541995a7769" /></form>', '20091103 09:00:44', 1157, N'ecommerce.customer', 0, 0, 0, 'cd867311-743a-4599-ba72-5fe29b1c4a9c', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_Customer', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_Customer">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CustomerID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="CustomerFirstName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CustomerLastName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CustomerEmail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CustomerPhone" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CustomerFax" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CustomerCompany" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CustomerUserID" type="xs:int" minOccurs="0" />
              <xs:element name="CustomerPreferredCurrencyID" type="xs:int" minOccurs="0" />
              <xs:element name="CustomerPreferredShippingOptionID" type="xs:int" minOccurs="0" />
              <xs:element name="CustomerCountryID" type="xs:int" minOccurs="0" />
              <xs:element name="CustomerEnabled" type="xs:boolean" />
              <xs:element name="CustomerPrefferedPaymentOptionID" type="xs:int" minOccurs="0" />
              <xs:element name="CustomerStateID" type="xs:int" minOccurs="0" />
              <xs:element name="CustomerGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="CustomerTaxRegistrationID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CustomerOrganizationID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CustomerDiscountLevelID" type="xs:int" minOccurs="0" />
              <xs:element name="CustomerCreated" type="xs:dateTime" minOccurs="0" />
              <xs:element name="CustomerLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_Customer" />
      <xs:field xpath="CustomerID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Order', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="OrderID" fieldcaption="OrderID" visible="false" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="f28fb94f-e177-48e6-aaf6-acfcd856ac3e" /><field column="OrderBillingAddressID" fieldcaption="OrderBillingAddressID" visible="false" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="27697294-392b-4a76-8a1d-e9a1cb05eb58" /><field column="OrderShippingAddressID" fieldcaption="OrderShippingAddressID" visible="false" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="26b8aeb8-fade-40be-9f3d-3ce906583670" /><field column="OrderShippingOptionID" fieldcaption="OrderShippingOptionID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0eccbbc8-1bb0-4ef9-9c34-6d30c87ea79c" /><field column="OrderTotalShipping" fieldcaption="OrderTotalShipping" visible="false" columntype="double" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="075b59b9-762d-46f7-8e08-e0008ad055ea" /><field column="OrderTotalPrice" fieldcaption="OrderTotalPrice" visible="false" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fef6bb8a-2c7d-4ea3-8335-920498657802" /><field column="OrderTotalTax" fieldcaption="OrderTotalTax" visible="false" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="786aede8-95e6-4d3a-a285-1627661b9ca7" /><field column="OrderDate" fieldcaption="OrderDate" visible="false" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fc38316b-50bd-49f2-b469-2a8649440ed8"><settings><editTime>false</editTime></settings></field><field column="OrderStatusID" fieldcaption="OrderStatusID" visible="false" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="11250533-9320-4950-b72c-f44d48c6903c" /><field column="OrderCurrencyID" fieldcaption="OrderCurrencyID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="efeadf73-e9a5-4283-b015-0702a04163e9" /><field column="OrderCustomerID" fieldcaption="OrderCustomerID" visible="false" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ae5f163d-d345-459c-97e2-3adb7150195f" /><field column="OrderCreatedByUserID" fieldcaption="OrderCreatedByUserID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1fda4f50-5db3-4803-aeb5-7c1ddfbf85ab" /><field column="OrderNote" fieldcaption="OrderNote" visible="false" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ad9e3376-04c8-42b3-b405-d0f36bebb606" /><field column="OrderSiteID" fieldcaption="OrderSiteID" visible="false" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="72c87c0f-a61b-4ff4-8c2b-4f6b510e03a1" /><field column="OrderPaymentOptionID" fieldcaption="OrderPaymentOptionID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d7942b91-97c7-46d0-a4b5-770a006065e1" /><field column="OrderInvoice" fieldcaption="OrderInvoice" visible="false" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="da660e03-1666-44e4-b05c-15b52a9e2f44" /><field column="OrderInvoiceNumber" fieldcaption="OrderInvoiceNumber" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="200" guid="2e515b8c-5186-488f-8836-84ae1a3a73bb" /><field column="OrderDiscountCouponID" fieldcaption="OrderDiscountCouponID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="dceb6698-1196-4cd4-803e-bdc73f5c3413" /><field column="OrderCompanyAddressID" fieldcaption="OrderCompanyAddressID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43aac489-4194-4d9e-898c-bdbad5dc4f0d" /><field column="OrderTrackingNumber" fieldcaption="OrderTrackingNumber" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="100" guid="a8e47954-ef84-46cc-a2e7-c8ae6515b4c5" /><field column="OrderCustomData" fieldcaption="OrderCustomData" visible="false" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0162e231-adda-4951-b014-fde264ec2103" /><field column="OrderPaymentResult" fieldcaption="OrderPaymentResult" visible="false" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="59784e17-5d9d-48e0-93a1-ae6d97564121" /><field column="OrderGUID" visible="false" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cfcbbfde-eda8-4fd8-b94c-7d92bac0b907" /><field column="OrderLastModified" visible="false" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0554677d-56cc-4b16-8992-7e02ae6c46f7" /></form>', '20091103 08:59:28', 1158, N'ecommerce.order', 0, 0, 0, '58eb48fc-83f4-41f7-add2-bc3ce5de8909', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_Order', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_Order">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="OrderID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="OrderBillingAddressID" type="xs:int" />
              <xs:element name="OrderShippingAddressID" type="xs:int" />
              <xs:element name="OrderShippingOptionID" type="xs:int" minOccurs="0" />
              <xs:element name="OrderTotalShipping" type="xs:double" minOccurs="0" />
              <xs:element name="OrderTotalPrice" type="xs:double" />
              <xs:element name="OrderTotalTax" type="xs:double" />
              <xs:element name="OrderDate" type="xs:dateTime" />
              <xs:element name="OrderStatusID" type="xs:int" />
              <xs:element name="OrderCurrencyID" type="xs:int" minOccurs="0" />
              <xs:element name="OrderCustomerID" type="xs:int" />
              <xs:element name="OrderCreatedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="OrderNote" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="OrderSiteID" type="xs:int" />
              <xs:element name="OrderPaymentOptionID" type="xs:int" minOccurs="0" />
              <xs:element name="OrderInvoice" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="OrderInvoiceNumber" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="OrderDiscountCouponID" type="xs:int" minOccurs="0" />
              <xs:element name="OrderCompanyAddressID" type="xs:int" minOccurs="0" />
              <xs:element name="OrderTrackingNumber" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="OrderCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="OrderPaymentResult" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="OrderGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="OrderLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_Order" />
      <xs:field xpath="OrderID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - SKU', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="SKUID" fieldcaption="SKUID" visible="false" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="95abe990-8663-4a8d-8db4-a4d104579424" visibility="none" ismacro="false" /><field column="SKUNumber" fieldcaption="SKUNumber" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="200" guid="c37e01b4-9477-4ccd-86f8-b6f000743184" /><field column="SKUName" fieldcaption="SKUName" visible="false" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="440" guid="161e6482-fbd6-41df-b251-5ef9603f8576" visibility="none" ismacro="false" /><field column="SKUDescription" fieldcaption="SKUDescription" visible="false" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b13a8a29-f8a9-49ed-8cb4-4db417e174ab" external="true" /><field column="SKUPrice" fieldcaption="SKUPrice" visible="false" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a3c3478f-0507-44ef-a8be-26f7fd3833c5" /><field column="SKUEnabled" fieldcaption="SKUEnabled" visible="false" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" defaultvalue="false" guid="bedcaf24-ad52-4293-9418-f7fcecbb9811" /><field column="SKUDepartmentID" fieldcaption="SKUDepartmentID" visible="false" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f97d35fc-6b5a-4c23-a666-15f8cfe7faa6" /><field column="SKUManufacturerID" fieldcaption="SKUManufacturerID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="808244ef-6595-42f2-90a0-9bf0717645e1" visibility="none" /><field column="SKUInternalStatusID" fieldcaption="SKUInternalStatusID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e6053672-514c-4f66-8c2e-87da9dd3f310" /><field column="SKUPublicStatusID" fieldcaption="SKUPublicStatusID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9e5eaa25-43b3-40af-b651-94387a5b77ef" /><field column="SKUSupplierID" fieldcaption="SKUSupplierID" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="57cc81c5-1f0c-45d4-9741-e37d7f35db34" /><field column="SKUAvailableInDays" fieldcaption="SKUAvailableInDays" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7d56b109-15d2-41ed-ab41-c505b50b8386" /><field column="SKUGUID" fieldcaption="SKUGUID" visible="false" columntype="file" fieldtype="upload" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="500" guid="99228497-3209-44bd-8e5c-cd9de56e7fbd" /><field column="SKUImagePath" fieldcaption="SKUImagePath" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="450" guid="6174e45a-ca37-4946-a8b9-a53bd4fb76d2" /><field column="SKUWeight" fieldcaption="SKUWeight" visible="false" columntype="double" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="433ec6bf-2a24-46f7-ab64-413723f8d2d4" /><field column="SKUWidth" fieldcaption="SKUWidth" visible="false" columntype="double" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cada95d5-f56f-47b8-8ec5-2311f6420e8f" /><field column="SKUDepth" fieldcaption="SKUDepth" visible="false" columntype="double" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="01b371cb-8598-4409-82a5-955e5178dbfa" /><field column="SKUHeight" fieldcaption="SKUHeight" visible="false" columntype="double" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="960b12da-a78a-47af-b30a-dd5d4c963c7f" /><field column="SKUAvailableItems" fieldcaption="SKUAvailableItems" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="db263b45-b660-45e4-b02e-fa0819e75472" /><field column="SKUSellOnlyAvailable" fieldcaption="SKUSellOnlyAvailable" visible="false" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" defaultvalue="false" guid="ca3ef874-bccc-401d-ae31-f5b40c99f900" /><field column="SKUCustomData" fieldcaption="SKUCustomData" visible="false" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="29e8c9c5-d3c7-4846-a18f-057d5cd0a352" /><field column="SKUOptionCategoryID" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5781a44f-28d4-4ac6-b393-55c9fb2b1f3a" /><field column="SKUOrder" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3e41eb69-f9f4-42ea-a51b-f47ebb489b6e" /><field column="SKULastModified" visible="false" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="87e7a912-8806-4971-9912-af8711f71707" /><field column="SKUCreated" visible="false" columntype="datetime" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="046115eb-09d5-4c9f-b4ae-e5da3761b436" visibility="none" ismacro="false"><settings><timezonetype>inherit</timezonetype></settings></field></form>', '20100506 12:14:28', 1159, N'ecommerce.sku', 0, 0, 0, 'f1349c42-bae7-4614-a2ec-a7e61d8867c5', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_SKU', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_SKU">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SKUID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SKUNumber" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SKUName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="440" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SKUDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SKUPrice" type="xs:double" />
              <xs:element name="SKUEnabled" type="xs:boolean" />
              <xs:element name="SKUDepartmentID" type="xs:int" />
              <xs:element name="SKUManufacturerID" type="xs:int" minOccurs="0" />
              <xs:element name="SKUInternalStatusID" type="xs:int" minOccurs="0" />
              <xs:element name="SKUPublicStatusID" type="xs:int" minOccurs="0" />
              <xs:element name="SKUSupplierID" type="xs:int" minOccurs="0" />
              <xs:element name="SKUAvailableInDays" type="xs:int" minOccurs="0" />
              <xs:element name="SKUGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="SKUImagePath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SKUWeight" type="xs:double" minOccurs="0" />
              <xs:element name="SKUWidth" type="xs:double" minOccurs="0" />
              <xs:element name="SKUDepth" type="xs:double" minOccurs="0" />
              <xs:element name="SKUHeight" type="xs:double" minOccurs="0" />
              <xs:element name="SKUAvailableItems" type="xs:int" minOccurs="0" />
              <xs:element name="SKUSellOnlyAvailable" type="xs:boolean" minOccurs="0" />
              <xs:element name="SKUCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SKUOptionCategoryID" type="xs:int" minOccurs="0" />
              <xs:element name="SKUOrder" type="xs:int" minOccurs="0" />
              <xs:element name="SKULastModified" type="xs:dateTime" />
              <xs:element name="SKUCreated" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_SKU" />
      <xs:field xpath="SKUID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'<search><item tokenized="False" name="SKUID" content="False" searchable="True" id="5a3c8a1a-15eb-499f-87de-7bbccf87f7bb" /><item tokenized="False" name="SKUNumber" content="False" searchable="True" id="2f6710a1-c0b8-4fd1-8059-e22bdf8243d9" /><item tokenized="True" name="SKUName" content="True" searchable="False" id="058b20c9-404a-4854-914c-7cdce8fbb12f" /><item tokenized="True" name="SKUDescription" content="True" searchable="False" id="e873e565-3934-4f0b-a7f3-af98ca934262" /><item tokenized="False" name="SKUPrice" content="False" searchable="True" id="679dfa2f-7d02-4f7e-8d6e-8cd4bb034189" /><item tokenized="False" name="SKUEnabled" content="False" searchable="True" id="9a58e5d5-2540-4f8a-96ed-fb458085e5ca" /><item tokenized="False" name="SKUDepartmentID" content="False" searchable="False" id="6307ca55-9b0f-4bfc-a15a-0d6b2e61f66e" /><item tokenized="False" name="SKUManufacturerID" content="False" searchable="False" id="b155fd3b-2cbe-4fdd-a487-ef0b792da003" /><item tokenized="False" name="SKUInternalStatusID" content="False" searchable="False" id="8c7edcf3-cb98-4d23-9eed-cb2b2ae1db9f" /><item tokenized="False" name="SKUPublicStatusID" content="False" searchable="False" id="6fbd9312-65c2-43b7-9b12-88b0b638b32c" /><item tokenized="False" name="SKUSupplierID" content="False" searchable="False" id="96bfc3ce-3dd9-4a86-bd8e-5df55cddc628" /><item tokenized="False" name="SKUAvailableInDays" content="False" searchable="True" id="3c19f259-dcad-47ba-87c2-250e8b752184" /><item tokenized="False" name="SKUGUID" content="False" searchable="False" id="1e03f2c6-2846-4ec0-a8fb-c7b76f1da629" /><item tokenized="False" name="SKUImagePath" content="False" searchable="True" id="041651ea-5e0a-4c16-9f50-c48a5f06a6cd" /><item tokenized="False" name="SKUWeight" content="False" searchable="True" id="f46f3407-c3e0-4001-a9c9-2dc81bd026c2" /><item tokenized="False" name="SKUWidth" content="False" searchable="True" id="0732d238-905c-449d-80e1-3d7c90bb7143" /><item tokenized="False" name="SKUDepth" content="False" searchable="True" id="eff8c070-8994-4361-ae6f-7d757e693655" /><item tokenized="False" name="SKUHeight" content="False" searchable="True" id="a2ad4a1f-c8f7-4c16-a08c-7958fdaa69c2" /><item tokenized="False" name="SKUAvailableItems" content="False" searchable="True" id="10da527a-f449-4fae-9270-732ce8432344" /><item tokenized="False" name="SKUSellOnlyAvailable" content="False" searchable="True" id="3574dcae-4514-48ea-a848-6f3202492ede" /><item tokenized="False" name="SKUCustomData" content="False" searchable="False" id="6662af9a-a7e2-4a6a-88fd-667ec7ec8722" /><item tokenized="False" name="SKUOptionCategoryID" content="False" searchable="False" id="2c1292d7-262a-47e6-a8dd-262f9fc31afb" /><item tokenized="False" name="SKUOrder" content="False" searchable="True" id="2506edb5-c40c-4499-812b-70209421f33a" /><item tokenized="False" name="SKULastModified" content="False" searchable="False" id="df2113f9-b818-48e6-bc9c-d05c4fc98d2c" /><item tokenized="False" name="SKUCreated" content="False" searchable="True" id="7158ba82-0c5b-43a0-9f34-5177974153dc" /></search>', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Order item', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="OrderItemID" fieldcaption="OrderItemID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="d4f766eb-288a-4b3e-87ea-d0929b2cd93f" /><field column="OrderItemOrderID" fieldcaption="OrderItemOrderID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6ff5cd02-cfdf-418e-b6df-dfb477ed8819" /><field column="OrderItemSKUID" fieldcaption="OrderItemSKUID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="94d1bc70-53c7-4753-9652-5f6929c372c8" /><field column="OrderItemSKUName" fieldcaption="OrderItemSKUName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" columnsize="450" guid="4cfc2089-cbc3-4e95-9135-ff18a4602836" /><field column="OrderItemUnitPrice" fieldcaption="OrderItemUnitPrice" visible="true" columntype="double" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="76c993a3-e9c5-416f-96d7-7a0a8985cd42" /><field column="OrderItemUnitCount" fieldcaption="OrderItemUnitCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="aa23435f-dc7b-4836-9056-79e473c56153" /><field column="OrderItemCustomData" fieldcaption="OrderItemCustomData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="274c6fda-298f-4dcd-84e8-2fdb1ec00f45" /><field column="OrderItemGuid" visible="false" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8d32219d-22fd-4305-8ad8-fcc9053e35c5" /><field column="OrderItemParentGuid" visible="false" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="581d726c-a02b-43f1-aa1d-1976a35aa14a" /><field column="OrderItemLastModified" visible="false" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="599a7c93-bc13-497e-8f08-8da4a23e093a" /></form>', '20090422 16:13:58', 1161, N'ecommerce.orderitem', 0, 0, 0, '201d37f4-8961-45d1-9b34-05303f8df065', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_OrderItem', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_OrderItem">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="OrderItemID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="OrderItemOrderID" type="xs:int" />
              <xs:element name="OrderItemSKUID" type="xs:int" />
              <xs:element name="OrderItemSKUName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="OrderItemUnitPrice" type="xs:double" />
              <xs:element name="OrderItemUnitCount" type="xs:int" />
              <xs:element name="OrderItemCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="OrderItemGuid" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="OrderItemParentGuid" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="OrderItemLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_OrderItem" />
      <xs:field xpath="OrderItemID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Shopping cart', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="ShoppingCartID" fieldcaption="ShoppingCartID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="c46cbaf5-3ff2-4503-a206-d6f9c7967de6" /><field column="ShoppingCartGUID" fieldcaption="ShoppingCartGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="595611ae-e9d0-48bc-8dc4-c03f1aedaca6" /><field column="ShoppingCartUserID" fieldcaption="ShoppingCartUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4d7e8b85-0d95-45b8-8667-2149bbd30656" /><field column="ShoppingCartSiteID" fieldcaption="ShoppingCartSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="970e2c1d-f283-4280-8f58-3032e55473c2" /><field column="ShoppingCartLastUpdate" fieldcaption="ShoppingCartLastUpdate" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fc89a87d-246c-41da-b28d-ce9577e7d705" /><field column="ShoppingCartCurrencyID" fieldcaption="ShoppingCartCurrencyID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fa403d6a-27e8-4c85-af97-4e2653f9823b" /><field column="ShoppingCartPaymentOptionID" fieldcaption="ShoppingCartPaymentOptionID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b4157cc5-c600-45af-b243-c4caea0d2b50" /><field column="ShoppingCartShippingOptionID" fieldcaption="ShoppingCartShippingOptionID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="58c0aa4e-e24e-4776-bb4b-e3adc1a67e63" /><field column="ShoppingCartDiscountCouponID" fieldcaption="ShoppingCartDiscountCouponID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="82f0b1e8-f3ac-4918-b414-c056744c346f" /><field column="ShoppingCartBillingAddressID" fieldcaption="ShoppingCartBillingAddressID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="00ab0979-20f9-452c-8cb0-09cf2ef776b6" /><field column="ShoppingCartShippingAddressID" fieldcaption="ShoppingCartShippingAddressID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7014e4e5-6067-416b-bd27-2815fad98bea" /><field column="ShoppingCartCustomerID" fieldcaption="ShoppingCartCustomerID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="746649e0-c4fd-4b06-a05a-908cb3b3b875" /><field column="ShoppingCartNote" fieldcaption="ShoppingCartNote" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0fcd34b7-3982-44ef-914d-922ed3f99395" /><field column="ShoppingCartCompanyAddressID" fieldcaption="ShoppingCartCompanyAddressID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="40009fbf-c9b9-41c2-b717-491de0cfb316" /><field column="ShoppingCartCustomData" fieldcaption="ShoppingCartCustomData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43d0e526-c14a-4406-a7d1-eaa5ebc6636a" /></form>', '20090714 19:18:49', 1162, N'ecommerce.shoppingcart', 0, 0, 0, 'c8a865c2-df9e-4f10-9b9e-f78bc0926f15', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'COM_ShoppingCart', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_ShoppingCart">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ShoppingCartID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ShoppingCartGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ShoppingCartUserID" type="xs:int" minOccurs="0" />
              <xs:element name="ShoppingCartSiteID" type="xs:int" />
              <xs:element name="ShoppingCartLastUpdate" type="xs:dateTime" />
              <xs:element name="ShoppingCartCurrencyID" type="xs:int" minOccurs="0" />
              <xs:element name="ShoppingCartPaymentOptionID" type="xs:int" minOccurs="0" />
              <xs:element name="ShoppingCartShippingOptionID" type="xs:int" minOccurs="0" />
              <xs:element name="ShoppingCartDiscountCouponID" type="xs:int" minOccurs="0" />
              <xs:element name="ShoppingCartBillingAddressID" type="xs:int" minOccurs="0" />
              <xs:element name="ShoppingCartShippingAddressID" type="xs:int" minOccurs="0" />
              <xs:element name="ShoppingCartCustomerID" type="xs:int" minOccurs="0" />
              <xs:element name="ShoppingCartNote" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ShoppingCartCompanyAddressID" type="xs:int" minOccurs="0" />
              <xs:element name="ShoppingCartCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_ShoppingCart" />
      <xs:field xpath="ShoppingCartID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Ecommerce - Order status - user', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="OrderID" fieldcaption="OrderID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="89c15ea5-4d0a-485a-8648-bf16a26608c8" /><field column="FromStatusID" fieldcaption="FromStatusID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4c7ca64c-b914-422b-95d5-aa390862dace" /><field column="ToStatusID" fieldcaption="ToStatusID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="02f7c0cb-be80-4f2a-a4d3-83a4f834a921" /><field column="ChangedByUserID" fieldcaption="ChangedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a01e87ea-ab09-40fa-87cd-d0a76b2dc3cf" /><field column="Date" fieldcaption="Date" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ddb5c3f2-4e45-4a6c-b9bc-bdb20510d49d" /><field column="Note" fieldcaption="Note" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cb010f29-a80d-4232-b876-2c6a4fc48ad2" /><field column="OrderStatusUserID" fieldcaption="OrderStatusUserID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="32fe6606-1f77-42bc-8835-b4b8d09b1770" /></form>', '20081229 09:07:02', 1163, N'ecommerce.OrderStatusUser', 0, 0, 0, 'd196077d-d956-4c09-9ffd-f8239ac3d81c', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'COM_OrderStatusUser', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="COM_OrderStatusUser">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="OrderID" type="xs:int" />
              <xs:element name="FromStatusID" type="xs:int" minOccurs="0" />
              <xs:element name="ToStatusID" type="xs:int" />
              <xs:element name="ChangedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="Date" type="xs:dateTime" />
              <xs:element name="Note" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="OrderStatusUserID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//COM_OrderStatusUser" />
      <xs:field xpath="OrderStatusUserID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
set identity_insert [cms_class] off
