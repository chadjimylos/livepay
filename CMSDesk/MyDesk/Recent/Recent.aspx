<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Recent.aspx.cs" Inherits="CMSDesk_MyDesk_Recent_Recent"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="MyDesk - Recent documents" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="server">
    <cms:UniGrid ID="ugRecentDocs" runat="server" GridName="recent_List.xml" OrderBy="DocumentModifiedWhen DESC"
        IsLiveSite="false" />

    <script type="text/javascript">
        //<![CDATA[
        // Select item action
        function SelectItem(nodeId, culture) {
            if (nodeId != 0) {
                parent.parent.location.href = "../../default.aspx?section=content&action=edit&nodeid=" + nodeId + "&culture=" + culture;
            }
        }
        //]]>
    </script>

</asp:Content>
