using System.IO;
using System.Data.SqlClient;
using System.Xml;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.IDataConnectionLibrary;
using CMS.DataEngine;

/// <summary>
/// Installer methods
/// </summary>
public static class InstallerFunctions
{
    /// <summary>
    /// Redirects user to the installation page if connectionString not set.
    /// </summary>
    public static bool InstallRedirect()
    {
        // Check if the connection string is initialized
        if (!SqlHelperClass.IsConnectionStringInitialized)
        {
            string currentPath = "";

            if (HttpContext.Current != null)
            {
                currentPath = HttpContext.Current.Request.Path.ToLower();
            }

            string relativePath = UrlHelper.RemoveApplicationPath(currentPath);

            string currentFile = Path.GetFileName(relativePath);
            if (currentFile == "install.aspx")
            {
                return true;
            }

            string fileExtension = Path.GetExtension(currentFile);
            if (fileExtension == ".aspx")
            {
                if (!IsInstallerExcluded(relativePath))
                {
                    if (HttpContext.Current != null)
                    {
                        UrlHelper.Redirect("~/cmsinstall/install.aspx");
                    }
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }


    /// <summary>
    /// Returns true if the path is excluded for the installer process
    /// </summary>
    /// <param name="path">Path to check</param>
    private static bool IsInstallerExcluded(string path)
    {
        if (path.StartsWith("/cmsmessages"))
        {
            return true;
        }

        return false;
    }


    /// <summary>
    /// Checks if all folders of the given path exist and if not, it creates them.
    /// </summary>
    /// <param name="path">Full disk path.</param>
    public static void EnsureDiskPath(string path)
    {
        // Get the folder path
        string folderPath = Path.GetDirectoryName(path);

        // Create the directory
        if (!Directory.Exists(folderPath))
        {
            Directory.CreateDirectory(folderPath);
        }
    }
}

