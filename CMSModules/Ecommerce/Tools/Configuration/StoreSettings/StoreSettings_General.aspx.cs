using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_StoreSettings_StoreSettings_General : CMSEcommerceConfigurationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get current main currency
        int mainCurrencyId = 0;
        CurrencyInfo mainCurrency = CurrencyInfoProvider.GetMainCurrency();
        if (mainCurrency != null)
        {
            DataSet ds = CurrencyInfoProvider.GetCurrencies("CurrencyEnabled = 1 AND NOT CurrencyID = " + mainCurrency.CurrencyID, null, 0, null);
            if (DataHelper.DataSourceIsEmpty(ds))
            {
                this.btnChangeCurrency.Enabled = false;
            }
            mainCurrencyId = mainCurrency.CurrencyID;
            lblMainCurrency.Text = HTMLHelper.HTMLEncode(ResHelper.LocalizeString(mainCurrency.CurrencyDisplayName));
        }
        else
        {
            lblMainCurrency.Text = ResHelper.GetString("general.na");
        }

        // Register scripts for currency change
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        btnChangeCurrency.OnClientClick = "modalDialog('StoreSettings_ChangeCurrency.aspx', 'Change main currency', 600, 480);";

        // control initializations				
        lblAllowAnonymous.Text = ResHelper.GetString("Configuration_StoreSettings.lblAllowAnonymous");
        lblShippingFreeLimit.Text = ResHelper.GetString("Configuration_StoreSettings.lblShippingFreeLimit");
        lblDefaultCountry.Text = ResHelper.GetString("Configuration_StoreSettings.lblDefaultCountry");
        lblOrganizationID.Text = ResHelper.GetString("Configuration_StoreSettings.lblOrganizationID");
        lblTaxRegistrationID.Text = ResHelper.GetString("Configuration_StoreSettings.lblTaxRegistrationID");
        lblUseCompanyAddress.Text = ResHelper.GetString("Configuration_StoreSettings.lblUseCompanyAddress");
        lblRequireIDs.Text = ResHelper.GetString("Configuration_StoreSettings.lblRequireIDs");
        lblCurrentMainCurrency.Text = ResHelper.GetString("Configuration_StoreSettings.lblMainCurrency");
        btnChangeCurrency.Text = ResHelper.GetString("general.change");

        rvShippingFreeLimit.ErrorMessage = ResHelper.GetString("Configuration_StoreSettings.ErrorShippingFreeLimit");

        btnOk.Text = ResHelper.GetString("General.OK");

        // fill editing form
        if (!RequestHelper.IsPostBack())
        {
            LoadData();
        }
    }


    /// <summary>
    /// Load data of editing currency.
    /// </summary>
    /// <param name="currencyObj">Currency object.</param>
    protected void LoadData()
    {
        bool noneCountryAdded = false;

        // Load countries
        drpCountry.DataSource = CountryInfoProvider.GetCountries(null, "CountryDisplayName ASC");
        drpCountry.DataBind();

        if (DataHelper.DataSourceIsEmpty(drpCountry.DataSource))
        {
            AddNoneCountrySelected();
            noneCountryAdded = true;
        }

        if (CMSContext.CurrentSite != null)
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(CMSContext.CurrentSiteName);
            if (si != null)
            {
                // Set default country
                try
                {
                    if (si.SiteDefaultCountryID > 0)
                    {
                        drpCountry.SelectedValue = Convert.ToString(si.SiteDefaultCountryID);
                    }
                    else if (!noneCountryAdded)
                    {
                        AddNoneCountrySelected();
                    }
                }
                catch
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Ecommerce.SiteDefaultCountry.SelectionFailed");
                }


                // Set shopping free limit
                if (si.SiteStoreShippingFreeLimit != -1)
                {
                    txtShippingFreeLimit.Text = Convert.ToString(si.SiteStoreShippingFreeLimit);
                }

                chkAllowAnonymous.Checked = si.SiteStoreAllowAnonymousCustomers;
                chkOrganizationID.Checked = si.SiteShowOrganizationID;
                chkTaxRegistrationID.Checked = si.SiteShowTaxRegistrationID;
                chkUseCompanyAddress.Checked = si.SiteUseExtraCompanyAddress;
                chkRequireIDs.Checked = si.SiteRequireOrgTaxRegIDs;
                chkRequreIDs_CheckedChanged(null, null);
            }
        }
    }


    // Inserts "(none)" string to the country selection dropdownlist
    protected void AddNoneCountrySelected()
    {
        drpCountry.Items.Insert(0, new ListItem(ResHelper.GetString("general.selectnone"), "0"));
        drpCountry.SelectedIndex = 0;
    }


    /// <summary>
    /// On chkRequireIDs check box checked changed.
    /// </summary>
    protected void chkRequreIDs_CheckedChanged(object sender, EventArgs e)
    {
        if (chkRequireIDs.Checked)
        {
            chkOrganizationID.Checked = true;
            chkTaxRegistrationID.Checked = true;
            pnlIDs.Enabled = false;
        }
        else
        {
            pnlIDs.Enabled = true;
        }
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        string errorMessage = "";

        // Check that some country is selected
        if (drpCountry.SelectedValue == "0")
        {
            errorMessage = ResHelper.GetString("Ecommerce.DefaultCountry.NoneSelected");
        }

        // Check Shipping free limit
        if ((errorMessage == "") && (txtShippingFreeLimit.Text != ""))
        {
            errorMessage = new Validator().IsPositiveNumber(txtShippingFreeLimit.Text, ResHelper.GetString("Configuration_StoreSettings.ErrorShippingFreeLimit")).Result;
        }

        // Process form data
        if (errorMessage == "")
        {
            if (CMSContext.CurrentSite != null)
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(CMSContext.CurrentSite.SiteID);
                if (si != null)
                {
                    if (txtShippingFreeLimit.Text == "")
                    {
                        si.SiteStoreShippingFreeLimit = -1;
                    }
                    else
                    {
                        si.SiteStoreShippingFreeLimit = ValidationHelper.GetDouble((txtShippingFreeLimit.Text), 0);
                    }
                    si.SiteStoreAllowAnonymousCustomers = chkAllowAnonymous.Checked;
                    si.SiteDefaultCountryID = ValidationHelper.GetInteger(drpCountry.SelectedValue, 0);
                    si.SiteRequireOrgTaxRegIDs = chkRequireIDs.Checked;
                    si.SiteShowTaxRegistrationID = chkTaxRegistrationID.Checked;
                    si.SiteShowOrganizationID = chkOrganizationID.Checked;
                    si.SiteUseExtraCompanyAddress = chkUseCompanyAddress.Checked;

                    SiteInfoProvider.SetSiteInfo(si);
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
            }
        }


        if (errorMessage != "")
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
