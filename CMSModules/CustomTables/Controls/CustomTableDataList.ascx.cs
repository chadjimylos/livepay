using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections;

using CMS.CMSHelper;
using CMS.FormEngine;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_CustomTables_Controls_CustomTableDataList : CMSUserControl
{
    #region "Private & protected variables"

    protected string editToolTip = String.Empty;
    protected string deleteToolTip = String.Empty;
    protected string viewToolTip = String.Empty;
    protected string upToolTip = String.Empty;
    protected string downToolTip = String.Empty;

    protected string columnNames = null;

    // Default pages
    private string mEditItemPage = "~/CMSModules/CustomTables/Tools/CustomTable_Data_EditItem.aspx";
    private string mViewItemPage = "~/CMSModules/CustomTables/Tools/CustomTable_Data_ViewItem.aspx";

    protected DataSet ds = null;
    private DataClassInfo mCustomTableClassInfo = null;
    private FormInfo mFormInfo = null;
    private readonly CustomTableItemProvider ctProvider = new CustomTableItemProvider(CMSContext.CurrentUser);

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets URL of the page where item is edited
    /// </summary>
    public string EditItemPage
    {
        get
        {
            return mEditItemPage;
        }
        set
        {
            mEditItemPage = value;
        }
    }


    /// <summary>
    /// Gets or sets URL of the page where whole item is displayed
    /// </summary>
    public string ViewItemPage
    {
        get
        {
            return mViewItemPage;
        }
        set
        {
            mViewItemPage = value;
        }
    }


    /// <summary>
    /// Gets or sets the class info of custom table which data are displayed
    /// </summary>
    public DataClassInfo CustomTableClassInfo
    {
        get
        {
            return mCustomTableClassInfo;
        }
        set
        {
            mCustomTableClassInfo = value;
        }
    }


    /// <summary>
    /// Gets or sets the form info
    /// </summary>
    public FormInfo FormInfo
    {
        get
        {
            if (mFormInfo == null)
            {
                if (CustomTableClassInfo != null)
                {
                    mFormInfo = new FormInfo();
                    mFormInfo.LoadXmlDefinition(CustomTableClassInfo.ClassFormDefinition);
                }
            }
            return mFormInfo;
        }
    }


    /// <summary>
    /// Determines whether custom table has ItemOrder field
    /// </summary>
    public bool HasItemOrderField
    {
        get
        {
            if (FormInfo != null)
            {
                return (FormInfo.GetFormField("ItemOrder") != null);
            }
            else
            {
                // If form info is not available assume ItemOrder is not present to prevent further exceptions
                return false;
            }
        }
    }

    #endregion


    /// <summary>
    /// Page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Register Javascripts
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "DeleteEditView", ScriptHelper.GetScript(
           "var deleteConfirmation = ''; " +
           "function DeleteConfirm() { return confirm(deleteConfirmation); } " +
           "function EditItem(customtableid, itemId) { " +
           "  document.location.replace('" + ResolveUrl(EditItemPage) + "?customtableid=' + customtableid + '&itemId=' + itemId); } " +
           "function ViewItem(customtableid, itemId) { " +
           "  modalDialog('" + ResolveUrl(ViewItemPage) + "?customtableid=' + customtableid + '&itemId=' + itemId,'ViewItem',600,600); } "
           ));

        // Buttons' tooltips
        editToolTip = ResHelper.GetString("general.edit");
        deleteToolTip = ResHelper.GetString("general.delete");
        viewToolTip = ResHelper.GetString("general.view");
        upToolTip = ResHelper.GetString("general.up");
        downToolTip = ResHelper.GetString("general.down");

        // Delete confirmation
        ltlScript.Text = ScriptHelper.GetScript("deleteConfirmation = '" + ResHelper.GetString("customtable.data.DeleteConfirmation") + "';");

        gridData.OnLoadColumns += gridData_OnLoadColumns;
        gridData.OnExternalDataBound += gridData_OnExternalDataBound;
        gridData.OnAction += gridData_OnAction;
        gridData.OnDataReload += gridData_OnDataReload;

        if (HasItemOrderField)
        {
            gridData.OrderBy = "ItemOrder ASC";
        }
    }


    protected DataSet gridData_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        return ctProvider.GetItems(CustomTableClassInfo.ClassName, completeWhere, currentOrder, currentTopN, columnNames, currentOffset, currentPageSize, ref totalRecords);
    }


    protected void gridData_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "delete":
                if (CheckPermissions("Delete"))
                {
                    if (CustomTableClassInfo != null)
                    {
                        CustomTableItem item = ctProvider.GetItem(ValidationHelper.GetInteger(actionArgument, 0), CustomTableClassInfo.ClassName);
                        if (item != null)
                        {
                            item.Delete();
                        }
                    }

                    UrlHelper.Redirect(Page.Request.Url.ToString());
                }
                break;

            case "moveup":
                if (CheckPermissions("Modify"))
                {
                    if (CustomTableClassInfo != null)
                    {
                        ctProvider.MoveItemUp(ValidationHelper.GetInteger(actionArgument, 0), CustomTableClassInfo.ClassName);
                    }

                    UrlHelper.Redirect(Page.Request.Url.ToString());
                }
                break;

            case "movedown":
                if (CheckPermissions("Modify"))
                {
                    if (CustomTableClassInfo != null)
                    {
                        ctProvider.MoveItemDown(ValidationHelper.GetInteger(actionArgument, 0), CustomTableClassInfo.ClassName);
                    }

                    UrlHelper.Redirect(Page.Request.Url.ToString());
                }
                break;
        }
    }


    protected object gridData_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        ImageButton button = sender as ImageButton;
        GridViewRow grv = parameter as GridViewRow;
        if (grv != null)
        {
            DataRowView drv = grv.DataItem as DataRowView;

            // Hide Move Up/Down buttons when there is no Order field
            switch (sourceName.ToLower())
            {
                case "edit":
                    if ((button != null) && (drv != null))
                    {
                        button.OnClientClick = "EditItem(" + CustomTableClassInfo.ClassID + ", " + drv["ItemID"] + "); return false;";
                    }
                    break;

                case "view":
                    if ((button != null) && (drv != null))
                    {
                        button.OnClientClick = "ViewItem(" + CustomTableClassInfo.ClassID + ", " + drv["ItemID"] + "); return false;";
                    }
                    break;

                case "moveup":
                case "movedown":
                    if (!HasItemOrderField && (button != null))
                    {
                        button.Visible = false;
                    }
                    break;
            }
        }

        return parameter;
    }


    protected void gridData_OnLoadColumns()
    {
        if (CustomTableClassInfo != null)
        {
            ArrayList columnList = new ArrayList();
            string columns = CustomTableClassInfo.ClassShowColumns;
            if (string.IsNullOrEmpty(columns))
            {
                columnList.AddRange(GetExistingColumns());
            }
            else
            {
                // Get existing columns names
                ArrayList existingColumns = GetExistingColumns();
                // Get selected columns
                ArrayList selectedColumns = GetSelectedColumns(columns);

                columnNames = "";

                // Remove nonexisting columns
                foreach (string col in selectedColumns)
                {
                    if (existingColumns.Contains(col))
                    {
                        columnList.Add(col);
                        columnNames += ",[" + col + "]";
                    }
                }

                // Ensure itemorder column
                if (!selectedColumns.Contains("itemorder") && HasItemOrderField)
                {
                    columnNames += ",[itemorder]";
                }

                // Ensure itemid column
                if (!selectedColumns.Contains("itemid"))
                {
                    columnNames = ",[itemid]" + columnNames;
                }

                columnNames = columnNames.TrimStart(',');
            }

            // Loop trough all columns
            for (int i = 0; i < columnList.Count; i++)
            {
                string column = columnList[i].ToString();

                // Get field caption
                FormFieldInfo ffi = FormInfo.GetFormField(column);
                string fieldCaption = string.Empty;
                if (ffi == null)
                {
                    fieldCaption = column;
                }
                else
                {
                    fieldCaption = (ffi.Caption == string.Empty) ? column : ResHelper.LocalizeString(ffi.Caption);
                }

                if (i == columnList.Count - 1)
                {
                    // Stretch last column
                    gridData.AddColumn(fieldCaption, column, 100, true);
                }
                else
                {
                    gridData.AddColumn(fieldCaption, column, 0, true);
                }
            }
        }
    }


    /// <summary>
    /// Checks the specified permission.
    /// </summary>
    private bool CheckPermissions(string permissionName)
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.customtables", permissionName) &&
            !CMSContext.CurrentUser.IsAuthorizedPerClassName(CustomTableClassInfo.ClassName, permissionName))
        {
            lblError.Visible = true;
            lblError.Text = String.Format(ResHelper.GetString("customtable.permissiondenied." + permissionName), CustomTableClassInfo.ClassName);
            return false;
        }
        return true;
    }


    private ArrayList GetExistingColumns()
    {
        ArrayList existingColumns = new ArrayList();
        string[] cols = FormInfo.GetColumnNames();
        foreach (string col in cols)
        {
            existingColumns.Add(col.ToLower());
        }
        return existingColumns;
    }


    private static ArrayList GetSelectedColumns(string columns)
    {
        ArrayList cols = new ArrayList();
        cols.AddRange(columns.ToLower().Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
        return cols;
    }
}
