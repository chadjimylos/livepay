using System;
using System.Data;
using System.Web;
using System.Web.UI;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.LicenseProvider;
using CMS.SiteProvider;
using CMS.SettingsProvider;

public partial class CMSModules_Reporting_Tools_Ecommerce_Reports : CMSEcommercePage
{
    private bool isSaved = false;

    public string mSave = "";
    public string mPrint = "";
    public string reportName = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check the license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), "") != "")
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Reporting);
        }

        // Check site availability
        if (!ResourceSiteInfoProvider.IsResourceOnSite("CMS.Reporting", CMSContext.CurrentSiteName))
        {
            RedirectToResourceNotAvailableOnSite("CMS.Reporting");
        }

        CurrentUserInfo user = CMSContext.CurrentUser;

        // Check 'Read' permission
        if (!user.IsAuthorizedPerResource("CMS.Reporting", "Read"))
        {
            RedirectToAccessDenied("CMS.Reporting", "Read");
        }


        imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        imgPrint.ImageUrl = GetImageUrl("General/print.png");


        mSave = ResHelper.GetString("general.save");
        mPrint = ResHelper.GetString("Ecommerce_Report.Print");

        ltlModal.Text = ScriptHelper.GetScript("function myModalDialog(url, name, width, height) { " +
            "win = window; " +
            "var dHeight = height; var dWidth = width; " +
            "if (( document.all )&&(navigator.appName != 'Opera')) { " +
            "try { win = wopener.window; } catch (e) {} " +
            "if ( parseInt(navigator.appVersion.substr(22, 1)) < 7 ) { dWidth += 4; dHeight += 58; }; " +
            "dialog = win.showModalDialog(url, this, 'dialogWidth:' + dWidth + 'px;dialogHeight:' + dHeight + 'px;resizable:yes;scroll:yes'); " +
            "} else { " +
            "oWindow = win.open(url, name, 'height=' + dHeight + ',width=' + dWidth + ',toolbar=no,directories=no,menubar=no,modal=yes,dependent=yes,resizable=yes,scroll=yes,scrollbars=yes'); oWindow.opener = this; oWindow.focus(); } } ");
    }


    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        reportName = ValidationHelper.GetString(Request.QueryString["reportCodeName"], "");

        ucDisplayReport.DisplayFilter = true;
        ucDisplayReport.ReportName = reportName;
        ucDisplayReport.LoadFormParameters = true;
    }


    /// <summary>
    /// On PreRender override
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.lnkPrint.OnClientClick = "myModalDialog('Reports_Print.aspx?reportname=" + reportName + "&parameters=" + HttpUtility.HtmlEncode(GetQueryStringParameters()) + "&UILang=" + System.Globalization.CultureInfo.CurrentUICulture.IetfLanguageTag + "', 'Print report" + ucDisplayReport.ReportName + "', 800, 700); return false;";
    }


    /// <summary>
    /// VerifyRenderingInServerForm
    /// </summary>
    /// <param name="control">Control</param>
    public override void VerifyRenderingInServerForm(Control control)
    {
        if (!isSaved)
        {
            base.VerifyRenderingInServerForm(control);
        }
    }


    /// <summary>
    /// Handles lnkSave click event.
    /// </summary>
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        /*
                // Check e-commerce save permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "SaveReports"))
                {
                    RedirectToAccessDenied("CMS.Ecommerce", "SaveReports");
                }
        */
        isSaved = true;

        if (ucDisplayReport.SaveReport() > 0)
        {
            lblInfo.Visible = true;
            lblInfo.Text = String.Format(ResHelper.GetString("Ecommerce_Report.ReportSavedTo"), ucDisplayReport.ReportInfo.ReportDisplayName + " - " + DateTime.Now.ToString());
        }

        isSaved = false;
    }


    /// <summary>
    /// Generates query string parameters.
    /// </summary>
    protected string GetQueryStringParameters()
    {
        string result = "";
        IFormatProvider culture = DateTimeHelper.DefaultIFormatProvider;

        if (this.ucDisplayReport.ReportParameters != null)
        {
            // Build the results array
            foreach (DataColumn col in this.ucDisplayReport.ReportParameters.Table.Columns)
            {
                if ((col.DataType.Name.ToLower() == "datetime") && ((ValidationHelper.GetDateTime(this.ucDisplayReport.ReportParameters[col.ColumnName], DataHelper.DATETIME_NOT_SELECTED)) != DataHelper.DATETIME_NOT_SELECTED))
                {
                    result += col.ColumnName + ";" + ((DateTime)this.ucDisplayReport.ReportParameters[col.ColumnName]).ToString(culture) + ";";
                }
                else
                {
                    result += col.ColumnName + ";" + Convert.ToString(this.ucDisplayReport.ReportParameters[col.ColumnName]) + ";";
                }
            }

            // Removes last semicolon (we can't use trimend because of situation when last parametr doesn't have value)
            if (result.EndsWith(";"))
            {
                result = result.Substring(0, result.Length - 1);
            }
        }

        return result;
    }
}
