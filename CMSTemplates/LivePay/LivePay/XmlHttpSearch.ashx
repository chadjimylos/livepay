﻿<%@ WebHandler Language="VB" Class="XmlHttpSearch" %>

Imports System
Imports System.Web
Imports CMS.SearchProviderSQL
Imports System.Data

Public Class XmlHttpSearch : Implements IHttpHandler
    
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim KeyWord As String = context.Request.QueryString("KeyWord")
        context.Response.ContentType = "text/plain"
            
        Dim SearchMthd As New CMS.SearchProviderSQL.SearchProvider
        Dim ds As DataSet
        ds = SearchMthd.Search("", "", "", "", CMS.ISearchEngine.SearchModeEnum.AllWords, False, "", False, False, "", "", False)
        Dim dt As DataTable
        dt = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            context.Response.Write(dt.Rows(i)(0).ToString & "<br>")
        Next
        
    End Sub
    
   
    
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class