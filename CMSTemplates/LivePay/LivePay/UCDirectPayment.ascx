﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCDirectPayment.ascx.vb" Inherits="CMSTemplates_LivePay_UCDirectPayment" %>
<script language="javascript" type="text/javascript" >

    var Tab1CloudCustCodeTop = 0;
    var Tab1CloudCustCodeLeft = 0;
    function GetAttributes() {

    }
    function ShowClound(obj, img) {
        var Object = document.getElementById(obj)
        Object.style.display = ''
        if (Tab1CloudCustCodeTop == 0)
            Tab1CloudCustCodeTop = Object.offsetTop
        if (Tab1CloudCustCodeLeft == 0)
            Tab1CloudCustCodeLeft = Object.offsetLeft

        var ExtraPx = 0;
        if (document.all) {
            ExtraPx = 10
        } else {
            ExtraPx = -10
        }

        Object.style.top = (Tab1CloudCustCodeTop - Object.offsetHeight + ExtraPx) + 'px'
        Object.style.left = (Tab1CloudCustCodeLeft - ExtraPx) + 'px'

    }

    function HideClound(obj) {
        var Object = document.getElementById(obj)
        Object.style.top = Tab1CloudCustCodeTop + 'px'
        Object.style.left = Tab1CloudCustCodeLeft + 'px'
        Object.style.display = 'none'
        Tab1CloudCustCodeTop = 0;
        Tab1CloudCustCodeLeft = 0;

    }

    function ChangeRadioChoice(ObjToCheck, ObjToUncheck) {
        document.getElementById(ObjToCheck).checked = true;
        document.getElementById(ObjToUncheck).checked = false;
    }


    function validateControls(oSrc, args) {
        var ControlToValidate = '';
        if (document.all) {
            ControlToValidate = oSrc.getAttribute('ControlToValidate')

        } else {
            ControlToValidate = oSrc.getAttribute('ControlValidated')
        }
        var MyVal = args.Value
        var bIsValid = true

        if (MyVal.length == 0) {
            bIsValid = false
        }

        args.IsValid = bIsValid;
    }

</script>
 <div class="RegUserTopMain" style="height:10px">
         <img src="/App_Themes/LivePay/RegisteredUsers/RegUserTopBG.png" />
     </div> 
 <div style="background-color:#f1f1f1;width:669px;">
    <div id="Tab1" style="display:none">
        <div style="padding-left:10px">
            <div style="background-image:url('/App_Themes/LivePay/RegisteredUsers/Tab1BG.png');background-repeat:no-repeat;width:650px;height:305px">
                <div id="DivTabsTab1">
                    <div style="float:left;font-size:12px;font-weight:bold;color:white;font-family:Tahoma;padding-top:8px;width:218px;text-align:center"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 1: Στοιχεία Πληρωμής|en-us=ΒΗΜΑ 1: Στοιχεία Πληρωμής$}") %></div>
                    <div style="float:left;font-size:12px;font-weight:bold;color:white;font-family:Tahoma;padding-top:8px;width:213px;text-align:center" onclick="document.getElementById('Tab1').style.display='none';document.getElementById('Tab2').style.display=''"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 2: Στοιχεία Χρέωσης|en-us=ΒΗΜΑ 2: Στοιχεία Χρέωσης$}") %></div>
                    <div style="float:left;font-size:12px;font-weight:bold;color:white;font-family:Tahoma;padding-top:8px;width:213px;text-align:center"><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 3: Επιβεβαίωση|en-us=ΒΗΜΑ 3: Επιβεβαίωση$}")%></div>
                    <div class="Clear"></div>
                </div>
                <div id="DivFormTab1" style="padding-left:10px;padding-top:30px">
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;text-align:right;padding-top:17px"><%=ResHelper.LocalizeString("{$=Κωδικός Πελάτη|en-us=Κωδικός Πελάτη$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" id="Tab1_txtCustomerCode" runat="server" ></asp:TextBox>
                        <div style="display:none">
                            <asp:CustomValidator id="Tab1_Val_CustomerCode"  runat="server" ValidationGroup="Tab1_PayForm" ValidateEmptyText="true" ControlToValidate = "Tab1_txtCustomerCode" ClientValidationFunction="validateControls" ></asp:CustomValidator>
                        </div> 
                    </div>
                    <div style="float:left;padding-left:5px;padding-top:17px" onmouseover="ShowClound('CustCodeCloud',this)" onmouseout="HideClound('CustCodeCloud')"><img  src="/App_Themes/LivePay/InfoBtn.png" />
                        <div style="position:absolute;display:none;width:195px;height:63px" id="CustCodeCloud"><img src="/App_Themes/LivePay/RegisteredUsers/Tab1CloudCustCode.png" /></div>
                    </div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;text-align:right;padding-top:17px"><%=ResHelper.LocalizeString("{$=Αριθμός Λογαριασμού|en-us=Αριθμός Λογαριασμού$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" id="Tab1_txtAcountNo" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:RequiredFieldValidator ID="Tab1_ReqFldVal_AcountNo"  runat="server" ControlToValidate="Tab1_txtAcountNo" text="*" ValidationGroup="Tab1_PayForm"/></div> 
                    </div>
                    <div style="float:left;padding-left:5px;padding-top:17px" onmouseover="ShowClound('AcountNoCloud',this)" onmouseout="HideClound('AcountNoCloud')"><img  src="/App_Themes/LivePay/InfoBtn.png" />
                        <div style="position:absolute;display:none" id="AcountNoCloud"><img src="/App_Themes/LivePay/RegisteredUsers/Tab1CloudCustCode.png" /></div>
                    </div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;text-align:right;padding-top:17px"><%= ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}")%></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" id="Tab1_txtPrice" runat="server" ></asp:TextBox>
                    <div style="display:none">
                    <asp:RequiredFieldValidator ID="Tab1_ReqFldVal_Price" runat="server" ControlToValidate="Tab1_txtPrice" text="*" ValidationGroup="Tab1_PayForm"/>
                    <asp:RegularExpressionValidator ID="Tab1_ReqularFldVal_Price" runat="server" ControlToValidate="Tab1_txtPrice" ValidationExpression="^\$?([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])?$" ValidationGroup="Tab1_PayForm"></asp:RegularExpressionValidator></div>
                    </div> 
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;text-align:right;padding-top:17px"><%=ResHelper.LocalizeString("{$=Σημειώσεις|en-us=Σημειώσεις$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" Rows="4" TextMode="MultiLine" id="Tab1_txtNotes" runat="server" ></asp:TextBox></div>
                    <div class="Clear"></div>
                    <div style="float:left;width:140px;text-align:right;">&nbsp;</div>
                    <div class="RegUsersTab2_TxtDiv"><asp:ImageButton ID="Tab1_BtnNext" ValidationGroup="Tab1_PayForm" runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/BtnNext.png" /></div>
                    <div class="Clear"></div>
                    <asp:ValidationSummary  ID="Tab1_ValSumm" runat="server" DisplayMode="BulletList"  ShowMessageBox="True" ShowSummary="false" ErrorMessage="" ValidationGroup="Tab1_PayForm"/>
                </div>
            </div>
        </div>
    </div>
    <div id="Tab2" style="display:">
        <div style="padding-left:10px">
            <div style="background-image:url('/app_themes/livepay/Directpayments/tab2bg.png');background-repeat:no-repeat;width:650px;min-height:324px">
                <div id="DivTabsTab2" style="padding-bottom:10px">
                    <div style="float:left;font-size:12px;font-weight:bold;color:white;font-family:Tahoma;padding-top:8px;width:218px;text-align:center" onclick="document.getElementById('Tab2').style.display='none';document.getElementById('Tab3').style.display='none';document.getElementById('Tab1').style.display='';"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 1: Στοιχεία Πληρωμής|en-us=ΒΗΜΑ 1: Στοιχεία Πληρωμής$}") %></div>
                    <div style="float:left;font-size:12px;font-weight:bold;color:white;font-family:Tahoma;padding-top:8px;width:213px;text-align:center" onclick="document.getElementById('Tab1').style.display='none';document.getElementById('Tab3').style.display='none';document.getElementById('Tab2').style.display='';"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 2: Στοιχεία Χρέωσης|en-us=ΒΗΜΑ 2: Στοιχεία Χρέωσης$}") %></div>
                    <div style="float:left;font-size:12px;font-weight:bold;color:white;font-family:Tahoma;padding-top:8px;width:213px;text-align:center" onclick="document.getElementById('Tab1').style.display='none';document.getElementById('Tab2').style.display='none';document.getElementById('Tab3').style.display='';"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 3: Επιβεβαίωση|en-us=ΒΗΜΑ 3: Επιβεβαίωση$}") %></div>
                    <div class="Clear"></div>
                </div>
                <div id="DivFormTab2" style="padding-left:10px;padding-top:10px;background-color:White;padding-bottom:5px">
                     <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:17px"><%=ResHelper.LocalizeString("{$=Τύπος Κάρτας|en-us=Τύπος Κάρτας$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:DropDownList style="font-family:Tahoma;font-size:12px;color:#43474a" width="264px" id="Tab2_ddlCardType" runat="server" ></asp:DropDownList>
                        <div style="display:none"><asp:RequiredFieldValidator ID="Tab2_ReqFldVal_CardType"  runat="server" ControlToValidate="Tab2_ddlCardType" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div style="float:left;padding-left:5px;padding-top:17px" >(*)</div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:17px"><%=ResHelper.LocalizeString("{$=Αριθμός Κάρτας|en-us=Αριθμός Κάρτας$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" id="Tab2_txtCardNo" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:RequiredFieldValidator ID="Tab2_ReqFldVal_CardNo"  runat="server" ControlToValidate="Tab2_txtCardNo" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div style="float:left;padding-left:5px;padding-top:17px" >(*)</div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:12px"><%=ResHelper.LocalizeString("{$=Ονοματεπώνυμο Κατόχου|en-us=Ονοματεπώνυμο Κατόχου$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" id="Tab2_txtFullNameOwner" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:RequiredFieldValidator ID="Tab2_ReqFldVal_FullNameOwner"  runat="server" ControlToValidate="Tab2_txtFullNameOwner" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div style="float:left;padding-left:5px;padding-top:17px" >(*)</div>
                    <div class="Clear"></div>
                    
                    <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:12px"><%= ResHelper.LocalizeString("{$=Ημερομηνία<Br />Λήξης|en-us=Ημερομηνία<Br />Λήξης$}")%></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:DropDownList style="font-family:Tahoma;font-size:12px;color:#43474a" width="112px" id="Tab2_DdlCardMonth" runat="server" ></asp:DropDownList>
                        <div style="display:none"><asp:RequiredFieldValidator ID="Tab2_ReqFldVal_CardMonth"  runat="server" ControlToValidate="Tab2_DdlCardMonth" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div style="float:left;padding-left:5px;padding-top:17px" >(*)</div>
                    <div class="RegUsersTab2_TxtDiv"><asp:DropDownList style="font-family:Tahoma;font-size:12px;color:#43474a" width="112px" id="Tab2_DdlCardYear" runat="server" ></asp:DropDownList>
                        <div style="display:none"><asp:RequiredFieldValidator ID="Tab2_ReqFldVal_CardYear" runat="server" ControlToValidate="Tab2_DdlCardYear" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div style="float:left;padding-left:5px;padding-top:17px" >(*)</div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:12px">CVV2</div>
                    <div style="float:left;padding-left:15px;padding-top:10px"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="178px" id="Tab2_txtCVV2" runat="server" ></asp:TextBox>
                       
                       <div style="display:none"><asp:RequiredFieldValidator ID="Tab2_ReqFldVal_CVV2" ErrorMessage="CVV2" runat="server" ControlToValidate="Tab2_txtCVV2" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div style="float:left;padding-left:5px;padding-top:12px" >(*)</div>
                    <div style="float:left;padding-left:5px;padding-top:12px" onmouseover="ShowClound('CardCVV2',this)" onmouseout="HideClound('CardCVV2')" ><img src="/app_themes/livepay/InfoBtn.png"  />
                        <div style="position:absolute;display:none;width:284px;height:266px" id="CardCVV2"><img src="/App_Themes/LivePay/Directpayments/CVV2Help.png" /></div>
                    </div>
                    
                    <div class="Clear"></div>
                   
                </div> 
                <div style="border-top:2px solid #f1f1f1;background-color:White;padding-left:10px;padding-bottom:10px">
                    <div style="padding-top:5px;">
                        <%=ResHelper.LocalizeString("{$=Σε περίπτωση που θέλετε να παραλάβετε το αποδεικτικό της συναλλαγής, συμπληρώστε το email σας|en-us=Σε περίπτωση που θέλετε να παραλάβετε το αποδεικτικό της συναλλαγής, συμπληρώστε το email σας$}") %>
                    </div>
                    <div class="RegUsersTab2_PhoneDiv" id="Tab2_PhoneDiv">
                        <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:17px"><%=ResHelper.LocalizeString("{$=Τηλέφωνο|en-us=Τηλέφωνο$}") %></div>
                        <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" id="Tab2_txtPhone" runat="server" ></asp:TextBox></div>
                        <div class="Clear"></div>
                    </div>
                    <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:17px"><%=ResHelper.LocalizeString("{$=E-mail|en-us=E-mail$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" id="Tab2_txtEmail" runat="server" ></asp:TextBox></div>
                    <div class="Clear"></div>
                    <div class="RegUsersTab2_BtnNextDiv" id="Tab2_BtnNextDiv">
                        <div style="float:left;width:128px;">&nbsp;</div>
                        <div style="float:left;"><asp:ImageButton ID="Tab2_btnNext" ValidationGroup="Tab2_PayForm" runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/BtnNext.png" /></div>
                        <div style="float:left;padding-left:10px"><asp:ImageButton ID="Tab2_btnReturn"  runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/btnReturn.png" /></div>
                        <div class="Clear"></div>
                    </div>
                    <div style="color:#43474a;font-size:11px;padding-top:14px;padding-top:expression(8)">
                        <%=ResHelper.LocalizeString("{$=Τα πεδία με τον αστερίσκο (*) είναι υποχρεωτικά|en-us=Τα πεδία με τον αστερίσκο (*) είναι υποχρεωτικά$}") %>
                    </div>
                    <div>
                        <asp:CheckBox ID="Tab2_ChlIWantInvoice" onclick="ShowHideInvoice(this)" style="color:#43474a;font-size:11px;" runat="server"/>
                    </div> 
                </div>
                <div id="InvoiceDiv" style="padding-left:10px;background-color:White;padding-bottom:5px;display:none">
                    <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:17px"><%=ResHelper.LocalizeString("{$=Επωνυμία Επιχείρησης|en-us=Επωνυμία Επιχείρησης$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" id="TextBox1" runat="server" ></asp:TextBox>
                    </div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:12px"><%=ResHelper.LocalizeString("{$=Επάγγελμα|en-us=Επάγγελμα$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" id="TextBox2" runat="server" ></asp:TextBox>
                    </div>
                    <div class="Clear"></div>

                    <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:12px"><%=ResHelper.LocalizeString("{$=Διεύθυνση|en-us=Διεύθυνση$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="260px" id="TextBox3" runat="server" ></asp:TextBox>
                    </div>
                    <div class="Clear"></div>

                    <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:12px"><%=ResHelper.LocalizeString("{$=Τ.Κ.|en-us=Τ.Κ.$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="60px" id="TextBox5" runat="server" ></asp:TextBox>
                    </div>
                    <div style="float:left;color:#43474a;font-size:12px;width:58px;padding-top:12px;text-align:right"><%=ResHelper.LocalizeString("{$=Πόλη|en-us=Πόλη$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="122px" id="TextBox6" runat="server" ></asp:TextBox>
                    </div>
                    <div class="Clear"></div>

                    <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:12px"><%=ResHelper.LocalizeString("{$=ΑΦΜ|en-us=ΑΦΜ$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" width="200px" id="TextBox4" runat="server" ></asp:TextBox>
                    </div>
                    <div class="Clear"></div>
                     <div style="float:left;color:#43474a;font-size:12px;width:128px;text-align:right;padding-top:12px"><%=ResHelper.LocalizeString("{$=ΔΟΥ|en-us=ΔΟΥ$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:DropDownList ID="ddlDoy" runat="server" ></asp:DropDownList>
                    </div>
                    <div class="Clear"></div>
                     <div class="RegUsersTab2_BtnNextDiv">
                        <div style="float:left;width:128px;">&nbsp;</div>
                        <div style="float:left;"><asp:ImageButton ID="ImageButton1" ValidationGroup="Tab2_PayForm" runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/BtnNext.png" /></div>
                        <div style="float:left;padding-left:10px"><asp:ImageButton ID="ImageButton2"  runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/btnReturn.png" /></div>
                        <div class="Clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="Tab3" style="display:none">
        <div style="padding-left:10px">
            <div style="background-image:url('/App_Themes/LivePay/RegisteredUsers/Tab3BG.png');background-repeat:no-repeat;width:650px;height:365px">
                <div id="DivTabsTab3">
                    <div style="float:left;font-size:12px;font-weight:bold;color:white;font-family:Tahoma;padding-top:8px;width:218px;text-align:center"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 1: Στοιχεία Πληρωμής|en-us=ΒΗΜΑ 1: Στοιχεία Πληρωμής$}") %></div>
                    <div style="float:left;font-size:12px;font-weight:bold;color:white;font-family:Tahoma;padding-top:8px;width:213px;text-align:center" onclick="document.getElementById('Tab1').style.display='none';document.getElementById('Tab2').style.display='';document.getElementById('Tab3').style.display='none';"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 2: Στοιχεία Χρέωσης|en-us=ΒΗΜΑ 2: Στοιχεία Χρέωσης$}") %></div>
                    <div style="float:left;font-size:12px;font-weight:bold;color:white;font-family:Tahoma;padding-top:8px;width:213px;text-align:center"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 3: Επιβεβαίωση|en-us=ΒΗΜΑ 3: Επιβεβαίωση$}") %></div>
                    <div class="Clear"></div>
                </div>
                <div id="DivFormTab3" style="padding-left:10px;padding-top:20px">
                    <div style="font-size:11px;font-family:Tahoma;color:#43474a"><%=ResHelper.LocalizeString("{$=Διαβάστε προσεχτικά τα παρακάτω και πατήστε το κουμπί <b>Ολοκλήρωση</b> προκειμένου να πραγματοποιήσετε την Πληρωμή|en-us=Διαβάστε προσεχτικά τα παρακάτω και πατήστε το κουμπί <b>Ολοκλήρωση</b> προκειμένου να πραγματοποιήσετε την Πληρωμή$}") %></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;text-align:right;padding-top:24px"><%=ResHelper.LocalizeString("{$=Πληρωμή προς|en-us=Πληρωμή προς$}") %></div>
                    <div style="float:left;padding-left:15px;padding-top:17px"><asp:Label ID="Tab3_lblPayTo" runat="server" style="color:#43474a;font-size:12px;;font-weight:bold;font-family:Tahoma" ></asp:Label></div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;text-align:right;padding-top:15px"><%=ResHelper.LocalizeString("{$=Κωδικός Πελάτη|en-us=Κωδικός Πελάτη$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:Label ID="Tab3_lblCustCode" runat="server" style="color:#43474a;font-size:12px;;font-weight:bold;font-family:Tahoma" ></asp:Label></div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;text-align:right;padding-top:15px"><%=ResHelper.LocalizeString("{$=Αριθμός Λογαριασμού|en-us=Αριθμός Λογαριασμού$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:Label ID="Tab3_lblAcountNo" runat="server" style="color:#43474a;font-size:12px;;font-weight:bold;font-family:Tahoma" ></asp:Label></div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;text-align:right;padding-top:15px"><%=ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:Label ID="Tab3_lblPrice" runat="server" style="color:#43474a;font-size:12px;;font-weight:bold;font-family:Tahoma" ></asp:Label></div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;text-align:right;padding-top:15px"><%=ResHelper.LocalizeString("{$=Κάρτα|en-us=Κάρτα$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:Label ID="Tab3_lblCard" runat="server" style="color:#43474a;font-size:12px;;font-weight:bold;font-family:Tahoma" ></asp:Label></div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;text-align:right;padding-top:15px"><%=ResHelper.LocalizeString("{$=Οροι<br />Χρήσης|en-us=Οροι<br />Χρήσης$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox style="border:1px solid #3b6db4;font-family:Tahoma;font-size:12px;color:#43474a" TextMode="MultiLine" Rows="4" width="260px" id="Tab3_lblTerms" runat="server" ></asp:TextBox></div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;">&nbsp;</div>
                    <div style="float:left;padding-left:11px;"><asp:CheckBox ID="Tab3_chkTermsAccept" style="color:#43474a;font-size:11px;" runat="server" /></div>
                    <div class="Clear"></div>
                     <div style="padding-top:12px;">
                        <div style="float:left;width:155px;">&nbsp;</div>
                        <div style="float:left;"><asp:ImageButton ID="Tab3_btnComplete" runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/btnComplete.png" /></div>
                        <div style="float:left;padding-left:10px"><asp:ImageButton ID="Tab3_btnReturn"  runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/btnReturn.png" /></div>
                        <div class="Clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div class="RegUserBotMain" style="height:10px">
         <img src="/App_Themes/LivePay/RegisteredUsers/RegUserBotBG.png" />
     </div> 
 </div>
  <script language="javascript" type="text/javascript" >
      GetAttributes()
      FixChromeHeight()
      function FixChromeHeight() {
          var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
          if (is_chrome) {
              document.getElementById('Tab2_PhoneDiv').className = 'RegUsersTab2_PhoneDivChrome'
              document.getElementById('Tab2_BtnNextDiv').className = 'RegUsersTab2_BtnNextDivChrome'
          }

      }

      function ShowHideInvoice(chb) {
          if (chb.checked) {
              document.getElementById('InvoiceDiv').style.display = ''
              document.getElementById('Tab2_BtnNextDiv').style.display = 'none'
          } else {
              document.getElementById('InvoiceDiv').style.display = 'none'
              document.getElementById('Tab2_BtnNextDiv').style.display = ''
          }
         
      } 
  </script>