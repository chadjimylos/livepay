CREATE VIEW View_Messaging_ContactList
AS
SELECT     CMS_UserSettings.UserNickName, CMS_User.UserName, Messaging_ContactList.ContactListUserID, 
                      Messaging_ContactList.ContactListContactUserID
FROM         CMS_UserSettings INNER JOIN
                      CMS_User ON CMS_UserSettings.UserSettingsUserID = CMS_User.UserID INNER JOIN
                      Messaging_ContactList ON Messaging_ContactList.ContactListContactUserID = CMS_User.UserID
