<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RoleList.ascx.cs" 
    Inherits="CMSSiteManager_Administration_Roles_Controls_RoleList" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
    
<cms:UniGrid runat="server" ID="gridElem" GridName="~/CMSSiteManager/Administration/Roles/Controls/Role_List.xml" OrderBy="RoleDisplayName" />
