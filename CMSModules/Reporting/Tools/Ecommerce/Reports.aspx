<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Reports.aspx.cs" Inherits="CMSModules_Reporting_Tools_Ecommerce_Reports"
    Theme="Default" %>

<%@ Register Src="~/CMSModules/Reporting/Controls/ReportTable.ascx" TagName="ReportTable"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Reporting/Controls/DisplayReport.ascx" TagName="DisplayReport"
    TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>E-commerce - Reports</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height:100%;
        }
    </style>
</head>
<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="pnlBody">
            <asp:Panel runat="server" ID="pnlMenu" CssClass="ContentEditMenu">
                <table width="100%">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkSave" runat="server" OnClick="lnkSave_Click" CssClass="MenuItemEdit" EnableViewState="false">
                                            <asp:Image ID="imgSave" runat="server" EnableViewState="false"/>
                                            <%=mSave%>
                                        </asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkPrint" runat="server" CssClass="MenuItemEdit" EnableViewState="false">
                                            <asp:Image ID="imgPrint" runat="server" EnableViewState="false" />
                                            <%=mPrint%>
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="TextRight">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlContent" CssClass="PageContent">
                <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
                    Visible="false" />
                <asp:Label Style="padding: 10px 0 0 20px;" ID="lblError" runat="server" EnableViewState="False"
                    Visible="False" CssClass="ErrorLabel">
                </asp:Label>
                <cms:DisplayReport ID="ucDisplayReport" runat="server" BodyCssClass="ReportBodyEcommerce"
                    FormCssClass="ReportFilter" IsLiveSite="false" RenderCssClasses="true" />
                <asp:Literal runat="server" ID="ltlModal" EnableViewState="false" />
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
