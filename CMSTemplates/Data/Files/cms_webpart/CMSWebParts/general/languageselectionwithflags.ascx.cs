using System;
using System.Data;
using System.IO;
using System.Web.Caching;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;

public partial class CMSWebParts_General_languageselectionwithflags : CMSAbstractWebPart
{
    private string mLayoutSeparator = " ";
    private string imgFlagIcon = "";
    public string selectionClass = "";

    #region "Public properties"

    /// <summary>
    /// Gets or sets the display layout
    /// </summary>
    public string DisplayLayout
    {
        get
        {
            return ValidationHelper.GetString(GetValue("DisplayLayout"), "");
        }
        set
        {
            SetValue("DisplayLayout", value);
            mLayoutSeparator = value.ToLower() == "vertical" ? "<br />" : " ";
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether the link for current culture should be hidden
    /// </summary>
    public bool HideCurrentCulture
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("HideCurrentCulture"), false);
        }
        set
        {
            SetValue("HideCurrentCulture", value);
        }
    }


    /// <summary>
    /// Gets or sets the value than indicates whether culture names are displayed
    /// </summary>
    public bool ShowCultureNames
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("ShowCultureNames"), true);
        }
        set
        {
            SetValue("ShowCultureNames", value);
        }
    }


    /// <summary>
    /// Gets or sets the separator between items
    /// </summary>
    public string Separator
    {
        get
        {
            return ValidationHelper.GetString(GetValue("Separator"), "");
        }
        set
        {
            SetValue("Separator", value);
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data for partial caching
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            mLayoutSeparator = DisplayLayout.ToLower() == "vertical" ? "<br />" : " ";

            DataSet ds = null;

            // Try to get data from cache
            using (CachedSection<DataSet> cs = new CachedSection<DataSet>(ref ds, this.CacheMinutes, true, this.CacheItemName, "languageselection", CMSContext.CurrentSiteName))
            {
                if (cs.LoadData)
                {
                    // Get the data
                    ds = CultureInfoProvider.GetSiteCultures(CMSContext.CurrentSiteName);

                    // Save to the cache
                    if (cs.Cached)
                    {
                        cs.CacheDependency = CacheHelper.GetCacheDependency(new string[] { "cms.culturesite|all" });
                        cs.Data = ds;
                    }
                }
            }

            if (!DataHelper.DataSourceIsEmpty(ds) && (ds.Tables[0].Rows.Count > 1))
            {
                // Build current URL
                string url = UrlHelper.CurrentURL;
                url = UrlHelper.RemoveParameterFromUrl(url, UrlHelper.LanguageParameterName);
                url = UrlHelper.RemoveParameterFromUrl(url, UrlHelper.AliasPathParameterName);

                // Render the cultures
                ltlHyperlinks.Text = "";

                int count = 0;
                int rows = ds.Tables[0].Rows.Count;

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string cultureCode = dr["CultureCode"].ToString();
                    string cultureShortName = dr["CultureShortName"].ToString();
                    if (!((HideCurrentCulture) && (String.Compare(CMSContext.CurrentDocument.DocumentCulture, cultureCode, true) == 0)))
                    {
                        // Get flag icon URL
                        imgFlagIcon = UIHelper.GetFlagIconUrl(this.Page, cultureCode, "16x16");

                        if (ShowCultureNames)
                        {
                            // Add flag icon before the link text
                            ltlHyperlinks.Text += "<img src=\"" + imgFlagIcon + "\" alt=\"" + HTMLHelper.HTMLEncode(cultureShortName) + "\" />";
                            ltlHyperlinks.Text += "<a href=\"" + HTMLHelper.HTMLEncode(UrlHelper.AddParameterToUrl(url, UrlHelper.LanguageParameterName, cultureCode)) + "\">";
                            ltlHyperlinks.Text += HTMLHelper.HTMLEncode(cultureShortName);

                            // Set surrounding div css class
                            selectionClass = "languageSelectionWithCultures";
                        }
                        else
                        {
                            ltlHyperlinks.Text += "<a href=\"" + UrlHelper.AddParameterToUrl(url, UrlHelper.LanguageParameterName, cultureCode) + "\">" + "<img src=\"" + imgFlagIcon + "\" alt=\"" + HTMLHelper.HTMLEncode(cultureShortName) + "\" />";

                            // Set surrounding div css class
                            selectionClass = "languageSelection";
                        }

                        count++;

                        // Check last item
                        if (count == rows)
                        {
                            ltlHyperlinks.Text += "</a>";
                        }
                        else
                        {
                            ltlHyperlinks.Text += "</a>" + Separator + mLayoutSeparator;
                        }
                    }
                }
            }
            else
            {
                // Hide if less than two cultures
                Visible = false;
            }

            if (string.IsNullOrEmpty(selectionClass))
            {
                ltrDivOpen.Text = "<div>";
            }
            else
            {
                ltrDivOpen.Text = "<div class=\"" + selectionClass + "\">";
            }
            ltrDivClose.Text = "</div>";

            // Check if RTL hack must be applied
            if (CultureHelper.IsPreferredCultureRTL())
            {
                ltrDivOpen.Text += "<span style=\"visibility:hidden;\">a</span>";
            }
        }
    }


    /// <summary>
    /// Clears the cached items
    /// </summary>
    public override void ClearCache()
    {
        string useCacheItemName = DataHelper.GetNotEmpty(CacheItemName, CMSContext.BaseCacheKey + "|" + Context.Request.Url + "|" + ClientID);

        CacheHelper.ClearCache(useCacheItemName);
    }
}
