CREATE TABLE [Staging_SyncLog] (
		[SyncLogID]           int NOT NULL IDENTITY(1, 1),
		[SyncLogTaskID]       int NOT NULL,
		[SyncLogServerID]     int NOT NULL,
		[SyncLogTime]         datetime NOT NULL,
		[SyncLogError]        nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [Staging_SyncLog]
	ADD
	CONSTRAINT [PK_Staging_SyncLog]
	PRIMARY KEY
	([SyncLogID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Staging_SyncLog]') IS NOT NULL
	AND OBJECT_ID(N'[Staging_Server]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Staging_SyncLog_SyncLogServerID_Staging_Server]') IS NULL
BEGIN
		ALTER TABLE [Staging_SyncLog]
			ADD CONSTRAINT [FK_Staging_SyncLog_SyncLogServerID_Staging_Server]
			FOREIGN KEY ([SyncLogServerID]) REFERENCES [Staging_Server] ([ServerID])
END
IF OBJECT_ID(N'[Staging_SyncLog]') IS NOT NULL
	AND OBJECT_ID(N'[Staging_Task]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Staging_SyncLog_SyncLogTaskID_Staging_Task]') IS NULL
BEGIN
		ALTER TABLE [Staging_SyncLog]
			ADD CONSTRAINT [FK_Staging_SyncLog_SyncLogTaskID_Staging_Task]
			FOREIGN KEY ([SyncLogTaskID]) REFERENCES [Staging_Task] ([TaskID])
END
CREATE INDEX [IX_Staging_SyncLog_SyncLogServerID]
	ON [Staging_SyncLog] ([SyncLogServerID])
	ON [PRIMARY]
CREATE INDEX [IX_Staging_SyncLog_SyncLogTaskID]
	ON [Staging_SyncLog] ([SyncLogTaskID])
	ON [PRIMARY]
