<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Order_List.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Orders_Order_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Order list" %>

<%@ Register Src="~/CMSModules/Ecommerce/Controls/UI/OrderList.ascx" TagName="OrderList"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <div class="Orders">
        <cms:OrderList ID="orderListElem" runat="server" />
    </div>
</asp:Content>
