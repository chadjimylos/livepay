<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocumentAttachments.ascx.cs" Inherits="CMSInlineControls_DocumentAttachments" %>
<%@ Register Src="~/CMSModules/Content/Controls/Attachments/DocumentAttachments/DocumentAttachments.ascx" TagName="DocumentAttachments"
    TagPrefix="cms" %>
<div>
    <cms:DocumentAttachments ID="ucAttachments" runat="server" />
</div>
