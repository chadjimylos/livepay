﻿Imports System.Data

Partial Class CMSTemplates_LivePay_xmlhttp
    Inherits System.Web.UI.Page

    Protected ReadOnly Property MerchantID() As Integer
        Get
            Dim iMerchantID As Integer = 1
            If Not String.IsNullOrEmpty(Request("MerchantID")) Then
                iMerchantID = Request("MerchantID")
            End If
            Return iMerchantID
        End Get
    End Property

    Protected ReadOnly Property UserCardID() As Integer
        Get
            Dim iUserCardID As Integer = 1
            If Not String.IsNullOrEmpty(Request("UserCardID")) Then
                iUserCardID = Request("UserCardID")
            End If
            Return iUserCardID
        End Get
    End Property

    Protected ReadOnly Property CardNo() As String
        Get
            Dim _CardNo As String = String.Empty
            If Not String.IsNullOrEmpty(Request("CardNo")) Then
                _CardNo = Request("CardNo")
            End If
            Return _CardNo
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = DBConnection.GetMerchantTransaction(UserID, Me.MerchantID, Me.UserCardID, Me.CardNo, 2)
        If dt.Rows.Count > 0 Then
            Response.Write(String.Concat("[", dt.Rows(0)("TransCountPerMerchantPerCard").ToString, "],", "[", dt.Rows(0)("TransCountPerMerchant").ToString, "],", "[", dt.Rows(0)("TransSumAmountPerCardPerMerchant").ToString, "],", "[", dt.Rows(0)("UserCardType").ToString, "],[", IsValidCard(dt.Rows(0)("CardNo").ToString), "],[", dt.Rows(0)("CardNoIsSaved").ToString, "]"))
        End If
    End Sub

    Public Shared Function IsValidCard(ByVal number As String) As Boolean
        Dim Digits() As Integer = Array.ConvertAll(number.ToCharArray(), New Converter(Of Char, Integer)(AddressOf CharToDigit))
        Array.Reverse(Digits)

        Dim Sum As Integer = 0
        Dim Flag As Boolean

        For Each digit As Integer In Digits
            If Flag Then
                digit *= 2
                If digit > 9 Then digit -= 9
            End If

            Sum += digit
            Flag = Not Flag
        Next

        Return (Sum Mod 10 = 0)
    End Function

    Private Shared Function CharToDigit(ByVal ch As Char) As Integer
        Return Convert.ToInt32(ch.ToString())
    End Function


End Class
