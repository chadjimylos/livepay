using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.URLRewritingEngine;
using CMS.Forums;

public partial class CMSWebParts_Forums_Search_ForumSearch : CMSAbstractWebPart
{
    #region "Properties"

    /// <summary>
    /// Gets or sets the path to the advance search page path
    /// </summary>
    public string AdvancedSearchPath
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("AdvancedSearchPath"), "");
        }
        set
        {
            this.SetValue("AdvancedSearchPath", value);
        }
    }


    /// <summary>
    /// Gets or sets the url where is the search result web part
    /// </summary>
    public string RedirectUrl
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("RedirectUrl"), "");
        }
        set
        {
            this.SetValue("RedirectUrl", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether search should be performed only in current context
    /// </summary>
    public bool SearchInCurrentContext
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("SearchInCurrentContext"), false);
        }
        set
        {
            this.SetValue("SearchInCurrentContext", value);
        }
    }
	

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads the control data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        // WAI validation
        lblSearch.ResourceString = "ForumSearch.SearchWord";
        lblSearch.Attributes.Add("style", "display: none;");

        btnGo.Text = ResHelper.GetString("ForumSearch.Go");
        if (!String.IsNullOrEmpty(this.AdvancedSearchPath))
        {
            
           lnkAdvanceSearch.NavigateUrl = ResolveUrl(TreePathUtils.GetUrl(this.AdvancedSearchPath));
           lnkAdvanceSearch.Visible = true;
           lnkAdvanceSearch.Text = ResHelper.GetString("ForumSearch.AdvanceSearch");
           if (!RequestHelper.IsPostBack())
           {
               txtSearch.Text = QueryHelper.GetString("searchtext", txtSearch.Text);
           }
        }
    }


    /// <summary>
    /// OnGo search click
    /// </summary>
    protected void btnGo_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(txtSearch.Text))
        {
            string contextQuery = String.Empty;
            
            if (this.SearchInCurrentContext)
            {
                if ((ForumContext.CurrentForum != null)&&(ForumContext.CurrentThread != null)&&(ForumContext.CurrentThread.PostForumID == ForumContext.CurrentForum.ForumID))
                {
                    contextQuery = "&searchforums=" + ForumContext.CurrentForum.ForumID + "&searchthread=" + ForumContext.CurrentThread.PostId;
                }
                else if (ForumContext.CurrentForum != null)
                {
                    contextQuery = "&searchforums=" + ForumContext.CurrentForum.ForumID;
                }
            }

            if (!String.IsNullOrEmpty(RedirectUrl.Trim()))
            {
                UrlHelper.Redirect(ResolveUrl(RedirectUrl) + "?searchtext=" + HttpUtility.UrlEncode(txtSearch.Text) + contextQuery);
            }
            else //Redirect back to current page
            {
                string url = UrlHelper.RemoveQuery(URLRewriter.CurrentURL);
                url = UrlHelper.UpdateParameterInUrl(url, "searchtext", HttpUtility.UrlEncode(txtSearch.Text));
                url = UrlHelper.RemoveParameterFromUrl(url, "searchforums");
                url = UrlHelper.RemoveParameterFromUrl(url, "searchthread");
                url += contextQuery;

                UrlHelper.Redirect(url);
            }
        }
    }
}
