CREATE TABLE [COM_TaxClass] (
		[TaxClassID]                   int NOT NULL IDENTITY(1, 1),
		[TaxClassName]                 nvarchar(200) NOT NULL,
		[TaxClassDisplayName]          nvarchar(200) NOT NULL,
		[TaxClassZeroIfIDSupplied]     bit NULL,
		[TaxClassGUID]                 uniqueidentifier NOT NULL,
		[TaxClassLastModified]         datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_TaxClass]
	ADD
	CONSTRAINT [PK_COM_TaxClass]
	PRIMARY KEY
	NONCLUSTERED
	([TaxClassID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_TaxClass_TaxClassDisplayName]
	ON [COM_TaxClass] ([TaxClassDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
