using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Newsletter;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using System.Collections.Generic;
using CMS.SettingsProvider;

public partial class CMSModules_Newsletters_Tools_Subscribers_Subscriber_Subscriptions : CMSNewsletterPage
{
    protected int subscriberId = 0;
    protected string currentValues = string.Empty;
    protected Subscriber sb = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        chkSendConfirmation.Text = ResHelper.GetString("Subscriber_Edit.SendConfirmation");
        lblSelected.Text = ResHelper.GetString("newsletter.subscriptions.selected");
        subscriberId = QueryHelper.GetInteger("subscriberid", 0);

        sb = SubscriberProvider.GetSubscriber(subscriberId);
        if (sb != null)
        {
            // Get selected newsletters
            DataSet ds = SubscriberNewsletterInfoProvider.GetSubscriberNewsletters(sb.SubscriberID, null, -1, "NewsletterID");
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "NewsletterID"));
            }

            if (!IsPostBack)
            {
                usNewsletters.Value = currentValues;
                chkSendConfirmation.Checked = false;
            }
        }

        usNewsletters.OnSelectionChanged += usSites_OnSelectionChanged;
        usNewsletters.WhereCondition = "NewsletterSiteID = " + sb.SubscriberSiteID;
        usNewsletters.ButtonRemoveSelected.CssClass = "XLongButton";
        usNewsletters.ButtonAddItems.CssClass = "XLongButton";
    }


    void usSites_OnSelectionChanged(object sender, EventArgs e)
    {
        // Check newsletter configure permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "configure"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "configure");
        }

        SaveNewsletters();
    }


    /// <summary>
    /// Save changes.
    /// </summary>
    protected void SaveNewsletters()
    {
        // Remove old items
        string newValues = ValidationHelper.GetString(usNewsletters.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                foreach (string item in newItems)
                {
                    int newsletterId = ValidationHelper.GetInteger(item, 0);

                    // If subscriber is subscribed, unsubscribe him
                    if (SubscriberProvider.IsSubscribed(subscriberId, newsletterId))
                    {
                        try
                        {
                            SubscriberProvider.Unsubscribe(sb.SubscriberGUID, newsletterId, chkSendConfirmation.Checked);
                        }
                        catch { };
                    }
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                foreach (string item in newItems)
                {
                    int newsletterId = ValidationHelper.GetInteger(item, 0);

                    // If subscriber is not subscribed, subscribe him
                    if (!SubscriberProvider.IsSubscribed(subscriberId, newsletterId))
                    {
                        try
                        {
                            SubscriberProvider.Subscribe(subscriberId, newsletterId, DateTime.Now, chkSendConfirmation.Checked);
                        }
                        catch { };
                    }
                }
            }
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}