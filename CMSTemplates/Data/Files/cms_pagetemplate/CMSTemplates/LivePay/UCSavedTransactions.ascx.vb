﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Partial Class CMSTemplates_LivePay_UCSavedTransactions
    Inherits CMSUserControl

#Region "Variables"

    ''' <summary>
    ''' My property
    ''' </summary>
    Private mMyProperty As String = Nothing

#End Region

#Region "Properties"

    ''' <summary>
    ''' Gets or sets value of MyProperty
    ''' </summary>
    Public Property MyProperty() As String
        Get
            Return mMyProperty
        End Get
        Set(ByVal value As String)
            mMyProperty = value
        End Set
    End Property

#End Region

#Region "Methods"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        Bind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Private Sub Bind()
        Dim UserId As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = DBConnection.GetTrasactionsByUser(UserId, 2, 1)
        Session("GetSvdTransDataTable") = dt
        GridViewTrans.DataSource = dt
        GridViewTrans.DataBind()
    End Sub

    Protected Sub openPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        Receipt.IsForSearch = False
        Receipt.TransID = btn.CommandArgument
        Receipt.ReloadData()
        JsScript("ShowReceipt();")
        Bind()
    End Sub

    Protected Sub Delete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        Dim SavedTransID As Integer = btn.CommandArgument
        DBConnection.UpdateTransaction(SavedTransID, 2, 0)
        Bind()
    End Sub

#End Region

#Region " Paging "

    Protected Sub GvNews_PageIndexChanged(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles GridViewTrans.PageIndexChanging
        GridViewTrans.PageIndex = e.NewPageIndex
        Bind()
    End Sub

    Protected Sub GridViewTrans_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridViewTrans.DataBound
        Dim pagerRow As GridViewRow = GridViewTrans.BottomPagerRow
        If GridViewTrans.Rows.Count > 0 Then
            Dim GetPageNow As Integer = GridViewTrans.PageIndex + 1
            Dim GetPageCount As Integer = GridViewTrans.PageCount
            Dim GetPageLinksIndex As HtmlGenericControl = DirectCast(pagerRow.Cells(0).FindControl("lblPageIndex"), HtmlGenericControl)
            If pagerRow.Visible = True Then
                If GetPageCount <= 7 Then
                    CreateGridPagerLinks(1, GridViewTrans.PageCount, GetPageNow, GetPageLinksIndex)
                Else
                    Dim GetLeft As Integer = 1
                    Dim GetRight As Integer = 7
                    If GetPageNow > 4 Then
                        GetLeft = GetPageNow - 3
                        GetRight = GetPageNow + 3
                        If GetRight > GetPageCount Then
                            GetLeft = GetLeft - (GetRight - GetPageCount)
                            If GetLeft < 1 Then
                                GetLeft = 1
                            End If
                            GetRight = GetPageCount
                        End If
                        CreateGridPagerLinks(GetLeft, GetRight, GetPageNow, GetPageLinksIndex)
                    Else
                        CreateGridPagerLinks(GetLeft, GetRight, GetPageNow, GetPageLinksIndex)
                    End If
                End If
            End If
            pagerRow.Cells(0).FindControl("lnkPrev").Visible = (GridViewTrans.PageIndex > 0)
            pagerRow.Cells(0).FindControl("lnkNext").Visible = (GridViewTrans.PageCount - 1 > GridViewTrans.PageIndex)
            Dim btnCompletePay As ImageButton = DirectCast(pagerRow.Cells(0).FindControl("btnCompletePay"), ImageButton)
            If Not IsNothing(btnCompletePay) Then
                btnCompletePay.Attributes.Add("onclick", "return CheckGridValues()")
            End If
            If pagerRow.Visible = False Then
                pagerRow.Visible = True
            End If
        End If
    End Sub

    Private Sub CreateGridPagerLinks(ByVal StartNo As Integer, ByVal EndNo As Integer, ByVal GetPageNow As Integer, ByVal GetPageLinksIndex As HtmlGenericControl)
        For i As Integer = StartNo To EndNo
            Dim NewDiv As New HtmlGenericControl("div")
            Dim NewLink As New LinkButton
            NewLink.ID = "PagerLink_" & i
            NewLink.OnClientClick = String.Concat("GoToPage('", i, "');return false")
            If i = GetPageNow Then
                NewDiv.Attributes.Add("class", "GridPagerNoSel")
                NewLink.CssClass = "GridPagerNolnkSel"
            Else
                NewDiv.Attributes.Add("class", "GridPagerNo")
                NewLink.CssClass = "GridPagerNolnk"
                NewLink.Style.Add("color", "#4f85d1")
            End If
            NewLink.Text = i
            NewDiv.Controls.Add(NewLink)
            GetPageLinksIndex.Controls.Add(NewDiv)
        Next
    End Sub

    Sub ChangePageByLinkNumber_Before(ByVal sender As Object, ByVal e As EventArgs)
        If GridViewTrans.PageIndex > 0 Then
            Dim pageList As LinkButton = CType(sender, LinkButton)
            GridViewTrans.PageIndex = GridViewTrans.PageIndex - 1
            GridViewTrans.DataSource = Session("GetSvdTransDataTable")
            GridViewTrans.DataBind()
            ClearHiddenValues()
        End If
    End Sub

    Sub ChangePageByLinkNumber_Next(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageList As LinkButton = CType(sender, LinkButton)
        GridViewTrans.PageIndex = GridViewTrans.PageIndex + 1
        GridViewTrans.DataSource = Session("GetSvdTransDataTable")
        GridViewTrans.DataBind()
        ClearHiddenValues()
    End Sub

    Protected Sub HiddenPageBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HiddenPageBtn.Click
        GridViewTrans.PageIndex = CInt(HiddenPage.Text) - 1
        GridViewTrans.DataSource = Session("GetSvdTransDataTable")
        GridViewTrans.DataBind()
        ClearHiddenValues()
    End Sub

    Private Sub ClearHiddenValues()
        HiddenGridSelectedRow.Text = 0
        HiddenGridPrice.Text = 0
    End Sub

#End Region

    Protected Sub GridViewTrans_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridViewTrans.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim GridRbSelectPay As RadioButton = DirectCast(e.Row.FindControl("GridRbSelectPay"), RadioButton)
            Dim lnkDelete As LinkButton = DirectCast(e.Row.FindControl("lnkDelete"), LinkButton)
            Dim lblID As Label = DirectCast(e.Row.FindControl("lblID"), Label)
            Dim lblMoney As Label = DirectCast(e.Row.FindControl("lblMoney"), Label)
            If Not IsNothing(GridRbSelectPay) Then
                lnkDelete.Attributes.Add("onclick", String.Concat("return confirm('", ResHelper.LocalizeString("{$=Διαγραφή Συναλλαγής?|en-us=Delete Transaction?$}"), "')"))
                GridRbSelectPay.Attributes.Add("onclick", String.Concat("GridRBSelect('", GridRbSelectPay.ClientID, "','", lblID.Text, "','", lblMoney.Text, "')"))
            End If
        End If
    End Sub

    Protected Sub btnCompletePayClick(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Dim RowID As Integer = HiddenGridSelectedRow.Text
        Dim Price As String = Replace(HiddenGridPrice.Text, ",", ".")
        Response.Redirect("RegisteredUser.aspx?MerchantID=" & RowID & "&SelPrice=" & Price)
    End Sub
    Private Sub JsScript(ByVal Script As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), Guid.NewGuid.ToString, Script, True)
    End Sub
End Class
