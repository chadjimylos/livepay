SET IDENTITY_INSERT [CMS_WorkflowStep] ON
INSERT INTO [CMS_WorkflowStep] ([StepID], [StepGUID], [StepLastModified], [StepOrder], [StepDisplayName], [StepName], [StepWorkflowID]) VALUES (1, '633c6b94-f891-4a37-ba9c-bd14f76bcde8', '20080820 13:53:20', 1, N'Edit', N'edit', 1)
INSERT INTO [CMS_WorkflowStep] ([StepID], [StepGUID], [StepLastModified], [StepOrder], [StepDisplayName], [StepName], [StepWorkflowID]) VALUES (2, '4391d9e9-5954-41d0-b082-fef4b09bcff3', '20080808 12:30:23', 4, N'Published', N'published', 1)
INSERT INTO [CMS_WorkflowStep] ([StepID], [StepGUID], [StepLastModified], [StepOrder], [StepDisplayName], [StepName], [StepWorkflowID]) VALUES (20, '9d987f6a-c420-49d8-aafd-898b7ec4ece9', '20080808 12:08:19', 5, N'Archived', N'archived', 1)
SET IDENTITY_INSERT [CMS_WorkflowStep] OFF
