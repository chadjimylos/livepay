﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports LivePay_ESBBridge
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.SiteProvider
Imports CMS.DataEngine
Imports CMS.SettingsProvider
Imports CMS.FormControls
Imports System.IO
Imports System.Collections.Generic

'Imports LivePayCustomField
Public Structure CustomFieldsType
    Private _TypeName As String
    Private _ErrorDivID As String
    Private _TypePath As String

    Public Property TypeName() As String
        Get
            Return _TypeName
        End Get
        Set(ByVal value As String)
            _TypeName = value
        End Set
    End Property

    Public Property ErrorDivID() As String
        Get
            Return _ErrorDivID
        End Get
        Set(ByVal value As String)
            _ErrorDivID = value
        End Set
    End Property

    Public Property TypePath() As String
        Get
            Return _TypePath
        End Get
        Set(ByVal value As String)
            _TypePath = value
        End Set
    End Property


End Structure

'Public Class LivePayCustomFieldType
'    Shared Sub New()
'    End Sub

'    Public Shared Function AddType(ByVal TypeName As String, ByVal TypePath As String) As CustomFieldsType
'        Dim fieldType As New CustomFieldsType
'        fieldType.TypeName = TypeName
'        fieldType.TypePath = TypePath
'        Return fieldType
'    End Function
'End Class

' ''' <summary>
' ''' Settings key form controls information
' ''' </summary>
'Public Structure SettingsKeyItem
'    Public settingsKey As String
'    ' Settings key code name
'    Public inputControlId As String
'    ' Checkbox id, textbox id
'    Public inheritId As String
'    ' Inherit checkbox id
'    Public isInherited As Boolean
'    Public errorLabelId As String
'    ' Error label id
'    Public value As String
'    ' Value
'    Public type As String
'    ' Type (int, boolean)
'    Public validation As String
'    ' Regex vor validation
'    Public changed As Boolean
'    ' Changed flag
'End Structure


Partial Class CMSTemplates_LivePay_UCRegisteredUsers
    Inherits CMSUserControl

    'Private LivePayMerchant As String
    'Private pathToGroupselector As String = "~/CMSModules/Groups/FormControls/MembershipGroupSelector.ascx"
    Private pathToGroupselector As String = "~/CMSTemplates/LivePay/CustomFields/USphone.ascx"
    Private selectInGroups As FormEngineUserControl
    'Private ArrCustomFieldType As New ArrayList()
    ReadOnly FieldType As New List(Of CustomFieldsType)()
    'Private mMerchantPublicSector As Boolean = False


#Region " Properties "

    Public ReadOnly Property TransPublic As Boolean
        Get
            Return IIf(CMSContext.CurrentDocument.NodeClassName = "LivePay.MerchantPublicSector", True, False)
        End Get
    End Property

    Protected ReadOnly Property MerchantID() As Integer
        Get
            Dim iMerchantID As Integer = 1

            If TransPublic Then
                iMerchantID = CMSContext.CurrentDocument.GetValue("MerchantPublicSectorID")
            Else
                iMerchantID = CMSContext.CurrentDocument.GetValue("MerchantID")
            End If
            'iMerchantID = CMSContext.CurrentDocument.GetValue("MerchantID")
            Return iMerchantID
        End Get
    End Property

    Protected ReadOnly Property SelectedPrice() As String
        Get
            Dim sPrice As String = "0"
            If Not String.IsNullOrEmpty(Request("SelPrice")) Then
                sPrice = Request("SelPrice")
            End If
            Return sPrice
        End Get
    End Property

    Protected ReadOnly Property TransactionID() As String
        Get
            Dim mTransactionID As String = "0"
            If Not String.IsNullOrEmpty(Request("TransactionID")) Then
                mTransactionID = Request("TransactionID")
            End If
            Return mTransactionID
        End Get
    End Property


    Private MinimumTransactionAmount As Decimal = 0
    Private MaximumTransactionAmount As Decimal = 0
    Private MaximumTransactionsPerDayCard As Integer = 0
    Private MaximumTransactionsPerDayGlobally As Integer = 0
    Private MaximumSumTransactionsPerDayInCard As Decimal = 0
    Private MaxTransactionsPerSessionPerCard As Integer = 0
    Private DTMerchantCardTypes As DataTable = Nothing
    Private CompanyName As String = String.Empty
    Private CompanyLogo As String = String.Empty
    Private MerchantValidCards As String = String.Empty
#End Region

#Region "Methods"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Tab1_BtnNext.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/BtnNext.png|en-us=/App_Themes/LivePay/RegisteredUsers/BtnNext_en-us.png$}")
        Tab2_btnReturn.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnReturn.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnReturn_en-us.png$}")
        ImageButton2.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnReturn.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnReturn_en-us.png$}")
        Tab3_btnReturn.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnReturn.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnReturn_en-us.png$}")
        Tab3_btnComplete.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnComplete.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnComplete_en-us.png$}")
        'LivePayMerchant = CMSContext.CurrentDocument.NodeID
        GetInstallments()

        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        ElseIf Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
        CreateCustomFields()
    End Sub

    Public Overloads Sub ReloadData()
        'mMerchantPublicSector = TransPublic

        If (TransPublic) Then
            Tab1_txtNotes.Visible = False
            divNotes.Visible = False
        End If

        'GetInstallments()



        ' Response.Redirect("/CMSTemplates/LivePay/xmlHttpMerchants.aspx")
        If IsNothing(Session("UserTransactions")) Then
            Session("UserTransactions") = 0
        End If
        Tab2_RbSavedCard.Attributes.Add("onclick", String.Concat("ChangeRadioChoice('", Tab2_RbSavedCard.ClientID, "','", Tab2_RbNewCard.ClientID, "')"))
        Tab2_RbNewCard.Attributes.Add("onclick", String.Concat("ChangeRadioChoice('", Tab2_RbNewCard.ClientID, "','", Tab2_RbSavedCard.ClientID, "')"))


        CreateCustomFields()


        SetDefaults()
        GetMerchantLimits()
        GetUserCards()
        If Me.SelectedPrice <> "0" Then
            Tab1_txtPrice.Text = SelectedPrice
        End If


        pnlNewCard.Style.Add("display", "none")

		Tab2_ChkSaveDetails.text = ResHelper.LocalizeString("{$=Αποθήκευση Στοιχείων Κάρτας για Μελλοντική Χρήση|en-us=Save Credit Card Info for future use$}")
		
        Dim IsLogedIn As Boolean = CMSContext.CurrentUser.IsPublic()
        If IsLogedIn Then
            pnlNewCard.Style.Add("display", "block")
            ForRegUsersTop.Style.Add("display", "none")
            Tab2_RbNewCardDivCont.Style.Add("display", "none")
            Tab2_RbNewCard.Checked = True
            ForUnRegUsers_PhoneMail.Style.Add("display", "")
            ForRegUsersChoiceToSave.Style.Add("display", "none")
            JsScript(String.Concat("; ChangeRadioChoice('", Tab2_RbNewCard.ClientID, "','", Tab2_RbSavedCard.ClientID, "'); "))
        Else
        End If

    End Sub

    Private Sub SetErrorMessages()
        Dim dt As DataTable = DBConnection.GetErrorMessages(1)
        Dim JsStringMessages As String = String.Empty
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim ErrorMessageStr As String = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("ErrorMessageGR").ToString, "|en-us=", dt.Rows(i)("ErrorMessageEN").ToString, "$}"))
            ErrorMessageStr = Replace(ErrorMessageStr, "'", "\'")

            If dt.Rows(i)("CodeName").ToString = "TermsOfUse" Then
                JsStringMessages = String.Concat(JsStringMessages, " ", dt.Rows(i)("CodeName").ToString, " = '", CMS.GlobalHelper.HTMLHelper.StripTags(ErrorMessageStr).Replace(vbCr, String.Empty).Replace(vbLf, String.Empty), "'; ")
            Else
                JsStringMessages = String.Concat(JsStringMessages, " ", dt.Rows(i)("CodeName").ToString, " = '", ErrorMessageStr.Replace(vbCr, String.Empty).Replace(vbLf, String.Empty), "'; ")
            End If


        Next
        JsStringMessages = String.Concat(JsStringMessages, " $('#", Tab3_lblTerms.ClientID, "').html(TermsOfUse);  ")
        JsScript(JsStringMessages)
    End Sub

    Private Sub GetMerchantLimits()
        SetErrorMessages()
        'Dim ds As DataSet = DBConnection.GetMerchantLimits(Me.MerchantID)
        'Response.Write("->" & Me.MerchantID & "<-")

        Dim ds As DataSet

        ds = DBConnection.GetMerchantLimits(Me.MerchantID, TransPublic)

        Dim dt As DataTable = ds.Tables(0)
        'Response.Write(dt.Rows.Count)
        If dt.Rows.Count > 0 Then
            Dim row As DataRow = dt.Rows(0)
            MinimumTransactionAmount = row("MinimumTransactionAmount")
            MaximumTransactionAmount = row("MaximumTransactionAmount")
            MaximumTransactionsPerDayCard = row("MaximumTransactionsPerDayCard")
            MaximumTransactionsPerDayGlobally = row("MaximumTransactionsPerDayGlobally")
            'MaximumSumTransactionsPerDayInCard --> MaximumSumTransactionsPerDayCard
            MaximumSumTransactionsPerDayInCard = row("MaximumSumTransactionsPerDayInCard")
            CompanyName = row("CompanyName").ToString
            CompanyLogo = row("Logo").ToString
            HiddenMinimumTransactionAmount.Text = row("MinimumTransactionAmount").ToString
            HiddenMaximumTransactionAmount.Text = row("MaximumTransactionAmount").ToString
            HiddenMaximumTransactionsPerDayCard.Text = row("MaximumTransactionsPerDayCard").ToString
            HiddenMaximumTransactionsPerDayGlobally.Text = row("MaximumTransactionsPerDayGlobally").ToString
            HiddenMaximumSumTransactionsPerDayInCard.Text = row("MaximumSumTransactionsPerDayInCard").ToString
            HiddenCompanyName.Text = row("CompanyName").ToString
            Session("NewPayment_Merchant_LivePay_ID") = row("LivePayID").ToString
            HiddenMerchantID.Text = MerchantID

            '--- Per Session
            Dim SessionLimits As Integer = 10
            HiddenMaxTransactionsPerSessionPerCard.Text = SessionLimits
            If Session("UserTransactions") > SessionLimits Then

                GotToTransErrorPage("SessionError")
            End If

            Dim UserID As Integer = CMSContext.CurrentUser.UserID
            Dim dtMaxMerchantTransLimit As DataTable = DBConnection.GetMerchantTransaction(UserID, Me.MerchantID, 0, String.Empty, 2)
            If dtMaxMerchantTransLimit.Rows.Count > 0 Then
                Dim MerchantsTrans As Integer = dtMaxMerchantTransLimit.Rows(0)("TransCountPerMerchant")
                If MerchantsTrans > MaximumTransactionsPerDayGlobally Then

                    GotToTransErrorPage("MaximumTransError")
                End If
            End If

            '--- Accepted Card Types
            DTMerchantCardTypes = ds.Tables(1)
            For i As Integer = 0 To DTMerchantCardTypes.Rows.Count - 1
                If MerchantValidCards = String.Empty Then
                    MerchantValidCards = String.Concat("[", DTMerchantCardTypes.Rows(i)("itemID").ToString, "]")
                Else
                    MerchantValidCards = String.Concat(MerchantValidCards, ",[", DTMerchantCardTypes.Rows(i)("itemID").ToString, "]")
                End If
                '---- Card Types DropDown

            Next
            HiddenMerchantValidCards.Text = MerchantValidCards



        End If
    End Sub

    Private Sub GotToTransErrorPage(ByVal ErrorName As String)
        Select Case ErrorName
            Case "SessionError"
                JsScript("alert(UsertMaxTransSessionErrorMessage);location.href='default.aspx';")
            Case "MaximumTransError"
                JsScript("alert(MerchantMaxTransErrorMessage);location.href='default.aspx';")
        End Select


    End Sub

    Private Sub GetUserCards()
        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = DBConnection.GetUserCards(UserID)
        Dim dtFinal As DataTable = dt
        dtFinal.Columns.Add("Text", GetType(String), "FriendlyName + ' - xxxxxxxx-xxxx-' + LastFor")
        dtFinal.Columns.Add("Value", GetType(String), "CardID + '_' + CardTypeID")
        Tab2_ddlSavedCard.DataTextField = "Text"
        Tab2_ddlSavedCard.DataValueField = "Value"
        Tab2_ddlSavedCard.DataSource = dtFinal
        Tab2_ddlSavedCard.DataBind()
        Tab2_ddlSavedCard.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=Επιλογή Κάρτας|en-us=Select Card$}"), String.Empty))

        Tab2_ddlSavedCard.Attributes.Add("onchange", String.Concat("CheckValidCard()"))

        If dt.Rows.Count = 0 Then
            ForRegUsersTop.Style.Add("display", "none")
            Tab2_RbNewCardDivCont.Style.Add("display", "none")
            Tab2_RbNewCard.Checked = True
            JsScript(String.Concat("; ChangeRadioChoice('", Tab2_RbNewCard.ClientID, "','", Tab2_RbSavedCard.ClientID, "'); "))
        End If

    End Sub

    Private Sub SetDefaults()
        Tab2_ChkSaveDetails.Attributes.Add("onclick", "CardForSave(this)")
        Tab1_ReqFldVal_Price.ErrorMessage = ResHelper.LocalizeString("{$=Ποσό|en-us=Amount$}")
        'Tab1_ReqularFldVal_Price.ErrorMessage = ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}")

        Tab2_RbSavedCard.Text = ResHelper.LocalizeString("{$=Επιλογή Αποθηκευμένης Κάρτας|en-us=Choose from your saved Cards$}")
        Tab2_RbNewCard.Text = ResHelper.LocalizeString("{$=Εισάγετε Νέα Κάρτα|en-us=Insert New Credit Card$}")
        Tab2_ReqFldVal_FriendName.ErrorMessage = ResHelper.LocalizeString("{$=Φιλική Ονομασία|en-us=Friendly Name$}")
        Tab2_ChlIWantInvoice.Text = ResHelper.LocalizeString("{$=Επιθυμώ να μου αποσταλεί τιμολόγιο|en-us=I wish to have an Invoice issued$}")

        For i As Integer = 1 To 12
            Dim dtm As Date = new date(2000,i,1)
            Tab2_DdlCardMonth.Items.Insert(i - 1, New ListItem(dtm.ToString("MMMM"), i))
        Next

        For i As Integer = 0 To 9
            Dim Years As Integer = Year(Now) + i
            Tab2_DdlCardYear.Items.Insert(i, New ListItem(Years, Years))
        Next

        ddlDoy.DataSource = DBConnection.GetTaxOffices
        ddlDoy.DataTextField = ResHelper.LocalizeString("{$=TitleGr|en-us=TitleEn$}")
        ddlDoy.DataValueField = "ItemID"
        ddlDoy.DataBind()
        ddlDoy.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=Επιλέξτε ΔΟΥ|en-us=Select Tax Office$}"), String.Empty))

    End Sub

    Private Sub CreateCustomFields()
        Tab2_ddlCardType.Items.Insert(0, New ListItem(ResHelper.LocalizeString("{$=Επιλέξτε Τύπο Κάρτας|en-us=Select Card Type$}"), String.Empty))
        Tab2_ddlCardType.Items.Insert(1, New ListItem("Visa", 0))
        Tab2_ddlCardType.Items.Insert(2, New ListItem("MasterCard", 1))
        Tab2_ddlCardType.Items.Insert(3, New ListItem("EuroLine", 2))
        SetErrorMessages()

        'Response.Write("CMSContext.CurrentDocument.NodeID --> " & CMSContext.CurrentDocument.NodeID)
        'Response.Write("MerchantID --> " & Me.MerchantID)
        'Response.End()

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(CMSContext.CurrentDocument.NodeID)
        Dim dt As DataTable = ds.Tables(0)

        For i As Integer = 0 To dt.Rows.Count - 1

            Dim CustomFieldValue As String = String.Empty
            'Response.Write(CustomFieldValue)

            If Me.TransactionID <> 0 Then
                Dim FieldID As Integer = dt.Rows(i)("ItemID")

                Dim FieldValueDT As DataTable = DBConnection.GetFavoritesCustomFields(Me.TransactionID, FieldID, CMSContext.CurrentUser.UserID)
                If FieldValueDT.Rows.Count > 0 Then
                    CustomFieldValue = FieldValueDT.Rows(0)(0).ToString()
                End If
            End If

            Dim CustomTitleDiv As New HtmlGenericControl("div")
            CustomTitleDiv.Attributes.Add("class", "RegUsersTab1CustomTitle")
            CustomTitleDiv.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))




            Dim CustomTextBoxDiv As New HtmlGenericControl("div")
            CustomTextBoxDiv.ID = "formUserControl_Div" & i
            CustomTextBoxDiv.Attributes.Add("class", "RegUsersTab2_TxtDiv")

            'If (CMSContext.CurrentDocument.NodeClassName = mMerchantPublicSector) Then

            Dim FormControlID As Integer = Convert.ToInt32(dt.Rows(i)("FieldType").ToString())
            Dim pathFormControl As String = LivePayCustomField.GetPathFormControl(FormControlID)

            'If Not IsPostBack Then
            Dim NDate As Integer = ValidationHelper.GetInteger(dt.Rows(i)("NDate").ToString, 0)
            Dim OrganismCode As Integer = ValidationHelper.GetInteger(dt.Rows(i)("OrganismCode").ToString, 0)
            Dim TypePayment As Integer = ValidationHelper.GetInteger(dt.Rows(i)("TypePayment").ToString, 0)
            Dim MaxLength As Integer = ValidationHelper.GetInteger(dt.Rows(i)("MaxLength").ToString, 0)
            Dim Mandatory As Boolean = ValidationHelper.GetBoolean(dt.Rows(i)("Mandatory").ToString, False)
            Dim RegExp As String = ValidationHelper.GetString(dt.Rows(i)("RegExp").ToString, "")
            Dim NameFieldEn As String = ValidationHelper.GetString(dt.Rows(i)("NameFieldEn").ToString, "")
            Dim NameFieldGr As String = ValidationHelper.GetString(dt.Rows(i)("NameFieldGr").ToString, "")

            CreateCustomFormControl(pathFormControl, CustomTextBoxDiv, i, NameFieldGr, NameFieldEn, RegExp, Mandatory, MaxLength, TypePayment, OrganismCode, NDate, CustomFieldValue)




            'End If



            'Select Case dt.Rows(i)("FieldType").ToString.ToLower()
            '    Case "text"
            '        Dim pathFormControlAFM As String = "~/CMSTemplates/LivePay/CustomFields/USphone.ascx"
            '        CreateCustomFormControl(pathFormControlAFM, CustomTextBoxDiv, i, dt.Rows(i)("NameFieldGr").ToString, dt.Rows(i)("NameFieldEn").ToString)
            '    Case "taxid"
            '        Dim pathFormControlAFM As String = "~/CMSTemplates/LivePay/CustomFields/taxid.ascx"
            '        CreateCustomFormControl(pathFormControlAFM, CustomTextBoxDiv, i, dt.Rows(i)("NameFieldGr").ToString, dt.Rows(i)("NameFieldEn").ToString)
            '    Case "test"
            '        Dim pathFormControlAFM As String = "~/CMSTemplates/LivePay/CustomFields/USphone.ascx"
            '        CreateCustomFormControl(pathFormControlAFM, CustomTextBoxDiv, i, dt.Rows(i)("NameFieldGr").ToString, dt.Rows(i)("NameFieldEn").ToString)
            '    Case Else
            '        Dim CustomCloudDiv As New HtmlGenericControl("div")
            '        CustomCloudDiv.InnerHtml = String.Concat("<a id='CustomCloud_", i, "' href='#'></a>")
            '        CustomTextBoxDiv.Controls.Add(CustomCloudDiv)

            '        Dim txtbx As New TextBox
            '        txtbx.CssClass = "RegUsersTab1CustomTxt"
            '        txtbx.Width = "260"
            '        txtbx.ID = "CustomField_" & i
            '        txtbx.MaxLength = dt.Rows(i)("maxlength").ToString
            '        txtbx.Attributes.Add("onclick", "CountValidations=0;")
            '        If CustomFieldValue <> String.Empty Then
            '            txtbx.Text = CustomFieldValue
            '        End If
            '        CustomTextBoxDiv.Controls.Add(txtbx)

            '        Dim CustomDivValidation As New HtmlGenericControl("div")
            '        CustomDivValidation.Style.Add("display", "none")

            '        Dim CustomValidat As New CustomValidator
            '        CustomValidat.ValidationGroup = "Tab1_PayForm"
            '        CustomValidat.ValidateEmptyText = True
            '        CustomValidat.ControlToValidate = txtbx.ClientID
            '        CustomValidat.ClientValidationFunction = "validateControls"
            '        CustomValidat.ErrorMessage = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
            '        CustomValidat.Attributes.Add("ControlValidated", txtbx.ClientID)
            '        CustomValidat.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", i))
            '        CustomValidat.Attributes.Add("CustomValidatorErrorMessage", ResHelper.LocalizeString(String.Concat("{$=Συμπληρώστε το πεδίο <b>", dt.Rows(i)("NameFieldGr").ToString, "</b>|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}")))

            '        If dt.Rows(i)("Mandatory").ToString = "True" Then
            '            CustomDivValidation.Controls.Add(CustomValidat)
            '        End If

            '        CustomTextBoxDiv.Controls.Add(CustomDivValidation)
            '        Exit Select
            'End Select

            '--- Add Controls
            CustomFields.Controls.Add(CustomTitleDiv)
            CustomFields.Controls.Add(CustomTextBoxDiv)


            'Dim ControlID As String = String.Concat("formUserControl_" & i)
            'Dim MyControl As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID), LivePayFormEngineUserControl)
            'If MyControl IsNot Nothing Then
            '    'Response.Write("|->" & MyControl.FindControl("IDTransaction").ClientID & "<-|")
            '    'Response.Write("|->" & Request.Form.ToString & "<-|")
            '    'Response.Write("|->" & MyControl.FindControl("IDTransaction").ClientID.Replace("%24", "$").Replace("$0", "_0") & "<-|")
            '    Response.Write("|->" & Request.Form(MyControl.FindControl("IDTransaction").ClientID.Replace("_", "$")) & "<-|")         '.Replace("$0", "_0")
            '    'For j As Integer = 0 To Request.Form.Keys.Count - 1
            '    'Response.Write(Request.Form.Keys(j) & "<br />")
            '    'Next

            'End If

            '-----
            Dim MainDivCloud As New HtmlGenericControl("div")
            MainDivCloud.Attributes.Add("class", "RegUsersMainDivCloud")
            'MainDivCloud.Attributes.Add("onmouseover", String.Concat("ShowClound('", CloudID, "',this)"))
            'MainDivCloud.Attributes.Add("onmouseout", String.Concat("HideClound('", CloudID, "')"))

            '--- Start Create Cloud

            Dim NewCustomCloudID As String = "NewCoustomCloud_" & i & "_CloudMain"
            Dim NewCustomCloudContentID As String = "NewCoustomCloud_" & i & "_CloudContent"
            Dim NewCloudContent As String = ResHelper.LocalizeString(String.Concat("{$=", Replace(dt.Rows(i)("DescriptionGR").ToString, "'", String.Empty), "|en-us=", Replace(dt.Rows(i)("DescriptionEn").ToString, "'", String.Empty), "$}"))
            NewCloudContent = Replace(NewCloudContent, vbCrLf, " ")
            Dim LinkOpener As New HtmlAnchor
            LinkOpener.Attributes.Add("onmouseover", String.Concat("OpenCustomCloud('", NewCloudContent, "','", NewCustomCloudContentID, "','", NewCustomCloudID, "',this,true)"))
            LinkOpener.Attributes.Add("onmouseout", String.Concat("OpenCustomCloud('", NewCloudContent, "','", NewCustomCloudContentID, "','", NewCustomCloudID, "',this,false)"))

            Dim imgInfo As New HtmlImage
            imgInfo.Src = "/App_Themes/LivePay/InfoBtn.png"
            LinkOpener.Controls.Add(imgInfo)
            MainDivCloud.Controls.Add(LinkOpener)

            Dim DivCloud As New HtmlGenericControl("div")
            DivCloud.Style.Add("position", "absolute")
            DivCloud.Style.Add("padding-bottom", "130px")
            DivCloud.Style.Add("display", "none")
            DivCloud.Attributes.Add("id", NewCustomCloudID)

            DivCloud.Controls.Add(CreatCloud(NewCustomCloudContentID))

            'MainDivCloud.Controls.Add(DivCloud)
            '--- Add Controls
            CustomFields.Controls.Add(MainDivCloud)
            CustomFields.Controls.Add(DivCloud)

            Dim DivClear As New HtmlGenericControl("div")
            DivClear.Attributes.Add("class", "Clear")
            CustomFields.Controls.Add(DivClear)
            '-----
            'CustomCloud_", i
            CreateCustomFieldsForTab3(dt.Rows(i)("NameFieldGr").ToString, dt.Rows(i)("NameFieldEn").ToString, i)



        Next



        Dim CloseCloudsFn As String = "function CloseAllClouds() { "
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardMsgNewCard').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#NewCardFullNameMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#NewCardCVVMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardCVV2Msg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#SavedCardMsgSec').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_CompanyNameMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_OccupationMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_AddressMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_TKMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_CityMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_AFMMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Inv_DOYMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#CardTypeMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#Tab1_PriceMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#NewCardFriendly').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#UnRegUserPhoneMsg').poshytip('hide'); ")
        CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#UnRegUserEmailMsg').poshytip('hide'); ")
        '---- Custom Clouds
        For i As Integer = 0 To dt.Rows.Count - 1
            CloseCloudsFn = String.Concat(CloseCloudsFn, "$('#CustomCloud_", i, "').poshytip('hide'); ")
        Next
        '---- Custom Clouds
        CloseCloudsFn = String.Concat(CloseCloudsFn, "} ")
        lit_JsScript.Text = "<script> " & CloseCloudsFn & " </script>"

        Tab2_RbSavedCard.TabIndex = 9
        Tab2_ddlSavedCard.TabIndex = 10
        Tab2_txtCVV2Top.TabIndex = 11
        Tab2_RbNewCard.TabIndex = 12
        Tab2_ddlCardType.TabIndex = 13
        Tab2_txtCardNo.TabIndex = 14
        Tab2_txtFullNameOwner.TabIndex = 15
        Tab2_DdlCardMonth.TabIndex = 16
        Tab2_DdlCardYear.TabIndex = 17
        Tab2_txtCVV2.TabIndex = 18
        Tab2_ChkSaveDetails.TabIndex = 19
        Tab2_txtFriendName.TabIndex = 20
        Tab2_txtPhone.TabIndex = 21
        Tab2_txtEmail.TabIndex = 22
        Tab2_btnNext.TabIndex = 23
        Tab2_ChlIWantInvoice.TabIndex = 24
        Tab2Inv_CompanyName.TabIndex = 25
        Tab2Inv_Occupation.TabIndex = 26
        Tab2Inv_Address.TabIndex = 27
        Tab2Inv_City.TabIndex = 28
        Tab2Inv_TK.TabIndex = 29
        Tab2Inv_AFM.TabIndex = 30
        ddlDoy.TabIndex = 31
        Tab2_NextBtnInvoice.TabIndex = 32
    End Sub

    Private Sub CreateCustomFieldsForTab3(ByVal NameGR As String, ByVal NameEN As String, ByVal iCount As Integer)
        'Dim ControlID1 As String = String.Concat("formUserControl_0")
        'Dim MyControl1 As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID1), LivePayFormEngineUserControl)
        'Response.Write(MyControl1.Value)

        Dim TitleDiv As New HtmlGenericControl("div")
        Dim ValueDiv As New HtmlGenericControl("div")
        Dim ClearDiv As New HtmlGenericControl("div")
        TitleDiv.Attributes.Add("class", "RegUserTab3TitleB")
        TitleDiv.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", NameGR, "|en-us=", NameEN, "$}"))
        ValueDiv.Attributes.Add("class", "RegUsersTab2_TxtDiv")

        Dim ControlID As String = String.Concat("formUserControl_" & iCount)
        Dim MyControl As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID), LivePayFormEngineUserControl)

        'MyControl.Value = "123444444"

        ValueDiv.InnerHtml = String.Concat("<span class='RegUSerTab3Control' id='Tab3_Span_CustomField_", iCount, "'></span>")
        ClearDiv.Attributes.Add("class", "Clear")
        With Tab3_CustomFields.Controls
            .Add(TitleDiv)
            .Add(ValueDiv)
            .Add(ClearDiv)
        End With
    End Sub

    Private Function CreatCloud(ByVal NewCustomCloudContentID As String) As HtmlTable
        Dim MainTable As New HtmlTable
        MainTable.CellPadding = 0
        MainTable.CellSpacing = 0
        MainTable.Border = 0



        Dim RowA As New HtmlTableRow
        Dim Cell_LeftTop As New HtmlTableCell
        Dim Cell_Top As New HtmlTableCell
        Dim Cell_RightTop As New HtmlTableCell

        Cell_LeftTop.Attributes.Add("class", "CustCloudTopLeft")
        Cell_Top.Attributes.Add("class", "CustCloudTop")
        Cell_RightTop.Attributes.Add("class", "CustCloudTopRight")
        With RowA.Controls
            .Add(Cell_LeftTop)
            .Add(Cell_Top)
            .Add(Cell_RightTop)
        End With

        Dim RowB As New HtmlTableRow
        Dim Cell_Left As New HtmlTableCell
        Dim Cell_Content As New HtmlTableCell
        Dim Cell_Right As New HtmlTableCell

        Cell_Left.Attributes.Add("class", "CustCloudLeft")
        Cell_Content.Attributes.Add("class", "CustCloudCenter")
        Cell_Right.Attributes.Add("class", "CustCloudRight")

        Cell_Content.Attributes.Add("id", NewCustomCloudContentID)

        With RowB.Controls
            .Add(Cell_Left)
            .Add(Cell_Content)
            .Add(Cell_Right)
        End With

        Dim RowC As New HtmlTableRow
        Dim Cell_LeftBottom As New HtmlTableCell
        Dim Cell_Bottom As New HtmlTableCell
        Dim Cell_RightBottom As New HtmlTableCell

        Cell_LeftBottom.Attributes.Add("class", "CustCloudBottomLeft")
        Cell_Bottom.Attributes.Add("class", "CustCloudBottom")
        Cell_RightBottom.Attributes.Add("class", "CustCloudBottomRight")

        With RowC.Controls
            .Add(Cell_LeftBottom)
            .Add(Cell_Bottom)
            .Add(Cell_RightBottom)
        End With

        With MainTable.Controls
            .Add(RowA)
            .Add(RowB)
            .Add(RowC)
        End With

        Return MainTable
    End Function
#End Region

#Region " Tabs Events "

    Protected Sub Tab1_BtnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab1_BtnNext.Click
        If Page.IsValid Then
            Dim validationFailed As Boolean = True

            'Dim ControlID As String = String.Concat("formUserControl_0")
            'Dim MyControl As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID), LivePayFormEngineUserControl)
            'Response.Write(MyControl.Value)

            For Each customFT As CustomFieldsType In FieldType

                Dim cntrl As Control = CustomFields.FindControl(customFT.TypeName)

                Dim control As LivePayFormEngineUserControl = TryCast(cntrl, LivePayFormEngineUserControl)
                If control IsNot Nothing Then
                    If Not control.IsValid() Then
                        validationFailed = False
                        JsScript("Tab1_ValidationCloud('#" & customFT.ErrorDivID & "', "" " & control.ValidationError & " "", true, '');")
                    End If
                End If
            Next
            If validationFailed Then
                JsScript("ShowHideTabs('none','','none');")
                If Tab2_RbNewCard.Checked Then
                    pnlNewCard.Style.Add("display", "block")
                End If
            End If

        End If
    End Sub

    Protected Sub Tab2_btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab2_btnNext.Click

        'Dim ControlID As String = String.Concat("formUserControl_0")
        'Dim MyControl As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID), LivePayFormEngineUserControl)
        'Response.Write("tab 2 " & MyControl.Value)

        If Page.IsValid AndAlso CheckInstallments() Then
            Tab3_lblPayTo.Text = HiddenCompanyName.Text
            Tab3_lblPrice.Text = Tab1_txtPrice.Text
            Dim CardNo As String = String.Empty
            If Tab2_RbNewCard.Checked Then
                Tab3_lblCard.Text = String.Concat("xxxxxxxx-xxxx-", Right(Tab2_txtCardNo.Text, 4))
                CardNo = Tab2_txtCardNo.Text
            Else
                Tab3_lblCard.Text = Split(Tab2_ddlSavedCard.SelectedItem.Text, " - ")(1).ToString
                Dim CardID As String = Split(Tab2_ddlSavedCard.SelectedValue, "_")(0)
                Dim DtCard As DataTable = DBConnection.GetUserCard(CMSContext.CurrentUser.UserID, CardID)
                If DtCard.Rows.Count > 0 Then
                    CardNo = Left(DtCard.Rows(0)("CardNumber").ToString, 4) & CryptoFunctions.DecryptString(Right(DtCard.Rows(0)("CardNumber").ToString, (DtCard.Rows(0)("CardNumber").ToString.Length - 4)))
                End If
            End If
            Dim jsCustomFeelFields As String = String.Empty
            'Dim ds As DataSet = DBConnection.GetMerchantCustomFields(Me.MerchantID)
            Dim ds As DataSet = DBConnection.GetMerchantCustomFields(CMSContext.CurrentDocument.NodeID)
            Dim dt As DataTable = ds.Tables(0)
            JsScript(String.Concat("SetCustomFieldsValues('CustomField_','Tab3_Span_CustomField_','", dt.Rows.Count - 1, "');ShowHideTabs('none','none','');"))
            Dim w As New LivePay_ESBBridge.Bridge
            Dim ResultGetCommission As String = w.GetCommission(CardNo, Session("NewPayment_Merchant_LivePay_ID"), Tab1_txtPrice.Text).Result

            If ResultGetCommission <> "" AndAlso ResultGetCommission <> "0" Then
                Tab3_Commission.Visible = True
                Text_Commission.Text = ResultGetCommission
            End If

        End If
    End Sub

    Protected Sub Tab2_NextBtnInvoice_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab2_NextBtnInvoice.Click


        If Page.IsValid Then
            Tab3_lblPayTo.Text = HiddenCompanyName.Text
            Tab3_lblPrice.Text = Tab1_txtPrice.Text
            If Tab2_RbNewCard.Checked Then
                Tab3_lblCard.Text = String.Concat("xxxxxxxx-xxxx-", Right(Tab2_txtCardNo.Text, 4))
            Else
                Tab3_lblCard.Text = Split(Tab2_ddlSavedCard.SelectedItem.Text, " - ")(1).ToString
            End If
            Dim jsCustomFeelFields As String = String.Empty
            'Dim ds As DataSet = DBConnection.GetMerchantCustomFields(Me.MerchantID)
            Dim ds As DataSet = DBConnection.GetMerchantCustomFields(CMSContext.CurrentDocument.NodeID)
            Dim dt As DataTable = ds.Tables(0)
            JsScript(String.Concat("SetCustomFieldsValues('CustomField_','Tab3_Span_CustomField_','", dt.Rows.Count - 1, "'); ShowHideTabs('none','none','');"))
        End If
    End Sub

    Function CheckInstallments() As Boolean

        If (hiddenInstallments.Text <> 0) Then

            Dim EncryptedCard As String = Left(Me.Tab2_txtCardNo.Text, 4) & CryptoFunctions.EncryptString(Right(Me.Tab2_txtCardNo.Text, 12))
            If Me.Tab2_txtCardNo.Text = String.Empty Then
                EncryptedCard = String.Empty
            End If

            Dim dtCards As DataTable = DBConnection.GetMerchantCardType(Me.hiddenUserCardID.Text, Me.Tab2_txtCardNo.Text)
            Dim CardLimitID As Integer = 0
            If dtCards.Rows.Count > 0 Then
                Dim UserCard As String = Me.Tab2_txtCardNo.Text
                If Me.Tab2_txtCardNo.Text <> String.Empty Then '---- New Card
                    For i As Integer = 0 To dtCards.Rows.Count - 1
                        Dim Limits As String = dtCards.Rows(i)("Limit").ToString
                        If Limits <> String.Empty Then
                            Dim LimitLength As Integer = Split(Limits, ",").Length - 1
                            For iCount As Integer = 0 To LimitLength
                                Dim CardLimitNo As String = Split(Limits, ",")(iCount)
                                If Me.Tab2_txtCardNo.Text.StartsWith(CardLimitNo) Then
                                    CardLimitID = dtCards.Rows(i)("itemid").ToString
                                End If
                            Next
                        End If
                    Next
                Else
                    For i As Integer = 0 To dtCards.Rows.Count - 1

                        Dim Limits As String = dtCards.Rows(i)("Limit").ToString
                        If Limits <> String.Empty Then
                            Dim LimitLength As Integer = Split(Limits, ",").Length - 1
                            For iCount As Integer = 0 To LimitLength
                                Dim CardLimitNo As String = Split(Limits, ",")(iCount)

                                Dim DecryptedCard As String = Left(dtCards.Rows(i)("CardNo").ToString, 4) & CryptoFunctions.DecryptString(Right(dtCards.Rows(i)("CardNo").ToString, dtCards.Rows(i)("CardNo").ToString.Length - 4))

                                If DecryptedCard.StartsWith(CardLimitNo) Then
                                    CardLimitID = dtCards.Rows(i)("itemid").ToString
                                End If
                            Next
                        End If
                    Next
                End If
            End If

            Dim MinInstallments_merchant As Integer = CMSContext.CurrentDocument.GetValue("MinInstallments")
            Dim MaxInstallments_merchant As Integer = CMSContext.CurrentDocument.GetValue("MaxInstallments")

            Dim dsItems As DataSet = GetTypeCard("active = 1 and Itemid = " & CardLimitID, "MinInstallments")

            Dim MinInstallments_cardtype As Integer = Convert.ToInt32(dsItems.Tables(0).Rows(0)("MinInstallments").ToString)
            Dim MaxInstallments_cardtype As Integer = Convert.ToInt32(dsItems.Tables(0).Rows(0)("MaxInstallments").ToString)

            Dim arr_Installments As New ArrayList

            For i As Integer = MinInstallments_merchant To MaxInstallments_merchant
                If (i >= MinInstallments_cardtype AndAlso i <= MaxInstallments_cardtype) Then
                    arr_Installments.Add(i)
                End If
            Next

            Dim status As Boolean = False

            For i As Integer = 0 To arr_Installments.Count - 1
                If CType(arr_Installments.Item(i), Integer) = Convert.ToInt32(hiddenInstallments.Text) Then
                    status = True
                    Exit For
                End If
            Next

            Return status
        Else
            Return True
        End If

    End Function

#End Region


    Protected Sub Tab3_btnComplete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Tab3_btnComplete.Click
        If Page.IsValid Then
            Dim CatchError As String = String.Empty
            Session("MakePaymentError") = String.Empty
            Dim TransID As Integer = 0
            Dim PublicTransID As String = String.Empty

            Try
                Dim w As New LivePay_ESBBridge.Bridge
                Dim obj As New LivePay_ESBBridge.PaymentObj
                Dim UserID As Integer = 0
                Dim UserEmail As String = Tab2_txtEmail.Text
                Dim USerPhone As String = Tab2_txtPhone.Text
                Dim CustomerName As String = Tab2_txtFullNameOwner.Text
                Dim CardNo As String = Tab2_txtCardNo.Text
                Dim CardCvv2 As String = Tab2_txtCVV2.Text
                Dim CardExpDate As String = String.Empty
                Dim CardDescrID As Integer = 0
                Dim ExpMonth As String = Tab2_DdlCardMonth.SelectedValue
                Dim ExpYear As String = Tab2_DdlCardYear.Text
                Dim CardID As Integer = 0



                Dim IsLogedIn As Boolean = CMSContext.CurrentUser.IsPublic()
                If IsLogedIn = False Then
                    UserID = CMSContext.CurrentUser.UserID
                    UserEmail = CMSContext.CurrentUser.Email
                    Try
                        USerPhone = CMSContext.CurrentUser.GetValue("UserPhone").ToString
                    Catch ex As Exception
                        USerPhone = ""
                    End Try
                End If

                If Tab2_RbSavedCard.Checked Then          '----- If Saved Cards is Checked
                    CardID = Split(Tab2_ddlSavedCard.SelectedValue, "_")(0)
                    Dim DtCard As DataTable = DBConnection.GetUserCard(UserID, CardID)
                    If DtCard.Rows.Count > 0 Then
                        CardNo = Left(DtCard.Rows(0)("CardNumber").ToString, 4) & CryptoFunctions.DecryptString(Right(DtCard.Rows(0)("CardNumber").ToString, (DtCard.Rows(0)("CardNumber").ToString.Length - 4)))
                        CardCvv2 = Tab2_txtCVV2Top.Text
                        ExpMonth = DtCard.Rows(0)("MonthExpiration").ToString
                        ExpYear = DtCard.Rows(0)("YearExpiration").ToString
                        CustomerName = DtCard.Rows(0)("FullName").ToString
                        CardDescrID = DtCard.Rows(0)("CardDescrID").ToString
                    End If
                Else
                    CardDescrID = Tab2_ddlCardType.SelectedValue
                End If
                If ExpMonth.Length = 1 Then
                    ExpMonth = String.Concat("0", ExpMonth)
                End If
                CardExpDate = String.Concat(ExpMonth, Right(ExpYear, 2))
                Dim Price As Decimal = Replace(Tab1_txtPrice.Text, ".", ",")
                Dim Doy As Integer = 0
                If Tab2_ChlIWantInvoice.Checked Then '--- If With Invoice is checked
                    Doy = ddlDoy.SelectedValue
                End If
                '--- Save New Card

                Dim EncryptedCard As String = Left(CardNo, 4) & CryptoFunctions.EncryptString(Right(CardNo, 12))
                If Tab2_ChkSaveDetails.Checked AndAlso Tab2_RbNewCard.Checked Then
                    Dim dtNewCard As DataTable = DBConnection.InsertNewCard(UserID, Tab2_txtFriendName.Text, CardDescrID, EncryptedCard, CustomerName, ExpMonth, ExpYear, Right(CardNo, 4))
                    If dtNewCard.Rows.Count > 0 Then
                        CardID = dtNewCard.Rows(0)("NewCardID").ToString
                    End If
                End If

                '-----------------
                '--Start--- I take the New Trans ID Status = 0 (before check)

                Dim CustomValue() As String

                'Dim dtTrans As DataTable = DBConnection.InsertMerchantTransaction(Me.MerchantID, UserID, CardID, EncryptedCard, CustomerName, Price, USerPhone, UserEmail, Tab2Inv_CompanyName.Text, _
                '                                                                  Tab2Inv_Occupation.Text, Tab2Inv_Address.Text, Tab2Inv_TK.Text, Tab2Inv_City.Text, Tab2Inv_AFM.Text, Doy, CardDescrID, 0)
                Dim dtTrans As DataTable = DBConnection.InsertMerchantTransaction(CMSContext.CurrentDocument.NodeID, UserID, CardID, EncryptedCard, CustomerName, Price, USerPhone, UserEmail, Tab2Inv_CompanyName.Text, _
                                                                  Tab2Inv_Occupation.Text, Tab2Inv_Address.Text, Tab2Inv_TK.Text, Tab2Inv_City.Text, Tab2Inv_AFM.Text, Doy, CardDescrID, 0, TransPublic)

                Dim TransGUID As Guid
                If dtTrans.Rows.Count > 0 Then
                    TransID = dtTrans.Rows(0)("TransID")
                    TransGUID = New Guid(dtTrans.Rows(0)("TransGUID").ToString)
                End If
                '--End--- I take the New Trans ID Status = 0 (before check)

                'JsAlert(Session("NewPayment_Merchant_LivePay_ID"))
                'JsAlert(UserID)
                'JsAlert(UserEmail)
                'JsAlert(USerPhone)
                'JsAlert(Tab1_txtNotes.Text)
                'JsAlert(CustomerName)
                'JsAlert(CardCvv2)
                'JsAlert(CardExpDate)

                'JsAlert(CardNo)
                'JsAlert(TransID)
                'JsAlert(Replace(Tab1_txtPrice.Text, ".", ","))
                'JsAlert(CardDescrID)

                'Response.Write(CardExpDate)
                'Response.End()

                obj.Public = TransPublic

                obj.MerchantId = Session("NewPayment_Merchant_LivePay_ID") ' Me.MerchantID ----- Na Bro To Merchant ID to kanoniko
                obj.CustomerId = UserID
                obj.CustomerEmail = UserEmail
                obj.CustomerTelephone = USerPhone
                obj.CustomerComments = Tab1_txtNotes.Text
                obj.CustomerName = CustomerName
                obj.CardCvv2 = CardCvv2
                obj.CardExpiryDate = CardExpDate
                obj.CardNumber = CardNo
                obj.TransactionId = TransID
                obj.TransactionAmount = Replace(Tab1_txtPrice.Text, ".", ",") '----
                obj.CardType = CardDescrID
                obj.Installments = Me.hiddenInstallments.Text

                'Response.Write("Me.hiddenInstallments.Text -> " & Me.hiddenInstallments.Text)

                'Dim ds As DataSet = DBConnection.GetMerchantCustomFields(Me.MerchantID)
                Dim ds As DataSet = DBConnection.GetMerchantCustomFields(CMSContext.CurrentDocument.NodeID)
                Dim dt As DataTable = ds.Tables(0)

                LivePayCustomField.RemoveFieldsCurrentUser(CMSContext.CurrentUser.UserID)

                Dim info1_Transaction As String = String.Empty

                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim ControlID As String = String.Concat("formUserControl_", i)
                    Dim MyControl As LivePayFormEngineUserControl = DirectCast(Me.FindControl(ControlID), LivePayFormEngineUserControl)
                    Dim ItemID As Integer = dt.Rows(i)("ItemID")
                    If Not IsNothing(MyControl) Then
                        Select Case i
                            Case 0
                                info1_Transaction = MyControl.Value
                                obj.Info1 = MyControl.Value
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 1
                                obj.Info2 = MyControl.Value
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 2
                                obj.Info3 = MyControl.Value
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 3
                                obj.Info4 = MyControl.Value
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 4
                                obj.Info5 = MyControl.Value
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 5
                                obj.Info6 = MyControl.Value
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 6
                                obj.Info7 = MyControl.Value
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 7
                                obj.Info8 = MyControl.Value
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 8
                                obj.Info9 = MyControl.Value
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                            Case 9
                                obj.Info10 = MyControl.Value
                                LivePayCustomField.Add(LivePayCustomField.AddField(CMSContext.CurrentUser.UserID, ItemID, MyControl.Value))
                        End Select
                    End If
                Next
                Dim IsValidCard As Boolean = True
                If CardBelongToOtherUser.Text <> "0" AndAlso Tab2_RbSavedCard.Checked = False Then
                    IsValidCard = False
                End If

                If IsValidCard Then

                    If 1 = 1 Then

                        Dim res As LivePay_ESBBridge.PaymentResponse = w.MakePayment(obj)
                        'Response.Write("ErrorDetails -> " & res.ErrorDetails)
                        'Response.Write("ErrorMessage -> " & res.ErrorMessage)
                        'Response.End()


                        Dim Result As Boolean = res.Result

                        If Result Then '---- Success
                            Dim TransactionsObj As New GetTransactionsObj
                            'Response.Write("Price  --> " & Price)
                            'Response.End()
                            TransactionsObj.CardNumber = CardNo
                            'TransactionsObj.CustomerId = UserID
                            TransactionsObj.AmountFrom = Price
                            TransactionsObj.amountTo = Price
                            TransactionsObj.MerchantId = CMSContext.CurrentDocument.GetValue("LivePayID")
                            TransactionsObj.TransactionDateFrom = DateTime.Now.AddHours(-3)
                            TransactionsObj.TransactionDateTo = DateTime.Now.AddHours(3)
                            TransactionsObj.info1 = info1_Transaction
                            TransactionsObj.PageSize = 10

                            Dim TransactionsResp As New GetTransactionsResponse

                            TransactionsResp = w.GetTransactions(TransactionsObj)
                            If TransactionsResp.Transactions IsNot Nothing Then
                                If TransactionsResp.Transactions.Length > 0 Then
                                    PublicTransID = TransactionsResp.Transactions(0).transactionId()
                                End If
                            End If

                            DBConnection.UpdateTransactionWithStatusMes(TransID, 2, 0, "Success", PublicTransID)
                            'Response.Write("PublicTransID - > " & PublicTransID)
                            'Response.End()
                            Dim MailHandler As New MailContext()

                            MailHandler.UserPhone = USerPhone
                            MailHandler.Transdate = Now
                            MailHandler.Transcode = "01" & TransID.ToString()
                            MailHandler.MerchantName = CMSContext.CurrentDocument.GetValue("DiscreetTitle")
                            MailHandler.MerchantID = Session("NewPayment_Merchant_LivePay_ID")
                            MailHandler.InvoiceBool = Tab2_ChlIWantInvoice.Checked

                            MailHandler.Comments = Tab1_txtNotes.Text
                            MailHandler.CardType = IIf(CardDescrID = 0, "Visa", IIf(CardDescrID = 1, "MasterCard", "EuroLine"))
                            MailHandler.CardNumber = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                            MailHandler.CardName = CustomerName
                            MailHandler.CardExpDate = CardExpDate
                            MailHandler.CardEmail = UserEmail
                            MailHandler.Amount = Tab1_txtPrice.Text
                            MailHandler.Installments = hiddenInstallments.Text
                            MailHandler.SendEmail("USERCARDSUCC", "info@livepay.gr", UserEmail)


                            MailHandler.UserPhone = USerPhone
                            MailHandler.Transdate = Now
                            MailHandler.Transcode = "01" + TransID.ToString()
                            MailHandler.MerchantName = CMSContext.CurrentDocument.GetValue("DiscreetTitle")
                            MailHandler.MerchantID = Session("NewPayment_Merchant_LivePay_ID")
                            Dim boolString As String = "NAI"
                            If Not Tab2_ChlIWantInvoice.Checked Then
                                boolString = "ΟΧΙ"
                            End If
                            MailHandler.InvoiceBool = boolString
                            MailHandler.Comments = Tab1_txtNotes.Text
                            MailHandler.CardType = IIf(CardDescrID = 0, "Visa", IIf(CardDescrID = 1, "MasterCard", "EuroLine"))
                            MailHandler.CardNumber = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                            MailHandler.CardName = CustomerName
                            MailHandler.CardExpDate = CardExpDate
                            MailHandler.CardEmail = UserEmail
                            MailHandler.Amount = Tab1_txtPrice.Text
                            MailHandler.SendEmail("MERCHCARDSUCC", "info@livepay.gr", GetMerchantUserEmail())


                            Session("UserTransactions") = Session("UserTransactions") + 1
                            Session("MakePaymentError") = String.Empty

                            'Response.Write("PublicTransID --> " & PublicTransID)
                            'Response.End()
                            Response.Redirect("~/TransactionReceipt.aspx?TransID=" & TransGUID.ToString)
                        Else '--- Failed

                            Dim MailHandler As New MailContext()

                            MailHandler.UserPhone = USerPhone
                            MailHandler.Transdate = Now
                            MailHandler.Transcode = "01" & TransID
                            MailHandler.MerchantName = CMSContext.CurrentDocument.GetValue("DiscreetTitle")
                            MailHandler.MerchantID = Session("NewPayment_Merchant_LivePay_ID")
                            Dim boolString As String = "NAI"
                            If Not Tab2_ChlIWantInvoice.Checked Then
                                boolString = "ΟΧΙ"
                            End If
                            MailHandler.InvoiceBool = boolString
                            MailHandler.Comments = Tab1_txtNotes.Text
                            MailHandler.CardType = IIf(CardDescrID = 0, "Visa", IIf(CardDescrID = 1, "MasterCard", "EuroLine"))
                            MailHandler.CardNumber = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                            MailHandler.CardName = CustomerName
                            MailHandler.CardExpDate = CardExpDate
                            MailHandler.CardEmail = UserEmail
                            MailHandler.Amount = Tab1_txtPrice.Text
                            MailHandler.SendEmail("USERACCFAIL", "info@livepay.gr", CMSContext.CurrentUser.Email)



                            DBConnection.UpdateTransactionWithStatusMes(TransID, 1, 0, res.ErrorCode & " : " & res.ErrorMessage, PublicTransID)
                            Session("MakePaymentError") = res.ErrorCode
                            Response.Redirect("~/TransactionReceipt.aspx?Result=ESBerror")
                        End If
                    End If
                Else
                    Dim MailHandler As New MailContext()

                    MailHandler.UserPhone = USerPhone
                    MailHandler.Transdate = Now
                    MailHandler.Transcode = "01" & TransID
                    MailHandler.MerchantName = CMSContext.CurrentDocument.GetValue("DiscreetTitle")
                    MailHandler.MerchantID = Session("NewPayment_Merchant_LivePay_ID")
                    Dim boolString As String = "NAI"
                    If Not Tab2_ChlIWantInvoice.Checked Then
                        boolString = "ΟΧΙ"
                    End If
                    MailHandler.InvoiceBool = boolString
                    MailHandler.Comments = Tab1_txtNotes.Text
                    MailHandler.CardType = IIf(CardDescrID = 0, "Visa", IIf(CardDescrID = 1, "MasterCard", "EuroLine"))
                    MailHandler.CardNumber = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                    MailHandler.CardName = CustomerName
                    MailHandler.CardExpDate = CardExpDate
                    MailHandler.CardEmail = UserEmail
                    MailHandler.Amount = Tab1_txtPrice.Text
                    MailHandler.SendEmail("USERACCFAIL", "info@livepay.gr", CMSContext.CurrentUser.Email)

                    Dim UserError As String = ResHelper.LocalizeString(String.Concat("{$=Η κάρτα έχει αποθηκευτεί από άλλο χρήστη|en-us=Η κάρτα έχει αποθηκευτεί από άλλο χρήστη$}"))

                    DBConnection.UpdateTransactionWithStatusMes(TransID, 1, 0, UserError, PublicTransID) '------ Update Transaction Set Status = 1 (Failed)
                    Session("MakePaymentError") = UserError
                    Response.Redirect("~/TransactionReceipt.aspx?Result=CatchError")
                End If


            Catch ex As Exception
                CatchError = ex.ToString
            End Try

            If CatchError <> String.Empty Then
                DBConnection.UpdateTransactionWithStatusMes(TransID, 1, 0, CatchError, PublicTransID) '------ Update Transaction Set Status = 1 (Failed)
                Session("MakePaymentError") = CatchError
                Response.Redirect("~/TransactionReceipt.aspx?Result=CatchError")
            End If



            'Dim ErrorMessage As String = String.Concat("<font color='red'>", res.ErrorCode, "</font>:", res.ErrorMessage)

        End If
    End Sub

    Private Function GetMerchantUserEmail() As String
        Dim LivePayID As Integer = CMSContext.CurrentDocument.GetValue("LivePayID")

        Dim ds As DataSet = UserInfoProvider.GetUsers("LivePayID = " & LivePayID, "UserCreated", 1, "Email", ConnectionHelper.GetConnection())

        If Not DataHelper.DataSourceIsEmpty(ds) Then
            Return ds.Tables(0).Rows(0)("Email")
        Else
            Return CMSContext.CurrentDocument.GetValue("Email")
        End If

    End Function

    Private Sub JsAlert(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, String.Concat(" alert('", Script, "'); "), True)
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

    Private Function GetTypeCard(ByVal where As String, ByVal orderby As String) As DataSet
        Dim customTableClassName As String = "LivePay.TypeCard"

        ' Get data class using custom table name
        Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass(customTableClassName)

        If customTableClassInfo Is Nothing Then
            Throw New Exception("Given custom table does not exist.")
        End If

        ' Initialize custom table item provider with current user info and general connection
        Dim ctiProvider As New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())

        ' Get custom table items
        Dim dsItems As DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, where, orderby)

        Return dsItems

    End Function

    Private Sub GetInstallments()
        Dim cardType As StringBuilder = New StringBuilder()
        Dim MinInstallments_merchant As Integer = CMSContext.CurrentDocument.GetValue("MinInstallments")
        Dim MaxInstallments_merchant As Integer = CMSContext.CurrentDocument.GetValue("MaxInstallments")


        Dim dsItems As DataSet = GetTypeCard("active = 1", "MinInstallments")

        For Each row As DataRow In dsItems.Tables(0).Rows
            If cardType.Length > 0 Then cardType.Append(", ")
            cardType.AppendFormat("{{""index"":{0}, min: ""{1}"", max: ""{2}""}}", row("itemid").ToString.ToLower(), row("MinInstallments").ToString, row("MaxInstallments").ToString)
        Next



        Dim merchantInstallments As String = String.Format("{{min: {0}, max: {1}}}", MinInstallments_merchant, MaxInstallments_merchant)
        Dim Installments As String = String.Format("var _installment = {{merchant: {0}, cardtype:{1}}}; ", merchantInstallments, String.Concat("[", cardType.ToString(), "]"))
        'Response.Write(Installments)
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), Guid.NewGuid().ToString(), Installments, True)

        'String.Format("var Installments = {merchant:{min:1, max:5}, cardtype:{visa:{min:1, max:20}, master:{min:20, max:25}}}")

        'Dim MinInstallments_merchant As Integer = CMSContext.CurrentDocument.GetValue("MinInstallments")
        'Dim MaxInstallments_merchant As Integer = CMSContext.CurrentDocument.GetValue("MaxInstallments")

        'Dim customTableClassName As String = "Merchant.LivePay_Merchant"

        '' Get data class using custom table name
        'Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass(customTableClassName)

        'If customTableClassInfo Is Nothing Then
        '    Throw New Exception("Given custom table does not exist.")
        'End If

        '' Initialize custom table item provider with current user info and general connection
        'Dim ctiProvider As New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())

        '' Get custom table items
        'Dim dsItems As DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, "active = 1 and ItemID = " & Tab2_ddlCardType.SelectedIndex, "MinInstallments")



        'Dim MinInstallments_Card As Integer = CMSContext.CurrentDocument.GetValue("MinInstallments")
        'Dim MaxInstallments_Card As Integer = CMSContext.CurrentDocument.GetValue("MaxInstallments")


    End Sub


    ''' <summary>
    ''' Create custom form control
    ''' </summary>
    ''' <param name="pathToControl">Path custom control</param>
    ''' <param name="CustomTextBoxDiv"></param>
    ''' <param name="i">Row Data Table</param>
    ''' <remarks></remarks>
    Private Sub CreateCustomFormControl(ByVal pathToControl As String, _
                                        ByVal CustomTextBoxDiv As Object, _
                                        ByVal i As Integer, _
                                        ByVal ErrorMsgGr As String, _
                                        ByVal ErrorMsgEn As String, _
                                        ByVal RegExp As String, _
                                        ByVal Mandatory As Boolean, _
                                        ByVal MaxLength As Integer, _
                                        ByVal TypePayment As Integer, _
                                        ByVal OrganismCode As Integer, _
                                        ByVal NDate As Integer, _
                                        ByVal CustomFieldValue As String)


        If File.Exists(HttpContext.Current.Request.MapPath(ResolveUrl(pathToGroupselector))) Then
            Dim CustomFT As New CustomFieldsType()
            Dim ctrl As LivePayFormEngineUserControl = Me.LoadControl(pathToControl)
            If ctrl IsNot Nothing Then
                Dim strError As String = "CustomCloud_"
                ctrl = Me.LoadControl(pathToControl)
                ctrl.ID = "formUserControl_" & i
                ctrl.ValidationError = ResHelper.LocalizeString(String.Concat("{$=Συμπληρώστε το πεδίο <b>", ErrorMsgGr, "</b>|en-us=Complete the field <b>", ErrorMsgEn, "</b>$}"))
                ctrl.RowCustomField = i
                ctrl.RegularExp = RegExp
                ctrl.Mandatory = Mandatory
                ctrl.MaxLength = MaxLength
                ctrl.TypePayment = TypePayment
                ctrl.OrganismCode = OrganismCode
                ctrl.NDate = NDate

                If CustomFieldValue <> String.Empty Then
                    ctrl.Value = CustomFieldValue
                End If


                CustomFT.TypeName = ctrl.ID.ToString()
                CustomFT.ErrorDivID = strError & i

                FieldType.Add(CustomFT)

                CustomTextBoxDiv.Controls.Add(ctrl)
            End If
        End If


    End Sub

End Class


