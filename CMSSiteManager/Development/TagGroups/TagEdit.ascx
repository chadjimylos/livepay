<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TagEdit.ascx.cs" Inherits="CMSSiteManager_Development_TagGroups_TagEdit" %>
<asp:Label ID="lblInfo" runat="server" Visible="false" CssClass="InfoLabel" EnableViewState="false" />
<asp:Label ID="lblError" runat="server" Visible="false" CssClass="ErrorLabel" EnableViewState="false" />
<table>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblDisplayName" runat="server" CssClass="FieldLabel" ResourceString="general.displayname"
                DisplayColon="true" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtDisplayName" runat="server" MaxLength="250" CssClass="TextBoxField"
                EnableViewState="false" />
            <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName"
                Display="Dynamic" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblCodeName" runat="server" CssClass="FieldLabel" ResourceString="general.codename"
                DisplayColon="true" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtCodeName" runat="server" MaxLength="250" CssClass="TextBoxField"
                EnableViewState="false" />
            <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtCodeName"
                Display="Dynamic" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top;">
            <cms:LocalizedLabel ID="lblDescription" runat="server" CssClass="FieldLabel" ResourceString="general.description"
                DisplayColon="true" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtDescription" runat="server" CssClass="TextAreaField" TextMode="MultiLine"
                EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            <cms:LocalizedButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton"
                ResourceString="general.ok" EnableViewState="false" />
        </td>
    </tr>
</table>
