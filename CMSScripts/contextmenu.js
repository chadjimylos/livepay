﻿var menuTimers = new Array();
var menuElements = new Array();
var dynamicMenus = new Array();
var menuLevels = new Array(10);
var loadingContent = new Array();
var mouseOverTimers = new Array();
var timerParameters = new Array();
var menuParameters = new Array();

for (i = 0; i < 10; i++) {
    menuLevels[i] = new Array();
}

var x = 0;
var y = 0;
var show = true;
var currentMenuId = null;
var currentElem = null;
var cursorOnMenu = false;

function DisableRight() { return ((window.event != null) ? (window.event.button != 2) : false) }
function DoNothing() { return false; }

function GetScrollX() {
    if (typeof (window.pageXOffset) == 'number') {
        return window.pageXOffset;
    }
    else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
        return document.body.scrollLeft;
    }
    else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
        return document.documentElement.scrollLeft;
    }

    return 0;
}

function GetScrollY() {
    if (typeof (window.pageYOffset) == 'number') {
        return window.pageYOffset;
    }
    else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
        return document.body.scrollTop;
    }
    else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
        return document.documentElement.scrollTop;
    }

    return 0;
}

function GetMouseX(ev) {
    return ev.clientX + GetScrollX();
}

function GetMouseY(ev) {
    return ev.clientY + GetScrollY();
}

function GetElementY(oElement) {
    var floats = false;
    var iReturnValue = 0;
    while (oElement != null) {
        if (oElement.style) {
            // Check floating
            var floatValue = ((oElement.style.cssFloat != null) ? oElement.style.cssFloat : oElement.style.styleFloat);
            if ((floatValue == 'left') || (floatValue == 'right')) {
                floats = true;
            }
            if (floats && (oElement != document.documentElement) && !IsSafari()) {
                iReturnValue -= oElement.scrollTop;
            }

            if (oElement.style.position == 'absolute') {
                return iReturnValue;
            }

            iReturnValue += oElement.offsetTop;
        }

        // Check parents behind the offset
        if (oElement.parentNode != oElement.offsetParent) {
            var parent = oElement.parentNode;
            while ((parent != null) && (parent != oElement.offsetParent)) {
                if (parent.style) {
                    // Check floating
                    var floatValue = ((parent.style.cssFloat != null) ? parent.style.cssFloat : parent.style.styleFloat);
                    if ((floatValue == 'left') || (floatValue == 'right')) {
                        floats = true;
                    }
                    if (floats && (parent != document.documentElement)) {
                        iReturnValue -= parent.scrollTop;
                    }
                }

                parent = parent.parentNode;
            }
        }

        oElement = oElement.offsetParent;
    }

    return iReturnValue;
}

function GetElementX(oElement) {
    var floats = false;
    var iReturnValue = 0;
    while (oElement != null) {
        if (oElement.style) {
            // Check floating
            var floatValue = ((oElement.style.cssFloat != null) ? oElement.style.cssFloat : oElement.style.styleFloat);
            if ((floatValue == 'left') || (floatValue == 'right')) {
                floats = true;
            }
            if (floats && (oElement != document.documentElement) && !IsSafari()) {
                iReturnValue -= oElement.scrollLeft;
            }

            if (oElement.style.position == 'absolute') {
                return iReturnValue;
            }

            iReturnValue += oElement.offsetLeft;
        }

        // Check parents behind the offset
        if (oElement.parentNode != oElement.offsetParent) {
            var parent = oElement.parentNode;
            while ((parent != null) && (parent != oElement.offsetParent)) {
                if (parent.style) {
                    // Check floating
                    var floatValue = ((parent.style.cssFloat != null) ? parent.style.cssFloat : parent.style.styleFloat);
                    if ((floatValue == 'left') || (floatValue == 'right')) {
                        floats = true;
                    }
                    if (floats && (parent != document.documentElement)) {
                        iReturnValue -= parent.scrollLeft;
                    }
                }

                parent = parent.parentNode;
            }
        }

        oElement = oElement.offsetParent;
    }

    return iReturnValue;
}

function IsIE() {
    return /*@cc_on!@*/false;
}

function IsGecko() {
    var s = navigator.userAgent.toLowerCase() + '';
    if (s.indexOf('gecko/') >= 0) {
        return true;
    }
    return false;
}

function IsSafari() {
    var s = navigator.userAgent.toLowerCase() + '';
    if (s.indexOf('safari/') >= 0) {
        return true;
    }
    return false;
}

function IsIE7() {
    var s = navigator.userAgent.toLowerCase() + '';
    if (s.indexOf('msie 7.0') >= 0) {
        return true;
    }
    return false;
}


function GetLeftButton() {
    var s = navigator.userAgent.toLowerCase() + '';
    if ((s.indexOf('gecko/') >= 0) || (s.indexOf('opera/') >= 0)) {
        return 0;
    }
    else {
        return 1;
    }
}

function PrepareContextMenu(ev) {
    if (ev != null) {
        var menuElem = document.getElementById(currentMenuId);
        if (menuElem != null) {
            if ((document.getElementById(currentMenuId + "_horizontal").value == "Cursor")) {
                x = GetMouseX(ev) + 1;
            }
            if ((document.getElementById(currentMenuId + "_vertical").value == "Cursor")) {
                y = GetMouseY(ev) + 1;
            }

            show = false;
            menuButtons = document.getElementById(currentMenuId + "_button").value;
            if (((menuButtons == "Right") || (menuButtons == "Both")) && (ev.button == 2)) {
                show = true;
            }
            if (((menuButtons == "Left") || (menuButtons == "Both")) && (ev.button == GetLeftButton())) {
                show = true;
            }
        }
    }
}


function ReturnFalse() {
    return false;
}

function DisableContextMenu(elem) {
    if (elem != null) {
        elem.oncontextmenu = ReturnFalse;
    }
}

function InitContextMenu(menuId, elem) {
    currentMenuId = menuId;
    if (elem != null) {
        if (!elem.contextMenuInitialized) {
            elem.contextMenuInitialized = true;
            elem.onmousedown = PrepareContextMenu;
            elem.oncontextmenu = ReturnFalse;
        }
    }
}

function ContextMenu(menuId, elem, param, forceShow, mousex, mousey) {
    var menuElem = document.getElementById(menuId);
    if (menuElem != null) {
        if (forceShow) {
            show = true;
        }
        if (elem != null) {
            currentElem = elem;
        }

        if (mouseOverTimers[menuId] != null) {
            clearInterval(mouseOverTimers[menuId]);
            mouseOverTimers[menuId] = null;
        }

        if (currentMenuId != null) {
            var menuLevel = parseInt(document.getElementById(currentMenuId + "_level").value);
            HideContextMenus(menuLevel);
        }

        HideContextMenu(menuId);

        if (window.event != null) {
            mousex = GetMouseX(window.event);
            mousey = GetMouseY(window.event);

            if (!forceShow) {
                show = false;
                var menuButtons = document.getElementById(currentMenuId + "_button").value;
                if (((menuButtons == "Right") || (menuButtons == "Both")) && (window.event.button == 2)) {
                    show = true;
                }
                if (((menuButtons == "Left") || (menuButtons == "Both")) && (window.event.button == GetLeftButton())) {
                    show = true;
                }
            }
        }

        if ((mousex != null) && (document.getElementById(menuId + "_horizontal").value == "Cursor")) {
            x = mousex;
        }
        if ((mousey != null) && (document.getElementById(menuId + "_vertical").value == "Cursor")) {
            y = mousey;
        }

        switch (document.getElementById(menuId + "_vertical").value) {
            case "Top":
                y = GetElementY(currentElem);
                break;

            case "Bottom":
                y = GetElementY(currentElem) + currentElem.offsetHeight;
                break;

            case "Middle":
                y = GetElementY(currentElem) + currentElem.offsetHeight / 2;
                break;
        }

        var rtl = (document.body.className.indexOf('RTL') >= 0);
        switch (document.getElementById(menuId + "_horizontal").value) {
            case "Left":
                x = GetElementX(currentElem);
                if (rtl) {
                    if (IsIE7()) {
                        // Icon offset of content tree menu for IE7
                        if (menuId == "nodeMenu" && menuLevel == 0) {
                            x += 16;
                            break;
                        }
                    }
                        x += currentElem.offsetWidth;                   
                }
                break;

            case "Right":
                x = GetElementX(currentElem);
                if (!rtl) {
                    currentElem.offsetWidth;
                }
                break;

            case "Center":
                x = GetElementX(currentElem) + currentElem.offsetWidth / 2;
                break;
        }

        if (rtl) {
            x -= parseInt(document.getElementById(menuId + "_offsetx").value);
        }
        else {
            x += parseInt(document.getElementById(menuId + "_offsetx").value);
        }
        y += parseInt(document.getElementById(menuId + "_offsety").value);

        if (show) {
            SetContextMenuParameter(menuId, param);

            if (dynamicMenus[menuId] != null) {
                loading = loadingContent[menuId];
                if (loading != null) {
                    menuElem.innerHTML = loading;
                }
                else {
                    menuElem.innerHTML = "";
                }
                dynamicMenus[menuId](param);
            }
            menuElem.style.display = 'block';
            if (rtl) {
                x -= menuElem.offsetWidth;
            }
            menuElem.style.left = x + 'px';
            menuElem.style.top = y + 'px';

            if (dynamicMenus[menuId] == null) {
                menuTimers[menuId] = setTimeout("HideContextMenu('" + menuId + "');", 2000);
            }
            var activecss = document.getElementById(menuId + '_activecss').value;
            if (activecss != '') {
                currentElem.oldClassName = currentElem.className;
                currentElem.className = activecss;
            }
            menuElements[menuId] = currentElem;
        }
    }
}

function CloseContextMenu(menuId, timeout) {
    menuTimers[menuId] = setTimeout("HideContextMenu('" + menuId + "', true);", timeout);
}

function CancelCloseContextMenu(menuId) {
    if (menuTimers[menuId] != null) {
        clearTimeout(menuTimers[menuId]);
        menuTimers[menuId] = null;
    }
}

function HideContextMenu(menuId, hideSubmenus) {
    var menuElem = document.getElementById(menuId);
    if (menuElem != null) {
        if (menuTimers[menuId] != null) {
            if (hideSubmenus) {
                var menuLevel = parseInt(document.getElementById(menuId + "_level").value);
                HideContextMenus(menuLevel + 1);
            }

            CancelCloseContextMenu(menuId);

            if (mouseOverTimers[menuId] != null) {
                clearInterval(mouseOverTimers[menuId]);
                mouseOverTimers[menuId] = null;
            }
            if (menuElements[menuId] != null) {
                if (currentElem.oldClassName != null) {
                    menuElements[menuId].className = menuElements[menuId].oldClassName;
                }
                else {
                    menuElements[menuId].className = '';
                }
                menuElements[menuId] = null;
            }

            var menuElem = document.getElementById(menuId);
            if (menuElem != null) {
                menuElem.style.display = 'none';
            }
        }
    }
}

function SetContextMenuParameter(menuId, param) {
    menuParameters[menuId] = param;
    var paramElem = document.getElementById(menuId + '_parameter');
    if (paramElem != null) {
        paramElem.value = param;
    }
    return null;
}

function GetContextMenuParameter(menuId) {
    return menuParameters[menuId];

    var paramElem = document.getElementById(menuId + '_parameter');
    if (paramElem != null) {
        return paramElem.value;
    }
    return null;
}

function ReceiveContextMenu(rvalue, context) {
    var menuElem = document.getElementById(context);
    if (menuElem != null) {
        menuElem.innerHTML = rvalue;
    }
    menuTimers[context] = setTimeout("HideContextMenu('" + context + "');", 2000);
}

function HideContextMenus(startLevel) {
    for (i = menuLevels.length - 1; i >= startLevel; i--) {
        var level = menuLevels[i];
        for (j = 0; j < level.length; j++) {
            HideContextMenu(level[j]);
        }
    }
}

function HideAllContextMenus() {
    if (!cursorOnMenu) {
        HideContextMenus(0);
    }
}

function ContextMouseOut(menuId, elem, param) {
    if (mouseOverTimers[menuId] != null) {
        clearInterval(mouseOverTimers[menuId]);
        mouseOverTimers[menuId] = null;
    }

    cursorOnMenu = false;
    return false;
}

function ContextMouseOver(menuId, elem, param) {
    InitContextMenu(menuId, elem);

    var menuElem = document.getElementById(menuId);
    if (menuElem != null) {
        if (document.getElementById(menuId + "_mouseover").value == "1") {
            currentElem = elem;

            if ((document.getElementById(menuId).style.display != 'none') && (timerParameters[menuId] != param)) {
                HideContextMenu(menuId, true);
            }

            if (document.getElementById(menuId).style.display == 'none') {
                timerParameters[menuId] = null;
                if (mouseOverTimers[menuId] == null) {
                    timerParameters[menuId] = param;
                    if (param == "") {
                        param = "''";
                    }
                    if (window.event != null) {
                        param += ", true";
                        param += ", " + GetMouseX(window.event);
                        param += ", " + GetMouseY(window.event);
                    }
                    mouseOverTimers[menuId] = setInterval("ContextMenu('" + menuId + "', null, " + param + ");", 1000);
                }
            }
        }
    }

    cursorOnMenu = true;
    return false;
}

function ContextMouseUp(menuId, elem, param) {
    ContextMenu(menuId, elem, param);

    return false;
}

if (typeof (Sys) != "undefined" && typeof (Sys.Application) != "undefined") {
    Sys.Application.notifyScriptLoaded();
}