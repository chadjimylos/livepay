﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSearchHistPay.ascx.vb" Inherits="CMSTemplates_LivePay_Merchants_UCSearchHistPay" %>
<%@ Register Src="~/CMSTemplates/livepay/PopUp/UCTransDetails.ascx" TagName="TransDetails" TagPrefix="uc"  %>
<script src="../../../CMSScripts/LivePay/RegisteredUsers.js" type="text/javascript"></script>

<% 
    ' If UserSeesFullVersion() Then
    '     Response.Write("Sees Full Version")
    '     Response.Write(StripHTMLFunctions.StripTags(txtMerchantID.Text))
    ' Else
    '     Response.Write("Not Full Version")
    '     Response.Write(CMSContext.CurrentUser.GetValue("LivePayID"))
    ' End If

    Response.Write(CMSContext.CurrentUser.GetValue("LivePayID"))


    ' Uncomment to see if in Eurobanking Mode or not
    If CMSContext.CurrentUser.IsInRole("EurophoneBanking", "LivePay") Then
        Response.Write("<h1 style='color:white'>I AM in EurophoneBanking Role</h1>")
    Else
        Response.Write("<h1 style='color:white'>I AM NOT in EurophoneBanking Role</h1>")
    End If
%>  

<script type="text/javascript">

    var event_c;
    var obj_c;

    function handle_keydownHome(e) {

       
    }

    var TestControl = '<%=txtRefund.clientID %>'
     function ShowPleaseWhait() {
        $.blockUI({
            message: '<b>Αποθήκευση σε PDF..</b>',
            fadeIn: 700,
            fadeOut: 700,
           
            showOverlay: false,
            centerY: false,
            css: {
                width: '190px',
                top: '10px',
                left: '',
                right: '10px',
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .6,
                color: '#fff'
            }
        });
    }

function integer(str) {
    if (isNaN(str.value))
        str.value = '';
//    var re = new RegExp("\\d{7}", "g");
//    var m = re.exec(re);
//    if (m)
//        str.value = str;      
//    else
//        str.value = "";

}

function ShowTransDetails() {
    $('#TransDetailsPopUp').LivePayLightBox({css:{backgroundColor: 'transparent'}});
    //$.blockUI({ css: { top: '10', border: '0px', backgroundColor: 'transparent', cursor: 'default' }, message: $('#TransDetailsPopUp') });
}

function ShowTransRefunt() {
    $('#TransRefundPopup').LivePayLightBox();
    //$.blockUI({ css: { top: '10', border: '0px', backgroundColor: 'transparent', cursor: 'default' }, message: $('#TransRefundPopup') });
}

function ShowTransChanges() {
    $('#TransChangesPopup').LivePayLightBox();
    //$.blockUI({ css: { top: '10', border: '0px', backgroundColor: 'transparent', cursor: 'default' }, message: $('#TransChangesPopup') });
}


function CloseTheBatch() {
    return confirm('<%=ResHelper.LocalizeString("{$=Επιθυμείτε το κλείσιμο του πακέτου?|en-us=Do you wish to submit the Current Batch?$}") %>')
}


function ExportFile(url) {
    document.getElementById('<%=exportFrame.ClientID %>').src = '/CMSTemplates/livepay/' + url;
    //return false
}



function Hist_RebindRpt(CachValue) {
  //  setTimeout("Hist_ExecSearch('" + CachValue + "')", 400)

    if (CheckAction(event_c, obj_c)) {
        CachValue = CachValue.replace(/[']/gi, "@@@@")
        setTimeout("Hist_ExecSearch('" + CachValue + "')", 400)
    } else {
        ControlRows(event_c, obj_c)
    }
}

function Hist_ExecSearch(CachValue) {
    var RealValue = document.getElementById('<%=txtMerchant.ClientID %>').value
    RealValue = RealValue.replace(/[']/gi, "@@@@") 

    if (RealValue == CachValue) {
        var text = document.getElementById('<%=txtMerchant.clientID %>');

        if (text.value != null && text.value.length > 0) {
            GetMerchantHistData(RealValue)
        } else {
            document.getElementById('<%=txtMerchantID.clientID %>').value = ''
            Hist_ShowHideQuickRes(false, 0)
        }
    }
}

function GetMerchantHistData(val) {
    var x;
    if (window.ActiveXObject) {
        x = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else {
        x = new XMLHttpRequest();
    }
    val = escape(val)
    x.open("GET", "/CMSTemplates/LivePay/xmlHttpMerchants.aspx?MerchantKeyWord=" + val + "&lang=" + '<%=request("lang") %>', true);
    x.onreadystatechange = function () {
        if (x.readyState == 4) {
            var Results = x.responseText;
            var RowCount = Results.split("#]")[0]
            Results = Results.split("#]")[1]

            if (RowCount.length > 0) {
                document.getElementById('Hist_QuickSrcResAll').innerHTML = Results;
                Hist_ShowHideQuickRes(true, RowCount);
            } else {
                Hist_ShowHideQuickRes(false, 0);
            }

        }
    }
    x.send(null);
}

function Hist_ShowHideQuickRes(IsVisible, RowCount) {
    if (IsVisible) {
        
        if (RowCount == 1)
            document.getElementById('Hist_QuickSrcResMain').className = 'QuickSrcResMain_1'
        if (RowCount == 2)
            document.getElementById('Hist_QuickSrcResMain').className = 'QuickSrcResMain_2'
        if (RowCount > 2)
            document.getElementById('Hist_QuickSrcResMain').className = 'QuickSrcResMain'

        document.getElementById('Hist_QuickResDiv').style.display = '';
        ControlRows(event_c, obj_c)
    }
    else {
        document.getElementById('Hist_QuickResDiv').style.display = 'none';
    }
}

function SetSuggestValue(txt, Val) {
    document.getElementById('<%=txtMerchant.clientID %>').value = txt;
    document.getElementById('<%=txtMerchantID.clientID %>').value = Val;
    document.getElementById('Hist_QuickResDiv').style.display = 'none';
}

function ControlRows(e, obj) {
    var keynum = 0;
    var QuickResDiv = document.getElementById('Hist_QuickResDiv')
    if (window.event) { keynum = e.keyCode; }  // IE (sucks)
    else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

    if (keynum == 38) { // up
        //Move selection up
        if (obj.value.length > 0 && QuickResDiv.style.display != 'none') {

            ChangeSelection(-1)
        }
    }
    if (keynum == 40) { // down
        //Move selection down
        if (obj.value.length > 0 && QuickResDiv.style.display != 'none') {
            ChangeSelection(1)
        }
    }
    if (keynum == 13) { // enter
        if (obj.value.length > 0) {
            var IsSelected = false
            var SelectedObj = null;
            var AllHtml = $('.QuickSrcResTitle').get()

            $.each(AllHtml, function (index, obj) { //-- Check If Menu Is Selected
                if ($(obj).attr("IsSelected") == 'true') {
                    IsSelected = true
                    SelectedObj = obj
                }
            });

            if (IsSelected == false) { // if nothing is selected go to search page
                if (window.event) {
                    return handle_keydownHome(event);
                }
                else if (e.which) {
                    return handle_keydownHome(e);
                }    // Netscape/Firefox/Opera
            } else {

             $(SelectedObj).find("a").click()
            
            }
        }
    }
}

function ChangeSelection(GoTo) {

    var LastSelected = -1
    //QuickSrcResTitle
    var IsSelected = false
    var AllHtml = $('.QuickSrcResTitle').get()

    $.each(AllHtml, function (index, obj) { //-- Check If Menu Is Selected
        if ($(obj).attr("IsSelected") == 'true') {
            IsSelected = true
            LastSelected = index
        }
    });

    if (IsSelected == false) { //-- If Not Selected Select Firstone
        $.each(AllHtml, function (index, obj) {
            if (index == 0) {
                $(obj).attr("IsSelected", "true")
                $(obj).css("background-color", "#d2d6db")
            }
        });
    } else {
        $.each(AllHtml, function (index, obj) {
            var GetNextSelection = LastSelected + GoTo

            if (GetNextSelection > -1 && GetNextSelection < 3) {
                $(obj).attr("IsSelected", "false")
                $(obj).css("background-color", "transparent")

                if (index == GetNextSelection) {
                    $(obj).attr("IsSelected", "true")
                    $(obj).css("background-color", "#d2d6db")
                }
            }
        });
    }

}

function CheckAction(e, obj) {
    var keynum = 0;
    if (window.event) { keynum = e.keyCode; }  // IE (sucks)
    else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

    var IsSearching = true

    if (keynum == 38) { // up
        IsSearching = false
    }

    if (keynum == 40) { // down
        IsSearching = false
    }

    if (keynum == 13) { // enter
        IsSearching = false
    }

    return IsSearching
}

function Setent(e) {
    var keynum = 0;
    if (window.event) { keynum = e.keyCode; }  // IE (sucks)
    else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

    if (keynum == 13) {
        return false
    } else {
        return true
    }


}
</script>


<style type="text/css">
div.growlUI { background: url(check48.png) no-repeat 10px 10px;width:200px }
div.growlUI h1, div.growlUI h2 {font-size:12px;color: white;text-align: left;width:200px}
.test
{
    display:none;
}
.PT20{padding-top:0px}
</style>


<asp:UpdatePanel ID="uptPanel" runat="server" UpdateMode="Always" >
<ContentTemplate>
<asp:TextBox ID="txtRefund" runat="server" style="display:none" ></asp:TextBox>
<div id="TransDetailsPopUp" style="display:none; margin-top:20px;">
        <uc:TransDetails ID="TransDetails" runat="server" />
</div>
<div id="TransRefundPopup" style="display: none;">
        <uc:TransDetails ID="TransRefunt" runat="server" />
</div>

<div id="TransChangesPopup" style="display:none;">
    <uc:TransDetails ID="TransChanges" runat="server" />
</div>



<asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
<div class="SrHistPayBG">
    <div class="SrHistPayTitle"><%= ResHelper.LocalizeString("{$=Ιστορικό Συναλλαγών|en-us=Payment History$}")%></div>
    <div class="SrHistPaySrcTitle"><%= ResHelper.LocalizeString("{$=Αναζήτηση Συναλλαγών|en-us=Payment$}")%></div>
    <div class="SrHistPaySrcDescr"><%=ResHelper.LocalizeString("{$=Στη σελίδα αυτή μπορείτε να δείτε όλες τις συναλλαγές που έχουν πραγματοποιηθεί στην επιχείρησή σας μέσω του Livepay.gr. Χρησιμοποιείστε τα παρακάτω κριτήρια αναζήτησης για να περιορίσετε τα αποτελέσματα|en-us=You can see your incoming payments here. Please use the following search criteria to minimize the results.$}") %></div>
    <div style="padding-top:8px;padding-top:expression(5)"><img src="/app_themes/LivePay/SrcBorderForm.png"</div>
    <div>
       <div class="SrHistPaySrcDtmTitle"><%=ResHelper.LocalizeString("{$=Αναζήτηση σε|en-us=Search in$}") %>:</div>
       <div class="SrHistPaySrcDtmRBL">
        <asp:RadioButtonList ID="rbtTrancType"  runat="server" RepeatDirection="Horizontal" cssClass="SrHistPayrbl" >
        </asp:RadioButtonList>
       </div>
       <asp:PlaceHolder ID="phAdmin" runat="server" Visible="false">
        <div class="Clear"></div>
        <div class="PT5">
           <div class="SrHistPaySrcDtmTitle">MerchantID:</div>
           <div class="MerchSrcTopDLLDiv">
                <asp:TextBox CssClass="SrcHisPayTxt" ID="txtMerchantID" runat="server" style="border:1px solid #3b6db4;width:102px;"/>
           </div>
        </div>
        <div class="Clear"></div>
        <div class="PT5">
           <div class="SrHistPaySrcDtmTitle">e-mail:</div>
           <div class="MerchSrcTopDLLDiv">
                <asp:TextBox CssClass="SrcHisPayTxt" ID="txtEmail" runat="server" style="border:1px solid #3b6db4;width:102px;"/>
           </div>
        </div>
        <div class="Clear"></div>
        <div class="PT5">
                <div class="SrHistPaySrcDtmTitle"><%=ResHelper.LocalizeString("{$=Πληρωμή Προς|en-us=Payment to$}") %>:</div>
                <div class="MerchSrcTopDLLDiv"><asp:textbox  CssClass="SrcHisPayTxt" id="txtMerchant" autocomplete="off" onkeydown="return Setent(event)" onkeyup="event_c = event; obj_c = this;Hist_RebindRpt(this.value);"  runat="server" style="border:1px solid #3b6db4;width:178px" /></div>
                <div class="Clear"></div><asp:Button style="display:none" Text="hidbtn" ID="Hist_hidBtn" runat="server" /><asp:TextBox ID="HiddentxtMerchantID" runat="server" style="display:none"></asp:TextBox>
                <div class="HistQuickSrcResTopMainNewPay" id="Hist_QuickResDiv" style="display:none">
                    <div class="QuickSrcResMain" id="Hist_QuickSrcResMain">
                        <div class="QuickSrcResAll" id="Hist_QuickSrcResAll">
                        
                        </div>
                    </div>
                </div>
            </div>
       </asp:PlaceHolder>
       <div class="Clear"></div>
       <div class="PT5">
           <div class="SrHistPaySrcDtmTitle"><%=ResHelper.LocalizeString("{$=Αποτελέσματα ανά σελίδα|en-us=Results per page$}") %>:</div>
           <div class="MerchSrcTopDLLDiv">
                <asp:DropDownList runat="server" ID="ddlRecordsPerPage" CssClass="SrcHisPayTxt" >
                    <asp:ListItem Text="10" Value="10" ></asp:ListItem>
                    <asp:ListItem Text="20" Value="20" ></asp:ListItem>
                    <asp:ListItem Text="40" Value="40" ></asp:ListItem>
                    <asp:ListItem Text="100" Value="100" ></asp:ListItem>
                </asp:DropDownList>
           </div>
           <div class="Clear"></div>
       </div>
       <div><img src="/app_themes/LivePay/SrcBorderForm.png"</div>
       <div id="DivFullSrc" class="SrHistPayBigForm" style="display:none">
            <div class="PB10PF10">
                <div class="SrHistPayTableTitleMerch"><%=ResHelper.LocalizeString("{$=Ημερομηνία Κλεισίματος Πακέτου|en-us=Batch Submission Date$}") %>:</div>
                <div class="SrHistPayTableFrom"><%=ResHelper.LocalizeString("{$=Από|en-us=From$}") %>:</div>
                <div class="SrHistPayTableTxt"><asp:textbox CssClass="SrcHisPayTxt" id="txtFrom" runat="server" style="border:1px solid #3b6db4;width:82px;" /></div>
                <div class="SrHistPayTableToMerc"><%=ResHelper.LocalizeString("{$=Εως|en-us=To$}") %>:</div>
                <div class="SrHistPayTableTxt"><asp:textbox CssClass="SrcHisPayTxt" id="txtTo" runat="server" style="border:1px solid #3b6db4;width:82px" /></div>
                <div class="Clear"></div>
            </div>
           <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
           <div class="PB10PF10">
                <div class="SrHistPayTableTitleMerch"><%=ResHelper.LocalizeString("{$=Ημερομηνία Συναλλαγής|en-us=Transaction Date$}") %>:</div>
                <div class="SrHistPayTableFrom">Από:</div>
                <div class="SrHistPayTableTxt">
                    <asp:textbox CssClass="SrcHisPayTxt" id="txtTransFrom" MaxLength="8"  runat="server" style="border:1px solid #3b6db4;width:82px" />
                </div>
                <div class="SrHistPayTableToSec">Εως:</div>
                <div class="SrHistPayTableTxt">
                    <asp:textbox CssClass="SrcHisPayTxt" id="txtTransTo" MaxLength="8"  runat="server" style="border:1px solid #3b6db4;width:82px" />
                </div>
                <div class="Clear"></div>
            </div>

            <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitleMerchRpt"><%= ResHelper.LocalizeString("{$=Κατάσταση Συναλλαγής|en-us=Transaction Status$}")%>:</div>
                <div class="FLEFT"><asp:DropDownList ID="drpTxnResult" runat="server" ></asp:DropDownList></div>
                <div class="Clear"></div>
            </div>
            <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitleMerchRpt"><%=ResHelper.LocalizeString("{$=Τύπος Συναλλαγής|en-us=Transaction Type$}") %>:</div>
                <div class="FLEFT"><asp:DropDownList ID="drpTxnType" runat="server" ></asp:DropDownList></div>
                <div class="Clear"></div>
            </div>

            <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitleMerchRpt"><%= ResHelper.LocalizeString("{$=Ονοματεπώνυμο Πελάτη|en-us=Customer Full Name$}")%>:</div>
                <div class="FLEFT"><asp:textbox CssClass="SrcHisPayTxt" id="TxtFullName" MaxLength="100" runat="server" style="border:1px solid #3b6db4;width:242px" /></div>
                <div class="Clear"></div>
            </div>
            
            <asp:Panel ID="europhone_numcard" Visible="false" runat="server">
            <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitleMerchRpt"><%= ResHelper.LocalizeString("{$=Αριθμό πιστωτικής κάρτας|en-us=Card Number$}")%>:</div>
                <div class="FLEFT">
                    <asp:textbox CssClass="SrcHisPayTxt" id="TxtNumCard" MaxLength="16" runat="server" style="border:1px solid #3b6db4;width:242px" />
<%--                    <asp:RegularExpressionValidator ID="RegCardNumber" runat="server" 
                         ControlToValidate="TxtNumCard" ErrorMessage="*" 
                         ValidationExpression="^[0-9]$" />--%>
                </div>
                <div class="Clear"></div>
            </div>
            </asp:Panel>
            
           <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
           <div class="PB10PF10">
                <div class="SrHistPayTableTitleSec"><%=ResHelper.LocalizeString("{$=Ποσό|en-us=Amount$}") %> (&euro;):</div>
                <div class="SrHistPayTableFrom">Από:</div>
                <div class="SrHistPayTableTxt">
                    <asp:textbox CssClass="SrcHisPayTxt" id="txtAmountFrom" MaxLength="8" onkeyup="FixMoney(this,false)" onblur="FixMoney(this,true)" runat="server" style="border:1px solid #3b6db4;width:82px" />
                    <asp:RegularExpressionValidator ID="revAmountFrom" runat="server" 
                         ControlToValidate="txtAmountFrom" ErrorMessage="*" 
                         ValidationExpression="^[0-9]*(\.)?[0-9]?[0-9]?$" />
                </div>
                <div class="SrHistPayTableToSec">Εως:</div>
                <div class="SrHistPayTableTxt">
                    <asp:textbox CssClass="SrcHisPayTxt" id="txtAmountTo" MaxLength="8" onkeyup="FixMoney(this,false)" onblur="FixMoney(this,true)" runat="server" style="border:1px solid #3b6db4;width:82px" />
                    <asp:RegularExpressionValidator ID="revAmountTo" runat="server" 
                         ControlToValidate="txtAmountTo" ErrorMessage="*" 
                         ValidationExpression="^[0-9]*(\.)?[0-9]?[0-9]?$" />
                </div>
                <div class="Clear"></div>
            </div>
           <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
           <div class="PB10PF10">
                <div class="SrHistPayTableTitleSec"><%=ResHelper.LocalizeString("{$=Αριθμός Πακέτου|en-us=Batch Number$}") %>:</div>
                <div class="SrHistPayTableFrom">Από:</div>
                <div class="SrHistPayTableTxt">
                    <asp:textbox CssClass="SrcHisPayTxt" id="txtBatchFrom" onkeyup="integer(this)" MaxLength="15" runat="server" style="border:1px solid #3b6db4;width:82px" />
                    <asp:RegularExpressionValidator ID="revBatchFrom" runat="server" 
                         ControlToValidate="txtBatchFrom" ErrorMessage="*" 
                         ValidationExpression="^([0-9]{0,15})$" />
                </div>
                <div class="SrHistPayTableToSec">Εως:</div>
                <div class="SrHistPayTableTxt">
                    <asp:textbox CssClass="SrcHisPayTxt" id="txtBatchTo" onkeyup="integer(this)" MaxLength="15" runat="server" style="border:1px solid #3b6db4;width:82px" />
                    <asp:RegularExpressionValidator ID="revBatchTo" runat="server" 
                         ControlToValidate="txtBatchTo" ErrorMessage="*" 
                         ValidationExpression="^([0-9]{0,15})$" />
                </div>
                <div class="Clear"></div>
            </div>
            <div ><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
       </div>
       <div class="SrHistPayResTitle" ><a onclick="ShowHideSrcForm();" class="Chand"><img border="0" id="imgPointSrcType" src="~/app_themes/LivePay/PointerDown.gif"/> <span ID="spnSrcType"><%=ResHelper.LocalizeString("{$=Αναλυτική Αναζήτηση|en-us=Detailed Search$}") %></span></a></div>
       <div class="MerchSrcBtnSearch"><asp:ImageButton runat="server" ID="btnSearchPayments" /></div>
       <div class="Clear"></div>

       
       <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
    </div>
 </div>

 <div  style="background-color:White">&nbsp;</div>

 <div class="SrHistPayBG ">
    <div class="SrHistPayTitle" id="OpenBatchDIVMainHeader" runat="server"><%=ResHelper.LocalizeString("{$=Συνοπτική Εικόνα Ανοικτού Πακέτου|en-us=Current Batch General Info$}") %></div>
    <div class="SrHistPayTitle" id="AllTransactionDIVMainHeader" runat="server" visible="false" ><%= ResHelper.LocalizeString("{$=Συνοπτική Εικόνα|en-us=General Info$}")%></div>

    <div style="padding:6px 10px 0px 10px;">
        <div class="SrchHisTable_MainTitle">
            <div class="SrchHisTable_Main_Title" style="width:111px;font-size:11px"><%= ResHelper.LocalizeString("{$=Πλήθος Συναλλαγών|en-us=Number of Transactions $}")%> :</div>
            <div class="SrchHisTable_Main_Value" style="font-size:11px"><asp:Literal runat="server" ID="litTotalTxn" /></div>
            <div class="Clear"></div>
            <div class="SrchHisTable_Main_SecTitle" style="width:111px;font-size:11px"><%=ResHelper.LocalizeString("{$=Πλήθος Εγκεκριμένων Πωλήσεων|en-us=Number of Approved Transactions$}") %> :</div>
            <div class="SrchHisTable_Main_SecValue" style="font-size:11px"><asp:Literal runat="server" ID="litTotalSales" /></div>
            <div class="Clear"></div>
            <div class="SrchHisTable_Main_SecTitle" style="width:111px;font-size:11px"><%= ResHelper.LocalizeString("{$=Πλήθος Επιστροφών|en-us=Number of Refunds$}")%> :</div>
            <div class="SrchHisTable_Main_SecValue" style="font-size:11px"><asp:Literal runat="server" ID="litTotalRefunds" /></div>
            <div class="Clear"></div>
        </div>
        <div style="float:left;border-right:1px solid #f8f6f6;border-left:1px solid #e3e3e3;padding-left:10px;height:90px;line-height:18px;width:190px;">
            <div class="SrchHisTable_Main_Title" style="width:111px;font-size:11px"><%=ResHelper.LocalizeString("{$=Αξία Συναλλαγών|en-us=Transaction Amount$}") %>(€) :</div>
            <div class="SrchHisTable_Main_Value" style="font-size:11px;width:75px;text-align:right"><asp:Literal runat="server" ID="litValueTxn" /></div>
            <div class="Clear"></div>
            <div class="SrchHisTable_Main_SecTitle" style="width:111px;font-size:11px"><%= ResHelper.LocalizeString("{$=Αξία Εγκεκριμένων Πωλήσεων|en-us=Approved Transaction Amount$}")%>(€) :</div>
            <div class="SrchHisTable_Main_SecValue" style="font-size:11px;width:75px;text-align:right"><asp:Literal runat="server" ID="litValueSales" /></div>
            <div class="Clear"></div>
            <div class="SrchHisTable_Main_SecTitle" style="width:111px;font-size:11px"><%= ResHelper.LocalizeString("{$=Αξία Επιστροφών|en-us=Refund Amount$}")%>(€) :</div>
            <div class="SrchHisTable_Main_SecValue" style="font-size:11px;width:75px;text-align:right"><asp:Literal runat="server" ID="litValueRefunds" /></div>
            <div class="Clear"></div>
        </div>
        <div class="SrchHisTable_LastVal">
            <div class="SrchHisTable_LastValTitle" style="font-size:11px"><%=ResHelper.LocalizeString("{$=Προγραμματισμένη ημερομηνία & ώρα κλεισίματος πακέτου|en-us=Scheduled Batch Submission Date and Time$}") %>:</div>
            <div class="SrchHisTable_LastValValue" style="font-size:11px" id="nextBatchCloseDate" runat="server"></div>
        </div>
         <div class="Clear"></div>
    </div>
    <div class="GridMainGrid">
            <div class="SrcHisPayGridBGTop650">
                <div class="SrcHisPayGridTopTitle" id="OpenBatchDIVHeader" runat="server" visible="false"><%=ResHelper.LocalizeString("{$=Συναλλαγές Ανοικτού Πακέτου|en-us=Current Batch Transactions$}") %></div>
                <div class="SrcHisPayGridTopTitle" id="AllTransactionDIVHeader" runat="server" visible="false"><%= ResHelper.LocalizeString("{$=Συναλλαγές|en-us=Transactions$}")%></div>
                <div class="SrcHisPayGridDiv">
                    <asp:GridView ID="GridViewPayments" AllowSorting="true" EmptyDataTemplate-CssClass="SrcHisPayPagerEmpty" PagerStyle-CssClass="SrcHisPayPager650" GridLines="None" runat="server" PageSize="8" AutoGenerateColumns="false" AllowPaging="true" Width="650px">
                    <HeaderStyle BackColor="#ffffff" ForeColor="#58595b" Height="35px" Font-Size="10px"  HorizontalAlign="Center"/>
                    <AlternatingRowStyle Height="40px" BackColor="#ffffff" ForeColor="#58595b" Font-Size="10px" Font-Names="tahoma" />
                    <RowStyle Height="40px" BackColor="#f7f7f7" ForeColor="#58595b" Font-Size="10px" Font-Names="tahoma" HorizontalAlign="Center" />
                        <Columns >
                            <asp:TemplateField SortExpression="BatchID"  HeaderStyle-CssClass="MerchSrcHeaderLeft MerchSrcHeader" ItemStyle-CssClass="MerchSrcItemLeft"  >
                                <ItemTemplate>
                                    <asp:label ID="lblBoxNo" runat="server" Text='<%#Eval("BatchID") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  ItemStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                    <asp:label ID="lblCloseDtm" runat="server" Text='<%#Eval("BatchDate")%>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  ItemStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                    
                                    <asp:label ID="lblBatchClosed" runat="server" Text='<%#Eval("BatchClosed") %>' style="display:none" ></asp:label>
                                    <asp:label ID="lblBoxStatus" runat="server" Text='<%#IIF(Eval("BatchClosed")=true,ResHelper.LocalizeString("{$=Κλειστό|en-us=Closed$}"),ResHelper.LocalizeString("{$=Ανοιχτό|en-us=Open$}")) %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  >
                                <ItemTemplate>
                                    <asp:label ID="lblTransCode" runat="server" ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField   HeaderStyle-CssClass="MerchSrcHeaderSimple "  ItemStyle-CssClass="MerchSrcItemSimple " >
                                <ItemTemplate>
                                    <asp:label ID="lblTransDtm" runat="server" Text='<%#Eval("transactiondDte") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple W60"  ItemStyle-CssClass="MerchSrcItemSimple W60" >
                                <ItemTemplate>
                                    <asp:label ID="lblCustName" runat="server" Text='<%#Eval("CustomerName") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple" ItemStyle-HorizontalAlign="Right"  ItemStyle-CssClass="MerchSrcItemSimple" >
                                <ItemTemplate>
                                    <asp:HiddenField ID="hidAmount" runat="server" value='<%#Eval("transactioAamount") %>' />
                                    <asp:Label ID="lblAmount" runat="server" />&nbsp;&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:label ID="lblTransType" runat="server" Text='<%#Eval("transactionType") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:label ID="lblInstallments" runat="server" Text='<%# Eval("Installments") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Κατάσταση<br>Συναλλαγής" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="SrcHisPayGridHeaderLast " ItemStyle-CssClass="SrcHisPayGridItemLast W100" >
                                <ItemTemplate>
                                    <div style="width:79px">
                                        <asp:Label ID="lblTransactionStatus" runat="server" style="display:none" Text='<%#Eval("TransactionStatus") %>' />
                                        <div style="float:left;padding:0px 0px 5px 2px;width:79px"><asp:label ID="lblTransStatus" runat="server" Text='<%#GetTransactionStatusDescr(Eval("TransactionStatus")) %>' ></asp:label></div>
                                        <div style="float:left;padding-left:3px;width:79px">
                                            <asp:LinkButton ID="openPopup" runat="server" OnClick="openPopup_Click" CommandArgument='<%#Eval("transactionId") & "_" & Eval("System") & "_" & Eval("merchantId") %>' ToolTip="Πληροφορίες"><img border="0" src="/app_themes/LivePay/InfoBtn.png" /></asp:LinkButton>
                                            <asp:LinkButton ID="openChanges" Visible='<%#Eval("related") %>'  runat="server" OnClick="openChanges_Click" CommandArgument='<%#Eval("transactionId") %>' ToolTip="Μεταβολές Συναλλαγής"><img border="0" src="/app_themes/LivePay/RedInfo.png" /></asp:LinkButton>
                                            <asp:LinkButton ID="openRefund" runat="server" OnClick="openRefund_Click" CommandArgument='<%#Eval("transactionId") %>' ToolTip="Αντιλογισμός"><img border="0" src="/app_themes/LivePay/Refund.png" /></asp:LinkButton>
                                            <asp:LinkButton ID="openCancel" runat="server" OnClick="openCancel_Click" CommandArgument='<%#Eval("transactionId") %>' ToolTip="Ακύρωση"><img border="0" src="/app_themes/LivePay/imgDelete.png" /></asp:LinkButton></div>
                                        <div class="Clear"></div>
                                   </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                        </Columns>
                        <EmptyDataTemplate >
                            <div class="SrcHisPayPagerEmpty650">
                                <div style="padding-top:5px;padding-left:10px"><%=ResHelper.LocalizeString("{$=Δεν βρέθηκαν συναλλαγές|en-us=No Transactions found$}") %></div>
                            </div>
                        </EmptyDataTemplate>
                        <PagerTemplate >
                        <div class="SrcHisPayPagerSpace" >
                            <div>
                                <div class="GridPagerNoLinkDivLeft5"><asp:linkbutton ID="lnkPrev" CssClass="PagerText" onclick="ChangePageByLinkNumber_Before" runat="server"><%= ResHelper.LocalizeString("{$=&laquo; Προηγούμενη|en-us=&laquo; Previous$}")%></asp:linkbutton><font color="#adadad">&nbsp;|&nbsp;</font></div>
                                 <div style="float:left;width:383px">
                                    <div class="GridPagerNoLinkDivCenter5" style="float:left;padding-top:13px;padding-top:expression(0)" id="lblPageIndex" runat="server">&nbsp;</div>
                                    <div class="GridPagerNoLinkDivRightss" style="float:left;padding-top:17px"><font color="#adadad">&nbsp;|&nbsp;</font><asp:linkbutton ID="lnkNext" CssClass="PagerText"  onclick="ChangePageByLinkNumber_Next" runat="server"><%=ResHelper.LocalizeString("{$=Επόμενη &raquo;|en-us=Next &raquo;$}") %></asp:linkbutton></div>
                                    <div class="Clear"></div>
                                </div> 
                                <div class="GridPagerCloseBox" style="width:110px"><asp:ImageButton ID="imgCloseBatch" OnClick="CloseBatch" OnClientClick="return CloseTheBatch()" runat="server" /></div>
                                <div class="GridPagerPDFss" style="float:left;padding-right:5px;padding-right:expression(6);padding-top:13px;"> 
                                <asp:LinkButton ID="lnkbtnimgPDF" runat="server" OnClick="btnSaveToPDFClick" >
                                    <img class="Chand" border="0" src="/app_themes/LivePay/PDFIcon.png" />
                                </asp:LinkButton>
                                
                                </div>
                                <div class="GridPagerExcel">
                                     <asp:LinkButton ID="lnkbtnimgExcel" runat="server" OnClick="btnSaveToExcelClick" >
                                        <img border="0" class="Chand" src="/app_themes/LivePay/ExcelIcon.png" />
                                     </asp:LinkButton> 
                                </div>

                            </div>
                                    
                                
                         </div>
                         </PagerTemplate>
                    </asp:GridView>
                </div>
                <asp:LinkButton ID="HiddenPageBtn" style="display:none" runat="server" Text="0"></asp:LinkButton>
                <asp:TextBox ID="HiddenPage"  style="display:none" runat="server" Text="1"></asp:TextBox>
                <asp:TextBox ID="HiddenSearchType"  style="display:none" runat="server" Text="0"></asp:TextBox>
                <div id="BottomDiv" style="display:none" runat="server" class="SrcHisPayGridBGBottom"></div> 
            </div>
            
       </div>
    <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
 </div> 
 </div>
 <asp:Label ID="lblDebug" runat="server" />
 <div style="width:500px;height:100px;border:1px solid black;display:none" id="FramDiv" runat="server" >
   <iframe id="exportFrame"  runat="server"></iframe>
 </div>
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnSearchPayments" />
    <asp:AsyncPostBackTrigger ControlID="GridViewPayments" />
</Triggers>
 </asp:UpdatePanel>

 <script language="javascript" type="text/javascript" >

     function GoToPage(no) {
         document.getElementById('<%=HiddenPage.ClientID %>').value = no;
         document.getElementById('<%=HiddenPageBtn.ClientID %>').innerHTML = no;
         var GetID = '<%=HiddenPageBtn.ClientID %>'
         GetID = GetID.replace(/[_]/gi, "$")
         __doPostBack(GetID, '')
     }

     
     function ShowHideSrcForm() {
         var GetForm = document.getElementById('DivFullSrc');
         var GetimgPoint = document.getElementById('imgPointSrcType');
         var GetSrcType = document.getElementById('spnSrcType');

         if (GetForm.style.display == '') {
             GetForm.style.display = 'none'
             document.getElementById('<%=HiddenSearchType.ClientID %>').value = '0';
             GetimgPoint.src = '/app_themes/LivePay/PointerDown.gif'
             GetSrcType.innerHTML ='<%=ResHelper.LocalizeString("{$=Αναλυτική Αναζήτηση|en-us=Detailed Search$}") %>'
         } else {
             GetForm.style.display = ''
             document.getElementById('<%=HiddenSearchType.ClientID %>').value = '1';
             GetimgPoint.src = '/app_themes/LivePay/PointerUp.png'
             GetSrcType.innerHTML = '<%=ResHelper.LocalizeString("{$=Αναλυτική Αναζήτηση|en-us=Detailed Search$}") %>'

         }
         PosBottom();
     }

     
     
     CheckAddtributes()
     function CheckAddtributes() {
         var GetSrcType = document.getElementById('<%=HiddenSearchType.ClientID %>').value
         var GetForm = document.getElementById('DivFullSrc');
         if (GetSrcType == '1') {
             GetForm.style.display = ''
         }
     }

    
 </script>
