CREATE PROCEDURE [Proc_Forums_Forum_UpdateForumCounts]
	@ID int,
	@ThreadLength int
AS
BEGIN
	/* Get number of posts */
    DECLARE @Threads int;
	DECLARE @AbsoluteThreads int;
	DECLARE @Posts int;
	DECLARE @AbsolutePosts int;
	
	SET @Threads = (SELECT COUNT(*) FROM [Forums_ForumPost] WHERE ([PostForumID] = @ID)AND(PostApproved=1)AND(PostLevel = 0));
	SET @AbsoluteThreads = (SELECT COUNT(*) FROM [Forums_ForumPost] WHERE ([PostForumID] = @ID)AND(PostLevel = 0));
	SET @Posts = (SELECT COUNT(*) FROM [Forums_ForumPost] AS temp WHERE ([PostForumID] = @ID)AND(PostApproved=1) AND ((SELECT  PostID FROM Forums_ForumPost WHERE PostIDPath = SUBSTRING(temp.PostIDPath, 0, @ThreadLength) AND PostApproved = 1) IS NOT NULL));
	SET @AbsolutePosts = (SELECT COUNT(*) FROM [Forums_ForumPost] AS temp WHERE ([PostForumID] = @ID) AND ((SELECT  PostID FROM Forums_ForumPost WHERE PostIDPath = SUBSTRING(temp.PostIDPath, 0, @ThreadLength)) IS NOT NULL));
	/* Get last post */
	DECLARE @lastPost TABLE (
		PostTime datetime,
		PostUserName nvarchar(200)
	);
	INSERT INTO @lastPost SELECT TOP 1 [PostTime], [PostUserName] FROM [Forums_ForumPost] AS temp WHERE ([PostForumID] = @ID) AND (PostApproved=1) AND ((SELECT  PostID FROM Forums_ForumPost WHERE PostIDPath = SUBSTRING(temp.PostIDPath, 0, @ThreadLength) AND PostApproved = 1) IS NOT NULL) ORDER BY PostTime DESC;
	DECLARE @LastPostTime datetime;
	DECLARE @LastPostUserName nvarchar(200);
	SET @LastPostTime = (SELECT TOP 1 PostTime FROM @lastPost);
	SET @LastPostUserName = (SELECT TOP 1 PostUserName FROM @lastPost);
	
	/* Get last absolute post */
	DECLARE @lastAbsolutePost TABLE (
		PostTime datetime,
		PostUserName nvarchar(200)
	);
	INSERT INTO @lastAbsolutePost SELECT TOP 1 [PostTime], [PostUserName] FROM [Forums_ForumPost] AS temp WHERE ([PostForumID] = @ID) AND ((SELECT PostID FROM Forums_ForumPost WHERE PostIDPath = SUBSTRING(temp.PostIDPath, 0, @ThreadLength)) IS NOT NULL) ORDER BY PostTime DESC;
	DECLARE @LastAbsolutePostTime datetime;
	DECLARE @LastAbsolutePostUserName nvarchar(200);
	SET @LastAbsolutePostTime = (SELECT TOP 1 PostTime FROM @lastAbsolutePost);
	SET @LastAbsolutePostUserName = (SELECT TOP 1 PostUserName FROM @lastAbsolutePost);
	/* Update the forum */
	UPDATE [Forums_Forum] SET  
		[ForumThreads] = @Threads,
		[ForumPosts] = @Posts, 
		[ForumLastPostTime] = @LastPostTime,
		[ForumLastPostUserName] = @LastPostUserName,
		[ForumThreadsAbsolute] = @AbsoluteThreads,
		[ForumPostsAbsolute] = @AbsolutePosts, 
		[ForumLastPostTimeAbsolute] = @LastAbsolutePostTime,
		[ForumLastPostUserNameAbsolute] = @LastAbsolutePostUserName
	WHERE ForumID = @ID
END
