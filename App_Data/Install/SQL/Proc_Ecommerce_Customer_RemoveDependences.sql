CREATE PROCEDURE [Proc_Ecommerce_Customer_RemoveDependences]
	@ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
	-- COM_Address
    DELETE FROM COM_Address WHERE AddressCustomerID = @ID;
	-- COM_CustomerCreditHistory
	DELETE FROM COM_CustomerCreditHistory WHERE EventCustomerID = @ID;
	COMMIT TRANSACTION;
END
