﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default"
    Theme="Default" ValidateRequest="false" %>

<%=DocType%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <asp:literal runat="server" id="ltlTags" enableviewstate="false" />
</head>
<body class="<%=BodyClass%>" <%=BodyParameters%>>
    <form id="form1" runat="server">
        <cms:CMSPageManager ID="CMSPageManager1" runat="server" />
        <div style="padding: 5px;">
            The web site doesn't contain any content. Sign in to <a href="<%= ResolveUrl("~/cmsdesk/default.aspx") %>">
                CMS Desk</a> and edit the content.
        </div>
    </form>
</body>
</html>