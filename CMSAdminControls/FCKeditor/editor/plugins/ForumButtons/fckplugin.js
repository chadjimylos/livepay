﻿
// Insert quote function
var InsertQuote = function() { this.Name = 'InsertQuote'; }
InsertQuote.prototype.GetState = function() { return FCK_TRISTATE_OFF ; }
InsertQuote.prototype.Execute = function()
{
    var fckEditor = window.FCK;
    if (fckEditor != null) {
        fckEditor.InsertHtml('[quote=][/quote]');
    }
}


// Insert image function
var InsertImage = function() { this.Name = 'InsertImage'; }
InsertImage.prototype.GetState = function() { return FCK_TRISTATE_OFF ; }
InsertImage.prototype.Execute = function()
{
    
    var url = prompt(FCKLang.InsertImagePromptUrl, 'http://'); 
    if (url != null) {

        var fckEditor = window.FCK;
        if (fckEditor != null) {
            fckEditor.InsertHtml('[img]' + url + '[/img]');
        }
        
    }
    
}


// Insert url function
var InsertUrl = function() { this.Name = 'InsertUrl'; }
InsertUrl.prototype.GetState = function() { return FCK_TRISTATE_OFF ; }
InsertUrl.prototype.Execute = function()
{

    var url = prompt(FCKLang.InsertUrlPromptUrl, 'http://');
    if (url != null) { 
        
        var desc = prompt(FCKLang.InsertUrlPromptDesc, '');
        if (desc == null) { desc = '' }
        
        var fckEditor = window.FCK;
        if (fckEditor != null) {
            fckEditor.InsertHtml('[url=' + url + ']' + desc + '[/url]');
        }
        
    }

}


// Register the related command.
FCKCommands.RegisterCommand( 'InsertQuote', new InsertQuote('InsertQuote') ) ;
FCKCommands.RegisterCommand( 'InsertUrl', new InsertUrl('InsertUrl') ) ;
FCKCommands.RegisterCommand( 'InsertImage', new InsertImage('InsertImage') ) ;


// Quote button
var oInsertQuote = new FCKToolbarButton('InsertQuote', FCKLang.InsertQuoteButton ) ;
oInsertQuote.IconPath = FCKPlugins.Items['ForumButtons'].Path + 'fckQuote.gif' ;
FCKToolbarItems.RegisterItem( 'InsertQuote', oInsertQuote ) ;


// Url button
var oInsertUrl = new FCKToolbarButton('InsertUrl', FCKLang.InsertUrlButton ) ;
oInsertUrl.IconPath = FCKPlugins.Items['ForumButtons'].Path + 'fckUrl.gif' ;
FCKToolbarItems.RegisterItem( 'InsertUrl', oInsertUrl ) ;


// Image button
var oInsertImage = new FCKToolbarButton('InsertImage', FCKLang.InsertImageButton ) ;
oInsertImage.IconPath = FCKPlugins.Items['ForumButtons'].Path + 'fckImage.gif' ;
FCKToolbarItems.RegisterItem( 'InsertImage', oInsertImage ) ;