CREATE PROCEDURE [Proc_CMS_SettingsCategory_MoveCategoryUp]
	@CategoryID int
AS
BEGIN
    /* Move the previous step(s) down */
	UPDATE CMS_SettingsCategory SET CategoryOrder = CategoryOrder + 1 WHERE CategoryOrder = (SELECT CategoryOrder FROM CMS_SettingsCategory WHERE CategoryID = @CategoryID) - 1 
	/* Move the current step up */
	UPDATE CMS_SettingsCategory SET CategoryOrder = CategoryOrder - 1 WHERE CategoryID = @CategoryID AND CategoryOrder > 1
END
