﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.SiteProvider;

public partial class CMSAdminControls_UI_UIProfiles_MenuActions : CMSUserControl
{
    #region "Variables"

    private int mResourceID = 0;
    private int mParentID = 0;
    private int mElementID = 0;
    private int mTabIndex = 0;

    #endregion

    #region "Properties"

    /// <summary>
    /// Resource ID.
    /// </summary>
    public int ResourceID
    {
        get
        {
            return this.mResourceID;
        }
        set
        {
            this.mResourceID = value;
        }
    }


    /// <summary>
    /// Element ID.
    /// </summary>
    public int ElementID
    {
        get
        {
            return this.mElementID;
        }
        set
        {
            this.mElementID = value;
        }
    }


    /// <summary>
    /// Parent element ID.
    /// </summary>
    public int ParentID
    {
        get
        {
            return this.mParentID;
        }
        set
        {
            this.mParentID = value;
        }
    }


    /// <summary>
    /// Gets or sets value of hidden field where are stroed ElementID and ParentID separated by |
    /// </summary>
    public string Value
    {
        get
        {
            return this.hidSelectedElem.Value;
        }
        set
        {
            this.hidSelectedElem.Value = value;
        }
    }


    /// <summary>
    /// Event called after menu action is executed
    /// </summary>
    public event OnActionEventHandler AfterAction = null;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!StopProcessing)
        {
            this.imgNewElem.ImageUrl = GetImageUrl("CMSModules/CMS_UIProfiles/add.png");
            this.imgDeleteElem.ImageUrl = GetImageUrl("CMSModules/CMS_UIProfiles/delete.png");
            this.imgMoveUp.ImageUrl = GetImageUrl("CMSModules/CMS_UIProfiles/up.png");
            this.imgMoveDown.ImageUrl = GetImageUrl("CMSModules/CMS_UIProfiles/down.png");

            this.btnNewElem.Text = ResHelper.GetString("resource.ui.newelem");
            this.btnDeleteElem.Text = ResHelper.GetString("resource.ui.deleteelem");
            this.btnMoveUp.Text = ResHelper.GetString("resource.ui.modeupelem");
            this.btnMoveDown.Text = ResHelper.GetString("resource.ui.modedownelem");

            // Create new element javascript
            string newScript = "var hidElem = document.getElementById('" + this.hidSelectedElem.ClientID + "'); var ids = hidElem.value.split('|');";
            newScript += "if ((window.parent != null) && (window.parent.frames['uicontent'] != null)) {";
            newScript += "window.parent.frames['uicontent'].location = '" + ResolveUrl("~/CMSSiteManager/Development/Modules/Module_UI_New.aspx") + "?moduleid=" + ResourceID + "&parentId=' + ids[0];";
            newScript += "} return false;";
            this.btnNewElem.OnClientClick = newScript;

            // Confirm delete
            this.btnDeleteElem.OnClientClick = "return deleteConfirm();";

            string script = "var menuHiddenId = '" + this.hidSelectedElem.ClientID + "';";
            script += "function deleteConfirm() {";
            script += "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("resource.ui.confirmdelete")) + ");";
            script += "}";
            this.ltlScript.Text = ScriptHelper.GetScript(script);
        }
    }

    #region "Page events"

    protected void btnMoveUp_Click(object sender, EventArgs e)
    {
        GetHiddenValues();
        if (ElementID > 0)
        {
            UIElementInfoProvider.MoveUIElementUp(ElementID);
            if (AfterAction != null)
            {
                AfterAction("moveup", ElementID + "|" + mTabIndex);
            }
        }
    }


    protected void btnMoveDown_Click(object sender, EventArgs e)
    {
        GetHiddenValues();
        if (ElementID > 0)
        {
            UIElementInfoProvider.MoveUIElementDown(ElementID);
            if (AfterAction != null)
            {
                AfterAction("movedown", ElementID + "|" + mTabIndex);
            }
        }
    }


    protected void btnNewElem_Click(object sender, EventArgs e)
    {
        GetHiddenValues();
        string script = "if ((window.parent != null) && (window.parent.frames['header'] != null) && (window.parent.frames['content'] != null)) {";
        script += "window.parent.frames['header'].location = '" + ResolveUrl("~/CMSSiteManager/Development/Modules/Module_UI_Header.aspx") + "?moduleid=" + ResourceID + "&new=1';";
        script += "window.parent.frames['content'].location = '" + ResolveUrl("~/CMSSiteManager/Development/Modules/Module_UI_General.aspx") + "?moduleid=" + ResourceID + "&parentId=" + ElementID + "';";
        script += "}";
        this.ltlScript.Text += ScriptHelper.GetScript(script);
        if (AfterAction != null)
        {
            AfterAction("new", ElementID);
        }
    }


    protected void btnDeleteElem_Click(object sender, EventArgs e)
    {
        GetHiddenValues();
        if ((ElementID > 0) && (ParentID > 0))
        {
            UIElementInfoProvider.DeleteUIElementInfo(ElementID);
            if (AfterAction != null)
            {
                AfterAction("delete", ParentID);
            }
        }
    }

    #endregion

    #region "Private methods"

    private void GetHiddenValues()
    {
        string hidValue = this.hidSelectedElem.Value;
        string[] split = hidValue.Split('|');
        if (split.Length >= 2)
        {
            ElementID = ValidationHelper.GetInteger(split[0], 0);
            ParentID = ValidationHelper.GetInteger(split[1], 0);
            if (split.Length == 3)
            {
                mTabIndex = ValidationHelper.GetInteger(split[2], 0);
            }
        }
    }
    #endregion
}
