<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fck_InsertUserControls.aspx.cs"
    Inherits="CMSAdminControls_FCKeditor_editor_plugins_InsertUserControls_fck_InsertUserControls"
    Theme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Insert inline control</title>

    <script type="text/javascript">
    var oEditor = window.parent.InnerDialogLoaded() ;

    function InsertUserControl(charValue)
    {
        oEditor.FCK.InsertHtml( charValue || "" ) ;
        window.parent.Cancel() ;
    }
    </script>

</head>
<body style="background-color: White;">
    <form id="form1" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="vertical-align: top;">
                <asp:Label ID="lblAvailableControls" runat="server" EnableViewState="false" /><br />
                <br />
                <asp:ListBox ID="lstControls" runat="server" Rows="16" Width="200px" DataTextField="ControlDisplayName"
                    DataValueField="ControlName" AutoPostBack="true" OnSelectedIndexChanged="lstControls_SelectedIndexChanged" />
            </td>
            <td class="PropertiesContent">
                <asp:Label ID="lblControlName" runat="server" CssClass="PropertiesControlName" EnableViewState="false" />
                <asp:Label ID="lblControlDescription" runat="server" CssClass="PropertiesControlDescription"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click" />
            </td>
            <td>
                <br />
                <asp:Label ID="lblControlParametrName" runat="server" EnableViewState="false" />
                <asp:TextBox ID="txtControlParametrName" runat="server" MaxLength="200" CssClass="SmallTextBox"
                    EnableViewState="false" />
            </td>
        </tr>
    </table>
    <asp:Literal ID="ltlScript" runat="server" />
    </form>
</body>
</html>
