<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PingedUrls.ascx.cs" Inherits="CMSModules_Blogs_FormControls_PingedUrls" %>
<br />
<asp:Panel ID="pnlPingedValues" runat="server" EnableViewState="false" Visible="false">
    <cms:LocalizedLabel ID="lblTitle" runat="server" ResourceString="blogs.trackbacks.pinged" DisplayColon="true" />
</asp:Panel>
