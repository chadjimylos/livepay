<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User_Edit_Frameset.aspx.cs" Inherits="CMSSiteManager_Administration_Users_User_Edit_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Users</title>
</head>

	<frameset border="0" rows="65, *" id="rowsFrameset">
		<frame name="menu" src="User_Edit.aspx?userid=<%= Request.QueryString["userid"] %>&siteid=<%= Request.QueryString["siteid"] %>" frameborder="0" scrolling="no"  />		
	    <frame name="content" src="User_Edit_General.aspx?userid=<%= Request.QueryString["userid"]%>&siteid=<%= Request.QueryString["siteid"] %>" frameborder="0" noresize="noresize"  />
	<noframes>
		<body>
		 <p id="p1">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		 </p>
		</body>
	</noframes>
</frameset>
</html>
