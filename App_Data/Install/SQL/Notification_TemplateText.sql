CREATE TABLE [Notification_TemplateText] (
		[TemplateTextID]               int NOT NULL IDENTITY(1, 1),
		[TemplateID]                   int NOT NULL,
		[GatewayID]                    int NOT NULL,
		[TemplateSubject]              nvarchar(250) NOT NULL,
		[TemplateHTMLText]             nvarchar(max) NOT NULL,
		[TemplatePlainText]            nvarchar(max) NOT NULL,
		[TemplateTextGUID]             uniqueidentifier NOT NULL,
		[TemplateTextLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Notification_TemplateText]
	ADD
	CONSTRAINT [PK_Notification_TemplateText]
	PRIMARY KEY
	([TemplateTextID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Notification_TemplateText]') IS NOT NULL
	AND OBJECT_ID(N'[Notification_Gateway]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Notification_TemplateText_GatewayID_Notification_Gateway]') IS NULL
BEGIN
		ALTER TABLE [Notification_TemplateText]
			ADD CONSTRAINT [FK_Notification_TemplateText_GatewayID_Notification_Gateway]
			FOREIGN KEY ([GatewayID]) REFERENCES [Notification_Gateway] ([GatewayID])
END
IF OBJECT_ID(N'[Notification_TemplateText]') IS NOT NULL
	AND OBJECT_ID(N'[Notification_Template]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Notification_TemplateText_TemplateID_Notification_Template]') IS NULL
BEGIN
		ALTER TABLE [Notification_TemplateText]
			ADD CONSTRAINT [FK_Notification_TemplateText_TemplateID_Notification_Template]
			FOREIGN KEY ([TemplateID]) REFERENCES [Notification_Template] ([TemplateID])
END
CREATE INDEX [IX_Notification_TemplateText_GatewayID]
	ON [Notification_TemplateText] ([GatewayID])
	ON [PRIMARY]
CREATE INDEX [IX_Notification_TemplateText_TemplateID]
	ON [Notification_TemplateText] ([TemplateID])
	ON [PRIMARY]
