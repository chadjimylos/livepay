CREATE TABLE [Board_Moderator] (
		[BoardID]     int NOT NULL,
		[UserID]      int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Board_Moderator]
	ADD
	CONSTRAINT [PK_Board_Moderator]
	PRIMARY KEY
	([BoardID], [UserID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Board_Moderator]') IS NOT NULL
	AND OBJECT_ID(N'[Board_Board]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Board_Moderator_BoardID_Board_Board]') IS NULL
BEGIN
		ALTER TABLE [Board_Moderator]
			ADD CONSTRAINT [FK_Board_Moderator_BoardID_Board_Board]
			FOREIGN KEY ([BoardID]) REFERENCES [Board_Board] ([BoardID])
END
IF OBJECT_ID(N'[Board_Moderator]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Board_Moderator_UserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Board_Moderator]
			ADD CONSTRAINT [FK_Board_Moderator_UserID_CMS_User]
			FOREIGN KEY ([UserID]) REFERENCES [CMS_User] ([UserID])
END
