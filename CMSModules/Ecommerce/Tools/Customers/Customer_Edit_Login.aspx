<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Customer_Edit_Login.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Ecommerce_Tools_Customers_Customer_Edit_Login"
    Theme="Default" Title="Address properties" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Panel ID="pnlEdit" runat="server">
        <table style="vertical-align: top">
            <tr>
                <td class="FieldLabel" colspan="2">
                    <asp:CheckBox ID="chkHasLogin" runat="server" CssClass="CheckBoxMovedLeft" AutoPostBack="true" />
                    <asp:Label runat="server" ID="lblHasLogin" EnableViewState="false" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel runat="server" ID="lblUserName" EnableViewState="false" ResourceString="general.username"
                        DisplayColon="true" />
                </td>
                <td>
                    <asp:TextBox ID="txtUserName" runat="server" CssClass="TextBoxField" MaxLength="100"
                        Enabled="false" EnableViewState="false" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rqvUserName" runat="server" ControlToValidate="txtUserName"
                        ValidationGroup="Login" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" style="vertical-align: top;">
                    <asp:Label runat="server" ID="lblPassword1" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtPassword1" runat="server" CssClass="TextBoxField" TextMode="Password"
                        Enabled="false" MaxLength="100" EnableViewState="false" />
                </td>
                <td style="vertical-align: top;">
                    <asp:RequiredFieldValidator ID="rqvPassword1" runat="server" ControlToValidate="txtPassword1"
                        ValidationGroup="Login" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" style="vertical-align: top;">
                    <asp:Label runat="server" ID="lblPassword2" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtPassword2" runat="server" CssClass="TextBoxField" TextMode="Password"
                        Enabled="false" MaxLength="100" EnableViewState="false" />
                </td>
                <td style="vertical-align: top;">
                    <asp:RequiredFieldValidator ID="rqvPassword2" runat="server" ControlToValidate="txtPassword2"
                        ValidationGroup="Login" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                        Enabled="false" CssClass="SubmitButton" ValidationGroup="Login" />
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlStatic" runat="server">
        <table style="vertical-align: top">
            <tr>
                <td class="FieldLabel" colspan="2">
                    <asp:CheckBox ID="chkHasLogin1" runat="server" CssClass="CheckBoxMovedLeft" Checked="true"
                        Enabled="false" EnableViewState="false" />
                    <asp:Label runat="server" ID="lblHasLogin1" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" nowrap="nowrap">
                    <cms:LocalizedLabel ID="lblUserName1" runat="server" EnableViewState="false" ResourceString="general.username"
                        DisplayColon="true" />
                </td>
                <td style="width: 100%;">
                    <asp:Label runat="server" ID="lblUserNameStaticValue" EnableViewState="false" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    </asp:Panel>
</asp:Content>
