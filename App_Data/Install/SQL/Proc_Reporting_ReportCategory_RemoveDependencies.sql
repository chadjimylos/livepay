-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Reporting_ReportCategory_RemoveDependencies] 
	@CategoryID int
AS
BEGIN
	DELETE FROM Reporting_ReportValue WHERE ValueReportID IN (
		SELECT ReportID FROM Reporting_Report WHERE ReportCategoryID=@CategoryID);
	DELETE FROM Reporting_ReportTable WHERE TableReportID IN (
		SELECT ReportID FROM Reporting_Report WHERE ReportCategoryID=@CategoryID);
	DELETE FROM Reporting_ReportGraph WHERE GraphReportID IN (
		SELECT ReportID FROM Reporting_Report WHERE ReportCategoryID=@CategoryID);
	-- SavedReport
	DELETE FROM Reporting_SavedGraph WHERE SavedGraphSavedReportID IN (
		SELECT SavedReportID FROM Reporting_SavedReport WHERE SavedReportReportID IN (
			SELECT ReportID FROM Reporting_Report WHERE ReportCategoryID=@CategoryID)
		);
	DELETE FROM Reporting_SavedReport WHERE SavedReportReportID IN (
		SELECT ReportID FROM Reporting_Report WHERE ReportCategoryID=@CategoryID);
	-- Report
	DELETE FROM Reporting_Report WHERE ReportCategoryID=@CategoryID;
END
