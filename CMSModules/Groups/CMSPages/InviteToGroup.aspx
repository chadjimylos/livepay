<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CMSMasterPages/LiveSite/Dialogs/ModalDialogPage.master"
    Theme="Default" CodeFile="InviteToGroup.aspx.cs" Inherits="CMSModules_Groups_CMSPages_InviteToGroup" %>

<%@ Register Src="~/CMSModules/Groups/Controls/GroupInvite.ascx" TagName="GroupInvite"
    TagPrefix="cms" %>
<asp:Content ID="cntContent" runat="server" ContentPlaceHolderID="plcContent">
    <div class="PageContent">
        <div class="CommunityInviteToGroup">
            <cms:groupinvite id="groupInviteElem" runat="server" displaybuttons="false" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:cmsbutton runat="server" cssclass="SubmitButton" id="btnInvite" enableviewstate="false" />
        <cms:localizedbutton cssclass="SubmitButton" id="btnCancel" OnClientClick = "Close();" runat="server" resourcestring="General.cancel"
            enableviewstate="false" />
    </div>
</asp:Content>
