<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImportConfiguration.ascx.cs"
    Inherits="CMSModules_ImportExport_Controls_ImportConfiguration" %>
<asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
<table>
    <tr>
        <td>
            <asp:Label runat="server" ID="lblImports" EnableViewState="false" CssClass="InfoLabel" /><cms:CMSUpdatePanel runat="server" ID="pnlUpdate" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:ListBox runat="server" ID="lstImports" CssClass="ContentListBoxLow" Width="450"
                        Enabled="false" />
                    <div class="ClearBoth">&nbsp;</div>
                    <asp:Panel runat="server" ID="pnlRefresh" CssClass="PageHeaderItemRight" Style="padding: 8px 5px">
                        <asp:Image runat="server" ID="imgRefresh" CssClass="NewItemImage" /><cms:LocalizedLinkButton
                            runat="server" ID="btnRefresh" CssClass="NewItemLink" ResourceString="general.refresh"
                            OnClick="btnRefresh_Click" />
                    </asp:Panel>
                </ContentTemplate>
            </cms:CMSUpdatePanel>
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton ID="radAll" runat="server" GroupName="Import" Checked="true" CssClass="RadioImport" />
            <asp:RadioButton ID="radNew" runat="server" GroupName="Import" CssClass="RadioImport" />
        </td>
    </tr>
</table>
