﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Partial Class CMSTemplates_LivePay_Merchants_UcHomePage
    Inherits CMSUserControl


#Region "Variables"

    ''' <summary>
    ''' My property
    ''' </summary>
    Private mMyProperty As String = Nothing

#End Region

#Region "Properties"


    ''' <summary>
    ''' Gets or sets value of MyProperty
    ''' </summary>
    Public Property MyProperty() As String
        Get
            Return mMyProperty
        End Get
        Set(ByVal value As String)
            mMyProperty = value
        End Set
    End Property

#End Region

#Region "Methods"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        BtnSend.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/Contact/btnSend.png|en-us=/App_Themes/LivePay/Contact/btnSend_en-us.png$}")
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        RequiredFieldValidator3.ErrorMessage = ResHelper.LocalizeString("{$=Επωνυμία Επιχείρησης|en-us=Company Name$}")
        RequiredFieldValidator1.ErrorMessage = ResHelper.LocalizeString("{$=Υπεύθυνος Επικοινωνίας|en-us=Contact Person$}")
        RequiredFieldValidator2.ErrorMessage = ResHelper.LocalizeString("{$=Τηλεφωνο Επικοινωνίας|en-us=Phone Number$}")
        RegularExpressionValidator1.ErrorMessage = ResHelper.LocalizeString("{$=Συμπληρώστε έγκυρη E-mail διεύθυνση|en-us=Enter a valid E-mail address$}")
        RequiredFieldValidator5.ErrorMessage = ResHelper.LocalizeString("{$=Α.Φ.Μ.|en-us=Vat Number$}")
        RequiredFieldValidator6.ErrorMessage = ResHelper.LocalizeString("{$=Σχόλια|en-us=Comments$}")
        ValidationSummary1.HeaderText = ResHelper.LocalizeString("{$=Παρακαλώ συμπληρώστε τα ακόλουθα πεδία:|en-us=Please complete the following fields:$}")
    End Sub

#End Region

End Class
