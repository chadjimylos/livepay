using System;
using System.Collections;

using CMS.Community;
using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Friends_CMSPages_Friends_Reject : CMSLiveModalPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), string.Empty) != string.Empty)
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Friends);
        }

        FriendsReject.SelectedFriends = null;
        FriendsReject.IsLiveSite = true;
        int requestedId = QueryHelper.GetInteger("requestid", 0);
        int userId = QueryHelper.GetInteger("userid", 0);
        int friendshipId = 0;
        CurrentMaster.Title.TitleText = ResHelper.GetString("friends.rejectfriendship");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Friends/rejected.png");

        // Multiple selection
        if (Request["ids"] != null)
        {
            string[] items = Request["ids"].Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            if (items.Length > 0)
            {
                ArrayList friends = new ArrayList();
                foreach (string item in items)
                {
                    friends.Add(ValidationHelper.GetInteger(item, 0));
                }
                FriendsReject.SelectedFriends = friends;
                if (friends.Count == 1)
                {
                    friendshipId = Convert.ToInt32(friends[0]);
                }
            }
        }
        // For one user
        else
        {
            FriendsReject.RequestedUserID = requestedId;
        }

        FriendInfo fi = null;
        if (friendshipId != 0)
        {
            fi = FriendInfoProvider.GetFriendInfo(friendshipId);
        }
        else if (requestedId != 0)
        {
            fi = FriendInfoProvider.GetFriendInfo(userId, requestedId);
        }

        if (fi != null)
        {
            UserInfo requestedUser = (userId == fi.FriendRequestedUserID) ? UserInfoProvider.GetFullUserInfo(fi.FriendUserID) : UserInfoProvider.GetFullUserInfo(fi.FriendRequestedUserID);
            string fullUserName = UserInfoProvider.GetFullUserName(Functions.GetFormattedUserName(requestedUser.UserName, true), requestedUser.UserNickName);
            Page.Title = ResHelper.GetString("friends.rejectfriendshipwith") + " " + HTMLHelper.HTMLEncode(fullUserName);
            CurrentMaster.Title.TitleText = Page.Title;
        }

        // Set current user
        FriendsReject.UserID = userId;
    }
}
