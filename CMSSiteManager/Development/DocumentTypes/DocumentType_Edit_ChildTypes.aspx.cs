using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_ChildTypes : SiteManagerPage
{
    #region "Variables"

    protected static int classId = 0;
    protected string currentValues = "";

    #endregion


    #region "Page Events"

    protected void Page_Load(object sender, EventArgs e)
    {
        classId = QueryHelper.GetInteger("documenttypeid", 0);
        if (classId > 0)
        {
            // Initializes labels
            lblTitle.Text = ResHelper.GetString("documenttype_edit_childtypes.info");

            // Get the active sites
            DataSet ds = DataClassInfoProvider.GetAllChildClasses(classId);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "ChildClassID"));
            }

            if (!IsPostBack)
            {
                this.uniSelector.Value = currentValues;
            }

            uniSelector.WhereCondition = "ClassIsDocumentType = 1";
            uniSelector.ResourcePrefix = "allowedclasscontrol";
            uniSelector.DisplayNameFormat = "{%ClassDisplayName%} ({%ClassName%})";
            uniSelector.OnSelectionChanged += new EventHandler(uniSelector_OnSelectionChanged);
        }
    }


    protected void uniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        // Remove old items
        string newValues = ValidationHelper.GetString(uniSelector.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int childId = ValidationHelper.GetInteger(item, 0);
                    AllowedChildClassInfoProvider.RemoveAllowedChildClass(classId, childId);
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int childId = ValidationHelper.GetInteger(item, 0);
                    AllowedChildClassInfoProvider.AddAllowedChildClass(classId, childId);
                }
            }
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("general.changessaved");
    }

    #endregion
}
