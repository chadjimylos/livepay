using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_WebContainers_Container_New : SiteManagerPage
{
    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Init labels
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");

        // Initialize the master page
        InitializeMasterPage();
    }


    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    private void InitializeMasterPage()
    {
        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.Title.HelpTopicName = "newedit_container";

        // Init the breadcrumbs
        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("Container_Edit.ItemListLink");
        breadcrumbs[0, 1] = ResolveUrl("Container_List.aspx");
        breadcrumbs[0, 2] = "";
        breadcrumbs[1, 0] = ResHelper.GetString("Container_Edit.NewItemCaption");
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;

        // Init the title and icon
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Container_Edit.NewHeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_WebPartContainer/new.png");
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Validate
        string errorMessage = new Validator().NotEmpty(txtContainerDisplayName.Text, rfvDisplayName.ErrorMessage)
            .NotEmpty(txtContainerName.Text, rfvCodeName.ErrorMessage)
            .IsCodeName(txtContainerName.Text, ResHelper.GetString("general.invalidcodename"))
            .Result;

        if (errorMessage == "")
        {
            // Create new webpart container
            WebPartContainerInfo webPartContainerObj = new WebPartContainerInfo();
            webPartContainerObj.ContainerTextBefore = txtContainerTextBefore.Text;
            webPartContainerObj.ContainerTextAfter = txtContainerTextAfter.Text;
            webPartContainerObj.ContainerName = txtContainerName.Text.Trim();
            webPartContainerObj.ContainerDisplayName = txtContainerDisplayName.Text.Trim();

            // Check for duplicity
            WebPartContainerInfo wPcI = WebPartContainerInfoProvider.GetWebPartContainerInfo(webPartContainerObj.ContainerName);
            if (wPcI != null)
            {
                errorMessage = ResHelper.GetString("Container_Edit.UniqueError");
            }
            else
            {
                WebPartContainerInfoProvider.SetWebPartContainerInfo(webPartContainerObj);
                UrlHelper.Redirect("Container_Edit_Frameset.aspx?containerid=" + webPartContainerObj.ContainerID.ToString() + "&saved=1");
            }
        }

        // Display error mesage if any
        if (!String.IsNullOrEmpty(errorMessage))
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }

    #endregion
}
