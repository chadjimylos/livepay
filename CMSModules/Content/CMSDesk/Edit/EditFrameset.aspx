<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditFrameset.aspx.cs" Inherits="CMSModules_Content_CMSDesk_Edit_EditFrameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Content - Edit</title>
    <base target="_self" />

    <script src="editframeset.js" type="text/javascript"></script>

    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
</head>

<script src="<%= ResolveUrl("~/CMSScripts/progress.js") %>" type="text/javascript"></script>

<frameset border="0" id="editFrameset" rows="<%=menuSize%>,<%=toolbarSize%>,*" border="0">
		<frame name="editheader" src="<%=menupage%>" scrolling="no" noresize="noresize" frameborder="0" />
		<frame name="edittoolbar" src="<%=toolbarpage%>" scrolling="auto" frameborder="0" />
		<frame name="editview" src="<%=viewpage%>" scrolling="auto" frameborder="0" />		
		<noframes>
			<p id="p1">
				This HTML frameset displays multiple Web pages. To view this frameset, use a 
				Web browser that supports HTML 4.0 and later.
			</p>
		</noframes>
	</frameset>
</html>
