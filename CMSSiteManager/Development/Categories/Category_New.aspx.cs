using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Categories_Category_New : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Category_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/Categories/Category_List.aspx";
        pageTitleTabs[0, 2] = "_self";
        pageTitleTabs[1, 0] = ResHelper.GetString("Category_Edit.NewCategory");
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Category_Edit.NewHeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Category/new.png");
        this.CurrentMaster.Title.HelpTopicName = "category_general";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        this.catEdit.RefreshPageURL = "~/CMSSiteManager/Development/Categories/Category_Edit.aspx";
    
    }
}
