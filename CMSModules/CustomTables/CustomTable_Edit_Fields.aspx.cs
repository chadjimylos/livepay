using System;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.FormEngine;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.CMSHelper;
using CMS.URLRewritingEngine;

public partial class CMSModules_CustomTables_CustomTable_Edit_Fields : CMSCustomTablesPage
{
    #region "Variables"

    protected DataClassInfo dci = null;
    protected string className = null;
    private EventLogProvider mEventLog = null;
    private FormInfo mFormInfo = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Event log
    /// </summary>
    public EventLogProvider EventLog
    {
        get
        {
            if (mEventLog == null)
            {
                mEventLog = new EventLogProvider();
            }
            return mEventLog;
        }
    }


    /// <summary>
    /// Form info
    /// </summary>
    public FormInfo FormInfo
    {
        get
        {
            if ((mFormInfo == null) && (dci != null))
            {
                mFormInfo = new FormInfo(dci.ClassFormDefinition);
            }
            return mFormInfo;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        int classId = QueryHelper.GetInteger("customtableid", 0);
        dci = DataClassInfoProvider.GetDataClass(classId);

        // Class exists
        if (dci != null)
        {
            className = dci.ClassName;
            if (dci.ClassIsCoupledClass)
            {
                FieldEditor.Visible = true;
                FieldEditor.ClassName = className;
                FieldEditor.Mode = FieldEditorModeEnum.CustomTable;

                // GUID column is not present
                bool guidColumnExists = (FormInfo.GetFormField("ItemGUID") != null);
                if (!guidColumnExists)
                {
                    btnGUID.Text = ResHelper.GetString("customtable.GenerateGUID");
                    btnGUID.Click += btnGUID_Click;
                    lblGUID.Text = ResHelper.GetString("customtable.GUIDColumMissing");
                }
                plcGUID.Visible = !guidColumnExists;
            }
            else
            {
                lblError.Text = ResHelper.GetString("customtable.ErrorNoFields");
            }

            if (!RequestHelper.IsPostBack() && QueryHelper.GetBoolean("gen", false))
            {
                lblError.Visible = true;
                lblError.CssClass = "InfoLabel";
                lblError.Text = ResHelper.GetString("customtable.GUIDFieldGenerated");
            }
        }
    }


    void btnGUID_Click(object sender, EventArgs e)
    {
        try
        {
            // Create GUID field
            FormFieldInfo ffiGuid = new FormFieldInfo();

            // Fill FormInfo object
            ffiGuid.Name = "ItemGUID";
            ffiGuid.Caption = "GUID";
            ffiGuid.DataType = FormFieldDataTypeEnum.GUID;
            ffiGuid.DefaultValue = "";
            ffiGuid.Description = "";
            ffiGuid.FieldType = FormFieldControlTypeEnum.LabelControl;
            ffiGuid.PrimaryKey = false;
            ffiGuid.System = true;
            ffiGuid.Visible = false;
            ffiGuid.Size = 0;
            ffiGuid.AllowEmpty = false;

            FormInfo.AddFormField(ffiGuid);

            // Update table structure - columns could be added
            bool old = TableManager.UpdateSystemFields;
            TableManager.UpdateSystemFields = true;
            string schema = FormInfo.GetXmlDefinition();
            TableManager.UpdateTableBySchema(dci.ClassTableName, schema);
            TableManager.UpdateSystemFields = old;

            // Update xml schema and form definition
            dci.ClassFormDefinition = schema;
            dci.ClassXmlSchema = TableManager.GetXmlSchema(dci.ClassTableName);

            dci.LogEvents = false;
            DataClassInfoProvider.SetDataClass(dci);
            dci.LogEvents = true;

            // Generate default queries
            SqlGenerator.GenerateDefaultQueries(dci, true, false);

            // Ensure GUIDs for all items
            CustomTableItemProvider tableProvider = new CustomTableItemProvider();
            tableProvider.UpdateSystemFields = false;
            tableProvider.LogSynchronization = false;
            DataSet dsItems = tableProvider.GetItems(className, null, null);
            if (!DataHelper.DataSourceIsEmpty(dsItems))
            {
                foreach (DataRow dr in dsItems.Tables[0].Rows)
                {
                    CustomTableItem item = new CustomTableItem(dr, className, tableProvider);
                    item.ItemGUID = Guid.NewGuid();
                    item.Update();
                }
            }

            // Log event
            UserInfo currentUser = CMSContext.CurrentUser;
            EventLog.LogEvent("I", DateTime.Now, "Custom table", "GENERATEGUID", currentUser.UserID, currentUser.UserName, 0, null, null, string.Format(ResHelper.GetAPIString("customtable.GUIDGenerated", "Field 'ItemGUID' for custom table '{0}' was created and GUID values were generated."), dci.ClassName), 0, null);

            UrlHelper.Redirect(UrlHelper.AddParameterToUrl(URLRewriter.CurrentURL, "gen", "1"));
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("customtable.ErrorGUID") + ex.Message;

            // Log event
            EventLog.LogEvent("Custom table", "GENERATEGUID", ex);
        }
    }
}
