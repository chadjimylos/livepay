using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_SystemTables_SystemTable_Header : SiteManagerPage
{
    DataClassInfo dci = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        int classId = QueryHelper.GetInteger("classid", 0);

        if (classId > 0)
        {
            dci = DataClassInfoProvider.GetDataClass(classId);
        }

        // Initializes page title
        string[,] breadcrumbs = new string[2, 3];

        breadcrumbs[0, 0] = ResHelper.GetString("systbl.header.listlink");
        breadcrumbs[0, 1] = "~/CMSSiteManager/Development/SystemTables/SystemTable_List.aspx";
        breadcrumbs[0, 2] = "_parent";
        breadcrumbs[1, 0] = (dci != null) ? dci.ClassDisplayName : String.Empty;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("SystemTable_Edit.Title"); ;
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_SystemTable/object.png");
        this.CurrentMaster.Title.HelpTopicName = "system_tables_fields";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        if (!RequestHelper.IsPostBack())
        {
            // Initialize menu
            InitializeMenu(classId);
        }
    }


    /// <summary>
    /// Initializes edit header
    /// </summary>
    /// <param name="docTypeId">DocumentTypeID</param>
    protected void InitializeMenu(int objId)
    {
        string[,] tabs = new string[4, 4];
        tabs[0, 0] = ResHelper.GetString("general.fields");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'system_tables_fields');";
        tabs[0, 2] = "SystemTable_Edit_Fields.aspx?classid=" + objId;
        tabs[1, 0] = ResHelper.GetString("systbl.header.query");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'system_tables_queries');";
        tabs[1, 2] = "SystemTable_Edit_Query_List.aspx?classid=" + objId;
        tabs[2, 0] = ResHelper.GetString("systbl.header.altforms");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'system_tables_alt_forms');";
        tabs[2, 2] = "AlternativeForms/AlternativeForms_List.aspx?classid=" + objId;

        if (dci != null)
        {
            if (String.Compare(dci.ClassName, "ecommerce.sku", true) == 0)
            {
                tabs[3, 0] = ResHelper.GetString("systbl.header.search");
                tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'system_tables_search_fields_tab');";
                tabs[3, 2] = UrlHelper.EncodeQueryString("SystemTable_Edit_Search.aspx?classname=cms.document");
            }

            if (String.Compare(dci.ClassName,"cms.user", true) == 0)
            {
                tabs[3, 0] = ResHelper.GetString("systbl.header.search");
                tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'system_tables_search_full_fields_tab');";
                tabs[3, 2] = UrlHelper.EncodeQueryString("SystemTable_Edit_SearchFields.aspx?classname=cms.user");
            }
        }
        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "content";
    }
}
