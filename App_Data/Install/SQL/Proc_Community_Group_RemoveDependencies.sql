-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Community_Group_RemoveDependencies]	
	@GroupID int
AS
BEGIN	
	SET NOCOUNT ON;
    -- Get AvatarID from current group
    DECLARE @AvatarID int;
    SELECT @AvatarID = GroupAvatarID FROM [Community_Group] WHERE GroupID=@GroupID
    IF (@AvatarID > 0)
    BEGIN
      DECLARE @AvatarIsCustom bit;
      SELECT @AvatarIsCustom = AvatarIsCustom FROM [CMS_Avatar]
      IF (@AvatarIsCustom=1)
      BEGIN
	    UPDATE [Community_Group] SET GroupAvatarID=NULL WHERE GroupAvatarID=@AvatarID;
        DELETE FROM [CMS_Avatar] WHERE AvatarID=@AvatarID
      END
    END
	-- Members
	DELETE FROM [Community_GroupMember] WHERE MemberGroupID = @GroupID
	-- Group role permission
	DELETE FROM [Community_GroupRolePermission] WHERE GroupID = @GroupID
	-- Forums
	UPDATE [Forums_ForumPost] SET PostParentID=NULL WHERE PostForumId IN (
			SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
			SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupGroupID = @GroupID
		));
	DELETE FROM [Forums_ForumSubscription] WHERE SubscriptionPostID IN (
			SELECT PostID FROM Forums_ForumPost WHERE (PostForumID  IN (
			SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
			SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupGroupID = @GroupID
		))));
	DELETE FROM [Forums_UserFavorites] WHERE PostID IN (
			SELECT PostID FROM [Forums_ForumPost] WHERE ( PostForumId IN (  
			SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
			SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupGroupID = @GroupID
		))));
	DELETE FROM [Forums_UserFavorites] WHERE ForumID IN (
			SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
			SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupGroupID = @GroupID
		));
	DELETE FROM [Forums_Attachment] WHERE AttachmentPostID IN (
			SELECT PostID FROM [Forums_ForumPost] WHERE ( PostForumId IN (  
			SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
			SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupGroupID = @GroupID
		))));	
	DELETE FROM [Forums_ForumPost] WHERE PostForumId IN (
			SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
			SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupGroupID = @GroupID
		));
	DELETE FROM [Forums_ForumRoles ] WHERE ForumId IN (
			SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
			SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupGroupID = @GroupID
		));
	DELETE FROM [Forums_ForumModerators] WHERE ForumID IN (
			SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
			SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupGroupID = @GroupID
		));
	DELETE FROM [Forums_ForumSubscription] WHERE SubscriptionForumId IN (
			SELECT ForumID FROM [Forums_Forum] WHERE ForumGroupID IN (
			SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupGroupID = @GroupID
		));
	DELETE FROM [Forums_Forum] WHERE ForumGroupId IN (
			SELECT GroupID FROM [Forums_ForumGroup] WHERE GroupGroupID = @GroupID
		);
	DELETE FROM Forums_ForumGroup WHERE GroupGroupID = @GroupID;
	-- Polls
	DELETE FROM [Polls_PollRoles] WHERE PollID IN (
			SELECT PollID FROM [Polls_Poll] WHERE PollGroupID = @GroupID
		);
	DELETE FROM [Polls_PollSite] WHERE PollID IN (
			SELECT PollID FROM [Polls_Poll] WHERE PollGroupID = @GroupID
		);
	DELETE FROM [Polls_PollAnswer] WHERE AnswerPollID IN (
			SELECT PollID FROM [Polls_Poll] WHERE PollGroupID = @GroupID
		);
	DELETE FROM [Polls_Poll] WHERE PollGroupID = @GroupID;
	-- Roles
	DELETE FROM [CMS_UserRole] WHERE RoleID IN (
			SELECT RoleID FROM [CMS_Role] WHERE RoleGroupID = @GroupID
		);
	DELETE FROM [CMS_RolePermission] WHERE RoleID IN (
			SELECT RoleID FROM [CMS_Role] WHERE RoleGroupID = @GroupID
		);
	DELETE FROM [CMS_ACLItem] WHERE RoleID IN (
			SELECT RoleID FROM [CMS_Role] WHERE RoleGroupID = @GroupID
		);
	DELETE FROM [CMS_WorkflowStepRoles] WHERE RoleID IN (
			SELECT RoleID FROM [CMS_Role] WHERE RoleGroupID = @GroupID
		);
    DELETE FROM [FORUMS_ForumRoles] WHERE RoleID IN (
			SELECT RoleID FROM [CMS_Role] WHERE RoleGroupID = @GroupID
		);
	DELETE FROM [Polls_PollRoles] WHERE RoleID IN (
			SELECT RoleID FROM [CMS_Role] WHERE RoleGroupID = @GroupID
		);
	DELETE FROM [Board_Role] WHERE RoleID IN (
			SELECT RoleID FROM [CMS_Role] WHERE RoleGroupID = @GroupID
		);
		
	DELETE FROM [CMS_RoleUIElement] WHERE RoleID IN (
		SELECT RoleID FROM [CMS_Role] WHERE RoleGroupID = @GroupID
	);
	DELETE FROM [CMS_Role] WHERE RoleGroupID = @GroupID;
	-- Group message boards
    DELETE FROM Board_Message WHERE MessageBoardID IN ( SELECT BoardID FROM Board_Board WHERE BoardGroupID = @GroupID );
	DELETE FROM Board_Moderator WHERE BoardID IN ( SELECT BoardID FROM Board_Board WHERE BoardGroupID = @GroupID );
	DELETE FROM Board_Role WHERE BoardID IN ( SELECT BoardID FROM Board_Board WHERE BoardGroupID = @GroupID );
	DELETE FROM Board_Subscription WHERE SubscriptionBoardID IN (SELECT BoardID FROM Board_Board WHERE BoardGroupID = @GroupID);
	DELETE FROM Board_Board WHERE BoardGroupID = @GroupID;
	-- Group invitation
	DELETE FROM Community_Invitation WHERE InvitationGroupID = @GroupID;
	-- Remove refrences to the tree node
	UPDATE CMS_TREE SET NodeGroupID = NULL WHERE NodeGroupID = @GroupID;
END
