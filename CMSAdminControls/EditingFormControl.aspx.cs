using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSAdminControls_EditingFormControl : CMSModalPage
{
    #region "Variables"

    // Initial text of textarea
    protected string initialText = "";
    protected string selectorId = "";
    protected string controlPanelId = "";
    protected string selectorPanelId = "";

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get query string parameters
        selectorId = QueryHelper.GetText("selectorid", "");
        initialText = QueryHelper.GetText("value", "").Replace("%%NEWLINE%%", "\n");
        controlPanelId = QueryHelper.GetText("controlPanelId", "");
        selectorPanelId = QueryHelper.GetText("selectorpanelid", "");

        // Initialize UI
        CurrentMaster.Title.TitleText = ResHelper.GetString("EditingFormControl.TitleText");
        CurrentMaster.Title.TitleImage = GetImageUrl("Design/Controls/EditingFormControl/title.png");

        this.btnOk.Text = ResHelper.GetString("General.Save");
        this.btnRemove.Text = ResHelper.GetString("editingformcontrol.removemacros");

        this.btnOk.OnClientClick = "setValueToParent(" + ScriptHelper.GetString(selectorId) + ", " + ScriptHelper.GetString(controlPanelId) + ", " + ScriptHelper.GetString(selectorPanelId) + "); window.close(); return false;";
        this.btnRemove.OnClientClick = "removeMacro(" + ScriptHelper.GetString(selectorId) + ", " + ScriptHelper.GetString(controlPanelId) + ", " + ScriptHelper.GetString(selectorPanelId) + "); window.close(); return false;";

        this.btnCancel.Text = ResHelper.GetString("General.Cancel");

        // Initialize Resolver
        this.macroElem.Resolver = MacroSelector.WebpartResolver;
        this.macroElem.JavaScripFunction = "insertAtCursorPosition";

        this.ltlScript.Text = ScriptHelper.GetScript("var txtElem = document.getElementById('" + txtArea.ClientID + "');");
        txtArea.Value = initialText;
    }
}
