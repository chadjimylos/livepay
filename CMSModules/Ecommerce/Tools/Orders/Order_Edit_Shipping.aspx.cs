using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_Shipping : CMSEcommercePage
{
    protected int orderId = 0;
    protected int customerId = 0;
    protected ShoppingCartInfo mShoppingCartInfoObj = null;


    private ShoppingCartInfo ShoppingCartInfoObj
    {
        get
        {
            if (mShoppingCartInfoObj == null)
            {
                mShoppingCartInfoObj = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(orderId);
            }
            return mShoppingCartInfoObj;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AddressChange", ScriptHelper.GetScript("   function AddressChange(AddressId) { if (AddressId > 0) { document.getElementById('" + hdnAddress.ClientID + "').value = AddressId; " + ClientScript.GetPostBackEventReference(btnChange, "") + " } } "));

        lblAddress.Text = ResHelper.GetString("Order_Edit_Shipping.lblAddress");
        lblOption.Text = ResHelper.GetString("Order_Edit_Shipping.lblOption");
        lblTrackingNumber.Text = ResHelper.GetString("order_edit.lblTrackingNumber");

        btnEdit.Text = ResHelper.GetString("general.edit");
        btnNew.Text = ResHelper.GetString("general.new");
        btnOk.Text = ResHelper.GetString("General.OK");

        btnOk.Click += new EventHandler(btnOk_Click);
        addressElem.DropDownSingleSelect.SelectedIndexChanged += new EventHandler(DropDownSingleSelect_SelectedIndexChanged);
        addressElem.DropDownSingleSelect.AutoPostBack = true;

        orderId = ValidationHelper.GetInteger(Request.QueryString["orderId"], 0);
        LoadData();

        btnNew.OnClientClick = "AddAddress('" + customerId + "'); return false;";
        btnEdit.OnClientClick = "EditAddress('" + customerId + "','" + addressElem.AddressID + "'); return false;";
    }


    /// <summary>
    /// Initialize shipping option dropdown list.
    /// </summary>
    protected void InitializeShippingList()
    {
        if ((ShoppingCartInfoObj != null) && (CMSContext.CurrentSite != null))
        {

            drpShipping.Items.Add(new ListItem(ResHelper.GetString("general.selectnone"), ""));

            ListItem li = null;
            DataSet ds = ShippingOptionInfoProvider.GetShippingOptions(ShoppingCartInfoObj, StatusEnum.Both);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                // for each row in shipping options
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    li = new ListItem();

                    // Apply free shipping limit
                    double shipping = ShippingOptionInfoProvider.ApplyShippingFreeLimit(
                                               ValidationHelper.GetDouble(dr["ShippingOptionCharge"], 0) / ShoppingCartInfoObj.ExchangeRate,
                                               ShoppingCartInfoObj.TotalPrice, CMSContext.CurrentSite.SiteStoreShippingFreeLimit);

                    // Get ShippingOptionCharge, apply free shipping, format the price into corrent format
                    string detailInfo = "";
                    if (shipping > 0)
                    {
                        detailInfo = "(" + CurrencyInfoProvider.GetFormatedPrice(shipping, ShoppingCartInfoObj.CurrencyInfoObj) + ")";
                    }

                    if (CultureHelper.IsUICultureRTL())
                    {
                        li.Text += (detailInfo == "" ? "" : detailInfo + " ") + Convert.ToString(dr["ShippingOptionDisplayName"]);
                    }
                    else
                    {
                        li.Text = Convert.ToString(dr["ShippingOptionDisplayName"]) + (detailInfo == "" ? "" : " " + detailInfo);
                    }
                    li.Value = Convert.ToString(dr["ShippingOptionID"]);

                    // Change class according to the enabled state of the shipping option
                    if (ValidationHelper.GetBoolean(dr["ShippingOptionEnabled"], true))
                    {
                        li.Attributes.Add("class", "DropDownItemEnabled");
                    }
                    else
                    {
                        li.Attributes.Add("class", "DropDownItemDisabled");
                    }

                    drpShipping.Items.Add(li);
                }
            }
        }

    }


    protected void DropDownSingleSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnAddress.Value = addressElem.AddressID.ToString();
    }


    protected void btnOk_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        OrderInfo oi = OrderInfoProvider.GetOrderInfo(orderId);
        if (oi != null)
        {
            // Check whether some address is selected
            int addressId = addressElem.AddressID;
            if (addressId <= 0)
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Order_Edit_Shipping.NoAddress");
                return;
            }

            try
            {
                oi.OrderTrackingNumber = txtTrackingNumber.Text.Trim();
                oi.OrderShippingAddressID = addressId;
                oi.OrderShippingOptionID = ValidationHelper.GetInteger(drpShipping.SelectedValue, 0);

                // Load the shopping cart to process the data
                ShoppingCartInfo sci = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(orderId);
                if (sci != null)
                {
                    SiteInfo si = SiteInfoProvider.GetSiteInfo(oi.OrderSiteID);
                    if (si != null)
                    {
                        sci.SiteName = si.SiteName;
                    }
                    else
                    {
                        sci.SiteName = CMSContext.CurrentSiteName;
                    }
                    sci.ExchangeRate = CurrencyInfoProvider.GetLastValidExchangeRate(sci.ShoppingCartCurrencyID);
                    sci.ShoppingCartShippingOptionID = oi.OrderShippingOptionID;
                    ShoppingCartInfoProvider.EvaluateShoppingCartContent(sci);

                    // Update shipping charge, total price, total tax
                    oi.OrderTotalPrice = sci.TotalPrice;
                    oi.OrderTotalShipping = sci.TotalShipping;
                    oi.OrderTotalTax = sci.TotalTax;

                    // Delete shopping cart info (including all items) from database
                    ShoppingCartInfoProvider.DeleteShoppingCartInfo(sci.ShoppingCartID);
                }

                // Set order info
                OrderInfoProvider.SetOrderInfo(oi);

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
            }
        }
    }


    private void LoadData()
    {
        OrderInfo oi = OrderInfoProvider.GetOrderInfo(orderId);
        if (oi != null)
        {
            customerId = oi.OrderCustomerID;
            addressElem.CustomerID = customerId;

            if (!RequestHelper.IsPostBack())
            {
                InitializeShippingList();

                txtTrackingNumber.Text = oi.OrderTrackingNumber;
                addressElem.CustomerID = customerId;
                addressElem.AddressID = oi.OrderShippingAddressID;
                try
                {
                    drpShipping.SelectedValue = oi.OrderShippingOptionID.ToString();
                }
                catch { }
            }
        }
    }
}
