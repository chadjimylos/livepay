using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_TaxClasses_TaxClass_State : CMSEcommerceConfigurationPage
{
    protected string mSave = "";
    protected int mTaxClassId = 0;
    protected int mCountryId = 0;

    private Hashtable mChangedTextBoxes = new Hashtable();
    private Hashtable mChangedCheckBoxes = new Hashtable();

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        gvStates.Columns[1].Visible = false;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get tax class Id from URL
        mTaxClassId = ValidationHelper.GetInteger(Request.QueryString["taxclassid"], 0);

        if (!RequestHelper.IsPostBack())
        {
            // Fill the drpCountry with countries which have some states or colonies
            drpCountry.DataSource = TaxClassInfoProvider.GetAllCountriesWithStates();
            drpCountry.DataValueField = "CountryID";
            drpCountry.DataTextField = "CountryDisplayName";
            drpCountry.DataBind();
        }
        // Set grid view properties
        gvStates.Columns[0].HeaderText = ResHelper.GetString("taxclass_state.gvstates.state");
        gvStates.Columns[1].HeaderText = ResHelper.GetString("Code");
        gvStates.Columns[2].HeaderText = ResHelper.GetString("taxclass_state.gvstates.value");
        gvStates.Columns[3].HeaderText = ResHelper.GetString("taxclass_state.gvstates.isflat");
        gvStates.Columns[4].HeaderText = ResHelper.GetString("StateId");

        gvStates.GridLines = GridLines.Horizontal;

        LoadGridViewData();

        lblCountry.Text = ResHelper.GetString("TaxClass_State.lblCountry");

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>" +
            ScriptHelper.GetScript("function SaveDocument() { " + this.ClientScript.GetPostBackEventReference(this.btnSave, null) + " } \n"
        ));

        this.imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        mSave = ResHelper.GetString("general.save");
    }


    protected void LoadGridViewData()
    {
        gvStates.Columns[4].Visible = true;
        mCountryId = ValidationHelper.GetInteger(drpCountry.SelectedValue, 0);
        DataSet ds = TaxClassInfoProvider.GetTaxClassStates(mTaxClassId, mCountryId);
        gvStates.DataSource = ds.Tables[0];
        gvStates.DataBind();
        gvStates.Columns[4].Visible = false;
    }


    protected void drpCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadGridViewData();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        TaxClassStateInfo stateInfo;
        // temporary references to server controls
        Label lblStateId = null;
        int stateId = 0;
        string stateName;
        TextBox txtValue = null;
        double value = 0.0;
        CheckBox chkIsFlat = null;
        bool isFlat = false;

        // go through the gridview
        for (int i = 0; i < gvStates.Rows.Count; i++)
        {
            // get stateId from unvisible label
            lblStateId = (Label)gvStates.Rows[i].Cells[4].Controls[0];
            stateId = ValidationHelper.GetInteger(lblStateId.Text, 0);

            if (stateId > 0)
            {
                // set temporary references to server controls in the gridview
                stateName = ((Label)gvStates.Rows[i].Cells[0].Controls[1]).Text;
                txtValue = (TextBox)gvStates.Rows[i].Cells[2].Controls[1];
                chkIsFlat = (CheckBox)gvStates.Rows[i].Cells[3].Controls[1];

                // look up into the hash table
                if ((mChangedTextBoxes[txtValue.ID] != null) || (mChangedCheckBoxes[chkIsFlat.ID]) != null)
                {
                    //if text box was set to "" then delete
                    if (txtValue.Text == "")
                    {
                        TaxClassInfoProvider.DeleteTaxClassStateInfo(stateId, mTaxClassId);
                        chkIsFlat.Checked = false;
                    }
                    else
                    {
                        // Update                      

                        //tax value must be greater or equal to zero
                        if (ValidationHelper.IsPositiveNumber(txtValue.Text))
                        {
                            // get values from gridview
                            value = ValidationHelper.GetDouble(txtValue.Text, 0);
                            isFlat = chkIsFlat.Checked;

                            stateInfo = new TaxClassStateInfo();
                            // Set stateInfo
                            stateInfo.StateID = stateId;
                            stateInfo.TaxClassID = mTaxClassId;
                            stateInfo.TaxValue = value;
                            stateInfo.IsFlatValue = isFlat;
                            // Store it in DB
                            TaxClassInfoProvider.SetTaxClassStateInfo(stateInfo);
                        }
                        else
                        {
                            //add state name where an error occurs, error message will be added after
                            lblError.Text += stateName + ", ";
                            lblError.Visible = true;
                        }
                    }
                }
            }
        }

        // add error message text
        if (lblError.Visible == true)
        {
            //delete last comma
            if (lblError.Text.EndsWith(", "))
            {
                lblError.Text = lblError.Text.Remove(lblError.Text.Length - 2, 2);
            }

            lblError.Text += " - " + ResHelper.GetString("taxclass_state.TaxValueError");
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }


    protected void gvStates_DataBound(object sender, EventArgs e)
    {
        for (int i = 0; i < gvStates.Rows.Count; i++)
        {
            //copy id from 5th column to invisible label in last column
            Label id = new Label();
            id.Visible = false;
            id.Text = gvStates.Rows[i].Cells[4].Text;
            gvStates.Rows[i].Cells[4].Controls.Add(id);

            // Set unique text box ID
            TextBox txtValue = (TextBox)gvStates.Rows[i].Cells[2].Controls[1];
            txtValue.ID = "txtTaxValue" + id.Text;
            // Set unique check box ID
            CheckBox chkIsFlat = (CheckBox)gvStates.Rows[i].Cells[3].Controls[1];
            chkIsFlat.ID = "chkIsFlatValue" + id.Text;
        }
    }


    protected void txtTaxValue_Changed(object sender, EventArgs e)
    {
        mChangedTextBoxes[((TextBox)sender).ID] = sender;
    }


    protected void chkIsFlatValue_Changed(object sender, EventArgs e)
    {
        mChangedCheckBoxes[((CheckBox)sender).ID] = sender;
    }


    protected bool ConvertToBoolean(object value)
    {
        return ValidationHelper.GetBoolean(value, false);
    }
}
