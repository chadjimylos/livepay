<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PortalTemplate.aspx.cs" Inherits="CMSPages_PortalTemplate"
    ValidateRequest="false" MaintainScrollPositionOnPostback="true" EnableEventValidation="false" %>

<%=DocType%>
<html xmlns="http://www.w3.org/1999/xhtml" <%=XmlNamespace%> >
<head id="Head1" runat="server" enableviewstate="false">
    <title id="Title1" runat="server">My site</title>
    <asp:Literal runat="server" ID="ltlTags" EnableViewState="false" />
</head>
<body class="<%=BodyClass%>" <%=BodyParameters%>>
    <form id="form1" runat="server">
    <asp:PlaceHolder runat="server" ID="plcManagers">
        <cms:CMSPortalManager ID="manPortal" runat="server" EnableViewState="false" />
        <asp:ScriptManager ID="manScript" runat="server" ScriptMode="Release" EnableViewState="false" />
    </asp:PlaceHolder>
    <cms:CMSPagePlaceholder ID="plc" runat="server" />
    <asp:PlaceHolder runat="server" ID="plcLogs" EnableViewState="false" />
    </form>
</body>
</html>
