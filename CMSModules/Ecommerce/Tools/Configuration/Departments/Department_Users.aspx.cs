using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Tools_Configuration_Departments_Department_Users : CMSEcommerceConfigurationPage
{
    protected int departmentId = 0;
    protected string currentValues = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblAvialable.Text = ResHelper.GetString("com.department.usersavailable");
        departmentId = QueryHelper.GetInteger("departmentid", 0);
        if (departmentId > 0)
        {
            // Get the active users
            string where = "UserID IN (SELECT UserID FROM COM_UserDepartment WHERE DepartmentID = " + departmentId + ")";
            DataSet ds = UserInfoProvider.GetUsers(where, null, 0, "UserID", null);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "UserID"));
            }

            if (!IsPostBack)
            {
                this.uniSelector.Value = currentValues;
            }
        }

        this.uniSelector.IconPath = GetObjectIconUrl("cms.user", "object.png");
        this.uniSelector.OnSelectionChanged += uniSelector_OnSelectionChanged;
        this.uniSelector.WhereCondition = "UserID IN (SELECT UserID FROM CMS_UserSite WHERE SiteID = " + CMSContext.CurrentSiteID + ")";
    }


    protected void uniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        SaveItems();
    }


    protected void SaveItems()
    {
        // Remove old items
        string newValues = ValidationHelper.GetString(uniSelector.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to user
                foreach (string item in newItems)
                {
                    int userId = ValidationHelper.GetInteger(item, 0);
                    UserDepartmentInfoProvider.RemoveUserFromDepartment(departmentId, userId);
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to user
                foreach (string item in newItems)
                {
                    int userId = ValidationHelper.GetInteger(item, 0);
                    UserDepartmentInfoProvider.AddUserToDepartment(departmentId, userId);
                }
            }
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}
