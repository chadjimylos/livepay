﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SystemTable_Edit_Search.aspx.cs"
    Inherits="CMSSiteManager_Development_SystemTables_SystemTable_Edit_Search" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="System table search fields" %>

<%@ Register Src="~/CMSModules/SmartSearch/Controls/Edit/ClassFields.ascx" TagName="ClassFields"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" EnableViewState="false" />
    <cms:LocalizedLabel runat="server" ID="lblInfo" ResourceString="systbl.search.warninfo"
        CssClass="InfoLabel" />
        <br />
    <cms:ClassFields ID="ClassFields" runat="server" />
</asp:Content>
