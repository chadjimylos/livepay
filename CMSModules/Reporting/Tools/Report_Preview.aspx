<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Report_Preview.aspx.cs" Inherits="CMSModules_Reporting_Tools_Report_Preview"
    Theme="Default" EnableEventValidation="false" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master"
    Title="ReportGraph Preview" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/CMSModules/Reporting/Controls/ReportGraph.ascx" TagName="ReportGraph"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Reporting/Controls/ReportTable.ascx" TagName="ReportTable"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Panel runat="server" ID="pnlContent" CssClass="PageContent">
        <div style="margin-bottom: 10px;">
            <cms:ReportGraph ID="reportGraph" runat="server" Visible="false" RenderCssClasses="true" />
            <cms:ReportTable ID="reportTable" runat="server" Visible="false" RenderCssClasses="true" />
        </div>
    </asp:Panel>
    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:CMSButton ID="btnClose" OnClientClick="window.close()" runat="server" CssClass="SubmitButton" />
    </div>
</asp:Content>
