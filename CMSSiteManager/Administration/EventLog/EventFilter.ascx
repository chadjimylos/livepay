<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EventFilter.ascx.cs" Inherits="CMSSiteManager_Administration_EventLog_EventFilter" %>
<asp:Panel ID="pnl" runat="server" DefaultButton="btnSearch">
<table cellpadding="0" cellspacing="2">
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblType" runat="server" ResourceString="general.type" DisplayColon="true" />
        </td>
        <td>
            <asp:DropDownList ID="drpType" runat="server" CssClass="DropDownFieldFilter" />
        </td>
    </tr>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblSource" runat="server" ResourceString="Unigrid.EventLog.Columns.Source"
                DisplayColon="true" />
        </td>
        <td>
            <asp:DropDownList ID="drpSource" runat="server" CssClass="ExtraSmallDropDown" />
            <asp:TextBox ID="txtSource" runat="server" CssClass="SelectorTextBox" />
        </td>
    </tr>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblEventCode" runat="server" ResourceString="Unigrid.EventLog.Columns.EventCode"
                DisplayColon="true" />
        </td>
        <td>
            <asp:DropDownList ID="drpEventCode" runat="server" CssClass="ExtraSmallDropDown" />
            <asp:TextBox ID="txtEventCode" runat="server" CssClass="SelectorTextBox" />
        </td>
    </tr>
    <asp:PlaceHolder ID="plcAdvancedSearch" runat="server" Visible="false">
        <tr runat="server">
            <td>
                <cms:LocalizedLabel ID="lblUserName" runat="server" ResourceString="general.username"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:DropDownList ID="drpUserName" runat="server" CssClass="ExtraSmallDropDown" />
                <asp:TextBox ID="txtUserName" runat="server" CssClass="SelectorTextBox" />
            </td>
        </tr>
        <tr runat="server">
            <td>
                <cms:LocalizedLabel ID="lblIPAddress" runat="server" ResourceString="Unigrid.EventLog.Columns.IPAdress"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:DropDownList ID="drpIPAddress" runat="server" CssClass="ExtraSmallDropDown" />
                <asp:TextBox ID="txtIPAddress" runat="server" CssClass="SelectorTextBox" />
            </td>
        </tr>
        <tr runat="server">
            <td>
                <cms:LocalizedLabel ID="lblDocumentName" runat="server" ResourceString="general.documentname"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:DropDownList ID="drpDocumentName" runat="server" CssClass="ExtraSmallDropDown" />
                <asp:TextBox ID="txtDocumentName" runat="server" CssClass="SelectorTextBox" />
            </td>
        </tr>
        <tr runat="server">
            <td>
                <cms:LocalizedLabel ID="lblMachineName" runat="server" ResourceString="Unigrid.EventLog.Columns.MachineName"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:DropDownList ID="drpMachineName" runat="server" CssClass="ExtraSmallDropDown" />
                <asp:TextBox ID="txtMachineName" runat="server" CssClass="SelectorTextBox" />
            </td>
        </tr>
        <tr runat="server">
            <td>
                <cms:LocalizedLabel ID="lblEventURL" runat="server" ResourceString="eventlogdetails.eventurl" />
            </td>
            <td>
                <asp:DropDownList ID="drpEventURL" runat="server" CssClass="ExtraSmallDropDown" />
                <asp:TextBox ID="txtEventURL" runat="server" CssClass="SelectorTextBox" />
            </td>
        </tr>
        <tr runat="server">
            <td>
                <cms:LocalizedLabel ID="lblURLReferrer" runat="server" ResourceString="EventLogDetails.UrlReferrer"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:DropDownList ID="drpURLReferrer" runat="server" CssClass="ExtraSmallDropDown" />
                <asp:TextBox ID="txtURLReferrer" runat="server" CssClass="SelectorTextBox" />
            </td>
        </tr>
        <tr runat="server">
            <td>
                <cms:LocalizedLabel ID="lblDescription" runat="server" ResourceString="general.description"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:DropDownList ID="drpDescription" runat="server" CssClass="ExtraSmallDropDown" />
                <asp:TextBox ID="txtDescription" runat="server" CssClass="SelectorTextBox" />
            </td>
        </tr>
        <tr runat="server">
            <td>
                <cms:LocalizedLabel ID="lblUserAgent" runat="server" ResourceString="EventLogDetails.UserAgent"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:DropDownList ID="drpUserAgent" runat="server" CssClass="ExtraSmallDropDown" />
                <asp:TextBox ID="txtUserAgent" runat="server" CssClass="SelectorTextBox" />
            </td>
        </tr>
        <tr runat="server">
            <td>
                <cms:LocalizedLabel ID="lblRoles" runat="server" ResourceString="EventLogDetails.Roles"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:DropDownList ID="drpRoles" runat="server" CssClass="DropDownFieldFilter" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblTimeBetween" runat="server" ResourceString="eventlog.timebetween"
                DisplayColon="true" />
        </td>
        <td>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <cms:DateTimePicker ID="dtmTimeFrom" runat="server" />
                    </td>
                    <td>
                        <cms:LocalizedLabel ID="lblTimeBetweenAnd" runat="server" ResourceString="eventlog.timebetweenand" style="padding: 0px 7px;" />
                    </td>
                    <td>
                        <cms:DateTimePicker ID="dtmTimeTo" runat="server" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <cms:LocalizedButton ID="btnSearch" runat="server" CssClass="ContentButton" ResourceString="general.search" />
        </td>
    </tr>
</table>
<br />
</asp:Panel>
<asp:Panel ID="pnlAdvanced" runat="server" Visible="true">
    <asp:Image runat="server" ID="imgShowSimpleFilter" CssClass="NewItemImage" />
    <asp:LinkButton ID="lnkShowSimpleFilter" runat="server" OnClick="lnkShowSimpleFilter_Click" />
</asp:Panel>
<asp:Panel ID="pnlSimple" runat="server" Visible="false">
    <asp:Image runat="server" ID="imgShowAdvancedFilter" CssClass="NewItemImage" />
    <asp:LinkButton ID="lnkShowAdvancedFilter" runat="server" OnClick="lnkShowAdvancedFilter_Click" />
</asp:Panel>
