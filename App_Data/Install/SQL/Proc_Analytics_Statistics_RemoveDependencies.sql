-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Analytics_Statistics_RemoveDependencies] 
	@StatisticsID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
	DELETE FROM Analytics_DayHits WHERE HitsStatisticsID=@StatisticsID
	DELETE FROM Analytics_HourHits WHERE HitsStatisticsID=@StatisticsID
	DELETE FROM Analytics_MonthHits WHERE HitsStatisticsID=@StatisticsID
	DELETE FROM Analytics_WeekHits WHERE HitsStatisticsID=@StatisticsID
	DELETE FROM Analytics_YearHits WHERE HitsStatisticsID=@StatisticsID
    COMMIT TRANSACTION;
END
