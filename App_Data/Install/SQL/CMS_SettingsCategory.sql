CREATE TABLE [CMS_SettingsCategory] (
		[CategoryID]              int NOT NULL IDENTITY(1, 1),
		[CategoryDisplayName]     nvarchar(200) NOT NULL,
		[CategoryOrder]           int NULL,
		[CategoryName]            nvarchar(100) NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_SettingsCategory]
	ADD
	CONSTRAINT [PK_CMS_SettingsCategory]
	PRIMARY KEY
	NONCLUSTERED
	([CategoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_SettingsCategory_CategoryOrder]
	ON [CMS_SettingsCategory] ([CategoryOrder])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
