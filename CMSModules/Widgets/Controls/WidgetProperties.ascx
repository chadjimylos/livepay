<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetProperties.ascx.cs"
    Inherits="CMSModules_Widgets_Controls_WidgetProperties" %>
<asp:PlaceHolder ID="plcToolbar" runat="server" Visible="false" EnableViewState="false">
    <div id="FCKToolbar" class="FCKToolbar">
    </div>

    <script type="text/javascript">
        //<![CDATA[
        var toolbarElem = null;
        var propElem = null;

        function SetToolbarHeight() {
            if (toolbarElem == null) {
                toolbarElem = document.getElementById('FCKToolbar');
            }

            var toolbarHeight = 0;
            if (toolbarElem.childNodes.length > 0) {
                var toolbarFrames = toolbarElem.getElementsByTagName('iframe');
                var toolbarFrame = null;
                if ((toolbarFrames != null) && (toolbarFrames.length > 0)) {

                    toolbarFrame = toolbarFrames[0];
                }
                if (toolbarFrame != null) {
                    toolbarHeight = toolbarFrame.height;
                }
                var h = toolbarHeight + 'px';
                if (toolbarElem.style.height != h) {
                    toolbarElem.style.height = h;
                }
            }

            return toolbarHeight;
        }

        function ResizeWorkingArea() {
            if (propElem == null) {
                propElem = document.getElementById('propArea');
            }
            if (propElem != null) {
                var toolbarHeight = SetToolbarHeight();
                var h = (document.body.offsetHeight - toolbarHeight - 1) + 'px';
                if (propElem.style.height != h) {
                    propElem.style.height = h;
                }
            }
        }

        setInterval("ResizeWorkingArea();", 500);
        //]]>
    </script>

</asp:PlaceHolder>
<div id="propArea" class="TabsPageScrollArea2">
    <asp:Panel runat="server" ID="pnlTab" CssClass="TabsPageContent">
        <asp:Panel runat="server" ID="pnlFormArea" CssClass="WebPartForm">
            <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" Visible="false" EnableViewState="false" />
            <cms:BasicForm runat="server" ID="formCustom" HtmlAreaToolbarLocation="Out:FCKToolbar"
                Enabled="true" DefaultFormLayout="Tables" DefaultCategoryName="Default" MarkRequiredFields="true" />
            <asp:Panel runat="server" ID="pnlExport" CssClass="InfoLabel">
                <asp:Literal runat="server" ID="ltlExport" EnableViewState="false" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:HiddenField runat="server" ID="hidRefresh" Value="0" />
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    <cms:CMSButton ID="btnOnApply" runat="server" Visible="false" EnableViewState="false" />
    <cms:CMSButton ID="btnOnOK" runat="server" Visible="false" EnableViewState="false" />
    <asp:HiddenField ID="hdnIsNewWebPart" runat="server" />
    <asp:HiddenField ID="hdnInstanceGUID" runat="server" />
    </asp:Panel>
</div>
