using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Controls_ShoppingCart_ShoppingCartSKUPriceDetail_Control : CMSUserControl
{
    /// <summary>
    /// Value not known
    /// </summary>
    public const double NOT_KNOWN = -1;

    // Input values
    private Guid mCartItemGuid = Guid.Empty;
    private ShoppingCartInfo mShoppingCart = null;

    // Others
    private DataTable mDiscountTable = null;
    private DataTable mTaxTable = null;
    private ShoppingCartItemInfo mCartItemObj = null;

    private double mUnitPriceWithoutTax = NOT_KNOWN;
    private double mUnitTotalDiscount = NOT_KNOWN;
    private double mUnitTotalTax = NOT_KNOWN;


    #region "Properties"

    /// <summary>
    /// Shopping cart info object.
    /// </summary>
    public ShoppingCartInfo ShoppingCartInfoObj
    {
        get
        {
            return mShoppingCart;
        }
        set
        {
            mShoppingCart = value;
        }
    }


    /// <summary>
    /// Shopping cart item GUID.
    /// </summary>
    public Guid CartItemGuid
    {
        get
        {
            return mCartItemGuid;
        }
        set
        {
            mCartItemGuid = value;
        }
    }


    /// <summary>
    /// Shopping cart item.
    /// </summary>
    public ShoppingCartItemInfo CartItemObj
    {
        get
        {
            if (mCartItemObj == null)
            {
                if ((this.CartItemGuid != Guid.Empty) && (this.ShoppingCartInfoObj != null))
                {
                    mCartItemObj = this.ShoppingCartInfoObj.GetShoppingCartItemInfo(this.CartItemGuid);
                }
            }
            return mCartItemObj;
        }
    }


    /// <summary>
    /// Discount table.
    /// </summary>
    private DataTable DiscountTable
    {
        get
        {
            if (mDiscountTable == null)
            {
                EvaluateSKUPriceDetail();
            }
            return mDiscountTable;
        }
    }


    /// <summary>
    /// Tax table.
    /// </summary>
    private DataTable TaxTable
    {
        get
        {
            if (mTaxTable == null)
            {
                EvaluateSKUPriceDetail();
            }
            return mTaxTable;
        }
    }


    /// <summary>
    /// Unit price after total discount in shopping cart currency
    /// </summary>
    public double UnitPriceAfterDiscount
    {
        get
        {
            double priceAfterDiscount = this.UnitPriceWithoutTax - this.UnitTotalDiscount;
            if ((priceAfterDiscount < 0) && (this.CartItemObj != null) && (this.CartItemObj.SKUObj != null) && (this.CartItemObj.SKUObj.SKUPrice > 0))
            {
                return 0;
            }
            return priceAfterDiscount;
        }
    }


    /// <summary>
    /// Unit price without tax in shopping cart currency
    /// </summary>
    public double UnitPriceWithoutTax
    {
        get
        {
            if (mUnitPriceWithoutTax == NOT_KNOWN)
            {
                if ((this.CartItemObj != null) && (this.CartItemObj.SKUObj != null) && (this.ShoppingCartInfoObj != null))
                {
                    mUnitPriceWithoutTax = ExchangeTableInfoProvider.ApplyExchangeRate(this.CartItemObj.SKUObj.SKUPrice, this.ShoppingCartInfoObj.ExchangeRate);
                }
            }
            return mUnitPriceWithoutTax;
        }
    }


    /// <summary>
    /// Unit total discount in shopping cart currency
    /// </summary>
    public double UnitTotalDiscount
    {
        get
        {
            if (mUnitTotalDiscount == NOT_KNOWN)
            {
                EvaluateSKUPriceDetail();
            }
            return mUnitTotalDiscount;
        }
    }


    /// <summary>
    /// Unit total tax in shoppinc cart currency
    /// </summary>
    public double UnitTotalTax
    {
        get
        {
            if (mUnitTotalTax == NOT_KNOWN)
            {
                EvaluateSKUPriceDetail();
            }
            return mUnitTotalTax;
        }
    }


    /// <summary>
    /// Unit price with total tax in shopping cart currency
    /// </summary>
    public double UnitPriceWithTax
    {
        get
        {
            return this.UnitPriceAfterDiscount + this.UnitTotalTax;
        }
    }


    /// <summary>
    /// Total price
    /// </summary>
    public double TotalPrice
    {
        get
        {
            if (this.CartItemObj != null)
            {
                return this.UnitPriceWithTax * this.CartItemObj.CartItemUnits;
            }
            return 0.0;
        }
    }


    /// <summary>
    /// Rounded total price
    /// </summary>
    public double RoundedTotalPrice
    {
        get
        {
            if ((this.ShoppingCartInfoObj != null) && (this.ShoppingCartInfoObj.CurrencyInfoObj != null))
            {
                return CurrencyInfoProvider.RoundTo(this.TotalPrice, this.ShoppingCartInfoObj.CurrencyInfoObj);
            }
            else
            {
                return this.TotalPrice;
            }
        }
    }

    #endregion    

    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize controls        
        lblPriceAfterDiscount.Text = ResHelper.GetString("ProductPriceDetail.PriceAfterDiscount");
        lblPriceWithoutTax.Text = ResHelper.GetString("ProductPriceDetail.PriceWithoutTax");
        lblPriceWithTax.Text = ResHelper.GetString("ProductPriceDetail.PriceWithTax");        
        lblProductUnits.Text = ResHelper.GetString("ProductPriceDetail.ProductUnits");
        lblTotalDiscount.Text = ResHelper.GetString("ProductPriceDetail.TotalDiscount");
        lblTotalTax.Text = ResHelper.GetString("ProductPriceDetail.TotalTax");        
        lblTotalPrice.Text = ResHelper.GetString("ProductPriceDetail.TotalPrice");

        // Set alternate header text when grid is empty and grid header is hidden
        lblTaxes.Text = ResHelper.GetString("ProductPriceDetail.Taxes");
        lblDiscounts.Text = ResHelper.GetString("ProductPriceDetail.Discounts");

        // Set header text
        gridDiscounts.Columns[0].HeaderText = lblDiscounts.Text;
        gridDiscounts.Columns[1].HeaderText = ResHelper.GetString("ProductPriceDetail.PerUnit");
        gridTaxes.Columns[0].HeaderText = lblTaxes.Text;
        gridTaxes.Columns[1].HeaderText = ResHelper.GetString("ProductPriceDetail.PerUnit");


        // Discounts
        gridDiscounts.DataSource = this.DiscountTable;
        gridDiscounts.DataBind();
        // Taxes
        gridTaxes.DataSource = this.TaxTable;
        gridTaxes.DataBind();

        // Initialize subtotals and totals
        lblPriceWithoutTaxValue.Text = GetFormattedSubtotalValue(this.UnitPriceWithoutTax, "");
        lblTotalDiscountValue.Text = GetFormattedSubtotalValue(this.UnitTotalDiscount, "-");
        lblPriceAfterDiscountValue.Text = GetFormattedSubtotalValue(this.UnitPriceAfterDiscount, "");
        lblTotalTaxValue.Text = GetFormattedSubtotalValue(this.UnitTotalTax, "+");
        lblPriceWithTaxValue.Text = GetFormattedSubtotalValue(this.UnitPriceWithTax, "");                
        lblTotalPriceValue.Text = GetFormattedSubtotalValue(this.RoundedTotalPrice, "");

        if (((this.CartItemObj != null)) && (this.CartItemObj.SKUObj != null))
        {
            lblProductName.Text = HTMLHelper.HTMLEncode(ResHelper.LocalizeString(this.CartItemObj.SKUObj.SKUName));
            lblProductUnitsValue.Text = this.CartItemObj.CartItemUnits.ToString();
        }

        // Show subtotals
        plcTotalDiscount.Visible = (this.DiscountTable.Rows.Count != 1);
        plcTotalTax.Visible = (this.TaxTable.Rows.Count != 1);

        // Show alternate header
        plcTaxes.Visible = (gridTaxes.Rows.Count == 0);
        plcDiscounts.Visible = (gridDiscounts.Rows.Count == 0);


        if (gridDiscounts.HeaderRow != null)
        {
            gridDiscounts.HeaderRow.Cells[1].ColumnSpan = 2;
            gridDiscounts.HeaderRow.Cells[1].Width = 160;
            gridDiscounts.HeaderRow.Cells[2].Visible = false;
        }
        if (gridTaxes.HeaderRow != null)
        {
            gridTaxes.HeaderRow.Cells[1].ColumnSpan = 2;
            gridTaxes.HeaderRow.Cells[1].Width = 160;
            gridTaxes.HeaderRow.Cells[2].Visible = false;
        }
    }


    #region "Tables"

    /// <summary>
    /// Creates discount table
    /// </summary>
    private DataTable CreateDiscountTable()
    {
        DataTable table = new DataTable();
        table.TableName = "DiscountTable";
        table.Columns.Add(new DataColumn("DiscountName", typeof(string)));
        table.Columns.Add(new DataColumn("DiscountIsFlat", typeof(bool)));
        table.Columns.Add(new DataColumn("DiscountValue", typeof(double)));
        table.Columns.Add(new DataColumn("DiscountRealValue", typeof(double)));
        return table;
    }


    /// <summary>
    /// Creates discount table row.
    /// </summary>
    /// <param name="taxTable">Tax table.</param>
    /// <param name="discountName">Discount display name.</param>
    /// <param name="discountIsFlat">Indicates whether discount value is flat value or procentage.</param>
    /// <param name="discountValue">Discount value.</param>
    /// <param name="discountRealValue">Discount real value.</param>
    private DataRow CreateDiscountTableRow(DataTable taxTable, string discountName, bool discountIsFlat, double discountValue, double discountRealValue)
    {
        DataRow row = taxTable.NewRow();
        DataHelper.SetDataRowValue(row, "DiscountName", discountName);
        DataHelper.SetDataRowValue(row, "DiscountIsFlat", discountIsFlat);
        DataHelper.SetDataRowValue(row, "DiscountValue", discountValue);
        DataHelper.SetDataRowValue(row, "DiscountRealValue", discountRealValue);
        return row;
    }


    /// <summary>
    /// Creates tax table.
    /// </summary>
    private DataTable CreateTaxTable()
    {
        DataTable table = new DataTable();
        table.TableName = "TaxTable";
        table.Columns.Add(new DataColumn("TaxClassID", typeof(int)));
        table.Columns.Add(new DataColumn("TaxClassDisplayName", typeof(string)));
        table.Columns.Add(new DataColumn("TaxIsFlat", typeof(bool)));
        table.Columns.Add(new DataColumn("TaxValue", typeof(double)));
        table.Columns.Add(new DataColumn("TaxRealValue", typeof(double)));
        return table;
    }


    /// <summary>
    /// Creates tax table row with calculated tax real value and calucalted tax value per given product unit.
    /// </summary>
    /// <param name="taxTable">Tax table.</param>
    /// <param name="taxRow">Tax row with input data.</param>
    /// <param name="taxRealValue">Tax real value.</param>    
    private DataRow CreateTaxTableRow(DataTable taxTable, DataRow taxRow, double taxRealValue)
    {
        DataRow row = taxTable.NewRow();
        DataHelper.SetDataRowValue(row, "TaxClassID", taxRow["TaxClassID"]);
        DataHelper.SetDataRowValue(row, "TaxClassDisplayName", taxRow["TaxClassDisplayName"]);
        DataHelper.SetDataRowValue(row, "TaxIsFlat", taxRow["IsFlatValue"]);
        DataHelper.SetDataRowValue(row, "TaxValue", taxRow["TaxValue"]);
        DataHelper.SetDataRowValue(row, "TaxRealValue", taxRealValue);
        return row;
    }

    #endregion


    #region "Price deatil evaluation"

    /// <summary>
    /// Evaluates SKU price detail.
    /// </summary>
    public void EvaluateSKUPriceDetail()
    {
        CalculateDiscounts();
        CalculateTaxes();
    }


    /// <summary>
    /// Calculate product discounts.
    /// </summary>
    private void CalculateDiscounts()
    {
        DataTable discountTable = CreateDiscountTable();
        double totalDiscount = 0.0;

        if ((this.CartItemObj != null) && (this.CartItemObj.SKUObj != null) && (this.ShoppingCartInfoObj != null))
        {
            double discountLevel = 0;
            double discountCoupon = 0;

            // Calculate discount for customer discount level
            DiscountLevelInfo dli = GetCustomerDiscountLevelInfo();
            if ((dli != null) && dli.IsInDepartment(this.CartItemObj.SKUObj.SKUDepartmentID) &&
                (this.ShoppingCartInfoObj.IsCreatedFromOrder || dli.IsValid) && (dli.DiscountLevelValue > 0))
            {
                discountLevel = GetDiscountValue(this.CartItemObj.SKUObj.SKUPrice, dli.DiscountLevelValue, false);
            }

            // Calculate discount for discount coupon
            DiscountCouponInfo dci = null;

            // Apply discount only to parent products
            if (!this.CartItemObj.IsProductOption)
            {
                if (this.ShoppingCartInfoObj.ShoppingCartDiscountCouponID > 0)
                {
                    dci = DiscountCouponInfoProvider.GetDiscountCouponInfo(this.ShoppingCartInfoObj.ShoppingCartDiscountCouponID);
                    if ((dci != null) && (dci.DiscountCouponValue > 0) &&
                        DiscountCouponInfoProvider.IsSKUAssignedToDiscountCoupon(this.CartItemObj.SKUID, dci.DiscountCouponID))
                    {
                        discountCoupon = GetDiscountValue(this.CartItemObj.SKUObj.SKUPrice, dci.DiscountCouponValue, dci.DiscountCouponIsFlatValue);
                    }
                }
            }

            double skuPriceAfterDiscount = this.CartItemObj.SKUObj.SKUPrice;

            // Compare discounts 
            if ((discountCoupon > discountLevel) && (dci != null))
            {                

                // Discount coupon is applied                
                skuPriceAfterDiscount = this.CartItemObj.SKUObj.SKUPrice - discountCoupon;
                discountCoupon = ExchangeTableInfoProvider.ApplyExchangeRate(discountCoupon, this.ShoppingCartInfoObj.ExchangeRate);
                discountTable.Rows.Add(CreateDiscountTableRow(discountTable, ResHelper.LocalizeString(dci.DiscountCouponDisplayName), dci.DiscountCouponIsFlatValue, dci.DiscountCouponValue, discountCoupon));

                // Increase total discount
                totalDiscount += discountCoupon;
            }
            else if (discountLevel > 0)
            {
                // Discount level is applied    
                skuPriceAfterDiscount = this.CartItemObj.SKUObj.SKUPrice - discountLevel;
                discountLevel = ExchangeTableInfoProvider.ApplyExchangeRate(discountLevel, this.ShoppingCartInfoObj.ExchangeRate);
                discountTable.Rows.Add(CreateDiscountTableRow(discountTable, ResHelper.LocalizeString(dli.DiscountLevelDisplayName), false, dli.DiscountLevelValue, discountLevel));

                // Increase total discount
                totalDiscount += discountLevel;
            }

            // Get product price after first discount            
            if ((this.CartItemObj.SKUObj.SKUPrice > 0) && (skuPriceAfterDiscount < 0))
            {
                skuPriceAfterDiscount = 0;
            }


            // Apply discount only to parent products
            if (!this.CartItemObj.IsProductOption)
            {
                int totalUnits = ValidationHelper.GetInteger(this.ShoppingCartInfoObj.CartSKUItems[this.CartItemObj.SKUID], 0);

                // Calculate volume discount
                VolumeDiscountInfo vdi = CMS.CMSEcommerce.VolumeDiscountInfoProvider.GetVolumeDiscountInfo(this.CartItemObj.SKUObj.SKUID, totalUnits);
                if (vdi != null)
                {
                    // Get volume discount value
                    double volumeDiscount = GetDiscountValue(skuPriceAfterDiscount, vdi.VolumeDiscountValue, vdi.VolumeDiscountIsFlatValue);
                    volumeDiscount = ExchangeTableInfoProvider.ApplyExchangeRate(volumeDiscount, this.ShoppingCartInfoObj.ExchangeRate);

                    // Apply volume discount
                    string discountName = string.Format(ResHelper.GetString("ProductPriceDetail.VolumeDiscount"), vdi.VolumeDiscountMinCount);
                    discountTable.Rows.Add(CreateDiscountTableRow(discountTable, discountName, vdi.VolumeDiscountIsFlatValue, vdi.VolumeDiscountValue, volumeDiscount));

                    // Increase total discount
                    totalDiscount += volumeDiscount;
                }
            }
        }

        // Initialize discount table
        mDiscountTable = discountTable;

        // Initialize unit total discount
        mUnitTotalDiscount = totalDiscount;
    }


    /// <summary>
    /// Calculate product taxes.
    /// </summary>
    private void CalculateTaxes()
    {
        DataTable taxTable = CreateTaxTable();
        double totalTax = 0.0;

        if ((this.ShoppingCartInfoObj != null) && (this.CartItemObj != null))
        {
            // Indicates wheter tax registration ID is supplied
            bool taxIDSupplied = ((this.ShoppingCartInfoObj.CustomerInfoObj != null) && (this.ShoppingCartInfoObj.CustomerInfoObj.CustomerTaxRegistrationID != ""));

            // State tax classes which were already applied to the product price
            ArrayList appliedStateTaxClasses = new ArrayList();

            // Get state tax classes for specified product
            string where = "COM_SKUTaxClasses.SKUID = " + this.CartItemObj.SKUID + " AND COM_TaxClassState.StateID = " + this.ShoppingCartInfoObj.StateID;
            DataSet dsStateTaxes = TaxClassInfoProvider.GetStateTaxClassesForCart(this.ShoppingCartInfoObj, where);

            if (!DataHelper.DataSourceIsEmpty(dsStateTaxes))
            {
                foreach (DataRow dr in dsStateTaxes.Tables[0].Rows)
                {
                    double unitTax = 0.0;

                    if (taxIDSupplied && ValidationHelper.GetBoolean(dr["TaxClassZeroIfIDSupplied"], false))
                    {
                        // Zero tax if Tax ID is supplied         
                        // unitTax = 0.0;
                    }
                    else
                    {
                        // Use state tax value                            
                        double taxValue = ValidationHelper.GetDouble(dr["TaxValue"], 0);
                        bool isFlat = ValidationHelper.GetBoolean(dr["IsFlatValue"], false);
                        unitTax = CMS.CMSEcommerce.TaxClassInfoProvider.GetTaxValue(this.UnitPriceAfterDiscount, taxValue, isFlat);
                        if (isFlat)
                        {
                            unitTax = ExchangeTableInfoProvider.ApplyExchangeRate(unitTax, this.ShoppingCartInfoObj.ExchangeRate);
                        }

                        // Increase unit total tax
                        totalTax += unitTax;
                    }

                    // Actualize temporary tax table                
                    taxTable.Rows.Add(CreateTaxTableRow(taxTable, dr, unitTax));

                    // Mark tax class as state tax class
                    appliedStateTaxClasses.Add(ValidationHelper.GetDouble(dr["TaxClassID"], 0));
                }
            }


            // Get country tax classes for specified product
            where = "COM_SKUTaxClasses.SKUID = " + this.CartItemObj.SKUID;
            DataSet dsCounryTaxes = TaxClassInfoProvider.GetTaxClassesForCart(this.ShoppingCartInfoObj, where);
            if (!DataHelper.DataSourceIsEmpty(dsCounryTaxes))
            {
                foreach (DataRow dr in dsCounryTaxes.Tables[0].Rows)
                {
                    // Apply country tax value only when state tax value is not already applied
                    if (!appliedStateTaxClasses.Contains(ValidationHelper.GetDouble(dr["TaxClassID"], 0)))
                    {
                        double unitTax = 0.0;

                        if (taxIDSupplied && ValidationHelper.GetBoolean(dr["TaxClassZeroIfIDSupplied"], false))
                        {
                            // Zero tax if Tax ID is supplied   
                            // unitTax = 0.0;
                        }
                        else
                        {
                            // Use country tax value
                            double taxValue = ValidationHelper.GetDouble(dr["TaxValue"], 0);
                            bool isFlat = ValidationHelper.GetBoolean(dr["IsFlatValue"], false);
                            unitTax = CMS.CMSEcommerce.TaxClassInfoProvider.GetTaxValue(this.UnitPriceAfterDiscount, taxValue, isFlat);
                            if (isFlat)
                            {
                                unitTax = ExchangeTableInfoProvider.ApplyExchangeRate(unitTax, this.ShoppingCartInfoObj.ExchangeRate);
                            }

                            // Increase unit total tax
                            totalTax += unitTax;
                        }

                        // Actualize temporary tax table                
                        taxTable.Rows.Add(CreateTaxTableRow(taxTable, dr, unitTax));
                    }
                }
            }
        }

        // Initialize tax table
        mTaxTable = taxTable;

        // Initialize unit total tax
        mUnitTotalTax = totalTax;
    }
  

    /// <summary>
    /// Applies the discount to the given value.
    /// </summary>
    /// <param name="value">Value the discount should be applied to.</param>
    /// <param name="discountValue">Discount value (procentage or flat)</param>
    /// <param name="isFlat">Indicates whether it is a flat discount value.</param>
    private double GetDiscountValue(double value, double discountValue, bool isFlat)
    {
        if (discountValue > 0)
        {
            double result = 0;
            if (isFlat)
            {
                result = discountValue;
            }
            else
            {
                result = value * discountValue / 100;
            }

            if ((value > 0) && (result < 0))
            {
                result = 0;
            }

            return result;
        }
        else
        {
            return 0;
        }
    }


    /// <summary>
    /// Returns current customer discount level information.
    /// </summary>
    private DiscountLevelInfo GetCustomerDiscountLevelInfo()
    {
        if ((this.ShoppingCartInfoObj != null) &&
            (this.ShoppingCartInfoObj.CustomerInfoObj != null) &&
            (this.ShoppingCartInfoObj.CustomerInfoObj.CustomerDiscountLevelID > 0))
        {
            return DiscountLevelInfoProvider.GetDiscountLevelInfo(this.ShoppingCartInfoObj.CustomerInfoObj.CustomerDiscountLevelID);
        }
        return null;
    }


    /// <summary>
    /// Returns formatted tax/discount name.
    /// </summary>
    /// <param name="name">Tax/discount name.</param>
    protected string GetFormattedName(object name)
    {
        return " - " + ResHelper.LocalizeString(Convert.ToString(name));
    }


    /// <summary>
    /// Returns formatted value string.
    /// </summary>
    /// <param name="value">Value.</param>
    /// <param name="isFlat">True - it is a flat value, False - it is a relative value.</param>
    /// <param name="negative">Indicates whether value should be formatted as negative value.</param>
    /// <param name="applyExchangeRate">Indicates whether exchange rate should be applied.</param>
    protected string GetFormattedValue(object value, object isFlat, bool negative, bool applyExchangeRate)
    {
        bool mIsFlat = ValidationHelper.GetBoolean(isFlat, false);
        double mValue = ValidationHelper.GetDouble(value, 0);
        string result = "";
        
        if (mIsFlat)
        {            
            if (applyExchangeRate)
            {
                // Apply echange rate
                mValue = ExchangeTableInfoProvider.ApplyExchangeRate(mValue, this.ShoppingCartInfoObj.ExchangeRate);
            }

            // Round value
            //double roundedValue = CurrencyInfoProvider.RoundTo(mValue, this.ShoppingCartInfoObj.CurrencyInfoObj);

            
            // Get positive value, '-' will be added later
            if (negative && (mValue < 0))
            {
                mValue = -mValue;
            }

            // Return formatted price
            result = CurrencyInfoProvider.GetFormatedPrice(mValue, this.ShoppingCartInfoObj.CurrencyInfoObj);
        }
        else
        {
            // Return formatted string for relative value
            result = mValue.ToString() + "%";
        }

        
        if (negative)
        {
            // Add char of '-' to negative value
            result = "-" + result;
        }
        else
        {
            // Add char of '+' to positive value
            result = "+" + result;
        }

        return result;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="value">Value.</param>
    /// <param name="preffix">Additional string which is atteched as preffix to the formatted value.</param>     
    private string GetFormattedSubtotalValue(double value, string preffix)
    {        
        if (value < 0)
        {
            preffix = "-";
            value = Math.Abs(value);
        }

        if ((this.ShoppingCartInfoObj != null) && (this.ShoppingCartInfoObj.CurrencyInfoObj != null))
        {
            return preffix + CurrencyInfoProvider.GetFormatedPrice(value, this.ShoppingCartInfoObj.CurrencyInfoObj);
        }
        else
        {
            return preffix + value;
        }        
    }

    #endregion
}
