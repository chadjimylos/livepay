﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="EuronetActivation.aspx.vb" Inherits="Kentico_MerchantActivationWorkflow.EuronetActivation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="width: 100%; height: 100%; font-family: Arial; background: #E0F0ff">
    <form id="MerchantMID" runat="server">
    <div>
        <asp:Panel runat="server" ID="Main" Style="margin: auto; width: 515px; background: white;
            border: 1px solid #000000; height: 250px;">
            <div style="height: 85px">
                <img src="https://www.hellaspay.gr/Images/logoHellasPay/" alt="" style="height: 48px;
                    width: 90px; margin: 10px" />
            </div>
            <div style="height: 35px">
                <div style="margin: auto">
                    <center>
                        <asp:Label ID="Title" Text="ΑΝΟΙΓΜΑ ΝΕΟΥ VTID" runat="server" Font-Bold="true"
                            Font-Size="Large" />
                    </center>
                </div>
            </div>
            <div style="height: 35px">
                <div style="margin-left: 12px; margin-top: 10px">
                    <asp:Label Text="ΕΜΠΟΡΟΣ:" runat="server" />&nbsp&nbsp
                    <asp:Label ID="MerchantStatement" Text="'MERCHANT HELLASPAY'" runat="server" Font-Bold="true" />
                </div>
            </div>
            <div style="height: 33px">
                <div style="width: 360px; margin-left: 12px; margin-top: 6px">
                    <asp:Label ID="VTIDLabel" Text="VTID:" runat="server" Font-Bold="true" Width="60px" />
                    <asp:TextBox ID="VTID" Text="VTID:" runat="server" Width="250px" />
                </div>
            </div>
            <div style="height: 80px">
                <div style="width: 90px; height: 30px; float: right; background: #114c94; margin-right: 10px;
                    margin-top: 5px; border: 1px solid black">
                    <div style="margin: 8px">
                        <asp:LinkButton Text="ΑΠΟΘΗΚΕΥΣΗ" runat="server" Font-Bold="True" Font-Size="X-Small"
                            Font-Underline="True" ForeColor="White" OnClick="Save_Click" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" Visible="false" ID="SuccessPanel" Style="margin: auto;
            width: 515px; background: white; border: 1px solid #000000; height: 150px;">
            <div style="height: 85px">
                <img src="https://www.hellaspay.gr/Images/logoHellasPay/" alt="" style="height: 48px;
                    width: 90px; margin: 10px" />
            </div>
            <center style="color: green">
                Η καταχώρηση ηταν επιτυχής<br />
                Ευχαριστούμε πολύ.
            </center>
        </asp:Panel>
        <asp:Panel runat="server" Visible="false" ID="FailedPanel" Style="margin: auto; width: 515px;
            background: white; border: 1px solid #000000; height: 150px;">
            <div style="height: 85px">
                <img src="https://www.hellaspay.gr/Images/logoHellasPay/" alt="" style="height: 48px;
                    width: 90px; margin: 10px" />
            </div>
            <center style="color: #f00">
                Κάποιο πρόβλημα παρουσιάστηκε κατα την καταχώρηση.
                <br />
                Παρακαλώ επικοινωνήστε μαζί μας στο
                <br />
                <a href="support@hellaspay.gr">support@hellaspay.gr</a> ή στο 2117604000
            </center>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
