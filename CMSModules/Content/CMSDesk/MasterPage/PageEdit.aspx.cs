using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.IO;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.SettingsProvider;
using CMS.WorkflowEngine;
using CMS.UIControls;
using CMS.Staging;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_MasterPage_PageEdit : CMSContentPage
{
    protected string mSave = null;
    protected int nodeId = 0;
    protected TreeNode node = null;
    protected TreeProvider tree = null;
    protected CurrentUserInfo currentUser = null;

    protected string mHead = null;
    protected string mBeforeLayout = null;
    protected string mAfterLayout = null;
    protected string mBody = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentUserInfo user = CMSContext.CurrentUser;

        // Check UIProfile
        if (!user.IsAuthorizedPerUIElement("CMS.Content", "MasterPage"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "MasterPage");
        }

        // Check "Design" permission
        if (!user.IsAuthorizedPerResource("CMS.Content", "Design"))
        {
            RedirectToAccessDenied("CMS.Content", "Design");
        }

        // Register the scripts
        ScriptHelper.RegisterProgress(Page);

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
            "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>"
        );

        nodeId = ValidationHelper.GetInteger(Request.QueryString["NodeId"], 0);
        currentUser = CMSContext.CurrentUser;

        try
        {
            CMSContext.CurrentPageInfo = PageInfoProvider.GetPageInfo(CMSContext.CurrentSiteName, "/", CMSContext.PreferredCultureCode, null, false, null);

            // Title
            string title = CMSContext.CurrentTitle;
            if (!string.IsNullOrEmpty(title))
            {
                title = "<title>" + title + "</title>";
            }

            // Body class
            string bodyCss = CMSContext.CurrentBodyClass;
            if (bodyCss != null && bodyCss.Trim() != "")
            {
                bodyCss = "class=\"" + bodyCss + "\"";
            }
            else
            {
                bodyCss = "";
            }

            // metadata
            string meta = "<meta http-equiv=\"pragma\" content=\"no-cache\" />";

            string description = CMSContext.CurrentDescription;
            if (description != "")
            {
                meta += "<meta name=\"description\" content=\"" + description + "\" />";
            }

            string keywords = CMSContext.CurrentKeyWords;
            if (keywords != "")
            {
                meta += "<meta name=\"keywords\"  content=\"" + keywords + "\" />";
            }

            // Site style sheet
            string cssSiteSheet = "";

            CssStylesheetInfo cssInfo = null;
            int stylesheetId = CMSContext.CurrentPageInfo.DocumentStylesheetID;

            if (stylesheetId > 0)
            {
                cssInfo = CssStylesheetInfoProvider.GetCssStylesheetInfo(stylesheetId);
            }
            else
            {
                cssInfo = CssStylesheetInfoProvider.GetCssStylesheetInfo(CMSContext.CurrentSite.SiteDefaultStylesheetID);
            }

            if (cssInfo != null)
            {
                string path = HttpContext.Current.Request.ApplicationPath;
                if (path.EndsWith("/"))
                {
                    path = path.TrimEnd('/');
                }
                cssSiteSheet = "<link type=\"text/css\" rel=\"stylesheet\" href=\"" + path + "/CMSPages/GetCSS.aspx?stylesheetname=" + cssInfo.StylesheetName + "\" />";
            }

            // Theme css files
            string themeCssFiles = "";
            if (cssInfo != null)
            {
                try
                {
                    string directory = HttpContext.Current.Server.MapPath("~/") + "\\App_Themes\\" + cssInfo.StylesheetName + "\\";
                    if (Directory.Exists(directory))
                    {
                        foreach (string file in Directory.GetFiles(directory, "*.css"))
                        {
                            themeCssFiles += "<link href=\"App_Themes/" + cssInfo.StylesheetName + "/" + file.Remove(0, file.LastIndexOf('\\') + 1) + "\" type=\"text/css\" rel=\"stylesheet\" />";
                        }
                    }
                }
                catch { }
            }

            // Add values to page
            mHead = FormatHTML(HighlightHTML(title + meta + cssSiteSheet + themeCssFiles), 2);
            mBody = bodyCss;
        }
        catch
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("MasterPage.PageEditErr");
        }

        ltlScript.Text = ScriptHelper.GetScript("function SaveDocument(){" + Page.ClientScript.GetPostBackEventReference(btnSave, "") + "}");

        if (!SettingsKeyProvider.UsingVirtualPathProvider)
        {
            lblChecked.Visible = true;
            lblChecked.Text = "<br />" + AddSpaces(2) + ResHelper.GetString("MasterPage.VirtualPathProviderNotRunning");
            txtLayout.ReadOnly = true;
        }

        LoadData();
    }


    protected override void OnPreInit(EventArgs e)
    {
        //((Panel)CurrentMaster.PanelBody.FindControl("pnlContent")).CssClass = "";
        base.OnPreInit(e);
    }


    public void LoadData()
    {
        if (nodeId > 0)
        {
            // Get the document
            tree = new TreeProvider(currentUser);
            node = tree.SelectSingleNode(nodeId, currentUser.PreferredCultureCode);
            if (node != null)
            {
                PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(node.DocumentPageTemplateID);
                if (pti != null)
                {
                    // Get shared layout
                    LayoutInfo li = LayoutInfoProvider.GetLayoutInfo(pti.LayoutID);
                    if (li != null)
                    {
                        // Load shared layout
                        if (!RequestHelper.IsPostBack())
                        {
                            txtLayout.Text = li.LayoutCode;
                        }

                        if (li.LayoutCheckedOutByUserID > 0)
                        {
                            lblChecked.Visible = true;
                            lblChecked.Text = "<br />" + AddSpaces(2) + ResHelper.GetString("MasterPage.LayoutCheckedOut");
                            if (!RequestHelper.IsPostBack())
                            {
                                txtLayout.Text = li.LayoutCode;
                                txtLayout.ReadOnly = true;
                            }
                        }
                    }
                    else
                    {
                        // Load custom layout
                        if (!RequestHelper.IsPostBack())
                        {
                            txtLayout.Text = ValidationHelper.GetString(pti.PageTemplateLayout, "");
                        }

                        if (pti.PageTemplateLayoutCheckedOutByUserID > 0)
                        {
                            lblChecked.Visible = true;
                            lblChecked.Text = "<br />" + AddSpaces(2) + ResHelper.GetString("MasterPage.LayoutCheckedOut");
                            if (!RequestHelper.IsPostBack())
                            {
                                txtLayout.ReadOnly = true;
                            }
                        }
                    }
                }
                else
                {
                    txtLayout.ReadOnly = true;
                }

                // Load node data
                if (!RequestHelper.IsPostBack())
                {
                    txtBodyCss.Text = node.NodeBodyElementAttributes;
                    txtDocType.Text = node.NodeDocType;
                    txtHeadTags.Value = node.NodeHeadTags;
                }
            }
        }

        lblAfterDocType.Text = HighlightHTML("<html>") + "<br />" + AddSpaces(1) + HighlightHTML("<head>");
        lblAfterHeadTags.Text = AddSpaces(1) + HighlightHTML("</head>");
        lblAfterLayout.Text = AddSpaces(1) + HighlightHTML("</body>") + "<br />" + HighlightHTML("</html>");
        lblBodyEnd.Text = HighlightHTML(">");
        lblBodyStart.Text = AddSpaces(1) + HighlightHTML("<body " + HttpUtility.HtmlDecode(mBody));
    }


    /// <summary>
    /// Format HTML text
    /// </summary>
    /// <param name="inputHTML">Input HTML</param>
    /// <param name="level">Indentation level</param>
    public string FormatHTML(string inputHTML, int level)
    {
        return AddSpaces(level) + inputHTML.Replace(HttpUtility.HtmlEncode(">"), HttpUtility.HtmlEncode(">") + "<br />" + AddSpaces(level));
    }


    /// <summary>
    /// Add spaces
    /// </summary>
    /// <param name="level">Indentation level</param>
    public string AddSpaces(int level)
    {
        string toReturn = "";
        for (int i = 0; i < level * 5; i++)
        {
            toReturn += "&nbsp;";
        }

        return toReturn;
    }


    /// <summary>
    /// Highlight HTML
    /// </summary>
    /// <param name="inputHtml">Input HTML</param>
    public string HighlightHTML(string inputHtml)
    {
        const string tagspan = "<span style=\"color: #0000ff;\">";
        const string tagcontentspan = "<span style=\"color: #a31515;\">";
        const string propertyspan = "<span style=\"color: #0000ff;\">";
        const string propertynamespan = "<span style=\"color: #ff0000;\">";
        const string endspan = "</span>";

        // highlight for open and closed tags
        inputHtml = inputHtml.Replace("<", "%%tagsspan%%" + HTMLHelper.HTMLEncode("<") + "%%endspan%%%%tagcontentspan%%");
        inputHtml = inputHtml.Replace("/>", "%%endspan%%%%tagsspan%%/" + HTMLHelper.HTMLEncode(">") + "%%endspan%%");
        inputHtml = inputHtml.Replace(">", "%%endspan%%%%tagsspan%%" + HTMLHelper.HTMLEncode(">") + "%%endspan%%");

        // tag properties
        inputHtml = Regex.Replace(inputHtml, "(\\s(\\w*|\\w*[\\S]*\\w*)\\s*(=))", " %%propertynamespan%%$2%%endspan%%$3", RegexOptions.IgnoreCase);
        inputHtml = Regex.Replace(inputHtml, "(=\"(\\w*|[^\"]*|\\s*)\")", "%%propertyspan%%$1%%endspan%%", RegexOptions.IgnoreCase);

        // Replace macros for tags
        inputHtml = inputHtml.Replace("%%tagcontentspan%%", tagcontentspan);
        inputHtml = inputHtml.Replace("%%propertynamespan%%", propertynamespan);
        inputHtml = inputHtml.Replace("%%propertyspan%%", propertyspan);
        inputHtml = inputHtml.Replace("%%tagsspan%%", tagspan);
        inputHtml = inputHtml.Replace("%%endspan%%", endspan);

        return inputHtml;
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (nodeId > 0)
        {
            if (node != null)
            {
                // Update the layout
                if (node.DocumentPageTemplateID > 0)
                {
                    PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(node.DocumentPageTemplateID);
                    if (pti != null)
                    {
                        // Get shared layout
                        LayoutInfo li = LayoutInfoProvider.GetLayoutInfo(pti.LayoutID);
                        if (li != null)
                        {
                            // Update shared layout
                            li.LayoutCode = txtLayout.Text;
                            LayoutInfoProvider.SetLayoutInfo(li);
                        }
                        else if (pti.PageTemplateLayoutCheckedOutByUserID <= 0)
                        {
                            // Update custom layout
                            pti.PageTemplateLayout = txtLayout.Text;
                            pti.PageTemplateVersionGUID = Guid.NewGuid().ToString();
                            PageTemplateInfoProvider.SetPageTemplateInfo(pti);
                        }
                    }
                }

                // Update fields
                node.NodeBodyElementAttributes = txtBodyCss.Text;
                node.NodeDocType = txtDocType.Text;
                node.NodeHeadTags = txtHeadTags.Value.ToString();

                // Update the node
                node.Update();

                // Update search index
                if ((node.PublishedVersionExists) && (SearchIndexInfoProvider.SearchEnabled))
                {
                    SearchTaskInfoProvider.CreateTask(SearchTaskTypeEnum.Update, PredefinedObjectType.DOCUMENT, SearchHelper.ID_FIELD, node.GetSearchID());
                }

                // Log synchronization
                DocumentHelper.LogSynchronization(node, TaskTypeEnum.UpdateDocument, tree);

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");

                // Clear cache
                PageInfoProvider.RemoveAllPageInfosFromCache();
            }
        }
    }
}
