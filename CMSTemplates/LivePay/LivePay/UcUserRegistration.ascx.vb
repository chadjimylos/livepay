﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.SiteProvider

Partial Class CMSTemplates_LivePay_UcUserRegistration
    Inherits CMSUserControl


#Region "Variables"

    ''' <summary>
    ''' My property
    ''' </summary>
    Private mMyProperty As String = Nothing

#End Region

#Region "Properties"


    ''' <summary>
    ''' Gets or sets value of MyProperty
    ''' </summary>
    Public Property MyProperty() As String
        Get
            Return mMyProperty
        End Get
        Set(ByVal value As String)
            mMyProperty = value
        End Set
    End Property

#End Region


#Region "Methods"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
    End Sub

#End Region

    Protected Sub BtnInsert_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles BtnInsert.Click
        Captchas.ValidateCaptcha(txtCaptcha.Text.ToUpper)
        If Captchas.UserValidated Then
            Dim ShowMsg As String = String.Concat("")
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "ShowCaptcha", ShowMsg, True)


            ' Check Email & password"
            ' Check whether user with same email does not exist 
            Dim ui As UserInfo = UserInfoProvider.GetUserInfo(StripHTMLFunctions.StripTags(txtEmail.Text))
            'If (ui = DBNull) Then
            'lblError.Visible = True
            'lblError.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.UserAlreadyExists").Replace("%%name%%", HTMLHelper.HTMLEncode(txtEmail.Text))
            'Return
            'End If


            ' Check whether password is same
            If (StripHTMLFunctions.StripTags(txtPassword.Text) <> StripHTMLFunctions.StripTags(txtConfirmPass.Text)) Then
                'lblError.Visible = True
                'lblError.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.PassworDoNotMatch")
                'Return
            End If


            'If ((Me.PasswordMinLength > 0) AndAlso (txtPassword.Text.Length < Me.PasswordMinLength)) Then
            '    lblError.Visible = True
            '    lblError.Text = String.Format(ResHelper.GetString("Webparts_Membership_RegistrationForm.PasswordMinLength"), Me.PasswordMinLength.ToString())
            '    Return
            'End If

            If (Not ValidationHelper.IsEmail(StripHTMLFunctions.StripTags(txtEmail.Text).ToLower())) Then
                'lblError.Visible = True
                'lblError.Text = ResHelper.GetString("Webparts_Membership_RegistrationForm.EmailIsNotValid")
                'Return
            End If

            ' Check whether email is unique if it is required
            If (Not UserInfoProvider.IsEmailUnique(StripHTMLFunctions.StripTags(txtEmail.Text).Trim(), CMSContext.CurrentSiteName, 0)) Then
                'lblError.Visible = True
                'lblError.Text = ResHelper.GetString("UserInfo.EmailAlreadyExist")
                'Return
            End If

            ' Store to DB
            UserInfoProvider.SetUserInfo(ui)

            ' Set password
            UserInfoProvider.SetPassword(ui.UserName, StripHTMLFunctions.StripTags(txtPassword.Text))

            ' Create UserInfo
            Dim user As UserInfo = New UserInfo()

            ' Set some properties
            user.UserName = "Alice"
            user.FullName = "Alice Cooper"
            user.Email = "alice.cooper@domain.com"
            user.IsEditor = True
            user.PreferredCultureCode = CMSContext.PreferredCultureCode


            ' Create new user
            CMS.SiteProvider.UserInfoProvider.SetUserInfo(user)

        Else
            '  TitleDivBlue.InnerHtml = "Παρακαλώ προσπαθήστε ξανά!"
            txtCaptcha.Text = ""
        End If
    End Sub
End Class
