CREATE TABLE [COM_ShoppingCartSKU] (
		[CartItemID]             int NOT NULL IDENTITY(1, 1),
		[ShoppingCartID]         int NOT NULL,
		[SKUID]                  int NOT NULL,
		[SKUUnits]               int NOT NULL,
		[CartItemCustomData]     nvarchar(max) NULL,
		[CartItemGuid]           uniqueidentifier NOT NULL,
		[CartItemParentGuid]     uniqueidentifier NULL
)
ON [PRIMARY]
ALTER TABLE [COM_ShoppingCartSKU]
	ADD
	CONSTRAINT [PK_COM_ShoppingCartSKU]
	PRIMARY KEY
	([CartItemID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_ShoppingCartSKU]') IS NOT NULL
	AND OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCartSKU_ShoppingCartID_COM_ShoppingCart]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCartSKU]
			ADD CONSTRAINT [FK_COM_ShoppingCartSKU_ShoppingCartID_COM_ShoppingCart]
			FOREIGN KEY ([ShoppingCartID]) REFERENCES [COM_ShoppingCart] ([ShoppingCartID])
END
IF OBJECT_ID(N'[COM_ShoppingCartSKU]') IS NOT NULL
	AND OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCartSKU_SKUID_COM_SKU]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCartSKU]
			ADD CONSTRAINT [FK_COM_ShoppingCartSKU_SKUID_COM_SKU]
			FOREIGN KEY ([SKUID]) REFERENCES [COM_SKU] ([SKUID])
END
CREATE INDEX [IX_COM_ShoppingCartSKU_ShoppingCartID]
	ON [COM_ShoppingCartSKU] ([ShoppingCartID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_ShoppingCartSKU_SKUID]
	ON [COM_ShoppingCartSKU] ([SKUID])
	ON [PRIMARY]
