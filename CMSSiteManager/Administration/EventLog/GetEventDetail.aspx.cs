using System;
using System.Data;
using System.Text;

using CMS.GlobalHelper;
using CMS.EventLog;
using CMS.DataEngine;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSSiteManager_Administration_EventLog_GetEventDetail : CMSEventLogPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int eventId = QueryHelper.GetInteger("eventid", 0);

        EventLogProvider eventProvider = new EventLogProvider();
        IDataClass ev = eventProvider.GetEventData(eventId);

        if (ev != null)
        {
            if (ev.DataRow != null)
            {
                UTF8Encoding enc = new UTF8Encoding();
                byte[] file = enc.GetBytes(EventLogHelper.GetEventText(ev));

                Response.AddHeader("Content-disposition", "attachment; filename=eventdetails.txt");
                Response.ContentType = "text/plain";
                Response.BinaryWrite(file);
                
                RequestHelper.EndResponse();
            }
        }
    }
}
