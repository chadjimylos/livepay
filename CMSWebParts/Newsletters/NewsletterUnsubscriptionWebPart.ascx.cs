using System;

using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.Newsletter;
using CMS.CMSHelper;

public partial class CMSWebParts_Newsletters_NewsletterUnsubscriptionWebPart : CMSAbstractWebPart
{
    #region "Public properties"

    /// <summary>
    /// Gets or sets the unsubscribed text
    /// </summary>
    public string UnsubscribedText
    {
        get
        {
            return ValidationHelper.GetString(GetValue("UnsubscribedText"), "");
        }
        set
        {
            SetValue("UnsubscribedText", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether confirmation email will be sent
    /// </summary>
    public bool SendConfirmationEmail
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("SendConfirmationEmail"), true);
        }
        set
        {
            SetValue("SendConfirmationEmail", value);
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            int siteId = 0;

            // Get current id
            if (CMSContext.CurrentSite != null)
            {
                siteId = CMSContext.CurrentSite.SiteID;
            }

            // Get subscriber and newsletter guid from query string
            Guid subscriberGuid = ValidationHelper.GetGuid(Request.QueryString["subscriberguid"], Guid.Empty);
            Guid newsletterGuid = ValidationHelper.GetGuid(Request.QueryString["newsletterguid"], Guid.Empty);

            // Check whether both guid exists
            if ((subscriberGuid != Guid.Empty) && (newsletterGuid != Guid.Empty))
            {
                // Check whether subscription is valid
                if (SubscriberProvider.IsSubscribed(subscriberGuid, newsletterGuid, siteId))
                {
                    // Unsubscribe action
                    SubscriberProvider.Unsubscribe(subscriberGuid, newsletterGuid, siteId, SendConfirmationEmail);

                    // Display coinfirmation message
                    lblInfo.Visible = true;
                    lblInfo.Text = UnsubscribedText == "" ? ResHelper.GetString("Unsubscribe.Unsubscribed") : UnsubscribedText;

                    //if subscriber has been unsubscribed after some issue, increase number of unsubscribed persons of the issue by 1
                    int issueId = ValidationHelper.GetInteger(Request.QueryString["issueid"], 0);
                    if (issueId > 0)
                    {
                        Issue issue = IssueProvider.GetIssue(issueId);
                        if (issue != null)
                        {
                            issue.IncreaseUnsubscribedNumber();
                            IssueProvider.SetIssue(issue);
                        }
                    }
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Unsubscribe.NotSubscribed");
                }
            }
            else
            {
                Visible = false;
            }
        }
    }
}
