<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentType_Edit_Transformation_Edit.aspx.cs"
    Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_Transformation_Edit"
    ValidateRequest="false" Theme="Default" EnableEventValidation="false" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Transformation Edit" %>
<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/TransformationEdit.ascx" TagName="TransformationEdit"
    TagPrefix="cms" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <%-- Parameters must be set here in ASPX --%>
    <cms:TransformationEdit ID="transformationEdit" runat="server" EditingPage="DocumentType_Edit_Transformation_Edit.aspx"
        ListPage="~/CMSSiteManager/Development/DocumentTypes/DocumentType_Edit_Transformation_List.aspx"
        ParameterName="documenttypeid" />
</asp:Content>
