using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.FormControls;
using CMS.ExtendedControls;

public partial class CMSModules_Ecommerce_FormControls_OrderStatusSelector : FormEngineUserControl
{
    #region "Variables"

    private bool mUseStatusNameForSelection = true;
    private bool mAddAllItemsRecord = true;

    private StatusEnum mDisplayItems = StatusEnum.Both;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Indicates which objects should be displayed (Enabled/Disabled/Both). Default values is 'Both'.
    /// </summary>
    public StatusEnum DisplayItems
    {
        get
        {
            return mDisplayItems;
        }
        set
        {
            mDisplayItems = value;
        }
    }


    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (this.mUseStatusNameForSelection)
            {
                return this.OrderStatusName;
            }
            else
            {
                return this.OrderStatusID;
            }
        }
        set
        {
            if (this.mUseStatusNameForSelection)
            {
                this.OrderStatusName = ValidationHelper.GetString(value, "");
            }
            else
            {
                this.OrderStatusID = ValidationHelper.GetInteger(value, 0);
            }
        }
    }


    /// <summary>
    /// Gets or sets the OrderStatus ID.
    /// </summary>
    public int OrderStatusID
    {
        get
        {
            if (this.mUseStatusNameForSelection)
            {
                string name = ValidationHelper.GetString(uniSelector.Value, "");
                OrderStatusInfo oi = OrderStatusInfoProvider.GetOrderStatusInfo(name);
                if (oi != null)
                {
                    return oi.StatusID;
                }
                return 0;
            }
            else
            {
                return ValidationHelper.GetInteger(uniSelector.Value, 0);
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.mUseStatusNameForSelection)
            {
                OrderStatusInfo tgi = OrderStatusInfoProvider.GetOrderStatusInfo(value);
                if (tgi != null)
                {
                    this.uniSelector.Value = tgi.StatusID;
                }
            }
            else
            {
                this.uniSelector.Value = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets the OrderStatus code name.
    /// </summary>
    public string OrderStatusName
    {
        get
        {
            if (this.mUseStatusNameForSelection)
            {
                return ValidationHelper.GetString(this.uniSelector.Value, "");
            }
            else
            {
                int id = ValidationHelper.GetInteger(this.uniSelector.Value, 0);
                OrderStatusInfo oi = OrderStatusInfoProvider.GetOrderStatusInfo(id);
                if (oi != null)
                {
                    return oi.StatusName;
                }
                return "";
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.mUseStatusNameForSelection)
            {
                this.uniSelector.Value = value;
            }
            else
            {
                OrderStatusInfo tgi = OrderStatusInfoProvider.GetOrderStatusInfo(value);
                if (tgi != null)
                {
                    this.uniSelector.Value = tgi.StatusName;
                }
            }
        }
    }


    /// <summary>
    ///  If true, selected value is OrderStatusName, if false, selected value is OrderStatusID
    /// </summary>
    public bool UseStatusNameForSelection
    {
        get
        {
            return mUseStatusNameForSelection;
        }
        set
        {
            mUseStatusNameForSelection = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add none item record to the dropdownlist.
    /// </summary>
    public bool AddAllItemsRecord
    {
        get
        {
            return mAddAllItemsRecord;
        }
        set
        {
            mAddAllItemsRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Returns ClientID of the dropdownlist.
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect.ClientID;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.AllowAll = this.AddAllItemsRecord;
        this.uniSelector.ReturnColumnName = (this.UseStatusNameForSelection ? "StatusName" : "StatusID");
        this.uniSelector.EnabledColumnName = "StatusEnabled";
        this.uniSelector.OrderBy = "StatusOrder";

        if (this.DisplayItems == StatusEnum.Disabled)
        {
            this.uniSelector.WhereCondition = "StatusEnabled = 0";
        }
        else if (this.DisplayItems == StatusEnum.Enabled)
        {
            this.uniSelector.WhereCondition = "StatusEnabled = 1";
        }

        if (this.UseStatusNameForSelection)
        {
            this.uniSelector.AllRecordValue = "";
            this.uniSelector.NoneRecordValue = "";
        }
    }
}
