<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewsletterEmailQueue.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_EmailQueue_NewsletterEmailQueue" Theme="Default"
    MaintainScrollPositionOnPostback="true" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Newsletter - E-mail queue" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label ID="lblIntro" runat="server" EnableViewState="false" CssClass="InfoLabel" />
    <br />
    <cms:UniGrid runat="server" ID="gridElem" GridName="NewsletterEmailQueue.xml" OrderBy="EmailID"
        IsLiveSite="false" />
</asp:Content>
