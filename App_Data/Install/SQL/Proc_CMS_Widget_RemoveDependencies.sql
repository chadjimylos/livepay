CREATE PROCEDURE [Proc_CMS_Widget_RemoveDependencies]
    @Id int
AS
BEGIN    
    DELETE FROM CMS_WidgetRole WHERE WidgetID = @Id
END
