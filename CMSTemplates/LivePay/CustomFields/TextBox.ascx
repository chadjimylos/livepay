<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TextBox.ascx.cs" Inherits="CMSFormControls_TextBox" %>

<div runat="server" id="CustomCloud_div"></div>

<asp:TextBox runat="server" cssclass="RegUsersTab1CustomTxt" width="260px" ID="TextBox" />

<div class="HideIt">
    <asp:RequiredFieldValidator Visible="false" ID="rfv_TextBox" runat="server" ControlToValidate="TextBox" ValidationGroup="Tab1_PayForm" />
    <asp:RegularExpressionValidator Visible="false" ID="regexp_TextBox" EnableClientScript="true" Display="Dynamic" runat="server" ControlToValidate="TextBox" ValidationGroup="Tab1_PayForm" />
</div>