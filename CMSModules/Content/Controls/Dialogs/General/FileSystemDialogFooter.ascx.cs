﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.FileManager;

public partial class CMSModules_Content_Controls_Dialogs_General_FileSystemDialogFooter : CMSUserControl
{
    #region "Private properties"

    /// <summary>
    /// Base tab file path.
    /// </summary>
    private string BaseFilePath
    {
        get
        {
                return "~/CMSFormControls/Selectors/SelectFileOrFolder/";
        }
    }

    #endregion


    #region "Page events"

    /// <summary>
    /// Page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.Visible = false;
        }
        else
        {
            // Setup controls
            SetupControls();
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes all the nested controls
    /// </summary>
    private void SetupControls()
    {
        // Register for events
        this.btnInsert.Click += new EventHandler(btnInsert_Click);
        this.btnCancel.Click += new EventHandler(btnCancel_Click);

        // Register scripts for current code editor
        string editor = ResolveUrl("~/CMSScripts/Dialogs/FileSystem.js");
        this.Page.ClientScript.RegisterClientScriptInclude("CodeEditor", editor);

        this.btnInsert.ResourceString = ResHelper.GetString("general.select");
        this.btnCancel.ResourceString = ResHelper.GetString("general.cancel");

    }

    #endregion


    #region "Event handlers"

    /// <summary>
    /// Button cancel click
    /// </summary>
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.ltlScript.Text = ScriptHelper.GetScript("window.parent.close();");
    }


    /// <summary>
    /// Button insert click
    /// </summary>
    protected void btnInsert_Click(object sender, EventArgs e)
    {
        this.ltlScript.Text = ScriptHelper.GetScript("if ((window.parent.frames['insertContent'])&&(window.parent.frames['insertContent'].insertItem)) {window.parent.frames['insertContent'].insertItem();}");
    }


    /// <summary>
    /// Callback hidden button click
    /// </summary>
    protected void btnHidden_Click(object sender, EventArgs e)
    {
        SessionHelper.SetValue("DialogParameters", null);
        SessionHelper.SetValue("DialogSelectedParameters", null);
        string selected = this.hdnSelected.Value;
 
    }

    #endregion
}
