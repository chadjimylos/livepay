<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PollProperties.ascx.cs"
    Inherits="CMSModules_Polls_Controls_PollProperties" %>
<asp:Panel ID="pnlBody" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblDisplayName" EnableViewState="false" ResourceString="general.displayname"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" /><br />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName"
                    ValidationGroup="required" Display="Dynamic" EnableViewState="false" />
            </td>
        </tr>
        <asp:PlaceHolder ID="plcCodeName" runat="Server">
            <tr>
                <td class="FieldLabel">
                    <cms:LocalizedLabel runat="server" ID="lblCodeName" EnableViewState="false" ResourceString="general.codename"
                        DisplayColon="true" />
                </td>
                <td>
                    <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="200" /><br />
                    <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtCodeName"
                        ValidationGroup="required" Display="Dynamic" EnableViewState="false" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblTitle" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="TextBoxField" MaxLength="100" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblQuestion" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtQuestion" runat="server" TextMode="MultiLine" CssClass="TextAreaField"
                    MaxLength="450" /><br />
                <asp:RequiredFieldValidator ID="rfvQuestion" runat="server" ControlToValidate="txtQuestion"
                    ValidationGroup="required" Display="Dynamic" EnableViewState="false" />
                <asp:RegularExpressionValidator ID="rfvMaxLength" Display="Dynamic" ControlToValidate="txtQuestion"
                    runat="server" ValidationExpression="^[\s\S]{0,450}$" ValidationGroup="required"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblOpenFrom" EnableViewState="false" />
            </td>
            <td>
                <cms:DateTimePicker ID="dtPickerOpenFrom" runat="server"></cms:DateTimePicker>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblOpenTo" EnableViewState="false" />
            </td>
            <td>
                <cms:DateTimePicker ID="dtPickerOpenTo" runat="server"></cms:DateTimePicker>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblResponseMessage" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtResponseMessage" runat="server" TextMode="MultiLine" CssClass="TextAreaField"
                    MaxLength="450" />
                <asp:RegularExpressionValidator ID="rfvMaxLengthResponse" Display="Dynamic" ControlToValidate="txtResponseMessage"
                    runat="server" ValidationExpression="^[\s\S]{0,450}$" ValidationGroup="required"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblAllowMultipleAnswers" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox ID="chkAllowMultipleAnswers" runat="server" CssClass="CheckBoxMovedLeft"
                    AutoPostBack="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" ValidationGroup="required" />
            </td>
        </tr>
    </table>
</asp:Panel>
