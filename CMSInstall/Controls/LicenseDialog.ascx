<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LicenseDialog.ascx.cs"
    Inherits="CMSInstall_Controls_LicenseDialog" %>
<table border="0" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <asp:Label ID="lblLicenseCaption" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblLicenseTip" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="padding-top: 6px; padding-bottom: 6px">
            <asp:Label ID="lblFreeLicenseInfo" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:Label ID="lblEnterLicenseKey" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="txtLicense" CssClass="InstallLicenseTextBox" runat="server" TextMode="MultiLine"
                MaxLength="100" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:LinkButton ID="lnkSkipLicense" runat="server" />
        </td>
    </tr>
</table>
