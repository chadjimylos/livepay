using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;

/// <summary>
/// Custom session events
/// </summary>
public static class CMSSession
{
    /// <summary>
    /// Fires before the session start event
    /// </summary>
    public static void BeforeSessionStart(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires after the session start event
    /// </summary>
    public static void AfterSessionStart(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before the session end event
    /// </summary>
    public static void BeforeSessionEnd(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires after the session end event
    /// </summary>
    public static void AfterSessionEnd(object sender, EventArgs e)
    {
        // Add your custom actions
    }
}
