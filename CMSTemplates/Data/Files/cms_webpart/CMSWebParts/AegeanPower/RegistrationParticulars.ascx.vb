﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI

Imports CMS.CMSHelper
Imports CMS.GlobalHelper
Imports CMS.PortalControls

Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Globalization

Partial Public Class CMSWebParts_AegeanPower_RegistrationParticulars
    Inherits CMSAbstractWebPart
#Region "Properties"

    ''' <summary>
    ''' Gets or sets value of MyProperty
    ''' </summary>
    Public Property MyProperty() As String
        Get
            Return ValidationHelper.GetString(Me.GetValue("MyProperty"), "")
        End Get
        Set(ByVal value As String)
            Me.SetValue("MyProperty", value)
        End Set
    End Property

#End Region

#Region "Methods"
    Public Overloads Overrides Sub OnContentLoaded()
        MyBase.OnContentLoaded()
        SetupControl()
    End Sub

    Protected Sub SetupControl()
        If Me.StopProcessing Then
            ' Do nothing

        Else
            ' etsi emfanizoume stin korifi tis selidas to root -------------- Response.Write(Server.MapPath("/App_Themes/AegeanPower/UpLoads"))
            ' Set WebPart properties
            ' this.MyControl.MyProperty = this.MyProperty;
        End If
    End Sub

    Public Overloads Overrides Sub ReloadData()
        MyBase.ReloadData()

        ' Reload data in all controls if needed
        ' this.MyControl.ReloadData();
    End Sub

    Private Sub UploadFileA()
        Dim daFilename As String
        Dim FullPathFilename As String
        Dim fileexistsFilename As Boolean = False
        If BrowseFile.PostedFile.FileName.ToString.Length > 0 Then 'Ean exei epileksei arxeio
            Dim ext As String = IO.Path.GetExtension(BrowseFile.PostedFile.FileName) 'pairnei ton tipo
            If Not (ext = ".xls" Or ext = ".xlsx") Then
                lblError.Text = "Δεν έχετε εισάγει αρχείο Excel."
                'ShowAlert("Δεν έχετε εισάγει αρχείο Excel " & IO.Path.GetExtension(BrowseFile.PostedFile.FileName))
                Exit Sub
            End If
            daFilename = System.DateTime.Now.ToString("yyyyMMdd") + "_" + Guid.NewGuid().ToString() + "_" + BrowseFile.PostedFile.FileName.Split("\")(BrowseFile.PostedFile.FileName.Split("\").Length - 1)
            FullPathFilename = String.Concat(Server.MapPath("/App_Themes/AegeanPower/File_Accounts/"), daFilename) ' --- To Url Pou thes na sosis to arxio
            Try
                BrowseFile.PostedFile.SaveAs(FullPathFilename)
            Catch ex As Exception
                Response.Write(ex.ToString)
                Exit Sub
            End Try
            GetExcelA(FullPathFilename, daFilename)
        End If
        'Save the File But first Check Size 
    End Sub

    Private Shared Function ParseDate(ByVal info As System.Globalization.CultureInfo, ByVal s As String) As Object
        Dim ret As Object = DBNull.Value
        Dim d As DateTime
        If Not String.IsNullOrEmpty(s) AndAlso DateTime.TryParse(s, info, DateTimeStyles.None, d) Then
            ret = DateTime.Parse(s, info).ToString("yyyy-MM-dd hh:mm:ss:mmm")
        End If
        Return ret
    End Function

    Private Sub GetExcelA(ByVal FullPathFilename As String, ByVal FileName As String)
        Dim strConn As String = GetExcelConnectionString(FullPathFilename)
        Using objConn As New OleDbConnection(strConn)
            objConn.Open()
            Dim dtTbl As DataTable = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim strSql As String = "Select  1 as Count_ID ,* from [Sheet1$] " ' --to onoma sto sheet
            If Not IsNothing(dtTbl) Then
                strSql = String.Concat("Select  1 as Count_ID ,* from [", dtTbl.Rows(0)("Table_Name").ToString(), "] ")
            Else
                lblError.Text = "Δεν υπάρχουν sheets."
                Exit Sub
            End If

            Using objCmd As New OleDbCommand(strSql, objConn)

                Dim dt As New DataTable
                Dim ad As New OleDbDataAdapter(objCmd)
                Try
                    ad.Fill(dt)
                Catch ex As Exception
                    Exit Sub
                End Try

                Dim WithError As Boolean = False
                Try
                    For i As Integer = 0 To dt.Rows.Count - 1
                        Dim ProNo As String = dt.Rows(i)("Provider_Number").ToString()
                        Dim TheDate As String = dt.Rows(i)("Date").ToString()
                        Dim TheAmount As String = dt.Rows(i)("Amount").ToString()
                        Dim TheOtherDate As String = dt.Rows(i)("Expiration_Date").ToString()
                        Dim ThePay As String = dt.Rows(i)("Payment_Code").ToString()

                        If String.IsNullOrEmpty(ProNo) OrElse Not IsNumeric(ProNo) Then
                            WithError = True
                        End If
                    Next
                Catch ex As Exception
                    lblError.Text = "Παρουσιάστηκε σφάλμα.Παρακάλώ κάντε έλεγχο στο όνομα των στηλών."
                    Exit Sub
                End Try

                If WithError = False Then

                    Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()
                    Using bulkCopy As New SqlBulkCopy(strConnection)
                        bulkCopy.DestinationTableName = "AegeanPower_ArchiveAccounts"
                        bulkCopy.WriteToServer(dt)
                    End Using

                    lblError.Text = "Επιτυχής καταχώρηση δεδομένων"
                    Exit Sub
                Else
                    lblError.Text = "Μή έγκυρα δεδομένα"
                    Exit Sub
                End If



            End Using
        End Using
        Exit Sub

    End Sub

#End Region

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        UploadFileA()
    End Sub

    Public Shared Function IsVat(ByVal value As String) As Boolean
        Dim Sum As Integer
        Dim bt As Byte
        Dim IsValid As Boolean = True

        If Not String.IsNullOrEmpty(value) AndAlso value.Length = 9 Then
            Sum = 0
            For i As Integer = 1 To value.Length - 1
                If Not Char.IsNumber(value.Substring(i - 1, 1)) Then
                    Sum = 0
                    Exit For
                End If

                Sum = Sum + Convert.ToInt32(value.Substring(i - 1, 1)) * (2 ^ (value.Length - i))
            Next

            If Sum = 0 Then
                IsValid = False
            Else
                bt = (Sum Mod 11)
                IsValid = (Convert.ToInt32(value.Substring(8, 1)) = bt OrElse (bt = 10 AndAlso Convert.ToInt32(value.Substring(8, 1)) = 0))
            End If
        Else
            IsValid = False
        End If

        Return IsValid
    End Function

#Region " Methods & Connections "

    Protected Function GetExcelConnectionString(ByVal fullpath As String) As String
        Return String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;'", fullpath)
    End Function

#End Region

#Region " UpLoad Files "

    Protected Sub BtnUploadCounter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnUploadCounter.Click
        Try
            Dim daFilename As String
            Dim FullPathFilename As String
            Dim fileexistsFilename As Boolean = False

            If UpLoadCounterExcel.PostedFile.FileName.ToString.Length > 0 Then 'Ean exei epileksei arxeio
                Dim ext As String = IO.Path.GetExtension(UpLoadCounterExcel.PostedFile.FileName) 'pairnei ton tipo
                If Not (ext = ".xls" Or ext = ".xlsx") Then
                    lblError.Text = "Δεν έχετε εισάγει αρχείο Excel."
                    Exit Sub
                End If

                daFilename = System.DateTime.Now.ToString("yyyyMMdd") + "_" + Guid.NewGuid().ToString() + "_" + UpLoadCounterExcel.PostedFile.FileName.Split("\")(UpLoadCounterExcel.PostedFile.FileName.Split("\").Length - 1)

                FullPathFilename = String.Concat(Server.MapPath("/App_Themes/AegeanPower/File_Cash/"), daFilename) ' --- To Url Pou thes na sosis to arxio


                UpLoadCounterExcel.PostedFile.SaveAs(FullPathFilename)
              
                ImportCounters(FullPathFilename, daFilename)

            End If
        Catch ex As Exception
            Label1.Text = ex.ToString
            Response.End()
        End Try
    End Sub

    Protected Sub BtnUploadAcount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnUploadAcount.Click
        Try
        Dim daFilename As String
        Dim FullPathFilename As String
        Dim fileexistsFilename As Boolean = False
        If UploadAcountExcel.PostedFile.FileName.ToString.Length > 0 Then 'Ean exei epileksei arxeio
            Dim ext As String = IO.Path.GetExtension(UploadAcountExcel.PostedFile.FileName) 'pairnei ton tipo
            If Not (ext = ".xls" Or ext = ".xlsx") Then
                lblError.Text = "Δεν έχετε εισάγει αρχείο Excel."
                Exit Sub
            End If
            daFilename = System.DateTime.Now.ToString("yyyyMMdd") + "_" + Guid.NewGuid().ToString() + "_" + UploadAcountExcel.PostedFile.FileName.Split("\")(UploadAcountExcel.PostedFile.FileName.Split("\").Length - 1)
            FullPathFilename = String.Concat(Server.MapPath("/App_Themes/AegeanPower/File_Accounts/"), daFilename) ' --- To Url Pou thes na sosis to arxio

            UploadAcountExcel.PostedFile.SaveAs(FullPathFilename)
            ImportAcounts(FullPathFilename, daFilename)
            End If
        Catch ex As Exception
            Response.Write("sdddddddddd")
            Response.End()
        End Try
    End Sub

#End Region

#Region " Import Data "

    Private Sub ImportCounters(ByVal FullPathFilename As String, ByVal daFilename As String)
        Try
            Dim strConn As String = GetExcelConnectionString(FullPathFilename)
            Using objConn As New OleDbConnection(strConn)
                objConn.Open()
                Dim dtTbl As DataTable = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim strSql As String = "Select  1 as Count_ID ,* from [Sheet1$] " ' --to onoma sto sheet
                If Not IsNothing(dtTbl) Then
                    strSql = String.Concat("Select  1 as Count_ID ,* from [", dtTbl.Rows(0)("Table_Name").ToString(), "] ")
                Else
                    lblError.Text = "Δεν υπάρχουν sheets."
                    Exit Sub
                End If
                Using objCmd As New OleDbCommand(strSql, objConn)
                    Dim dt As New DataTable
                    Dim ad As New OleDbDataAdapter(objCmd)
                    ad.Fill(dt)
                    Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()
                    Using bulkCopy As New SqlBulkCopy(strConnection)
                        bulkCopy.DestinationTableName = "AegeanPower_ArchiveCounter"
                        bulkCopy.WriteToServer(dt)
                    End Using
                End Using
            End Using
        Catch ex As Exception
            Label1.Text = ex.ToString
            Response.End()
        End Try
    End Sub

    Private Sub ImportAcounts(ByVal FullPathFilename As String, ByVal daFilename As String)
        Try
            Dim strConn As String = GetExcelConnectionString(FullPathFilename)
        Using objConn As New OleDbConnection(strConn)
            objConn.Open()
            Dim dtTbl As DataTable = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
            Dim strSql As String = "Select  1 as Count_ID ,* from [Sheet1$] " ' --to onoma sto sheet
            If Not IsNothing(dtTbl) Then
                strSql = String.Concat("Select  1 as Count_ID ,* from [", dtTbl.Rows(0)("Table_Name").ToString(), "] ")
            Else
                lblError.Text = "Δεν υπάρχουν sheets."
                Exit Sub
            End If
            Using objCmd As New OleDbCommand(strSql, objConn)
                Dim dt As New DataTable
                Dim ad As New OleDbDataAdapter(objCmd)
                ad.Fill(dt)
                Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()
                Using bulkCopy As New SqlBulkCopy(strConnection)
                    bulkCopy.DestinationTableName = "AegeanPower_ArchiveAccounts"
                    bulkCopy.WriteToServer(dt)
                End Using
            End Using
            End Using
        Catch ex As Exception
            Response.Write(ex.ToString)
            Response.End()
        End Try
    End Sub


#End Region

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub
  
End Class

