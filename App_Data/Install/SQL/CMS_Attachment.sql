CREATE TABLE [CMS_Attachment] (
		[AttachmentID]                int NOT NULL IDENTITY(1, 1),
		[AttachmentName]              nvarchar(255) NOT NULL,
		[AttachmentExtension]         nvarchar(50) NOT NULL,
		[AttachmentSize]              int NOT NULL,
		[AttachmentMimeType]          nvarchar(100) NOT NULL,
		[AttachmentBinary]            varbinary(max) NULL,
		[AttachmentImageWidth]        int NULL,
		[AttachmentImageHeight]       int NULL,
		[AttachmentDocumentID]        int NULL,
		[AttachmentGUID]              uniqueidentifier NOT NULL,
		[AttachmentLastHistoryID]     int NULL,
		[AttachmentSiteID]            int NULL,
		[AttachmentLastModified]      datetime NOT NULL,
		[AttachmentIsUnsorted]        bit NULL,
		[AttachmentOrder]             int NULL,
		[AttachmentGroupGUID]         uniqueidentifier NULL,
		[AttachmentFormGUID]          uniqueidentifier NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Attachment]
	ADD
	CONSTRAINT [PK_CMS_Attachment]
	PRIMARY KEY
	NONCLUSTERED
	([AttachmentID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_Attachment]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Document]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Attachment_AttachmentDocumentID_CMS_Document]') IS NULL
BEGIN
		ALTER TABLE [CMS_Attachment]
			ADD CONSTRAINT [FK_CMS_Attachment_AttachmentDocumentID_CMS_Document]
			FOREIGN KEY ([AttachmentDocumentID]) REFERENCES [CMS_Document] ([DocumentID])
END
IF OBJECT_ID(N'[CMS_Attachment]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Attachment_AttachmentSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_Attachment]
			ADD CONSTRAINT [FK_CMS_Attachment_AttachmentSiteID_CMS_Site]
			FOREIGN KEY ([AttachmentSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_CMS_Attachment_AttachmentDocumentID_AttachmentIsUnsorted_AttachmentName_AttachmentOrder]
	ON [CMS_Attachment] ([AttachmentDocumentID], [AttachmentName], [AttachmentIsUnsorted], [AttachmentOrder])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Attachment_AttachmentGUID_AttachmentSiteID]
	ON [CMS_Attachment] ([AttachmentGUID], [AttachmentSiteID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Attachment_AttachmentIsUnsorted_AttachmentGroupGUID_AttachmentFormGUID_AttachmentOrder]
	ON [CMS_Attachment] ([AttachmentIsUnsorted], [AttachmentGroupGUID], [AttachmentFormGUID], [AttachmentOrder])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
