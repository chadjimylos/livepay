CREATE TABLE [CMS_DocumentAlias] (
		[AliasID]               int NOT NULL IDENTITY(1, 1),
		[AliasNodeID]           int NOT NULL,
		[AliasCulture]          nvarchar(20) NULL,
		[AliasURLPath]          nvarchar(450) NULL,
		[AliasExtensions]       nvarchar(100) NULL,
		[AliasCampaign]         nvarchar(100) NULL,
		[AliasWildcardRule]     nvarchar(440) NULL,
		[AliasPriority]         int NULL,
		[AliasGUID]             uniqueidentifier NULL,
		[AliasLastModified]     datetime NOT NULL,
		[AliasSiteID]           int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_DocumentAlias]
	ADD
	CONSTRAINT [PK_CMS_DocumentAlias]
	PRIMARY KEY
	NONCLUSTERED
	([AliasID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_DocumentAlias]
	ADD
	CONSTRAINT [DEFAULT_CMS_DocumentAlias_AliasLastModified]
	DEFAULT ('10/22/2008 12:55:43 PM') FOR [AliasLastModified]
ALTER TABLE [CMS_DocumentAlias]
	ADD
	CONSTRAINT [DEFAULT_CMS_DocumentAlias_AliasSiteID]
	DEFAULT ((0)) FOR [AliasSiteID]
IF OBJECT_ID(N'[CMS_DocumentAlias]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_DocumentAlias_AliasNodeID_CMS_Tree]') IS NULL
BEGIN
		ALTER TABLE [CMS_DocumentAlias]
			ADD CONSTRAINT [FK_CMS_DocumentAlias_AliasNodeID_CMS_Tree]
			FOREIGN KEY ([AliasNodeID]) REFERENCES [CMS_Tree] ([NodeID])
END
IF OBJECT_ID(N'[CMS_DocumentAlias]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_DocumentAlias_AliasSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_DocumentAlias]
			ADD CONSTRAINT [FK_CMS_DocumentAlias_AliasSiteID_CMS_Site]
			FOREIGN KEY ([AliasSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE INDEX [IX_CMS_Document_AliasCulture]
	ON [CMS_DocumentAlias] ([AliasCulture])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_DocumentAlias_AliasNodeID]
	ON [CMS_DocumentAlias] ([AliasNodeID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_DocumentAlias_AliasSiteID]
	ON [CMS_DocumentAlias] ([AliasSiteID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_DocumentAlias_AliasURLPath]
	ON [CMS_DocumentAlias] ([AliasURLPath])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_DocumentAlias_AliasWildcardRule_AliasPriority]
	ON [CMS_DocumentAlias] ([AliasWildcardRule], [AliasPriority])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
