using System;
using System.Web;
using System.Web.UI.WebControls;

using CMS.ExtendedControls;
using CMS.GlobalHelper;
using CMS.Messaging;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Messaging_Controls_SendMessage : CMSUserControl
{
    #region "Protected & private variables"

    /// <summary>
    /// Enumeration for send message mode
    /// </summary>
    public enum SendMessageEnum
    {
        Unasigned = 0,
        NewMessage = 1,
        ReplyMessage = 2,
        ForwardMessage = 3
    }


    protected string mMessageSubject = null;
    protected string mMessageBody = null;
    protected bool isAnonymousUser = false;
    protected CurrentUserInfo currentUser = CMSContext.CurrentUser;

    private bool mAllowAnonymousUsers = true;
    private bool mAllowAnonymousRecipientSelection = true;
    private string mDefaultRecipient = "";
    private string mErrorMessage = "";

    #endregion


    #region "Public events"

    /// <summary>
    /// Event for send button click
    /// </summary>
    public event EventHandler SendButtonClick;


    /// <summary>
    /// Event for close button click
    /// </summary>
    public event EventHandler CloseButtonClick;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Display close button
    /// </summary>
    public bool DisplayCloseButton
    {
        get
        {
            return btnClose.Visible;
        }
        set
        {
            btnClose.Visible = value;
        }
    }


    /// <summary>
    /// Related message ID
    /// </summary>
    public int MessageId
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["MessageId"], 0);
        }
        set
        {
            ViewState["MessageId"] = value;
        }
    }


    /// <summary>
    /// Error message
    /// </summary>
    public string ErrorMessage
    {
        get
        {
            return mErrorMessage;
        }
        set
        {
            mErrorMessage = value;
        }
    }


    /// <summary>
    /// Send message mode
    /// </summary>
    public SendMessageEnum SendMessageMode
    {
        get
        {
            object sendMessageMode = ViewState["SendMessageMode"];
            return (sendMessageMode != null)
                       ? (SendMessageEnum)Enum.Parse(typeof(SendMessageEnum), sendMessageMode.ToString())
                       : SendMessageEnum.Unasigned;
        }
        set
        {
            ViewState["SendMessageMode"] = value;
        }
    }


    /// <summary>
    /// Paste original message
    /// </summary>
    public bool PasteOriginalMessage
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["PasteOriginalMessage"], true);
        }
        set
        {
            ViewState["PasteOriginalMessage"] = value;
        }
    }


    /// <summary>
    /// True if anonymous users should be able to send messages
    /// </summary>
    public bool AllowAnonymousUsers
    {
        get
        {
            return mAllowAnonymousUsers;
        }
        set
        {
            mAllowAnonymousUsers = value;
        }
    }


    /// <summary>
    /// True if anonymous users should be able to select recipient fo the messages
    /// </summary>
    public bool AllowAnonymousRecipientSelection
    {
        get
        {
            return mAllowAnonymousRecipientSelection;
        }
        set
        {
            mAllowAnonymousRecipientSelection = value;
        }
    }


    /// <summary>
    /// Default recipient of the message
    /// </summary>
    public string DefaultRecipient
    {
        get
        {
            return mDefaultRecipient;
        }
        set
        {
            mDefaultRecipient = value;
        }
    }


    /// <summary>
    /// Message subject
    /// </summary>
    public string MessageSubject
    {
        get
        {
            return mMessageSubject;
        }
        set
        {
            mMessageSubject = value;
        }
    }


    /// <summary>
    /// Message body
    /// </summary>
    public string MessageBody
    {
        get
        {
            return mMessageBody;
        }
        set
        {
            mMessageBody = value;
        }
    }


    /// <summary>
    /// Use prompt dialog
    /// </summary>
    public bool UsePromptDialog
    {
        get
        {
            return ucBBEditor.UsePromptDialog;
        }
        set
        {
            ucBBEditor.UsePromptDialog = value;
        }
    }


    /// <summary>
    /// Information text
    /// </summary>
    public string InformationText
    {
        get
        {
            return lblSendInfo.Text;
        }
    }


    /// <summary>
    /// Errror text
    /// </summary>
    public string ErrorText
    {
        get
        {
            return lblSendError.Text;
        }
    }


    /// <summary>
    /// Send button
    /// </summary>
    public LocalizedButton SendButton
    {
        get
        {
            return btnSendMessage;
        }
    }


    /// <summary>
    /// Cancel button
    /// </summary>
    public LocalizedButton CancelButton
    {
        get
        {
            return btnClose;
        }
    }


    /// <summary>
    /// BB editor
    /// </summary>
    public BBEditor BBEditor
    {
        get
        {
            return ucBBEditor;
        }
    }


    /// <summary>
    /// Box with subject
    /// </summary>
    public TextBox SubjectBox
    {
        get
        {
            return txtSubject;
        }
    }


    /// <summary>
    /// Box with "From" item
    /// </summary>
    public TextBox FromBox
    {
        get
        {
            return txtFrom;
        }
    }

    #endregion


    #region "Page events"

    /// <summary>
    /// Load page.
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event arguments</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        ucMessageUserSelector.IsLiveSite = IsLiveSite;
        ReloadData();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        ucBBEditor.TextArea.CssClass = "BodyField";

        // Dialogs mode
        if (DisplayCloseButton)
        {
            pnlButtons.CssClass += " PageFooterLine";
            pnlSubPanel.CssClass = "FloatRight";
        }
    }

    #endregion


    #region "Other methods"

    /// <summary>
    /// Reload data
    /// </summary>
    public void ReloadData()
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            // WAI validation
            lblText.AssociatedControlClientID = ucBBEditor.TextArea.ClientID;

            // Default settings
            lblSendInfo.Visible = false;
            lblSendError.Visible = false;
            pnlSendMessage.Visible = true;
            pnlNoUser.Visible = false;

            if (currentUser.IsAuthenticated() || AllowAnonymousUsers)
            {
                // Decide if the user is anonymous
                isAnonymousUser = currentUser.IsPublic();

                // Change controls
                if (isAnonymousUser)
                {
                    lblFrom.Visible = false;
                    txtFrom.Visible = true;
                }
                else
                {
                    lblFrom.Visible = true;
                    string userName = Functions.GetFormattedUserName(currentUser.UserName, IsLiveSite);
                    lblFrom.Text = HTMLHelper.HTMLEncode(UserInfoProvider.GetFullUserName(userName, currentUser.UserNickName));
                    txtFrom.Visible = false;
                }

                // Reacting on the message
                if (MessageId != 0)
                {
                    // Get message
                    MessageInfo message = MessageInfoProvider.GetMessageInfo(MessageId);
                    if (message != null)
                    {
                        UserInfo sender = UserInfoProvider.GetUserInfo(message.MessageSenderUserID);
                        // Reply
                        switch (SendMessageMode)
                        {
                            case SendMessageEnum.ReplyMessage:
                                if (sender != null)
                                {
                                    hdnUserId.Value = sender.UserID.ToString();

                                    // Set message subject
                                    MessageSubject += message.MessageSubject;
                                    // Set message body
                                    if (currentUser.UserSignature.Trim() != string.Empty)
                                    {
                                        MessageBody += "\n\n\n" + currentUser.UserSignature;
                                    }
                                    if (PasteOriginalMessage)
                                    {
                                        MessageBody += "\n\n// " + message.MessageBody.Replace("\n", "\n// ");
                                    }

                                    // Set recipient
                                    lblTo.Text = HTMLHelper.HTMLEncode(UserInfoProvider.GetFullUserName(Functions.GetFormattedUserName(sender.UserName, IsLiveSite), sender.UserNickName));
                                    lblTo.Visible = true;
                                    ucMessageUserSelector.Visible = false;
                                }
                                else
                                {
                                    pnlNoUser.Visible = true;
                                    pnlSendMessage.Visible = false;
                                    lblNoUser.Text = ResHelper.GetString("SendMessage.NoUser");
                                }
                                break;

                            case SendMessageEnum.ForwardMessage:
                                MessageSubject += message.MessageSubject;
                                string signature = (currentUser.UserSignature.Trim() != "") ? "\n\n\n" + currentUser.UserSignature : "";
                                MessageBody += signature + "\n\n// " + message.MessageBody.Replace("\n", "\n//");
                                lblTo.Visible = false;
                                ucMessageUserSelector.Visible = true;
                                break;
                        }
                    }
                }
                // New message
                else if (SendMessageMode == SendMessageEnum.NewMessage)
                {
                    if (isAnonymousUser)
                    {
                        lblTo.Visible = !AllowAnonymousRecipientSelection;
                        ucMessageUserSelector.Visible = AllowAnonymousRecipientSelection;
                        if (!AllowAnonymousRecipientSelection)
                        {
                            string userName = UserInfoProvider.GetUserNameById(ValidationHelper.GetInteger(DefaultRecipient, 0));
                            userName = Functions.GetFormattedUserName(userName, this.IsLiveSite);

                            lblTo.Text = HTMLHelper.HTMLEncode(userName);
                            hdnUserId.Value = DefaultRecipient;
                        }
                    }
                    else if (DefaultRecipient != "")
                    {
                        lblTo.Visible = true;
                        ucMessageUserSelector.Visible = false;
                        string userName = UserInfoProvider.GetUserNameById(ValidationHelper.GetInteger(DefaultRecipient, 0));
                        userName = Functions.GetFormattedUserName(userName, this.IsLiveSite);

                        lblTo.Text = HTMLHelper.HTMLEncode(userName);
                        hdnUserId.Value = DefaultRecipient;

                        // Add signature
                        if (currentUser.UserSignature.Trim() != "")
                        {
                            MessageBody += "\n\n\n" + currentUser.UserSignature;
                        }
                    }
                    else
                    {
                        lblTo.Visible = false;
                        ucMessageUserSelector.Visible = true;

                        // Add signature
                        if (currentUser.UserSignature.Trim() != "")
                        {
                            MessageBody += "\n\n\n" + currentUser.UserSignature;
                        }
                    }
                }

                // Initialize subject and body
                if (txtSubject.Text == string.Empty)
                {
                    txtSubject.Text = MessageSubject;
                }
                if (ucBBEditor.Text == string.Empty)
                {
                    ucBBEditor.Text = MessageBody;
                }
            }
            else
            {
                Visible = false;
            }
        }
    }


    private bool ValidateBody(string body)
    {
        return ((body.Trim() != string.Empty) && (body.Trim().ToLower() != currentUser.UserSignature.Trim().ToLower()));
    }

    #endregion


    #region "Button handling"

    protected void btnClose_Click(object sender, EventArgs e)
    {
        // External event
        if (CloseButtonClick != null)
        {
            CloseButtonClick(sender, e);
        }
    }


    protected void btnSendMessage_Click(object sender, EventArgs e)
    {
        // This is because of ASP.NET default behaviour
        // The first empty line was trimmed after each postback
        if (BBEditor.Text.StartsWith("\n"))
        {
            BBEditor.Text = "\n" + BBEditor.Text;
        }
        // Flood protection
        if (!FloodProtectionHelper.CheckFlooding(CMSContext.CurrentSiteName, CMSContext.CurrentUser))
        {
            // Check banned IP
            if (BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.AllNonComplete))
            {
                int recipientId = ucMessageUserSelector.Visible
                                      ? ucMessageUserSelector.SelectedUserID
                                      : ValidationHelper.GetInteger(hdnUserId.Value, 0);
                string message = string.Empty;
                string nickName = HttpContext.Current.Server.HtmlEncode(txtFrom.Text.Trim());
                if (!ValidateBody(DiscussionMacroHelper.RemoveTags(ucBBEditor.Text)))
                {
                    message = ResHelper.GetString("SendMessage.EmptyBody");
                }


                // Check sender nick name if anonymous
                if (isAnonymousUser && (nickName == string.Empty))
                {
                    message = ResHelper.GetString("SendMesage.NoNickName");
                }

                // Check recipient
                if (recipientId == 0)
                {
                    message = ResHelper.GetString("SendMesage.NoRecipient");
                }

                if (message == string.Empty)
                {
                    // Send message
                    try
                    {
                        // Check if current user is in recipient's ignore list
                        bool isIgnored = IgnoreListInfoProvider.IsInIgnoreList(recipientId, currentUser.UserID);

                        UserInfo recipient = UserInfoProvider.GetUserInfo(recipientId);
                        MessageInfo mi = new MessageInfo();
                        mi.MessageBody = ucBBEditor.Text;
                        string subject = (txtSubject.Text.Trim() == string.Empty) ? ResHelper.GetString("Messaging.NoSubject") : txtSubject.Text.Trim();
                        mi.MessageSubject = TextHelper.LimitLength(subject, 200);
                        mi.MessageRecipientUserID = recipientId;
                        mi.MessageRecipientNickName = TextHelper.LimitLength(UserInfoProvider.GetFullUserName(Functions.GetFormattedUserName(recipient.UserName, IsLiveSite), recipient.UserNickName), 200);
                        mi.MessageSent = DateTime.Now;

                        // Anonymous user
                        if (isAnonymousUser)
                        {
                            mi.MessageSenderNickName = TextHelper.LimitLength(nickName, 200);
                            mi.MessageSenderDeleted = true;
                        }
                        else
                        {
                            mi.MessageSenderUserID = currentUser.UserID;
                            mi.MessageSenderNickName = TextHelper.LimitLength(UserInfoProvider.GetFullUserName(Functions.GetFormattedUserName(currentUser.UserName, IsLiveSite), currentUser.UserNickName), 200);

                            // If the user is ignored, delete message automatically
                            if (isIgnored)
                            {
                                mi.MessageRecipientDeleted = true;
                            }
                        }

                        string error = string.Empty;

                        // Check bad words
                        if (!BadWordInfoProvider.CanUseBadWords(currentUser, CMSContext.CurrentSiteName))
                        {
                            error = BadWordsHelper.CheckBadWords(mi, "MessageSubject;MessageBody", currentUser.UserID);
                        }

                        if (error != string.Empty)
                        {
                            lblSendError.Visible = true;
                            lblSendError.Text = error;
                        }
                        else
                        {
                            // Check message subject, if empty set no subject text
                            if (mi.MessageSubject.Trim() == string.Empty)
                            {
                                mi.MessageSubject = ResHelper.GetString("Messaging.NoSubject");
                            }

                            // Whole text has been removed
                            if (!ValidateBody(mi.MessageBody))
                            {
                                lblSendError.Visible = true;
                                lblSendError.Text = ResHelper.GetString("SendMessage.EmptyBodyBadWords");
                            }
                            else
                            {
                                // Save the message
                                MessageInfoProvider.SetMessageInfo(mi);

                                // Send notification email, if not ignored
                                if (!isIgnored)
                                {
                                    MessageInfoProvider.SendNotificationEmail(mi, recipient, currentUser, CMSContext.CurrentSiteName);
                                }

                                lblSendInfo.Visible = true;
                                lblSendInfo.Text = ResHelper.GetString("SendMesage.MessageSent");
                                MessageId = 0;
                                ucMessageUserSelector.SelectedUserID = 0;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        lblSendError.Visible = true;
                        lblSendError.Text = ex.Message;
                        ErrorMessage = ex.Message;
                    }
                }
                // Error in the form
                else
                {
                    lblSendError.Visible = true;
                    lblSendError.Text = message;
                    ErrorMessage = message;
                }
            }
            else
            {
                lblSendError.Visible = true;
                lblSendError.Text = ResHelper.GetString("General.BannedIP");
            }
        }
        else
        {
            lblSendError.Visible = true;
            lblSendError.Text = ResHelper.GetString("General.FloodProtection");
        }

        // External event
        if (SendButtonClick != null)
        {
            SendButtonClick(sender, e);
        }
    }

    #endregion
}
