<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocumentAttachmentsConfiguration.ascx.cs"
    Inherits="CMSModules_Content_Controls_Attachments_DocumentAttachmentsConfiguration" %>
<%@ Register Src="~/CMSAdminControls/AutoResizeConfiguration.ascx" TagName="AutoResize"
    TagPrefix="cms" %>
<br />
<strong style="margin-left: 3px;">
    <cms:LocalizedLabel ID="lblCaption" runat="server" EnableViewState="false" ResourceString="attach.ConfigCaption" />
</strong>
<br />
<table width="303">
    <asp:PlaceHolder ID="plcAdvanced" runat="server">
        <tr>
            <td style="width: 142px; white-space: nowrap;">
                <cms:LocalizedLabel ID="lblAllowChangeOrder" runat="server" EnableViewState="false"
                    ResourceString="attach.allowchangeorder" />
            </td>
            <td>
                <asp:CheckBox ID="chkAllowChangeOrder" runat="server" CssClass="CheckBoxMovedLeft"
                    Checked="true" />
            </td>
        </tr>
        <tr>
            <td style="width: 142px; white-space: nowrap;">
                <cms:LocalizedLabel ID="lblAllowPaging" runat="server" EnableViewState="false" ResourceString="attach.allowpaging" />
            </td>
            <td>
                <asp:CheckBox ID="chkAllowPaging" runat="server" CssClass="CheckBoxMovedLeft" Checked="true" />
            </td>
        </tr>
        <tr>
            <td style="width: 142px; white-space: nowrap;">
                <cms:LocalizedLabel ID="lblPageSize" runat="server" EnableViewState="false" ResourceString="attach.pagesize" />
            </td>
            <td>
                <asp:TextBox ID="txtPageSize" runat="server" CssClass="SmallTextBox" Text="10" />
            </td>
        </tr>
        <tr style="height: 5px; line-height: 5px;">
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td style="width: 142px; white-space: nowrap;">
            &nbsp;
        </td>
        <td>
            <cms:LocalizedCheckBox ID="chkInehrit" runat="server" CssClass="CheckBoxMovedLeft"
                ResourceString="attach.inheritfromsettings" Checked="true" />
        </td>
    </tr>
    <tr>
        <td style="width: 142px; white-space: nowrap; padding-top: 5px; vertical-align: top;">
            <cms:LocalizedLabel ID="lblAllowedExtensions" runat="server" EnableViewState="false"
                ResourceString="attach.allowedextensions" />
        </td>
        <td>
            <asp:TextBox ID="txtAllowedExtensions" runat="server" CssClass="SmallTextBox" /><br />
            <em>
                <cms:LocalizedLabel ID="lblExtExample" runat="server" ResourceString="attach.extensionexample" /></em>
        </td>
    </tr>
</table>
<br />
<strong style="margin-left: 3px;">
    <cms:LocalizedLabel ID="lblAutoresize" runat="server" EnableViewState="false" ResourceString="attach.ResizeCaption" />
</strong>
<br />
<cms:AutoResize ID="ucAutoResize" runat="server" />
<br />