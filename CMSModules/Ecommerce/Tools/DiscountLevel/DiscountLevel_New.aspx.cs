using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_DiscountLevel_DiscountLevel_New : CMSEcommercePage
{
    protected int discountLevelId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Control initializations
        this.lblDiscountLevelValidFrom.Text = ResHelper.GetString("DiscountLevel_Edit.DiscountLevelValidFromLabel");
        this.lblDiscountLevelValue.Text = ResHelper.GetString("DiscountLevel_Edit.DiscountLevelValueLabel");
        this.lblDiscountLevelValidTo.Text = ResHelper.GetString("DiscountLevel_Edit.DiscountLevelValidToLabel");
        this.btnOk.Text = ResHelper.GetString("General.OK");
        this.dtPickerDiscountLevelValidFrom.SupportFolder = "~/CMSAdminControls/Calendar";
        this.dtPickerDiscountLevelValidTo.SupportFolder = "~/CMSAdminControls/Calendar";

        // Init Validators
        this.rfvDiscountLevelDisplayName.ErrorMessage = ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelDisplayName.ErrorMessage");
        this.rfvDiscountLevelName.ErrorMessage = ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelName.ErrorMessage");
        this.rfvDiscountLevelValue.ErrorMessage = ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelValue.ErrorMessage");

        // Page title
        this.CurrentMaster.Title.HelpTopicName = "new_levelgeneral_tab";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Initializes page title breadcrumbs control
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("DiscountLevel_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/DiscountLevel/DiscountLevel_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = ResHelper.GetString("DiscountLevel_Edit.NewItemCaption");
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        string errorMessage = new Validator().NotEmpty(txtDiscountLevelDisplayName.Text, ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelDisplayName.ErrorMessage"))
                                             .NotEmpty(txtDiscountLevelName.Text, ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelName.ErrorMessage"))
                                             .NotEmpty(txtDiscountLevelValue.Text, ResHelper.GetString("DiscountLevel_Edit.rfvDiscountLevelValue.ErrorMessage")).Result;

        // Discount level value must be between 0-100
        if (errorMessage == "")
        {
            double value = ValidationHelper.GetDouble(txtDiscountLevelValue.Text.Trim(), -1);
            if ((value < 0) || (value > 100))
            {
                errorMessage = ResHelper.GetString("DiscountLevel_Edit.rvDiscountLevelValue.ErrorMessage");
            }
        }

        // from/to date validation
        if (errorMessage == "")
        {
            if ((dtPickerDiscountLevelValidFrom.SelectedDateTime != DateTime.MinValue) &&
            (dtPickerDiscountLevelValidTo.SelectedDateTime != DateTime.MinValue) &&
            (dtPickerDiscountLevelValidFrom.SelectedDateTime >= dtPickerDiscountLevelValidTo.SelectedDateTime))
            {
                errorMessage = ResHelper.GetString("General.DateOverlaps");
            }
        }

        if (errorMessage == "")
        {
            // get discount by name
            DiscountLevelInfo discountLevelObj = DiscountLevelInfoProvider.GetDiscountLevelInfo(txtDiscountLevelName.Text.Trim());
            // if name is unique OR ids are same
            if ((discountLevelObj == null) || (discountLevelObj.DiscountLevelID == discountLevelId))
            {
                // if name is unique
                if ((discountLevelObj == null))
                {
                    // and id not exist -> insert new
                    discountLevelObj = DiscountLevelInfoProvider.GetDiscountLevelInfo(discountLevelId);
                    if (discountLevelObj == null)
                    {
                        // Create new DiscountLevelInfo
                        discountLevelObj = new DiscountLevelInfo();
                    }
                }

                // Set values
                discountLevelObj.DiscountLevelValidFrom = dtPickerDiscountLevelValidFrom.SelectedDateTime;
                discountLevelObj.DiscountLevelEnabled = chkDiscountLevelEnabled.Checked;
                discountLevelObj.DiscountLevelName = txtDiscountLevelName.Text.Trim();
                discountLevelObj.DiscountLevelValue = Convert.ToDouble(txtDiscountLevelValue.Text.Trim());
                discountLevelObj.DiscountLevelValidTo = dtPickerDiscountLevelValidTo.SelectedDateTime;
                discountLevelObj.DiscountLevelDisplayName = txtDiscountLevelDisplayName.Text.Trim();

                DiscountLevelInfoProvider.SetDiscountLevelInfo(discountLevelObj);

                UrlHelper.Redirect("DiscountLevel_Edit_Frameset.aspx?discountLevelId=" + Convert.ToString(discountLevelObj.DiscountLevelID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("DiscountLevel_Edit.DiscountLevelNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
