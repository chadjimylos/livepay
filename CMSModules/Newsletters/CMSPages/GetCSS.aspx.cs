using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Caching;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Newsletter;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Newsletters_CMSPages_GetCSS : GetFilePage
{
    #region "Variables"

    protected bool useClientCache = true;

    protected CMSOutputCSS outputFile = null;
    protected CssStylesheetInfo si = null;
    protected EmailTemplate et = null;
    string newsletterTemplateName = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Returns true if the process allows cache
    /// </summary>
    public override bool AllowCache
    {
        get
        {
            if (mAllowCache == null)
            {
                // By default, cache for the newsletter CSS is always enabled (even outside of the live site)
                if (ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSAlwaysCacheNewsletterCSS"], true))
                {
                    mAllowCache = true;
                }
                else
                {
                    mAllowCache = (this.ViewMode == ViewModeEnum.LiveSite);
                }
            }

            return mAllowCache.Value;
        }
        set
        {
            mAllowCache = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check the site
        if (string.IsNullOrEmpty(CurrentSiteName))
        {
            throw new Exception("[GetCSS.aspx]: Site not running.");
        }

        newsletterTemplateName = QueryHelper.GetString("newslettertemplatename", "");

        int cacheMinutes = this.CacheMinutes;

        // Try to get data from cache
        using (CachedSection<CMSOutputCSS> cs = new CachedSection<CMSOutputCSS>(ref outputFile, cacheMinutes, true, null, "getnewslettercss", CMSContext.CurrentSiteName, newsletterTemplateName))
        {
            if (cs.LoadData)
            {
                // Process the file
                ProcessStylesheet();

                // Ensure the cache settings
                if (cs.Cached)
                {
                    // Add cache dependency
                    CacheDependency cd = CacheHelper.GetCacheDependency(new string[] { "newsletter.emailtemplate|byname|" + newsletterTemplateName.ToLower() });

                    // Cache the data
                    cs.CacheDependency = cd;
                    cs.Data = outputFile;
                }
            }
        }

        // Send the data
        SendFile(outputFile);
    }


    /// <summary>
    /// Processes the stylesheet.
    /// </summary>
    protected void ProcessStylesheet()
    {
        // Newsletter template stylesheet
        if (!string.IsNullOrEmpty(newsletterTemplateName))
        {
            // Get the template
            et = EmailTemplateProvider.GetEmailTemplate(newsletterTemplateName, CMSContext.CurrentSiteID);
            if (et != null)
            {
                // Create the output file
                outputFile = new CMSOutputCSS();
                outputFile.OutputData = HTMLHelper.ResolveCSSUrls(et.TemplateStylesheetText, Request.ApplicationPath);
                outputFile.LastModified = DateTime.Now;
            }
        }
    }


    /// <summary>
    /// Sends the given file within response.
    /// </summary>
    /// <param name="file">File to send</param>
    protected void SendFile(CMSOutputCSS file)
    {
        // Clear response.
        Response.Cookies.Clear();
        Response.Clear();

        Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);

        // Prepare etag
        string etag = "";

        // Send the file
        if ((file != null) && (file.OutputData != null))
        {
            // Client caching - only on the live site
            if (useClientCache && (CMSContext.ViewMode == ViewModeEnum.LiveSite) && (CacheHelper.CacheImageEnabled(CurrentSiteName)))
            {
                // Determine the last modified date and etag sent from the browser
                string currentETag = Request.Headers["If-None-Match"];
                string ifModifiedString = Request.Headers["If-Modified-Since"];
                if ((ifModifiedString != null) && (currentETag == etag))
                {
                    // IE-browsers send something like this:
                    // If-Modified-Since: Wed, 19 Jul 2006 11:19:59 GMT; length=3350
                    DateTime ifModified;
                    if (DateTime.TryParse(ifModifiedString.Split(";".ToCharArray())[0], out ifModified))
                    {
                        // If not changed, let the browser use the cached data
                        if (file.LastModified <= ifModified.AddSeconds(1))
                        {
                            RespondNotModified(etag, true);
                            return;
                        }
                    }
                }
            }

            // Prepare response
            Response.ContentType = "text/css";

            if (useClientCache && (CMSContext.ViewMode == ViewModeEnum.LiveSite) && (CacheHelper.CacheImageEnabled(CurrentSiteName)))
            {
                DateTime expires = DateTime.Now;

                // Send last modified header to allow client caching
                Response.Cache.SetLastModified(file.LastModified);
                Response.Cache.SetCacheability(HttpCacheability.Public);
                if (DocumentBase.AllowClientCache() && DocumentBase.UseFullClientCache)
                {
                    expires = DateTime.Now.AddMinutes(CacheHelper.CacheImageMinutes(CurrentSiteName));
                }

                Response.Cache.SetExpires(expires);
                Response.Cache.SetETag(etag);
            }

            // Add the file data
            Response.Write(file.OutputData);
        }

        CompleteRequest();
    }
}
