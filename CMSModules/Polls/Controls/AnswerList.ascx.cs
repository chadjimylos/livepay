using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Polls;
using CMS.UIControls;

public partial class CMSModules_Polls_Controls_AnswerList : CMSAdminListControl
{
    #region "Properties"

    /// <summary>
    /// Gets and sets Poll ID.
    /// </summary>
    public int PollId
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState[this.ClientID + "PollID"], 0);
        }
        set
        {
            ViewState[this.ClientID + "PollID"] = value;
        }
    }


    /// <summary>
    /// Indicates if DelayedReload for Unigrid should be used.
    /// </summary>
    public bool DelayedReload
    {
        get
        {
            return uniGrid.DelayedReload;
        }
        set
        {
            uniGrid.DelayedReload = value;
        }
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        uniGrid.IsLiveSite = this.IsLiveSite;
        uniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        uniGrid.GridView.AllowSorting = false;
        uniGrid.Query = "polls.pollanswer.selectall";
        uniGrid.WhereCondition = "AnswerPollID=" + this.PollId;
        uniGrid.Columns = "AnswerID, AnswerText, AnswerCount, AnswerEnabled";
        uniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
        uniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);
    }


    object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "answerenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            this.SelectedItemID = Convert.ToInt32(actionArgument);
            this.RaiseOnEdit();
        }
        else if (actionName == "delete")
        {
            if (!CheckPermissions("cms.polls", CMSAdminControl.PERMISSION_MODIFY))
            {
                return;
            }

            // Delete PollAnswerInfo object from database
            PollAnswerInfoProvider.DeletePollAnswerInfo(Convert.ToInt32(actionArgument));
            this.ReloadData(true);
        }
        else if (actionName == "moveup")
        {
            if (!CheckPermissions("cms.polls", CMSAdminControl.PERMISSION_MODIFY))
            {
                return;
            }

            // Move the answer up in order
            PollAnswerInfoProvider.MoveAnswerUp(this.PollId, Convert.ToInt32(actionArgument));
            this.ReloadData(true);
        }
        else if (actionName == "movedown")
        {
            if (!CheckPermissions("cms.polls", CMSAdminControl.PERMISSION_MODIFY))
            {
                return;
            }

            // Move the answer down in order
            PollAnswerInfoProvider.MoveAnswerDown(this.PollId, Convert.ToInt32(actionArgument));
            this.ReloadData(true);
        }
    }


    /// <summary>
    /// Forces unigrid to reload data.
    /// </summary>
    public override void ReloadData(bool forceReload)
    {
        uniGrid.Query = "polls.pollanswer.selectall";
        uniGrid.WhereCondition = "AnswerPollID=" + this.PollId;

        if (forceReload)
        {
            uniGrid.WhereClause = null;
            uniGrid.ResetFilter();
        }

        uniGrid.ReloadData();
    }

    #endregion
}
