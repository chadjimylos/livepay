using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSAdminControls_Debug_OutputLog : OutputLog
{
    protected string cmsVersion = null;
    protected int index = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.EnableViewState = false;
        this.Visible = true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.Visible = false;

        if (this.Log != null)
        {
            // Get the output
            string output = ValidationHelper.GetString(this.Log.Value, null);
            if (!String.IsNullOrEmpty(output))
            {
                this.Visible = true;

                if (LogStyle != "")
                {
                    this.ltlInfo.Text = "<div style=\"padding: 2px; font-weight: bold; background-color: #eecccc; border-bottom: solid 1px #ff0000;\">" + ResHelper.GetString("OutputLog.Info") + "</div>";
                }

                this.plcLog.Controls.Add(new LiteralControl("<div><textarea style=\"width: 100%; height: 530px;\">"));
                this.plcLog.Controls.Add(new LiteralControl(HTMLHelper.HTMLEncode(output)));
                this.plcLog.Controls.Add(new LiteralControl("</textarea><div>&nbsp;<strong>"));
                this.plcLog.Controls.Add(new LiteralControl(DataHelper.GetSizeString(output.Length)));
                this.plcLog.Controls.Add(new LiteralControl("</strong></div></div>"));

                this.TotalSize = output.Length;
            }
        }
    }
}
