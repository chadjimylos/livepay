<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BizForm_Edit_Security.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Bizform - Security"
    Inherits="CMSModules_BizForms_Tools_BizForm_Edit_Security" Theme="Default" %>

<%@ Register Src="~/CMSModules/Membership/FormControls/Roles/securityAddRoles.ascx" TagName="AddRoles" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblTitle" CssClass="SectionTitle" EnableViewState="false"
        Visible="true" />
    <table>
        <tr>
            <td colspan="2">
                <asp:RadioButton ID="radAllUsers" runat="server" GroupName="form" AutoPostBack="True" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:RadioButton ID="radOnlyRoles" runat="server" GroupName="form" AutoPostBack="True"
                    OnCheckedChanged="radOnlyRoles_CheckedChanged" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:CMSUpdatePanel runat="server" ID="pnlUpdate" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:ListBox ID="lstRoles" runat="server" CssClass="PermissionsListBox" SelectionMode="Multiple"
                            DataTextField="RoleName" DataValueField="RoleID" />
                    </ContentTemplate>
                </cms:CMSUpdatePanel>
            </td>
            <td style="vertical-align: top;">
                <table cellspacing="0" cellpadding="1">
                    <tr>
                        <td>
                            <cms:AddRoles runat="server" ID="addRoles" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <cms:CMSButton ID="btnRemoveRole" runat="server" Text="" CssClass="ContentButton"
                                OnClick="btnRemoveRole_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <cms:CMSButton ID="btnOk" runat="server" Text="" CssClass="SubmitButton" OnClick="btnOk_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
