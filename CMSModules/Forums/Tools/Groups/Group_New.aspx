<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Group_New.aspx.cs" Inherits="CMSModules_Forums_Tools_Groups_Group_New"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="default" %>

<%@ Register Src="~/CMSModules/Forums/Controls/Groups/GroupNew.ascx" TagName="GroupNew" TagPrefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:GroupNew ID="groupNewElem" runat="server" />
</asp:Content>