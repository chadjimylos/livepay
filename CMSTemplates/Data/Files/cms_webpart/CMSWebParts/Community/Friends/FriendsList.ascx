<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Community/Friends/FriendsList.ascx.cs" Inherits="CMSWebParts_Community_Friends_FriendsList" %>
<%@ Register Src="~/CMSModules/Friends/Controls/FriendsList.ascx" TagName="FriendsList"
    TagPrefix="cms" %>
<cms:FriendsList ID="lstFriends" runat="server" />