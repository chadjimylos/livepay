-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Proc_Newsletter_Newsletter_RemoveDependences
	@NewsletterID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
	-- Newsletter_Emails
    DELETE FROM Newsletter_Emails WHERE EmailNewsletterIssueID IN 
		(SELECT IssueID FROM Newsletter_NewsletterIssue WHERE IssueNewsletterID = @NewsletterID);
	-- Newsletter_NewsletterIssue
	DELETE FROM Newsletter_NewsletterIssue WHERE IssueNewsletterID = @NewsletterID;
	-- Newsletter_SubscriberNewsletter
	DELETE FROM Newsletter_SubscriberNewsletter WHERE NewsletterID = @NewsletterID;
	COMMIT TRANSACTION;
END
