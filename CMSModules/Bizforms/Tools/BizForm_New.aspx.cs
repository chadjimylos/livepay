using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.FormEngine;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.IDataConnectionLibrary;
using CMS.LicenseProvider;
using CMS.UIControls;

public partial class CMSModules_BizForms_Tools_BizForm_New : CMSBizFormPage
{
    private const string bizFormNamespace = "BizForm";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'CreateForm' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "CreateForm"))
        {
            RedirectToCMSDeskAccessDenied("cms.form", "CreateForm");
        }

        // Validator initializations
        rfvFormDisplayName.ErrorMessage = ResHelper.GetString("BizForm_Edit.ErrorEmptyFormDispalyName");
        rfvFormName.ErrorMessage = ResHelper.GetString("BizForm_Edit.ErrorEmptyFormName");
        rfvTableName.ErrorMessage = ResHelper.GetString("BizForm_Edit.ErrorEmptyFormTableName");

        // Control initializations						
        lblFormDisplayName.Text = ResHelper.GetString("BizForm_Edit.FormDisplayNameLabel");
        lblFormName.Text = ResHelper.GetString("BizForm_Edit.FormNameLabel");
        lblTableName.Text = ResHelper.GetString("BizForm_Edit.TableNameLabel");
        btnOk.Text = ResHelper.GetString("General.OK");

        // Page title control initialization	
        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("BizForm_Edit.ItemListLink");
        breadcrumbs[0, 1] = "~/CMSModules/BizForms/Tools/BizForm_List.aspx";
        breadcrumbs[0, 2] = "";
        breadcrumbs[1, 0] = ResHelper.GetString("BizForm_Edit.NewItemCaption"); ;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.TitleText = ResHelper.GetString("BizForm_New.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Form/new.png");
        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        this.CurrentMaster.Title.HelpTopicName = "new_bizform";
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        DataClassInfo dci = null;
        BizFormInfo bizFormObj = null;

        string errorMessage = new Validator().NotEmpty(txtFormDisplayName.Text, rfvFormDisplayName.ErrorMessage).
                                               NotEmpty(txtFormName.Text, rfvFormName.ErrorMessage).
                                               NotEmpty(txtTableName.Text, rfvTableName.ErrorMessage).
                                               IsIdentificator(txtFormName.Text, ResHelper.GetString("BizForm_Edit.ErrorFormNameInIdentificatorFormat")).
                                               IsIdentificator(txtTableName.Text, ResHelper.GetString("BizForm_Edit.ErrorFormTableNameInIdentificatorFormat")).Result;
        if (errorMessage == "")
        {
            string formCodeName = txtFormName.Text.Trim();

            // formName must be unique
            BizFormInfo testObj = BizFormInfoProvider.GetBizFormInfo(formCodeName, CMSContext.CurrentSiteName);

            // if formName value is unique														
            if (testObj == null)
            {
                string primaryKey = formCodeName + "ID";
                string formDisplayName = txtFormDisplayName.Text.Trim();
                string className = bizFormNamespace + "." + formCodeName;
                string tableName = txtTableName.Text.Trim();
                string errorOwner = "";

                if (!BizFormInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.BizForms, VersionActionEnum.Insert))
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("LicenseVersion.BizForm");
                }

                if (!lblError.Visible)
                {
                    try
                    {
                        // create new table in DB
                        TableManager.CreateTable(tableName, primaryKey);
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.Message;

                        // table with the same name already exists
                        lblError.Visible = true;
                        lblError.Text = errorMessage;
                    }

                    if (errorMessage == "")
                    {
                        // Change table owner                        
                        try
                        {
                            string owner = SqlHelperClass.GetDBSchema(CMSContext.CurrentSiteName);
                            if ((owner != "") && (owner.ToLower() != "dbo"))
                            {
                                TableManager.ChangeDBObjectOwner(tableName, owner);
                                tableName = owner + "." + tableName;
                            }
                        }
                        catch (Exception ex)
                        {
                            errorOwner = ex.Message;
                        }

                        // Convert default datetime to string in english format
                        string defaultDateTime = DateTime.Now.ToString(CultureHelper.EnglishCulture.DateTimeFormat);

                        try
                        {
                            // add FormInserted and FormUpdated columns to the table
                            TableManager.AddTableColumn(tableName, "FormInserted", "datetime", false, defaultDateTime);
                            TableManager.AddTableColumn(tableName, "FormUpdated", "datetime", false, defaultDateTime);
                        }
                        catch (Exception ex)
                        {
                            errorMessage = ex.Message;

                            // column wasnt added successfuly
                            lblError.Visible = true;
                            lblError.Text = errorMessage;

                            // delete created table
                            TableManager.DropTable(tableName);
                        }
                    }

                    if (errorMessage == "")
                    {
                        dci = BizFormInfoProvider.CreateBizFormDataClass(className, formDisplayName, tableName, primaryKey);

                        try
                        {
                            // create new bizform dataclass
                            dci.DisableLogging();
                            DataClassInfoProvider.SetDataClass(dci);
                            dci.EnableLogging();
                        }
                        catch (Exception ex)
                        {
                            errorMessage = ex.Message;

                            // class with the same name already exists
                            lblError.Visible = true;
                            lblError.Text = errorMessage;

                            // delete created table
                            TableManager.DropTable(tableName);
                        }
                    }

                    if (errorMessage == "")
                    {
                        // create new bizform
                        bizFormObj = new BizFormInfo();
                        bizFormObj.FormDisplayName = txtFormDisplayName.Text.Trim();
                        bizFormObj.FormName = txtFormName.Text.Trim();
                        bizFormObj.FormClassID = dci.ClassID;
                        bizFormObj.FormSiteID = CMSContext.CurrentSite.SiteID;
                        bizFormObj.FormEmailAttachUploadedDocs = true;
                        bizFormObj.FormItems = 0;
                        bizFormObj.FormClearAfterSave = false;

                        try
                        {
                            // create new bizform
                            BizFormInfoProvider.SetBizFormInfo(bizFormObj);

                            // generate basic queries
                            SqlGenerator.GenerateQuery(className, "selectall", SqlOperationTypeEnum.SelectAll, false);
                            SqlGenerator.GenerateQuery(className, "delete", SqlOperationTypeEnum.DeleteQuery, false);
                            SqlGenerator.GenerateQuery(className, "insert", SqlOperationTypeEnum.InsertQuery, true);
                            SqlGenerator.GenerateQuery(className, "update", SqlOperationTypeEnum.UpdateQuery, false);
                            SqlGenerator.GenerateQuery(className, "select", SqlOperationTypeEnum.SelectQuery, false);
                        }
                        catch (Exception ex)
                        {
                            errorMessage = ex.Message;

                            lblError.Visible = true;
                            lblError.Text = errorMessage;

                            // Delete created class (includes deleting its table)
                            if (dci != null)
                            {
                                SynchronizationTypeEnum oldValue = dci.LogSynchronization;

                                dci.DisableLogging();
                                DataClassInfoProvider.DeleteDataClass(dci);
                                dci.EnableLogging();
                            }
                        }

                        if (errorMessage == "")
                        {
                            UrlHelper.Redirect("BizForm_Frameset.aspx?formId=" + Convert.ToString(bizFormObj.FormID) + "&newform=1");
                        }
                    }
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("BizForm_Edit.FormNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    /// <summary>
    /// Returns new form field info of the datetime datatype indicating when form data were changed(inserted/updated).
    /// </summary>
    /// <param name="fielName">Field name.</param>
    /// <param name="fieldCaption">Field caption.</param>
    private FormFieldInfo GetInsertedOrUpdatedField(string fieldName, string fieldCaption)
    {
        FormFieldInfo ffi = null;

        if ((fieldName != null) && (fieldCaption != null) && (fieldCaption != String.Empty) && (fieldCaption != String.Empty))
        {
            ffi = new FormFieldInfo();

            // fill FormInfo object
            ffi.Name = fieldName;
            ffi.Caption = fieldCaption;
            ffi.DataType = FormFieldDataTypeEnum.DateTime;
            ffi.DefaultValue = String.Empty;
            ffi.Description = String.Empty;
            ffi.FieldType = FormFieldControlTypeEnum.CalendarControl;
            ffi.PrimaryKey = false;
            ffi.System = true;
            ffi.Visible = false;
            ffi.Size = 0;
            ffi.AllowEmpty = false;
        }

        return ffi;
    }
}
