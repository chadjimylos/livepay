using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSAdminControls_Debug_QueryLog : QueryLog
{
    #region "Variables"

    protected string cmsVersion = null;
    protected int index = 0;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        this.EnableViewState = false;
        this.Visible = true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.Visible = false;

        if (this.Log != null)
        {
            // Get the log table
            DataTable dt = this.Log.LogTable;
            if (!DataHelper.DataSourceIsEmpty(dt))
            {
                this.Visible = true;

                cmsVersion = ResHelper.GetString("Footer.Version") + "&nbsp;" + CMSContext.SYSTEM_VERSION + "&nbsp;" + ResHelper.GetString("Footer.Build") + "&nbsp;" + CMSContext.FullSystemVersion;

                gridQueries.Columns[1].HeaderText = ResHelper.GetString("QueryLog.QueryText");
                //gridQueries.Columns[2].HeaderText = ResHelper.GetString("QueryLog.QueryParameters");
                gridQueries.Columns[2].HeaderText = ResHelper.GetString("QueryLog.Context");
                gridQueries.Columns[3].HeaderText = ResHelper.GetString("QueryLog.QueryDuration");

                if (LogStyle != "")
                {
                    this.ltlInfo.Text = "<div style=\"padding: 2px; font-weight: bold; background-color: #eecccc; border-bottom: solid 1px #ff0000;\">" + ResHelper.GetString("QueryLog.Info") + "</div>";
                }

                gridQueries.DataSource = dt;
                gridQueries.DataBind();
            }
        }
    }


    /// <summary>
    /// Gets the query duration
    /// </summary>
    protected string GetDuration(object startTime, object endTime)
    {
        DateTime start = ValidationHelper.GetDateTime(startTime, DateTimeHelper.ZERO_TIME);
        DateTime end = ValidationHelper.GetDateTime(endTime, DateTimeHelper.ZERO_TIME);

        if ((start == DateTimeHelper.ZERO_TIME) || (end == DateTimeHelper.ZERO_TIME) || (end < start))
        {
            return "N/A";
        }

        TimeSpan duration = end.Subtract(start);
        this.TotalDuration += duration.TotalSeconds;

        return duration.TotalSeconds.ToString("F3");
    }


    protected object GetIndex(object resultsSize, object queryText)
    {
        if (queryText == DBNull.Value)
        {
            return null;
        }
        else
        {
            this.TotalSize += ValidationHelper.GetInteger(resultsSize, 0);

            return ++index;
        }
    }
}
