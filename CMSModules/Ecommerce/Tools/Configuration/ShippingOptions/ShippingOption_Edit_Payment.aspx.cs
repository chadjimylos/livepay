using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Tools_Configuration_ShippingOptions_ShippingOption_Edit_Payment : CMSEcommerceConfigurationPage
{
    protected int shippingOptionId = 0;
    protected string currentValues = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblAvialable.Text = ResHelper.GetString("com.shippingoption.payments");
        shippingOptionId = QueryHelper.GetInteger("shippingoptionid", 0);
        if (shippingOptionId > 0)
        {
            DataSet ds = PaymentOptionInfoProvider.GetShippingPayments(shippingOptionId, true, true);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "PaymentOptionID"));
            }

            if (!IsPostBack)
            {
                uniSelector.Value = currentValues;
            }
        }

        uniSelector.IconPath = GetObjectIconUrl("ecommerce.paymentoption", "object.png");
        uniSelector.OnSelectionChanged += uniSelector_OnSelectionChanged;
        uniSelector.WhereCondition = "PaymentOptionSiteID = (SELECT ShippingOptionSiteID FROM COM_ShippingOption WHERE ShippingOptionID = " + shippingOptionId + ")";
    }


    protected void uniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        SaveItems();
    }


    protected void SaveItems()
    {
        // Remove old items
        string newValues = ValidationHelper.GetString(uniSelector.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to user
                foreach (string item in newItems)
                {
                    int paymentId = ValidationHelper.GetInteger(item, 0);
                    PaymentOptionInfoProvider.RemovePaymentFromShipping(paymentId, shippingOptionId);
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to user
                foreach (string item in newItems)
                {
                    int paymentId = ValidationHelper.GetInteger(item, 0);
                    PaymentOptionInfoProvider.AddPaymentToShipping(paymentId, shippingOptionId);
                }
            }
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}
