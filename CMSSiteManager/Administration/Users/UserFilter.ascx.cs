using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.FormControls;
using CMS.CMSHelper;

public partial class CMSSiteManager_Administration_Users_UserFilter : CMSUserControl
{
    #region "Variables"

    bool showGroups = false;
    private const string pathToGroupselector = "~/CMSModules/Groups/FormControls/MembershipGroupSelector.ascx";
    private string mAlphabetSeparator = "";

    private int mSiteId = 0;

    private bool isAdvancedMode = false;
    private Hashtable alphabetHash = new Hashtable();
    FormEngineUserControl selectInGroups = null;
    FormEngineUserControl selectNotInGroups = null;    

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the site id for which the users should be filtered.
    /// </summary>
    public int SiteID
    {
        get
        {
            return this.mSiteId;
        }
        set
        {
            this.mSiteId = value;
        }
    }


    /// <summary>
    /// Gets or sets the visibility of alphabet panel
    /// </summary>
    public bool AlphabetVisible
    {
        get
        {
            return this.pnlAlphabet.Visible;
        }
        set
        {
            this.pnlAlphabet.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets the alphabet separator
    /// </summary>
    public string AlphabetSeparator
    {
        get
        {
            return this.mAlphabetSeparator;
        }
        set
        {
            this.mAlphabetSeparator = value;
        }
    }


    /// <summary>
    /// Gets the where condition created using filtered parameters.
    /// </summary>
    public string WhereCondition
    {
        get
        {
            return GenerateWhereCondition();
        }
    }

    #endregion


    #region "Page methods"

    protected override void OnInit(EventArgs e)
    {        
        if (File.Exists(HttpContext.Current.Request.MapPath(ResolveUrl(pathToGroupselector))))
        {
            Control ctrl = this.LoadControl(pathToGroupselector);
            if (ctrl != null)
            {
                selectInGroups = ctrl as FormEngineUserControl;
                ctrl.ID = "selGroups"; 
                ctrl = this.LoadControl(pathToGroupselector);
                selectNotInGroups = ctrl as FormEngineUserControl;
                ctrl.ID = "selNoGroups";

                this.plcGroups.Visible = true;
                plcSelectInGroups.Controls.Add(selectInGroups);
                plcSelectNotInGroups.Controls.Add(selectNotInGroups);
                
                this.selectNotInGroups.SetValue("UseFriendlyMode", true);
                this.selectInGroups.IsLiveSite = false;  
                this.selectInGroups.SetValue("UseFriendlyMode", true);
                this.selectNotInGroups.IsLiveSite = false;

            }            
        }               

        base.OnInit(e);
    }   


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check "read" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Read"))
        {
            RedirectToAccessDenied("CMS.Users", "Read");
        }       

        InitializeForm();

        // Show alphaphet filter if enabled
        if (this.pnlAlphabet.Visible)
        {
            this.pnlAlphabet.Controls.Add(CreateAlphabetTable());
        }

        // Show correct filter panel
        EnsureFilterMode();
        pnlAdvancedFilter.Visible = isAdvancedMode;
        pnlSimpleFilter.Visible = !isAdvancedMode;

        // Show group filter only if enabled
        if (this.mSiteId > 0)
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(this.mSiteId);
            if ((si != null) && isAdvancedMode)
            {
                showGroups = ModuleCommands.CommunitySiteHasGroup(si.SiteID);
            }
        }          
        
        // Setup role selector
        this.selectNotInRole.SiteID = this.mSiteId;
        this.selectRoleElem.SiteID = this.mSiteId;
        this.selectRoleElem.CurrentSelector.ResourcePrefix = "addroles";
        this.selectNotInRole.CurrentSelector.ResourcePrefix = "addroles";
        this.selectRoleElem.UseFriendlyMode = true;
        this.selectNotInRole.UseFriendlyMode = true;

        // Setup groups selectors
        plcGroups.Visible = showGroups;
        if (selectInGroups != null)
        {
            selectInGroups.StopProcessing = !showGroups;
            this.selectInGroups.FormControlParameter = this.mSiteId;
        }

        if (selectNotInGroups != null)
        {
            selectNotInGroups.StopProcessing = !showGroups;
            this.selectNotInGroups.FormControlParameter = this.mSiteId;
        }        
    }

    #endregion


    #region "UI methods"

    /// <summary>
    /// Initializes the layout of the form.
    /// </summary>
    private void InitializeForm()
    {
        // General UI
        this.btnSimpleSearch.Text = ResHelper.GetString("General.Search");
        this.btnAdvancedSearch.Text = ResHelper.GetString("General.Search");
        this.lnkShowAdvancedFilter.Text = ResHelper.GetString("user.filter.showadvanced");
        this.imgShowAdvancedFilter.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/SortDown.png");
        this.lnkShowSimpleFilter.Text = ResHelper.GetString("user.filter.showsimple");
        this.imgShowSimpleFilter.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/SortUp.png");
        this.pnlSimpleFilter.Visible = !isAdvancedMode;
        this.pnlAdvancedFilter.Visible = isAdvancedMode;

        // Initialize advanced filter dropdownlists
        if (!RequestHelper.IsPostBack())
        {
            this.drpUserName.Items.Add(new ListItem("LIKE", "LIKE"));
            this.drpUserName.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
            this.drpUserName.Items.Add(new ListItem("=", "="));
            this.drpUserName.Items.Add(new ListItem("<>", "<>"));

            this.drpFullName.Items.Add(new ListItem("LIKE", "LIKE"));
            this.drpFullName.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
            this.drpFullName.Items.Add(new ListItem("=", "="));
            this.drpFullName.Items.Add(new ListItem("<>", "<>"));

            this.drpEmail.Items.Add(new ListItem("LIKE", "LIKE"));
            this.drpEmail.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
            this.drpEmail.Items.Add(new ListItem("=", "="));
            this.drpEmail.Items.Add(new ListItem("<>", "<>"));

            this.drpNickName.Items.Add(new ListItem("LIKE", "LIKE"));
            this.drpNickName.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
            this.drpNickName.Items.Add(new ListItem("=", "="));
            this.drpNickName.Items.Add(new ListItem("<>", "<>"));

            this.drpTypeSelectInRoles.Items.Add(new ListItem(ResHelper.GetString("General.All"), "ALL"));
            this.drpTypeSelectInRoles.Items.Add(new ListItem(ResHelper.GetString("General.Any"), "ANY"));

            this.drpTypeSelectNotInRoles.Items.Add(new ListItem(ResHelper.GetString("General.All"), "ALL"));
            this.drpTypeSelectNotInRoles.Items.Add(new ListItem(ResHelper.GetString("General.Any"), "ANY"));

            this.drpTypeSelectInGroups.Items.Add(new ListItem(ResHelper.GetString("General.All"), "ALL"));
            this.drpTypeSelectInGroups.Items.Add(new ListItem(ResHelper.GetString("General.Any"), "ANY"));

            this.drpTypeSelectNotInGroups.Items.Add(new ListItem(ResHelper.GetString("General.All"), "ALL"));
            this.drpTypeSelectNotInGroups.Items.Add(new ListItem(ResHelper.GetString("General.Any"), "ANY"));
        }

        // Labels
        this.lblFullName.Text = ResHelper.GetString("general.FullName") + ResHelper.Colon;
        this.lblNickName.Text = ResHelper.GetString("userlist.NickName") + ResHelper.Colon;
        this.lblEmail.Text = ResHelper.GetString("userlist.Email") + ResHelper.Colon;
        this.lblInRoles.Text = ResHelper.GetString("userlist.InRoles") + ResHelper.Colon;
        this.lblNotInRoles.Text = ResHelper.GetString("userlist.NotInRoles") + ResHelper.Colon;
        this.lblInGroups.Text = ResHelper.GetString("userlist.InGroups") + ResHelper.Colon;
        this.lblNotInGroups.Text = ResHelper.GetString("userlist.NotInGroups") + ResHelper.Colon;
    }


    /// <summary>
    /// Creates table element with alphabet.
    /// </summary>
    private Table CreateAlphabetTable()
    {
        // Register javascript for alphabet filtering
        string postbackScript = Page.ClientScript.GetPostBackEventReference(this, null);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "alphabetSelect", ScriptHelper.GetScript(
            @"function SetAlphabetLetter(letter) {
                var hiddenElem = document.getElementById('" + hdnAlpha.ClientID + @"');
                if (hiddenElem != null) {
                    hiddenElem.value = letter;
                    " + postbackScript + @";
                }
                return false;
              }"));

        Table table = new Table();
        table.EnableViewState = false;
        table.Attributes.Add("style", "width: 100%");
        table.Rows.Add(new TableRow());

        TableCell cell = new TableCell();
        HyperLink link = new HyperLink();

        // Create "ALL" link
        link.NavigateUrl = "javascript:SetAlphabetLetter('ALL');";
        link.Text = ResHelper.GetString("general.all");

        cell.Controls.Add(link);
        table.Rows[0].Cells.Add(cell);
        alphabetHash["ALL"] = link;

        // Create alphabet links
        for (char c = 'A'; c <= 'Z'; c++)
        {
            // Add separator            
            //TableCell cellSep = new TableCell();
            //cellSep.Controls.Add(new LiteralControl(this.mAlphabetSeparator));
            //table.Rows[0].Cells.Add(cellSep);

            // Add link
            cell = new TableCell();
            link = new HyperLink();
            table.Rows[0].Cells.Add(cell);

            link.NavigateUrl = "javascript:SetAlphabetLetter('" + c + "');";

            link.Text = c.ToString();
            cell.Controls.Add(link);
            table.Rows[0].Cells.Add(cell);

            // Add to hashtable
            alphabetHash[c.ToString()] = link;
        }

        // If something is already selected, hightlight the letter, otherwise all
        string key = String.IsNullOrEmpty(hdnAlpha.Value) ? "ALL" : hdnAlpha.Value;
        link = alphabetHash[key] as HyperLink;
        if (link != null)
        {
            link.CssClass = "ActiveLink";
        }

        return table;
    }


    /// <summary>
    /// Ensures correct filter mode flag if filter mode was just changed
    /// </summary>
    private void EnsureFilterMode()
    {
        if (UrlHelper.IsPostback())
        {
            // Get current event target
            string uniqieId = ValidationHelper.GetString(Request.Params["__EVENTTARGET"], String.Empty);
            uniqieId = uniqieId.Replace("$", "_");

            // If postback was fired by mode switch, update isAdvancedMode variable
            if (uniqieId == lnkShowAdvancedFilter.ClientID)
            {
                isAdvancedMode = true;
            }
            else if (uniqieId == lnkShowSimpleFilter.ClientID)
            {
                isAdvancedMode = false;
            }
            else
            {
                isAdvancedMode = ValidationHelper.GetBoolean(ViewState["IsAdvancedMode"], false);
            }
        }
    }


    /// <summary>
    /// Sets the advanced mode
    /// </summary>
    protected void lnkShowAdvancedFilter_Click(object sender, EventArgs e)
    {
        isAdvancedMode = true;
        ViewState["IsAdvancedMode"] = isAdvancedMode;
        this.pnlSimpleFilter.Visible = !isAdvancedMode;
        this.pnlAdvancedFilter.Visible = isAdvancedMode;
    }


    /// <summary>
    /// Sets the simple mode
    /// </summary>
    protected void lnkShowSimpleFilter_Click(object sender, EventArgs e)
    {
        isAdvancedMode = false;
        ViewState["IsAdvancedMode"] = isAdvancedMode;
        this.pnlSimpleFilter.Visible = !isAdvancedMode;
        this.pnlAdvancedFilter.Visible = isAdvancedMode;
    }

    #endregion


    #region "Search methods - where condition"

    /// <summary>
    /// Generates complete filter where condition
    /// </summary>    
    private string GenerateWhereCondition()
    {
        // Get mode from viewstate
        EnsureFilterMode();

        string whereCond = null;

        // Create first where condition depending on mode
        if (isAdvancedMode)
        {
            whereCond = AdvancedSearch();
        }
        else
        {
            whereCond = SimpleSearch();
        }

        // Append site condition if siteid given.
        if (this.mSiteId > 0)
        {
            // Get site related condition
            if (this.mSiteId != 0)
            {
                whereCond += (String.IsNullOrEmpty(whereCond) ? "" : " AND ") + "(UserID IN (SELECT UserID FROM CMS_UserSite WHERE SiteID=" + this.mSiteId + "))";
            }
        }

        return whereCond ;
    }

    /// <summary>
    /// Generates where condition for advanced filter
    /// </summary>
    public string AdvancedSearch()
    {
        string whereCond = String.Empty;
        // Get condition parts
        string usernameWhere = GetWherePart("UserName", this.txtUserName.Text, this.drpUserName.SelectedValue);
        string fullnameWhere = GetWherePart("FullName", this.txtFullName.Text, this.drpFullName.SelectedValue);
        string emailWhere = GetWherePart("Email", this.txtEmail.Text, this.drpEmail.SelectedValue);
        string nicknameWhere = GetWherePart("UserNickName", this.txtNickName.Text, this.drpNickName.SelectedValue);
        string roleWhere = string.Empty;
        string roleWhereNot = string.Empty;
        
        if (CMSContext.CurrentUser.IsInRole("MerchantsSupport", "LivePay"))
        {
            roleWhere = GetMultipleSelectorCondition("ALL", "(UserID IN (SELECT UserID FROM CMS_UserRole WHERE RoleID IN (SELECT RoleID FROM CMS_Role WHERE RoleName = N'##VALUE##')))", "LivePayMerchants", 1);
            drpTypeSelectInRoles.Enabled = false;
            selectRoleElem.Enabled = false;
            drpTypeSelectNotInRoles.Enabled = false;
            selectNotInRole.Enabled = false;
            
        }
        else if (CMSContext.CurrentUser.IsInRole("eBusinessOps", "LivePay"))
        {
            roleWhere = GetMultipleSelectorCondition("ALL", "(UserID IN (SELECT UserID FROM CMS_UserRole WHERE RoleID IN (SELECT RoleID FROM CMS_Role WHERE RoleName = N'##VALUE##')))", "LivePayUsers", 1);
            drpTypeSelectInRoles.Enabled = false;
            selectRoleElem.Enabled = false;
            drpTypeSelectNotInRoles.Enabled = false;
            selectNotInRole.Enabled = false;
        }
        else
        {
            roleWhere = GetMultipleSelectorCondition(drpTypeSelectInRoles.SelectedValue, "(UserID IN (SELECT UserID FROM CMS_UserRole WHERE RoleID IN (SELECT RoleID FROM CMS_Role WHERE RoleName = N'##VALUE##')))", selectRoleElem.Value.ToString().Trim(), 1);
            roleWhereNot = GetMultipleSelectorCondition(drpTypeSelectNotInRoles.SelectedValue, "(UserID NOT IN (SELECT UserID FROM CMS_UserRole WHERE RoleID IN (SELECT RoleID FROM CMS_Role WHERE RoleName = N'##VALUE##')))", selectNotInRole.Value.ToString().Trim(), 2);
        }

        string groupWhere = (showGroups) ? GetMultipleSelectorCondition(drpTypeSelectInGroups.SelectedValue, "(UserID IN (SELECT MemberUserID FROM Community_GroupMember WHERE MemberGroupID IN (SELECT GroupID FROM Community_Group WHERE GroupName = N'##VALUE##')))", selectInGroups.Value.ToString().Trim(), 0) : "";
        string groupWhereNot = (showGroups) ? GetMultipleSelectorCondition(drpTypeSelectNotInGroups.SelectedValue, "(UserID NOT IN (SELECT MemberUserID FROM Community_GroupMember WHERE MemberGroupID IN (SELECT GroupID FROM Community_Group WHERE GroupName = N'##VALUE##')))", selectNotInGroups.Value.ToString().Trim(), 20) : "";

        // Join where conditions
        if ((roleWhere != "") && (roleWhereNot != ""))
        {
            roleWhere = roleWhere + " AND " + roleWhereNot;
        }
        else
        {
            roleWhere = roleWhere + roleWhereNot;
        }


        // Final where condition
        whereCond = (String.IsNullOrEmpty(usernameWhere) ? "" : " AND " + usernameWhere) +
                               (String.IsNullOrEmpty(fullnameWhere) ? "" : " AND " + fullnameWhere) +
                               (String.IsNullOrEmpty(emailWhere) ? "" : " AND " + emailWhere) +
                               (String.IsNullOrEmpty(nicknameWhere) ? "" : " AND " + nicknameWhere) +
                               (String.IsNullOrEmpty(roleWhere) ? "" : " AND " + roleWhere) +
                               (String.IsNullOrEmpty(roleWhereNot) ? "" : " AND " + roleWhereNot) +
                               (String.IsNullOrEmpty(groupWhere) ? "" : " AND " + groupWhere) +
                               (String.IsNullOrEmpty(groupWhereNot) ? "" : " AND " + groupWhereNot);

        // Remove first AND
        if (whereCond != "")
        {
            whereCond = whereCond.Substring(5);
        }

        // Starting letter for username
        string firstLetter = hdnAlpha.Value;
        if (firstLetter != "" && firstLetter != "ALL")
        {
            whereCond += (String.IsNullOrEmpty(whereCond) ? "" : " AND ") + "(UserName LIKE N'" + firstLetter + "%')";
        }

        return whereCond;
    }


    /// <summary>
    /// Returns where condition for specialized role and group conditions.
    /// </summary>
    /// <param name="op">Condition to use (ANY/ALL)</param>
    /// <param name="baseQuery">Condition query text vith ##VALUE## macro</param>
    /// <param name="valuesStr">Values separated with semicolon</param>
    /// <param name="roleType">0 - Not role, 1 - In Role, 2 - Not in Role</param>
    private string GetMultipleSelectorCondition(string op, string baseQuery, string valuesStr, int roleType)
    {
        string retval = String.Empty;
        if (!String.IsNullOrEmpty(valuesStr))
        {
            string innerOp = " AND ";
            if (op == "ANY")
            {
                innerOp = " OR  ";
            }

            // Is in role
            if (roleType == 1)
            {
                string role = ";" + valuesStr + ";";

                if (role.Contains(";" + RoleInfoProvider.EVERYONE + ";"))
                {
                    if (op == "ANY")
                    {
                        return "1=1";
                    }
                    else
                    {
                    }
                }


            }

            string[] values = valuesStr.Replace("'", "''").Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string val in values)
            {
                if (val == RoleInfoProvider.EVERYONE)
                {
                    if (roleType == 1)
                    {
                        if (op == "ANY")
                        {
                            return "1=1";
                        }
                        else
                        {
                            continue;
                        }
                    }


                    if (roleType == 2)
                    {
                        if (op == "ANY")
                        {
                            return "1=2";
                        }
                        else
                        {
                            if (values.Length == 1)
                            {
                                return "1=2";
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                }
                retval += innerOp + baseQuery.Replace("##VALUE##", val);
            }

            // Remove first condition
            if (!String.IsNullOrEmpty(retval))
            {
                retval = "(" + retval.Substring(5) + ")";
            }
        }
        return retval;
    }


    /// <summary>
    /// Returns part of the wherecondition for given column.
    /// </summary>
    /// <param name="column">Column name</param>
    /// <param name="value">Value to be searched</param>
    /// <param name="op">Operator to condition</param>
    private string GetWherePart(string column, string value, string op)
    {
        // Avoid SQL Injenction
        string tempVal = value.Trim().Replace("'", "''");

        // No condition
        if (tempVal == "")
        {
            return null;
        }

        string where = "(" + column + " {0} N'{1}')";

        // Support for exact phrase search
        if (op.Contains("LIKE"))
        {
            tempVal = "%" + tempVal + "%";
        }

        // Get final where condition
        where = String.Format(where, op, tempVal);

        return where;
    }


    /// <summary>
    /// Generates where condition for simple filter
    /// </summary>    
    public string SimpleSearch()
    {
        string where = String.Empty;
        string searchExpression = null;
        string queryOperator = "LIKE";
        Boolean isMerchantSupport = CMSContext.CurrentUser.IsInRole("MerchantsSupport", "LivePay");
        Boolean iseBusinessOps = CMSContext.CurrentUser.IsInRole("eBusinessOps", "LivePay");

        if (txtSearch.Text != String.Empty)
        {
            // Create skeleton of where condition (ensure also site and starting letter)
            where = "((UserName {0} N'{1}') OR (Email {0} N'{1}') OR (FullName {0} N'{1}') OR (UserNickName {0} N'{1}')) ";

            if(isMerchantSupport || isMerchantSupport){
                where += " AND ";
            }

            // Avoid SQL Injenction
            searchExpression = txtSearch.Text.Trim().Replace("'", "''");

            // Choose the operator (if surrounded with quotes use '=' operator instead of LIKE)            
            if (searchExpression.StartsWith("\"") && searchExpression.EndsWith("\""))
            {
                queryOperator = "=";

                // Remove quotes
                searchExpression = searchExpression.Substring(1, searchExpression.Length - 2);
            }
            else
            {
                searchExpression = "%" + searchExpression + "%";
            }
        }

        // Starting letter for username
        string firstLetter = hdnAlpha.Value;
        if (firstLetter != "" && firstLetter != "ALL")
        {
            if (!String.IsNullOrEmpty(where))
            {
                where += " AND ";
            }
            where += "(UserName LIKE N'" + firstLetter.Replace("'", "''") + "%')";
            if(isMerchantSupport || isMerchantSupport){
                where += " AND ";
            }
        }


        if (isMerchantSupport)
        {
            where += GetMultipleSelectorCondition("ALL", " (UserID IN (SELECT UserID FROM CMS_UserRole WHERE RoleID IN (SELECT RoleID FROM CMS_Role WHERE RoleName = N'##VALUE##'))) ", "LivePayMerchants", 1);
        }
        else if (isMerchantSupport)
        {
            where = GetMultipleSelectorCondition("ALL", " (UserID IN (SELECT UserID FROM CMS_UserRole WHERE RoleID IN (SELECT RoleID FROM CMS_Role WHERE RoleName = N'##VALUE##')))", "LivePayUsers", 1);
        }

        // Get final where condition
        return String.Format(where, queryOperator, searchExpression);
    }

    #endregion
}
