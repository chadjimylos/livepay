CREATE TABLE [Staging_Synchronization] (
		[SynchronizationID]               int NOT NULL IDENTITY(1, 1),
		[SynchronizationTaskID]           int NOT NULL,
		[SynchronizationServerID]         int NOT NULL,
		[SynchronizationLastRun]          datetime NULL,
		[SynchronizationErrorMessage]     nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [Staging_Synchronization]
	ADD
	CONSTRAINT [PK_Staging_Synchronization]
	PRIMARY KEY
	([SynchronizationID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Staging_Synchronization]') IS NOT NULL
	AND OBJECT_ID(N'[Staging_Server]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Staging_Synchronization_SynchronizationServerID_Staging_Server]') IS NULL
BEGIN
		ALTER TABLE [Staging_Synchronization]
			ADD CONSTRAINT [FK_Staging_Synchronization_SynchronizationServerID_Staging_Server]
			FOREIGN KEY ([SynchronizationServerID]) REFERENCES [Staging_Server] ([ServerID])
END
IF OBJECT_ID(N'[Staging_Synchronization]') IS NOT NULL
	AND OBJECT_ID(N'[Staging_Task]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Staging_Synchronization_SynchronizationTaskID_Staging_Task]') IS NULL
BEGIN
		ALTER TABLE [Staging_Synchronization]
			ADD CONSTRAINT [FK_Staging_Synchronization_SynchronizationTaskID_Staging_Task]
			FOREIGN KEY ([SynchronizationTaskID]) REFERENCES [Staging_Task] ([TaskID])
END
CREATE INDEX [IX_Staging_Synchronization_SynchronizationServerID]
	ON [Staging_Synchronization] ([SynchronizationServerID])
	ON [PRIMARY]
CREATE INDEX [IX_Staging_Synchronization_SynchronizationTaskID]
	ON [Staging_Synchronization] ([SynchronizationTaskID])
	ON [PRIMARY]
