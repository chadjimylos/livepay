<%@ Control Language="C#" AutoEventWireup="true" CodeFile="media_library.ascx.cs"
    Inherits="CMSModules_ImportExport_Controls_Export_media_library" %>

<script type="text/javascript">
    //<![CDATA[
    function medCheckChange() {
        medChck2.disabled = !medChck1.checked;
    }

    function medInitCheckboxes() {
        if (!medChck1.checked) {
            medChck2.disabled = true;
        }
    }
    //]]>
</script>

<asp:Panel runat="server" ID="pnlCheck" CssClass="WizardHeaderLine" BackColor="transparent">
    <asp:Label ID="lblInfo" runat="Server" EnableViewState="false" />
    <br />
    <br />
    <div>
        <asp:CheckBox ID="chkFiles" runat="server" />
    </div>
    <div>
        <asp:CheckBox ID="chkPhysicalFiles" runat="server" />
    </div>
</asp:Panel>
<asp:Literal ID="ltlScript" EnableViewState="false" runat="Server" />