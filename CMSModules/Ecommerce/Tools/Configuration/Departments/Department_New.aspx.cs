using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_Departments_Department_New : CMSEcommerceConfigurationPage
{
    protected int departmentId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        // field validator error messages initialization
        rfvDisplayName.ErrorMessage = ResHelper.GetString("Department_Edit.errorEmptyDisplayName");
        rfvCodeName.ErrorMessage = ResHelper.GetString("Department_Edit.errorEmptyCodeName");

        // control initializations				
        lblDepartmentName.Text = ResHelper.GetString("Department_Edit.DepartmentNameLabel");
        lblDepartmentDisplayName.Text = ResHelper.GetString("Department_Edit.DepartmentDisplayNameLabel");
        
        btnOk.Text = ResHelper.GetString("General.OK");

        // initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Department_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/Departments/Department_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = ResHelper.GetString("Department_Edit.NewItemCaption");
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Department_Edit.NewItemCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_Department/new.png");

        this.CurrentMaster.Title.HelpTopicName = "departments_new";
        this.CurrentMaster.Title.HelpName = "helpTopic";

    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        // check input values from textboxes and other contrlos - new Validator()
        string errorMessage = new Validator()
            .NotEmpty(txtDepartmentDisplayName.Text, ResHelper.GetString("Department_Edit.errorEmptyDisplayName"))
            .NotEmpty(txtDepartmentName.Text, ResHelper.GetString("Department_Edit.errorEmptyCodeName")).Result;

        if (!ValidationHelper.IsCodeName(txtDepartmentName.Text))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

        if (errorMessage == "")
        {
            // departmentName must to be unique
            DepartmentInfo departmentObj = DepartmentInfoProvider.GetDepartmentInfo(txtDepartmentName.Text.Trim());

            // if departmentName value is unique														
            if ((departmentObj == null) || (departmentObj.DepartmentID == departmentId))
            {
                departmentObj = new DepartmentInfo();

                departmentObj.DepartmentID = departmentId;
                departmentObj.DepartmentName = txtDepartmentName.Text.Trim();
                departmentObj.DepartmentDisplayName = txtDepartmentDisplayName.Text.Trim();
                
                DepartmentInfoProvider.SetDepartmentInfo(departmentObj);

                UrlHelper.Redirect("Department_Frameset.aspx?departmentId=" + Convert.ToString(departmentObj.DepartmentID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Department_Edit.DepartmentNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
