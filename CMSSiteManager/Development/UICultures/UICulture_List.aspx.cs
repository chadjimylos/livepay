using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_UICultures_UICulture_List : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitializeMasterPage();

        UniGridUICultures.OnAction += new OnActionEventHandler(UniGridUICultures_OnAction);
        UniGridUICultures.OrderBy = "UICultureName";
        UniGridUICultures.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that threw event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGridUICultures_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("UICulture_Frameset.aspx?UIcultureID=" + actionArgument.ToString());
        }
        else if (actionName == "delete")
        {
            UICultureInfo culture = UICultureInfoProvider.GetUICultureInfo(ValidationHelper.GetInteger(actionArgument, 0));
            if (culture != null)
            {
                if (culture.UICultureCode.ToLower() != CultureHelper.DefaultUICulture.ToLower())
                {
                    // Delete UI culture object if it is not the default one
                    UICultureInfoProvider.DeleteUICultureInfo(culture);
                }
                else
                {
                    // Display error message
                    lblError.Text = string.Format(ResHelper.GetString("Development-UICulture_List.DeleteError"), culture.UICultureName);
                    lblError.Visible = true;
                }
            }
        }
    }


    protected void InitializeMasterPage()
    {
        // Set title
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Development-UICulture_List.Title");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_UICulture/object.png");

        // Set actions
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("Development-UICulture_List.NewUICulture");
        actions[0, 3] = "~/CMSSiteManager/Development/UICultures/UICulture_New.aspx";
        actions[0, 5] = GetImageUrl("Objects/CMS_UICulture/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;

        // Set help
        this.CurrentMaster.Title.HelpTopicName = "ui_cultures_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }
}


  