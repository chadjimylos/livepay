﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.Controls

Partial Class CMSTemplates_AegeanPower_UCOnlinePay
    Inherits CMSUserControl

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If

        If Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()

        End If
    End Sub


    Public Overloads Sub ReloadData()
        'Bind()
        btnOk.Text = ResHelper.LocalizeString("{$=Ολοκλήρωση πληρωμής|en-us=Ολοκλήρωση πληρωμής$}")
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
        btnOk.Text = ResHelper.LocalizeString("{$=Ολοκλήρωση πληρωμής|en-us=Ολοκλήρωση πληρωμής$}")
        ' Bind()
    End Sub

    Private Sub Bind()
        'btnOk.Text = ResHelper.LocalizeString("{$=Ολοκλήρωση πληρωμής|en-us=Ολοκλήρωση πληρωμής$}")
        'If txtboxOnlineNumber.Text = String.Empty OrElse txtboxOnlineNumber.Text.Length < 16 Then
        '    ErrorOnline.Visible = True
        'Else
        '    ErrorOnline.Visible = False
        'End If
        'If txtboxAmmount.Text = String.Empty Then
        '    ErrorAmmount.Visible = True
        'Else
        '    ErrorAmmount.Visible = False
        'End If

    End Sub

    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        'Bind()
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub
End Class
