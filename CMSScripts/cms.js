﻿// Bug fix for "__pendingCallbacks is not defined"
function WebForm_CallbackComplete_SyncFixed() {
    if (window.__pendingCallbacks) {
        for (var i = 0; i < window.__pendingCallbacks.length; i++) {
            callbackObject = window.__pendingCallbacks[i];
            if (callbackObject && callbackObject.xmlRequest && (callbackObject.xmlRequest.readyState == 4)) {
                if (!window.__pendingCallbacks[i].async) {
                    __synchronousCallBackIndex = -1;
                }
                window.__pendingCallbacks[i] = null;

                var callbackFrameID = "__CALLBACKFRAME" + i;
                var xmlRequestFrame = document.getElementById(callbackFrameID);
                if (xmlRequestFrame) {
                    xmlRequestFrame.parentNode.removeChild(xmlRequestFrame);
                }

                WebForm_ExecuteCallback(callbackObject);
            }
        }
    }
}

// Page completeness checking on postback
var notLoadedText = 'The page is not yet fully loaded, please wait until it loads.';
var originalUnload;

function __doPostBackWithCheck(eventTarget, eventArgument) {
    var editorsLoaded = true;
    if ((typeof (FCKeditorAPI) != 'undefined') && (FCKeditorAPI.__Instances != null)) {
        for (var name in FCKeditorAPI.__Instances) {
            var oEditor = FCKeditorAPI.__Instances[name];
            if (oEditor.Status == FCK_STATUS_NOTLOADED) {
                editorsLoaded = false;
                break;
            }
        }
    }

    if (document.pageLoaded && editorsLoaded) {
        originalUnload = document.onunload;

        originalPostback(eventTarget, eventArgument);
    }
    else {
        alert(notLoadedText);
    }
}

function __OnUnload() {
    document.pageLoaded = false;

    originalUnload();
}


// FCK editor enhancements
if (typeof (disabledFCKEditors) == 'undefined') {
    disabledFCKEditors = new Array;
}

function FCKeditor_OnComplete(editorInstance) {
    if (disabledFCKEditors[editorInstance.Name]) {
        DisableFCKeditor(editorInstance);
    }
    editorInstance.ResetIsDirty();
}

function DisableFCKeditor(editorInstance) {
    if ((!document.all && editorInstance.EditorDocument.designMode.toLowerCase() != "off") || (document.all && editorInstance.EditorDocument.body.disabled == false)) {
        editorInstance.Disabled = true;

        // disable the editArea
        if (document.all) {
            editorInstance.EditorWindow.parent.FCK.ToolbarSet.Disable();
            editorInstance.EditorDocument.body.disabled = true;
        }
        else {
            editorInstance.EditorDocument.designMode = "off";
            editorInstance.EditorWindow.parent.FCK.ToolbarSet.Disable();
            editorInstance.EditorWindow.parent.FCKConfig.BrowserContextMenu = true;
            buttonRefreshStateClone = editorInstance.EditorWindow.parent.FCKToolbarButton.prototype.RefreshState;
            specialComboRefreshStateClone = editorInstance.EditorWindow.parent.FCKToolbarSpecialCombo.prototype.RefreshState;
            editorInstance.EditorWindow.parent.FCKToolbarButton.prototype.RefreshState = function() { return false; };
            editorInstance.EditorWindow.parent.FCKToolbarSpecialCombo.prototype.RefreshState = function() { return false; };
        }
    }
}

if (typeof (Sys) != "undefined" && typeof (Sys.Application) != "undefined") {
    Sys.Application.notifyScriptLoaded();
}
