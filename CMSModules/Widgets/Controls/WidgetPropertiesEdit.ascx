<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WidgetPropertiesEdit.ascx.cs"
    Inherits="CMSModules_Widgets_Controls_WidgetPropertiesEdit" %>
<%@ Register Src="~/CMSModules/Widgets/Controls/WidgetPropertiesFieldEditor.ascx" TagName="WidgetPropertiesFieldEditor"
    TagPrefix="cms" %>
    
<cms:WidgetPropertiesFieldEditor ID="widgetFieldEditor" runat="server" />
