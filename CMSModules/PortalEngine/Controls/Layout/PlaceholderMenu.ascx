<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PlaceholderMenu.ascx.cs"
    Inherits="CMSModules_PortalEngine_Controls_Layout_PlaceholderMenu" %>
<%@ Register TagPrefix="cms" Namespace="CMS.UIControls" Assembly="CMS.UIControls" %>
<asp:Panel runat="server" ID="pnlPlaceholderMenu" CssClass="PortalContextMenu PlaceholderContextMenu">
    <cms:UIPlaceHolder ID="pnlUILayout" runat="server" ModuleName="CMS.Content" ElementName="Design.EditLayout">
        <asp:Panel runat="server" ID="pnlLayout" CssClass="Item">
            <asp:Panel runat="server" ID="pnlLayoutPadding" CssClass="ItemPadding">
                <asp:Image runat="server" ID="imgLayout" CssClass="Icon" EnableViewState="false" />&nbsp;
                <asp:Label runat="server" ID="lblLayout" CssClass="Name" EnableViewState="false"
                    Text="Edit layout" />
            </asp:Panel>
        </asp:Panel>
    </cms:UIPlaceHolder>
    <cms:UIPlaceHolder ID="pnlUITemplate" runat="server" ModuleName="CMS.Content" ElementName="Design.EditTemplateProperties">
        <asp:Panel runat="server" ID="pnlTemplate" CssClass="Item">
            <asp:Panel runat="server" ID="pnlTemplatePadding" CssClass="ItemPadding">
                <asp:Image runat="server" ID="imgTemplate" CssClass="Icon" EnableViewState="false" />&nbsp;
                <asp:Label runat="server" ID="lblTemplate" CssClass="Name" EnableViewState="false"
                    Text="Edit template" />
            </asp:Panel>
        </asp:Panel>
    </cms:UIPlaceHolder>
    <cms:UIPlaceHolder ID="pnlUIClone" runat="server" ModuleName="CMS.Content" ElementName="Design.CloneAdHoc">
        <asp:Panel runat="server" ID="pnlClone" CssClass="ItemLast">
            <asp:Panel runat="server" ID="pnlClonePadding" CssClass="ItemPadding">
                <asp:Image runat="server" ID="imgClone" CssClass="Icon" EnableViewState="false" />&nbsp;
                <asp:Label runat="server" ID="lblClone" CssClass="Name" EnableViewState="false" Text="Clone template as ad-hoc" />
            </asp:Panel>
        </asp:Panel>
    </cms:UIPlaceHolder>
    <asp:Panel runat="server" ID="pnlSep1" CssClass="Separator">
        &nbsp;
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlRefresh" CssClass="ItemLast">
        <asp:Panel runat="server" ID="pnlRefreshPadding" CssClass="ItemPadding">
            <asp:Image runat="server" ID="imgRefresh" CssClass="Icon" EnableViewState="false" />&nbsp;<asp:Label
                runat="server" ID="lblRefresh" CssClass="Name" EnableViewState="false" Text="Refresh page" />
        </asp:Panel>
    </asp:Panel>
</asp:Panel>

<script type="text/javascript">
    //<![CDATA[
    function ContextEditLayout(aliasPath) {
        var popupBehavior = $find(aliasPath + '_layoutbehavior');
        popupBehavior.show();
    }
    //]]>
</script>

