using System;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.WebAnalytics;
using CMS.LicenseProvider;
using CMS.ExtendedControls;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_WebAnalytics_Tools_Default : CMSWebAnalyticsPage
{
    protected override void OnPreRender(EventArgs e)
    {
        // Initialize frames
        if (!AnalyticsHelper.AnalyticsEnabled(CMSContext.CurrentSiteName))
        {
            rowsFrameset.Attributes["rows"] = "136, 158, *";
        }
        
        if (CultureHelper.IsUICultureRTL())
        {
            ControlsHelper.ReverseFrames(this.colsFramesetAnalytics);
        }

        this.ltlScript.Text = ScriptHelper.GetTitleScript(ResHelper.GetString("tools.ui.webanalytics") + " ");

        base.OnPreRender(e);
    }
}
