using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.TreeEngine;
using CMS.Controls;
using CMS.UIControls;

public partial class CMSAdminControls_Categories_MultipleCategoriesSelector : CMSAdminEditControl
{
    #region "Variables"

    private bool mCombineWithGlobalCategories = true;
    private bool mSelectOnlyEnabled = true;
    private bool mHideOKButton = false;
    private bool mHideNewButton = false;
    private bool mAdministrationMode = false;
    private bool mFormControlMode = false;
    private int mPagerSize = 10;
    private int mDocumentID = 0;
    private int mUserID = 0;

    private DataSet dsUserCategories = null;
    private DataSet dsGlobalCategories = null;
    private DataSet dsDocumentCategories = null;

    private bool isSaved = false;

    #endregion


    #region "Properties"

    /// <summary>
    /// ID of the category to edit
    /// </summary>
    public int CategoryID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["CategoryID"], 0);
        }
        set
        {
            ViewState["CategoryID"] = value;
        }
    }


    /// <summary>
    /// Gets or sets the DocumentID for which the categories should be loaded
    /// </summary>
    public int DocumentID
    {
        get
        {
            return this.mDocumentID;
        }
        set
        {
            this.mDocumentID = value;
        }
    }


    /// <summary>
    /// Gets or sets the UserID whose categories should be displayed
    /// </summary>
    public int UserID
    {
        get
        {
            if (this.mUserID > 0)
            {
                return this.mUserID;
            }
            else
            {
                return CMSContext.CurrentUser.UserID;
            }
        }
        set
        {
            this.mUserID = value;
        }
    }


    /// <summary>
    /// Gets or sets the number of categories displayed per page
    /// </summary>
    public int PagerSize
    {
        get
        {
            return this.mPagerSize;
        }
        set
        {
            this.mPagerSize = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to show also global categories
    /// </summary>
    public bool CombineWithGlobalCategories
    {
        get
        {
            return this.mCombineWithGlobalCategories;
        }
        set
        {
            this.mCombineWithGlobalCategories = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to display only enabled categories
    /// </summary>
    public bool SelectOnlyEnabled
    {
        get
        {
            return this.mSelectOnlyEnabled;
        }
        set
        {
            this.mSelectOnlyEnabled = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to display OK button or not
    /// </summary>
    public bool HideOKButton
    {
        get
        {
            return this.mHideOKButton;
        }
        set
        {
            this.mHideOKButton = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to display New button or not
    /// </summary>
    public bool HideNewButton
    {
        get
        {
            return this.mHideNewButton;
        }
        set
        {
            this.mHideNewButton = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines if administration mode is On or not
    /// </summary>
    public bool AdministrationMode
    {
        get
        {
            return this.mAdministrationMode;
        }
        set
        {
            this.mAdministrationMode = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines if form control mode is On or not
    /// </summary>
    public bool FormControlMode
    {
        get
        {
            return this.mFormControlMode;
        }
        set
        {
            this.mFormControlMode = value;
        }
    }


    /// <summary>
    /// Gets or sets the text whixh is displayed when no actegories found
    /// </summary>
    public string ZeroRowsText
    {
        get
        {
            return this.gridUser.ZeroRowsText;
        }
        set
        {
            this.gridUser.ZeroRowsText = value;
            this.gridGlobal.ZeroRowsText = value;
        }
    }


    /// <summary>
    /// Gets or sets User categories unigrid.
    /// </summary>
    public UniGrid GridMyCategories
    {
        get
        {
            return ((UniGrid)this.gridUser);
        }
    }


    /// <summary>
    /// Gets or sets Global categories unigrid.
    /// </summary>
    public UniGrid GridGlobalCategories
    {
        get
        {
            return ((UniGrid)this.gridGlobal);
        }
    }


    /// <summary>
    /// Return true if editing form is visible.
    /// </summary>
    public bool Editing
    {
        get
        {
            return this.pnlEdit.Visible;
        }
    }


    /// <summary>
    /// Returns string of categories ID
    /// </summary>
    public string GetSelectedCategories()
    {
        StringBuilder builder = new StringBuilder();

        ArrayList userCategories = gridUser.SelectedItems;
        ArrayList globalCategories = gridGlobal.SelectedItems;

        // Go through all user categories
        foreach (string categoryId in userCategories)
        {
            builder.Append(categoryId + ",");
        }

        // Go through all global categories
        foreach (string categoryId in globalCategories)
        {
            builder.Append(categoryId + ",");
        }

        // Return categories
        return builder.ToString().TrimEnd(',');
    }

    #endregion


    #region "Events"

    /// <summary>
    /// Occurs after data is saved to the database.
    /// </summary>
    public delegate void OnAfterSaveEventHandler();

    /// <summary>
    /// OnAfterSave event.
    /// </summary>
    public event OnAfterSaveEventHandler OnAfterSave;

    #endregion


    protected override void OnPreRender(EventArgs e)
    {
        // Get categories for the document type
        this.dsDocumentCategories = CategoryInfoProvider.GetDocumentCategories(this.mDocumentID, null, null);
        if (!DataHelper.DataSourceIsEmpty(this.dsDocumentCategories))
        {
            ArrayList list = new ArrayList();
            foreach (DataRow row in this.dsDocumentCategories.Tables[0].Rows)
            {
                list.Add(row["CategoryID"].ToString());
            }
            // Load selection only once
            bool selectionLoaded = ValidationHelper.GetBoolean(ViewState["CategorySelectionLoaded"], false);
            if (!selectionLoaded)
            {
                ViewState["CategorySelectionLoaded"] = true;

                this.gridUser.SelectedItems = list;
                this.gridGlobal.SelectedItems = list;

                HiddenField hidNewSelection = this.gridUser.FindControl("hidNewSelection") as HiddenField;
                if (hidNewSelection != null)
                {
                    hidNewSelection.Value = "";
                }
                HiddenField hidDeSelection = this.gridUser.FindControl("hidDeSelection") as HiddenField;
                if (hidDeSelection != null)
                {
                    hidDeSelection.Value = "";
                }
            }

            this.gridUser.ReloadData();
            this.gridGlobal.ReloadData();
        }

        base.OnPreRender(e);

        if (this.AdministrationMode)
        {
            // Hide global unigrid and label
            gridGlobal.Visible = false;
            lblGlobalTitle.Visible = false;

            // Hide selector
            gridUser.GridView.Columns[0].Visible = false;

            // Hide my categories title
            this.lblUserTitle.Visible = false;

            // Hide Ok button
            this.HideOKButton = true;

            // Show checkbox enabled in edit
            this.categoryEditElem.ShowEnabled = true;

            // Show all categories
            this.SelectOnlyEnabled = false;
        }

        else if (this.FormControlMode)
        {
            this.HideNewButton = true;
            this.HideOKButton = true;
            this.gridUser.GridView.Columns[1].Visible = false;
            this.gridGlobal.GridView.Columns[1].Visible = false;
        }

        if (RequestHelper.IsPostBack() && !isSaved)
        {
            this.dsDocumentCategories = CategoryInfoProvider.GetDocumentCategories(this.mDocumentID, null, null);
            if (!this.FormControlMode)
            {
                gridGlobal.ReloadData();
                gridUser.ReloadData();
            }

            // Set category id from edit element
            if (this.categoryEditElem.CategoryID > 0)
            {
                this.CategoryID = this.categoryEditElem.CategoryID;
            }

            if (this.CategoryID > 0)
            {
                CategoryInfo categoryInfo = CategoryInfoProvider.GetCategoryInfo(this.CategoryID);
                if (categoryInfo != null)
                {
                    this.lblCurrent.Text = HTMLHelper.HTMLEncode(categoryInfo.CategoryDisplayName);
                }
            }
            else
            {
                this.lblCurrent.Text = ResHelper.GetString("MultipleCategoriesSelector.NewCategory");
            }
        }

        if ((!this.FormControlMode) && (isSaved))
        {
            // Changes saved
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            pnlInfo.Visible = true;
        }

        // If no "My categories" presents
        if (this.gridUser.GridView.Rows.Count == 0)
        {
            if (this.AdministrationMode)
            {
                // Hide "My categories" title
                this.lblUserTitle.Visible = false;
            }
            else
            {
                this.pnlUserGrid.Visible = false;
            }

            // If user is not global admin
            if (!CMSContext.CurrentUser.IsGlobalAdministrator)
            {
                // Hide actions column in global gridview
                this.gridGlobal.GridView.Columns[1].Visible = false;
            }
        }
        else
        {
            if ((!this.gridGlobal.GridView.Columns[1].Visible) && (!this.FormControlMode))
            {
                // Show actions column in global gridview if hidden
                this.gridGlobal.GridView.Columns[1].Visible = true;
            }
        }

        if (HideNewButton)
        {
            this.pnlNewItem.Visible = false;
        }
        if (HideOKButton)
        {
            this.btnOk.Visible = false;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.StopProcessing)
        {
            // Set category id from edit element
            if (this.categoryEditElem.CategoryID > 0)
            {
                this.CategoryID = this.categoryEditElem.CategoryID;
            }

            isSaved = QueryHelper.GetBoolean("saved", false);

            this.lblUserTitle.Text = ResHelper.GetString("Categories.UserCategories");
            this.lnkList.Text = ResHelper.GetString("Categories.CategoriesList");
            this.lblGlobalTitle.Text = ResHelper.GetString("Categories.GlobalCategories");
            this.imgNewItem.ImageUrl = GetImageUrl("Objects/CMS_Category/add.png");
            this.imgNewItem.DisabledImageUrl = GetImageUrl("Objects/CMS_Category/adddisabled.png");
            this.imgNewItem.AlternateText = ResHelper.GetString("MultipleCategoriesSelector.NewCategory");

            this.categoryEditElem.UserID = this.UserID;
            this.categoryEditElem.CategoryID = this.CategoryID;
            this.categoryEditElem.ShowCodeName = false;
            this.categoryEditElem.ShowEnabled = false;
            this.categoryEditElem.OnSaved += new EventHandler(categoryEditElem_OnSaved);
            this.categoryEditElem.OnCheckPermissions += new CMSAdminControl.CheckPermissionsEventHandler(categoryEditElem_OnCheckPermissions);

            this.gridUser.PageSize = this.PagerSize.ToString();
            this.gridGlobal.PageSize = this.PagerSize.ToString();
            this.gridUser.IsLiveSite = this.IsLiveSite;
            this.gridGlobal.IsLiveSite = this.IsLiveSite;

            this.gridUser.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridUser_OnExternalDataBound);
            this.gridGlobal.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridGlobal_OnExternalDataBound);
            this.gridUser.OnAction += new OnActionEventHandler(gridUser_OnAction);
            this.gridGlobal.OnAction += new OnActionEventHandler(gridGlobal_OnAction);

            string where = (this.mSelectOnlyEnabled ? "CategoryEnabled = 1" : null);
            this.gridUser.WhereCondition = CreateWhereCondition(where, false);
            this.gridGlobal.WhereCondition = CreateWhereCondition(where, true);

            // Delete confirmation
            ltlScript.Text = ScriptHelper.GetScript("deleteConfirmation = '" + ResHelper.GetString("Categories.DeleteConfirmation") + "';");

            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "DeleteScript", ScriptHelper.GetScript("function DeleteConfirm() { return confirm(deleteConfirmation); }"));

            // Hide OK button if specified
            if (!this.HideOKButton)
            {
                // Disable OK button if no categories found
                this.btnOk.Text = ResHelper.GetString("General.OK");
                if ((DataHelper.DataSourceIsEmpty(dsUserCategories)) && (DataHelper.DataSourceIsEmpty(dsGlobalCategories)))
                {
                    this.btnOk.Enabled = false;
                }
            }
            else
            {
                this.btnOk.Visible = false;
            }

            // Hide new button if specified
            if (!this.HideNewButton)
            {
                this.lnkNewItem.Text = ResHelper.GetString("MultipleCategoriesSelector.NewCategory");
            }
            else
            {
                this.pnlNewItem.Visible = false;
            }

            // Show edit category control if needed
            if (this.CategoryID > 0)
            {
                this.categoryEditElem.CategoryID = this.CategoryID;
                this.categoryEditElem.UserID = CMSContext.CurrentUser.UserID;

                CategoryInfo categoryInfo = CategoryInfoProvider.GetCategoryInfo(this.CategoryID);
                if (categoryInfo != null)
                {
                    this.lblCurrent.Text = HTMLHelper.HTMLEncode(categoryInfo.CategoryName);
                }
            }
            else
            {
                this.lblCurrent.Text = ResHelper.GetString("MultipleCategoriesSelector.NewCategory");
            }
        }
    }


    protected void categoryEditElem_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        if (!RaiseOnCheckPermissions(permissionType, this))
        {
            CurrentUserInfo cui = CMSContext.CurrentUser;
            if ((cui == null) || ((this.UserID != cui.UserID) && !cui.IsAuthorizedPerResource("CMS.Users", permissionType)))
            {
                RedirectToAccessDenied("CMS.Users", permissionType);
            }
        }
    }


    protected void gridGlobal_OnAction(string actionName, object actionArgument)
    {
        // Get category Id
        int categoryId = ValidationHelper.GetInteger(actionArgument, 0);

        switch (actionName.ToLower())
        {
            case "delete":
                // Delete category from DB
                CategoryInfoProvider.DeleteCategoryInfo(categoryId);

                // We need to refresh the page to reload all categories
                if (!this.FormControlMode)
                {
                    UrlHelper.Redirect(UrlHelper.AddParameterToUrl(UrlHelper.CurrentURL, "saved", "0"));
                }
                break;
            case "edit":
                this.CategoryID = categoryId;
                this.categoryEditElem.CategoryID = categoryId;
                this.categoryEditElem.ReloadData();
                this.pnlEdit.Visible = true;
                this.pnlUserGrid.Visible = false;
                this.pnlGlobalGrid.Visible = false;
                this.pnlNewItem.Visible = false;
                this.pnlBreadCrumb.Visible = true;

                this.pnlInfo.Visible = false;
                isSaved = false;
                break;
        }
    }


    protected void gridUser_OnAction(string actionName, object actionArgument)
    {
        // Get category Id
        int categoryId = ValidationHelper.GetInteger(actionArgument, 0);

        switch (actionName.ToLower())
        {
            case "delete":

                if (!RaiseOnCheckPermissions(CMSAdminControl.PERMISSION_MODIFY, this))
                {
                    CurrentUserInfo cui = CMSContext.CurrentUser;
                    if ((cui == null) || ((this.UserID != cui.UserID) && !cui.IsAuthorizedPerResource("CMS.Users", CMSAdminControl.PERMISSION_MODIFY)))
                    {
                        RedirectToAccessDenied("CMS.Users", CMSAdminControl.PERMISSION_MODIFY);
                    }
                }

                // Delete category from DB
                CategoryInfoProvider.DeleteCategoryInfo(categoryId);

                // We need to refresh the page to reload all categories
                if (!this.FormControlMode)
                {
                    UrlHelper.Redirect(UrlHelper.AddParameterToUrl(UrlHelper.CurrentURL, "saved", "0"));
                }
                break;
            case "edit":
                this.CategoryID = categoryId;
                this.categoryEditElem.CategoryID = categoryId;
                this.categoryEditElem.ReloadData();
                this.pnlEdit.Visible = true;
                this.pnlUserGrid.Visible = false;
                this.pnlGlobalGrid.Visible = false;
                this.pnlNewItem.Visible = false;
                this.pnlBreadCrumb.Visible = true;

                this.pnlInfo.Visible = false;
                isSaved = false;
                break;
        }
    }

    protected object gridGlobal_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "edit":
                if (!CMSContext.CurrentUser.IsGlobalAdministrator)
                {
                    ImageButton editImg = sender as ImageButton;
                    if (editImg != null)
                    {
                        editImg.Visible = false;
                    }
                }
                break;
            case "delete":
                ImageButton deleteImg = sender as ImageButton;
                if (deleteImg != null)
                {
                    deleteImg.Visible = false;
                }
                break;
            case "displayname":
                return HTMLHelper.HTMLEncode(parameter.ToString());
        }
        return parameter;
    }

    protected object gridUser_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "displayname":
                return HTMLHelper.HTMLEncode(parameter.ToString());
        }
        return parameter;
    }


    /// <summary>
    /// Edit button click
    /// </summary>
    protected void btnEdit_Command(object sender, CommandEventArgs e)
    {
        int categoryId = ValidationHelper.GetInteger(e.CommandArgument, 0);

        this.CategoryID = categoryId;
        this.categoryEditElem.CategoryID = categoryId;
        this.categoryEditElem.ReloadData();
        this.pnlEdit.Visible = true;
        this.pnlUserGrid.Visible = false;
        this.pnlGlobalGrid.Visible = false;
        this.pnlNewItem.Visible = false;
        this.pnlBreadCrumb.Visible = true;

        this.pnlInfo.Visible = false;
        isSaved = false;
    }


    /// <summary>
    /// Edit button click
    /// </summary>
    protected void btnDelete_Command(object sender, CommandEventArgs e)
    {
        // Get category Id
        int categoryId = ValidationHelper.GetInteger(e.CommandArgument, 0);

        // Delete category from DB
        CategoryInfoProvider.DeleteCategoryInfo(categoryId);

        // We need to refresh the page to reload all categories
        if (!this.FormControlMode)
        {
            UrlHelper.Redirect(UrlHelper.AddParameterToUrl(UrlHelper.CurrentURL, "saved", "0"));
        }
    }


    /// <summary>
    /// OK button click - save the values
    /// </summary>
    protected void lnkNewItem_Click(object sender, EventArgs e)
    {
        this.CategoryID = 0;
        this.categoryEditElem.ShowEnabled = false;
        this.categoryEditElem.ShowCodeName = false;
        this.categoryEditElem.CategoryID = 0;
        this.categoryEditElem.UserID = CMSContext.CurrentUser.UserID;
        this.categoryEditElem.ReloadData();
        this.pnlEdit.Visible = true;
        this.pnlUserGrid.Visible = false;
        this.pnlGlobalGrid.Visible = false;
        this.pnlNewItem.Visible = false;
        this.pnlBreadCrumb.Visible = true;

        this.pnlInfo.Visible = false;
        isSaved = false;
    }


    void categoryEditElem_OnSaved(object sender, EventArgs e)
    {
        lnkList_Click(sender, e);
    }


    /// <summary>
    /// lnkList click event handler.
    /// </summary>
    protected void lnkList_Click(object sender, EventArgs e)
    {
        this.pnlEdit.Visible = false;
        this.pnlUserGrid.Visible = true;
        this.pnlGlobalGrid.Visible = true;
        this.pnlNewItem.Visible = true;
        this.pnlBreadCrumb.Visible = false;

        // We need to refresh the page to reload all categories
        if (!this.FormControlMode)
        {
            UrlHelper.Redirect(UrlHelper.AddParameterToUrl(UrlHelper.CurrentURL, "saved", "0"));
        }
    }


    /// <summary>
    /// OK button click - save the values
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        Save();
    }


    /// <summary>
    /// OK button click - save the values
    /// </summary>
    public void Save()
    {
        if (!RaiseOnCheckPermissions(CMSAdminControl.PERMISSION_MODIFY, this))
        {
            CurrentUserInfo cui = CMSContext.CurrentUser;
            if ((cui == null) || ((this.UserID != cui.UserID) && !cui.IsAuthorizedPerResource("CMS.Users", CMSAdminControl.PERMISSION_MODIFY)))
            {
                RedirectToAccessDenied("CMS.Users", CMSAdminControl.PERMISSION_MODIFY);
            }
        }

        ArrayList userNew = gridUser.NewlySelectedItems;
        foreach (string category in userNew)
        {
            int categoryId = ValidationHelper.GetInteger(category, 0);
            // Add document to category
            DocumentCategoryInfoProvider.AddDocumentToCategory(this.mDocumentID, categoryId);
        }
        ArrayList userDeselect = gridUser.DeselectedItems;
        foreach (string category in userDeselect)
        {
            int categoryId = ValidationHelper.GetInteger(category, 0);
            // Remove document from category
            DocumentCategoryInfoProvider.RemoveDocumentFromCategory(this.mDocumentID, categoryId);
        }

        ArrayList globalNew = gridGlobal.NewlySelectedItems;
        foreach (string category in globalNew)
        {
            int categoryId = ValidationHelper.GetInteger(category, 0);
            // Add document to category
            DocumentCategoryInfoProvider.AddDocumentToCategory(this.mDocumentID, categoryId);
        }
        ArrayList globalDeselect = gridGlobal.DeselectedItems;
        foreach (string category in globalDeselect)
        {
            int categoryId = ValidationHelper.GetInteger(category, 0);
            // Remove document from category
            DocumentCategoryInfoProvider.RemoveDocumentFromCategory(this.mDocumentID, categoryId);
        }

        // Raise on after save
        if (OnAfterSave != null)
        {
            OnAfterSave();
        }

        if (!this.FormControlMode)
        {
            // Refresh page
            UrlHelper.Redirect(UrlHelper.AddParameterToUrl(UrlHelper.CurrentURL, "saved", "1"));
        }

        isSaved = true;
    }

    #region "Private helper functions"

    /// <summary>
    /// Return where condition with userID and combine with global.
    /// </summary>
    /// <param name="where">Main where condition</param>
    /// <param name="global">Global categories</param>
    private string CreateWhereCondition(string where, bool global)
    {
        string whereText = "(";

        // if the user was specified
        if ((this.UserID > 0) && (!global))
        {
            whereText += "CategoryUserID = " + this.UserID;
        }
        else
        {
            whereText += "CategoryUserID IS NULL";
        }

        whereText += ")";

        // if some user was specified
        if (whereText.Trim() != "()")
        {
            if ((where != null) && (where.Trim() != ""))
            {
                whereText += " AND (" + where + ")";
            }
        }
        else
        {
            whereText = where;
        }

        return whereText;
    }

    #endregion

}
