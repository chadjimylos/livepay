﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports LivePay_ESBBridge

Partial Class CMSTemplates_LivePay_PopUp_UCTransDetails
    Inherits CMSUserControl

#Region "Variables"

    Private sTransID As String = "0"
    Private sPageType As PopUpType = Nothing
    Private sBoxNo As String = Nothing
    Private sBoxCloseDtm As String = Nothing
    Private sBoxStatus As String = Nothing
    Private sTransCode As String = Nothing
    Private sTransDtm As String = Nothing
    Private sCustFullName As String = Nothing
    Private sPrice As String = Nothing
    Private sTransType As String = Nothing
    Private sTransStatus As String = Nothing
    Private sCardType As String = Nothing
    Private sCardNo As String = Nothing

    Private sAcountNo As String = Nothing
    Private sEmail As String = Nothing
    Private sContactPhone As String = Nothing
    Private sNotes As String = Nothing


#End Region

#Region " Properties For Javascript "

    Public Property TransID() As String
        Get
            Return sTransID
        End Get
        Set(ByVal value As String)
            sTransID = value
        End Set
    End Property

    Public Property PageType() As PopUpType
        Get
            Return sPageType
        End Get
        Set(ByVal value As PopUpType)
            sPageType = value
        End Set
    End Property

#End Region
#Region "Methods"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then

        End If
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = New Guid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

    Public Enum PopUpType
        Detalis = 1
        Cancel = 2
        Report = 3
    End Enum

    Private Sub ClearAllStyles()
        DetailsBtnRow.Style.Add("display", "none")
        CancelRow.Style.Add("display", "none")
        CancelBtnRow.Style.Add("display", "none")
        ReportRow.Style.Add("display", "none")
        ReportBtnRow.Style.Add("display", "none")
        TRPrice.Style.Add("display", "none")
        TRPriceTxt.Style.Add("display", "none")
    End Sub

    Public Overloads Sub loadData()
        ClearAllStyles()
        If Me.TransID <> "0" Then
            Me.TransID = Replace(Me.TransID, " ", String.Empty)
            Me.TransID = Right(Me.TransID, (Me.TransID.Length - 2))

            btnRefund.CommandArgument = Me.TransID
            btnRefund.Attributes.Add("onclick", "return SetIt()")
            btnCancel.CommandArgument = Me.TransID
            btnSaveToPDF.CommandArgument = Me.TransID
            Session("TransDetails_TransIDForExport") = Me.TransID
            btnSaveToPDF.Attributes.Add("onclick", "return ExportDetails('/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=MerchantTransDetails')")

            Select Case Me.PageType
                Case PopUpType.Detalis
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Πληροφορίες Συναλλαγής|en-us=Closed$}")
                    DetailsBtnRow.Style.Add("display", "")
                Case PopUpType.Cancel
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Επιβεβαίωση Ακύρωσης Συναλλαγής|en-us=Closed$}")
                    CancelRow.Style.Add("display", "")
                    CancelBtnRow.Style.Add("display", "")
                Case PopUpType.Report
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Αντιλογισμός Συναλλαγής|en-us=Closed$}")
                    ReportRow.Style.Add("display", "")
                    ReportBtnRow.Style.Add("display", "")
            End Select

            Dim w As New LivePay_ESBBridge.Bridge
            Dim TransResponse As TransactionDetailsResponse = w.GetTransactionDetails(Me.TransID)
            Dim TransInfo As TransactionDetailsInfo = TransResponse.TransactionDetails

            If Not IsNothing(TransInfo) Then
                tdBoxNo.InnerHtml = TransInfo.batchId
                tdBoxCloseDtm.InnerHtml = TransInfo.batchDate
                tdBoxStatus.InnerHtml = IIf(TransInfo.batchClosed, ResHelper.LocalizeString("{$=Κλειστό|en-us=Closed$}"), ResHelper.LocalizeString("{$=Ανοιχτό|en-us=Open$}"))
                tdTransCode.InnerHtml = TransInfo.transactionId
                tdTransDtm.InnerHtml = TransInfo.transactionDate
                tdCustFullName.InnerHtml = TransInfo.customerName

                Dim amount As String = Replace(TransInfo.transactionAmount, ".", ",")
                Dim FinalAmount As String
                If amount.Split(",").Length > 1 Then
                    FinalAmount = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
                ElseIf amount.Split(",").Length = 1 Then
                    FinalAmount = amount & ",00"
                Else
                    FinalAmount = amount
                End If


                If Me.PageType = PopUpType.Detalis Or Me.PageType = PopUpType.Cancel Then
                    tdPrice.InnerHtml = FinalAmount
                    TRPrice.Style.Add("display", "")
                Else
                    TRPriceTxt.Style.Add("display", "")

                    txtPrice.Text = Replace(FinalAmount, ",", ".")
                    txtPrice.ReadOnly = False
                End If
                btnRefund.CommandName = TransInfo.transactionAmount
                tdTransType.InnerHtml = TransInfo.transactionType.ToString
                tdTransStatus.InnerHtml = TransInfo.transactionStatus.ToString
                tdCardType.InnerHtml = IIf(TransInfo.cardType = 0, "Visa", "MasterCard")
                Dim CardNo As String = Replace(TransInfo.cardNumber, " ", String.Empty)
                tdCardNo.InnerHtml = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                tdEmail.InnerHtml = TransInfo.customerEmail
                tdContactPhone.InnerHtml = TransInfo.customerTelephone
                tdNotes.InnerHtml = TransInfo.customerComments
                CreateCustomFields(TransInfo.merchantId, TransInfo)
                btnRefund.Enabled = True
                btnCancel.Enabled = True

            Else
                tdBoxNo.InnerHtml = String.Empty
                tdBoxCloseDtm.InnerHtml = String.Empty
                tdBoxStatus.InnerHtml = String.Empty
                tdTransCode.InnerHtml = String.Empty
                tdTransDtm.InnerHtml = String.Empty
                tdCustFullName.InnerHtml = String.Empty
                If Me.PageType = PopUpType.Detalis Or Me.PageType = PopUpType.Cancel Then
                    tdPrice.InnerHtml = String.Empty
                    TRPrice.Style.Add("display", "")
                Else
                    TRPriceTxt.Style.Add("display", "")
                    txtPrice.Text = 0
                    txtPrice.ReadOnly = True
                End If
                btnRefund.CommandName = String.Empty
                tdTransType.InnerHtml = String.Empty
                tdTransStatus.InnerHtml = String.Empty
                tdCardType.InnerHtml = String.Empty

                tdCardNo.InnerHtml = String.Empty
                tdEmail.InnerHtml = String.Empty
                tdContactPhone.InnerHtml = String.Empty
                tdNotes.InnerHtml = String.Empty
                btnRefund.Enabled = False
                btnCancel.Enabled = False

            End If
        End If
    End Sub

    Private Sub CreateCustomFields(ByVal MerchantID As String, ByVal TransInfo As TransactionDetailsInfo)
        Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(MerchantID)
        Dim LivePay_MerchantID As Integer = 0
        If dtMerc.Rows.Count > 0 Then
            LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
        End If

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(LivePay_MerchantID)
        Dim dt As DataTable = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim CustomTR As New HtmlTableRow
            Dim CustomTDTitle As New HtmlTableCell
            Dim CustomTDValue As New HtmlTableCell
            CustomTDTitle.Attributes.Add("class", "PopTransDetTitle2")
            CustomTDValue.Attributes.Add("class", "PopTransDetValue")
            CustomTDTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
            Select Case i
                Case 0
                    CustomTDValue.InnerHtml = TransInfo.info1
                Case 1
                    CustomTDValue.InnerHtml = TransInfo.info2
                Case 2
                    CustomTDValue.InnerHtml = TransInfo.info3
                Case 3
                    CustomTDValue.InnerHtml = TransInfo.info4
                Case 4
                    CustomTDValue.InnerHtml = TransInfo.info5
                Case 5
                    CustomTDValue.InnerHtml = TransInfo.info6
                Case 6
                    CustomTDValue.InnerHtml = TransInfo.info7
                Case 7
                    CustomTDValue.InnerHtml = TransInfo.info8
                Case 8
                    CustomTDValue.InnerHtml = TransInfo.info9
                Case 9
                    CustomTDValue.InnerHtml = TransInfo.info10
            End Select

            CustomTR.Controls.Add(CustomTDTitle)
            CustomTR.Controls.Add(CustomTDValue)
            tblCustomFields.Controls.Add(CustomTR)
        Next
    End Sub

    Sub Refund_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRefund.Click
        Dim CatchError As String = String.Empty
        Session("RefundError") = String.Empty
        Dim NewTransID As Integer = 0
        Dim NewTransGUID As Guid = Nothing
        Dim OldTransGUID As Guid = Nothing
        Try
            Session("RefundError") = String.Empty
            Dim AllRequest As String = Server.UrlDecode(Request.Form.ToString)
            AllRequest = Split(AllRequest, "txtRefund=")(1)
            AllRequest = Split(AllRequest, "&plc$")(0)
            Me.TransID = btnRefund.CommandArgument
            Dim dt As DataTable = DBConnection.InsertOtherTypeTransaction(Me.TransID, AllRequest, 0, Me.TransID, 0)
          
            If dt.Rows.Count > 0 Then
                NewTransID = dt.Rows(0)("TransID").ToString()
                NewTransGUID = New Guid(dt.Rows(0)("TransGUID").ToString())
                OldTransGUID = New Guid(dt.Rows(0)("OldTransGUID").ToString())
                Dim w As New LivePay_ESBBridge.Bridge
                Dim c As RefundTransactionServiceResponse = w.RefundTransaction(Me.TransID, NewTransID, AllRequest)
                If c.Result Then
                    DBConnection.UpdateTransaction(NewTransID, 2, 0) '------ Update Transaction Set Status = 2 (Success)
                    Session("RefundError") = String.Empty
                    Response.Redirect("TransConfirmation.aspx?PageType=Refund&Result=ok&TransID=" & NewTransGUID.ToString) '---- Nkal I Need to take the New GUID
                Else

                    DBConnection.UpdateTransaction(NewTransID, 1, 0) '------ Update Transaction Set Status = 1 (Failed)
                    Session("RefundError") = c.ErrorCode & " : " & c.ErrorMessage
                    Response.Redirect("TransConfirmation.aspx?PageType=REfund&Result=ESBerror")
                End If
            End If
        Catch ex As Exception
            CatchError = ex.ToString
        End Try
        If CatchError <> String.Empty Then
            DBConnection.UpdateTransaction(NewTransID, 1, 0) '------ Update Transaction Set Status = 1 (Failed)
            Session("RefundError") = CatchError
            Response.Redirect("TransConfirmation.aspx?PageType=Refund&Result=Catcherror")
        End If
        
    End Sub

    Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Dim CatchError As String = String.Empty
        Session("CancelError") = String.Empty
        Dim NewTransID As Integer = 0
        Dim NewTransGUID As Guid = Nothing
        Dim OldTransGUID As Guid = Nothing
        Try
            Me.TransID = btnCancel.CommandArgument
            Dim dt As DataTable = DBConnection.InsertOtherTypeTransaction(Me.TransID, 0, Me.TransID, 0, 0)
            If dt.Rows.Count > 0 Then
                NewTransID = dt.Rows(0)("TransID").ToString()
                NewTransGUID = New Guid(dt.Rows(0)("TransGUID").ToString())
                OldTransGUID = New Guid(dt.Rows(0)("OldTransGUID").ToString())
                Dim w As New LivePay_ESBBridge.Bridge
                Dim c As CancelTransactionServiceResponse = w.CancelTransaction(Me.TransID, NewTransID)
                If c.Result Then
                    DBConnection.UpdateTransaction(NewTransID, 2, 0) '------ Update Transaction Set Status = 2 (Success)
                    Session("CancelError") = String.Empty
                    Response.Redirect("TransConfirmation.aspx?PageType=Cancel&Result=ok&TransID=" & OldTransGUID.ToString) '---- Nkal I Need to take the old GUID
                Else
                    DBConnection.UpdateTransaction(NewTransID, 1, 0) '------ Update Transaction Set Status = 1 (Failed)
                    Session("CancelError") = c.ErrorCode & " : " & c.ErrorMessage
                    Response.Redirect("TransConfirmation.aspx?PageType=Cancel&Result=ESBerror")
                End If
            End If
        Catch ex As Exception
            CatchError = ex.ToString
        End Try
        If CatchError <> String.Empty Then
            DBConnection.UpdateTransaction(NewTransID, 1, 0) '------ Update Transaction Set Status = 1 (Failed)
            Session("CancelError") = CatchError
            Response.Redirect("TransConfirmation.aspx?PageType=Cancel&Result=Catcherror")
        End If
        
    End Sub
#End Region

   
    
    Protected Sub btnSaveToPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveToPDF.Click

        Session("TransDetails_TransIDForExport") = btnSaveToPDF.CommandArgument
        Response.Redirect("/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=MerchantTransDetails")
    End Sub
End Class
