﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports LivePay_ESBBridge
Imports CMS.GlobalHelper
Imports CMS.UIControls

Imports CMS.SiteProvider
Imports CMS.CMSHelper

Partial Class CMSTemplates_AegeanPower_UCAegeanPowerAccountActivation
    Inherits CMSUserControl

#Region "Methods"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            'GET USER AND ACTIVATE HIM

            Try

                Dim g As Guid = New Guid(Request.QueryString("userguid").ToString)

                ' grab a user from the database.
                Dim usr As UserInfo = UserInfoProvider.GetUserInfoByGUID(g)
                If usr.Enabled Then
                    lblResultTitle.Text = "Ο λογαριασμός σας είναι ήδη ενεργοποιημένος."
                Else
                    ' update their Enabled.
                    usr.Enabled = True
                    ' commit changes to database.
                    UserInfoProvider.SetUserInfo(usr)

                    'if success
                    lblResultTitle.Text = "Ο λογαριασμός σας ενεργοποιήθηκε με επιτυχία"
                End If
               
            Catch ex As Exception
                'if not success
                lblResultTitle.Text = "Ο λογαριασμός δεν ενεργοποιήθηκε.<br />Βεβαιωθείτε ότι ακολουθήσατε το σωστό σύνδεσμο (link) και ξαναπροσπαθήστε."
                lblResultTitle.ForeColor = Drawing.Color.Red
                lblInfo.Visible = False
            End Try

        End If
    End Sub

    Public Overloads Sub ReloadData()

    End Sub

#End Region


End Class
