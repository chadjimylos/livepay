﻿using System;
using System.Web.UI;
using System.Collections;
using System.IO;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;

public partial class CMSModules_Content_Controls_Dialogs_Properties_FileSystemPathProperties : ItemProperties
{
    #region "Properties"

    /// <summary>
    /// Gets or sets if selected item is file or folder.
    /// </summary>
    public bool IsFile
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["IsFile"], true);
        }
        set
        {
            ViewState["IsFile"] = value;
        }
    }

    #endregion


    #region "Page events"

    /// <summary>
    /// Page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get refference causing postback to hidden button
        string postBackRef = ControlsHelper.GetPostBackEventReference(this.hdnButton, "");
        string postBackKeyDownRef = "var keynum;if(window.event){keynum = event.keyCode;}else if(event.which){keynum = event.which;}if(keynum == 13){" + postBackRef + "; return false;}";

        this.txtPathText.Attributes["onkeydown"] = postBackKeyDownRef;
        
    }


    /// <summary>
    /// Page prerender
    /// </summary>
    protected void Page_Prerender(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(this.txtPathText.Text))
        {
            if (this.lblError.Text == String.Empty)
            {
                this.lblEmpty.Text = this.NoSelectionText;
                this.lblEmpty.Visible = true;
                this.plnPathUpdate.Visible = false;
            }
            else
            {
                this.plnPathUpdate.Visible = true;
                this.lblEmpty.Visible = false;
            }
            
        }
        else
        {
            this.plnPathUpdate.Visible = true;
            this.lblEmpty.Visible = false;
        }
    }


    /// <summary>
    /// Click action on hidden button
    /// </summary>
    protected void hdnButton_Click(object sender, EventArgs e)
    {
        if (this.Validate())
        {
            // Get selected item information
            Hashtable properties = GetItemProperties();

            // Get JavaScript for inserting the item
            string script = CMSDialogHelper.GetFileSystemItem(properties);
            if (!string.IsNullOrEmpty(script))
            {
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "insertItemScript", script, true);
            }
        }
        
    }

    #endregion


    #region "Overriden methods"

    /// <summary>
    /// Loads the properites into control
    /// </summary>
    /// <param name="properties"></param>
    public override void LoadItemProperties(Hashtable properties)
    {
        if (properties != null)
        {
            string filePath = ValidationHelper.GetString(properties[DialogParameters.ITEM_PATH], "");
            string fileSize = ValidationHelper.GetString(properties[DialogParameters.ITEM_SIZE], "");
            bool showSize = ValidationHelper.GetBoolean(properties[DialogParameters.ITEM_ISFILE], true);
            bool isPathRelative = ValidationHelper.GetBoolean(properties[DialogParameters.ITEM_RELATIVEPATH], true);

            this.IsFile = showSize;
            if (isPathRelative)
            {
                filePath = filePath.Replace(Server.MapPath("~").TrimEnd('/'),"~").Replace("\\","/"); 
            }
            this.txtPathText.Text = filePath;
            if (showSize)
            {
                this.plcFileSize.Visible = true;
                this.lblFileSizeText.Text = fileSize;
            }
            else
            {
                this.plcFileSize.Visible = false; 
            }
        }
    }


    /// <summary>
    /// Returns all parameters of the selected item as name – value collection.
    /// </summary>
    public override Hashtable GetItemProperties()
    {
        Hashtable retval = new Hashtable();

        retval[DialogParameters.ITEM_PATH] = this.txtPathText.Text.Trim();
        retval[DialogParameters.ITEM_SIZE] = this.lblFileSizeText.Text.Trim();
        retval[DialogParameters.ITEM_ISFILE] = ValidationHelper.IsFileName(this.txtPathText.Text.Trim());
        
        retval[DialogParameters.EDITOR_CLIENTID] = QueryHelper.GetString(DialogParameters.EDITOR_CLIENTID, "");
        return retval;
    }


    /// <summary>
    /// Validates From, Cc and Bcc e-mails.
    /// </summary>
    public override bool Validate()
    {
        string errorMessage = "";

        if (string.IsNullOrEmpty(this.txtPathText.Text))
        {
            errorMessage += ResHelper.GetString("dialogs.filesystem.requirepath");
        }
        else if (!((File.Exists(this.txtPathText.Text.Trim())) || (File.Exists(Server.MapPath(this.txtPathText.Text.Trim())))) && (this.IsFile))
        {
            errorMessage += ResHelper.GetString("general.filedoesntexist");
        }
        else if (!((Directory.Exists(this.txtPathText.Text.Trim())) || (Directory.Exists(Server.MapPath(this.txtPathText.Text.Trim())))) && (!this.IsFile))
        {
            errorMessage += ResHelper.GetString("general.folderdoesntexist");
        }
        
        errorMessage = errorMessage.Trim();

        if (errorMessage != "")
        {
            this.lblError.Text = errorMessage;
            this.lblError.Visible = true;
            this.plcFileSize.Visible = false;
            this.plnPathUpdate.Update();
            return false;
        }

        return true;
    }


    /// <summary>
    /// Clears the properties form.
    /// </summary>
    public override void ClearProperties(bool hideProperties)
    {
        this.txtPathText.Text = "";
        this.lblFileSizeText.Text = "";
    }

    #endregion
}
