using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.CMSHelper;
using CMS.EventManager;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.SiteProvider;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;
using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_EventManager_CMSPages_AddToOutlook : LivePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get event NodeID from querystring
        int eventNodeId = QueryHelper.GetInteger("eventid", 0);
        int timeZoneId = QueryHelper.GetInteger("timezoneid", 0);

        if (eventNodeId != 0)
        {
            TreeProvider mTree = new TreeProvider();
            TreeNode eventInfo = mTree.SelectSingleNode(eventNodeId);

            if (eventInfo != null && eventInfo.NodeClassName.ToLower() == "cms.bookingevent")
            {
                // Get file content.
                byte[] fileContent = GetContent(eventInfo.DataRow, timeZoneId);

                if (fileContent != null)
                {
                    // Clear response.
                    Response.Cookies.Clear();
                    Response.Clear();

                    // Prepare response.
                    Response.ContentType = "text/plain";
                    // Disposition type - For files "attachment" is default
                    Response.AddHeader("Content-Disposition", "attachment; filename=Reminder.ics");

                    Response.OutputStream.Write(fileContent, 0, fileContent.Length);

                    //RequestHelper.CompleteRequest();
                    RequestHelper.EndResponse();
                }
            }
        }
    }


    /// <summary>
    /// Get iCalendar file content.
    /// </summary>
    /// <param name="row">Datarow</param>
    protected byte[] GetContent(DataRow row, int timeZoneId)
    {
        if (row != null)
        {
            // Get time zone info for shifting event time to specific time zone
            TimeZoneInfo tzi = null;
            if (timeZoneId > 0)
            {
                tzi = TimeZoneInfoProvider.GetTimeZoneInfo(timeZoneId);
            }

            TimeZoneInfo serverTimeZone = TimeZoneHelper.ServerTimeZone;
            // Shift event start time to GMT
            DateTime eventStartGMT = ValidationHelper.GetDateTime(DataHelper.GetDataRowValue(row, "EventDate"), DataHelper.DATETIME_NOT_SELECTED);
            eventStartGMT = TimeZoneHelper.ConvertTimeZoneDateTimeToGMT(eventStartGMT, serverTimeZone);

            // Shift current time to GMT
            DateTime currentDateGMT = DateTime.Now;
            currentDateGMT = TimeZoneHelper.ConvertTimeZoneDateTimeToGMT(currentDateGMT, serverTimeZone);

            // Get location
            string location = ValidationHelper.GetString(DataHelper.GetDataRowValue(row, "EventLocation"), "");

            // Create string
            string content = "BEGIN:vCalendar\r\n" +
                //"VERSION:2.0\r\n" +
                "METHOD:PUBLISH\r\n" +
                "BEGIN:vEvent\r\n" +
                "DTSTART:" + eventStartGMT.ToString("yyyyMMdd'T'HHmmss") + "Z\r\n" +
                "DTSTAMP:" + currentDateGMT.ToString("yyyyMMdd'T'HHmmss") + "Z\r\n";

            // Include location if specified
            if (!String.IsNullOrEmpty(location))
            {
                content += "LOCATION:" + HTMLHelper.StripTags(HttpUtility.HtmlDecode(location)) + "\r\n";
            }

            content += "DESCRIPTION:" + HTMLHelper.StripTags(HttpUtility.HtmlDecode(ValidationHelper.GetString(DataHelper.GetDataRowValue(row, "EventDetails"), "")).Replace("\r\n", "").Replace("<br />", "\\n")) + "\\n\\n" + HTMLHelper.StripTags(HttpUtility.HtmlDecode(ValidationHelper.GetString(DataHelper.GetDataRowValue(row, "EventLocation"), "")).Replace("\r\n", "").Replace("<br />", "\\n")) + "\r\n" +
                 "SUMMARY:" + HttpUtility.HtmlDecode(ValidationHelper.GetString(DataHelper.GetDataRowValue(row, "EventName"), "")) + "\r\n" +
                 "PRIORITY:3\r\n" +
                 "BEGIN:vAlarm\r\n" +
                 "TRIGGER:P0DT0H15M\r\n" +
                 "ACTION:DISPLAY\r\n" +
                 "DESCRIPTION:Reminder\r\n" +
                 "END:vAlarm\r\n" +
                 "END:vEvent\r\n" +
                 "END:vCalendar\r\n";

            // Return byte array
            return System.Text.Encoding.UTF8.GetBytes(content);
        }

        return null;
    }
}
