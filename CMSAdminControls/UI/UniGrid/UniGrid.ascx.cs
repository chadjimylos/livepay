using System;
using System.IO;
using System.Xml;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.URLRewritingEngine;
using CMS.UIControls;
using CMS.Controls;
using CMS.ExtendedControls;
using CMS.SettingsProvider;

public partial class CMSAdminControls_UI_UniGrid_UniGrid : UniGrid, IUniPageable
{
    #region "Variables"

    private Button showButton = null;
    private int rowIndex = 1;
    private bool resetSelection = false;
    private bool visiblePagesSet = false;
    private const int halfPageCountLimit = 1000;

    #endregion


    #region "Events"

    /// <summary>
    /// OnShowButtonClick event handler
    /// </summary>
    public event EventHandler OnShowButtonClick;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets GridView control of UniGrid
    /// </summary>
    public override GridView GridView
    {
        get
        {
            return UniGridView;
        }

        set
        {
            UniGridView = value;
        }
    }


    /// <summary>
    /// Gets or sets UniGrid pager control of UniGrid
    /// </summary>
    public override UniGridPager Pager
    {
        get
        {
            return pagerElem;
        }
    }


    /// <summary>
    /// Gets selected items from UniGrid.
    /// </summary>
    public override ArrayList SelectedItems
    {
        get
        {
            return GetSelectedItems();
        }
        set
        {
            SetSectedItems(value);
        }
    }


    /// <summary>
    /// Gets selected items from UniGrid.
    /// </summary>
    public override ArrayList DeselectedItems
    {
        get
        {
            return GetDeselectedItems();
        }
    }


    /// <summary>
    /// Gets selected items from UniGrid.
    /// </summary>
    public override ArrayList NewlySelectedItems
    {
        get
        {
            return GetNewlySelectedItems();
        }
    }


    /// <summary>
    /// Gets filter placeHolder from Unigrid.
    /// </summary>
    public override PlaceHolder FilterPlaceHolder
    {
        get
        {
            return filter;
        }
    }


    /// <summary>
    /// Gets page size dropdown from Unigrid pager.
    /// </summary>
    public override DropDownList PageSizeDropdown
    {
        get
        {
            return Pager.PageSizeDropdown;
        }
    }

    #endregion


    #region "Page evet methods"

    /// <summary>
    /// Control load event handler
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            Visible = false;
        }
        else
        {
            SetPager();

            if (ImageDirectoryPath == null)
            {
                ImageDirectoryPath = GetImageUrl("Design/Controls/UniGrid/Actions", IsLiveSite, true);
            }

            if (LoadGridDefinition())
            {
                ActionsHash = hidActionsHash;
                // Check whether current request is row action command and if so, raise action
                if (!String.IsNullOrEmpty(hidCmdName.Value) && (Request.Form["__EVENTTARGET"] == UniqueID) && ((Request.Form["__EVENTARGUMENT"] == "UniGridAction")))
                {
                    RaiseAction(hidCmdName.Value, hidCmdArg.Value);
                }

                // Set order by clause
                ProcessSorting();
                // Get data from database and set them to the grid view
                if (FilterByQueryString)
                {
                    if (displayFilter)
                    {
                        SetFilter(true);
                    }
                    else
                    {
                        if (!EventRequest() && !DelayedReload)
                        {
                            ReloadData();
                        }
                    }
                }
                else
                {
                    if (!EventRequest() && !DelayedReload)
                    {
                        ReloadData();
                    }
                }
            }
        }

        // Clear hidden action on load event. If unigrid is invisible, page pre render is not fired
        ClearActions();
    }


    /// <summary>
    /// Control PreRender event handler
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (FilterIsSet)
        {
            // Check for FilteredZeroRowsText
            if ((GridView.Rows.Count == 0) && !String.IsNullOrEmpty(FilteredZeroRowsText))
            {
                // Display filter zero rows text
                lblInfo.Text = FilteredZeroRowsText;
                lblInfo.Visible = true;
                pagerElem.Visible = false;
            }
            else
            {
                lblInfo.Visible = false;
                pagerElem.Visible = true;
            }
        }
        else
        {
            // Check for ZeroRowsText
            if (GridView.Rows.Count == 0)
            {
                if (!HideControlForZeroRows && !String.IsNullOrEmpty(ZeroRowsText))
                {
                    // Display zero rows text
                    lblInfo.Text = ZeroRowsText;
                    lblInfo.Visible = true;
                    pagerElem.Visible = false;
                    // Check additional filter visibility
                    CheckFilterVisibility();
                }
                else
                {
                    lblInfo.Visible = false;
                    pagerElem.Visible = false;
                    filter.Visible = false;
                }
            }
            else
            {
                lblInfo.Visible = false;
                pagerElem.Visible = true;
                // Check additional filter visibility
                CheckFilterVisibility();
            }
        }

        if (Visible && !StopProcessing)
        {
            RegisterCmdScripts();
        }

        if (Pager.CurrentPage > halfPageCountLimit)
        {
            // Enlarge direct page textbox
            TextBox txtPage = ControlsHelper.GetChildControl(pagerElem, typeof(TextBox), "txtPage") as TextBox;
            if (txtPage != null)
            {
                txtPage.Style.Add(HtmlTextWriterStyle.Width, "50px");
            }
        }
    }

    #endregion


    #region "Public methods"

    /// <summary>
    /// Clears UniGrid's information on recently performed action. Under normal circumstances there is no need to perform this action.
    /// However sometimes forcing grid to clear the actions is required.
    /// </summary>
    public void ClearActions()
    {
        // Clear hiddden fields
        hidCmdName.Value = null;
        hidCmdArg.Value = null;
    }


    /// <summary>
    /// Clears all selected items from hidden values.
    /// </summary>
    public void ClearSelectedItems()
    {
        ClearHiddenValues(hidSelection);
    }


    /// <summary>
    /// Loads the grid definition
    /// </summary>
    public bool LoadGridDefinition()
    {
        if (string.IsNullOrEmpty(GridName))
        {
            return false;
        }
        string xmlFilePath = Server.MapPath(GridName);

        // Check the configuration file
        if (!File.Exists(xmlFilePath))
        {
            lblError.Text = String.Format(ResHelper.GetString("unigrid.noxmlfile"), xmlFilePath);
            lblError.Visible = true;
            return false;
        }

        Table filterTable = new Table();
        XmlDocument document = new XmlDocument();
        document.Load(xmlFilePath);
        XmlNode node = document.DocumentElement;

        filter.Controls.Clear();
        UniGridView.Columns.Clear(); // Clear all columns from the grid view
        UniGridView.GridLines = GridLines.Horizontal;

        if (node != null)
        {
            // Load options definition
            XmlNode optionNode = node.SelectSingleNode("options");
            LoadOptionsDefinition(optionNode, filterTable);

            // Load actions definition
            XmlNode actionsNode = node.SelectSingleNode("actions");
            LoadActionsDefinition(actionsNode);

            // Load pager definition
            XmlNode pagerNode = node.SelectSingleNode("pager");
            LoadPagerDefinition(pagerNode);

            // Select list of "column" nodes
            XmlNodeList columnList = node.SelectSingleNode("columns").SelectNodes("column");

            // Get attributes AND filter node of each "column" node
            if (columnList != null)
            {
                foreach (XmlNode columnNode in columnList)
                {
                    // Load column definition
                    LoadColumnDefinition(columnNode, filterTable);
                }
            }

            // Raise load columns event
            RaiseLoadColumns();

            if (displayFilter)
            {
                // Finish filter form with "Show" button
                CreateFilterButton(filterTable);
            }

            if (DataSource == null)
            {
                // Try to get ObjectType from definition
                XmlNode objectTypeNode = node.SelectSingleNode("objecttype");
                if (objectTypeNode != null)
                {
                    // Get object type information
                    LoadObjectTypeDefinition(objectTypeNode);
                }
                else
                {
                    // Get query information
                    XmlNode queryNode = node.SelectSingleNode("query");
                    LoadQueryDefinition(queryNode);
                }
            }
            return true;
        }
        return false;
    }


    /// <summary>
    /// Reloads the grid data
    /// </summary>
    public override void ReloadData()
    {
        // Ensure grid definition before realod data
        if (GridView.Columns.Count == 0)
        {
            LoadGridDefinition();
        }

        RaiseOnBeforeDataReload();
        rowIndex = 1;
        DataSet ds = null;

        int currentPageSize = Pager.CurrentPageSize;
        int currentOffset = currentPageSize * (Pager.CurrentPage - 1);
        if (!Pager.DisplayPager)
        {
            currentOffset = 0;
            currentPageSize = 0;
        }

        // Set order by
        string currentOrder = OrderBy;
        if (!String.IsNullOrEmpty(SortDirect))
        {
            currentOrder = SortDirect;
        }

        // Get Current TOP N
        if (currentPageSize > 0)
        {
            int currentPageIndex = Pager.CurrentPage;
            if (currentPageSize > 0)
            {
                CurrentTopN = currentPageSize * (currentPageIndex + Pager.CurrentPagesGroupSize);
            }
            else
            {
                CurrentTopN = UniGridView.PageSize * (currentPageIndex + Pager.CurrentPagesGroupSize);
            }
        }

        if (CurrentTopN < TopN)
        {
            CurrentTopN = TopN;
        }

        // If first/last button and direct page contol in pager is hidden use current topN for better performance
        if (!Pager.ShowDirectPageControl && !Pager.ShowFirstLastButtons)
        {
            TopN = CurrentTopN;
        }

        // If UniGrid is in ObjectType mode, get the data according to given object type.
        if (!string.IsNullOrEmpty(ObjectType))
        {
            IInfoObject objectType = CMSObjectHelper.GetObject(ObjectType);

            // Get the result set
            ds = objectType.GetModifiedFrom(DateTimeHelper.ZERO_TIME, Columns, CompleteWhereCondition, currentOrder, TopN, currentOffset, currentPageSize, ref pagerForceNumberOfResults, false, null);
        }
        // If datasource for unigrid is query (not dataset), then execute query
        else if (!string.IsNullOrEmpty(Query))
        {
            // Reload the data with current parameters
            GeneralConnection con = ConnectionHelper.GetConnection();

            ds = con.ExecuteQuery(Query, QueryParameters, CompleteWhereCondition, currentOrder, TopN, Columns, currentOffset, currentPageSize, ref pagerForceNumberOfResults);
        }
        // External dataset is used
        else
        {
            ds = RaiseDataReload();
        }

        if (ds == null)
        {
            // Bind to empty data source
            ds = new DataSet();
            ds.Tables.Add();
            UniGridView.DataSource = ds;
        }
        else
        {
            UniGridView.DataSource = ds;
            
            // Sort unigrid data source if external DataSet is used
            if (string.IsNullOrEmpty(Query) && string.IsNullOrEmpty(ObjectType) && !DataSourceIsSorted)
            {
                SortUniGridDataSource();
            }
        }

        SetUnigridControls(currentOffset, currentPageSize);

        RaiseOnAfterDataReload();

        // Check if datasource is loaded
        if (DataHelper.DataSourceIsEmpty(GridView.DataSource) && (pagerElem.CurrentPage > 1))
        {
            pagerElem.UniPager.CurrentPage = 1;
            ReloadData();
        }

        UniGridView.DataBind();

        mRowsCount = DataHelper.GetItemsCount(UniGridView.DataSource);

        CheckFilterVisibility();
    }


    /// <summary>
    /// Returns where condition from unigrid filters
    /// </summary>
    public string GetFilter()
    {
        return GetFilter(false);
    }


    /// <summary>
    /// Returns where condition from unigrid filters for DataTable.Select
    /// </summary>
    public string GetDataTableFilter()
    {
        return GetFilter(true);
    }


    /// <summary>
    /// Returns where condition from unigrid filters
    /// </summary>
    public string GetFilter(bool isDataTable)
    {
        string where = string.Empty;
        // Count of the conditions in the 'where clause'
        int whereConditionCount = 0;

        // Process all filter fields
        foreach (object[] filterField in mFilterFields)
        {
            string filterFormat = (string)filterField[1];
            // AND in 'where clause'  
            string andExpression;
            Control mainControl = (Control)filterField[2];
            Control valueControl = (Control)filterField[3];
            string filterPath = (string)filterField[4];

            if (String.IsNullOrEmpty(filterPath))
            {
                if (mainControl is DropDownList)
                {
                    // Dropdown list filter
                    DropDownList ddlistControl = (DropDownList)mainControl;
                    TextBox txtControl = (TextBox)valueControl;

                    string textboxValue = txtControl.Text;
                    string textboxID = txtControl.ID;

                    // Empty field -> no filter is set for this field
                    if (textboxValue != "")
                    {
                        string op = ddlistControl.SelectedValue;
                        string value = textboxValue.Replace("\'", "''");
                        string columnName = ddlistControl.ID;

                        // Format {0} = column name, {1} = operator, {2} = value, {3} = default condition
                        string defaultFormat = null;

                        if (textboxID.EndsWith("TextValue"))
                        {
                            switch (op.ToLower())
                            {
                                // LIKE operators
                                case "like":
                                case "not like":
                                    defaultFormat = isDataTable ? "[{0}] {1} '%{2}%'" : "[{0}] {1} N'%{2}%'";
                                    break;

                                // Standard operators
                                default:
                                    defaultFormat = isDataTable ? "[{0}] {1} '{2}'" : "[{0}] {1} N'{2}'";
                                    break;
                            }
                        }
                        else // textboxID.EndsWith("NumberValue")
                        {
                            if (ValidationHelper.IsDouble(value) || ValidationHelper.IsInteger(value))
                            {
                                defaultFormat = "[{0}] {1} {2}";
                            }
                        }

                        if (!String.IsNullOrEmpty(defaultFormat))
                        {
                            string defaultCondition = String.Format(defaultFormat, columnName, op, value);

                            string condition = defaultCondition;
                            if (filterFormat != null)
                            {
                                condition = String.Format(filterFormat, columnName, op, value, defaultCondition);
                            }

                            andExpression = (whereConditionCount > 0 ? " AND " : string.Empty);

                            // ddlistControl.ID                 - column name
                            // ddlistControl.SelectedValue      - condition option
                            // textboxSqlValue                  - condition value                        
                            where += andExpression + "(" + condition + ")";
                            whereConditionCount++;
                        }
                    }

                    // Prepare query string
                    if (FilterByQueryString)
                    {
                        queryStringHashTable[ddlistControl.ID] = ddlistControl.SelectedValue + ";" + textboxValue;
                    }
                }
                else if (valueControl is DropDownList)
                {
                    // Checkbox filter
                    DropDownList currentControl = (DropDownList)valueControl;
                    string value = currentControl.SelectedValue;
                    if (value != "")
                    {
                        andExpression = (whereConditionCount > 0) ? " AND" : "";
                        where += andExpression + " " + currentControl.ID + " = " + value;
                        whereConditionCount++;
                    }

                    // Prepare query string
                    if (FilterByQueryString)
                    {
                        queryStringHashTable[currentControl.ID] = ";" + value;
                    }
                }
            }
            // If is defined filter path
            else
            {
                CMSAbstractBaseFilterControl customFilter = (CMSAbstractBaseFilterControl)filterField[3];
                string customWhere = customFilter.WhereCondition;
                if (!String.IsNullOrEmpty(customWhere))
                {
                    andExpression = (whereConditionCount > 0) ? " AND " : "";
                    where += andExpression + customWhere;
                    whereConditionCount++;
                }

                // Prepare query string
                if (FilterByQueryString && RequestHelper.IsPostBack())
                {
                    queryStringHashTable[customFilter.ID] = customFilter.Value;
                }
            }
        }

        return where;
    }


    /// <summary>
    /// Uncheck all checkboxes in selection column
    /// </summary>
    public void ResetSelection()
    {
        hidSelection.Value = String.Empty;
        hidNewSelection.Value = String.Empty;
        hidDeSelection.Value = String.Empty;
        resetSelection = true;
    }

    #endregion


    #region "UniGrid events"

    /// <summary>
    /// Process data from filter
    /// </summary>
    protected void ShowButton_Click(object sender, EventArgs e)
    {
        if (OnShowButtonClick != null)
        {
            OnShowButtonClick(sender, e);
        }
        Pager.UniPager.CurrentPage = 1;
        SetFilter(!DelayedReload);
    }


    protected void pageSizeDropdown_SelectedIndexChanged(object sender, EventArgs e)
    {
        RaisePageSizeChanged();
    }


    protected void UniGridView_Sorting(object sender, EventArgs e)
    {
        RaiseBeforeSorting(sender, e);
    }


    protected void UniGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        // No action is required
    }


    /// <summary>
    /// After data bound event
    /// </summary>
    protected void UniGridView_DataBound(object sender, EventArgs e)
    {
        // Set actions hash into hidden field
        SetActionsHash();

        SetPager();

        // Call page binding event
        if (OnPageBinding != null)
        {
            OnPageBinding(this, null);
        }
    }


    protected void UniGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        // Deleting is handled in custom method
    }


    protected void UniGridView_RowEditing(object sender, GridViewEditEventArgs e)
    {
        // Editing is handled in custom method
    }


    protected void UniGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        // Updating is handled in custom method
    }


    protected void UniGridView_RowCreating(object sender, GridViewRowEventArgs e)
    {
        // If row type is header
        if (e.Row.RowType == DataControlRowType.Header)
        {
            // Parse the sort expression
            string sort = SortDirect.ToLower().Trim().TrimStart('[');
            if (sort.StartsWith("cast("))
            {
                sort = sort.Substring(5).TrimStart('[');
            }

            Match sortMatch = OrderByRegex.Match(sort);
            string sortColumn = null;
            string sortDirection = null;
            if (sortMatch.Success)
            {
                // Get column name
                if (sortMatch.Groups[1].Success)
                {
                    sortColumn = sortMatch.Groups[1].Value;
                }
                // Get sort direction
                sortDirection = sortMatch.Groups[2].Success ? sortMatch.Groups[2].Value : "asc";
            }
            else
            {
                // Get column name from sort expression
                int space = sort.IndexOfAny(new char[] { ' ', ',' });
                sortColumn = space > -1 ? sort.Substring(0, space) : sort;
                sortDirection = "asc";
            }

            // Prepare the columns
            foreach (TableCell Cell in e.Row.Cells)
            {
                // If there is some sorting expression
                DataControlFieldCell dataField = (DataControlFieldCell)Cell;
                if (!DataHelper.IsEmpty(dataField.ContainingField.SortExpression))
                {
                    // If actual sorting expressions is this cell
                    if (String.Equals(sortColumn, dataField.ContainingField.SortExpression, StringComparison.InvariantCultureIgnoreCase))
                    {
                        // Initialize sort arrow
                        Image sortArrow = new Image();
                        sortArrow.ID = "imgSort_" + Guid.NewGuid();
                        sortArrow.EnableViewState = false;
                        sortArrow.AlternateText = ResHelper.GetString("general.sort");
                        sortArrow.ImageUrl = (sortDirection == "desc") ? GetActionImage("SortDown.png") : GetActionImage("SortUp.png");

                        if (DataHelper.IsEmpty(Cell.Text))
                        {
                            if ((Cell.Controls.Count != 0) && (Cell.Controls[0] != null))
                            {
                                // Add original text
                                Cell.Controls[0].Controls.Add(new LiteralControl(((LinkButton)(Cell.Controls[0])).Text));
                                // Add one space before image
                                Cell.Controls[0].Controls.Add(new LiteralControl("&nbsp;"));
                                Cell.Controls[0].Controls.Add(sortArrow);
                            }
                            else
                            {
                                // Add one space before image
                                Cell.Controls.Add(new LiteralControl("&nbsp;"));
                                Cell.Controls.Add(sortArrow);
                            }
                        }
                    }
                }
            }
        }
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.CssClass = (rowIndex % 2 == 0) ? "OddRow" : "EvenRow";
            rowIndex++;
        }
        else if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.CssClass = "UniGridFooter";
        }
        else if (e.Row.RowType == DataControlRowType.Pager)
        {
            e.Row.CssClass = "UniGridPager";
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Sets unigrid controls.
    /// </summary>
    private void SetUnigridControls(int currentOffset, int currentPageSize)
    {
        filter.Visible = displayFilter;

        // Indicates whether unigrid datasource is empty or not
        isEmpty = DataHelper.DataSourceIsEmpty(UniGridView.DataSource);

        if (isEmpty)
        {
            // Try to reload data for previous page if action was used and no data loaded (mostly delete)
            if (onActionUsed && Pager.CurrentPage > 1)
            {
                Pager.UniPager.CurrentPage = Pager.CurrentPage - 1;
                ReloadData();
            }
            else if (HideControlForZeroRows && (WhereClause == ""))
            {
                // Hide filter
                filter.Visible = false;
            }
        }
        else
        {
            // Disable GridView paging because UniGridPager will provide paging
            UniGridView.AllowPaging = false;

            // Process paging if pager is displayed
            if (Pager.DisplayPager)
            {
                // Get items count
                int itemsCount = DataHelper.GetItemsCount(UniGridView.DataSource);
                int pageSize = Pager.CurrentPageSize;

                if ((pageSize > 0) && (itemsCount > pageSize))
                {
                    // Get data table from datasource
                    DataTable dt = DataHelper.GetDataTable(UniGridView.DataSource);
                    if (dt != null)
                    {
                        DataTable newTable = dt.Clone();
                        int lastOffset = Math.Min(currentOffset + currentPageSize, dt.Rows.Count);
                        // Get only rows for current page
                        for (int i = currentOffset; i < lastOffset; i++)
                        {
                            DataRow dataRow = newTable.NewRow();

                            dataRow.ItemArray = dt.DefaultView[i].Row.ItemArray;
                            newTable.Rows.Add(dataRow);
                        }

                        UniGridView.DataSource = newTable;
                        PagerForceNumberOfResults = itemsCount;
                    }
                }

                if (dataSourceIsUsed && (itemsCount > pageSize))
                {
                    PagerForceNumberOfResults = itemsCount;
                }
                if (currentPageSize > 0)
                {
                    if (!visiblePagesSet)
                    {
                        Pager.VisiblePages = (((currentOffset / currentPageSize) + 1)) > halfPageCountLimit ? 5 : 10;
                    }

                    Pager.DirectPageControlID = ((float)PagerForceNumberOfResults / pageSize > 20.0f) ? "txtPage" : "drpPage";
                    // Save direct page control id in viewstate
                    ViewState["DirectPageControlID"] = Pager.DirectPageControlID;
                }
            }
        }
    }


    /// <summary>
    /// Load options definition from XML.
    /// </summary>
    /// <param name="optionNode">XML options definition node</param>
    /// <param name="filterTable">Table for filter</param>
    private void LoadOptionsDefinition(XmlNode optionNode, Table filterTable)
    {
        // Create filter table according to the key value "DisplayFilter"            
        displayFilter = (GetKeyValue(optionNode, "DisplayFilter").ToLower() == "true") ? true : false;
        if (displayFilter)
        {
            filter.Controls.Add(filterTable);
        }

        // Filter limit
        int filterLimit = ValidationHelper.GetInteger(GetKeyValue(optionNode, "FilterLimit"), -1);
        if (filterLimit > -1)
        {
            FilterLimit = filterLimit;
        }

        // Display sort direction images
        string showDirection = GetKeyValue(optionNode, "ShowSortDirection");
        if ((showDirection != "") && (showDirection.ToLower() == "false"))
        {
            showSortDirection = false;
        }
        else
        {
            showSortDirection = true;
        }

        // Display selection column with checkboxes
        showSelection = (GetKeyValue(optionNode, "ShowSelection").ToLower() == "true") ? true : false;
        if (showSelection)
        {
            TemplateField chkColumn = new TemplateField();

            CheckBox headerBox = new CheckBox();
            headerBox.ID = "headerBox";

            CheckBox itemBox = new CheckBox();
            itemBox.ID = "itemBox";
            // Set selection argument
            itemBox.Attributes["selectioncolumn"] = GetKeyValue(optionNode, "SelectionColumn");

            GridViewTemplate headerTemplate = new GridViewTemplate(ListItemType.Header, headerBox);
            GridViewTemplate itemTemplate = new GridViewTemplate(ListItemType.Item, itemBox);

            chkColumn.HeaderTemplate = headerTemplate;
            chkColumn.ItemTemplate = itemTemplate;
            UniGridView.Columns.Add(chkColumn);
        }

        // Get pagesize options
        string pageSize = GetKeyValue(optionNode, "PageSize");
        if (!String.IsNullOrEmpty(pageSize))
        {
            Pager.PageSizeOptions = pageSize;
        }

        // Set pagging acording to the key value "DisplayPageSizeDropdown"                
        string displayPageSize = GetKeyValue(optionNode, "DisplayPageSizeDropdown");
        if (!String.IsNullOrEmpty(displayPageSize))
        {
            Pager.ShowPageSize = ValidationHelper.GetBoolean(displayPageSize, true);
        }
    }


    /// <summary>
    /// Load actions column definition from XML.
    /// </summary>
    /// <param name="actionsNode">XML actions definition node</param>
    private void LoadActionsDefinition(XmlNode actionsNode)
    {
        if (actionsNode != null)
        {
            TemplateField ckhColumn = new TemplateField(); // Custom template field of the grid view

            // Ensure width of the column
            if (actionsNode.Attributes["width"] != null)
            {
                ckhColumn.ItemStyle.Width = new Unit(actionsNode.Attributes["width"].Value);
            }

            // Show header?
            XmlAttribute showHeader = actionsNode.Attributes["showheader"];
            if ((showHeader == null) || (showHeader.Value.ToLower() == "true"))
            {
                // Fill in the custom template field
                ckhColumn.HeaderTemplate = new GridViewTemplate(ListItemType.Header, ResHelper.GetString("unigrid.actions"));
            }

            GridViewTemplate actionsTemplate = new GridViewTemplate(ListItemType.Item, actionsNode, ImageDirectoryPath, DefaultImageDirectoryPath);
            actionsTemplate.Page = Page;
            ckhColumn.ItemTemplate = actionsTemplate;

            ckhColumn.ItemStyle.Wrap = false;

            // Add custom column to grid view
            UniGridView.Columns.Add(ckhColumn);

            actionsTemplate.OnExternalDataBound += RaiseExternalDataBound;
        }
    }


    /// <summary>
    /// Load unigrid pager definition from XML.
    /// </summary>
    /// <param name="pagerNode">XML pager definition node</param>
    private void LoadPagerDefinition(XmlNode pagerNode)
    {
        if (pagerNode != null)
        {
            string displayPager = GetKeyValue(pagerNode, "DisplayPager");
            if (!String.IsNullOrEmpty(displayPager))
            {
                Pager.DisplayPager = ValidationHelper.GetBoolean(displayPager, true);
            }

            string pageSizeOptions = GetKeyValue(pagerNode, "PageSizeOptions");
            if (!String.IsNullOrEmpty(pageSizeOptions))
            {
                Pager.PageSizeOptions = pageSizeOptions;
            }

            string showDirectPageControl = GetKeyValue(pagerNode, "ShowDirectPageControl");
            if (!String.IsNullOrEmpty(showDirectPageControl))
            {
                Pager.ShowDirectPageControl = ValidationHelper.GetBoolean(showDirectPageControl, true);
            }

            string showFirstLastButtons = GetKeyValue(pagerNode, "ShowFirstLastButtons");
            if (!String.IsNullOrEmpty(showFirstLastButtons))
            {
                Pager.ShowFirstLastButtons = ValidationHelper.GetBoolean(showFirstLastButtons, true);
            }

            string showPageSize = GetKeyValue(pagerNode, "ShowPageSize");
            if (!String.IsNullOrEmpty(showPageSize))
            {
                Pager.ShowPageSize = ValidationHelper.GetBoolean(showPageSize, true);
            }

            string showPreviousNextButtons = GetKeyValue(pagerNode, "ShowPreviousNextButtons");
            if (!String.IsNullOrEmpty(showPreviousNextButtons))
            {
                Pager.ShowPreviousNextButtons = ValidationHelper.GetBoolean(showPreviousNextButtons, true);
            }

            string showPreviousNextPageGroup = GetKeyValue(pagerNode, "ShowPreviousNextPageGroup");
            if (!String.IsNullOrEmpty(showPreviousNextPageGroup))
            {
                Pager.ShowPreviousNextPageGroup = ValidationHelper.GetBoolean(showPreviousNextPageGroup, true);
            }

            string visiblePages = GetKeyValue(pagerNode, "VisiblePages");
            if (!String.IsNullOrEmpty(visiblePages))
            {
                int pages = ValidationHelper.GetInteger(visiblePages, 0);
                if (pages > 0)
                {
                    Pager.VisiblePages = pages;
                    visiblePagesSet = true;
                }
            }

            string defaultPageSize = GetKeyValue(pagerNode, "DefaultPageSize");
            // Try to get page size from request
            string selectedPageSize = Request.Form[Pager.PageSizeDropdown.UniqueID];
            int pageSize = 0;
            if (selectedPageSize != null)
            {
                pageSize = ValidationHelper.GetInteger(selectedPageSize, 0);
            }
            else if (!String.IsNullOrEmpty(defaultPageSize))
            {
                pageSize = ValidationHelper.GetInteger(defaultPageSize, 0);
            }
            if ((pageSize > 0) || (pageSize == -1))
            {
                Pager.DefaultPageSize = pageSize;
            }
        }
        // Set direct page control id from viewstate
        if (ViewState["DirectPageControlID"] != null)
        {
            Pager.DirectPageControlID = ViewState["DirectPageControlID"].ToString();
        }
    }


    /// <summary>
    /// Load single column definition from XML.
    /// </summary>
    /// <param name="columnNode">Node to use</param>
    /// <param name="filterTable">Table for filter</param>
    private void LoadColumnDefinition(XmlNode columnNode, Table filterTable)
    {
        string columnSource = null, columnCaption = null; // Attributes of the "column" node
        DataControlField field = null;

        XmlAttribute href = columnNode.Attributes["href"];
        XmlAttribute externalSourceName = columnNode.Attributes["externalsourcename"];
        XmlAttribute localize = columnNode.Attributes["localize"];
        XmlAttribute icon = columnNode.Attributes["icon"];
        XmlNode tooltipNode = columnNode.SelectSingleNode("tooltip");
        XmlAttribute action = columnNode.Attributes["action"];
        XmlAttribute maxlength = columnNode.Attributes["maxlength"];

        // Process the column type Hyperlink or BoundColumn based on the parameters
        if ((href != null) || (externalSourceName != null) || (localize != null) || (icon != null) || (tooltipNode != null) || (action != null) || (maxlength != null))
        {
            ExtendedBoundField linkColumn = new ExtendedBoundField();
            field = linkColumn;

            // Attribute "source"
            XmlAttribute source = columnNode.Attributes["source"];
            if (source != null)
            {
                columnSource = source.Value;
                linkColumn.DataField = columnSource;
                XmlAttribute allowSorting = columnNode.Attributes["allowsorting"];
                if ((allowSorting == null) || ValidationHelper.GetBoolean(allowSorting.Value, true))
                {
                    XmlAttribute sort = columnNode.Attributes["sort"];
                    if (sort != null)
                    {
                        linkColumn.SortExpression = sort.Value;
                    }
                    else
                    {
                        if (columnSource.ToLower() != ExtendedBoundField.ALL_DATA.ToLower())
                        {
                            linkColumn.SortExpression = columnSource;
                        }
                    }
                }
            }

            // Action parameters
            if (action != null)
            {
                linkColumn.Action = action.Value;

                // Action parameters
                XmlAttribute commandArgument = columnNode.Attributes["commandargument"];
                if (commandArgument != null)
                {
                    linkColumn.CommandArgument = commandArgument.Value;
                }
            }

            // Action parameters
            XmlAttribute parameters = columnNode.Attributes["parameters"];
            if (parameters != null)
            {
                linkColumn.ActionParameters = parameters.Value;
            }

            // Navigate URL
            if (href != null)
            {
                linkColumn.NavigateUrl = href.Value;
            }

            // External source
            if (externalSourceName != null)
            {
                linkColumn.ExternalSourceName = externalSourceName.Value;
                linkColumn.OnExternalDataBound += RaiseExternalDataBound;
            }

            // Localize strings?
            if (localize != null)
            {
                linkColumn.LocalizeStrings = ValidationHelper.GetBoolean(localize.Value, false);
            }

            // Style
            XmlAttribute style = columnNode.Attributes["style"];
            if (style != null)
            {
                linkColumn.Style = style.Value;
            }

            // Icon
            if (icon != null)
            {
                if (linkColumn.DataField == "")
                {
                    linkColumn.DataField = ExtendedBoundField.ALL_DATA;
                }
                linkColumn.Icon = GetActionImage(icon.Value);
            }

            // Max length
            if (maxlength != null)
            {
                linkColumn.MaxLength = ValidationHelper.GetInteger(maxlength.Value, 0);
            }

            // Process "tooltip" node
            if (tooltipNode != null)
            {
                XmlAttribute tooltipSource = tooltipNode.Attributes["source"];
                XmlAttribute tooltipExternalSource = tooltipNode.Attributes["externalsourcename"];
                XmlAttribute tooltipWidth = tooltipNode.Attributes["width"];
                XmlAttribute tooltipEncode = tooltipNode.Attributes["encode"];

                // If there is some tooltip register TooltipScript
                if ((tooltipSource != null) || (tooltipExternalSource != null))
                {
                    ScriptHelper.RegisterTooltip(Page);
                }

                // Tooltip source
                if (tooltipSource != null)
                {
                    linkColumn.TooltipSourceName = tooltipSource.Value;
                }

                // Tooltip external source
                if (tooltipExternalSource != null)
                {
                    linkColumn.TooltipExternalSourceName = tooltipExternalSource.Value;
                }

                // Tooltip width
                if (tooltipWidth != null)
                {
                    linkColumn.TooltipWidth = tooltipWidth.Value;
                }

                // Encode tooltip
                if (tooltipEncode != null)
                {
                    linkColumn.TooltipEncode = ValidationHelper.GetBoolean(tooltipEncode.Value, true);
                }
            }
        }
        else
        {
            BoundField userColumn = new BoundField(); // Custom column of the grid view
            field = userColumn;

            // Attribute "source"
            XmlAttribute source = columnNode.Attributes["source"];
            if (source != null)
            {
                columnSource = source.Value;
                userColumn.DataField = columnSource;

                // Allow sorting
                XmlAttribute allowSorting = columnNode.Attributes["allowsorting"];
                if ((allowSorting == null) || ValidationHelper.GetBoolean(allowSorting.Value, true))
                {
                    if (columnSource.ToLower() != ExtendedBoundField.ALL_DATA.ToLower())
                    {
                        userColumn.SortExpression = columnSource;
                    }
                    else
                    {
                        // Sort
                        XmlAttribute sort = columnNode.Attributes["sort"];
                        if (sort != null)
                        {
                            userColumn.SortExpression = sort.Value;
                        }
                    }
                }
            }
        }

        field.HeaderStyle.Wrap = false;

        // Caption
        XmlAttribute caption = columnNode.Attributes["caption"];
        if (caption != null)
        {
            columnCaption = caption.Value;
            columnCaption = ResHelper.GetResourceName(columnCaption);
            field.HeaderText = ResHelper.GetString(columnCaption);
        }

        // Width
        XmlAttribute width = columnNode.Attributes["width"];
        if (width != null)
        {
            field.ItemStyle.Width = new Unit(width.Value);
        }

        // Visible
        XmlAttribute visible = columnNode.Attributes["visible"];
        if (visible != null)
        {
            field.Visible = ValidationHelper.GetBoolean(visible.Value, true);
        }

        // Is text?
        XmlAttribute isText = columnNode.Attributes["istext"];
        if ((isText != null) && ValidationHelper.GetBoolean(isText.Value, false))
        {
            TextColumns.Add(columnSource.ToLower());
        }

        // Wrap?
        XmlAttribute wrap = columnNode.Attributes["wrap"];
        if (wrap != null)
        {
            if (wrap.Value.ToLower() == "false")
            {
                field.ItemStyle.Wrap = false;
            }
        }


        // Process "filter" node
        if (displayFilter)
        {
            // Filter
            XmlNode filterNode = columnNode.SelectSingleNode("filter");
            if (filterNode != null)
            {
                // Type
                string filterType = null;
                XmlAttribute type = filterNode.Attributes["type"];
                if (type != null)
                {
                    filterType = type.Value;
                }

                // Source
                string filterSource = null;
                XmlAttribute source = filterNode.Attributes["source"];
                filterSource = (source != null) ? source.Value : columnSource;

                // Format
                string filterFormat = null;
                XmlAttribute format = filterNode.Attributes["format"];
                if (format != null)
                {
                    filterFormat = format.Value;
                }

                // Path 
                string filterPath = null;
                XmlAttribute path = filterNode.Attributes["path"];
                if (path != null)
                {
                    filterPath = path.Value;
                }

                object option = null;
                object value = null;
                if (FilterByQueryString)
                {
                    if (String.IsNullOrEmpty(filterPath))
                    {
                        string values = QueryHelper.GetString(columnSource, null);
                        if (!string.IsNullOrEmpty(values))
                        {
                            string[] pair = values.Split(';');
                            option = pair[0];
                            value = pair[1];
                        }
                    }
                    else
                    {
                        value = QueryHelper.GetString(columnSource, null);
                    }
                }

                AddFilterField(filterType, filterPath, filterFormat, filterSource, ResHelper.GetString(columnCaption), filterTable, option, value);
            }
        }

        // Add custom column to gridview
        UniGridView.Columns.Add(field);
    }


    /// <summary>
    /// Load query definition from XML.
    /// </summary>
    /// <param name="objectTypeNode">XML query definition node</param>
    private void LoadObjectTypeDefinition(XmlNode objectTypeNode)
    {
        if (objectTypeNode != null)
        {
            ObjectType = objectTypeNode.Attributes["name"].Value;

            // Sets the columns property if columns are defined
            XmlAttribute columns = objectTypeNode.Attributes["columns"];
            if (columns != null)
            {
                Columns = DataHelper.GetNotEmpty(columns.Value, Columns);
            }
        }
    }


    /// <summary>
    /// Load query definition from XML.
    /// </summary>
    /// <param name="queryNode">XML query definition node</param>
    private void LoadQueryDefinition(XmlNode queryNode)
    {
        if (queryNode != null)
        {
            Query = queryNode.Attributes["name"].Value;

            // Sets the columns property if columns are defined
            XmlAttribute columns = queryNode.Attributes["columns"];
            if (columns != null)
            {
                Columns = DataHelper.GetNotEmpty(columns.Value, Columns);
            }

            // Load the query parameters
            XmlNodeList parameters = queryNode.SelectNodes("parameter");
            if ((parameters != null) && (parameters.Count > 0))
            {
                object[,] newParams = new object[parameters.Count, 3];
                const int paramIndex = 0;

                // Process all parameters
                foreach (XmlNode param in parameters)
                {
                    newParams[paramIndex, 0] = param.Attributes["name"].Value;
                    switch (param.Attributes["type"].Value.ToLower())
                    {
                        case "string":
                            newParams[paramIndex, 1] = param.Attributes["value"].Value;
                            break;

                        case "int":
                            newParams[paramIndex, 1] = ValidationHelper.GetInteger(param.Attributes["value"].Value, 0);
                            break;

                        case "double":
                            newParams[paramIndex, 1] = Convert.ToDouble(param.Attributes["value"].Value);
                            break;

                        case "bool":
                            newParams[paramIndex, 1] = Convert.ToBoolean(param.Attributes["value"].Value);
                            break;
                    }

                    QueryParameters = newParams;
                }
            }
        }
    }


    /// <summary>
    /// Returns attribute value of the specific option (specific key node)
    /// </summary>
    /// <param name="optionNode">option node containing key nodes</param>
    /// <param name="keyName">Attribute name of the specific key node</param>
    /// <returns>Attribute value of the specific key node</returns>
    private static string GetKeyValue(XmlNode optionNode, string keyName)
    {
        string keyValue = "";

        if (optionNode != null)
        {
            XmlNode keyNode = optionNode.SelectSingleNode("key[@name='" + keyName + "']");
            if (keyNode != null)
            {
                keyValue = keyNode.Attributes["value"].Value;
            }
        }

        return keyValue;
    }


    /// <summary>
    /// Add filter field to the filter table
    /// </summary>
    /// <param name="fieldType">Field type</param>
    /// <param name="fieldPath">Filter contol file path. If not starts with ~/ default directory '~/CMSAdminControls/UI/UniGrid/Filters/' will be inserted at begining</param>
    /// <param name="filterFormat">Filter format string</param>
    /// <param name="fieldSourceName">Source field name</param>
    /// <param name="fieldDisplayName">Field display name</param>
    /// <param name="filterTable">Filter table</param>
    /// <param name="filterOption">Filter option</param>
    /// <param name="filterValue">Filter value</param>
    private void AddFilterField(string fieldType, string fieldPath, string filterFormat, string fieldSourceName, string fieldDisplayName, Table filterTable, object filterOption, object filterValue)
    {
        TableRow tRow = new TableRow();

        TableCell tCellName = new TableCell();
        TableCell tCellOption = new TableCell();
        TableCell tCellValue = new TableCell();

        // Ensure fieldSourceName is Javascript valid
        fieldSourceName = fieldSourceName.Replace(ALL, "__ALL__");

        // Label
        Label textName = new Label();
        textName.Text = fieldDisplayName + ":";
        textName.ID = fieldSourceName + "Name";
        textName.EnableViewState = false;
        tCellName.Controls.Add(textName);
        tRow.Cells.Add(tCellName);

        // Filter option
        string option = null;
        if (filterOption != null)
        {
            option = ValidationHelper.GetString(filterOption, null);
        }

        // Filter value
        string value = null;
        if (filterValue != null)
        {
            value = ValidationHelper.GetString(filterValue, null);
        }

        // Filter definition
        // 0 = Type
        // 1 = Format
        // 2 = Option control
        // 3 = Filter value control
        // 4 = FilterID
        object[] filterDefinition = new object[5];
        filterDefinition[0] = fieldType;
        filterDefinition[1] = filterFormat;

        // If no filter path us default filter
        if (String.IsNullOrEmpty(fieldPath))
        {
            switch (fieldType)
            {
                // Text filter
                case "text":
                    {
                        DropDownList textOptionFilterField = new DropDownList();
                        textOptionFilterField.Items.Add(new ListItem("LIKE", "LIKE"));
                        textOptionFilterField.Items.Add(new ListItem("NOT LIKE", "NOT LIKE"));
                        textOptionFilterField.Items.Add(new ListItem("=", "="));
                        textOptionFilterField.Items.Add(new ListItem("<>", "<>"));
                        textOptionFilterField.CssClass = "ContentDropdown";
                        textOptionFilterField.ID = fieldSourceName;

                        // Select filter option
                        try
                        {
                            textOptionFilterField.SelectedValue = option;
                        }
                        catch
                        {
                        }

                        tCellOption.Controls.Add(textOptionFilterField);
                        tRow.Cells.Add(tCellOption);

                        // Add text field
                        TextBox textValueFilterField = new TextBox();
                        textValueFilterField.ID = fieldSourceName + "TextValue";
                        textValueFilterField.Text = value;
                        tCellValue.Controls.Add(textValueFilterField);
                        tRow.Cells.Add(tCellValue);

                        filterDefinition[2] = textOptionFilterField;
                        filterDefinition[3] = textValueFilterField;
                    }
                    break;

                // Boolean filter
                case "bool":
                    {
                        DropDownList booleanOptionFilterField = new DropDownList();
                        booleanOptionFilterField.Items.Add(new ListItem(ResHelper.GetString("general.selectall"), ""));
                        booleanOptionFilterField.Items.Add(new ListItem(ResHelper.GetString("general.yes"), "1"));
                        booleanOptionFilterField.Items.Add(new ListItem(ResHelper.GetString("general.no"), "0"));
                        booleanOptionFilterField.CssClass = "ContentDropdown";
                        booleanOptionFilterField.ID = fieldSourceName;

                        // Select filter option
                        try
                        {
                            booleanOptionFilterField.SelectedValue = value;
                        }
                        catch
                        {
                        }

                        tCellValue.Controls.Add(booleanOptionFilterField);
                        tRow.Cells.Add(tCellValue);

                        filterDefinition[3] = booleanOptionFilterField;
                    }
                    break;

                // Integer filter
                case "integer":
                case "double":
                    {
                        DropDownList numberOptionFilterField = new DropDownList();
                        numberOptionFilterField.Items.Add(new ListItem("=", "="));
                        numberOptionFilterField.Items.Add(new ListItem("<>", "<>"));
                        numberOptionFilterField.Items.Add(new ListItem("<", "<"));
                        numberOptionFilterField.Items.Add(new ListItem(">", ">"));
                        numberOptionFilterField.CssClass = "ContentDropdown";
                        numberOptionFilterField.ID = fieldSourceName;

                        // Select filter option
                        try
                        {
                            numberOptionFilterField.SelectedValue = option;
                        }
                        catch
                        {
                        }

                        // Add filter field
                        tCellOption.Controls.Add(numberOptionFilterField);
                        tRow.Cells.Add(tCellOption);

                        TextBox numberValueFilterField = new TextBox();
                        numberValueFilterField.ID = fieldSourceName + "NumberValue";
                        numberValueFilterField.Text = value;
                        numberValueFilterField.EnableViewState = false;

                        RangeValidator numberValidator = new RangeValidator();
                        numberValidator.ID = "numberValidator" + numberValueFilterField.ID;
                        numberValidator.Type = (fieldType == "integer") ? ValidationDataType.Integer : ValidationDataType.Double;
                        numberValidator.MinimumValue = "0";
                        numberValidator.MaximumValue = "1000";
                        numberValidator.ControlToValidate = numberValueFilterField.ID;
                        numberValidator.ErrorMessage = "Number type must be " + fieldType + " less than " + numberValidator.MaximumValue + "!";
                        numberValidator.EnableViewState = false;

                        tCellValue.Controls.Add(numberValueFilterField);
                        tCellValue.Controls.Add(numberValidator);
                        tRow.Cells.Add(tCellValue);

                        filterDefinition[2] = numberOptionFilterField;
                        filterDefinition[3] = numberValueFilterField;
                    }
                    break;
            }
        }
        // Else if filter path is defined use custom filter
        else
        {
            string path = fieldPath.StartsWith("~/") ? fieldPath : FilterDirectoryPath + fieldPath.TrimStart('/');
            CMSAbstractBaseFilterControl filterControl = LoadControl(path) as CMSAbstractBaseFilterControl;

            if (filterControl != null)
            {
                filterControl.ID = fieldSourceName;
                tCellValue.Controls.Add(filterControl);
            }
            tCellValue.Attributes["colspan"] = "2";
            tRow.Cells.Add(tCellValue);
            if (filterControl != null)
            {
                filterControl.FilteredControl = this;
                if (!RequestHelper.IsPostBack())
                {
                    filterControl.Value = value;
                }

                filterDefinition[3] = filterControl;
                filterDefinition[4] = filterControl.ID;
            }
        }

        mFilterFields.Add(filterDefinition);

        filterTable.Rows.Add(tRow);
    }


    /// <summary>
    /// Create filter show button.
    /// </summary>
    private void CreateFilterButton(Table filterTable)
    {
        // Add button to the bottom of the filter table
        showButton = new CMSButton();
        Literal ltlBreak = new Literal();
        TableRow tRow = new TableRow();
        TableCell tCell = new TableCell();
        showButton.ID = "btnShow";
        showButton.Text = ResHelper.GetString("general.show");
        showButton.CssClass = "ContentButton";
        showButton.Click += ShowButton_Click;
        showButton.EnableViewState = false;
        ltlBreak.EnableViewState = false;
        ltlBreak.Text = "<br /><br />";
        tCell.Controls.Add(showButton);
        tCell.Controls.Add(ltlBreak);
        tCell.ColumnSpan = 2;
        tCell.EnableViewState = false;
        TableCell indentCell = new TableCell();
        indentCell.EnableViewState = false;
        indentCell.Text = "&nbsp;";
        tRow.Cells.Add(indentCell);   // Indent 'Show' button
        tRow.Cells.Add(tCell);
        filterTable.Rows.Add(tRow);
        pnlHeader.DefaultButton = showButton.ID;
    }


    /// <summary>
    /// Set filter to the grid view and save it to the view state
    /// </summary>
    /// <param name="reloadData">Reload data</param>
    private void SetFilter(bool reloadData)
    {
        string where = GetFilter();

        // Filter by query string
        if (FilterByQueryString && !reloadData)
        {
            string url = URLRewriter.CurrentURL;
            foreach (string name in queryStringHashTable.Keys)
            {
                if (queryStringHashTable[name] != null)
                {
                    string value = HttpContext.Current.Server.UrlEncode(queryStringHashTable[name].ToString());
                    url = UrlHelper.AddParameterToUrl(url, name, value);
                }
            }
            UrlHelper.Redirect(url);
        }
        else
        {
            WhereClause = where;
            FilterIsSet = true;
            if ((!DelayedReload) && (reloadData))
            {
                // Get data from database and set them to the grid view
                ReloadData();
            }
        }
    }


    /// <summary>
    /// Checks filter visibility according to filter limit and rows count.
    /// </summary>
    private void CheckFilterVisibility()
    {
        if (displayFilter)
        {
            if (FilterLimit > 0)
            {
                if (FilterByQueryString)
                {
                    // Hide filters if rowcount is lower than limit
                    if (!FilterIsSet && ((Pager.UniPager.DataSourceItemsCount <= FilterLimit) || (Pager.UniPager.DataSourceItemsCount == 0)))
                    {
                        // Set filter visibility
                        filter.Visible = false;
                    }
                    else
                    {
                        // Set filter visibility
                        filter.Visible = true;
                    }
                }
                else
                {
                    if (!RequestHelper.IsPostBack() || !FilterIsSet)
                    {
                        // Hide filters if rowcount is lower than limit
                        if ((Pager.UniPager.DataSourceItemsCount <= FilterLimit) || (Pager.UniPager.DataSourceItemsCount == 0))
                        {
                            // Set filter visibility
                            filter.Visible = false;
                        }
                        else
                        {
                            // Set filter visibility
                            filter.Visible = true;
                        }
                    }
                }
            }
        }
        else
        {
            // Hide filter
            filter.Visible = false;
        }
    }


    /// <summary>
    /// Sorts UniGrid data source according to sort directive saved in viewstate
    /// </summary>
    private void SortUniGridDataSource()
    {
        if (SortDirect != "")
        {
            // If source isn't empty
            if (!DataHelper.DataSourceIsEmpty(UniGridView.DataSource))
            {
                // Set sort directive from viewstate
                if (UniGridView.DataSource is DataTable)
                {
                    try
                    {
                        ((DataTable)(UniGridView.DataSource)).DefaultView.Sort = SortDirect;
                    }
                    catch { }
                }
                else if (UniGridView.DataSource is DataSet)
                {
                    try
                    {
                        ((DataSet)(UniGridView.DataSource)).Tables[0].DefaultView.Sort = SortDirect;
                        UniGridView.DataSource = ((DataSet)(UniGridView.DataSource)).Tables[0].DefaultView;
                    }
                    catch { }
                }
            }
        }
    }


    /// <summary>
    /// Changes sorting direction by specified column
    /// </summary>
    /// <param name="orderByColumn">Column name to order by.</param>
    /// <param name="orderByString">Old order by string.</param> 
    private void ChangeSortDirection(string orderByColumn, string orderByString)
    {
        orderByColumn = orderByColumn.ToLower().Trim().TrimStart('[').TrimEnd(']').Trim();
        orderByString = orderByString.ToLower().Trim().TrimStart('[');

        // If order by column is long text use CAST in ORDER BY part of query
        if (TextColumns.Contains(orderByColumn))
        {
            if (orderByString.ToLower().StartsWith("cast([" + orderByColumn + "]"))
            {
                if (orderByString.EndsWith("desc"))
                {
                    SortDirect = "CAST([" + orderByColumn + "] AS nvarchar(32)) asc";
                }
                else
                {
                    SortDirect = "CAST([" + orderByColumn + "] AS nvarchar(32)) desc";
                }
            }
            else
            {
                SortDirect = "CAST([" + orderByColumn + "] AS nvarchar(32)) asc";
            }
        }
        else
        {
            string orderByDirection = "asc";
            Match orderByMatch = OrderByRegex.Match(orderByString);
            if (orderByMatch.Success)
            {
                if (orderByMatch.Groups[2].Success)
                {
                    orderByDirection = orderByMatch.Groups[2].Value;
                }
            }

            // Sort by the same column -> the other directon
            if (orderByString.StartsWith(orderByColumn.ToLower()))
            {
                SortDirect = (orderByDirection == "desc") ? "[" + orderByColumn + "] asc" : "[" + orderByColumn + "] desc";
            }
            // Sort by a new column -> implicitly direction is ASC
            else
            {
                SortDirect = "[" + orderByColumn + "] asc";
            }
        }
    }


    /// <summary>
    /// Return List of selected Items.
    /// </summary>
    private ArrayList GetSelectedItems()
    {
        return GetHiddenValues(hidSelection);
    }


    /// <summary>
    /// Sets selection values for UniGrid.
    /// </summary>
    /// <param name="values">Arraylist of values to selection</param>
    private void SetSectedItems(ArrayList values)
    {
        SetHiddenValues(values, hidSelection);
    }


    /// <summary>
    /// Return List of deselected Items.
    /// </summary>
    private ArrayList GetDeselectedItems()
    {
        return GetHiddenValues(hidDeSelection);
    }


    /// <summary>
    /// Return List of newly selected Items.
    /// </summary>
    private ArrayList GetNewlySelectedItems()
    {
        return GetHiddenValues(hidNewSelection);
    }


    /// <summary>
    /// Return array list from hidden field 
    /// </summary>
    /// <param name="field">Hidden field with values separated with |</param>
    private static ArrayList GetHiddenValues(HiddenField field)
    {
        string hiddenValue = field.Value.Trim('|');
        ArrayList list = new ArrayList();
        string[] values = hiddenValue.Split('|');
        foreach (string value in values)
        {
            if (!list.Contains(value))
            {
                list.Add(value);
            }
        }
        list.Remove("");
        return list;
    }


    /// <summary>
    /// Sets array list values into hidden field.
    /// </summary>
    /// <param name="values">Arraylist of values to selection</param>
    /// <param name="field">Hidden field</param>
    private static void SetHiddenValues(ArrayList values, HiddenField field)
    {
        if ((field != null) && (values != null))
        {
            StringBuilder sb = new StringBuilder();
            foreach (string value in values)
            {
                sb.Append(value + "|");
            }
            field.Value = "|" + sb;
        }
    }


    /// <summary>
    /// Clears all selected items from hidden values.
    /// </summary>
    /// <param name="field">Hidden field</param>
    private static void ClearHiddenValues(HiddenField field)
    {
        if (field != null)
        {
            field.Value = "";
        }
    }


    /// <summary>
    /// Sets hidden field with actions hashes.
    /// </summary>
    private void SetActionsHash()
    {
        if (ActionsID.Count > 0)
        {
            SetHiddenValues(ActionsID, hidActionsHash);
        }
    }


    /// <summary>
    /// Set pager control
    /// </summary>
    private void SetPager()
    {
        Pager.PagedControl = this;
    }


    /// <summary>
    /// Sets the sort direction if current request is sorting
    /// </summary>
    private void ProcessSorting()
    {
        // Get current event target
        string uniqieId = ValidationHelper.GetString(Request.Params["__EVENTTARGET"], String.Empty);
        // Get current argument
        string eventargument = ValidationHelper.GetString(Request.Params["__EVENTARGUMENT"], String.Empty);

        if ((uniqieId == GridView.UniqueID) && (eventargument.StartsWith("Sort")))
        {
            string orderByColumn = Convert.ToString(eventargument.Split('$')[1]);

            // If sorting is called for the first time and default sorting (OrderBy property) is set
            if ((SortDirect == "") && !string.IsNullOrEmpty(OrderBy))
            {
                ChangeSortDirection(orderByColumn, OrderBy);
            }
            else
            {
                ChangeSortDirection(orderByColumn, SortDirect);
            }
        }
    }


    /// <summary>
    /// Returns true if current request was fired by page change or filter show button
    /// </summary>
    private bool EventRequest()
    {
        if (UrlHelper.IsPostback())
        {
            // Get current event target
            string uniqieId = ValidationHelper.GetString(Request.Params["__EVENTTARGET"], String.Empty);
            // Get current argument
            string eventargument = ValidationHelper.GetString(Request.Params["__EVENTARGUMENT"], String.Empty).ToLower();

            // Check whether current request is paging
            if (!String.IsNullOrEmpty(uniqieId) && (uniqieId == GridView.UniqueID) && eventargument.StartsWith("page"))
            {
                return true;
            }

            // Check whether show button is defined
            if (showButton != null)
            {
                // If button name is not empty => button fire postback
                if (!string.IsNullOrEmpty(Request.Params[showButton.UniqueID]))
                {
                    return true;
                }
            }
        }

        // Non-paging request by default
        return false;
    }


    /// <summary>
    /// Returns icon file for current theme or from default if current doesn't exist
    /// </summary>
    /// <param name="iconfile">Icon file name</param>
    private string GetActionImage(string iconfile)
    {
        if (File.Exists(MapPath(ImageDirectoryPath + iconfile)))
        {
            return (ImageDirectoryPath + iconfile);
        }

        return GetImageUrl("Design/Controls/UniGrid/Actions/" + iconfile);
    }


    /// <summary>
    /// Register unigrid commands scripts
    /// </summary>
    private void RegisterCmdScripts()
    {
        StringBuilder builder = new StringBuilder();

        // Actions
        builder.Append("function SetUniGridCmd_" + ClientID + "(name, arg){\n");
        builder.Append("var nameObj = document.getElementById('" + hidCmdName.ClientID + "');\n");
        builder.Append("var argObj = document.getElementById('" + hidCmdArg.ClientID + "');\n");
        builder.Append("if ((nameObj != null)&&(argObj != null)){\n");
        builder.Append("nameObj.value = name;\n");
        builder.Append("argObj.value = arg;\n");
        builder.Append(Page.ClientScript.GetPostBackEventReference(this, "UniGridAction"));
        builder.Append(";} return false;\n");
        builder.Append("}\n");

        // Selection - click
        builder.Append("function SelectionClick_" + ClientID + "(checkBox, arg){\n");
        builder.Append("if (checkBox != null){\n");
        builder.Append("    var selectionObj = document.getElementById('" + hidSelection.ClientID + "');\n");
        builder.Append("    var newSelectionObj = document.getElementById('" + hidNewSelection.ClientID + "');\n");
        builder.Append("    var deSelectionObj = document.getElementById('" + hidDeSelection.ClientID + "');\n");
        builder.Append("    if ((selectionObj != null)&&(newSelectionObj != null)&&(deSelectionObj != null)){\n");
        builder.Append("        if (newSelectionObj.value == '') {\n");
        builder.Append("            newSelectionObj.value = '|';\n");
        builder.Append("        }\n");
        builder.Append("        if (deSelectionObj.value == '') {\n");
        builder.Append("            deSelectionObj.value = '|';\n");
        builder.Append("        }\n");
        builder.Append("        if (selectionObj.value == '') {\n");
        builder.Append("            selectionObj.value = '|';\n");
        builder.Append("        }\n");
        builder.Append("        if (checkBox.checked) {\n");
        builder.Append("            selectionObj.value += arg + '|';\n");
        builder.Append("            if (deSelectionObj.value.indexOf('|' + arg + '|') >= 0) {\n");
        builder.Append("                deSelectionObj.value = deSelectionObj.value.replace('|' + arg + '|', '|');\n");
        builder.Append("            }\n");
        builder.Append("            else {\n");
        builder.Append("                newSelectionObj.value += arg + '|';\n");
        builder.Append("            }\n");
        builder.Append("        }\n");
        builder.Append("        else {\n");
        builder.Append("            selectionObj.value = selectionObj.value.replace('|' + arg + '|', '|');\n");
        builder.Append("            if (newSelectionObj.value.indexOf('|' + arg + '|') >= 0) {\n");
        builder.Append("                newSelectionObj.value = newSelectionObj.value.replace('|' + arg + '|', '|');\n");
        builder.Append("            }\n");
        builder.Append("            else {\n");
        builder.Append("                deSelectionObj.value += arg + '|';\n");
        builder.Append("            }\n");
        builder.Append("        }\n");
        builder.Append("    }\n");
        builder.Append("}\n");
        builder.Append("}\n");

        // Selection - select all
        builder.Append("function SelectAllClick_" + ClientID + "(checkBox){\n");
        builder.Append("var elements = document.getElementsByTagName(\"INPUT\");\n");
        builder.Append("var regularExpresssion = new RegExp('" + ClientID + "');\n");
        builder.Append("for(i=0; i<elements.length;i++){\n");
        builder.Append("    if(elements[i].type == 'checkbox'){\n");
        builder.Append("        if(elements[i].id.match(regularExpresssion)) {\n");
        builder.Append("            if((!elements[i].id != checkBox.id) && (checkBox.checked != elements[i].checked)) {\n");
        builder.Append("                elements[i].click();\n");
        builder.Append("            }\n");
        builder.Append("        }\n");
        builder.Append("    }\n");
        builder.Append("}");
        builder.Append("}");

        // Selection - clear
        builder.Append("function ClearSelection_" + ClientID + "() {\n");
        builder.Append(" var inputs = document.getElementsByTagName('input');\n");
        builder.Append(" if (inputs != null) {\n");
        builder.Append("     for (var i = 0; i< inputs.length; i++) {\n");
        builder.Append("         if ((inputs[i].type.toLowerCase() == 'checkbox') && (inputs[i].id.match(/^" + ClientID + "/i))) {\n");
        builder.Append("             inputs[i].checked = false;\n");
        builder.Append("         }\n");
        builder.Append("     }\n");
        builder.Append(" }\n");
        builder.Append(" var hidSelection = document.getElementById('" + hidSelection.ClientID + "');\n");
        builder.Append(" var hidNewSelection = document.getElementById('" + hidNewSelection.ClientID + "');\n");
        builder.Append(" var hidDeSelection = document.getElementById('" + hidDeSelection.ClientID + "');\n");
        builder.Append(" if (hidSelection != null) {\n");
        builder.Append("     hidSelection.value = '';\n");
        builder.Append(" }\n");
        builder.Append(" if (hidNewSelection != null) {\n");
        builder.Append("     hidNewSelection.value = '';\n");
        builder.Append(" }\n");
        builder.Append(" if (hidDeSelection != null) {\n");
        builder.Append("     hidDeSelection.value = '';\n");
        builder.Append(" }\n");
        builder.Append("}\n");

        // Selection - IsSelectionEmpty
        builder.AppendLine("function IsSelectionEmpty_" + ClientID + "() {");
        builder.AppendLine("var hidSelection = document.getElementById('" + hidSelection.ClientID + "');");
        builder.AppendLine("var items = hidSelection.value;");
        builder.AppendLine("return !(items != '' && items != '|');");
        builder.AppendLine("}");

        if (resetSelection)
        {
            builder.Append("if (typeof(ClearSelection_" + ClientID + ") != 'undefined')\n");
            builder.Append("{\n");
            builder.Append("    ClearSelection_" + ClientID + "();\n");
            builder.Append("}\n");
        }

        ScriptHelper.RegisterStartupScript(this, typeof(string), "SetUniGridCmd_" + ClientID, ScriptHelper.GetScript(builder.ToString()));
    }

    #endregion


    #region "IUniPageable Members"

    public object PagerDataItem
    {
        get
        {
            return UniGridView.DataSource;
        }
        set
        {
            UniGridView.DataSource = value;
        }
    }


    public int PagerForceNumberOfResults
    {
        get
        {
            return pagerForceNumberOfResults;
        }
        set
        {
            pagerForceNumberOfResults = value;
        }
    }

    public event EventHandler<EventArgs> OnPageBinding;

    public event EventHandler<EventArgs> OnPageChanged;

    public virtual void ReBind()
    {
        if (OnPageChanged != null)
        {
            OnPageChanged(this, null);
        }

        ReloadData();
    }

    #endregion
}
