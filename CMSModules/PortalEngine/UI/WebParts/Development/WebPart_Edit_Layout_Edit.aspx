<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebPart_Edit_Layout_Edit.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_PortalEngine_UI_WebParts_Development_WebPart_Edit_Layout_Edit"
    Theme="Default" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Panel ID="pnlCheckOutInfo" runat="server" CssClass="InfoLabel" Visible="false">
        <asp:Label runat="server" ID="lblCheckOutInfo" />
    </asp:Panel>
    <br />
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCodeName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtCodeName" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblDescription" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="TextAreaField"
                    MaxLength="450" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCode" EnableViewState="false" />
            </td>
            <td>
                <asp:PlaceHolder runat="server" ID="plcVirtualInfo" Visible="false">
                    <br />
                    <asp:Label runat="server" ID="lblVirtualInfo" CssClass="ErrorLabel" EnableViewState="false" />
                </asp:PlaceHolder>
                <cms:ExtendedTextArea ID="etaCode" runat="server" CssClass="TextAreaCode" TextMode="MultiLine"
                    Width="600px" />
            </td>
        </tr>
    </table>
</asp:Content>
