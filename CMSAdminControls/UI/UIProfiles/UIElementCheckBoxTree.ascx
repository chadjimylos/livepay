﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UIElementCheckBoxTree.ascx.cs"
    Inherits="CMSAdminControls_UI_UIProfiles_UIElementCheckBoxTree" %>
<%@ Register Src="~/CMSAdminControls/UI/Trees/UniTree.ascx" TagName="UniTree" TagPrefix="cms" %>
<div class="UIPersonalizationTree">
    <cms:UniTree ID="treeElem" runat="server" Localize="true" />
</div>
<asp:HiddenField runat="server" ID="hdnValue" />
