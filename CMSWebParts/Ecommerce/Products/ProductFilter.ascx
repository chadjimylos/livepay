<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Ecommerce/Products/ProductFilter.ascx.cs"
    Inherits="CMSWebParts_Ecommerce_Products_ProductFilter" %>
<%@ Register Src="~/CMSModules/Ecommerce/Controls/Filters/ProductFilter.ascx" TagName="ProductFilter"
    TagPrefix="cms" %>
<cms:ProductFilter ID="filterElem" runat="server" />
