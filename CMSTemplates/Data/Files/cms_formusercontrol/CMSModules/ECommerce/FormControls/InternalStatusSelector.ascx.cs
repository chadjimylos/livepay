using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Ecommerce;
using CMS.GlobalHelper;

public partial class CMSModules_Ecommerce_FormControls_InternalStatusSelector : CMS.FormControls.FormEngineUserControl
{
    #region "Variables"

    private bool mUseStatusNameForSelection = true;
    private bool mAddAllItemsRecord = true;
    private bool mAddNoneRecord = true;

    private StatusEnum mDisplayItems = StatusEnum.Both;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Indicates which objects should be displayed (Enabled/Disabled/Both). Default values is 'Both'.
    /// </summary>
    public StatusEnum DisplayItems
    {
        get
        {
            return mDisplayItems;
        }
        set
        {
            mDisplayItems = value;
        }
    }


    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (this.mUseStatusNameForSelection)
            {
                return this.InternalStatusName;
            }
            else
            {
                return this.InternalStatusID;
            }
        }
        set
        {
            if (this.mUseStatusNameForSelection)
            {
                this.InternalStatusName = ValidationHelper.GetString(value, "");
            }
            else
            {
                this.InternalStatusID = ValidationHelper.GetInteger(value, 0);
            }
        }
    }


    /// <summary>
    /// Gets or sets the InternalStatus ID.
    /// </summary>
    public int InternalStatusID
    {
        get
        {
            if (this.mUseStatusNameForSelection)
            {
                string name = ValidationHelper.GetString(uniSelector.Value, "");
                InternalStatusInfo tgi = InternalStatusInfoProvider.GetInternalStatusInfo(name);
                if (tgi != null)
                {
                    return tgi.InternalStatusID;
                }
                return 0;
            }
            else
            {
                return ValidationHelper.GetInteger(uniSelector.Value, 0);
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.mUseStatusNameForSelection)
            {
                InternalStatusInfo tgi = InternalStatusInfoProvider.GetInternalStatusInfo(value);
                if (tgi != null)
                {
                    this.uniSelector.Value = tgi.InternalStatusID;
                }
            }
            else
            {
                this.uniSelector.Value = value;
            }
        }
    }


    /// <summary>
    /// Gets or sets the InternalStatus code name.
    /// </summary>
    public string InternalStatusName
    {
        get
        {
            if (this.mUseStatusNameForSelection)
            {
                return ValidationHelper.GetString(this.uniSelector.Value, "");
            }
            else
            {
                int id = ValidationHelper.GetInteger(this.uniSelector.Value, 0);
                InternalStatusInfo tgi = InternalStatusInfoProvider.GetInternalStatusInfo(id);
                if (tgi != null)
                {
                    return tgi.InternalStatusName;
                }
                return "";
            }
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }

            if (this.mUseStatusNameForSelection)
            {
                this.uniSelector.Value = value;
            }
            else
            {
                InternalStatusInfo tgi = InternalStatusInfoProvider.GetInternalStatusInfo(value);
                if (tgi != null)
                {
                    this.uniSelector.Value = tgi.InternalStatusName;
                }
            }
        }
    }


    /// <summary>
    ///  If true, selected value is InternalStatusName, if false, selected value is InternalStatusID
    /// </summary>
    public bool UseStatusNameForSelection
    {
        get
        {
            return mUseStatusNameForSelection;
        }
        set
        {
            mUseStatusNameForSelection = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add all item record to the dropdownlist.
    /// </summary>
    public bool AddAllItemsRecord
    {
        get
        {
            return mAddAllItemsRecord;
        }
        set
        {
            mAddAllItemsRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines, whether to add none item record to the dropdownlist.
    /// </summary>
    public bool AddNoneRecord
    {
        get
        {
            return mAddNoneRecord;
        }
        set
        {
            mAddNoneRecord = value;
        }
    }


    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Returns ClientID of the dropdownlist.
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect.ClientID;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    public void ReloadData()
    {
        this.uniSelector.EnabledColumnName = "InternalStatusEnabled";
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.AllowEmpty = this.AddNoneRecord;
        this.uniSelector.AllowAll = this.AddAllItemsRecord;
        this.uniSelector.ReturnColumnName = (this.UseStatusNameForSelection ? "InternalStatusName" : "InternalStatusID");

        if (this.DisplayItems == StatusEnum.Disabled)
        {
            this.uniSelector.WhereCondition = "InternalStatusEnabled = 0";
        }
        else if (this.DisplayItems == StatusEnum.Enabled)
        {
            this.uniSelector.WhereCondition = "InternalStatusEnabled = 1";
        }

        if (this.UseStatusNameForSelection)
        {
            this.uniSelector.AllRecordValue = "";
            this.uniSelector.NoneRecordValue = "";
        }
    }
}
