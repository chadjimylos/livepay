using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_InvoicePrint : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int orderId = ValidationHelper.GetInteger(Request.QueryString["orderid"], 0);
        OrderInfo order = OrderInfoProvider.GetOrderInfo(orderId);
        if (order != null)
        {
            lblInvoice.Text = UrlHelper.MakeLinksAbsolute(order.OrderInvoice);
        }
    }
}
