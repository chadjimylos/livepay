<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DepartmentSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_FormControls_DepartmentSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%DepartmentDisplayName%}"
            ObjectType="ecommerce.department" ResourcePrefix="departmentselector"
            SelectionMode="SingleDropDownList" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
