<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/LivePay/registrationform.ascx.cs"
    Inherits="CMSWebParts_LivePay_registrationform" %>
<%@ Register Src="~/CMSFormControls/Inputs/SecurityCode.ascx" TagName="SecurityCode" TagPrefix="cms" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cms" %>

<style>
.registration-textbox
{
	border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma
}
.registration-email{
    border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma
}
.ContentButton
{
    border: none;
    background: url('/App_Themes/LivePay/UserRegistration/BtnRegister.png') no-repeat top left;
    width:75px;
    height:23px;
}
div.growlUI { background: url(check48.png) no-repeat 10px 10px;width:200px }
div.growlUI h1, div.growlUI h2 {font-size:12px;color: white;text-align: left;width:200px}
.test
{
    display:none;
}
</style>


<script language="javascript" type="text/javascript" >
    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

    var RegFormCaptchaCloudMess = ''
    var RegFormFnameCloudMess = ''
    var RegFormLnameCloudMess = ''
    var RegFormAnsCloudMess = ''
    var RegFormConfirmCloudMess = ''
    var RegFormPassCloudMess = ''
    var RegFormEmailCloudMess = ''
    var RegFormTermsCloudMess = ''


    function RegValidationForm(oSrc, args) {
       var bIsValid = true
       
       var MyVal = args.Value
       
       if (MyVal.length == 0) {
           bIsValid = false
       }

       if (IsNumeric(MyVal) == false && oSrc.getAttribute('IsNumber') == 'yes') {
           bIsValid = false
       }

       var MaxLenght = MyVal.length
       if (oSrc.getAttribute('CheckLength') == 'yes' && MaxLenght != oSrc.getAttribute('LengthLimit')) {

           bIsValid = false
       }

       if (oSrc.getAttribute('IsEmail') == 'yes') {
           if (checkTheEmail(MyVal) == false)
               bIsValid = false
       }

       if (oSrc.getAttribute('IsPass') == 'yes') {
           bIsValid = IsPass(MyVal)
       }


      

       if (oSrc.getAttribute('IsConfirm') == 'yes') {
           var PassValue = document.getElementById('<%=txtPassword.ClientID %>').value
           if (PassValue != MyVal) {
               bIsValid = false
           }
       }

       if (oSrc.getAttribute('IsEmail') == 'yes') {
           if (bIsValid == false) {
               RegFormCloud('#EmailCloud', RegFormEmailCloudMess, true,'')
           } else {
               RegFormCloud('#EmailCloud', '', false, '')
           }
       }

       if (oSrc.getAttribute('IsPass') == 'yes') {
           if (bIsValid == false) {
               RegFormCloud('#PassWordCloud', RegFormPassCloudMess, true, '')
           } else {
               RegFormCloud('#PassWordCloud', '', false, '')
           }
       }

       if (oSrc.getAttribute('IsConfirm') == 'yes') {
           if (bIsValid == false) {
               RegFormCloud('#ConfirmCloud', RegFormConfirmCloudMess, true, '')
           } else {
               RegFormCloud('#ConfirmCloud', '', false, '')
           }
       }


       if (oSrc.getAttribute('IsAns') == 'yes') {
           if (bIsValid == false) {
               RegFormCloud('#AnswerCloud', RegFormAnsCloudMess, true, '')
           } else {
               RegFormCloud('#AnswerCloud', '', false, '')
           }
       }


       if (oSrc.getAttribute('IsLName') == 'yes') {
           if (bIsValid == false) {
               RegFormCloud('#LastNameCloud', RegFormLnameCloudMess, true, '')
           } else {
               RegFormCloud('#LastNameCloud', '', false, '')
           }
       }

       if (oSrc.getAttribute('IsFname') == 'yes') {
           if (bIsValid == false) {
               RegFormCloud('#FirstNameCloud', RegFormFnameCloudMess, true, '')
           } else {
               RegFormCloud('#FirstNameCloud', '', false, '')
           }
       }

       if (oSrc.getAttribute('IsCaptcha') == 'yes') {
           if (bIsValid == false) {
               RegFormCloud('#CaptchaCloud', RegFormCaptchaCloudMess, true, '-27')
           } else {
               RegFormCloud('#CaptchaCloud', '', false, '')
           }
       }


       if (oSrc.getAttribute('IsTerms') == 'yes') {
           if (bIsValid == false) {
               RegFormCloud('#TermsCloud', RegFormTermsCloudMess, true, '-35')
           } else {
               RegFormCloud('#TermsCloud', '', false, '')
           }
       }


       args.IsValid = bIsValid;
   }

  

   function SetHiddenTerms(obj) {
       var HiddenTerms = document.getElementById('<%=HiddenTerms.ClientID%>')
       if (obj.checked) {
           RegFormCloud('#TermsCloud', '', false, '-35')
           HiddenTerms.value = 'ok'
       } else {
           RegFormCloud('#TermsCloud', RegFormTermsCloudMess, true, '-35')
           HiddenTerms.value = ''
       }
   }
   
     
    function IsPass(sPass){
     if (sPass.length > 5){
        var symbolFound = false
        var NumberFound = false
        var Symbols = '!@#$%^&*?_~-�()'
        var Nums = "0123456789"
        for (i = 0; i < sPass.length; i++) {
            Char = sPass.charAt(i);

            if (Symbols.indexOf(Char)!= -1) {
                symbolFound = true;
            }
            
            if (Nums.indexOf(Char)!= -1) {
                NumberFound = true;
            }
            
        }
        
        if (symbolFound && NumberFound){
            return true
        }else{
            return false
        }

     }else{
       return false
     }
    }

    function IsNumeric(sText) {
        var ValidChars = "0123456789";
        var IsNumber = true;
        var Char;
        for (i = 0; i < sText.length && IsNumber == true; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) == -1) {
                IsNumber = false;
            }
        }
        return IsNumber;
    }

    function checkTheEmail(MailValue) {
        var filter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(MailValue))
            return false
        else
            return true
    }


    function RegFormCloud(CloudID, CloudMess, show,YposCustom) {
        var YPos = -24;
        if (is_chrome) {
            YPos = -40
            if (YposCustom!='' && YposCustom.length > 0) {
                YPos = YposCustom;
            }
        }

        if (is_firefox) {
            YPos = -10
        }

        $(CloudID).poshytip('hide');
        $(CloudID).poshytip({
            className: 'tip-livepay',
            content: CloudMess,
            showOn: 'none',
            alignTo: 'target',
            alignX: 'right',
            offsetX: 238,
            offsetY: YPos, hideAniDuration: false
        });

        if (show) {
            $(CloudID).poshytip('show');
        }
    }

</script>


<asp:Label ID="lblText" runat="server" Visible="false" EnableViewState="false" />
<asp:Panel ID="pnlForm" runat="server" DefaultButton="btnOK">

        <div class="CUDContentBG">
        <div class="ErrorLabel">
<asp:Label ID="lblError" runat="server" ForeColor="red" EnableViewState="false" />
</div>
             <%-- Email --%>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail" EnableViewState="false" />
                </div>
                <div style="float:left;padding-top:13px;padding-top:expression(0)">
                    <a id="EmailCloud" href="#"></a><cms:ExtendedTextBox ID="txtEmail" runat="server" TabIndex="1" CssClass="registration-textbox" MaxLength="100" Width="234" />
                </div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div class="HideIt">
                    <asp:CustomValidator ID="UnRegUserEmailValid" ClientValidationFunction="RegValidationForm" ValidateEmptyText="true" runat="server" IsEmail="yes" ControlToValidate="txtEmail" text="*" ValidationGroup="RegistrationForm"/>
                </div> 
               
            </div>
             <%-- FristName --%>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName" EnableViewState="false" />
                </div>
                <div style="float:left;padding-top:13px;padding-top:expression(0)">
                    <a id="FirstNameCloud" href="#"></a><cms:ExtendedTextBox ID="txtFirstName" TabIndex="2" EnableEncoding="true" runat="server" CssClass="registration-textbox"
                    MaxLength="100" Width="234" />
                </div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
               <div class="HideIt">
                    <asp:CustomValidator ID="UnRegUserFirstName" ClientValidationFunction="RegValidationForm" ValidateEmptyText="true" runat="server" IsFname="yes" ControlToValidate="txtFirstName" text="*" ValidationGroup="RegistrationForm"/>
                </div>
            </div>
             <%-- LastName --%>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" />
                </div>
                <div style="float:left;padding-top:13px;padding-top:expression(0)">
                    <a id="LastNameCloud" href="#"></a><cms:ExtendedTextBox ID="txtLastName" TabIndex="3" EnableEncoding="true" runat="server" CssClass="registration-textbox"
                    MaxLength="100" Width="234" />
                </div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div class="HideIt">
                    <asp:CustomValidator ID="UnRegUserLastName" ClientValidationFunction="RegValidationForm" ValidateEmptyText="true" runat="server" IsLName="yes" ControlToValidate="txtLastName" text="*" ValidationGroup="RegistrationForm"/>
                </div>
            </div>
             <%-- Password --%>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblPassword" runat="server" AssociatedControlID="txtPassword" EnableViewState="false" />
                </div>
                <div style="float:left;padding-top:13px;padding-top:expression(0)">
                    <a id="PassWordCloud" href="#"></a><asp:TextBox ID="txtPassword" runat="server" TabIndex="4" TextMode="Password" CssClass="registration-textbox"
                    MaxLength="100" Width="234" />
                </div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div class="HideIt">
                    <asp:CustomValidator ID="UnRegUserPassword" ClientValidationFunction="RegValidationForm" ValidateEmptyText="true" runat="server" IsPass="yes" ControlToValidate="txtPassword" text="*" ValidationGroup="RegistrationForm"/>
                </div>
            </div>
             <%-- ConfirmPassword --%>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblConfirmPassword" runat="server" AssociatedControlID="txtConfirmPassword"
                    EnableViewState="false" />
                </div>
                <div style="float:left;padding-top:13px;padding-top:expression(0)">
                    <a id="ConfirmCloud" href="#"></a><asp:TextBox ID="txtConfirmPassword" runat="server" TabIndex="5" TextMode="Password" CssClass="registration-textbox"
                    MaxLength="100" Width="234" />
                </div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div class="HideIt">
                    <asp:CustomValidator ID="UnRegUserConfirmPassword" ClientValidationFunction="RegValidationForm" ValidateEmptyText="true" runat="server" IsConfirm="yes" ControlToValidate="txtConfirmPassword" text="*" ValidationGroup="RegistrationForm"/>
                </div>
            </div>
             <%-- Phone --%>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblPhone" runat="server" AssociatedControlID="txtPhone" />
                </div>
                <div style="float:left;padding-top:13px;padding-top:expression(0)">
                    <cms:ExtendedTextBox ID="txtPhone" EnableEncoding="true" runat="server" TabIndex="6" CssClass="registration-textbox"
                    MaxLength="100" Width="234" />
                </div>
                <div class="Clear"></div>
             </div>
             <%-- Question --%>
             <div style="">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right;padding-top:12px">
                    <asp:Label ID="lblQuestion" runat="server" AssociatedControlID="ddlQuestion" /> 
                </div>
                <div style="float:left;padding-top:14px">
                    <asp:DropDownList ID="ddlQuestion" runat="server" width="234" TabIndex="7" CssClass="registration-textbox"></asp:DropDownList>
                </div>
                <div class="Clear"></div>
             </div>
             <%-- Answer --%>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">
                    <asp:Label ID="lblAnswer" runat="server" AssociatedControlID="txtAnswer" />
                </div>
                <div style="float:left;padding-top:13px;padding-top:expression(0)">
                    <a id="AnswerCloud" href="#"></a><asp:TextBox ID="txtAnswer" TabIndex="8" runat="server" CssClass="registration-textbox" width="234"/>
                </div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                  <div class="HideIt">
                    <asp:CustomValidator ID="UnRegUserAnswer" ClientValidationFunction="RegValidationForm" ValidateEmptyText="true" runat="server" IsAns="yes" ControlToValidate="txtAnswer" text="*" ValidationGroup="RegistrationForm"/>
                </div>
             </div>
             <%-- Captchas --%>
             <asp:PlaceHolder runat="server" ID="plcCaptcha">
            <div>
                <div style="float:left;width:155px;">&nbsp;</div>
                <div style="float:left" >
                    <cms:CaptchaControl ID="scCaptcha" runat="server" BorderWidth="1px" BorderColor="#3b6db4" BorderStyle="Solid"
	                    CaptchaBackgroundNoise="None"
	                    CaptchaLength="5" 
	                    CaptchaHeight="58"
	                    CaptchaWidth="236"
	                    Width="236"
	                    CaptchaLineNoise="Extreme" 
	                    CacheStrategy="HttpRuntime"
	                    CaptchaMaxTimeout="240" 
                    />
                </div>
                <div class="Clear"></div>
            </div>
            <div style="height:30px;line-height:30px;vertical-align:middle">
                <div style="float:left;width:155px;">&nbsp;</div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;"><asp:Label ID="lblCaptcha" runat="server" AssociatedControlID="scCaptcha" EnableViewState="false" /></div>
                <div class="Clear"></div>

            </div>
             <div style="">
                <div style="float:left;width:155px;">&nbsp;</div> 
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;"><a id="CaptchaCloud" href="#"></a><asp:TextBox TabIndex="9" ID="txtCaptcha" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div class="HideIt">
                    <asp:CustomValidator ID="UnRegUserCaptcha" ClientValidationFunction="RegValidationForm" ValidateEmptyText="true" runat="server" IsCaptcha="yes" ControlToValidate="txtCaptcha" text="*" ValidationGroup="RegistrationForm"/>
                </div>
            </div>
            </asp:PlaceHolder>
             <%-- Terms --%>
             <div style="">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right;padding-top:12px">
                    <asp:Label ID="lblTerms" runat="server" AssociatedControlID="txtTerms" /> 
                </div>
                <div style="float:left;padding-top:14px">
                    <asp:TextBox ID="txtTerms" TextMode="MultiLine" Rows="6" ReadOnly="true" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="420px"/>
                </div>
                <div class="Clear"></div>
            </div>
             <%-- Check Terms --%>
             <div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:6px">
                   <a id="TermsCloud" href="#"></a> <asp:CheckBox ID="chbAcceptTerms" onclick="SetHiddenTerms(this)" TabIndex="10" runat="server" style="color:#43474a;font-size:11px" />
                </div>
                <div class="Clear"></div> 
                <div class="HideIt">
                <asp:TextBox ID="HiddenTerms" runat="server" style="display:none"></asp:TextBox>
                    <asp:CustomValidator ID="UnRegUserTerms" ClientValidationFunction="RegValidationForm" ValidateEmptyText="true" runat="server" IsTerms="yes" ControlToValidate="HiddenTerms" text="*" ValidationGroup="RegistrationForm"/>
                </div>
             </div>
             <%-- Check News --%>
             <div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px">
                    <asp:CheckBox ID="chbAcceptSendNews" TabIndex="11" runat="server" style="color:#43474a;font-size:11px" />
                </div>
                <div class="Clear"></div>
             </div>
             <%-- Button --%>
             <div style="padding-top:5px;padding-bottom:10px">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px">
                  <%--  <cms:CMSButton ID="btnOk" style="cursor:pointer" runat="server" OnClick="btnOK_Click" ValidationGroup="pnlForm"
                    CssClass="ContentButton" EnableViewState="false" /> --%>
                   <asp:ImageButton id="btnOk" ValidationGroup="RegistrationForm" TabIndex="12" OnClick="btnOK_Click" runat="server" ImageUrl="/App_Themes/LivePay/UserRegistration/BtnRegister.png"
                   EnableViewState="false"   />
                </div>
                <div class="Clear"></div>
            </div>
             <div style="padding-top:5px;padding-bottom:10px">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px;font-size:10px;color:#094595">�� ����� �� ��� ��������� (*) ����� �����������
                </div>
                <div class="Clear"></div>
            </div>

        </div>
</asp:Panel>

<asp:Literal ID="lit_JsScript" runat="server" ></asp:Literal>

