using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Categories_Category_List : SiteManagerPage
{
    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize the controls
        SetupControl();
    }

    #endregion


    #region "UniGrid Events"

    private object gridCategories_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "enabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);

            case "displayname":
                return HTMLHelper.HTMLEncode(parameter.ToString());
        }
        return parameter;
    }


    private void gridCategories_OnAction(string actionName, object actionArgument)
    {
        int categoryId = -1;

        switch (actionName)
        {
            // Editing of the category fired
            case "edit":
                // Get category ID
                categoryId = ValidationHelper.GetInteger(actionArgument, -1);
                // create a target site URL and pass the category ID as a parameter
                string editUrl = "Category_Edit.aspx?categoryId=" + categoryId.ToString();
                UrlHelper.Redirect(editUrl);
                break;

            // Deleteing of the category was fired
            case "delete":
                categoryId = ValidationHelper.GetInteger(actionArgument, -1);

                // If no Document-Category relationship exist concerning current category
                if (categoryId > -1)
                {
                    // Delete category
                    CategoryInfoProvider.DeleteCategoryInfo(categoryId);
                }

                break;

            default:
                return;
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes the controls on the page
    /// </summary>
    private void SetupControl()
    {
        // Initialize the grid view
        this.gridCategories.OnAction += new OnActionEventHandler(gridCategories_OnAction);
        this.gridCategories.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridCategories_OnExternalDataBound);
        this.gridCategories.GridName = "~/CMSSiteManager/Development/Categories/Category_List.xml";
        this.gridCategories.OrderBy = "CategoryDisplayName";
        this.gridCategories.ZeroRowsText = ResHelper.GetString("general.nodatafound");
        this.gridCategories.WhereCondition = "(CategoryUserID IS NULL)";

        // Setup page title text and image
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Development.Categories");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Category/object.png");

        this.CurrentMaster.Title.HelpTopicName = "categories_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Prepare the new class header element
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("Category_List.NewItemCaption");
        actions[0, 3] = "~/CMSSiteManager/Development/Categories/Category_New.aspx";
        actions[0, 5] = GetImageUrl("Objects/CMS_Category/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }

    #endregion
}
