<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Attachments/AttachmentLightBoxGallery.ascx.cs"
    Inherits="CMSWebParts_Attachments_AttachmentLightBoxGallery" %>
<%@ Register Src="~/CMSModules/Content/Controls/Attachments/AttachmentLightboxGallery.ascx"
    TagName="AttachmentLightboxGallery" TagPrefix="cms" %>
<cms:AttachmentLightboxGallery ID="galleryElem" runat="server" />