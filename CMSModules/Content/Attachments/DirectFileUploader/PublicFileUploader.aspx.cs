using System;

using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.CMSHelper;

public partial class CMSModules_Content_Attachments_DirectFileUploader_PublicFileUploader : LivePage
{
    #region "Private variables"

    private MediaSourceEnum mSourceType = MediaSourceEnum.DocumentAttachments;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Gets or sets current source type.
    /// </summary>
    private MediaSourceEnum SourceType 
    {
        get 
        {
            return this.mSourceType;
        }
        set 
        {
            this.mSourceType = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Validate query string
        if (!QueryHelper.ValidateHash("hash"))
        {
            // Do nothing
        }
        else
        {
            // Get information on current source type
            string sourceType = QueryHelper.GetString("source", "attachments");
            this.SourceType = CMSDialogHelper.GetMediaSource(sourceType);

            // Get uploader control based on the current source type
            string uploaderPath = "";
            if (this.SourceType == MediaSourceEnum.MediaLibraries)
            {
                // If media library module is running
                if (ModuleEntry.IsModuleRegistered(ModuleEntry.MEDIALIBRARY) && ModuleEntry.IsModuleLoaded(ModuleEntry.MEDIALIBRARY))
                {
                    uploaderPath = "~/CMSModules/MediaLibrary/Controls/Dialogs/DirectFileUploader/DirectMediaFileUploaderControl.ascx";
                }
            }
            else
            {
                uploaderPath = "~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploaderControl.ascx";
            }

            // Load direct file uploader
            if (uploaderPath != "")
            {
                DirectFileUploader fileUploaderElem = this.LoadControl(uploaderPath) as DirectFileUploader;
                if (fileUploaderElem != null)
                {
                    // Insert uploader to parent container
                    this.pnlUploaderElem.Controls.Add(fileUploaderElem);

                    // Initialize uploader control properties by query string values
                    fileUploaderElem.ImageWidth = QueryHelper.GetInteger("imagewidth", 24);
                    fileUploaderElem.ImageHeight = QueryHelper.GetInteger("imageheight", 24);
                    fileUploaderElem.ImageUrl = QueryHelper.GetString("imageurl", GetImageUrl("Design/Controls/DirectUploader/upload.png"));
                    fileUploaderElem.ImageUrlOver = QueryHelper.GetString("imageurlover", String.Empty);
                    fileUploaderElem.InnerDivHtml = QueryHelper.GetString("innerdivhtml", String.Empty);
                    fileUploaderElem.InnerDivClass = QueryHelper.GetString("innerdivclass", String.Empty);
                    fileUploaderElem.InnerLoadingDivHtml = QueryHelper.GetString("innerloadingdivhtml", String.Empty);
                    fileUploaderElem.InnerLoadingDivClass = QueryHelper.GetString("innerloadingdivclass", String.Empty);
                    fileUploaderElem.LoadingImageUrl = QueryHelper.GetString("loadingimageurl", GetImageUrl("Design/Preloaders/preload16.gif"));
                    fileUploaderElem.AttachmentGUID = QueryHelper.GetGuid("attachmentguid", Guid.Empty);
                    fileUploaderElem.AttachmentGroupGUID = QueryHelper.GetGuid("attachmentgroupguid", Guid.Empty);
                    fileUploaderElem.AttachmentGUIDColumnName = QueryHelper.GetString("attachmentguidcolumnname", null);
                    fileUploaderElem.FormGUID = QueryHelper.GetGuid("formguid", Guid.Empty);
                    fileUploaderElem.DocumentID = QueryHelper.GetInteger("documentid", 0);
                    fileUploaderElem.NodeParentNodeID = QueryHelper.GetInteger("parentid", 0);
                    fileUploaderElem.NodeClassName = QueryHelper.GetString("classname", "");
                    fileUploaderElem.InsertMode = QueryHelper.GetBoolean("insertmode", false);
                    fileUploaderElem.OnlyImages = QueryHelper.GetBoolean("onlyimages", false);
                    fileUploaderElem.ParentElemID = QueryHelper.GetString("parentelemid", String.Empty);
                    fileUploaderElem.CheckPermissions = QueryHelper.GetBoolean("checkperm", true);
                    fileUploaderElem.IsLiveSite = QueryHelper.GetBoolean("islive", true);
                    fileUploaderElem.RaiseOnClick = QueryHelper.GetBoolean("click", false);
                    fileUploaderElem.NodeSiteName = QueryHelper.GetString("sitename", null);
                    fileUploaderElem.SourceType = this.SourceType;

                    // Library info initialization
                    fileUploaderElem.LibraryID = QueryHelper.GetInteger("libraryid", 0);
                    fileUploaderElem.MediaFileID = QueryHelper.GetInteger("mediafileid", 0);
                    fileUploaderElem.MediaFileName = QueryHelper.GetString("filename", null);
                    fileUploaderElem.IsMediaThumbnail = QueryHelper.GetBoolean("ismediathumbnail", false);
                    fileUploaderElem.LibraryFolderPath = QueryHelper.GetString("path", "");
                    fileUploaderElem.IncludeNewItemInfo = QueryHelper.GetBoolean("includeinfo", false);

                    string siteName = CMSContext.CurrentSiteName;
                    string allowed = QueryHelper.GetString("allowedextensions", null);
                    if (allowed == null)
                    {
                        if (fileUploaderElem.SourceType == MediaSourceEnum.MediaLibraries)
                        {
                            allowed = SettingsKeyProvider.GetStringValue(siteName + ".CMSMediaFileAllowedExtensions");
                        }
                        else
                        {
                            allowed = SettingsKeyProvider.GetStringValue(siteName + ".CMSUploadExtensions");
                        }
                    }
                    fileUploaderElem.AllowedExtensions = allowed;
                    fileUploaderElem.ResizeToWidth = QueryHelper.GetInteger("autoresize_width", 0); ;
                    fileUploaderElem.ResizeToHeight = QueryHelper.GetInteger("autoresize_height", 0); ;
                    fileUploaderElem.ResizeToMaxSideSize = QueryHelper.GetInteger("autoresize_maxsidesize", 0);

                    fileUploaderElem.AfterSaveJavascript = QueryHelper.GetString("aftersave", String.Empty);
                }
            }
        }
    }
}
