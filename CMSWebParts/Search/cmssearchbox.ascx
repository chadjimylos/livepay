<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Search/cmssearchbox.ascx.cs"
    Inherits="CMSWebParts_Search_cmssearchbox" %>
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnImageButton" CssClass="searchBox">
    <asp:Label ID="lblSearch" runat="server" AssociatedControlID="txtWord" EnableViewState="false" /><asp:TextBox
        ID="txtWord" runat="server" /><cms:CMSButton ID="btnGo" runat="server" OnClick="btnGo_Click"
            EnableViewState="false" /><asp:ImageButton ID="btnImageButton" runat="server" Visible="false"
                OnClick="btnImageButton_Click" EnableViewState="false" />
</asp:Panel>
