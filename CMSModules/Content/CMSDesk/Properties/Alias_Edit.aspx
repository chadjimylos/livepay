<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Alias_Edit.aspx.cs" Inherits="CMSModules_Content_CMSDesk_Properties_Alias_Edit"
    Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>

<%@ Register Src="~/CMSFormControls/Cultures/SiteCultureSelector.ascx" TagName="SiteCultureSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/editmenu.ascx" TagName="editmenu"
    TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Properties - Page</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>
</head>
<body class="VerticalTabsBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel ID="pnlHeader" runat="server" CssClass="PageHeader">
        <cms:PageTitle ID="pageAlias" runat="server" />
    </asp:Panel>
    <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
        <asp:Label ID="lblError" runat="server" EnableViewState="false" CssClass="ErrorLabel" /><asp:Label
            ID="lblInfo" runat="server" EnableViewState="false" CssClass="InfoLabel" />
        <asp:Panel ID="pnlForm" runat="server" CssClass="PageContent">
            <table>
                <tr>
                    <td class="FieldLabel">
                        <asp:Label ID="lblURLPath" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtURLPath" runat="server" CssClass="TextBoxField" MaxLength="450" />
                        <asp:RequiredFieldValidator ID="valURLPath" runat="server" ControlToValidate="txtURLPath"
                            Display="dynamic" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <asp:Label ID="lblDocumentCulture" runat="server" />
                    </td>
                    <td>
                        <cms:SiteCultureSelector runat="server" ID="cultureSelector" IsLiveSite="false" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <asp:Label ID="lblURLExtensions" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtURLExtensions" runat="server" CssClass="TextBoxField" MaxLength="100" /><br />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <asp:Label ID="lblTrackCampaign" runat="server" />
                    </td>
                    <td>
                        <asp:TextBox ID="txtTrackCampaign" runat="server" CssClass="TextBoxField" MaxLength="100" /><br />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
