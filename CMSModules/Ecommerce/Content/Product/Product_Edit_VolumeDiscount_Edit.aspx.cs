using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.LicenseProvider;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Content_Product_Product_Edit_VolumeDiscount_Edit : CMSContentPage
{
	protected int volumeDiscountID = 0;
    protected int productID = 0;


	protected void Page_Load(object sender, EventArgs e)
	{
        // Check the license
        LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Ecommerce);

        // Check site availability
        if (!ResourceSiteInfoProvider.IsResourceOnSite("CMS.Ecommerce", CMSContext.CurrentSiteName))
        {
            RedirectToResourceNotAvailableOnSite("CMS.Ecommerce");
        }

        // Check 'EcommerceRead' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceRead"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceRead");
        }

        // Init labels
		lblVolumeDiscountValue.Text = ResHelper.GetString("product_edit_volumediscount_edit.volumediscountvaluelabel");
		lblVolumeDiscountMinCount.Text = ResHelper.GetString("product_edit_volumediscount_edit.volumediscountmincountlabel");
        radDiscountAbsolute.Text = ResHelper.GetString("product_edit_volumediscount_edit.rbdiscountabsolute");
        radDiscountRelative.Text = ResHelper.GetString("product_edit_volumediscount_edit.rbdiscountrelative");
        // Init value validator error messages
        rfvDiscountValue.ErrorMessage = ResHelper.GetString("product_edit_volumediscount_edit.rfvDiscountValue");
        rvDiscountValue.ErrorMessage = ResHelper.GetString("product_edit_volumediscount_edit.rvDiscountValue");
        rvDiscountValue.MaximumValue = int.MaxValue.ToString();
        // Init min count validator error messages
        rfvMinCount.ErrorMessage = ResHelper.GetString("product_edit_volumediscount_edit.rfvMinCount");
        rvMinCount.ErrorMessage = ResHelper.GetString("product_edit_volumediscount_edit.rvMinCount");
        rvMinCount.MaximumValue = int.MaxValue.ToString();
        
		btnOk.Text = ResHelper.GetString("General.OK");
       
		string currentVolumeDiscount = ResHelper.GetString("Product_Edit_VolumeDiscount_Edit.NewItemCaption");

		// get parameters from querystring		
        volumeDiscountID = ValidationHelper.GetInteger(Request.QueryString["VolumeDiscountID"], 0);
        productID = ValidationHelper.GetInteger(Request.QueryString["ProductID"], 0);
        // If true, then we will edit existing record                
        if (volumeDiscountID > 0)
        {
            // Check if there is already VolumeDiscountInfo with this volumeDiscountID 
            VolumeDiscountInfo volumeDiscountObj = VolumeDiscountInfoProvider.GetVolumeDiscountInfo(volumeDiscountID);
            if (volumeDiscountObj != null)
            {
                // fill editing form with existing data when not postback
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(volumeDiscountObj);
                    // set caption of edited item
                    currentVolumeDiscount = ResHelper.GetString("product_edit_volumediscount_edit.edittitletext");

                    // show that the volumeDiscount was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");

                        // Refresh parent page when editing in modal dialog
                        ltlScript.Text = ScriptHelper.GetScript("wopener.RefreshPage();");
                    }
                }
            }
            // Set page title to "volume discount properties"
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("product_edit_volumediscount_edit.edittitletext");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_VolumeDiscount/object.png");
        }
        // do this when creating new discount
        else
        {
            // Set default radio button value
            radDiscountRelative.Checked = true;
            // Init page header to "new item"		
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("product_edit_volumediscount_edit.newitemcaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_VolumeDiscount/new.png");
        }

        this.CurrentMaster.Title.HelpTopicName = "newedit_discount";
        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.HeadElements.Text = "<base target=\"_self\" />";
        this.CurrentMaster.HeadElements.Visible = true;

        ScriptHelper.RegisterWOpenerScript(Page);
	}


	/// <summary>
	/// Load data of editing volumeDiscount.
	/// </summary>
	/// <param name="volumeDiscountObj">VolumeDiscount object.</param>
	protected void LoadData(VolumeDiscountInfo volumeDiscountObj)
	{
	    // load data from database
		txtVolumeDiscountValue.Text = Convert.ToString(volumeDiscountObj.VolumeDiscountValue);
		txtVolumeDiscountMinCount.Text = Convert.ToString(volumeDiscountObj.VolumeDiscountMinCount);
        radDiscountAbsolute.Checked = (volumeDiscountObj.VolumeDiscountIsFlatValue);
        radDiscountRelative.Checked = !(volumeDiscountObj.VolumeDiscountIsFlatValue);
        
	 }


	/// <summary>
	/// Sets data to database.
	/// </summary>
	protected void btnOK_Click(object sender, EventArgs e)
	{
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }
         
        // True if there is already same min count;
        bool isMinCountUnique = false;
        // Server side validation of user input 
        string errorMessage = new Validator().NotEmpty(txtVolumeDiscountValue.Text, "product_edit_volumediscount_edit.volumediscountvaluelabel")
                                             .NotEmpty(txtVolumeDiscountMinCount.Text, "product_edit_volumediscount_edit.volumediscountmincountlabel").Result;
        if (errorMessage == "")
		{
			VolumeDiscountInfo volumeDiscountObj = VolumeDiscountInfoProvider.GetVolumeDiscountInfo(volumeDiscountID);
			// if volumeDiscount doesnt already exist, create new one
			if (volumeDiscountObj == null)
			{
				// Create new volume discount
                volumeDiscountObj = new VolumeDiscountInfo();

                // When creating new one, set his SKUID to productID (obtained from URL)
                volumeDiscountObj.VolumeDiscountSKUID = productID;
			}

            // Set volumeDiscountObj values
            volumeDiscountObj.VolumeDiscountValue = Convert.ToDouble(txtVolumeDiscountValue.Text.Trim());
			volumeDiscountObj.VolumeDiscountMinCount = Convert.ToInt32(txtVolumeDiscountMinCount.Text.Trim());
            volumeDiscountObj.VolumeDiscountIsFlatValue =  radDiscountAbsolute.Checked;
            
            // set isMinCountUnique
            VolumeDiscountInfo vdi = VolumeDiscountInfoProvider.GetVolumeDiscounts(productID, volumeDiscountObj.VolumeDiscountMinCount);
            if (vdi == null)
            {
                isMinCountUnique = true;
            }
            else
            {
                isMinCountUnique = (vdi.VolumeDiscountMinCount != volumeDiscountObj.VolumeDiscountMinCount);                                    
            }

            // Check if min count is unique or it is update of existing item
            if ((isMinCountUnique) || (vdi.VolumeDiscountID == volumeDiscountObj.VolumeDiscountID))
            {
                // Sets data to database
                VolumeDiscountInfoProvider.SetVolumeDiscountInfo(volumeDiscountObj);
                string redirectUrl = "Product_Edit_VolumeDiscount_Edit.aspx?VolumeDiscountID=" + Convert.ToString(volumeDiscountObj.VolumeDiscountID) + "&saved=1&productID=" + productID;
                UrlHelper.Redirect(redirectUrl);
            }
            else
            {
                lblError.Text = ResHelper.GetString("product_edit_volumediscount_edit.minamountexists");
                lblError.Visible = true;
            }
        }
		else
		{
			lblError.Visible = true;
			lblError.Text = errorMessage;
		}
	}

    
}
