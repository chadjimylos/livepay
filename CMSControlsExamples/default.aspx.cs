using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.UIControls;

namespace CMSControlsExamples
{
    public partial class defaultMain : ControlsExamplesPage
    { 
        protected void Page_Load(Object sender, EventArgs e)
        {
            if (CultureHelper.IsUICultureRTL())
            {
                ControlsHelper.ReverseFrames(this.colsFrameset);
            }
        }
    } 
    
} 
