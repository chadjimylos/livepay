﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Widget_Edit_Frameset.aspx.cs" Inherits="CMSModules_Widgets_UI_Widget_Edit_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" enableviewstate="false">
    <title>Widget - Edit</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height:100%; 
        }
    </style>
</head>
<frameset border="0" rows="65, *" framespacing="0" id="rowsFrameset">
    <frame name="widgetheader" src="Widget_Edit_Header.aspx<%= Request.Url.Query %>"
        scrolling="no" frameborder="0" noresize="noresize" />
    <frame name="widgeteditcontent" src="Widget_Edit_General.aspx<%= Request.Url.Query %>"
        frameborder="0" />
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
</frameset>
</html>
