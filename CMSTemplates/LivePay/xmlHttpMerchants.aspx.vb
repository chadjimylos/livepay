﻿Imports System.Data
Imports CMS.SettingsProvider
Imports CMS.SiteProvider
Imports CMS.DataEngine

Imports System
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls

Partial Class CMSTemplates_LivePay_xmlHttpMerchants
    Inherits System.Web.UI.Page


    Protected ReadOnly Property Language() As String
        Get
            Dim sLanguage As String = "el-gr"
            If Not String.IsNullOrEmpty(Request("lang")) Then
                sLanguage = Request("lang")
            End If
            Return sLanguage
        End Get
    End Property

    Protected ReadOnly Property MerchantKeyWord() As String
        Get
            Dim sMerchantKeyWord As String = String.Empty
            If Not String.IsNullOrEmpty(Request("MerchantKeyWord")) Then
                sMerchantKeyWord = Request("MerchantKeyWord")
            End If
            Return sMerchantKeyWord
        End Get
    End Property

    Protected ReadOnly Property FormPage() As String
        Get
            Dim sFormPage As String = String.Empty
            If Not String.IsNullOrEmpty(Request("FormPage")) Then
                sFormPage = Request("FormPage")
            End If
            Return sFormPage
        End Get
    End Property



    Protected ReadOnly Property RecordsNo() As Integer
        Get
            Dim iRecordsNo As Integer = 5
            If Not String.IsNullOrEmpty(Request("RecordsNo")) Then
                iRecordsNo = Request("RecordsNo")
            End If
            Return iRecordsNo
        End Get
    End Property


    Protected ReadOnly Property nomos() As Integer
        Get
            Dim inomos As Integer = 0
            If Not String.IsNullOrEmpty(Request("nomos")) Then
                inomos = Request("nomos")
            End If
            Return inomos
        End Get
    End Property

    Protected ReadOnly Property area() As Integer
        Get
            Dim iarea As Integer = 0
            If Not String.IsNullOrEmpty(Request("area")) Then
                iarea = Request("area")
            End If
            Return iarea
        End Get
    End Property

    Protected ReadOnly Property NodeID() As Integer
        Get
            Dim iNodeID As Integer = 0
            If Not String.IsNullOrEmpty(Request("NodeID")) Then
                iNodeID = Request("NodeID")
            End If
            Return iNodeID
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetItemsCustomTable("LivePay.Merchant", "MerchantID", "CompanyName")
        'Me.repSearch.TransformationName = "LivePay.Merchant.Preview"
    End Sub

    Private Sub GetItemsCustomTable(ByVal CustomTable As String, ByVal ColumeValue As String, ByVal ColumeText As String)
      
        Dim key As String = Replace(MerchantKeyWord, "'", String.Empty)
        key = Replace(key, """", String.Empty)
        If key <> String.Empty Then

            If FormPage <> String.Empty Then
                Response.Write(Me.rptSearchSec.Items.Count.ToString & "#]")
            Else
                Response.Write(Me.rptSearchSec_Form.Items.Count.ToString & "#]")
            End If
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim SearchType As Integer = 0
        Dim nodePath As String = String.Empty
        Dim key As String = Replace(MerchantKeyWord, "'", String.Empty)
        key = Replace(key, """", String.Empty)
        key = Replace(key, "%", String.Empty)
        key = Replace(key, "@@@@", "'")
        If key <> String.Empty Then
            ' Me.repSearch.CultureCode = Me.Language
            Dim ExtraWhere As String = String.Empty
            If Me.nomos > 0 Then
                ExtraWhere = " and prefecture = " & Me.nomos
            End If
            If Me.area > 0 Then
                ExtraWhere = ExtraWhere & " and Municipality = " & Me.area
            End If

            If Me.NodeID > 0 Then
                Dim ds_Nodes As New DataSet
                Dim customTableClassName As String = "Livepay.Settings"
                Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass(customTableClassName)
                If customTableClassInfo Is Nothing Then

                End If
                Dim ctiProvider As CustomTableItemProvider = New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())
                Dim dsItems As DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, String.Empty, String.Empty)

                If (DataHelper.DataSourceIsEmpty(dsItems) = False) Then
                    For i As Integer = 0 To dsItems.Tables(0).Rows.Count - 1
                        If dsItems.Tables(0).Rows(i)("code").ToString = "Geographic" Then
                            Dim AllNodes As Array = dsItems.Tables(0).Rows(i)("value").ToString.Split(",")
                            For iNode As Integer = 0 To AllNodes.Length - 1
                                If Me.NodeID = AllNodes(iNode) Then
                                    '  repSearch.ClassNames = "LivePay.Merchant"
                                    SearchType = 1
                                    Dim tp As CMS.TreeEngine.TreeProvider = New CMS.TreeEngine.TreeProvider(CMS.CMSHelper.CMSContext.CurrentUser)
                                    Dim node As CMS.TreeEngine.TreeNode = tp.SelectSingleNode(Me.NodeID)
                                    'repSearch.Path = node.NodeAliasPath & "/%"
                                    nodePath = node.NodeAliasPath & "/%"

                                End If
                            Next
                        End If
                    Next
                End If
            End If

         
            'Me.repSearch.WhereCondition = " ( (DiscreetTitle COLLATE Greek_CI_AI like N'" & key & "%') or (DiscreetTitle COLLATE Greek_CI_AI like N'%" & key & "%')) " & ExtraWhere ' and lang='" & Me.Language & "'"
            'Me.repSearch.TopN = RecordsNo
            If FormPage <> String.Empty Then
                '    Me.repSearch.TransformationName = "LivePay.Merchant.SearchMerchants"
                rptSearchSec.Visible = True
                rptSearchSec_Form.Visible = False
                rptSearchSec.DataSource = DBConnection.SearchMerchant("Proc_LivePay_SearchMerchantAutoComplete", key, nodePath, nomos, area, Me.Language, SearchType)
                rptSearchSec.DataBind()
            Else
                rptSearchSec.Visible = False
                rptSearchSec_Form.Visible = True
                rptSearchSec_Form.DataSource = DBConnection.SearchMerchant("Proc_LivePay_SearchMerchantAutoComplete", key, nodePath, nomos, area, Me.Language, SearchType)
                rptSearchSec_Form.DataBind()

            End If
        End If


        


    End Sub

End Class
