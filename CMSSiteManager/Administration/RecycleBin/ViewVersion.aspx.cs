﻿using System;

using CMS.UIControls;
using CMS.GlobalHelper;

public partial class CMSSiteManager_Administration_RecycleBin_ViewVersion : CMSDeskPage
{
    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentMaster.Title.HelpName = "helpTopic";
        CurrentMaster.Title.HelpTopicName = "versions";
        CurrentMaster.Title.TitleText = ResHelper.GetString("RecycleBin.ViewVersion");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_RecycleBin/viewversion.png");

        // Register tooltip script
        ScriptHelper.RegisterTooltip(Page);

        // Register the dialog script
        ScriptHelper.RegisterDialogScript(this);
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        RequireSite = false;
    }

    #endregion
}
