CREATE TABLE [Board_Subscription] (
		[SubscriptionID]               int NOT NULL IDENTITY(1, 1),
		[SubscriptionBoardID]          int NOT NULL,
		[SubscriptionUserID]           int NULL,
		[SubscriptionEmail]            nvarchar(250) NOT NULL,
		[SubscriptionLastModified]     datetime NOT NULL,
		[SubscriptionGUID]             uniqueidentifier NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Board_Subscription]
	ADD
	CONSTRAINT [PK_Board_Subscription]
	PRIMARY KEY
	([SubscriptionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Board_Subscription]') IS NOT NULL
	AND OBJECT_ID(N'[Board_Board]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Board_Subscription_SubscriptionBoardID_Board_Board]') IS NULL
BEGIN
		ALTER TABLE [Board_Subscription]
			ADD CONSTRAINT [FK_Board_Subscription_SubscriptionBoardID_Board_Board]
			FOREIGN KEY ([SubscriptionBoardID]) REFERENCES [Board_Board] ([BoardID])
END
IF OBJECT_ID(N'[Board_Subscription]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Board_Subscription_SubscriptionUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Board_Subscription]
			ADD CONSTRAINT [FK_Board_Subscription_SubscriptionUserID_CMS_User]
			FOREIGN KEY ([SubscriptionUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_Board_Subscription_SubscriptionBoardID]
	ON [Board_Subscription] ([SubscriptionBoardID])
	ON [PRIMARY]
CREATE INDEX [IX_Board_Subscription_SubscriptionUserID]
	ON [Board_Subscription] ([SubscriptionUserID])
	ON [PRIMARY]
