<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="User_Edit_Categories.aspx.cs" Inherits="CMSSiteManager_Administration_Users_User_Edit_Categories"
    Title="User - Categories" Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/Categories/MultipleCategoriesSelector.ascx" TagName="CategoriesEdit"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Panel ID="pnlCategories" runat="server" Style="padding:0px 7px;">
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
        <cms:CategoriesEdit ID="categoriesEditElem" runat="server" HideOKButton="true" />
    </asp:Panel>
</asp:Content>
