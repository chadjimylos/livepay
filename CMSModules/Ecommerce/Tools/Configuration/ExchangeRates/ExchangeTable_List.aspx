<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExchangeTable_List.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_ExchangeRates_ExchangeTable_List" Theme="default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Exchangle table - List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:UniGrid runat="server" ID="gridElem" GridName="ExchangeTable_List.xml" OrderBy="ExchangeTableDisplayName"
        IsLiveSite="false" Columns="ExchangeTableID,ExchangeTableDisplayName,ExchangeTableValidFrom,ExchangeTableValidTo" />
</asp:Content>
