CREATE PROCEDURE [Proc_CMS_WidgetCategory_UpdateChildPaths] 
	@OldCategoryPath nvarchar(450), 
    @NewCategoryPath nvarchar(450)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- Update category paths
UPDATE CMS_WidgetCategory SET 
    WidgetCategoryPath = @NewCategoryPath + right(WidgetCategoryPath, len(WidgetCategoryPath) - len(@OldCategoryPath)) 
WHERE
    left(WidgetCategoryPath, len(@OldCategoryPath) + 1) = @OldCategoryPath + '/' ; 
END
