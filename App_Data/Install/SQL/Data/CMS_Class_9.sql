set identity_insert [cms_class] on
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Group Role Permission', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="GroupID" fieldcaption="GroupID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="f04ba8b6-2c34-45b8-a9a9-f7c71e5a14ee" /><field column="RoleID" fieldcaption="RoleID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4e68ead1-29d8-4b6c-9c82-b07e2d3c38f0" /><field column="PermissionID" fieldcaption="PermissionID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b22650fa-12f1-4751-a852-b918ef48c6e2" /></form>', '20100201 08:33:26', 1995, N'Community.GroupRolePermission', 0, 0, 0, 'a0b09036-ae6a-41f2-bee9-58fd49710360', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Community_GroupRolePermission', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Community_GroupRolePermission">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="GroupID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="RoleID" type="xs:int" />
              <xs:element name="PermissionID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Community_GroupRolePermission" />
      <xs:field xpath="GroupID" />
      <xs:field xpath="RoleID" />
      <xs:field xpath="PermissionID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Blog post subscription', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="SubscriptionID" fieldcaption="SubscriptionID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="60d7d9ed-561d-4e2c-a1dc-a1b813e216df" /><field column="SubscriptionPostDocumentID" fieldcaption="SubscriptionPostDocumentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3922393b-dc76-4466-a2f7-ebff75d9ed91" /><field column="SubscriptionUserID" fieldcaption="SubscriptionUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="710adf87-4854-41a1-bb31-82c1a7013848" /><field column="SubscriptionEmail" fieldcaption="SubscriptionEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="250" publicfield="false" spellcheck="true" guid="0b82f5f2-019b-431f-b618-e75d43a83788" /><field column="SubscriptionLastModified" fieldcaption="SubscriptionLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e797244c-52a6-4165-a765-313b63a84e9c" /><field column="SubscriptionGUID" fieldcaption="SubscriptionGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c34dfa10-582d-4dc3-816f-eb2f00823b6f" /></form>', '20100308 13:41:29', 2018, N'blog.postsubscription', 0, 0, 0, 'b396f281-260f-458f-94a7-edc7c78588ea', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Blog_PostSubscription', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Blog_PostSubscription">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SubscriptionID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SubscriptionPostDocumentID" type="xs:int" />
              <xs:element name="SubscriptionUserID" type="xs:int" minOccurs="0" />
              <xs:element name="SubscriptionEmail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriptionLastModified" type="xs:dateTime" />
              <xs:element name="SubscriptionGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Blog_PostSubscription" />
      <xs:field xpath="SubscriptionID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Search index', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="IndexID" fieldcaption="IndexID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="11a12bb8-8daa-4d81-a93d-18b6ee6dd7fe" /><field column="IndexName" fieldcaption="IndexName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="1e61111e-be57-4ed5-bf42-5a831ef74e02" /><field column="IndexDisplayName" fieldcaption="IndexDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="f6062f9f-ddcb-432b-aa26-3eb19bec2136" /><field column="IndexType" fieldcaption="IndexType" visible="false" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="e196f557-21ab-4b65-8977-d5b14d33d333" visibility="none" /><field column="IndexAnalyzerType" fieldcaption="IndexAnalyzerType" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="38f0655c-0b92-4436-915f-8a2545ee548a" /><field column="IndexIsCommunityGroup" fieldcaption="IndexIsCommunityGroup" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9779c4b1-155e-473f-a19d-a54afbf99f8a" /><field column="IndexSettings" fieldcaption="IndexSettings" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6baa44c0-892e-4136-be2e-2e625347981f" /><field column="IndexGUID" fieldcaption="IndexGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7b121874-5f4d-44e1-a652-26273df5f674" visibility="none" /><field column="IndexLastModified" fieldcaption="IndexLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f52606a9-543c-4554-98c5-c1a3680ef7ee" visibility="none"><settings><displayNow>true</displayNow><editTime>false</editTime><timezonetype>inherit</timezonetype></settings></field><field column="IndexLastRebuildTime" fieldcaption="IndexLastRebuildTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5a4a4036-c437-41ce-b2f7-2c1e5bf7eff1" visibility="none"><settings><displayNow>true</displayNow><editTime>false</editTime><timezonetype>inherit</timezonetype></settings></field><field column="IndexStopWordsFile" visible="false" columntype="text" fieldtype="label" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="16b06aff-63dc-4a32-8f0e-99bff88a0cf3" visibility="none" ismacro="false" /><field column="IndexCustomAnalyzerName" visible="false" columntype="text" fieldtype="label" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="ee4cca86-033f-4e43-aa34-b969f8114ade" visibility="none" ismacro="false" /></form>', '20100301 08:44:29', 2043, N'cms.SearchIndex', 0, 0, 0, 'd81f1c1e-da26-43c5-9962-0e663c448629', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_SearchIndex', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_SearchIndex">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="IndexID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="IndexName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IndexDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IndexAnalyzerType" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IndexIsCommunityGroup" type="xs:boolean" />
              <xs:element name="IndexSettings" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IndexGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="IndexLastModified" type="xs:dateTime" />
              <xs:element name="IndexLastRebuildTime" type="xs:dateTime" minOccurs="0" />
              <xs:element name="IndexType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IndexStopWordsFile" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IndexCustomAnalyzerName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_SearchIndex" />
      <xs:field xpath="IndexID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Search index site', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="IndexID" fieldcaption="IndexID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="92bfdd7c-ff31-494f-aeb8-40bc60b27098" visibility="none" /><field column="IndexSiteID" fieldcaption="IndexSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f6c6fe0a-45e8-45cf-9266-5a214dbdd6ae" /></form>', '20090424 14:59:31', 2044, N'cms.SearchIndexSite', 0, 0, 0, 'edd838a8-13e8-4195-9ba8-092da8b0680f', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'cms_SearchIndexSite', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="cms_SearchIndexSite">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="IndexID" type="xs:int" />
              <xs:element name="IndexSiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//cms_SearchIndexSite" />
      <xs:field xpath="IndexID" />
      <xs:field xpath="IndexSiteID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Search index culture', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="IndexID" fieldcaption="IndexID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="8e994fd3-5f76-40cd-9dba-20c5ac7ff343" visibility="none" /><field column="IndexCultureID" fieldcaption="IndexCultureID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1a35e0e0-f57a-4e2e-96f5-6df781c65214" /></form>', '20090825 14:04:29', 2045, N'cms.SearchIndexCulture', 0, 0, 0, '3c8d501e-1051-41e4-b0a5-0acac7c7d065', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'cms_SearchIndexCulture', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="cms_SearchIndexCulture">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="IndexID" type="xs:int" />
              <xs:element name="IndexCultureID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//cms_SearchIndexCulture" />
      <xs:field xpath="IndexID" />
      <xs:field xpath="IndexCultureID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Search task', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="SearchTaskID" fieldcaption="SearchTaskID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="2d7ca691-6e80-4ab6-88ac-7609f9fc216f" /><field column="SearchTaskType" fieldcaption="SearchTaskType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="851b8b0b-5aad-49d3-9d01-12dadd8c2bd4" /><field column="SearchTaskObjectType" fieldcaption="SearchTaskObjectType" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="db90abf1-a656-4c80-a617-48ebc9f056b5" /><field column="SearchTaskField" fieldcaption="SearchTaskField" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="2deab677-b15a-4540-94d5-3a35d17d66ef" /><field column="SearchTaskValue" fieldcaption="SearchTaskValue" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="600" publicfield="false" spellcheck="true" guid="91d12388-cfbb-42ec-9504-d46641293e97" visibility="none" ismacro="false" /><field column="SearchTaskServerName" fieldcaption="SearchTaskServerName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="178cb9b9-eb79-471e-ab7e-22ce39b8d02d" /><field column="SearchTaskStatus" fieldcaption="SearchTaskStatus" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="fada5670-c89f-4799-b985-f918efeb9531" /><field column="SearchTaskCreated" visible="false" columntype="datetime" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1d560e5a-d4bb-4aa1-9a9a-3766244f444a" visibility="none"><settings><timezonetype>inherit</timezonetype></settings></field></form>', '20091212 20:00:46', 2046, N'CMS.SearchTask', 0, 0, 0, '3ff254fe-d4de-4ad9-a6c9-cb71fc96c684', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'cms_SearchTask', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="cms_SearchTask">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SearchTaskID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SearchTaskType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SearchTaskObjectType" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SearchTaskField" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SearchTaskValue">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="600" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SearchTaskServerName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SearchTaskStatus">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SearchTaskPriority" type="xs:int" />
              <xs:element name="SearchTaskCreated" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//cms_SearchTask" />
      <xs:field xpath="SearchTaskID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'User culture', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="UserID" fieldcaption="UserID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="bb23cacf-feb8-46f8-8d64-e5c6007d54ab" /><field column="CultureID" fieldcaption="CultureID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="558b122d-669e-486c-981a-a64f786f1fd7" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2b263018-f23a-4640-84bc-674a6292c321" /></form>', '20090609 13:51:22', 2132, N'cms.userculture', 0, 0, 0, '04792d69-14ca-4e35-a851-461c0fe2502a', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_UserCulture', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_UserCulture">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="UserID" type="xs:int" />
              <xs:element name="CultureID" type="xs:int" />
              <xs:element name="SiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_UserCulture" />
      <xs:field xpath="UserID" />
      <xs:field xpath="CultureID" />
      <xs:field xpath="SiteID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'UI Element', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ElementID" fieldcaption="ElementID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="645c0c6d-7049-4e6b-b82c-4e955281a6ab" /><field column="ElementDisplayName" fieldcaption="ElementDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="b4631bcc-8acc-48ed-b44a-6db67e7a9a70" /><field column="ElementName" fieldcaption="ElementName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="e3267f50-9cd8-462c-b6a4-8e682c0a7a2b" /><field column="ElementCaption" fieldcaption="ElementCaption" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="44ebc475-fa8e-4b62-bdb3-facec141da14" /><field column="ElementTargetURL" fieldcaption="ElementTargetURL" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="7dd2f1ba-80b1-4295-b937-029afdc2d584" /><field column="ElementResourceID" fieldcaption="ElementResourceID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cddb57a0-5500-4011-a414-2e6e6e214f90" /><field column="ElementParentID" fieldcaption="ElementParentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c1534088-c759-4c87-9c2c-e39f75cdfa87" /><field column="ElementChildCount" fieldcaption="ElementChildCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ee66fff4-f1c0-4f20-81e0-cbb00de1c95a" /><field column="ElementOrder" fieldcaption="ElementOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="53486043-468b-4937-a6e0-f9fe28adb638" /><field column="ElementLevel" fieldcaption="ElementLevel" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d3a88b9f-adbb-4f04-a212-2d391b5ea5d6" /><field column="ElementIDPath" fieldcaption="ElementIDPath" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="0cbf65ee-21c4-447e-92c9-139b9ccf882d" /><field column="ElementIconPath" fieldcaption="ElementIconPath" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="4aa48ec6-c813-439d-9a5b-4c94a92ee26d" /><field column="ElementIsCustom" fieldcaption="ElementIsCustom" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ab307308-5525-4044-96ea-a2596417b87e" /><field column="ElementLastModified" fieldcaption="ElementLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0a3574c2-c3f2-4434-bf1a-7bd043b58a76" /><field column="ElementGUID" fieldcaption="ElementGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ec245fac-abd8-488a-8b9a-780afeca2e15" /></form>', '20100204 18:05:59', 2195, N'CMS.UIElement', 0, 0, 0, '756292a7-ea43-4b8c-a343-6e88dbc3d38e', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_UIElement', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_UIElement">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ElementID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ElementDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ElementName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ElementCaption" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ElementTargetURL" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ElementResourceID" type="xs:int" />
              <xs:element name="ElementParentID" type="xs:int" minOccurs="0" />
              <xs:element name="ElementChildCount" type="xs:int" />
              <xs:element name="ElementOrder" type="xs:int" minOccurs="0" />
              <xs:element name="ElementLevel" type="xs:int" />
              <xs:element name="ElementIDPath">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ElementIconPath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ElementIsCustom" type="xs:boolean" />
              <xs:element name="ElementLastModified" type="xs:dateTime" />
              <xs:element name="ElementGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_UIElement" />
      <xs:field xpath="ElementID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Role UI Element', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="RoleID" fieldcaption="RoleID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="4ace5a61-c739-4478-a14b-e66f7cdd093b" /><field column="ElementID" fieldcaption="ElementID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="39275590-79b9-41e4-81b9-2c026730c5d7" /></form>', '20090903 12:16:18', 2196, N'CMS.RoleUIElement', 0, 0, 0, 'ea08b2b5-89d6-4011-be2c-b4eb47283f6e', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_RoleUIElement', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_RoleUIElement">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="RoleID" type="xs:int" />
              <xs:element name="ElementID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_RoleUIElement" />
      <xs:field xpath="RoleID" />
      <xs:field xpath="ElementID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Widget', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="WidgetID" fieldcaption="WidgetID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="4f7cba83-cc62-4cad-8b6a-f35cd55f4b5f" visibility="none" ismacro="false" /><field column="WidgetWebPartID" fieldcaption="WidgetWebPartID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9f4ac38f-9358-45be-9a5a-3e0000716c7b" /><field column="WidgetDisplayName" fieldcaption="WidgetDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="576b3117-e2b8-4377-9143-e792e48e32cc" /><field column="WidgetName" fieldcaption="WidgetName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="16fa68bb-c899-410a-aef7-08527c4dacaa" /><field column="WidgetDescription" fieldcaption="WidgetDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fc5bf0ad-5c38-4857-b953-c03f2918d4c1" /><field column="WidgetCategoryID" fieldcaption="WidgetCategoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="35c79638-a901-43c3-982f-561d48567d4f" /><field column="WidgetProperties" fieldcaption="WidgetProperties" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1c7e4924-a259-4d69-bcb6-e24e05845158" /><field column="WidgetSecurity" fieldcaption="WidgetSecurity" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6027bc44-27d1-490d-af69-45b586a6c823" /><field column="WidgetForGroup" fieldcaption="WidgetForGroup" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d7e8eedf-c6a6-4976-84e2-d3edadde5bf6" defaultvalue="false" visibility="none" ismacro="false" /><field column="WidgetForEditor" fieldcaption="WidgetForEditor" visible="true" defaultvalue="false" columntype="boolean" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e8ff4627-d131-401e-9da3-ea6b97b2d443" visibility="none" ismacro="false" /><field column="WidgetForUser" fieldcaption="WidgetForUser" visible="true" defaultvalue="false" columntype="boolean" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="48cfed61-17fc-4985-80ad-ad1b4fb585bc" visibility="none" ismacro="false" /><field column="WidgetGUID" fieldcaption="WidgetGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9a1bd07b-a623-49af-a5ce-f7ed5959136c" /><field column="WidgetLastModified" fieldcaption="WidgetLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="aca51648-3b54-4e8f-82c5-5748e0d72ae2" /><field column="WidgetIsEnabled" fieldcaption="WidgetIsEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c7106140-fec2-43f7-8794-4e929bd14f6b" defaultvalue="false" visibility="none" /></form>', '20091125 09:08:30', 2197, N'cms.Widget', 0, 0, 0, '3abbd36b-5e12-410a-a6d9-fa22d4007970', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Widget', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Widget">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="WidgetID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="WidgetWebPartID" type="xs:int" />
              <xs:element name="WidgetDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WidgetName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WidgetDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WidgetCategoryID" type="xs:int" />
              <xs:element name="WidgetProperties" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WidgetSecurity" type="xs:int" />
              <xs:element name="WidgetGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="WidgetLastModified" type="xs:dateTime" />
              <xs:element name="WidgetIsEnabled" type="xs:boolean" />
              <xs:element name="WidgetForGroup" type="xs:boolean" />
              <xs:element name="WidgetForEditor" type="xs:boolean" />
              <xs:element name="WidgetForUser" type="xs:boolean" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Widget" />
      <xs:field xpath="WidgetID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'WidgetCategory', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="WidgetCategoryID" fieldcaption="WidgetCategoryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="28fe3f1b-f233-41f5-95aa-b31f6cfbe7c7" visibility="none" ismacro="false" /><field column="WidgetCategoryName" fieldcaption="WidgetCategoryName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="f8ee7f18-26fc-46e4-82fd-c64479e3989f" /><field column="WidgetCategoryDisplayName" fieldcaption="WidgetCategoryDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="31e41178-316e-4ee0-9b70-3e0a970ac2d8" /><field column="WidgetCategoryParentID" fieldcaption="WidgetCategoryParentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3f848c87-8f3d-44c9-ab7c-9f4ae008ad28" /><field column="WidgetCategoryPath" fieldcaption="WidgetCategoryPath" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="c7708ea9-a750-4e5e-92af-7079316e0a86" /><field column="WidgetCategoryLevel" fieldcaption="WidgetCategoryLevel" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c4224d1f-13c0-442f-9b75-521dea57fbd3" /><field column="WidgetCategoryOrder" fieldcaption="WidgetCategoryOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f0af481c-07e0-48e8-ae90-aac6d353f7cd" /><field column="WidgetCategoryChildCount" fieldcaption="WidgetCategoryChildCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="70aa81f0-9142-49f4-a9c2-6621f448f573" /><field column="WidgetCategoryWidgetChildCount" fieldcaption="WidgetCategoryWidgetChildCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a127d34b-ec04-40ab-bdb2-4d46697268f5" /><field column="WidgetCategoryImagePath" fieldcaption="WidgetCategoryImagePath" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="977af818-688b-43eb-89f1-fd720b8553d5" /><field column="WidgetCategoryGUID" fieldcaption="WidgetCategoryGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cedcae5c-aaff-41da-ba76-d120e5b9ed39" /><field column="WidgetCategoryLastModified" fieldcaption="WidgetCategoryLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f614a167-5ad4-4507-b05b-6b06afe26102" /></form>', '20100120 10:25:02', 2198, N'CMS.WidgetCategory', 0, 0, 0, '3e6a29e9-75e0-423c-b989-58b44e689a66', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_WidgetCategory', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WidgetCategory">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="WidgetCategoryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="WidgetCategoryName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WidgetCategoryDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WidgetCategoryParentID" type="xs:int" minOccurs="0" />
              <xs:element name="WidgetCategoryPath">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WidgetCategoryLevel" type="xs:int" />
              <xs:element name="WidgetCategoryOrder" type="xs:int" minOccurs="0" />
              <xs:element name="WidgetCategoryChildCount" type="xs:int" minOccurs="0" />
              <xs:element name="WidgetCategoryWidgetChildCount" type="xs:int" minOccurs="0" />
              <xs:element name="WidgetCategoryImagePath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WidgetCategoryGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="WidgetCategoryLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WidgetCategory" />
      <xs:field xpath="WidgetCategoryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'WidgetRole', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="WidgetID" fieldcaption="WidgetID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="7a693a1c-2362-41ed-8d71-efe36276ac49" visibility="none" ismacro="false" /><field column="RoleID" fieldcaption="RoleID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d32ed50f-96d8-4874-81f7-f9b46c5699a1" /><field column="PermissionID" fieldcaption="PermissionID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c62e9caf-e4d2-4627-86df-91650e9fa1fa" visibility="none" /></form>', '20100325 08:17:49', 2199, N'CMS.WidgetRole', 0, 0, 0, '6bd0e677-683e-4875-8dcb-686295c0320b', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_WidgetRole', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WidgetRole">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="WidgetID" type="xs:int" />
              <xs:element name="RoleID" type="xs:int" />
              <xs:element name="PermissionID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WidgetRole" />
      <xs:field xpath="WidgetID" />
      <xs:field xpath="RoleID" />
      <xs:field xpath="PermissionID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Page template scope', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="PageTemplateScopeID" fieldcaption="PageTemplateScopeID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="false" publicfield="false" spellcheck="true" guid="2198e351-37d1-413b-8478-96c340d4fa87" ismacro="false" /><field column="PageTemplateScopePath" visible="false" columntype="text" fieldtype="label" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="9546eb9d-d482-4df7-81d3-7269cf644ff8" visibility="none" ismacro="false" /><field column="PageTemplateScopeClassID" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6dce0208-c825-49b2-809f-c945a3abe9ff" visibility="none" ismacro="false" /><field column="PageTemplateScopeCultureID" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="aab07400-a230-4de9-81a1-9db8be770830" visibility="none" ismacro="false" /><field column="PageTemplateScopeLevels" visible="false" columntype="text" fieldtype="label" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="31e04b8d-0afe-4828-9b19-ea18be43c132" visibility="none" ismacro="false" /><field column="PageTemplateScopeSiteID" fieldcaption="PageTemplateScopeSiteID" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2ba9fc4f-2ec2-400e-9676-d077214eba69" visibility="none" ismacro="false" /><field column="PageTemplateScopeTemplateID" visible="false" columntype="integer" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a86754fa-ce7c-4997-9082-f44adf46b012" visibility="none" ismacro="false" /><field column="PageTemplateScopeLastModified" visible="false" columntype="datetime" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ca965801-5580-4dc5-88cf-59601e7f5a2a" visibility="none" ismacro="false"><settings><timezonetype>inherit</timezonetype></settings></field><field column="PageTemplateScopeGUID" visible="false" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4be17f8e-dcca-452c-927a-e7c94c4a4187" visibility="none" ismacro="false" /></form>', '20100302 15:47:50', 2337, N'cms.pagetemplatescope', 0, 0, 0, '9bd4c694-369e-444e-842e-947383f8da43', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_PageTemplateScope', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_PageTemplateScope">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="PageTemplateScopeID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="PageTemplateScopePath">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateScopeLevels" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PageTemplateScopeCultureID" type="xs:int" minOccurs="0" />
              <xs:element name="PageTemplateScopeClassID" type="xs:int" minOccurs="0" />
              <xs:element name="PageTemplateScopeTemplateID" type="xs:int" />
              <xs:element name="PageTemplateScopeSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="PageTemplateScopeLastModified" type="xs:dateTime" />
              <xs:element name="PageTemplateScopeGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_PageTemplateScope" />
      <xs:field xpath="PageTemplateScopeID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'OpenIDUser', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="OpenIDUserID" fieldcaption="OpenIDUserID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e41dcedb-534b-4546-8822-7b22a484340b" ismacro="false" visibility="none" /><field column="OpenID" fieldcaption="OpenID" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="4dfa8c83-42e4-4856-ba2b-d1b789f18ff0" ismacro="false" /><field column="OpenIDProviderURL" fieldcaption="OpenIDProviderURL" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="29626164-9916-4631-926f-fa32554566c5" ismacro="false" visibility="none" /><field column="UserID" fieldcaption="UserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="98e15d62-ce90-4fda-9736-02444c4470a9" ismacro="false" visibility="none" /></form>', '20100421 13:08:21', 2371, N'cms.OpenIDUser', 0, 0, 0, '1d4b85e6-4780-4f85-a0ab-894ac18d261a', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'cms_OpenIDUser', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="cms_OpenIDUser">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="OpenIDUserID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="OpenID">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="OpenIDProviderURL" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//cms_OpenIDUser" />
      <xs:field xpath="OpenIDUserID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 0)
set identity_insert [cms_class] off
