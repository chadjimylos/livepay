CREATE TABLE [COM_Order] (
		[OrderID]                    int NOT NULL IDENTITY(1, 1),
		[OrderBillingAddressID]      int NOT NULL,
		[OrderShippingAddressID]     int NOT NULL,
		[OrderShippingOptionID]      int NULL,
		[OrderTotalShipping]         float NULL,
		[OrderTotalPrice]            float NOT NULL,
		[OrderTotalTax]              float NOT NULL,
		[OrderDate]                  datetime NOT NULL,
		[OrderStatusID]              int NOT NULL,
		[OrderCurrencyID]            int NULL,
		[OrderCustomerID]            int NOT NULL,
		[OrderCreatedByUserID]       int NULL,
		[OrderNote]                  nvarchar(max) NULL,
		[OrderSiteID]                int NOT NULL,
		[OrderPaymentOptionID]       int NULL,
		[OrderInvoice]               nvarchar(max) NULL,
		[OrderInvoiceNumber]         nvarchar(200) NULL,
		[OrderDiscountCouponID]      int NULL,
		[OrderCompanyAddressID]      int NULL,
		[OrderTrackingNumber]        nvarchar(100) NULL,
		[OrderCustomData]            nvarchar(max) NULL,
		[OrderPaymentResult]         nvarchar(max) NULL,
		[OrderGUID]                  uniqueidentifier NOT NULL,
		[OrderLastModified]          datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_Order]
	ADD
	CONSTRAINT [PK_COM_Order]
	PRIMARY KEY
	NONCLUSTERED
	([OrderID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Address]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Order_OrderBillingAddressID_COM_Adress]') IS NULL
BEGIN
		ALTER TABLE [COM_Order]
			ADD CONSTRAINT [FK_COM_Order_OrderBillingAddressID_COM_Adress]
			FOREIGN KEY ([OrderBillingAddressID]) REFERENCES [COM_Address] ([AddressID])
END
IF OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Address]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Order_OrderCompanyAddressID_COM_Address]') IS NULL
BEGIN
		ALTER TABLE [COM_Order]
			ADD CONSTRAINT [FK_COM_Order_OrderCompanyAddressID_COM_Address]
			FOREIGN KEY ([OrderCompanyAddressID]) REFERENCES [COM_Address] ([AddressID])
END
IF OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Currency]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Order_OrderCurrencyID_COM_Currency]') IS NULL
BEGIN
		ALTER TABLE [COM_Order]
			ADD CONSTRAINT [FK_COM_Order_OrderCurrencyID_COM_Currency]
			FOREIGN KEY ([OrderCurrencyID]) REFERENCES [COM_Currency] ([CurrencyID])
END
IF OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Order_OrderCustomerID_COM_Customer]') IS NULL
BEGIN
		ALTER TABLE [COM_Order]
			ADD CONSTRAINT [FK_COM_Order_OrderCustomerID_COM_Customer]
			FOREIGN KEY ([OrderCustomerID]) REFERENCES [COM_Customer] ([CustomerID])
END
IF OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[COM_DiscountCoupon]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Order_OrderDiscountCouponID_COM_DiscountCoupon]') IS NULL
BEGIN
		ALTER TABLE [COM_Order]
			ADD CONSTRAINT [FK_COM_Order_OrderDiscountCouponID_COM_DiscountCoupon]
			FOREIGN KEY ([OrderDiscountCouponID]) REFERENCES [COM_DiscountCoupon] ([DiscountCouponID])
END
IF OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[COM_PaymentOption]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Order_OrderPaymentOptionID_COM_PaymentOption]') IS NULL
BEGIN
		ALTER TABLE [COM_Order]
			ADD CONSTRAINT [FK_COM_Order_OrderPaymentOptionID_COM_PaymentOption]
			FOREIGN KEY ([OrderPaymentOptionID]) REFERENCES [COM_PaymentOption] ([PaymentOptionID])
END
IF OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Address]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Order_OrderShippingAddressID_COM_Adress]') IS NULL
BEGIN
		ALTER TABLE [COM_Order]
			ADD CONSTRAINT [FK_COM_Order_OrderShippingAddressID_COM_Adress]
			FOREIGN KEY ([OrderShippingAddressID]) REFERENCES [COM_Address] ([AddressID])
END
IF OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[COM_ShippingOption]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Order_OrderShippingOptionID_COM_ShippingOption]') IS NULL
BEGIN
		ALTER TABLE [COM_Order]
			ADD CONSTRAINT [FK_COM_Order_OrderShippingOptionID_COM_ShippingOption]
			FOREIGN KEY ([OrderShippingOptionID]) REFERENCES [COM_ShippingOption] ([ShippingOptionID])
END
IF OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Order_OrderSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [COM_Order]
			ADD CONSTRAINT [FK_COM_Order_OrderSiteID_CMS_Site]
			FOREIGN KEY ([OrderSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[COM_OrderStatus]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_Order_OrderStatusID_COM_Status]') IS NULL
BEGIN
		ALTER TABLE [COM_Order]
			ADD CONSTRAINT [FK_COM_Order_OrderStatusID_COM_Status]
			FOREIGN KEY ([OrderStatusID]) REFERENCES [COM_OrderStatus] ([StatusID])
END
CREATE INDEX [IX_COM_Order_OrderBillingAddressID]
	ON [COM_Order] ([OrderBillingAddressID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Order_OrderCompanyAddressID]
	ON [COM_Order] ([OrderCompanyAddressID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Order_OrderCurrencyID]
	ON [COM_Order] ([OrderCurrencyID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Order_OrderCustomerID]
	ON [COM_Order] ([OrderCustomerID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Order_OrderDiscountCouponID]
	ON [COM_Order] ([OrderDiscountCouponID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Order_OrderPaymentOptionID]
	ON [COM_Order] ([OrderPaymentOptionID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Order_OrderShippingAddressID]
	ON [COM_Order] ([OrderShippingAddressID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_Order_OrderShippingOptionID]
	ON [COM_Order] ([OrderShippingOptionID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_Order_OrderSiteID_OrderDate]
	ON [COM_Order] ([OrderSiteID], [OrderDate] DESC)
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_Order_OrderStatusID]
	ON [COM_Order] ([OrderStatusID])
	ON [PRIMARY]
