using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSImportExport;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.FormEngine;
using CMS.EmailEngine;
using CMS.Scheduler;
using CMS.PortalEngine;
using CMS.TreeEngine;

public partial class CMSModules_ImportExport_Controls_Export___objects__ : ImportExportControl
{
    /// <summary>
    /// Export settings
    /// </summary>
    public SiteExportSettings ExportSettings
    {
        get 
        {
            if (this.Settings != null)
            {
                return (SiteExportSettings)this.Settings;
            }
            return null;
        }
        set
        {
        	 this.Settings = value; 
        }
    }
	

    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblInfo.Text = ResHelper.GetString("ExportObjects.Info");
        this.lblSelection.Text = ResHelper.GetString("ImportObjects.Selection");
        this.lblSettings.Text = ResHelper.GetString("ExportObjects.Settings");
        
        this.lnkSelectAll.Text = ResHelper.GetString("ImportObjects.SelectAll");
        this.lnkSelectNone.Text = ResHelper.GetString("ImportObjects.SelectNone");
        this.lnkSelectDefault.Text = ResHelper.GetString("ImportObjects.SelectDefault");

        this.chkCopyFiles.Text = ResHelper.GetString("ExportObjects.CopyObjectFiles");
        this.chkCopyGlobalFiles.Text = ResHelper.GetString("ExportObjects.CopyFiles");
        this.chkCopySiteFiles.Text = ResHelper.GetString("ExportObjects.CopySiteFiles");
        this.chkCopyASPXTemplatesFolder.Text = ResHelper.GetString("ExportObjects.CopyASPXTemplatesFolder");
        this.chkCopyForumCustomLayoutsFolder.Text = ResHelper.GetString("ExportObjects.CopyForumCustomLayuoutsFolder");
        this.chkExportTasks.Text = ResHelper.GetString("ExportObjects.ExportTasks");
        
        // Javascript
        string script = "var ex_g_parent = document.getElementById('" + chkCopyFiles.ClientID + "'); \n" +
                        "var ex_g_childIDs = ['" + chkCopyGlobalFiles.ClientID + "', '" + chkCopySiteFiles.ClientID + "', '" + chkCopyASPXTemplatesFolder.ClientID + "', '" + chkCopyForumCustomLayoutsFolder.ClientID + "']; \n" +
                        "InitCheckboxes(); \n";

        ltlScript.Text = ScriptHelper.GetScript(script);

        this.chkCopyFiles.Attributes.Add("onclick", "CheckChange();");
    }


    /// <summary>
    /// Get settings
    /// </summary>
    public override void SaveSettings()
    {
        this.ExportSettings.CopyFiles = this.chkCopyFiles.Checked;

        // Copy files property is stronger
        bool copyGlobal = this.chkCopyFiles.Checked ? this.chkCopyGlobalFiles.Checked : false;
        bool copySite = this.chkCopyFiles.Checked ? this.chkCopySiteFiles.Checked : false;
        bool copyASPX = this.chkCopyFiles.Checked ? this.chkCopyASPXTemplatesFolder.Checked : false;
        bool copyForumLayouts = this.chkCopyFiles.Checked ? this.chkCopyForumCustomLayoutsFolder.Checked : false;

        this.ExportSettings.SetSettings(ImportExportHelper.SETTINGS_GLOBAL_FOLDERS, copyGlobal);
        this.ExportSettings.SetSettings(ImportExportHelper.SETTINGS_SITE_FOLDERS, copySite);
        this.ExportSettings.SetSettings(ImportExportHelper.SETTINGS_COPY_ASPX_TEMPLATES_FOLDER, copyASPX);
        this.ExportSettings.SetSettings(ImportExportHelper.SETTINGS_COPY_FORUM_CUSTOM_LAYOUTS_FOLDER, copyForumLayouts);
        this.ExportSettings.SetSettings(ImportExportHelper.SETTINGS_TASKS, chkExportTasks.Checked);
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData()
    {
        // Hide copy files option
        if (this.ExportSettings.SiteId == 0)
        {
            this.plcSiteFiles.Visible = false;
        }

        this.chkCopyFiles.Checked = this.ExportSettings.CopyFiles;
        this.chkCopyGlobalFiles.Checked = ValidationHelper.GetBoolean(this.ExportSettings.GetSettings(ImportExportHelper.SETTINGS_GLOBAL_FOLDERS), true);
        this.chkCopySiteFiles.Checked = ValidationHelper.GetBoolean(this.ExportSettings.GetSettings(ImportExportHelper.SETTINGS_SITE_FOLDERS), true);
        this.chkCopyASPXTemplatesFolder.Checked = ValidationHelper.GetBoolean(this.ExportSettings.GetSettings(ImportExportHelper.SETTINGS_COPY_ASPX_TEMPLATES_FOLDER), true);
        this.chkCopyForumCustomLayoutsFolder.Checked = ValidationHelper.GetBoolean(this.ExportSettings.GetSettings(ImportExportHelper.SETTINGS_COPY_FORUM_CUSTOM_LAYOUTS_FOLDER), true);
        this.chkExportTasks.Checked = ValidationHelper.GetBoolean(this.ExportSettings.GetSettings(ImportExportHelper.SETTINGS_TASKS), true);
    }


    protected void lnkSelectAll_Click(object sender, EventArgs e)
    {
        ExportTypeEnum exportType = this.ExportSettings.ExportType;
        DateTime timeStamp = this.ExportSettings.TimeStamp;

        this.ExportSettings.ExportType = ExportTypeEnum.All;
        this.ExportSettings.TimeStamp = DateTimeHelper.ZERO_TIME;
        this.ExportSettings.LoadDefaultSelection(false);

        SaveSettings();

        this.ExportSettings.ExportType = exportType;
        this.ExportSettings.TimeStamp = timeStamp;

        this.lblInfo.Text = ResHelper.GetString("ImportObjects.AllSelected");
    }


    protected void lnkSelectNone_Click(object sender, EventArgs e)
    {
        ExportTypeEnum exportType = this.ExportSettings.ExportType;
        DateTime timeStamp = this.ExportSettings.TimeStamp;

        this.ExportSettings.ExportType = ExportTypeEnum.None;
        this.ExportSettings.TimeStamp = DateTimeHelper.ZERO_TIME;
        this.ExportSettings.LoadDefaultSelection(false);

        SaveSettings();

        this.ExportSettings.ExportType = exportType;
        this.ExportSettings.TimeStamp = timeStamp;

        this.lblInfo.Text = ResHelper.GetString("ImportObjects.NoneSelected");
    }


    protected void lnkSelectDefault_Click(object sender, EventArgs e)
    {
        this.ExportSettings.LoadDefaultSelection(false);

        SaveSettings();

        this.lblInfo.Text = ResHelper.GetString("ImportObjects.DefaultSelected");
    }
}
