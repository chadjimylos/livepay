<%@ Page Language="C#" AutoEventWireup="true" CodeFile="License_List.aspx.cs" Inherits="CMSSiteManager_Licenses_License_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Licenses - Licenses List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <cms:UniGrid ID="UniGridLicenses" runat="server" GridName="License_List.xml" OrderBy="LicenseDomain"
        IsLiveSite="false" />
</asp:Content>
