<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UICulture_Strings_List.aspx.cs"
    Inherits="CMSSiteManager_Development_UICultures_UICulture_Strings_List" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:UniGrid ID="gridStrings" runat="server" GridName="UICulture_Strings_List.xml"
        OrderBy="StringKey" IsLiveSite="false" />
</asp:Content>
