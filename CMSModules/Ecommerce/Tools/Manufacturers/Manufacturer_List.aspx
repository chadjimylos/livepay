<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Manufacturer_List.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Manufacturers_Manufacturer_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Manufacturers" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Manufacturer_List.xml" IsLiveSite="false"
        Columns="ManufacturerID, ManufacturerDisplayName, ManufacturerEnabled" OrderBy="ManufacturerDisplayName" />
</asp:Content>
