using System;
using System.Collections;
using System.Web.UI;
using System.Security.Principal;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSAdminControls_AsyncControl : CMSUserControl, ICallbackEventHandler
{
    #region "Variables"

    /// <summary>
    /// Table of the worker processes
    /// </summary>
    private static readonly Hashtable mWorkers = new Hashtable();


    private AsyncWorker mWorker = null;
    private string mCallbackResult = null;
    private bool mPostbackOnError = true;

    private string mLog = null;
    private bool mAscendantLog = false;

    private bool mRenderScripts = false;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets the current log
    /// </summary>
    public string Log
    {
        get
        {
            if ((mLog == null) && (OnRequestLog != null))
            {
                OnRequestLog(this, new EventArgs());
            }

            return mLog;
        }
        set
        {
            mLog = value;
        }
    }


    /// <summary>
    /// Process GUID
    /// </summary>
    public Guid ProcessGUID
    {
        get
        {
            if (ViewState["ProcessGUID"] == null)
            {
                ViewState["ProcessGUID"] = Guid.NewGuid();
            }
            return ValidationHelper.GetGuid(ViewState["ProcessGUID"], Guid.NewGuid());
        }
        set
        {
            ViewState["ProcessGUID"] = value;
        }
    }


    /// <summary>
    /// Asynchronous worker
    /// </summary>
    public AsyncWorker Worker
    {
        get
        {
            if (mWorker == null)
            {
                string key = "AsyncWorker_" + ProcessGUID;
                mWorker = (AsyncWorker)mWorkers[key];
                if (mWorker == null)
                {
                    mWorker = new AsyncWorker();

                    // Raises the postbacks twice
                    //mWorker.OnError += new EventHandler(this.RaiseError);
                    //mWorker.OnFinished += new EventHandler(this.RaiseFinished);

                    mWorkers[key] = mWorker;
                }
            }

            return mWorker;
        }
    }


    /// <summary>
    /// True if the postback should occure after error
    /// </summary>
    public bool PostbackOnError
    {
        get
        {
            return mPostbackOnError;
        }
        set
        {
            mPostbackOnError = value;
        }
    }


    /// <summary>
    /// Process parameter
    /// </summary>
    public new object Parameter
    {
        get
        {
            return Worker.Parameter;
        }
        set
        {
            Worker.Parameter = value;
        }
    }


    /// <summary>
    /// Indicates if the logging is ascendant
    /// </summary>
    public bool AscendantLog
    {
        get
        {
            return mAscendantLog;
        }
        set
        {
            mAscendantLog = value;
        }
    }


    /// <summary>
    /// Gets the worker status
    /// </summary>
    public AsyncWorkerStatusEnum Status
    {
        get
        {
            return Worker.Status;
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Returns true if the worker for current control exists
    /// </summary>
    protected bool WorkerExists()
    {
        string key = "AsyncWorker_" + ProcessGUID;
        return (mWorkers[key] != null);
    }


    /// <summary>
    /// Runs the asynchronous process without any thread
    /// </summary>
    public void RunAsync()
    {
        RenderScripts();
    }


    /// <summary>
    /// Runs the asynchronous action
    /// </summary>
    /// <param name="action">Action to run</param>
    /// <param name="wi">Windows identity (windows user)</param>
    public void RunAsync(AsyncAction action, WindowsIdentity wi)
    {
        mRenderScripts = true;

        Worker.Stop();
        Worker.Reset();

        Worker.RunAsync(action, wi);
    }


    /// <summary>
    /// Stops the worker
    /// </summary>
    public void Stop()
    {
        Worker.Stop();
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        btnFinished.Attributes.Add("style", "display: none;");
        btnError.Attributes.Add("style", "display: none;");
        btnCancel.Attributes.Add("style", "display: none;");
    }


    protected void RenderScripts()
    {
        ltlScript.Text += ScriptHelper.GetScript(
            @"var logText = '';
            var asyncProcessFinished = false;

            function CancelAction() { asyncProcessFinished = true; " + Page.ClientScript.GetPostBackEventReference(btnCancel, null) + @" }
            function CancelWithoutPostback() { asyncProcessFinished = true; clearInterval(interval); }
            
            function AC_GetAsyncStatus() { " + Page.ClientScript.GetCallbackEventReference(this, "logText.length", "AC_ReceiveAsyncStatus", "logText.length", true) + @"; }
            function AC_SetClose() { CancelWithoutPostback(); var cancelElem = document.getElementById('btnCancel'); if (cancelElem != null) { cancelElem.value = '" + ResHelper.GetString("General.Close") + @"'; } }
            
            function AC_ReceiveAsyncStatus(rvalue, context) {
                if (asyncProcessFinished) return;
                values = rvalue.split('|');
                code = values[0]; 
                var i = 1;
                var resultValue = '';                            
                for(i = 1;i<values.length;i++)
                {
                    resultValue += values[i];
                }
                AC_SetLog(resultValue, context);
                if ( code == 'finished' ) { asyncProcessFinished = true;" + Page.ClientScript.GetPostBackEventReference(btnFinished, null) + @" }
                else if ( code == 'running') {}
                else if ( code == 'threadlost') { asyncProcessFinished = true; AC_SetClose(); }
                else if ( code == 'error' ) { asyncProcessFinished = true; " + (PostbackOnError ? Page.ClientScript.GetPostBackEventReference(btnError, null) : "AC_SetClose()") + @" }
                
            }
            
            function AC_SetLog(text, length) {
                var elem = document.getElementById('AsyncLog');
                var messageText = logText;" +
                (AscendantLog ? "   messageText = messageText.substring(0, length) + text;\n" : "   messageText = text + messageText.substring(messageText.length - length);\n") +
                @"if(messageText.length > logText.length){ logText = elem.innerHTML = messageText; }
            }

            var interval = setInterval('AC_GetAsyncStatus()', 200);"
            );
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!Page.IsCallback || mRenderScripts)
        {
            switch (Worker.Status)
            {
                // Running, cyclic check the status
                case AsyncWorkerStatusEnum.Running:
                case AsyncWorkerStatusEnum.WaitForFinish:
                    RenderScripts();
                    break;

                default:
                    break;
            }
        }
    }

    #endregion


    #region "Callback handling"

    /// <summary>
    /// Raises the callback event
    /// </summary>
    /// <param name="eventArgument">Event argument</param>
    public void RaiseCallbackEvent(string eventArgument)
    {
        int requestedLength = ValidationHelper.GetInteger(eventArgument, 0);
        mCallbackResult = "";

        if (WorkerExists())
        {
            switch (Worker.Status)
            {
                case AsyncWorkerStatusEnum.Finished:
                case AsyncWorkerStatusEnum.WaitForFinish:
                    // Allow worker to finish
                    mCallbackResult = "finished";
                    break;

                case AsyncWorkerStatusEnum.Running:
                    mCallbackResult = "running";
                    break;

                case AsyncWorkerStatusEnum.Error:
                    // Allow worker to finish
                    mCallbackResult = "error";
                    break;

                default:
                    break;
            }

            string log = Log;
            if (!string.IsNullOrEmpty(log))
            {
                int logLength = log.Length;
                if (requestedLength > logLength)
                {
                    requestedLength = logLength;
                }

                if (AscendantLog)
                {
                    log = log.Substring(requestedLength);
                }
                else
                {
                    log = log.Substring(0, logLength - requestedLength);
                }
            }

            mCallbackResult += "|" + log;
        }
        else
        {
            mCallbackResult += "threadlost|" + ResHelper.GetString("AsyncControl.ThreadLost");
        }
    }


    /// <summary>
    /// Returns the result of a callback
    /// </summary>
    public string GetCallbackResult()
    {
        return mCallbackResult;
    }

    #endregion


    #region "Events"

    /// <summary>
    /// Finished event handler
    /// </summary>
    public event EventHandler OnFinished;


    /// <summary>
    /// Error event handler
    /// </summary>
    public new event EventHandler OnError;


    /// <summary>
    /// Cancel event handler
    /// </summary>
    public event EventHandler OnCancel;


    /// <summary>
    /// Error event handler
    /// </summary>
    public event EventHandler OnRequestLog;


    /// <summary>
    /// Raises the finished event
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event args</param>
    public void RaiseFinished(object sender, EventArgs e)
    {
        if (OnFinished != null)
        {
            OnFinished(this, e);
        }

        // Remove worker
        //mWorkers["AsyncWorker_" + ProcessGUID] = null;
    }


    /// <summary>
    /// Raises the Error event
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event args</param>
    public void RaiseError(object sender, EventArgs e)
    {
        if (OnError != null)
        {
            OnError(this, e);
        }
    }


    /// <summary>
    /// Raises the Cancel event
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Event args</param>
    public void RaiseCancel(object sender, EventArgs e)
    {
        if (OnCancel != null)
        {
            OnCancel(this, e);
        }
    }


    protected void btnFinished_Click(object sender, EventArgs e)
    {
        RaiseFinished(this, e);
    }


    protected void btnError_Click(object sender, EventArgs e)
    {
        RaiseError(this, e);
    }


    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if (WorkerExists())
        {
            if (Worker.Status == AsyncWorkerStatusEnum.Running)
            {
                Worker.Stop();
            }
        }

        RaiseCancel(this, e);
    }

    #endregion
}
