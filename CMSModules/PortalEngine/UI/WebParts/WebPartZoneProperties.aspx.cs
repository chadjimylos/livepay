using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSModules_PortalEngine_UI_WebParts_WebPartZoneProperties : CMSModalDesignPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check UI elements for web part zone
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        if (!currentUser.IsAuthorizedPerUIElement("CMS.Content", new string[] { "Design", "Design.WebPartZoneProperties" }, CMSContext.CurrentSiteName))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Design;Design.WebPartZoneProperties");
        }

        // Title
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("webpartzone.propertiesheader");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_PortalEngine/WebpartZoneProperties/title.png");
        this.CurrentMaster.Title.HelpTopicName = "webpartzoneproperties";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // UI Strings
        this.btnOk.Text = ResHelper.GetString("general.ok");
        this.btnApply.Text = ResHelper.GetString("general.apply");
        this.btnCancel.Text = ResHelper.GetString("general.cancel");
        this.chkRefresh.Text = ResHelper.GetString("webpartzone.refresh");
    }


    /// <summary>
    /// Saves the webpart zone properties and closes the window.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Save webpart properties
        if (this.webPartZonePropertiesElem.Save())
        {
            if (this.chkRefresh.Checked)
            {
                ltlScript.Text += ScriptHelper.GetScript("wopener.location.replace(wopener.location); ");
            }

            // Close the window
            ltlScript.Text += ScriptHelper.GetScript("window.close();");
        }
    }


    /// <summary>
    /// Saves the webpart zone properties
    /// </summary>
    protected void btnApply_Click(object sender, EventArgs e)
    {
        this.webPartZonePropertiesElem.Save();
    }
}
