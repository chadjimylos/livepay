using System;
using System.Web.UI.WebControls;
using System.Data;

using CMS.Community;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;


public partial class CMSModules_Groups_Controls_Members_MemberList : CMSAdminListControl
{
    #region "Variables"

    private int mGroupId = 0;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the group ID for which the members should be displayed
    /// </summary>
    public int GroupID
    {
        get
        {
            return this.mGroupId;
        }
        set
        {
            this.mGroupId = value;
            this.gridElem.WhereCondition = CreateWhereCondition();
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Unigrid
        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        gridElem.WhereCondition = CreateWhereCondition();
        gridElem.ImageDirectoryPath = GetImageUrl("Design/Controls/UniGrid/Actions/", IsLiveSite, true);
        gridElem.IsLiveSite = this.IsLiveSite;
        gridElem.OnBeforeDataReload += new OnBeforeDataReload(gridElem_OnBeforeDataReload);
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }

    /// <summary>
    /// Reloads the grid data.
    /// </summary>
    public void ReloadGrid()
    {
        this.gridElem.ReloadData();
    }


    #region "GridView actions handling"

    /// <summary>
    /// On before data reyload action 
    /// </summary>
    void gridElem_OnBeforeDataReload()
    {
        string where = CreateWhereCondition();
        if (!string.IsNullOrEmpty(filterMembers.WhereCondition))
        {
            where = where + " AND (" + filterMembers.WhereCondition + ")";
        }

        gridElem.WhereCondition = where;
    }

    /// <summary>
    /// Unigrid OnExternalDataBound event
    /// </summary>
    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        GroupMemberStatus status = GroupMemberStatus.Approved;
        DataRowView drv = null;
        GridViewRow gvr = null;
        bool current = false;

        switch (sourceName.ToLower())
        {
            case "memberapprovedwhen":
            case "memberrejectedwhen":
                if (parameter != DBNull.Value)
                {
                    return CMSContext.ConvertDateTime(Convert.ToDateTime(parameter), this);
                }
                break;

            case "approve":
                gvr = parameter as GridViewRow;
                if (gvr != null)
                {
                    drv = gvr.DataItem as DataRowView;
                    if (drv != null)
                    {
                        if (IsLiveSite && (CMSContext.CurrentUser.UserID == ValidationHelper.GetInteger(drv["MemberUserID"], 0)))
                        {
                            current = true;
                        }

                        status = (GroupMemberStatus)ValidationHelper.GetInteger(drv["MemberStatus"], 0);
                        if (!current && (status != GroupMemberStatus.Approved))
                        {
                            ImageButton button = ((ImageButton)sender);
                            button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Approve.png");
                            button.ToolTip = ResHelper.GetString("general.approve");
                            button.Enabled = true;
                        }
                        else
                        {
                            ImageButton button = ((ImageButton)sender);
                            button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/approvedisabled.png");
                            button.ToolTip = ResHelper.GetString("general.approve");
                            button.Enabled = false;
                        }
                    }
                }

                break;

            case "reject":
                gvr = parameter as GridViewRow;
                if (gvr != null)
                {
                    drv = gvr.DataItem as DataRowView;
                    if (drv != null)
                    {
                        if (IsLiveSite && (CMSContext.CurrentUser.UserID == ValidationHelper.GetInteger(drv.Row["MemberUserID"], 0)))
                        {
                            current = true;
                        }

                        status = (GroupMemberStatus)ValidationHelper.GetInteger(drv["MemberStatus"], 0);
                        if (!current && (status != GroupMemberStatus.Rejected))
                        {
                            ImageButton button = ((ImageButton)sender);
                            button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Reject.png");
                            button.ToolTip = ResHelper.GetString("general.reject");
                            button.Enabled = true;
                        }
                        else
                        {
                            ImageButton button = ((ImageButton)sender);
                            button.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/rejectdisabled.png");
                            button.ToolTip = ResHelper.GetString("general.reject");
                            button.Enabled = false;
                        }
                    }
                }
                break;

            case "formattedusername":
                return HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(Convert.ToString(parameter), this.IsLiveSite));

        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        switch (actionName)
        {
            case "delete":
            case "approve":
            case "reject":

                if (!CheckPermissions("cms.groups", CMSAdminControl.PERMISSION_MANAGE, this.GroupID))
                {
                    return;
                }

                break;
        }

        if (actionName == "delete")
        {
            GroupMemberInfoProvider.DeleteGroupMemberInfo(ValidationHelper.GetInteger(actionArgument, 0));
        }
        else if (actionName == "approve")
        {
            GroupMemberInfo gmi = GroupMemberInfoProvider.GetGroupMemberInfo(ValidationHelper.GetInteger(actionArgument, 0));
            if (gmi != null)
            {
                gmi.MemberApprovedByUserID = CMSContext.CurrentUser.UserID;
                gmi.MemberStatus = GroupMemberStatus.Approved;
                gmi.MemberApprovedWhen = DateTime.Now;
                gmi.MemberRejectedWhen = DataHelper.DATETIME_NOT_SELECTED;
                GroupMemberInfoProvider.SetGroupMemberInfo(gmi);
                GroupInfo group = GroupInfoProvider.GetGroupInfo(this.GroupID);
                if ((group != null) && (group.GroupSendWaitingForApprovalNotification))
                {
                    GroupMemberInfoProvider.SendNotificationMail("Groups.MemberApproved", CMSContext.CurrentSiteName, gmi, false);
                }
            }
        }
        else if (actionName == "reject")
        {
            GroupMemberInfo gmi = GroupMemberInfoProvider.GetGroupMemberInfo(ValidationHelper.GetInteger(actionArgument, 0));
            if (gmi != null)
            {
                gmi.MemberApprovedByUserID = CMSContext.CurrentUser.UserID;
                gmi.MemberStatus = GroupMemberStatus.Rejected;
                gmi.MemberApprovedWhen = DataHelper.DATETIME_NOT_SELECTED;
                gmi.MemberRejectedWhen = DateTime.Now;
                GroupMemberInfoProvider.SetGroupMemberInfo(gmi);
                GroupInfo group = GroupInfoProvider.GetGroupInfo(this.GroupID);
                if ((group != null) && (group.GroupSendWaitingForApprovalNotification))
                {
                    GroupMemberInfoProvider.SendNotificationMail("Groups.MemberRejected", CMSContext.CurrentSiteName, gmi, false);
                }
            }
        }
        this.RaiseOnAction(actionName, actionArgument);
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Creates where condition for unigrid according to the parameters
    /// </summary>
    private string CreateWhereCondition()
    {
        string where = null;

        if (this.mGroupId > 0)
        {
            where = "(MemberGroupID = " + this.mGroupId + ") AND (SiteID = " + CMSContext.CurrentSiteID + ")";
        }

        return where;
    }

    #endregion
}
