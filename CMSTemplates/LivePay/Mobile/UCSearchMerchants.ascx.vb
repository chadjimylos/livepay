﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.SiteProvider
Imports CMS.SettingsProvider
Imports CMS.DataEngine
Imports CMS.TreeEngine

Partial Class CMSTemplates_LivePay_Mobile_UCSearchMerchants
    Inherits CMSUserControl

    Public Overloads Sub ReloadData()

    End Sub

    Protected Sub btn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn.Click


        'Dim tree As TreeProvider = New TreeProvider() '(DiscreetTitle like '%' + @CompanyName + '%' OR CompanyName like '%' + @CompanyName + '%') or @CompanyName = '')
        'Dim ds As DataSet = tree.SelectNodes(CMSContext.CurrentSiteName, "/%", CMSContext.PreferredCultureCode, False, "LivePay.Merchant;LivePay.MerchantPublicSector", " (DiscreetTitle COLLATE Greek_CI_AI like N'" & txt.Text & "%' )", Nothing, -1, True)
        Dim ds As DataSet = DBConnection.SearchMerchant("Proc_LivePay_SearchMerchant", txt.Text, 0, 0, 0, CMSContext.CurrentDocument.DocumentCulture)
        div_results.Visible = True
        GVMerchants.DataSource = ds
        GVMerchants.DataBind()
    End Sub


End Class
