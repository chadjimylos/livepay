CREATE TABLE [CMS_RolePermission] (
		[RoleID]           int NOT NULL,
		[PermissionID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_RolePermission]
	ADD
	CONSTRAINT [PK_CMS_RolePermission]
	PRIMARY KEY
	([RoleID], [PermissionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_RolePermission]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Permission]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_RolePermission_PermissionID_CMS_Permission]') IS NULL
BEGIN
		ALTER TABLE [CMS_RolePermission]
			ADD CONSTRAINT [FK_CMS_RolePermission_PermissionID_CMS_Permission]
			FOREIGN KEY ([PermissionID]) REFERENCES [CMS_Permission] ([PermissionID])
END
IF OBJECT_ID(N'[CMS_RolePermission]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_RolePermission_RoleID_CMS_Role]') IS NULL
BEGIN
		ALTER TABLE [CMS_RolePermission]
			ADD CONSTRAINT [FK_CMS_RolePermission_RoleID_CMS_Role]
			FOREIGN KEY ([RoleID]) REFERENCES [CMS_Role] ([RoleID])
END
