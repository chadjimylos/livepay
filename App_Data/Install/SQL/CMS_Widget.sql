CREATE TABLE [CMS_Widget] (
		[WidgetID]               int NOT NULL IDENTITY(1, 1),
		[WidgetWebPartID]        int NOT NULL,
		[WidgetDisplayName]      nvarchar(100) NOT NULL,
		[WidgetName]             nvarchar(100) NOT NULL,
		[WidgetDescription]      nvarchar(max) NULL,
		[WidgetCategoryID]       int NOT NULL,
		[WidgetProperties]       nvarchar(max) NULL,
		[WidgetSecurity]         int NOT NULL,
		[WidgetGUID]             uniqueidentifier NOT NULL,
		[WidgetLastModified]     datetime NOT NULL,
		[WidgetIsEnabled]        bit NOT NULL,
		[WidgetForGroup]         bit NOT NULL,
		[WidgetForEditor]        bit NOT NULL,
		[WidgetForUser]          bit NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Widget]
	ADD
	CONSTRAINT [PK_CMS_Widget]
	PRIMARY KEY
	NONCLUSTERED
	([WidgetID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_Widget]
	ADD
	CONSTRAINT [DEFAULT_CMS_Widget_WidgetForEditor]
	DEFAULT ((0)) FOR [WidgetForEditor]
ALTER TABLE [CMS_Widget]
	ADD
	CONSTRAINT [DEFAULT_CMS_Widget_WidgetForGroup]
	DEFAULT ((0)) FOR [WidgetForGroup]
ALTER TABLE [CMS_Widget]
	ADD
	CONSTRAINT [DEFAULT_CMS_Widget_WidgetForUser]
	DEFAULT ((0)) FOR [WidgetForUser]
ALTER TABLE [CMS_Widget]
	ADD
	CONSTRAINT [DEFAULT_CMS_Widget_WidgetIsEnabled]
	DEFAULT ((0)) FOR [WidgetIsEnabled]
ALTER TABLE [CMS_Widget]
	ADD
	CONSTRAINT [DF_CMS_Widget_WidgetSecurity]
	DEFAULT ((2)) FOR [WidgetSecurity]
IF OBJECT_ID(N'[CMS_Widget]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WidgetCategory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Widget_WidgetCategoryID_CMS_WidgetCategory]') IS NULL
BEGIN
		ALTER TABLE [CMS_Widget]
			ADD CONSTRAINT [FK_CMS_Widget_WidgetCategoryID_CMS_WidgetCategory]
			FOREIGN KEY ([WidgetCategoryID]) REFERENCES [CMS_WidgetCategory] ([WidgetCategoryID])
END
IF OBJECT_ID(N'[CMS_Widget]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WebPart]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Widget_WidgetWebPartID_CMS_WebPart]') IS NULL
BEGIN
		ALTER TABLE [CMS_Widget]
			ADD CONSTRAINT [FK_CMS_Widget_WidgetWebPartID_CMS_WebPart]
			FOREIGN KEY ([WidgetWebPartID]) REFERENCES [CMS_WebPart] ([WebPartID])
END
CREATE CLUSTERED INDEX [IX_CMS_Widget_WidgetCategoryID_WidgetDisplayName]
	ON [CMS_Widget] ([WidgetCategoryID], [WidgetDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Widget_WidgetIsEnabled_WidgetForGroup_WidgetForEditor_WidgetForUser]
	ON [CMS_Widget] ([WidgetIsEnabled], [WidgetForGroup], [WidgetForEditor], [WidgetForUser])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Widget_WidgetWebPartID]
	ON [CMS_Widget] ([WidgetWebPartID])
	ON [PRIMARY]
