<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="Category_Edit_General.aspx.cs" Inherits="CMSSiteManager_Development_Categories_Category_Edit_General"
    Title="Category - Edit general" Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/Categories/CategoryEdit.ascx" TagName="CategoryEdit"
    TagPrefix="cms" %>
<asp:Content ID="Content2" ContentPlaceHolderID="plcContent" runat="Server">
    <cms:CategoryEdit ID="catEdit" runat="server" Visible="true" />
</asp:Content>
