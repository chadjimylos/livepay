﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCReceipt.ascx.vb" Inherits="CMSTemplates_LivePay_PopUp_UCReceipt" %>

<style type="text/css" >
@media print
{
    .MasterMain { display:none  }
}
</style>

<script>
    function ExportDetails(url) {
        document.getElementById('<%=exportpopFrame.ClientID %>').src = url;
        return false
    }
</script>
<div style="display:none">
<iframe id="exportpopFrame" runat="server" ></iframe>
</div>
<table cellpadding="0" cellspacing="0" border="0" class="PopUpMainTable">
    <tr>
	<td class="PopTopLeft">&nbsp;</td>
        <td class="PopTopCenter">&nbsp;</td>
        <td class="PopTopClose"><img src="/App_Themes/LivePay/PopUp/Close.png"  alt="Close" title="Close" style="cursor:pointer;" onclick="$.unblockUI()"/></td>
	<td class="PopTopRight">&nbsp;</td>  
    </tr>
    <tr>
	<td class="PopCenterLeft">&nbsp;</td>
	<td colspan="2" class="PopCenter">
        <table cellpadding="0" cellspacing="0">
            <tr><td class="PopTopTitleSmall" id="TdCompanyTitle" runat="server">sss</td>
            <td class="PopTopLogo">
            <asp:Image runat="server" ID="imgLogo" />
            </tr>
            <tr><td class="PopTopTitleUnder"  colspan="2">&nbsp;</td></tr>
            <tr><td class="PopContent" colspan="2">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Ημερομηνία & Ώρα|en-us=Ημερομηνία & Ώρα $}") %></td>
                        <td class="PopTransDetValue" id="tdDtm" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Πληρωμή προς |en-us=Πληρωμή προς $}") %></td>
                        <td class="PopTransDetValue" id="tdCompPay" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Κωδικός Πληρωμής |en-us=Κωδικός Πληρωμής $}") %></td>
                        <td class="PopTransDetValue" id="tdPayCode" runat="server"></td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Κωδικός Πελάτη |en-us=Κωδικός Πελάτη $}") %></td>
                        <td class="PopTransDetValue" id="tdCustCode" runat="server"></td>
                    </tr>
                    <tr>
                        <td colspan="2" width="100%">
                            <table cellpadding="0" cellspacing="0" id="tblCustomFields" runat="server">
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Ποσό |en-us=Ποσό $}") %></td>
                        <td class="PopTransDetValue" id="tdPrice" runat="server"></td>
                    </tr>

                    <tr>
                        <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Κάρτα |en-us=Κάρτα $}") %></td>
                        <td class="PopTransDetValue" id="tdCardNo" runat="server"></td>
                    </tr>
                    <tr id="ErrosCell" runat="server" visible="false">
                        <td class="PopTransRecTitle"><%= ResHelper.LocalizeString("{$=Αιτιολογία |en-us=Reason $}")%></td>
                        <td class="PopTransDetValue" id="tdErrorMessage" runat="server"></td>
                    </tr>


                 
                    <tr>
                        <td colspan="2" class="PopTransDetButtons">
                        <asp:LinkButton runat="server" ID="btnSaveToPDF" runat="server" ><img class="Chand" border="0" src="/app_themes/LivePay/popup/btnpdf.png"  /></asp:LinkButton>
                        &nbsp;&nbsp;&nbsp;
                        <img border="0" onclick=document.getElementById('<%=btnSaveToPDF.clientID %>').click() class="Chand" src="/app_themes/LivePay/popup/btnPrint.png" />
                        </td>
                    </tr>
                   
                </table>
            </td></tr>
        </table>
    </td>
	<td class="PopCenterRight">&nbsp;</td>
    </tr>
    <tr>
	<td class="PopBotLeft">&nbsp;</td>
	<td colspan="2" class="PopBotCenter"></td>
	<td class="PopBotRight">&nbsp;</td>
    </tr>
</table>