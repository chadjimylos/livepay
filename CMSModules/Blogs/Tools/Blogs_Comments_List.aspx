<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="Blogs_Comments_List.aspx.cs" Inherits="CMSModules_Blogs_Tools_Blogs_Comments_List"
    Title="Blogs - Comments list" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<%@ Register Src="~/CMSModules/Blogs/Controls/CommentFilter.ascx" TagName="CommentFilter"
    TagPrefix="cms" %>
<asp:Content ID="Content2" ContentPlaceHolderID="plcContent" runat="Server">
    <cms:CommentFilter runat="server" ID="filterElem" DisplayAllRecord="true" />
    <cms:UniGrid ID="gridComments" runat="server" GridName="~/CMSModules/Blogs/Tools/Comments_List.xml"
        OrderBy="CommentDate DESC" DelayedReload="true" IsLiveSite="false" />
    <br />
    <asp:Panel ID="pnlActions" runat="server">
        <cms:LocalizedLabel ID="lblAction" runat="server" EnableViewState="false" DisplayColon="true"
            ResourceString="blog.comments.action" />
        <asp:DropDownList ID="drpAction" runat="server" CssClass="DropDownFieldSmall" />
        <cms:CMSButton ID="btnAction" runat="server" CssClass="ContentButton" OnClick="btnAction_Click"
            EnableViewState="false" />
    </asp:Panel>

    <script type="text/javascript"> 
    <!--
        // Refreshes current page when comment properties are changed
        function RefreshPage(filterParams) 
        {                 
            url = window.location.href;          
            
            index = window.location.href.indexOf('?');
            if(index > 0)
            {            
                url = window.location.href.substring(0, index);
            }
                                                     
            window.location.replace(url + filterParams); 
        } 
        
        // Confirm mass delete
        function MassConfirm(dropdown, msg)
        {
            var drop = document.getElementById(dropdown);
            if (drop != null)
            {
                if (drop.value == "delete")
                {
                    return confirm(msg);
                }
                return true;
            }
            return true;
        }       
    -->
    </script>

</asp:Content>
