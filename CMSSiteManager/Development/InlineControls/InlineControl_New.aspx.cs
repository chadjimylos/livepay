using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.ExtendedControls.Dialogs;

public partial class CMSSiteManager_Development_InlineControls_InlineControl_New : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {        

        // control initializations				
        lblControlDisplayName.Text = ResHelper.GetString("InlineControl_Edit.ControlDisplayNameLabel");
        lblControlFileName.Text = ResHelper.GetString("InlineControl_Edit.ControlFileNameLabel");
        lblControlParameterName.Text = ResHelper.GetString("InlineControl_Edit.ControlParameterNameLabel");
        lblControlName.Text = ResHelper.GetString("InlineControl_Edit.ControlNameLabel");

        rfvDisplayName.ErrorMessage = ResHelper.GetString("InlineControl_Edit.ErrorDisplayName");
        rfvName.ErrorMessage = ResHelper.GetString("InlineControl_Edit.ErrorName");
        
        // check 'Assign with current web site' check box
        if ((CMSContext.CurrentSite != null) && (!RequestHelper.IsPostBack()))
        {
            chkAssign.Text = ResHelper.GetString("General.AssignWithWebSite") + " " + CMSContext.CurrentSite.DisplayName;
            chkAssign.Checked = true;
            chkAssign.Visible = true;
        }

        btnOk.Text = ResHelper.GetString("General.OK");

        // initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("InlineControl_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/InlineControls/InlineControl_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = ResHelper.GetString("InlineControl_Edit.NewItemCaption");
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("InlineControl_Edit.NewControl");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_InlineControl/new.png");
        this.CurrentMaster.Title.HelpTopicName = "new_control";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        FileSystemDialogConfiguration config = new FileSystemDialogConfiguration();
        config.DefaultPath = "CMSInlineControls";
        config.AllowedExtensions = "ascx";
        config.ShowFolders = false;
        FileSystemSelector.DialogConfig = config;
        FileSystemSelector.AllowEmptyValue = false;
        FileSystemSelector.SelectedPathPrefix = "~/CMSInlineControls/";
        FileSystemSelector.ValidationError = ResHelper.GetString("InlineControl_Edit.ErrorFileName");
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        string errorMessage = new Validator().NotEmpty(txtControlDisplayName.Text.Trim(), rfvDisplayName.ErrorMessage).NotEmpty(txtControlName.Text.Trim(), rfvName.ErrorMessage).NotEmpty(txtControlName.Text, rfvName.ErrorMessage).NotEmpty(FileSystemSelector.Value.ToString().Trim(), FileSystemSelector.ValidationError).Result;

        if (errorMessage == "")
        {
            InlineControlInfo inlineControlObj = new InlineControlInfo();

            inlineControlObj.ControlDescription = txtControlDescription.Text.Trim();
            inlineControlObj.ControlDisplayName = txtControlDisplayName.Text.Trim();
            inlineControlObj.ControlFileName = FileSystemSelector.Value.ToString().Trim();
            inlineControlObj.ControlParameterName = txtControlParameterName.Text.Trim();
            inlineControlObj.ControlName = txtControlName.Text.Trim();

            try
            {
                // Create new inline control
                InlineControlInfoProvider.SetInlineControlInfo(inlineControlObj);
                if ((chkAssign.Visible) && (chkAssign.Checked) && (CMSContext.CurrentSite != null))
                {
                    // Add new control to the actual site
                    InlineControlSiteInfoProvider.AddInlineControlToSite(inlineControlObj.ControlID, CMSContext.CurrentSite.SiteID);
                }
                UrlHelper.Redirect("InlineControl_Frameset.aspx?inlinecontrolid=" + Convert.ToString(inlineControlObj.ControlID) + "&saved=1");
            }
            catch(Exception ex)
            {
                lblError.Text = ex.Message.Replace("%%name%%", txtControlName.Text);
                lblError.Visible = true;
            }

            
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
