using System;
using System.Data;
using System.Collections;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.CMSImportExport;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.PortalEngine;
using CMS.Staging;
using CMS.UIControls;

public partial class CMSModules_ImportExport_Controls_ExportGridView : CMSUserControl
{
    #region "Variables"

    protected string codeNameColumnName = "";
    protected string displayNameColumnName = "";

    protected string preposition = "echck_";
    protected string taskPreposition = "etchck_";

    private SiteExportSettings mSettings = null;
    private ArrayList mSelectedValues = null;
    private ArrayList mSelectedTasks = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Export settings
    /// </summary>
    public SiteExportSettings Settings
    {
        get
        {
        	 return mSettings; 
        }
        set
        {
        	 mSettings = value; 
        }
    }


    /// <summary>
    /// Selected values
    /// </summary>
    public ArrayList SelectedValues
    {
        get
        {
        	 return mSelectedValues; 
        }
    }


    /// <summary>
    /// Selected tasks
    /// </summary>
    public ArrayList SelectedTasks
    {
        get
        {
        	 return mSelectedTasks; 
        }
    }


    /// <summary>
    /// Current object type
    /// </summary>
    public string ObjectType
    {
        get
        {
        	 return ValidationHelper.GetString(ViewState["ObjectType"], CMSObjectHelper.GROUP_OBJECTS); 
        }
        set
        {
        	 ViewState["ObjectType"] = value; 
        }
    }


    /// <summary>
    /// True if site object
    /// </summary>
    public bool SiteObject
    {
        get
        {
        	 return ValidationHelper.GetBoolean(ViewState["SiteObject"], false); 
        }
        set
        {
        	 ViewState["SiteObject"] = value; 
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (RequestHelper.IsPostBack())
        {
            // Initialize selected values
            foreach (string key in Request.Form.AllKeys)
            {
                if (key == null)
                {
                    continue;
                }

                if (key.StartsWith(preposition))
                {
                    if (mSelectedValues == null)
                    {
                        mSelectedValues = new ArrayList();
                    }

                    mSelectedValues.Add(key.Remove(0, preposition.Length).ToLower());
                }

                if (key.StartsWith(taskPreposition))
                {
                    if (mSelectedTasks == null)
                    {
                        mSelectedTasks = new ArrayList();
                    }

                    int taskId = ValidationHelper.GetInteger(key.Remove(0, taskPreposition.Length), 0);
                    if (taskId > 0)
                    {
                        mSelectedTasks.Add(taskId);
                    }
                }
            }
        }
    }


    /// <summary>
    /// Bind the data
    /// </summary>
    public void Bind()
    {
        if (!string.IsNullOrEmpty(ObjectType))
        {
            pnlGrid.Visible = true;
            // Initilaize strings
            btnAll.Text = ResHelper.GetString("ImportExport.All");
            btnNone.Text = ResHelper.GetString("export.none");
            btnAllTasks.Text = ResHelper.GetString("ImportExport.All");
            btnNoneTasks.Text = ResHelper.GetString("export.none");
            btnDefault.Text = ResHelper.GetString("General.Default");
            lblTasks.Text = ResHelper.GetString("Export.Tasks");

            // Add selection links functionality
            btnAll.OnClientClick = "egvChangeCheckboxes('" + ValidationHelper.GetIdentificator(ObjectType) + "', 1, " + (SiteObject ? "1" : "0") + ");return false;";
            btnNone.OnClientClick = "egvChangeCheckboxes('" + ValidationHelper.GetIdentificator(ObjectType) + "', 0, " + (SiteObject ? "1" : "0") + ");return false;";
            btnDefault.OnClientClick = "egvChangeCheckboxes('" + ValidationHelper.GetIdentificator(ObjectType) + "', -1, " + (SiteObject ? "1" : "0") + ");return false;";
            btnAllTasks.OnClientClick = "egvChangeCheckboxes('export_task', 1, " + (SiteObject ? "1" : "0") + ");return false;";
            btnNoneTasks.OnClientClick = "egvChangeCheckboxes('export_task', 0, " + (SiteObject ? "1" : "0") + ");return false;";

            // Get object info
            IInfoObject info = CMSObjectHelper.GetObject(ObjectType);
            if (info != null)
            {
                plcGrid.Visible = true;
                codeNameColumnName = info.CodeNameColumn;
                displayNameColumnName = info.DisplayNameColumn;

                // Get data source
                string where = GenerateWhereCondition();
                string orderBy = GetOrderByExpression(info);

                // Prepare the columns
                string columns = null;
                if (info.CodeNameColumn != TypeInfo.COLUMN_NAME_UNKNOWN)
                {
                    columns += info.CodeNameColumn;
                }
                if ((info.DisplayNameColumn.ToLower() != info.CodeNameColumn.ToLower()) && (info.DisplayNameColumn != TypeInfo.COLUMN_NAME_UNKNOWN))
                {
                    if (columns != null)
                    {
                        columns += ", ";
                    }
                    columns += info.DisplayNameColumn;
                }

                DataSet ds = info.GetModifiedFrom(DateTimeHelper.ZERO_TIME, columns, where, orderBy, -1, false, null);

                // Prepare checkbox field
                TemplateField checkBoxField = (TemplateField)gvObjects.Columns[0];
                checkBoxField.HeaderText = ResHelper.GetString("General.Export");

                // Prepare display name field
                TemplateField nameField = (TemplateField)gvObjects.Columns[1];
                nameField.HeaderText = ResHelper.GetString("general.displayname");

                // Load data
                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    plcObjects.Visible = true;
                    lblNoData.Visible = false;
                    gvObjects.DataSource = ds;
                    gvObjects.DataBind();
                }
                else
                {
                    plcObjects.Visible = false;
                    lblNoData.Visible = true;
                    lblNoData.Text = String.Format(ResHelper.GetString("ExportGridView.NoData"), ResHelper.GetString("objecttype." + ObjectType.Replace(".", "_").Replace("#", "_")));
                }


                // Task fields
                TemplateField taskCheckBoxField = (TemplateField)gvTasks.Columns[0];
                taskCheckBoxField.HeaderText = ResHelper.GetString("General.Export");

                TemplateField titleField = (TemplateField)gvTasks.Columns[1];
                titleField.HeaderText = ResHelper.GetString("Export.TaskTitle");

                BoundField typeField = (BoundField)gvTasks.Columns[2];
                typeField.HeaderText = ResHelper.GetString("general.type");

                BoundField timeField = (BoundField)gvTasks.Columns[3];
                timeField.HeaderText = ResHelper.GetString("Export.TaskTime");

                // Load tasks
                int siteId = (SiteObject ? Settings.SiteId : 0);

                ds = ExportTaskInfoProvider.SelectTaskList(siteId, ObjectType, null, "TaskTime DESC");
                if (!DataHelper.DataSourceIsEmpty(ds) && (ValidationHelper.GetBoolean(Settings.GetSettings(ImportExportHelper.SETTINGS_TASKS), true)))
                {
                    plcTasks.Visible = true;
                    gvTasks.DataSource = ds;
                    gvTasks.DataBind();
                }
                else
                {
                    plcTasks.Visible = false;
                }
            }
            else
            {
                plcGrid.Visible = false;
            }
        }
        else
        {
            pnlGrid.Visible = false;
            gvObjects.DataSource = null;
            gvObjects.DataBind();
        }
    }


    // Genearate where condition
    private string GenerateWhereCondition()
    {
        return Settings.GetObjectWhereCondition(ObjectType, SiteObject);
    }


    // Get orderby expression
    private static string GetOrderByExpression(IInfoObject info)
    {
        switch(info.ObjectType)
        {
            case PortalObjectType.PAGETEMPLATE:
                return "PageTemplateIsReusable DESC," + info.DisplayNameColumn;
            default:
                {
                    if (info.DisplayNameColumn != TypeInfo.COLUMN_NAME_UNKNOWN)
                    {
                        return info.DisplayNameColumn;
                    }
                    else
                    {
                        return info.CodeNameColumn;
                    }
                }
        }
    }


    /// <summary>
    /// Ensure objects preselection
    /// </summary>
    /// <param name="codeName">Code name</param>
    protected string IsChecked(object codeName)
    {
        string name = ValidationHelper.GetString(codeName, "");
        if (Settings.IsSelected(ObjectType, name, SiteObject))
        {
            return " checked=\"checked\" ";
        }
        return "";
    }


    /// <summary>
    /// Ensure tasks preselection
    /// </summary>
    /// <param name="taskId">Task ID</param>
    protected string IsTaskChecked(object taskId)
    {
        int id = ValidationHelper.GetInteger(taskId, 0);
        if (Settings.IsTaskSelected(ObjectType, id, SiteObject))
        {
            return " checked=\"checked\" ";
        }
        return "";
    }


    protected string GetName(object codeNameObj, object displayNameObj)
    {
        string codeName = ValidationHelper.GetString(codeNameObj, "");
        string displayName = ValidationHelper.GetString(displayNameObj, "");

        if (string.IsNullOrEmpty(displayName))
        {
            return codeName;
        }
        return ResHelper.GetString(displayName);
    }
}
