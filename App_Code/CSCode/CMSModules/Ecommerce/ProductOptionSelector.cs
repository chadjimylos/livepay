using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.ExtendedControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.UIControls;
using CMS.SettingsProvider;

namespace CMS.Ecommerce
{
    /// <summary>
    /// Base class of the option category sku selector control.
    /// </summary>
    public class ProductOptionSelector : CMSUserControl
    {
        #region "Variables"

        private int mOptionCategoryId = 0;
        private bool mShowOptionCategoryName = true;
        private bool mShowOptionCategoryDescription = true;
        private Control mSelectionControl = null;
        private OptionCategoryInfo mOptionCategory = null;
        private DataRow mOptionCategoryDataRow = null;        
        private bool mUseDefaultCurrency = false;
        private ShoppingCartInfo mLocalShoppingCartObj = null;
        private bool mShowPriceIncludingTax = false;
        private Hashtable mProductOptionPrice = new Hashtable();
        private Hashtable mProductOptionDepartment = new Hashtable();

        #endregion


        #region "Public properties"

        /// <summary>
        /// Option category ID.
        /// </summary>
        public int OptionCategoryId
        {
            get
            {
                if ((mOptionCategoryId == 0) && (this.OptionCategory != null))
                {
                    mOptionCategoryId = this.OptionCategory.CategoryID;
                }
                return mOptionCategoryId;
            }
            set
            {
                // Force loading new selection control
                //mSelectionControl = null;

                // Force creating new option category object
                mOptionCategory = null;

                mOptionCategoryId = value;
            }
        }


        /// <summary>
        /// Option category data.
        /// </summary>
        public DataRow OptionCategoryDataRow
        {
            get
            {
                return mOptionCategoryDataRow;
            }
            set
            {
                // Force loading new selection control
                //mSelectionControl = null;

                // Force creating new option category object
                mOptionCategory = null;

                mOptionCategoryDataRow = value;
            }
        }


        /// <summary>
        /// Indicates whether option category name should be displayed to the user.
        /// </summary>
        public bool ShowOptionCategoryName
        {
            get
            {
                return mShowOptionCategoryName;
            }
            set
            {
                mShowOptionCategoryName = value;
            }
        }


        /// <summary>
        /// Indicates whether option category description should be displayed to the user.
        /// </summary>
        public bool ShowOptionCategoryDescription
        {
            get
            {
                return mShowOptionCategoryDescription;
            }
            set
            {
                mShowOptionCategoryDescription = value;
            }
        }


        /// <summary>
        /// Selection control according to the current option category selection type.
        /// </summary>
        public Control SelectionControl
        {
            get
            {
                if (mSelectionControl == null)
                {
                    mSelectionControl = GetSelectionControl();
                }
                return mSelectionControl;
            }
        }


        /// <summary>
        /// Option category object. It is loaded from option category datarow by default, if it is not specified it is loaded by option category id.
        /// </summary>
        public OptionCategoryInfo OptionCategory
        {
            get
            {
                if (mOptionCategory == null)
                {
                    // Load option category data from datarow
                    if (this.OptionCategoryDataRow != null)
                    {
                        mOptionCategory = new OptionCategoryInfo(this.OptionCategoryDataRow);
                    }
                    // Load option category data by option category ID
                    else
                    {
                        mOptionCategory = OptionCategoryInfoProvider.GetOptionCategoryInfo(mOptionCategoryId);
                    }
                }
                return mOptionCategory;
            }
        }


        /// <summary>
        /// Shopping cart object required for price formatting. Use this property when in CMSDesk.
        /// </summary>
        public ShoppingCartInfo LocalShoppingCartObj
        {
            get
            {
                return mLocalShoppingCartObj;
            }
            set
            {
                mLocalShoppingCartObj = value;
            }
        }


        /// <summary>
        /// TRUE - default currency is used for price formatting and no discounts and taxes are applied to price, by default it is set to FALSE.
        /// </summary>
        public bool UseDefaultCurrency
        {
            get
            {
                return mUseDefaultCurrency;
            }
            set
            {
                mUseDefaultCurrency = value;
            }
        }


        /// <summary>
        /// TRUE - product option price is displayed including tax, FALSE - product option price is displayed without tax
        /// </summary>
        public bool ShowPriceIncludingTax
        {
            get
            {
                return mShowPriceIncludingTax;
            }
            set
            {
                mShowPriceIncludingTax = value;
            }
        }


        public Hashtable ProductOptionPrice
        {
            get
            {
                if (mProductOptionPrice.Count == 0)
                {
                    LoadHashTables();
                }
                return mProductOptionPrice;
            }
        }


        public Hashtable ProductOptionDepartment
        {
            get
            {
               if (mProductOptionDepartment.Count == 0)
               { 
                   LoadHashTables();
               }
               return mProductOptionDepartment;
            }
        }

        #endregion


        #region "Public methods"

        /// <summary>
        /// Reloads selection control according to the option category selection type.
        /// </summary>
        public void LoadCategorySelectionControl()
        {
            mSelectionControl = GetSelectionControl();
        }


        /// <summary>
        /// Reloads selection control data according to the option category data.
        /// </summary>
        public void ReloadData()
        {
            DebugHelper.SetContext("ProductOptionSelector");

            // Load actual product options
            this.LoadSKUOptions();

            // Mark default product options
            this.SetDefaultSKUOptions();

            DebugHelper.ReleaseContext();
        }


        /// <summary>
        /// Get selected product options from the selection control. 
        /// </summary>
        public string GetSelectedSKUOptions()
        {
            if (this.SelectionControl != null)
            {
                // Dropdown list, Radiobutton list - single selection
                if ((this.SelectionControl.GetType() == typeof(LocalizedDropDownList)) ||
                    (this.SelectionControl.GetType() == typeof(LocalizedRadioButtonList)))
                {
                    return ((ListControl)this.SelectionControl).SelectedValue;
                }
                // Checkbox list - multiple selection
                else if (this.SelectionControl.GetType() == typeof(LocalizedCheckBoxList))
                {
                    string result = "";
                    foreach (ListItem item in ((CheckBoxList)this.SelectionControl).Items)
                    {
                        if (item.Selected)
                        {
                            result += item.Value + ",";
                        }
                    }
                    return result.TrimEnd(',');
                }
            }
            return null;
        }


        /// <summary>
        /// Returns TRUE when selection control is empty or only '(none)' record is included, otherwise returns FALSE.
        /// </summary>
        public bool IsSelectionControlEmpty()
        {
            if (this.SelectionControl != null)
            {
                bool noItems = (((ListControl)this.SelectionControl).Items.Count == 0);
                bool onlyNoneRecord = ((((ListControl)this.SelectionControl).Items.Count == 1) && (((ListControl)this.SelectionControl).Items.FindByValue("0") != null));

                return (noItems || onlyNoneRecord);
            }
            return true;
        }

        #endregion


        #region "Private methods"

        /// <summary>
        /// Returns initialized selection control according to the specified selection type.
        /// </summary>
        private Control GetSelectionControl()
        {
            if (this.OptionCategory != null)
            {
                switch (this.OptionCategory.CategorySelectionType)
                {
                    // Dropdownlist                     
                    case OptionCategorySelectionTypeEnum.Dropdownlist:

                        LocalizedDropDownList dropDown = new LocalizedDropDownList();
                        dropDown.ID = "dropdown";
                        dropDown.DataTextField = "SKUName";
                        dropDown.DataValueField = "SKUID";
                        dropDown.DataBound += new EventHandler(SelectionControl_DataBound);
                        return dropDown;

                    // Checkbox list with horizontal direction
                    case OptionCategorySelectionTypeEnum.CheckBoxesHorizontal:

                        LocalizedCheckBoxList boxListHorizontal = new LocalizedCheckBoxList();
                        boxListHorizontal.ID = "chkHorizontal";
                        boxListHorizontal.RepeatDirection = RepeatDirection.Horizontal;
                        boxListHorizontal.DataTextField = "SKUName";
                        boxListHorizontal.DataValueField = "SKUID";
                        boxListHorizontal.DataBound += new EventHandler(SelectionControl_DataBound);
                        return boxListHorizontal;

                    // Checkbox list with vertical direction
                    case OptionCategorySelectionTypeEnum.CheckBoxesVertical:

                        LocalizedCheckBoxList boxListVertical = new LocalizedCheckBoxList();
                        boxListVertical.ID = "chkVertical";
                        boxListVertical.RepeatDirection = RepeatDirection.Vertical;
                        boxListVertical.DataTextField = "SKUName";
                        boxListVertical.DataValueField = "SKUID";
                        boxListVertical.DataBound += new EventHandler(SelectionControl_DataBound);
                        return boxListVertical;

                    // Radiobuton list with horizontal direction
                    case OptionCategorySelectionTypeEnum.RadioButtonsHorizontal:

                        LocalizedRadioButtonList buttonListHorizontal = new LocalizedRadioButtonList();
                        buttonListHorizontal.ID = "radHorizontal";
                        buttonListHorizontal.RepeatDirection = RepeatDirection.Horizontal;
                        buttonListHorizontal.DataTextField = "SKUName";
                        buttonListHorizontal.DataValueField = "SKUID";
                        buttonListHorizontal.DataBound += new EventHandler(SelectionControl_DataBound);
                        return buttonListHorizontal;

                    // Radiobuton list with vertical direction
                    case OptionCategorySelectionTypeEnum.RadioButtonsVertical:

                        LocalizedRadioButtonList buttonListVertical = new LocalizedRadioButtonList();
                        buttonListVertical.ID = "radVertical";
                        buttonListVertical.RepeatDirection = RepeatDirection.Vertical;
                        buttonListVertical.DataTextField = "SKUName";
                        buttonListVertical.DataValueField = "SKUID";
                        buttonListVertical.DataBound += new EventHandler(SelectionControl_DataBound);
                        return buttonListVertical;
                }
            }

            return null;
        }


        private void SelectionControl_DataBound(object sender, EventArgs e)
        {            
            if (this.OptionCategory.CategoryDisplayPrice)
            {
                if (!DataHelper.DataSourceIsEmpty(((ListControl)sender).DataSource))
                {
                    foreach (DataRow row in ((DataSet)((ListControl)sender).DataSource).Tables[0].Rows)
                    {
                        int skuId = ValidationHelper.GetInteger(row["SKUId"], 0);
                        ListItem item = ((ListControl)sender).Items.FindByValue(skuId.ToString());

                        if (item != null)
                        {
                            // Format product option price
                            double skuPrice = ValidationHelper.GetDouble(row["SKUPrice"], 0);
                            string preffix = (skuPrice >= 0) ? "+ " : "- ";
                            string formattedPrice = "";

                            // Get positive price
                            skuPrice = Math.Abs(skuPrice);

                            // Use default currency for price formatting
                            if (this.UseDefaultCurrency)
                            {
                                // Get default currency
                                CurrencyInfo defaultCurrency = CurrencyInfoProvider.GetMainCurrency();

                                // Round price
                                skuPrice = CurrencyInfoProvider.RoundTo(skuPrice, defaultCurrency);

                                // Format price
                                formattedPrice = CurrencyInfoProvider.GetFormatedPrice(skuPrice, defaultCurrency);
                            }
                            // Use shopping cart data for price formatting
                            else
                            {
                                int skuDepartmentId = ValidationHelper.GetInteger(row["SKUDepartmentID"], 0);
                                if (this.ShowPriceIncludingTax)
                                {
                                    // Show price including tax
                                    formattedPrice = EcommerceFunctions.GetFormatedPrice(skuPrice, skuDepartmentId, this.LocalShoppingCartObj, skuId);
                                }
                                else
                                {
                                    // Show price without tax
                                    formattedPrice = EcommerceFunctions.GetFormatedPrice(skuPrice, skuDepartmentId, this.LocalShoppingCartObj);
                                }
                            }

                            item.Text += " (" + preffix + formattedPrice + ")";
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Sets category default options as 'Selected' in selection control.
        /// </summary>        
        private void SetDefaultSKUOptions()
        {
            if ((this.SelectionControl != null) && (this.OptionCategory != null))
            {
                // Dropdown list - single selection
                // Radiobutton list - single selection
                if ((this.SelectionControl.GetType() == typeof(LocalizedDropDownList)) ||
                    (this.SelectionControl.GetType() == typeof(LocalizedRadioButtonList)))
                {
                    try
                    {
                        ((ListControl)this.SelectionControl).SelectedValue = this.OptionCategory.CategoryDefaultOptions;
                    }
                    catch
                    {
                    }
                }
                // Checkbox list - multiple selection
                else if (this.SelectionControl.GetType() == typeof(LocalizedCheckBoxList))
                {
                    try
                    {
                        if (this.OptionCategory.CategoryDefaultOptions != "")
                        {
                            foreach (string skuId in this.OptionCategory.CategoryDefaultOptions.Split(','))
                            {
                                // Ensure value is not empty
                                string value = (skuId != "") ? skuId : "0";

                                ListItem item = ((CheckBoxList)this.SelectionControl).Items.FindByValue(value);
                                if (item != null)
                                {
                                    item.Selected = true;
                                }
                            }
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }


        /// <summary>
        /// Loads data (SKU options) to the selection control.
        /// </summary>
        private void LoadSKUOptions()
        {
            if (this.SelectionControl != null)
            {
                // Bind data
                DataSet ds = SKUInfoProvider.GetOptionCategorySKUs(this.OptionCategoryId, "SKUEnabled = 1");
                ((ListControl)this.SelectionControl).DataSource = ds;
                this.SelectionControl.DataBind();

                // Add '(none)' record when it is allowed
                if ((this.OptionCategory != null) && (this.OptionCategory.CategoryDefaultRecord != ""))
                {
                    ListItem noneRecord = new ListItem(this.OptionCategory.CategoryDefaultRecord, "0");
                    ((ListControl)this.SelectionControl).Items.Insert(0, noneRecord);
                }
            }
        }


        /// <summary>
        /// Fill hash tables with data
        /// </summary>
        private void LoadHashTables()
        {
            DataSet ds = SKUInfoProvider.GetOptionCategorySKUs(this.OptionCategoryId, "SKUEnabled = 1");
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    this.mProductOptionPrice.Add(row["SKUID"], row["SKUPrice"]);
                    this.mProductOptionDepartment.Add(row["SKUID"], row["SKUDepartmentID"]);
                }
            }
        }

        #endregion
    }
}
