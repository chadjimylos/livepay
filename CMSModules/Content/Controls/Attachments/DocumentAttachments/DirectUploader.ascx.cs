using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI;

using CMS.CMSHelper;
using CMS.FileManager;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.FormControls;
using CMS.ExtendedControls;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Content_Controls_Attachments_DocumentAttachments_DirectUploader : AttachmentsControl, IUploaderControl
{
    #region "Variables"

    private string mInnerDivClass = "NewAttachment";
    private Guid attachmentGuid = Guid.Empty;
    private AttachmentInfo innerAttachment = null;
    private bool createTempAttachment = false;

    #endregion


    #region "Properties"

    /// <summary>
    /// CSS class of the new attachment link
    /// </summary>
    public string InnerDivClass
    {
        get
        {
            return mInnerDivClass;
        }
        set
        {
            mInnerDivClass = value;
        }
    }


    /// <summary>
    /// Last performed action
    /// </summary>
    public string LastAction
    {
        get
        {
            return ValidationHelper.GetString(ViewState["LastAction"], null);
        }
        set
        {
            ViewState["LastAction"] = value;
        }
    }


    /// <summary>
    /// Info label
    /// </summary>
    public Label InfoLabel
    {
        get
        {
            return lblInfo;
        }
    }


    /// <summary>
    /// Inner attachment GUID (GUID of temporary attachment created for new culture version)
    /// </summary>
    public override Guid InnerAttachmentGUID
    {
        get
        {
            return ValidationHelper.GetGuid(ViewState["InnerAttachmentGUID"], base.InnerAttachmentGUID);
        }
        set
        {
            ViewState["InnerAttachmentGUID"] = value;
            base.InnerAttachmentGUID = value;
        }
    }


    /// <summary>
    /// Name of document attachment column
    /// </summary>
    public override string GUIDColumnName
    {
        get
        {
            return base.GUIDColumnName;
        }
        set
        {
            base.GUIDColumnName = value;
            if ((dsAttachments != null) && (newAttachmentElem != null))
            {
                newAttachmentElem.AttachmentGUIDColumnName = value;
            }
        }
    }


    /// <summary>
    /// Width of the attachment
    /// </summary>
    public override int ResizeToWidth
    {
        get
        {
            return base.ResizeToWidth;
        }
        set
        {
            base.ResizeToWidth = value;
            if (newAttachmentElem != null)
            {
                newAttachmentElem.ResizeToWidth = value;
            }
        }
    }


    /// <summary>
    /// Height of the attachment
    /// </summary>
    public override int ResizeToHeight
    {
        get
        {
            return base.ResizeToHeight;
        }
        set
        {
            base.ResizeToHeight = value;
            if (newAttachmentElem != null)
            {
                newAttachmentElem.ResizeToHeight = value;
            }
        }
    }


    /// <summary>
    /// Maximal side size of the attachment
    /// </summary>
    public override int ResizeToMaxSideSize
    {
        get
        {
            return base.ResizeToMaxSideSize;
        }
        set
        {
            base.ResizeToMaxSideSize = value;
            if (newAttachmentElem != null)
            {
                newAttachmentElem.ResizeToMaxSideSize = value;
            }
        }
    }


    /// <summary>
    /// List of allowed extensions
    /// </summary>
    public override string AllowedExtensions
    {
        get
        {
            return base.AllowedExtensions;
        }
        set
        {
            base.AllowedExtensions = value;
            if (newAttachmentElem != null)
            {
                newAttachmentElem.AllowedExtensions = value;
            }
        }
    }


    /// <summary>
    /// Specifies the document for which the attachments should be displayed
    /// </summary>
    public override int DocumentID
    {
        get
        {
            return base.DocumentID;
        }
        set
        {
            base.DocumentID = value;
            if (newAttachmentElem != null)
            {
                newAttachmentElem.DocumentID = value;
            }
        }
    }


    /// <summary>
    /// Specifies the version of the document (optional)
    /// </summary>
    public override int VersionHistoryID
    {
        get
        {
            int versionHistoryId = base.VersionHistoryID;
            if ((versionHistoryId == 0) && (Node != null))
            {
                versionHistoryId = Node.DocumentCheckedOutVersionHistoryID;
            }
            return versionHistoryId;
        }
        set
        {
            base.VersionHistoryID = value;
            if ((dsAttachments != null) && UsesWorkflow)
            {
                dsAttachments.DocumentVersionHistoryID = value;
            }
        }
    }


    /// <summary>
    /// Defines the form GUID; indicates that the temporary attachment will be handled
    /// </summary>
    public override Guid FormGUID
    {
        get
        {
            return base.FormGUID;
        }
        set
        {
            base.FormGUID = value;
            if ((dsAttachments != null) && (newAttachmentElem != null))
            {
                dsAttachments.AttachmentFormGUID = value;
                newAttachmentElem.FormGUID = value;
            }
        }
    }


    /// <summary>
    /// Value of the control
    /// </summary>
    public override object Value
    {
        get
        {
            return ViewState["Value"];
        }
        set
        {
            ViewState["Value"] = value;
        }
    }


    /// <summary>
    /// Name of the attachment
    /// </summary>
    public string AttachmentName
    {
        get
        {
            return hdnAttachName.Value;
        }
    }


    /// <summary>
    /// If true, control does not process the data
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return base.StopProcessing;
        }
        set
        {
            base.StopProcessing = value;
            if ((dsAttachments != null) && (newAttachmentElem != null))
            {
                dsAttachments.StopProcessing = value;
                newAttachmentElem.StopProcessing = value;
            }
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register script for tooltips
        if (ShowTooltip)
        {
            ScriptHelper.RegisterTooltip(Page);
        }

        // Ensure info message
        if ((Request["__EVENTTARGET"] == hdnPostback.ClientID) || Request["__EVENTTARGET"] == hdnFullPostback.ClientID)
        {
            string action = (string)Request["__EVENTARGUMENT"];

            if (action != null)
            {
                string[] values = action.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                action = values[0];
                if ((values.Length > 1) && ValidationHelper.IsGuid(values[1]))
                {
                    Value = values[1];
                }
            }

            LastAction = action;
        }


        #region "Scripts"

        // Refresh script
        string refreshScript = 
                    "function RefreshUpdatePanel_" + ClientID + @"(hiddenFieldID, action) {
                        var hiddenField = document.getElementById(hiddenFieldID);
                        if (hiddenField) {
                            __doPostBack(hiddenFieldID, action);
                        }
                    }

                    function FullRefresh(hiddenFieldID, action) {
                        if (PassiveRefresh != null)
                        {
                            PassiveRefresh();
                        }

                        var hiddenField = document.getElementById(hiddenFieldID);
                        if (hiddenField) {
                            __doPostBack(hiddenFieldID, action);
                        }
                    }

                    function FullPageRefresh_" + ClientID + @"(guid) {
                        if (PassiveRefresh != null)
                        {
                            PassiveRefresh();
                        }

                        var hiddenField = document.getElementById('" + hdnFullPostback.ClientID + "');" +
                        @"if (hiddenField) {
                            __doPostBack('" + hdnFullPostback.ClientID + "', 'refresh|' + guid);" +
                        @"}
                    }";

        // Initialize refresh script for update panel
        string initRefreshScript =
            "function InitRefresh_" + ClientID + "(msg, fullRefresh, guid, action)\n" +
            "{\n" +
            "   if ((msg != null) && (msg != \"\")){ alert(msg); action='error'; }\n" +
            "   if (fullRefresh) { FullRefresh('" + hdnFullPostback.ClientID + "', action + '|' + guid); }\n" +
            "   else { RefreshUpdatePanel_" + ClientID + "('" + hdnPostback.ClientID + "', action + '|' + guid); }\n" +
            "}\n";

        // Initialize deletion confirmation
        string deleteConfirmation = 
            "function DeleteConfirmation(){var conf = confirm('" +
                ResHelper.GetString("attach.deleteconfirmation") + "'); return conf;}\n";

        #endregion

        ScriptHelper.RegisterClientScriptBlock(this, GetType(), "AttachmentScripts_" + ClientID, ScriptHelper.GetScript(refreshScript + initRefreshScript + deleteConfirmation));

        // Register dialog script
        ScriptHelper.RegisterDialogScript(Page);

        // Register jQuery script for thumbnails updating
        ScriptHelper.RegisterJQuery(Page);

        // Grid initialization
        gridAttachments.OnExternalDataBound += GridDocsOnExternalDataBound;
        gridAttachments.OnAction += GridAttachmentsOnAction;
        gridAttachments.IsLiveSite = IsLiveSite;
        gridAttachments.Pager.PageSizeOptions = "10";
        pnlGrid.Attributes.Add("style", "padding-top: 2px;");

        // Ensure to raise the events
        if (!RequestHelper.IsAsyncPostback() && RequestHelper.IsPostBack())
        {
            switch (LastAction)
            {
                case "delete":
                    RaiseDeleteFile(this, e);
                    break;

                case "update":
                    RaiseUploadFile(this, e);
                    break;
            }

            InnerAttachmentGUID = ValidationHelper.GetGuid(Value, Guid.Empty);
        }

        // Load data
        ReloadData(false);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            lblError.Visible = (lblError.Text != "");
            lblInfo.Visible = (lblInfo.Text != "");

            // Ensure uplaoder button
            plcUploader.Visible = Enabled;
            plcUploaderDisabled.Visible = !Enabled;

            // Hide actions
            gridAttachments.GridView.Columns[0].Visible = !HideActions;
            gridAttachments.GridView.Columns[1].Visible = !HideActions;
            newAttachmentElem.Visible = !HideActions && Enabled;
            plcUploaderDisabled.Visible = !HideActions && !Enabled && (attachmentGuid == Guid.Empty);

            // Ensure correct layout
            bool gridHasData = !DataHelper.DataSourceIsEmpty(gridAttachments.DataSource);
            Visible = gridHasData || !HideActions;
            pnlGrid.Visible = gridHasData;

            // Initialize button for adding attachments
            plcUploader.Visible = (attachmentGuid == Guid.Empty) || !gridHasData;

            // Dialog for editing image
            string editImageScript =
                "function EditImage_" + ClientID + "(attachmentGUID, formGUID, versionHistoryID, parentId, hash) { " +
                    "var form = ''; \n" +
                    "if(formGUID != ''){ form = '&formguid=' + formGUID + '&parentid=' + parentId;}" +
                    ((Node != null) ? "else{form = '&siteid=' + " + Node.NodeSiteID + ";}" : "") +
                    "modalDialog('" + ResolveUrl((IsLiveSite ? "~/CMSFormControls/LiveSelectors/ImageEditor.aspx" : "~/CMSModules/Content/CMSDesk/Edit/ImageEditor.aspx") + "?attachmentGUID=' + attachmentGUID + '&versionHistoryID=' + versionHistoryID + form + '&clientid=" + ClientID + "&hash=' + hash") + ", 'imageEditorDialog', 905, 670); " +
                    "return false; }\n";

            // Register script for image editing
            ScriptHelper.RegisterClientScriptBlock(this, GetType(), "AttachmentEditScripts_" + ClientID, ScriptHelper.GetScript(editImageScript));
        }
    }

    #endregion


    #region "Public methods"

    /// <summary>
    /// Indicates if the control contains some data
    /// </summary>
    public override bool HasData()
    {
        return !DataHelper.DataSourceIsEmpty(dsAttachments.DataSource);
    }


    /// <summary>
    /// Clears data
    /// </summary>
    public void Clear()
    {
        Value = Guid.Empty;
        ReloadData(true);
    }

    #endregion


    #region "Private & protected methods"

    public override void ReloadData(bool forceReload)
    {
        Visible = !StopProcessing;
        if (StopProcessing)
        {
            dsAttachments.StopProcessing = true;
            newAttachmentElem.StopProcessing = true;
            // Do nothing
        }
        else
        {
            if (Node != null)
            {
                dsAttachments.Path = Node.NodeAliasPath;
                dsAttachments.CultureCode = CMSContext.PreferredCultureCode;

                SiteInfo si = SiteInfoProvider.GetSiteInfo(Node.NodeSiteID);
                SiteName = si.SiteName;
                dsAttachments.SiteName = SiteName;
            }

            // For new culture version temporary attachments are always used
            if ((Form != null) && (Form.Mode == CMS.FormEngine.FormModeEnum.InsertNewCultureVersion) && ((LastAction == "update") || (LastAction == "insert")))
            {
                VersionHistoryID = 0;
            }

            // Get attachment GUID
            attachmentGuid = ValidationHelper.GetGuid(Value, Guid.Empty);
            if (attachmentGuid == Guid.Empty)
            {
                dsAttachments.StopProcessing = true;
            }

            if (UsesWorkflow)
            {
                dsAttachments.DocumentVersionHistoryID = VersionHistoryID;
            }
            dsAttachments.AttachmentFormGUID = FormGUID;
            dsAttachments.AttachmentGUID = attachmentGuid;

            // Force reload datasource
            if (forceReload)
            {
                dsAttachments.DataSource = null;
                dsAttachments.DataBind();
            }

            // Ensure right column name (for attachments under workflow)
            if (!DataHelper.DataSourceIsEmpty(dsAttachments.DataSource))
            {
                DataSet ds = (DataSet)dsAttachments.DataSource;
                if (ds != null)
                {
                    DataTable dt = (ds).Tables[0];
                    if (!dt.Columns.Contains("AttachmentFormGUID"))
                    {
                        dt.Columns.Add("AttachmentFormGUID");
                    }

                    // Get inner attachment
                    innerAttachment = new AttachmentInfo(dt.Rows[0], TreeProvider.Connection);
                    Value = innerAttachment.AttachmentGUID;
                    hdnAttachName.Value = innerAttachment.AttachmentName;

                    // Check if temporary attachment should be created
                    createTempAttachment = ((DocumentID == 0) && (DocumentID != innerAttachment.AttachmentDocumentID));
                }
            }

            // Initialize button for adding attachments
            newAttachmentElem.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/upload_new.png"));
            newAttachmentElem.SourceType = MediaSourceEnum.Attachment;
            newAttachmentElem.ImageWidth = 200;
            newAttachmentElem.ImageHeight = 16;
            newAttachmentElem.DocumentID = DocumentID;
            newAttachmentElem.NodeParentNodeID = NodeParentNodeID;
            newAttachmentElem.NodeClassName = NodeClassName;
            newAttachmentElem.ResizeToWidth = ResizeToWidth;
            newAttachmentElem.ResizeToHeight = ResizeToHeight;
            newAttachmentElem.ResizeToMaxSideSize = ResizeToMaxSideSize;
            newAttachmentElem.FormGUID = FormGUID;
            newAttachmentElem.AttachmentGUIDColumnName = GUIDColumnName;
            newAttachmentElem.AllowedExtensions = AllowedExtensions;
            newAttachmentElem.ParentElemID = ClientID;
            newAttachmentElem.ForceLoad = true;
            newAttachmentElem.InnerDivHtml = ResHelper.GetString("attach.uploadfile");
            newAttachmentElem.InnerDivClass = InnerDivClass;
            newAttachmentElem.InnerLoadingDivHtml = ResHelper.GetString("attach.loading");
            newAttachmentElem.InnerLoadingDivClass = InnerLoadingDivClass;
            newAttachmentElem.IsLiveSite = IsLiveSite;
            newAttachmentElem.IncludeNewItemInfo = true;
            newAttachmentElem.CheckPermissions = CheckPermissions;
            newAttachmentElem.NodeSiteName = SiteName;

            imgDisabled.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/upload_newdisabled.png"));

            // Bind UniGrid to DataSource
            gridAttachments.DataSource = dsAttachments.DataSource;
            gridAttachments.LoadGridDefinition();
            gridAttachments.ReloadData();
        }
    }


    /// <summary>
    /// UniGrid action buttons event handler.
    /// </summary>
    protected void GridAttachmentsOnAction(string actionName, object actionArgument)
    {
        if (Enabled && !HideActions)
        {
            // Check the permissions
            #region "Check permissions"

            if (CheckPermissions)
            {
                if (FormGUID != Guid.Empty)
                {
                    if (!RaiseOnCheckPermissions("Create", this))
                    {
                        if (!CMSContext.CurrentUser.IsAuthorizedToCreateNewDocument(NodeParentNodeID, NodeClassName))
                        {
                            lblError.Text = ResHelper.GetString("attach.actiondenied");
                            return;
                        }
                    }
                }
                else
                {
                    if (!RaiseOnCheckPermissions("Modify", this))
                    {
                        if (CMSContext.CurrentUser.IsAuthorizedPerDocument(Node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                        {
                            lblError.Text = ResHelper.GetString("attach.actiondenied");
                            return;
                        }
                    }
                }
            }

            #endregion

            int attachmentId = 0;
            Guid attachmentGuid = Guid.Empty;

            // Get action argument (Guid or int)
            if (ValidationHelper.IsGuid(actionArgument))
            {
                attachmentGuid = ValidationHelper.GetGuid(actionArgument, Guid.Empty);
            }
            if (ValidationHelper.IsInteger(actionArgument))
            {
                attachmentId = ValidationHelper.GetInteger(actionArgument, 0);
            }

            // Process proper action
            switch (actionName.ToLower())
            {
                case "delete":
                    if (!createTempAttachment)
                    {
                        if (attachmentGuid != Guid.Empty)
                        {
                            // Delete attachment
                            if (FormGUID == Guid.Empty)
                            {
                                // Ensure automatic check-in/ check-out
                                VersionManager vm = null;

                                // Check out the document
                                if (AutoCheck && UsesWorkflow)
                                {
                                    vm = new VersionManager(TreeProvider);
                                    vm.CheckOut(Node);
                                }

                                DocumentHelper.DeleteAttachment(Node, attachmentGuid, TreeProvider);
                                Node.SetValue(GUIDColumnName, null);
                                DocumentHelper.UpdateDocument(Node, TreeProvider);

                                // Ensure full page refresh
                                if (UsesWorkflow && AutoCheck)
                                {
                                    ScriptHelper.RegisterStartupScript(Page, typeof(Page), "deleteRefresh", ScriptHelper.GetScript("InitRefresh_" + ClientID + "('', true, '" + attachmentGuid + "', 'delete');"));
                                }

                                // Check in the document
                                if (AutoCheck && UsesWorkflow)
                                {
                                    if (vm != null)
                                    {
                                        vm.CheckIn(Node, null, null);
                                        VersionHistoryID = Node.DocumentCheckedOutVersionHistoryID;
                                    }
                                }

                                // Log synchronization task if not under workflow
                                if (!UsesWorkflow)
                                {
                                    DocumentHelper.LogSynchronization(Node, CMS.Staging.TaskTypeEnum.UpdateDocument, TreeProvider);
                                }
                            }
                            else
                            {
                                AttachmentManager.DeleteTemporaryAttachment(attachmentGuid, CMSContext.CurrentSiteName);
                            }
                        }
                    }

                    LastAction = "delete";
                    Value = Guid.Empty;
                    break;
            }

            // Force reload data
            ReloadData(true);
        }
    }


    /// <summary>
    /// Overloaded ReloadData
    /// </summary>
    public override void ReloadData()
    {
        ReloadData(false);
    }


    /// <summary>
    /// UniGrid external data bound.
    /// </summary>
    protected object GridDocsOnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "update":
                PlaceHolder plcUpd = new PlaceHolder();
                plcUpd.ID = "plcUdateAction";

                // Add disabled image
                ImageButton imgUpdate = new ImageButton();
                imgUpdate.ID = "imgUpdate";
                imgUpdate.PreRender += imgUpdate_PreRender;
                plcUpd.Controls.Add(imgUpdate);

                // Add update control
                // Dynamically load uploader control
                DirectFileUploader dfuElem = Page.LoadControl("~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploader.ascx") as DirectFileUploader;

                // Set uploader's properties
                if (dfuElem != null)
                {
                    dfuElem.ID = "dfuElem" + DocumentID;
                    dfuElem.SourceType = MediaSourceEnum.Attachment;
                    if (!createTempAttachment)
                    {
                        dfuElem.AttachmentGUID = GetAttachmentGuid(sender as DataControlFieldCell);
                    }
                    dfuElem.PreRender += dfuElem_PreRender;
                    plcUpd.Controls.Add(dfuElem);
                }
                return plcUpd;

            case "edit":
                // Get file extension
                string extension = ValidationHelper.GetString(((DataRowView)((GridViewRow)parameter).DataItem).Row["AttachmentExtension"], string.Empty).ToLower();
                // Get attachment GUID
                attachmentGuid = ValidationHelper.GetGuid(((DataRowView)((GridViewRow)parameter).DataItem).Row["AttachmentGUID"], Guid.Empty);

                ImageButton img = (ImageButton)sender;
                if (createTempAttachment)
                {
                    img.Visible = false;
                }
                else
                {
                    img.AlternateText = extension;
                    img.ToolTip = attachmentGuid.ToString();
                    img.PreRender += img_PreRender;
                }
                break;

            case "delete":
                ImageButton imgDelete = (ImageButton)sender;
                // Turn off validation
                imgDelete.CausesValidation = false;
                imgDelete.PreRender += imgDelete_PreRender;
                // Explicitly initialize confirmation
                imgDelete.OnClientClick = "if(DeleteConfirmation() == false){return false;}";
                break;

            case "attachmentname":
                {
                    DataControlFieldCell cell = sender as DataControlFieldCell;
                    // Get attachment GUID
                    attachmentGuid = GetAttachmentGuid(cell);

                    // Get attachment extension
                    string attachmentExt = GetAttachmentExtension(cell);
                    bool isImage = ImageHelper.IsImage(attachmentExt);
                    string iconUrl = GetFileIconUrl(attachmentExt, "List");

                    // Get link for attachment
                    string attachmentUrl = null;
                    string attName = parameter.ToString();
                    int documentId = DocumentID;

                    if (Node != null)
                    {
                        if (IsLiveSite && (documentId > 0))
                        {
                            attachmentUrl = ResolveUrl(TreePathUtils.GetAttachmentUrl(attachmentGuid, UrlHelper.GetSafeFileName(attName, CMSContext.CurrentSiteName), 0, "getattachment"));
                        }
                        else
                        {
                            attachmentUrl = ResolveUrl(TreePathUtils.GetAttachmentUrl(attachmentGuid, UrlHelper.GetSafeFileName(attName, CMSContext.CurrentSiteName), VersionHistoryID, "getattachment"));
                        }
                    }
                    else
                    {
                        attachmentUrl = ResolveUrl(DocumentHelper.GetAttachmentUrl(attachmentGuid, VersionHistoryID));
                    }
                    attachmentUrl = UrlHelper.UpdateParameterInUrl(attachmentUrl, "chset", Guid.NewGuid().ToString());

                    // Ensure correct URL
                    if (SiteName != CMSContext.CurrentSiteName)
                    {
                        attachmentUrl = UrlHelper.AddParameterToUrl(attachmentUrl, "sitename", SiteName);
                    }

                    // Add latest version requirement for live site
                    if (IsLiveSite && (documentId > 0))
                    {
                        // Add requirement for latest version of files for current document
                        string newparams = "latestfordocid=" + documentId;
                        newparams += "&hash=" + ValidationHelper.GetHashString("d" + documentId);

                        attachmentUrl += "&" + newparams;
                    }

                    // Optionally trim attachment name
                    string attachmentName = TextHelper.LimitLength(attName, ATTACHMENT_NAME_LIMIT, "...");

                    // Tooltip
                    string tooltip = null;
                    if (ShowTooltip)
                    {
                        if (isImage)
                        {
                            int attachmentWidth = GetAttachmentWidth(cell);
                            if (attachmentWidth > 300)
                            {
                                attachmentWidth = 300;
                            }
                            tooltip = "onmouseout=\"UnTip()\" onmouseover=\"Tip('<div style=\\'width:" + attachmentWidth + "px;text-align:center;\\'><img src=\\'" + UrlHelper.AddParameterToUrl(attachmentUrl, "width", "300") + "\\' alt=\\'" + attachmentName + "\\' /></div>')\"";
                        }
                        else
                        {
                            tooltip = "onmouseout=\"UnTip()\" onmouseover=\"Tip('" + TextHelper.EnsureMaximumLineLength(attName, 90) + "', WIDTH, 0)\"";
                        }
                    }

                    // Icon
                    string imageTag = "<img class=\"Icon\" src=\"" + iconUrl + "\" alt=\"" + attachmentName + "\" />";
                    if (isImage)
                    {
                        return "<a href=\"#\" onclick=\"javascript: window.open('" + attachmentUrl + "'); return false;\"><span " + tooltip + ">" + imageTag + attachmentName + "</span></a>";
                    }
                    else
                    {
                        return "<a href=\"" + attachmentUrl + "\">" + imageTag + "</a><a href=\"" + attachmentUrl + "\"><span id=\"" + attachmentGuid + "\" " + tooltip + ">" + attachmentName + "</span></a>";
                    }
                }

            case "attachmentsize":
                long size = ValidationHelper.GetLong(parameter, 0);
                return DataHelper.GetSizeString(size);
        }

        return parameter;
    }


    #region "Grid actions events"

    protected void dfuElem_PreRender(object sender, EventArgs e)
    {
        DirectFileUploader dfuElem = (DirectFileUploader)sender;
        if (Enabled)
        {
            dfuElem.ForceLoad = true;
            dfuElem.FormGUID = FormGUID;
            dfuElem.AttachmentGUIDColumnName = GUIDColumnName;
            dfuElem.DocumentID = DocumentID;
            dfuElem.NodeParentNodeID = NodeParentNodeID;
            dfuElem.NodeClassName = NodeClassName;
            dfuElem.ResizeToWidth = ResizeToWidth;
            dfuElem.ResizeToHeight = ResizeToHeight;
            dfuElem.ResizeToMaxSideSize = ResizeToMaxSideSize;
            dfuElem.AllowedExtensions = AllowedExtensions;
            dfuElem.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/upload.png"));
            dfuElem.ImageHeight = 16;
            dfuElem.ImageWidth = 16;
            dfuElem.InsertMode = false;
            dfuElem.ParentElemID = ClientID;
            dfuElem.IncludeNewItemInfo = true;
            dfuElem.CheckPermissions = CheckPermissions;
            dfuElem.NodeSiteName = SiteName;
        }
        else
        {
            dfuElem.Visible = false;
        }
    }


    protected void imgUpdate_PreRender(object sender, EventArgs e)
    {
        ImageButton imgUpdate = (ImageButton)sender;
        if (!Enabled)
        {
            imgUpdate.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/uploaddisabled.png"));
            imgUpdate.Style.Add("cursor", "default");
            imgUpdate.Enabled = false;
        }
        else
        {
            imgUpdate.Visible = false;
        }
    }


    protected void img_PreRender(object sender, EventArgs e)
    {
        ImageButton img = (ImageButton)sender;

        if (CMSContext.CurrentUser.IsAuthenticated())
        {
            // If the file is not an image don't allow image editing
            if (!ImageHelper.IsSupportedByImageEditor(img.AlternateText) || !Enabled)
            {
                // Disable edit icon in case that attachment is not an image
                img.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/editdisabled.png");
                img.Enabled = false;
                img.Style.Add("cursor", "default");
            }
            else
            {
                string strForm = (FormGUID == Guid.Empty) ? "" : FormGUID.ToString();

                // Create security hash
                string form = null;
                if (!String.IsNullOrEmpty(strForm))
                {
                    form = "&formguid=" + strForm + "&parentid=" + NodeParentNodeID;
                }
                else if (Node != null)
                {
                    form += "&siteid=" + Node.NodeSiteID;
                }
                string parameters = "?attachmentGUID=" + img.ToolTip + "&versionHistoryID=" + VersionHistoryID + form + "&clientid=" + ClientID;
                string validationHash = QueryHelper.GetHash(parameters);
                img.OnClientClick = "EditImage_" + ClientID + "('" + img.ToolTip + "', '" + strForm + "', " + VersionHistoryID + ", " + NodeParentNodeID + ", '" + validationHash + "');return false;";
            }

            img.AlternateText = ResHelper.GetString("general.edit");
            img.ToolTip = "";
        }
        else
        {
            img.Visible = false;
        }
    }


    void imgDelete_PreRender(object sender, EventArgs e)
    {
        ImageButton imgDelete = (ImageButton)sender;
        if (!Enabled || !AllowDelete)
        {
            // Disable delete icon in case that editing is not allowed
            imgDelete.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/deletedisabled.png");
            imgDelete.Enabled = false;
            imgDelete.Style.Add("cursor", "default");
        }
    }

    #endregion


    /// <summary>
    /// Gets GUID value from DataControlFieldCell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>GUID of attachment</returns>
    protected Guid GetAttachmentGuid(DataControlFieldCell dcf)
    {
        // Get GUID of attachment
        return ValidationHelper.GetGuid(GetAttachmentProperty(dcf, "AttachmentGUID"), Guid.Empty);
    }


    /// <summary>
    /// Gets width value from DataControlFieldCell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Width of attachment</returns>
    protected int GetAttachmentWidth(DataControlFieldCell dcf)
    {
        // Get width of attachment
        return ValidationHelper.GetInteger(GetAttachmentProperty(dcf, "AttachmentImageWidth"), 0);
    }


    /// <summary>
    /// Gets extension value from DataControlFieldCell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Extension of attachment</returns>
    protected string GetAttachmentExtension(DataControlFieldCell dcf)
    {
        // Get extension of attachment
        return ValidationHelper.GetString(GetAttachmentProperty(dcf, "AttachmentExtension"), "");
    }


    protected object GetAttachmentProperty(DataControlFieldCell dcf, string property)
    {
        // Get datarowview
        DataRowView drv = ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
        if (drv != null)
        {
            // Get GUID of attachment
            return drv.Row[property];
        }
        return null;
    }

    #endregion
}
