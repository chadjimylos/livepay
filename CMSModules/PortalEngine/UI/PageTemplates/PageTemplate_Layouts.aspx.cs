using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.CMSHelper;
using CMS.DirectoryUtilities;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_Layouts : CMSEditTemplatePage
{
    protected int templateId = 0;
    protected PageTemplateInfo pti = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get the template
        templateId = QueryHelper.GetInteger("templateid", 0);
        pti = PageTemplateInfoProvider.GetPageTemplateInfo(templateId);

        radCustom.Text = ResHelper.GetString("TemplateLayout.Custom");
        radShared.Text = ResHelper.GetString("TemplateLayout.Shared");

        if (!RequestHelper.IsPostBack())
        {
            ReloadData();
        }       

        InitializeHeaderActions();
    }

    


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Disable items when virtual path provider is disabled
        if (!SettingsKeyProvider.UsingVirtualPathProvider && (pti != null))
        {
            this.lblVirtualInfo.Text = String.Format(ResHelper.GetString("TemplateLayout.VirtualPathProviderNotRunning"), PageTemplateInfoProvider.GetLayoutUrl(pti.CodeName, null));
            this.plcVirtualInfo.Visible = true;
            this.pnlCheckOutInfo.Visible = false;
            this.txtCustom.Enabled = false;
        }
    }


    /// <summary>
    /// Initializes header action control
    /// </summary>
    private void InitializeHeaderActions()
    {
        // Header actions
        string[,] actions = new string[4, 11];

        // Save button
        actions[0, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[0, 1] = ResHelper.GetString("General.Save");
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        actions[0, 6] = "save";
        actions[0, 8] = "true";

        if (SettingsKeyProvider.UsingVirtualPathProvider)
        {
            // CheckOut
            actions[1, 0] = HeaderActions.TYPE_SAVEBUTTON;
            actions[1, 1] = ResHelper.GetString("General.CheckOutToFile");
            actions[1, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkout.png");
            actions[1, 6] = "checkout";
            actions[1, 10] = "false";

            // CheckIn
            actions[2, 0] = HeaderActions.TYPE_SAVEBUTTON;
            actions[2, 1] = ResHelper.GetString("General.CheckInFromFile");
            actions[2, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkin.png");
            actions[2, 6] = "checkin";
            actions[2, 10] = "false";

            // UndoCheckOut
            actions[3, 0] = HeaderActions.TYPE_SAVEBUTTON;
            actions[3, 1] = ResHelper.GetString("General.UndoCheckout");
            actions[3, 2] = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("General.ConfirmUndoCheckOut")) + ");";
            actions[3, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/undocheckout.png");
            actions[3, 6] = "undocheckout";
            actions[3, 10] = "false";
        }

        if (pti != null)
        {
            // Check whether current layout is not shared and if is checked out by current user
            if ((pti.LayoutID == 0) && (pti.PageTemplateLayoutCheckedOutByUserID > 0))
            {
                // Checked out by current machine
                if (pti.PageTemplateLayoutCheckedOutMachineName.ToLower() == HTTPHelper.MachineName.ToLower())
                {
                    actions[2, 10] = "true";
                }
                if (CMSContext.CurrentUser.IsGlobalAdministrator)
                {
                    actions[3, 10] = "true";
                }
            }
            else
            {
                actions[1, 10] = "true";
            }
            if (pti.LayoutID > 0)
            {
                actions[1, 10] = "false";
            }
        }

        this.CurrentMaster.HeaderActions.LinkCssClass = "ContentSaveLinkButton";
        this.CurrentMaster.HeaderActions.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);
        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    /// <summary>
    /// Actions handler
    /// </summary>
    protected void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "save":

                SaveLayout();

                // Reload header actions
                InitializeHeaderActions();
                this.CurrentMaster.HeaderActions.ReloadData();
                break;

            case "checkout":

                // Save first
                if (!SaveLayout())
                {
                    return;
                }

                try
                {
                    SiteManagerFunctions.CheckOutTemplateLayout(templateId);
                }
                catch (Exception ex)
                {
                    this.lblError.Text = ResHelper.GetString("Layout.ErrorCheckout") + ": " + ex.Message;
                    this.lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;

            case "checkin":

                try
                {
                    SiteManagerFunctions.CheckInTemplateLayout(templateId);
                }
                catch (Exception ex)
                {
                    this.lblError.Text = ResHelper.GetString("Layout.ErrorCheckin") + ": " + ex.Message;
                    this.lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;

            case "undocheckout":

                try
                {
                    SiteManagerFunctions.UndoCheckOutTemplateLayout(templateId);
                }
                catch (Exception ex)
                {
                    this.lblError.Text = ResHelper.GetString("Layout.ErrorUndoCheckout") + ": " + ex.Message;
                    this.lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;
        }

    }


    /// <summary>
    /// Reload data
    /// </summary>
    public void ReloadData()
    {
        if (templateId > 0)
        {
            PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(templateId);

            if (pti != null)
            {
                if (!pti.IsPortal)
                {
                    plcContent.Visible = false;
                    pnlCheckOutInfo.Visible = false;
                    lblAspxInfo.Visible = true;
                    lblAspxInfo.Text = ResHelper.GetString("TemplateLayout.ASPXtemplate");

                    return;
                }

                this.pnlCheckOutInfo.Visible = true;

                if (pti.PageTemplateLayoutCheckedOutByUserID > 0)
                {
                    this.txtCustom.ReadOnly = true;
                    string username = null;
                    UserInfo ui = UserInfoProvider.GetUserInfo(pti.PageTemplateLayoutCheckedOutByUserID);
                    if (ui != null)
                    {
                        username = HTMLHelper.HTMLEncode(ui.FullName);
                    }

                    // Checked out by current machine
                    if (pti.PageTemplateLayoutCheckedOutMachineName.ToLower() == HTTPHelper.MachineName.ToLower())
                    {
                        this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("PageLayout.CheckedOut"), Server.MapPath(pti.PageTemplateLayoutCheckedOutFileName));
                    }
                    else
                    {
                        this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("PageLayout.CheckedOutOnAnotherMachine"), pti.PageTemplateLayoutCheckedOutFileName, username);
                    }
                }
                else
                {
                    string url = null;
                    if (pti.IsReusable)
                    {
                        url = Server.MapPath(PageTemplateInfoProvider.GetLayoutUrl(pti.CodeName, null));
                    }
                    else
                    {
                        url = Server.MapPath(PageTemplateInfoProvider.GetAdhocLayoutUrl(pti.CodeName, null));
                    }

                    this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("PageLayout.CheckOutInfo"), url);
                }                

                if (ValidationHelper.GetInteger(pti.LayoutID, 0) > 0)
                {
                    pnlCheckOutInfo.Visible = false;

                    radShared.Checked = true;
                    txtCustom.Enabled = false;
                    selectShared.Enabled = true;
                    
                    try
                    {
                        txtCustom.Text = pti.PageTemplateLayout;                        
                        selectShared.Value = pti.LayoutID.ToString();

                        if (ValidationHelper.GetString(pti.PageTemplateLayout, "") == "")
                        {
                            LayoutInfo li = LayoutInfoProvider.GetLayoutInfo(pti.LayoutID);
                            if (li != null)
                            {
                                txtCustom.Text = li.LayoutCode;
                            }
                        }
                    }
                    catch
                    {
                    }
                }
                else
                {
                    radCustom.Checked = true;
                    txtCustom.Text = pti.PageTemplateLayout;
                    txtCustom.Enabled = true;                    
                    selectShared.Enabled = false;
                }
            }
        }
    }


    /// <summary>
    /// Save layout
    /// </summary>
    public bool SaveLayout()
    {
        PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(templateId);

        if (pti != null)
        {
            //Shared layout
            if (radShared.Checked)
            {
                pnlCheckOutInfo.Visible = false;                
                pti.LayoutID = ValidationHelper.GetInteger(selectShared.Value, 0);
                pti.PageTemplateLayout = txtCustom.Text;
            }
            else //Custom layout
            {
                pnlCheckOutInfo.Visible = true;
                pti.LayoutID = 0;
                pti.PageTemplateLayout = txtCustom.Text;
            }

            PageTemplateInfoProvider.SetPageTemplateInfo(pti);

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");

            return true;
        }

        return false;
    }


    /// <summary>
    /// Radio button changed
    /// </summary>
    protected void radShared_CheckedChanged(object sender, EventArgs e)
    {
        if (radCustom.Checked)
        {
            txtCustom.Enabled = true;
            selectShared.Enabled = false;
        }
        else
        {
            txtCustom.Enabled = false;            
            selectShared.Enabled = true;
            LayoutInfo li = LayoutInfoProvider.GetLayoutInfo(ValidationHelper.GetInteger(selectShared.Value, 0));
            if (li != null)
            {
                txtCustom.Text = li.LayoutCode;
            }
        }
    }


    /// <summary>
    /// DropDownlist change
    /// </summary>
    protected void selectShared_Changed()
    {
        LayoutInfo li = LayoutInfoProvider.GetLayoutInfo(ValidationHelper.GetInteger(selectShared.Value, 0));
        if (li != null)
        {
            txtCustom.Text = li.LayoutCode;
        }
    }
}
