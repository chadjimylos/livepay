<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyProfile_Subscriptions.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSDesk_MyDesk_MyProfile_MyProfile_Subscriptions"
    Theme="Default" %>

<%@ Register Src="~/CMSModules/Membership/Controls/Subscriptions.ascx" TagName="Subscriptions"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:Subscriptions ID="elemSubscriptions" runat="server" IsLiveSite="false" />
</asp:Content>
