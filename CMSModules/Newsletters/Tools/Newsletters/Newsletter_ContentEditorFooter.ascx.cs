using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Newsletter;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_ContentEditorFooter : CMSUserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        string frame = "0";

        if (this.Page.Request.Browser.Browser == "IE")
        {
            frame = "1";
        }

        this.btnInsertField.Attributes.Add("onMouseOver", "if ( frames[" + frame + "] != null ) { frames[" + frame + "].RememberFocusedRegion();}");
        this.btnInsertField.Attributes.Add("onClick", " if ( frames[" + frame + "] != null ) {  frames[" + frame + "].InsertHTML('{%' + document.getElementById('" + this.lstInsertField.ClientID + "').value +  '%}'); } return false;");

        btnInsertField.Text = ResHelper.GetString("NewsletterTemplate_Edit.Insert");
        lblInsertField.Text = ResHelper.GetString("NewsletterTemplate_Edit.TemplateInsertFieldLabel");

        macroSelectorElm.Resolver = MacroSelector.NewsletterResolver;

        if (!RequestHelper.IsPostBack())
        {
            FillFieldsList();
        }

        // If item in list is selected, it gets focus => we must remember id of last focused editable region
        lstInsertField.Attributes.Add("onMouseOver", "if (frames[" + frame + "] != null) {  frames[" + frame + "].RememberFocusedRegion(); }");
    }


    /// <summary>
    /// Fills list of available fields
    /// </summary>
    protected void FillFieldsList()
    {
        ListItem newItem = new ListItem(ResHelper.GetString("general.email"), IssueHelper.MacroEmail);
        lstInsertField.Items.Add(newItem);
        newItem = new ListItem(ResHelper.GetString("NewsletterTemplate.MacroFirstName"), IssueHelper.MacroFirstName);
        lstInsertField.Items.Add(newItem);
        newItem = new ListItem(ResHelper.GetString("NewsletterTemplate.MacroLastName"), IssueHelper.MacroLastName);
        lstInsertField.Items.Add(newItem);
        newItem = new ListItem(ResHelper.GetString("NewsletterTemplate.MacroUnsubscribeLink"), IssueHelper.MacroUnsubscribeLink);
        lstInsertField.Items.Add(newItem);
    }
}
