using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;

public partial class CMSModules_AdminControls_Controls_Class_ExtensionsConfiguration : CMSUserControl, ICallbackEventHandler, IFieldEditorControl
{
    #region "Public properties"

    /// <summary>
    /// Allowed extensions
    /// </summary>
    public string AllowedExtensions
    {
        get
        {
            return txtAllowedExtensions.Text.Trim();
        }

        set
        {
            txtAllowedExtensions.Text = value;
            if (!string.IsNullOrEmpty(value))
            {
                chkInehrit.Checked = false;
            }
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Registred scripts
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AllowedExt_EnableDisableForm", GetScriptEnableDisableForm());
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AllowedExt_ReceiveExtensions", GetScriptReceiveExtensions());
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AllowedExt_LoadSiteSettings", ScriptHelper.GetScript("function GetExts(txtAllowedExtensions){ return " + this.Page.ClientScript.GetCallbackEventReference(this, "txtAllowedExtensions", "ReceiveExts", null) + " } \n"));

        // Initialize form
        chkInehrit.Attributes.Add("onclick", GetEnableDisableFormDefinition());
        EnableDisableForm();
    }


    #region "Private methods"

    private string GetScriptReceiveExtensions()
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("function ReceiveExts(rValue, context){");
        builder.Append("var extensions = rValue.split(\"|\");");
        builder.Append("var txtExtensions = document.getElementById(extensions[1]);\n");
        builder.Append("if (txtExtensions != null)\n");
        builder.Append("{ txtExtensions.value = extensions[0]; }}\n");

        return ScriptHelper.GetScript(builder.ToString());
    }


    private string GetScriptEnableDisableForm()
    {
        StringBuilder builder = new StringBuilder();
        builder.Append("function EnableDisableExtForm(chkAllowedExtensions, txtAllowedExtensions){");
        builder.Append("var txtAllowedExt = document.getElementById(txtAllowedExtensions);\n");
        builder.Append("var chkAllowedExt = document.getElementById(chkAllowedExtensions);\n");
        builder.Append("if (txtAllowedExt != null) {\n");
        builder.Append("var disabled = chkAllowedExt.checked;\n");
        builder.Append("txtAllowedExt.disabled = disabled;\n");
        builder.Append("if (disabled){ GetExts(txtAllowedExtensions); }\n");
        builder.Append("}}\n");

        return ScriptHelper.GetScript(builder.ToString());
    }


    private string GetEnableDisableFormDefinition()
    {
        return string.Format("EnableDisableExtForm('{0}', '{1}');", this.chkInehrit.ClientID, this.txtAllowedExtensions.ClientID);
    }


    private void EnableDisableForm()
    {
        ScriptHelper.RegisterStartupScript(this, typeof(string), "AllowedExt_EnableDisableFormStartup", ScriptHelper.GetScript(GetEnableDisableFormDefinition()));
    }


    private object GetKeyValue(Hashtable config, string key)
    {
        if ((config != null) && config.ContainsKey(key))
        {
            return config[key];
        }
        return null;
    }

    #endregion


    #region "Public methods"

    /// <summary>
    /// Sets inner controls according to the parameters and their values included in configuration collection. Parameters collection will be passed from Field editor
    /// </summary>
    /// <param name="config">Parameters collection.</param>
    public void LoadConfiguration(Hashtable config)
    {
        object value = GetKeyValue(config, "extensions");
        if (ValidationHelper.GetString(value, "").ToLower() == "custom")
        {
            // Use custom settings
            chkInehrit.Checked = false;

            // Load allowed extensions
            value = GetKeyValue(config, "allowed_extensions");
            string allowedExt = ValidationHelper.GetString(value, "");
            txtAllowedExtensions.Text = allowedExt;
        }
        else
        {
            // Use site settings
            chkInehrit.Checked = true;

            string siteName = CMSContext.CurrentSiteName;
            txtAllowedExtensions.Text = SettingsKeyProvider.GetStringValue(siteName + ".CMSUploadExtensions");
        }

        EnableDisableForm();
    }


    /// <summary>
    /// Updates parameters collection of parameters and values according to the values of the inner controls. Parameters collection which should be updated will be passed from Field editor.
    /// </summary>
    /// <param name="config">Parameters collection.</param>
    public void UpdateConfiguration(Hashtable config)
    {
        config.Remove("extensions");
        config.Remove("allowed_extensions");

        if (!chkInehrit.Checked)
        {
            config["extensions"] = "custom";

            if (txtAllowedExtensions.Text.Trim() != "")
            {
                config["allowed_extensions"] = txtAllowedExtensions.Text.Trim();
            }
        }
    }


    /// <summary>
    /// Clears inner controls - sets them to the default values.
    /// </summary>
    public void ClearControl()
    {
        LoadConfiguration(new Hashtable());
    }


    /// <summary>
    /// If input values are invalid dislays error message.
    /// </summary>    
    public string Validate()
    {
        return "";
    }

    #endregion


    #region "Callback handling"

    string extensions = "";

    /// <summary>
    /// Gets callback result
    /// </summary>
    public string GetCallbackResult()
    {
        return extensions;
    }


    public void RaiseCallbackEvent(string eventArgument)
    {
        // Get site settings
        string siteName = CMSContext.CurrentSiteName;
        extensions = SettingsKeyProvider.GetStringValue(siteName + ".CMSUploadExtensions");

        // Returns site settings back to the client
        extensions = string.Format("{0}|{1}", extensions, eventArgument);
    }

    #endregion
}
