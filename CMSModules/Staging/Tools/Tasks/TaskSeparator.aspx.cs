using System;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.Staging;

public partial class CMSModules_Staging_Tools_Tasks_TaskSeparator : CMSStagingPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string siteName = CMSContext.CurrentSiteName;

        // Check enabled servers
        if (!ServerInfoProvider.IsEnabledServer(CMSContext.CurrentSiteID))
        {
            lblInfo.Text = ResHelper.GetString("ObjectStaging.NoEnabledServer");
        }
        else if (ValidationHelper.GetBoolean(SettingsKeyProvider.GetBoolValue(siteName + ".CMSStagingLogChanges"), false))
        {
            // Check DLL required for for staging
            if (SiteManagerFunctions.CheckStagingDLL())
            {
                UrlHelper.Redirect("Frameset.aspx");
            }

            lblInfo.Text = ResHelper.GetString("ObjectStaging.RenameDll");
        }
        else
        {
            lblInfo.Text = ResHelper.GetString("ContentStaging.TaskSeparator");
        }
    }
}
