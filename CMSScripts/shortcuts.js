﻿var allowShortcuts = true;

function EditKeyCommand(evt) {
    var key = null;
    if (evt.keyCode) {
        key = evt.keyCode;
    }
    else if (evt.which) {
        key = evt.which;
    }
    else {
        key = evt.charCode;
    }

    var stringKey = String.fromCharCode(key).toLowerCase();
    var cmd = '';
    if (!evt || !evt.ctrlKey) {
        return true;
    }

    switch (stringKey) {
        case 's':
            {
                evt.returnValue = false;
                evt.cancelBubble = true;

                if (evt.preventDefault) {
                    evt.preventDefault();
                    evt.stopPropagation();
                }

                if (!window.disableShortcuts) {
                    if (window.SaveDocument) {
                        SaveDocument();
                    }
                    else if ((window.parent != null) && window.parent.SaveDocument) {
                        window.parent.SaveDocument();
                    }
                }
                return false;
            }
    };
    return true;
}

if (document.addEventListener) {
    document.addEventListener("keydown", EditKeyCommand, true);
}
else if (document.attachEvent) {
    document.attachEvent("onkeydown", function(evt) { EditKeyCommand(evt) });
}

if (typeof (Sys) != "undefined" && typeof (Sys.Application) != "undefined") {
    Sys.Application.notifyScriptLoaded();
}