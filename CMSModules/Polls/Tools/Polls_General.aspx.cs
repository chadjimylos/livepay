using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Polls;
using CMS.UIControls;

public partial class CMSModules_Polls_Tools_Polls_General : CMSPollsPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PollProperties.ItemID = QueryHelper.GetInteger("pollid", 0);
        PollProperties.IsLiveSite = false;
    }
}
