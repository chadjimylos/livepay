using System;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_InlineControls_InlineControl_Header : SiteManagerPage
{
    protected int inlinecontrolID;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["inlinecontrolID"]))
        {
            inlinecontrolID = Convert.ToInt32(Request.QueryString["inlinecontrolID"]);
        }

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();            
        }

        string currentInlineControlName = string.Empty;
        try
        {
            if (InlineControlInfoProvider.GetInlineControlInfo(inlinecontrolID) != null)
            {
                currentInlineControlName = InlineControlInfoProvider.GetInlineControlInfo(inlinecontrolID).ControlDisplayName;
            }
        }
        catch
        {
        }

        // initializes page title
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("InlineControl_Header.Inlinecontrols");
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/InlineControls/InlineControl_List.aspx";
        pageTitleTabs[0, 2] = "_parent";
        pageTitleTabs[1, 0] = currentInlineControlName;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("InlineControl_Header.Title");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_InlineControl/object.png");
        this.CurrentMaster.Title.HelpTopicName = "general_tab16";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Initializes edit menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string[,] tabs = new string[3, 4];
        tabs[0, 0] = ResHelper.GetString("General.General");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab16');";
        tabs[0, 2] = "InlineControl_General.aspx?inlinecontrolID=" + inlinecontrolID;
        
        // Not supported yet
        //tabs[1, 0] = ResHelper.GetString("general.properties");
        //tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'inline_control_properties');";
        //tabs[1, 2] = "InlineControl_Properties.aspx?inlinecontrolID=" + inlinecontrolID;

        tabs[2, 0] = ResHelper.GetString("general.sites");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'sites_tab5');";
        tabs[2, 2] = "InlineControl_Sites.aspx?inlinecontrolID=" + inlinecontrolID;

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "content";
    }
}
