﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentType_Edit_SearchFields.aspx.cs"
    Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_SearchFields"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Document Type Edit - Search" %>

<%@ Register Src="~/CMSModules/SmartSearch/Controls/Edit/SearchFields.ascx"
    TagName="SearchFields" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" EnableViewState="false" />
    <cms:SearchFields runat="server" ID="SearchFields" />
</asp:Content>
