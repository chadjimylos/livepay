<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BizForm_Edit_Export.aspx.cs"
    Inherits="CMSModules_BizForms_Tools_BizForm_Edit_Export" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server" enableviewstate="false">
    <title>BizForm Edit - export data to excel</title>
</head>
<body>
	<form id="Form1" runat="server">
		<asp:GridView ID="gridView" runat="server" AutoGenerateColumns="false" />
	</form>
</body>
</html>
