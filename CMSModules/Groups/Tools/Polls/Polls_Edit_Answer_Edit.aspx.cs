using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Polls;
using CMS.UIControls;
using CMS.Community;

public partial class CMSModules_Groups_Tools_Polls_Polls_Edit_Answer_Edit : CMSGroupPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get AnswerID and PollID from querystring
        int pollId = QueryHelper.GetInteger("pollid", 0);
        int answerId = QueryHelper.GetInteger("answerId", 0);
        string currentPollAnswer = ResHelper.GetString("Polls_Answer_Edit.NewItemCaption");

        // Initialize AnswerEdit control
        if (QueryHelper.GetInteger("saved", 0) == 1)
        {
            AnswerEdit.Saved = true;
        }
        AnswerEdit.ItemID = answerId;
        AnswerEdit.PollId = pollId;
        AnswerEdit.OnSaved += new EventHandler(AnswerEdit_OnSaved);
        AnswerEdit.OnCheckPermissions += new CMSAdminControl.CheckPermissionsEventHandler(AnswerEdit_OnCheckPermissions);

        if (answerId > 0)
        {
            this.CurrentMaster.Title.HelpTopicName = "answer_edit";
            PollAnswerInfo pollAnswerObj = PollAnswerInfoProvider.GetPollAnswerInfo(answerId);
            if (pollAnswerObj != null)
            {
                currentPollAnswer = ResHelper.GetString("Polls_Answer_Edit.AnswerLabel") + " " + pollAnswerObj.AnswerOrder.ToString();
                pollId = pollAnswerObj.AnswerPollID;
            }
        }
        else
        {
            this.CurrentMaster.Title.HelpTopicName = "new_answer";
        }

        if (!RequestHelper.IsPostBack())
        {
            AnswerEdit.ReloadData();
        }

        // Initializes page title control		
        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("Polls_Answer_Edit.ItemListLink");
        breadcrumbs[0, 1] = "~/CMSModules/Groups/Tools/Polls/Polls_Edit_Answer_List.aspx?pollId=" + pollId;
        breadcrumbs[0, 2] = "";
        breadcrumbs[1, 0] = currentPollAnswer;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Polls_Answer_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("Polls_Edit_Answer_Edit.aspx?pollId=" + pollId.ToString());
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Polls_PollAnswer/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    void AnswerEdit_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        // Check 'Manage' permission
        PollInfo pi = PollInfoProvider.GetPollInfo(AnswerEdit.PollId);
        int groupId = 0;

        if (pi != null)
        {
            groupId = pi.PollGroupID;
        }

        // Check permissions
        CheckPermissions(groupId, CMSAdminControl.PERMISSION_MANAGE);
    }


    /// <summary>
    /// AnswerEdit event handler.
    /// </summary>
    void AnswerEdit_OnSaved(object sender, EventArgs e)
    {
        UrlHelper.Redirect("Polls_Edit_Answer_Edit.aspx?answerId=" + AnswerEdit.ItemID.ToString() + "&saved=1");
    }
}
