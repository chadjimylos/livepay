using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;

public partial class CMSAdminControls_UI_PageElements_PageTitle : PageTitle
{
    #region "Public propetries"

    /// <summary>
    /// Title CSS class.
    /// </summary>
    public override string TitleCssClass
    {
        get
        {
            return pnlTitle.CssClass;
        }
        set
        {
            pnlTitle.CssClass = value;
        }
    }

    /// <summary>
    /// Placeholder after image and title text
    /// </summary>
    public override PlaceHolder RightPlaceHolder
    {
        get
        {
            return plcMisc;
        }
        set
        {
            plcMisc = value;
        }
    }

    #endregion


    protected void Page_PreRender(object sender, EventArgs e)
    {
        string titlePart = null;

        // Create the breadcrumbs
        if (Breadcrumbs != null)
        {
            pnlBreadCrumbs.Visible = true;

            // Generate the breadcrumbs controls
            int breadCrumbsLength = Breadcrumbs.GetUpperBound(0);
            if (CultureHelper.IsUICultureRTL())
            {
                for (int i = breadCrumbsLength; i >= 0; i--)
                {
                    CreateBreadCrumbsItem(i, breadCrumbsLength);
                }
            }
            else
            {
                for (int i = 0; i <= breadCrumbsLength; i++)
                {
                    CreateBreadCrumbsItem(i, 0);
                }
            }

            if (breadCrumbsLength >= 0)
            {
                titlePart = Breadcrumbs[breadCrumbsLength, 0];
            }
        }

        // Set the title text if set
        if (!string.IsNullOrEmpty(TitleText))
        {
            pnlTitle.Visible = true;
            lblTitle.Text = TitleText;

            titlePart = TitleText;
        }
        else
        {
            pnlTitle.Visible = false;
        }

        // Allways render alt attribute
        if (!string.IsNullOrEmpty(AlternateText))
        {
            imgTitle.AlternateText = AlternateText;
        }
        else
        {
            imgTitle.Attributes.Add("alt", string.Empty);
        }
        
        if (!string.IsNullOrEmpty(TitleImage))
        {
            imgTitle.Visible = true;
            imgTitle.ImageUrl = TitleImage;
        }
        else
        {
            imgTitle.Visible = false;
        }

        // Set the window title accordingly
        if (SetWindowTitle && (titlePart != null))
        {
            ScriptHelper.RegisterTitleScript(this.Page, titlePart);
        }

    }


    /// <summary>
    /// Creates the breadcrumbs array from the given parameters
    /// </summary>
    /// <param name="namepath">Name path</param>
    public void CreateStaticBreadCrumbs(string namepath)
    {
        if ((namepath == null) || (namepath.Trim().Trim('/') == ""))
        {
            Breadcrumbs = null;
            return;
        }

        string[] names = namepath.Trim('/').Split('/');
        string[,] bc = new string[names.Length, 3];
        
        //
        int index = 0;
        foreach (string name in names)
        {
            bc[index, 0] = name;
            bc[index, 1] = string.Empty;
            bc[index, 2] = string.Empty;
            index++;
        }
        Breadcrumbs = bc;
    }


    private void CreateBreadCrumbsItem(int index, int startIndex)
    {
        // Add separator
        if (index != startIndex)
        {
            plcBreadcrumbs.Controls.Add(new LiteralControl(" "));
            Label sepLabel = new Label();
            sepLabel.Text = !string.IsNullOrEmpty(SeparatorText) ? SeparatorText : "&nbsp;";
            sepLabel.CssClass = "TitleBreadCrumbSeparator";
            plcBreadcrumbs.Controls.Add(sepLabel);
            plcBreadcrumbs.Controls.Add(new LiteralControl(" "));
        }

        // Make link if URL specified
        string text = EncodeBreadcrumbs ? HTMLHelper.HTMLEncode(Breadcrumbs[index, 0]) : Breadcrumbs[index, 0];
        string cssClass = (index != Breadcrumbs.GetUpperBound(0)) ? "TitleBreadCrumb" : "TitleBreadCrumbLast";

        if (!string.IsNullOrEmpty(Breadcrumbs[index, 1]) && (index != Breadcrumbs.GetUpperBound(0)))
        {
            HyperLink newLink = new HyperLink();
            newLink.Text = text;
            newLink.NavigateUrl = Breadcrumbs[index, 1];
            newLink.Target = Breadcrumbs[index, 2];
            newLink.CssClass = cssClass;
            plcBreadcrumbs.Controls.Add(newLink);
        }
        else // Make label if last item or URL not specified
        {
            Label newLabel = new Label();
            newLabel.Text = text;
            newLabel.CssClass = cssClass;
            plcBreadcrumbs.Controls.Add(newLabel);
        }
    }
}
