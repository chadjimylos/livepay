using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.Ecommerce;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.LicenseProvider;

public partial class CMSModules_Ecommerce_Administration_Users_User_Edit_Departments : CMSUsersPage
{
    protected int userId = 0;
    protected string currentValues = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check the license
        LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Ecommerce);

        // Check 'EcommerceRead' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceRead"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceRead");
        }

        // Check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            this.uniSelector.Enabled = false;
        }

        bool ecommerceOnSite = false;
        ResourceInfo ri = ResourceInfoProvider.GetResourceInfo("CMS.Ecommerce");
        if (ri != null)
        {
            ecommerceOnSite = ResourceInfoProvider.IsResourceOnSite(ri.ResourceId, CMSContext.CurrentSiteID);
        }

        // Check 'EcommerceRead' permission
        if (!ecommerceOnSite || !CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationRead"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationRead");
        }

        userId = QueryHelper.GetInteger("userid", 0);

        if (userId > 0)
        {
            // Check that only global administrator can edit global administrator's accouns
            UserInfo ui = UserInfoProvider.GetUserInfo(userId);
            if (!CheckGlobalAdminEdit(ui))
            {
                plcTable.Visible = false;
                lblError.Text = ResHelper.GetString("Administration-User_List.ErrorGlobalAdmin");
                lblError.Visible = true;
                return;
            }

            DataSet ds = DepartmentInfoProvider.GetUserDepartments(userId);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "DepartmentID"));
            }

            if (!IsPostBack)
            {
                uniSelector.Value = currentValues;
            }
        }
        uniSelector.ButtonAddItems.Text = ResHelper.GetString("general.ok");
        uniSelector.IconPath = GetImageUrl("Objects/Ecommerce_Department/object.png");
        uniSelector.OnSelectionChanged += usSites_OnSelectionChanged;
    }


    /// <summary>
    /// Check whether current user is allowed to modify another user. Return "" or error message.
    /// </summary>
    /// <param name="userId">Modified user</param>
    protected string ValidateGlobalAndDeskAdmin(int userId)
    {
        string result = String.Empty;

        if (CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            return result;
        }

        UserInfo userInfo = UserInfoProvider.GetUserInfo(userId);
        if (userInfo == null)
        {
            result = ResHelper.GetString("Administration-User.WrongUserId");
        }
        else
        {
            if (userInfo.IsGlobalAdministrator)
            {
                result = ResHelper.GetString("Administration-User.NotAllowedToModify");
            }
        }
        return result;
    }


    protected void usSites_OnSelectionChanged(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        string result = ValidateGlobalAndDeskAdmin(userId);

        if (result != String.Empty)
        {
            lblError.Visible = true;
            lblError.Text = result;
            return;
        }

        // Remove old items
        string newValues = ValidationHelper.GetString(uniSelector.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int departmentId = ValidationHelper.GetInteger(item, 0);
                    DepartmentInfoProvider.RemoveUserFromDepartment(departmentId, userId);
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int departmentId = ValidationHelper.GetInteger(item, 0);
                    DepartmentInfoProvider.AddUserToDepartment(departmentId, userId);
                }
            }
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}
