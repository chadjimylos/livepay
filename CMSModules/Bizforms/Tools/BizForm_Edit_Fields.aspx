<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BizForm_Edit_Fields.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="BizForm Fields"
    Inherits="CMSModules_BizForms_Tools_BizForm_Edit_Fields" EnableEventValidation="false"
    Theme="Default" %>
<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/FieldEditor.ascx" TagName="FieldEditor"
    TagPrefix="cms" %>
        
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" EnableViewState="false" />
    <cms:FieldEditor ID="FieldEditor" runat="server" />
</asp:Content>
