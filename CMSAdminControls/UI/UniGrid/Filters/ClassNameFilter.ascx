<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ClassNameFilter.ascx.cs"
    Inherits="CMSAdminControls_UI_UniGrid_Filters_ClassNameFilter" %>
<asp:Panel CssClass="Filter" runat="server" ID="pnlSearch">
    <asp:PlaceHolder ID="plcLabel" runat="server">
        <cms:LocalizedLabel ID="lblClass" runat="server" DisplayColon="true" ResourceString="general.documenttype"
            CssClass="FieldLabel" />&nbsp;</asp:PlaceHolder>
    <asp:DropDownList ID="drpOperator" runat="server" CssClass="ContentDropdown" />
    <asp:TextBox ID="txtClassName" runat="server" MaxLength="100" Class="SelectorTextBox" />
</asp:Panel>
