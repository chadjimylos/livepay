using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;


public partial class CMSModules_Content_Controls_Dialogs_LinkMediaSelector_Search : CMSUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.StopProcessing)
        {
            // Initialize nested controls
            SetupControls();
        }
        else 
        {
            this.Visible = false;
        }
    }


    /// <summary>
    /// Makes search empty.
    /// </summary>
    public void ResetSearch() 
    {
        this.txtSearchByName.Text = "";
    }


    /// <summary>
    /// Initializes all the nested controls.
    /// </summary>
    private void SetupControls()
    {
        // Get JavaScript definition of method used to set hidden attributes
        string setSearchAction = "function SetSearchAction(){                                                           " +
                                 "  var searchTxt = document.getElementById('" + this.txtSearchByName.ClientID + "');   " +                                 
                                 @"  if(searchTxt!=null){                                                                
                                       SetAction('search', searchTxt.value);                                           
                                   }                                                                                   
                                 }                                                                                     
                                                                                                                        
                                 function SetSearchFocus(){                                                             " +
                                 "  var searchTxt = document.getElementById('" + this.txtSearchByName.ClientID + "');   " +
                                 @"  if(searchTxt!=null){                                                              
                                       searchTxt.focus();      
                                       searchTxt.select();                                     
                                   }                                                                                   
                                 }                                                                                      
                                 ";

        ScriptHelper.RegisterStartupScript(this, this.GetType(), "SearchScripts", ScriptHelper.GetScript(setSearchAction));

        // Set search scripts
        this.txtSearchByName.Attributes["onkeydown"] = "var keynum; if(window.event){ keynum = event.keyCode; } else if(event.which){ keynum = event.which; } if(keynum == 13){ SetSearchAction(); RaiseHiddenPostBack(); return false; }";
        this.btnSearch.Attributes["onclick"]= "SetSearchAction(); RaiseHiddenPostBack(); return false;";
    }
}
