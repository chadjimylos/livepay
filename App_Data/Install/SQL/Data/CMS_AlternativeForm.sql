SET IDENTITY_INSERT [CMS_AlternativeForm] ON
INSERT INTO [CMS_AlternativeForm] ([FormLastModified], [FormDisplayName], [FormClassID], [FormID], [FormGUID], [FormLayout], [FormCoupledClassID], [FormDefinition], [FormName]) VALUES ('20100305 15:32:51', N'Registration form', 59, 124, '005a6c1c-a442-4229-ba5c-80f423dea704', N'<table class="CustomRegistrationForm">
    <tbody>
        <tr>
            <td>$$label:UserName$$</td>
            <td>$$input:UserName$$$$validation:UserName$$</td>
        </tr>
        <tr>
            <td>$$label:FirstName$$</td>
            <td>$$input:FirstName$$$$validation:FirstName$$</td>
        </tr>
        <tr>
            <td>$$label:LastName$$</td>
            <td>$$input:LastName$$$$validation:LastName$$</td>
        </tr>
        <tr>
            <td>$$label:Email$$</td>
            <td>$$input:Email$$$$validation:Email$$</td>
        </tr>
        <tr>
            <td valign="top" style="vertical-align: top; padding-top: 3px;"><span class="EditingFormLabel">$$label:UserPassword$$</span>
            <div style="margin-top: 13px;"><span class="EditingFormLabel">Confirm password:</span></div>
            </td>
            <td>$$input:UserPassword$$$$validation:UserPassword$$</td>
        </tr>
        <tr>
            <td>$$label:UserGender$$</td>
            <td>$$input:UserGender$$$$validation:UserGender$$</td>
        </tr>
    </tbody>
</table>', 1768, N'<form><field column="UserID" visible="false" /><field column="UserName" fieldcaption="User name" fieldtype="usercontrol"><settings><controlname>username</controlname></settings></field><field column="FirstName" fieldcaption="First name" minstringlength="1" validationerrormessage="Please enter some first name." /><field column="MiddleName" visible="false" /><field column="LastName" fieldcaption="Last name" minstringlength="1" validationerrormessage="Please enter some last name." /><field column="FullName" fieldcaption="Full name" visible="false" allowempty="false" ismacro="false" /><field column="Email" fieldtype="usercontrol" minstringlength="5" validationerrormessage="Please enter some email."><settings><controlname>emailinput</controlname></settings></field><field column="UserPassword" fieldcaption="Password" fieldtype="usercontrol" minstringlength="6" regularexpression="\w+"><settings><controlname>passwordconfirmator</controlname></settings></field><field column="PreferredCultureCode" visible="false" /><field column="PreferredUICultureCode" visible="false" /><field column="UserEnabled" visible="false" defaultvalue="false" /><field column="UserIsEditor" visible="false" defaultvalue="false" /><field column="UserIsGlobalAdministrator" visible="false" defaultvalue="false" /><field column="UserIsExternal" visible="false" defaultvalue="false" /><field column="UserPasswordFormat" visible="false" /><field column="UserCreated" visible="false"><settings><editTime>false</editTime></settings></field><field column="LastLogon" visible="false"><settings><editTime>false</editTime></settings></field><field column="UserStartingAliasPath" visible="false" /><field column="UserGUID" visible="false" /><field column="UserLastModified" visible="false"><settings><editTime>false</editTime></settings></field><field column="UserLastLogonInfo" visible="false" /><field column="UserIsHidden" visible="false" defaultvalue="false" /><field column="UserVisibility" visible="false" /><field column="UserSettingsID" visible="false" /><field column="UserNickName" visible="false" /><field column="UserPicture" visible="false" /><field column="UserSignature" visible="false" /><field column="UserURLReferrer" visible="false" /><field column="UserCampaign" visible="false" /><field column="UserMessagingNotificationEmail" visible="false" /><field column="UserCustomData" visible="false" /><field column="UserRegistrationInfo" visible="false" /><field column="UserPreferences" visible="false" /><field column="UserActivationDate" visible="false" /><field column="UserActivatedByUserID" visible="false" /><field column="UserTimeZoneID" fieldcaption="Time zone" visible="false" fieldtype="usercontrol"><settings><controlname>timezoneselector</controlname></settings></field><field column="UserAvatarID" fieldcaption="Avatar" visible="false" fieldtype="usercontrol"><settings><controlname>useravatarselector</controlname></settings></field><field column="UserBadgeID" visible="false" /><field column="UserShowSplashScreen" visible="false" /><field column="UserActivityPoints" visible="false" /><field column="UserForumPosts" visible="false" /><field column="UserBlogComments" visible="false" /><field column="UserGender" fieldcaption="Gender" fieldtype="radiobuttons"><settings><repeatdirection>horizontal</repeatdirection><options><item value="1" text="{$general.male$}" /><item value="2" text="{$general.female$}" /></options></settings></field><field column="UserDateOfBirth" fieldcaption="Date of birth" visible="false"><settings><editTime>false</editTime></settings></field><field column="UserMessageBoardPosts" visible="false" /><field column="UserSettingsUserGUID" visible="false" /><field column="UserSettingsUserID" visible="false" /><field column="UserBlogPosts" visible="false" /></form>', N'RegistrationForm')
INSERT INTO [CMS_AlternativeForm] ([FormLastModified], [FormDisplayName], [FormClassID], [FormID], [FormGUID], [FormLayout], [FormCoupledClassID], [FormDefinition], [FormName]) VALUES ('20100305 15:31:37', N'Display profile', 59, 125, '58c69789-2f4b-4e9b-b557-cabf89c65064', N'<table cellpadding="2">
    <tbody>
        <tr>
            <td rowspan="8" colspan="2">$$input:UserAvatarID$$</td>
            <td colspan="2">
            <div style="font-size: 20px; font-weight: bold;">$$input:UserName$$</div>
            </td>
        </tr>
        <tr>
            <td><strong>$$label:UserBadgeID$$</strong></td>
            <td>$$input:UserBadgeID$$</td>
        </tr>
        <tr>
            <td><strong>$$label:FullName$$</strong></td>
            <td>$$input:FullName$$</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><strong>$$label:Email$$</strong></td>
            <td>$$input:Email$$</td>
        </tr>
        <tr>
            <td><strong>$$label:UserCreated$$</strong></td>
            <td>$$input:UserCreated$$</td>
        </tr>
        <tr>
            <td><strong>$$label:UserGender$$</strong></td>
            <td>$$input:UserGender$$</td>
        </tr>
        <tr>
            <td><strong>$$label:UserDateOfBirth$$</strong></td>
            <td>$$input:UserDateOfBirth$$</td>
        </tr>
        <tr>
            <td><strong>$$label:UserForumPosts$$</strong></td>
            <td>$$input:UserForumPosts$$</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><strong>$$label:UserMessageBoardPosts$$</strong></td>
            <td>$$input:UserMessageBoardPosts$$</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><strong>$$label:UserBlogPosts$$</strong></td>
            <td>$$input:UserBlogPosts$$</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><strong>$$label:UserBlogComments$$</strong></td>
            <td>$$input:UserBlogComments$$</td>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td><strong>$$label:UserActivityPoints$$</strong></td>
            <td>$$input:UserActivityPoints$$</td>
            <td colspan="2">&nbsp;</td>
        </tr>
    </tbody>
</table>', 1768, N'<form><field column="UserID" visible="false" /><field column="UserName" fieldcaption="Name" fieldtype="usercontrol" ismacro="false"><settings><controlname>viewsecuretext</controlname></settings></field><field column="FirstName" visible="false" fieldtype="label" /><field column="MiddleName" visible="false" fieldtype="label" /><field column="LastName" visible="false" fieldtype="label" /><field column="FullName" fieldcaption="Full name" fieldtype="usercontrol" ismacro="false"><settings><controlname>viewsecuretext</controlname></settings></field><field column="Email" fieldtype="label" visibility="authenticated" /><field column="UserPassword" visible="false" fieldtype="label" /><field column="PreferredCultureCode" visible="false" /><field column="PreferredUICultureCode" visible="false" /><field column="UserEnabled" visible="false" defaultvalue="false" /><field column="UserIsEditor" visible="false" defaultvalue="false" /><field column="UserIsGlobalAdministrator" visible="false" defaultvalue="false" /><field column="UserIsExternal" visible="false" defaultvalue="false" /><field column="UserPasswordFormat" visible="false" /><field column="UserCreated" fieldcaption="Created" fieldtype="usercontrol"><settings><controlname>viewdate</controlname></settings></field><field column="LastLogon" visible="false"><settings><editTime>false</editTime></settings></field><field column="UserStartingAliasPath" visible="false" /><field column="UserGUID" visible="false" /><field column="UserLastModified" visible="false"><settings><editTime>false</editTime></settings></field><field column="UserLastLogonInfo" visible="false" /><field column="UserIsHidden" visible="false" defaultvalue="false" /><field column="UserVisibility" visible="false" fieldtype="label" /><field column="UserSettingsID" visible="false" /><field column="UserNickName" fieldtype="label" /><field column="UserPicture" visible="false" fieldtype="label" /><field column="UserSignature" visible="false" fieldtype="label" /><field column="UserURLReferrer" visible="false" fieldtype="label" /><field column="UserCampaign" visible="false" fieldtype="label" /><field column="UserMessagingNotificationEmail" visible="false" fieldtype="label" /><field column="UserCustomData" visible="false" fieldtype="label" /><field column="UserRegistrationInfo" visible="false" fieldtype="label" /><field column="UserPreferences" visible="false" fieldtype="label" /><field column="UserActivationDate" visible="false" fieldtype="usercontrol"><settings><controlname>viewdate</controlname></settings></field><field column="UserActivatedByUserID" visible="false" fieldtype="label" /><field column="UserTimeZoneID" visible="false" fieldtype="label" /><field column="UserAvatarID" fieldtype="usercontrol"><settings><controlname>viewuseravatar</controlname></settings></field><field column="UserBadgeID" fieldcaption="Badge" fieldtype="usercontrol"><settings><controlname>viewbadgeimage</controlname></settings></field><field column="UserShowSplashScreen" visible="false" fieldtype="label" defaultvalue="false" /><field column="UserActivityPoints" fieldcaption="Community points" fieldtype="label" /><field column="UserForumPosts" fieldcaption="Forum posts" fieldtype="usercontrol"><settings><controlname>viewintegernumber</controlname></settings></field><field column="UserBlogComments" fieldcaption="Blog comments" fieldtype="usercontrol"><settings><controlname>viewintegernumber</controlname></settings></field><field column="UserGender" fieldcaption="Gender" fieldtype="usercontrol"><settings><controlname>viewusergender</controlname></settings></field><field column="UserDateOfBirth" fieldcaption="Date of birth" fieldtype="usercontrol"><settings><controlname>viewdate</controlname></settings></field><field column="UserMessageBoardPosts" fieldcaption="Message board posts" fieldtype="usercontrol"><settings><controlname>viewintegernumber</controlname></settings></field><field column="UserSettingsUserGUID" visible="false" /><field column="UserSettingsUserID" visible="false" fieldtype="label" /><field column="UserBlogPosts" fieldcaption="Blog posts" fieldtype="usercontrol"><settings><controlname>viewintegernumber</controlname></settings></field></form>', N'DisplayProfile')
INSERT INTO [CMS_AlternativeForm] ([FormLastModified], [FormDisplayName], [FormClassID], [FormID], [FormGUID], [FormLayout], [FormCoupledClassID], [FormDefinition], [FormName]) VALUES ('20081210 14:50:06', N'Display profile', 1748, 126, '1cd04c91-eee1-49c3-b7a8-385a5f521ea5', N'<table>
    <tbody>
        <tr>
            <td rowspan="4">$$input:GroupAvatarID$$</td>
            <td style="vertical-align: top;" colspan="3">
            <div style="font-size: 20px; font-weight: bold;">$$input:GroupDisplayName$$</div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">$$input:GroupDescription$$</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><strong>$$label:GroupAccess$$</strong></td>
            <td style="width: 100%;">$$input:GroupAccess$$</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><strong>$$label:GroupCreatedWhen$$</strong></td>
            <td style="width: 100%;">$$input:GroupCreatedWhen$$</td>
        </tr>
    </tbody>
</table>', NULL, N'<form><field column="GroupID" visible="false" /><field column="GroupGUID" visible="false" /><field column="GroupLastModified" visible="false"><settings><editTime>false</editTime></settings></field><field column="GroupSiteID" visible="false" /><field column="GroupDisplayName" fieldcaption="Group display name" fieldtype="usercontrol"><settings><controlname>viewsecuretext</controlname></settings></field><field column="GroupName" visible="false" /><field column="GroupDescription" fieldcaption="Description" fieldtype="usercontrol"><settings><controlname>viewsecuretext</controlname></settings></field><field column="GroupNodeGUID" visible="false" /><field column="GroupApproveMembers" visible="false" /><field column="GroupAccess" fieldcaption="Access" fieldtype="usercontrol"><settings><controlname>viewgroupaccess</controlname></settings></field><field column="GroupCreatedByUserID" visible="false" /><field column="GroupEnabled" visible="false" defaultvalue="false" /><field column="GroupApprovedByUserID" visible="false" /><field column="GroupAvatarID" fieldcaption="Group avatar ID" fieldtype="usercontrol"><settings><controlname>viewgroupavatar</controlname></settings></field><field column="GroupApproved" visible="false" /><field column="GroupCreatedWhen" fieldcaption="Created" fieldtype="usercontrol"><settings><controlname>viewdate</controlname></settings></field></form>', N'DisplayProfile')
INSERT INTO [CMS_AlternativeForm] ([FormLastModified], [FormDisplayName], [FormClassID], [FormID], [FormGUID], [FormLayout], [FormCoupledClassID], [FormDefinition], [FormName]) VALUES ('20100305 15:30:49', N'Edit profile', 59, 128, 'ae8d424c-7eed-4b91-a555-f87c0df05597', N'', 1768, N'<form><field column="UserID" visible="false" /><field column="UserName" fieldcaption="Username" fieldtype="usercontrol" ismacro="false"><settings><controlname>viewsecuretext</controlname></settings></field><field column="FirstName" visible="false" /><field column="MiddleName" visible="false" /><field column="LastName" visible="false" /><field column="FullName" fieldcaption="Full name" allowempty="false" ismacro="false" /><field column="Email" fieldtype="usercontrol" visibility="authenticated"><settings><controlname>emailinput</controlname></settings></field><field column="UserPassword" visible="false" /><field column="PreferredCultureCode" visible="false" /><field column="PreferredUICultureCode" visible="false" /><field column="UserEnabled" visible="false" /><field column="UserIsEditor" visible="false" /><field column="UserIsGlobalAdministrator" visible="false" /><field column="UserIsExternal" visible="false" defaultvalue="false" /><field column="UserPasswordFormat" visible="false" /><field column="UserCreated" fieldcaption="User created" visible="false" fieldtype="usercontrol"><settings><controlname>viewdate</controlname></settings></field><field column="LastLogon" visible="false"><settings><editTime>false</editTime></settings></field><field column="UserStartingAliasPath" visible="false" /><field column="UserGUID" visible="false" /><field column="UserLastModified" visible="false"><settings><editTime>false</editTime></settings></field><field column="UserLastLogonInfo" visible="false" /><field column="UserIsHidden" visible="false" defaultvalue="false" /><field column="UserVisibility" visible="false" /><field column="UserIsDomain" visible="false" defaultvalue="false" /><field column="UserHasAllowedCultures" visible="false" defaultvalue="false" /><field column="UserSettingsID" visible="false" /><field column="UserNickName" fieldcaption="Nickname" /><field column="UserPicture" visible="false" /><field column="UserSignature" fieldcaption="Signature"><settings><cols>33</cols><rows>5</rows></settings></field><field column="UserURLReferrer" visible="false" /><field column="UserCampaign" visible="false" /><field column="UserMessagingNotificationEmail" fieldcaption="Messaging notification e-mail" fieldtype="usercontrol" allowusertochangevisibility="false"><settings><controlname>emailinput</controlname></settings></field><field column="UserCustomData" visible="false" /><field column="UserRegistrationInfo" visible="false" /><field column="UserPreferences" visible="false" /><field column="UserActivationDate" visible="false" /><field column="UserActivatedByUserID" visible="false" /><field column="UserTimeZoneID" fieldcaption="Time zone" fieldtype="usercontrol" fielddescription="Enables user to select his timezone."><settings><controlname>timezoneselector</controlname></settings></field><field column="UserAvatarID" fieldcaption="Avatar" fieldtype="usercontrol"><settings><controlname>useravatarselector</controlname></settings></field><field column="UserBadgeID" visible="false" /><field column="UserShowSplashScreen" visible="false" /><field column="UserActivityPoints" visible="false" /><field column="UserForumPosts" visible="false" /><field column="UserBlogComments" visible="false" /><field column="UserGender" fieldcaption="Gender" fieldtype="radiobuttons"><settings><repeatdirection>horizontal</repeatdirection><options><item value="1" text="{$General.Male$}" /><item value="2" text="{$General.Female$}" /></options></settings></field><field column="UserDateOfBirth" fieldcaption="Date of birth"><settings><displayNow>false</displayNow><editTime>false</editTime><timezonetype>inherit</timezonetype></settings></field><field column="UserMessageBoardPosts" visible="false" /><field column="UserSettingsUserGUID" visible="false" /><field column="UserSettingsUserID" visible="false" /><field column="UserBlogPosts" visible="false" /><field column="UserWaitingForApproval" visible="false" /><field column="UserDialogsConfiguration" visible="false" /><field column="UserDescription" visible="false" /></form>', N'EditProfile')
INSERT INTO [CMS_AlternativeForm] ([FormLastModified], [FormDisplayName], [FormClassID], [FormID], [FormGUID], [FormLayout], [FormCoupledClassID], [FormDefinition], [FormName]) VALUES ('20100305 15:30:39', N'Edit profile (MyDesk)', 59, 129, '8815b022-8e63-4f96-9710-a901e35e04d2', N'<table cellspacing="2" cellpadding="2">
    <tbody>
        <tr>
            <td>$$label:UserName$$</td>
            <td>$$input:UserName$$ $$validation:UserName$$</td>
        </tr>
        <tr>
            <td>$$label:FullName$$</td>
            <td>$$input:FullName$$ $$validation:FullName$$</td>
        </tr>
        <tr>
            <td>$$label:FirstName$$</td>
            <td>$$input:FirstName$$ $$validation:FirstName$$</td>
        </tr>
        <tr>
            <td>$$label:LastName$$</td>
            <td>$$input:LastName$$ $$validation:LastName$$</td>
        </tr>
        <tr>
            <td>$$label:UserNickName$$</td>
            <td>$$input:UserNickName$$ $$validation:UserNickName$$</td>
        </tr>
        <tr>
            <td>$$label:Email$$</td>
            <td>$$input:Email$$ $$validation:Email$$</td>
        </tr>
        <tr>
            <td>$$label:PreferredCultureCode$$</td>
            <td>$$input:PreferredCultureCode$$ $$validation:PreferredCultureCode$$</td>
        </tr>
        <tr>
            <td>$$label:PreferredUICultureCode$$</td>
            <td>$$input:PreferredUICultureCode$$ $$validation:PreferredUICultureCode$$</td>
        </tr>
        <tr>
            <td>$$label:UserMessagingNotificationEmail$$</td>
            <td>$$input:UserMessagingNotificationEmail$$ $$validation:UserMessagingNotificationEmail$$</td>
        </tr>
        <tr>
            <td>$$label:UserTimeZoneID$$</td>
            <td>$$input:UserTimeZoneID$$$$validation:UserTimeZoneID$$</td>
        </tr>
        <tr>
            <td>$$label:UserSignature$$</td>
            <td>$$input:UserSignature$$ $$validation:UserSignature$$</td>
        </tr>
        <tr>
            <td>$$label:UserGender$$</td>
            <td>$$input:UserGender$$ $$validation:UserGender$$</td>
        </tr>
        <tr>
            <td>$$label:UserDateOfBirth$$</td>
            <td>$$input:UserDateOfBirth$$ $$validation:UserDateOfBirth$$</td>
        </tr>
        <tr>
            <td>$$label:UserAvatarID$$</td>
            <td>$$input:UserAvatarID$$ $$validation:UserAvatarID$$</td>
        </tr>
        <tr>
            <td>$$label:UserShowSplashScreen$$</td>
            <td>$$input:UserShowSplashScreen$$ $$validation:UserShowSplashScreen$$</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>$$submitbutton$$</td>
        </tr>
    </tbody>
</table>', 1768, N'<form><field column="UserID" visible="false" /><field column="UserName" fieldcaption="{$general.username$}" fieldtype="usercontrol" ismacro="false"><settings><controlname>viewsecuretext</controlname></settings></field><field column="FirstName" fieldcaption="{$general.firstname$}" /><field column="MiddleName" visible="false" /><field column="LastName" fieldcaption="{$general.lastname$}" /><field column="FullName" fieldcaption="{$general.fullname$}" fieldtype="usercontrol" allowempty="false" ismacro="false"><settings><controlname>viewsecuretext</controlname></settings></field><field column="Email" fieldcaption="{$general.email$}" fieldtype="usercontrol"><settings><controlname>emailinput</controlname></settings></field><field column="UserPassword" visible="false" /><field column="PreferredCultureCode" fieldcaption="{$MyDesk.MyProfile.Culture$}" fieldtype="usercontrol"><settings><controlname>sitecultureselector</controlname></settings></field><field column="PreferredUICultureCode" fieldcaption="{$MyDesk.MyProfile.UICulture$}" fieldtype="usercontrol"><settings><controlname>uicultureselector</controlname></settings></field><field column="UserEnabled" visible="false" /><field column="UserIsEditor" visible="false" /><field column="UserIsGlobalAdministrator" visible="false" /><field column="UserIsExternal" visible="false" defaultvalue="false" /><field column="UserPasswordFormat" visible="false" /><field column="UserCreated" visible="false"><settings><editTime>false</editTime></settings></field><field column="LastLogon" visible="false"><settings><editTime>false</editTime></settings></field><field column="UserStartingAliasPath" visible="false" /><field column="UserGUID" visible="false" /><field column="UserLastModified" visible="false"><settings><editTime>false</editTime></settings></field><field column="UserLastLogonInfo" visible="false" /><field column="UserIsHidden" visible="false" defaultvalue="false" /><field column="UserSettingsID" visible="false" /><field column="UserNickName" fieldcaption="{$general.nickname$}" /><field column="UserPicture" visible="false" /><field column="UserSignature" fieldcaption="{$general.signature$}" controlcssclass="TextAreaField"><settings><size>200</size><cols>33</cols><rows>5</rows></settings></field><field column="UserURLReferrer" visible="false" /><field column="UserCampaign" visible="false" /><field column="UserMessagingNotificationEmail" fieldcaption="{$Messaging.NotificationEmail$}" fieldtype="usercontrol"><settings><controlname>emailinput</controlname></settings></field><field column="UserCustomData" visible="false" /><field column="UserRegistrationInfo" visible="false" /><field column="UserPreferences" visible="false" /><field column="UserActivationDate" visible="false" /><field column="UserActivatedByUserID" visible="false" /><field column="UserTimeZoneID" fieldcaption="{$general.timezone$}" fieldtype="usercontrol"><settings><controlname>timezoneselector</controlname></settings></field><field column="UserAvatarID" fieldcaption="{$general.avatar$}" fieldtype="usercontrol"><settings><controlname>useravatarselector</controlname></settings></field><field column="UserBadgeID" visible="false" /><field column="UserShowSplashScreen" fieldcaption="{$adm.user.lblusershowsplashscreen$}" /><field column="UserActivityPoints" visible="false" /><field column="UserForumPosts" visible="false" /><field column="UserBlogComments" visible="false" /><field column="UserGender" fieldcaption="{$general.gender$}" fieldtype="radiobuttons"><settings><repeatdirection>horizontal</repeatdirection><options><item value="1" text="{$General.Male$}" /><item value="2" text="{$General.Female$}" /></options></settings></field><field column="UserDateOfBirth" fieldcaption="{$general.DateOfBirth$}"><settings><editTime>false</editTime><timezonetype>inherit</timezonetype></settings></field><field column="UserMessageBoardPosts" visible="false" /><field column="UserSettingsUserGUID" visible="false" /><field column="UserSettingsUserID" visible="false" /><field column="UserBlogPosts" visible="false" /><field column="UserWaitingForApproval" visible="false" /></form>', N'EditProfileMyDesk')
INSERT INTO [CMS_AlternativeForm] ([FormLastModified], [FormDisplayName], [FormClassID], [FormID], [FormGUID], [FormLayout], [FormCoupledClassID], [FormDefinition], [FormName]) VALUES ('20100305 15:30:29', N'Edit profile (Community)', 59, 150, '52ab7093-1c5a-4af7-b085-b223f67ab909', N'<table>
    <tbody>
        <tr>
            <td>$$label:UserName$$</td>
            <td>$$input:UserName$$ $$validation:UserName$$</td>
        </tr>
        <tr>
            <td>$$label:FullName$$</td>
            <td>$$input:FullName$$ $$validation:FullName$$</td>
        </tr>
        <tr>
            <td>$$label:Email$$</td>
            <td>$$input:Email$$ $$validation:Email$$</td>
        </tr>
        <tr>
            <td><span class="EditingFormLabel">Display my e-mail to:</span></td>
            <td>$$visibility:Email$$</td>
        </tr>
        <tr>
            <td>$$label:UserNickName$$</td>
            <td>$$input:UserNickName$$ $$validation:UserNickName$$</td>
        </tr>
        <tr>
            <td>$$label:UserSignature$$</td>
            <td>$$input:UserSignature$$ $$validation:UserSignature$$</td>
        </tr>
        <tr>
            <td>$$label:UserMessagingNotificationEmail$$</td>
            <td>$$input:UserMessagingNotificationEmail$$ $$validation:UserMessagingNotificationEmail$$</td>
        </tr>
        <tr>
            <td>$$label:UserTimeZoneID$$</td>
            <td>$$input:UserTimeZoneID$$ $$validation:UserTimeZoneID$$</td>
        </tr>
        <tr>
            <td>$$label:UserAvatarID$$</td>
            <td>$$input:UserAvatarID$$ $$validation:UserAvatarID$$</td>
        </tr>
        <tr>
            <td>$$label:UserGender$$</td>
            <td>$$input:UserGender$$ $$validation:UserGender$$</td>
        </tr>
        <tr>
            <td>$$label:UserDateOfBirth$$</td>
            <td>$$input:UserDateOfBirth$$ $$validation:UserDateOfBirth$$</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>$$submitbutton$$</td>
        </tr>
    </tbody>
</table>', 1768, N'<form><field column="UserID" visible="false" /><field column="UserName" fieldcaption="Username" fieldtype="usercontrol" ismacro="false"><settings><controlname>viewsecuretext</controlname></settings></field><field column="FirstName" visible="false" /><field column="MiddleName" visible="false" /><field column="LastName" visible="false" /><field column="FullName" fieldcaption="Full name" allowempty="false" ismacro="false" /><field column="Email" fieldtype="usercontrol" allowusertochangevisibility="true" visibilitycontrol="RadioButtonsHorizontalVisibilityControl" visibility="authenticated"><settings><controlname>emailinput</controlname></settings></field><field column="UserPassword" visible="false" /><field column="PreferredCultureCode" visible="false" /><field column="PreferredUICultureCode" visible="false" /><field column="UserEnabled" visible="false" /><field column="UserIsEditor" visible="false" /><field column="UserIsGlobalAdministrator" visible="false" /><field column="UserIsExternal" visible="false" defaultvalue="false" /><field column="UserPasswordFormat" visible="false" /><field column="UserCreated" visible="false"><settings><editTime>false</editTime><timezonetype>inherit</timezonetype></settings></field><field column="LastLogon" visible="false"><settings><editTime>false</editTime><timezonetype>inherit</timezonetype></settings></field><field column="UserStartingAliasPath" visible="false" /><field column="UserGUID" visible="false" /><field column="UserLastModified" visible="false"><settings><editTime>false</editTime><timezonetype>inherit</timezonetype></settings></field><field column="UserLastLogonInfo" visible="false" /><field column="UserIsHidden" visible="false" defaultvalue="false" /><field column="UserSettingsID" visible="false" /><field column="UserNickName" fieldcaption="Nickname" /><field column="UserPicture" visible="false" /><field column="UserSignature" fieldcaption="Signature" controlcssclass="SignatureField"><settings><cols>33</cols><rows>5</rows></settings></field><field column="UserURLReferrer" visible="false" /><field column="UserCampaign" visible="false" /><field column="UserMessagingNotificationEmail" fieldcaption="Messaging notification e-mail" fieldtype="usercontrol"><settings><controlname>emailinput</controlname></settings></field><field column="UserCustomData" visible="false" /><field column="UserRegistrationInfo" visible="false" /><field column="UserPreferences" visible="false" /><field column="UserActivationDate" visible="false"><settings><timezonetype>inherit</timezonetype></settings></field><field column="UserActivatedByUserID" visible="false" /><field column="UserTimeZoneID" fieldcaption="Time zone" fieldtype="usercontrol" fielddescription="Enables user to select his timezone."><settings><controlname>timezoneselector</controlname></settings></field><field column="UserAvatarID" fieldcaption="Avatar" fieldtype="usercontrol"><settings><controlname>useravatarselector</controlname></settings></field><field column="UserBadgeID" visible="false" /><field column="UserShowSplashScreen" visible="false" /><field column="UserActivityPoints" visible="false" /><field column="UserForumPosts" visible="false" /><field column="UserBlogComments" visible="false" /><field column="UserGender" fieldcaption="Gender" fieldtype="radiobuttons"><settings><repeatdirection>horizontal</repeatdirection><options><item value="1" text="{$General.Male$}" /><item value="2" text="{$General.Female$}" /></options></settings></field><field column="UserDateOfBirth" fieldcaption="Date of birth"><settings><editTime>false</editTime><timezonetype>inherit</timezonetype></settings></field><field column="UserMessageBoardPosts" visible="false" /><field column="UserSettingsUserGUID" visible="false" /><field column="UserSettingsUserID" visible="false" /><field column="UserBlogPosts" visible="false" /><field column="UserWaitingForApproval" visible="false" /></form>', N'EditProfileCommunity')
SET IDENTITY_INSERT [CMS_AlternativeForm] OFF
