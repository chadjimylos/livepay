using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.CMSImportExport;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_AbuseReport_AbuseReport_List : CMSAbuseReportPage
{
    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Set the page title
        CurrentMaster.Title.TitleText = ResHelper.GetString("abuse.reportabuse");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_AbuseReport/object.png");
        CurrentMaster.Title.HelpTopicName = "abusereport_list";

        // Load unigrid
        UniGrid.OnAction += UniGrid_OnAction;
        UniGrid.OnExternalDataBound += UniGrid_OnExternalDataBound;

        // Create WHERE condition with ReportStatus
        string completeWhere = "";
        if (!String.IsNullOrEmpty(drpStatus.SelectedValue) && (drpStatus.SelectedValue != "-1"))
        {
            completeWhere = SqlHelperClass.AddWhereCondition(completeWhere, "(ReportStatus = " + drpStatus.SelectedValue.Replace("'", "''") + ")");
        }

        // Create WHERE condition with ReportTitle
        string txt = txtTitle.Text.Trim().Replace("'", "''");
        if (!string.IsNullOrEmpty(txt))
        {
            completeWhere = SqlHelperClass.AddWhereCondition(completeWhere, "(ReportTitle LIKE '%" + txt + "%')");
        }

        // Create WHERE condition with ReportObjectType
        int siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
        if (siteId == 0)
        {
            siteId = CMSContext.CurrentSiteID;
        }
        if (siteId > 0)
        {
            completeWhere = SqlHelperClass.AddWhereCondition(completeWhere, "(ReportSiteID = '" + siteId + "')");
        }
        UniGrid.WhereCondition = completeWhere;

        siteSelector.DropDownSingleSelect.AutoPostBack = false;

        btnShow.Click += btnShow_Click;

        // Load dropdown list
        if (!RequestHelper.IsPostBack())
        {
            InitializeComponents();
            siteSelector.Value = CMSContext.CurrentSiteID;
        }


        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }

    #endregion


    #region "Other events"

    protected void btnShow_Click(object sender, EventArgs e)
    {
    }

    #endregion


    #region "Other methods"

    /// <summary>
    /// Loads status from enumeration to dropdown list.
    /// </summary>
    private void InitializeComponents()
    {
        drpStatus.Items.Add(new ListItem(ResHelper.GetString("general.all"), "-1"));
        drpStatus.Items.Add(new ListItem(ResHelper.GetString("general.new"), "0"));
        drpStatus.Items.Add(new ListItem(ResHelper.GetString("general.solved"), "1"));
        drpStatus.Items.Add(new ListItem(ResHelper.GetString("general.rejected"), "2"));

        // Show site selector only for global admin
        if (CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            // Set site selector
            siteSelector.AllowAll = true;
        }
        else
        {
            plcSites.Visible = false;
        }
    }

    #endregion


    #region "Grid events"

    /// <summary>
    /// Unigrid external databound handler.
    /// </summary>
    protected object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            // Display link instead of title
            case "title":
                if (parameter != DBNull.Value)
                {
                    DataRowView row = (DataRowView)parameter;
                    string url = ValidationHelper.GetString(row["ReportURL"], "");
                    string title = HTMLHelper.HTMLEncode(ValidationHelper.GetString(row["ReportTitle"], ""));

                    HyperLink link = new HyperLink();
                    string culture = ValidationHelper.GetString(row["ReportCulture"], "");
                    if(culture != String.Empty)
                    {
                        url = UrlHelper.AddParameterToUrl(url, "lang", culture);
                    }
                    link.NavigateUrl = url;
                    link.Target = "_blank";
                    link.Text = title;
                    link.ToolTip = HTMLHelper.HTMLEncode(url);
                    link.Style.Add("cursor", "help");
                    return link;
                }
                return sourceName;

            // Insert status label
            case "status":
                switch (parameter.ToString().ToLower())
                {
                    default:
                        return ResHelper.GetString("general.new");

                    case "1":
                        return "<span class=\"AbuseSolved\">" + ResHelper.GetString("general.solved") + "</span>";

                    case "2":
                        return "<span class=\"AbuseRejected\">" + ResHelper.GetString("general.rejected") + "</span>";
                }

            case "objecttype":
                string objectType = ImportExportHelper.GetSafeObjectTypeName(parameter.ToString());
                if (!string.IsNullOrEmpty(objectType))
                {
                    parameter = ResHelper.GetString("ObjectType." + objectType);
                }
                else
                {
                    return "-";
                }
                break;

            case "comment":
                string resultText = parameter.ToString();//.Replace("\n", "<br />");
                parameter = HTMLHelper.HTMLEncode(TextHelper.LimitLength(resultText, 297, "..."));
                break;
        }

        return parameter.ToString();
    }


    /// <summary>
    /// Unigrid event handler.
    /// </summary>
    /// <param name="actionName">Name of action</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row</param>
    protected void UniGrid_OnAction(string actionName, object actionArgument)
    {
        // Edit report
        if (actionName == "edit")
        {
            UrlHelper.Redirect("AbuseReport_Edit.aspx?reportid=" + actionArgument);
        }
        // Delete report
        else if (actionName == "delete")
        {
            // Check permissions
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.AbuseReport", "Manage"))
            {
                RedirectToAccessDenied("CMS.AbuseReport", "Manage");
            }

            AbuseReportInfoProvider.DeleteAbuseReportInfo(ValidationHelper.GetInteger(actionArgument, 0));
            btnShow_Click(null, null);
        }
        // Solve report
        else if (actionName == "solve")
        {
            // Check permissions
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.AbuseReport", "Manage"))
            {
                RedirectToAccessDenied("CMS.AbuseReport", "Manage");
            }

            AbuseReportInfo ari = AbuseReportInfoProvider.GetAbuseReportInfo(ValidationHelper.GetInteger(actionArgument, 0));
            ari.ReportStatus = AbuseReportStatusEnum.Solved;
            AbuseReportInfoProvider.SetAbuseReportInfo(ari);
            btnShow_Click(null, null);
        }
        // Reject report
        else if (actionName == "reject")
        {
            // Check permissions
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.AbuseReport", "Manage"))
            {
                RedirectToAccessDenied("CMS.AbuseReport", "Manage");
            }

            AbuseReportInfo ari = AbuseReportInfoProvider.GetAbuseReportInfo(ValidationHelper.GetInteger(actionArgument, 0));
            ari.ReportStatus = AbuseReportStatusEnum.Rejected;
            AbuseReportInfoProvider.SetAbuseReportInfo(ari);
            btnShow_Click(null, null);
        }
    }

    #endregion
}
