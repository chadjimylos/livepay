<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewProduct.ascx.cs" Inherits="CMSModules_Ecommerce_Content_Product_NewProduct" %>
<%@ Register Src="~/CMSAdminControls/MetaFiles/File.ascx" TagName="File" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/Controls/UI/PriceSelector.ascx" TagName="PriceSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/FormControls/DepartmentSelector.ascx" TagName="DepartmentSelector"
    TagPrefix="cms" %>
<div style="padding-left: 9px;">
    <asp:Label ID="lblError" runat="server" EnableViewState="false" Visible="false" CssClass="ErrorLabel" />
</div>
<asp:PlaceHolder ID="plcSKUControls" runat="server">
    <table class="EditingFormTable">
        <asp:PlaceHolder ID="plcSKUName" runat="server">
            <tr>
                <td class="FieldLabel" style="padding-left: 35px;">
                    <asp:Label ID="lblSKUName" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtSKUName" runat="server" MaxLength="440" CssClass="Textboxfield"
                        EnableViewState="false" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcSKUPrice" runat="server">
            <tr>
                <td class="FieldLabel" style="padding-left: 35px;">
                    <asp:Label ID="lblSKUPrice" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <cms:PriceSelector ID="txtSKUPrice" runat="server" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcImagePath" runat="server" Visible="false">
            <tr>
                <td class="FieldLabel" style="padding-left: 35px;">
                    <asp:Label runat="server" ID="lblSKUImagePathSelect" EnableViewState="false" />
                </td>
                <td>
                    <cms:ImageSelector ID="imgSelect" runat="server" ImageHeight="50" ShowImagePreview="true"
                        ShowClearButton="true" UseImagePath="true" IsLiveSite="false" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcMetaFile" runat="server" Visible="false">
            <tr>
                <td class="FieldLabel" style="padding-left: 35px;">
                    <asp:Label runat="server" ID="lblSKUImagePath" EnableViewState="false" />
                </td>
                <td>
                    <cms:File ID="ucMetaFile" runat="server" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcSKUDescription" runat="server">
            <tr>
                <td class="FieldLabel" style="vertical-align: top; padding-top: 5px; padding-left: 35px;">
                    <cms:LocalizedLabel ID="lblSKUDescription" runat="server" EnableViewState="false"
                        ResourceString="general.description" DisplayColon="true" />
                </td>
                <td>
                    <cms:CMSHtmlEditor ID="htmlSKUDescription" runat="server" Width="400px" Height="300px" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <tr>
            <td class="FieldLabel" style="padding-left: 35px;">
                <asp:Label ID="lblSKUDepartment" runat="server" EnableViewState="false" />
            </td>
            <td>
                <cms:DepartmentSelector runat="server" ID="departmentElem" AddAllItemsRecord="false"
                    AddAllMyRecord="false" AddNoneRecord="false" UseDepartmentNameForSelection="false"
                    IsLiveSite="false" />
            </td>
        </tr>
    </table>
</asp:PlaceHolder>
