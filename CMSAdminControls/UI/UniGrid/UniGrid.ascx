<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UniGrid.ascx.cs" Inherits="CMSAdminControls_UI_UniGrid_UniGrid" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/Controls/UniGridPager.ascx" TagName="UniGridPager"
    TagPrefix="cms" %>
<div id="<%= this.ClientID %>" class="UniGridBody">
    <asp:Panel ID="pnlHeader" runat="server" CssClass="UniGridHeader">
        <asp:PlaceHolder ID="filter" runat="server" />
        <asp:Label ID="lblInfo" runat="server" EnableViewState="false" />
        <asp:Label ID="lblError" runat="server" EnableViewState="false" CssClass="ErrorLabel"
            Visible="false" />
    </asp:Panel>
    <asp:Panel ID="pnlContent" runat="server" CssClass="UniGridContent">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:GridView ID="UniGridView" runat="server" AllowPaging="True" PageSize="25" AutoGenerateColumns="False"
                        AllowSorting="true" OnSorting="UniGridView_Sorting" OnRowCommand="UniGridView_RowCommand"
                        OnRowDeleting="UniGridView_RowDeleting" OnRowCreated="UniGridView_RowCreating"
                        OnRowEditing="UniGridView_RowEditing" OnRowUpdating="UniGridView_RowUpdating"
                        CellPadding="3" CssClass="UniGridGrid" EnableViewState="false" OnDataBound="UniGridView_DataBound"
                        GridLines="Horizontal">
                        <HeaderStyle HorizontalAlign="Left" CssClass="UniGridHead" />
                    </asp:GridView>
                    <cms:UniGridPager ID="pagerElem" runat="server" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hidSelection" runat="server" />
        <asp:HiddenField ID="hidNewSelection" runat="server" />
        <asp:HiddenField ID="hidDeSelection" runat="server" />
        <asp:HiddenField ID="hidCmdName" runat="server" />
        <asp:HiddenField ID="hidCmdArg" runat="server" />
        <asp:HiddenField ID="hidActionsHash" runat="server" />
    </asp:Panel>
</div>
