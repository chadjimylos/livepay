<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="CMSDesk_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>CMS Desk</title>
    
    <script type="text/javascript">
        //<![CDATA[
        document.titlePart = 'CMS Desk';
        //]]>
    </script>
</head>
<frameset border="0" rows="49,*" id="rowsFrameset"> 
	<frame name="cmsheader" src="<%= headerPageURL %><%=Request.Url.Query%>" scrolling="no" noresize="noresize" frameborder="0" />
	<frame name="cmsdesktop" src="<%=desktoppage%>" scrolling="no" noresize="noresize" frameborder="0" />
    <noframes>
        <p id="p1">
            This HTML frameset displays multiple Web pages. To view this frameset, use a Web
            browser that supports HTML 4.0 and later.
        </p>
    </noframes>
</frameset>
</html>
