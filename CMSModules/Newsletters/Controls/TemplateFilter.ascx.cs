﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.Controls;
using CMS.CMSHelper;

public partial class CMSModules_Newsletters_Controls_TemplateFilter : CMSAbstractBaseFilterControl
{
    protected int siteId = 0;

    /// <summary>
    /// Where condition.
    /// </summary>
    public override string WhereCondition
    {
        get
        {
            base.WhereCondition = GenerateWhereCondition();
            return base.WhereCondition;
        }
        set
        {
            base.WhereCondition = value;
        }
    }


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        siteId = CMSContext.CurrentSite.SiteID;
        if (!RequestHelper.IsPostBack())
        {
            this.Reload();
        }
    }


    /// <summary>
    /// Reloads control.
    /// </summary>
    protected void Reload()
    {
        filter.Items.Clear();
        filter.Items.Add(new ListItem(ResHelper.GetString("general.all"), "-1"));
        filter.Items.Add(new ListItem(ResHelper.GetString("NewsletterTemplate_List.Issue"), "I"));
        filter.Items.Add(new ListItem(ResHelper.GetString("NewsletterTemplate_List.subscription"), "S"));
        filter.Items.Add(new ListItem(ResHelper.GetString("NewsletterTemplate_List.Unsubscription"), "U"));
    }


    /// <summary>
    /// Generates WHERE condition.
    /// </summary>
    private string GenerateWhereCondition()
    {
        switch (filter.SelectedValue)
        {
            // All priorities
            default:
            case "-1":
                break;

            // Low priority
            case "I":
                return "TemplateType = 'I'";

            // Normal priority
            case "S":
                return "TemplateType = 'S'";

            // High priority
            case "U":
                return "TemplateType = 'U'";
        }
        return null;
    }

    #endregion
}
