using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_Category : SiteManagerPage
{
    protected int templateCategoryId = 0;
    protected int parentCategoryId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblCategoryDisplayName.Text = ResHelper.GetString("Development-PageTemplate_Category.DisplayName");
        lblCategoryName.Text = ResHelper.GetString("Development-PageTemplate_Category.Name");
        btnOk.Text = ResHelper.GetString("General.OK");
        rfvCategoryDisplayName.ErrorMessage = ResHelper.GetString("General.RequiresDisplayName");
        rfvCategoryName.ErrorMessage = ResHelper.GetString("General.RequiresCodeName");

        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Development-PageTemplate_Category.TitleNew");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_PageTemplateCategory/new.png");
        this.CurrentMaster.Title.HelpTopicName = "new_category";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        string templateCategoryDisplayName = "";
        string templateCategoryName = "";
        string categoryImagePath = "";

        templateCategoryId = QueryHelper.GetInteger("categoryid", 0);

        if (templateCategoryId > 0)
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("Development-PageTemplate_Category.Title");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_PageTemplateCategory/object.png");
            PageTemplateCategoryInfo ptci = PageTemplateCategoryInfoProvider.GetPageTemplateCategoryInfo(templateCategoryId);
            if (ptci != null)
            {
                templateCategoryDisplayName = ptci.DisplayName;
                templateCategoryName = ptci.CategoryName;
                categoryImagePath = ptci.CategoryImagePath;
                parentCategoryId = ptci.ParentId;

                // If it's root category hide category name textbox
                if ((parentCategoryId == 0) || (ptci.CategoryName =="AdHoc"))
                {
                    plcCategoryName.Visible = false;
                }
            }
        }

        parentCategoryId = QueryHelper.GetInteger("parentcategoryid", 0);


        if (!RequestHelper.IsPostBack())
        {
            txtCategoryDisplayName.Text = templateCategoryDisplayName;
            txtCategoryName.Text = templateCategoryName;
            txtCategoryImagePath.Text = categoryImagePath;
        }
    }


    /// <summary>
    /// Saves data of edited workflow scope from TextBoxes into DB.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Finds whether required fields are not empty

        string displayName = txtCategoryDisplayName.Text.Trim();
        string codeName = txtCategoryName.Text.Trim();
        string imagePath = txtCategoryImagePath.Text.Trim();

        string result = new Validator().NotEmpty(displayName, ResHelper.GetString("General.RequiresDisplayName")).NotEmpty(codeName, ResHelper.GetString("General.RequiresCodeName")).Result;

        // Validate the codename
        if (parentCategoryId != 0)
        {
            if (!ValidationHelper.IsCodeName(codeName))
            {
                result = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
            }
        }

        // Check codename uniqness
        if ((templateCategoryId == 0) && (PageTemplateCategoryInfoProvider.GetPageTemplateCategoryInfoByCodeName(codeName) != null))
        {
            result = ResHelper.GetString("General.CodeNameExists");
        }

        if (result == "")
        {
            if (templateCategoryId > 0)
            {
                PageTemplateCategoryInfo ptci = PageTemplateCategoryInfoProvider.GetPageTemplateCategoryInfo(templateCategoryId);
                if (ptci != null)
                {
                    ptci.DisplayName = displayName;
                    ptci.CategoryName = codeName;
                    ptci.CategoryImagePath = imagePath;
                    PageTemplateCategoryInfoProvider.SetPageTemplateCategoryInfo(ptci);
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    // Reload tree
                    ltlScript.Text = "<script type=\"text/javascript\">";
                    ltlScript.Text += "parent.frames['pt_tree'].location.href = 'PageTemplate_Tree.aspx?categoryid=" + templateCategoryId + "';";
                    ltlScript.Text += "</script>";
                }
            }
            else
            {
                PageTemplateCategoryInfo ptci = new PageTemplateCategoryInfo();
                ptci.DisplayName = displayName;
                ptci.CategoryName = codeName;
                ptci.CategoryImagePath = imagePath;
                ptci.ParentId = parentCategoryId;
                try
                {
                    PageTemplateCategoryInfoProvider.SetPageTemplateCategoryInfo(ptci);
                    ltlScript.Text = "<script type=\"text/javascript\">";
                    ltlScript.Text += "parent.frames['pt_tree'].location.href = 'PageTemplate_Tree.aspx?categoryid=" + ptci.CategoryId + "';";
                    ltlScript.Text += "this.location.href = 'PageTemplate_Category.aspx?categoryid=" + ptci.CategoryId + "';";
                    ltlScript.Text += "</script>";
                }
                catch (Exception ex)
                {
                    lblError.Text = ex.Message;
                    lblError.Visible = true;
                }
                //UrlHelper.Redirect("PageTemplate_Category.aspx?categoryid=" + ptci.CategoryId);
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }
}
