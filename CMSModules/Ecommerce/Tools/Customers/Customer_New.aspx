<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    CodeFile="Customer_New.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Customers_Customer_New"
    Theme="Default" Title="Customer - New" %>

<%@ Register Src="~/CMSFormControls/CountrySelector.ascx" TagName="CountrySelector"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel" style="padding-top: 10px; font-weight: bold;">
                <asp:Label runat="server" ID="lblCustomerData" EnableViewState="false" />
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerFirstName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCustomerFirstName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerLastName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCustomerLastName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerCompany" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCustomerCompany" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top; padding-top: 6px">
                <asp:Label runat="server" ID="lblCountry" EnableViewState="false" />
            </td>
            <td>
                <cms:CountrySelector ID="drpCountry" runat="server" UseCodeNameForSelection="false"
                    AddSelectCountryRecord="false" IsLiveSite="false" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblCustomerEnabled" EnableViewState="false"
                    ResourceString="general.enabled" DisplayColon="true" />
            </td>
            <td>
                <asp:CheckBox ID="chkCustomerEnabled" runat="server" CssClass="CheckBoxMovedLeft"
                    Checked="true" EnableViewState="false" />
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="padding-top: 10px; font-weight: bold;">
                <asp:Label runat="server" ID="lblUserData" EnableViewState="false" />
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblCreateLogin" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox ID="chkCreateLogin" runat="server" CssClass="CheckBoxMovedLeft" EnableViewState="false" />
            </td>
            <td>
            </td>
        </tr>
        <tr id="loginLine1">
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblUserName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtUserName" runat="server" CssClass="TextBoxField" MaxLength="100"
                    EnableViewState="false" />
            </td>
            <td>
                <asp:RequiredFieldValidator ID="rqvUserName" runat="server" ControlToValidate="txtUserName"
                    ValidationGroup="Login" EnableViewState="false" />
            </td>
        </tr>
        <tr id="loginLine2">
            <td class="FieldLabel" style="vertical-align: top;">
                <asp:Label runat="server" ID="lblPassword1" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtPassword1" runat="server" CssClass="TextBoxField" TextMode="Password"
                    MaxLength="100" EnableViewState="false" />
            </td>
            <td style="vertical-align: top;">
                <asp:RequiredFieldValidator ID="rqvPassword1" runat="server" ControlToValidate="txtPassword1"
                    ValidationGroup="Login" EnableViewState="false" />
            </td>
        </tr>
        <tr id="loginLine3">
            <td class="FieldLabel" style="vertical-align: top;">
                <asp:Label runat="server" ID="lblPassword2" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtPassword2" runat="server" CssClass="TextBoxField" TextMode="Password"
                    MaxLength="100" EnableViewState="false" />
            </td>
            <td style="vertical-align: top;">
                <asp:RequiredFieldValidator ID="rqvPassword2" runat="server" ControlToValidate="txtPassword2"
                    ValidationGroup="Login" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
</asp:Content>
