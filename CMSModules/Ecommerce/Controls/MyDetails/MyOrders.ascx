<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MyOrders.ascx.cs" Inherits="CMSModules_Ecommerce_Controls_MyDetails_MyOrders" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<div class="MyOrders">
    <cms:UniGrid runat="server" ID="gridOrders" GridName="~/CMSModules/Ecommerce/Controls/MyDetails/MyOrders.xml"
        OrderBy="OrderDate DESC" Columns="OrderID,OrderDate,CurrencyFormatString,OrderTotalPrice,StatusDisplayName,OrderTrackingNumber" />
</div>
