<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Country_Edit_State_List.aspx.cs"
    Inherits="CMSSiteManager_Development_Countries_Country_Edit_State_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Country edit - State list" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="gridStates" GridName="Country_Edit_State_List.xml"
        OrderBy="StateDisplayName" Columns="StateID, StateDisplayName" IsLiveSite="false" />
</asp:Content>
