<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fck_InsertPolls.aspx.cs"
    Inherits="CMSModules_Polls_Controls_FCKEditor_fck_InsertPolls"
    Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Insert Polls</title>

    <script type="text/javascript">
        //<![CDATA[
        var oEditor = window.parent.InnerDialogLoaded();
        function insertPolls(charValue) {
            oEditor.FCK.InsertHtml(charValue || "");
            window.parent.Cancel();
        }
        //]]>
    </script>

</head>
<body style="background-color: White;" runat="server" ID="bodyElem">
    <form id="form1" runat="server">
        <cms:UniGrid runat="server" ID="uniGrid" GridName="~/CMSModules/Polls/Controls/FCKEditor/InsertPolls.xml"
            OrderBy="PollDisplayName" IsLiveSite="true" />
    </form>
</body>
</html>
