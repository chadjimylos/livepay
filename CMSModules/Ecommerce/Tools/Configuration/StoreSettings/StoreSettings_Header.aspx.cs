using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_StoreSettings_StoreSettings_Header : CMSEcommerceConfigurationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initializes page title
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Store_Settings.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Ecommerce/storesettings.png");
        this.CurrentMaster.Title.HelpTopicName = "genral_tab";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Initialize tabs
        this.CurrentMaster.Tabs.Tabs = new string[3, 4];
        this.CurrentMaster.Tabs.Tabs[0, 0] = ResHelper.GetString("General.General");
        this.CurrentMaster.Tabs.Tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'genral_tab');";
        this.CurrentMaster.Tabs.Tabs[0, 2] = "StoreSettings_General.aspx";
        this.CurrentMaster.Tabs.Tabs[1, 0] = ResHelper.GetString("StoreSettings.Emails");
        this.CurrentMaster.Tabs.Tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'emails_tab');";
        this.CurrentMaster.Tabs.Tabs[1, 2] = "StoreSettings_Emails.aspx";
        this.CurrentMaster.Tabs.Tabs[2, 0] = ResHelper.GetString("StoreSettings.CheckoutProcess");
        this.CurrentMaster.Tabs.Tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'checkou_process');";
        this.CurrentMaster.Tabs.Tabs[2, 2] = "StoreSettings_CheckoutProcess.aspx";
        this.CurrentMaster.Tabs.UrlTarget = "storesettingsContent";
    }
}
