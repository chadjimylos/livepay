using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;

public partial class CMSFormControls_TaxID : FormEngineUserControl
{
    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;

            this.TaxID.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (IsEmpty())
            {
                return "";
            }

            return TaxID.Text;
        }
        set
        {
            string number = (string)value;
            Clear();

            // Parse numbers from incoming string.
            if ((number != null) && (number != ""))
            {
                try
                {
                    TaxID.Text = number.Substring(1, number.IndexOf(")") - 1);
                }
                catch
                {
                }
            }
        }
    }


    /// <summary>
    /// Clears current value
    /// </summary>
    public void Clear()
    {
        TaxID.Text = "";
    }


    /// <summary>
    /// Returns true if the number is empty
    /// </summary>
    public bool IsEmpty()
    {
        return (DataHelper.IsEmpty(TaxID.Text));
    }


    /// <summary>
    /// Returns true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        if (IsEmpty())
        {
            return true;
        }

 
        //if (TaxID.ValidationGroup. != "")
        //{
        //    this.ValidationError = ResHelper.GetString("TaxID.ValidationError");
        //    return false;
        //}

        Validator val = new Validator();
        //string result = val.IsRegularExp(txt1st.Text, @"\d{3}", "error").IsRegularExp(txt2nd.Text, @"\d{3}", "error").IsRegularExp(txt3rd.Text, @"\d{4}", "error").Result;
        string result = val.IsRegularExp(TaxID.Text, @"\d{9}", "error").Result;

        if (result != "")
        {
            this.ValidationError = ResHelper.LocalizeString("{$=�������� ����������� �� �����|en-us=�������� ����������� �� ����� en$}");
            return false;
        }

		int telestis = 2;
        int sum = 0;
        string afm = TaxID.Text;
        for (int i = 7; i >=0; i--)
		{
            sum += int.Parse(afm[i].ToString()) * telestis;
            telestis = telestis * 2;
		}
        int checkDigit = int.Parse(afm[8].ToString());
        if (sum % 11 % 10 != checkDigit){
            this.ValidationError = ResHelper.LocalizeString("{$=�� ��� ��� ����� ������� ��� ����� ������|en-us=�� ��� ��� ����� ������� ��� ����� ������ en$}");
            return false;
        }


        return true;
    }
}
