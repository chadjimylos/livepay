using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.FormControls;
using CMS.UIControls;
using CMS.Ecommerce;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_CustomFields : CMSEcommercePage
{
    protected int orderId = 0;


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // set edit mode
        orderId = ValidationHelper.GetInteger(Request.QueryString["orderId"], 0);
        if (orderId > 0)
        {
            formOrderCustomFields.Info = OrderInfoProvider.GetOrderInfo(orderId);
            formOrderCustomFields.OnBeforeSave += new DataForm.OnBeforeSaveEventHandler(formOrderCustomFields_OnBeforeSave);
            formOrderCustomFields.OnAfterSave += new DataForm.OnAfterSaveEventHandler(formOrderCustomFields_OnAfterSave);
        }
        else
        {
            formOrderCustomFields.Enabled = false;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (formOrderCustomFields.BasicForm != null)
        {
            if (formOrderCustomFields.BasicForm.FieldControls.Count <= 0)
            {
                // Hide submit button if no field is present
                formOrderCustomFields.BasicForm.SubmitButton.Visible = false;
            }
            else
            {
                // Set submit button's css class
                formOrderCustomFields.BasicForm.SubmitButton.CssClass = "ContentButton";
            }
        }
    }


    void formOrderCustomFields_OnBeforeSave()
    {
        // Check 'EcommerceModify' permission
        if ((CMSContext.CurrentSite != null) && (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify")))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }
    }


    void formOrderCustomFields_OnAfterSave()
    {
        // Display 'changes saved' information
        this.lblInfo.Visible = true;
        this.lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}
