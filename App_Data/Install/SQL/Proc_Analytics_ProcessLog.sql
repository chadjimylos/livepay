-- =============================================
-- Author:		Name
-- Create date: 25.7.2007
-- Description:	Stores log records to DB
-- =============================================
CREATE PROCEDURE [Proc_Analytics_ProcessLog]
	@SiteID int,
	@Codename nvarchar(50),
	@Culture nvarchar(10),
    @ObjectName nvarchar(450),
    @ObjectID int,
    @Hits int,
    @HourStart datetime,
	@HourEnd datetime,
    @DayStart datetime,
	@DayEnd datetime,
    @WeekStart datetime,
	@WeekEnd datetime,
    @MonthStart datetime,
	@MonthEnd datetime,
    @YearStart datetime,
	@YearEnd datetime
AS
BEGIN
	/* Declare the @statisticsID variable */
	DECLARE @statisticsID int;
	SET @statisticsID = 0;
    SELECT @StatisticsID = StatisticsID FROM Analytics_Statistics
	WHERE (StatisticsSiteID = @SiteID) AND (StatisticsCode = @Codename) AND ((StatisticsObjectName = @ObjectName) OR (StatisticsObjectName IS NULL AND @ObjectName IS NULL))
		AND	((StatisticsObjectID = @ObjectID) OR (StatisticsObjectID IS NULL OR @ObjectID IS NULL)) AND ((StatisticsObjectCulture = @Culture) OR (StatisticsObjectCulture IS NULL AND @Culture IS NULL));
	
	/* If @statisticsID is 0 insert new record */
	IF @statisticsID = 0
	BEGIN
		INSERT INTO Analytics_Statistics (StatisticsSiteID, StatisticsCode, StatisticsObjectName, StatisticsObjectID, StatisticsObjectCulture)
		VALUES (@SiteID, @Codename, @ObjectName, @ObjectID, @Culture);
		/* Get StatisticsID */
        SELECT @StatisticsID = StatisticsID FROM Analytics_Statistics
     	WHERE (StatisticsSiteID = @SiteID) AND (StatisticsCode = @Codename) AND ((StatisticsObjectName = @ObjectName) OR (StatisticsObjectName IS NULL AND @ObjectName IS NULL))
	  		AND	((StatisticsObjectID = @ObjectID) OR (StatisticsObjectID IS NULL OR @ObjectID IS NULL)) AND ((StatisticsObjectCulture = @Culture) OR (StatisticsObjectCulture IS NULL AND @Culture IS NULL));
	END
	/* Declare @hitsID and @hitsCount variables */
	DECLARE @hitsID int, @hitsCount int;
	SET @hitsCount = 0;
	SET @hitsID = 0;
	/* HOURS */	
	SELECT @hitsID = HitsID, @hitsCount = HitsCount FROM [Analytics_HourHits]
	WHERE HitsStatisticsID=@statisticsID AND HitsStartTime=@HourStart AND HitsEndTime=@HourEnd;
	IF @hitsID > 0
		UPDATE [Analytics_HourHits] SET HitsCount=(@hitsCount+@Hits) WHERE HitsID=@hitsID;
	ELSE
		INSERT INTO [Analytics_HourHits] ([HitsStatisticsID],[HitsStartTime],[HitsEndTime],[HitsCount])
		VALUES (@statisticsID,@HourStart,@HourEnd,@Hits);
	/* DAYS */
	SET @hitsID = 0;
	SELECT @hitsID = HitsID, @hitsCount = HitsCount FROM [Analytics_DayHits]
	WHERE HitsStatisticsID=@statisticsID AND HitsStartTime=@DayStart AND HitsEndTime=@DayEnd;
	IF @hitsID > 0
		UPDATE [Analytics_DayHits] SET HitsCount=(@hitsCount+@Hits) WHERE HitsID=@hitsID;
	ELSE
		INSERT INTO [Analytics_DayHits] ([HitsStatisticsID],[HitsStartTime],[HitsEndTime],[HitsCount])
		VALUES (@statisticsID,@DayStart,@DayEnd,@Hits);
	/* WEEKS */
	SET @hitsID = 0;
	SELECT @hitsID = HitsID, @hitsCount = HitsCount FROM [Analytics_WeekHits]
	WHERE HitsStatisticsID=@statisticsID AND HitsStartTime=@WeekStart AND HitsEndTime=@WeekEnd;
	IF @hitsID > 0
		UPDATE [Analytics_WeekHits] SET HitsCount=(@hitsCount+@Hits) WHERE HitsID=@hitsID;
	ELSE
		INSERT INTO [Analytics_WeekHits] ([HitsStatisticsID],[HitsStartTime],[HitsEndTime],[HitsCount])
		VALUES (@statisticsID,@WeekStart,@WeekEnd,@Hits);
	/* MONTHS */
	SET @hitsID = 0;
	SELECT @hitsID = HitsID, @hitsCount = HitsCount FROM [Analytics_MonthHits]
	WHERE HitsStatisticsID=@statisticsID AND HitsStartTime=@MonthStart AND HitsEndTime=@MonthEnd;
	IF @hitsID > 0
		UPDATE [Analytics_MonthHits] SET HitsCount=(@hitsCount+@Hits) WHERE HitsID=@hitsID;
	ELSE
		INSERT INTO [Analytics_MonthHits] ([HitsStatisticsID],[HitsStartTime],[HitsEndTime],[HitsCount])
		VALUES (@statisticsID,@MonthStart,@MonthEnd,@Hits);
	
	/* YEARS */
	SET @hitsID = 0;
	SELECT @hitsID = HitsID, @hitsCount = HitsCount FROM [Analytics_YearHits]
	WHERE HitsStatisticsID=@statisticsID AND HitsStartTime=@YearStart AND HitsEndTime=@YearEnd;
	IF @hitsID > 0
		UPDATE [Analytics_YearHits] SET HitsCount=(@hitsCount+@Hits) WHERE HitsID=@hitsID;
	ELSE
		INSERT INTO [Analytics_YearHits] ([HitsStatisticsID],[HitsStartTime],[HitsEndTime],[HitsCount])
		VALUES (@statisticsID,@YearStart,@YearEnd,@Hits);
END
