using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;
using CMS.CMSHelper;
using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSFormControls_Date : LivePayFormEngineUserControl
{
    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.Date.Enabled = value;
        }
    }

    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            //return this.Date.Text.Trim();

            DateTime mDate;
            if (DateTime.TryParse(this.Date.Text, out mDate) == true){
                return mDate.ToString("ddMMyyyy");
            }
            else{
                return this.Date.Text;
            }

        }
        set
        {
            this.Date.Text = (string)value;
        }
    }


    /// <summary>
    /// Clears current value
    /// </summary>
    public void Clear()
    {
        Date.Text = "";
    }


    /// <summary>
    /// Returns true if the number is empty
    /// </summary>
    public bool IsDate()
    {
        DateTime mDate;
        if (DateTime.TryParse(this.Date.Text, out mDate) == true)
        {
            return true;
        }
        else { return false; }
    }

    /// <summary>
    /// Returns true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        if ((!IsDate()) && this.Mandatory)
        {
            return false;
        }

        DateTime mDate;
        if (DateTime.TryParse(this.Date.Text, out mDate) == true)
        {

            if (this.NDate != "")
            {
                string mDay = mDate.ToString("dd");
                string mMonth = mDate.ToString("MM");
                string mYear = mDate.ToString("yyyy");
            
                DateTime currentDate = DateTime.Now.AddDays(+ Convert.ToInt32(this.NDate));

                string cDay = currentDate.ToString("dd");
                string cMonth = currentDate.ToString("MM");
                string cYear = currentDate.ToString("yyyy");

                Int32 val_mDate = Convert.ToInt32(mYear + mMonth + mDay);
                Int32 val_cDate = Convert.ToInt32(cYear + cMonth + cDay);

                if (val_cDate > val_mDate)
                {
                    return false;
                }
            }
        }

        return true;
    }



    protected void Page_Load(object sender, EventArgs e)
    {
        CustomCloud_div.InnerHtml = string.Concat("<a id='CustomCloud_", this.RowCustomField, "' href='#'></a>");

        string twoLettersLanguage = string.Empty;
        twoLettersLanguage = CMSContext.CurrentDocumentCulture.CultureCode.ToLower().Substring(0,2).ToLower();
        if (twoLettersLanguage == "en")
            twoLettersLanguage = "";

        string dateScript = string.Empty;
        dateScript = "var " + this.ClientID + "_nDate = '" + this.NDate + "'; \n" +
            "$('#" + Date.ClientID + "').datepicker($.datepicker.regional['" + twoLettersLanguage + "']); \n";
        ScriptManager.RegisterStartupScript(this.Page, GetType(), "Date_" + Guid.NewGuid(), dateScript, true);

        string checkDate = string.Empty;
        checkDate = " function " + this.ClientID + "_checkDate(sender, args) { \n" +
                        "var bIsValid = false;\n" +
                        //"alert(" + this.ClientID + "_nDate)\n" +
                        "if (" + this.ClientID + "_nDate != '') {\n" +
                        "    var mDate = new Date();\n" +
                        "    var currentDate = new Date();\n" +
                        "    currentDate.setDate(currentDate.getDate() + parseInt(" + this.ClientID + "_nDate));\n" +
                        "\n" +
                        "    var objDate = $('#' + sender.controltovalidate); \n" +
                        "    var mDay = $.datepicker.formatDate('dd', objDate.datepicker('getDate')); \n" +
                        "    var mMonth = $.datepicker.formatDate('mm', objDate.datepicker('getDate')); \n" +
                        "    var mYear = objDate.datepicker('getDate').getFullYear(); \n" +
                        "    var valDate = 0;\n" +
                        "    valDate = parseInt(mYear.toString() + mMonth.toString() + mDay.toString());\n" +
                        "\n" +
                        "\n" +
                        "    var cDay = $.datepicker.formatDate('dd', currentDate);\n" +
                        "    var cMonth = $.datepicker.formatDate('mm', currentDate);\n" +
                        "    var cYear = currentDate.getFullYear();\n" +
                        "    var valCurrentDate = 0;\n" +
                        "    valCurrentDate = parseInt(cYear.toString() + cMonth.toString() + cDay.toString());\n" +
                        "\n" +
                        "\n" +
                        "    if (valDate >= valCurrentDate ) {\n" +
                        "        bIsValid = true;\n" +
                        "    }\n" +
                        "}\n" +
                        "else { bIsValid = true; }\n" +
                        "\n" +
                        "var IsCutomValid = sender.getAttribute('IsCustomValidator');\n" +
                        "var CustomErrorMess = sender.getAttribute('CustomValidatorErrorMessage');\n" +
                        "if (IsCutomValid != null) {\n" +
                        "    if (IsCutomValid.indexOf('CustomCloud_') != -1) {\n" +
                        "        if (!bIsValid) {\n" +
                        "            Tab1_ValidationCloud('#' + IsCutomValid, CustomErrorMess, true, '')\n" +
                        "        } else {\n" +
                        "            Tab1_ValidationCloud('#' + IsCutomValid, '', false, '')\n" +
                        "        }\n" +
                        "    }\n" +
                        "\n" +
                        "}\n" +
                        "args.IsValid = bIsValid; \n" +
                    "} \n" ;

        ScriptManager.RegisterStartupScript(this.Page, GetType(), "checkDate_" + Guid.NewGuid(), checkDate, true);




        cv_Date.Attributes.Add("ControlValidated", Date.ClientID);
        cv_Date.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", this.RowCustomField));
        cv_Date.Attributes.Add("CustomValidatorErrorMessage", this.ValidationError);
        cv_Date.ClientValidationFunction = this.ClientID + "_checkDate";
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        ScriptManager.GetCurrent(this.Page).EnableScriptGlobalization = true;
        ScriptManager.GetCurrent(this.Page).EnableScriptLocalization = true;
        ScriptManager.GetCurrent(this.Page).EnablePageMethods = true;
        ScriptManager.GetCurrent(this.Page).EnablePartialRendering = true;
            
        System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(CMSContext.CurrentDocumentCulture.CultureCode);
        System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(CMSContext.CurrentDocumentCulture.CultureCode);
    }

    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }
}

