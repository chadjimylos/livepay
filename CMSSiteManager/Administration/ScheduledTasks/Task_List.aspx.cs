using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Scheduler;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.ExtendedControls;

public partial class CMSSiteManager_Administration_ScheduledTasks_Task_List : CMSScheduledTasksPage
{
    #region "Variables"

    private int siteId = 0;
    private int selectedsiteid;
    private SiteInfo si = null;

    #endregion


    #region "Page events"

    protected override void OnPreInit(EventArgs e)
    {
        ((Panel)this.CurrentMaster.PanelBody.FindControl("pnlContent")).CssClass = "";
        base.OnPreInit(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Setup page title text and image
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Task_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_ScheduledTask/object.png");

        this.CurrentMaster.Title.HelpTopicName = "tasks_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        this.btnRestart.Text = ResHelper.GetString("Task_List.Restart");
        this.btnRun.Text = ResHelper.GetString("Task_List.RunNow");

        // control initialization
        HyperlinkNew.Text = ResHelper.GetString("Task_List.NewItemCaption");
        HyperlinkRefresh.Text = ResHelper.GetString("General.Refresh");

        UniGridTasks.OnAction += new OnActionEventHandler(UniGridTasks_OnAction);

        ImageNew.ImageUrl = GetImageUrl("Objects/CMS_ScheduledTask/add.png");
        ImageRefresh.ImageUrl = GetImageUrl("Objects/CMS_ScheduledTask/refresh.png");


        siteId = this.GetSiteID(Request.QueryString["siteid"]);

        if (siteId > 0)
        {
            pnlSites.Visible = false;
            siteSelector.StopProcessing = true;
            si = SiteInfoProvider.GetSiteInfo(siteId);
        }
        else
        {
            // Set site selector
            siteSelector.DropDownSingleSelect.AutoPostBack = true;
            siteSelector.OnlyRunningSites = false;
            siteSelector.AllowAll = false;
            siteSelector.UniSelector.SpecialFields = new string[1, 2] { { ResHelper.GetString("general.global"), "0" } };
            siteSelector.UniSelector.OnSelectionChanged += new EventHandler(UniSelector_OnSelectionChanged);

            if (!RequestHelper.IsPostBack())
            {
                selectedsiteid = QueryHelper.GetInteger("selectedsiteid", -1);
                if (selectedsiteid == -1)
                {
                    selectedsiteid = CMSContext.CurrentSiteID;
                }

                siteSelector.Value = selectedsiteid;
            }
            else
            {
                selectedsiteid = ValidationHelper.GetInteger(siteSelector.Value, 0);
            }

            si = SiteInfoProvider.GetSiteInfo(selectedsiteid);
        }

        UniGridTasks.WhereCondition = GenerateWhereCondition();
        UniGridTasks.ZeroRowsText = ResHelper.GetString("general.nodatafound");


        HyperlinkNew.NavigateUrl = "Task_Edit.aspx?" + GetSiteOrSelectedSite();
        HyperlinkRefresh.NavigateUrl = "Task_List.aspx?" + GetSiteOrSelectedSite();

        // Force action buttons to cause full postback so that tasks can be properly executed in global.asax
        ControlsHelper.RegisterPostbackControl(UniGridTasks);
        ControlsHelper.RegisterPostbackControl(btnRestart);
        ControlsHelper.RegisterPostbackControl(btnRun);
    }


    protected override void OnPreRender(EventArgs e)
    {
        if (si != null)
        {
            pnlLastRun.Visible = true;

            if (SchedulingHelper.UseAutomaticScheduler || !SchedulingHelper.RunSchedulerWithinRequest)
            {
                lblLastRun.Visible = true;
                btnRestart.Visible = true;

                string siteName = si.SiteName.ToLower();

                if (SchedulingTimer.TimerExists(siteName))
                {
                    btnRun.Enabled = true;
                    DateTime lastRun = ValidationHelper.GetDateTime(SchedulingTimer.LastRuns[siteName], DateTimeHelper.ZERO_TIME);

                    if (lastRun != DateTimeHelper.ZERO_TIME)
                    {
                        this.lblLastRun.Text = ResHelper.GetString("Task_List.LastRun") + " " + lastRun.ToString();
                    }
                    else
                    {
                        this.lblLastRun.Text = ResHelper.GetString("Task_List.Running");
                    }
                }
                else
                {
                    btnRun.Enabled = false;
                    this.lblLastRun.Text = ResHelper.GetString("Task_List.NoRun");
                }
            }
            else
            {
                lblLastRun.Visible = false;
                btnRestart.Visible = false;
            }
        }
        else
        {
            // Hide panel in sitemanager for global scheduled tasks
            pnlLastRun.Visible = false;
        }

        this.pnlUpdateTimer.Update();

        base.OnPreRender(e);
    }

    #endregion


    /// <summary>
    /// Generates where condition for unigrid
    /// </summary>    
    private string GenerateWhereCondition()
    {
        if (siteId > 0)
        {
            return "TaskSiteID = " + siteId;
        }
        else if (selectedsiteid > 0)
        {
            return "TaskSiteID = " + selectedsiteid;
        }
        else
        {
            return "TaskSiteID IS NULL";
        }
    }


    /// <summary>
    /// Returns 'siteid' or 'selectedsiteid' parameter depending on query string.
    /// </summary>
    /// <returns>Query parameter</returns>
    private string GetSiteOrSelectedSite()
    {
        // Site ID is used in CMS desk
        if (siteId > 0)
        {
            return "siteId=" + siteId;
        }
        // SelectedSiteID is used in CMS Site Manager
        else
        {
            return "selectedsiteid=" + selectedsiteid;
        }
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        // Update unigrid
        this.pnlUpdate.Update();
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that threw event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGridTasks_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "edit":

                UrlHelper.Redirect("Task_Edit.aspx?taskname=" + actionArgument.ToString() + "&" + GetSiteOrSelectedSite());

                break;

            case "delete":
                {
                    // Check "modify" permission
                    if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.ScheduledTasks", "Modify"))
                    {
                        RedirectToAccessDenied("CMS.ScheduledTasks", "Modify");
                    }

                    // Delete the task
                    try
                    {
                        int taskId = Convert.ToInt32(actionArgument);
                        TaskInfo ti = TaskInfoProvider.GetTaskInfo(taskId);
                        if (ti != null)
                        {
                            ti.LogSynchronization = SynchronizationTypeEnum.LogSynchronization;
                            TaskInfoProvider.DeleteTaskInfo(ti);
                        }
                    }
                    catch (Exception ex)
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("Task_List.DeleteError") + " Original exception: " + ex.Message;
                    }
                }
                break;

            case "execute":
                {
                    // Check "modify" permission
                    if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.ScheduledTasks", "Modify"))
                    {
                        RedirectToAccessDenied("CMS.ScheduledTasks", "Modify");
                    }

                    TaskInfo ti = TaskInfoProvider.GetTaskInfo(Convert.ToInt32(actionArgument));
                    if (ti != null)
                    {
                        if (ti.TaskEnabled)
                        {
                            ti.TaskNextRunTime = DateTime.Now;

                            // Update the task
                            TaskInfoProvider.SetTaskInfo(ti);

                            // Run the task
                            SchedulingTimer.RunSchedulerImmediately = true;

                            string url = Request.Url.AbsoluteUri;
                            url = UrlHelper.AddParameterToUrl(url, "selectedsiteid", selectedsiteid.ToString());

                            lblInfo.Text = ResHelper.GetString("ScheduledTask.WasExecuted");
                            lblInfo.Visible = true;

                            //ScriptHelper.RegisterStartupScript(this, typeof(string), "InformExecuted",
                            //        "alert('" + ResHelper.GetString("ScheduledTask.WasExecuted") + "'); \n" +
                            //        "document.location = '" + url + "'; \n", true);
                        }
                        else
                        {
                            lblError.Text = ResHelper.GetString("ScheduledTask.TaskNotEnabled");
                            lblError.Visible = true;
                        }
                    }
                }
                break;
        }
    }


    protected void btnRestart_Click(object sender, EventArgs e)
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.ScheduledTasks", "Modify"))
        {
            RedirectToAccessDenied("CMS.ScheduledTasks", "Modify");
        }

        SchedulingTimer.RestartTimer(si.SiteName);
    }


    protected void btnRun_Click(object sender, EventArgs e)
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.ScheduledTasks", "Modify"))
        {
            RedirectToAccessDenied("CMS.ScheduledTasks", "Modify");
        }

        SchedulingTimer.RunSchedulerASAP(si.SiteName);
    }
}


