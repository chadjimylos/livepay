using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Web.Caching;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.PortalEngine;
using CMS.UIControls;
using CMS.URLRewritingEngine;
using CMS.SettingsProvider;

public partial class CMSPages_GetCSS : GetFilePage
{
    #region "Advanced settings"

    /// <summary>
    /// Set to false to disable the client caching
    /// </summary>
    protected bool useClientCache = true;

    #endregion


    #region "Variables"

    protected CMSOutputCSS outputFile = null;
    string stylesheetName = null;
    protected CssStylesheetInfo si = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Returns true if the process allows cache
    /// </summary>
    public override bool AllowCache
    {
        get
        {
            if (mAllowCache == null)
            {
                // By default, cache for the stylesheets is always enabled (even outside of the live site)
                if (ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSAlwaysCacheStylesheets"], true))
                {
                    mAllowCache = true;
                }
                else
                {
                    mAllowCache = (this.ViewMode == ViewModeEnum.LiveSite);
                }
            }

            return mAllowCache.Value;
        }
        set
        {
            mAllowCache = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        DebugHelper.SetContext("GetCSS");

        // Check the site
        if (CurrentSiteName == "")
        {
            throw new Exception("[GetFile.aspx]: Site not running.");
        }

        stylesheetName = QueryHelper.GetString("stylesheetname", String.Empty);

        int cacheMinutes = this.CacheMinutes;

        // Try to get data from cache
        using (CachedSection<CMSOutputCSS> cs = new CachedSection<CMSOutputCSS>(ref outputFile, cacheMinutes, true, null, "getcss", stylesheetName))
        {
            if (cs.LoadData)
            {
                // Process the file
                ProcessStylesheet();

                // Ensure the cache settings
                if (cs.Cached)
                {
                    // Prepare the cache dependency
                    CacheDependency cd = null;
                    if (outputFile != null)
                    {
                        string[] dependencies = new string[] { "cms.cssstylesheet|byguid|" + si.StylesheetGUID.ToString().ToLower() };
                        cd = CacheHelper.GetCacheDependency(dependencies);
                    }

                    if (cd == null)
                    {
                        // Set default dependency based on name
                        cd = CacheHelper.GetCacheDependency(new string[] { "cms.cssstylesheet|byname|" + stylesheetName.ToLower() });
                    }

                    // Cache the data
                    cs.CacheDependency = cd;
                    cs.Data = outputFile;
                }
            }
        }

        // Send the data
        SendFile(outputFile);

        DebugHelper.ReleaseContext();
    }


    /// <summary>
    /// Sends the given file within response
    /// </summary>
    /// <param name="file">File to send</param>
    protected void SendFile(CMSOutputCSS file)
    {
        // Clear response.
        Response.Cookies.Clear();
        Response.Clear();

        Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);

        // Send the file
        if ((file != null) && (file.OutputData != null))
        {
            // Prepare etag
            string etag = file.VersionGUID;

            // Put etag into ""
            etag = "\"" + etag + "\"";

            // Client caching - only on the live site
            if (useClientCache && AllowCache && (CacheHelper.CacheImageEnabled(CurrentSiteName)))
            {
                // Determine the last modified date and etag sent from the browser
                string currentETag = Request.Headers["If-None-Match"];
                string ifModifiedString = Request.Headers["If-Modified-Since"];
                if ((ifModifiedString != null) && (currentETag == etag))
                {
                    // IE-browsers send something like this:
                    // If-Modified-Since: Wed, 19 Jul 2006 11:19:59 GMT; length=3350
                    DateTime ifModified;
                    if (DateTime.TryParse(ifModifiedString.Split(";".ToCharArray())[0], out ifModified))
                    {
                        // If not changed, let the browser use the cached data
                        if (file.LastModified <= ifModified.AddSeconds(1))
                        {
                            RespondNotModified(etag, true);
                            return;
                        }
                    }
                }
            }

            // Prepare response
            Response.ContentType = "text/css";

            if (useClientCache && AllowCache && (CacheHelper.CacheImageEnabled(CurrentSiteName)))
            {
                DateTime expires = DateTime.Now;

                // Send last modified header to allow client caching
                Response.Cache.SetLastModified(file.LastModified);
                Response.Cache.SetCacheability(HttpCacheability.Public);
                if (DocumentBase.AllowClientCache() && DocumentBase.UseFullClientCache)
                {
                    expires = DateTime.Now.AddMinutes(CacheHelper.CacheImageMinutes(CurrentSiteName));
                }

                Response.Cache.SetExpires(expires);
                Response.Cache.SetETag(etag);
            }

            // Add the file data
            Response.Write(file.OutputData);
        }
        else
        {
            NotFound();
        }

        CompleteRequest();
    }


    /// <summary>
    /// Processes the stylesheet.
    /// </summary>
    protected void ProcessStylesheet()
    {
        // Standard CSS stylesheet
        if (stylesheetName != "")
        {
            si = CssStylesheetInfoProvider.GetCssStylesheetInfo(stylesheetName);
            if (si != null)
            {
                string content = null;

                if ((si.StylesheetCheckedOutByUserID > 0) && (si.StylesheetCheckedOutMachineName.ToLower() == HTTPHelper.MachineName.ToLower()) && (File.Exists(HttpContext.Current.Server.MapPath(si.StylesheetCheckedOutFilename))))
                {
                    TextReader tr = new StreamReader(HttpContext.Current.Server.MapPath(si.StylesheetCheckedOutFilename));
                    content = tr.ReadToEnd();
                    tr.Close();
                }
                else
                {
                    content = si.StylesheetText;
                }

                outputFile = new CMSOutputCSS();
                outputFile.OutputData = content;
                outputFile.LastModified = DateTime.Now;
                outputFile.VersionGUID = si.StylesheetVersionGUID;
            }
        }

        // Newsletter template stylesheet
        string newsletterTemplateName = QueryHelper.GetString("newslettertemplatename", String.Empty);
        if (newsletterTemplateName != "")
        {
            Server.Transfer("~/CMSModules/Newsletters/CMSPages/GetCSS.aspx?newslettertemplatename=" + newsletterTemplateName);
        }

        // Resolve URLs
        if ((outputFile != null) && (outputFile.OutputData != null))
        {
            outputFile.OutputData = HTMLHelper.ResolveCSSUrls(outputFile.OutputData, Request.ApplicationPath);
        }
    }
}
