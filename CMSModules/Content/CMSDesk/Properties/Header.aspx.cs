using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Content_CMSDesk_Properties_Header : CMSPropertiesPage
{
    #region "Variables"

    private string selected = null;
    private int selectedTabIndex = 0;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        selected = DataHelper.GetNotEmpty(QueryHelper.GetString("tab", String.Empty).ToLower(), PropertyTabCode.ToString(UIContext.PropertyTab).ToLower());

        CurrentMaster.Tabs.OnTabCreated += tabElem_OnTabCreated;
        CurrentMaster.Tabs.ModuleName = "CMS.Content";
        CurrentMaster.Tabs.ElementName = "Properties";
        CurrentMaster.Tabs.UrlTarget = "propedit";
        CurrentMaster.SetRTL();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        CurrentMaster.Tabs.SelectedTab = selectedTabIndex;

        // Call the script for tab which is selected
        if (!CurrentMaster.Tabs.TabsEmpty)
        {
            ScriptHelper.RegisterStartupScript(Page, typeof(string), "FirstTabSelection", ScriptHelper.GetScript(" redir('" + CurrentMaster.Tabs.Tabs[selectedTabIndex, 2] + "','" + CurrentMaster.Tabs.UrlTarget + "'); " + CurrentMaster.Tabs.Tabs[selectedTabIndex, 1]));
        }
        else
        {
            ScriptHelper.RegisterStartupScript(Page, typeof(string), "FirstTabSelection", ScriptHelper.GetScript(" redir('" + UrlHelper.ResolveUrl("~/CMSMessages/Information.aspx") + "?message=" + ResHelper.GetString("uiprofile.uinotavailable") + "','" + CurrentMaster.Tabs.UrlTarget + "'); "));
        }
    }


    protected string[] tabElem_OnTabCreated(UIElementInfo element, string[] parameters, int tabIndex)
    {
        string elementName = element.ElementName.ToLower();
        if ((elementName == "properties.languages") && (!CultureInfoProvider.IsSiteMultilignual(CMSContext.CurrentSiteName) || !CultureInfoProvider.LicenseVersionCheck()))
        {
            return null;
        }

        if (elementName.StartsWith("properties.") && (elementName.Substring("properties.".Length) == selected))
        {
            selectedTabIndex = tabIndex;
        }

        return parameters;
    }
}
