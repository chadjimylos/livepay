﻿$(function () {
    $('[id$=_btnSearch]').clearSubmitDefaultValue('_txtSearch');
   
    
});

$.fn.LivePayLightBox = function (_options) {
    var popup = $(this)
    var position = popup.position();
    var width = popup.width();
    var height = popup.height();

    var topCss = (($(window).height() - height) / 2);
    var leftCss = (($(window).width() - width) / 2);

    var _options = $.extend(true,{
        message: popup,
        css: {
            top: (topCss < 0 ? 0 : topCss) + 'px',
            left: (leftCss < 0 ? 0 : leftCss) + 'px',
            border: 0,
            width: width + 'px',
            position: 'absolute'
        },
        overlayCSS: {
            cursor: 'default'
        }
    }, _options);

    $.blockUI(
            {
                message: _options.message
                , css: _options.css
                , overlayCSS: _options.overlayCSS
            });

};

        /******************************/
        var deviceAndroid = "android";
        var deviceIphone = "iphone";
        var deviceIpod = "ipod";
        var uagent = navigator.userAgent.toLowerCase();

        // Detects if the current device is an iPhone.
        function DetectIphone() {
            if (uagent.search(deviceIphone) > -1)
                return true;
            else
                return false;
        }

        //**************************
        // Detects if the current device is an iPod Touch.
        function DetectIpod() {

            if (uagent.search(deviceIpod) > -1)
                return true;
            else
                return false;
        }

        // Detects if the current device is an iPhone or iPod Touch. 
        function DetectIphoneOrIpod() {
            if (DetectIphone())
                return true;
            else if (DetectIpod())
                return true;
            else
                return false;
        }

        // Detects if the current device is an Android OS-based device.   
        function DetectAndroid() {
            if (uagent.search(deviceAndroid) > -1)
                return true;
            else
                return false;
        }


        function CheckIfIsnotMobile() {
            var IsnotMobile = true
            if (DetectIphoneOrIpod())
                IsnotMobile = false

            if (DetectAndroid())
                IsnotMobile = false

            return IsnotMobile
        }
        //------------------------------------
/****** Get mobile agent ******/
var mobileAgent = { "diviceAndroid": "android", "deviceIphone": "iphone","deviceIpod":"ipod" };
function _mobileDivice() {
    this.agent = navigator.userAgent.toLowerCase();
}
_mobileDivice.prototype = {
    isAndroid: function () { return (this.agent.search(mobileAgent.diviceAndroid) > -1 ? true : false) },
    isIphone: function () { return (this.agent.search(mobileAgent.deviceIphone) > -1 ? true : false); },
    isIpod: function () { return (this.agent.search(mobileAgent.deviceIpod) > -1 ? true : false); },
    isMobile: function () { return ((this.isAndroid() || this.isIphone()) ? true : false); }
};

var mobileDivice = new _mobileDivice();


/****** Get query string values ******/
var urlParams = {};
(function () {
    var e,
        a = /\+/g,  // Regex for replacing addition symbol with a space
        r = /([^&=]+)=?([^&]*)/g,
        d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
        q = window.location.search.substring(1);

    while (e = r.exec(q))
        urlParams[d(e[1])] = d(e[2]);
})();

$.fn.clearSubmitDefaultValue = function (textBoxID) {
  
	$(this).click(function () {
		var current_value = $('input[id$=' + textBoxID + ']').val();
		var default_value = $(this).attr('_defaultvalue');
       
		if (current_value == default_value) {
			$('input[id$=' + textBoxID + ']').val('');
			return false;
		}
	});
}