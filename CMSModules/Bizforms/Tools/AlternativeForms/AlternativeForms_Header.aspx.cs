using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormEngine;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_BizForms_Tools_AlternativeForms_AlternativeForms_Header : CMSBizFormPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'ReadForm' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "ReadForm"))
        {
            RedirectToCMSDeskAccessDenied("cms.form", "ReadForm");
        }

        int altFormId = QueryHelper.GetInteger("altformid", 0);
        int formId = QueryHelper.GetInteger("formid", 0);
        int saved = QueryHelper.GetInteger("saved", 0);

        AlternativeFormInfo afi = AlternativeFormInfoProvider.GetAlternativeFormInfo(altFormId);

        // Init breadcrumbs
        string[,] breadcrumbs = new string[2, 3];

        // Edit item breadcrumbs
        breadcrumbs[0, 0] = ResHelper.GetString("altforms.listlink");
        breadcrumbs[0, 1] = "~/CMSModules/BizForms/Tools/AlternativeForms/AlternativeForms_List.aspx?formid=" + formId;
        breadcrumbs[0, 2] = "BizFormContent";
        breadcrumbs[1, 0] = afi != null?afi.FormDisplayName:String.Empty;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu(altFormId, formId, saved);
        }
    }


    /// <summary>
    /// Initializes edit menu
    /// </summary>
    protected void InitalizeMenu(int altFormId, int formId, int saved)
    {
        string urlParams = "?altformid=" + altFormId.ToString() + "&formid=" + formId.ToString();

        string[,] tabs = new string[3, 4];
        tabs[0, 0] = ResHelper.GetString("general.general");
        tabs[0, 1] = "";
        tabs[0, 2] = "AlternativeForms_Edit_General.aspx" + urlParams + "&saved=" + saved;
        tabs[1, 0] = ResHelper.GetString("general.fields");
        tabs[1, 1] = "";
        tabs[1, 2] = "AlternativeForms_Fields.aspx" + urlParams;
        tabs[2, 0] = ResHelper.GetString("general.layout");
        tabs[2, 1] = "";
        tabs[2, 2] = "AlternativeForms_Layout.aspx" + urlParams;

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "altFormsContent";
    }
}
