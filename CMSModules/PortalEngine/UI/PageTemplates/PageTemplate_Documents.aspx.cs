using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.PortalEngine;

public partial class CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_Documents : CMSEditTemplatePage
{
    #region "Private variables"

    private int mPageTemplateId = 0;
    private SiteInfo siteInfo = null;
    private UserInfo currentUser = null;
    private PageTemplateInfo pti = null;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        currentUser = CMSContext.CurrentUser;

        filterDocuments.OnSiteSelectionChanged += UniSelector_OnSelectionChanged;

        // Get current page template ID
        mPageTemplateId = QueryHelper.GetInteger("templateid", 0);
        pti = PageTemplateInfoProvider.GetPageTemplateInfo(mPageTemplateId);
        if (pti != null)
        {
            if (pti.IsReusable)
            {
                filterDocuments.LoadSites = true;
            }
            else
            {
                filterDocuments.SitesPlaceHolder.Visible = false;
            }

            ucDocuments.OnExternalDataBound += ucDocuments_OnExternalDataBound;
            ucDocuments.ZeroRowsText = ResHelper.GetString("Administration-PageTemplate_Header.Documents.nodata");
            ucDocuments.OnDataReload += new UniGrid.OnDataReloadEventHandler(ucDocuments_OnDataReload);
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (filterDocuments.SelectedSite != TreeProvider.ALL_SITES)
        {
            ucDocuments.GridView.Columns[4].Visible = false;
        }
    }

    #endregion


    #region "Events handling"

    protected DataSet ucDocuments_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        // Generate where condition
        string where = "DocumentPageTemplateID =" + mPageTemplateId;
        where = SqlHelperClass.AddWhereCondition(where, completeWhere);
        where = SqlHelperClass.AddWhereCondition(where, filterDocuments.WhereCondition);

        TreeProvider tp = new TreeProvider(CMSContext.CurrentUser);
        // User is GlobalAdministrator so the sites are available
        string site = filterDocuments.SelectedSite;

        // For ad-hoc templates use all sites
        if (!pti.IsReusable)
        {
            site = TreeProvider.ALL_SITES;
        }

        // Get documents
        DataSet ds = tp.SelectNodes(site, TreeProvider.ALL_DOCUMENTS, TreeProvider.ALL_CULTURES, false, null, where, "DocumentName, SiteName", -1, false, currentTopN, "DocumentName, DocumentNamePath, NodeID, NodeSiteID, DocumentCulture, SiteName, NodeClassID, ClassName, NodeACLID");

        // Filter data by permissions
        ds = TreeSecurityProvider.FilterDataSetByPermissions(ds, NodePermissionsEnum.Read, currentUser);

        totalRecords = DataHelper.GetItemsCount(ds);
        return ds;
    }


    protected object ucDocuments_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView drv = GetDataRowView(sender as DataControlFieldCell);
        string siteName = ValidationHelper.GetString(drv["SiteName"], string.Empty);
        string documentCulture = null;

        // Get document site
        siteInfo = SiteInfoProvider.GetSiteInfo(siteName);

        switch (sourceName.ToLower())
        {
            case "documentname":
                // Generate default link for the document
                string documentName = TextHelper.LimitLength(ValidationHelper.GetString(drv["DocumentName"], string.Empty), 50);
                int nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                documentCulture = ValidationHelper.GetString(drv["DocumentCulture"], string.Empty);
                return GetDocumentLink(documentName, siteInfo, nodeId, documentCulture);

            case "documentnametooltip":
                drv = (DataRowView)parameter;
                return UniGridFunctions.DocumentNameTooltip(drv);

            case "culture":
                return UniGridFunctions.DocumentCultureFlag(drv, this.Page);

            case "nodeclassid":
            case "nodeclassidtooltip":
                int nodeClassId = ValidationHelper.GetInteger(drv["NodeClassID"], 0);
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(nodeClassId);
                string result = (dci != null) ? dci.ClassDisplayName : string.Empty;
                if (sourceName.ToLower() == "nodeclassid")
                {
                    result = TextHelper.LimitLength(result, 50);
                }
                result = HTMLHelper.HTMLEncode(result);
                return result;

            case "sitename":
                return siteInfo.DisplayName;

            case "icon":
                string className = ValidationHelper.GetString(drv["ClassName"], string.Empty);
                nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                documentCulture = ValidationHelper.GetString(drv["DocumentCulture"], string.Empty);
                return GetDocumentIconLink(siteInfo, nodeId, documentCulture, className);

            default:
                return parameter;
        }
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        pnlUpdate.Update();
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private static object GetDocumentLink(string documentName, SiteInfo si, int nodeId, string cultureCode)
    {
        // If the document name is empty alter it with the '-'
        if (string.IsNullOrEmpty(documentName))
        {
            documentName = "-";
        }

        string url = GetDocumentUrl(si, nodeId, cultureCode);
        string toReturn = HTMLHelper.HTMLEncode(documentName);

        if (!string.IsNullOrEmpty(url))
        {
            toReturn = "<a href=\"" + url + "\" target=\"_blank\">" + toReturn + "</a>";
        }
        return toReturn;
    }


    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private object GetDocumentIconLink(SiteInfo si, int nodeId, string cultureCode, string className)
    {
        string url = GetDocumentUrl(si, nodeId, cultureCode);
        string toReturn = "<img style=\"border-style:none;\" src=\"" + GetDocumentTypeIconUrl(className) + "\" alt=\"" + className + "\" />";

        if (!string.IsNullOrEmpty(url))
        {
            return "<a href=\"" + url + "\" target=\"_blank\">" + toReturn + "</a>";
        }
        return toReturn;
    }


    /// <summary>
    /// Generates URL for document
    /// </summary>
    private static string GetDocumentUrl(SiteInfo si, int nodeId, string cultureCode)
    {
        string url = string.Empty;

        if (si.Status == SiteStatusEnum.Running)
        {
            if (si.SiteName != CMSContext.CurrentSiteName)
            {
                // Make url for site in form 'http(s)://sitedomain/application/cmsdesk'.
                url = UrlHelper.GetApplicationUrl(si.DomainName) + "/cmsdesk/default.aspx?section=content&nodeid=" + nodeId + "&culture=" + cultureCode;
            }
            else
            {
                // Make url for current site (with port)
                url = UrlHelper.ResolveUrl("~/cmsdesk/default.aspx?section=content&nodeid=" + nodeId + "&culture=" + cultureCode);
            }
        }
        return url;
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }

    #endregion
}
