<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="CMSSiteManager_Settings_default" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head runat="server" enableviewstate="false">
    <title>Site Manager - Settings</title>

    <script type="text/javascript">
        //<![CDATA[
        document.titlePart = 'Settings';
        //]]>
    </script>

</head>
<frameset border="0" cols="300, *" runat="server" id="colsFrameset" enableviewstate="false"
    framespacing="0">
  		<frame name="categories" src="categories.aspx" scrolling="no" frameborder="0" runat="server" class="TreeFrame" />		
	    <frame name="keys" src="keys.aspx" frameborder="0" runat="server" />
		<noframes>
			<body>
			 <p id="p1">
				This HTML frameset displays multiple Web pages. To view this frameset, use a 
				Web browser that supports HTML 4.0 and later.
			 </p>
			</body>
		</noframes>
	</frameset>
</html>
