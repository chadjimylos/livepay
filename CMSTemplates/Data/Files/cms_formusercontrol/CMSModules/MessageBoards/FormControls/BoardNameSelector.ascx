<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BoardNameSelector.ascx.cs"
    Inherits="CMSModules_MessageBoards_FormControls_BoardNameSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" ReturnColumnName="BoardName" DisplayNameFormat="{%BoardDisplayName%}"
            ObjectType="board.board" ResourcePrefix="boardselector" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
