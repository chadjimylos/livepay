<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletter_Frameset.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Newsletter - Properties</title>
</head>
<frameset border="0" rows="65, *" id="rowsFrameset">
    <frame name="newsletterMenu" src="<%= newsletterHeaderUrl %>" frameborder="0" scrolling="no"
        noresize="noresize" />
    <frame name="newsletterContent" src="<%= newsletterContentUrl %>"
        frameborder="0" />
<noframes>
    <body>
        <p id="p1">
            This HTML frameset displays multiple Web pages. To view this frameset, use a Web
            browser that supports HTML 4.0 and later.
        </p>
    </body>
</noframes>
</frameset>
</html>
