using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Properties_Attachments : CMSPropertiesPage
{
    #region "Variables"

    protected bool hasModifyPermission = true;
    protected TreeProvider tree = null;
    protected int nodeId = 0;

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.Attachments"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.Attachments");
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        UIContext.PropertyTab = PropertyTabEnum.Attachments;

        nodeId = QueryHelper.GetInteger("nodeid", 0);
        if (nodeId > 0)
        {
            tree = new TreeProvider(CMSContext.CurrentUser);
            menuElem.Node = tree.SelectSingleNode(nodeId);
            if (menuElem.Node != null)
            {
                // Check read permissions
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(menuElem.Node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                {
                    RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), menuElem.Node.NodeAliasPath));
                }
                // Check modify permissions
                else if (CMSContext.CurrentUser.IsAuthorizedPerDocument(menuElem.Node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                {
                    hasModifyPermission = false;
                    lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), menuElem.Node.NodeAliasPath);
                }
                ucAttachments.DocumentID = menuElem.Node.DocumentID;
                ucAttachments.NodeParentNodeID = menuElem.Node.NodeParentID;
                ucAttachments.NodeClassName = menuElem.Node.NodeClassName;

                // Resize attachment due to site settings
                string siteName = CMSContext.CurrentSiteName;
                ucAttachments.ResizeToHeight = ImageHelper.GetAutoResizeToHeight(siteName);
                ucAttachments.ResizeToWidth = ImageHelper.GetAutoResizeToWidth(siteName);
                ucAttachments.ResizeToMaxSideSize = ImageHelper.GetAutoResizeToMaxSideSize(siteName);
                ucAttachments.PageSize = "10,25,50,100,##ALL##";
            }
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        ucAttachments.VersionHistoryID = menuElem.Node.DocumentCheckedOutVersionHistoryID;
        ucAttachments.Enabled = (menuElem.AllowEdit && hasModifyPermission && menuElem.AllowSave);
        lblInfo.Text = menuElem.WorkflowInfo;
        pnlAttachments.Visible = (lblInfo.Text != string.Empty);
    }

    #endregion
}
