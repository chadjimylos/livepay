<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Membership/Profile/ChangePassword.ascx.cs"
    Inherits="CMSWebParts_Membership_Profile_ChangePassword" %>
<asp:Panel ID="pnlWebPart" runat="server" DefaultButton="btnOk">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table class="ChangePasswordTable">
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblOldPassword" AssociatedControlID="txtOldPassword" runat="server" />
            </td>
            <td class="FieldInput">
                <asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblNewPassword" AssociatedControlID="txtNewPassword" runat="server" />
            </td>
            <td class="FieldInput">
                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblConfirmPassword" AssociatedControlID="txtConfirmPassword" runat="server" />
            </td>
            <td class="FieldInput">
                <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" />
            </td>
        </tr>
        <tr>
            <td colspan="2" class="ChangeButton">
                <br />
                <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOk_Click" CssClass="LongSubmitButton" />
            </td>
        </tr>
    </table>
</asp:Panel>
