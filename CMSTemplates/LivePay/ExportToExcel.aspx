﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExportToExcel.aspx.vb" Inherits="CMSTemplates_LivePay_ExportToExcel" %>
<link type="text/css" rel="stylesheet" href="/CMSPages/GetCSS.aspx?stylesheetname=LivePay" /> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divRep1" runat="server" visible="false" >
        <asp:GridView  ID="GridViewPayments" runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Names="arial" HeaderStyle-Font-Size="14px" HeaderStyle-Font-Bold="true" RowStyle-Font-Names="arial" RowStyle-Font-Size="12px" >
            <Columns>
                <asp:BoundField DataField="transactiondDte"  />
                 <asp:TemplateField   >
                    <ItemTemplate>
                        <asp:Label ID="lblmerchantId" style="padding-left:3px" runat="server" Text='<%#Eval("merchantId") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"   ItemStyle-CssClass="MerchSrcItemSimple" >
                    <ItemTemplate>
                        <asp:Label ID="lblCardNumber" runat="server" Text='<%# "xxxxxxxx-xxxx-" & Right(Replace(Eval("CardNumber")," ",""),4) %> ' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple" ItemStyle-HorizontalAlign="Right"  ItemStyle-CssClass="MerchSrcItemSimple" >
                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("transactioAamount") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField  HeaderStyle-Width="150px" ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:Label ID="lbltransactionStatus" runat="server" Text='<%#GetTransactionStatusDescrPay(Eval("TransactionStatus")) %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField  HeaderStyle-Width="70px" ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:Label ID="lbltransactionType" runat="server" Text='<%#Eval("TransactionType") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

    <div id="divRep2" runat="server" visible="false" >
        <asp:GridView  ID="GridViewMerchantsPayments" runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Names="arial" HeaderStyle-Font-Size="14px" HeaderStyle-Font-Bold="true" RowStyle-Font-Names="arial" RowStyle-Font-Size="12px" >
            <Columns>
                <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderLeft MerchSrcHeader" ItemStyle-CssClass="MerchSrcItemLeft"  >
                    <ItemTemplate>
                        <asp:label ID="lblBoxNo" runat="server" Text='<%#Eval("BatchID") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:label ID="lblCloseDtm" runat="server" Text='<%#Eval("BatchDate").toString().split(" ")(0) & "<br />" & Eval("BatchDate").toString().split(" ")(1) %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:label ID="lblBoxStatus" runat="server" Text='<%#IIF(Eval("BatchClosed")=true,"Κλειστό","Ανοιχτό") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  >
                    <ItemTemplate>
                        <asp:label ID="lblTransCode" runat="server" Text='' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple "  ItemStyle-CssClass="MerchSrcItemSimple " >
                    <ItemTemplate>
                        <asp:label ID="lblTransDtm" runat="server" Text='<%#Eval("transactiondDte").toString().split(" ")(0) & "<br />" & Eval("transactiondDte").toString().split(" ")(1) %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple W60"  ItemStyle-CssClass="MerchSrcItemSimple W60" >
                    <ItemTemplate>
                        <asp:label ID="lblCustName" runat="server" Text='<%#Eval("CustomerName") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple" ItemStyle-HorizontalAlign="Right"  ItemStyle-CssClass="MerchSrcItemSimple" >
                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("transactioAamount") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:label ID="lblTransType" runat="server" Text='<%#Eval("transactionType") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField  HeaderStyle-Width="70px" ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:Label ID="lblInstallments" runat="server" Text='<%#Eval("Installments") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblTransStatus" runat="server" Text='<%#GetTransactionStatusDescr(Eval("TransactionStatus")) %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>


                 <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo1" runat="server" Text='<%#Eval("info1") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo2" runat="server" Text='<%#Eval("info2") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo3" runat="server" Text='<%#Eval("info3") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false"  HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo4" runat="server" Text='<%#Eval("info4") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo5" runat="server" Text='<%#Eval("info5") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo6" runat="server" Text='<%#Eval("info6") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo7" runat="server" Text='<%#Eval("info7") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo8" runat="server" Text='<%#Eval("info8") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo9" runat="server" Text='<%#Eval("info9") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo10" runat="server" Text='<%#Eval("info10") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
