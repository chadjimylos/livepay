﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchResults.ascx.cs"
    Inherits="CMSModules_SmartSearch_Controls_SearchResults" %>
<asp:PlaceHolder runat="server" ID="plcBasicRepeater"></asp:PlaceHolder>
<cms:UniPager runat="server" ID="pgrSearch" PageControl="repSearchResults" />
<cms:LocalizedLabel runat="server" ID="lblNoResults" CssClass="ContentLabel" Visible="false"
    EnableViewState="false" />