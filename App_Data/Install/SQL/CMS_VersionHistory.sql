CREATE TABLE [CMS_VersionHistory] (
		[VersionHistoryID]     int NOT NULL IDENTITY(1, 1),
		[NodeSiteID]           int NULL,
		[DocumentID]           int NULL,
		[DocumentNamePath]     nvarchar(450) NOT NULL,
		[NodeXML]              nvarchar(max) NOT NULL,
		[ModifiedByUserID]     int NULL,
		[ModifiedWhen]         datetime NOT NULL,
		[VersionNumber]        nvarchar(50) NULL,
		[VersionComment]       nvarchar(max) NULL,
		[ToBePublished]        bit NOT NULL,
		[PublishFrom]          datetime NULL,
		[PublishTo]            datetime NULL,
		[WasPublishedFrom]     datetime NULL,
		[WasPublishedTo]       datetime NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_VersionHistory]
	ADD
	CONSTRAINT [PK_CMS_VersionHistory]
	PRIMARY KEY
	NONCLUSTERED
	([VersionHistoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_VersionHistory]
	ADD
	CONSTRAINT [DF__CMS_Versi__ToBeP__71D1E811]
	DEFAULT ((0)) FOR [ToBePublished]
IF OBJECT_ID(N'[CMS_VersionHistory]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_VersionHistory_ModifiedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_VersionHistory]
			ADD CONSTRAINT [FK_CMS_VersionHistory_ModifiedByUserID_CMS_User]
			FOREIGN KEY ([ModifiedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[CMS_VersionHistory]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_VersionHistory_NodeSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_VersionHistory]
			ADD CONSTRAINT [FK_CMS_VersionHistory_NodeSiteID_CMS_Site]
			FOREIGN KEY ([NodeSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_CMS_VersionHistory_DocumentID]
	ON [CMS_VersionHistory] ([DocumentID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_VersionHistory_ModifiedByUserID]
	ON [CMS_VersionHistory] ([ModifiedByUserID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_VersionHistory_NodeSiteID]
	ON [CMS_VersionHistory] ([NodeSiteID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_VersionHistory_ToBePublished_PublishFrom_PublishTo]
	ON [CMS_VersionHistory] ([ToBePublished], [PublishFrom], [PublishTo])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
