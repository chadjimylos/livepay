<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectModule.ascx.cs"
    Inherits="CMSFormControls_SelectModule" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>
<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%ResourceDisplayName%}"
            SelectionMode="SingleDropDownList" ObjectType="cms.resource" ResourcePrefix="moduleselector"
            AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
