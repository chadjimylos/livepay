CREATE TABLE [Reporting_ReportTable] (
		[TableID]                         int NOT NULL IDENTITY(1, 1),
		[TableName]                       nvarchar(100) NOT NULL,
		[TableDisplayName]                nvarchar(450) NOT NULL,
		[TableQuery]                      nvarchar(max) NOT NULL,
		[TableQueryIsStoredProcedure]     bit NOT NULL,
		[TableReportID]                   int NOT NULL,
		[TableSettings]                   nvarchar(max) NULL,
		[TableGUID]                       uniqueidentifier NOT NULL,
		[TableLastModified]               datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Reporting_ReportTable]
	ADD
	CONSTRAINT [PK_Reporting_ReportTable]
	PRIMARY KEY
	([TableID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Reporting_ReportTable]') IS NOT NULL
	AND OBJECT_ID(N'[Reporting_Report]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Reporting_ReportTable_TableReportID_Reporting_Report]') IS NULL
BEGIN
		ALTER TABLE [Reporting_ReportTable]
			ADD CONSTRAINT [FK_Reporting_ReportTable_TableReportID_Reporting_Report]
			FOREIGN KEY ([TableReportID]) REFERENCES [Reporting_Report] ([ReportID])
END
CREATE UNIQUE INDEX [IX_Reporting_ReportTable_TableReportID_TableName]
	ON [Reporting_ReportTable] ([TableName], [TableReportID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
