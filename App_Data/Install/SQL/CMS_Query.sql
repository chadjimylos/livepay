CREATE TABLE [CMS_Query] (
		[QueryID]                      int NOT NULL IDENTITY(1, 1),
		[QueryName]                    nvarchar(100) NOT NULL,
		[QueryTypeID]                  int NOT NULL,
		[QueryText]                    nvarchar(max) NOT NULL,
		[QueryRequiresTransaction]     bit NOT NULL,
		[ClassID]                      int NOT NULL,
		[QueryIsLocked]                bit NOT NULL,
		[QueryLastModified]            datetime NOT NULL,
		[QueryGUID]                    uniqueidentifier NOT NULL,
		[QueryLoadGeneration]          int NOT NULL,
		[QueryIsCustom]                bit NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Query]
	ADD
	CONSTRAINT [PK_CMS_Query]
	PRIMARY KEY
	NONCLUSTERED
	([QueryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_Query]
	ADD
	CONSTRAINT [DEFAULT_CMS_Query_QueryIsCustom]
	DEFAULT ((0)) FOR [QueryIsCustom]
ALTER TABLE [CMS_Query]
	ADD
	CONSTRAINT [DEFAULT_CMS_Query_QueryLoadGeneration]
	DEFAULT ((0)) FOR [QueryLoadGeneration]
IF OBJECT_ID(N'[CMS_Query]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Query_ClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_Query]
			ADD CONSTRAINT [FK_CMS_Query_ClassID_CMS_Class]
			FOREIGN KEY ([ClassID]) REFERENCES [CMS_Class] ([ClassID])
END
CREATE INDEX [IX_CMS_Query_QueryClassID_QueryName]
	ON [CMS_Query] ([ClassID], [QueryName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_Query_QueryLoadGeneration]
	ON [CMS_Query] ([QueryLoadGeneration])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
