﻿Imports System.Web
Imports System.Collections
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports CMS.TreeEngine
Imports System.Collections.Generic
Imports System.IO
Imports System.IO.Compression

''' <summary>
''' Empty web service template
''' </summary>
<WebService([Namespace]:="LivePay.WebService")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class LivePayWebService
    Inherits System.Web.Services.WebService
    ''' <summary>
    ''' Constructor.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
    End Sub

    <WebMethod()> _
    Public Function GetCategories() As List(Of LivePayCategory)
        Dim ds As DataSet = TreeHelper.GetDocuments(CMSContext.CurrentSiteName, "/διαθεσιμες-πληρωμες/%", "el-gr", False, "cms.menuitem", "", "NodeOrder", 1, True, -1)
        Dim Categories As New List(Of LivePayCategory)

        If (Not DataHelper.DataSourceIsEmpty(ds)) Then
            For Each row As DataRow In ds.Tables(0).Rows
                'HttpContext.Current.Response.Write(row("DocumentNodeID").ToString())
                Categories.Add(New LivePayCategory(Convert.ToInt32(row("DocumentNodeID"))))
            Next
        End If

        Return Categories
    End Function

    <WebMethod()> _
    Public Function SearchMerchant(ByVal search As String) As List(Of LivePayMerchant)
        Dim merchant As New List(Of LivePayMerchant)
        search = search.Replace("'", "''")
        Dim dsItems As DataSet = TreeHelper.GetDocuments(CMSContext.CurrentSiteName, "/διαθεσιμες-πληρωμες/%", "el-gr", False, "LivePay.Merchant;LivePay.MerchantPublicSector", "DiscreetTitle Like N'%" & search & "%'", Nothing, -1, True, -1)

        If (Not DataHelper.DataSourceIsEmpty(dsItems)) Then
            For Each row As DataRow In dsItems.Tables(0).Rows
                Dim cf As New List(Of LivePayMerchantCustomField)

                'cf.Add(New LivePayMerchantCustomField(ValidationHelper.GetInteger(row("nodeid").ToString(), 0)))

                'merchant.Add(New LivePayMerchant With { _
                '         .DiscreetTitle = row("DiscreetTitle").ToString(), _
                '         .livePayID = row("LivePayID").ToString(), _
                '         .Barcode = ValidationHelper.GetBoolean(row("Barcode").ToString(), False), _
                '         .OCR = ValidationHelper.GetBoolean(row("OCR").ToString(), False), _
                '         .NodeID = ValidationHelper.GetInteger(row("nodeid").ToString(), 0), _
                '         .CategoryID = ValidationHelper.GetInteger(row("nodeParentID").ToString(), 0) _
                '     })
                merchant.Add(New LivePayMerchant(row("LivePayID").ToString(), _
                                                 ValidationHelper.GetInteger(row("nodeid").ToString(), 0), _
                                                 "<![CDATA[" & row("DiscreetTitle").ToString() & "]]>", _
                                                 ValidationHelper.GetBoolean(row("Barcode").ToString(), False), _
                                                 ValidationHelper.GetBoolean(row("OCR").ToString(), False), _
                                                 ValidationHelper.GetInteger(row("nodeParentID").ToString(), 0)))
            Next
        End If

        Return merchant
    End Function

    <WebMethod()> _
    Public Function GetMerchants(ByVal categoryID As Integer) As List(Of LivePayMerchant)
        Dim merchant As New List(Of LivePayMerchant)

        Dim dsItems As DataSet = TreeHelper.GetDocuments(CMSContext.CurrentSiteName, "/διαθεσιμες-πληρωμες/%", "el-gr", False, "LivePay.Merchant;LivePay.MerchantPublicSector", "NodeParentID = " & categoryID, Nothing, -1, True, -1)

        If (Not DataHelper.DataSourceIsEmpty(dsItems)) Then
            For Each row As DataRow In dsItems.Tables(0).Rows
                merchant.Add(New LivePayMerchant With { _
                         .DiscreetTitle = "<![CDATA[" & row("DiscreetTitle").ToString() & "]]>", _
                         .Barcode = ValidationHelper.GetBoolean(row("Barcode").ToString(), False), _
                         .OCR = ValidationHelper.GetBoolean(row("OCR").ToString(), False), _
                         .NodeID = ValidationHelper.GetInteger(row("nodeid").ToString(), 0), _
                         .CategoryID = ValidationHelper.GetInteger(row("nodeParentID").ToString(), 0)})
            Next
        End If

        Return merchant
    End Function

    <WebMethod()> _
    Public Function GetAllXMLDecompress() As String
        ' Build output XML
        Dim AllXML As New StringBuilder
        AllXML.Append("<?xml version=""1.0"" encoding=""utf-8"" ?>")
        AllXML.Append("<Categories>")

        AllXML.Append(GetCategoriesXML())

        AllXML.Append("</Categories>")

        Return AllXML.ToString
    End Function
    
    <WebMethod()> _
    Public Function GetAllXML() As String
        ' Build output XML
        Dim AllXML As New StringBuilder
        AllXML.Append("<?xml version=""1.0"" encoding=""utf-8"" ?>")
        AllXML.Append("<Categories>")

        AllXML.Append(GetCategoriesXML())

        AllXML.Append("</Categories>")

        Return Compress(AllXML.ToString)
    End Function

    Public Function GetCategoriesXML(Optional ByVal DocumentNodeID As Integer = 0, Optional ByVal NodeLevel As Integer = 1) As String
        Dim ds As DataSet = Nothing
        If DocumentNodeID = 0 Then
            ds = TreeHelper.GetDocuments(CMSContext.CurrentSiteName, "/διαθεσιμες-πληρωμες/%", "el-gr", False, "cms.menuitem", "", "NodeOrder", NodeLevel, True, -1)
        Else
            ds = TreeHelper.GetDocuments(CMSContext.CurrentSiteName, "/διαθεσιμες-πληρωμες/%", "el-gr", False, "cms.menuitem", "NodeParentID = " & DocumentNodeID, "NodeOrder", NodeLevel, True, -1)
        End If


        ' Build output XML
        Dim Categories As New StringBuilder

        ' Add categories
        If (Not DataHelper.DataSourceIsEmpty(ds)) Then
            For Each row As DataRow In ds.Tables(0).Rows
                Categories.Append("<Category>")

                'CategoryName
                Categories.Append("<CategoryName>")
                Categories.Append("<![CDATA[" & row("MenuItemName").ToString() & "]]>")
                Categories.Append("</CategoryName>")

                'CategoryID
                Categories.Append("<CategoryID>")
                Categories.Append(row("NodeID").ToString())
                Categories.Append("</CategoryID>")

                'Sub Category
                Categories.Append(GetCategoriesXML(row("DocumentNodeID").ToString(), Convert.ToInt32(row("NodeLevel").ToString())))

                'Merchants
                Categories.Append(GetMerchantsXML(row("DocumentNodeID").ToString()))

                Categories.Append("</Category>")
            Next
        End If

        Return Categories.ToString()
    End Function

    Public Function GetMerchantsXML(ByVal categoryID As Integer) As String
        Dim merchant As New StringBuilder


        Dim dsItems As DataSet = TreeHelper.GetDocuments(CMSContext.CurrentSiteName, "/διαθεσιμες-πληρωμες/%", "el-gr", False, "LivePay.Merchant;LivePay.MerchantPublicSector", "NodeParentID = " & categoryID, Nothing, -1, True, -1)

        Dim ds As DataSet = TreeHelper.GetDocuments(CMSContext.CurrentSiteName, "/διαθεσιμες-πληρωμες/%", "el-gr", False, "cms.menuitem", "NodeParentID = " & categoryID, "DocumentName", -1, True, -1)


        If (Not DataHelper.DataSourceIsEmpty(dsItems)) AndAlso DataHelper.DataSourceIsEmpty(ds) Then
            merchant.Append("<Merchants>")

            'Loop LivePay.Merchant
            For Each MyTable As DataTable In dsItems.Tables
                For Each row As DataRow In MyTable.Rows
                    merchant.Append("<Merchant>")

                    'MerchantName
                    merchant.Append("<MerchantName>")
                    merchant.Append("<![CDATA[" & row("DiscreetTitle") & "]]>")
                    merchant.Append("</MerchantName>")

                    'MerchantID
                    merchant.Append("<MerchantID>")
                    merchant.Append(row("NodeID"))
                    merchant.Append("</MerchantID>")

                    'MerchantLivePayID
                    merchant.Append("<MerchantLivePayID>")
                    merchant.Append(row("LivePayID"))
                    merchant.Append("</MerchantLivePayID>")

                    'Barcode
                    merchant.Append("<Barcode>")
                    merchant.Append(row("Barcode"))
                    merchant.Append("</Barcode>")

                    'OCR
                    merchant.Append("<OCR>")
                    merchant.Append(row("OCR"))
                    merchant.Append("</OCR>")

                    merchant.Append("</Merchant>")
                Next
            Next
            merchant.Append("</Merchants>")
        End If

        Return merchant.ToString
    End Function

    Public Function Compress(ByVal text As String) As String
        Dim buffer As Byte() = Encoding.UTF8.GetBytes(text)
        Dim ms As New MemoryStream()
        Using zip As New GZipStream(ms, CompressionMode.Compress, True)
            zip.Write(buffer, 0, buffer.Length)
        End Using

        ms.Position = 0
        Dim outStream As New MemoryStream()

        Dim compressed As Byte() = New Byte(ms.Length - 1) {}
        ms.Read(compressed, 0, compressed.Length)

        Dim gzBuffer As Byte() = New Byte(compressed.Length + 3) {}
        System.Buffer.BlockCopy(compressed, 0, gzBuffer, 4, compressed.Length)
        System.Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gzBuffer, 0, 4)
        Return Convert.ToBase64String(gzBuffer)
    End Function

    Public Function Decompress(ByVal compressedText As String) As String
        Dim gzBuffer As Byte() = Convert.FromBase64String(compressedText)
        Using ms As New MemoryStream()
            Dim msgLength As Integer = BitConverter.ToInt32(gzBuffer, 0)
            ms.Write(gzBuffer, 4, gzBuffer.Length - 4)

            Dim buffer As Byte() = New Byte(msgLength - 1) {}

            ms.Position = 0
            Using zip As New GZipStream(ms, CompressionMode.Decompress)
                zip.Read(buffer, 0, buffer.Length)
            End Using

            Return Encoding.UTF8.GetString(buffer)
        End Using
    End Function

End Class
