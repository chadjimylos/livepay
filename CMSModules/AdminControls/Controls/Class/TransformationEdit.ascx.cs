using System;
using System.Web.UI.WebControls;
using System.Xml;
using System.IO;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_AdminControls_Controls_Class_TransformationEdit : CMSUserControl
{
    #region "Private variables"

    private CMSMasterPage currentMaster = null;

    private int transformationId = 0;
    private string className = "";
    private TransformationInfo ti = new TransformationInfo();
    private bool onlyCodeEdit = false;

    private string mListPage;
    private string mEditingPage;
    private string mParameterName;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets the URL of editing page(redirect after save)
    /// </summary>
    public string EditingPage
    {
        get
        {
            return mEditingPage;
        }
        set
        {
            mEditingPage = value;
        }
    }


    /// <summary>
    /// Gets or sets URL od listing page(breadcrumbs)
    /// </summary>
    public string ListPage
    {
        get
        {
            return mListPage;
        }
        set
        {
            mListPage = value;
        }
    }


    /// <summary>
    /// Gets or sets the paramatername with transformation class id
    /// </summary>
    public string ParameterName
    {
        get
        {
            return mParameterName;
        }
        set
        {
            mParameterName = value;
        }
    }

    #endregion


    protected void Page_Init(object sender, EventArgs e)
    {
        // Check master page
        if (!(Page.Master is CMSMasterPage))
        {
            throw new Exception("Page using this control must have CMSMasterPage master page.");
        }

        // Get CMS master page
        currentMaster = (CMSMasterPage)Page.Master;

        // Initializes validator
        RequiredFieldValidatorTransformationName.ErrorMessage = ResHelper.GetString("DocumentType_Edit_Transformation_Edit.TransformationNameRequired");
        // Initializes labels
        LabelInit();

        // Initialize drop down list
        DropDownListInit();

        // Init transformation help link
        lnkHelp.ResourceString = "documenttype_edit_transformation.help";
        lnkHelp.NavigateUrl = ResolveUrl("~/CMSHelp/") + "newedit_transformation_methods.htm";
        lnkHelp.Target = "_target";


        int classId = 0;
        string actualTransformName = ResHelper.GetString("DocumentType_Edit_Transformation_Edit.NewTransformation");

        currentMaster.Title.HelpTopicName = "newedit_transformation";
        currentMaster.Title.HelpName = "helpTopic";

        // Page has been opened in CMSDesk and only transformation code editing is allowed
        onlyCodeEdit = QueryHelper.GetBoolean("editonlycode", false);
        if (onlyCodeEdit)
        {
            // Set page title
            currentMaster.Title.TitleText = ResHelper.GetString("TransformationEdit.Title");
            currentMaster.Title.TitleImage = GetImageUrl("Design/Selectors/selecttransformation.png");

            string transformationName = QueryHelper.GetString("name", "");
            if (!String.IsNullOrEmpty(transformationName))
            {
                // Get transformation info
                ti = TransformationInfoProvider.GetTransformation(transformationName);
                if (ti != null)
                {
                    transformationId = ti.TransformationID;
                    className = DataClassInfoProvider.GetClassName(ti.TransformationClassID);
                    tbTransformationName.Enabled = false;
                }
                else
                {
                    // Hide panel Menu and write error message
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("formcontrols_selecttransformation.transofrmationnotexist").Replace("%%code%%", Server.HtmlEncode(transformationName));
                    return;
                }
            }
        }
        else
        {
            // Gets classID from querystring, used when creating new transformation
            classId = QueryHelper.GetInteger(ParameterName, 0);
            if (classId > 0)
            {
                className = DataClassInfoProvider.GetClassName(classId);
            }

            // Gets transformationID from querystring, used when editing transformation
            transformationId = QueryHelper.GetInteger("transformationid", 0);
            if (transformationId > 0)
            {
                string fullName = TransformationInfoProvider.GetTransformationFullName(transformationId);
                ti = TransformationInfoProvider.GetTransformation(fullName);
                actualTransformName = ti.TransformationName;

                className = DataClassInfoProvider.GetClassName(ti.TransformationClassID);
                classId = DataClassInfoProvider.GetDataClass(className).ClassID;

                if (!RequestHelper.IsPostBack())
                {
                    // Shows that the new transformation was created or updated successfully
                    if (QueryHelper.GetBoolean("saved", false))
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
            else
            {
                ti.TransformationFullName = "";
            }

            // Initializes PageTitle
            string transformations = ResHelper.GetString("DocumentType_Edit_Transformation_Edit.Transformations");
            string[,] pageTitleTabs = new string[2, 3];

            pageTitleTabs[0, 0] = transformations;
            pageTitleTabs[0, 1] = ListPage + "?" + ParameterName + "=" + classId;
            pageTitleTabs[0, 2] = "";
            pageTitleTabs[1, 0] = actualTransformName;
            pageTitleTabs[1, 1] = "";
            pageTitleTabs[1, 2] = "";
            currentMaster.Title.Breadcrumbs = pageTitleTabs;
        }

        if (!RequestHelper.IsPostBack() && (ti != null))
        {
            // Fills form with transformation information
            drpTransformationType.SelectedValue = ti.TransformationType.ToString();

            tbTransformationName.Text = ti.TransformationName;
            tbTransformationText.Text = ti.TransformationCode;
        }

        if (ti != null)
        {
            if (ti.TransformationCheckedOutByUserID > 0)
            {
                tbTransformationText.ReadOnly = true;
                string username = null;
                UserInfo ui = UserInfoProvider.GetUserInfo(ti.TransformationCheckedOutByUserID);
                if (ui != null)
                {
                    username = HTMLHelper.HTMLEncode(ui.FullName);
                }

                // Checked out by current machine
                if (ti.TransformationCheckedOutMachineName.ToLower() == HTTPHelper.MachineName.ToLower())
                {
                    lblCheckOutInfo.Text = String.Format(ResHelper.GetString("Transformation.CheckedOut"), Server.MapPath(ti.TransformationCheckedOutFilename));
                }
                else
                {
                    lblCheckOutInfo.Text = String.Format(ResHelper.GetString("Transformation.CheckedOutOnAnotherMachine"), ti.TransformationCheckedOutMachineName, username);
                }
            }
            else
            {
                lblCheckOutInfo.Text = String.Format(ResHelper.GetString("Transformation.CheckOutInfo"), Server.MapPath(TransformationInfoProvider.GetTransformationUrl(ti.TransformationFullName, null, ti.TransformationType)));
            }
        }

        pnlCheckOutInfo.Visible = (transformationId > 0);

        InitializeHeaderActions(ti);

        // Hide generate button DDL for code if not coupled document or custom table
        if (className != String.Empty)
        {
            DataClassInfo classInfo = DataClassInfoProvider.GetDataClass(className);
            if ((classInfo.ClassIsCustomTable) || (classInfo.ClassIsDocumentType && classInfo.ClassIsCoupledClass))
            {
                btnDefaultTransformation.Visible = true;
                // Hide code definition DDL if XSLT transformation type is selected
                drpTransformationCode.Visible = (drpTransformationType.SelectedValue == TransformationTypeEnum.Ascx.ToString());
            }
            else
            {
                drpTransformationCode.Visible = false;
                btnDefaultTransformation.Visible = false;
            }
        }
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        // Hide or display directives
        UpdateDirectives();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Disable items when virtual path provider is disabled
        if (!SettingsKeyProvider.UsingVirtualPathProvider && (ti != null) && (ti.TransformationType == TransformationTypeEnum.Ascx))
        {
            lblVirtualInfo.Text = String.Format(ResHelper.GetString("Transformation.VirtualPathProviderNotRunning"), TransformationInfoProvider.GetTransformationUrl(ti.TransformationFullName, null, TransformationTypeEnum.Ascx));
            plcVirtualInfo.Visible = true;
            pnlCheckOutInfo.Visible = false;
            tbTransformationText.Enabled = false;
        }
    }


    /// <summary>
    /// Initializes header action control
    /// </summary>
    private void InitializeHeaderActions(TransformationInfo ti)
    {
        // Header actions
        string[,] actions = new string[4, 11];

        // Save button
        actions[0, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[0, 1] = ResHelper.GetString("General.Save");
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        actions[0, 6] = "save";
        actions[0, 8] = "true";

        if (SettingsKeyProvider.UsingVirtualPathProvider || (ti != null) && (ti.TransformationType == TransformationTypeEnum.Xslt))
        {
            // CheckOut
            actions[1, 0] = HeaderActions.TYPE_SAVEBUTTON;
            actions[1, 1] = ResHelper.GetString("General.CheckOutToFile");
            actions[1, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkout.png");
            actions[1, 6] = "checkout";
            actions[1, 10] = "false";

            // CheckIn
            actions[2, 0] = HeaderActions.TYPE_SAVEBUTTON;
            actions[2, 1] = ResHelper.GetString("General.CheckInFromFile");
            actions[2, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkin.png");
            actions[2, 6] = "checkin";
            actions[2, 10] = "false";

            // UndoCheckOut
            actions[3, 0] = HeaderActions.TYPE_SAVEBUTTON;
            actions[3, 1] = ResHelper.GetString("General.UndoCheckout");
            actions[3, 2] = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("General.ConfirmUndoCheckOut")) + ");";
            actions[3, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/undocheckout.png");
            actions[3, 6] = "undocheckout";
            actions[3, 10] = "false";

            if (ti != null)
            {
                if (ti.TransformationCheckedOutByUserID > 0)
                {
                    // Checked out by current machine
                    if (ti.TransformationCheckedOutMachineName.ToLower() == HTTPHelper.MachineName.ToLower())
                    {
                        actions[2, 10] = "true";
                    }
                    if (CMSContext.CurrentUser.IsGlobalAdministrator)
                    {
                        actions[3, 10] = "true";
                    }
                }
                else
                {
                    actions[1, 10] = "true";
                }
            }
        }

        currentMaster.HeaderActions.LinkCssClass = "ContentSaveLinkButton";
        currentMaster.HeaderActions.ActionPerformed += HeaderActions_ActionPerformed;
        currentMaster.HeaderActions.Actions = actions;
    }


    /// <summary>
    /// Actions handler
    /// </summary>
    protected void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "checkout":
            case "save":

                // Validate transformation name for emptyness and code name format
                string result = new Validator().NotEmpty(tbTransformationName.Text, ResHelper.GetString("DocumentType_Edit_Transformation_Edit.TransformationNameRequired"))
                    .IsRegularExp(tbTransformationName.Text.Trim(), "^[A-Za-z][A-Za-z0-9_-]*$", ResHelper.GetString("DocumentType_Edit.TransformationNameFormat"))
                    .Result;

                if (result == "")
                {
                    if (drpTransformationType.SelectedValue.ToLower() == "xslt")
                    {
                        // Validates XSLT transformatin text
                        result = XMLValidator(tbTransformationText.Text);
                    }
                    if (result == "")
                    {
                        bool isSaved = false;
                        TransformationInfo newTransformation = null;
                        // Gets full transformation name in the form - "ClassName.TransformationName"
                        string fullTransformName;
                        if (className != "")
                        {
                            fullTransformName = className + "." + tbTransformationName.Text;
                        }
                        else
                        {
                            fullTransformName = tbTransformationName.Text;
                        }
                        // Finds whether new or edited transformation is unique in the document type
                        try
                        {
                            // Throws exception if transformation of the given name doesn't exist
                            newTransformation = TransformationInfoProvider.GetTransformation(fullTransformName);

                            if (newTransformation.TransformationID == transformationId)
                            {
                                SaveTransformation();
                                isSaved = true;
                            }
                            else
                            {
                                lblError.Visible = true;
                                lblError.Text = ResHelper.GetString("DocumentType_Edit_Transformation_Edit.UniqueTransformationName");
                            }
                        }
                        catch
                        {
                            SaveTransformation();
                            isSaved = true;
                        }

                        if (e.CommandName.ToLower() == "save")
                        {
                            if ((isSaved) && (!onlyCodeEdit))
                            {
                                UrlHelper.Redirect(EditingPage + "?transformationid=" + ti.TransformationID + "&saved=1");
                            }
                            if ((isSaved) && (onlyCodeEdit))
                            {
                                lblInfo.Visible = true;
                                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                            }
                        }
                        else
                        {
                            try
                            {
                                SiteManagerFunctions.CheckOutTransformation(transformationId);
                                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                            }
                            catch (Exception ex)
                            {
                                lblError.Text = ResHelper.GetString("Transformation.ErrorCheckout") + ": " + ex.Message;
                                lblError.Visible = true;
                                return;
                            }
                        }
                    }
                    else
                    {
                        // XML validation error
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("DocumentType_Edit_Transformation_Edit.XSLTTransformationError") + "'" + result + "'";
                    }
                }
                else
                {
                    // Required field validation error
                    lblError.Visible = true;
                    lblError.Text = result;
                }
                break;

            case "checkin":

                try
                {
                    SiteManagerFunctions.CheckInTransformation(transformationId);
                }
                catch (Exception ex)
                {
                    lblError.Text = ResHelper.GetString("Transformation.ErrorCheckin") + ": " + ex.Message;
                    lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;

            case "undocheckout":

                try
                {
                    SiteManagerFunctions.UndoCheckOutTransformation(transformationId);
                }
                catch (Exception ex)
                {
                    lblError.Text = ResHelper.GetString("Transformation.ErrorUndoCheckout") + ": " + ex.Message;
                    lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;
        }
    }


    /// <summary>
    /// Initializes labels
    /// </summary>
    private void LabelInit()
    {
        // Initializes labels
        lblTransformationName.Text = ResHelper.GetString("DocumentType_Edit_Transformation_Edit.TransformName");
        lblTransformationType.Text = ResHelper.GetString("DocumentType_Edit_Transformation_Edit.TransformType");
        lblTransformationCode.Text = ResHelper.GetString("DocumentType_Edit_Transformation_Edit.TransformCode");
        btnDefaultTransformation.Text = ResHelper.GetString("DocumentType_Edit_Transformation_Edit.ButtonDefault");
        string lang = DataHelper.GetNotEmpty(SettingsHelper.AppSettings["CMSProgrammingLanguage"], "C#");
        ltlDirectives.Text = "&lt;%@ Control Language=\"" + lang + "\" AutoEventWireup=\"true\" Inherits=\"CMS.Controls.CMSAbstractTransformation\" %&gt;<br />&lt;%@ Register TagPrefix=\"cc1\" Namespace=\"CMS.Controls\" Assembly=\"CMS.Controls\" %&gt;";
    }


    /// <summary>
    /// Initializes dropdown lists
    /// </summary>
    private void DropDownListInit()
    {
        // Initialize
        drpTransformationType.Items.Add(new ListItem(ResHelper.GetString("TransformationType.ASCX"), TransformationTypeEnum.Ascx.ToString()));
        drpTransformationType.Items.Add(new ListItem(ResHelper.GetString("TransformationType.XSLT"), TransformationTypeEnum.Xslt.ToString()));

        drpTransformationCode.Items.Add(new ListItem(ResHelper.GetString("TransformationTypeCode.Default"), DefaultTransformationTypeEnum.Default.ToString()));
        drpTransformationCode.Items.Add(new ListItem(ResHelper.GetString("TransformationTypeCode.Atom"), DefaultTransformationTypeEnum.Atom.ToString()));
        drpTransformationCode.Items.Add(new ListItem(ResHelper.GetString("TransformationTypeCode.RSS"), DefaultTransformationTypeEnum.RSS.ToString()));
        drpTransformationCode.Items.Add(new ListItem(ResHelper.GetString("TransformationTypeCode.XML"), DefaultTransformationTypeEnum.XML.ToString()));
    }


    /// <summary>
    /// Checks whether XSLT transformation text is valid
    /// </summary>
    /// <param name="xmlText">XML text.</param>
    /// <returns>Error message.</returns>
    protected string XMLValidator(string xmlText)
    {
        // Creates memory stream from transformation text
        Stream stream = new MemoryStream();
        StreamWriter writer = new StreamWriter(stream);
        writer.Write(xmlText);
        writer.Flush();
        stream.Seek(0, SeekOrigin.Begin);
        // New xml text reader from the stream
        XmlTextReader tr = new XmlTextReader(stream);
        try
        {
            // Need to read the data to validate
            while (tr.Read()) ;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

        return "";
    }


    /// <summary>
    /// Default Transformation Click event
    /// </summary>
    protected void btnDefaultTransformation_Click(object sender, EventArgs e)
    {
        DefaultTransformation();
        UpdateDirectives();
    }


    /// <summary>
    /// Generates transformation code depending on the transformation type (Xslt, Ascx)
    /// </summary>
    private void DefaultTransformation()
    {
        // Gets Xml schema of the document type
        DataClassInfo dci = DataClassInfoProvider.GetDataClass(className);
        string formDef = "";
        if (dci != null)
        {
            formDef = dci.ClassFormDefinition;
        }

        // Gets transformation type
        TransformationTypeEnum transformType = TransformationTypeEnum.Ascx;
        DefaultTransformationTypeEnum transformCode = DefaultTransformationTypeEnum.Default;
        if (drpTransformationType.SelectedValue == TransformationTypeEnum.Ascx.ToString())
        {
            switch (drpTransformationCode.SelectedValue)
            {
                // Atom transformation code
                case "Atom":
                    transformCode = DefaultTransformationTypeEnum.Atom;
                    break;

                // RSS transformation code
                case "RSS":
                    transformCode = DefaultTransformationTypeEnum.RSS;
                    break;

                // XML transformation code
                case "XML":
                    transformCode = DefaultTransformationTypeEnum.XML;
                    break;

                // Default ASCX transformation code
                default:
                    transformCode = DefaultTransformationTypeEnum.Default;
                    break;
            }

        }
        else
        {
            transformType = TransformationTypeEnum.Xslt;
        }
        // Writes the result to the text box
        tbTransformationText.Text = TransformationInfoProvider.GenerateTransformationCode(formDef, transformType, className, transformCode);
    }


    /// <summary>
    /// Saves new or edited transformation of the given name and returns to the transformation list.
    /// </summary>
    private void SaveTransformation()
    {
        // Sets transformation object's properties
        ti.TransformationName = tbTransformationName.Text;

        DataClassInfo dci = DataClassInfoProvider.GetDataClass(className);
        if (dci != null)
        {
            ti.TransformationClassID = dci.ClassID;


            ti.TransformationType = (drpTransformationType.SelectedValue == "Ascx") ? TransformationTypeEnum.Ascx : TransformationTypeEnum.Xslt;

            if (ti.TransformationCheckedOutByUserID <= 0)
            {
                ti.TransformationCode = tbTransformationText.Text;
            }

            TransformationInfoProvider.SetTransformation(ti);
        }
    }


    protected void drpTransformationType_SelectedIndexChanged(object sender, EventArgs e)
    {
        UpdateDirectives();
    }


    private void UpdateDirectives()
    {
        // Hide or display directives and examples
        if (drpTransformationType.SelectedValue.ToLower() == "ascx")
        {
            lnkHelp.Visible = true;
            ltlDirectives.Visible = true;
            plcTransformationCode.Visible = true;
        }
        else
        {
            lnkHelp.Visible = false;
            ltlDirectives.Visible = false;
            plcTransformationCode.Visible = false;
        }
    }
}
