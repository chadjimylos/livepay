<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyMessages_ContactList.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Messaging_MyMessages_MyMessages_ContactList"
    Title="My messages - Contact list" ValidateRequest="false" Theme="Default" %>

<%@ Register Src="~/CMSModules/Messaging/Controls/ContactList.ascx" TagName="ContactList"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:ContactList ID="contactListElem" runat="server" IsLiveSite="false" ShowItemAsLink="false" />
</asp:Content>
