using System;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Countries_Country_Edit_Header : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string currentCountry = "";

        int countryId = QueryHelper.GetInteger("countryid", 0);
        CountryInfo ci = CountryInfoProvider.GetCountryInfo(countryId);
        if (ci != null)
        {
            currentCountry = ResHelper.LocalizeString(ci.CountryDisplayName);
        }

        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("Country_Edit.ItemListLink");
        breadcrumbs[0, 1] = "~/CMSSiteManager/Development/Countries/Country_List.aspx";
        breadcrumbs[0, 2] = "_parent";
        breadcrumbs[1, 0] = currentCountry;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Country_Edit.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Country/object.png");
        this.CurrentMaster.Title.HelpTopicName = "new_country";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        string[,] tabs = new string[8, 4];
        tabs[0, 0] = ResHelper.GetString("general.general");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'new_country');";
        tabs[0, 2] = "Country_Edit_General.aspx?countryID=" + countryId;
        tabs[1, 0] = ResHelper.GetString("Country_Edit.States");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'states_list');";
        tabs[1, 2] = "Country_Edit_State_List.aspx?countryID=" + countryId;

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "content";
    }
}
