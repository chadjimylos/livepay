using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.Ecommerce;
using CMS.FormControls;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;
using CMS.LicenseProvider;
using CMS.SettingsProvider;
using CMS.SiteProvider;

public partial class CMSModules_Ecommerce_Content_Product_Product_Edit_General : CMSContentPage
{
    protected int nodeId = 0;
    protected int productId = 0;

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        this["TabControl"] = BasicTabControlMenu;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        lblInfo.Visible = !string.IsNullOrEmpty(lblInfo.Text);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentUserInfo currentUser = CMSContext.CurrentUser;

        if (currentUser != null)
        {
            // Check the license
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Ecommerce);

            // Check site availability
            if (!ResourceSiteInfoProvider.IsResourceOnSite("CMS.Ecommerce", CMSContext.CurrentSiteName))
            {
                RedirectToResourceNotAvailableOnSite("CMS.Ecommerce");
            }

            // Check 'EcommerceRead' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceRead"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "EcommerceRead");
            }

            productId = ValidationHelper.GetInteger(Request.QueryString["productID"], 0);
            nodeId = ValidationHelper.GetInteger(Request.QueryString["nodeID"], 0);

            TreeProvider tree = new TreeProvider(currentUser);
            TreeNode node = DocumentHelper.GetDocument(nodeId, currentUser.PreferredCultureCode, tree);
            if (node != null)
            {
                // Check read permissions
                if (currentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                {
                    RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
                }
                else
                {
                    // Check modify permissions
                    if (currentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                    {
                        // disable form editing                                                            
                        DisableFormEditing();

                        // show access denied message
                        lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                    }
                    // check User - Department association
                    else if (!currentUser.IsGlobalAdministrator)
                    {
                        SKUInfo skuObj = SKUInfoProvider.GetSKUInfo(productId);
                        if (skuObj != null)
                        {
                            if (!DepartmentInfoProvider.IsUserInDepartment(skuObj.SKUDepartmentID, currentUser.UserID))
                            {
                                // disable form editing                                                            
                                DisableFormEditing();

                                DepartmentInfo departmentObj = DepartmentInfoProvider.GetDepartmentInfo(skuObj.SKUDepartmentID);
                                if (departmentObj != null)
                                {
                                    // show access denied message
                                    lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtperdepartment"), departmentObj.DepartmentName);
                                }
                            }
                        }
                    }


                    ctrlProduct.ProductID = productId;
                    ctrlProduct.NodeID = nodeId;
                    ctrlProduct.OnOkClickHandler += new EventHandler(NewProduct1_OnOkClickHandler);

                    // custom fields
                    DataForm df = new DataForm();
                    df.ClassName = "Ecommerce.SKU";
                    df.ItemID = productId;

                    int showCustomFields = (df.BasicForm.FormInformation.GetFormElements(true, false).Count <= 0 ? 0 : 1); ;

                    BasicTabControlMenu.Tabs = new string[5 + showCustomFields, 4];
                    int lastTabIndex = 0;

                    BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("general.general");
                    BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_General.aspx?productId=" + ctrlProduct.ProductID + "&nodeId=" + ctrlProduct.NodeID;
                    lastTabIndex++;

                    if (showCustomFields != 0)
                    {
                        BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.CustomFields");
                        BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_CustomFields.aspx?productId=" + ctrlProduct.ProductID + "&nodeId=" + ctrlProduct.NodeID;
                        lastTabIndex++;
                    }

                    BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.Tax");
                    BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_Tax.aspx?productId=" + ctrlProduct.ProductID + "&nodeId=" + ctrlProduct.NodeID;
                    lastTabIndex++;

                    BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.VolumeDiscounts");
                    BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_VolumeDiscounts.aspx?productId=" + ctrlProduct.ProductID + "&nodeId=" + ctrlProduct.NodeID;
                    lastTabIndex++;

                    BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.Options");
                    BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_Options.aspx?productId=" + ctrlProduct.ProductID + "&nodeId=" + ctrlProduct.NodeID;
                    lastTabIndex++;

                    BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.Documents");
                    BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_Documents.aspx?productId=" + ctrlProduct.ProductID + "&nodeId=" + ctrlProduct.NodeID;
                    lastTabIndex++;

                    BasicTabControlMenu.UrlTarget = "_self";

                    chkMarkDocAsProd.Text = ResHelper.GetString("Product_Selection.DocIsAsProd");
                }
            }
        }
    }


    protected void chkMarkDocAsProd_CheckedChanged(object sender, EventArgs e)
    {
        ctrlProduct.FormEnabled = chkMarkDocAsProd.Checked;
    }


    /// <summary>
    /// On OK click redirect to edit page
    /// </summary>
    void NewProduct1_OnOkClickHandler(object sender, EventArgs e)
    {
        //int nodeId = ValidationHelper.GetInteger(((CMSFormControls_NewProduct)sender).NodeID, 0);
        //int productId = ValidationHelper.GetInteger(((CMSFormControls_NewProduct)sender).ProductID, 0);

        if (!chkMarkDocAsProd.Checked)
        {
            TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
            TreeNode node = tree.SelectSingleNode(nodeId, TreeProvider.ALL_CULTURES);
            node.NodeSKUID = 0;
            DocumentHelper.UpdateDocument(node, tree);

            UrlHelper.Redirect("Product_Selection.aspx?nodeid=" + nodeId);
        }
        else
        {
           // UrlHelper.Redirect("Product_Edit_General.aspx?productID=" + productId + "&saved=1&nodeID=" + nodeId);
        }
    }


    /// <summary>
    /// Disables form editing.
    /// </summary>
    protected void DisableFormEditing()
    {
        this.chkMarkDocAsProd.Enabled = false;
        this.ctrlProduct.FormEnabled = false;
        this.ctrlProduct.ButtonEnabled = false;
    }
}
