using System;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.LicenseProvider;

public partial class CMSSiteManager_Administration_UIPersonalization_UI_Header : CMSUIPersonalizationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize the master page elements
        InitializeMasterPage();
    }


    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    private void InitializeMasterPage()
    {
        // Set the master page title
        CurrentMaster.Title.TitleText = ResHelper.GetString("uiprofile.personalization");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_UIElement/object.png");
        CurrentMaster.Title.HelpTopicName = "UIPersonalization_Dialogs";
        CurrentMaster.Title.HelpName = "helpTopic";

        int siteId = QueryHelper.GetInteger("siteId", 0);

        // Set the tabs
        string[,] tabs = new string[2, 8];
        tabs[0, 0] = ResHelper.GetString("uiprofile.dialogs");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'UIPersonalization_Dialogs');";
        tabs[0, 2] = "UI_Dialogs.aspx?siteId=" + siteId;
        tabs[1, 0] = ResHelper.GetString("uiprofile.editor");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'UIPersonalization_Editor');";
        tabs[1, 2] = "UI_Editor.aspx?siteId=" + siteId;

        CurrentMaster.Tabs.UrlTarget = "uiContent";
        CurrentMaster.Tabs.Tabs = tabs;
    }
}
