using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Customers_Customer_Edit_Credit_Edit : CMSEcommercePage
{
	protected int eventid = 0;
    protected int customerId = 0;
	
	protected void Page_Load(object sender, EventArgs e)
	{
        string currentCreditEvent = "";
        string eventImage = "";
        string title = "";

		// Required field validator error messages initialization
        rfvEventName.ErrorMessage = ResHelper.GetString("Ecommerce.Customer.CreditEventNameEmpty");
				
		// control initializations				
		lblEventName.Text = ResHelper.GetString("CreditEvent_Edit.EventNameLabel");		
		lblEventDescription.Text = ResHelper.GetString("CreditEvent_Edit.EventDescriptionLabel");
		lblEventDate.Text = ResHelper.GetString("CreditEvent_Edit.EventDateLabel");
		lblEventCreditChange.Text = ResHelper.GetString("CreditEvent_Edit.EventCreditChangeLabel");
		btnOk.Text = ResHelper.GetString("General.OK");
        dtPickerEventDate.SupportFolder = "~/CMSAdminControls/Calendar";

		// get credit event id and customer id from querystring
        customerId = ValidationHelper.GetInteger(Request.QueryString["customerid"], 0);
        eventid = ValidationHelper.GetInteger(Request.QueryString["eventid"], 0);

        // Edit existin credit event
        if (eventid > 0)
        {
            CreditEventInfo creditEventObj = CreditEventInfoProvider.GetCreditEventInfo(eventid);
            if (creditEventObj != null)
            {
                currentCreditEvent = creditEventObj.EventName;                

                // Fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(creditEventObj);

                    // Show that the creditEvent was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }

            eventImage = "creditevent.png";
            title = ResHelper.GetString("CreditEvent_Edit.HeaderCaption");
        }
        // Create new credit event
        else
        {
            currentCreditEvent = ResHelper.GetString("CreditEvent_Edit.NewItemCaption");
            if (!RequestHelper.IsPostBack())
            {
                dtPickerEventDate.SelectedDateTime = DateTime.Now;
            }
            eventImage = "newcreditevent.png";
            title = ResHelper.GetString("CreditEvent_Edit.NewItemCaption");
        }


		// initializes page title control		
        InitializeMasterPage(title, eventImage, currentCreditEvent);
	}


    /// <summary>
    /// Initializes master page
    /// </summary>
    private void InitializeMasterPage(string title, string eventImage, string currentCreditEvent)
    {
        // Set the master page title element
        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.Title.HelpTopicName = "newedit_credit_event";

        // Set the tabs
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("CreditEvent_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Customers/Customer_Edit_Credit_List.aspx?customerid=" + customerId;
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentCreditEvent;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


	/// <summary>
	/// Load data of editing creditEvent.
	/// </summary>
	/// <param name="creditEventObj">CreditEvent object.</param>
	protected void LoadData(CreditEventInfo creditEventObj)
	{	
		txtEventName.Text = creditEventObj.EventName;		
		txtEventDescription.Text = creditEventObj.EventDescription;		
		txtEventCreditChange.Text = Convert.ToString(creditEventObj.EventCreditChange);
        dtPickerEventDate.SelectedDateTime = creditEventObj.EventDate;
	 }


	/// <summary>
	/// Sets data to database.
	/// </summary>
	protected void btnOK_Click(object sender, EventArgs e)
	{
        // Check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            CMSPage.RedirectToCMSDeskAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

		string errorMessage = ValidateForm(); 

		if (errorMessage == "")
		{
			CreditEventInfo creditEventObj = CreditEventInfoProvider.GetCreditEventInfo(eventid);
			
			// if creditEvent doesnt already exist, create new one
			if (creditEventObj == null)
			{
				creditEventObj = new CreditEventInfo();
                creditEventObj.EventCustomerID = customerId;                
			}

			creditEventObj.EventName = txtEventName.Text.Trim();				
			creditEventObj.EventDescription = txtEventDescription.Text.Trim();				
			creditEventObj.EventCreditChange = Convert.ToDouble(txtEventCreditChange.Text.Trim());
            creditEventObj.EventDate = dtPickerEventDate.SelectedDateTime;
				
			CreditEventInfoProvider.SetCreditEventInfo(creditEventObj);

			UrlHelper.Redirect("Customer_Edit_Credit_Edit.aspx?customerid=" + customerId + "&eventid=" + Convert.ToString(creditEventObj.EventID) + "&saved=1");
		}
		else
		{
			lblError.Visible = true;
			lblError.Text = errorMessage;
		}
	}


    /// <summary>
    /// Validates form input data and returns error message if occures.
    /// </summary>
    private string ValidateForm()
    {
        string errorMessage = "";

        // Event name must be not empty
        if (txtEventName.Text.Trim() == "")
        {
            errorMessage = rfvEventName.ErrorMessage;
        }

        if (errorMessage == "")
        {
            // Credit event change must be none zero value in double format
            if (ValidationHelper.GetDouble(txtEventCreditChange.Text.Trim(), 0) == 0)
            {
                errorMessage = ResHelper.GetString("Ecommerce.Customer.CreditChangeFormat");
            }
        }

        if (errorMessage == "")
        {
            // Credit event date must be selected
            if (dtPickerEventDate.SelectedDateTime == DataHelper.DATETIME_NOT_SELECTED)
            {
                errorMessage = ResHelper.GetString("Ecommerce.Customer.CreditDateEmpty");
            }
            // Credit event date has wrong format
            else if (ValidationHelper.GetDateTime(dtPickerEventDate.SelectedDateTime, DataHelper.DATETIME_NOT_SELECTED) == DataHelper.DATETIME_NOT_SELECTED)
            {
                errorMessage = ResHelper.GetString("Ecommerce.Customer.CreditDateFormat");
            }
        }

        return errorMessage;
    }
}
