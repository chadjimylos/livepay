using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_Roles_Role_Edit_Frameset : CMSRolesPage
{
    protected string parameters = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        parameters += "?roleid=" + (Request.QueryString["roleid"]).ToString(); 
        if ((Request.QueryString["siteid"] != null) && (Request.QueryString["siteid"].ToString() != ""))
        {
            parameters += "&siteid=" + this.GetSiteID(Request.QueryString["siteid"]).ToString();
        }
        else if (Request.QueryString["selectedsiteid"] != null)
        {
            parameters += "&selectedsiteid=" + this.GetSiteID(Request.QueryString["selectedsiteid"]).ToString();
        }
    }
}
