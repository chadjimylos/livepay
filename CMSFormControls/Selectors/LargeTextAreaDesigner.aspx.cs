using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSFormControls_Selectors_LargeTextAreaDesigner : MessagePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize UI
        CurrentMaster.Title.TitleText = ResHelper.GetString("EditingFormControl.TitleText");
        CurrentMaster.Title.TitleImage = GetImageUrl("Design/Controls/EditingFormControl/title.png");

        // Get id of textbox
        string mId = QueryHelper.GetString("areaid", "");


        btnOk.Text = ResHelper.GetString("general.ok");
        btnCancel.Text = ResHelper.GetString("general.cancel");

        bool allowMacros = QueryHelper.GetBoolean("allowMacros", true);
        if (allowMacros)
        {
            macroSelectorElem.Resolver = MacroSelector.EmailTemplateResolver;
            macroSelectorElem.JavaScripFunction = "InsertMacroPlain";
        }
        else
        {
            macroSelectorElem.Visible = false;
        }

        btnOk.OnClientClick += "Set(); return false;";
        btnCancel.OnClientClick += "window.close(); return false;";

        string script =
            "var txtElem = document.getElementById('" + txtLargeArea.ClientID + "');\n" +
            "function Load() {  txtElem.value = wopener.GetAreaValue('" + mId + "');  return false;}\n" +
            "function Set() { wopener.SetAreaValue('" + mId + "', txtElem.value); window.close(); }\n" +
            "Load();";

        // Register macro scripts
        RegisterModalPageScripts();
        RegisterEscScript();


        this.ltlScript.Text = ScriptHelper.GetScript(script);
    }


    /// <summary>
    /// Disable handler base tag
    /// </summary>
    protected override void OnInit(EventArgs e)
    {
        UseBaseTagForHandlerPage = false;
        base.OnInit(e);
    }
}