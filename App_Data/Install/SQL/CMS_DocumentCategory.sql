CREATE TABLE [CMS_DocumentCategory] (
		[DocumentID]     int NOT NULL,
		[CategoryID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_DocumentCategory]
	ADD
	CONSTRAINT [PK_CMS_DocumentCategory]
	PRIMARY KEY
	([DocumentID], [CategoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_DocumentCategory]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Category]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_DocumentCategory_CategoryID_CMS_Category]') IS NULL
BEGIN
		ALTER TABLE [CMS_DocumentCategory]
			ADD CONSTRAINT [FK_CMS_DocumentCategory_CategoryID_CMS_Category]
			FOREIGN KEY ([CategoryID]) REFERENCES [CMS_Category] ([CategoryID])
END
IF OBJECT_ID(N'[CMS_DocumentCategory]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Document]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_DocumentCategory_DocumentID_CMS_Document]') IS NULL
BEGIN
		ALTER TABLE [CMS_DocumentCategory]
			ADD CONSTRAINT [FK_CMS_DocumentCategory_DocumentID_CMS_Document]
			FOREIGN KEY ([DocumentID]) REFERENCES [CMS_Document] ([DocumentID])
END
