<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TaxID.ascx.cs" Inherits="CMSFormControls_TaxID" %>

<div runat="server" id="CustomCloud_div"></div>

<asp:TextBox runat="server" cssclass="RegUsersTab1CustomTxt" width="260px" ID="TaxID" MaxLength="9" />

<div class="HideIt">
    <asp:CustomValidator ID="cv_TaxID" 
                        ValidateEmptyText="true" runat="server"   
                        ControlToValidate="TaxID" ValidationGroup="Tab1_PayForm" />
</div>