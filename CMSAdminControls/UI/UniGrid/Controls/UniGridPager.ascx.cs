﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.Controls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;

public partial class CMSAdminControls_UI_UniGrid_Controls_UniGridPager : UniGridPager
{
    #region "Variables"

    protected string mBackgroundStyle = null;

    #endregion

    #region "Properties"

    /// <summary>
    /// UniPager control.
    /// </summary>
    public override UniPager UniPager
    {
        get
        {
            return this.pagerElem;
        }
    }


    /// <summary>
    /// PageSize dropdown control.
    /// </summary>
    public override DropDownList PageSizeDropdown
    {
        get
        {
            return this.drpPageSize;
        }
    }


    /// <summary>
    /// Default page size at first load.
    /// </summary>
    public override int DefaultPageSize
    {
        get
        {
            return base.DefaultPageSize;
        }
        set
        {
            base.DefaultPageSize = value;
            SetupControls(true);
        }
    }


    /// <summary>
    /// Page size values separates with comma. 
    /// Macro ##ALL## indicates that all the results will be displayed at one page.
    /// </summary>
    public override string PageSizeOptions
    {
        get
        {
            return base.PageSizeOptions;
        }
        set
        {
            base.PageSizeOptions = value;
            SetupControls(true);
        }
    }

    #endregion

    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        SetupControls();

        base.OnInit(e);
    }


    protected override void OnLoad(EventArgs e)
    {
        SetupControls();

        base.OnLoad(e);
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        plcPageSize.Visible = (ShowPageSize && (drpPageSize.Items.Count > 1));

        // Handle pager only if visible
        if (pagerElem.Visible)
        {
            if (UniPager.PageCount > UniPager.GroupSize)
            {
                if (UniPager.PageCount > 20)
                {
                    Control drpPage = ControlsHelper.GetChildControl(UniPager, typeof(DropDownList), "drpPage");
                    if (drpPage != null)
                    {
                        drpPage.Visible = false;
                    }
                }
                else
                {
                    Control txtPage = ControlsHelper.GetChildControl(UniPager, typeof(TextBox), "txtPage");
                    if (txtPage != null)
                    {
                        txtPage.Visible = false;
                    }
                }
            }
            else
            {
                // Remove direct page control if only one group of pages is  shown
                Control plcDirectPage = ControlsHelper.GetChildControl(UniPager, typeof(PlaceHolder), "plcDirectPage");
                if (plcDirectPage != null)
                {
                    plcDirectPage.Controls.Clear();
                }
            }
        }

        // Hide entire control if pager and page size drodown is hidden
        if (!plcPageSize.Visible && !pagerElem.Visible)
        {
            this.Visible = false;
        }
    }


    protected void drpPageSize_SelectedIndexChanged(object sender, EventArgs e)
    {
        UniPager.CurrentPage = 1;
        UniPager.PageSize = ValidationHelper.GetInteger(drpPageSize.SelectedValue, -1);
        
        if (UniPager.PagedControl != null)
        {
            UniPager.PagedControl.ReBind();
        }
    }

    #endregion

    #region "Private methods"

    private void SetupControls()
    {
        SetupControls(false);
    }


    private void SetupControls(bool forceReload)
    {
        string browserClass = BrowserHelper.GetBrowserClass();
        if (!String.IsNullOrEmpty(browserClass) && browserClass.StartsWith("IE"))
        {
            mBackgroundStyle = "background-position: left top;";
        }
        else
        {
            mBackgroundStyle = "background: none;";
        }
        drpPageSize.SelectedIndexChanged += new EventHandler(drpPageSize_SelectedIndexChanged);
        SetPageSize(forceReload);

        UniPager.HidePagerForSinglePage = true;
        UniPager.PagerMode = UniPagerMode.PostBack;
        UniPager.PageSize = ValidationHelper.GetInteger(drpPageSize.SelectedValue, -1);
        UniPager.DirectPageControlID = DirectPageControlID;
    }


    /// <summary>
    /// Set page size dropdown list according to PageSize property
    /// </summary>
    private void SetPageSize(bool forceReload)
    {
        if ((drpPageSize.Items.Count == 0) || forceReload)
        {
            string[] sizes = PageSizeOptions.Split(',');
            drpPageSize.Items.Clear();
            foreach (string size in sizes)
            {
                ListItem item = null;
                if (size.ToUpper() != UniGrid.ALL)
                {
                    item = new ListItem(size);
                }
                else
                {
                    item = new ListItem(ResHelper.GetString("general.selectall"), "-1");
                }

                if (item.Value == DefaultPageSize.ToString())
                {
                    item.Selected = true;
                }

                drpPageSize.Items.Add(item);
            }
        }
    }

    #endregion
}
