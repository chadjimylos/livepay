<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebPartZoneProperties.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_WebParts_WebPartZoneProperties" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master" Title="Web part zone - Properties" %>

<%@ Register Src="~/CMSModules/PortalEngine/Controls/WebParts/WebPartZoneProperties.ascx"
    TagName="WebPartZoneProperties" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <div class="WebPartZoneProperties">
        <div class="PageContent">
            <cms:WebPartZoneProperties ID="webPartZonePropertiesElem" runat="server" />
        </div>
    </div>
    <br />
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
</asp:Content>
<asp:Content ID="cntFooter" ContentPlaceHolderID="plcFooter" runat="server">
    <div class="FloatLeft">
        <asp:CheckBox runat="server" ID="chkRefresh" Checked="true" />
    </div>
    <div class="FloatRight">
        <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" Visible="true" OnClick="btnOK_Click" /><cms:CMSButton
            ID="btnCancel" runat="server" CssClass="SubmitButton" OnClientClick="window.close(); return false;" /><cms:CMSButton
                ID="btnApply" runat="server" CssClass="SubmitButton" Visible="true" OnClick="btnApply_Click" />
    </div>
</asp:Content>
