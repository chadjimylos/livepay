using System;

using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.FormEngine;
using CMS.EventLog;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Membership_FormControls_Roles_SecurityAddRoles : FormEngineUserControl
{
    #region "Private variables"

    private int mNodeID = 0;
    private int mPollID = 0;
    private int mFormID = 0;
    private int mBoardID = 0;
    private int mGroupID = 0;
    private string mCurrentValues = string.Empty;
    private bool mShowSiteFilter = true;
    private TreeNode mNode = null;
    private TreeProvider mTree = null;
    private EventLogProvider mEventLog = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Tree provider
    /// </summary>
    protected TreeProvider Tree
    {
        get
        {
            if (mTree == null)
            {
                mTree = new TreeProvider(CMSContext.CurrentUser);
            }
            return mTree;
        }
    }


    /// <summary>
    /// Event log provider
    /// </summary>
    protected EventLogProvider EventLog
    {
        get
        {
            if (mEventLog == null)
            {
                mEventLog = new EventLogProvider(Tree.Connection);
            }
            return mEventLog;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets node id.
    /// </summary>
    public int NodeID
    {
        get
        {
            return mNodeID;
        }
        set
        {
            // Clear TreeNode on id change
            if (mNodeID != value)
            {
                mNode = null;
            }

            mNodeID = value;
        }
    }


    /// <summary>
    /// Gets or sets the TreeNode
    /// </summary>
    public TreeNode Node
    {
        get
        {
            if ((mNode == null) && (this.NodeID > 0))
            {
                // Get node
                mNode = Tree.SelectSingleNode(this.NodeID);
            }
            return mNode;
        }
        set
        {
            mNode = value;
            // Update NodeID
            if (mNode != null)
            {
                this.NodeID = mNode.NodeID;
            }
        }
    }


    /// <summary>
    /// Gets or sets poll id.
    /// </summary>
    public int PollID
    {
        get
        {
            return mPollID;
        }
        set
        {
            mPollID = value;
        }
    }


    /// <summary>
    /// Gets or sets bizform id.
    /// </summary>
    public int FormID
    {
        get
        {
            return mFormID;
        }
        set
        {
            mFormID = value;
        }
    }


    /// <summary>
    /// Gets or sets board id.
    /// </summary>
    public int BoardID
    {
        get
        {
            return mBoardID;
        }
        set
        {
            mBoardID = value;
        }
    }


    /// <summary>
    /// Gets or sets group id.
    /// </summary>
    public int GroupID
    {
        get
        {
            return mGroupID;
        }
        set
        {
            mGroupID = value;
        }
    }


    /// <summary>
    /// Returns current uniselector.
    /// </summary>
    public UniSelector CurrentSelector
    {
        get
        {
            return usRoles;
        }
    }


    /// <summary>
    /// Gets or sets subscriber.
    /// </summary>
    public string CurrentValues
    {
        get
        {
            return mCurrentValues;
        }
        set
        {
            mCurrentValues = value;
        }
    }


    /// <summary>
    /// Gets or sets if live site property.
    /// </summary>
    public override bool IsLiveSite
    {
        get
        {
            return base.IsLiveSite;
        }
        set
        {
            base.IsLiveSite = value;
            usRoles.IsLiveSite = value;
        }
    }


    /// <summary>
    /// Gets or sets if site filter is should be shown.
    /// </summary>
    public bool ShowSiteFilter
    {
        get
        {
            return mShowSiteFilter;
        }
        set
        {
            mShowSiteFilter = value;
        }
    }

    #endregion


    #region "Events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (GroupID > 0)
        {
            usRoles.WhereCondition = "RoleGroupID=" + GroupID;
        }
        else
        {
            // Set up groups where condition
            usRoles.WhereCondition = "RoleGroupID IS NULL";

            if (ShowSiteFilter)
            {
                // Add sites filter
                usRoles.FilterControl = "~/CMSFormControls/Filters/SiteFilter.ascx";
                usRoles.SetValue("DefaultFilterValue", CMSContext.CurrentSiteID);
                usRoles.SetValue("FilterMode", "role");
            }
            else
            {
                usRoles.WhereCondition += " AND SiteID = " + CMSContext.CurrentSiteID;
            }
        }

        // Check node permissions
        if (this.Node != null)
        {
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(this.Node, NodePermissionsEnum.ModifyPermissions) != AuthorizationResultEnum.Allowed)
            {
                usRoles.Enabled = false;
                return;
            }
        }

        // Check bizform 'EditForm' permission
        if (this.FormID > 0)
        {
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "EditForm"))
            {
                usRoles.Enabled = false;
                return;
            }
        }

        // Check poll permission
        if (this.PollID > 0)
        {
            if (GroupID > 0)
            {
                if (!CMSContext.CurrentUser.IsGroupAdministrator(GroupID))
                {
                    usRoles.Enabled = false;
                    return;
                }
            }
            else
            {
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.polls", "Modify"))
                {
                    usRoles.Enabled = false;
                    return;
                }
            }
        }

        // Check message board permission
        if (this.BoardID > 0)
        {
            IInfoObject boardObj = ModuleCommands.MessageBoardGetMessageBoardInfo(this.BoardID);
            if (boardObj != null)
            {
                int boardGroupId = ValidationHelper.GetInteger(boardObj.GetValue("BoardGroupID"), 0);
                if (boardGroupId > 0)
                {
                    if (!CMSContext.CurrentUser.IsGroupAdministrator(boardGroupId))
                    {
                        usRoles.Enabled = false;
                        return;
                    }
                }
                else if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.MessageBoards", "Modify"))
                {
                    usRoles.Enabled = false;
                    return;
                }
            }
        }

        // Add event handling
        usRoles.OnItemsSelected += usRoles_OnItemsSelected;
    }


    /// <summary>
    /// Reloads control data.
    /// </summary>
    public void ReloadData()
    {
        usRoles.Reload(true);
    }


    /// <summary>
    /// On items selected event handling.
    /// </summary>    
    void usRoles_OnItemsSelected(object sender, EventArgs e)
    {
        AclProvider aclProv = null;

        // Create Acl provider to current treenode
        if (this.Node != null)
        {
            aclProv = new AclProvider(Tree);
        }

        // Remove old items
        string newValues = ValidationHelper.GetString(usRoles.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, CurrentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int roleID = ValidationHelper.GetInteger(item, 0);

                    if (PollID > 0)
                    {
                        // Remove role from poll
                        ModuleCommands.PollsRemoveRoleFromPoll(roleID, PollID);
                    }
                    else if (FormID > 0)
                    {
                        // Remove role from form
                        BizFormInfoProvider.RemoveRoleFromForm(roleID, FormID);
                    }
                    else if (BoardID > 0)
                    {
                        // Remove message board from board
                        ModuleCommands.MessageBoardRemoveRoleFromBoard(roleID, BoardID);
                    }
                    else if (this.Node != null)
                    {
                        if (aclProv != null)
                        {
                            // Remove role from treenode
                            aclProv.RemoveRole(this.NodeID, roleID);
                        }
                    }
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(CurrentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to site
                foreach (string item in newItems)
                {
                    int roleID = ValidationHelper.GetInteger(item, 0);

                    if (PollID > 0)
                    {
                        // Add poll role
                        ModuleCommands.PollsAddRoleToPoll(roleID, PollID);
                    }
                    else if (FormID > 0)
                    {
                        // Add BizForm role
                        BizFormInfoProvider.AddRoleToForm(roleID, FormID);
                    }
                    else if (BoardID > 0)
                    {
                        // Add role to the message board
                        ModuleCommands.MessageBoardAddRoleToBoard(roleID, BoardID);
                    }
                    else if (this.Node != null)
                    {
                        // Add role to treenode
                        if (aclProv != null)
                        {
                            aclProv.SetRolePermissions(this.Node, 0, 0, roleID);
                        }
                    }
                }
            }
        }

        RaiseOnChanged();
    }

    #endregion
}
