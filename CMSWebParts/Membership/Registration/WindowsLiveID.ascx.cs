using System;
using System.Web.UI.WebControls;
using System.Web.Security;

using CMS.PortalControls;
using CMS.SettingsProvider;
using CMS.MembershipProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.PortalEngine;

public partial class CMSWebParts_Membership_Registration_WindowsLiveID : CMSAbstractWebPart
{
    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether to show sign out link
    /// </summary>
    public bool ShowSignOut
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("ShowSignOut"), false);
        }
        set
        {
            SetValue("ShowSignOut", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that buttons will be used instead of links
    /// </summary>
    public bool ShowAsButton
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("ShowAsButton"), false);
        }
        set
        {
            SetValue("ShowAsButton", value);
        }
    }


    /// <summary>
    /// Get or sets sign in button image URL
    /// </summary>
    public string SignInImageURL
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("SignInImageURL"), GetImageUrl("Others/LiveID/signin.gif"));
        }
        set
        {
            SetValue("SignInImageURL", value);
        }
    }


    /// <summary>
    /// Get or sets sign out button image URL
    /// </summary>
    public string SignOutImageURL
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("SignOutImageURL"), GetImageUrl("Others/LiveID/signout.gif"));
        }
        set
        {
            SetValue("SignOutImageURL", value);
        }
    }


    /// <summary>
    /// Get or sets sign in text
    /// </summary>
    public string SignInText
    {
        get
        {
            return ValidationHelper.GetString(GetValue("SignInText"), "");
        }
        set
        {
            SetValue("SignInText", value);
        }
    }


    /// <summary>
    /// Get or sets sign out text
    /// </summary>
    public string SignOutText
    {
        get
        {
            return ValidationHelper.GetString(GetValue("SignOutText"), "");
        }
        set
        {
            SetValue("SignOutText", value);
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            if (SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSEnableWindowsLiveID"))
            {

                string siteName = CMSContext.CurrentSiteName;
                if (!string.IsNullOrEmpty(siteName))
                {
                    // Get LiveID settings
                    string appId = SettingsKeyProvider.GetStringValue(siteName + ".CMSApplicationID");
                    string secret = SettingsKeyProvider.GetStringValue(siteName + ".CMSApplicationSecret");
                    string algorithm = SettingsKeyProvider.GetStringValue(siteName + ".CMSsecurityAlgorithm");

                    // Check valid Windows LiveID parameters
                    if ((appId == string.Empty) || (secret == string.Empty) || (algorithm == string.Empty))
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("liveid.incorrectsettings");
                        return;
                    }

                    WindowsLiveLogin wll = new WindowsLiveLogin(appId, secret, algorithm);

                    // If user is already authenticated 
                    if (CMSContext.CurrentUser.IsAuthenticated())
                    {
                        // If signout should be visible and user has LiveID registered
                        if ((ShowSignOut) && (!String.IsNullOrEmpty(CMSContext.CurrentUser.UserSettings.WindowsLiveID)))
                        {
                            // Get data from auth cookie 
                            string[] userData = UserInfoProvider.GetUserDataFromAuthCookie();

                            // Check if user has truly logged in by LiveID
                            if ((userData != null) && (Array.IndexOf(userData, "liveidlogin") >= 0))
                            {
                                string navUrl = wll.GetLogoutUrl();

                                // If text is set use text/button link
                                if (!string.IsNullOrEmpty(SignOutText))
                                {
                                    // Button link
                                    if (ShowAsButton)
                                    {
                                        btnSignOut.CommandArgument = navUrl;
                                        btnSignOut.Text = SignOutText;
                                        btnSignOut.Visible = true;
                                    }
                                    // Text link
                                    else
                                    {
                                        btnSignOutLink.CommandArgument = navUrl;
                                        btnSignOutLink.Text = SignOutText;
                                        btnSignOutLink.Visible = true;
                                    }
                                }
                                // Image link
                                else
                                {
                                    btnSignOutImage.CommandArgument = navUrl;
                                    btnSignOutImage.ImageUrl = ResolveUrl(SignOutImageURL);
                                    btnSignOutImage.Visible = true;
                                    btnSignOut.Text = ResHelper.GetString("webparts_membership_signoutbutton.signout");
                                }
                            }
                        }
                        else
                        {
                            Visible = false;
                        }
                    }
                    // Sign In
                    else
                    {
                        string returnUrl = QueryHelper.GetText("returnurl", "");
                        // Get login url with possible return URL address in context

                        string navUrl = wll.GetLoginUrl(returnUrl);


                        // If text is set use text/button link
                        if (!string.IsNullOrEmpty(SignInText))
                        {
                            // Button link
                            if (ShowAsButton)
                            {
                                btnSignIn.CommandArgument = navUrl;
                                btnSignIn.Text = SignInText;
                                btnSignIn.Visible = true;
                            }
                            // Text link
                            else
                            {
                                lnkSignIn.NavigateUrl = navUrl;
                                lnkSignIn.Text = SignInText;
                                lnkSignIn.Visible = true;
                            }
                        }
                        // Image link
                        else
                        {
                            lnkSignIn.NavigateUrl = navUrl;
                            lnkSignIn.ImageUrl = ResolveUrl(SignInImageURL);
                            lnkSignIn.Visible = true;
                            lnkSignIn.Text = ResHelper.GetString("webparts_membership_signoutbutton.signin");
                        }
                    }
                }
            }
            else
            {
                // Error label is displayed in Design mode when Windows Live ID is disabled
                if (CMSContext.ViewMode == ViewModeEnum.Design)
                {
                    string parameter = null;
                    if (CMSContext.CurrentUser.IsGlobalAdministrator)
                    {
                        parameter = "<a href=\"" + UrlHelper.GetAbsoluteUrl("~/CMSSiteManager/default.aspx?section=settings") + "\" target=\"_top\">SiteManager -> Settings -> Windows LiveID</a>";
                    }
                    else
                    {
                        parameter = "SiteManager -> Settings -> Windows LiveID";
                    }
                    lblError.Text = String.Format(ResHelper.GetString("mem.liveid.disabled"), parameter);
                    lblError.Visible = true;
                }
                else
                {
                    this.Visible = false;
                }
            }
        }
    }


    /// <summary>
    /// SignOut handler
    /// </summary>
    protected void btnSignOut_Click(object sender, CommandEventArgs e)
    {
        if (StopProcessing)
        {
            // Do not process
        }
        else
        {
            if (CMSContext.CurrentUser.IsAuthenticated())
            {
                // Sign out from CMS
                FormsAuthentication.SignOut();
                CMSContext.CurrentUser = null;
                CMSContext.ClearShoppingCart();

                Response.Cache.SetNoStore();

                if (Session != null)
                {
                    // Store info about logout request, for validation logout request
                    Session["liveidlogout"] = DateTime.Now;
                }

                // Redirect to sign out from Windows Live
                UrlHelper.Redirect(e.CommandArgument.ToString());
            }
        }
    }


    /// <summary>
    /// SignIn handler
    /// </summary>
    protected void btnSignIn_Click(object sender, CommandEventArgs e)
    {
        if (StopProcessing)
        {
            // Do not process
        }
        else
        {
            // Redirect to sign in to Windows Live
            UrlHelper.Redirect(e.CommandArgument.ToString());
        }
    }

    #endregion
}
