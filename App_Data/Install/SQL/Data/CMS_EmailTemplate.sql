SET IDENTITY_INSERT [CMS_EmailTemplate] ON
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (744, '20100302 13:35:17', '744e6923-0224-4a10-b633-7922646db03d', N'Blogs - Notification to blog moderators', NULL, N'<html>
	<head>
	  <style>
		body, td
		{
		  font-size: 12px; 
		  font-family: Arial;
		}
	  </style>
	</head>	
	<body>
	<p>
	  New blog post comment was added and now is waiting for your approval:
	</p>
	<table>
	  <tr valign="top">
		<td>
		<strong>Blog post:&nbsp;</strong>
		</td>
		<td>
		<a href="{%BlogPostLink%}">{%BlogPost.DocumentName%}</a>
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Blog:&nbsp;</strong>
		</td>
		<td>
		<a href="{%BlogLink%}">{%Blog.DocumentName%}</a>
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Added by:&nbsp;</strong>
		</td>
		<td>
		{%Comment.CommentUserName%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Date and time:&nbsp;</strong>
		</td>
		<td>
		{%Comment.CommentDate%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Text:&nbsp;</strong>
		</td>
		<td>
		{%Comment.CommentText%}
		</td>
	  </tr>
	</table>	  
	</body>
</html>', N'New blog post comment was added and now is waiting for your approval: 
Blog post:   [url={%BlogPostLink%}]{%BlogPost.DocumentName%}[/url]
Blog:   [url={%BlogLink%}]{%Blog.DocumentName%}[/url]
Added by:   {%Comment.CommentUserName%}  
Date and time:   {%Comment.CommentDate%}  
Text:   {%Comment.CommentText%}', N'Blog.NotificationToModerators')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (769, '20100203 16:06:28', 'f5083a57-1355-430a-a1b9-5679e3d0e5fc', N'Blogs - Notification to blog owner', NULL, N'<html>
	<head>
	  <style>
		body, td
		{
		  font-size: 12px; 
		  font-family: Arial
		}
	  </style>
	</head>	
	<body>
	<p>
	  New comment was added to your blog post:
	</p>
	<table>
	  <tr valign="top">
		<td>
		<strong>Blog post:&nbsp;</strong>
		</td>
		<td>
		<a href="{%BlogPostLink%}">{%BlogPost.DocumentName%}</a>
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Blog:&nbsp;</strong>
		</td>
		<td>
		<a href="{%BlogLink%}">{%Blog.DocumentName%}</a>
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Added by:&nbsp;</strong>
		</td>
		<td>
		{%Comment.CommentUserName%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Date and time:&nbsp;</strong>
		</td>
		<td>
		{%Comment.CommentDate%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Text:&nbsp;</strong>
		</td>
		<td>
		{%Comment.CommentText%}
		</td>
	  </tr>
	</table>	  
	</body>
</html>', N'New comment was added to your blog post:
Blog post: [url={%BlogPostLink%}]{%BlogPost.DocumentName%}[/url]
Blog: [url={%BlogLink%}]{%Blog.DocumentName%}[/url]
Added by: {%Comment.CommentUserName%}
Date and time: {%Comment.CommentDate%}
Text: {%Comment.CommentText%}', N'Blog.NewCommentNotification')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (768, '20090122 11:29:03', 'c82d3577-1657-43b5-8fd8-b3172a87b9d1', N'Blogs - Notification to blog post subscribers', NULL, N'<html>
	<head>
	  <style>
		body, td
		{
		  font-size: 12px; 
		  font-family: Arial
		}
	  </style>
	</head>	
	<body>
	<p>
	  New comment was added to the blog post you are subscribed to:
	</p>
	<table>
	  <tr valign="top">
		<td>
		<strong>Blog post:&nbsp;</strong>
		</td>
		<td>
		<a href="{%BlogPostLink%}">{%BlogPost.DocumentName%}</a>
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Blog:&nbsp;</strong>
		</td>
		<td>
		<a href="{%BlogLink%}">{%Blog.DocumentName%}</a>
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Added by:&nbsp;</strong>
		</td>
		<td>
		{%Comment.CommentUserName%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Date and time:&nbsp;</strong>
		</td>
		<td>
		{%Comment.CommentDate%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Text:&nbsp;</strong>
		</td>
		<td>
		{%Comment.CommentText%}
		</td>
	  </tr>
	</table>	  
	<p>
        <a href="{%unsubscriptionlink%}">Click here to unsubscribe</a>
	</p>
	</body>
</html>', N'New comment was added to the blog post you are subscribed to:
Blog post: [url={%BlogPostLink%}]{%BlogPost.DocumentName%}[/url]
Blog: [url={%BlogLink%}]{%Blog.DocumentName%}[/url]
Added by: {%Comment.CommentUserName%}
Date and time: {%Comment.CommentDate%}
Text: {%Comment.CommentText%}
[url={%unsubscriptionlink%}]Click here to unsubscribe[/url]', N'Blog.NotificationToSubcribers')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (760, '20090122 11:29:14', '35a95893-ee26-449e-b257-edb134a67c44', N'Boards - Notification to board moderators', NULL, N'<html>
	<head>
	  <style>
		body, td
		{
		  font-size: 12px; 
		  font-family: Arial;
		}
	  </style>
	</head>	
	<body>
	<p>
	  New message was added and now is waiting for your approval:
	</p>
	<table>
	  <tr valign="top">
		<td>
		<strong>Board:&nbsp;</strong>
		</td>
		<td>
		<a href="{%DocumentLink%}">{%Board.BoardDisplayName%}</a>
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Added by:&nbsp;</strong>
		</td>
		<td>
		{%MessageUser.UserName%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Date and time:&nbsp;</strong>
		</td>
		<td>
		{%Message.MessageInserted%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Text:&nbsp;</strong>
		</td>
		<td>
		{%Message.MessageText%}
		</td>
	  </tr>
	</table>	  
	</body>
</html>', N'New message was added and now is waiting for your approval: 
Board:   [url={%DocumentLink%}]{%Board.BoardDsiplayName%}[/url]
Added by:   {%MessageUser.FullName%}  
Date and time:   {%Message.MessageInserted%}  
Text:   {%Message.MessageText%}', N'Boards.NotificationToModerators')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (751, '20090122 11:29:22', '856a1cbf-6340-4c20-b7da-ac32810b8546', N'Boards - Notification to board subscribers', NULL, N'<html>
	<head>
	  <style>
		body, td
		{
		  font-size: 12px; 
		  font-family: Arial;
		}
	  </style>
	</head>	
	<body>
	<p>
	  New message was added to the board you are subscribed to:
	</p>
	<table>
	  <tr valign="top">
		<td>
		<strong>Board:&nbsp;</strong>
		</td>
		<td>
		<a href="{%DocumentLink%}">{%Board.BoardDisplayName%}</a>
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Added by:&nbsp;</strong>
		</td>
		<td>
		{%MessageUser.UserName%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Date and time:&nbsp;</strong>
		</td>
		<td>
		{%Message.MessageInserted%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Text:&nbsp;</strong>
		</td>
		<td>
		{%Message.MessageText%}
		</td>
	  </tr>
	</table>	  
	<p>
        <a href="{%UnsubscriptionLink%}">Click here to unsubscribe</a>
	</p>
	</body>
</html>', N'New message was added to the board you are subscribed to: 
Board:   [url={%DocumentLink%}"]{%Board.BoardDisplayName%}[/url]
Added by:   {%MessageUser.FullName%}  
Date and time:   {%Message.MessageInserted%}  
Text:   {%Message.MessageText%}  
[url={%UnsubscriptionLink%}]Click here to unsubscribe[/url]', N'Boards.NotificationToSubscribers')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (705, '20100209 17:20:23', 'eb37ce02-7853-4f91-bbd1-127597ebce66', N'Booking system - Event invitation', NULL, N'<html><head></head><body>
<p>Hello,</p>
<p>Thank you for your registration for event {%EventName|(encode)false%}. This is an e-mail confirmation that you have been registered. Below, you can find event details:</p>
<p><strong>Event: {%EventName|(encode)false%}</strong></p>
<p><em>{%EventSummary|(encode)false%}</em></p>
<p>{%EventDetails|(encode)false%}</p>
<p><strong>Location:</strong><br />
{%EventLocation|(encode)false%}</p>
<p><strong>Date:</strong><br />
{%EventDateString|(encode)false%}</p>
</body>
</html>', N'Hello,
Thank you for your registration for event {%EventName|(encode)false%}. This is an e-mail confirmation that you have been registered. Below, you can find event details:
Event: {%EventName|(encode)false%}
{%EventSummary|(encode)false%}
{%EventDetails|(encode)false%}
Location:
{%EventLocation|(encode)false%}
Date:
{%EventDateString|(encode)false%}', N'BookingEvent.Invitation')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (167, '20091127 10:09:17', 'f49163f2-32c3-4c7b-ab1b-c128d621c02f', N'E-commerce - Order notification to administrator', NULL, N'<html><head></head><body>
<table cellspacing="0" cellpadding="5" bordercolor="black" border="1" width="600">
    <tbody>
        <tr>
            <td height="50" valign="bottom" colspan="2">
            <table height="100%" width="100%">
                <tbody>
                    <tr>
                        <td style="text-align: left; vertical-align: bottom;"><span style="font-size: 18pt;">New order</span></td>
                        <td style="text-align: center; vertical-align: middle;"><span style="font-family: Garamond,Times,serif; font-size: 24pt; font-style: italic;">Company logo</span></td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;"><br />
            <table width="100%">
                <tbody>
                    <tr>
                        <td valign="bottom" style="text-align: left;"><strong>Invoice number:</strong></td>
                        <td style="text-align: right; padding-right: 10px;">{%INVOICENUMBER%}</td>
                    </tr>
                </tbody>
            </table>
            </td>
            <td style="text-align: left;"><br />
            <table width="100%">
                <tbody>
                    <tr>
                        <td valign="bottom" style="text-align: left;"><strong>Order date:</strong></td>
                        <td style="text-align: right; padding-right: 10px;">{%ORDERDATE%}</td>
                    </tr>
                    <tr>
                        <td valign="bottom" style="text-align: left;"><strong>Order status:</strong></td>
                        <td style="text-align: right; padding-right: 10px;">{%OrderStatus.StatusDisplayName%}</td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td width="50%" style="text-align: left; vertical-align: top;"><strong>Supplier</strong>       <br />
            <br />
            <table>
                <tbody>
                    <tr>
                        <td>Company address</td>
                    </tr>
                </tbody>
            </table>
            </td>
            <td width="50%" style="text-align: left; vertical-align: top;"><span style="font-weight: bold;"> Customer </span><br />
            <br />
            {%BILLINGADDRESS%}       <br />
            <strong>Company address:</strong> 	{%COMPANYADDRESS%}       </td>
        </tr>
        <tr>
            <td colspan="2">
            <table width="100%">
                <tbody>
                    <tr>
                        <td style="text-align: left;"><span style="font-weight: bold;"> Payment option </span></td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">{%PAYMENTOPTION%}</td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <table width="100%">
                <tbody>
                    <tr>
                        <td style="text-align: left;"><span style="font-weight: bold;"> Shipping option </span></td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">{%SHIPPINGOPTION%}</td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left;">{%PRODUCTLIST%}        <hr size="1" />
            <div style="text-align: right;">
            <table cellpadding="5" style="text-align: left;">
                <tbody>
                    <tr>
                        <td><strong>Total shipping:</strong></td>
                        <td style="text-align: right; padding-right: 0px;"><strong>{%TOTALSHIPPING%}</strong></td>
                    </tr>
                    <tr>
                        <td><strong>Total price:</strong></td>
                        <td style="text-align: right; padding-right: 0px;"><strong>{%TOTALPRICE%}</strong></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;"><strong>Tax summary:</strong></td>
                        <td style="text-align: right; padding-right: 0px;">{%TAXRECAPITULATION%}</td>
                    </tr>
                </tbody>
            </table>
            </div>
            <div style="height: 120px;">&nbsp;</div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <table width="100%">
                <tbody>
                    <tr>
                        <td style="text-align: left;"><span style="font-weight: bold;"> Order note </span></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">{%ORDERNOTE%}</td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
    </tbody>
</table>
<div style="padding-top-10px;">    {%NEWORDERLINK%} </div>
</body>
</html>', N'New order Company logo 
 
Invoice number: {%INVOICENUMBER%} 
 
Order date: {%ORDERDATE%} 
Order status: {%OrderStatus.StatusDisplayName%} 
 
Supplier 
Company address 
Customer 
{%BILLINGADDRESS%} 
Company address: {%COMPANYADDRESS%}  
Payment option  
{%PAYMENTOPTION%} 
 
Shipping option  
{%SHIPPINGOPTION%} 
 
{%PRODUCTLIST%} 
--------------------------------------------------------------------------------
Total shipping: {%TOTALSHIPPING%} 
Total price: {%TOTALPRICE%} 
Tax summary: {%TAXRECAPITULATION%} 
  
Order note  
{%ORDERNOTE%}', N'Ecommerce.OrderNotificationToAdmin')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (166, '20100412 10:56:58', '674d1b85-ce19-40bd-b2df-166a5891090a', N'E-commerce - Order notification to customer', NULL, N'<html><head></head><body>
<p>{%thankyoutext%}</p>
<table width="600" cellspacing="0" cellpadding="5" bordercolor="black" border="1">
    <tbody>
        <tr>
            <td height="50" valign="bottom" colspan="2">
            <table height="100%" width="100%">
                <tbody>
                    <tr>
                        <td style="text-align: left; vertical-align: bottom;"><span style="font-size: 18pt;">Your order</span></td>
                        <td style="text-align: center; vertical-align: middle;"><span style="font-family: Garamond,Times,serif; font-size: 24pt; font-style: italic;">Company logo</span></td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td style="text-align: left;"><br />
            <table width="100%">
                <tbody>
                    <tr>
                        <td valign="bottom" style="text-align: left;"><strong>Invoice number:</strong></td>
                        <td style="text-align: right; padding-right: 10px;">{%INVOICENUMBER%}</td>
                    </tr>
                </tbody>
            </table>
            </td>
            <td style="text-align: left;"><br />
            <table width="100%">
                <tbody>
                    <tr>
                        <td valign="bottom" style="text-align: left;"><strong>Order date:</strong></td>
                        <td style="text-align: right; padding-right: 10px;">{%ORDERDATE%}</td>
                    </tr>
                    <tr>
                        <td valign="bottom" style="text-align: left;"><strong>Order status:</strong></td>
                        <td style="text-align: right; padding-right: 10px;">{%OrderStatus.StatusDisplayName%}</td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td width="50%" style="text-align: left; vertical-align: top;"><strong>Supplier</strong>       <br />
            <br />
            <table>
                <tbody>
                    <tr>
                        <td>Company address</td>
                    </tr>
                </tbody>
            </table>
            </td>
            <td width="50%" style="text-align: left; vertical-align: top;"><span style="font-weight: bold;"> Customer </span><br />
            <br />
            {%BILLINGADDRESS%}       <br />
            <strong>Company address:</strong> 	{%COMPANYADDRESS%}       </td>
        </tr>
        <tr>
            <td colspan="2">
            <table width="100%">
                <tbody>
                    <tr>
                        <td style="text-align: left;"><span style="font-weight: bold;"> Payment option </span></td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">{%PAYMENTOPTION%}</td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <table width="100%">
                <tbody>
                    <tr>
                        <td style="text-align: left;"><span style="font-weight: bold;"> Shipping option </span></td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">{%SHIPPINGOPTION%}</td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: left;">{%PRODUCTLIST%}        <hr size="1" />
            <div style="text-align: right;">
            <table cellpadding="5" style="text-align: left;">
                <tbody>
                    <tr>
                        <td><strong>Total shipping:</strong></td>
                        <td style="text-align: right; padding-right: 0px;"><strong>{%TOTALSHIPPING%}</strong></td>
                    </tr>
                    <tr>
                        <td><strong>Total price:</strong></td>
                        <td style="text-align: right; padding-right: 0px;"><strong>{%TOTALPRICE%}</strong></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;"><strong>Tax summary:</strong></td>
                        <td style="text-align: right; padding-right: 0px;">{%TAXRECAPITULATION%}</td>
                    </tr>
                </tbody>
            </table>
            </div>
            <div style="height: 120px;">&nbsp;</div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <table width="100%">
                <tbody>
                    <tr>
                        <td style="text-align: left;"><span style="font-weight: bold;"> Order note </span></td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">{%ORDERNOTE%}</td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
    </tbody>
</table>
</body>
</html>', N'{%thankyoutext%}
Your order Company logo 
 
Invoice number: {%INVOICENUMBER%} 
 
Order date: {%ORDERDATE%} 
Order status: {%OrderStatus.StatusDisplayName%} 
 
Supplier 
Company address 
 Customer 
{%BILLINGADDRESS%} 
Company address: {%COMPANYADDRESS%}  
Payment option  
{%PAYMENTOPTION%} 
 
Shipping option  
{%SHIPPINGOPTION%} 
 
{%PRODUCTLIST%} 
--------------------------------------------------------------------------------
Total shipping: {%TOTALSHIPPING%} 
Total price: {%TOTALPRICE%} 
Tax summary: {%TAXRECAPITULATION%} 
  
Order note  
{%ORDERNOTE%}', N'Ecommerce.OrderNotificationToCustomer')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (706, '20081217 13:13:48', '7b821e4a-d695-42a7-85f2-1ca14c208953', N'E-commerce - Order payment notification to administrator', NULL, N'<html>
<head>
</head>
<body style="FONT-SIZE: 10x; FONT-FAMILY: arial">
<p>{%paymentreceivedtext%}</p>
<table cellspacing="0" cellpadding="5" border="1" bordercolor="black" width="600px">
  <tr>
    <td colspan="2" valign="bottom" height="50">
      <table width="100%" height="100%">
        <tr>
          <td style="text-align: left; vertical-align: bottom;"
            <span style="font-size: 18pt">New order</span>
          </td>
          <td style="text-align: center; vertical-align: middle;">
            <span style="font-family: Garamond, Times, serif; font-size: 24pt; font-style: italic;">Company logo</span>
          </td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr>
    <td style="text-align: left">    
      <br />
      <table width="100%">
        <tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Invoice number:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%INVOICENUMBER%}
          </td>
        </tr>
      </table> 
      <br />
    </td>
    <td style="text-align: left">    
      <br />
      <table width="100%">
        <tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Order date:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%ORDERDATE%} 
          </td>
        </tr>
	<tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Order status:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%OrderStatus.StatusDisplayName%} 
          </td>
        </tr>
      </table>  
      <br />
    </td>
  </tr>
  <tr>
    <td style="text-align: left; vertical-align: top" width="50%">
      <strong>Supplier</strong>
      <br/>
      <br/>
      <table>
        <tr>
          <td>
            Company address
          </td>
        </tr>
      </table>
      <br />
    </td>
    <td style="text-align: left; vertical-align: top" width="50%">
      <span style="font-weight: bold"> Customer </span><br />
      <br />
        {%BILLINGADDRESS%}
      <br />
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Payment option </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            {%PAYMENTOPTION%}
          </td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Shipping option </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            {%SHIPPINGOPTION%}
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="text-align: left" colspan="2">
      {%PRODUCTLIST%} 
      <hr size="1" />
      <div style="text-align: right;">
      <table style="text-align: left;" cellpadding="5">
	<tr>
	  <td><strong>Total shipping:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            <strong>{%TOTALSHIPPING%}</strong>
          </td>
	</tr>
	<tr>
	  <td><strong>Total price:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            <strong>{%TOTALPRICE%}</strong>
          </td>
	</tr>
	<tr>
	  <td style="vertical-align:top;"><strong>Tax summary:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            {%TAXRECAPITULATION%}
          </td>
	</tr>
      </table>
      </div>
      <div style="height: 120px;">&nbsp;</div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Order note </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: left">
            {%ORDERNOTE%}
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>', N'{%paymentreceivedtext%}
New order  Company logo  
 
Invoice number:  {%INVOICENUMBER%}  
 
Order date:  {%ORDERDATE%}  
Order status:  {%OrderStatus.StatusDisplayName%}  
 
Supplier 
Company address  
Customer 
{%BILLINGADDRESS%} 
 
Payment option  
{%PAYMENTOPTION%}  
 
Shipping option  
{%SHIPPINGOPTION%}  
 
{%PRODUCTLIST%} 
--------------------------------------------------------------------------------
Total shipping: {%TOTALSHIPPING%}  
Total price: {%TOTALPRICE%}  
Tax summary: {%TAXRECAPITULATION%}  
  
Order note  
{%ORDERNOTE%}', N'Ecommerce.OrderPaymentNotificationToAdmin')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (707, '20081217 13:14:21', '5da46ce2-23bf-4c2e-9dbb-22d67c550399', N'E-commerce - Order payment notification to customer', NULL, N'<html>
<head>
</head>
<body style="FONT-SIZE: 10x; FONT-FAMILY: arial">
<p>{%paymentreceivedtext%}</p>
<table cellspacing="0" cellpadding="5" border="1" bordercolor="black" width="600px">
  <tr>
    <td colspan="2" valign="bottom" height="50">
      <table width="100%" height="100%">
        <tr>
          <td style="text-align: left; vertical-align: bottom;"
            <span style="font-size: 18pt">Your order</span>
          </td>
          <td style="text-align: center; vertical-align: middle;">
            <span style="font-family: Garamond, Times, serif; font-size: 24pt; font-style: italic;">Company logo</span>
          </td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr>
    <td style="text-align: left">    
      <br />
      <table width="100%">
        <tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Invoice number:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%INVOICENUMBER%}
          </td>
        </tr>
      </table> 
      <br />
    </td>
    <td style="text-align: left">    
      <br />
      <table width="100%">
        <tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Order date:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%ORDERDATE%} 
          </td>
        </tr>
	<tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Order status:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%OrderStatus.StatusDisplayName%} 
          </td>
        </tr>
      </table>  
      <br />
    </td>
  </tr>
  <tr>
    <td style="text-align: left; vertical-align: top" width="50%">
      <strong>Supplier</strong>
      <br/>
      <br/>
      <table>
        <tr>
          <td>
            Company address
          </td>
        </tr>
      </table>
      <br />
    </td>
    <td style="text-align: left; vertical-align: top" width="50%">
      <span style="font-weight: bold"> Customer </span><br />
      <br />
        {%BILLINGADDRESS%}
      <br />
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Payment option </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            {%PAYMENTOPTION%}
          </td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Shipping option </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            {%SHIPPINGOPTION%}
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="text-align: left" colspan="2">
      {%PRODUCTLIST%} 
      <hr size="1" />
      <div style="text-align: right;">
      <table style="text-align: left;" cellpadding="5">
	<tr>
	  <td><strong>Total shipping:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            <strong>{%TOTALSHIPPING%}</strong>
          </td>
	</tr>
	<tr>
	  <td><strong>Total price:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            <strong>{%TOTALPRICE%}</strong>
          </td>
	</tr>
	<tr>
	  <td style="vertical-align:top;"><strong>Tax summary:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            {%TAXRECAPITULATION%}
          </td>
	</tr>
      </table>
      </div>
      <div style="height: 120px;">&nbsp;</div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Order note </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: left">
            {%ORDERNOTE%}
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>', N'{%paymentreceivedtext%}
Your order  Company logo  
 
Invoice number:  {%INVOICENUMBER%}  
 
Order date:  {%ORDERDATE%}  
Order status:  {%OrderStatus.StatusDisplayName%}  
 
Supplier 
Company address  
Customer 
{%BILLINGADDRESS%} 
 
Payment option  
{%PAYMENTOPTION%}  
 
Shipping option  
{%SHIPPINGOPTION%}  
 
{%PRODUCTLIST%} 
--------------------------------------------------------------------------------
Total shipping: {%TOTALSHIPPING%}  
Total price: {%TOTALPRICE%}  
Tax summary: {%TAXRECAPITULATION%}  
  
Order note  
{%ORDERNOTE%}', N'Ecommerce.OrderPaymentNotificationToCustomer')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (731, '20081217 13:14:56', 'a32836fb-5d7e-4e71-9dcf-33bc920859b6', N'E-commerce - Order status notification to administrator', NULL, N'<html>
<head>
</head>
<body style="FONT-SIZE: 10x; FONT-FAMILY: arial">
<table cellspacing="0" cellpadding="5" border="1" bordercolor="black" width="600px">
  <tr>
    <td colspan="2" valign="bottom" height="50">
      <table width="100%" height="100%">
        <tr>
          <td style="text-align: left; vertical-align: bottom;"
            <span style="font-size: 18pt">New order</span>
          </td>
          <td style="text-align: center; vertical-align: middle;">
            <span style="font-family: Garamond, Times, serif; font-size: 24pt; font-style: italic;">Company logo</span>
          </td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr>
    <td style="text-align: left">    
      <br />
      <table width="100%">
        <tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Invoice number:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%INVOICENUMBER%}
          </td>
        </tr>
      </table> 
      <br />
    </td>
    <td style="text-align: left">    
      <br />
      <table width="100%">
        <tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Order date:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%ORDERDATE%} 
          </td>
        </tr>
	<tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Order status:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%OrderStatus.StatusDisplayName%} 
          </td>
        </tr>
      </table>  
      <br />
    </td>
  </tr>
  <tr>
    <td style="text-align: left; vertical-align: top" width="50%">
      <strong>Supplier</strong>
      <br/>
      <br/>
      <table>
        <tr>
          <td>
            Company address
          </td>
        </tr>
      </table>
      <br />
    </td>
    <td style="text-align: left; vertical-align: top" width="50%">
      <span style="font-weight: bold"> Customer </span><br />
      <br />
        {%BILLINGADDRESS%}
      <br />
	<strong>Company address:</strong>
	{%COMPANYADDRESS%}
      <br />
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Payment option </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            {%PAYMENTOPTION%}
          </td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Shipping option </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            {%SHIPPINGOPTION%}
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="text-align: left" colspan="2">
      {%PRODUCTLIST%} 
      <hr size="1" />
      <div style="text-align: right;">
      <table style="text-align: left;" cellpadding="5">
	<tr>
	  <td><strong>Total shipping:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            <strong>{%TOTALSHIPPING%}</strong>
          </td>
	</tr>
	<tr>
	  <td><strong>Total price:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            <strong>{%TOTALPRICE%}</strong>
          </td>
	</tr>
	<tr>
	  <td style="vertical-align:top;"><strong>Tax summary:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            {%TAXRECAPITULATION%}
          </td>
	</tr>
      </table>
      </div>
      <div style="height: 120px;">&nbsp;</div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Order note </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: left">
            {%ORDERNOTE%}
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<div style="padding-top-10px;">
   {%NEWORDERLINK%}
</div>
</body>
</html>', N'New order  Company logo  
 
Invoice number:  {%INVOICENUMBER%}  
 
Order date:  {%ORDERDATE%}  
Order status:  {%OrderStatus.StatusDisplayName%}  
 
Supplier 
Company address  
Customer 
{%BILLINGADDRESS%} 
Company address: {%COMPANYADDRESS%} 
 
Payment option  
{%PAYMENTOPTION%}  
 
Shipping option  
{%SHIPPINGOPTION%}  
 
{%PRODUCTLIST%} 
--------------------------------------------------------------------------------
Total shipping: {%TOTALSHIPPING%}  
Total price: {%TOTALPRICE%}  
Tax summary: {%TAXRECAPITULATION%}  
  
Order note  
{%ORDERNOTE%}  
 
{%NEWORDERLINK%}', N'Ecommerce.OrderStatusNotificationToAdmin')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (732, '20081217 13:15:34', '68ca5608-f80f-4972-88f3-24daaf669c32', N'E-commerce - Order status notification to customer', NULL, N'<html>
<head>
</head>
<body style="FONT-SIZE: 10x; FONT-FAMILY: arial">
<p>{%thankyoutext%}</p>
<table cellspacing="0" cellpadding="5" border="1" bordercolor="black" width="600px">
  <tr>
    <td colspan="2" valign="bottom" height="50">
      <table width="100%" height="100%">
        <tr>
          <td style="text-align: left; vertical-align: bottom;"
            <span style="font-size: 18pt">Your order</span>
          </td>
          <td style="text-align: center; vertical-align: middle;">
            <span style="font-family: Garamond, Times, serif; font-size: 24pt; font-style: italic;">Company logo</span>
          </td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr>
    <td style="text-align: left">    
      <br />
      <table width="100%">
        <tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Invoice number:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%INVOICENUMBER%}
          </td>
        </tr>
      </table> 
      <br />
    </td>
    <td style="text-align: left">    
      <br />
      <table width="100%">
        <tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Order date:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%ORDERDATE%} 
          </td>
        </tr>
	<tr>
          <td style="text-align: left;" valign="bottom"> 
            <strong>Order status:</strong>
          </td>
          <td style="text-align: right; padding-right: 10px">
            {%OrderStatus.StatusDisplayName%} 
          </td>
        </tr>
      </table>  
      <br />
    </td>
  </tr>
  <tr>
    <td style="text-align: left; vertical-align: top" width="50%">
      <strong>Supplier</strong>
      <br/>
      <br/>
      <table>
        <tr>
          <td>
            Company address
          </td>
        </tr>
      </table>
      <br />
    </td>
    <td style="text-align: left; vertical-align: top" width="50%">
      <span style="font-weight: bold"> Customer </span><br />
      <br />
        {%BILLINGADDRESS%}
      <br />
	<strong>Company address:</strong>
	{%COMPANYADDRESS%}
      <br />
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Payment option </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            {%PAYMENTOPTION%}
          </td>
        </tr>
      </table> 
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Shipping option </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: center">
            {%SHIPPINGOPTION%}
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="text-align: left" colspan="2">
      {%PRODUCTLIST%} 
      <hr size="1" />
      <div style="text-align: right;">
      <table style="text-align: left;" cellpadding="5">
	<tr>
	  <td><strong>Total shipping:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            <strong>{%TOTALSHIPPING%}</strong>
          </td>
	</tr>
	<tr>
	  <td><strong>Total price:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            <strong>{%TOTALPRICE%}</strong>
          </td>
	</tr>
	<tr>
	  <td style="vertical-align:top;"><strong>Tax summary:</strong></td>
          <td style="text-align: right; padding-right: 0px;">
            {%TAXRECAPITULATION%}
          </td>
	</tr>
      </table>
      </div>
      <div style="height: 120px;">&nbsp;</div>
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <table width="100%">
        <tr>
          <td style="text-align: left">
            <span style="font-weight: bold"> Order note </span>
          </td>
        </tr>
        <tr>
          <td style="text-align: left">
            {%ORDERNOTE%}
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>', N'{%thankyoutext%}
Your order  Company logo  
 
Invoice number:  {%INVOICENUMBER%}  
 
Order date:  {%ORDERDATE%}  
Order status:  {%OrderStatus.StatusDisplayName%}  
 
Supplier 
Company address  
Customer 
{%BILLINGADDRESS%} 
Company address: {%COMPANYADDRESS%} 
 
Payment option  
{%PAYMENTOPTION%}  
 
Shipping option  
{%SHIPPINGOPTION%}  
 
{%PRODUCTLIST%} 
--------------------------------------------------------------------------------
Total shipping: {%TOTALSHIPPING%}  
Total price: {%TOTALPRICE%}  
Tax summary: {%TAXRECAPITULATION%}  
  
Order note  
{%ORDERNOTE%}', N'Ecommerce.OrderStatusNotificationToCustomer')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (161, '20090112 21:17:42', '2fd97ef6-d0c3-4cd0-99fa-2cda6928773d', N'Forums -  New post', NULL, N'<html>
	<head>
	  <style>
		BODY, TD
		{
		  font-size: 12px; 
		  font-family: arial
		}
	  </style>
	</head>	
	<body>
	<p>
	  This is a notification of a new post added to the forum you subscribed to:
	</p>
	<table>
	  <tr valign="top">
		<td>
		<strong>Forum:</strong>
		</td>
		<td>
		{%forumdisplayname%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Subject:</strong>
		</td>
		<td>
		{%postsubject%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Posted by:</strong>
		</td>
		<td>
		{%postusername%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Date and time:</strong>
		</td>
		<td>
		{%posttime%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Text:</strong>
		</td>
		<td>
		{%posttext%}
		</td>
	  </tr>
	</table>	  
	<p>
	<a href="{%link%}">Click here to view forum on-line</a> &nbsp;
        <a href="{%unsubscribelink%}">Click here to unsubscribe</a>
	</p>
	</body>
</html>', N'This is a notification of a new post added to the forum you subscribed to: 
Forum:  {%forumdisplayname%}  
Subject:  {%postsubject%}  
Posted by:  {%postusername%}  
Date and time:  {%posttime%}  
Text:  {%posttextplain%}  
Click here to view forum on-line:
{%link%} 
Click here to unsubscribe:
{%unsubscribelink%}', N'Forums.NewPost')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (165, '20090112 19:18:31', 'f1fc231d-d1c4-425c-83d0-dd5197bc4c7b', N'Forums - Moderator notification', NULL, N'<html>
	<head>
	  <style>
		BODY, TD
		{
		  font-size: 12px; 
		  font-family: arial
		}
	  </style>
	</head>	
	<body>
	<p>
	  A new forum post is waiting for your approval.
	</p>
	<table>
	  <tr valign="top">
		<td>
		<strong>Forum:</strong>
		</td>
		<td>
		{%forumdisplayname%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Subject:</strong>
		</td>
		<td>
		{%postsubject%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Posted by:</strong>
		</td>
		<td>
		{%postusername%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Date and time:</strong>
		</td>
		<td>
		{%posttime%}
		</td>
	  </tr>
	  <tr valign="top">
		<td>
		<strong>Text:</strong>
		</td>
		<td>
		{%posttext%}
		</td>
	  </tr>
	</table>	  
	<p>
	<a href="{%link%}">Click here to view the forum on-line</a> &nbsp;
	</p>
	</body>
</html>', N'A new forum post is waiting for your approval. 
Forum:  {%forumdisplayname%}  
Subject:  {%postsubject%}  
Posted by:  {%postusername%}  
Date and time:  {%posttime%}  
Text:  {%posttext%}  
Click here to view the forum on-line:
{%link%}', N'Forums.ModeratorNotice')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (764, '20090112 19:19:03', 'dc4a0178-3055-4339-bab5-5dc1011b9a41', N'Forums - subscription confirmation', NULL, N'<html>
	<head>
	  <style>
		BODY, TD
		{
		  font-size: 12px; 
		  font-family: arial
		}
	  </style>
	</head>	
	<body>
	<p>
	  You have been successfully subscribed to		
	<strong>Forum</strong> {%forumdisplayname%}{%separator%}{%subject%}.		    
	<p/>
	<p>
	<a href="{%link%}">Click here to view forum on-line</a> &nbsp;
        <a href="{%unsubscribelink%}">Click here to unsubscribe</a>
	</p>
	</body>
</html>', N'You have been successfully subscribed to Forum {%forumdisplayname%}{%separator%}{%subject%}. 
Click here to view forum on-line:
{%link%}
Click here to unsubscribe:
{%unsubscribelink%}', N'Forums.SubscribeConfirmation')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (765, '20081221 15:48:49', '1d636437-f61c-40dc-8bc1-383c66a1bfc1', N'Forums - unsubscription confirmation', NULL, N'<html>
	<head>
	  <style>
		BODY, TD
		{
		  font-size: 12px; 
		  font-family: arial
		}
	  </style>
	</head>	
	<body>
	<p>
	  You have been successfully unsubscribed from		
	<strong>Forum</strong> {%forumdisplayname%}{%separator%}{%subject%}.		    
	<p/>	
	</body>
</html>', N'You have been successfully unsubscribed from Forum {%forumdisplayname%}{%separator%}{%subject%}.', N'Forums.UnsubscribeConfirmation')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (745, '20100304 12:53:54', '14926c79-639a-414b-b22d-2ca7cea58f2b', N'Friends - Friend approval', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>{%Sender.FullName%} ({%FORMATTEDSENDERNAME%}) is now your friend.</p>
		<p>Comment: {%Friendship.FriendComment%}</p>
		<p>Sent: {%Friendship.FriendApprovedWhen%}</p>
	</body>
</html>', N'{%Sender.FullName%} ({%FORMATTEDSENDERNAME%}) is now your friend.
Comment: {%Friendship.FriendComment%}
Sent: {%Friendship.FriendApprovedWhen%}', N'Friends.Approve')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (746, '20100304 12:53:05', 'd5d2b5f4-bcbc-4746-a5af-08833dd4c51a', N'Friends - Friend rejection', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>{%Sender.FullName%} ({%FORMATTEDSENDERNAME%}) rejected the friendship.</p>
		<p>Comment: {%Friendship.FriendComment%}</p>
		<p>Sent: {%Friendship.FriendRejectedWhen%}</p>
	</body>
</html>', N'{%Sender.FullName%} ({%FORMATTEDSENDERNAME%}) has rejected the friendship.
Comment: {%Friendship.FriendComment%}
Sent: {%Friendship.FriendRejectedWhen%}', N'Friends.Reject')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (747, '20100304 12:53:25', '6fe616ca-b9d8-4980-90be-9124c2546cde', N'Friends - Friend request', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>{%Sender.FullName%} ({%FORMATTEDSENDERNAME%}) wants to be your friend.</p>
		<p>Comment: {%Friendship.FriendComment%}</p>
		<p>Sent: {%Friendship.FriendRequestedWhen%}</p>
		<p>Choose one of the following actions:</p>		
		<p><a href="{%MANAGEMENTURL%}">Accept or reject</a></p>
		<p><a href="{%PROFILEURL%}">Open user profile</a></p>
	</body>
</html>', N'{%Sender.FullName%} ({%FORMATTEDSENDERNAME%}) wants to be your friend.
Comment: {%Friendship.FriendComment%}
Sent: {%Friendship.FriendRequestedWhen%}
Choose one of the following actions:
[url={%MANAGEMENTURL%}]Accept or reject[/url]
[url={%PROFILEURL%}]Open user profile[/url]', N'Friends.Request')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (763, '20090127 20:43:40', '0f68bc2f-bf08-40cf-b4e4-c2550e94f570', N'Groups - Member accepted invitation', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>{%Sender.FullName%}({%Sender.UserName%}) has accepted your invitation to group ''{%Group.GroupDisplayName%}''.<br />
		When: {%GroupMember.MemberJoined%}</p>
		<br />
		<br />
		Best Regards,<br />
		<br />
		The Community Team
	</body>
</html>', N'{%Sender.FullName%}({%Sender.UserName%}) has accepted your invitation to group ''{%Group.GroupDisplayName%}''.
When: {%GroupMember.MemberJoined%}
Best Regards,
The Community Team', N'Groups.MemberAcceptedInvitation')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (758, '20090127 20:20:52', 'd57092d8-28c3-4784-98d2-7d02f4a73a66', N'Groups - Member approved', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>Dear {%MemberUser.FirstName%},<br />
		<br />
		you have been approved as a full member of group ''{%Group.GroupDisplayName%}''.</p>
		<br />
		<br />
		Best Regards,<br />
		<br />
		The Community Team
	</body>
</html>', N'Dear {%MemberUser.FirstName%},
you have been approved as a full member of group ''{%Group.GroupDisplayName%}''.
Best Regards,
The Community Team', N'Groups.MemberApproved')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (761, '20090127 20:21:24', '8909e20f-80b3-4be1-9c47-61000e856784', N'Groups - Member invitation', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>User {%InvitedBy%} invites you to group ''{%Group.GroupDisplayName%}''.<br />
		With comment "{%Invitation.InvitationComment%}"<br /><br />
		Follow the link below to join the group:<br /><br />
		<a href="{%ACCEPTIONURL%}">{%ACCEPTIONURL%}</a>
		</p>
		<br />
		<br />
		Best Regards,
		<br />
		<br />
		The Community Team
	</body>
</html>', N'User {%InvitedBy%} invites you to group ''{%Group.GroupDisplayName%}''.
With comment "{%Invitation.InvitationComment%}"
Follow the link below to join the group:
[url={%ACCEPTIONURL%}]{%ACCEPTIONURL%}[/url]
Best Regards,
The Community Team', N'Groups.Invitation')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (753, '20090127 20:18:09', '7125b6a1-d72b-4e14-9995-2a4e73b910bc', N'Groups - Member join', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>User {%MemberUser.FullName%}({%MemberUser.UserName%}) joined group ''{%Group.GroupDisplayName%}''.</p>
		<br />
		<br />
		Best Regards,<br />
		<br />
		The Community Team
	</body>
</html>', N'User {%MemberUser.FullName%}({%MemberUser.UserName%}) joined group ''{%Group.GroupDisplayName%}''.
Best Regards,
The Community Team', N'Groups.MemberJoin')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (757, '20090127 20:20:25', '03636c82-1f7f-44e0-9b51-b7a6c705114e', N'Groups - Member joined confirmation', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>Dear {%MemberUser.FirstName%},<br />
		<br />
		welcome to group ''{%Group.GroupDisplayName%}''.</p>
		<br />
		<br />
		Best Regards,
		<br />
		<br />
		The Community Team
	</body>
</html>', N'Dear {%MemberUser.FirstName%},
welcome to group ''{%Group.GroupDisplayName%}''.
Best Regards,
The Community Team', N'Groups.MemberJoinedConfirmation')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (756, '20090127 20:19:55', '4c80e736-d3b4-4288-9d67-bf764634a32e', N'Groups - Member joined waiting for approval', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>Dear {%MemberUser.FirstName%},<br />
		<br />
		you are now waiting for approval to group ''{%Group.GroupDisplayName%}''.</p>
		<br />
		<br />
		Best Regards,<br />
		<br />
		The Community Team
	</body>
</html>', N'Dear {%MemberUser.FirstName%},
you are now waiting for approval to group ''{%Group.GroupDisplayName%}''.
Best Regards,
The Community Team', N'Groups.MemberJoinedWaitingForApproval')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (754, '20090127 20:18:47', '85636277-a9a7-46ba-80f0-50e3fd30bc24', N'Groups - Member leave', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>User {%MemberUser.FullName%}({%MemberUser.UserName%}) just left group ''{%Group.GroupDisplayName%}''.</p>
		<br />
		<br />
		Best Regards,<br />
		<br />
		The Community Team
	</body>
</html>', N'User {%MemberUser.FullName%}({%MemberUser.UserName%}) just left group ''{%Group.GroupDisplayName%}''.
Best Regards,
The Community Team', N'Groups.MemberLeave')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (759, '20090127 20:21:48', '0d52d7da-4ab4-4342-b52c-a94f14f23ee9', N'Groups - Member rejected', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>Dear {%MemberUser.FirstName%},<br />
		<br />
		you have been rejected from group ''{%Group.GroupDisplayName%}''.</p>
		<br />
		<br />
		Best Regards,<br />
		<br />
		The Community Team
	</body>
</html>', N'Dear {%MemberUser.FirstName%},
you have been rejected from group ''{%Group.GroupDisplayName%}''.
Best Regards,
The Community Team', N'Groups.MemberRejected')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (755, '20090127 20:19:28', '719d4419-0b2d-49db-ba8b-a5e29648d98c', N'Groups - Member waiting for approval', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>User {%MemberUser.FullName%}({%MemberUser.UserName%}) is waiting for approval into group ''{%Group.GroupDisplayName%}''.</p>
		<br />
		<br />
		Best Regards,<br />
		<br />
		The Community Team
	</body>
</html>', N'User {%MemberUser.FullName%}({%MemberUser.UserName%}) is waiting for approval into group ''{%Group.GroupDisplayName%}''.
Best Regards,
The Community Team', N'Groups.MemberWaitingForApproval')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (738, '20081217 13:39:37', '97b2f851-0693-40e6-8be1-6edd27d138d4', N'Membership - Changed password', NULL, N'<html>
<head>
</head>
<body>
Your password has been changed.<br />
<p>Your user name and new password are:</p>
<p><strong>User name:</strong> {%UserName%}<br />
<strong>Password:</strong> {%Password%}</p>
<br />
<br />
</body>
</html>', N'Your password has been changed.
Your user name and new password are:
User name: {%UserName%}
Password: {%Password%}', N'Membership.ChangedPassword')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (162, '20081217 13:40:41', 'd46985fb-e598-4b35-88e1-32bc8508aff5', N'Membership - Forgotten password', NULL, N'<html>
	<head>
	</head>
	<body style="FONT-SIZE: 12px; FONT-FAMILY: arial">
		<p>
                 You requested a forgotten password at <a href="{%LogonUrl%}">{%LogonUrl%}</a>. 
		</p>
		<p>
		Your user name and new password are:
		</p>
		<p>
		<strong>User name:</strong> {%UserName%}
                <br/>
                <strong>Password:</strong> {%Password%}
		</p>
	</body>
</html>', N'You requested a forgotten password at [url={%LogonUrl%}]{%LogonUrl%}[/url]. 
Your user name and new password are: 
User name: {%UserName%} 
Password: {%Password%}', N'forgottenPassword')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (736, '20081217 13:41:15', 'fe695a9a-5268-40d4-93ae-ff59b2f7c712', N'Membership - New password', NULL, N'<html>
<head>
</head>
<body>
<p>You requested new password.</p>
<p>Your user name and new password are:</p>
<p><strong>User name:</strong> {%UserName%}<br />
<strong>Password:</strong> {%Password%}</p>
</body>
</html>', N'You requested new password.
Your user name and new password are:
User name: {%UserName%}
Password: {%Password%}', N'Membership.NewPassword')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (164, '20081217 13:41:49', '89e8fed9-3316-435e-8ea6-630e64de1a12', N'Membership - Notification - New registration', NULL, N'<html>
	<head>
	</head>
	<body style="FONT-SIZE: 12px; FONT-FAMILY: arial">
		<p>
This is a notification that a new user has just registered:<br />
<br />
First name: {%firstname%} <br />
<br />
Last name: {%lastname%}<br />
<br />
E-mail: {%email%}<br />
<br />
User name: {%username%}<br />
<br />
This e-mail is only for your information. No action is required.
<br />
</p>
</body>
</html>', N'This is a notification that a new user has just registered:
First name: {%firstname%} 
Last name: {%lastname%}
E-mail: {%email%}
User name: {%username%}
This e-mail is only for your information. No action is required.', N'Registration.New')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (163, '20090113 16:50:30', '8ee2a9d2-0ddd-4ae9-9066-f9edc6261938', N'Membership - Notification - Waiting for approval', NULL, N'<html>
	<head>
	</head>
	<body style="FONT-SIZE: 12px; FONT-FAMILY: arial">
		<p>
This is a notification that a new user has just registered and waiting for your approval:
<br />
<br />
First name: {%firstname%} <br />
<br />
Last name: {%lastname%}<br />
<br />
E-mail: {%email%}<br />
<br />
User name: {%username%}<br />
<br />
Please go to CMS Desk or CMS Site Manager -> Administration -> Users -> Waiting for approval and approve or reject the user.<br />
</p>
</body>
</html>', N'This is a notification that a new user has just registered and waiting for your approval: 
First name: {%firstname%} 
Last name: {%lastname%}
E-mail: {%email%}
User name: {%username%}
Please go to CMS Desk or CMS Site Manager -> Administration -> Users -> Waiting for approval and approve or reject the user.', N'Registration.Approve')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (766, '20090113 15:27:26', 'dcac774e-5ddb-4645-9704-cdadb8eda10a', N'Membership - Registration', NULL, N'<html>
	<head>
	</head>
	<body>
		<p>Thank you for registering at our site. You can find your credentials below:<br />
		<br />
		Username: {%username%}<br />
		Password: {%password%}<br />
	</body>
</html>', N'Thank you for registering at our site. You can find your credentials below:
Username: {%username%}
Password: {%password%}', N'Membership.Registration')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (750, '20090113 16:55:10', '017c6bf7-16bf-4c65-ac16-03b5fa8ed5b7', N'Membership - Registration approved', NULL, N'<html>
	<head>
	</head>
	<body>
		Your registration has been approved by administrator. Now you can sign in using your username and password.  <br />
<br />
<a href="{%homepageurl%}">Click here to navigate to the web site</a>
	</body>
</html>', N'Your registration has been approved by administrator. Now you can sign in using your username and password. 
Click to the following link to navigate to the website:
{%homepageurl%}', N'RegistrationUserApproved')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (739, '20090113 15:31:41', '11e9d672-0fbc-46ae-8f0d-6cf8001578ce', N'Membership - Registration confirmation', NULL, N'<html>
	<head>
	</head>
	<body>
		Thank you for registering at our site. Please click the link below to complete your registration:  <br />
<a href="{%confirmaddress%}">{%confirmaddress%}</a>
<br />
<br />
You can find your credentials below:<br />
		Username: {%username%}<br />
		Password: {%password%}<br />
	</body>
</html>', N'Thank you for registering at our site. Please click the link below to complete your registration:
{%confirmaddress%}
You can find your credentials below:
Username: {%username%}
Password: {%password%}', N'RegistrationConfirmation')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (767, '20081217 13:45:07', '132be2c1-2db6-4c7d-8165-40041f2e4938', N'Membership - Registration waiting for approval', NULL, N'<html>
	<head>
	</head>
	<body>
		Thank you for registering at our site&nbsp;{%currentsite.sitename%}. Your registration must be approved by administrator.<br />
		<br />
		<br />
		Registration details:<br />
		<br />
		Username: {%username%}<br />
		Password: {%password%}<br />
	</body>
</html>', N'Thank you for registering at our site {%currentsite.sitename%}. Your registration must be approved by administrator.
Registration details:
Username: {%username%}
Password: {%password%}', N'Membership.RegistrationWaitingForApproval')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (737, '20081217 13:45:41', '30db61b9-2737-4e8e-90b0-0ec304baffda', N'Membership - Resend password', NULL, N'<html>
<head>
</head>
<body>
<p>You have requested the current password information.</p>
<p>Your user name and current password are:</p>
<p><strong>User name:</strong> {%UserName%}<br />
<strong>Password:</strong> {%Password%}</p>
</body>
</html>', N'You have requested the current password information.
Your user name and current password are:
User name: {%UserName%}
Password: {%Password%}', N'Membership.ResendPassword')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (719, '20090128 12:01:03', '3d863d80-a3ab-46d8-99c5-1bcd9c2bd570', N'Messaging  - Notification email', NULL, N'<html>
	<head>
	</head>
	<body style="FONT-SIZE: 12px; FONT-FAMILY: arial">
<p>
Hello {%Recipient.UserName%},
<br />
you''ve just recieved new message from user ''{%Sender.UserName%}''.
<br />
Original message:
<br />
<hr />
<br />
{%Message.MessageBody|(resolvebbcode)%}
<br/>
<hr/>
</p>
</body>
</html>', N'Hello {%Recipient.UserName%}, 
you''ve just recieved new message from user ''{%Sender.UserName%}''. 
Original message: 
--------------------------------------------------------------------------------
{%Message.MessageBody|(resolvebbcode)%} 
--------------------------------------------------------------------------------', N'messaging.messagenotification')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (762, '20090203 09:27:14', '0e0cbe1c-7349-426a-9937-dd84a20496c2', N'Newsletters - Unsubscription request', NULL, N'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Newsletter</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<style type="text/css">
h1,h2,h3,h4,h5
{
	color: #002756;
	font-family: Arial;
}
p
{
	font-family: Arial;
}
h1
{
	font-size: 16px;
	color: #ffa21e;
}
h2
{
	font-size: 14px;
}
h3
{
	font-size: 12px;
}
a
{
	color: #000000;
	text-decoration: underline;
}
img
{
	border: 0;
	padding:0;
	margin: 0;
}
body
{
	font-family: Arial;
	font-size: 12px;
}
#page
{
	margin: auto;
	width: 700px;
}
</style>
</head>
<body>
<div id="page">
<table cellspacing="0" cellpadding="0" border="0" style="width: 696px;">
    <tbody>
        <tr>
            <td><img height="75" width="255" src="/CMS/getfile/9836d166-e320-4533-b49b-8cded71ccbfc/CompanyLogo.aspx" alt="Company Logo" /></td>
        </tr>
        <tr>
            <td style="padding: 20px 0px; text-align: justify;">
            <h1>You have requested unsubscription from our newsletter - {%NewsletterDisplayName%}.</h1>
            <br />
            If you would like to unsubscribe please use the following link {%UnsubscribeLink%}</td>
        </tr>
        <tr>
            <td style="border-top: 1px solid rgb(34, 34, 34); padding: 3px 0px;"><font color="#222222" size="1">Company, address, state&nbsp; All rights reserved.</font></td>
        </tr>
    </tbody>
</table>
</div>
</body>
</html>', N'You have requested unsubscription from our newsletter - {%NewsletterDisplayName%}.
If you would like to unsubscribe please use the following link {%UnsubscribeLink%} 
Company, address, state  All rights reserved.', N'newsletter.unsubscriptionrequest')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (160, '20090126 11:48:37', '5d4c7b49-0a86-457e-b39c-79be2cc48173', N'Workflow - Document approved', NULL, N'<html>
	<head>
	</head>
	<body style="FONT-SIZE: 12px; FONT-FAMILY: arial">
		<p>
		This is an automatic notification sent by Viva CMS. The following document was approved.
		</p>
		<p>
		<strong>Document:</strong> <a href="{%applicationurl%}/cmsdesk/default.aspx?section=content&action=edit&nodeid={%nodeid%}&culture={%documentculture%}">{%documentname%}</a>
		<br />
		<strong>Approved by:</strong> {%approvedby%}
		<br />
		<strong>Approved when:</strong> {%approvedwhen%}
		<br />
		<strong>Original step:</strong> {%originalstep%}
		<br />
		<strong>Current step:</strong> {%currentstep%}
		<br />
		<strong>Comment:</strong>
		<br />
		{%comment%}
		</p>
	</body>
</html>', N'This is an automatic notification sent by Viva CMS. The following document was approved. 
Document: [url={%applicationurl%}/cmsdesk/default.aspx?section=content&action=edit&nodeid={%nodeid%}&culture=%documentculture%}]{%documentname%}[/url]
Approved by: {%approvedby%} 
Approved when: {%approvedwhen%} 
Original step: {%originalstep%} 
Current step: {%currentstep%} 
Comment: 
{%comment%}', N'Workflow.Approved')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (158, '20090126 11:48:54', '53d086cb-dc0c-4e5a-b48d-77a6b58fd549', N'Workflow - Document archived', NULL, N'<html>
	<head>
	</head>
	<body style="FONT-SIZE: 12px; FONT-FAMILY: arial">
		<p>
		This is an automatic notification sent by Viva CMS. The following document has been archived.</p>
		<p>
		<strong>Document:</strong> <a href="{%applicationurl%}/cmsdesk/default.aspx?section=content&action=edit&nodeid={%nodeid%}&culture={%documentculture%}">{%documentname%}</a>
		<br />
		<strong>Archived by:</strong> {%approvedby%}
		<br />
		<strong>Archived when:</strong> {%approvedwhen%}
		<br />
		<strong>Comment:</strong>
		<br />
		{%comment%}
		</p>
	</body>
</html>', N'This is an automatic notification sent by Viva CMS. The following document has been archived.
Document: [url={%applicationurl%}/cmsdesk/default.aspx?section=content&action=edit&nodeid={%nodeid%}&culture={%documentculture%}]{%documentname%}[/url]
Archived by: {%approvedby%}
Archived when: {%approvedwhen%}
Comment: 
{%comment%}', N'Workflow.Archived')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (159, '20090126 11:49:03', 'd2c5a1b0-c434-4427-ab81-22c0ca8f2313', N'Workflow - Document published', NULL, N'<html>
	<head>
	</head>
	<body style="FONT-SIZE: 12px; FONT-FAMILY: arial">
		<p>
		This is an automatic notification sent by Viva CMS. The following document was published.
		</p>
		<p>
		<strong>Document:</strong> <a href="{%applicationurl%}/cmsdesk/default.aspx?section=content&action=edit&nodeid={%nodeid%}&culture={%documentculture%}">{%documentname%}</a>
		<br />
		<strong>Last approved by:</strong> {%approvedby%}
		<br />
		<strong>Last approved when:</strong> {%approvedwhen%}
		<br />
		<strong>Original step:</strong> {%originalstep%}
		<br />
		<strong>Comment:</strong>
		<br />
		{%comment%}
		</p>
	</body>
</html>', N'This is an automatic notification sent by Viva CMS. The following document was published. 
Document: [url={%applicationurl%}/cmsdesk/default.aspx?section=content&action=edit&nodeid={%nodeid%}&culture={%documentculture%}]{%documentname%}[/url]
Last approved by: {%approvedby%} 
Last approved when: {%approvedwhen%} 
Original step: {%originalstep%} 
Comment: 
{%comment%}', N'Workflow.Published')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (155, '20090126 11:49:18', 'cfa7ee6c-4ee1-4594-9760-d07fe8545336', N'Workflow - Document ready for approval', NULL, N'<html>
	<head>
	</head>
	<body style="FONT-SIZE: 12px; FONT-FAMILY: arial">
		<p>
		This is an automatic notification sent by Viva CMS. The following document is waiting for your approval. Please sign in to Viva CMS Desk and approve it.
		</p>
		<p>
		<strong>Document:</strong> <a href="{%applicationurl%}/CMSDesk/default.aspx?section=content&action=edit&nodeid={%nodeid%}&culture={%documentculture%}">{%documentname%}</a>
		<br />
		<strong>Last approved by:</strong> {%approvedby%}
		<br />
		<strong>Last approved when:</strong> {%approvedwhen%}
		<br />
		<strong>Original step:</strong> {%originalstep%}
		<br />
		<strong>Current step:</strong> {%currentstep%}
		<br />
		<strong>Comment:</strong>
		<br />
		{%comment%}
		</p>
	</body>
</html>', N'This is an automatic notification sent by Viva CMS. The following document is waiting for your approval. Please sign in to Viva CMS Desk and approve it. 
Document: [url={%applicationurl%}/CMSDesk/default.aspx?section=content&action=edit&nodeid={%nodeid%}&culture={%documentculture%}]{%documentname%}[/url]
Last approved by: {%approvedby%} 
Last approved when: {%approvedwhen%} 
Original step: {%originalstep%} 
Current step: {%currentstep%} 
Comment: 
{%comment%}', N'Workflow.ReadyForApproval')
INSERT INTO [CMS_EmailTemplate] ([EmailTemplateID], [EmailTemplateLastModified], [EmailTemplateGUID], [EmailTemplateDisplayName], [EmailTemplateSiteID], [EmailTemplateText], [EmailTemplatePlainText], [EmailTemplateName]) VALUES (156, '20090126 11:49:32', '5b98fd54-1db8-4f57-b802-c7639fe08184', N'Workflow - Document rejected', NULL, N'<html>
	<head>
	</head>
	<body style="FONT-SIZE: 12px; FONT-FAMILY: arial">
		<p>
		This is an automatic notification sent by Viva CMS. The following document was rejected.
		</p>
		<p>
		<strong>Document:</strong> <a href="{%applicationurl%}/cmsdesk/default.aspx?section=content&action=edit&nodeid={%nodeid%}&culture={%documentculture%}">{%documentname%}</a>
		<br />
		<strong>Rejected by:</strong> {%approvedby%}
		<br />
		<strong>Rejected when:</strong> {%approvedwhen%}
		<br />
		<strong>Original step:</strong> {%originalstep%}
		<br />
		<strong>Current step:</strong> {%currentstep%}
		<br />
		<strong>Comment:</strong>
		<br />
		{%comment%}
		</p>
	</body>
</html>', N'This is an automatic notification sent by Viva CMS. The following document was rejected. 
Document: [url={%applicationurl%}/cmsdesk/default.aspx?section=content&action=edit&nodeid={%nodeid%}&culture={%documentculture%}]{%documentname%}[/url]
Rejected by: {%approvedby%} 
Rejected when: {%approvedwhen%} 
Original step: {%originalstep%} 
Current step: {%currentstep%} 
Comment: 
{%comment%}', N'Workflow.Rejected')
SET IDENTITY_INSERT [CMS_EmailTemplate] OFF
