<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageTemplate_General.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_General" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Page Template Edit - General" %>

<%@ Register Src="~/CMSAdminControls/MetaFiles/File.ascx" TagName="File" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/PortalEngine/FormControls/PageTemplates/PageTemplateLevels.ascx" TagName="InheritLevels" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/PortalEngine/Controls/PageTemplates/SelectPageTemplate.ascx"
    TagName="SelectPageTemplate" TagPrefix="cms" %>
    <%@ Register Src="~/CMSFormControls/System/UserControlSelector.ascx" TagPrefix="cms"
    TagName="FileSystemSelector" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">    
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />    
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblTemplateDisplayName" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtTemplateDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" /><br />
                <asp:RequiredFieldValidator ID="rfvTemplateDisplayName" runat="server" ControlToValidate="txtTemplateDisplayName"
                    Display="dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblTemplateCodeName" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtTemplateCodeName" runat="server" CssClass="TextBoxField" MaxLength="100" /><br />
                <asp:RequiredFieldValidator ID="rfvTemplateCodeName" runat="server" ControlToValidate="txtTemplateCodeName"
                    Display="dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblTemplateCategory" runat="server" />
            </td>
            <td>
                <cms:SelectPageTemplate ID="categorySelector" runat="server" ShowAdHocCategory="true" EnableCategorySelection="true" ShowTemplates="false" />
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; padding-top: 5px" class="FieldLabel">
                <asp:Label ID="lblTemplateDescription" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtTemplateDescription" runat="server" TextMode="MultiLine" CssClass="TextAreaField" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblUploadFile" runat="server" />
            </td>
            <td>
                <cms:File ID="UploadFile" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; padding-top: 5px" class="FieldLabel">
                <asp:Label ID="lblTemplateType" runat="server" />
            </td>
            <td>
                <asp:RadioButton ID="radPortal" runat="server" GroupName="PageType" AutoPostBack="true"
                    OnCheckedChanged="radPortal_CheckedChange" />
                <asp:RadioButton ID="radAspx" runat="server" GroupName="PageType" AutoPostBack="true"
                    OnCheckedChanged="radAspx_CheckedChange" /><br />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <table style="width: 100%;">
                    <tr>
                        <td class="FieldLabel" style="width: 60px;">
                            <cms:LocalizedLabel ID="lblTemplateFileName" runat="server" ResourceString="general.filename"
                                DisplayColon="true" />
                        </td>
                        <td style="">
                            <cms:FileSystemSelector ID="FileSystemSelector" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; padding-top: 5px" class="FieldLabel">
                <asp:Label ID="lblShowAsMasterTemplate" runat="server" />
            </td>
            <td>
                <asp:CheckBox ID="chkShowAsMasterTemplate" runat="server" />
            </td>
        </tr>        
        <tr id="inheritLevels" style="display:none;">
            <td class="FieldLabel" style="vertical-align: top; padding-top: 3px">
                <asp:Label ID="lblInheritLevels" runat="server" />
            </td>
            <td>
                <cms:InheritLevels ID="InheritLevels1" runat="server" />
            </td>
        </tr>        
    </table>
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
</asp:Content>
