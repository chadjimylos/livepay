<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Newsletters/MySubscriptionsWebpart.ascx.cs" Inherits="CMSWebParts_Newsletters_MySubscriptionsWebpart" %>
<%@ Register Src="~/CMSModules/Newsletters/Controls/MySubscriptions.ascx" TagName="MySubscriptions" TagPrefix="cms" %>
<cms:MySubscriptions id="ucMySubsriptions" runat="server" IsLiveSite="true" />
