using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;

using CMS.GlobalHelper;
using CMS.FormControls;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.DataEngine;
using CMS.Controls;

public partial class CMSAdminControls_UI_UniControls_UniMatrix : UniMatrix, ICallbackEventHandler, IUniPageable
{
    #region "Variables"

    private string mCallbackResult = null;
    private bool mLoaded = false;
    private DataSet ds = null;
    private int mTotalRows = 0;
    private string mNoRecordsMessage = null;
    private bool mUsePercentage = false;

    #endregion


    #region "Properties"

    /// <summary>
    /// Items per page
    /// </summary>
    public override int ItemsPerPage
    {
        get
        {
            return base.ItemsPerPage;
        }
        set
        {
            base.ItemsPerPage = value;
            this.uniPager.PageSize = value;
        }
    }


    /// <summary>
    /// Gets the value that indicates whether current selector in multiple mode displays some data or whether the dropdown contains some data
    /// </summary>
    public override bool HasData
    {
        get
        {
            // Ensure the data
            if (!StopProcessing)
            {
                ReloadData(false);
            }

            return ValidationHelper.GetBoolean(ViewState["HasData"], false);
        }
        protected set
        {
            ViewState["HasData"] = value;
        }
    }


    /// <summary>
    /// Filter where condition
    /// </summary>
    private string FilterWhere
    {
        get
        {
            return ValidationHelper.GetString(ViewState["FilterWhere"], "");
        }
        set
        {
            ViewState["FilterWhere"] = value;
        }
    }


    /// <summary>
    /// Number of expected matrix columns
    /// </summary>
    public int ColumnsCount
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["ColumnsCount"], 10);
        }
        set
        {
            ViewState["ColumnsCount"] = value;
        }
    }


    /// <summary>
    /// Sets or gets fixed width of cell. If it's 0 no fixed width is inserted in style. 
    /// </summary>
    public int FixedWidth
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["FixedWidth"], 75);
        }
        set
        {
            ViewState["FixedWidth"] = value;
        }
    }


    /// <summary>
    /// Sets or gets fixed width of first column. 
    /// </summary>
    public int FirstColumnsWidth
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["FirstColumnsWidth"], 300);
        }
        set
        {
            ViewState["FirstColumnsWidth"] = value;
        }
    }


    /// <summary>
    /// Indicates if last column will have 100% width.
    /// </summary>
    public bool LastColumnFullWidth
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["LastColumnFullWidth"], false);
        }
        set
        {
            ViewState["LastColumnFullWidth"] = value;
        }
    }


    /// <summary>
    /// Gets or sets the message which is displayed if there are no records.
    /// </summary>
    public string NoRecordsMessage
    {
        get
        {
            return mNoRecordsMessage;
        }
        set
        {
            mNoRecordsMessage = value;
        }
    }


    /// <summary>
    /// Indicates if percentage should be used in column width definition.
    /// </summary>
    public bool UsePercentage
    {
        get
        {
            return this.mUsePercentage;
        }
        set
        {
            this.mUsePercentage = value;
        }
    }


    /// <summary>
    /// UniPager control of UniMatrix.
    /// </summary>
    public UniPager Pager
    {
        get
        {
            return this.uniPager;
        }
    }

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.mContentBefore = "<table class=\"UniGridGrid\" cellspacing=\"0\" cellpadding=\"0\" rules=\"rows\" border=\"1\" style=\"border-collapse:collapse;\">";

        this.uniPager.PagedControl = this;
        this.uniPager.PageControl = "uniMatrix";
        this.uniPager.PageSize = this.ItemsPerPage;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!RequestHelper.IsCallback())
        {
            ReloadData(false);
        }

        this.plcPager.Visible = this.uniPager.Visible;
    }


    /// <summary>
    /// Reloads the control data
    /// </summary>
    /// <param name="forceReload">Force the reload of the control</param>
    public override void ReloadData(bool forceReload)
    {
        base.ReloadData(forceReload);

        // Clear filter if forced reload
        if (forceReload)
        {
            this.txtFilter.Text = "";
            this.FilterWhere = null;
        }

        if (forceReload || (!mLoaded && !this.StopProcessing))
        {
            this.plcPager.Visible = true;

            StringBuilder sb = new StringBuilder();

            GeneralConnection conn = ConnectionHelper.GetConnection();

            // Prepare the order by
            string orderBy = this.OrderBy;
            if (orderBy == null)
            {
                orderBy = this.RowItemDisplayNameColumn + " ASC";
                if (this.ColumnsCount > 1)
                {
                    orderBy += ", " + this.ColumnItemDisplayNameColumn + " ASC";
                }
            }

            int currentPage = uniPager.CurrentPage;
            string where = SqlHelperClass.AddWhereCondition(this.WhereCondition, this.FilterWhere);

            bool headersOnly = false;
            bool hasData = false;
            mTotalRows = 0;

            ArrayList columns = null;

            // Load the data
            while (true)
            {
                // Get specific page
                int pageItems = this.ColumnsCount * this.ItemsPerPage;
                ds = conn.ExecuteQuery(this.QueryName, this.QueryParameters, where, orderBy, 0, null, (currentPage - 1) * pageItems, pageItems, ref mTotalRows);
                
                hasData = !DataHelper.DataSourceIsEmpty(ds);

                // If no records found, get the records for the original dataset
                if (!hasData && !String.IsNullOrEmpty(FilterWhere))
                {
                    // Get only first line
                    ds = conn.ExecuteQuery(this.QueryName, this.QueryParameters, this.WhereCondition, orderBy, this.ColumnsCount);
                    hasData = !DataHelper.DataSourceIsEmpty(ds);
                    headersOnly = true;
                }

                // Load the list of columns
                if (hasData)
                {
                    columns = DataHelper.GetUniqueRows(ds.Tables[0], this.ColumnItemIDColumn);

                    // If more than current columns count found, and there is more data, get the correct data again
                    if ((columns.Count <= this.ColumnsCount) || (mTotalRows < pageItems))
                    {
                        break;
                    }
                    else
                    {
                        this.ColumnsCount = columns.Count;
                    }
                }
                else
                {
                    break;
                }
            }

            if (hasData)
            {
                string imagesUrl = GetImageUrl("Design/Controls/UniMatrix/", IsLiveSite, true);

                string firstColumnsWidth = (FirstColumnsWidth > 0) ? "width:" + this.FirstColumnsWidth + (UsePercentage ? "%;" : "px;") : "";

                if (!headersOnly)
                {
                    // Register the scripts
                    string script =
                        "var umImagesUrl = '" + GetImageUrl("Design/Controls/UniMatrix/", IsLiveSite, true) + "';" +
                        "function UM_ItemChanged_" + this.ClientID + "(item) { " + this.Page.ClientScript.GetCallbackEventReference(this, "item.id + ':' + (item.src.indexOf('denied.png') >= 0)", "UM_ItemSaved_" + this.ClientID, "item.id") + "; } \n" +
                        "function UM_ItemSaved_" + this.ClientID + "(rvalue, context) { var elem = document.getElementById(context); if (rvalue == 'true') { elem.src = umImagesUrl + 'allowed.png'; } else { elem.src = umImagesUrl + 'denied.png'; } } \n";

                    ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "UniMatrix_" + this.ClientID, ScriptHelper.GetScript(script));
                }

                // Render header
                this.ltlBeforeFilter.Text = "<tr class=\"UniGridHead\" align=\"left\"><th style=\"" + firstColumnsWidth + "white-space:nowrap;\" scope=\"col\"><div class=\"UniMatrixFilter\">";

                StringBuilder headersb = new StringBuilder();

                headersb.Append("</div></th>");

                string width = (FixedWidth > 0) ? "width:" + FixedWidth.ToString() + (UsePercentage ? "%;" : "px;") : "";
                foreach (DataRow dr in columns)
                {
                    if (this.ShowHeaderRow)
                    {
                        headersb.Append("<th scope=\"col\" style=\"text-align: center; white-space:nowrap; " + width + "\"");
                        if (this.ColumnItemTooltipColumn != null)
                        {
                            headersb.Append(" title=\"");
                            headersb.Append(dr[this.ColumnItemTooltipColumn]);
                            headersb.Append("\"");
                        }
                        headersb.Append(">");
                        headersb.Append(HTMLHelper.HTMLEncode(Convert.ToString(dr[this.ColumnItemDisplayNameColumn])).Replace(" ", "&nbsp;"));
                        headersb.Append("</th>\n");
                    }
                    else
                    {
                        headersb.Append("<th scope=\"col\" style=\"text-align: center; " + width + "\">&nbsp;</td>");
                    }
                }

                // Set the correct number of columns
                this.ColumnsCount = columns.Count;
                mTotalRows = mTotalRows / this.ColumnsCount;

                headersb.Append("<th " + (LastColumnFullWidth ? "style=\"width:100%;\" " : "") + ">&nbsp;</th></tr>");

                ltlAfterFilter.Text = headersb.ToString();

                if (!headersOnly)
                {
                    string lastId = "";

                    int firstIndex = 0; // columns.Count * this.ItemsPerPage * (currentPage - 1);
                    int lastIndex = firstIndex + columns.Count * this.ItemsPerPage;

                    int colIndex = 0;
                    int rowIndex = 0;

                    bool evenRow = true;

                    // Render matrix rows
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string id = ValidationHelper.GetString(dr[this.RowItemIDColumn], "");
                        if (id != lastId)
                        {
                            if (rowIndex++ >= this.ItemsPerPage)
                            {
                                break;
                            }

                            // New row
                            if (lastId != "")
                            {
                                // Close the previous row
                                sb.Append("<td style=\"white-space:nowrap;\">&nbsp;</td></tr>");
                            }
                            sb.Append("<tr class=\"" + (evenRow ? "EvenRow" : "OddRow") + "\"><td class=\"MatrixHeader\" style=\"" + firstColumnsWidth + "white-space:nowrap;\"");
                            if (this.RowItemTooltipColumn != null)
                            {
                                sb.Append(" title=\"" + HTMLHelper.HTMLEncode(ValidationHelper.GetString(DataHelper.GetDataRowValue(dr, this.RowItemTooltipColumn), "")) + "\"");
                            }
                            sb.Append(">");
                            sb.Append(HTMLHelper.HTMLEncode(Convert.ToString(dr[this.RowItemDisplayNameColumn])));
                            sb.Append("</td>\n");

                            lastId = id;
                            colIndex = 0;
                            evenRow = !evenRow;
                        }

                        // Render cell
                        sb.Append("<td style=\"white-space:nowrap; text-align: center;\"><img src=\"");
                        sb.Append(imagesUrl);
                        if (!this.Enabled || disabledColumns.Contains(colIndex))
                        {
                            // Disabled
                            if (Convert.ToInt32(dr["Allowed"]) == 1)
                            {
                                sb.Append("alloweddisabled.png");
                            }
                            else
                            {
                                sb.Append("denieddisabled.png");
                            }
                        }
                        else
                        {
                            // Enabled
                            if (Convert.ToInt32(dr["Allowed"]) == 1)
                            {
                                sb.Append("allowed.png");
                            }
                            else
                            {
                                sb.Append("denied.png");
                            }

                            sb.Append("\" id=\"");
                            sb.Append("chk:");
                            sb.Append(id);
                            sb.Append(":");
                            sb.Append(dr[this.ColumnItemIDColumn]);
                            sb.Append("\" onclick=\"UM_ItemChanged_" + this.ClientID + "(this);");
                        }

                        sb.Append("\" style=\"cursor:pointer;\" title=\"");
                        string tooltip = HTMLHelper.HTMLEncode(ValidationHelper.GetString(DataHelper.GetDataRowValue(dr, this.ItemTooltipColumn), ""));
                        sb.Append(tooltip);
                        sb.Append("\" alt=\"");
                        sb.Append(tooltip);
                        sb.Append("\" /></td>\n");

                        colIndex++;
                    }

                    // Close the latest row if present
                    if (lastId != "")
                    {
                        sb.Append("<td>&nbsp;</td></tr>");
                    }

                    this.ltlPagerBefore.Text = "<tr class=\"UniGridPager\"><td colspan=\"" + (columns.Count + 2) + "\">";
                    this.ltlPagerAfter.Text = "</td></tr>";
                }

                // Show filter / header
                bool hideFilter = ((this.FilterLimit > 0) && String.IsNullOrEmpty(FilterWhere) && (mTotalRows < this.FilterLimit));
                this.pnlFilter.Visible = !hideFilter;

                if (this.ShowFilterRow && !hideFilter)
                {
                    //this.lblFilter.ResourceString = this.ResourcePrefix + ".entersearch";
                    this.btnFilter.ResourceString = "general.search";
                }
                else if (!ShowHeaderRow)
                {
                    this.plcFilter.Visible = false;
                }
            }
            else
            {
                pnlFilter.Visible = false;

                // If norecords message set, hide everything and show message
                if (!String.IsNullOrEmpty(this.NoRecordsMessage))
                {
                    lblInfo.Text = this.NoRecordsMessage;
                    lblInfo.Visible = true;
                    ltlMatrix.Visible = false;
                    ltlContentAfter.Visible = false;
                    ltlContentBefore.Visible = false;
                }
            }

            HasData = hasData;

            this.ltlContentBefore.Text = this.ContentBefore;
            this.ltlMatrix.Text = sb.ToString();
            this.ltlContentAfter.Text = this.ContentAfter;

            mLoaded = true;

            // Call page binding event
            if (OnPageBinding != null)
            {
                OnPageBinding(this, null);
            }
        }
    }


    /// <summary>
    /// Filters the content
    /// </summary>
    protected void btnFilter_Click(object sender, EventArgs e)
    {
        this.uniPager.CurrentPage = 1;

        // Get the expression
        string expr = this.txtFilter.Text.Trim();
        if (!String.IsNullOrEmpty(expr))
        {
            // Build the where condition for display name
            this.FilterWhere = this.RowItemDisplayNameColumn + " LIKE '%" + expr.Replace("'", "''") + "%'";
        }
        else
        {
            this.FilterWhere = null;
        }

        this.txtFilter.Focus();
    }


    /// <summary>
    /// Sets pager to first page
    /// </summary>
    public void ResetPager()
    {
        uniPager.CurrentPage = 1;
    }


    #region ICallbackEventHandler Members

    /// <summary>
    /// Gets the callback result
    /// </summary>
    public string GetCallbackResult()
    {
        return mCallbackResult;
    }


    /// <summary>
    /// Processes the callback event
    /// </summary>
    public void RaiseCallbackEvent(string eventArgument)
    {
        string[] parameters = eventArgument.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
        if ((parameters != null) && (parameters.Length == 4))
        {
            int rowItemId = ValidationHelper.GetInteger(parameters[1], 0);
            int colItemId = ValidationHelper.GetInteger(parameters[2], 0);
            bool newState = ValidationHelper.GetBoolean(parameters[3], false);

            // Raise the change
            RaiseOnItemChanged(rowItemId, colItemId, newState);

            mCallbackResult = newState.ToString().ToLower();
        }
    }

    #endregion


    #region "IUniPageable Members"

    /// <summary>
    /// Pager data item object
    /// </summary>
    public object PagerDataItem
    {
        get
        {
            return ds;
        }
        set
        {
            ds = (DataSet)value;
        }
    }


    /// <summary>
    /// Occurs when the control bind data
    /// </summary>
    public event EventHandler<EventArgs> OnPageBinding;


    /// <summary>
    /// Occurs when the pager change the page and current mode is postback => reload data
    /// </summary>
    public event EventHandler<EventArgs> OnPageChanged;


    /// <summary>
    /// Evokes control databind
    /// </summary>
    public void ReBind()
    {
        if (OnPageChanged != null)
        {
            OnPageChanged(this, null);
        }

        this.DataBind();
    }


    /// <summary>
    /// Gets or sets the number of result. Enables proceed "fake" datasets, where number 
    /// of results in the dataset is not correspondent to the real number of results
    /// This property must be equal -1 if should be disabled
    /// </summary>
    public int PagerForceNumberOfResults
    {
        get
        {
            return mTotalRows;
        }
        set
        {
        }
    }

    #endregion
}
