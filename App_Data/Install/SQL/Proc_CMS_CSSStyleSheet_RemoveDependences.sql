CREATE PROCEDURE [Proc_CMS_CSSStyleSheet_RemoveDependences]
	@Id int
AS
BEGIN
	-- CMS_CssStyleSheetSite
    DELETE FROM CMS_CssStyleSheetSite WHERE StyleSheetID = @Id;
	-- CMS_Document
	UPDATE CMS_Document SET DocumentStyleSheetID = NULL WHERE DocumentStyleSheetID = @Id;
	-- CMS_Site
	UPDATE CMS_Site SET SiteDefaultStyleSheetID = NULL WHERE SiteDefaultStyleSheetID = @Id;
	UPDATE CMS_Site SET SiteDefaultEditorStylesheet = NULL WHERE SiteDefaultEditorStylesheet = @Id;
END
