using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_DiscountCoupon_Discount_List : CMSEcommercePage
{
    CurrencyInfo ci = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Page title
        this.CurrentMaster.HeaderActions.HelpTopicName = "discount_coupons_list";
        this.CurrentMaster.HeaderActions.HelpName = "helpTopic";

        // New item link
        string[,] actions = new string[1, 7];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("DiscounCoupon_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("Discount_New.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_DiscountCoupon/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        ci = CurrencyInfoProvider.GetMainCurrency();

        gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "couponvalue":
                DataRowView row = (DataRowView)parameter;
                double value = ValidationHelper.GetDouble(row["DiscountCouponValue"], 0);
                bool isFlat = ValidationHelper.GetBoolean(row["DiscountCouponIsFlatValue"], false);
                if (isFlat)
                {
                    return CurrencyInfoProvider.GetFormatedPrice(value, ci);
                }
                else
                {
                    return value.ToString() + "%";
                }
        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Discount_Edit_Frameset.aspx?discountid=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // check 'EcommerceModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
            }

            if (DiscountCouponInfoProvider.CheckDependencies(ValidationHelper.GetInteger(actionArgument, 0)))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                return;
            }

            // delete DiscountCouponInfo object from database
            DiscountCouponInfoProvider.DeleteDiscountCouponInfo(ValidationHelper.GetInteger(actionArgument, 0));
        }
    }
}
