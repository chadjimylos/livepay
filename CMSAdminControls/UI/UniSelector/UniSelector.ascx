<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UniSelector.ascx.cs" Inherits="CMSAdminControls_UI_UniSelector_UniSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<asp:Label ID="lblStatus" runat="server" EnableViewState="False" CssClass="InfoLabel" />
<asp:PlaceHolder runat="server" ID="plcButtonSelect" Visible="false" EnableViewState="false">
    <cms:LocalizedButton runat="server" ID="btnDialog" CssClass="ContentButton" />
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plcImageSelect" Visible="false" EnableViewState="false">
    <asp:Image runat="server" ID="imgDialog" CssClass="ActionImage" />
    <cms:LocalizedHyperlink runat="server" ID="lnkDialog" CssClass="ActionLink" />
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plcTextBoxSelect" Visible="false">
    <asp:TextBox ID="txtSingleSelect" runat="server" CssClass="SelectorTextBox" ReadOnly="true" /><cms:LocalizedButton
        ID="btnSelect" runat="server" CssClass="ContentButton" EnableViewState="False" /><cms:LocalizedButton
            ID="btnNew" runat="server" CssClass="ContentButton" Visible="false" EnableViewState="False" /><cms:LocalizedButton
                ID="btnEdit" runat="server" CssClass="ContentButton" Visible="false" EnableViewState="False" /><cms:LocalizedButton
                    ID="btnClear" runat="server" CssClass="ContentButton" EnableViewState="False" />
</asp:PlaceHolder>
<asp:PlaceHolder runat="server" ID="plcDropdownSelect" Visible="false">
    <asp:DropDownList ID="drpSingleSelect" runat="server" CssClass="DropDownField" />
    <cms:LocalizedButton runat="server" ID="btnDropEdit" EnableViewState="false" Visible="false"
        CssClass="ContentButton" RenderScript="true" />
</asp:PlaceHolder>
<asp:HiddenField ID="hiddenField" runat="server" EnableViewState="false" />
<asp:HiddenField ID="hiddenSelected" runat="server" EnableViewState="false" />
<asp:Panel runat="server" ID="pnlGrid" CssClass="UniSelector" Visible="false">
    <cms:Unigrid ID="uniGrid" runat="server" />
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="False" />
    <div id="UniSelectorSpacer" runat="server" class="UniSelectorSpacer">
    </div>
    <cms:LocalizedButton ID="btnRemoveAll" runat="server" CssClass="LongButton" EnableViewState="False"
        OnClick="btnRemoveAll_Click" Visible="false" /><cms:LocalizedButton ID="btnRemoveSelected"
            runat="server" CssClass="LongButton" EnableViewState="False" OnClick="btnRemoveSelected_Click" /><cms:LocalizedButton
                ID="btnAddItems" runat="server" CssClass="LongButton" EnableViewState="False" />
</asp:Panel>
<asp:HiddenField ID="hdnDialogSelect" runat="server" EnableViewState="false" />
