<%@ Page Language="VB" AutoEventWireup="false" CodeFile="INITIATION.aspx.vb" Inherits="TRANSACTION_INITIATION" %>

<html>
  <head>
    <title>INITIATION</title>
    <script>
	function SubmitForm() {
            document.forms[0].action = '<asp:literal id="APACSURL" runat="server"/>';
            document.forms[0].submit();
        }
    </script>
  </head>
  <body onLoad="SubmitForm();">
    <form id="Form1" name="initiationForm" Method="POST" runat="server">
		<input type="hidden" Name="APACScommand" id="APACScommand" value="NewPayment" runat="server" />
		<INPUT TYPE="hidden" Name="merchantID" id="merchantID" runat="server" />
		<INPUT TYPE="hidden" Name="amount" id="amount" runat="server" />
		<INPUT TYPE="hidden" Name="merchantRef" id="merchantRef" runat="server" />
		<INPUT TYPE="hidden" Name="merchantDesc" id="merchantDesc" runat="server" />
		<INPUT TYPE="hidden" Name="currency" id="currency" Value="0978" runat="server" />
		<INPUT TYPE="hidden" Name="lang" id="lang" runat="server" />
		<INPUT TYPE="hidden" Name="Var1" id="Var1" runat="server" />
		<INPUT TYPE="hidden" Name="Var2" id="Var2" runat="server" />
		<INPUT TYPE="hidden" Name="Var3" id="Var3" runat="server" />
		<INPUT TYPE="hidden" Name="Var4" id="Var4" runat="server" />
		<INPUT TYPE="hidden" Name="Var5" id="Var5" runat="server" />
		<INPUT TYPE="hidden" Name="Var6" id="Var6" runat="server" />
		<INPUT TYPE="hidden" Name="Var7" id="Var7" runat="server" />
		<INPUT TYPE="hidden" Name="Var8" id="Var8" runat="server" />
		<INPUT TYPE="hidden" Name="Var9" id="Var9" runat="server" />
		<INPUT TYPE="hidden" Name="CustomerEmail" id="CustomerEmail" runat="server" />
		<INPUT TYPE="hidden" Name="Offset" id="Offset" runat="server" />
		<INPUT TYPE="hidden" Name="Period" id="Period" runat="server" />
		<asp:Button ID="btnSubmit" Text="Proceed to payment" Runat="server" style="visibility:hidden;"/>
    </form>
  </body>
</html>