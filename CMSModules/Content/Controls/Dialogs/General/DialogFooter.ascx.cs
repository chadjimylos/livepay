using System;
using System.Collections;
using System.Web;
using System.Web.UI;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.FileManager;

public partial class CMSModules_Content_Controls_Dialogs_General_DialogFooter : CMSUserControl
{
    #region "Variables"

    private OutputFormatEnum mOutputFormat = OutputFormatEnum.HTMLMedia;
    private string CustomOutputCode = "";
    private bool mIsYouTubeDialog = false;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Base tab file path.
    /// </summary>
    private string BaseFilePath
    {
        get
        {
            if (IsLiveSite)
            {
                return "~/CMSFormControls/LiveSelectors/InsertImageOrMedia/";
            }
            else
            {
                return "~/CMSFormControls/Selectors/InsertImageOrMedia/";
            }

        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Output format of the dialog.
    /// </summary>
    public OutputFormatEnum OutputFormat
    {
        get
        {
            return mOutputFormat;
        }
        set
        {
            mOutputFormat = value;
        }
    }


    /// <summary>
    /// Inidicates if footer is used within the 'Insert YouTube video' dialog
    /// </summary>
    public bool IsYouTubeDialog
    {
        get
        {
            return mIsYouTubeDialog;
        }
        set
        {
            mIsYouTubeDialog = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (StopProcessing)
        {
            Visible = false;
        }
        else
        {
            // Setup controls
            SetupControls();
        }
    }


    #region "Public methods"

    public void InitFromQueryString()
    {
        // Get output format from query string
        string output = QueryHelper.GetString("output", "html");
        bool link = QueryHelper.GetBoolean("link", false);

        // Get output format
        OutputFormat = CMSDialogHelper.GetOutputFormat(output, link);

        // Set custom format code if required
        if (OutputFormat == OutputFormatEnum.Custom)
        {
            CustomOutputCode = output;
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Returns true if the given output format need XXXEditor.js.
    /// </summary>
    private bool UseEditorScript()
    {
        if (OutputFormat == OutputFormatEnum.Custom)
        {
            switch (CustomOutputCode.ToLower())
            {
                case "copy":
                case "move":
                case "link":
                case "linkdoc":
                    return false;
            }
        }
        return true;
    }


    /// <summary>
    /// Initializes all the nested controls
    /// </summary>
    private void SetupControls()
    {
        ScriptHelper.RegisterJQuery(Page);

        // Register scripts for current code editor
        if (UseEditorScript())
        {
            string editor = ResolveUrl("~/CMSScripts/Dialogs/" + GetEditorFileName());
            ScriptHelper.RegisterClientScriptInclude(Page, typeof(Page), "CodeEditor", editor);
        }

        if (!RequestHelper.IsPostBack())
        {
            ltlScript.Text = ScriptHelper.GetScript("function DoHiddenPostback(){" + Page.ClientScript.GetPostBackEventReference(btnHidden, "") + "}");
            if (UseEditorScript())
            {
                ltlScript.Text += ScriptHelper.GetScript("GetSelected('" + hdnSelected.ClientID + "','" + hdnAnchors.ClientID + "','" + hdnIds.ClientID + "','" + QueryHelper.GetString("editor_clientid", "") + "');");
            }
            else
            {
                ltlScript.Text += ScriptHelper.GetScript("DoHiddenPostback();");
            }
        }

        if ((OutputFormat == OutputFormatEnum.URL) || (OutputFormat == OutputFormatEnum.NodeGUID))
        {
            btnInsert.ResourceString = "general.select";
        }
        else if (OutputFormat == OutputFormatEnum.Custom)
        {
            switch (CustomOutputCode.ToLower())
            {
                case "copy":
                    btnInsert.ResourceString = "general.copy";
                    break;

                case "move":
                    btnInsert.ResourceString = "general.move";
                    break;

                case "link":
                    btnInsert.ResourceString = "general.link";
                    break;

                case "linkdoc":
                    btnInsert.ResourceString = "general.link";
                    break;

                default:
                    btnInsert.ResourceString = "general.select";
                    break;
            }
        }
    }


    /// <summary>
    /// Returns file name with javascript methods for inserting/selecting items using specified editor.
    /// File name format is XXXEditor.js where XXX is current output code.
    /// </summary>
    private string GetEditorFileName()
    {
        string output = "";
        if (IsYouTubeDialog)
        {
            output = "HTML";
        }
        else
        {
            // Select output format
            switch (OutputFormat)
            {
                case OutputFormatEnum.HTMLMedia:
                case OutputFormatEnum.HTMLLink:
                    output = "HTML";
                    break;

                case OutputFormatEnum.BBMedia:
                case OutputFormatEnum.BBLink:
                    output = "BB";
                    break;

                case OutputFormatEnum.NodeGUID:
                    output = "NodeGUID";
                    break;

                case OutputFormatEnum.URL:
                    output = "URL";
                    break;

                case OutputFormatEnum.Custom:
                    output = CustomOutputCode;
                    break;
            }
        }

        return (output + "Editor.js");
    }


    /// <summary>
    /// Setup media dialog from selected item.
    /// </summary>
    /// <param name="selectionTable">Hash table from selected item</param>
    /// <param name="anchorsList">List of anchors from document</param>
    /// <param name="idsList">List of ids from document</param>
    private void SelectMediaDialog(IDictionary selectionTable, ICollection anchorsList, ICollection idsList)
    {
        string insertHeaderLocation = BaseFilePath + "Header.aspx" + Request.Url.Query;
        if (selectionTable.Count > 0)
        {
            string siteName = null;
            string url = null;
            // If link dialog use only link url
            if (Request.Url.Query.ToLower().Contains("link=1"))
            {
                if (selectionTable[DialogParameters.LINK_URL] != null)
                {
                    url = selectionTable[DialogParameters.LINK_URL].ToString();
                    if ((selectionTable[DialogParameters.LINK_PROTOCOL] != null) && (selectionTable[DialogParameters.LINK_PROTOCOL].ToString() != "other"))
                    {
                        url = selectionTable[DialogParameters.LINK_PROTOCOL] + url;
                    }
                }
            }
            else
            {
                // Get url from selection table
                if (selectionTable[DialogParameters.IMG_URL] != null)
                {
                    url = selectionTable[DialogParameters.IMG_URL].ToString();
                }
                else if (selectionTable[DialogParameters.FLASH_URL] != null)
                {
                    url = selectionTable[DialogParameters.FLASH_URL].ToString();
                }
                else if (selectionTable[DialogParameters.AV_URL] != null)
                {
                    url = selectionTable[DialogParameters.AV_URL].ToString();
                }
                else if (selectionTable[DialogParameters.LINK_URL] != null)
                {
                    url = selectionTable[DialogParameters.LINK_URL].ToString();
                }
                else if (selectionTable[DialogParameters.URL_URL] != null)
                {
                    url = selectionTable[DialogParameters.URL_URL].ToString();
                    siteName = (selectionTable[DialogParameters.URL_SITENAME] != null ? selectionTable[DialogParameters.URL_SITENAME].ToString() : null);
                }
            }
            string query = UrlHelper.RemoveUrlParameter(Request.Url.Query, "hash");

            // Get the data for media source
            MediaSource ms = CMSDialogHelper.GetMediaData(url, siteName);
            if (ms != null)
            {
                SessionHelper.SetValue("MediaSource", ms);

                // Preselect the tab
                if (!selectionTable.Contains(DialogParameters.EMAIL_TO) || !selectionTable.Contains(DialogParameters.ANCHOR_NAME))
                {
                    switch (ms.SourceType)
                    {
                        case MediaSourceEnum.DocumentAttachments:
                            query = UrlHelper.AddUrlParameter(query, "tab", "attachments");
                            break;

                        case MediaSourceEnum.Content:
                            query = UrlHelper.AddUrlParameter(query, "tab", "content");
                            break;

                        case MediaSourceEnum.MediaLibraries:
                            query = UrlHelper.AddUrlParameter(query, "tab", "libraries");
                            break;

                        default:
                            query = UrlHelper.AddUrlParameter(query, "tab", "web");
                            break;
                    }
                }

                // Update old format url
                if ((selectionTable.Contains(DialogParameters.URL_OLDFORMAT)) && (selectionTable.Contains(DialogParameters.URL_GUID)))
                {
                    if (String.IsNullOrEmpty(siteName))
                    {
                        siteName = CMSContext.CurrentSiteName;
                    }
                    string outUrl = ModuleCommands.MediaLibraryGetMediaFileUrl(selectionTable[DialogParameters.URL_GUID].ToString(), siteName);
                    if (!String.IsNullOrEmpty(outUrl))
                    {
                        selectionTable[DialogParameters.URL_URL] = outUrl;
                    }
                }

                // Set extension if not exist in selection table
                if ((selectionTable[DialogParameters.URL_EXT] == null) || ((selectionTable[DialogParameters.URL_EXT] != null) && (String.IsNullOrEmpty(selectionTable[DialogParameters.URL_EXT].ToString()))))
                {
                    selectionTable[DialogParameters.URL_EXT] = ms.Extension;
                }

                // Update selection table if only URL presents
                if (selectionTable.Contains(DialogParameters.URL_URL))
                {
                    switch (ms.MediaType)
                    {
                        case MediaTypeEnum.Image:
                            // Image
                            selectionTable.Add(DialogParameters.IMG_URL, UrlHelper.ResolveUrl(selectionTable[DialogParameters.URL_URL].ToString()));
                            selectionTable.Add(DialogParameters.IMG_WIDTH, selectionTable[DialogParameters.URL_WIDTH]);
                            selectionTable.Add(DialogParameters.IMG_HEIGHT, selectionTable[DialogParameters.URL_HEIGHT]);
                            break;

                        case MediaTypeEnum.AudioVideo:
                            // Media
                            selectionTable.Add(DialogParameters.AV_URL, UrlHelper.ResolveUrl(selectionTable[DialogParameters.URL_URL].ToString()));
                            selectionTable.Add(DialogParameters.AV_WIDTH, selectionTable[DialogParameters.URL_WIDTH]);
                            selectionTable.Add(DialogParameters.AV_HEIGHT, selectionTable[DialogParameters.URL_HEIGHT]);
                            selectionTable.Add(DialogParameters.AV_EXT, ms.Extension);
                            break;

                        case MediaTypeEnum.Flash:
                            // Flash
                            selectionTable.Add(DialogParameters.FLASH_URL, UrlHelper.ResolveUrl(selectionTable[DialogParameters.URL_URL].ToString()));
                            selectionTable.Add(DialogParameters.FLASH_WIDTH, selectionTable[DialogParameters.URL_WIDTH]);
                            selectionTable.Add(DialogParameters.FLASH_HEIGHT, selectionTable[DialogParameters.URL_HEIGHT]);
                            break;

                        default:
                            break;
                    }

                    if ((ms.SourceType == MediaSourceEnum.Content) && (ms.AttachmentGuid != null) && (ms.FileName != null) && (OutputFormat == OutputFormatEnum.NodeGUID))
                    {
                        string fileUrl = AttachmentURLProvider.GetPermanentFileUrl(ms.NodeGuid, ms.FileName);
                        selectionTable[DialogParameters.URL_URL] = UrlHelper.ResolveUrl(fileUrl);
                    }
                    else if (OutputFormat != OutputFormatEnum.URL)
                    {
                        selectionTable[DialogParameters.URL_URL] = UrlHelper.ResolveUrl(selectionTable[DialogParameters.URL_URL].ToString());
                    }

                    selectionTable.Add(DialogParameters.FILE_NAME, ms.FileName);
                    selectionTable.Add(DialogParameters.FILE_SIZE, ms.FileSize);
                }

                // Add original size into table
                selectionTable.Add(DialogParameters.IMG_ORIGINALWIDTH, ms.MediaWidth);
                selectionTable.Add(DialogParameters.IMG_ORIGINALHEIGHT, ms.MediaHeight);
            }
            else
            {
                if (selectionTable.Contains(DialogParameters.EMAIL_TO))
                {
                    query = UrlHelper.AddUrlParameter(query, "tab", "email");
                }
                if (selectionTable.Contains(DialogParameters.ANCHOR_NAME))
                {
                    query = UrlHelper.AddUrlParameter(query, "tab", "anchor");
                }
            }

            query = UrlHelper.AddUrlParameter(query, "hash", QueryHelper.GetHash(query));
            insertHeaderLocation = BaseFilePath + "Header.aspx" + query;
        }


        // Set selected item into session
        SessionHelper.SetValue("DialogParameters", selectionTable);

        if ((anchorsList != null) && (anchorsList.Count > 0))
        {
            SessionHelper.SetValue("Anchors", anchorsList);
        }
        if ((idsList != null) && (idsList.Count > 0))
        {
            SessionHelper.SetValue("Ids", idsList);
        }

        if (((selectionTable[DialogParameters.LINK_TEXT] != null) &&
            (selectionTable[DialogParameters.LINK_TEXT].ToString() == "##LINKTEXT##")) ||
            ((selectionTable[DialogParameters.EMAIL_LINKTEXT] != null) &&
            (selectionTable[DialogParameters.EMAIL_LINKTEXT].ToString() == "##LINKTEXT##")) ||
            ((selectionTable[DialogParameters.ANCHOR_LINKTEXT] != null) &&
            (selectionTable[DialogParameters.ANCHOR_LINKTEXT].ToString() == "##LINKTEXT##")))
        {
            SessionHelper.SetValue("HideLinkText", true);
        }

        ltlScript.Text = ScriptHelper.GetScript("if (window.parent.frames['insertHeader']) { window.parent.frames['insertHeader'].location= \"" + ResolveUrl(insertHeaderLocation) + "\";} ");
    }


    /// <summary>
    /// Setup youtube dialog from selected item.
    /// </summary>
    /// <param name="selectionTable">Hash table from selected item</param>
    private void SelectYouTube(ICollection selectionTable)
    {
        if (selectionTable.Count > 0)
        {
            SessionHelper.SetValue("DialogParameters", selectionTable);
        }

        string dialogUrl = "~/CMSFormControls/Selectors/InsertYouTubeVideo/Content.aspx";
        if (IsLiveSite)
        {
            dialogUrl = "~/CMSFormControls/LiveSelectors/InsertYouTubeVideo/Content.aspx";
        }

        ltlScript.Text = ScriptHelper.GetScript("if (window.parent.frames['insertContent']) { window.parent.frames['insertContent'].location= '" + ResolveUrl(dialogUrl) + "';} ");
    }


    /// <summary>
    /// Return hastable parsed from string.
    /// </summary>
    /// <param name="Input">Input string in form key1|value1|key2|value2|...</param>
    private static Hashtable GetHashTableFromString(string Input)
    {
        Hashtable table = new Hashtable();

        if (!String.IsNullOrEmpty(Input))
        {
            string[] selection = Input.Split('|');
            for (int i = 0; i < selection.Length; i += 2)
            {
                if (selection[i + 1].Contains("%25"))
                {
                    string selectionValue = HttpUtility.UrlDecode(selection[i + 1]);
                    table.Add(selection[i], HttpUtility.UrlDecode(selectionValue));
                }
                else
                {
                    table.Add(selection[i], HttpUtility.UrlDecode(selection[i + 1]));
                }
            }
        }

        return table;
    }


    /// <summary>
    /// Return array list from input string
    /// </summary>
    /// <param name="input">Input string contains values separated with '|' (value1|value2|...)</param>
    private static ArrayList GetListFromString(string input)
    {
        ArrayList list = new ArrayList();

        if (!String.IsNullOrEmpty(input))
        {
            string[] selection = input.Split('|');
            for (int i = 0; i < selection.Length; i++)
            {
                list.Add(HttpUtility.UrlDecode(selection[i]));
            }
        }

        return list;
    }

    #endregion


    #region "Event handlers"

    protected void btnHidden_Click(object sender, EventArgs e)
    {
        SessionHelper.SetValue("MediaSource", null);
        SessionHelper.SetValue("DialogParameters", null);
        SessionHelper.SetValue("DialogSelectedParameters", null);
        SessionHelper.SetValue("Anchors", null);
        SessionHelper.SetValue("Ids", null);
        SessionHelper.SetValue("HideLinkText", null);

        string selected = hdnSelected.Value;
        Hashtable tSelection = GetHashTableFromString(selected);

        // 'Insert YouTube video' dialog
        if (IsYouTubeDialog)
        {
            SelectYouTube(tSelection);
        }
        else
        {
            // 'Insert link' dialog in FCK editor
            if (OutputFormat == OutputFormatEnum.HTMLLink)
            {
                string anchors = hdnAnchors.Value;
                string ids = hdnIds.Value;
                ArrayList lAnchors = GetListFromString(anchors);
                ArrayList lIds = GetListFromString(ids);
                lAnchors.Sort();
                lIds.Sort();
                SelectMediaDialog(tSelection, lAnchors, lIds);
            }
            // Dialogs in other editors
            else
            {
                SelectMediaDialog(tSelection, null, null);
            }
        }
    }

    #endregion
}
