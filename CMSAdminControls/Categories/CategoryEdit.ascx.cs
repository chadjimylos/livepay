using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSAdminControls_Categories_CategoryEdit : CMSAdminEditControl
{
    #region "Private fields"

    private int mCategoryID = 0;
    private int mUserID = 0;
    private bool mShowEnabled = true;
    private bool mShowCodeName = true;

    private string mRefreshPageURL = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Current category ID
    /// </summary>
    public int CategoryID
    {
        get
        {
            return this.mCategoryID;
        }
        set
        {
            this.mCategoryID = value;
        }
    }


    /// <summary>
    /// Current user ID
    /// </summary>
    public int UserID
    {
        get
        {
            return this.mUserID;
        }
        set
        {
            this.mUserID = value;
        }
    }


    /// <summary>
    /// Show enabled checkbox
    /// </summary>
    public bool ShowEnabled
    {
        get
        {
            return this.mShowEnabled;
        }
        set
        {
            this.mShowEnabled = value;
        }
    }


    /// <summary>
    /// Show code name textbox
    /// </summary>
    public bool ShowCodeName
    {
        get
        {
            return this.mShowCodeName;
        }
        set
        {
            this.mShowCodeName = value;
        }
    }


    /// <summary>
    /// The URL of the page where the result is redirected
    /// </summary>
    public string RefreshPageURL
    {
        get
        {
            return this.mRefreshPageURL;
        }
        set
        {
            this.mRefreshPageURL = value;
        }
    }


    /// <summary>
    /// Indicates whether the categor changes were saved
    /// </summary>
    public bool WasSaved
    {
        get
        {
            return (!string.IsNullOrEmpty(QueryHelper.GetString("saved", string.Empty)));
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize the controls
        SetupControl();

        if (!RequestHelper.IsPostBack())
        {
            if (this.WasSaved)
            {
                // Display information on success
                this.lblInfo.Text = ResHelper.GetString("general.changessaved");
                this.lblInfo.Visible = true;
            }
            LoadData();
        }
    }


    /// <summary>
    /// Reloads the category data in the control
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
        LoadData();
    }


    #region "Event handling"

    protected void btnSaveCategory_Click(object sender, EventArgs e)
    {
        // Check "modify" permission
        if (!this.RaiseOnCheckPermissions("Modify", this))
        {
            if ((this.UserID != CMSContext.CurrentUser.UserID) && (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify")))
            {
                RedirectToAccessDenied("CMS.Users", "Modify");
            }
        }

        // Validate form entries
        string errorMessage = ValidateForm();

        if (errorMessage == "")
        {
            CategoryInfo category = null;

            try
            {
                // Update existing item
                if (this.CategoryID > 0)
                {
                    category = CategoryInfoProvider.GetCategoryInfo(this.CategoryID);
                }
                else
                {
                    // Create a new category info
                    category = new CategoryInfo();

                    // Initialize the deault count when creating new category
                    category.CategoryCount = 0;
                }

                if (category != null)
                {
                    // Update category fields
                    category.CategoryDisplayName = this.txtDisplayName.Text.Trim();
                    category.CategoryName = this.txtCodeName.Text.Trim();
                    category.CategoryDescription = this.txtDescription.Text;
                    category.CategoryEnabled = this.chkEnabled.Checked;

                    // Set new category user to default value
                    //int userId = ValidationHelper.GetInteger(this.userSelector.Value, 0);
                    if (this.CategoryID == 0)
                    {
                        category.CategoryUserID = this.UserID;
                    }

                    // Save category in the database
                    CategoryInfoProvider.SetCategoryInfo(category);

                    if (!string.IsNullOrEmpty(this.RefreshPageURL))
                    {
                        UrlHelper.Redirect(this.RefreshPageURL + "?categoryid=" + category.CategoryID.ToString() + "&saved=1");
                    }
                    else
                    {
                        this.CategoryID = category.CategoryID;
                    }

                    RaiseOnSaved();

                    // Display information on success
                    this.lblInfo.Text = ResHelper.GetString("general.changessaved");
                    this.lblInfo.Visible = true;
                    this.lblError.Visible = false;
                }
            }
            catch (Exception ex)
            {
                // Display error message
                this.lblError.Text = ResHelper.GetString("general.erroroccurred") + " " + ex.Message;
                this.lblError.Visible = true;
            }
        }
        else
        {
            // Display error message
            this.lblError.Text = errorMessage;
            this.lblError.Visible = true;
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes the contol
    /// </summary>
    private void SetupControl()
    {
        // Intialize the control labels and error messages
        this.rfvDisplayName.ErrorMessage = ResHelper.GetString("category_edit.erroremptydisplayname");
        this.rfvCodeName.ErrorMessage = ResHelper.GetString("category_edit.erroremptycodename");
        this.btnSaveCategory.Text = ResHelper.GetString("general.ok");

        // Handle show enabled checkbox
        if (!this.ShowEnabled)
        {
            this.lblEnabled.Visible = false;
            this.chkEnabled.Visible = false;
            this.chkEnabled.Checked = true;
        }

        // Handle show code name
        if (!this.ShowCodeName)
        {
            this.lblCodeName.Visible = false;
            this.txtCodeName.Visible = false;
            this.rfvCodeName.Visible = false;
            if (this.CategoryID <= 0)
            {
                this.txtCodeName.Text = Guid.NewGuid().ToString();
            }
        }
    }


    private void LoadData()
    {

        if (this.CategoryID == 0)
        {
            this.CategoryID = QueryHelper.GetInteger("categoryid", 0);
        }

        // Handle existing category editing - prepare the data
        if (this.CategoryID > 0)
        {
            // Perform actions when editing existing category
            HandleExistingCategory();
        }
        else
        {
            // Clear textboxes
            this.txtCodeName.Text = "";
            this.txtDescription.Text = "";
            this.txtDisplayName.Text = "";
        }
    }


    /// <summary>
    /// Handles actions related to the existing category editing
    /// </summary>
    private void HandleExistingCategory()
    {
        // Get information on current category
        CategoryInfo category = CategoryInfoProvider.GetCategoryInfo(this.CategoryID);
        if (category != null)
        {
            // Pre-fill the data of current category
            this.txtDisplayName.Text = category.CategoryDisplayName;
            this.txtCodeName.Text = category.CategoryName;
            this.txtDescription.Text = category.CategoryDescription;
            this.chkEnabled.Checked = category.CategoryEnabled;
        }
    }


    /// <summary>
    /// Validates required entries
    /// </summary>
    /// <returns>True if all the necessary data were entered, otherwise false is returned</returns>
    private bool CodeNameIsUnique(string newCodeName)
    {
        CategoryInfo category = CategoryInfoProvider.GetCategoryInfo(newCodeName);
        if (category == null)
        {
            return true;
        }
        else
        {
            // If the existing category is updated the code name already exist
            if ((this.CategoryID > 0) && (this.CategoryID == category.CategoryID))
            {
                return true;
            }

            return false;
        }
    }


    /// <summary>
    /// Validates the form entries
    /// </summary>
    /// <returns>Empty string if validation passed otherwise error message is returned</returns>
    private string ValidateForm()
    {
        string errorMessage = new Validator().IsCodeName(this.txtCodeName.Text.Trim(), ResHelper.GetString("general.invalidcodename")).Result;

        if (string.IsNullOrEmpty(errorMessage))
        {
            // Validate display name & code name entry
            this.rfvDisplayName.Validate();
            this.rfvCodeName.Validate();

            if ((this.rfvDisplayName.IsValid) && (this.rfvCodeName.IsValid))
            {
                // Validate code name entry
                if (!CodeNameIsUnique(this.txtCodeName.Text.Trim()))
                {
                    errorMessage = ResHelper.GetString("category_edit.uniquecodenameerror");

                    // Display validation error message
                    this.lblError.Text = errorMessage;
                    this.lblError.Visible = true;
                }
            }
            else
            {
                errorMessage = this.rfvCodeName.ErrorMessage;
            }
        }

        return errorMessage;
    }

    #endregion
}
