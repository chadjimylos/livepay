<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomTable_Edit_Query_List.aspx.cs"
    Inherits="CMSModules_CustomTables_CustomTable_Edit_Query_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Custom table edit - Query List" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/ClassQueries.ascx" TagName="ClassQueries" TagPrefix="cms" %>
    
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:ClassQueries ID="classEditQuery" runat="server" />
</asp:Content>
