CREATE TABLE [COM_OrderStatusUser] (
		[OrderID]               int NOT NULL,
		[FromStatusID]          int NULL,
		[ToStatusID]            int NOT NULL,
		[ChangedByUserID]       int NULL,
		[Date]                  datetime NOT NULL,
		[Note]                  nvarchar(max) NULL,
		[OrderStatusUserID]     int NOT NULL IDENTITY(1, 1)
)
ON [PRIMARY]
ALTER TABLE [COM_OrderStatusUser]
	ADD
	CONSTRAINT [PK_COM_OrderStatusUser]
	PRIMARY KEY
	NONCLUSTERED
	([OrderStatusUserID])
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_OrderStatusUser]') IS NOT NULL
	AND OBJECT_ID(N'[COM_OrderStatus]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_OrderStatusUser_FromStatusID_COM_Status]') IS NULL
BEGIN
		ALTER TABLE [COM_OrderStatusUser]
			ADD CONSTRAINT [FK_COM_OrderStatusUser_FromStatusID_COM_Status]
			FOREIGN KEY ([FromStatusID]) REFERENCES [COM_OrderStatus] ([StatusID])
END
IF OBJECT_ID(N'[COM_OrderStatusUser]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_OrderStatusUser_ChangedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [COM_OrderStatusUser]
			ADD CONSTRAINT [FK_COM_OrderStatusUser_ChangedByUserID_CMS_User]
			FOREIGN KEY ([ChangedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[COM_OrderStatusUser]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Order]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_OrderStatusUser_OrderID_COM_Order]') IS NULL
BEGIN
		ALTER TABLE [COM_OrderStatusUser]
			ADD CONSTRAINT [FK_COM_OrderStatusUser_OrderID_COM_Order]
			FOREIGN KEY ([OrderID]) REFERENCES [COM_Order] ([OrderID])
END
IF OBJECT_ID(N'[COM_OrderStatusUser]') IS NOT NULL
	AND OBJECT_ID(N'[COM_OrderStatus]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_OrderStatusUser_ToStatusID_COM_Status]') IS NULL
BEGIN
		ALTER TABLE [COM_OrderStatusUser]
			ADD CONSTRAINT [FK_COM_OrderStatusUser_ToStatusID_COM_Status]
			FOREIGN KEY ([ToStatusID]) REFERENCES [COM_OrderStatus] ([StatusID])
END
CREATE INDEX [IX_COM_OrderStatusUser_FromStatusID]
	ON [COM_OrderStatusUser] ([FromStatusID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_OrderStatusUser_ChangedByUserID]
	ON [COM_OrderStatusUser] ([ChangedByUserID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_OrderStatusUser_OrderID_Date]
	ON [COM_OrderStatusUser] ([OrderID], [Date] DESC)
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_OrderStatusUser_ToStatusID]
	ON [COM_OrderStatusUser] ([ToStatusID])
	ON [PRIMARY]
