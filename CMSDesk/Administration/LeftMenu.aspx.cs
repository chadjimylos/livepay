using System;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Web.UI;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.LicenseProvider;

public partial class CMSDesk_Administration_LeftMenu : CMSAdministrationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptHelper.RegisterClientScriptBlock(this.Page, typeof(string), "AdministrationLoadItem", ScriptHelper.GetScript(
            "function LoadItem(elementName, elementUrl) \n" +
            "{ \n" +
            "    parent.frames['frameMain'].location.href = elementUrl; \n" +
            "} \n"));

        this.menuElem.RootTargetURL = "administration.aspx";
        this.menuElem.OnNodeCreated += new CMSAdminControls_UI_UIProfiles_UIMenu.NodeCreatedEventHandler(menuElem_OnNodeCreated);        
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Call the script for tab which is selected
        if (this.menuElem.MenuEmpty)
        {
            ScriptHelper.RegisterStartupScript(this.Page, typeof(string), "FirstTabSelection", ScriptHelper.GetScript(" parent.frames['frameMain'].location.href = '" + UrlHelper.ResolveUrl("~/CMSMessages/Information.aspx") + "?message=" + ResHelper.GetString("uiprofile.uinotavailable") + "'; "));
        }
    }


    protected TreeNode menuElem_OnNodeCreated(UIElementInfo uiElement, TreeNode defaultNode)
    {
        if (uiElement != null)
        {
            if (!IsAdministrationUIElementAvailable(uiElement.ElementName))
            {
                return null;
            }
        }
        return defaultNode;
    }
}
