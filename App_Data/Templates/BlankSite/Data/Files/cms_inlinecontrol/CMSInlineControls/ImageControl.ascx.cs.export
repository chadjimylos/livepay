﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.ExtendedControls;
using System.Text;
using CMS.UIControls;


public partial class CMSInlineControls_ImageControl : InlineUserControl
{
    #region "Properties"

    /// <summary>
    /// Gets or sets the value which determines whether to use the control in special mode (icon of the filetype with hovereffect).
    /// </summary>
    public bool ShowFileIcons
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("ShowFileIcons"), false);
        }
        set
        {
            this.SetValue("ShowFileIcons", value);
        }
    }


    /// <summary>
    /// URL of the image media.
    /// </summary>
    public string URL
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("URL"), null);
        }
        set
        {
            this.SetValue("URL", value);
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether to append size parameters to URL ot not.
    /// </summary>
    public bool SizeToURL
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("SizeToURL"), true);
        }
        set
        {
            this.SetValue("SizeToURL", value);
        }
    }


    /// <summary>
    /// Image extension.
    /// </summary>
    public string Extension
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Extension"), null);
        }
        set
        {
            this.SetValue("Extension", value);
        }
    }


    /// <summary>
    /// Image alternative text.
    /// </summary>
    public string Alt
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Alt"), null);
        }
        set
        {
            this.SetValue("Alt", value);
        }
    }


    /// <summary>
    /// Image width.
    /// </summary>
    public int Width
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("Width"), -1);
        }
        set
        {
            this.SetValue("Width", value);
        }
    }


    /// <summary>
    /// Image height.
    /// </summary>
    public int Height
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("Height"), -1);
        }
        set
        {
            this.SetValue("Height", value);
        }
    }


    /// <summary>
    /// Image border width.
    /// </summary>
    public int BorderWidth
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("BorderWidth"), -1);
        }
        set
        {
            this.SetValue("BorderWidth", value);
        }
    }


    /// <summary>
    /// Image border color.
    /// </summary>
    public string BorderColor
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("BorderColor"), null);
        }
        set
        {
            this.SetValue("BorderColor", value);
        }
    }

    /// <summary>
    /// Image horizontal space.
    /// </summary>
    public int HSpace
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("HSpace"), -1);
        }
        set
        {
            this.SetValue("HSpace", value);
        }
    }


    /// <summary>
    /// Image vertical space.
    /// </summary>
    public int VSpace
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("VSpace"), -1);
        }
        set
        {
            this.SetValue("VSpace", value);
        }
    }


    /// <summary>
    /// Image align.
    /// </summary>
    public string Align
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Align"), null);
        }
        set
        {
            this.SetValue("Align", value);
        }
    }


    /// <summary>
    /// Image ID.
    /// </summary>
    public string ImageID
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Id"), null);
        }
        set
        {
            this.SetValue("Id", value);
        }
    }


    /// <summary>
    /// Image tooltip text.
    /// </summary>
    public string Tooltip
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Tooltip"), null);
        }
        set
        {
            this.SetValue("Tooltip", value);
        }
    }


    /// <summary>
    /// Image css class.
    /// </summary>
    public string Class
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Class"), null);
        }
        set
        {
            this.SetValue("Class", value);
        }
    }


    /// <summary>
    /// Image inline style.
    /// </summary>
    public string Style
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Style"), null);
        }
        set
        {
            this.SetValue("Style", value);
        }
    }


    /// <summary>
    /// Image link destination.
    /// </summary>
    public string Link
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Link"), null);
        }
        set
        {
            this.SetValue("Link", value);
        }
    }


    /// <summary>
    /// Image link target (_blank/_self/_parent/_top)
    /// </summary>
    public string Target
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Target"), null);
        }
        set
        {
            this.SetValue("Target", value);
        }
    }


    /// <summary>
    /// Image behavior.
    /// </summary>
    public string Behavior
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Behavior"), null);
        }
        set
        {
            this.SetValue("Behavior", value);
        }
    }


    /// <summary>
    /// Width of the thumbnail image which is displayed when mouse is moved over the image.
    /// </summary>
    public int MouseOverWidth
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("MouseOverWidth"), 0);
        }
        set
        {
            this.SetValue("MouseOverWidth", value);
        }
    }


    /// <summary>
    /// Height of the thumbnail image which is displayed when mouse is moved over the image.
    /// </summary>
    public int MouseOverHeight
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("MouseOverHeight"), 0);
        }
        set
        {
            this.SetValue("MouseOverHeight", value);
        }
    }


    /// <summary>
    /// Control parameter
    /// </summary>
    public override string Parameter
    {
        get
        {
            return this.URL;
        }
        set
        {
            this.URL = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (this.Behavior == "hover")
        {
            StringBuilder sb = new StringBuilder();
            // If jQuery not loaded
            sb.Append("if (typeof jQuery == 'undefined') { \n");
            sb.Append("var jQueryCore=document.createElement('script'); \n");
            sb.Append("jQueryCore.setAttribute('type','text/javascript'); \n");
            sb.Append("jQueryCore.setAttribute('src', '" + ResolveUrl("~/CMSScripts/jquery/jquery-core.js") + "'); \n");
            sb.Append("setTimeout('document.body.appendChild(jQueryCore)',100); setTimeout('loadTooltip()',200); \n");
            sb.Append(" \n}");

            // If jQuery tooltip plugin not loaded
            sb.Append("var jQueryTooltips=document.createElement('script'); \n");
            sb.Append("function loadTooltip() { \n");
            sb.Append("if (typeof jQuery == 'undefined') { setTimeout('loadTooltip()',200); return;} \n");
            sb.Append("if (typeof jQuery.fn.tooltip == 'undefined') { \n");
            sb.Append("jQueryTooltips.setAttribute('type','text/javascript'); \n");
            sb.Append("jQueryTooltips.setAttribute('src', '" + ResolveUrl("~/CMSScripts/jquery/jquery-tooltips.js") + "'); \n");
            sb.Append("setTimeout('document.body.appendChild(jQueryTooltips)',100); \n");
            sb.Append(" \n}");
            sb.Append(" \n}");

            sb.Append("function hover(imgID, width, height, sizeInUrl) { \n");

            // Timer for loading jQuery or tooltip plugin
            sb.Append(" if ((typeof jQuery == 'undefined')||(typeof jQuery.fn.tooltip == 'undefined')) {\n");

            sb.Append(" var imgIDForTimeOut = imgID.replace(/\\\\/gi,\"\\\\\\\\\").replace(/'/gi,\"\\\\'\");\n");
            sb.Append(" setTimeout(\"loadTooltip();hover('\"+imgIDForTimeOut+\"',\"+width+\",\"+height+\",\"+sizeInUrl+\")\",100); return;\n");
            sb.Append(" }\n");
            // Tooltip definition
            sb.Append(" $j('img[id='+imgID+']').tooltip({ \n");
            sb.Append("     delay: 0, \n");
            sb.Append("     track: true, \n");
            sb.Append("     showBody: \" - \", \n");
            sb.Append("     showBody: \" - \", \n");
            sb.Append("     extraClass: \"ImageExtraClass\", \n");
            sb.Append("     showURL: false, \n");
            if (this.IsLiveSite)
            {
                if (CultureHelper.IsPreferredCultureRTL())
                {
                    sb.Append("     positionLeft: true, \n");
                    sb.Append("     left: -15, \n");
                }
            }
            else
            {
                if (CultureHelper.IsUICultureRTL())
                {
                    sb.Append("     positionLeft: true, \n");
                    sb.Append("     left: -15, \n");
                }
            }
            sb.Append("     bodyHandler: function() { \n");
            sb.Append("         var hidden = $j(\"#\" + imgID + \"_src\"); \n");
            sb.Append("         var source = this.src; \n");
            sb.Append("         if (hidden[0] != null) { \n");
            sb.Append("             source = hidden[0].value; \n");
            sb.Append("         } \n");
            sb.Append("         var hoverDiv = $j(\"<div/>\"); \n");
            sb.Append("         var hoverImg = $j(\"<img/>\").attr(\"class\", \"ImageTooltip\").attr(\"src\", source); \n");
            sb.Append("         hoverImg.css({'width' : width, 'height' : height}); \n");
            sb.Append("         hoverDiv.append(hoverImg); \n");
            sb.Append("         return hoverDiv;\n");
            sb.Append("     } \n");
            sb.Append(" }); \n");
            sb.Append("} \n");
            
            ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "JQueryImagePreview", ScriptHelper.GetScript(sb.ToString()));
        }

        ImageParameters imgParams = new ImageParameters();
        if (!String.IsNullOrEmpty(this.URL))
        {
            imgParams.Url = ResolveUrl(this.URL);
        }
        imgParams.Align = this.Align;
        imgParams.Alt = this.Alt;
        imgParams.Behavior = this.Behavior;
        imgParams.BorderColor = this.BorderColor;
        imgParams.BorderWidth = this.BorderWidth;
        imgParams.Class = this.Class;
        imgParams.Extension = this.Extension;
        imgParams.Height = this.Height;
        imgParams.HSpace = this.HSpace;
        imgParams.Id = (String.IsNullOrEmpty(this.ImageID) ? Guid.NewGuid().ToString() : this.ImageID);
        imgParams.Link = this.Link;
        imgParams.MouseOverHeight = this.MouseOverHeight;
        imgParams.MouseOverWidth = this.MouseOverWidth;
        imgParams.SizeToURL = this.SizeToURL;
        imgParams.Style = this.Style;
        imgParams.Target = this.Target;
        imgParams.Tooltip = this.Tooltip;
        imgParams.VSpace = this.VSpace;
        imgParams.Width = this.Width;

        if (this.ShowFileIcons && (this.Extension != null))
        {
            imgParams.Width = 0;
            imgParams.Height = 0;
            imgParams.Url = GetFileIconUrl(this.Extension, "List");
        }

        this.ltlImage.Text = MediaHelper.GetImage(imgParams);

        // Dynamic JQuery hover effect
        if (this.Behavior == "hover")
        {
            string imgId = HTMLHelper.HTMLEncode(HttpUtility.UrlDecode(imgParams.Id));
            string url = HttpUtility.HtmlDecode(this.URL);
            if (this.SizeToURL)
            {
                if (MouseOverWidth > 0)
                {
                    url = UrlHelper.UpdateParameterInUrl(url, "width", this.MouseOverWidth.ToString());
                }
                if (MouseOverHeight > 0)
                {
                    url = UrlHelper.UpdateParameterInUrl(url, "height", this.MouseOverHeight.ToString());
                }
                url = UrlHelper.RemoveParameterFromUrl(url, "maxsidesize");
            }
            this.ltlImage.Text += "<input type=\"hidden\" id=\"" + imgId + "_src\" value=\"" + ResolveUrl(url) + "\" />";

            ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "ImageHover_" + imgId, ScriptHelper.GetScript("hover(" +ScriptHelper.GetString(ScriptHelper.EscapeJQueryCharacters(imgId)) + ", " + MouseOverWidth + ", " + MouseOverHeight + ", " + (SizeToURL ? "true" : "false") + ");"));
            if (!RequestStockHelper.Contains("DialogsImageHoverStyle"))
            {
                RequestStockHelper.Add("DialogsImageHoverStyle", true);
                this.Page.Header.Controls.Add(new LiteralControl("<style type=\"text/css\">#tooltip {position: absolute;z-index:5000;}</style>"));
            }
        }
    }

    #endregion
}
