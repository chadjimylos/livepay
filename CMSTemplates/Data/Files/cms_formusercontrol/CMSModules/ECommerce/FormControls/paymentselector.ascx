<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PaymentSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_FormControls_PaymentSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%PaymentOptionDisplayName%}"
            ObjectType="ecommerce.paymentoption" ResourcePrefix="paymentselector" ReturnColumnName="PaymentOptionID"
            SelectionMode="SingleDropDownList" AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
