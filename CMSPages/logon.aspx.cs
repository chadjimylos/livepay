using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.ExtendedControls;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.OutputFilter;

public partial class CMSPages_logon : CMSPage
{
    #region "Properties"

    private LocalizedLabel mFailureLabel = null;


    /// <summary>
    /// Failure text label
    /// </summary>
    public LocalizedLabel FailureLabel
    {
        get
        {
            if (mFailureLabel == null)
            {
                mFailureLabel = (LocalizedLabel)Login1.FindControl("FailureText");
            }
            return mFailureLabel;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        SetBrowserClass();
        AddNoCacheTag();

        // Ensure the refresh script
        bool sameWindow = (FormStateHelper.PreservePostbackValuesOnLogin && (FormStateHelper.GetSavedState() != null));
        string defaultCondition = "((top.frames['cmsheader'] != null) || (top.frames['propheader'] != null))";
        if (sameWindow)
        {
            ScriptHelper.RegisterStartupScript(this, typeof(string), "TopWindow", ScriptHelper.GetScript("if (((document.body.clientWidth < 510) || (document.body.clientHeight < 300)) && " + defaultCondition + ") { var loginUrl = top.window.location.href.replace(/\\/\\(\\w\\([^\\)]+\\)\\)/g, ''); if (loginUrl.indexOf('restorepost=0') < 0) { if (loginUrl.indexOf('?') >= 0) { loginUrl = loginUrl + '&restorepost=0'; } else { loginUrl = loginUrl + '?restorepost=0'; } } top.window.location.replace(loginUrl); }"));
        }
        else
        {
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "TopWindow", ScriptHelper.GetScript("if " + defaultCondition + " { top.window.location.replace(top.window.location.href.replace(/\\/\\(\\w\\([^\\)]+\\)\\)/g, '')); }"));
        }

        // Enable caps lock check
        if (ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSUseCapsLockNotification"], true))
        {
            RegisterCAPSLOCKScript();
            TextBox txtPassword = (TextBox)Login1.FindControl("Password");
            if (txtPassword != null)
            {
                txtPassword.Attributes.Add("onkeypress", "CheckCapsLock(event)");
            }
        }


        LocalizedLabel lblItem = (LocalizedLabel)Login1.FindControl("lblUserName");
        if (lblItem != null)
        {
            lblItem.Text = "{$LogonForm.UserName$}";
        }
        lblItem = (LocalizedLabel)Login1.FindControl("lblPassword");
        if (lblItem != null)
        {
            lblItem.Text = "{$LogonForm.Password$}";
        }

        // Display culture link due to value of the key stored in the web.config file
        bool showCultureSelector = ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSShowLogonCultureSelector"], true);
        if (showCultureSelector)
        {
            ImageButton lnkLanguage = (ImageButton)Login1.FindControl("lnkLanguage");
            if (lnkLanguage != null)
            {
                lnkLanguage.Visible = true;

                // Ensure language selection panel functionality
                HtmlGenericControl pnlLanguage = (HtmlGenericControl)Login1.FindControl("pnlLanguage");
                if (pnlLanguage != null)
                {
                    lnkLanguage.ImageUrl = GetImageUrl("Others/LogonForm/LanguageButton.png");
                    lnkLanguage.AlternateText = ResHelper.GetString("LogonForm.SelectLanguage");
                    lnkLanguage.ToolTip = ResHelper.GetString("LogonForm.SelectLanguage");

                    this.ltlScript.Text = ScriptHelper.GetScript("function ShowLanguage(id){var panel=document.getElementById(id);if(panel!=null){panel.style.display=(panel.style.display == 'block')?'none':'block';}}");
                    lnkLanguage.Attributes.Add("onclick", "ShowLanguage('" + pnlLanguage.ClientID + "'); return false;");
                }
            }
        }

        LocalizedCheckBox chkItem = (LocalizedCheckBox)Login1.FindControl("chkRememberMe");
        if (chkItem != null)
        {
            chkItem.Text = "{$LogonForm.RememberMe$}";
        }
        LocalizedButton btnItem = (LocalizedButton)Login1.FindControl("LoginButton");
        if (btnItem != null)
        {
            btnItem.Text = "{$LogonForm.LogOnButton$}";
        }

        RequiredFieldValidator rfvUserName = (RequiredFieldValidator)Login1.FindControl("rfvUserNameRequired");
        if (rfvUserName != null)
        {
            rfvUserName.ToolTip = ResHelper.GetString("LogonForm.NameRequired");
            rfvUserName.ErrorMessage = ResHelper.GetString("LogonForm.NameRequired");
        }

        // Load UI cultures for the dropdown list
        if (!RequestHelper.IsPostBack())
        {
            LoadCultures();
        }

        Label lblVersion = (Label)this.FindControl("lblVersion");
        if (lblVersion != null)
        {
            lblVersion.Text = ResHelper.GetString("Footer.Version") + "&nbsp;" + CMSContext.SYSTEM_VERSION + "&nbsp;" + ResHelper.GetString("Footer.Build") + "&nbsp;" + CMSContext.FullSystemVersion;
        }

        Login1.LoggingIn += new LoginCancelEventHandler(Login1_LoggingIn);
        Login1.LoggedIn += new EventHandler(Login1_LoggedIn);
        Login1.LoginError += new EventHandler(Login1_LoginError);

        if (!RequestHelper.IsPostBack())
        {
            Login1.UserName = QueryHelper.GetString("username", String.Empty);
        }

        // Ensure username textbox focus
        TextBox txtUserName = (TextBox)Login1.FindControl("UserName");
        if (txtUserName != null)
        {
            ScriptHelper.RegisterStartupScript(this, GetType(), "SetFocus", ScriptHelper.GetScript("var txt=document.getElementById('" + txtUserName.ClientID + "');if(txt!=null){txt.focus();}"));
        }
    }


    void Login1_LoginError(object sender, EventArgs e)
    {        
        if (this.FailureLabel != null)
        {
            FailureLabel.Text = ResHelper.GetString("Login_FailureText");
        }
    }


    void Login1_LoggedIn(object sender, EventArgs e)
    {
        // Ensure response cookie
        CookieHelper.EnsureResponseCookie(FormsAuthentication.FormsCookieName);

        // Set cookie expiration
        if (Login1.RememberMeSet)
        {
            CookieHelper.ChangeCookieExpiration(FormsAuthentication.FormsCookieName, DateTime.Now.AddYears(1), false);
        }
        else
        {
            // Extend the expiration of the authentication cookie if required
            if (!UserInfoProvider.UseSessionCookies && (HttpContext.Current != null) && (HttpContext.Current.Session != null))
            {
                CookieHelper.ChangeCookieExpiration(FormsAuthentication.FormsCookieName, DateTime.Now.AddMinutes(Session.Timeout), false);
            }
        }
        
        // Current username
        string userName = Login1.UserName;

        // Check whether safe user name is required and if so get safe username
        if (RequestHelper.IsMixedAuthentication() && UserInfoProvider.UseSafeUserName)
        {
            userName = ValidationHelper.GetSafeUserName(this.Login1.UserName, CMSContext.CurrentSiteName);
            FormsAuthentication.SetAuthCookie(userName, this.Login1.RememberMeSet);
        }

        // Get info on the authenticated user
        UserInfo ui = UserInfoProvider.GetUserInfo(userName);

        // Set culture
        DropDownList drpCulture = (DropDownList)Login1.FindControl("drpCulture");
        if (drpCulture != null)
        {
            string selectedCulture = drpCulture.SelectedValue;

            // Not the default culture
            if (selectedCulture != "")
            {
                // Update the user
                ui.PreferredUICultureCode = selectedCulture;
                UserInfoProvider.SetUserInfo(ui);

                // Update current user
                CMSContext.CurrentUser.PreferredUICultureCode = selectedCulture;
            }
        }

        // Splash screen handling    
        //bool splashScreenEnabled = false;
        LicenseKeyInfo lki = LicenseKeyInfoProvider.GetLicenseKeyInfo(UrlHelper.GetCurrentDomain());
        if ((lki != null) && lki.IsTrial && ui.UserSettings.UserShowSplashScreen)
        {
            if (lki.ExpirationDate != DateTimeHelper.ZERO_TIME)
            {
                // Display splash screen only if using trial license
                //splashScreenEnabled = true;
            }
        }

        // Splash screen
        string returnUrl = QueryHelper.GetString("returnurl", string.Empty).ToLower();

        // Destination page URL
        if (returnUrl.Contains("restorepost"))
        {
            // Delete the saved state
            SavedFormState state = FormStateHelper.GetSavedState();
            if (state != null)
            {
                state.Delete();
            }

            returnUrl = UrlHelper.RemoveParameterFromUrl(returnUrl, "restorepost");
            UrlHelper.Redirect(ResolveUrl(returnUrl));
        }
    }


    void Login1_LoggingIn(object sender, LoginCancelEventArgs e)
    {
        // Ban IP addresses which are blocked for login
        if ((CMSContext.CurrentSite != null) && !BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.Login))
        {
            e.Cancel = true;
            
            if (FailureLabel != null)
            {
                FailureLabel.Visible = true;
                FailureLabel.Text = ResHelper.GetString("banip.ipisbannedlogin");
            }
        }

        if (((CheckBox)Login1.FindControl("chkRememberMe")).Checked)
        {
            Login1.RememberMeSet = true;
        }
        else
        {
            Login1.RememberMeSet = false;
        }
    }


    /// <summary>
    /// Load UI cultures for the dropdown list
    /// </summary>
    private void LoadCultures()
    {
        DropDownList drpCulture = (DropDownList)Login1.FindControl("drpCulture");
        if (drpCulture != null)
        {
            DataSet ds = UICultureInfoProvider.GetAllUICultures();
            DataView dvCultures = ds.Tables[0].DefaultView;
            dvCultures.Sort = "UICultureName ASC";

            drpCulture.DataValueField = "UICultureCode";
            drpCulture.DataTextField = "UICultureName";
            drpCulture.DataSource = dvCultures;
            drpCulture.DataBind();

            // Add default value
            drpCulture.Items.Insert(0, new ListItem(ResHelper.GetString("LogonForm.DefaultCulture"), ""));
        }
    }


    /// <summary>
    /// Registers the script to handle the CAPSLOCK check
    /// </summary>
    private void RegisterCAPSLOCKScript()
    {
        string script =
            "function OnCapslockOn() {\n " +            
                "document.getElementById('" + FailureLabel.ClientID + "').innerHTML = " + ScriptHelper.GetString(ResHelper.GetString("General.Capslock")) + "; \n" +
                "} \n" +

            "function OnCapslockOff() {\n " +            
                    "var elem = document.getElementById('" + FailureLabel.ClientID + "');\n" +
                    "if(elem.innerHTML != ''){elem.innerHTML = '';} \n" +
                "} \n";

        ScriptHelper.RegisterClientScriptBlock(Page, typeof(string), "CapsLockHandling", ScriptHelper.GetScript(script) + ScriptHelper.CapslockScript);
    }
}
