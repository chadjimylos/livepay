<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Library_Edit_Security.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Media library - Security"
    Inherits="CMSModules_Groups_Tools_MediaLibrary_Library_Edit_Security" Theme="Default" %>

<%@ Register Src="~/CMSModules/MediaLibrary/Controls/UI/MediaLibrarySecurity.ascx" TagName="MediaLibrarySecurity"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:MediaLibrarySecurity ID="librarySecurity" runat="server" />
</asp:Content>
