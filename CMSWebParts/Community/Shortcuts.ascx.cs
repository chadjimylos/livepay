using System;
using System.Web.UI;
using System.Web.Security;

using CMS.PortalControls;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.TreeEngine;
using CMS.MembershipProvider;
using CMS.Community;
using CMS.SiteProvider;
using CMS.ExtendedControls;

public partial class CMSWebParts_Community_Shortcuts : CMSAbstractWebPart, IPostBackEventHandler
{
    #region "Variables"

    /// <summary>
    /// Current user info
    /// </summary>
    protected CurrentUserInfo currentUser = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets property to mark if Sign in link should be displayed.
    /// </summary>
    public bool DisplaySignIn
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplaySignIn"), true);
        }
        set
        {
            SetValue("DisplaySignIn", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks SignIn link.
    /// </summary>
    public string SignInPath
    {
        get
        {
            // Get path from path selector
            string signInPath = ValidationHelper.GetString(GetValue("SignInPath"), "");

            // If empty then use default logon page from settings
            if (signInPath == "")
            {
                signInPath = ResolveUrl(SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSSecuredAreasLogonPage"));
            }
            else
            {
                signInPath = GetUrl(signInPath);
            }

            return signInPath;
        }
        set
        {
            SetValue("SignInPath", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Join the community link should be displayed.
    /// </summary>
    public bool DisplayJoinCommunity
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayJoinCommunity"), true);
        }
        set
        {
            SetValue("DisplayJoinCommunity", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks Join the community link.
    /// </summary>
    public string JoinCommunityPath
    {
        get
        {
            // Get path from path selector
            return ValidationHelper.GetString(GetValue("JoinCommunityPath"), "");
        }
        set
        {
            SetValue("JoinCommunityPath", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if My profile link should be displayed.
    /// </summary>
    public bool DisplayMyProfileLink
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayMyProfileLink"), true);
        }
        set
        {
            SetValue("DisplayMyProfileLink", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Edit my profile link should be displayed.
    /// </summary>
    public bool DisplayEditMyProfileLink
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayEditMyProfileLink"), true);
        }
        set
        {
            SetValue("DisplayEditMyProfileLink", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Sign out link should be displayed.
    /// </summary>
    public bool DisplaySignOut
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplaySignOut"), true);
        }
        set
        {
            SetValue("DisplaySignOut", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks Sign out link.
    /// </summary>
    public string SignOutPath
    {
        get
        {
            // Get path from path selector
            return ValidationHelper.GetString(GetValue("SignOutPath"), "");
        }
        set
        {
            SetValue("SignOutPath", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Create new group link should be displayed.
    /// </summary>
    public bool DisplayCreateNewGroup
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayCreateNewGroup"), true);
        }
        set
        {
            SetValue("DisplayCreateNewGroup", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks Create new group link.
    /// </summary>
    public string CreateNewGroupPath
    {
        get
        {
            // Get path from path selector
            return ValidationHelper.GetString(GetValue("CreateNewGroupPath"), "");
        }
        set
        {
            SetValue("CreateNewGroupPath", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Create new blok link should be displayed.
    /// </summary>
    public bool DisplayCreateNewBlog
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayCreateNewBlog"), true);
        }
        set
        {
            SetValue("DisplayCreateNewBlog", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks Create new blog link.
    /// </summary>
    public string CreateNewBlogPath
    {
        get
        {
            // Get path from path selector
            return ValidationHelper.GetString(GetValue("CreateNewBlogPath"), "");
        }
        set
        {
            SetValue("CreateNewBlogPath", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Join/Leave the group link should be displayed.
    /// </summary>
    public bool DisplayGroupLinks
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayGroupLinks"), true);
        }
        set
        {
            SetValue("DisplayGroupLinks", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks Join the group link.
    /// </summary>
    public string JoinGroupPath
    {
        get
        {
            // Get path from path selector
            return ValidationHelper.GetString(GetValue("JoinGroupPath"), "");
        }
        set
        {
            SetValue("JoinGroupPath", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Friendship link should be displayed.
    /// </summary>
    public bool DisplayFriendshipLinks
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayFriendshipLinks"), true);
        }
        set
        {
            SetValue("DisplayFriendshipLinks", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Send message link should be displayed.
    /// </summary>
    public bool DisplaySendMessage
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplaySendMessage"), true);
        }
        set
        {
            SetValue("DisplaySendMessage", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Add to contact list link should be displayed.
    /// </summary>
    public bool DisplayAddToContactList
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayAddToContactList"), true);
        }
        set
        {
            SetValue("DisplayAddToContactList", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Add to ignore list link should be displayed.
    /// </summary>
    public bool DisplayAddToIgnoreList
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayAddToIgnoreList"), true);
        }
        set
        {
            SetValue("DisplayAddToIgnoreList", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Invite to group link should be displayed.
    /// </summary>
    public bool DisplayInviteToGroup
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayInviteToGroup"), true);
        }
        set
        {
            SetValue("DisplayInviteToGroup", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks Invite to the group link.
    /// </summary>
    public string InviteGroupPath
    {
        get
        {
            // Get path from path selector
            return ValidationHelper.GetString(GetValue("InviteGroupPath"), string.Empty);
        }
        set
        {
            SetValue("InviteGroupPath", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks Leave the group link.
    /// </summary>
    public string LeaveGroupPath
    {
        get
        {
            // Get path from path selector
            return ValidationHelper.GetString(GetValue("LeaveGroupPath"), "");
        }
        set
        {
            SetValue("LeaveGroupPath", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if Manage the group should be displayed.
    /// </summary>
    public bool DisplayManageGroup
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayManageGroup"), true);
        }
        set
        {
            SetValue("DisplayManageGroup", value);
        }
    }


    /// <summary>
    /// Indicates if the messaging module is loaded
    /// </summary>
    public bool MessagingPresent
    {
        get
        {
            if (!RequestStockHelper.Contains("messagingPresent"))
            {
                RequestStockHelper.Add("messagingPresent", ModuleEntry.IsModuleLoaded(ModuleEntry.MESSAGING));
            }
            return ValidationHelper.GetBoolean(RequestStockHelper.GetItem("messagingPresent"), false);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if My messages link should be displayed.
    /// </summary>
    public bool DisplayMyMessages
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayMyMessages"), true);
        }
        set
        {
            SetValue("DisplayMyMessages", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks My messages link.
    /// </summary>
    public string MyMessagesPath
    {
        get
        {
            // Get path from path selector
            return ValidationHelper.GetString(GetValue("MyMessagesPath"), string.Empty);
        }
        set
        {
            SetValue("MyMessagesPath", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if My friends link should be displayed.
    /// </summary>
    public bool DisplayMyFriends
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayMyFriends"), true);
        }
        set
        {
            SetValue("DisplayMyFriends", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks My messages link.
    /// </summary>
    public string MyFriendsPath
    {
        get
        {
            // Get path from path selector
            return ValidationHelper.GetString(GetValue("MyFriendsPath"), string.Empty);
        }
        set
        {
            SetValue("MyFriendsPath", value);
        }
    }


    /// <summary>
    /// Gets or sets property to mark if My invitations link should be displayed.
    /// </summary>
    public bool DisplayMyInvitations
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("DisplayMyInvitations"), true);
        }
        set
        {
            SetValue("DisplayMyInvitations", value);
        }
    }


    /// <summary>
    /// Gets or sets path to be redirected to when user clicks My messages link.
    /// </summary>
    public string MyInvitationsPath
    {
        get
        {
            // Get path from path selector
            return ValidationHelper.GetString(GetValue("MyInvitationsPath"), string.Empty);
        }
        set
        {
            SetValue("MyInvitationsPath", value);
        }
    }

    #endregion


    #region "Events and methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do nothing
        }
        else
        {
            // Initialize properties
            string script = "";

            // Set current user
            currentUser = CMSContext.CurrentUser;

            // If current user is public...
            if (currentUser.IsPublic())
            {
                // Display Sign In link if set so
                if (DisplaySignIn)
                {
                    // SignInPath returns URL - because of settings value
                    lnkSignIn.NavigateUrl = SignInPath;
                    pnlSignIn.Visible = true;
                    pnlSignInOut.Visible = true;
                }

                // Display Join the community link if set so
                if (DisplayJoinCommunity)
                {
                    lnkJoinCommunity.NavigateUrl = GetUrl(JoinCommunityPath);
                    pnlJoinCommunity.Visible = true;
                    pnlPersonalLinks.Visible = true;
                }
            }
            // If user is logged in
            else
            {
                // Display Sign out link if set so
                if (DisplaySignOut)
                {
                    pnlSignOut.Visible = true;
                    pnlSignInOut.Visible = true;
                }

                // Display Edit my profile link if set so
                if (DisplayEditMyProfileLink)
                {
                    lnkEditMyProfile.NavigateUrl = ResolveUrl(TreePathUtils.GetUrl(GroupMemberInfoProvider.GetMemberManagementPath(currentUser.UserName, CMSContext.CurrentSiteName)));
                    pnlEditMyProfile.Visible = true;
                    pnlProfileLinks.Visible = true;
                }

                // Display My profile link if set so
                if (DisplayMyProfileLink)
                {
                    lnkMyProfile.NavigateUrl = ResolveUrl(TreePathUtils.GetUrl(GroupMemberInfoProvider.GetMemberProfilePath(currentUser.UserName, CMSContext.CurrentSiteName)));
                    pnlMyProfile.Visible = true;
                    pnlProfileLinks.Visible = true;
                }

                // Display Create new group link if set so
                if (DisplayCreateNewGroup)
                {
                    lnkCreateNewGroup.NavigateUrl = GetUrl(CreateNewGroupPath);
                    pnlCreateNewGroup.Visible = true;
                    pnlGroupLinks.Visible = true;
                }

                // Display Create new blog link if set so
                if (DisplayCreateNewBlog)
                {
                    // Check that Community Module is present
                    ModuleEntry entry = ModuleEntry.GetModuleEntry(ModuleEntry.BLOGS);
                    if (entry != null)
                    {
                        lnkCreateNewBlog.NavigateUrl = GetUrl(CreateNewBlogPath);
                        pnlCreateNewBlog.Visible = true;
                        pnlBlogLinks.Visible = true;
                    }
                }

                // Display My messages link
                if (DisplayMyMessages)
                {
                    lnkMyMessages.NavigateUrl = GetUrl(MyMessagesPath);
                    pnlMyMessages.Visible = true;
                    pnlPersonalLinks.Visible = true;
                }

                // Display My friends link
                if (DisplayMyFriends)
                {
                    lnkMyFriends.NavigateUrl = GetUrl(MyFriendsPath);
                    pnlMyFriends.Visible = true;
                    pnlPersonalLinks.Visible = true;
                }

                // Display My invitations link
                if (DisplayMyInvitations)
                {
                    lnkMyInvitations.NavigateUrl = GetUrl(MyInvitationsPath);
                    pnlMyInvitations.Visible = true;
                    pnlPersonalLinks.Visible = true;
                }

                GroupMemberInfo gmi = null;

                if (CommunityContext.CurrentGroup != null)
                {
                    // Get group info from community context
                    GroupInfo currentGroup = CommunityContext.CurrentGroup;

                    if (DisplayGroupLinks)
                    {
                        script += "function ReloadPage(){" + ControlsHelper.GetPostBackEventReference(this, "") + "}";

                        // Display Join group link if set so and user is visiting a group page
                        gmi = GetGroupMember(CMSContext.CurrentUser.UserID, currentGroup.GroupID);
                        if (gmi == null)
                        {
                            if (String.IsNullOrEmpty(JoinGroupPath))
                            {
                                script += "function JoinToGroupRequest() {\n" +
                                "modalDialog('" + CMSContext.ResolveDialogUrl("~/CMSModules/Groups/CMSPages/JoinTheGroup.aspx") + "?groupid=" + currentGroup.GroupID + "','requestJoinToGroup', 500, 180); \n" +
                                " } \n";

                                lnkJoinGroup.Attributes.Add("onclick", "JoinToGroupRequest();return false;");
                                lnkJoinGroup.NavigateUrl = UrlHelper.CurrentURL;
                            }
                            else
                            {
                                lnkJoinGroup.NavigateUrl = GetUrl(JoinGroupPath);
                            }
                            pnlJoinGroup.Visible = true;
                            pnlGroupLinks.Visible = true;
                        }
                        else if ((gmi.MemberStatus == GroupMemberStatus.Approved) || (CMSContext.CurrentUser.IsGlobalAdministrator))
                        // Display Leave the group link if user is the group member
                        {
                            if (String.IsNullOrEmpty(LeaveGroupPath))
                            {
                                script += "function LeaveTheGroupRequest() {\n" +
                                "modalDialog('" + CMSContext.ResolveDialogUrl("~/CMSModules/Groups/CMSPages/LeaveTheGroup.aspx") + "?groupid=" + currentGroup.GroupID + "','requestLeaveThGroup', 500, 180); \n" +
                                " } \n";

                                lnkLeaveGroup.Attributes.Add("onclick", "LeaveTheGroupRequest();return false;");
                                lnkLeaveGroup.NavigateUrl = UrlHelper.CurrentURL;
                            }
                            else
                            {
                                lnkLeaveGroup.NavigateUrl = GetUrl(LeaveGroupPath);
                            }

                            pnlLeaveGroup.Visible = true;
                            pnlGroupLinks.Visible = true;
                        }
                    }

                    // Display Manage the group link if set so and user is logged as group administrator and user is visiting a group page
                    if (DisplayManageGroup && (currentUser.IsGroupAdministrator(currentGroup.GroupID) || (currentUser.IsGlobalAdministrator)))
                    {
                        lnkManageGroup.NavigateUrl = ResolveUrl(TreePathUtils.GetUrl(GroupInfoProvider.GetGroupManagementPath(currentGroup.GroupName, CMSContext.CurrentSiteName)));
                        pnlManageGroup.Visible = true;
                        pnlGroupLinks.Visible = true;
                    }
                }

                if (DisplayInviteToGroup)
                {
                    // Get group info from community context
                    GroupInfo currentGroup = CommunityContext.CurrentGroup;
                    // Get user info from site context
                    UserInfo siteContextUser = SiteContext.CurrentUser;

                    // Display invite to group link for user who is visiting a group page
                    if (currentGroup != null)
                    {
                        // Get group user
                        if (gmi == null)
                        {
                            gmi = GetGroupMember(CMSContext.CurrentUser.UserID, currentGroup.GroupID);
                        }

                        if (((gmi != null) && (gmi.MemberStatus == GroupMemberStatus.Approved)) || (CMSContext.CurrentUser.IsGlobalAdministrator))
                        {
                            pnlInviteToGroup.Visible = true;

                            if (String.IsNullOrEmpty(InviteGroupPath))
                            {
                                script += "function InviteToGroup() {\n modalDialog('" + CMSContext.ResolveDialogUrl("~/CMSModules/Groups/CMSPages/InviteToGroup.aspx") + "?groupid=" + currentGroup.GroupID + "','inviteToGroup', 500, 310); \n } \n";
                                lnkInviteToGroup.Attributes.Add("onclick", "InviteToGroup();return false;");
                                lnkInviteToGroup.NavigateUrl = UrlHelper.CurrentURL;
                            }
                            else
                            {
                                lnkInviteToGroup.NavigateUrl = GetUrl(InviteGroupPath);
                            }
                        }
                    }
                    // Display invite to group link for user who is visiting another user's page
                    else if ((siteContextUser != null) && (siteContextUser.UserName != currentUser.UserName) && (GroupInfoProvider.GetUserGroupsCount(currentUser, CMSContext.CurrentSite) != 0))
                    {
                        pnlInviteToGroup.Visible = true;

                        if (String.IsNullOrEmpty(InviteGroupPath))
                        {
                            script += "function InviteToGroup() {\n modalDialog('" + CMSContext.ResolveDialogUrl("~/CMSModules/Groups/CMSPages/InviteToGroup.aspx") + "?invitedid=" + siteContextUser.UserID + "','inviteToGroup', 500, 310); \n } \n";
                            lnkInviteToGroup.Attributes.Add("onclick", "InviteToGroup();return false;");
                            lnkInviteToGroup.NavigateUrl = UrlHelper.CurrentURL;
                        }
                        else
                        {
                            lnkInviteToGroup.NavigateUrl = GetUrl(InviteGroupPath);
                        }
                    }
                }

                if (SiteContext.CurrentUser != null)
                {
                    // Get user info from site context
                    UserInfo siteContextUser = SiteContext.CurrentUser;

                    // Display Friendship link if set so and user is visiting an user's page
                    if (DisplayFriendshipLinks && (currentUser.UserID != siteContextUser.UserID))
                    {
                        FriendshipStatusEnum status = CMSContext.CurrentUser.HasFriend(siteContextUser.UserID);
                        switch (status)
                        {
                            case FriendshipStatusEnum.Approved:
                                // Friendship rejection
                                script += "function ShortcutFriendshipReject(id) { \n" +
                                        "modalDialog('" + CMSContext.ResolveDialogUrl("~/CMSModules/Friends/CMSPages/Friends_Reject.aspx") + "?userid=" + currentUser.UserID + "&requestid=' + id , 'rejectFriend', 410, 270); \n" +
                                        " } \n";

                                lnkRejectFriendship.Attributes.Add("onclick", "ShortcutFriendshipReject('" + siteContextUser.UserID + "');return false;");
                                lnkRejectFriendship.NavigateUrl = UrlHelper.CurrentURL;
                                pnlRejectFriendship.Visible = true;
                                pnlFriendshipLinks.Visible = true;
                                break;

                            case FriendshipStatusEnum.None:
                                requestFriendshipElem.UserID = currentUser.UserID;
                                requestFriendshipElem.RequestedUserID = siteContextUser.UserID;
                                requestFriendshipElem.LinkText = ResHelper.GetString("shortcuts.addasfriend");
                                pnlFriendshipLink.Visible = true;
                                pnlFriendshipLinks.Visible = true;
                                break;
                        }
                    }

                    // Show messaging links if enabled
                    if (MessagingPresent && (currentUser.UserID != siteContextUser.UserID))
                    {
                        // Display Send message link if user is visiting an user's page
                        if (DisplaySendMessage)
                        {
                            // Send private message
                            script += "function ShortcutPrivateMessage(id) { \n" +
                                    "modalDialog('" + CMSContext.ResolveDialogUrl("~/CMSModules/Messaging/CMSPages/SendMessage.aspx") + "?userid=" + currentUser.UserID + "&requestid=' + id , 'sendMessage', 390, 390); \n" +
                                    " } \n";

                            lnkSendMessage.Attributes.Add("onclick", "ShortcutPrivateMessage('" + siteContextUser.UserID + "');return false;");
                            lnkSendMessage.NavigateUrl = UrlHelper.CurrentURL;
                            pnlSendMessage.Visible = true;
                            pnlMessageLinks.Visible = true;
                        }

                        // Display Add to contact list link if user is visiting an user's page
                        if (DisplayAddToContactList)
                        {
                            // Check if user is in contact list
                            bool isInContactList = ModuleCommands.MessagingIsInContactList(currentUser.UserID, siteContextUser.UserID);

                            // Add to actions
                            if (!isInContactList)
                            {
                                lnkAddToContactList.Attributes.Add("onclick", "return ShortcutAddToContactList('" + siteContextUser.UserID + "')");
                                lnkAddToContactList.NavigateUrl = UrlHelper.CurrentURL;
                                pnlAddToContactList.Visible = true;
                                pnlMessageLinks.Visible = true;

                                // Add to contact list
                                script += "function ShortcutAddToContactList(usertoadd) { \n" +
                                        "var confirmation = confirm(" + ScriptHelper.GetString(ResHelper.GetString("messaging.contactlist.addconfirmation")) + ");" +
                                        "if(confirmation)" +
                                        "{" +
                                        "selectedIdElem = document.getElementById('" + hdnSelectedId.ClientID + "'); \n" +
                                        "if (selectedIdElem != null) { selectedIdElem.value = usertoadd;}" +
                                        ControlsHelper.GetPostBackEventReference(this, "addtocontactlist", false) +
                                        "} return confirmation;}\n";
                            }
                        }

                        // Display Add to ignore list link if user is visiting an user's page
                        if (DisplayAddToIgnoreList)
                        {
                            // Check if user is in ignore list
                            bool isInIgnoreList = ModuleCommands.MessagingIsInIgnoreList(currentUser.UserID, siteContextUser.UserID);

                            // Add to ignore list 
                            if (!isInIgnoreList)
                            {
                                lnkAddToIgnoreList.Attributes.Add("onclick", "return ShortcutAddToIgnoretList('" + siteContextUser.UserID + "')");
                                lnkAddToIgnoreList.NavigateUrl = UrlHelper.CurrentURL;
                                pnlAddToIgnoreList.Visible = true;
                                pnlMessageLinks.Visible = true;

                                // Add to ignore list
                                script += "function ShortcutAddToIgnoretList(usertoadd) { \n" +
                                        "var confirmation = confirm(" + ScriptHelper.GetString(ResHelper.GetString("messaging.ignorelist.addconfirmation")) + ");" +
                                        "if(confirmation)" +
                                        "{" +
                                        "selectedIdElem = document.getElementById('" + hdnSelectedId.ClientID + "'); \n" +
                                        "if (selectedIdElem != null) { selectedIdElem.value = usertoadd;}" +
                                        ControlsHelper.GetPostBackEventReference(this, "addtoignorelist", false) +
                                        "} return confirmation; } \n";
                            }
                        }
                    }
                }
            }

            // Register menu management scripts
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "Shortcuts", ScriptHelper.GetScript(script));

            // Register the dialog script
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        }
    }


    /// <summary>
    /// SignOut click event handler.
    /// </summary>
    protected void btnSignOut_Click(object sender, EventArgs e)
    {
        if (currentUser == null)
        {
            currentUser = CMSContext.CurrentUser;
        }
        if (CMSContext.CurrentUser.IsAuthenticated())
        {
            FormsAuthentication.SignOut();
            CMSContext.ClearShoppingCart();

            string redirectUrl = SignOutPath != "" ? GetUrl(SignOutPath) : UrlHelper.CurrentURL;

            // If the user is Windows Live user
            if (!string.IsNullOrEmpty(currentUser.UserSettings.WindowsLiveID))
            {
                string siteName = CMSContext.CurrentSiteName;

                // Get LiveID settings
                string appId = SettingsKeyProvider.GetStringValue(siteName + ".CMSApplicationID");
                string secret = SettingsKeyProvider.GetStringValue(siteName + ".CMSApplicationSecret");
                string algorithm = SettingsKeyProvider.GetStringValue(siteName + ".CMSsecurityAlgorithm");

                // Check valid Windows LiveID parameters
                if ((appId != string.Empty) && (secret != string.Empty) && (algorithm != string.Empty))
                {
                    WindowsLiveLogin wll = new WindowsLiveLogin(appId, secret, algorithm);

                    // Redirect to Windows Live 
                    redirectUrl = wll.GetLogoutUrl();
                }
            }

            CMSContext.CurrentUser = null;

            Response.Cache.SetNoStore();
            UrlHelper.Redirect(redirectUrl);
        }
    }


    /// <summary>
    /// Postback handling
    /// </summary>
    /// <param name="eventArgument">Argument of postback event</param>
    public void RaisePostBackEvent(string eventArgument)
    {
        if ((eventArgument == null))
        {
            return;
        }

        // Get ID of user
        int selectedId = ValidationHelper.GetInteger(hdnSelectedId.Value, 0);

        // Add only if messaging is present
        if (MessagingPresent)
        {
            // Get the module entry
            if (currentUser == null)
            {
                currentUser = CMSContext.CurrentUser;
            }

            // Add to contact or ignore list
            switch (eventArgument)
            {
                case "addtoignorelist":
                    ModuleCommands.MessagingAddToIgnoreList(currentUser.UserID, selectedId);
                    break;

                case "addtocontactlist":
                    ModuleCommands.MessagingAddToContactList(currentUser.UserID, selectedId);
                    break;
            }

            UrlHelper.Redirect(UrlHelper.CurrentURL);
        }
    }


    /// <summary>
    /// Gets URL from given path
    /// </summary>
    /// <param name="path">Node alias path</param>
    private string GetUrl(string path)
    {
        return ResolveUrl(TreePathUtils.GetUrl(path));
    }


    /// <summary>
    /// Returns group member info, reult is cached in request
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="groupId">Group ID</param>
    private GroupMemberInfo GetGroupMember(int userId, int groupId)
    {
        GroupMemberInfo gmi = RequestStockHelper.GetItem("CommunityShortCuts" + userId.ToString() + "_" + groupId.ToString()) as GroupMemberInfo;
        if ((gmi == null) && (!RequestStockHelper.Contains("CommunityShortCuts" + userId.ToString() + "_" + groupId.ToString())))
        {
            gmi = GroupMemberInfoProvider.GetGroupMemberInfo(userId, groupId);
            if (gmi != null)
            {
                RequestStockHelper.Add("CommunityShortCuts" + userId.ToString() + "_" + groupId.ToString(), gmi);
            }
            else
            {
                RequestStockHelper.Add("CommunityShortCuts" + userId.ToString() + "_" + groupId.ToString(), false);
            }
        }

        return gmi;
    }

    #endregion
}
