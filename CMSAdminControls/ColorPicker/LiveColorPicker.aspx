﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LiveColorPicker.aspx.cs"
    Inherits="CMSAdminControls_ColorPicker_LiveColorPicker" Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/ColorPicker/ColorPicker.ascx" TagName="ColorPicker"
    TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <meta http-equiv="pragma" content="no-cache" />
    <title>Color picker</title>
    <base target="_self" />
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>
</head>
<body class="<%= mBodyClass %>">
    <form id="form1" runat="server">
    <cms:ColorPicker ID="colorPickerElem" runat="server" />
    </form>

    <script src="colorpicker.js" type="text/javascript"></script>

</body>
</html>
