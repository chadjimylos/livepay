using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_List : CMSNewsletterPage
{
    protected int newsletterId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        newsletterId = QueryHelper.GetInteger("newsletterid", 0);
        if (newsletterId == 0)
        {
            return;
        }

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY,
            ScriptHelper.DialogScript);

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditItem",
            ScriptHelper.GetScript("function EditItem(id) { modalDialog('" +
            ResolveUrl("~\\CMSModules\\Newsletters\\Tools\\Newsletters\\Newsletter_Issue_Frameset.aspx") +
            "?issueid=' + id, 'NewsletterIssueEdit', screen.availWidth - 10, screen.availHeight - 80); }"));

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "NewItem",
            ScriptHelper.GetScript("function NewItem(id) { modalDialog('" +
            ResolveUrl("~\\CMSModules\\Newsletters\\Tools\\Newsletters\\Newsletter_Issue_New_Edit.aspx") +
            "?newsletterid=' + id, 'NewsletterNewIssue', screen.availWidth - 10, screen.availHeight - 80); }"));

        imgNewIssue.ImageUrl = GetImageUrl("Objects/Newsletter_Issue/add.png");

        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.WhereCondition = "IssueNewsletterID = " + newsletterId;
        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);

        //if (!IsPostBack)
        //{
        //    UniGrid.ReloadData();
        //}
        UniGrid.ZeroRowsText = ResHelper.GetString("Newsletter_Issue_List.NoIssuesFound");

        Newsletter news = NewsletterProvider.GetNewsletter(newsletterId);
        if ((news != null) && (news.NewsletterType != "D"))
        {
            btnNewIssue.Text = ResHelper.GetString("Newsletter_Issue_List.NewItemCaption");
            btnNewIssue.OnClientClick = "NewItem(" + newsletterId.ToString() + "); return false;";
        }
        else
        {
            pnlNewIssue.Visible = false;
        }
    }

    object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "issuesubject":
                return HttpUtility.HtmlEncode(parameter.ToString());
        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "delete")
        {
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "authorissues"))
            {
                RedirectToCMSDeskAccessDenied("cms.newsletter", "authorissues");
            }

            // delete Issue object from database
            IssueProvider.DeleteIssue(Convert.ToInt32(actionArgument));
        }
    }
}