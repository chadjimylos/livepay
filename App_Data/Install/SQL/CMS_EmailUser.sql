CREATE TABLE [CMS_EmailUser] (
		[EmailID]             int NOT NULL,
		[UserID]              int NOT NULL,
		[LastSendResult]      nvarchar(max) NULL,
		[LastSendAttempt]     datetime NULL,
		[Status]              int NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_EmailUser]
	ADD
	CONSTRAINT [PK_CMS_EmailUser]
	PRIMARY KEY
	([EmailID], [UserID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_EmailUser]
	ADD
	CONSTRAINT [DEFAULT_CMS_EmailRole_UserID]
	DEFAULT ((0)) FOR [UserID]
IF OBJECT_ID(N'[CMS_EmailUser]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Email]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_EmailUser_EmailID_CMS_Email]') IS NULL
BEGIN
		ALTER TABLE [CMS_EmailUser]
			ADD CONSTRAINT [FK_CMS_EmailUser_EmailID_CMS_Email]
			FOREIGN KEY ([EmailID]) REFERENCES [CMS_Email] ([EmailID])
END
IF OBJECT_ID(N'[CMS_EmailUser]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_EmailUser_UserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_EmailUser]
			ADD CONSTRAINT [FK_CMS_EmailUser_UserID_CMS_User]
			FOREIGN KEY ([UserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_CMS_EmailUser_Status]
	ON [CMS_EmailUser] ([Status])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
