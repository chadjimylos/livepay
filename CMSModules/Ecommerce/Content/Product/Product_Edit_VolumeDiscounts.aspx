<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_Edit_VolumeDiscounts.aspx.cs"
    Inherits="CMSModules_Ecommerce_Content_Product_Product_Edit_VolumeDiscounts"
    Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Product Edit - Volume discounts</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>
</head>
<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="pnlHeader" CssClass="PageHeaderLine">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false" />
        <asp:CheckBox CssClass="CheckBoxProd" ID="chkMarkDocAsProd" runat="server" OnCheckedChanged="chkMarkDocAsProd_CheckedChanged"
            AutoPostBack="true" Checked="true" />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlLeft" CssClass="FullTabsLeft" EnableViewState="false">
        &nbsp;
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlPropTabs" CssClass="TabsTabs">
        <asp:Panel runat="server" ID="pnlWhite" CssClass="TabsWhite">
            <cms:BasicTabControl ID="BasicTabControlMenu" runat="server" />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlRight" CssClass="FullTabsRight" EnableViewState="false">
        &nbsp;
    </asp:Panel>
    <asp:Panel ID="pnlBody" runat="server" CssClass="PageBody">
        <asp:Panel runat="server" ID="Panel1" CssClass="PageHeader" EnableViewState="false">
            <cms:PageTitle runat="server" ID="PageTitle" EnableViewState="false" />
        </asp:Panel>
        <asp:Panel ID="pnlNewItem" runat="server" CssClass="PageHeaderLine" EnableViewState="false">
            <asp:Image ID="imgNewItem" runat="server" CssClass="NewItemImage" EnableViewState="false" />
            <asp:HyperLink ID="lnkNewItem" runat="server" CssClass="NewItemLink" EnableViewState="false" />
        </asp:Panel>
        <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
            <cms:UniGrid runat="server" ID="UniGrid" GridName="Product_Edit_VolumeDiscount_List.xml"
                IsLiveSite="false" OrderBy="VolumeDiscountID" />
        </asp:Panel>
    </asp:Panel>

    <script type="text/javascript"> 
        //<![CDATA[
            // Refreshes current page when volume discount level properties are changed in modal dialog window
            function RefreshPage() 
            { 
                window.location.replace(window.location.href); 
            }
        //]]>
    </script>

    </form>
</body>
</html>
