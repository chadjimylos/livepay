<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/MessageBoards/MessageBoardUnsubscription.ascx.cs"
    Inherits="CMSWebParts_MessageBoards_MessageBoardUnsubscription" %>

<cms:LocalizedLabel runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<cms:LocalizedLabel runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
    Visible="false" />