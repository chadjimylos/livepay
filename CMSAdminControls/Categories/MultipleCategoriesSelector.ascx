<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultipleCategoriesSelector.ascx.cs"
    Inherits="CMSAdminControls_Categories_MultipleCategoriesSelector" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/Categories/CategoryEdit.ascx" TagName="CategoryEdit"
    TagPrefix="uc1" %>
<asp:Panel ID="pnlActions" runat="server">
    <asp:Panel ID="pnlNewItem" runat="server">
        <asp:Panel ID="pnlNewLink" runat="server" Style="padding: 10px;">
            <cms:CMSImage ID="imgNewItem" runat="server" ImageAlign="AbsMiddle" EnableViewState="false" />
            <asp:LinkButton ID="lnkNewItem" runat="server" CssClass="NewItemLink" OnClick="lnkNewItem_Click"
                EnableViewState="false" />
        </asp:Panel>
        <asp:Panel ID="pnlNewLine" runat="server" Style="margin-bottom: 15px; border-bottom: solid 1px #cccccc;" />
    </asp:Panel>
    <asp:Panel ID="pnlBreadCrumb" runat="server" Style="padding: 7px 0px 7px 3px; border-bottom: solid 1px #cccccc;" Visible="false" EnableViewState="false">
        <div class="PageTitleBreadCrumbsPadding" style="float:left; width:90%;">
            <asp:LinkButton ID="lnkList" runat="server" OnClick="lnkList_Click" CssClass="TitleBreadCrumb"
                EnableViewState="false" />
            <span class="TitleBreadCrumbSeparator">&nbsp;</span>
            <asp:Label ID="lblCurrent" runat="server" EnableViewState="false" CssClass="TitleBreadCrumbLast" />
        </div>
        <div class="TextRight">
            
        </div>
    </asp:Panel>
</asp:Panel>
<asp:Panel ID="pnlInfo" runat="server" Visible="false">
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" /><br />
</asp:Panel>
<asp:Panel ID="pnlUserGrid" runat="server">
    <asp:Label ID="lblUserTitle" runat="server" Font-Bold="true" EnableViewState="false"
        Style="padding-bottom: 10px; display: block;" />
    <cms:UniGrid ID="gridUser" runat="server" GridName="~/CMSModules/Content/CMSDesk/Properties/Category.xml"
        Columns="CategoryID,CategoryDisplayName,CategoryUserId" OrderBy="CategoryDisplayName" />
    <br />
</asp:Panel>
<asp:Panel ID="pnlGlobalGrid" runat="server">
    <asp:Label ID="lblGlobalTitle" runat="server" Font-Bold="true" EnableViewState="false"
        Style="padding-bottom: 10px; display: block;" />
    <cms:UniGrid ID="gridGlobal" runat="server" GridName="~/CMSModules/Content/CMSDesk/Properties/Category.xml"
        Columns="CategoryID,CategoryDisplayName,CategoryUserId" OrderBy="CategoryDisplayName" />
</asp:Panel>
<asp:Panel ID="pnlEdit" runat="server" Visible="false">
    <br />
    <uc1:CategoryEdit ID="categoryEditElem" runat="server" />
</asp:Panel>
<cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton"
    EnableViewState="false" />
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
