<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Polls_Edit_View.aspx.cs"
    Inherits="CMSModules_Groups_Tools_Polls_Polls_Edit_View" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Groups polls edit - view" %>

<%@ Register Src="~/CMSModules/Polls/Controls/PollView.ascx" TagName="PollView" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <div style="width: 300px;">
        <cms:PollView ID="PollView" runat="server" />
    </div>
</asp:Content>
