<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Newsletter_ContentEditorFooter.ascx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_ContentEditorFooter" %>
<%@ Register Src="~/CMSAdminControls/UI/Selectors/MacroSelector.ascx" TagName="MacroSelector"
    TagPrefix="cms" %>
<table width="99%" style="background-color: #E9F3FE; border-left: solid 1px #CCCCCC;
    border-right: solid 1px #CCCCCC; border-bottom: solid 1px #CCCCCC;">
    <tr>
        <td class="FormFieldLabel" style="padding: 5px; vertical-align: top;">
            <asp:Label ID="lblInsertField" runat="server" EnableViewState="false" /><br />
            <asp:DropDownList ID="lstInsertField" runat="server" CssClass="SourceFieldDropDown" /><cms:CMSButton ID="btnInsertField"
                 runat="server" CssClass="ContentButton" />
        </td>
        <td class="FormFieldLabel" onmouseover="RememberFocusedRegion();" style="padding: 5px;
            vertical-align: top;">
            <cms:MacroSelector ID="macroSelectorElm" runat="server" />
        </td>
    </tr>
</table>

<script type="text/javascript">
    //<![CDATA[
    function InsertMacro(macro)
    {
        if ((window.frames['iframeContent'] != null) && (window.frames['iframeContent'].InsertHTML != null))
	    {
	        window.frames['iframeContent'].InsertHTML(macro);
	    }
    }
    //]]>
</script>

