CREATE TABLE [Analytics_Statistics] (
		[StatisticsID]                int NOT NULL IDENTITY(1, 1),
		[StatisticsSiteID]            int NULL,
		[StatisticsCode]              nvarchar(50) NOT NULL,
		[StatisticsObjectName]        nvarchar(450) NULL,
		[StatisticsObjectID]          int NULL,
		[StatisticsObjectCulture]     nvarchar(10) NULL
)
ON [PRIMARY]
ALTER TABLE [Analytics_Statistics]
	ADD
	CONSTRAINT [PK_Analytics_Statistics]
	PRIMARY KEY
	NONCLUSTERED
	([StatisticsID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Analytics_Statistics]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Analytics_Statistics_StatisticsSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Analytics_Statistics]
			ADD CONSTRAINT [FK_Analytics_Statistics_StatisticsSiteID_CMS_Site]
			FOREIGN KEY ([StatisticsSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_Analytics_Statistics_StatisticsCode_StatisticsSiteID_StatisticsObjectID_StatisticsObjectCulture]
	ON [Analytics_Statistics] ([StatisticsCode], [StatisticsSiteID], [StatisticsObjectID], [StatisticsObjectCulture])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
