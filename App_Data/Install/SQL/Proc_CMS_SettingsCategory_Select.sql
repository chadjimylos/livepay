CREATE PROCEDURE [Proc_CMS_SettingsCategory_Select]
AS
SELECT * FROM CMS_SettingsCategory ORDER BY CategoryOrder, CategoryDisplayName;
