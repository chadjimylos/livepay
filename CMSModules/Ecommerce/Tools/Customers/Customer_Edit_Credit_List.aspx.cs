using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Customers_Customer_Edit_Credit_List : CMSEcommercePage
{
    protected int customerId = 0;
    protected CurrencyInfo mainCurrency = CurrencyInfoProvider.GetMainCurrency();

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get customerId from url
        customerId = QueryHelper.GetInteger("customerid", 0);

        // Display customer total credit        
        lblCredit.Text = ResHelper.GetString("CreditEvent_List.TotalCredit");
        lblCreditValue.Text = GetFormatedTotalCredit();

        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);
        UniGrid.OrderBy = "EventDate DESC, EventName ASC";
        UniGrid.WhereCondition = "EventCustomerID = " + customerId;

        InitialzeMsterPage();
    }


    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    private void InitialzeMsterPage()
    {
        // Set the action link
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("CreditEvent_List.NewItemCaption");
        actions[0, 3] = "~/CMSModules/Ecommerce/Tools/Customers/Customer_Edit_Credit_Edit.aspx?customerid=" + customerId;
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Ecommerce/addcreditevent.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    protected object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        // Show only date part from date-time value
        switch (sourceName.ToLower())
        {
            case "eventdate":
                DateTime date = ValidationHelper.GetDateTime(parameter, DataHelper.DATETIME_NOT_SELECTED);
                if (date != DataHelper.DATETIME_NOT_SELECTED)
                {
                    return date.ToShortDateString();
                }
                else
                {
                    return "";
                }

            case "eventcreditchange":
                return CurrencyInfoProvider.GetFormatedPrice(ValidationHelper.GetDouble(parameter, 0), mainCurrency);
        }

        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Customer_Edit_Credit_Edit.aspx?customerid=" + customerId + "&eventid=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // Check 'EcommerceModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
            {
                CMSPage.RedirectToCMSDeskAccessDenied("CMS.Ecommerce", "EcommerceModify");
            }

            // delete CreditEventInfo object from database
            CreditEventInfoProvider.DeleteCreditEventInfo(Convert.ToInt32(actionArgument));
            UniGrid.ReloadData();
            lblCreditValue.Text = GetFormatedTotalCredit();
        }
    }


    /// <summary>
    /// Returns formated total credit string
    /// </summary>
    private string GetFormatedTotalCredit()
    {
        // Get total credit in main currency
        double totalCredit = CreditEventInfoProvider.GetCustomerTotalCredit(customerId);

        // Return formated total credit according to the main currency formated string
        return CurrencyInfoProvider.GetFormatedPrice(totalCredit, mainCurrency);
    }
}
