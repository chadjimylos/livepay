using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.ExtendedControls;
using CMS.Community;

public partial class CMSModules_Groups_Tools_Forums_Posts_ForumPost_Frameset : CMSGroupPage
{
    protected string postsTreeUrl = "ForumPost_Tree.aspx?forumid=";
    protected string postsEditUrl = "ForumPost_View.aspx?forumid=";
    protected string postFrameTree = "posts_tree";
    protected string postFrameEdit = "posts_edit";

    protected void Page_Load(object sender, EventArgs e)
    {
        int forumId = ValidationHelper.GetInteger(Request.QueryString["forumid"], 0);
        if (ValidationHelper.GetInteger(Request.QueryString["saved"], 0) > 0)
        {
            postsTreeUrl += forumId.ToString() + "&saved=1";
            postsEditUrl += forumId.ToString() + "&saved=1";
        }
        else
        {
            postsTreeUrl += forumId.ToString();
            postsEditUrl += forumId.ToString();
        }

        if (CultureHelper.IsUICultureRTL())
        {
            string url = postsEditUrl;
            postsEditUrl = postsTreeUrl;
            postsTreeUrl = url;
            postFrameEdit = "posts_tree";
            postFrameTree = "posts_edit";
            ControlsHelper.ReverseFrames(this.colsFrameset);
        }
    }
}
