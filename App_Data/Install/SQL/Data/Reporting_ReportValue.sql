SET IDENTITY_INSERT [Reporting_ReportValue] ON
INSERT INTO [Reporting_ReportValue] ([ValueQuery], [ValueLastModified], [ValueDisplayName], [ValueFormatString], [ValueName], [ValueID], [ValueQueryIsStoredProcedure], [ValueReportID], [ValueGUID]) VALUES (N'select count(userid) from CMS_User', '20080822 11:15:10', N'UserCount', N'', N'UserCount', 3, 0, 142, '748d38d8-9f62-41df-b229-80280f94072b')
INSERT INTO [Reporting_ReportValue] ([ValueQuery], [ValueLastModified], [ValueDisplayName], [ValueFormatString], [ValueName], [ValueID], [ValueQueryIsStoredProcedure], [ValueReportID], [ValueGUID]) VALUES (N'SELECT Count(NodeId)
FROM View_CMS_Tree_Joined
WHERE (@OnlyPublished = 0 OR Published = @OnlyPublished)  
AND (NodeSiteID = @CMSContextCurrentSiteID)
AND (@ModifiedFrom IS NULL OR DocumentModifiedWhen >= @ModifiedFrom)
AND (@ModifiedTo IS NULL OR DocumentModifiedWhen < @ModifiedTo) 
AND (NodeAliasPath LIKE @path)
AND (@Language IS NULL OR @Language = ''-1'' OR DocumentCulture = @Language)
AND (@name IS NULL OR DocumentName LIKE ''%''+@name+''%'')', '20100304 12:08:18', N'DocumentCount', N'', N'DocumentCount', 28, 0, 186, '526030e0-b87b-4f0c-bc2a-54d12e485a38')
SET IDENTITY_INSERT [Reporting_ReportValue] OFF
