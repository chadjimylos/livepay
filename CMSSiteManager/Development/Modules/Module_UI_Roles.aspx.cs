﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;

public partial class CMSSiteManager_Development_Modules_Module_UI_Roles : SiteManagerPage
{

    #region "Variables"

    private int mElementID = 0;
    private bool noSite = false;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!noSite)
        {
            if (!RequestHelper.IsPostBack() && (CMSContext.CurrentSiteID > 0))
            {
                siteSelector.SiteID = CMSContext.CurrentSiteID;
            }

            gridMatrix.ContentBefore = "<table class=\"UIPermissionMatrix\" cellspacing=\"0\" cellpadding=\"3\" rules=\"rows\" border=\"1\" style=\"width:100%;border-collapse:collapse;\">";
            gridMatrix.ContentAfter = "</table>";
            gridMatrix.ColumnsCount = 1;
            gridMatrix.OnItemChanged += new UniMatrix.ItemChangedEventHandler(gridMatrix_OnItemChanged);

            siteSelector.DropDownSingleSelect.AutoPostBack = true;
            siteSelector.DropDownSingleSelect.SelectedIndexChanged += new EventHandler(DropDownSingleSelect_SelectedIndexChanged);            
            siteSelector.IsLiveSite = false;
        }
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (SiteInfoProvider.GetSitesCount() <= 0)
        {
            noSite = true;

            this.CurrentMaster.DisplayControlsPanel = false;
            this.plcUpdate.Visible = false;
            this.lblInfo.Text = ResHelper.GetString("uiprofile.nosite");
        }
        else
        {
            this.CurrentMaster.DisplayControlsPanel = true;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (!noSite)
        {
            // Load the matrix
            mElementID = QueryHelper.GetInteger("elementid", 0);
            if (mElementID > 0)
            {
                UIElementInfo elemInfo = UIElementInfoProvider.GetUIElementInfo(mElementID);
                if (elemInfo != null)
                {
                    lblInfo.Text = String.Format(ResHelper.GetString("resource.ui.rolesinfo"), HTMLHelper.HTMLEncode(elemInfo.ElementDisplayName));
                    
                    if (siteSelector.UniSelector.HasData)
                    {
                        gridMatrix.QueryParameters = GetQueryParameters(siteSelector.SiteID, mElementID, elemInfo.ElementDisplayName);
                        gridMatrix.WhereCondition = "RoleGroupId IS NULL";
                    }
                }
            }

            this.lblDisabled.Visible = !SettingsKeyProvider.GetBoolValue(siteSelector.SiteName + ".CMSPersonalizeUserInterface");
            if (!this.gridMatrix.HasData)
            {
                this.plcUpdate.Visible = false;
                this.lblInfo.Text = ResHelper.GetString("uiprofile.norole");
                this.lblInfo.Visible = true;
            }
            else
            {
                this.plcUpdate.Visible = true;
                this.lblInfo.Visible = false;
            }
        }
        else
        {
            this.lblDisabled.Visible = false;
        }
    }

    #region "Page events"

    protected void DropDownSingleSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        mElementID = QueryHelper.GetInteger("elementid", 0);
        if (mElementID > 0)
        {
            UIElementInfo elemInfo = UIElementInfoProvider.GetUIElementInfo(mElementID);
            if (elemInfo != null)
            {
                // Reload permission matrix
                gridMatrix.QueryParameters = GetQueryParameters(siteSelector.SiteID, mElementID, elemInfo.ElementDisplayName);
                gridMatrix.WhereCondition = "RoleGroupId IS NULL";
                gridMatrix.Pager.CurrentPage = 1;
                gridMatrix.ReloadData(true);
            }
        }
    }


    protected void gridMatrix_OnItemChanged(object sender, int rowItemId, int colItemId, bool newState)
    {
        if (newState)
        {
            RoleUIElementInfoProvider.AddRoleUIElementInfo(rowItemId, colItemId);
        }
        else
        {
            RoleUIElementInfoProvider.DeleteRoleUIElementInfo(rowItemId, colItemId);
        }

        // Invalidate all users
        UserInfo.TYPEINFO.InvalidateAllObjects();
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Returns query parameters for permission matrix.
    /// </summary>
    /// <param name="siteId">Site ID</param>
    /// <param name="elementId">Element ID</param>
    /// <param name="elementName">Element display name</param>
    /// <returns></returns>
    private object[,] GetQueryParameters(int siteId, int elementId, string elementName)
    {
        object[,] parameters = new object[3, 3];
        parameters[0, 0] = "@SiteID";
        parameters[0, 1] = siteSelector.SiteID;
        parameters[1, 0] = "@ElementID";
        parameters[1, 1] = mElementID;
        parameters[2, 0] = "@ElementDisplayName";
        parameters[2, 1] = ResHelper.LocalizeString(elementName);

        return parameters;
    }

    #endregion
}
