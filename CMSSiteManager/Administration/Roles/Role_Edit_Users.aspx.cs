using System;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSSiteManager_Administration_Roles_Role_Edit_Users : CMSRolesPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        roleUsersElem.RoleID = QueryHelper.GetInteger("roleid", 0);
        roleUsersElem.IsLiveSite = false;
        roleUsersElem.OnCheckPermissions += new CMSAdminControl.CheckPermissionsEventHandler(roleUsersElem_OnCheckPermissions);
    }

    
    protected void roleUsersElem_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Roles", permissionType))
        {
            RedirectToCMSDeskAccessDenied("CMS.Roles", permissionType);
        }
    }
}
