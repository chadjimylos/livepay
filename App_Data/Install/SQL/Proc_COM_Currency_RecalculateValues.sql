CREATE PROCEDURE [Proc_COM_Currency_RecalculateValues]
    @rate real,
    @round int,
    @updateCurrencyExchangeRate bit,
    @updateSKU bit,
    @updateTaxClass bit,
    @updateDiscountCoupon bit,
    @updateVolumeDiscount bit,
    @updateCustomerCreditHistory bit,
    @updateShippingOption bit,
    @updateFreeShipping bit
AS
IF @updateCurrencyExchangeRate = 1
    BEGIN
        UPDATE COM_CurrencyExchangeRate SET ExchangeRateValue = ROUND(ExchangeRateValue / @rate, @round)
    END
IF @updateSKU = 1
    BEGIN
        UPDATE COM_SKU SET SKUPrice = ROUND(SKUPrice * @rate, @round)
    END
IF @updateTaxClass = 1
    BEGIN
        UPDATE COM_TaxClassCountry SET TaxValue = ROUND(TaxValue * @rate, @round) WHERE IsFlatValue = 1
        UPDATE COM_TaxClassState SET TaxValue = ROUND(TaxValue * @rate, @round) WHERE IsFlatValue = 1
    END
IF @updateDiscountCoupon = 1
    BEGIN
        UPDATE COM_DiscountCoupon SET DiscountCouponValue = ROUND(DiscountCouponValue * @rate, @round) WHERE DiscountCouponIsFlatValue = 1
    END
IF @updateVolumeDiscount = 1
    BEGIN
        UPDATE COM_VolumeDiscount SET VolumeDiscountValue = ROUND(VolumeDiscountValue * @rate, @round) WHERE VolumeDiscountIsFlatValue = 1
    END
IF @updateCustomerCreditHistory = 1
    BEGIN
        UPDATE COM_CustomerCreditHistory SET EventCreditChange = ROUND(EventCreditChange * @rate, @round)
    END
IF @updateShippingOption = 1
    BEGIN
        UPDATE COM_ShippingOption SET ShippingOptionCharge = ROUND(ShippingOptionCharge * @rate, @round)
    END
IF @updateFreeShipping = 1
    BEGIN
        UPDATE CMS_Site SET SiteStoreShippingFreeLimit = ROUND(SiteStoreShippingFreeLimit * @rate, @round) WHERE SiteStoreShippingFreeLimit > 0
    END
