Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.UI

Imports CMS.CMSHelper
Imports CMS.GlobalHelper
Imports CMS.PortalControls

Imports CMS.SiteProvider
Imports CMS.SettingsProvider
Imports CMS.DataEngine
Imports CMS.Controls
Imports System.IO
Imports System.Collections.Generic

Partial Public Class CMSWebParts_AegeanPower_RepeatCategories
    Inherits CMSAbstractWebPart

    Private list As New Dictionary(Of Integer, List(Of MenuItem))

    Private UniqueNode As Integer = 0

#Region "Document properties"

    Protected mDataSourceControl As CMSDocumentsDataSource = Nothing


    ''' <summary>
    ''' Gets or sets the cache item name
    ''' </summary>
    Public Overrides Property CacheItemName() As String
        Get
            Return MyBase.CacheItemName
        End Get
        Set(ByVal value As String)
            MyBase.CacheItemName = value
            repItems.CacheItemName = value
        End Set
    End Property


    ''' <summary>
    ''' Cache dependencies, each cache dependency on a new line
    ''' </summary>
    Public Overrides Property CacheDependencies() As String
        Get
            Return ValidationHelper.GetString(MyBase.CacheDependencies, repItems.CacheDependencies)
        End Get
        Set(ByVal value As String)
            MyBase.CacheDependencies = value
            repItems.CacheDependencies = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the cache minutes
    ''' </summary>
    Public Overrides Property CacheMinutes() As Integer
        Get
            Return MyBase.CacheMinutes
        End Get
        Set(ByVal value As Integer)
            MyBase.CacheMinutes = value
            repItems.CacheMinutes = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the value that indicates whether permissions should be checked
    ''' </summary>
    Public Property CheckPermissions() As Boolean
        Get
            Return ValidationHelper.GetBoolean(GetValue("CheckPermissions"), repItems.CheckPermissions)
        End Get
        Set(ByVal value As Boolean)
            SetValue("CheckPermissions", value)
            repItems.CheckPermissions = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the class names which should be displayed
    ''' </summary>
    Public Property ClassNames() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("Classnames"), repItems.ClassNames), repItems.ClassNames)
        End Get
        Set(ByVal value As String)
            SetValue("ClassNames", value)
            repItems.ClassNames = value
        End Set
    End Property



    ''' <summary>
    ''' Gets or sets the class names which should be displayed - Repeater SubCategories
    ''' </summary>
    Public Property SubClassNames() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("SubClassNames"), ""), "")
        End Get
        Set(ByVal value As String)
            SetValue("SubClassNames", value)
            'repItems.ClassNames = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the value that indicates whether documents are combined with default culture version
    ''' </summary>
    Public Property CombineWithDefaultCulture() As Boolean
        Get
            Return ValidationHelper.GetBoolean(GetValue("CombineWithDefaultCulture"), repItems.CombineWithDefaultCulture)
        End Get
        Set(ByVal value As Boolean)
            SetValue("CombineWithDefaultCulture", value)
            repItems.CombineWithDefaultCulture = value
        End Set
    End Property



    ''' <summary>
    ''' Gets or sets the value that indicates whether filter out duplicate documents
    ''' </summary>
    Public Property FilterOutDuplicates() As Boolean
        Get
            Return ValidationHelper.GetBoolean(GetValue("FilterOutDuplicates"), repItems.FilterOutDuplicates)
        End Get
        Set(ByVal value As Boolean)
            SetValue("FilterOutDuplicates", value)
            repItems.FilterOutDuplicates = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the culture code of the documents
    ''' </summary>
    Public Property CultureCode() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("CultureCode"), repItems.CultureCode), repItems.CultureCode)
        End Get
        Set(ByVal value As String)
            SetValue("CultureCode", value)
            repItems.CultureCode = value
        End Set
    End Property



    ''' <summary>
    ''' Gets or sets the maximal relative level of the documents to be shown
    ''' </summary>
    Public Property MaxRelativeLevel() As Integer
        Get
            Return ValidationHelper.GetInteger(GetValue("MaxRelativeLevel"), repItems.MaxRelativeLevel)
        End Get
        Set(ByVal value As Integer)
            SetValue("MaxRelativeLevel", value)
            repItems.MaxRelativeLevel = value
        End Set
    End Property



    ''' <summary>
    ''' Gets or sets the order by clause
    ''' </summary>
    Public Property OrderBy() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("OrderBy"), repItems.OrderBy), repItems.OrderBy)
        End Get
        Set(ByVal value As String)
            SetValue("OrderBy", value)
            repItems.OrderBy = value
        End Set
    End Property



    ''' <summary>
    ''' Gets or sets the path of the documents
    ''' </summary>
    Public Property Path() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("Path"), Nothing), Nothing)
        End Get
        Set(ByVal value As String)
            SetValue("Path", value)
            repItems.Path = value
        End Set
    End Property


    '''<summary>
    '''Gets or sets the value that indicates whether only published documents are selected
    '''</summary>
    Public Property SelectOnlyPublished() As Boolean
        Get
            Return ValidationHelper.GetBoolean(GetValue("SelectOnlyPublished"), repItems.SelectOnlyPublished)
        End Get
        Set(ByVal value As Boolean)
            SetValue("SelectOnlyPublished", value)
            repItems.SelectOnlyPublished = value
        End Set
    End Property



    ''' <summary>
    ''' Gets or sets the site name
    ''' </summary>
    Public Property SiteName() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("SiteName"), repItems.SiteName), repItems.SiteName)
        End Get
        Set(ByVal value As String)
            SetValue("SiteName", value)
            repItems.SiteName = value
        End Set
    End Property



    ''' <summary>
    ''' Gets or sets the where condition
    ''' </summary>
    Public Property WhereCondition() As String
        Get
            Return DataHelper.GetNotEmpty(GetValue("WhereCondition"), repItems.WhereCondition)
        End Get
        Set(ByVal value As String)
            SetValue("WhereCondition", value)
            repItems.WhereCondition = value
        End Set
    End Property



    ''' <summary>
    ''' Gets or sets the number which indicates how many documents should be displayed
    ''' </summary>
    Public Property SelectTopN() As Integer
        Get
            Return ValidationHelper.GetInteger(GetValue("SelectTopN"), repItems.SelectTopN)
        End Get
        Set(ByVal value As Integer)
            SetValue("SelectTopN", value)
            repItems.SelectTopN = value
        End Set
    End Property



    ''' <summary>
    ''' Gets or sets the columns to get
    ''' </summary>
    Public Property Columns() As String
        Get
            Return DataHelper.GetNotEmpty(GetValue("Columns"), repItems.Columns)
        End Get
        Set(ByVal value As String)
            SetValue("Columns", value)
            repItems.Columns = value
        End Set
    End Property


#End Region

#Region "Transformation properties"

    ''' <summary>
    ''' Gets or sets the name of the transforamtion which is used for displaying the results
    ''' </summary>
    Public Property TransformationName() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("TransformationName"), repItems.TransformationName), repItems.TransformationName)
        End Get
        Set(ByVal value As String)
            SetValue("TransformationName", value)
            repItems.TransformationName = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the name of the SubTransforamtion which is used for displaying the results - Sub Repeater
    ''' </summary>
    Public Property SubTransformationName() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("SubTransformationName"), ""), "")
        End Get
        Set(ByVal value As String)
            SetValue("SubTransformationName", value)
        End Set
    End Property



    ''' <summary>
    ''' Gets or sets the name of the transforamtion which is used for displaying the results of the selected item
    ''' </summary>
    Public Property SelectedItemTransformationName() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("SelectedItemTransformationName"), repItems.SelectedItemTransformationName), repItems.SelectedItemTransformationName)
        End Get
        Set(ByVal value As String)
            SetValue("SelectedItemTransformationName", value)
            repItems.SelectedItemTransformationName = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the name of the transforamtion which is used for displaying the alternate results
    ''' </summary>
    Public Property AlternatingTransformationName() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("AlternatingTransformationName"), repItems.AlternatingTransformationName), repItems.AlternatingTransformationName)
        End Get
        Set(ByVal value As String)
            SetValue("AlternatingTransformationName", value)
            repItems.AlternatingTransformationName = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the name of the Subtransforamtion which is used for displaying the alternate results
    ''' </summary>
    Public Property SubAlternatingTransformationName() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("SubAlternatingTransformationName"), ""), "")
        End Get
        Set(ByVal value As String)
            SetValue("SubAlternatingTransformationName", value)
        End Set
    End Property

#End Region

#Region "Public properties"

    ''' <summary>
    ''' Gets or sets the nested controls IDs. Use ';' like separator.
    ''' </summary>
    Public Property NestedControlsID() As String
        Get
            Return ValidationHelper.GetString(GetValue("NestedControlsID"), repItems.NestedControlsID)
        End Get
        Set(ByVal value As String)
            SetValue("NestedControlsID", value)
            repItems.NestedControlsID = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the value that indicates whether control should be hidden if no data found
    ''' </summary>
    Public Property HideControlForZeroRows() As Boolean
        Get
            Return ValidationHelper.GetBoolean(GetValue("HideControlForZeroRows"), repItems.HideControlForZeroRows)
        End Get
        Set(ByVal value As Boolean)
            SetValue("HideControlForZeroRows", value)
            repItems.HideControlForZeroRows = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the text which is displayed for zero rows results
    ''' </summary>
    Public Property ZeroRowsText() As String
        Get
            Return DataHelper.GetNotEmpty(ValidationHelper.GetString(GetValue("ZeroRowsText"), repItems.ZeroRowsText), repItems.ZeroRowsText)
        End Get
        Set(ByVal value As String)
            SetValue("ZeroRowsText", value)
            repItems.ZeroRowsText = value
        End Set
    End Property


    ''' <summary>
    ''' Gets or sets the separator (tetx, html code) which is displayed between displayed items 
    ''' </summary>
    Public Property ItemSeparator() As String
        Get
            Return DataHelper.GetNotEmpty(GetValue("ItemSeparator"), repItems.ItemSeparator)
        End Get
        Set(ByVal value As String)
            SetValue("ItemSeparator", value)
            repItems.ItemSeparator = value
        End Set
    End Property


    ''' <summary>
    ''' Filter name.
    ''' </summary>
    Public Property FilterName() As String
        Get
            Return ValidationHelper.GetString(GetValue("FilterName"), repItems.FilterName)
        End Get
        Set(ByVal value As String)
            SetValue("FilterName", value)
            repItems.FilterName = value
        End Set
    End Property


    ''' <summary>
    ''' Data source name.
    ''' </summary>
    Public Property DataSourceName() As String
        Get
            Return ValidationHelper.GetString(GetValue("DataSourceName"), repItems.DataSourceName)
        End Get
        Set(ByVal value As String)
            SetValue("DataSourceName", value)
            repItems.DataSourceName = value
        End Set
    End Property


    ''' <summary>
    ''' Control with data source.
    ''' </summary>
    Public Property DataSourceControl() As CMSDocumentsDataSource
        Get
            Return mDataSourceControl
        End Get
        Set(ByVal value As CMSDocumentsDataSource)
            mDataSourceControl = value
            repItems.DataSourceControl = value
        End Set
    End Property


    ''' <summary>
    ''' Repeater control
    ''' </summary>
    Public ReadOnly Property RepeaterControl() As CMSRepeater
        Get
            Return repItems
        End Get
    End Property

#End Region

#Region "Stop processing"

    ''' <summary>
    ''' Returns true if the control processing should be stopped
    ''' </summary>
    Public Overrides Property StopProcessing() As Boolean
        Get
            Return MyBase.StopProcessing
        End Get
        Set(ByVal value As Boolean)
            MyBase.StopProcessing = value
            repItems.StopProcessing = value
            btnAdd.StopProcessing = value
        End Set
    End Property

#End Region




#Region "Methods"

    ''' <summary>
    ''' Content loaded event handler
    ''' </summary>
    Public Overloads Overrides Sub OnContentLoaded()
        MyBase.OnContentLoaded()
        SetupControl()
    End Sub


    ''' <summary>
    ''' Initializes the control properties
    ''' </summary>
    Protected Sub SetupControl()
        If Me.StopProcessing Then
            ' Do nothing
        Else

            ' Document properties
            repItems.NestedControlsID = NestedControlsID
            repItems.CacheItemName = CacheItemName
            repItems.CacheDependencies = CacheDependencies
            repItems.CacheMinutes = CacheMinutes
            repItems.CheckPermissions = CheckPermissions
            repItems.ClassNames = ClassNames
            repItems.CombineWithDefaultCulture = CombineWithDefaultCulture
            repItems.CultureCode = CultureCode
            repItems.MaxRelativeLevel = MaxRelativeLevel
            repItems.OrderBy = OrderBy
            repItems.SelectTopN = SelectTopN
            repItems.Columns = Columns
            repItems.SelectOnlyPublished = SelectOnlyPublished
            repItems.FilterOutDuplicates = FilterOutDuplicates
            repItems.Path = Path

            ' Transformation properties
            repItems.TransformationName = TransformationName
            repItems.SelectedItemTransformationName = SelectedItemTransformationName
            repItems.AlternatingTransformationName = AlternatingTransformationName

            ' Public properties
            repItems.HideControlForZeroRows = HideControlForZeroRows
            repItems.ZeroRowsText = ZeroRowsText
            repItems.ItemSeparator = ItemSeparator
            repItems.FilterName = FilterName

        End If
    End Sub

    Protected Sub repItems_ItemCreated(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles repItems.ItemCreated
        Dim dr As DataRow = TryCast(e.Item.DataItem, DataRow)

        If dr Is Nothing Then
            Dim dv As DataRowView = TryCast(e.Item.DataItem, DataRowView)
            If dv IsNot Nothing Then
                dr = dv.Row
            End If
        End If

        'Response.Write(dr("DocumentNamePath") & "/%")

        'Response.Write(e.Item.Controls.Count)

        'TextHelper.LimitLength("sdf", 280, "...")

        'TextHelper.LimitLength(HttpUtility.HtmlDecode(HTMLHelper.StripTags(DataHelper.GetNotEmpty(Eval("TextNews"), ""), False, " ")), 280, "...")


        For index As Integer = 0 To e.Item.Controls.Count - 1
            'Response.Write("@" & e.Item.Controls(index).Controls(1).GetType().ToString() & "#")
            If e.Item.Controls(index).Controls(1).GetType().ToString() = "CMS.Controls.CMSRepeater" Then
                'Response.Write("3")
                Dim repSub As CMSRepeater = DirectCast(e.Item.Controls(index).Controls(1), CMSRepeater)
                'Response.Write("@" & repSub.ID.ToString() & "#")
                repSub.Path = dr("NodeAliasPath") & "/%"
                repSub.ClassNames = SubClassNames '"cms.faq"
                repSub.TransformationName = SubTransformationName '"AegeanPower.Transformation.QuestionAnswerDisplayHide"
            End If
        Next


    End Sub


    ''' <summary>
    ''' Reload data
    ''' </summary>
    Public Overloads Overrides Sub ReloadData()
        MyBase.ReloadData()
        SetupControl()
        repItems.ReloadData(True)
    End Sub


    ''' <summary>
    ''' Clears cache.
    ''' </summary>
    Public Overloads Overrides Sub ClearCache()
        repItems.ClearCache()
    End Sub


#End Region
    Private Class MenuItem
        Public itemID As Integer

        Public Label As String

        Public Sub New(ByVal itemid As Integer, ByVal label As String)
            Me.itemID = itemid
            Me.Label = label
        End Sub
    End Class
End Class

