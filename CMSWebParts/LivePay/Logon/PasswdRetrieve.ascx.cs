using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.ExtendedControls;
using CMS.DataEngine;
using CMS.EmailEngine;
using CMS.SiteProvider;
using CMS.EventLog;
using CMS.URLRewritingEngine;
using CMS.MembershipProvider;
using CMS.PortalEngine;

public partial class CMSWebParts_LivePay_Logon_PasswdRetrieve : CMSAbstractWebPart
{
    #region "Private properties"

    //private string mDefaultTargetUrl = "";

    #endregion
    
    #region "Public properties"

    /// <summary>
    /// Gets or sets the sender e-mail (from)
    /// </summary>
    public string SendEmailFrom
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("SendEmailFrom"), SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSSendPasswordEmailsFrom"));
        }
        set
        {
            this.SetValue("SendEmailFrom", value);
        }
    }


    ///// <summary>
    ///// Gets or sets the default target url (rediredction when the user is logged in)
    ///// </summary>
    //public string DefaultTargetUrl
    //{
    //    get
    //    {
    //        return ValidationHelper.GetString(this.GetValue("DefaultTargetUrl"), mDefaultTargetUrl);
    //    }
    //    set
    //    {
    //        this.SetValue("DefaultTargetUrl", value);
    //        this.mDefaultTargetUrl = value;
    //    }
    //}

    ///// <summary>
    ///// Label HeaderTitle
    ///// </summary>
    //public string HeaderTitle
    //{
    //    get
    //    {
    //        return DataHelper.GetNotEmpty(this.GetValue("HeaderTitle"), "");
    //    }
    //    set
    //    {
    //        this.SetValue("HeaderTitle", value);
    //    }
    //}

    /// <summary>
    /// PasswdRetrieval Text
    /// </summary>
    public string PasswdRetrievalText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("PasswdRetrievalText"), "");
        }
        set
        {
            this.SetValue("PasswdRetrievalText", value);
        }
    }

    /// <summary>
    /// PasswdRetrieval Button
    /// </summary>
    public string PasswdRetrievalButton
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("PasswdRetrievalButton"), "");
        }
        set
        {
            this.SetValue("PasswdRetrievalButton", value);
        }
    }
    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            //this.rqValue.Visible = false;
            // Do not process
        }
        else
        {
            btnPasswdRetrieval.CssClass = ResHelper.LocalizeString("{$=button|en-us=buttonen$}");
            btnPasswdRetrieval.Style.Add("background-image", ResHelper.LocalizeString("{$=url('/App_Themes/LivePay/BtnNext.png')|en-us=url('/App_Themes/LivePay/BtnNext_en-us.png')$}"));
            btnPasswdRetrieval.Style.Add("width", "105px");
            if (!CMSContext.CurrentUser.IsAuthenticated())
            {
                lblPasswdRetrieval.Text = PasswdRetrievalText;
                lblPasswdRetrieval.Text ="<font color='black'>" + ResHelper.LocalizeString("{$=username|en-us=Enter your username$}") + "</font>";
                Panel1.Visible = true;
               // rqValue.ErrorMessage = ResHelper.GetString("LogonForm.rqValue");
                btnPasswdRetrieval.Text = PasswdRetrievalButton;

                litTitle.Text = ResHelper.LocalizeString("{$=�������� ��� ������ ���;|en-us=Password Retrieve$}");
                litText.Text = ResHelper.LocalizeString("{$=�������� �� ���������� �������� ���  ������ ��� ���������� ���� �� ��������� ��� e-mail ��� ��� ��� ������<br>���� 1 ��� 2: �������� �� username ��� ��� ���� ���������� ��� ������� ���� ������ ��� ������� ���������.|en-us=Please fill in your username and a new password will be sent to your email account$}");
                lnkRefresh.Text = "<img src='/App_Themes/LivePay/UserRegistration/refresh.png' alt='Close' border='0' />";
                btnPasswdRetrieval.Click += new EventHandler(btnPasswdRetrieval_Click);
                CMSBtnSendAns.Click += new EventHandler(btnSendAns_Click);
                //rqValue.ErrorMessage = ResHelper.LocalizeString("{$=�������� �� UserName ���|en-us=Enter your username$}");
               // rqCap.ErrorMessage = "<br /><br /><br />" + ResHelper.LocalizeString("{$=�������� ��� ������ ���������|en-us=Enter security code$}"); 
                UserErrorMessage.Text = ResHelper.LocalizeString("{$=�������� �� UserName ���|en-us=Enter your username$}");
                CaptchErrorMessage.Text = ResHelper.LocalizeString("{$=�������� ��� ������ ���������|en-us=Enter security code$}"); 
                if (!RequestHelper.IsPostBack())
                {
                    //Login1.UserName = ValidationHelper.GetString(Request.QueryString["username"], "");
                    // Set SkinID properties
                    //if (!this.StandAlone && (this.PageCycle < PageCycleEnum.Initialized) && (ValidationHelper.GetString(this.Page.StyleSheetTheme, "") == ""))
                    //{
                        //SetSkinID(this.SkinID);
                    //}
                }
            }
            else
            {
                // Set HeaderTitleLogout text
                //lblHeaderTitleLogout.Text = HeaderTitleLogout;

                Panel1.Visible = false;
                //Panel_Logout.Visible = true;
                //lblFullName.Text = CMSContext.CurrentUser.FullName;
            }
        }
    }

    ///// <summary>
    ///// OnLoad override (show hide password retrieval)
    ///// </summary>
    //protected override void OnLoad(EventArgs e)
    //{
    //    base.OnLoad(e);

    //    if (hdnPasswDisplayed.Value == "")
    //    {
    //        this.pnlPasswdRetrieval.Attributes.Add("style", "display:none;");
    //    }
    //    else
    //    {
    //        this.pnlPasswdRetrieval.Attributes.Add("style", "display:block;");
    //    }
    //}

    /// <summary>
    /// Retrieve the user password
    /// </summary>
    /// 
    

   void btnSendAns_Click(object sender, EventArgs e)
   {
       string value = StripHTMLFunctions.StripTags(txtPasswordRetrieval.Text.Trim());
       if (UserInfoProvider.GetUserInfo(value) != null)
           {
               UserInfo usr = UserInfoProvider.GetUserInfo(value);
               QuestDiv.Style.Add("display", "");
               txtPasswordRetrieval.Style.Add("display", "none");
               lblPasswdRetrieval.Style.Add("display", "none");
               int QuestID = Convert.ToInt32((usr.GetValue("UserQuestions").ToString()));

               DataTable dt = DBConnection.GetSecQuestion(QuestID);
               if (dt.Rows.Count > 0)
               {
                   quest.InnerHtml = ResHelper.LocalizeString("{$=" + dt.Rows[0]["QuestGR"].ToString() + "|en-us=" + dt.Rows[0]["QuestEN"].ToString() + "$}");
               }
               lblResult.Visible = false;
               btnPasswdRetrieval.Visible = false;
               EmailDiv.Visible = false;


               string Answertxt = txtAnswer.Text.ToLower();
               if (Answertxt == usr.GetValue("UserAnswers").ToString().ToLower())
               {
                   SendPasswordEmail(usr);

                  // lblResult.Text = UserInfoProvider.ForgottenEmailRequest( value,  CMSContext.CurrentSiteName, "LOGONFORM", "info@livepay.gr",  CMSContext.CurrentResolver);
                   litTitle.Text = ResHelper.LocalizeString("{$=�������� ��� ������ ���;|en-us=Password Retrieve$}");
                   //litText.Text = ResHelper.LocalizeString("{$=���� 2 ��� 2: ��������� ����� ���� ������� ��������� ��� ������ ����������� ���� ��� ������� ��� ��� LivePay ��� ������� ��������޻|en-us=Please provide with the correct answer to the security question$}");
                   litText.Visible = false;
                   lblResult.Text = ResHelper.LocalizeString("{$=� ���� ��� ������� ���� ������ ���� ���������� ���|en-us=Your new password was sent to your e-mail address$}"); 
                   //� ���� ��� ������� ���� ������ ���� ���������� ���
                   lblResult.Style.Add("color", "green");
                   lblResult.Visible = true;
                   QuestDiv.Style.Add("display", "none");
                   QuestAnsDescr.Visible = false;
               }
               else
               {
                   QuestAnsDescr.Visible = true;
                   lblEroorAns.Text = "<font color='red'>" + ResHelper.LocalizeString("{$=� �������� ��� ��� ����� �����|en-us=Your answer was not correct$}") + "</font>";
                   lblEroorAns.Visible = true;
               }

           }

       }
   void SendPasswordEmail(UserInfo user)
   {

      

       string password = ValidationHelper.GetString(user.GetValue("UserPassword"), "");

       switch (user.PasswordFormat.ToLower())
       {
           // SHA1 hash of the password, generate new
           case "sha1":
               password = RandomPassword.Generate(7, 16); //UserInfoProvider.GenerateNewPassword();
               break;
       }
       try
       {

           UserInfoProvider.SetPassword(user.UserName, password);

           // Get UserInfo of the specified name
           UserInfo ui = CMS.SiteProvider.UserInfoProvider.GetUserInfo(user.UserName);
           ui.SetValue("UserLastPasswordModified", null);
           // Update the UserInfo
           CMS.SiteProvider.UserInfoProvider.SetUserInfo(ui);

           EventLogProvider ev = new EventLogProvider();
           MailContext RegMail = new MailContext();
           //RegMail.Username = user.UserName;
           RegMail.CardEmail = user.UserName;
           RegMail.Password = password;
           RegMail.SendEmail("USRNEWPASS" + ResHelper.LocalizeString("{$=|en-us=_en$}"), "info@livepay.gr", user.Email);
       }
       catch (Exception e)
       {
           Response.Write(e);
       }
       
   }





    void btnPasswdRetrieval_Click(object sender, EventArgs e)
    {
        //FIRST CHECK CAPTCHA
        scCaptcha.ValidateCaptcha(txtCaptcha.Text.ToUpper());
        if (!scCaptcha.UserValidated)
        {
            // Display error message if catcha text is not valid
            //lblErrorDIV.Visible = true;
            //lblError.Visible = true;
            //lblError.Text = String.Concat("<span style=\"line-height:18px\">",ResHelper.GetString("Webparts_Membership_RegistrationForm.captchaError"),"</span>");
            string err = String.Concat("<span style=\"line-height:18px\">",ResHelper.GetString("Webparts_Membership_RegistrationForm.captchaError"),"</span>"); 
            //"ShowTheCloud('#CaptchaMsg', '<%=CaptchErrorMessage.Text %>', true)"
            litScript.Text = "<script>ShowTheCloud('#CaptchaMsg', '" + err + "', true);</script>";
            txtCaptcha.Text = string.Empty;

            return;
        }
        else
        {
            // Generate new code and clear captcha textbox if cpatcha code is valid
            lblErrorDIV.Visible = false;
            lblError.Visible = false;
            txtCaptcha.Text = string.Empty;
            //scCaptcha.GenerateNewCode();
        }


        string value = txtPasswordRetrieval.Text.Trim();
        
        if (value != String.Empty)
        {
            //---- GetUserByEmail
            if (UserInfoProvider.GetUserInfo(value) != null)
            {
                QuestAnsDescr.Visible = true;
                UserInfo usr = UserInfoProvider.GetUserInfo(value);
                QuestDiv.Style.Add("display", "");
                txtPasswordRetrieval.Style.Add("display", "none");
                lblPasswdRetrieval.Style.Add("display", "none");

                divPasswdRetrievalHeader.Visible = false;

                string Question = DataHelper.GetNotEmpty(usr.GetValue("UserQuestions"), string.Empty);

                if (Question == string.Empty)
                {
                    pnlPasswdRetrieval.Visible = false;
                    pnlNoUserQuestions.Visible = true;
                    litErrorMsg.Text = ResHelper.LocalizeString("{$=� �������� Password Reminder ��� ����� ������ ��� �� ���������� ���!<br/>" +
                            "��� ��� �������� ���� ������� ���������, �������� ������������� �� �� EuroPhone Banking ��� 210.95.55.019 ." +
                            "|en-us=Password reminder is not available for your account!<br/>" +
                            "To obtain a new password, please contact EuroPhone Banking at 210.95.55.019.$}");
                }
                else
                {
                    int QuestID = Convert.ToInt32((Question));

                    DataTable dt = DBConnection.GetSecQuestion(QuestID);
                    if (dt.Rows.Count > 0)
                    {
                        quest.InnerHtml = ResHelper.LocalizeString("{$=" + dt.Rows[0]["QuestGR"].ToString() + "|en-us=" + dt.Rows[0]["QuestEN"].ToString() + "$}");
                    }
                }
                lblResult.Visible = false;
                btnPasswdRetrieval.Visible = false;
                EmailDiv.Visible = false;

                litTitle.Text = ResHelper.LocalizeString("{$=�������� ��� ������ ���;|en-us=Password Retrieve$}");
                litText.Text = ResHelper.LocalizeString("{$=���� 2 ��� 2: ��������� ����� ���� ������� ��������� ��� ������ ����������� ���� ��� ������� ��� ��� LivePay ��� ������� ��������޻|en-us=Please provide with the correct answer to the security question$}");
            }
            else
            {
                QuestDiv.Style.Add("display", "none");
                lblResult.Visible = true;
                lblResult.Text = "<font color='red'>" + ResHelper.LocalizeString("{$=��� ������� �������������  ������� �� ����  �� email|en-us=No registered user found with this e-mail address$}") + "</font>"; 
            }
            

            

            //this.pnlPasswdRetrieval.Attributes.Add("style", "display:block;");            
        }
    }
}
