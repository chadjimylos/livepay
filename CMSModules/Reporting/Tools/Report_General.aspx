<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Report_General.aspx.cs" Inherits="CMSModules_Reporting_Tools_Report_General"
    Theme="Default" ValidateRequest="false" EnableEventValidation="false" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" %>

<%@ Register Src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" TagName="PageTitle"
    TagPrefix="cms" %>
<%@ Register Src="ItemsList.ascx" TagName="ItemsList" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/MetaFiles/FileList.ascx" TagName="FileList"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Reporting/FormControls/SelectReportCategory.ascx"
    TagName="SelectReportCategory" TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">

    <script type="text/javascript">
    //<![CDATA[
        // Insert desired HTML at the current cursor position of the HTML editor
        function InsertHTML(htmlString) {
            // Get the editor instance that we want to interact with.
            var oEditor = FCKeditorAPI.GetInstance(reporting_htmlTemplateBody);
            // Check the active editing mode.
            if (oEditor.EditMode == FCK_EDITMODE_WYSIWYG) {
                // Insert the desired HTML.
                oEditor.InsertHtml(htmlString);
            }
            else
                alert('You must be in WYSIWYG mode!');
            return false;
        }

        function PasteImage(imageurl) {
            imageurl = '<img src="' + imageurl + '" />';
            return InsertHTML(imageurl);
        }
    //]]>
    </script>

    <asp:Panel runat="server" ID="pnlMenu" CssClass="TabsEditMenu">
        <table>
            <tr>
                <td>
                    <%--OnClientClick="return CheckContent();--%>
                    <asp:LinkButton ID="lnkSave" runat="server" CssClass="MenuItemEdit" OnClick="lnkSave_Click"
                        EnableViewState="false">
                        <asp:Image ID="imgSave" runat="server" EnableViewState="false" />
                        <%=mSave%>
                    </asp:LinkButton>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <table style="width: 100%;">
            <tr>
                <td>
                    <table style="vertical-align: top;">
                        <tr>
                            <td class="FieldLabel">
                                <asp:Label runat="server" ID="lblReportDisplayName" EnableViewState="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtReportDisplayName" runat="server" CssClass="TextBoxField" MaxLength="440"
                                    EnableViewState="false" />
                                <asp:RequiredFieldValidator ID="rfvReportDisplayName" runat="server" ErrorMessage=""
                                    ControlToValidate="txtReportDisplayName" Display="Dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <asp:Label runat="server" ID="lblReportName" EnableViewState="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtReportName" runat="server" CssClass="TextBoxField" MaxLength="100"
                                    EnableViewState="false" />
                                <asp:RequiredFieldValidator ID="rfvReportName" runat="server" ErrorMessage="" ControlToValidate="txtReportName"
                                    Display="Dynamic" EnableViewState="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <asp:Label runat="server" ID="lblReportCategory" EnableViewState="false" />
                            </td>
                            <td>
                                <cms:SelectReportCategory ID="selectCategory" runat="server" IsLiveSite="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <asp:Label runat="server" ID="lblReportAccess" EnableViewState="false" />
                            </td>
                            <td>
                                <asp:CheckBox CssClass="CheckBoxMovedLeft" ID="chkReportAccess" runat="server" Checked="True" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="width: 100%;">
            <tr style="height: 1em;">
            </tr>
            <tr>
                <td style="padding-left: 5px; width: 100%;">
                    <asp:Label runat="server" ID="lblLayout" EnableViewState="false" />
                    <cms:CMSHtmlEditor ID="htmlTemplateBody" runat="server" Width="100%" Height="435px"
                        ToolbarSet="Reporting" />
                </td>
                <td>
                    <asp:Label runat="server" ID="lblGraphs" EnableViewState="false" />
                    <cms:ItemsList ID="ilGraphs" runat="server" />
                    <br />
                    <asp:Label runat="server" ID="lblTables" EnableViewState="false" />
                    <cms:ItemsList ID="ilTables" runat="server" />
                    <br />
                    <asp:Label runat="server" ID="lblValues" EnableViewState="false" />
                    <cms:ItemsList ID="ilValues" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <cms:PageTitle ID="AttachmentTitle" runat="server" TitleText="Attachments" TitleCssClass="SubTitleHeader" />
        <br />
        <cms:FileList ID="attachmentList" runat="server" />
    </asp:Panel>
    <cms:CMSButton runat="server" ID="btnHdnReload" EnableViewState="false" CssClass="HiddenButton" />
</asp:Content>
