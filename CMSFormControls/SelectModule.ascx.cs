using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.FormControls;
using CMS.TreeEngine;

public partial class CMSFormControls_SelectModule : FormEngineUserControl
{
    #region "Variables"

    private int mSiteID = 0;
    private bool mDisplayAllModules = false;
    private bool mDisplayOnlyWithPermission = false;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the enabled state of the control.
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.Enabled = value;
            }
        }
    }


    /// <summary>
    /// Returns ClientID of the DLL with module.
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect.ClientID;
        }
    }


    /// <summary>
    /// Gets or sets the field value.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.uniSelector.Value;
        }
        set
        {
            if (uniSelector == null)
            {
                this.pnlUpdate.LoadContainer();
            }
            this.uniSelector.Value = value;
        }
    }


    /// <summary>
    /// Gets or sets the ID of the site for which the modules should be returned. 0 means current site.
    /// </summary>
    public int SiteID
    {
        get
        {
            return this.mSiteID;
        }
        set
        {
            this.mSiteID = value;
            if (this.uniSelector != null)
            {
                this.uniSelector.WhereCondition = GetWhereCondition();
            }
        }
    }


    /// <summary>
    /// Gets the inner UniSelector control.
    /// </summary>
    public UniSelector UniSelector
    {
        get
        {
            return this.uniSelector;
        }
    }


    /// <summary>
    /// Gets the inner DDL control.
    /// </summary>
    public DropDownList DropDownSingleSelect
    {
        get
        {
            return this.uniSelector.DropDownSingleSelect;
        }
    }


    /// <summary>
    /// If true, displays only modules which have some permissions to be displayed in permission matrix.
    /// </summary>
    public bool DisplayOnlyWithPermission
    {
        get
        {
            return this.mDisplayOnlyWithPermission;
        }
        set
        {
            this.mDisplayOnlyWithPermission = value;
        }
    }


    /// <summary>
    /// If true, displays all modules. If false, displayes only modules with some UI Elements.
    /// </summary>
    public bool DisplayAllModules
    {
        get
        {
            return this.mDisplayAllModules;
        }
        set
        {
            this.mDisplayAllModules = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            this.uniSelector.StopProcessing = true;
        }
        else
        {
            ReloadData(false);
        }
    }


    /// <summary>
    /// Reloads the data in the selector.
    /// </summary>
    /// <param name="forcedReload">Indicates whether the UniSelector should be reloaded</param>
    public void ReloadData(bool forcedReload)
    {
        this.uniSelector.IsLiveSite = this.IsLiveSite;
        this.uniSelector.WhereCondition = GetWhereCondition();
        this.uniSelector.OrderBy = "ResourceDisplayName";

        if (forcedReload)
        {
            this.uniSelector.Reload(false);
        }
    }


    /// <summary>
    /// Returns proper where condition.
    /// </summary>
    private string GetWhereCondition()
    {
        string where = "";

        if (!this.DisplayAllModules)
        {
            if (CMSContext.CurrentUser.IsGlobalAdministrator)
            {
                where = "(ResourceID IN (SELECT ElementResourceID FROM CMS_UIElement WHERE ElementParentID IS NULL AND ElementChildCount > 0)) AND NOT ResourceName = 'CMS.WYSIWYGEditor'";
            }
            else
            {
                where = "(ResourceID IN (SELECT ResourceID FROM CMS_ResourceSite WHERE SiteID = " + (this.SiteID == 0 ? CMSContext.CurrentSiteID : this.SiteID) + ")) AND (ResourceID IN (SELECT ElementResourceID FROM CMS_UIElement WHERE ElementParentID IS NULL AND ElementChildCount > 0)) AND NOT ResourceName = 'CMS.WYSIWYGEditor'";
            }
        }

        if (this.DisplayOnlyWithPermission)
        {
            where = "EXISTS (SELECT PermissionID FROM CMS_Permission WHERE CMS_Permission.ResourceID = CMS_Resource.ResourceID AND PermissionDisplayInMatrix = 1)";
        }

        return where;
    }

}
