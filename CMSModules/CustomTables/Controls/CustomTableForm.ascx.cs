using System;

using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.EventLog;

public partial class CMSModules_CustomTables_Controls_CustomTableForm : CMSUserControl
{
    #region "Private variables"

    private string editItemPage = "~/CMSModules/CustomTables/Tools/CustomTable_Data_EditItem.aspx";
    private DataClassInfo dci = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Custom table class id
    /// </summary>
    public int CustomTableId
    {
        get
        {
            return customTableForm.CustomTableId;
        }
        set
        {
            customTableForm.CustomTableId = value;
        }
    }


    /// <summary>
    /// Custom table item id
    /// </summary>
    public int ItemId
    {
        get
        {
            return customTableForm.ItemID;
        }
        set
        {
            customTableForm.ItemID = value;
        }
    }

    #endregion


    /// <summary>
    /// Page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (CustomTableId > 0)
        {
            // Get form info
            dci = DataClassInfoProvider.GetDataClass(CustomTableId);
            
            if (dci != null)
            {
                customTableForm.ShowPrivateFields = true;

                if (dci.ClassEditingPageURL != String.Empty)
                {
                    editItemPage = dci.ClassEditingPageURL;
                }

                customTableForm.OnAfterSave += customTableForm_OnAfterSave;
                customTableForm.OnBeforeSave += customTableForm_OnBeforeSave;
            }
        }
        // Show message about successful save
        if (QueryHelper.GetString("saved", String.Empty) != String.Empty)
        {
            lblInfo.Text = ResHelper.GetString("general.changessaved");
            lblInfo.Visible = true;
        }
    }


    private void customTableForm_OnBeforeSave()
    {
        // If editing item
        if (ItemId > 0)
        {
            // Check 'Modify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.customtables", "Modify") &&
                !CMSContext.CurrentUser.IsAuthorizedPerClassName(dci.ClassName, "Modify"))
            {
                // Show error message
                lblError.Visible = true;
                lblError.Text = String.Format(ResHelper.GetString("customtable.permissiondenied.modify"), dci.ClassName);
                // Hide 'successful' message
                lblInfo.Visible = false;
                customTableForm.StopProcessing = true;
            }
        }
        else
        {
            // Check 'Create' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.customtables", "Create") &&
                !CMSContext.CurrentUser.IsAuthorizedPerClassName(dci.ClassName, "Create"))
            {
                // Show error message
                lblError.Visible = true;
                lblError.Text = String.Format(ResHelper.GetString("customtable.permissiondenied.create"), dci.ClassName);
                // Hide 'successful' message
                lblInfo.Visible = false;
                customTableForm.StopProcessing = true;
            }
        }
    }


    private void customTableForm_OnAfterSave()
    {
        string param = "&saved=1";

        // Reflect new in query string
        param += (QueryHelper.GetString("new", String.Empty) != String.Empty) ? "&new=1" : String.Empty;

        // Log the event
        EventLogProvider eventLog = new EventLogProvider();
        //eventLog.LogEvent("I", DateTime.Now, "User", "Edit Custom Table", CMSContext.CurrentUser.UserID, CMSContext.CurrentUser.UserName, CMSContext.CurrentDocument.NodeID, "DocumentName", Request.UserHostAddress.ToString(),  "Edit Custom Table " + dci.ClassDisplayName, CMSContext.CurrentSite.SiteID, "");
        eventLog.LogEvent("I", DateTime.Now, "CustomTable", (QueryHelper.GetString("new", String.Empty) != String.Empty) ? "New Custom Table" : "Update Custom Table", Request.Path, "Custom Table " + dci.ClassDisplayName);





        //Redirect to edit page with saved parameter and new itemId (new item)
        UrlHelper.Redirect(UrlHelper.GetAbsoluteUrl(editItemPage) + "?customtableid=" + CustomTableId + "&itemid=" + customTableForm.ItemID + param);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (customTableForm.BasicForm != null)
        {
            customTableForm.BasicForm.SubmitButton.CssClass = "SubmitButton";
        }
    }
}
