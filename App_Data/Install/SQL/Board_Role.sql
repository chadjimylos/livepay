CREATE TABLE [Board_Role] (
		[BoardID]     int NOT NULL,
		[RoleID]      int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Board_Role]
	ADD
	CONSTRAINT [PK_Board_Role]
	PRIMARY KEY
	([BoardID], [RoleID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Board_Role]') IS NOT NULL
	AND OBJECT_ID(N'[Board_Board]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Board_Role_BoardID_Board_Board]') IS NULL
BEGIN
		ALTER TABLE [Board_Role]
			ADD CONSTRAINT [FK_Board_Role_BoardID_Board_Board]
			FOREIGN KEY ([BoardID]) REFERENCES [Board_Board] ([BoardID])
END
IF OBJECT_ID(N'[Board_Role]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Board_Role_RoleID_CMS_Role]') IS NULL
BEGIN
		ALTER TABLE [Board_Role]
			ADD CONSTRAINT [FK_Board_Role_RoleID_CMS_Role]
			FOREIGN KEY ([RoleID]) REFERENCES [CMS_Role] ([RoleID])
END
