using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.UIControls;
using CMS.GlobalHelper;


/// <summary>
/// Action event handler
/// </summary>
/// <param name="actionArgument">Action parameter</param>
public delegate void OnEditDeleteActionEventHandler(object actionArgument);


public partial class CMSModules_AdminControls_Controls_Class_AlternativeFormList : CMSUserControl
{
    private int mFormClassID = 0;


    #region "Public properties"

    /// <summary>
    /// Gets or sets class id
    /// </summary>
    public int FormClassID
    {
        get
        {
            return mFormClassID;
        }
        set
        {
            mFormClassID = value;
        }
    }


    /// <summary>
    /// On edit event
    /// </summary>
    public event OnEditDeleteActionEventHandler OnEdit;


    /// <summary>
    /// On delete event
    /// </summary>
    public event OnEditDeleteActionEventHandler OnDelete;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        gridElem.GridName = "~/CMSModules/AdminControls/Controls/Class/AlternativeFormList.xml";
        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.WhereCondition = " FormClassID=" + FormClassID.ToString();
        gridElem.IsLiveSite = this.IsLiveSite;
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Grid action handling
    /// </summary>
    void gridElem_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "edit":
                if (OnEdit != null)
                {
                    OnEdit(actionArgument);
                }
                break;

            case "delete":
                if (OnDelete != null)
                {
                    OnDelete(actionArgument);
                }
                break;
        }
    }
}
