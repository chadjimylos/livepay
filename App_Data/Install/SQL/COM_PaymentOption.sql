CREATE TABLE [COM_PaymentOption] (
		[PaymentOptionID]                         int NOT NULL IDENTITY(1, 1),
		[PaymentOptionName]                       nvarchar(200) NOT NULL,
		[PaymentOptionDisplayName]                nvarchar(200) NOT NULL,
		[PaymentOptionEnabled]                    bit NOT NULL,
		[PaymentOptionSiteID]                     int NULL,
		[PaymentOptionPaymentGateUrl]             nvarchar(500) NULL,
		[PaymentOptionAssemblyName]               nvarchar(200) NULL,
		[PaymentOptionClassName]                  nvarchar(200) NULL,
		[PaymentOptionSucceededOrderStatusID]     int NULL,
		[PaymentOptionFailedOrderStatusID]        int NULL,
		[PaymentOptionGUID]                       uniqueidentifier NOT NULL,
		[PaymentOptionLastModified]               datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_PaymentOption]
	ADD
	CONSTRAINT [PK_COM_PaymentOption]
	PRIMARY KEY
	NONCLUSTERED
	([PaymentOptionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_PaymentOption]') IS NOT NULL
	AND OBJECT_ID(N'[COM_OrderStatus]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_PaymentOption_PaymentOptionFailedOrderStatusID_COM_OrderStatus]') IS NULL
BEGIN
		ALTER TABLE [COM_PaymentOption]
			ADD CONSTRAINT [FK_COM_PaymentOption_PaymentOptionFailedOrderStatusID_COM_OrderStatus]
			FOREIGN KEY ([PaymentOptionFailedOrderStatusID]) REFERENCES [COM_OrderStatus] ([StatusID])
END
IF OBJECT_ID(N'[COM_PaymentOption]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_PaymentOption_PaymentOptionSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [COM_PaymentOption]
			ADD CONSTRAINT [FK_COM_PaymentOption_PaymentOptionSiteID_CMS_Site]
			FOREIGN KEY ([PaymentOptionSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[COM_PaymentOption]') IS NOT NULL
	AND OBJECT_ID(N'[COM_OrderStatus]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_PaymentOption_PaymentOptionSucceededOrderStatusID_COM_OrderStatus]') IS NULL
BEGIN
		ALTER TABLE [COM_PaymentOption]
			ADD CONSTRAINT [FK_COM_PaymentOption_PaymentOptionSucceededOrderStatusID_COM_OrderStatus]
			FOREIGN KEY ([PaymentOptionSucceededOrderStatusID]) REFERENCES [COM_OrderStatus] ([StatusID])
END
CREATE INDEX [IX_COM_PaymentOption_PaymentOptionFailedOrderStatusID]
	ON [COM_PaymentOption] ([PaymentOptionFailedOrderStatusID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_PaymentOption_PaymentOptionSiteID_PaymentOptionDisplayName_PaymentOptionEnabled]
	ON [COM_PaymentOption] ([PaymentOptionSiteID], [PaymentOptionDisplayName], [PaymentOptionEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_PaymentOption_PaymentOptionSucceededOrderStatusID]
	ON [COM_PaymentOption] ([PaymentOptionSucceededOrderStatusID])
	ON [PRIMARY]
