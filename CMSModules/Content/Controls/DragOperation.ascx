﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DragOperation.ascx.cs"
    Inherits="CMSModules_Content_Controls_DragOperation" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>
<%@ Register Src="~/CMSAdminControls/AsyncControl.ascx" TagName="AsyncControl" TagPrefix="cms" %>
<asp:Panel runat="server" ID="pnlLog" Visible="false">
    <div class="AsyncLogBackground">
        &nbsp;</div>
    <div class="AsyncLogArea">
        <div style="width: 96%;">
            <asp:Panel ID="pnlAsyncBody" runat="server" CssClass="PageBody">
                <asp:Panel ID="pnlTitleAsync" runat="server" CssClass="PageHeader">
                    <cms:PageTitle ID="titleElemAsync" runat="server" />
                </asp:Panel>
                <asp:Panel ID="pnlCancel" runat="server" CssClass="PageHeaderLine">
                    <cms:LocalizedButton runat="server" ID="btnCancel" ResourceString="General.Cancel" CssClass="SubmitButton" />
                </asp:Panel>
                <asp:Panel ID="pnlAsyncContent" runat="server" CssClass="PageContent">
                    <cms:AsyncControl ID="ctlAsync" runat="server" />
                </asp:Panel>
            </asp:Panel>
        </div>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlContent" CssClass="PageContent">
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Panel ID="pnlAction" runat="server" EnableViewState="false">
        <cms:LocalizedLabel ID="lblQuestion" runat="server" CssClass="ContentLabel" EnableViewState="false"
            Font-Bold="true" />
        <br />
        <br />
        <cms:LocalizedLabel ID="lblTarget" runat="server" CssClass="ContentLabel" EnableViewState="false" />
        <br />
        <asp:PlaceHolder ID="plcCopyCheck" runat="server" EnableViewState="false" Visible="false">
            <br />
            <div>
                <cms:LocalizedCheckBox ID="chkCopyChild" runat="server" CssClass="ContentCheckbox"
                    EnableViewState="false" ResourceString="contentrequest.copyunderlying" Checked="true" />
            </div>
        </asp:PlaceHolder>
        <asp:Panel ID="pnlDeleteSKU" runat="server" EnableViewState="false">
            <asp:CheckBox ID="chkDeleteSKU" runat="server" CssClass="ContentCheckBox" Visible="false" />
        </asp:Panel>
        <br />
        <cms:LocalizedButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click"
            ResourceString="general.yes" EnableViewState="false" /><cms:LocalizedButton ID="btnNo"
                runat="server" CssClass="SubmitButton" ResourceString="general.no" EnableViewState="false" />
    </asp:Panel>
</asp:Panel>

<script type="text/javascript">
    //<![CDATA[
    // Refresh action
    function RefreshTree(nodeId, selectNodeId) {
        parent.frames['contenttree'].RefreshTree(nodeId, selectNodeId);
    }

    // Selects the node within the tree
    function SelectNode(nodeId) {
        parent.frames['contenttree'].SelectNode(nodeId, null);
    }

    // Display the document
    function DisplayDocument(nodeId) {
        parent.frames['contentmenu'].SelectNode(nodeId);
    }

    // Cancel the operation
    function CancelDragOperation() {
        DisplayDocument();
    }
    //]]>                        
</script>

<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />

<script src="../../../CMSScripts/cms.js" type="text/javascript"></script>

