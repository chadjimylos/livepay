﻿Imports CMS.EmailEngine
Imports System.Data
Imports CMS.DataEngine
Imports CMS.SiteProvider
Imports CMS.SettingsProvider
''' <summary>
''' Use the Properties to define the variables used in the templates
''' </summary>
''' <remarks>Use the Properties to define the variables used in the templates</remarks>
Public Class MailContext

#Region "PROPERTIES"

    'Private _Host As String
    'Public Property Host() As String
    '    Get
    '        Return _Host
    '    End Get
    '    Set(ByVal value As String)
    '        _Host = Value
    '    End Set
    'End Property

    'Private _Port As String
    'Public Property Port() As String
    '    Get
    '        Return _Port
    '    End Get
    '    Set(ByVal value As String)
    '        _Port = Value
    '    End Set
    'End Property

    Private _accEmail As String
    Public Property AccEmail() As String
        Get
            Return _accEmail
        End Get
        Set(ByVal value As String)
            _accEmail = value
        End Set
    End Property

    Private _accLastName As String
    Public Property AccLastName() As String
        Get
            Return _accLastName
        End Get
        Set(ByVal value As String)
            _accLastName = value
        End Set
    End Property

    Private _accName As String
    Public Property AccName() As String
        Get
            Return _accName
        End Get
        Set(ByVal value As String)
            _accName = value
        End Set
    End Property

    Private _accNumber As String
    Public Property AccNumber() As String
        Get
            Return _accNumber
        End Get
        Set(ByVal value As String)
            _accNumber = value
        End Set
    End Property

    Private _accPhone As String
    Public Property AccPhone() As String
        Get
            Return _accPhone
        End Get
        Set(ByVal value As String)
            _accPhone = value
        End Set
    End Property

    Private _amount As String
    Public Property Amount() As String
        Get
            Return _amount
        End Get
        Set(ByVal value As String)
            _amount = value
        End Set
    End Property

    Private _ava As String
    Public Property Ava() As String
        Get
            Return _ava
        End Get
        Set(ByVal value As String)
            _ava = value
        End Set
    End Property

    Private _cardCVV2 As String
    Public Property CardCVV2() As String
        Get
            Return _cardCVV2
        End Get
        Set(ByVal value As String)
            _cardCVV2 = value
        End Set
    End Property

    Private _cardEmail As String
    Public Property CardEmail() As String
        Get
            Return _cardEmail
        End Get
        Set(ByVal value As String)
            _cardEmail = value
        End Set
    End Property

    Private _cardExpDate As String
    Public Property CardExpDate() As String
        Get
            Return _cardExpDate
        End Get
        Set(ByVal value As String)
            _cardExpDate = value
        End Set
    End Property

    Private _cardLastName As String
    Public Property CardLastName() As String
        Get
            Return _cardLastName
        End Get
        Set(ByVal value As String)
            _cardLastName = value
        End Set
    End Property

    Private _cardName As String
    Public Property CardName() As String
        Get
            Return _cardName
        End Get
        Set(ByVal value As String)
            _cardName = value
        End Set
    End Property

    Private _cardNumber As String
    Public Property CardNumber() As String
        Get
            Return _cardNumber
        End Get
        Set(ByVal value As String)
            _cardNumber = value
        End Set
    End Property

    Private _cardType As String
    Public Property CardType() As String
        Get
            Return _cardType
        End Get
        Set(ByVal value As String)
            _cardType = value
        End Set
    End Property

    Private _comments As String
    Public Property Comments() As String
        Get
            Return _comments
        End Get
        Set(ByVal value As String)
            _comments = value
        End Set
    End Property



    Private _invoiceBool As String
    Public Property InvoiceBool() As String
        Get
            Return _invoiceBool
        End Get
        Set(ByVal value As String)
            _invoiceBool = value
        End Set
    End Property

    Private _merchantID As String
    Public Property MerchantID() As String
        Get
            Return _merchantID
        End Get
        Set(ByVal value As String)
            _merchantID = value
        End Set
    End Property

    Private _merchantName As String
    Public Property MerchantName() As String
        Get
            Return _merchantName
        End Get
        Set(ByVal value As String)
            _merchantName = value
        End Set
    End Property

    Private _password As String
    Public Property Password() As String
        Get
            Return _password
        End Get
        Set(ByVal value As String)
            _password = value
        End Set
    End Property



    Private _registrationLink As String
    Public Property RegistrationLink() As String
        Get
            Return _registrationLink
        End Get
        Set(ByVal value As String)
            _registrationLink = value
        End Set
    End Property

    Private _resNumber As String
    Public Property ResNumber() As String
        Get
            Return _resNumber
        End Get
        Set(ByVal value As String)
            _resNumber = value
        End Set
    End Property

    Private _transcode As String
    Public Property Transcode() As String
        Get
            Return _transcode
        End Get
        Set(ByVal value As String)
            _transcode = value
        End Set
    End Property

    Private _transdate As DateTime
    Public Property Transdate() As DateTime
        Get
            Return _transdate
        End Get
        Set(ByVal value As DateTime)
            _transdate = value
        End Set
    End Property

    Private _username As String
    Public Property Username() As String
        Get
            Return _username
        End Get
        Set(ByVal value As String)
            _username = value
        End Set
    End Property

    Private _usernameGreeklish As String
    Public Property UsernameGreeklish() As String
        Get
            Return _usernameGreeklish
        End Get
        Set(ByVal value As String)
            _usernameGreeklish = value
        End Set
    End Property

    Private _userPhone As String
    Public Property UserPhone() As String
        Get
            Return _userPhone
        End Get
        Set(ByVal value As String)
            _userPhone = value
        End Set
    End Property

    Private _Installments As String
    Public Property Installments() As String
        Get
            Return _Installments
        End Get
        Set(ByVal value As String)
            _Installments = value
        End Set
    End Property

    Private _CustomFields As String
    Public Property CustomFields() As String
        Get
            Return _CustomFields
        End Get
        Set(ByVal value As String)
            _CustomFields = value
        End Set
    End Property

    Private _Commission As String
    Public Property Commission() As String
        Get
            Return _Commission
        End Get
        Set(ByVal value As String)
            _Commission = value
        End Set
    End Property
#End Region

    Private Function GetEmail(ByVal templateCode As String) As EmailMessage
        Dim res As New EmailMessage()
        Dim Template As String = String.Empty
        Dim subject As String = String.Empty

        'Get data class using custom table name
        Dim customTableClassInfo As DataClassInfo = DataClassInfoProvider.GetDataClass("LivePay.EmailTemplates")
        If customTableClassInfo Is Nothing Then
            Throw New Exception("Given custom table does not exist.")
        End If

        ' Initialize custom table item provider with current user info and general connection
        Dim ctiProvider As New CustomTableItemProvider(CMSContext.CurrentUser, ConnectionHelper.GetConnection())

        ' Get custom table items
        Dim dsItems As DataSet = ctiProvider.GetItems(customTableClassInfo.ClassName, String.Format(" TemplateCode = '{0}' ", templateCode), Nothing)

        ' Check if DataSet is not empty
        If Not DataHelper.DataSourceIsEmpty(dsItems) Then
            ' Handle the retrieved data
            If dsItems.Tables(0).Rows.Count > 0 Then
                Dim row As DataRow = dsItems.Tables(0).Rows(0)
                Template = row("TemplateText").ToString()
                subject = row("TemplateSubject").ToString()
            Else
                Throw New Exception("Wrong EmailTemplate Code")

            End If
        End If


        With (Me)
            Template = Template.Replace("[username]", .CardEmail) ' .Username)
            Template = Template.Replace("[usernameGrl]", .CardEmail) '.UsernameGreeklish)
            Template = Template.Replace("[regLink]", .RegistrationLink)
            Template = Template.Replace("[pswrd]", .Password)
            Template = Template.Replace("[transdate]", .Transdate.ToString("dd/MM/yyyy hh:mm:ss")) '29/11/2006 11:20:41 
            Template = Template.Replace("[transcode]", .Transcode)
            Template = Template.Replace("[merchantName]", .MerchantName)
            Template = Template.Replace("[ava]", .Ava)
            Template = Template.Replace("[resNumber]", .ResNumber)
            Template = Template.Replace("[cardName]", .CardName)
            Template = Template.Replace("[cardLastName]", .CardLastName)
            Template = Template.Replace("[cardEmail]", .CardEmail)
            Template = Template.Replace("[cardType]", .CardType)
            Template = Template.Replace("[cardExpDate]", .CardExpDate)
            Template = Template.Replace("[userPhone]", .UserPhone)
            Template = Template.Replace("[cardNumber]", .CardNumber)
            Template = Template.Replace("[cardCVV2]", .CardCVV2)
            Template = Template.Replace("[amount]", .Amount)
            Template = Template.Replace("[invoiceBool]", .InvoiceBool)
            Template = Template.Replace("[merchantID]", .MerchantID)
            Template = Template.Replace("[accName]", .AccName)
            Template = Template.Replace("[accLastName]", .AccLastName)
            Template = Template.Replace("[accPhone]", .AccPhone)
            Template = Template.Replace("[comments]", .Comments)
            Template = Template.Replace("[accEmail]", .AccEmail)
            Template = Template.Replace("[accNumber]", .AccNumber)
            Template = Template.Replace("[Installments]", .Installments)
            Template = Template.Replace("[CustomFields]", .CustomFields)
            Template = Template.Replace("[Commission]", .Commission)
        End With

        res.Subject = subject
        res.Body = Template
        Return res

    End Function

    ''' <summary>
    ''' Sends one email!!!
    ''' </summary>
    ''' <param name="emailType">*** From LivePay Only*** Use EmailTypes Enum to use the predifined email types. Otherwise use the value of the TeplateCode field that you previously set in the database table</param>
    ''' <param name="addressFrom"></param>
    ''' <param name="addressTo"></param>
    ''' <remarks></remarks>
    Public Sub SendEmail(ByVal emailType As String, ByVal addressFrom As String, ByVal addressTo As String)
        Dim email As EmailMessage = Me.GetEmail(emailType)
        email.From = addressFrom
        email.Recipients = addressTo
        email.EmailFormat = EmailFormatEnum.Html
        EmailSender.SendEmail(email)
    End Sub

    Public Enum EmailTypes
        USERREG
        USRNEWPASS
        MERCHCARDSUCC
        MERCHACCSUCC
        USERACCFAIL
        USERACCSUCC
        USERCARDSUCC
        MERCHNEWPASS
    End Enum


End Class

