using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Countries_Country_Edit_State_List : SiteManagerPage
{
    protected int countryID = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get country ID from url
        countryID = ValidationHelper.GetInteger(Request.QueryString["countryID"], 0);

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Country_State_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("Country_Edit_State_Edit.aspx?countryID=" + countryID);
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/CMS_Country/addstate.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        // Set country ID
        object[,] parameters = new object[1, 3];
        parameters[0, 0] = "@CountryID";
        parameters[0, 1] = countryID;
        gridStates.QueryParameters = parameters;

        gridStates.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        gridStates.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Country_Edit_State_Edit.aspx?countryID=" + countryID + "&stateID=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            if (StateInfoProvider.CheckDependencies(ValidationHelper.GetInteger(actionArgument, 0)))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("ecommerce.deletedisabledwithoutenable");
                return;
            }
            // delete StateInfo object from database
            StateInfoProvider.DeleteStateInfo(ValidationHelper.GetInteger(actionArgument, 0));
        }
    }
}
