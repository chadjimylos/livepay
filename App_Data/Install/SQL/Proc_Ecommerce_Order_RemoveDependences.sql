-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Ecommerce_Order_RemoveDependences]
	@OrderID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
	-- COM_OrderItem - Deleted by special method
	--DELETE FROM COM_OrderItem WHERE OrderItemOrderID = @OrderID;
	-- COM_OrderStatusUser
	DELETE FROM COM_OrderStatusUser WHERE OrderID = @OrderID;
	COMMIT TRANSACTION;
END
