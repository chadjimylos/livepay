CREATE TABLE [Newsletter_Emails] (
		[EmailID]                    int NOT NULL IDENTITY(1, 1),
		[EmailNewsletterIssueID]     int NOT NULL,
		[EmailSubscriberID]          int NOT NULL,
		[EmailSiteID]                int NOT NULL,
		[EmailLastSendResult]        nvarchar(max) NULL,
		[EmailLastSendAttempt]       datetime NULL,
		[EmailSending]               bit NULL,
		[EmailGUID]                  uniqueidentifier NOT NULL,
		[EmailLastModified]          datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Newsletter_Emails]
	ADD
	CONSTRAINT [PK_Newsletter_Emails]
	PRIMARY KEY
	([EmailID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Newsletter_Emails]
	ADD
	CONSTRAINT [DEFAULT_Newsletter_Emails_EmailSiteID]
	DEFAULT ((0)) FOR [EmailSiteID]
IF OBJECT_ID(N'[Newsletter_Emails]') IS NOT NULL
	AND OBJECT_ID(N'[Newsletter_NewsletterIssue]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_Emails_EmailNewsletterIssueID_Newsletter_NewsletterIssue]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_Emails]
			ADD CONSTRAINT [FK_Newsletter_Emails_EmailNewsletterIssueID_Newsletter_NewsletterIssue]
			FOREIGN KEY ([EmailNewsletterIssueID]) REFERENCES [Newsletter_NewsletterIssue] ([IssueID])
END
IF OBJECT_ID(N'[Newsletter_Emails]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_Emails_EmailSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_Emails]
			ADD CONSTRAINT [FK_Newsletter_Emails_EmailSiteID_CMS_Site]
			FOREIGN KEY ([EmailSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[Newsletter_Emails]') IS NOT NULL
	AND OBJECT_ID(N'[Newsletter_Subscriber]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_Emails_EmailSubscriberID_Newsletter_Subscriber]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_Emails]
			ADD CONSTRAINT [FK_Newsletter_Emails_EmailSubscriberID_Newsletter_Subscriber]
			FOREIGN KEY ([EmailSubscriberID]) REFERENCES [Newsletter_Subscriber] ([SubscriberID])
END
CREATE INDEX [IX_Newsletter_Emails_EmailNewsletterIssueID]
	ON [Newsletter_Emails] ([EmailNewsletterIssueID])
	ON [PRIMARY]
CREATE INDEX [IX_Newsletter_Emails_EmailSending]
	ON [Newsletter_Emails] ([EmailSending])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Newsletter_Emails_EmailSiteID]
	ON [Newsletter_Emails] ([EmailSiteID])
	ON [PRIMARY]
CREATE INDEX [IX_Newsletter_Emails_EmailSubscriberID]
	ON [Newsletter_Emails] ([EmailSubscriberID])
	ON [PRIMARY]
