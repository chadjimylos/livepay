using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.FileManager;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSAdminControls_MetaFiles_FileList : CMSUserControl
{
    #region "Variables"

    protected string colActions = "";
    protected string colFileName = "";
    protected string colFileSize = "";
    protected string colURL = "";

    protected string btnDeleteImageUrl = "";
    protected string btnDeleteToolTip = "";

    protected string btnPasteImageUrl = "";
    protected string btnPasteToolTip = "";

    protected string btnImageEditorImageUrl = "";
    protected string btnImageEditorToolTip = "";

    #endregion


    #region "Public Properties"

    /// <summary>
    /// Object id
    /// </summary>
    public int ObjectID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["ObjectID"], 0);
        }
        set
        {
            ViewState["ObjectID"] = value;
        }
    }


    /// <summary>
    /// Object type
    /// </summary>
    public string ObjectType
    {
        get
        {
            return ValidationHelper.GetString(ViewState["ObjectType"], "");
        }
        set
        {
            ViewState["ObjectType"] = value;
        }
    }


    /// <summary>
    /// Site name
    /// </summary>
    public int SiteID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["SiteID"], 0);
        }
        set
        {
            ViewState["SiteID"] = value;
        }
    }


    /// <summary>
    /// Attachment category/group
    /// </summary>
    public string Category
    {
        get
        {
            return ValidationHelper.GetString(ViewState["Category"], "");
        }
        set
        {
            ViewState["Category"] = value;
        }
    }


    /// <summary>
    /// Where condition
    /// </summary>
    public string Where
    {
        get
        {
            return ValidationHelper.GetString(ViewState["Where"], "");
        }
        set
        {
            ViewState["Where"] = value;
        }
    }


    /// <summary>
    /// Order by
    /// </summary>
    public string OrderBy
    {
        get
        {
            return ValidationHelper.GetString(ViewState["OrderBy"], "MetaFileName");
        }
        set
        {
            ViewState["OrderBy"] = value;
        }
    }


    /// <summary>
    /// Allows pasting file URLs into editable areas.
    /// </summary>
    public bool AllowPasteAttachments
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["AllowPasteAttachments"], false);
        }
        set
        {
            ViewState["AllowPasteAttachments"] = value;
        }
    }


    /// <summary>
    /// Allow edit flag
    /// </summary>
    public bool AllowEdit
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["AllowEdit"], false);
        }
        set
        {
            ViewState["AllowEdit"] = value;
        }
    }


    /// <summary>
    /// Gets or sets javascript for upload button.
    /// </summary>
    public string UploadOnClickScript
    {
        get
        {
            return btnUpload.OnClientClick;
        }
        set
        {
            btnUpload.OnClientClick = value;
        }
    }

    #endregion


    #region "Events"

    /// <summary>
    /// Event fired before upload processing
    /// </summary>
    public event EventHandler OnBeforeUpload;


    /// <summary>
    /// Event fired after upload processing
    /// </summary>
    public event EventHandler OnAfterUpload;


    /// <summary>
    /// Event fired before delete processing
    /// </summary>
    public event EventHandler OnBeforeDelete;


    /// <summary>
    /// Event fired after delete processing
    /// </summary>
    public event EventHandler OnAfterDelete;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register dialog script for Image Editor
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "OpenImageEditor",
            ScriptHelper.GetScript("function OpenImageEditor(MetafileGUID) { " +
                "modalDialog('" + UrlHelper.GetFullApplicationUrl() + "/CMSModules/Content/CMSDesk/Edit/ImageEditor.aspx?metafileguid=' + MetafileGUID, '" + ResHelper.GetString("general.editimage") + "', 905, 670); " +
                "return false; } "));
        // Register javascript 'postback' function
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "PostBack", ScriptHelper.GetScript(
                "function UpdatePage(){ " + this.Page.ClientScript.GetPostBackEventReference(btnHidden, "") + "; } \n"));

        // Register javascript to confirm delete action
        string script = "function ConfirmDelete() {return confirm(" + ScriptHelper.GetString(ResHelper.GetString("general.confirmdelete")) + ");}";
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ConfirmDeleteFile", ScriptHelper.GetScript(script));

        this.btnDeleteImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Delete.png");
        this.btnPasteImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/pasteimg.png");
        this.btnImageEditorImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/Edit.png");

        this.btnDeleteToolTip = ResHelper.GetString("general.delete");
        this.btnPasteToolTip = ResHelper.GetString("checkoutprocess.btnpastetooltip");
        this.btnImageEditorToolTip = ResHelper.GetString("general.editimage");

        this.colActions = ResHelper.GetString("general.action");
        this.colFileName = ResHelper.GetString("general.filename");
        this.colFileSize = ResHelper.GetString("filelist.unigrid.colfilesize");
        this.colURL = ResHelper.GetString("filelist.unigrid.colurl");

        this.btnUpload.Text = ResHelper.GetString("filelist.btnupload");
        this.btnUpload.Enabled = AllowEdit;
        this.lblUpload.Text = ResHelper.GetString("filelist.lblupload");

        this.gridFiles.OnAction += new OnActionEventHandler(gridFiles_OnAction);
        this.gridFiles.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridFiles_OnExternalDataBound);
        this.gridFiles.IsLiveSite = this.IsLiveSite;
        this.gridFiles.WhereCondition = MetaFileInfoProvider.GetWhereCondition(ObjectID, ObjectType, Category, Where);
        this.gridFiles.OrderBy = this.OrderBy;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Hide filename filter column.
        this.gridFiles.GridView.Columns[3].Visible = false;
    }


    /// <summary>
    /// btnUpload click event handler.
    /// </summary>
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (!AllowEdit || (uploader.PostedFile == null) || (uploader.PostedFile.FileName.Trim() == ""))
        {
            return;
        }

        try
        {
            if (this.OnBeforeUpload != null)
            {
                OnBeforeUpload(this, EventArgs.Empty);
            }

            // Create new meta file
            MetaFileInfo mfi = new MetaFileInfo(uploader.PostedFile, this.ObjectID, this.ObjectType, this.Category);
            mfi.MetaFileSiteID = this.SiteID;

            // Save file to the database
            MetaFileInfoProvider.SetMetaFileInfo(mfi);

            this.gridFiles.ReloadData();

            if (this.OnAfterUpload != null)
            {
                OnAfterUpload(this, EventArgs.Empty);
            }
        }
        catch (Exception ex)
        {
            this.lblError.Visible = true;
            this.lblError.Text = ex.Message;
        }
    }


    #region "UniGrid Events"

    protected object gridFiles_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        GridViewRow gvr = null;
        DataRowView drv = null;
        string fileName = null;
        string fileGuid = null;

        switch (sourceName.ToLower())
        {
            case "edit":
                if (sender is ImageButton)
                {
                    gvr = (GridViewRow)parameter;
                    drv = (DataRowView)gvr.DataItem;

                    fileGuid = ValidationHelper.GetString(drv["MetaFileGUID"], "");
                    string fileExtension = ValidationHelper.GetString(drv["MetaFileExtension"], "");

                    // Initialize properties
                    ImageButton btnImageEditor = (ImageButton)sender;
                    btnImageEditor.Visible = true;

                    // Display button only if metafile is in supported image format and only if user is global administrator
                    if (ImageHelper.IsSupportedByImageEditor(fileExtension) && (CMSContext.CurrentUser != null) && (CMSContext.CurrentUser.IsGlobalAdministrator))
                    {
                        // Initialize button with script
                        btnImageEditor.Attributes.Add("onclick", "OpenImageEditor('" + fileGuid + "'); return false;");
                    }
                    else
                    {
                        btnImageEditor.ImageUrl = GetImageUrl("Design/Controls/UniGrid/Actions/editdisabled.png");
                        btnImageEditor.Enabled = false;
                    }
                }
                break;

            case "paste":
                if (sender is ImageButton)
                {

                    gvr = (GridViewRow)parameter;
                    drv = (DataRowView)gvr.DataItem;

                    fileGuid = ValidationHelper.GetString(drv["MetaFileGUID"], "");
                    int fileWidth = ValidationHelper.GetInteger(drv["MetaFileImageWidth"], 0);
                    int fileHeight = ValidationHelper.GetInteger(drv["MetaFileImageHeight"], 0);

                    ImageButton btnPaste = (ImageButton)sender;
                    btnPaste.Visible = AllowPasteAttachments;
                    if (AllowPasteAttachments)
                    {
                        if ((fileWidth > 0) && (fileHeight > 0))
                        {
                            string appPath = UrlHelper.GetApplicationPath();
                            if ((appPath == null) || (appPath == "/"))
                            {
                                appPath = String.Empty;
                            }
                            btnPaste.OnClientClick = "PasteImage('" +
                                 appPath + "/CMSPages/GetMetaFile.aspx"
                                + "?fileguid=" + fileGuid + "'); return false";
                        }
                        else
                        {
                            btnPaste.Visible = false;
                        }
                    }
                    else
                    {
                        btnPaste.Visible = false;
                    }
                }
                break;

            case "delete":
                if (sender is ImageButton)
                {
                    ImageButton btnDelete = ((ImageButton)sender);
                    btnDelete.Visible = this.AllowEdit;
                }
                break;

            case "name":

                drv = (DataRowView)parameter;

                fileName = ValidationHelper.GetString(drv["MetaFileName"], "");
                fileGuid = ValidationHelper.GetString(drv["MetaFileGUID"], "");

                return "<a href=\"" + UrlHelper.GetAbsoluteUrl("~/CMSPages/GetMetaFile.aspx") + "?fileguid=" +
                    fileGuid + "\" target=\"_blank\">" + fileName + "</a>";

            case "size":
                return DataHelper.GetSizeString(ValidationHelper.GetLong(parameter, 0));
        }
        return parameter;
    }


    protected void gridFiles_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "delete":
                try
                {
                    if (this.OnBeforeDelete != null)
                    {
                        OnBeforeDelete(this, EventArgs.Empty);
                    }

                    MetaFileInfoProvider.DeleteMetaFileInfo(ValidationHelper.GetInteger(actionArgument, 0));

                    if (this.OnAfterDelete != null)
                    {
                        OnAfterDelete(this, EventArgs.Empty);
                    }
                }
                catch (Exception ex)
                {
                    lblError.Visible = true;
                    lblError.Text = ex.Message;
                }
                break;
        }
    }


    #endregion
}