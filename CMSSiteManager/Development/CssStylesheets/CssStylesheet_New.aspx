<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CssStylesheet_New.aspx.cs"
    Inherits="CMSSiteManager_Development_CssStylesheets_CssStylesheet_New" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Css stylesheets - new" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCssStylesheetDisplayName" runat="server" EnableViewState="False" />
            </td>
            <td>
                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" /><br />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" EnableViewState="false"
                    ControlToValidate="txtDisplayName" Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCssStylesheetName" runat="server" EnableViewState="False" />
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server" CssClass="TextBoxField" MaxLength="200" /><br />
                <asp:RequiredFieldValidator ID="rfvName" runat="server" EnableViewState="false" ControlToValidate="txtName"
                    Display="dynamic" />
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:Label ID="lblCssStylesheetText" runat="server" Text="Label" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:ExtendedTextArea ID="txtText" runat="server" CssClass="TextAreaField" Height="330px"
                    TextMode="MultiLine" Width="770px"></cms:ExtendedTextArea><br />
                <asp:RequiredFieldValidator ID="rfvText" runat="server" ControlToValidate="txtText"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox runat="server" ID="chkAssign" Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" EnableViewState="false"
                    OnClick="btnOK_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
