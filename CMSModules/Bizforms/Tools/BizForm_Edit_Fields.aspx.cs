using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.FormEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_BizForms_Tools_BizForm_Edit_Fields : CMSBizFormPage
{
    protected int formId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'ReadForm' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.form", "ReadForm"))
        {
            RedirectToCMSDeskAccessDenied("cms.form", "ReadForm");
        }

        formId = QueryHelper.GetInteger("formid", 0);
        BizFormInfo bfi = BizFormInfoProvider.GetBizFormInfo(formId);
               
        string className = null;
        bool isCoupledClass = false;
        DataClassInfo dci = null;        
        FieldEditor.Visible = false;

        if (bfi != null)
        {
            // Get form class info
            dci = DataClassInfoProvider.GetDataClass(bfi.FormClassID);            
            if (dci != null)
            {
                className = dci.ClassName;
                isCoupledClass = dci.ClassIsCoupledClass;

                if (dci.ClassIsCoupledClass)
                {
                    FieldEditor.Visible = true;
                    FieldEditor.ClassName = className;
                    FieldEditor.EnableSimplifiedMode = true;
                    FieldEditor.Mode = FieldEditorModeEnum.BizFormDefinition;
                    FieldEditor.OnAfterDefinitionUpdate += new OnAfterDefinitionUpdateEventHandler(FieldEditor_OnAfterDefinitionUpdate);
                }
                else
                {
                    lblError.Text = ResHelper.GetString("EditTemplateFields.ErrorIsNotCoupled");
                }
            }
        }
    }


    void FieldEditor_OnAfterDefinitionUpdate()
    {
        // Update form to log synchronization
        if (formId > 0)
        {
            BizFormInfo formObj = BizFormInfoProvider.GetBizFormInfo(formId);
            if (formObj != null)
            {
                // Enforce Form property reload next time the data are needed
                formObj.ResetFormInfo();
                BizFormInfoProvider.SetBizFormInfo(formObj);
            }
        }
    }
}
