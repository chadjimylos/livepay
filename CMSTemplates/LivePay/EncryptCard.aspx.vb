﻿
Partial Class CMSTemplates_LivePay_EncryptCard
    Inherits System.Web.UI.Page

    Protected Sub encrypt_Click(sender As Object, e As EventArgs) Handles encrypt.Click
        Dim Card As String = StripHTMLFunctions.StripTags(tbCard.Text)
        Dim EncryptedCard As String = Left(Card, 4) & CryptoFunctions.EncryptString(Right(Card, 12))
        litEncryptCard.Text = EncryptedCard
        resultEncryptCard.Visible = True
    End Sub
End Class
