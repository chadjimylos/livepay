CREATE TABLE [CMS_InlineControl] (
		[ControlID]                int NOT NULL IDENTITY(1, 1),
		[ControlDisplayName]       nvarchar(200) NOT NULL,
		[ControlName]              nvarchar(200) NOT NULL,
		[ControlParameterName]     nvarchar(200) NULL,
		[ControlDescription]       nvarchar(max) NULL,
		[ControlGUID]              uniqueidentifier NOT NULL,
		[ControlLastModified]      datetime NOT NULL,
		[ControlFileName]          nvarchar(400) NULL,
		[ControlProperties]        nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_InlineControl]
	ADD
	CONSTRAINT [PK_CMS_InlineControl]
	PRIMARY KEY
	NONCLUSTERED
	([ControlID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_InlineControl]
	ADD
	CONSTRAINT [DEFAULT_CMS_InlineControl_ControlFileName]
	DEFAULT ('') FOR [ControlFileName]
CREATE CLUSTERED INDEX [IX_CMS_InlineControl_ControlDisplayName]
	ON [CMS_InlineControl] ([ControlDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
