﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;

using CMS.CMSHelper;
using CMS.DataEngine;
using CMS.FileManager;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.Staging;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;
using CMS.EventLog;

using TreeNode = CMS.TreeEngine.TreeNode;

/// <summary>
/// Image editor for attachments and meta files.
/// </summary>
public partial class CMSAdminControls_ImageEditor_ImageEditor : CMSUserControl
{
    #region "Variables"

    private Guid attachmentGuid = Guid.Empty;
    private Guid metafileGuid = Guid.Empty;
    private AttachmentInfo ai = null;
    private MetaFileInfo mf = null;
    private AttachmentManager mAttachmentManager = null;
    private VersionManager mVersionManager = null;
    private string mCurrentSiteName = null;
    private bool mRefreshAfterAction = true;
    private string externalControlID = null;
    private int siteId = 0;

    #endregion


    #region "Properties"

    /// <summary>
    /// Version manager
    /// </summary>
    public VersionManager VersionManager
    {
        get
        {
            if (mVersionManager == null)
            {
                mVersionManager = new VersionManager(baseImageEditor.Tree);
            }
            return mVersionManager;
        }
    }


    /// <summary>
    /// Attachment manager
    /// </summary>
    public AttachmentManager AttachmentManager
    {
        get
        {
            if (mAttachmentManager == null)
            {
                mAttachmentManager = new AttachmentManager(baseImageEditor.Tree.Connection);
            }
            return mAttachmentManager;
        }
    }


    /// <summary>
    /// Indicates whether the refresh should be risen after edit action takes place.
    /// </summary>
    public bool RefreshAfterAction
    {
        get
        {
            return this.mRefreshAfterAction;
        }
        set
        {
            this.mRefreshAfterAction = value;
        }
    }


    /// <summary>
    /// Returns the site name from query string 'sitename' or 'siteid' if present, otherwise CMSContext.CurrentSiteName.
    /// </summary>
    private string CurrentSiteName
    {
        get
        {
            if (mCurrentSiteName == null)
            {
                mCurrentSiteName = QueryHelper.GetString("sitename", CMSContext.CurrentSiteName);

                siteId = QueryHelper.GetInteger("siteid", 0);

                SiteInfo site = SiteInfoProvider.GetSiteInfo(siteId);
                if (site != null)
                {
                    mCurrentSiteName = site.SiteName;
                }
            }
            return mCurrentSiteName;
        }
    }


    /// <summary>
    /// Version history ID
    /// </summary>
    public int VersionHistoryID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["VersionHistoryID"], 0);
        }
        set
        {
            ViewState["VersionHistoryID"] = value;
        }
    }

    #endregion


    #region "Events"

    /// <summary>
    /// Loads image URL.
    /// </summary>
    void baseImageEditor_LoadImageUrl()
    {
        // Use appropriate parameter from URL
        string url = null;
        if (attachmentGuid != Guid.Empty)
        {
            int documentId = (ai != null) ? ai.AttachmentDocumentID : 0;
            bool useLatestDoc = (IsLiveSite && (documentId > 0));

            url = "~/CMSPages/GetFile.aspx?guid=" + attachmentGuid + "&sitename=" + this.CurrentSiteName;
            if ((VersionHistoryID != 0) && !useLatestDoc)
            {
                url += "&versionhistoryid=" + VersionHistoryID;
            }

            // Add latest version requirement for live site
            if (useLatestDoc)
            {
                // Add requirement for latest version of files for current document
                string newparams = "latestfordocid=" + documentId;
                newparams += "&hash=" + ValidationHelper.GetHashString("d" + documentId);

                url += "&" + newparams;
            }

        }
        else if (metafileGuid != Guid.Empty)
        {
            url = "~/CMSPages/GetMetaFile.aspx?fileguid=" + metafileGuid + "&sitename=" + this.CurrentSiteName;
        }
        baseImageEditor.AttUrl = UrlHelper.UpdateParameterInUrl(url, "chset", Guid.NewGuid().ToString());
    }


    /// <summary>
    /// Loads image type from querystring.
    /// </summary>
    void baseImageEditor_LoadImageType()
    {
        // Use appropriate parameter from URL
        if (attachmentGuid != Guid.Empty)
        {
            baseImageEditor.ImageType = ImageHelper.ImageTypeEnum.Attachment;
        }
        else if (metafileGuid != Guid.Empty)
        {
            baseImageEditor.ImageType = ImageHelper.ImageTypeEnum.Metafile;
        }
    }


    /// <summary>
    /// Initializes common properties used for processing image. 
    /// </summary>
    void baseImageEditor_InitializeProperties()
    {
        CurrentUserInfo currentUser = CMSContext.CurrentUser;

        // Process attachment
        switch (baseImageEditor.ImageType)
        {
            default:
            case ImageHelper.ImageTypeEnum.Attachment:
                {
                    baseImageEditor.Tree = new TreeProvider(currentUser);

                    // If using workflow then get versioned attachment
                    if (VersionHistoryID != 0)
                    {
                        IDataClass dc = VersionManager.GetAttachmentVersion(VersionHistoryID, attachmentGuid);
                        if ((dc == null) || (dc.IsEmpty()))
                        {
                            ai = null;
                        }
                        else
                        {
                            ai = new AttachmentInfo(dc.DataRow, ConnectionHelper.GetConnection());
                            ai.AttachmentID = ValidationHelper.GetInteger(dc.DataRow["AttachmentHistoryID"], 0);
                        }
                    }
                    // else get file without binary data
                    else
                    {
                        ai = AttachmentManager.GetAttachmentInfoWithoutBinary(attachmentGuid, this.CurrentSiteName);
                    }

                    // If file is not null and current user is set
                    if (ai != null)
                    {
                        TreeNode node = null;
                        if (ai.AttachmentDocumentID > 0)
                        {
                            node = baseImageEditor.Tree.SelectSingleDocument(ai.AttachmentDocumentID);
                        }
                        else
                        {
                            // Get parent node ID in case attachment is edited for document not created yet
                            int parentNodeId = QueryHelper.GetInteger("parentId", 0);

                            node = baseImageEditor.Tree.SelectSingleNode(parentNodeId);
                        }

                        // If current user has appropriate permissions then set image - check hash fro live site otherwise check node permissions
                        if ((currentUser != null) && (node != null) && ((IsLiveSite && QueryHelper.ValidateHash("hash")) || (!IsLiveSite && (currentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Allowed))))
                        {
                            // Ensure attachment binary data
                            if (VersionHistoryID == 0)
                            {
                                ai.AttachmentBinary = AttachmentManager.GetFile(ai, this.CurrentSiteName);
                            }

                            if (ai.AttachmentBinary != null)
                            {
                                baseImageEditor.ImgHelper = new ImageHelper(ai.AttachmentBinary);
                            }
                            else
                            {
                                baseImageEditor.LoadingFailed = true;
                                baseImageEditor.LblLoadFailed.ResourceString = "img.errors.loading";
                            }
                        }
                        else
                        {
                            baseImageEditor.LoadingFailed = true;
                            baseImageEditor.LblLoadFailed.ResourceString = "img.errors.filemodify";
                        }
                    }
                    else
                    {
                        baseImageEditor.LoadingFailed = true;
                        baseImageEditor.LblLoadFailed.ResourceString = "img.errors.loading";
                    }
                }
                break;

            // Process metafile
            case ImageHelper.ImageTypeEnum.Metafile:
                {
                    // Get metafile
                    mf = MetaFileInfoProvider.GetMetaFileInfoWithoutBinary(metafileGuid, this.CurrentSiteName, true);

                    // If file is not null and current user is global administrator then set image
                    if (mf != null)
                    {
                        if ((currentUser != null) && (currentUser.IsGlobalAdministrator))
                        {
                            // Ensure metafile binary data
                            mf.MetaFileBinary = MetaFileInfoProvider.GetFile(mf, this.CurrentSiteName);
                            if (mf.MetaFileBinary != null)
                            {
                                baseImageEditor.ImgHelper = new ImageHelper(mf.MetaFileBinary);
                            }
                            else
                            {
                                baseImageEditor.LoadingFailed = true;
                                baseImageEditor.LblLoadFailed.ResourceString = "img.errors.loading";
                            }
                        }
                        else
                        {
                            baseImageEditor.LoadingFailed = true;
                            baseImageEditor.LblLoadFailed.ResourceString = "img.errors.rights";
                        }
                    }
                    else
                    {
                        baseImageEditor.LoadingFailed = true;
                        baseImageEditor.LblLoadFailed.ResourceString = "img.errors.loading";
                    }
                }
                break;
        }

        // Check that image is in supported formats
        if ((!baseImageEditor.LoadingFailed) && (baseImageEditor.ImgHelper.ImageFormatToString() == null))
        {
            baseImageEditor.LoadingFailed = true;
            baseImageEditor.LblLoadFailed.ResourceString = "img.errors.format";
        }
    }


    /// <summary>
    /// Initialize labels according to current image type.
    /// </summary>
    void baseImageEditor_InitializeLabels(bool reloadName)
    {
        double doubleValue = 0;

        //Initialize strings depending on image type
        switch (baseImageEditor.ImageType)
        {
            default:
            case ImageHelper.ImageTypeEnum.Attachment:
                if (ai != null)
                {
                    if (reloadName)
                    {
                        baseImageEditor.TxtFileName.Text = ai.AttachmentName.Substring(0, (ai.AttachmentName.Length - (ai.AttachmentExtension.Length)));
                    }
                    baseImageEditor.LblExtensionValue.Text = ai.AttachmentExtension.Substring(1, (ai.AttachmentExtension.Length - 1));
                    doubleValue = ai.AttachmentBinary.Length / (double)1024;
                    baseImageEditor.LblImageSizeValue.Text = doubleValue.ToString("F");
                    baseImageEditor.LblWidthValue.Text = ai.AttachmentImageWidth.ToString();
                    baseImageEditor.LblHeightValue.Text = ai.AttachmentImageHeight.ToString();
                }
                break;

            case ImageHelper.ImageTypeEnum.Metafile:
                if (mf != null)
                {
                    if (!RequestHelper.IsPostBack())
                    {
                        baseImageEditor.TxtFileName.Text = mf.MetaFileName.Substring(0, (mf.MetaFileName.Length - (mf.MetaFileExtension.Length)));
                    }
                    baseImageEditor.LblExtensionValue.Text = mf.MetaFileExtension.Substring(1, (mf.MetaFileExtension.Length - 1));
                    doubleValue = mf.MetaFileBinary.Length / (double)1024;
                    baseImageEditor.LblImageSizeValue.Text = doubleValue.ToString("F");
                    baseImageEditor.LblWidthValue.Text = mf.MetaFileImageWidth.ToString();
                    baseImageEditor.LblHeightValue.Text = mf.MetaFileImageHeight.ToString();
                }
                break;
        }
    }


    /// <summary>
    /// Saves modified image data.
    /// </summary>
    /// <param name="name">Image name</param>
    /// <param name="extension">Image extension</param>
    /// <param name="mimetype">Image mimetype</param>
    /// <param name="binary">Image binary data</param>
    /// <param name="width">Image width</param>
    /// <param name="height">Image height</param>
    void baseImageEditor_SaveImage(string name, string extension, string mimetype, byte[] binary, int width, int height)
    {
        SaveImage(name, extension, mimetype, binary, width, height);
    }


    /// <summary>
    /// Returns image name according to image type.
    /// </summary>
    /// <returns>Image name</returns>
    void baseImageEditor_GetName()
    {
        string name = "";

        switch (baseImageEditor.ImageType)
        {
            case ImageHelper.ImageTypeEnum.Attachment:
                if (ai != null)
                {
                    name = ai.AttachmentName.Substring(0, (ai.AttachmentName.Length - ai.AttachmentExtension.Length));
                }
                break;

            case ImageHelper.ImageTypeEnum.Metafile:
                if (mf != null)
                {
                    name = mf.MetaFileName.Substring(0, (mf.MetaFileName.Length - mf.MetaFileExtension.Length));
                }
                break;
        }

        baseImageEditor.GetNameResult = name;
    }


    /// <summary>
    /// Sets file name according to image type.
    /// </summary>
    /// <param name="name">New name</param>
    void baseImageEditor_SetName(string name)
    {
        string newName = name + "." + baseImageEditor.CurrentFormat;

        // Check that file name is unique in case that it is attachment
        if (((baseImageEditor.ImageType == ImageHelper.ImageTypeEnum.Attachment) && IsNameUnique(newName)) || (baseImageEditor.ImageType == ImageHelper.ImageTypeEnum.Metafile))
        {
            if (this.RefreshAfterAction)
            {
                baseImageEditor.LtlScript.Text = ScriptHelper.GetScript("Refresh();");
            }
            SaveImage(newName, "", "", null, 0, 0);
            baseImageEditor.PropagateChanges(true);
        }
        else
        {
            baseImageEditor.LblFileNameValidationFailed.ResourceString = "img.namenotunique";
            baseImageEditor.LblFileNameValidationFailed.Visible = true;
        }
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize GUID from query
        attachmentGuid = QueryHelper.GetGuid("attachmentguid", Guid.Empty);
        metafileGuid = QueryHelper.GetGuid("metafileguid", Guid.Empty);
        if (!RequestHelper.IsPostBack())
        {
            VersionHistoryID = QueryHelper.GetInteger("VersionHistoryID", 0);
        }
        externalControlID = QueryHelper.GetString("clientid", null);

        baseImageEditor.LoadImageType += new CMSAdminControls_ImageEditor_BaseImageEditor.OnLoadImageType(baseImageEditor_LoadImageType);
        baseImageEditor.LoadImageUrl += new CMSAdminControls_ImageEditor_BaseImageEditor.OnLoadImageUrl(baseImageEditor_LoadImageUrl);
        baseImageEditor.InitializeProperties += new CMSAdminControls_ImageEditor_BaseImageEditor.OnInitializeProperties(baseImageEditor_InitializeProperties);
        baseImageEditor.InitializeLabels += new CMSAdminControls_ImageEditor_BaseImageEditor.OnInitializeLabels(baseImageEditor_InitializeLabels);
        baseImageEditor.SaveImage += new CMSAdminControls_ImageEditor_BaseImageEditor.OnSaveImage(baseImageEditor_SaveImage);
        baseImageEditor.GetName += new CMSAdminControls_ImageEditor_BaseImageEditor.OnGetName(baseImageEditor_GetName);
        baseImageEditor.SetName += new CMSAdminControls_ImageEditor_BaseImageEditor.OnSetName(baseImageEditor_SetName);
    }


    /// <summary>
    /// Checks whether the name is unique
    /// </summary>
    /// <returns></returns>
    private bool IsNameUnique(string name)
    {
        if ((ai != null) && (AttachmentManager != null))
        {
            // Check that the name is unique in the document or version context
            Guid attachmentFormGuid = QueryHelper.GetGuid("formguid", Guid.Empty);
            bool nameIsUnique = false;
            if (attachmentFormGuid == Guid.Empty)
            {
                // Get the node
                TreeNode node = DocumentHelper.GetDocument(ai.AttachmentDocumentID, baseImageEditor.Tree);
                nameIsUnique = DocumentHelper.IsUniqueAttachmentName(node, name, ai.AttachmentID, baseImageEditor.Tree);
            }
            else
            {
                nameIsUnique = AttachmentManager.IsUniqueTemporaryAttachmentName(attachmentFormGuid, name, ai.AttachmentID);
            }
            return nameIsUnique;
        }

        return false;
    }


    /// <summary>
    /// Saves modified image data.
    /// </summary>
    /// <param name="name">Image name</param>
    /// <param name="extension">Image extension</param>
    /// <param name="mimetype">Image mimetype</param>
    /// <param name="binary">Image binary data</param>
    /// <param name="width">Image width</param>
    /// <param name="height">Image height</param>
    private void SaveImage(string name, string extension, string mimetype, byte[] binary, int width, int height)
    {
        // Save image data depending to image type
        switch (baseImageEditor.ImageType)
        {
            // Process attachment
            case ImageHelper.ImageTypeEnum.Attachment:
                if ((ai != null) && (AttachmentManager != null))
                {
                    // Save new data
                    try
                    {
                        // Get the node
                        TreeNode node = DocumentHelper.GetDocument(ai.AttachmentDocumentID, baseImageEditor.Tree);
                        bool doCheck = false;
                        // Ensure automatic check-in/ check-out
                        bool useWorkflow = false;
                        bool autoCheck = !VersionManager.UseCheckInCheckOut(this.CurrentSiteName);
                        WorkflowManager workflowMan = new WorkflowManager(baseImageEditor.Tree);
                        if (node != null)
                        {
                            // Check if the document uses workflow
                            if (workflowMan.GetNodeWorkflow(node) != null)
                            {
                                useWorkflow = true;
                            }

                            // Check out the document
                            doCheck = autoCheck && useWorkflow; // && (workflowMan.GetStepInfo(node).StepName == "published");
                            if (doCheck)
                            {
                                VersionManager.CheckOut(node);
                                VersionHistoryID = node.DocumentCheckedOutVersionHistoryID;
                            }

                            // Workflow has been lost, get published attachment
                            if (useWorkflow && (VersionHistoryID == 0))
                            {
                                ai = AttachmentManager.GetAttachmentInfo(ai.AttachmentGUID, this.CurrentSiteName);
                            }
                        }

                        if (ai != null)
                        {
                            // Test all parameters to empty values and update new value if available
                            if (name != "")
                            {
                                ai.AttachmentName = name;
                            }
                            if (extension != "")
                            {
                                ai.AttachmentExtension = extension;
                            }
                            if (mimetype != "")
                            {
                                ai.AttachmentMimeType = mimetype;
                            }
                            if (binary != null)
                            {
                                ai.AttachmentBinary = binary;
                                ai.AttachmentSize = binary.Length;
                            }
                            if (width > 0)
                            {
                                ai.AttachmentImageWidth = width;
                            }
                            if (height > 0)
                            {
                                ai.AttachmentImageHeight = height;
                            }

                            if (VersionHistoryID > 0)
                            {
                                VersionManager.SaveAttachmentVersion(ai, VersionHistoryID);
                            }
                            else
                            {
                                AttachmentManager.SetAttachmentInfo(ai);

                                // Log the sycnhronization task for the document
                                if (node != null)
                                {
                                    DocumentHelper.LogSynchronization(node, TaskTypeEnum.UpdateDocument, baseImageEditor.Tree);
                                }
                            }

                            // Check in the document
                            if (doCheck)
                            {
                                if (VersionManager != null)
                                {
                                    if (VersionHistoryID > 0 && node != null)
                                    {
                                        VersionManager.CheckIn(node, null, null);
                                    }
                                }
                            }

                            if (this.RefreshAfterAction)
                            {
                                if (doCheck || (node == null))
                                {
                                    baseImageEditor.LtlScript.Text = ScriptHelper.GetScript("InitRefresh('" + externalControlID + "', true, '" + ai.AttachmentGUID + "', 'refresh')");
                                }
                                else
                                {
                                    baseImageEditor.LtlScript.Text = ScriptHelper.GetScript("InitRefresh('" + externalControlID + "', false, '" + ai.AttachmentGUID + "', 'refresh')");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        baseImageEditor.LblLoadFailed.Visible = true;
                        baseImageEditor.LblLoadFailed.ResourceString = "img.errors.processing";
                        ScriptHelper.AppendTooltip(baseImageEditor.LblLoadFailed, ex.Message, "help");
                        EventLogProvider.LogException("Image editor", "SAVEIMAGE", ex);
                        baseImageEditor.LoadingFailed = true;
                    }
                }
                break;

            // Process metafile
            case ImageHelper.ImageTypeEnum.Metafile:
                if (mf != null)
                {
                    try
                    {
                        // Test all parameters to empty values and update new value if available
                        if (name.CompareTo("") != 0)
                        {
                            mf.MetaFileName = name;
                        }
                        if (extension.CompareTo("") != 0)
                        {
                            mf.MetaFileExtension = extension;
                        }
                        if (mimetype.CompareTo("") != 0)
                        {
                            mf.MetaFileMimeType = mimetype;
                        }
                        if (binary != null)
                        {
                            mf.MetaFileBinary = binary;
                            mf.MetaFileSize = binary.Length;
                        }
                        if (width > 0)
                        {
                            mf.MetaFileImageWidth = width;
                        }
                        if (height > 0)
                        {
                            mf.MetaFileImageHeight = height;
                        }

                        // Save new data
                        MetaFileInfoProvider.SetMetaFileInfo(mf);
                    }
                    catch (Exception ex)
                    {
                        baseImageEditor.LblLoadFailed.Visible = true;
                        baseImageEditor.LblLoadFailed.ResourceString = "img.errors.processing";
                        ScriptHelper.AppendTooltip(baseImageEditor.LblLoadFailed, ex.Message, "help"); 
                        EventLogProvider.LogException("Image editor", "SAVEIMAGE", ex);
                        baseImageEditor.LoadingFailed = true;
                    }
                }
                break;
        }
    }

    #endregion
}
