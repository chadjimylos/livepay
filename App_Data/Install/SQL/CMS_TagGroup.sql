CREATE TABLE [CMS_TagGroup] (
		[TagGroupID]               int NOT NULL IDENTITY(1, 1),
		[TagGroupDisplayName]      nvarchar(250) NOT NULL,
		[TagGroupName]             nvarchar(250) NOT NULL,
		[TagGroupDescription]      nvarchar(max) NOT NULL,
		[TagGroupSiteID]           int NOT NULL,
		[TagGroupIsAdHoc]          bit NOT NULL,
		[TagGroupLastModified]     datetime NOT NULL,
		[TagGroupGUID]             uniqueidentifier NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_TagGroup]
	ADD
	CONSTRAINT [PK_CMS_TagGroup]
	PRIMARY KEY
	NONCLUSTERED
	([TagGroupID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_TagGroup]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_TagGroup_TagGroupSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_TagGroup]
			ADD CONSTRAINT [FK_CMS_TagGroup_TagGroupSiteID_CMS_Site]
			FOREIGN KEY ([TagGroupSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_CMS_TagGroup_TagGroupDisplayName]
	ON [CMS_TagGroup] ([TagGroupDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_TagGroup_TagGroupSiteID]
	ON [CMS_TagGroup] ([TagGroupSiteID])
	ON [PRIMARY]
