CREATE VIEW [View_Poll_AnswerCount]
AS
SELECT     Polls_Poll.*,
			(SELECT     SUM(AnswerCount) AS AnswerCount
                            FROM          Polls_PollAnswer
                            WHERE      (AnswerPollID = Polls_Poll.PollID)) AS AnswerCount
FROM         Polls_Poll
