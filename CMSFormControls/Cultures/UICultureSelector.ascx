<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UICultureSelector.ascx.cs"
    Inherits="CMSFormControls_Cultures_UICultureSelector" %>
<%@ Register Src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" TagName="UniSelector" TagPrefix="cms" %>
<cms:UniSelector ID="uniSelector" runat="server" ObjectType="CMS.UICulture" ReturnColumnName="UICultureCode"
    OrderBy="UICultureName ASC"  />
