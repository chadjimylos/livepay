CREATE TABLE [CMS_PageTemplateCategory] (
		[CategoryID]                     int NOT NULL IDENTITY(1, 1),
		[CategoryDisplayName]            nvarchar(100) NOT NULL,
		[CategoryParentID]               int NULL,
		[CategoryName]                   nvarchar(100) NOT NULL,
		[CategoryGUID]                   uniqueidentifier NOT NULL,
		[CategoryLastModified]           datetime NOT NULL,
		[CategoryImagePath]              nvarchar(450) NULL,
		[CategoryChildCount]             int NULL,
		[CategoryTemplateChildCount]     int NULL,
		[CategoryPath]                   nvarchar(450) NULL,
		[CategoryOrder]                  int NULL,
		[CategoryLevel]                  int NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_PageTemplateCategory]
	ADD
	CONSTRAINT [PK_CMS_PageTemplateCategory]
	PRIMARY KEY
	NONCLUSTERED
	([CategoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_PageTemplateCategory]
	ADD
	CONSTRAINT [DF_CMS_PageTemplateCategory_CategoryChildCount]
	DEFAULT ((0)) FOR [CategoryChildCount]
ALTER TABLE [CMS_PageTemplateCategory]
	ADD
	CONSTRAINT [DF_CMS_PageTemplateCategory_CategoryTemplateChildCount]
	DEFAULT ((0)) FOR [CategoryTemplateChildCount]
IF OBJECT_ID(N'[CMS_PageTemplateCategory]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_PageTemplateCategory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_PageTemplateCategory_CategoryParentID_CMS_PageTemplateCategory]') IS NULL
BEGIN
		ALTER TABLE [CMS_PageTemplateCategory]
			ADD CONSTRAINT [FK_CMS_PageTemplateCategory_CategoryParentID_CMS_PageTemplateCategory]
			FOREIGN KEY ([CategoryParentID]) REFERENCES [CMS_PageTemplateCategory] ([CategoryID])
END
CREATE INDEX [IX_CMS_PageTemplateCategory_CategoryLevel]
	ON [CMS_PageTemplateCategory] ([CategoryLevel])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_PageTemplateCategory_CategoryParentID]
	ON [CMS_PageTemplateCategory] ([CategoryParentID])
	ON [PRIMARY]
CREATE UNIQUE CLUSTERED INDEX [IX_CMS_PageTemplateCategory_CategoryPath]
	ON [CMS_PageTemplateCategory] ([CategoryPath])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
