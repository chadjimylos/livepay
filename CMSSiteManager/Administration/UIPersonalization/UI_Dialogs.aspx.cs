using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;

public partial class CMSSiteManager_Administration_UIPersonalization_UI_Dialogs : CMSUIPersonalizationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.editElem.SiteID =  this.GetSiteID(QueryHelper.GetString("siteid", ""));
        this.editElem.IsLiveSite = false;
    }
}
