using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Ecommerce;
using CMS.TreeEngine;
using CMS.SettingsProvider;
using CMS.WorkflowEngine;
using CMS.PortalEngine;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Ecommerce_Content_Product_Product_Selection : CMSContentPage
{
    protected int nodeId = 0;
    TreeNode node = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'EcommerceRead' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceRead"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceRead");
        }

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Product"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Product");
        }

        nodeId = ValidationHelper.GetInteger(Request.QueryString["nodeid"], 0);

        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, tree);
        if (node != null)
        {
            // Check read permissions
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
            }
            else
            {
                // Check modify permissions
                if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                {
                    // disable form editing                                                            
                    DisableFormEditing();

                    // show access denied message
                    lblGlobalInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                }

                CMSContext.ViewMode = ViewModeEnum.Product;

                ctrlProduct.NodeID = nodeId;

                if (node.NodeSKUID > 0)
                {
                    // Get the product
                    SKUInfo si = SKUInfoProvider.GetSKUInfo(node.NodeSKUID);
                    if (si != null)
                    {
                        UrlHelper.Redirect("Product_Edit_General.aspx?productid=" + node.NodeSKUID + "&nodeid=" + nodeId);
                    }
                }

                chkMarkDocAsProd.Text = ResHelper.GetString("Product_Selection.MarkDocAsProd");
                radSelect.Text = ResHelper.GetString("Product_Selection.SelectProduct");
                radCreate.Text = ResHelper.GetString("Product_Selection.CreateProduct");
                btnOk.Text = ResHelper.GetString("General.Ok");

                ctrlProduct.ShowOkButton = false;

                if (ctrlProduct.ProductID > 0)
                {
                    pnlNew.Visible = true;
                    pnlSelect.Visible = false;
                    ctrlProduct.Visible = true;
                    radCreate.Visible = false;
                    radSelect.Visible = false;
                    chkMarkDocAsProd.Text = ResHelper.GetString("com_SKU_edit_general.DocumentIsProduct");
                    return;
                }

                if (!RequestHelper.IsPostBack())
                {
                    chkMarkDocAsProd.Checked = false;
                    radSelect.Checked = true;
                    radCreate.Checked = false;
                    pnlSelect.Enabled = true;
                    pnlContent.Enabled = false;
                }
            }
        }
    }


    protected void chkMarkDocAsProd_CheckedChanged(object sender, EventArgs e)
    {
        if (ctrlProduct.ProductID > 0)
        {
            if (!chkMarkDocAsProd.Checked)
            {
                pnlNew.Enabled = false;
            }
            else
            {
                pnlNew.Enabled = true;
            }
        }
        else
        {
            if (chkMarkDocAsProd.Checked)
            {
                pnlContent.Enabled = true;
            }
            else
            {
                pnlContent.Enabled = false;
            }
        }
    }


    protected void radCreate_CheckedChanged(object sender, EventArgs e)
    {
        CheckedChanged();
    }


    protected void radSelect_CheckedChanged(object sender, EventArgs e)
    {
        CheckedChanged();
    }


    private void CheckedChanged()
    {
        if (radSelect.Checked)
        {
            pnlSelect.Enabled = true;
            pnlSelect.Visible = true;
            ctrlProduct.Visible = false;
            pnlNew.Visible = false;
        }
        else
        {
            pnlSelect.Enabled = false;
            pnlSelect.Visible = false;
            ctrlProduct.Visible = true;
            pnlNew.Visible = true;

            // Set mappings
            if (node != null)
            {
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(node.NodeClassName);
                if (dci != null)
                {
                    ctrlProduct.ProductName = ValidationHelper.GetString(DataHelper.GetDataRowValue(node.DataRow, Convert.ToString(dci.SKUMappings["SKUName"])), "");
                    ctrlProduct.ProductPrice = ValidationHelper.GetDouble(DataHelper.GetDataRowValue(node.DataRow, Convert.ToString(dci.SKUMappings["SKUPrice"])), 0);
                    ctrlProduct.ProductWeight = ValidationHelper.GetDouble(DataHelper.GetDataRowValue(node.DataRow, Convert.ToString(dci.SKUMappings["SKUWeight"])), 0);
                    ctrlProduct.ProductHeight = ValidationHelper.GetDouble(DataHelper.GetDataRowValue(node.DataRow, Convert.ToString(dci.SKUMappings["SKUHeight"])), 0);
                    ctrlProduct.ProductWidth = ValidationHelper.GetDouble(DataHelper.GetDataRowValue(node.DataRow, Convert.ToString(dci.SKUMappings["SKUWidth"])), 0);
                    ctrlProduct.ProductDepth = ValidationHelper.GetDouble(DataHelper.GetDataRowValue(node.DataRow, Convert.ToString(dci.SKUMappings["SKUDepth"])), 0);
                    ctrlProduct.ProductDescription = ValidationHelper.GetString(DataHelper.GetDataRowValue(node.DataRow, Convert.ToString(dci.SKUMappings["SKUDescription"])), "");

                    Guid guid = ValidationHelper.GetGuid(DataHelper.GetDataRowValue(node.DataRow, Convert.ToString(dci.SKUMappings["SKUImagePath"])), Guid.Empty);
                    ctrlProduct.ProductImagePath = (guid != Guid.Empty) ? TreePathUtils.GetAttachmentUrl(guid, node.NodeAlias) : "";
                }
            }
            // prefill form with values from the document
            ctrlProduct.SetValues();
        }
    }


    protected void btnOk_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        if (ctrlProduct.ProductID > 0)
        {
            //Update
            if (chkMarkDocAsProd.Checked)
            {
                int mProductId = ctrlProduct.OnBtnOkClick();

                if (mProductId > 0)
                {
                    UrlHelper.Redirect("Product_Selection.aspx?productID=" + mProductId + "&saved=1&nodeid=" + nodeId);
                }
            }
            else //DELETE
            {
                TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                node = tree.SelectSingleNode(nodeId, TreeProvider.ALL_CULTURES);
                node.NodeSKUID = 0;
                DocumentHelper.UpdateDocument(node, tree);

                UrlHelper.Redirect("Product_Selection.aspx?nodeid=" + nodeId);
            }
        }
        else
        {
            if (radSelect.Checked)
            {
                if (skuElem.SKUID > 0)
                {
                    int productId = skuElem.SKUID;
                    TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                    node = tree.SelectSingleNode(nodeId, TreeProvider.ALL_CULTURES);
                    node.NodeSKUID = productId;
                    DocumentHelper.UpdateDocument(node, tree);

                    UrlHelper.Redirect("Product_Edit_General.aspx?productid=" + productId + "&nodeId=" + nodeId);
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Products.EmptyList");
                }
            }
            else
            {
                int mProductId = ctrlProduct.OnBtnOkClick();

                if (mProductId > 0)
                {
                    UrlHelper.Redirect("Product_Selection.aspx?productID=" + mProductId + "&saved=1&nodeid=" + nodeId);
                }
            }
        }
    }


    /// <summary>
    /// Disables form editing.
    /// </summary>
    protected void DisableFormEditing()
    {
        chkMarkDocAsProd.Enabled = false;
        radCreate.Enabled = false;
        radSelect.Enabled = false;
        btnOk.Enabled = false;
        skuElem.Enabled = false;
        ctrlProduct.FormEnabled = false;
    }
}
