using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_BannedIP_Tools_BannedIP_Edit : CMSBannedIPsPage
{
    protected int itemid = 0;
    private int siteId = 0;
    private int selectedSiteId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get siteId from query parameter
        siteId = QueryHelper.GetInteger("siteId", 0);
        selectedSiteId = QueryHelper.GetInteger("selectedsiteId", 0);

        // Controls initialization
        radBanIP.Text = ResHelper.GetString("banip.radBanIP");
        radAllowIP.Text = ResHelper.GetString("banip.radAllowIP");
        lblIPAddressBanType.Text = ResHelper.GetString("banip.IPAddressBanType") + ResHelper.Colon;
        lblIPAddressBanEnabled.Text = ResHelper.GetString("general.enabled") + ResHelper.Colon;
        lblIPAddress.Text = ResHelper.GetString("banip.IPAddress") + ResHelper.Colon;
        lblIPAddressBanReason.Text = ResHelper.GetString("banip.IPAddressBanReason") + ResHelper.Colon;
        btnOk.Text = ResHelper.GetString("General.OK");
        rfvIPAddress.ErrorMessage = ResHelper.GetString("banip.IPAddressEmpty");
        lblIPAddressAllowOverride.Text = ResHelper.GetString("banip.IPAddressAllowOverride") + ResHelper.Colon;


        if (!RequestHelper.IsPostBack())
        {
            // Add list items to ban type drop down list
            DataHelper.FillListControlWithEnum(typeof(BanControlEnum), drpIPAddressBanType, "banip.bantype", BannedIPInfoProvider.BanControlEnumString);

            drpIPAddressBanType.SelectedValue = BannedIPInfoProvider.BanControlEnumString(BanControlEnum.AllNonComplete);
        }

        string currentBannedIP = ResHelper.GetString("banip.NewItemCaption");

        // get bannedIP id from querystring		
        itemid = QueryHelper.GetInteger("itemid", 0);
        if (itemid > 0)
        {
            BannedIPInfo bannedIPObj = BannedIPInfoProvider.GetBannedIPInfo(itemid);
            if (bannedIPObj != null)
            {
                //Check whether the item truly belogs to specified site
                if (((siteId > 0) && (bannedIPObj.IPAddressSiteID != siteId)) ||
                    ((selectedSiteId > 0) && (bannedIPObj.IPAddressSiteID != selectedSiteId)))
                {
                    RedirectToAccessDenied(ResHelper.GetString("banip.invaliditem"));
                }

                currentBannedIP = bannedIPObj.IPAddress;

                // Add site info to breadcrumbs in sitemanager
                if (siteId == 0)
                {
                    if (bannedIPObj.IPAddressSiteID == 0)
                    {
                        currentBannedIP += " (global)";
                        radAllowIP.Text = ResHelper.GetString("banip.radAllowIPglobal");

                        plcIPOveride.Visible = true;
                    }
                    else
                    {
                        SiteInfo si = SiteInfoProvider.GetSiteInfo(bannedIPObj.IPAddressSiteID);
                        if (si != null)
                        {
                            currentBannedIP += " (" + si.DisplayName + ")";
                        }
                    }
                }

                // Fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(bannedIPObj);

                    // show that the bannedIP was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
        }

        // initializes page title control		
        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = ResHelper.GetString("banip.listHeaderCaption");
        breadcrumbs[0, 1] = ResolveUrl("BannedIP_List.aspx?siteId=" + siteId + "&selectedsiteid=" + selectedSiteId);
        breadcrumbs[0, 2] = "";
        breadcrumbs[1, 0] = currentBannedIP;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        // Add info about selected site in Site manager for new item
        if ((siteId == 0) && (itemid == 0))
        {
            if (selectedSiteId > 0)
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(selectedSiteId);
                if (si != null)
                {
                    breadcrumbs[1, 0] += " (" + si.DisplayName + ")";
                }
            }
            else
            {
                breadcrumbs[1, 0] += " (global)";
                radAllowIP.Text = ResHelper.GetString("banip.radAllowIPglobal");

                plcIPOveride.Visible = true;
            }
        }

        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        // Different header and icon if it is new item
        if (itemid > 0)
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("banip.editHeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_BannedIP/object.png");
        }
        else
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("banip.newHeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_BannedIP/new.png");
        }

        this.CurrentMaster.Title.HelpTopicName = "banip_properties";
    }


    /// <summary>
    /// Load data of editing bannedIP.
    /// </summary>
    /// <param name="bannedIPObj">BannedIP object.</param>
    protected void LoadData(BannedIPInfo bannedIPObj)
    {
        // check proper radio button
        if (bannedIPObj.IPAddressAllowed)
        {
            radAllowIP.Checked = true;
        }
        else
        {
            radBanIP.Checked = true;
        }
        drpIPAddressBanType.SelectedValue = bannedIPObj.IPAddressBanType;
        chkIPAddressBanEnabled.Checked = bannedIPObj.IPAddressBanEnabled;
        txtIPAddress.Text = bannedIPObj.IPAddress;
        txtIPAddressBanReason.Text = bannedIPObj.IPAddressBanReason;

        if (siteId == 0)
        {
            chkIPAddressAllowOverride.Checked = bannedIPObj.IPAddressAllowOverride;
        }
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'Modify' permission
        CheckPermissions("Modify");

        string errorMessage = new Validator().NotEmpty(txtIPAddress.Text, ResHelper.GetString("banip.IPAddressEmpty")).Result;

        // Check if regularized ip address doesn't overflow database column
        if (BannedIPInfoProvider.GetRegularIPAddress(txtIPAddress.Text).Length > 200)
        {
            errorMessage = ResHelper.GetString("banip.IPAddressInvalid");
        }

        if (errorMessage == "")
        {
            BannedIPInfo bannedIPObj = BannedIPInfoProvider.GetBannedIPInfo(itemid);

            // if bannedIP doesnt already exist, create new one
            if (bannedIPObj == null)
            {
                bannedIPObj = new BannedIPInfo();
            }

            bannedIPObj.IPAddressAllowed = radAllowIP.Checked;
            bannedIPObj.IPAddressBanType = drpIPAddressBanType.SelectedValue;
            bannedIPObj.IPAddressBanEnabled = chkIPAddressBanEnabled.Checked;
            bannedIPObj.IPAddress = txtIPAddress.Text.Trim();

            // Make sure text is not too long
            if (txtIPAddressBanReason.Text.Length > 450)
            {
                txtIPAddressBanReason.Text = txtIPAddressBanReason.Text.Substring(0, 450);
            }
            bannedIPObj.IPAddressBanReason = txtIPAddressBanReason.Text.Trim();

            if (siteId == 0)
            {
                // For (global) set overriding from checkbox, otherwise is true
                bannedIPObj.IPAddressAllowOverride = (selectedSiteId > 0) || chkIPAddressAllowOverride.Checked;

                // If site selected assign it to banned IP
                if (selectedSiteId > 0)
                {
                    bannedIPObj.IPAddressSiteID = selectedSiteId;
                }
            }
            else
            {
                // default setting for editing from CMSDesk
                bannedIPObj.IPAddressAllowOverride = true;
                bannedIPObj.IPAddressSiteID = siteId;
            }

            BannedIPInfoProvider.SetBannedIPInfo(bannedIPObj);

            UrlHelper.Redirect("Bannedip_Edit.aspx?siteid=" + siteId + "&selectedsiteid=" + selectedSiteId + "&itemid=" + bannedIPObj.IPAddressID + "&saved=1");
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    /// <summary>
    /// Check specifies permission
    /// </summary>
    /// <param name="permissionName">name of permission</param>
    private void CheckPermissions(string permissionName)
    {
        //Check permissions
        if (siteId == 0)
        {
            // Must be global admin for manage all bannedip
            CheckGlobalAdministrator();
        }
        else
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);

            // check 'Read' permission for specified site
            if ((si == null) || (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.bannedip", permissionName, si.SiteName)))
            {
                RedirectToAccessDenied("cms.bannedip", permissionName);
            }
        }
    }
}
