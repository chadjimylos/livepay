CREATE TABLE [CMS_WebPartLayout] (
		[WebPartLayoutID]                        int NOT NULL IDENTITY(1, 1),
		[WebPartLayoutCodeName]                  nvarchar(200) NOT NULL,
		[WebPartLayoutDisplayName]               nvarchar(200) NOT NULL,
		[WebPartLayoutDescription]               nvarchar(max) NULL,
		[WebPartLayoutCode]                      nvarchar(max) NULL,
		[WebPartLayoutCheckedOutFilename]        nvarchar(450) NULL,
		[WebPartLayoutCheckedOutByUserID]        int NULL,
		[WebPartLayoutCheckedOutMachineName]     nvarchar(50) NULL,
		[WebPartLayoutVersionGUID]               nvarchar(50) NULL,
		[WebPartLayoutWebPartID]                 int NOT NULL,
		[WebPartLayoutGUID]                      uniqueidentifier NOT NULL,
		[WebPartLayoutLastModified]              datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WebPartLayout]
	ADD
	CONSTRAINT [PK_CMS_WebPartLayout]
	PRIMARY KEY
	NONCLUSTERED
	([WebPartLayoutID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_WebPartLayout]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WebPartLayout_WebPartLayoutCheckedOutByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_WebPartLayout]
			ADD CONSTRAINT [FK_CMS_WebPartLayout_WebPartLayoutCheckedOutByUserID_CMS_User]
			FOREIGN KEY ([WebPartLayoutCheckedOutByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[CMS_WebPartLayout]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WebPart]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WebPartLayout_WebPartLayoutWebPartID_CMS_WebPart]') IS NULL
BEGIN
		ALTER TABLE [CMS_WebPartLayout]
			ADD CONSTRAINT [FK_CMS_WebPartLayout_WebPartLayoutWebPartID_CMS_WebPart]
			FOREIGN KEY ([WebPartLayoutWebPartID]) REFERENCES [CMS_WebPart] ([WebPartID])
END
CREATE INDEX [IX_CMS_WebPartLayout_WebPartLayoutCheckedOutByUserID]
	ON [CMS_WebPartLayout] ([WebPartLayoutCheckedOutByUserID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_WebPartLayout_WebPartLayoutWebPartID_WebPartLayoutCodeName]
	ON [CMS_WebPartLayout] ([WebPartLayoutWebPartID], [WebPartLayoutCodeName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
