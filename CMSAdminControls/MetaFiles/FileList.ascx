<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileList.ascx.cs" Inherits="CMSAdminControls_MetaFiles_FileList" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<table class="UploadMetaFile">
    <tr>
        <td>
            <asp:Label ID="lblUpload" runat="server" Text="" EnableViewState="false" />
        </td>
        <td>
            <cms:CMSFileUpload ID="uploader" runat="server" /><cms:CMSButton ID="btnUpload" runat="server"
                OnClick="btnUpload_Click" CssClass="ContentButton" EnableViewState="false" />
        </td>
    </tr>
</table>
<br />
<cms:UniGrid runat="server" ID="gridFiles" GridName="~/CMSAdminControls/MetaFiles/FileList.xml"
    OrderBy="MetaFileName" Columns="MetaFileID,MetaFileGUID,MetaFileObjectType,MetaFileObjectID,MetaFileGroupName,MetaFileName,MetaFileExtension,MetaFileSize,MetaFileImageWidth,MetaFileImageHeight" />
<br />
<asp:Label ID="lblError" runat="server" Visible="false" CssClass="ErrorLabel" EnableViewState="false" />
<cms:CMSButton ID="btnHidden" runat="server" CssClass="HiddenButton" EnableViewState="false" />
