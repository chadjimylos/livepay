﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.SiteProvider

Partial Class CMSTemplates_LivePay_UCChangeUserDetails
    Inherits CMSUserControl


#Region "Variables"

    ''' <summary>
    ''' My property
    ''' </summary>
    Private mMyProperty As String = Nothing

#End Region

#Region "Properties"


    ''' <summary>
    ''' Gets or sets value of MyProperty
    ''' </summary>
    Public Property MyProperty() As String
        Get
            Return mMyProperty
        End Get
        Set(ByVal value As String)
            mMyProperty = value
        End Set
    End Property

#End Region

#Region "Methods"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        btnSave.ImageUrl = ResHelper.LocalizeString(String.Concat("{$=/App_Themes/LivePay/ChangeUserDetails/BtnSave.png|en-us=/App_Themes/LivePay/ChangeUserDetails/BtnSave_en-us.png$}"))
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If

        Visible = Session("userName_RequirePasswords") Is Nothing

        If Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            SetErrorMessages()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        txtEmail.Text = CMSContext.CurrentUser.Email
        txtFullName.Text = CMSContext.CurrentUser.FullName
        txtContactPhone.Text = CMSContext.CurrentUser.GetValue("UserPhone")
        SetErrorMessages()
    End Sub

    Private Sub SetErrorMessages()
        Dim dt As DataTable = DBConnection.GetErrorMessages(4)
        Dim JsStringMessages As String = String.Empty
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim ErrorMessageGR As String = Replace(dt.Rows(i)("ErrorMessageGR").ToString, vbCrLf, String.Empty)
            Dim ErrorMessageEN As String = Replace(dt.Rows(i)("ErrorMessageEN").ToString, vbCrLf, String.Empty)
            ErrorMessageGR = Replace(ErrorMessageGR, "'", String.Empty)
            ErrorMessageEN = Replace(ErrorMessageEN, "'", String.Empty)

            Dim ErrorMessageStr As String = ResHelper.LocalizeString(String.Concat("{$=", ErrorMessageGR, "|en-us=", ErrorMessageEN, "$}"))
            ErrorMessageStr = Replace(ErrorMessageStr, "'", "\'")
            JsStringMessages = String.Concat(JsStringMessages, " ", dt.Rows(i)("CodeName").ToString, " = '", ErrorMessageStr, "'; ")
        Next
        JsScript(JsStringMessages)
    End Sub

#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSave.Click
        'Dim ui As UserInfo = UserInfoProvider.GetUserInfo(txtEmail.Text)
        'Dim CurrentUser As String = CMSContext.CurrentUser.UserName

        'Dim IsValidMail As Boolean = True
        'If txtEmail.Text <> CurrentUser Then
        '    If Not IsNothing(ui) Then
        '        IsValidMail = False
        '    End If
        'End If
        Dim user As UserInfo = CMSContext.CurrentUser
        If Not IsNothing(user) Then
            Dim UserName_ As String = user.UserName
            Dim UserEmail As String = user.Email
          
            user.Email = StripHTMLFunctions.StripTags(txtEmail.Text)
            user.FullName = StripHTMLFunctions.StripTags(txtFullName.Text)
            user.SetValue("UserPhone", StripHTMLFunctions.StripTags(txtContactPhone.Text))
            If IsNothing(CMSContext.CurrentUser.GetValue("livepayid")) Then
                If UserName_ = UserEmail Then
                    user.UserName = StripHTMLFunctions.StripTags(txtEmail.Text)
                End If
            Else
                If CMSContext.CurrentUser.GetValue("livepayid").ToString.Length = 0 Then
                    If UserName_ = UserEmail Then
                        user.UserName = StripHTMLFunctions.StripTags(txtEmail.Text)
                    End If
                End If
            End If
                UserInfoProvider.SetUserInfo(user)
                Response.Redirect("UpdateUserDetailsSuccess.aspx")
            End If

    End Sub
    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub
End Class
