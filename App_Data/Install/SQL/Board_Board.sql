CREATE TABLE [Board_Board] (
		[BoardID]                      int NOT NULL IDENTITY(1, 1),
		[BoardName]                    nvarchar(250) NOT NULL,
		[BoardDisplayName]             nvarchar(250) NOT NULL,
		[BoardDescription]             nvarchar(max) NOT NULL,
		[BoardOpened]                  bit NOT NULL,
		[BoardOpenedFrom]              datetime NULL,
		[BoardOpenedTo]                datetime NULL,
		[BoardEnabled]                 bit NOT NULL,
		[BoardAccess]                  int NOT NULL,
		[BoardModerated]               bit NOT NULL,
		[BoardUseCaptcha]              bit NOT NULL,
		[BoardMessages]                int NOT NULL,
		[BoardLastModified]            datetime NOT NULL,
		[BoardGUID]                    uniqueidentifier NOT NULL,
		[BoardDocumentID]              int NOT NULL,
		[BoardUserID]                  int NULL,
		[BoardGroupID]                 int NULL,
		[BoardLastMessageTime]         datetime NULL,
		[BoardLastMessageUserName]     nvarchar(250) NULL,
		[BoardUnsubscriptionURL]       nvarchar(450) NULL,
		[BoardRequireEmails]           bit NULL,
		[BoardSiteID]                  int NOT NULL,
		[BoardEnableSubscriptions]     bit NOT NULL,
		[BoardBaseURL]                 nvarchar(450) NULL
)
ON [PRIMARY]
ALTER TABLE [Board_Board]
	ADD
	CONSTRAINT [PK_Board_Board]
	PRIMARY KEY
	([BoardID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Board_Board]
	ADD
	CONSTRAINT [DEFAULT_Board_Board_BoardEnableSubscriptions]
	DEFAULT ((0)) FOR [BoardEnableSubscriptions]
ALTER TABLE [Board_Board]
	ADD
	CONSTRAINT [DEFAULT_Board_Board_BoardRequireEmails]
	DEFAULT ((0)) FOR [BoardRequireEmails]
IF OBJECT_ID(N'[Board_Board]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Document]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Board_Board_BoardDocumentID_CMS_Document]') IS NULL
BEGIN
		ALTER TABLE [Board_Board]
			ADD CONSTRAINT [FK_Board_Board_BoardDocumentID_CMS_Document]
			FOREIGN KEY ([BoardDocumentID]) REFERENCES [CMS_Document] ([DocumentID])
END
IF OBJECT_ID(N'[Board_Board]') IS NOT NULL
	AND OBJECT_ID(N'[Community_Group]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Board_Board_BoardGroupID_Community_Group]') IS NULL
BEGIN
		ALTER TABLE [Board_Board]
			ADD CONSTRAINT [FK_Board_Board_BoardGroupID_Community_Group]
			FOREIGN KEY ([BoardGroupID]) REFERENCES [Community_Group] ([GroupID])
END
IF OBJECT_ID(N'[Board_Board]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Board_Board_BoardSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Board_Board]
			ADD CONSTRAINT [FK_Board_Board_BoardSiteID_CMS_Site]
			FOREIGN KEY ([BoardSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[Board_Board]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Board_Board_BoardUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Board_Board]
			ADD CONSTRAINT [FK_Board_Board_BoardUserID_CMS_User]
			FOREIGN KEY ([BoardUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE UNIQUE INDEX [IX_Board_Board_BoardDocumentID_BoardName]
	ON [Board_Board] ([BoardDocumentID], [BoardName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Board_Board_BoardGroupID_BoardName]
	ON [Board_Board] ([BoardGroupID], [BoardName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Board_Board_BoardSiteID]
	ON [Board_Board] ([BoardSiteID])
	ON [PRIMARY]
CREATE INDEX [IX_Board_Board_BoardUserID_BoardName]
	ON [Board_Board] ([BoardUserID], [BoardName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
