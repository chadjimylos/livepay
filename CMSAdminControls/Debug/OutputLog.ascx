<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OutputLog.ascx.cs" Inherits="CMSAdminControls_Debug_OutputLog" %>
<div style="<%=mLogStyle%>">
    <asp:Literal runat="server" ID="ltlInfo" EnableViewState="false" />
    <asp:PlaceHolder runat="server" ID="plcLog" EnableViewState="false" />
</div>
