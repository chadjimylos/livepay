﻿<%@ Page Title="Module edit - User interface - Roles" Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    AutoEventWireup="true" CodeFile="Module_UI_Roles.aspx.cs" Inherits="CMSSiteManager_Development_Modules_Module_UI_Roles" Theme="Default" %>

<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/UniControls/UniMatrix.ascx" TagName="UniMatrix" TagPrefix="cms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="plcBeforeBody" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="plcControls" runat="Server">
    <cms:LocalizedLabel ID="lblSites" runat="server" ResourceString="general.site" DisplayColon="true" />
    <cms:SiteSelector ID="siteSelector" runat="server" AllowAll="false" OnlyRunningSites="false" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="plcContent" runat="Server">
    <asp:UpdatePanel ID="pnlUpdate" runat="server">
        <ContentTemplate>
            <cms:LocalizedLabel ID="lblDisabled" runat="server" EnableViewState="false" CssClass="InfoLabel" ResourceString="resource.ui.disabled" />
            <cms:LocalizedLabel ID="lblInfo" runat="server" CssClass="InfoLabel" />
            <asp:PlaceHolder ID="plcUpdate" runat="server">
                <cms:UniMatrix ID="gridMatrix" runat="server" QueryName="CMS.UIElement.getpermissionmatrix"
                    RowItemIDColumn="RoleID" ColumnItemIDColumn="ElementID" RowItemDisplayNameColumn="RoleDisplayName"
                    ColumnItemDisplayNameColumn="ElementDisplayName" RowTooltipColumn="RowDisplayName"
                    ColumnTooltipColumn="PermissionDescription" ItemTooltipColumn="PermissionDescription"
                    FixedWidth="0" FirstColumnsWidth="0" LastColumnFullWidth="true"/>
            </asp:PlaceHolder>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
