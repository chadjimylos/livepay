﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Maps/YahooMaps.ascx.cs"
    Inherits="CMSWebParts_Maps_YahooMaps" %>
<asp:Literal ID="ltlDesign" runat="server" />
<asp:Label runat="server" ID="lblError" Visible="false" />
<asp:Literal runat="server" ID="ltlBefore" EnableViewState="false" />
<cms:CMSRepeater ID="repItems" runat="server" EnableViewState="true" OnItemDataBound="repItems_ItemDataBound" />
<asp:Literal runat="server" ID="ltlAfter" EnableViewState="false" />
