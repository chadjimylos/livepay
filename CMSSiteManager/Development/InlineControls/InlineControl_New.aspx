<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InlineControl_New.aspx.cs"
    Inherits="CMSSiteManager_Development_InlineControls_InlineControl_New" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Inline control - edit" %>
    <%@ Register Src="~/CMSFormControls/System/UserControlSelector.ascx" TagPrefix="cms"
    TagName="FileSystemSelector" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td style="vertical-align: top; padding-top: 5px" class="FieldLabel">
                <asp:Label runat="server" ID="lblControlDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtControlDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" /><br />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtControlDisplayName"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblControlName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtControlName" runat="server" CssClass="TextBoxField" MaxLength="200" /><br />
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtControlName"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblControlFileName" DisplayColon="true" EnableViewState="false" />
            </td>
            <td>
                <cms:FileSystemSelector ID="FileSystemSelector" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblControlParameterName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtControlParameterName" runat="server" CssClass="TextBoxField"
                    MaxLength="200" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblDescription" EnableViewState="false" ResourceString="general.description"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtControlDescription" runat="server" TextMode="MultiLine" CssClass="TextAreaField" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:CheckBox runat="server" ID="chkAssign" Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
