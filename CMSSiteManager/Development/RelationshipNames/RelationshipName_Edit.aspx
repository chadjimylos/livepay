<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RelationshipName_Edit.aspx.cs" Inherits="CMSSiteManager_Development_RelationshipNames_RelationshipName_Edit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Relationship name - edit</title>
</head>
		<frameset border="0" rows="102, *" id="rowsFrameset">
    		<frame name="relationshipNameMenu" src="RelationshipName_Header.aspx?relationshipnameid=<%=Request.QueryString["relationshipnameid"]%> " scrolling="no" frameborder="0" noresize="noresize" />		
		    <frame name="relationshipNameContent" src="RelationshipName_General.aspx?relationshipnameid=<%=Request.QueryString["relationshipnameid"]%>&saved=<%=Request.QueryString["saved"]%>" frameborder="0"  noresize="noresize" />
    	<noframes>
			<body>
			 <p id="p1">
				This HTML frameset displays multiple Web pages. To view this frameset, use a 
				Web browser that supports HTML 4.0 and later.
			 </p>
			</body>
		</noframes>
</frameset>
</html>
