<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Community/Membership/GroupMembers.ascx.cs"
    Inherits="CMSWebParts_Community_Membership_GroupMembers" %>
<%@ Register Src="~/CMSModules/Groups/Controls/Members/Members.ascx" TagName="Members"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/System/PermissionMessage.ascx" TagName="PermissionMessage"
    TagPrefix="cms" %>
<cms:PermissionMessage ID="messageElem" runat="server" Visible="false" EnableViewState="false" />
<cms:Members ID="membersElem" runat="server" />
