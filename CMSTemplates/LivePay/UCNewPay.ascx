﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCNewPay.ascx.vb" Inherits="CMSTemplates_LivePay_UCNewPay" %>
<script language="javascript" type="text/javascript" >
    var event_c;
    var obj_c;
    var IsBigType = false;
    function handle_keydownHome(e) {
        document.getElementById('<%=btnSearch.ClientId %>').focus();
        $('#' + '<%=btnSearch.ClientId %>').click()
    }

    function handle_keydownHome_big(e) {
        document.getElementById('<%=btnSearch_big.ClientId %>').focus();
        $('#' + '<%=btnSearch_big.ClientId %>').click()
    }
   
    $(document).ready(function () {
        $('.SearchBG-big').click(function (event) { SetClick(event) })
        SetGeograficValues()
    });



    function RebindRpt(CachValue, obj) {
//        CachValue = CachValue.replace(/[']/gi, "")
//        setTimeout("ExecSearch('" + CachValue + "','" + obj.id + "')", 500)
        if (CheckAction(event_c, obj_c)) {
            CachValue = CachValue.replace(/[']/gi, "@@@@")
            setTimeout("ExecSearch('" + CachValue + "','" + obj.id + "')", 500)
        } else {
            ControlRows(event_c, obj_c)
        }
    }

    function ExecSearch(CachValue, obj) {
        var RealValue = document.getElementById(obj).value
        RealValue = RealValue.replace(/[']/gi, "@@@@") 

        if (RealValue == CachValue) {
            var text = document.getElementById(obj).value;
            text = text.replace(/[']/gi, "@@@@")
            if (text != null && text.length > 0) {
                GetHomeMerchantData(RealValue, obj)
            } else {
                ShowHideQuickRes(false, 0, obj)
            }
        }
    }

    function GetHomeMerchantData(val, obj) {
      
        var x;
        if (window.ActiveXObject) {
            x = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else {
            x = new XMLHttpRequest();
        }
        val = escape(val)
        //NomosVal AreaVal
        x.open("GET", "/CMSTemplates/LivePay/xmlHttpMerchants.aspx?MerchantKeyWord=" + val + "&RecordsNo=3&FormPage=home&lang=" + '<%=request("lang") %>&nomos=' +  $('input[id$=NomosVal]').val() + '&area=' + $('input[id$=AreaVal]').val() + '&NodeID=' + '<%= iif(string.isnullorempty(request("nodeid")),CMSContext.CurrentDocument.NodeID,request("nodeid")) %>', true);
        
        x.onreadystatechange = function () {
            if (x.readyState == 4) {

                var Results = x.responseText;
              
                var RowCount = Results.split("#]")[0]
                Results = Results.split("#]")[1]
                if (RowCount > 0) {
                    document.getElementById('QuickSrcResAll').innerHTML = Results;
                    document.getElementById('QuickSrcResAll_big').innerHTML = Results;
                   
                    ShowHideQuickRes(true, RowCount, obj);
                } else {
                  
                    ShowHideQuickRes(false, 0, obj);
                }

            }
        }
        x.send(null);
    }

    function SetFocus() {
        var elem = document.getElementById('<%=txtSearch.clientID %>');
        var caretPos = elem.value.length
        if (elem != null) {
            if (elem.createTextRange) {
                var range = elem.createTextRange();
                range.move('character', caretPos);
                range.select();
            }
            else {
                elem.setSelectionRange(caretPos, caretPos);
                elem.focus();
                // Workaround for FF overflow no scroll problem 
                // Trigger a "space" keypress. 
                var evt = document.createEvent("KeyboardEvent");
                evt.initKeyEvent("keypress", true, true, null, false, false, false, false, 0, 32);
                elem.dispatchEvent(evt);
                // Trigger a "backspace" keypress. 
                evt = document.createEvent("KeyboardEvent");
                evt.initKeyEvent("keypress", true, true, null, false, false, false, false, 8, 0);
                elem.dispatchEvent(evt);

            }
        }
    }

    function SetAllResults(val) {
     
        document.getElementById('lnkViewMoreResults_big').href = '/Search.aspx?searchtext=' + val + '&NodeID=' + '<%= iif(string.isnullorempty(request("nodeid")),CMSContext.CurrentDocument.NodeID,request("nodeid")) %>' + '&nomos=' + $('input[id$=NomosVal]').val() + '&area=' + $('input[id$=AreaVal]').val();
    }

    function ShowHideQuickRes(IsVisible, RowCount, obj) {
       
        document.getElementById('ViewMoreResults').style.display = 'none';
        document.getElementById('ViewMoreResults_big').style.display = 'none';

        if (IsVisible) {
//            if (RowCount == 1) {
//                document.getElementById('QuickSrcResMain').className = 'QuickSrcResMain_1'
//                document.getElementById('QuickSrcResMain_Big').className = 'QuickSrcResMain_1'
//            }

//            if (RowCount == 2) {
//                document.getElementById('QuickSrcResMain').className = 'QuickSrcResMain_2'
//                document.getElementById('QuickSrcResMain_Big').className = 'QuickSrcResMain_2'
//            }

//            if (RowCount > 2) {
//                document.getElementById('QuickSrcResMain').className = 'QuickSrcResMain'
//                document.getElementById('QuickSrcResMain_Big').className = 'QuickSrcResMain'
//            }


            document.getElementById('QuickResDiv').style.display = '';
            document.getElementById('QuickResDiv_big').style.display = '';
           
            if (RowCount > 2) {
                document.getElementById('ViewMoreResults').style.display = '';
                document.getElementById('ViewMoreResults_big').style.display = '';
              
                var val = document.getElementById(obj).value
                document.getElementById('lnkViewMoreResults').href = '/Search.aspx?searchtext=' + val //+ '&NodeID=' + '<%= iif(string.isnullorempty(request("nodeid")),CMSContext.CurrentDocument.NodeID,request("nodeid")) %>'

                $('#lnkViewMoreResults_big').click(function () { SetAllResults(val) })
               // document.getElementById('lnkViewMoreResults_big').href = '/Search.aspx?searchtext=' + val + '&NodeID=' + '<%= iif(string.isnullorempty(request("nodeid")),CMSContext.CurrentDocument.NodeID,request("nodeid")) %>' + '&nomos=' + $('input[id$=NomosVal]').val() + '&area=' + $('input[id$=AreaVal]').val();
            }
            ControlRows(event_c, obj_c)
        }
        else {
            document.getElementById('QuickResDiv').style.display = 'none';
            document.getElementById('QuickResDiv_big').style.display = 'none';
        }
    }

    function ReplaceLetters(vals) {
        var LettersToReplace = new Array('έ', 'ύ', 'ί', 'ό', 'ά', 'ή', 'ώ')
        var Letters = new Array('ε', 'υ', 'ι', 'ο', 'α', 'η', 'ω')
        
        $.each(LettersToReplace, function (key, val) {
            vals = vals.replace(RegExp(val, "gi"), Letters[key])
        })

        return vals
    }

    function GetData(obj, MainDivData, ItemHolder, hidfield) {
        $('#' + MainDivData).show()

      
        if (obj.value != '') {
            //--- Clear Childs
            var MainDivHolder = document.getElementById(ItemHolder);
            if (MainDivHolder.hasChildNodes()) {
                while (MainDivHolder.childNodes.length >= 1) {
                    MainDivHolder.removeChild(MainDivHolder.firstChild);
                }
            }

            //--- Clear Childs
            var DistrictID = $('input[id$=NomosVal]').val()
            $.getJSON('/CMSTemplates/LivePay/XmlHttpData.aspx?DataType=' + MainDivData + '&DistrictID=' + DistrictID + '&lang=' + '<%=request("lang") %>', function (data) {
                $.each(data, function (key, val) {
                    var txt = obj.value.toLowerCase()
                    txt = ReplaceLetters(txt)
                    var itemtxt = val.Text.toLowerCase()
                    itemtxt = ReplaceLetters(itemtxt)
                    if (itemtxt.startsWith(txt)) {
                        var ClickCode = "SetTheDllVal('" + MainDivData + "','" + val.Value + "','" + val.Text + "','" + obj.id + "','" + hidfield + "')"
                        $('#' + ItemHolder).append('<div class="QuickSrcResTitle" style="width:200px"><a onclick="' + ClickCode + '" class="Chand">» ' + val.Text + '</a></div><div><img src="/app_themes/LivePay/DDLUnderLine.png"/></div>')
                    }
                });
                $('.scroll-pane').jScrollPane({ scrollbarWidth: 11, scrollbarMargin: 2, dragMinHeight: 10 });

                $('#DistrictResutls .jScrollPaneContainer').css("width", "227px")
                $('#DistrictResutls .jScrollPaneContainer').css("height", "125px")

                $('#AreaResults .jScrollPaneContainer').css("width", "227px")
                $('#AreaResults .jScrollPaneContainer').css("height", "125px")
            });

        } else {
            $('#' + MainDivData).hide()
            OpenCloseDDL(obj.id, MainDivData, ItemHolder, hidfield)
        }
    }

    function OpenCloseDDL(txtSrc,MainDivData, ItemHolder,hidfield) {
        if ($('#' + MainDivData).is(":visible")) {
            $('#' + MainDivData).hide()
        } else {
          
          var txtSrcO = document.getElementById(txtSrc)
           
           txtSrcO.focus()
           txtSrcO.select();
            
            //--- Clear Childs
            var MainDivHolder = document.getElementById(ItemHolder);
            if (MainDivHolder.hasChildNodes()) {
                while (MainDivHolder.childNodes.length >= 1) {
                    MainDivHolder.removeChild(MainDivHolder.firstChild);
                }
            }
            //--- Clear Childs
            var cnt = 0
            var KeyWord = txtSrcO.value;
            var DistrictID = $('input[id$=NomosVal]').val()
            var itest = 0
            $.getJSON('/CMSTemplates/LivePay/XmlHttpData.aspx?DataType=' + MainDivData + '&DistrictID=' + DistrictID + '&lang=' + '<%=request("lang") %>', function (data) {
                $.each(data, function (key, val) {
                    var txt = KeyWord.toLowerCase()
                    txt = ReplaceLetters(txt)
                    var SearchByText = false
                    itest = itest + 1

                    if (hidfield == 'Nomos') {

                        if (txt != '' && txt != '<%=ResHelper.LocalizeString("{$=επιλεξτε νομο|en-us=enter district$}") %>') { // if user enter to the District View by Text
                            SearchByText = true
                        }
                    } else {
                       
                        if (txt != '' && txt != '<%=ResHelper.LocalizeString("{$=επιλεξτε περιοχη|en-us=enter area$}") %>' && $('input[id$=NomosVal]').val() > 0) {// if user enter to the Area and The District is Selected View by Text
                            SearchByText = false
                        }
                    }
                                     
                    if (SearchByText) {
                        var itemtxt = val.Text.toLowerCase()
                        itemtxt = ReplaceLetters(itemtxt)
                        if (itemtxt.startsWith(txt)) {
                            var ClickCode = "SetTheDllVal('" + MainDivData + "','" + val.Value + "','" + val.Text + "','" + txtSrc + "','" + hidfield + "')"
                            $('#' + ItemHolder).append('<div class="QuickSrcResTitle" style="width:200px"><a onclick="' + ClickCode + '" class="Chand">» ' + val.Text + '</a></div><div><img src="/app_themes/LivePay/DDLUnderLine.png"/></div>')
                        }
                    } else {
                        var ClickCode = "SetTheDllVal('" + MainDivData + "','" + val.Value + "','" + val.Text + "','" + txtSrc + "','" + hidfield + "')"
                        $('#' + ItemHolder).append('<div class="QuickSrcResTitle" style="width:200px"><a onclick="' + ClickCode + '" class="Chand">» ' + val.Text + '</a></div><div><img src="/app_themes/LivePay/DDLUnderLine.png"/></div>')
                    }


                });
                $('.scroll-pane').jScrollPane({ scrollbarWidth: 11, scrollbarMargin: 2, dragMinHeight: 10 });
            });

           $('#DistrictResutls .jScrollPaneContainer').css("width", "227px")
           $('#DistrictResutls .jScrollPaneContainer').css("height", "125px")

           $('#AreaResults .jScrollPaneContainer').css("width", "227px")
           $('#AreaResults .jScrollPaneContainer').css("height", "125px")
           $('#' + MainDivData).show()
           

        }
    }

    function SetTheDllVal(MainDivHolder, val, txt, obj, hidfield) {
        $('#' + MainDivHolder).hide()
        $('#' + obj).val(txt)

        $('input[id$=' + hidfield + 'Val]').val(val)
        $('input[id$=' + hidfield + 'Text]').val(txt)

        if (hidfield == 'Nomos') {

            $('input[id$=AreaText]').val('<%=ResHelper.LocalizeString("{$=Επιλέξτε Περιοχή|en-us=Enter Area$}") %>')
            $('input[id$=AreaVal]').val('0')
            $('#AreaResults').hide()
            $('input[id$=txt_Area]').val('<%=ResHelper.LocalizeString("{$=Επιλέξτε Περιοχή|en-us=Enter Area$}") %>')
        }
    }

    function SetClick(e) {
        if (!e) e = window.event;
        if (e.stopPropagation) {
            e.stopPropagation();
        } else {
            e.cancelBubble = true;
        }
    }

    $(document).click(function () {
        DocClick()
    });

    function DocClick() {

        var AllHolders = $('.MainDataDiv').get()

        $.each(AllHolders, function (index, obj) {
            $(obj).hide()
        });



        $('input[id$=txt_Nomos]').val($('input[id$=NomosText]').val())

        $('input[id$=txt_Area]').val($('input[id$=AreaText]').val())
    }

    function SetGeograficValues() {
        var nomos = '<%=request("nomos") %>'
        var area = '<%=request("area") %>'

        var nomostxt = '<%=request("nomostxt") %>'
        var areatxt = '<%=request("areatxt") %>'

        if (nomos != '' && nomostxt!='') {
            $('input[id$=NomosVal]').val(nomos)
            $('input[id$=NomosText]').val(nomostxt)
            $('input[id$=txt_Nomos]').val(nomostxt)
        }

        if (area != '' && areatxt != '') {
            $('input[id$=AreaVal]').val(area)
            $('input[id$=AreaText]').val(areatxt)
            $('input[id$=txt_Area]').val(areatxt)
        }

    }

    function ControlRows(e, obj) {
       
        var keynum = 0;
        var QuickResDiv = document.getElementById('QuickResDiv')
        if (IsBigType)
            QuickResDiv = document.getElementById('QuickResDiv_big')

      
        if (window.event) { keynum = e.keyCode; }  // IE (sucks)
        else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

     
        if (keynum == 38) { // up
            //Move selection up
            if (obj.value.length > 0 && QuickResDiv.style.display != 'none') {

                ChangeSelection(-1)
            }
        }
        if (keynum == 40) { // down
            //Move selection down
           
            if (obj.value.length > 0 && QuickResDiv.style.display != 'none') {
                ChangeSelection(1)
            }
        }
        
        if (keynum == 13) { // enter
           
            if (obj.value.length > 0) {
                var IsSelected = false
                var SelectedObj = null;
                var AllHtml = $('.QuickSrcResTitle').get()

                $.each(AllHtml, function (index, obj) { //-- Check If Menu Is Selected
                    if ($(obj).attr("IsSelected") == 'true') {
                        IsSelected = true
                        SelectedObj = obj
                    }
                });

                if (IsSelected == false) { // if nothing is selected go to search page
                    if (window.event) {
                        if (IsBigType) {
                            return handle_keydownHome_big(event);
                        } else {
                            return handle_keydownHome(event);
                        }
                        
                    }
                    else if (e.which) {
                        if (IsBigType) {
                            return handle_keydownHome_big(e);
                        } else {
                            return handle_keydownHome(e);
                        }
                    }    // Netscape/Firefox/Opera
                } else {
                   
                    var Href = $(SelectedObj).find("a").attr("href")
                    location.href = Href
                }
            }
        }
    }

    function ChangeSelection(GoTo) {

        var LastSelected = -1
        //QuickSrcResTitle
        var IsSelected = false
        var AllHtml;
       
        if (IsBigType) {
            AllHtml = $('#QuickSrcResAll_big .QuickSrcResTitle').get()
        } else {
            AllHtml = $('#QuickSrcResAll .QuickSrcResTitle').get()
        }

          

        $.each(AllHtml, function (index, obj) { //-- Check If Menu Is Selected
          
            if ($(obj).attr("IsSelected") == 'true') {
                IsSelected = true
                LastSelected = index
            }
           
        });
     
        if (IsSelected == false) { //-- If Not Selected Select Firstone
            $.each(AllHtml, function (index, obj) {

                if (index == 0) {
                   
                    $(obj).attr("IsSelected", "true")
                    $(obj).css("background-color", "#d2d6db")
                }
            });
        } else {
            $.each(AllHtml, function (index, obj) {
                var GetNextSelection = LastSelected + GoTo

                if (GetNextSelection > -1 && GetNextSelection < 3) {
                    $(obj).attr("IsSelected", "false")
                    $(obj).css("background-color", "transparent")

                    if (index == GetNextSelection) {
                        $(obj).attr("IsSelected", "true")
                        $(obj).css("background-color", "#d2d6db")
                    }
                }
            });
        }

    }

    function CheckAction(e, obj) {
        var keynum = 0;
        if (window.event) { keynum = e.keyCode; }  // IE (sucks)
        else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

        var IsSearching = true

        if (keynum == 38) { // up
            IsSearching = false
        }

        if (keynum == 40) { // down
            IsSearching = false
        }

        if (keynum == 13) { // enter
            IsSearching = false
        }

        return IsSearching
    }

    function Setent(e) {
        var keynum = 0;
        if (window.event) { keynum = e.keyCode; }  // IE (sucks)
        else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

        if (keynum == 13) {
            return false
        } else {
            return true
        }


    }
   
</script>
<style type="text/css">
    .MainDataDiv{margin-top:5px;position:absolute;width:227px;height:125px;background-image:url('/app_themes/LivePay/bgAreaDDL.png');background-repeat:no-repeat;}
   
</style>
<div style="display:none">
    <input id="NomosVal" runat="server" type="text" value="0" />
    <input id="AreaVal" runat="server" type="text" value="0" />
    <input id="NomosText" runat="server" type="text"/>
    <input id="AreaText" runat="server" type="text" />
</div>
  <div id="BigSearch" runat="server" style="display:none">

<div class="SearchBG-big" >
    <div class="SearchLbl"><%= ResHelper.LocalizeString("{$=Νέα πληρωμή|en-us=New Payment$}")%></div>
    <div class="SearchTxtDiv-big"><asp:TextBox BorderWidth="0" BackColor="Transparent" cssClass="SearchTxt" style="width:440px;" autocomplete="off" ID="txtSearch_big"  onkeydown="return Setent(event)"  onkeyup="IsBigType =  true;event_c = event; obj_c = this;RebindRpt(this.value,this);" runat="server" ></asp:TextBox></div>
    <div class="SearchBTN"><div style="position:absolute"><asp:LinkButton ID="btnSearch_big" runat="server"><img border="0" id="imgSearch_big" runat="server" /></asp:LinkButton></div></div>
    <div class="Clear"></div>
    <div class="QuickSrcResTopMainNewPay-big" id="QuickResDiv_big" style="display:none">
        <div id="QuickSrcResMain_Big">
            <div class="AutoCom-Top">&nbsp;</div>
            <div class="AutoCom-Center">
                <div class="QuickSrcResAll" id="QuickSrcResAll_big">
                </div>
                <div class="QuickSrcResViewMore" id="ViewMoreResults_big" style="display:none">
                    <div class="QuickSrcResTitleUnder"><a href="#" id="lnkViewMoreResults_big">» <%=ResHelper.LocalizeString("{$=Δείτε Περισσότερα Αποτελέσματα|en-us=View more results$}") %></a></div>
                    <div class="QuickSrcResTitleSmall"><%= ResHelper.LocalizeString("{$=Εμφανίζονται τα πρώτα -3- αποτελέσματα|en-us=First - 3 - results en $}")%></div>
                </div> 
            </div> 
            <div class="AutoCom-Bottom">&nbsp;</div>
        </div>
    </div>
    
    <div>
        <div class="SearchLbl" style="width:127px">
        <%= ResHelper.LocalizeString("{$=Αναζήτηση με..|en-us=Search with$}")%>
        </div>
        <div style="float:left;padding-top:10px;">
            <div style="float:left;width:204px;height:30px;background-image:url('/app_themes/LivePay/DDLLeft.png');background-repeat:no-repeat">
                <div style="padding-top:2px;padding-left:3px;">
                <input autocomplete="off" runat="server" type="text" id="txt_Nomos" onclick="this.focus();this.select()" class="SearchTxt ddl" style="width:190px;background:transparent;border:0px" onkeyup="GetData(this,'DistrictResutls','DistrictHolder','Nomos')" />
                </div>
                <div class="MainDataDiv" style="display:none;" id="DistrictResutls">
                    <div id="DistrictHolder" class="scroll-pane" style="padding-left:5px;overflow:auto;height:119px;padding-top:5px"></div>
                </div>
            </div>
            <div style="float:left;width:23px;height:30px;">
                <img src="/app_themes/LivePay/DDLRight.png" onclick="OpenCloseDDL('<%=txt_Nomos.ClientID %>','DistrictResutls','DistrictHolder','Nomos')" style="cursor:pointer" />
            </div>
            <div style="float:left;width:204px;height:30px;background-image:url('/app_themes/LivePay/DDLLeft.png');background-repeat:no-repeat;margin-left:10px">
                <div style="padding-top:2px;padding-left:3px;"><input autocomplete="off" runat="server" type="text" id="txt_Area" onclick="this.focus();this.select()" value="Choose" class="SearchTxt" style="width:190px;background:transparent;border:0px" onkeyup="GetData(this,'AreaResults','AreaHolder','Area')"  /></div>
                
                <div class="MainDataDiv" style="display:none;" id="AreaResults">
                    <div id="AreaHolder" class="scroll-pane" style="padding-left:5px;overflow:auto;height:119px;padding-top:5px"></div>
                </div>
            </div>
            <div style="float:left;width:23px;height:30px;">
                <img src="/app_themes/LivePay/DDLRight.png" onclick="OpenCloseDDL('<%=txt_Area.ClientID %>','AreaResults','AreaHolder','Area')" style="cursor:pointer" />
            </div>
            <div class="Clear"></div>
        </div>
        <div class="Clear"></div>
    </div>
  </div>
    </div> 
  <div id="SimpleSearch" runat="server" style="display:none">
<div class="SearchBG" >
    <div class="SearchLbl"><%= ResHelper.LocalizeString("{$=Νέα πληρωμή|en-us=New Payment$}")%></div>
    <div class="SearchTxtDiv"><asp:TextBox BorderWidth="0" BackColor="Transparent" cssClass="SearchTxt" autocomplete="off" ID="txtSearch" onkeydown="return Setent(event)"  onkeyup="IsBigType = false;event_c = event; obj_c = this;RebindRpt(this.value,this);" runat="server" ></asp:TextBox></div>
    <div class="SearchBTN"><asp:ImageButton runat="server" ID="btnSearch" /></div>
    <div class="Clear"></div>
    <div class="QuickSrcResTopMainNewPay" id="QuickResDiv" style="display:none">
        <div id="QuickSrcResMain">
            <div class="AutoCom-Top">&nbsp;</div>
            <div class="AutoCom-Center">
                <div class="QuickSrcResAll" id="QuickSrcResAll">
           
                </div>
                <div class="QuickSrcResViewMore" id="ViewMoreResults" style="display:none">
                    <div class="QuickSrcResTitleUnder"><a href="#" id="lnkViewMoreResults">» <%=ResHelper.LocalizeString("{$=Δείτε Περισσότερα Αποτελέσματα|en-us=View more results$}") %></a></div>
                    <div class="QuickSrcResTitleSmall"><%= ResHelper.LocalizeString("{$=Εμφανίζονται τα πρώτα -3- αποτελέσματα|en-us=First - 3 - results en $}")%></div>
                </div> 
            </div> 
            <div class="AutoCom-Bottom">&nbsp;</div>
        </div>
    </div>
  </div>
  </div>
