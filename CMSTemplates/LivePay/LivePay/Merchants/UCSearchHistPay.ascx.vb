﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports LivePay_ESBBridge
Imports CMS.GlobalHelper
Imports CMS.UIControls

Imports CMS.SiteProvider
Imports CMS.CMSHelper
Partial Class CMSTemplates_LivePay_Merchants_UCSearchHistPay
    Inherits CMSUserControl
#Region "Variables"

    ''' <summary>
    ''' My property
    ''' </summary>
    Private mMyProperty As String = Nothing

#End Region

#Region "Properties"


    ''' <summary>
    ''' Gets or sets value of MyProperty
    ''' </summary>
    Public Property MyProperty() As String
        Get
            Return mMyProperty
        End Get
        Set(ByVal value As String)
            mMyProperty = value
        End Set
    End Property

#End Region



#Region "Methods"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        If Request.QueryString("T") <> "" Then
            Dim w As New Bridge
        End If

        If Not StopProcessing AndAlso (Not Page.IsPostBack()) Then
            ReloadData()
            SetDatePickers()
            If Not UserSeesFullVersion() Then BindStatistics()
        End If

        If Not StopProcessing AndAlso (Page.IsPostBack()) Then
            SetDatePickers()
        End If

        SetDatePickers()

        If UserSeesFullVersion() Then
            phAdmin.Visible = True
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
        'GRID
        '1:batchid,2:batchDate,3:ΑΝ BATCHCLOSED=true -> ΚΛΕΙΣΤΟ ELSE ΑΝΟΙΧΤΟ,4:TransactionID,5:transactiondDte,6:?,7:transactioAamount,8:transactionType,9:transactionstatus
    End Sub

    Public Overloads Sub ReloadData()
        If drpTxnResult.Items.Count > 0 Then Exit Sub

        drpTxnResult.Items.Clear()
        drpTxnResult.Items.Add(New ListItem("Όλες", -1))

        Dim ResultType As TxnResultEnum
        For Each ResultType In [Enum].GetValues(GetType(TxnResultEnum))
            Dim strMsgType As String = ResultType.ToString()
            drpTxnResult.Items.Add(New ListItem(strMsgType, Integer.Parse(ResultType)))
        Next

        drpTxnType.Items.Clear()
        drpTxnType.Items.Add(New ListItem("Όλες", -1))

        Dim TransactionType As TxnTypeEnum
        For Each TransactionType In [Enum].GetValues(GetType(TxnTypeEnum))
            Dim strMsgType As String = TransactionType.ToString()
            drpTxnType.Items.Add(New ListItem(strMsgType, Integer.Parse(TransactionType)))
        Next

        'Bind()
    End Sub

    Sub BindStatistics()
        Dim w As New LivePay_ESBBridge.Bridge
        Dim merchID As String = CMSContext.CurrentUser.GetValue("LivePayID")
        Dim obj As GetTransactionsObj = Get_TransactionsObject()

        Dim stats As MerchantStatisticsResponse = w.GetMerchantStatistics(obj) 'MerchantID

        If obj IsNot Nothing AndAlso stats IsNot Nothing Then
            litTotalTxn.Text = stats.MerchantStatistics.totalRefundsCount + stats.MerchantStatistics.totalSalesCount
            litTotalSales.Text = stats.MerchantStatistics.totalSalesCount
            litTotalRefunds.Text = stats.MerchantStatistics.totalRefundsCount
            litValueTxn.Text = FormatAmount(stats.MerchantStatistics.totalRefundsValue + stats.MerchantStatistics.totalSalesValue)
            litValueSales.Text = FormatAmount(stats.MerchantStatistics.totalSalesValue)
            litValueRefunds.Text = FormatAmount(stats.MerchantStatistics.totalRefundsValue)
        Else
            litTotalTxn.Text = String.Empty
            litTotalSales.Text = String.Empty
            litTotalRefunds.Text = String.Empty
            litValueTxn.Text = String.Empty
            litValueSales.Text = String.Empty
            litValueRefunds.Text = String.Empty
        End If
    End Sub

    Function FormatAmount(ByVal val As String) As String
        Dim dblVal As Double = Double.Parse(val)
        Return dblVal.ToString("c")
    End Function

    Function UserSeesFullVersion() As Boolean
        Return CMSContext.CurrentUser.IsInRole("LivePayMerchantsAdministrator", "LivePay") Or CMSContext.CurrentUser.IsInRole("LivePayContentAdministrator", "LivePay") Or CMSContext.CurrentUser.IsGlobalAdministrator
    End Function

    Private Function Get_TransactionsObject() As GetTransactionsObj
        Dim obj As New GetTransactionsObj

        'obj.BatchDateFrom   'ΑΝ ΔΕ ΣΥΜΠΛΗΡΩΣΕΙ ΗΜ/ΝΙΑ, ΤΟΤΕ ΠΕΡΝΑΣ DateTime.MinValue
        'obj.BatchDateTo 'ΑΝ ΔΕ ΣΥΜΠΛΗΡΩΣΕΙ ΗΜ/ΝΙΑ, ΤΟΤΕ ΠΕΡΝΑΣ DateTime.MinValue
        'obj.TransactionResult 'ΚΑΤΑΣΤΑΣΗ ΣΥΝΑΛΛΑΓΗΣ
        'obj.TransactionType= 'ΤΥΠΟΣ ΣΥΝΑΛΛΑΓΗΣ, AN EPILEKSEI "OLES" PERNAS -1
        'obj.CustomerName
        'obj.AmountFrom 'DOUBLE, AN DEN EXEI BALEI BAZEIS -1
        'obj.amountTo 'DOUBLE, AN DEN EXEI BALEI BAZEIS -1
        'obj.BatchIdFrom 'ΑΡΙΘΜΟΣ ΠΑΚΕΤΟΥ, INT, AN DEN EXEI BALEI BAZEIS -1
        'obj.BatchIdTo 'ΑΡΙΘΜΟΣ ΠΑΚΕΤΟΥ, INT, AN DEN EXEI BALEI BAZEIS -1
        'obj.BatchClosed 'ΑΝ ΑΝΟΙΧΤΟ ΠΑΚΕΤΟ -> FALSE, ELSE TRUE

        If txtFrom.Text.Length > 0 Then obj.BatchDateFrom = New Date(txtFrom.Text.Split("/")(2), txtFrom.Text.Split("/")(1), txtFrom.Text.Split("/")(0), 0, 0, 0, 0, DateTimeKind.Local)
        If txtTo.Text.Length > 0 Then obj.BatchDateTo = New Date(txtTo.Text.Split("/")(2), txtTo.Text.Split("/")(1), txtTo.Text.Split("/")(0), 23, 59, 59, 0, DateTimeKind.Local)
        If txtAmountFrom.Text.Length > 0 Then
            obj.AmountFrom = StripHTMLFunctions.StripTags(txtAmountFrom.Text.Replace(".", ","))
        Else
            obj.AmountFrom = 1
        End If
        If txtAmountTo.Text.Length > 0 Then
            obj.amountTo = StripHTMLFunctions.StripTags(txtAmountTo.Text.Replace(".", ","))
        Else
            obj.amountTo = 100000
        End If

        OpenBatchDIVHeader.Visible = (rbtTrancType.SelectedValue = 1)
        AllTransactionDIVHeader.Visible = Not OpenBatchDIVHeader.Visible
        OpenBatchDIVMainHeader.Visible = OpenBatchDIVHeader.Visible
        AllTransactionDIVMainHeader.Visible = AllTransactionDIVHeader.Visible

        If rbtTrancType.SelectedValue = "1" Then
            obj.BatchClosedSpecified = True
            obj.BatchClosedSpecified1 = True
            obj.BatchClosed = False
        End If

        If TxtFullName.Text.Length > 0 Then obj.CustomerName = StripHTMLFunctions.StripTags(TxtFullName.Text)
        If txtBatchFrom.Text.Length > 0 Then obj.BatchIdFrom = StripHTMLFunctions.StripTags(txtBatchFrom.Text)
        If txtBatchTo.Text.Length > 0 Then obj.BatchIdTo = StripHTMLFunctions.StripTags(txtBatchTo.Text)

        If drpTxnType.SelectedValue > -1 Then
            Dim ER?(0) As LivePay_ESBBridge.TxnTypeEnum
            ER(0) = drpTxnType.SelectedValue
            obj.TransactionTypeSpecified1 = True
        Else
            Dim ER?(3) As LivePay_ESBBridge.TxnTypeEnum
            ER(0) = LivePay_ESBBridge.TxnTypeEnum.Cancellation_Refund
            ER(1) = LivePay_ESBBridge.TxnTypeEnum.Cancellation_Sale
            ER(2) = LivePay_ESBBridge.TxnTypeEnum.Refund
            ER(3) = LivePay_ESBBridge.TxnTypeEnum.Sale
            obj.TransactionTypeSpecified1 = True
        End If


        If rbtTrancType.SelectedValue = 1 Then  'ΑΝΟΙΧΤΟ ΠΑΚΕΤΟ
            If drpTxnResult.SelectedValue > -1 Then
                Dim TR?(0) As LivePay_ESBBridge.TxnResultEnum
                TR(0) = drpTxnResult.SelectedValue
                obj.TransactionResult = TR
                obj.TransactionResultSpecified1 = True
                lblDebug.Text = "<font color=""#f7f7f7"">PASSING SELECTED DROPDOWN VALUE</font>"
            Else
                Dim TR?(0) As LivePay_ESBBridge.TxnResultEnum
                TR(0) = LivePay_ESBBridge.TxnResultEnum.Successful
                obj.TransactionResult = TR
                obj.TransactionResultSpecified1 = True
                lblDebug.Text = "<font color=""#f7f7f7"">PASSING VALUE 'SUCCESSFUL'</font>"
            End If
        Else 'OLES OI SYNALLAGES
            If txtBatchFrom.Text.Length > 0 Or txtBatchTo.Text.Length > 0 Or txtFrom.Text.Length > 0 Or txtTo.Text.Length > 0 Then  'KLEISTO PAKETO
                Dim TR?(1) As LivePay_ESBBridge.TxnResultEnum
                TR(0) = LivePay_ESBBridge.TxnResultEnum.Successful_Uploaded
                TR(1) = LivePay_ESBBridge.TxnResultEnum.Voided_Uploaded
                obj.TransactionResult = TR
                obj.TransactionResultSpecified1 = True
                lblDebug.Text = "<font color=""#f7f7f7"">PASSING VALUES 'SUCCESSFUL_UPLOADED,VOIDED_UPLOADED'</font>"
            Else
                If drpTxnResult.SelectedValue > -1 Then
                    Dim TR?(0) As LivePay_ESBBridge.TxnResultEnum
                    TR(0) = drpTxnResult.SelectedValue
                    obj.TransactionResult = TR
                    obj.TransactionResultSpecified1 = True
                    lblDebug.Text = "<font color=""#f7f7f7"">PASSING  SELECTED DROPDOWN VALUE</font>"
                Else
                    Dim TR?(5) As LivePay_ESBBridge.TxnResultEnum
                    TR(0) = LivePay_ESBBridge.TxnResultEnum.Failed
                    TR(1) = LivePay_ESBBridge.TxnResultEnum.Reversal
                    TR(2) = LivePay_ESBBridge.TxnResultEnum.Successful
                    TR(3) = LivePay_ESBBridge.TxnResultEnum.Successful_Uploaded
                    TR(4) = LivePay_ESBBridge.TxnResultEnum.Voided
                    TR(5) = LivePay_ESBBridge.TxnResultEnum.Voided_Uploaded
                    obj.TransactionResult = TR
                    obj.TransactionResultSpecified1 = True
                    lblDebug.Text = "<font color=""#f7f7f7"">PASSING  VALUES FAILED,REVERSAL,SUCCESSFUL,SUCCESSFUL_UPLOADED,VOIDED,VOIDED_UPLOADED</font>"
                End If
            End If
        End If

        If UserSeesFullVersion() Then
            If txtMerchantID.Text.Length > 0 Then
                obj.MerchantId = StripHTMLFunctions.StripTags(txtMerchantID.Text)
            ElseIf txtEmail.Text.Length > 0 AndAlso UserInfoProvider.GetUserInfo(StripHTMLFunctions.StripTags(txtEmail.Text)) IsNot Nothing Then
                Dim usr As UserInfo = UserInfoProvider.GetUserInfo(StripHTMLFunctions.StripTags(txtEmail.Text))
                obj.CustomerId = usr.UserID
                obj.MerchantId = ""
            Else
                lblError.Text = "Μή έγκυρο MerchantID ή Email χρήστη"
                Exit Function
            End If
        Else
            obj.MerchantId = CMSContext.CurrentUser.GetValue("LivePayID")
        End If

        Return obj

    End Function

    Private Sub Bind()
        BindStatistics()

        Dim w As New LivePay_ESBBridge.Bridge

        Dim obj As GetTransactionsObj = Get_TransactionsObject()

        If obj IsNot Nothing Then
            Dim currentPage As Integer = 1
            If HiddenPage.Text.Length > 0 Then currentPage = HiddenPage.Text

            obj.PageNumber = currentPage - 1

            Dim pageSize As Integer = ddlRecordsPerPage.SelectedValue
            obj.PageSize = pageSize
            GridViewPayments.PageSize = pageSize

            Dim resp As GetTransactionsResponse
            Try
                resp = w.GetTransactions(obj)
            Catch ex As Exception
                lblError.Text = "ESB Error: " & ex.ToString
                Exit Sub
            End Try

            If resp.ErrorMessage IsNot Nothing AndAlso resp.ErrorMessage.Length > 0 Then
                lblError.Text = "ESB Error: " & resp.ErrorMessage.ToString
                Exit Sub
            End If

            GridViewPayments.DataSource = resp.Transactions
            GridViewPayments.DataBind()


            ' Session("Merchants_GetSrcHisPayDataTable") = resp.Transactions
            'PAGING
            Dim records As Integer = resp.TotalRecords

            Dim pagerRow As GridViewRow = GridViewPayments.BottomPagerRow
            If pagerRow IsNot Nothing Then pagerRow.Visible = True
            If records > pageSize Then
                Dim lastPage As Integer = Math.Floor(records \ pageSize)
                If records Mod pageSize > 0 Then lastPage += 1
                Dim totalpages As Integer = lastPage 'HOLD A VALUE FOR THE TOTAL RECORDS AVAILABLE

                Dim GetPageLinksIndex As HtmlGenericControl = DirectCast(pagerRow.Cells(0).FindControl("lblPageIndex"), HtmlGenericControl)

                Dim startPage As Integer = currentPage

                If totalpages <= 5 Then
                    startPage = 1
                    lastPage = totalpages
                Else
                    If startPage > 1 Then startPage = currentPage - 1
                    If lastPage > startPage + 4 Then lastPage = startPage + 4
                End If

                CreateGridPagerLinks(startPage, lastPage, currentPage, GetPageLinksIndex)
                pagerRow.Cells(0).FindControl("lnkPrev").Visible = (currentPage > 1)
                pagerRow.Cells(0).FindControl("lnkNext").Visible = (currentPage < lastPage)
            ElseIf pagerRow IsNot Nothing Then
                pagerRow.Cells(0).FindControl("lnkPrev").Visible = False
                pagerRow.Cells(0).FindControl("lnkNext").Visible = False
            End If
        Else
            GridViewPayments.DataSource = New DataTable
            GridViewPayments.DataBind()
        End If


        

    End Sub


    Private Sub BindForExrpot()
        Dim w As New LivePay_ESBBridge.Bridge

        Dim obj As GetTransactionsObj = Get_TransactionsObject()
        obj.PageNumber = 0
        obj.PageSize = 200

        Dim resp As GetTransactionsResponse
        Try
            resp = w.GetTransactions(obj)
        Catch ex As Exception
            lblError.Text = "ESB Error: " & ex.ToString
            Exit Sub
        End Try

        If resp.ErrorMessage IsNot Nothing AndAlso resp.ErrorMessage.Length > 0 Then
            lblError.Text = "ESB Error: " & resp.ErrorMessage.ToString
            Exit Sub
        End If
        Session("Merchants_GetSrcHisPayDataTable") = resp.Transactions
        JsScript(" ExportFile('ExportToPDF.aspx?ExportType=MerchantsSrcHisPay&lang=" & CMSContext.CurrentDocumentCulture.CultureCode & "'); ")
    End Sub

    Sub GridViewPayments_DataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridViewPayments.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim amount As String = CType(e.Row.FindControl("hidAmount"), HiddenField).Value.ToString
            Dim IsBatchClosed As Boolean = DirectCast(e.Row.FindControl("lblBatchClosed"), Label).Text
            Dim TransactionStatus As String = DirectCast(e.Row.FindControl("lblTransactionStatus"), Label).Text
            Dim lblCloseDtm As Label = DirectCast(e.Row.FindControl("lblCloseDtm"), Label)
            Dim lblTransDtm As Label = DirectCast(e.Row.FindControl("lblTransDtm"), Label)

            DirectCast(e.Row.FindControl("openCancel"), LinkButton).Visible = (IsBatchClosed = False AndAlso TransactionStatus.ToString.ToLower.Contains("successful"))
            DirectCast(e.Row.FindControl("openRefund"), LinkButton).Visible = (TransactionStatus.ToString.ToLower.Contains("successful"))

            If amount.Split(",").Length > 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
            ElseIf amount.Split(",").Length = 1 Then
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount & ",00"
            Else
                CType(e.Row.FindControl("lblAmount"), Label).Text = amount
            End If

            Dim openPopupa As LinkButton = DirectCast(e.Row.FindControl("openPopup"), LinkButton)
            If TransactionStatus.ToString.ToLower.Contains("voided") Then
                openPopupa.CommandName = "canceltype"
            Else
                openPopupa.CommandName = "otherType"
            End If


            If lblCloseDtm.Text.Contains("/0001") Then
                lblCloseDtm.Text = "-"
            End If
            If lblTransDtm.Text.Contains("/0001") Then
                lblTransDtm.Text = "-"
            Else
                lblTransDtm.Text = Split(lblTransDtm.Text, " ")(0) & "<br>" & Split(lblTransDtm.Text, " ")(1)
            End If

        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim imgBtn As ImageButton = DirectCast(e.Row.FindControl("imgCloseBatch"), ImageButton)
            If rbtTrancType.SelectedValue = "1" Then
                imgBtn.Visible = True
            Else
                imgBtn.Visible = False
            End If
        End If
    End Sub

#Region " Shorting "

    Private Sub sadasd()

    End Sub



#End Region
#Region " Paging "

    Protected Sub GvNews_PageIndexChanged(ByVal sender As Object, ByVal e As GridViewPageEventArgs) Handles GridViewPayments.PageIndexChanging
        GridViewPayments.PageIndex = e.NewPageIndex
        Bind()
    End Sub

    Protected Sub GvNews_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridViewPayments.DataBound

       
    End Sub

    Private Sub CreateGridPagerLinks(ByVal StartNo As Integer, ByVal EndNo As Integer, ByVal GetPageNow As Integer, ByVal GetPageLinksIndex As HtmlGenericControl)
        For i As Integer = StartNo To EndNo
            Dim NewDiv As New HtmlGenericControl("div")
            Dim NewLink As New LinkButton
            NewLink.ID = "PagerLink_" & i
            NewLink.OnClientClick = String.Concat("GoToPage('", i, "');return false")
            If i = GetPageNow Then
                NewDiv.Attributes.Add("class", "GridPagerNoSel")
                NewLink.CssClass = "GridPagerNolnkSel"
            Else
                NewDiv.Attributes.Add("class", "GridPagerNo")
                NewLink.CssClass = "GridPagerNolnk"
                NewLink.Style.Add("color", "#4f85d1")
            End If
            NewLink.Text = i
            NewDiv.Controls.Add(NewLink)
            GetPageLinksIndex.Controls.Add(NewDiv)
        Next
    End Sub

    Sub ChangePageByLinkNumber_Before(ByVal sender As Object, ByVal e As EventArgs)
        HiddenPage.Text = HiddenPage.Text - 1
        Bind()
    
    End Sub

    Sub ChangePageByLinkNumber_Next(ByVal sender As Object, ByVal e As EventArgs)
        HiddenPage.Text = HiddenPage.Text + 1
        Bind()
   
    End Sub

    Protected Sub HiddenPageBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HiddenPageBtn.Click
       
        Bind()
    End Sub
#End Region

#End Region

#Region "Handlers & Methods"

    Private Sub SetDatePickers()
        Dim DateNamesSmall As String = ResHelper.LocalizeString("{$='Κυ', 'Δε', 'Τρ', 'Τε', 'Πε', 'Πα', 'Σα'|en-us='Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'$}")
        Dim DateNames As String = ResHelper.LocalizeString("{$='Κυριακή', 'Δευτέρα', 'Τρίτη', 'Τετάρτη', 'Πέμπτη', 'Παρασκευή', 'Σάββατο'|en-us='Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'$}")
        Dim MonthNames As String = ResHelper.LocalizeString("{$='Ιανουάριος','Φεβρουάριος','Μάρτιος','Απρίλιος','Μάιος','Ιούνιος','Ιούλιος','Αύγουστος','Σεπτέμβριος','Οκτώβριος','Νοέμβριος','Δεκέμβριος'|en-us='Januar','Februar','Marts','April','Maj','Juni','Juli','August','September','Oktober','November','December'$}")
        JsScript(String.Concat(" $(""[id$=txtFrom]"").datepicker({ monthNames: [", MonthNames, "], dayNames: [", DateNames, "], dayNamesMin: [", DateNamesSmall, "], dateFormat: 'dd/mm/yy' }); $(""[id$=txtTo]"").datepicker({ monthNames: [", MonthNames, "], dayNames: [", DateNames, "], dayNamesMin: [", DateNamesSmall, "],dateFormat: 'dd/mm/yy' }); ShowHideSrcForm(true); "))
    End Sub

    Protected Sub openPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        TransDetails.TransID = btn.CommandArgument
        TransDetails.PageType = ASP.cmstemplates_livepay_popup_uctransdetails_ascx.PopUpType.Detalis

        If btn.CommandName = "canceltype" Then
            TransDetails.PageTypeForFields = ASP.cmstemplates_livepay_popup_uctransdetails_ascx.PopUpType.Cancel
        Else
            TransDetails.PageTypeForFields = ASP.cmstemplates_livepay_popup_uctransdetails_ascx.PopUpType.Detalis
        End If
        TransDetails.loadData()
        JsScript("ShowTransDetails();")
        Bind()
    End Sub

    Protected Sub openRefund_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btn As LinkButton = DirectCast(sender, LinkButton)

        TransRefunt.TransID = btn.CommandArgument
        TransRefunt.PageType = ASP.cmstemplates_livepay_popup_uctransdetails_ascx.PopUpType.Report
        TransDetails.PageTypeForFields = ASP.cmstemplates_livepay_popup_uctransdetails_ascx.PopUpType.Detalis
        TransRefunt.loadData()

        JsScript("ShowTransRefunt();")
        Bind()
    End Sub

    Protected Sub openCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim btn As LinkButton = DirectCast(sender, LinkButton)
        TransDetails.TransID = btn.CommandArgument
        TransDetails.PageType = ASP.cmstemplates_livepay_popup_uctransdetails_ascx.PopUpType.Cancel
        TransDetails.PageTypeForFields = ASP.cmstemplates_livepay_popup_uctransdetails_ascx.PopUpType.Detalis
        TransDetails.loadData()
        JsScript("ShowTransDetails();")
        Bind()
    End Sub

    Private Sub JsScript(ByVal Script As String)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), Guid.NewGuid.ToString, Script, True)
    End Sub

    Function GetTransactionStatusDescr(ByVal status As LivePay_ESBBridge.TxnResultEnum) As String
        'Failed  1, Reversal 2, Successful 0, Successful_Uploaded 4, Voided 3, Voided_Uploaded 5
        Select Case status
            Case 0 : Return "Επιτυχημένη"
            Case 1 : Return "Αποτυχημένη"
            Case 2 : Return "Αντιλογισμός"
            Case 3 : Return "Ακυρωμένη"
            Case 4 : Return "Successful_Uploaded"
            Case 5 : Return "Voided_Uploaded"
        End Select
        Return status
    End Function

    Sub CloseBatch()
        Dim CatchError As String = String.Empty
        Session("CloseBatchError") = String.Empty


        Try
            Dim ESB_MerchantID As String = DataHelper.GetNotEmpty(CMSContext.CurrentUser.GetValue("LivePayID"), "")
            Dim w As New LivePay_ESBBridge.Bridge

            Dim c As CloseTransactionServiceResponse = w.CloseTransaction(ESB_MerchantID)
            If c.Result Then
                Dim LivePay_MerchantID As Integer = 0
                Dim dt As DataTable = DBConnection.GetMerchantByESBID(ESB_MerchantID)
                If dt.Rows.Count > 0 Then
                    LivePay_MerchantID = dt.Rows(0)("MerchantID").ToString
                End If
                DBConnection.InsertCloseBatch(LivePay_MerchantID)
                Response.Redirect("TransConfirmation.aspx?PageType=CloseBatch&Result=ok")
            Else
                Session("CloseBatchError") = c.ErrorCode
                Response.Redirect("TransConfirmation.aspx?PageType=CloseBatch&Result=ESBerror")
            End If

        Catch ex As Exception
            CatchError = ex.ToString
        End Try

        If CatchError <> String.Empty Then
            Session("CloseBatchError") = CatchError
            Response.Redirect("TransConfirmation.aspx?PageType=CloseBatch&Result=Catcherror")
        End If
    End Sub
#End Region

#Region " Export Methods "

    'ExportFile('ExportToPDF.aspx?ExportType=MerchantsSrcHisPay&lang=<%=CMSContext.CurrentDocumentCulture.CultureCode %>');

    Protected Sub btnSaveToPDFClick(ByVal sender As Object, ByVal e As System.EventArgs)
        BindForExrpot()

    End Sub
   

#End Region

    Protected Sub btnSearchPayments_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearchPayments.Click
        HiddenPage.Text = 1
        Bind()
    End Sub

End Class
