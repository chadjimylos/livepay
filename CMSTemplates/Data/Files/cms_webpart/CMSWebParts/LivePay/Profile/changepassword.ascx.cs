using System;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;

public partial class CMSWebParts_LivePay_Profile_ChangePassword : CMSAbstractWebPart
{
    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether this webpart is displayed only when user is authenticated
    /// </summary>
    public bool ShowOnlyWhenAuthenticated
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("ShowOnlyWhenAuthenticated"), true);
        }
        set
        {
            SetValue("ShowOnlyWhenAuthenticated", value);
            Visible = (!value || CMSContext.CurrentUser.IsAuthenticated());
        }
    }


    /// <summary>
    /// Gets or sets the maximal new password length
    /// </summary>
    public int MaximalPasswordLength
    {
        get
        {
            return ValidationHelper.GetInteger(GetValue("MaximalPasswordLength"), 0);
        }
        set
        {
            SetValue("MaximalPasswordLength", value);
            txtNewPassword.MaxLength = value;
            txtConfirmPassword.MaxLength = value;
        }
    }

    /// <summary>
    /// Gets or sets the Existing Password
    /// </summary>
    public string TextErrorNewPassword
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("TextErrorNewPassword"), ResHelper.LocalizeString("{$=������� ������� ���������:|en-us=Existing Password :$}"));
        }
        set
        {
            this.SetValue("TextErrorNewPassword", value);
        }
    }


    /// <summary>
    /// Gets or sets the Existing Password
    /// </summary>
    public string TextErrorOldPassword
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("TextErrorOldPassword"), ResHelper.LocalizeString("{$=������� ������� ���������:|en-us=Existing Password :$}"));
        }
        set
        {
            this.SetValue("TextErrorOldPassword", value);
        }
    }


    /// <summary>
    /// Gets or sets the Existing Password
    /// </summary>
    public string TextChangesSaved
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("TextChangesSaved"), ResHelper.LocalizeString("{$=������� ������� ���������:|en-us=Existing Password :$}"));
        }
        set
        {
            this.SetValue("TextChangesSaved", value);
        }
    }


    /// <summary>
    /// Gets or sets the Existing Password
    /// </summary>
    public string OldPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("OldPasswordText"), ResHelper.LocalizeString("{$=������� ������� ���������:|en-us=Existing Password :$}"));
        }
        set
        {
            this.SetValue("OldPasswordText", value);
        }
    }


    /// <summary>
    /// Gets or sets the new password
    /// </summary>
    public string NewPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("NewPasswordText"), ResHelper.LocalizeString("{$=���� ������� ��������� :|en-us=new password :$}"));
        }
        set
        {
            this.SetValue("NewPasswordText", value);
        }
    }


    /// <summary>
    /// Gets or sets the Confirm password
    /// </summary>
    public string ConfirmPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ConfirmPasswordText"), ResHelper.LocalizeString("{$=���������� ���� ������� :|en-us=Confirm password :$}"));
        }
        set
        {
            this.SetValue("ConfirmPasswordText", value);
        }
    }


    /// <summary>
    /// Gets or sets the Button Submit
    /// </summary>
    public string ButtonSubmitText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ButtonSubmitText"), ResHelper.LocalizeString("{$=������ �������|en-us=OK$}"));
        }
        set
        {
            this.SetValue("ButtonSubmitText", value);
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            // Do not process
        }
        else
        {
            Visible = (!ShowOnlyWhenAuthenticated || CMSContext.CurrentUser.IsAuthenticated());
            txtNewPassword.MaxLength = MaximalPasswordLength;
            txtConfirmPassword.MaxLength = MaximalPasswordLength;

            // Set labels text
            lblOldPassword.Text = this.OldPasswordText; //ResHelper.GetString("ChangePassword.lblOldPassword") + "1";
            lblNewPassword.Text = this.NewPasswordText; //ResHelper.GetString("ChangePassword.lblNewPassword") + "2";
            lblConfirmPassword.Text = this.ConfirmPasswordText; //ResHelper.GetString("ChangePassword.lblConfirmPassword") + "3";
            btnOk.Text = this.ButtonSubmitText; // ResHelper.GetString("ChangePassword.btnOK");
        }
    }


    /// <summary>
    /// OnClick handler (Set password)
    /// </summary>
    protected void btnOk_Click(object sender, EventArgs e)
    {
        // Get current user info object
        CurrentUserInfo ui = CMSContext.CurrentUser;
        
        // Get current site info object
        CurrentSiteInfo si = CMSContext.CurrentSite;
        
        if ((ui != null) && (si!=null))
        {
            string userName = ui.UserName;
            string siteName = si.SiteName;

            // new password correctly filled
            if (txtConfirmPassword.Text == txtNewPassword.Text)
            {
                // Old password match
                if (UserInfoProvider.AuthenticateUser(userName, txtOldPassword.Text.Trim(), siteName) != null)
                {
                    UserInfoProvider.SetPassword(userName, txtNewPassword.Text.Trim());
                    lblInfo.Visible = true;
                    lblInfo.Text = TextChangesSaved;//ResHelper.GetString("ChangePassword.ChangesSaved");
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = TextErrorOldPassword;//ResHelper.GetString("ChangePassword.ErrorOldPassword");
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = TextErrorNewPassword;//ResHelper.GetString("ChangePassword.ErrorNewPassword");
            }
        }
    }
}