<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Relateddocs_Add.aspx.cs"
    Inherits="CMSModules_Content_CMSDesk_Properties_Relateddocs_Add" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>

<%@ Register Src="~/CMSModules/Content/FormControls/Relationships/selectRelationshipNames.ascx" TagName="RelationshipNameSelector"
    TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Document relationship</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>

    <script type="text/javascript">
        //<![CDATA[
        var currentOnLeft = true;

        function SwitchSides() {
            currentOnLeft = (!currentOnLeft);
            if (currentOnLeft) {
                document.getElementById('leftCurrentNode').style.display = 'block';
                document.getElementById('leftSelectedNode').style.display = 'none';
                document.getElementById('rightCurrentNode').style.display = 'none';
                document.getElementById('rightSelectedNode').style.display = 'block';
                document.getElementById('currentOnLeft').value = 'true';
                document.getElementById('txtRightNode').value = document.getElementById('txtLeftNode').value;
            }
            else {
                document.getElementById('leftCurrentNode').style.display = 'none';
                document.getElementById('leftSelectedNode').style.display = 'block';
                document.getElementById('rightCurrentNode').style.display = 'block';
                document.getElementById('rightSelectedNode').style.display = 'none';
                document.getElementById('currentOnLeft').value = 'false';
                document.getElementById('txtLeftNode').value = document.getElementById('txtRightNode').value;
            }
            return false;
        }
        //]]>
    </script>

</head>
<body class="VerticalTabsBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:PlaceHolder ID="plcMain" runat="server" />
    <asp:Panel ID="pnlBody" runat="server" CssClass="VerticalTabsPageBody">
        <asp:Panel ID="pnlHeader" runat="server" CssClass="PageHeader">
            <cms:PageTitle ID="titleElem" runat="server" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlContent" CssClass="PageContent">
            <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
                Visible="false" />
            <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
                Visible="false" />
            <asp:Table ID="TableRelationship" CssClass="UniGridGrid" runat="server" BorderStyle="solid"
                BorderWidth="1" CellPadding="3" CellSpacing="0" Width="100%">
                <asp:TableHeaderRow CssClass="UniGridHead">
                    <asp:TableHeaderCell ID="leftCell" HorizontalAlign="Left" runat="server" />
                    <asp:TableHeaderCell ID="middleCell" HorizontalAlign="Center" runat="server" />
                    <asp:TableHeaderCell ID="rightCell" HorizontalAlign="Right" runat="server" />
                </asp:TableHeaderRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="40%" VerticalAlign="Top">
                        <div id="leftCurrentNode" style="display: block; padding: 3px;">
                            <asp:Label ID="lblLeftNode" runat="server" />
                        </div>
                        <div id="leftSelectedNode" style="display: none">
                            <asp:TextBox ID="txtLeftNode" runat="server" CssClass="TextBoxField" Width="220" />
                            <br />
                            <cms:LocalizedButton ID="btnLeftNode" runat="server" ResourceString="Relationship.SelectDocument" CssClass="LongButton" />
                        </div>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="center" Width="20%">
                        <cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
                            <ContentTemplate>
                                <cms:RelationshipNameSelector ID="relNameSelector" runat="server" ReturnColumnName="RelationshipNameID"
                                    AllowedForObjects="false" />
                            </ContentTemplate>
                        </cms:CMSUpdatePanel>
                        <cms:LocalizedButton ID="btnSwitchSides" runat="server" OnClientClick="SwitchSides();return false;"
                            CssClass="LongButton" ResourceString="Relationship.SwitchSides" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="right" Width="40%" VerticalAlign="Top">
                        <div id="rightCurrentNode" style="display: none; padding: 3px;">
                            <asp:Label ID="lblRightNode" runat="server" />
                        </div>
                        <div id="rightSelectedNode" style="display: block;">
                            <asp:TextBox ID="txtRightNode" runat="server" CssClass="TextBoxField" Width="220" />
                            <br />
                            <cms:LocalizedButton ID="btnRightNode" runat="server" ResourceString="Relationship.SelectDocument" CssClass="LongButton" />
                        </div>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <br />
            <table width="100%">
                <tr>
                    <td align="right">
                        <cms:LocalizedButton ID="btnOk" runat="server" Text="" OnClick="btnOk_Click" CssClass="SubmitButton"
                            ResourceString="General.OK" />
                    </td>
                </tr>
            </table>
            <input type="hidden" id="selectedNodeId" name="selectedNodeId" />
            <input type="hidden" id="currentOnLeft" name="currentOnLeft" value="true" />
        </asp:Panel>
    </asp:Panel>
    </form>

    <script type="text/javascript">
        //<![CDATA[
        function DialogSelectNode(nodeId, nodeName) {
            document.getElementById('txtRightNode').value = nodeName;
            document.getElementById('txtRightNode').disabled = true;
            document.getElementById('txtLeftNode').value = nodeName;
            document.getElementById('txtLeftNode').disabled = true;
            document.getElementById('selectedNodeId').value = nodeId;
        }
        //]]>
    </script>

</body>
</html>
