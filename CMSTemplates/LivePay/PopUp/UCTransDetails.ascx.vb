﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports LivePay_ESBBridge

Partial Class CMSTemplates_LivePay_PopUp_UCTransDetails
    Inherits CMSUserControl

#Region "Variables"

    Public Shared sTransID As String = "0"
    Public Shared sPageType As PopUpType = Nothing
    Public Shared sPageTypeForFields As PopUpType = Nothing

    Private sBoxNo As String = Nothing
    Private sBoxCloseDtm As String = Nothing
    Private sBoxStatus As String = Nothing
    Private sTransCode As String = Nothing
    Private sTransDtm As String = Nothing
    Private sCustFullName As String = Nothing
    Private sPrice As String = Nothing
    Private sTransType As String = Nothing
    Private sTransStatus As String = Nothing
    Private sCardType As String = Nothing
    Private sCardNo As String = Nothing

    Private sAcountNo As String = Nothing
    Private sEmail As String = Nothing
    Private sContactPhone As String = Nothing
    Private sNotes As String = Nothing
    Private sMerachant As Boolean = True

    Public Shared mSystem As LivePay_ESBBridge.SystemEnum

    Private TimeoutLivePayBridge As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("TimeoutLivePayBridge"))

#End Region

#Region " Properties For Javascript "

    Public Shared Property TransID() As String
        Get
            Return sTransID
        End Get
        Set(ByVal value As String)
            sTransID = value
        End Set
    End Property

    Public Shared Property PageTypeForFields() As PopUpType
        Get
            Return sPageTypeForFields
        End Get
        Set(ByVal value As PopUpType)
            sPageTypeForFields = value
        End Set
    End Property

    Public Shared Property PageType() As PopUpType
        Get
            Return sPageType
        End Get
        Set(ByVal value As PopUpType)
            sPageType = value
        End Set
    End Property

    Public Shared Property System() As LivePay_ESBBridge.SystemEnum
        Get
            Return mSystem
        End Get
        Set(ByVal value As LivePay_ESBBridge.SystemEnum)
            mSystem = value
        End Set
    End Property
#End Region
#Region "Methods"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then

        End If
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = New Guid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

    Public Enum PopUpType
        Detalis = 1
        Cancel = 2
        Report = 3
        Changes = 4
    End Enum

    Private Sub ClearAllStyles()
        DetailsBtnRow.Style.Add("display", "none")
        CancelRow.Style.Add("display", "none")
        CancelBtnRow.Style.Add("display", "none")
        ReportRow.Style.Add("display", "none")
        ReportBtnRow.Style.Add("display", "none")
        TRPrice.Style.Add("display", "none")
        TRPriceTxt.Style.Add("display", "none")
    End Sub

    Public Overloads Sub loadData()
        FieldBatchNo.Visible = True
        FieldBatchCloseDtm.Visible = True
        FieldBatchStatus.Visible = True

        ClearAllStyles()
        If Me.TransID <> "0" Then

            If Me.System = LivePay_ESBBridge.SystemEnum.Merchants Then
                Me.TransID = Replace(Me.TransID, " ", String.Empty)
                Me.TransID = Right(Me.TransID, (Me.TransID.Length - 2))
            End If

            btnRefund.CommandArgument = Me.TransID
            btnRefund.Attributes.Add("onclick", "return SetIt()")
            btnCancel.CommandArgument = Me.TransID
            btnSaveToPDF.CommandArgument = Me.TransID
            Session("TransDetails_TransIDForExport") = Me.TransID
            Session("TransDetails_SystemForExport") = Me.System

            If Me.PageType = PopUpType.Detalis Then
                btnSaveToPDF.Attributes.Add("onclick", "return ExportDetails('/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=MerchantTransDetails&FromPopUpDetails=1&lang=" & CMSContext.CurrentDocumentCulture.CultureCode & "')")
                btnPrint.Attributes.Add("onclick", "window.open('/CMSTemplates/LivePay/PrintPage.aspx?ExportType=MerchantTransDetails&FromPopUpDetails=1&lang=" & CMSContext.CurrentDocumentCulture.CultureCode & "')")
            Else
                btnSaveToPDF.Attributes.Add("onclick", "return ExportDetails('/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=MerchantTransDetails&lang=" & CMSContext.CurrentDocumentCulture.CultureCode & "')")
                btnPrint.Attributes.Add("onclick", "window.open('/CMSTemplates/LivePay/PrintPage.aspx?ExportType=MerchantTransDetails&lang=" & CMSContext.CurrentDocumentCulture.CultureCode & "')")
            End If
            btnPrint.Src = ResHelper.LocalizeString("{$=/app_themes/LivePay/popup/btnPrint.png|en-us=/app_themes/LivePay/popup/btnPrint_en-us.png$}")

            Select Case Me.PageType
                Case PopUpType.Detalis
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Πληροφορίες Συναλλαγής|en-us=Closed$}")
                    DetailsBtnRow.Style.Add("display", "")
                    If Me.PageTypeForFields = PopUpType.Cancel Then
                        FieldBatchNo.Visible = False
                        FieldBatchCloseDtm.Visible = False
                        FieldBatchStatus.Visible = False
                    End If
                Case PopUpType.Cancel
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Επιβεβαίωση Ακύρωσης Συναλλαγής|en-us=Closed$}")
                    CancelRow.Style.Add("display", "")
                    CancelBtnRow.Style.Add("display", "")
                Case PopUpType.Report
                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Αντιλογισμός Συναλλαγής|en-us=Closed$}")
                    ReportRow.Style.Add("display", "")
                    ReportBtnRow.Style.Add("display", "")
                Case PopUpType.Changes

                    PageTitle.InnerHtml = ResHelper.LocalizeString("{$=Μεταβολές Συναλλαγής |en-us=Closed$}")

                    DetailsBtnRow.Style.Add("display", "")
                    FieldBatchNo.Visible = False
                    FieldBatchCloseDtm.Visible = False
                    FieldBatchStatus.Visible = False

            End Select

            Dim w As New LivePay_ESBBridge.Bridge
            w.Timeout = TimeoutLivePayBridge
            'IIf(CMSContext.CurrentDocument.NodeClassName = "LivePay.MerchantPublicSector", LivePay_ESBBridge.SystemEnum.Public, LivePay_ESBBridge.SystemEnum.Merchants)



            Dim TransResponse As TransactionDetailsResponse = w.GetTransactionDetails(Me.TransID, Me.System)
            Dim TransInfo As TransactionDetailsInfo = TransResponse.TransactionDetails
            If Me.PageType <> PopUpType.Changes Then
                If Not IsNothing(TransInfo) Then
                    If TransInfo.transactionStatus = TxnResultEnum.Failed Then
                        ErrosCell.Visible = True
                        tdErrorMessage.InnerHtml = GetESBError(TransInfo.spdhresp)
                    End If

                    'Get Invoice
                    Dim Invoice As LivePay.Invoice
                    Invoice = LivePay.Invoice.GetInvoice(Me.TransID)

                    If Not Invoice Is Nothing Then
                        ph_Invoice.Visible = True
                        tdCompanyName.InnerText = Invoice.CompanyName
                        tdProfession.InnerText = Invoice.Profession
                        tdAddress.InnerText = Invoice.Address
                        tdPostalCode.InnerText = Invoice.PostalCode
                        tdCity.InnerText = Invoice.City
                        tdVATNumber.InnerText = Invoice.VATNumber
                        tdTaxOffice.InnerText = Invoice.TaxOffice
                    Else
                        ph_Invoice.Visible = False
                    End If

                    tdBoxNo.InnerHtml = TransInfo.batchId

                    If TransInfo.batchDate.ToString.Contains("/0001") Then
                        tdBoxCloseDtm.InnerHtml = "-"
                    Else
                        tdBoxCloseDtm.InnerHtml = TransInfo.batchDate
                    End If


                    tdBoxStatus.InnerHtml = IIf(TransInfo.batchClosed, ResHelper.LocalizeString("{$=Κλειστό|en-us=Closed$}"), ResHelper.LocalizeString("{$=Ανοιχτό|en-us=Open$}"))
                    If Me.System = LivePay_ESBBridge.SystemEnum.Public Then
                        'tdTransCode.InnerHtml = TransInfo.info1
                        tdTransCode.InnerHtml = "---"
                    Else
                        tdTransCode.InnerHtml = TransInfo.transactionId
                    End If
                    tdTransDtm.InnerHtml = TransInfo.transactionDate
                    tdCustFullName.InnerHtml = TransInfo.customerName

                    Dim amount As String = TransInfo.transactionAmount 'Replace(TransInfo.transactionAmount, ".", ",") '22
                    Dim amountToCheck As String = "0"
                    If Me.PageType = PopUpType.Report Then
                        If TransInfo.related Then

                            Dim RefundedAmount As Decimal = GetRefundedAmount()
                            If RefundedAmount > 0 Then
                                amount = amount - RefundedAmount
                            End If
                        End If

                    End If
                    amountToCheck = amount
                    amount = Replace(amount, ".", ",")
                    Dim FinalAmount As String
                    If amount.Split(",").Length > 1 Then
                        FinalAmount = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
                    ElseIf amount.Split(",").Length = 1 Then
                        FinalAmount = amount & ",00"
                    Else
                        FinalAmount = amount
                    End If


                    If Me.PageType = PopUpType.Detalis Then
                        AuthorisationCodeTR.Visible = True
                        tdAuthorisationCode.InnerHtml = TransInfo.appcode
                    Else
                        AuthorisationCodeTR.Visible = False
                    End If

                    If Me.PageType = PopUpType.Detalis Or Me.PageType = PopUpType.Cancel Then
                        tdPrice.InnerHtml = FinalAmount
                        TRPrice.Style.Add("display", "")
                    Else
                        TRPriceTxt.Style.Add("display", "")

                        txtPrice.Text = Replace(FinalAmount, ",", ".")
                        txtPrice.ReadOnly = False
                    End If
                    btnRefund.CommandName = amountToCheck
                    tdTransType.InnerHtml = TransInfo.transactionType.ToString

                    tdTransStatus.InnerHtml = GetTransactionStatusDescr(TransInfo.transactionStatus)

                    Dim CardTypeDescr As String = "Visa"
                    If TransInfo.cardType = 1 Then
                        CardTypeDescr = "MasterCard"
                    ElseIf TransInfo.cardType = 2 Then
                        CardTypeDescr = "EuroLine"
                    End If
                    tdCardType.InnerHtml = CardTypeDescr
                    Dim CardNo As String = Replace(TransInfo.cardNumber, " ", String.Empty)
                    tdCardNo.InnerHtml = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))
                    tdEmail.InnerHtml = TransInfo.customerEmail
                    tdContactPhone.InnerHtml = TransInfo.customerTelephone
                    tdNotes.InnerHtml = TransInfo.customerComments
                    CreateCustomFields(TransInfo.merchantId, TransInfo)
                    btnRefund.Enabled = True
                    btnCancel.Enabled = True

                Else

                    tdBoxNo.InnerHtml = String.Empty
                    tdBoxCloseDtm.InnerHtml = String.Empty
                    tdBoxStatus.InnerHtml = String.Empty
                    tdTransCode.InnerHtml = String.Empty
                    tdTransDtm.InnerHtml = String.Empty
                    tdCustFullName.InnerHtml = String.Empty
                    If Me.PageType = PopUpType.Detalis Or Me.PageType = PopUpType.Cancel Then
                        tdPrice.InnerHtml = String.Empty
                        TRPrice.Style.Add("display", "")
                    Else
                        TRPriceTxt.Style.Add("display", "")
                        txtPrice.Text = 0
                        txtPrice.ReadOnly = True
                    End If
                    btnRefund.CommandName = String.Empty
                    tdTransType.InnerHtml = String.Empty
                    tdTransStatus.InnerHtml = String.Empty
                    tdCardType.InnerHtml = String.Empty

                    tdCardNo.InnerHtml = String.Empty
                    tdEmail.InnerHtml = String.Empty
                    tdContactPhone.InnerHtml = String.Empty
                    tdNotes.InnerHtml = String.Empty
                    btnRefund.Enabled = False
                    btnCancel.Enabled = False

                End If
            Else

                tdBoxNo.InnerHtml = String.Empty
                tdBoxCloseDtm.InnerHtml = String.Empty
                tdBoxStatus.InnerHtml = String.Empty
                tdTransCode.InnerHtml = String.Empty
                tdTransDtm.InnerHtml = String.Empty
                tdCustFullName.InnerHtml = String.Empty
                btnRefund.CommandName = String.Empty
                tdTransType.InnerHtml = String.Empty
                tdTransStatus.InnerHtml = String.Empty
                tdCardType.InnerHtml = String.Empty
                tdCardNo.InnerHtml = String.Empty
                tdEmail.InnerHtml = String.Empty
                tdContactPhone.InnerHtml = String.Empty
                tdNotes.InnerHtml = String.Empty
                btnRefund.Enabled = False
                btnCancel.Enabled = False
                MainForm.Visible = False
                ChangesDiv.Visible = True
                PageTitle.InnerHtml = "Μεταβολές Συναλλαγής"

                If Not IsNothing(TransInfo) Then
                    If TransInfo.transactionStatus = TxnResultEnum.Failed Then
                        ErrosCell.Visible = True
                        tdErrorMessage.InnerHtml = GetESBError(TransInfo.spdhresp)
                    End If
                    If TransInfo.related Then
                        CreateChanges()

                    End If
                End If


            End If
        End If


    End Sub

    Private Sub CreateChanges()
        Dim obj As New GetTransactionsObj
        Dim w As New LivePay_ESBBridge.Bridge
        w.Timeout = TimeoutLivePayBridge
        Dim resp As GetTransactionsResponse
        Try
            obj.OriginalTransactionID = Me.TransID

            Dim ER?(2) As LivePay_ESBBridge.TxnTypeEnum
            ER(0) = LivePay_ESBBridge.TxnTypeEnum.Refund
            ER(1) = LivePay_ESBBridge.TxnTypeEnum.Cancellation_Refund
            ER(2) = LivePay_ESBBridge.TxnTypeEnum.Cancellation_Sale
            obj.TransactionType = ER
            obj.TransactionTypeSpecified1 = True

            Dim ERS?(1) As LivePay_ESBBridge.TxnResultEnum
            ERS(0) = LivePay_ESBBridge.TxnResultEnum.Successful
            ERS(1) = LivePay_ESBBridge.TxnResultEnum.Successful_Uploaded
            obj.TransactionResult = ERS
            obj.TransactionResultSpecified1 = True
            obj.AmountFrom = 0
            obj.amountTo = 100000000
            obj.PageSize = 50
            obj.PageNumber = 0

            resp = w.GetTransactions(obj)


            Dim TransInfo As TransactionInfo() = resp.Transactions
            If Not IsNothing(TransInfo) Then
                For i As Integer = 0 To TransInfo.Length - 1
                    Dim NewRow As New HtmlTableRow

                    Dim NewBoxNo As New HtmlTableCell
                    Dim NewDtmCloseBox As New HtmlTableCell

                    Dim NewTrans As New HtmlTableCell
                    Dim NewDate As New HtmlTableCell
                    Dim NewPrice As New HtmlTableCell

                    Dim NewTransType As New HtmlTableCell

                    NewBoxNo.InnerHtml = TransInfo(i).batchId
                    NewDtmCloseBox.InnerHtml = TransInfo(i).batchClosed

                    NewTrans.InnerHtml = TransInfo(i).transactionId
                    NewDate.InnerHtml = TransInfo(i).transactiondDte
                    NewPrice.InnerHtml = FormatNumber(TransInfo(i).transactioAamount, 2)
                    NewTransType.InnerHtml = TransInfo(i).transactionType.ToString

                    NewRow.Controls.Add(NewBoxNo)
                    NewRow.Controls.Add(NewDtmCloseBox)

                    NewRow.Controls.Add(NewTrans)
                    NewRow.Controls.Add(NewDate)
                    NewRow.Controls.Add(NewPrice)

                    NewRow.Controls.Add(NewTransType)

                    NewRow.Attributes.Add("class", "PopTransChangesValue")
                    ChangesTable.Controls.Add(NewRow)
                Next
            End If


        Catch ex As Exception
            JsScript("alert('" & ex.Message.ToString & "');")

        End Try
    End Sub

    Function GetRefundedAmount() As Decimal
        Dim Amount As Decimal = 0

        Dim obj As New GetTransactionsObj

        Dim w As New LivePay_ESBBridge.Bridge
        w.Timeout = TimeoutLivePayBridge
        Dim resp As GetTransactionsResponse
        Try
            obj.OriginalTransactionID = Me.TransID

            Dim ER?(0) As LivePay_ESBBridge.TxnTypeEnum
            ER(0) = LivePay_ESBBridge.TxnTypeEnum.Refund
            obj.TransactionType = ER
            obj.TransactionTypeSpecified1 = True

            Dim ERS?(1) As LivePay_ESBBridge.TxnResultEnum
            ERS(0) = LivePay_ESBBridge.TxnResultEnum.Successful
            ERS(1) = LivePay_ESBBridge.TxnResultEnum.Successful_Uploaded
            obj.TransactionResult = ERS
            obj.TransactionResultSpecified1 = True
            obj.AmountFrom = 0
            obj.amountTo = 100000000
            obj.PageSize = 50
            obj.PageNumber = 0

            resp = w.GetTransactions(obj)


            Dim TransInfo As TransactionInfo() = resp.Transactions
            If Not IsNothing(TransInfo) Then
                For i As Integer = 0 To TransInfo.Length - 1
                    Amount = Amount + TransInfo(i).transactioAamount
                Next
            End If

            Return Amount
        Catch ex As Exception
            JsScript("alert('" & ex.Message.ToString & "');")
            Return 0
        End Try
    End Function

    Function GetTransactionStatusDescr(ByVal status As LivePay_ESBBridge.TxnResultEnum) As String
        'Failed  1, Reversal 2, Successful 0, Successful_Uploaded 4, Voided 3, Voided_Uploaded 5
        Select Case status
            Case 0 : Return "Επιτυχημένη"
            Case 1 : Return "Αποτυχημένη"
            Case 2 : Return "Αντιλογισμός"
            Case 3 : Return "Ακυρωμένη"
            Case 4 : Return "Ολοκληρωμένη Συναλλαγή"
            Case 5 : Return "Ακυρωμένη Συναλλαγή"
        End Select
        Return status
    End Function

    Private Sub CreateCustomFields(ByVal MerchantID As String, ByVal TransInfo As TransactionDetailsInfo)
        Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(MerchantID, CMSContext.CurrentDocumentCulture.CultureCode)
        Dim LivePay_MerchantID As Integer = 0
        If dtMerc.Rows.Count > 0 Then
            LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
        End If

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(LivePay_MerchantID)
        Dim dt As DataTable = ds.Tables(0)
        CustomFieldsRow.Visible = (ds.Tables(0).Rows.Count > 0)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim CustomTR As New HtmlTableRow
            Dim CustomTDTitle As New HtmlTableCell
            Dim CustomTDValue As New HtmlTableCell
            CustomTDTitle.Attributes.Add("class", "PopTransDetTitle2")
            CustomTDValue.Attributes.Add("class", "PopTransDetValue")
            CustomTDTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
            Select Case i
                Case 0
                    CustomTDValue.InnerHtml = TransInfo.info1
                Case 1
                    CustomTDValue.InnerHtml = TransInfo.info2
                Case 2
                    CustomTDValue.InnerHtml = TransInfo.info3
                Case 3
                    CustomTDValue.InnerHtml = TransInfo.info4
                Case 4
                    CustomTDValue.InnerHtml = TransInfo.info5
                Case 5
                    CustomTDValue.InnerHtml = TransInfo.info6
                Case 6
                    CustomTDValue.InnerHtml = TransInfo.info7
                Case 7
                    CustomTDValue.InnerHtml = TransInfo.info8
                Case 8
                    CustomTDValue.InnerHtml = TransInfo.info9
                Case 9
                    CustomTDValue.InnerHtml = TransInfo.info10
            End Select

            CustomTR.Controls.Add(CustomTDTitle)
            CustomTR.Controls.Add(CustomTDValue)
            tblCustomFields.Controls.Add(CustomTR)
        Next
    End Sub

    Sub Refund_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRefund.Click
        Dim CatchError As String = String.Empty
        Session("RefundError") = String.Empty
        Dim NewTransID As Integer = 0
        Dim NewTransGUID As Guid = Nothing
        Dim OldTransGUID As Guid = Nothing
        Try
            Session("RefundError") = String.Empty
            Dim AllRequest As String = Server.UrlDecode(Request.Form.ToString)
            AllRequest = Split(AllRequest, "txtRefund=")(1)
            AllRequest = Split(AllRequest, "&plc$")(0)
            Me.TransID = btnRefund.CommandArgument


            Dim dt As DataTable = DBConnection.InsertOtherTypeTransaction(Me.TransID, AllRequest, 0, Me.TransID, 0)

            If dt.Rows.Count > 0 Then
                NewTransID = dt.Rows(0)("TransID").ToString()
                NewTransGUID = New Guid(dt.Rows(0)("TransGUID").ToString())
                OldTransGUID = New Guid(dt.Rows(0)("OldTransGUID").ToString())
                Dim w As New LivePay_ESBBridge.Bridge
                w.Timeout = TimeoutLivePayBridge
                Dim c As RefundTransactionServiceResponse = w.RefundTransaction(Me.TransID, NewTransID, AllRequest)
                If c.Result Then
                    DBConnection.UpdateTransaction(NewTransID, 2, 0) '------ Update Transaction Set Status = 2 (Success)
                    Session("RefundError") = String.Empty
                    Response.Redirect("~/TransConfirmation.aspx?PageType=Refund&Result=ok&TransID=" & NewTransGUID.ToString) '---- Nkal I Need to take the New GUID
                Else

                    DBConnection.UpdateTransaction(NewTransID, 1, 0) '------ Update Transaction Set Status = 1 (Failed)
                    Session("RefundError") = c.ErrorCode
                    Response.Redirect("~/TransConfirmation.aspx?PageType=REfund&Result=ESBerror")
                End If
            End If
        Catch ex As Exception
            CatchError = ex.ToString
        End Try
        If CatchError <> String.Empty Then
            DBConnection.UpdateTransaction(NewTransID, 1, 0) '------ Update Transaction Set Status = 1 (Failed)
            Session("RefundError") = CatchError
            Response.Redirect("~/TransConfirmation.aspx?PageType=Refund&Result=Catcherror")
        End If

    End Sub

    Sub btnCancel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnCancel.Click
        Dim CatchError As String = String.Empty
        Session("CancelError") = String.Empty
        Dim NewTransID As Integer = 0
        Dim NewTransGUID As Guid = Nothing
        Dim OldTransGUID As Guid = Nothing
        Try
            Me.TransID = btnCancel.CommandArgument
            Dim dt As DataTable = DBConnection.InsertOtherTypeTransaction(Me.TransID, 0, Me.TransID, 0, 0)
            If dt.Rows.Count > 0 Then
                NewTransID = dt.Rows(0)("TransID").ToString()
                NewTransGUID = New Guid(dt.Rows(0)("TransGUID").ToString())
                OldTransGUID = New Guid(dt.Rows(0)("OldTransGUID").ToString())
                Dim w As New LivePay_ESBBridge.Bridge
                w.Timeout = TimeoutLivePayBridge
                Dim c As CancelTransactionServiceResponse = w.CancelTransaction(Me.TransID, NewTransID)
                If c.Result Then
                    DBConnection.UpdateTransaction(NewTransID, 2, 0) '------ Update Transaction Set Status = 2 (Success)
                    Session("CancelError") = String.Empty
                    Response.Redirect("~/TransConfirmation.aspx?PageType=Cancel&Result=ok&TransID=" & OldTransGUID.ToString) '---- Nkal I Need to take the old GUID
                Else
                    DBConnection.UpdateTransaction(NewTransID, 1, 0) '------ Update Transaction Set Status = 1 (Failed)
                    Session("CancelError") = c.ErrorCode
                    Response.Redirect("~/TransConfirmation.aspx?PageType=Cancel&Result=ESBerror")
                End If
            End If
        Catch ex As Exception
            CatchError = ex.ToString
        End Try
        If CatchError <> String.Empty Then
            DBConnection.UpdateTransaction(NewTransID, 1, 0) '------ Update Transaction Set Status = 1 (Failed)
            Session("CancelError") = CatchError
            Response.Redirect("~/TransConfirmation.aspx?PageType=Cancel&Result=Catcherror")
        End If

    End Sub
#End Region


    Private Function GetESBError(ByVal ErrorCode As String) As String
        Dim dt As DataTable = DBConnection.GetESBError(CMSContext.CurrentDocumentCulture.CultureCode, ErrorCode)
        If dt.Rows.Count > 0 Then
            Return ErrorCode & ": " & dt.Rows(0)("ErrorMessage").ToString
        End If
    End Function

    Protected Sub btnSaveToPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveToPDF.Click

        Session("TransDetails_TransIDForExport") = btnSaveToPDF.CommandArgument
        Response.Redirect("/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=MerchantTransDetails&lang=" & CMSContext.CurrentDocumentCulture.CultureCode)
    End Sub
End Class
