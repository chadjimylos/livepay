<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Supplier_List.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Suppliers_Supplier_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Supplier - list" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="gridElem" GridName="supplier_List.xml" OrderBy="SupplierDisplayName"
        IsLiveSite="false" Columns="SupplierID, SupplierDisplayName, SupplierEnabled" />
</asp:Content>
