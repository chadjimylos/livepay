<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OptionCategory_New.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_ProductOptions_OptionCategory_New" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Option Category - New" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblDisplayName" EnableViewState="false" ResourceString="general.displayname"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" ControlToValidate="txtDisplayName"
                    runat="server" Display="Dynamic" ValidationGroup="OptionCategories" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblCategoryName" EnableViewState="false" ResourceString="general.codename"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtCategoryName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvCategoryName" ControlToValidate="txtCategoryName"
                    runat="server" Display="Dynamic" ValidationGroup="OptionCategories" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" ValidationGroup="OptionCategories" />
            </td>
        </tr>
    </table>
</asp:Content>
