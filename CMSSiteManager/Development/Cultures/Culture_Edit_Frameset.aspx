<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Culture_Edit_Frameset.aspx.cs" Inherits="CMSSiteManager_Development_Cultures_Culture_Edit_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Cultures</title>
</head>

	<frameset border="0" rows="102, *" id="rowsFrameset">
		<frame name="menu" src="Culture_Edit_Header.aspx?cultureID=<%=Request.QueryString["cultureID"] %> " scrolling="no" frameborder="0" noresize="noresize" />		
	    <frame name="content" src="Culture_Edit_General.aspx?cultureID=<%=Request.QueryString["cultureID"]%>&saved=<%=Request.QueryString["saved"]%>  " frameborder="0" noresize="noresize" />
	<noframes>
		<body>
		 <p id="p2">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		 </p>
		</body>
	</noframes>
</frameset>
</html>
