<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RelationshipName_List.aspx.cs"
    Inherits="CMSSiteManager_Development_RelationshipNames_RelationshipName_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Relationship names - List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <cms:UniGrid ID="UniGridRelationshipNames" runat="server" GridName="RelationshipNames_List.xml"
        OrderBy="RelationshipDisplayName" Columns="RelationshipNameID,RelationshipDisplayName,RelationshipName,RelationshipAllowedObjects"
        IsLiveSite="false" />
</asp:Content>
