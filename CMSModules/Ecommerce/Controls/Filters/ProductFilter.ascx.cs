using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Controls;

public partial class CMSModules_Ecommerce_Controls_Filters_ProductFilter : CMSAbstractDataFilterControl
{
    #region "Variables"

    private bool mShowPublicStatusFilter = true;
    private bool mShowManufacturerFilter = true;
    private bool mShowPagingFilter = true;
    private bool mShowStockFilter = true;
    private bool mShowSortingFilter = true;
    private string mPagingOptions = null;
    private bool mFilterByQuery = false;

    #endregion


    #region "Properties"

    /// <summary>
    /// Show public status filter.
    /// </summary>
    public bool ShowPublicStatusFilter
    {
        get
        {
            return this.mShowPublicStatusFilter;
        }
        set
        {
            this.mShowPublicStatusFilter = value;
        }
    }


    /// <summary>
    /// Show manufacturer filter.
    /// </summary>
    public bool ShowManufacturerFilter
    {
        get
        {
            return this.mShowManufacturerFilter;
        }
        set
        {
            this.mShowManufacturerFilter = value;
        }
    }


    /// <summary>
    /// Show paging filter.
    /// </summary>
    public bool ShowPagingFilter
    {
        get
        {
            return this.mShowPagingFilter;
        }
        set
        {
            this.mShowPagingFilter = value;
        }
    }


    /// <summary>
    /// Show stock filter.
    /// </summary>
    public bool ShowStockFilter
    {
        get
        {
            return this.mShowStockFilter;
        }
        set
        {
            this.mShowStockFilter = value;
        }
    }


    /// <summary>
    /// Show sorting filter.
    /// </summary>
    public bool ShowSortingFilter
    {
        get
        {
            return this.mShowSortingFilter;
        }
        set
        {
            this.mShowSortingFilter = value;
        }
    }


    /// <summary>
    /// Paging filter options (values separated by comma).
    /// </summary>
    public string PagingOptions
    {
        get
        {
            return this.mPagingOptions;
        }
        set
        {
            this.mPagingOptions = value;
        }
    }


    /// <summary>
    /// Filter by query parameters.
    /// </summary>
    public bool FilterByQuery
    {
        get
        {
            return this.mFilterByQuery;
        }
        set
        {
            this.mFilterByQuery = value;
        }
    }

    #endregion


    protected void btnFilter_Click(object sender, EventArgs e)
    {
        if (this.FilterByQuery)
        {
            // Handle all query parameters
            string url = UrlHelper.RawUrl;

            url = UrlHelper.RemoveParameterFromUrl(url, "statusid");
            if (Convert.ToInt32(statusSelector.Value) > 0)
            {
                url = UrlHelper.AddParameterToUrl(url, "statusid", statusSelector.Value.ToString());
            }

            url = UrlHelper.RemoveParameterFromUrl(url, "manufacturerid");
            if (Convert.ToInt32(manufacturerSelector.Value) > 0)
            {
                url = UrlHelper.AddParameterToUrl(url, "manufacturerid", manufacturerSelector.Value.ToString());
            }

            url = UrlHelper.RemoveParameterFromUrl(url, "available");
            if (chkStock.Checked)
            {
                url = UrlHelper.AddParameterToUrl(url, "available", "1");
            }

            url = UrlHelper.RemoveParameterFromUrl(url, "pagesize");
            if (drpPaging.SelectedValue != "")
            {
                url = UrlHelper.AddParameterToUrl(url, "pagesize", drpPaging.SelectedValue);
            }

            url = UrlHelper.RemoveParameterFromUrl(url, "order");
            if (drpSort.SelectedValue != "")
            {
                url = UrlHelper.AddParameterToUrl(url, "order", drpSort.SelectedValue);
            }

            // Redirect with new query parameters
            UrlHelper.Redirect(url);
        }        
    }


    /// <summary>
    /// Setups the control
    /// </summary>
    private void SetupControl()
    {
        // Show/Hide manufacturer filter
        if (this.ShowManufacturerFilter)
        {
            this.lblManufacturer.Text = ResHelper.GetString("ecommerce.filter.product.manufacturer");
            this.manufacturerSelector.InnerControl.CssClass = "DropDownList";
        }
        else
        {
            this.lblManufacturer.Visible = false;
            this.manufacturerSelector.Visible = false;
        }

        // Show/Hide public status filter
        if (this.ShowPublicStatusFilter)
        {
            this.lblStatus.Text = ResHelper.GetString("ecommerce.filter.product.status");
            this.statusSelector.InnerControl.CssClass = "DropDownList";
        }
        else
        {
            this.lblStatus.Visible = false;
            this.statusSelector.Visible = false;
        }

        // Show/Hide stock filter
        if (this.ShowStockFilter)
        {
            this.chkStock.Text = ResHelper.GetString("ecommerce.filter.product.stock");
        }
        else
        {
            this.chkStock.Visible = false;
        }

        // Show/Hide paging filter
        if (this.ShowPagingFilter)
        {
            this.lblPaging.Text = ResHelper.GetString("ecommerce.filter.product.paging");
        }
        else
        {
            this.lblPaging.Visible = false;
            this.drpPaging.Visible = false;
        }

        // Show/Hide sorting filter
        if (this.ShowSortingFilter)
        {
            this.lblSort.Text = ResHelper.GetString("ecommerce.filter.product.sort");
        }
        else
        {
            this.lblSort.Visible = false;
            this.drpSort.Visible = false;
        }

        this.btnFilter.Text = ResHelper.GetString("ecommerce.filter.product.filter");

        if (!RequestHelper.IsPostBack())
        {
            // Initialize dropdowns
            this.drpSort.Items.Add(new ListItem(ResHelper.GetString("ecommerce.filter.product.nameasc"), "nameasc"));
            this.drpSort.Items.Add(new ListItem(ResHelper.GetString("ecommerce.filter.product.namedesc"), "namedesc"));
            this.drpSort.Items.Add(new ListItem(ResHelper.GetString("ecommerce.filter.product.priceasc"), "priceasc"));
            this.drpSort.Items.Add(new ListItem(ResHelper.GetString("ecommerce.filter.product.pricedesc"), "pricedesc"));

            this.drpPaging.Items.Add(new ListItem(ResHelper.GetString("General.SelectAll"), ""));

            if ((this.PagingOptions != null) && (this.PagingOptions != ""))
            {
                // Parse PagingOptions string and fill dropdown
                string[] pageOptions = this.PagingOptions.Split(',');
                foreach (string pageOption in pageOptions)
                {
                    if (pageOption.Trim() != "")
                    {
                        this.drpPaging.Items.Add(new ListItem(pageOption.Trim()));
                    }
                }
            }
            else
            {
                // Hide paging
                this.drpPaging.Visible = false;
                this.lblPaging.Visible = false;
            }

            if (this.FilterByQuery)
            {
                // Get filter setings from query
                GetFilter();
            }
        }

        // Section 508 validation
        lblManufacturer.AssociatedControlClientID = manufacturerSelector.ValueElementID;
        lblStatus.AssociatedControlClientID = statusSelector.ValueElementID;       
    }


    private void SetFilter()
    {
        string where = "";

        // Build where condition according to dropdowns setings
        if (Convert.ToInt32(statusSelector.Value) > 0)
        {
            where += "SKUPublicStatusID = " + statusSelector.Value + " AND ";
        }
        if (Convert.ToInt32(manufacturerSelector.Value) > 0)
        {
            where += "SKUManufacturerID = " + manufacturerSelector.Value + " AND ";
        }
        if (chkStock.Checked)
        {
            where += "SKUAvailableItems > 0 AND ";
        }
        if (where != "")
        {
            // Remove last AND
            where = where.Substring(0, where.Length - 4);
        }

        // Process drpSort dropdown
        int paging = 0;
        if (drpPaging.SelectedValue != "")
        {
            paging = ValidationHelper.GetInteger(drpPaging.SelectedValue, 0);
        }

        // Process drpSort dropdown
        string order = null;
        if (drpSort.SelectedValue != "")
        {
            switch (drpSort.SelectedValue.ToLower())
            {
                case "nameasc":
                    order = "SKUName";
                    break;

                case "namedesc":
                    order = "SKUName DESC";
                    break;

                case "priceasc":
                    order = "SKUPrice";
                    break;

                case "pricedesc":
                    order = "SKUPrice DESC";
                    break;
            }
        }

        if (where != "")
        {
            // Set where condition
            this.WhereCondition = where;
        }
        if (order != "")
        {
            // Set orderBy condition
            this.OrderBy = order;
        }
        if (paging > 0)
        {
            // Set paging
            this.PageSize = paging;
        }

        // Filter changed event
        this.RaiseOnFilterChanged();
    }


    private void GetFilter()
    {
        int status = QueryHelper.GetInteger("statusid", 0);
        int manufacturer = QueryHelper.GetInteger("manufacturerid", 0);
        bool stock = QueryHelper.GetBoolean("available", false);
        int paging = QueryHelper.GetInteger("pagesize", 0);
        string order = QueryHelper.GetString("order", "");

        // Set internal status if in query
        if (status > 0)
        {
            this.statusSelector.Value = status;
        }

        // Set manufacturer if in query
        if (manufacturer > 0)
        {
            this.manufacturerSelector.Value = manufacturer;
        }

        // Set only in stock if in query
        if (stock)
        {
            this.chkStock.Checked = true;
        }

        // Set paging if in query
        if (paging > 0)
        {
            try
            {
                this.drpPaging.SelectedValue = paging.ToString();
            }
            catch { }
        }

        // Set order if in query
        if (order != "")
        {
            try
            {
                this.drpSort.SelectedValue = order.ToString();
            }
            catch { }
        }
    }

    /// <summary>
    /// Child control creation.
    /// </summary>
    protected override void CreateChildControls()
    {
        SetupControl();
        base.CreateChildControls();
    }


    /// <summary>
    /// Pre render event.
    /// </summary>
    /// <param name="e"></param>
    protected override void OnPreRender(EventArgs e)
    {
        // Set filter settings
        if (RequestHelper.IsPostBack() && !this.FilterByQuery)
        {
            SetFilter();
        }
        else if (this.FilterByQuery)
        {
            SetFilter();
        }
    }

}
