using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_WebContainers_Container_Edit_General : SiteManagerPage
{
    #region "Private variables"

    protected int containerId = 0;
    WebPartContainerInfo webPartContainerObj = null ;

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Init labels
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");

        string currentWebPartContainer = ResHelper.GetString("Container_Edit.NewItemCaption");

        containerId = QueryHelper.GetInteger("containerid", 0);

        // Load the data
        if (containerId > 0)
        {
            webPartContainerObj = WebPartContainerInfoProvider.GetWebPartContainerInfo(containerId);
            currentWebPartContainer = webPartContainerObj.ContainerDisplayName;

            if (!RequestHelper.IsPostBack())
            {
                LoadData(webPartContainerObj);

                // Show changes saved if specified from querystring
                if (Request.QueryString["saved"] == "1")
                {
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
            }
        }
    }


    /// <summary>
    /// Load data of editing webPartContainer.
    /// </summary>
    /// <param name="webPartContainerObj">WebPartContainer object.</param>
    protected void LoadData(WebPartContainerInfo webPartContainerObj)
    {
        // Load fields
        txtContainerTextBefore.Text = webPartContainerObj.ContainerTextBefore;
        txtContainerTextAfter.Text = webPartContainerObj.ContainerTextAfter;
        txtContainerName.Text = webPartContainerObj.ContainerName;
        txtContainerDisplayName.Text = webPartContainerObj.ContainerDisplayName;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Validate
        string errorMessage = new Validator().NotEmpty(txtContainerDisplayName.Text, rfvDisplayName.ErrorMessage)
            .NotEmpty(txtContainerName.Text, rfvCodeName.ErrorMessage)
            .IsCodeName(txtContainerName.Text, ResHelper.GetString("general.invalidcodename"))
            .Result;

        if (errorMessage == "")
        {
            // If webPartContainer doesnt already exist, create new one
            if (webPartContainerObj == null)
            {
                webPartContainerObj = new WebPartContainerInfo();
            }
            webPartContainerObj.ContainerTextBefore = txtContainerTextBefore.Text;
            webPartContainerObj.ContainerTextAfter = txtContainerTextAfter.Text;
            webPartContainerObj.ContainerName = txtContainerName.Text.Trim();
            webPartContainerObj.ContainerDisplayName = txtContainerDisplayName.Text.Trim();

            WebPartContainerInfo wPcI = WebPartContainerInfoProvider.GetWebPartContainerInfo(webPartContainerObj.ContainerName);

            if ((wPcI != null) && (wPcI.ContainerID != containerId))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Container_Edit.UniqueError");
                return;
            }

            WebPartContainerInfoProvider.SetWebPartContainerInfo(webPartContainerObj);

            UrlHelper.Redirect("Container_Edit_General.aspx?containerid=" + Convert.ToString(webPartContainerObj.ContainerID) + "&saved=1");
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }

    #endregion
}
