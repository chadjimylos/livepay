<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteCultureSelector.ascx.cs"
    Inherits="CMSFormControls_Cultures_SiteCultureSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>
<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%CultureName%}"
            OrderBy="CultureName" ObjectType="cms.culture" ResourcePrefix="cultureselect"
            AllowEmpty="false" AllowAll="false" SelectionMode="SingleDropDownList" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
