<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Polls_Sites.aspx.cs" Inherits="CMSModules_Polls_Tools_Polls_Sites"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Polls - sites" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>



<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" runat="server" CssClass="ErrorLabel" Visible="false"
        EnableViewState="false" />
    <asp:Label ID="lblAvialable" runat="server" CssClass="BoldInfoLabel" />
    <cms:UniSelector ID="usSites" runat="server" IsLiveSite="false" ObjectType="cms.site"
        SelectionMode="Multiple" ResourcePrefix="sitesselect" />
</asp:Content>
