using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Newsletter;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_New_Send : CMSNewsletterPage
{
    protected int newsletterIssueId = 0;
    protected int newsletterId = 0;
    Issue issue = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get newsletter ID from query string
        newsletterIssueId = QueryHelper.GetInteger("issueid", 0);

        if (newsletterIssueId > 0)
        {
            // Get issue object
            issue = IssueProvider.GetIssue(newsletterIssueId);
        }
        else
        {
            btnFinish.Enabled = false;
            return;
        }

        if (issue != null)
        {
            // Get newsletter ID
            newsletterId = issue.IssueNewsletterID;
        }

        // Initializes page title control		
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("newsletter_issue_list.title");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Newsletter_Issue/new.png");

        // Initialize controls' labels
        radSendNow.Text = ResHelper.GetString("newsletter_issue_new_send.sendnow");
        radSendLater.Text = ResHelper.GetString("newsletter_issue_new_send.sendlater");
        radSchedule.Text = ResHelper.GetString("newsletter_issue_new_send.schedule");
        lblDateTime.Text = ResHelper.GetString("newsletter_issue_new_send.datetime");
        btnFinish.Text = ResHelper.GetString("general.finish");
        btnClose.Text = ResHelper.GetString("general.close");
        ucHeader.Title = ResHelper.GetString("Newsletter_Issue_New_Send.Step3");
        ucHeader.Header = ResHelper.GetString("Newsletter_Issue_New_Send.header");
        ucHeader.DescriptionVisible = false;

        if (!RequestHelper.IsPostBack())
        {
            radSendNow.Checked = true;
        }

        calendarControl.SupportFolder = "~/CMSAdminControls/Calendar";
        calendarControl.DateTimeTextBox.CssClass = "EditingFormCalendarTextBox";

        RegisterModalPageScripts();
    }
    

    protected void btnFinish_Click(object sender, EventArgs e)
    {
        // Check 'Author issues' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "authorissues"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "authorissues");
        }

        if (radSendNow.Checked) //Send now
        {
            IssueProvider.SendIssue(newsletterIssueId);
        }
        else if (radSchedule.Checked) //Schedule sending
        {
            if (calendarControl.SelectedDateTime == DateTimeHelper.ZERO_TIME)
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("newsletter.incorrectdate");
                return;
            }
            else
            {
                IssueProvider.SendIssue(newsletterIssueId, calendarControl.SelectedDateTime);
            }
        }
        //else if (radSendLater.Checked) => send later => do nothing now
        ltlScript.Text = ScriptHelper.GetScript("RefreshPage(); setTimeout(\"top.window.close()\",200);");
    }


    protected void radGroupSend_CheckedChanged(object sender, EventArgs e)
    {
        calendarControl.Enabled = radSchedule.Checked;
    }
}
