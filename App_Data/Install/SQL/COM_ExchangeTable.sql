CREATE TABLE [COM_ExchangeTable] (
		[ExchangeTableID]               int NOT NULL IDENTITY(1, 1),
		[ExchangeTableDisplayName]      nvarchar(200) NOT NULL,
		[ExchangeTableValidFrom]        datetime NULL,
		[ExchangeTableValidTo]          datetime NULL,
		[ExchangeTableGUID]             uniqueidentifier NOT NULL,
		[ExchangeTableLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_ExchangeTable]
	ADD
	CONSTRAINT [PK_COM_ExchangeTable]
	PRIMARY KEY
	NONCLUSTERED
	([ExchangeTableID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_COM_ExchangeTable_ExchangeTableValidFrom_ExchangeTableValidTo]
	ON [COM_ExchangeTable] ([ExchangeTableValidFrom] DESC, [ExchangeTableValidTo] DESC)
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
