using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.DirectoryUtilities;
using CMS.FileManager;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_PageLayouts_PageLayout_Edit : SiteManagerPage
{
    protected int layoutId = 0;
    protected string[,] pageTitleTabs = new string[2, 3];
    protected LayoutInfo li = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        lbCodeName.Text = ResHelper.GetString("Administration-PageLayout_New.LayoutCodeName");
        lbLayoutDisplayName.Text = ResHelper.GetString("Administration-PageLayout_New.LayoutDisplayName");
        lbLayoutDescription.Text = ResHelper.GetString("Administration-PageLayout_New.LayoutDescription");
        lbLayoutCode.Text = ResHelper.GetString("Administration-PageLayout_New.LayoutCode");
        rfvLayoutDisplayName.ErrorMessage = ResHelper.GetString("Administration-PageLayout_New.ErrorEmptyLayoutDisplayName");
        rfvLayoutCode.ErrorMessage = ResHelper.GetString("Administration-PageLayout_New.ErrorEmptyLayoutCode");
        rfvCodeName.ErrorMessage = ResHelper.GetString("Administration-PageLayout_New.ErrorEmptyLayoutCodeName");
        ltlHint.Text = ResHelper.GetString("Administration-PageLayout_New.Hint");
        string lang = DataHelper.GetNotEmpty(SettingsHelper.AppSettings["CMSProgrammingLanguage"], "C#");
        ltlDirectives.Text = "&lt;%@ Control Language=\"" + lang + "\" ClassName=\"Simple\" Inherits=\"CMS.PortalControls.CMSAbstractLayout\" %&gt;<br />&lt;%@ Register Assembly=\"CMS.PortalControls\" Namespace=\"CMS.PortalControls\" TagPrefix=\"cc1\" %&gt;";

        layoutId = QueryHelper.GetInteger("layoutId", 0);
        if (layoutId > 0)
        {
            if (!RequestHelper.IsPostBack())
            {
                LoadData();
            }
        }
        else
        {
            if (tbCodeName.Text == "")
            {
                // default layout code content
                tbLayoutCode.Text = "<cms:CMSWebPartZone ID=\"zoneCenter\" runat=\"server\" />";
            }
        }

        string layouts = ResHelper.GetString("Administration-PageLayout_New.NewLayout");
        string title = ResHelper.GetString("Administration-PageLayout_New.Title");
        string image = GetImageUrl("Objects/CMS_Layout/new.png");
        // gets 'layoutid' from querystring
        string currentLayout = ResHelper.GetString("Administration-PageLayout_New.CurrentLayout");

        // Gets page layout info of specified 'layoutid'
        li = LayoutInfoProvider.GetLayoutInfo(layoutId);
        if (li != null)
        {
            currentLayout = li.LayoutDisplayName;
            title = ResHelper.GetString("Administration-PageLayout_New.EditLayout");
            image = GetImageUrl("Objects/CMS_Layout/object.png");

            lblUploadFile.Visible = true;
            lblUploadFile.Text = ResHelper.GetString("Administration-PageLayout_New.PageLayoutThumbnail");
            UploadFile.ObjectID = layoutId;
            UploadFile.Category = MetaFileInfoProvider.OBJECT_CATEGORY_THUMBNAIL;
            UploadFile.ObjectType = SiteObjectType.PAGELAYOUT;
            UploadFile.Visible = true;
        }

        InitializeHeaderActions(li);

        if (Request.QueryString["saved"] != null && Request.QueryString["saved"] == "1")
        {
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }

        pageTitleTabs[0, 0] = layouts;
        pageTitleTabs[0, 1] = ResolveUrl("PageLayout_List.aspx");
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentLayout;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = title;
        this.CurrentMaster.Title.TitleImage = image;
        this.CurrentMaster.Title.HelpTopicName = "newedit_page_layout";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!SettingsKeyProvider.UsingVirtualPathProvider && (li != null))
        {
            this.pnlCheckOutInfo.Visible = false;
            this.tbLayoutCode.Enabled = false;
            this.plcVirtualInfo.Visible = true;
            this.lblVirtualInfo.Text = String.Format(ResHelper.GetString("PageLayout.VirtualPathProviderNotRunning"), LayoutInfoProvider.GetLayoutUrl(li.LayoutCodeName, null));
        }
    }


    /// <summary>
    /// Initializes header action control
    /// </summary>
    private void InitializeHeaderActions(LayoutInfo li)
    {
        // Header actions
        string[,] actions = new string[4, 11];

        // Save button
        actions[0, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[0, 1] = ResHelper.GetString("General.Save");
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        actions[0, 6] = "save";
        actions[0, 8] = "true";

        if (SettingsKeyProvider.UsingVirtualPathProvider)
        {
            // CheckOut
            actions[1, 0] = HeaderActions.TYPE_SAVEBUTTON;
            actions[1, 1] = ResHelper.GetString("General.CheckOutToFile");
            actions[1, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkout.png");
            actions[1, 6] = "checkout";
            actions[1, 10] = "false";

            // CheckIn
            actions[2, 0] = HeaderActions.TYPE_SAVEBUTTON;
            actions[2, 1] = ResHelper.GetString("General.CheckInFromFile");
            actions[2, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/checkin.png");
            actions[2, 6] = "checkin";
            actions[2, 10] = "false";

            // UndoCheckOut
            actions[3, 0] = HeaderActions.TYPE_SAVEBUTTON;
            actions[3, 1] = ResHelper.GetString("General.UndoCheckout");
            actions[3, 2] = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("General.ConfirmUndoCheckOut")) + ");";
            actions[3, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/undocheckout.png");
            actions[3, 6] = "undocheckout";
            actions[3, 10] = "false";
        }

        if (li != null)
        {
            if (li.LayoutCheckedOutByUserID > 0)
            {
                // Checked out by current machine
                if (li.LayoutCheckedOutMachineName.ToLower() == HTTPHelper.MachineName.ToLower())
                {
                    actions[2, 10] = "true";
                }
                if (CMSContext.CurrentUser.IsGlobalAdministrator)
                {
                    actions[3, 10] = "true";
                }
            }
            else
            {
                actions[1, 10] = "true";
            }
        }

        this.CurrentMaster.HeaderActions.LinkCssClass = "ContentSaveLinkButton";
        this.CurrentMaster.HeaderActions.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);
        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    /// <summary>
    /// Actions handler
    /// </summary>
    protected void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "save":

                if (SaveLayout())
                {
                    UrlHelper.Redirect("PageLayout_Edit.aspx?layoutID=" + layoutId + "&saved=1");
                }
                else
                {
                    lblInfo.Visible = false;
                }
                break;

            case "checkout":

                // Save first
                if (!SaveLayout())
                {
                    return;
                }

                try
                {
                    SiteManagerFunctions.CheckOutLayout(layoutId);
                }
                catch (Exception ex)
                {
                    this.lblError.Text = ResHelper.GetString("Layout.ErrorCheckout") + ": " + ex.Message;
                    this.lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;

            case "checkin":

                try
                {
                    SiteManagerFunctions.CheckInLayout(layoutId);
                }
                catch (Exception ex)
                {
                    this.lblError.Text = ResHelper.GetString("Layout.ErrorCheckin") + ": " + ex.Message;
                    this.lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;

            case "undocheckout":

                try
                {
                    SiteManagerFunctions.UndoCheckOutLayout(layoutId);
                }
                catch (Exception ex)
                {
                    this.lblError.Text = ResHelper.GetString("Layout.ErrorUndoCheckout") + ": " + ex.Message;
                    this.lblError.Visible = true;
                    return;
                }

                UrlHelper.Redirect(Request.Url.AbsoluteUri);
                break;
        }
    }


    /// <summary>
    /// Saves the layout
    /// </summary>
    /// <returns>Returns true if successful</returns>
    private bool SaveLayout()
    {
        string codeName = tbCodeName.Text.Trim();
        // Find out whether required fields are not empty
        string result = new Validator().NotEmpty(codeName, ResHelper.GetString("Administration-PageLayout_New.ErrorEmptyLayoutCodeName")).NotEmpty(tbLayoutDisplayName.Text, ResHelper.GetString("Administration-PageLayout_New.ErrorEmptyLayoutDisplayName")).NotEmpty(codeName, ResHelper.GetString("Administration-PageLayout_New.ErrorEmptyLayoutCode"))
            .IsCodeName(codeName, ResHelper.GetString("general.invalidcodename"))
            .Result;

        if (result == "")
        {
            LayoutInfo li = LayoutInfoProvider.GetLayoutInfo(layoutId);
            if (li == null)
            {
                li = new LayoutInfo();
            }
            li.LayoutCodeName = codeName;
            li.LayoutDisplayName = tbLayoutDisplayName.Text.Trim();
            li.LayoutDescription = tbLayoutDescription.Text;
            if (li.LayoutCheckedOutByUserID <= 0)
            {
                li.LayoutCode = tbLayoutCode.Text;
            }

            try
            {
                LayoutInfoProvider.SetLayoutInfo(li);
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                pageTitleTabs[1, 0] = li.LayoutDisplayName;
            }
            catch (Exception ex)
            {
                lblInfo.Visible = false;
                lblError.Visible = true;
                lblError.Text = ex.Message.Replace("%%name%%", li.LayoutCodeName);
                return false;
            }
            layoutId = li.LayoutId;
            return !UploadFile.SavingFailed;
        }
        else
        {
            lblError.Text = result;
            lblError.Visible = true;
            return false;
        }
    }


    /// <summary>
    /// Loads data of edited layout from DB into TextBoxes.
    /// </summary>
    protected void LoadData()
    {
        LayoutInfo li = LayoutInfoProvider.GetLayoutInfo(layoutId);

        if (li != null)
        {
            this.pnlCheckOutInfo.Visible = true;
            tbLayoutDisplayName.Text = li.LayoutDisplayName;
            tbLayoutDescription.Text = li.LayoutDescription;
            tbLayoutCode.Text = li.LayoutCode;
            tbCodeName.Text = li.LayoutCodeName;

            if (li.LayoutCheckedOutByUserID > 0)
            {
                this.tbLayoutCode.ReadOnly = true;
                string username = null;
                UserInfo ui = UserInfoProvider.GetUserInfo(li.LayoutCheckedOutByUserID);
                if (ui != null)
                {
                    username = HTMLHelper.HTMLEncode(ui.FullName);
                }

                // Checked out by current machine
                if (li.LayoutCheckedOutMachineName.ToLower() == HTTPHelper.MachineName.ToLower())
                {
                    this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("PageLayout.CheckedOut"), Server.MapPath(li.LayoutCheckedOutFilename));
                }
                else
                {
                    this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("PageLayout.CheckedOutOnAnotherMachine"), li.LayoutCheckedOutMachineName, username);
                }
            }
            else
            {
                this.lblCheckOutInfo.Text = String.Format(ResHelper.GetString("PageLayout.CheckOutInfo"), Server.MapPath(LayoutInfoProvider.GetLayoutUrl(li.LayoutCodeName, null)));
            }
        }
    }
}
