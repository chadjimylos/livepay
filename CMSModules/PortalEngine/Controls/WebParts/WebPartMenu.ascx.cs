using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.PortalControls;
using CMS.PortalEngine;

public partial class CMSModules_PortalEngine_Controls_WebParts_WebPartMenu : CMSAbstractPortalUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Use UI culture for strings
        string culture = CMSContext.CurrentUser.PreferredUICultureCode;

        // Main menu
        this.imgProperties.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/ContextMenu/Properties.png");
        this.lblProperties.Text = ResHelper.GetString("WebPartMenu.IconProperties", culture);
        this.pnlProperties.Attributes.Add("onclick", "ContextConfigureWebPart(GetContextMenuParameter('webPartMenu'));");

        // Up
        this.imgUp.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/ContextMenu/Up.png");
        this.lblUp.Text = ResHelper.GetString("WebPartMenu.IconUp", culture);
        this.imgUp.AlternateText = this.lblUp.Text;
        this.pnlUp.Attributes.Add("onclick", "ContextMoveWebPartUp(GetContextMenuParameter('webPartMenu'));");

        // Down
        this.imgDown.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/ContextMenu/Down.png");
        this.lblDown.Text = ResHelper.GetString("WebPartMenu.IconDown", culture);
        this.imgDown.AlternateText = this.lblDown.Text;
        this.pnlDown.Attributes.Add("onclick", "ContextMoveWebPartDown(GetContextMenuParameter('webPartMenu'));");

        // Move to
        this.imgMoveTo.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/ContextMenu/MoveTo.png");
        this.lblMoveTo.Text = ResHelper.GetString("WebPartMenu.IconMoveTo", culture);

        // Clone
        this.imgClone.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/ContextMenu/Clonewebpart.png");
        this.lblClone.Text = ResHelper.GetString("WebPartMenu.IconClone", culture);
        this.imgClone.AlternateText = this.lblClone.Text;
        this.pnlClone.Attributes.Add("onclick", "ContextCloneWebPart(GetContextMenuParameter('webPartMenu'));");

        // Delete
        this.imgDelete.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/ContextMenu/Delete.png");
        this.lblDelete.Text = ResHelper.GetString("general.remove", culture);
        this.imgDelete.AlternateText = this.lblDelete.Text;
        this.pnlDelete.Attributes.Add("onclick", "ContextRemoveWebPart(GetContextMenuParameter('webPartMenu'));");

        // Up menu - Bottom
        this.lblTop.Text = ResHelper.GetString("UpMenu.IconTop", culture);
        this.pnlTop.Attributes.Add("onclick", "ContextMoveWebPartTop(GetContextMenuParameter('webPartMenu'));");

        // Down menu - Bottom
        this.lblBottom.Text = ResHelper.GetString("DownMenu.IconBottom", culture);
        this.pnlBottom.Attributes.Add("onclick", "ContextMoveWebPartBottom(GetContextMenuParameter('webPartMenu'));");
        
        if (this.PortalManager.CurrentPlaceholder != null)
        {
            // Build the list of web part zones
            ArrayList webPartZones = new ArrayList();

            foreach (CMSWebPartZone zone in this.PortalManager.CurrentPlaceholder.WebPartZones)
            {
                // Add only standard zones to the list
                if (zone.ZoneInstance.WidgetZoneType == WidgetZoneTypeEnum.None)
                {
                    webPartZones.Add(zone);
                }
            }

            this.repZones.DataSource = webPartZones;
            this.repZones.DataBind();
        }
    }
}
