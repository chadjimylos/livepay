<%@ Page Language="C#" AutoEventWireup="true" CodeFile="System_ViewObject.aspx.cs"
    Inherits="CMSSiteManager_Administration_System_Debug_System_ViewObject" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master" MaintainScrollPositionOnPostback="true" %>

<%@ Register Src="~/CMSAdminControls/UI/System/ViewObject.ascx" TagName="ViewObject"
    TagPrefix="uc1" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Panel runat="server" ID="pnlBody" CssClass="PageContent">
        <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" />
        <uc1:ViewObject ID="objElem" runat="server" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="cntFooter" ContentPlaceHolderID="plcFooter" runat="server">
    <div class="FloatRight">
        <cms:LocalizedButton ID="btnCancel" runat="server" ResourceString="General.Close"
            CssClass="SubmitButton" OnClientClick="window.close(); return false;" EnableViewState="false" />
    </div>
</asp:Content>
