<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Blogs/BlogCommentView.ascx.cs" Inherits="CMSWebParts_Blogs_BlogCommentView" %>
<%@ Register Src="~/CMSModules/Blogs/Controls/BlogCommentView.ascx" TagName="BlogCommentView" TagPrefix="cms" %>

<cms:BlogCommentView ID="commentView" runat="server" />
