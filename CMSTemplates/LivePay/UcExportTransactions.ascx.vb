﻿Imports System.Data
Imports CMS.DataEngine
Imports CMS.SiteProvider
Imports System.Collections.Generic

Partial Class CMSTemplates_LivePay_UcExportTransactions
    Inherits System.Web.UI.UserControl

    Private strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        InitializeForm()
    End Sub

    Private Sub InitializeForm()
        If (Not RequestHelper.IsPostBack()) Then
            Me.drpFilterClientIP.Items.Add(New ListItem("LIKE", "LIKE"))
            Me.drpFilterClientIP.Items.Add(New ListItem("NOT LIKE", "NOT LIKE"))
            Me.drpFilterClientIP.Items.Add(New ListItem("=", "="))
            Me.drpFilterClientIP.Items.Add(New ListItem("<>", "<>"))
        End If
    End Sub

    Protected Sub lnkExportExcel_Click(sender As Object, e As System.EventArgs) Handles lnkExportExcel.Click
        Dim admin As UserInfo = UserInfoProvider.GetUserInfo("administrator")
        Dim gn As GeneralConnection = ConnectionHelper.GetConnection(strConnection, admin.UserID)

        Dim parameters As Object(,) = New Object(0, 2) {}
        parameters(0, 0) = "@where"
        parameters(0, 1) = AdvancedSearch()

        'Response.Write(AdvancedSearch)
        'Response.End()

        ''execute stored procedure
        Dim ds As DataSet = gn.ExecuteQuery("LivePay_Export_MerchantTransactions", parameters, CMS.IDataConnectionLibrary.QueryTypeEnum.StoredProcedure, False)

        Dim dt As DataTable = ds.Tables(0)

        For Each row As DataRow In dt.Rows
            Dim CardNo As String = row("Card No").ToString()
            If CardNo.Length > 4 Then
                Dim DecryptedCard As String = Left(CardNo, 4) & CryptoFunctions.DecryptString(Right(CardNo, CardNo.Length - 4))
                row("Card No") = Left(DecryptedCard, 6) & "**********"
            End If
        Next

        ExportToExcel(dt)

    End Sub

    Private Function AdvancedSearch() As String
        Dim whereCond As String = String.Empty

        Dim clientIPWhere As String = GetWherePart("ClientIP", Me.tbClientIP.Text, Me.drpFilterClientIP.SelectedValue)
        Dim dateToWhere As String = If(String.IsNullOrEmpty(Me.ucDateTo.Value), DateTime.Now, Me.ucDateTo.Value)

        Dim dateFromWhere As String = If(String.IsNullOrEmpty(Me.ucDateFrom.Value), "", QueryBetweenDate(Me.ucDateFrom.Value, dateToWhere))
        'Dim merchantIdWhere As String = If(String.IsNullOrEmpty(Me.drpMerchantName.SelectedValue), "", GetWherePart("MerchantID", Me.drpMerchantName.SelectedValue, "="))

        ' Final where condition
        whereCond = (If(String.IsNullOrEmpty(clientIPWhere), "", " AND " & clientIPWhere)) & _
        (If(String.IsNullOrEmpty(dateFromWhere), "", " AND " & dateFromWhere))

        ' Remove first AND
        If whereCond <> "" Then
            whereCond = whereCond.Substring(5)
        End If

        Return whereCond
    End Function

    Public Sub ExportToExcel(dt As DataTable)
        If dt.Rows.Count > 0 Then

            Dim filename As String = String.Format("ExportTransactions_{0}.xls", DateTime.Now.ToString("yyyyMMddHHmmss"))
            Dim tw As New System.IO.StringWriter()
            Dim hw As New System.Web.UI.HtmlTextWriter(tw)
            Dim dgGrid As New DataGrid()
            dgGrid.DataSource = dt
            dgGrid.DataBind()

            'Get the HTML for the control.
            dgGrid.RenderControl(hw)
            'Write the HTML back to the browser.
            Response.Clear()
            Response.AppendHeader("Content-Disposition", "attachment; filename=" & filename & "")
            Response.ContentType = "application/vnd.ms-excel"
            Response.ContentEncoding = System.Text.Encoding.Unicode
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble())
            Me.EnableViewState = False
            Response.Write(tw.ToString())
            Response.End()
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "Error_" & Guid.NewGuid().ToString, "alert('Δεν βρέθηκαν αποτελέσματα.')", True)
        End If
    End Sub

    Private Function QueryBetweenDate(DateFrom As String, DateTo As String) As String
        Dim where As String

        Try
            Dim localDateFrom As New DateTime(DateFrom.Substring(4, 4), DateFrom.Substring(2, 2), DateFrom.Substring(0, 2))
            Dim localDateTo As New DateTime(DateTo.Substring(4, 4), DateTo.Substring(2, 2), DateTo.Substring(0, 2))
            where = " mt.TransactionDate BETWEEN '" & localDateFrom.ToString("MM/dd/yyyy") & "' AND '" & localDateTo.ToString("MM/dd/yyyy") & "' "
        Catch ex As Exception
            Response.Write(ex.Message.ToString)
            Response.End()
            Return ""
        End Try


        Return where
    End Function

    ''' <summary>
    ''' Returns part of the wherecondition for given column.
    ''' </summary>
    ''' <param name="column">Column name</param>
    ''' <param name="value">Value to be searched</param>
    ''' <param name="op">Operator to condition</param>
    Private Function GetWherePart(column As String, value As String, op As String) As String
        ' Avoid SQL Injenction
        Dim tempVal As String = value.Trim().Replace("'", "''")

        ' No condition
        If tempVal = "" Then
            Return Nothing
        End If

        Dim where As String = "(" & column & " {0} N'{1}')"

        ' Support for exact phrase search
        If op.Contains("LIKE") Then
            tempVal = "%" & tempVal & "%"
        End If

        ' Get final where condition
        where = [String].Format(where, op, tempVal)

        Return where
    End Function

End Class
