﻿/*
 * Initialize global variables
 */
$j(function(){
    InitResizers();
});

function InitGlobalVariables(){
    window.mainBlockElem = $j('.DialogMainBlock');
    window.leftBlockElem = $j('.DialogLeftBlock');
    window.separatorElem = $j('.DialogTreeAreaSeparator');
    window.rightBlockElem = $j('.DialogRightBlock');
    window.resizerElemV = $j('.DialogResizerVLine');
    window.resizerElemH = $j('.DialogResizerH');
    window.resizerArrowV = $j('.DialogResizerArrowV');
    window.propElem = $j('.DialogProperties');
    window.previewObj = $j('.DialogPropertiesPreview');
    window.propertiesFullObj = $j('.DialogPropertiesFullSize');
    window.webContentObj = $j('.DialogWebContent');
    window.webPropertiesObj = $j('.DialogWebProperties');
    window.menuElem = $j('.DialogMenu');
    window.viewElem = $j('.DialogViewContent');
    window.titleElem = $j('.DialogHeader');
    window.bodyObj = $j('body');
    if ($j('.DialogMainBlock').length > 0) {
        window.containerElem = $j('.DialogMainBlock').offsetParent();
    }
    else {
        window.containerElem = window.bodyObj;
    }
    window.isSafari = bodyObj.hasClass('Safari');
    window.isGecko = bodyObj.hasClass('Gecko');
    window.isOpera = bodyObj.hasClass('Opera');
    window.isRTL = bodyObj.hasClass('RTL');
    window.resizerLineHeight = 7;
}

/*
 * Resizer handling
 */
function InitResizers(){
    $j('.DialogResizerArrowH').unbind('click');
    $j('.DialogResizerArrowH').click(function(){
        InitGlobalVariables();
        ResetListItemWidth();
        var thisElem = $j(this);
        var imgUrl = thisElem.css('background-image');
        imgUrl = imgUrl.replace('/RTL/Design/', '/Design/');
        imgUrl = imgUrl.replace(/\/minimize\./i, '/##status##.');
        imgUrl = imgUrl.replace(/\/maximize\./i, '/##status##.');
        var minimizeLTR = imgUrl.replace('/##status##.', '/minimize.');
        var maximizeLTR = imgUrl.replace('/##status##.', '/maximize.');
        var minimizeRTL = minimizeLTR.replace('/Design/', '/RTL/Design/');
        var maximizeRTL = maximizeLTR.replace('/Design/', '/RTL/Design/');
        var parentWidth = '233px';
        var thisSideUp = '233px';
        var thisSideDown = '0px';
        if (isRTL) {
            if (thisElem.hasClass('ResizerDown')) {
                var width = mainBlockElem.innerWidth() - separatorElem.outerWidth() - leftBlockElem.outerWidth();
                rightBlockElem.width(width);
                resizerElemH.css('margin-right', '0px');
                leftBlockElem.css('margin-right', '0px');
                thisElem.css({
                    'right': thisSideUp,
                    'background-image': minimizeRTL
                });
                if ((!isSafari) && (!isOpera)) {
                    rightBlockElem.css('margin-right', '243px');
                }
                thisElem.removeClass('ResizerDown');
            }
            else {
                var width = mainBlockElem.innerWidth() - 10;
                rightBlockElem.width(width);
                leftBlockElem.css('margin-right', '-' + parentWidth);
                resizerElemH.css('margin-right', '-' + parentWidth);
                thisElem.css({
                    'right': thisSideDown,
                    'background-image': maximizeRTL
                });
                if ((!isSafari) && (!isOpera)) {
                    rightBlockElem.css('margin-right', '10px');
                }
                thisElem.addClass('ResizerDown');
            }
        }
        else {
            if (thisElem.hasClass('ResizerDown')) {
                if ((!isSafari) && (!isGecko) && (!isOpera)) {
                    rightBlockElem.css('margin-left', parentWidth);
                }
                resizerElemH.css('margin-left', '0px');
                leftBlockElem.css('margin-left', '0px');
                thisElem.css({
                    'left': thisSideUp,
                    'background-image': minimizeLTR
                });
                thisElem.removeClass('ResizerDown');
            }
            else {
                if ((!isSafari) && (!isGecko) && (!isOpera)) {
                    rightBlockElem.css('margin-left', '0px');
                }
                resizerElemH.css('margin-left', '-' + parentWidth);
                leftBlockElem.css('margin-left', '-' + parentWidth);
                thisElem.css({
                    'left': thisSideDown,
                    'background-image': maximizeLTR
                });
                thisElem.addClass('ResizerDown');
            }
        }
        SetListItemWidth();
        SetPreviewBox();
    });
    
    $j('.DialogResizerArrowV').unbind('click');
    $j('.DialogResizerArrowV').click(function(){
        InitGlobalVariables();
        var thisElem = $j(this);
        var imgUrl = thisElem.css('background-image');
        imgUrl = imgUrl.replace(/\/minimize\./i, '/##status##.');
        imgUrl = imgUrl.replace(/\/maximize\./i, '/##status##.');
        var maximize = imgUrl.replace('/##status##.', '/minimize.');
        var minimize = imgUrl.replace('/##status##.', '/maximize.');
        
        if (thisElem.hasClass('ResizerDown')) {
            viewElem.css('height', mainBlockElem.innerHeight() - propElem.outerHeight() - menuElem.outerHeight() - resizerLineHeight);
            propElem.css('display', 'block');
            thisElem.css('background-image', minimize);
            thisElem.removeClass('ResizerDown');
        }
        else {
            viewElem.css('height', mainBlockElem.innerHeight() - menuElem.outerHeight() - resizerLineHeight);
            propElem.css('display', 'none');
            thisElem.css('background-image', maximize);
            thisElem.addClass('ResizerDown');
        }
    });
}

/*
 * Footer handling
 */
function GetSelected(hdnId, hdnAnchors, hdnIds, editorId){
    var selElem = GetSelectedItem(editorId);
    var selected = '';
    if (selElem) {
        for (var i in selElem) {
            selected += i + '|' + selElem[i] + '|';
        }
    }
    if (selected.length > 0) {
        selected = selected.substring(0, selected.length - 1);
        var hdnElement = document.getElementById(hdnId);
        if (hdnElement) {
            hdnElement.value = selected;
        }
    }
    if (typeof GetAnchorNames == 'function') {
        var aAnchors = GetAnchorNames();
        if ((aAnchors != null) && (aAnchors.length > 0)) {
            var sAnchors = '';
            for (i = 0; i < aAnchors.length; i++) {
                sAnchors += escape(aAnchors[i]) + '|';
            }
            if (sAnchors.length > 0) {
                sAnchors = sAnchors.substring(0, sAnchors.length - 1);
                var eAnchors = document.getElementById(hdnAnchors);
                if (eAnchors) {
                    eAnchors.value = sAnchors;
                }
            }
        }
    }
    if (typeof GetIds == 'function') {
        var aIds = GetIds();
        if ((aIds != null) && (aIds.length > 0)) {
            var sIds = '';
            for (i = 0; i < aIds.length; i++) {
                sIds += escape(aIds[i]) + '|';
            }
            if (sIds.length > 0) {
                sIds = sIds.substring(0, sIds.length - 1);
                var eIds = document.getElementById(hdnIds);
                if (eIds) {
                    eIds.value = sIds;
                }
            }
        }
    }
    DoHiddenPostback();
}


/*
 * YouTube properties handling
 */
var youTubeLoaderIsVisible = true;
function Loading(preview, loading){
    $j('.YouTubeLoader').remove();
    var loader = null;
    if ($j('.YouTubeUrl')[0].value == '') {
        loader = $j('<div class="YouTubeLoader"><span>' + preview + '</span></div>');
    }
    else {
        loader = $j('<div class="YouTubeLoader"><span>' + loading + '</span></div>');
    }
    $j('div.YouTubeBox').append(loader);
    youTubeLoaderIsVisible = true;
}

function onYouTubePlayerReady(playerId){
    if (youTubeLoaderIsVisible) {
        $j('div.YouTubeLoader').hide();
        youTubeLoaderIsVisible = false;
    }
}


/*
 * Quickly insert image handling
 */
function InsertImage(url, name, width, height){
    var instance = null;
    
    if (window.parent && window.parent.parent) {
        instance = window.parent.parent.currentFCKInstance;
        if (instance == null) {
            if (window.parent.parent.toolbarElem) {
                // From shared toolbar
                var instance = window.parent.parent.toolbarElem.__FCKToolbarSet.CurrentInstance;
            }
            else 
                if (window.parent.parent.FCKeditorAPI) {
                    var match = window.parent.location.search.match(/InstanceName=([^&]*)/i);
                    if (match[1] != null) {
                        var instance = window.parent.parent.FCKeditorAPI.GetInstance(match[1]);
                    }
                }
        }
    }
    
    if (instance != null) {
        url = url.toString().replace(/~/, '');
        name = name.toString().replace(/'/, '\'');
        
        instance.disableNextBlur = true;
        instance.Selection.Restore();
        instance.InsertHtml('<img src="' + url + '" alt="' + name + '" style="width:' + width + 'px; height:' + height + 'px;" />');
        instance.wasSaved = false;
        
        var quickFrame = instance.ToolbarSet.quickFrame;
        if (quickFrame != null) {
            quickFrame.contentWindow.mouseIsOnDirectUploader = false;
        }
    }
}


/*
 * Design scripts
 */
function SetListItemWidth(){
    // Dialog list name row IE6 
    var listItemsObj = $j('.DialogListItem');
    if (listItemsObj.length > 0) {
        var listItemCell = listItemsObj.parent();
        listItemsObj.width(listItemCell.width());
    }
}

function ResetListItemWidth(){
    var listItemsObj = $j('.DialogListItem');
    if (listItemsObj.length > 0) {
        listItemsObj.width(100);
    }
}

function SetPreviewBox(){
    if (previewObj.length > 0) {
        var previewTd = previewObj.parents('td');
        var previewWidth = 0;
        if (previewTd.length > 0) {
            previewWidth = previewTd.width() - 20;
        }
        if (webContentObj.length > 0) {
            previewObj.width(previewWidth);
            previewObj.height(webPropertiesObj.height() - 60);
        }
        else {
            // Width
            previewObj.width(previewWidth);
            // Height
            if (propertiesFullObj.length > 0) {
                previewObj.height(mainBlockElem.height() - 140);
            }
            else {
                previewObj.height(210);
            }
        }
        previewObj.css('display', 'block');
    }
}

function InitializeDesign(){

    InitGlobalVariables();
    ResetListItemWidth();
    
    var menuHeight = menuElem.outerHeight();
    // Dialog view content height
    if (viewElem.length > 0) {
        var mainBlockHeight = containerElem.height() - titleElem.outerHeight();
        mainBlockElem.height(mainBlockHeight);
        var mainHeight = mainBlockHeight - menuHeight;
        var propHeight = propElem.outerHeight();
        if (resizerArrowV.hasClass('ResizerDown')) {
            viewElem.height(mainHeight - resizerLineHeight);
        }
        else 
            if (viewElem.height() != (mainHeight - propHeight)) {
                viewElem.height(mainHeight - propHeight - resizerLineHeight);
            }
    }
    // Dialog tree height
    var treeAreaObj = $j('.DialogTreeArea');
    var siteBlockObj = $j('.DialogSiteBlock');
    var mediaLibraryBlockObj = $j('.DialogMediaLibraryBlock');
    if (mediaLibraryBlockObj.length == 0) {
        mediaLibraryBlockObj = $j('.MediaLibraryFolderActions');
    }
    var mediaLibraryTreeBlockObj = $j('.DialogMediaLibraryTreeArea');
    
    if (mainBlockElem.length > 0) {
        var treeHeight = 0;
        if (siteBlockObj.length > 0) {
            // Content tree
            treeHeight = mainBlockElem.innerHeight() - menuHeight - siteBlockObj.outerHeight();
        }
        else 
            if (mediaLibraryBlockObj.length > 0) {
                // Media library tree
                treeHeight = mainBlockElem.innerHeight() - menuHeight - mediaLibraryBlockObj.outerHeight();
            }
            else {
                // Copy/Move tree
                treeHeight = mainBlockElem.innerHeight() - menuHeight;
            }
        if (rightBlockElem.length > 0) {
            var rightWidth = 0;
            if ($j('.DialogResizerArrowH').hasClass('ResizerDown')) {
                rightWidth = mainBlockElem.innerWidth() - 10;
            }
            else {
                rightWidth = mainBlockElem.innerWidth() - treeAreaObj.outerWidth() - separatorElem.outerWidth();
                // Fix IE6 double margin bug
                if ($j.browser.msie && jQuery.browser.version == '6.0') {
                    rightWidth = rightWidth - 5;
                }
            }            
            if (rightBlockElem.width() != rightWidth) {
                rightBlockElem.width(rightWidth);
            }
        }
        if ((treeAreaObj.length > 0) && (treeAreaObj.height() != treeHeight)) {
            treeAreaObj.height(treeHeight);
        }
        if (mediaLibraryTreeBlockObj.length > 0) {
            var diferent = mediaLibraryTreeBlockObj.outerHeight() - mediaLibraryTreeBlockObj.height();
            if (mediaLibraryTreeBlockObj.height() != (treeHeight - diferent)) {
                mediaLibraryTreeBlockObj.height(treeHeight - diferent);
            }
        }
        else {
            var contentTreeObj = $j('.ContentTree');
            var diferent = contentTreeObj.outerHeight() - contentTreeObj.height();
            treeHeight = treeHeight - diferent;
            if (contentTreeObj.height() != treeHeight) {
                contentTreeObj.height(treeHeight);
            }
        }
    }
    // Dialog preview box
    SetPreviewBox();
    // Dialog list name row IE6
    SetListItemWidth();
}
