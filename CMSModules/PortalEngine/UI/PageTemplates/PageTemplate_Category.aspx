<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageTemplate_Category.aspx.cs" 
    Inherits="CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_Category" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Development - Page template category" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" id="lblInfo" CssClass="InfoLabel" EnableViewState="false" />
    <asp:Label runat="server" ID="lblError" ForeColor="red" EnableViewState="false" Visible="false" />
    <asp:Literal ID="ltlScript" runat="server" />
    <table>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblCategoryDisplayName" runat="server" EnableViewState="false" /> 
        </td>
        <td>
            <asp:TextBox ID="txtCategoryDisplayName" runat="server" CssClass="TextBoxField" MaxLength="100" />
            <asp:RequiredFieldValidator ID="rfvCategoryDisplayName" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtCategoryDisplayName" />
        </td>
    </tr>
    <asp:PlaceHolder runat="server" ID="plcCategoryName">
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCategoryName" runat="server" EnableViewState="false" /> 
            </td>
            <td>
                <asp:TextBox ID="txtCategoryName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtCategoryName" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td class="FieldLabel">
            <cms:LocalizedLabel ID="lblCategoryImagePath" runat="server" EnableViewState="false" ResourceString="pagetemplatecategory.imagepath" DisplayColon="true" />
        </td>
        <td>
            <asp:TextBox ID="txtCategoryImagePath" runat="server" CssClass="TextBoxField" MaxLength="450" />                
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton" />
        </td>
    </tr>   
    </table>
</asp:Content>
