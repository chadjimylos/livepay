<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GroupSecurity.ascx.cs"
    Inherits="CMSModules_Groups_Controls_Security_GroupSecurity" %>
<%@ Register Src="~/CMSAdminControls/UI/UniControls/UniMatrix.ascx" TagName="UniMatrix" TagPrefix="cms" %>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
<asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<asp:Table runat="server" ID="tblMatrix" GridLines="horizontal" CssClass="PermissionMatrix"
    CellPadding="3" EnableViewState="false" CellSpacing="0">
</asp:Table>
<br />
<cms:UniMatrix ID="gridMatrix" runat="server" QueryName="Community.GroupRolePermission.getpermissionMatrix"
    RowItemIDColumn="RoleID" ColumnItemIDColumn="PermissionID" RowItemDisplayNameColumn="RoleDisplayName"
    ColumnItemDisplayNameColumn="PermissionDisplayName" RowTooltipColumn="RowDisplayName"
    ColumnTooltipColumn="PermissionDescription" ItemTooltipColumn="PermissionDescription"
    FirstColumnsWidth="30" FixedWidth="18" UsePercentage="true" />
