﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCAccountActivation.ascx.vb" Inherits="CMSTemplates_LivePay_UcAccountActivation" %>
<script language="javascript" type="text/javascript" >
</script>
<div class="CUDDarkBlueBGTitle">
<div class="SvdCardsTopTitle">Ενεργοποίηση Λογαριασμού</div>
<div class="CUDContentBG">
<div style="padding-left: 10px; padding-top: 10px">
<div class="AccActMainTitleBG">
<div class="AccActGreenTitleText"><asp:label ID="lblResultTitle" runat="server" /></div>
</div>
</div>
<div class="AccActContent"><asp:label ID="lblInfo" runat="server">Μπορείτε να πραγματοποιήσετε είσοδο (login) με το e-mail και τον κωδικό πρόσβασης σας απο την περιοχή που βρίσκεστε στο αριστερό μέρος αυτής της οθόνης!</asp:label></div>
</div>
<div><img alt="" src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
</div>