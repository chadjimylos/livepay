<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectRole.ascx.cs" Inherits="CMSModules_Membership_FormControls_Roles_SelectRole" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ObjectType="cms.role" SelectionMode="MultipleTextBox"
            OrderBy="RoleDisplayName" ResourcePrefix="roleselect" runat="server"
            ID="usRoles" AllowEditTextBox="true" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
