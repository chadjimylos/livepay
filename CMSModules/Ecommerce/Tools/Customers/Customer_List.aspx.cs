using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Customers_Customer_List : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        InitializeMasterPage();

        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Initializes properties of the master page
    /// </summary>
    private void InitializeMasterPage()
    {
        this.CurrentMaster.HeaderActions.HelpTopicName = "customers_list";
        this.CurrentMaster.HeaderActions.HelpName = "helpTopic";

        // Initialize the new customer element
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("Customers_List.NewItemCaption");
        actions[0, 3] = "~/CMSModules/Ecommerce/Tools/Customers/Customer_New.aspx";
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_Customer/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    #region "UniGrid Events"

    protected object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "custenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
            case "statedisplayname":
            case "countrydisplayname":
                return ResHelper.LocalizeString(Convert.ToString(parameter));
            //break;
        }

        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Customer_Edit_Frameset.aspx?customerid=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // check 'EcommerceModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
            }

            if (CustomerInfoProvider.CheckDependencies(ValidationHelper.GetInteger(actionArgument, 0)))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                return;
            }

            // delete CustomerInfo object from database
            CustomerInfoProvider.DeleteCustomerInfo(ValidationHelper.GetInteger(actionArgument, 0));

            UniGrid.ReloadData();
        }
    }

    #endregion
}
