<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User_Edit_Settings.aspx.cs"
    Inherits="CMSSiteManager_Administration_Users_User_Edit_Settings" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="User edit - Custom fields" %>

<%@ Register Src="~/CMSModules/Membership/FormControls/Users/UserPictureEdit.ascx"
    TagName="UserPictureFormControl" TagPrefix="upfc" %>
<%@ Register Src="~/CMSFormControls/TimeZones/TimeZoneSelector.ascx" TagName="TimeZoneSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Badges/FormControls/BadgeSelector.ascx" TagName="BadgeSelector"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:PlaceHolder ID="plcTable" runat="server">
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblNickName" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtNickName" MaxLength="200" runat="server" CssClass="TextBoxField" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserPicture" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <upfc:UserPictureFormControl ID="UserPictureFormControl" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserSignature" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtUserSignature" CssClass="TextAreaField" TextMode="MultiLine" />
                </td>
            </tr>
            <tr>
                <td>
                    <cms:LocalizedLabel ID="lblUserDescription" runat="server" EnableViewState="false"
                        ResourceString="Administration-User_Edit_General.UserDescription" DisplayColon="true" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtUserDescription" CssClass="TextAreaField" TextMode="MultiLine" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblURLReferrer" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtURLReferrer" MaxLength="450" runat="server" CssClass="TextBoxField" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCampaign" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtCampaign" MaxLength="200" runat="server" CssClass="TextBoxField" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMessageNotifEmail" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtMessageNotifEmail" MaxLength="200" runat="server" CssClass="TextBoxField" />
                </td>
            </tr>
            <%--        <tr>
            <td>
                <asp:Label ID="lblCustomData" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCustomData" runat="server" CssClass="TextAreaField" TextMode="MultiLine" />
            </td>
        </tr>--%>
            <%--        <tr>
            <td>
                <asp:Label ID="lblPreferences" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtPreferences" runat="server" CssClass="TextAreaField" TextMode="MultiLine" />
            </td>
        </tr>--%>
            <%--       <tr>
            <td>
                <asp:Label ID="lblVisibility" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtVisibility" runat="server" CssClass="TextAreaField" TextMode="MultiLine" />
            </td>
        </tr>--%>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblTimeZone" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <cms:TimeZoneSelector ID="timeZone" runat="server" UseZoneNameForSelection="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblBadge" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <cms:BadgeSelector ID="badgeSelector" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserActivityPoints" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtUserActivityPoints" MaxLength="9" runat="server" CssClass="TextBoxField" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserLiveID" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtUserLiveID" MaxLength="50" runat="server" CssClass="TextBoxField" />
                </td>
            </tr>
            <tr>
                <td>
                    <cms:LocalizedLabel ID="lblFacebookUserID" runat="server" EnableViewState="false"
                        DisplayColon="true" ResourceString="adm.user.lblfacebookid" />
                </td>
                <td>
                    <asp:TextBox ID="txtFacebookUserID" MaxLength="100" runat="server" CssClass="TextBoxField" />
                </td>
            </tr>
            <tr>
                <td>
                    <cms:LocalizedLabel ID="lblUserOpenID" runat="server" EnableViewState="false" DisplayColon="true" />
                </td>
                <td>
                    <asp:TextBox ID="txtOpenID" runat="server" MaxLength="450" CssClass="TextBoxField" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblActivationDate" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <cms:DateTimePicker ID="activationDate" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblActivatedByUser" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:Label ID="lblUserFullName" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblRegInfo" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:PlaceHolder runat="server" ID="plcUserLastLogonInfo" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserGender" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:RadioButtonList ID="rbtnlGender" runat="server" RepeatDirection="Horizontal" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserDateOfBirth" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <cms:DateTimePicker ID="dtUserDateOfBirth" runat="server" EditTime="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblWaitingForActivation" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:CheckBox ID="chkWaitingForActivation" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserShowSplashScreen" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:CheckBox ID="chkUserShowSplashScreen" runat="server" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserForumPosts" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:Label ID="lblUserForumPostsValue" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserBlogPosts" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:Label ID="lblUserBlogPostsValue" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserBlogComments" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:Label ID="lblUserBlogCommentsValue" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblUserMessageBoardPosts" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:Label ID="lblUserMessageBoardPostsValue" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <cms:CMSButton ID="btnOk" runat="server" OnClick="ButtonOK_Click" CssClass="SubmitButton"
                        EnableViewState="false" />
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>
</asp:Content>
