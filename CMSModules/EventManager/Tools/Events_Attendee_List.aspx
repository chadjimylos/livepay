<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Events_Attendee_List.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Attendee list" Inherits="CMSModules_EventManager_Tools_Events_Attendee_List"
    Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Events_Attendee_List.xml" OrderBy="AttendeeEmail"
        IsLiveSite="false" />
    <asp:Literal ID="ltlScript" runat="server" />
</asp:Content>
