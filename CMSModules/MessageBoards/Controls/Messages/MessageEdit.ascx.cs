using System;
using System.Data;
using System.Web;
using System.Web.UI;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.MessageBoard;
using CMS.UIControls;
using CMS.ExtendedControls;
using CMS.LicenseProvider;
using CMS.SettingsProvider;

public partial class CMSModules_MessageBoards_Controls_Messages_MessageEdit : CMSAdminEditControl
{
    #region "Events"

    public event OnAfterMessageSavedEventHandler OnAfterMessageSaved;
    public event OnBeforeMessageSavedEventHandler OnBeforeMessageSaved;

    #endregion


    #region "Variables"

    private bool mAdvancedMode = false;
    private bool mCheckFloodProtection = false;
    private int mMessageID = 0;
    private int mMessageBoardID = 0;
    private bool mModalMode = false;

    private AbstractRatingControl ratingControl = null;
    private BoardProperties mBoardProperties = new BoardProperties();
    private BoardMessageInfo messageInfo = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Advance mode
    /// </summary>
    public bool AdvancedMode
    {
        get
        {
            return this.mAdvancedMode;
        }
        set
        {
            this.mAdvancedMode = ValidationHelper.GetBoolean(value, false);
        }
    }


    /// <summary>
    /// Advance mode
    /// </summary>
    public bool CheckFloodProtection
    {
        get
        {
            return this.mCheckFloodProtection;
        }
        set
        {
            this.mCheckFloodProtection = value;
        }
    }


    /// <summary>
    /// Message Id
    /// </summary>
    public int MessageID
    {
        get
        {
            return this.mMessageID;
        }
        set
        {
            this.mMessageID = ValidationHelper.GetInteger(value, 0);
        }
    }


    /// <summary>
    /// Message board Id
    /// </summary>
    public int MessageBoardID
    {
        get
        {
            if (this.mMessageBoardID == 0)
            {
                this.mMessageBoardID = (messageInfo != null) ? messageInfo.MessageBoardID : 0;
            }
            return this.mMessageBoardID;
        }
        set
        {
            this.mMessageBoardID = ValidationHelper.GetInteger(value, 0);
        }
    }


    /// <summary>
    /// Message board properties
    /// </summary>
    public BoardProperties BoardProperties
    {
        get
        {
            return this.mBoardProperties;
        }
        set
        {
            this.mBoardProperties = value;
        }
    }


    /// <summary>
    /// Indicates if message edit is in modal dialog.
    /// </summary>
    public bool ModalMode
    {
        get
        {
            return this.mModalMode;
        }
        set
        {
            this.mModalMode = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Let parent check 'Modify' permission if required
        if (!RaiseOnCheckPermissions(PERMISSION_MODIFY, this))
        {
            // Parent page doesn't check permissions
        }

        this.SetContext();

        // Initialize the controls
        SetupControls();

        // Reload data if necessary
        if (!UrlHelper.IsPostback())
        {
            ReloadData();
        }

        this.ReleaseContext();
    }


    #region "Events handling"

    protected void ratingControl_RatingEvent(AbstractRatingControl sender)
    {
        ViewState["ratingvalue"] = sender.CurrentRating;
    }


    protected void btnOk_Click(object sender, EventArgs e)
    {
        // Let the parent control now new message is being saved
        if (OnBeforeMessageSaved != null)
        {
            OnBeforeMessageSaved();
        }

        // Check banned ip
        if (!BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.AllNonComplete))
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("General.BannedIP");
            return;
        }

        // Validate form
        string errorMessage = ValidateForm();

        if (errorMessage == "")
        {
            // Check flooding when message being inserted through the LiveSite
            if (this.CheckFloodProtection && this.IsLiveSite && FloodProtectionHelper.CheckFlooding(CMSContext.CurrentSiteName, CMSContext.CurrentUser))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("General.FloodProtection");
                return;
            }

            CurrentUserInfo currentUser = CMSContext.CurrentUser;

            BoardMessageInfo messageInfo = null;
            string filterParams = "?";

            // Check if the messages are displayed for particular board
            if (MessageBoardID > 0)
            {
                filterParams = "?boardid=" + MessageBoardID + "&";
            }

            if (MessageID > 0)
            {
                // Get message info
                messageInfo = BoardMessageInfoProvider.GetBoardMessageInfo(MessageID);
                MessageBoardID = messageInfo.MessageBoardID;
            }
            else
            {
                // Create new info
                messageInfo = new BoardMessageInfo();
            }

            // Setup message info
            messageInfo.MessageEmail = txtEmail.Text.Trim();
            messageInfo.MessageText = txtMessage.Text.Trim();

            // Handle message URL
            string url = txtURL.Text.Trim();
            if ((url != "http://") && (url != "https://") && (url != ""))
            {
                if ((!url.ToLower().StartsWith("http://")) && (!url.ToLower().StartsWith("https://")))
                {
                    url = "http://" + url;
                }
            }
            else
            {
                url = "";
            }
            messageInfo.MessageURL = url;
            messageInfo.MessageURL = messageInfo.MessageURL.ToLower().Replace("javascript", "_javascript");

            messageInfo.MessageUserName = this.txtUserName.Text.Trim();
            if (!currentUser.IsPublic())
            {
                messageInfo.MessageUserID = currentUser.UserID;
            }

            messageInfo.MessageIsSpam = ValidationHelper.GetBoolean(this.chkSpam.Checked, false);

            if (this.BoardProperties.EnableContentRating && (ratingControl != null) &&
                (ratingControl.GetCurrentRating() > 0))
            {
                messageInfo.MessageRatingValue = ratingControl.CurrentRating;
            }
            messageInfo.MessageUserInfo = string.Empty;

            BoardInfo boardInfo = null;

            // If there is message board
            if (MessageBoardID > 0)
            {
                // Load message board
                boardInfo = BoardInfoProvider.GetBoardInfo(MessageBoardID);
            }
            else
            {
                // Create new message board according to webpart properties
                boardInfo = new BoardInfo(this.BoardProperties);
                BoardInfoProvider.SetBoardInfo(boardInfo);

                // Update information on current message board
                this.MessageBoardID = boardInfo.BoardID;

                // Set board-role relationship                
                BoardRoleInfoProvider.SetBoardRoles(this.MessageBoardID, this.BoardProperties.BoardRoles);

                // Set moderators
                BoardModeratorInfoProvider.SetBoardModerators(this.MessageBoardID, this.BoardProperties.BoardModerators);
            }

            if (boardInfo != null)
            {
                // If the very new message is inserted
                if (this.MessageID == 0)
                {
                    // If creating message set inserted to now and assign to board
                    messageInfo.MessageInserted = currentUser.DateTimeNow;
                    messageInfo.MessageBoardID = MessageBoardID;

                    // Handle auto approve action
                    bool isAuthorized = BoardInfoProvider.IsUserAuthorizedToManageMessages(boardInfo);
                    if (isAuthorized)
                    {
                        messageInfo.MessageApprovedByUserID = currentUser.UserID;
                        messageInfo.MessageApproved = true;
                    }
                    else
                    {
                        // Is board moderated ?
                        messageInfo.MessageApprovedByUserID = 0;
                        messageInfo.MessageApproved = !boardInfo.BoardModerated;
                    }
                }
                else
                {
                    if (this.chkApproved.Checked)
                    {
                        // Set current user as approver
                        messageInfo.MessageApproved = true;
                        messageInfo.MessageApprovedByUserID = currentUser.UserID;
                    }
                    else
                    {
                        messageInfo.MessageApproved = false;
                        messageInfo.MessageApprovedByUserID = 0;
                    }
                }

                if (!AdvancedMode)
                {
                    if (!BadWordInfoProvider.CanUseBadWords(CMSContext.CurrentUser, CMSContext.CurrentSiteName))
                    {
                        // Perform bad words check 
                        errorMessage = BadWordsHelper.CheckBadWords(messageInfo, "MessageText;MessageUserName",
                                                                    "MessageApproved", "MessageApprovedByUserID",
                                                                    messageInfo.MessageText, currentUser.UserID);

                        // Additionaly check empty fields
                        if (errorMessage == string.Empty)
                        {
                            if (!ValidateMessage(messageInfo))
                            {
                                errorMessage = ResHelper.GetString("board.messageedit.emptybadword");
                            }
                        }
                    }
                }

                // Subscribe this user to message board
                if (chkSubscribe.Checked)
                {
                    string email = messageInfo.MessageEmail;

                    // Check for duplicit e-mails
                    DataSet ds = BoardSubscriptionInfoProvider.GetSubscriptions("SubscriptionBoardID=" + this.MessageBoardID +
                        " AND SubscriptionEmail='" + email.Replace("'", "''") + "'", null);
                    if (DataHelper.DataSourceIsEmpty(ds))
                    {
                        BoardSubscriptionInfo bsi = new BoardSubscriptionInfo();
                        bsi.SubscriptionBoardID = this.MessageBoardID;
                        bsi.SubscriptionEmail = email;
                        if (!currentUser.IsPublic())
                        {
                            bsi.SubscriptionUserID = currentUser.UserID;
                        }
                        BoardSubscriptionInfoProvider.SetBoardSubscriptionInfo(bsi);
                        ClearForm();
                    }
                    else
                    {
                        errorMessage = ResHelper.GetString("board.subscription.emailexists");
                    }
                }

                if (errorMessage == "")
                {
                    try
                    {
                        // Save message info
                        BoardMessageInfoProvider.SetBoardMessageInfo(messageInfo);

                        // If the board is moderated let the user know message is waiting for approval
                        if (boardInfo.BoardModerated && (messageInfo.MessageApproved == false))
                        {
                            this.lblInfo.Text = ResHelper.GetString("board.messageedit.waitingapproval");
                            this.lblInfo.Visible = true;
                        }

                        // Rise after message saved event
                        if (OnAfterMessageSaved != null)
                        {
                            OnAfterMessageSaved(messageInfo);
                        }

                        // Clear form content
                        ClearForm();

                        // Get filter parameters
                        filterParams += "site=" + QueryHelper.GetString("site", "") + "&board=" + QueryHelper.GetString("board", "") +
                                    "&username=" + QueryHelper.GetString("username", "") + "&message=" + QueryHelper.GetString("message", "") +
                                    "&approved=" + QueryHelper.GetString("approved", "") + "&isspam=" + QueryHelper.GetString("isspam", "") +
                                    "&changemaster=" + QueryHelper.GetString("changemaster", "");
                    }
                    catch (Exception ex)
                    {
                        errorMessage = ex.Message;
                    }
                }
            }
        }


        if (errorMessage != "")
        {
            lblError.Text = errorMessage;
            lblError.Visible = true;
        }
        else
        {
            // Regenerate security code
            capchaElem.GenerateNewCode();
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes the controls
    /// </summary>
    private void SetupControls()
    {
        lblRating.Text = ResHelper.GetString("board.messageedit.rating");
        lblEmail.Text = ResHelper.GetString("board.messageedit.email");
        lblMessage.Text = ResHelper.GetString("board.messageedit.message");
        lblURL.Text = ResHelper.GetString("board.messageedit.url");
        lblUserName.Text = ResHelper.GetString("board.messageedit.username");

        chkSubscribe.Text = ResHelper.GetString("board.messageedit.subscribe");

        // WAI validation
        lblCapcha.AssociatedControlClientID = capchaElem.InputClientID;

        rfvMessage.ErrorMessage = ResHelper.GetString("board.messageedit.rfvmessage");
        rfvUserName.ErrorMessage = ResHelper.GetString("board.messageedit.rfvusername");
        revEmailValid.ErrorMessage = ResHelper.GetString("board.messageedit.revemail");
        rfvEmail.ErrorMessage = ResHelper.GetString("board.messageedit.rfvemail");

        // Ensure unique validation group name in case of multiple controls in one page
        string valGroup = UniqueID;

        txtUserName.ValidationGroup = valGroup;
        rfvUserName.ValidationGroup = valGroup;

        txtEmail.ValidationGroup = valGroup;
        rfvEmail.ValidationGroup = valGroup;
        revEmailValid.ValidationGroup = valGroup;
        revEmailValid.ValidationExpression = @"^([\w0-9_\-\+]+(\.[\w0-9_\-\+]+)*@[\w0-9_-]+(\.[\w0-9_-]+)+)*$";

        txtMessage.ValidationGroup = valGroup;
        rfvMessage.ValidationGroup = valGroup;

        btnOk.ValidationGroup = valGroup;

        if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
        {
            lblMessage.CssClass = "";
        }

        // Load message board
        if (BoardProperties != null)
        {
            if (!BoardProperties.BoardRequireEmails)
            {
                rfvEmail.Enabled = false;
            }

            if ((BoardProperties.BoardUseCaptcha) && (!AdvancedMode))
            {
                // Show capcha text and control
                lblCapcha.Text = ResHelper.GetString("board.messageedit.securitycode");
                pnlCapcha.Visible = true;
            }
        }

        plcRating.Visible = false;
        if (!AdvancedMode && this.BoardProperties.EnableContentRating)
        {
            if (CMSContext.CurrentDocument != null)
            {
                plcRating.Visible = true;
                try
                {
                    // Insert rating control to page
                    ratingControl = (AbstractRatingControl)(Page.LoadControl(AbstractRatingControl.GetRatingControlUrl(BoardProperties.RatingType + ".ascx")));
                }
                catch (Exception ex)
                {
                    Controls.Add(new LiteralControl(ex.Message));
                    return;
                }

                // Init values
                ratingControl.ID = this.ID + "_RatingControl";
                ratingControl.MaxRating = BoardProperties.MaxRatingValue;
                ratingControl.Visible = true;
                ratingControl.Enabled = true;
                ratingControl.RatingEvent += ratingControl_RatingEvent;
                ratingControl.CurrentRating = ValidationHelper.GetDouble(ViewState["ratingvalue"], 0);
                ratingControl.ExternalManagement = true;
                pnlRating.Controls.Clear();
                pnlRating.Controls.Add(ratingControl);
            }
        }

        if (AdvancedMode)
        {
            // Initialize advanced controls
            plcAdvanced.Visible = true;
            lblApproved.Text = ResHelper.GetString("board.messageedit.approved");
            lblSpam.Text = ResHelper.GetString("board.messageedit.spam");
            lblInsertedCaption.Text = ResHelper.GetString("board.messageedit.inserted");
            btnOk.ResourceString = "general.ok";
            btnOkFooter.ResourceString = "general.ok";

            // Show or hide "Inserted" label
            bool showInserted = (MessageID > 0);
            lblInsertedCaption.Visible = showInserted;
            lblInserted.Visible = showInserted;
            chkSubscribe.Visible = false;
        }
        else
        {
            // If is not moderated then autocheck approve
            if (!this.BoardProperties.BoardModerated)
            {
                chkApproved.Checked = true;
            }
        }

        if (ModalMode)
        {
            plcFooter.Visible = true;
            pnlOkButton.Visible = false;
        }
        else
        {
            plcFooter.Visible = false;
            pnlOkButton.Visible = true;
        }

        // Show/hide subscription option
        plcChkSubscribe.Visible = this.BoardProperties.BoardEnableSubscriptions;

        // For new message hide Is approved chkbox (auto approve)
        if (this.MessageID <= 0)
        {
            this.plcApproved.Visible = false;
        }
    }


    private static bool ValidateMessage(BoardMessageInfo messageInfo)
    {
        if ((messageInfo.MessageText == null) || (messageInfo.MessageUserName == null))
        {
            return false;
        }

        return ((messageInfo.MessageText.Trim() != "") && (messageInfo.MessageUserName.Trim() != ""));
    }


    /// <summary>
    /// Validate message form and return error message if is some.
    /// </summary>
    private string ValidateForm()
    {
        txtUserName.Text = txtUserName.Text.Trim();
        txtEmail.Text = txtEmail.Text.Trim();
        txtMessage.Text = txtMessage.Text.Trim();

        // Check for required fields
        string errorMessage = new Validator()
            .NotEmpty(txtUserName.Text, rfvUserName.ErrorMessage)
            .NotEmpty(txtMessage.Text, rfvMessage.ErrorMessage).Result;

        if (errorMessage == "")
        {
            if (BoardProperties.BoardRequireEmails)
            {
                // Check e-mail address if board require
                errorMessage = new Validator()
                .NotEmpty(txtEmail.Text, rfvEmail.ErrorMessage)
                .IsEmail(txtEmail.Text, revEmailValid.ErrorMessage).Result;
            }
            else
            {
                if (txtEmail.Text != "")
                {
                    // Check e-mail address if is some
                    errorMessage = new Validator()
                    .IsEmail(txtEmail.Text, revEmailValid.ErrorMessage).Result;
                }
            }
        }

        if ((chkSubscribe.Checked) && (errorMessage == String.Empty))
        {
            errorMessage = new Validator()
            .NotEmpty(txtEmail.Text, ResHelper.GetString("board.messageedit.rfvemail"))
            .IsEmail(txtEmail.Text, ResHelper.GetString("board.messageedit.revemail")).Result;
        }

        if ((BoardProperties.BoardUseCaptcha) && (errorMessage == ""))
        {
            // Check whether security code is correct
            if (!capchaElem.IsValid())
            {
                errorMessage = capchaElem.ValidationError;
            }
        }

        return errorMessage;
    }


    /// <summary>
    /// Rloads the form data.
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();

        if (this.mMessageID > 0)
        {
            messageInfo = BoardMessageInfoProvider.GetBoardMessageInfo(this.mMessageID);
            if (messageInfo != null)
            {
                // Set textfields and checkboxes
                txtEmail.Text = messageInfo.MessageEmail;
                txtMessage.Text = messageInfo.MessageText;
                txtURL.Text = messageInfo.MessageURL;
                txtUserName.Text = messageInfo.MessageUserName;
                chkApproved.Checked = messageInfo.MessageApproved;
                chkSpam.Checked = messageInfo.MessageIsSpam;
                lblInserted.Text = CMSContext.ConvertDateTime(messageInfo.MessageInserted, this).ToString();
            }
        }
        else
        {
            ClearForm();
        }
    }


    /// <summary>
    /// Clears all input boxes.
    /// </summary>
    public override void ClearForm()
    {
        txtUserName.Text = String.Empty;
        txtEmail.Text = String.Empty;
        txtMessage.Text = String.Empty;
        txtURL.Text = "http://";

        if (!CMSContext.CurrentUser.IsPublic())
        {
            txtUserName.Text = !DataHelper.IsEmpty(CMSContext.CurrentUser.UserNickName) ? CMSContext.CurrentUser.UserNickName : CMSContext.CurrentUser.FullName;
            txtEmail.Text = CMSContext.CurrentUser.Email;
        }
    }

    #endregion
}
