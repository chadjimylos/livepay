﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSearchMerchants.ascx.vb" Inherits="CMSTemplates_LivePay_Mobile_UCSearchMerchants" %>
<link type="text/css" rel="stylesheet" href="/CMSScripts/jquery_mobile/jquery.mobile-1.0a4.1.css" />
<script>
    $(function () {
        $('input[id$=_txt]').each(function () {
            var default_value = this.value;
            $(this).focus(function () {
                if (this.value == default_value) {
                    this.value = '';
                }
            });
            $(this).blur(function () {
                if (this.value == '') {
                    this.value = default_value;
                }
            });
        });
        SetZoom_();
    });

   
</script>
<div > 
  
    <div style="padding-top:30px;padding-left:12px;padding-bottom:20px;zoom:1.3;">
        <img src="/App_Themes/LivePay/Mobile/SearchTitle.png"  />
    </div>
    <div style="float:left;padding-left:12px;padding-top:4px">
        <div style="background-image:url('/App_Themes/LivePay/Mobile/SearchTxtBG.png');background-repeat:no-repeat;width:503px;height:58px;zoom:1.3;"><asp:TextBox Text="Επωνυμία Επιχείρησης" data-role="none" ID="txt" runat="server" style="background:transparent;width:503px;height:58px;color:#a2a2a2;font-family:Arial;border:0px;font-size:larger" ></asp:TextBox></div>
    </div>
    <div style="float:left;padding-left:10px;zoom:1.3;"><asp:ImageButton data-role="none" runat="server" ID="btn" ImageUrl="/App_Themes/LivePay/Mobile/SearchBtn.png" /></div>
    <div style="clear:both"></div>

     <div style="width:100%;min-height:100px;zoom:1.3;">
        <div id="div_results" runat="server" visible="false" style="padding-top:30px;padding-left:12px;"><img src="/App_Themes/LivePay/Mobile/ResultsTitle.png" /></div>
        <div>
            <asp:UpdatePanel ID="SrcUpdatePanel" runat="server" UpdateMode="Always">
                <ContentTemplate >
                     <asp:GridView ID="GVMerchants" GridLines="None" ShowHeader="false" runat="server" PageSize="200000" AutoGenerateColumns="false" AllowPaging="true" Width="100%" >
                      <Columns >
                        <asp:TemplateField>
                            <ItemTemplate>
                                <div style="background-image:url('/App_Themes/LivePay/Mobile/ResultsRpt.png');background-repeat:no-repeat;width:800px;height:80px"><a style="display:block;line-height:80px;margin-left:42px;text-decoration:none;color:#000000;font-family:Arial;" href="/Mobile/Payment.aspx?nodeid=<%# Eval("NodeID") %>"><%# Eval("DiscreetTitle")%></a></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                      </Columns>
                         
                      <EmptyDataTemplate >
                      <div><%= ResHelper.LocalizeString("{$=Δεν βρέθηκαν αποτελέσματα με τα κριτήρια που θέσατε|en-us=No results found$}")%></div>
                      </EmptyDataTemplate>
                      </asp:GridView> 
                       <asp:LinkButton ID="HiddenPageBtn" style="display:none" runat="server" Text="0"></asp:LinkButton>
                        <asp:TextBox ID="HiddenPage"  style="display:none" runat="server" Text="1"></asp:TextBox>
                        <asp:TextBox ID="HiddenSearchType"  style="display:none" runat="server" Text="0"></asp:TextBox>
                        <div id="BottomDiv" style="display:none" runat="server" class="SrcHisPayGridBGBottom"></div> 
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div style="padding-top:20px;padding-bottom:15px;padding-left:12px;"><a href="SearchCategories.aspx"><img  src="/App_Themes/LivePay/Mobile/CategorySearch.png" /></a></div>
         
    </div>
    </div>
</div> 

<script>
    SetZoom_();
</script>

