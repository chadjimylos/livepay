<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageTemplate_New.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_New" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Page Templates - New Page Template" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblError" ForeColor="red" EnableViewState="false" Visible="false" />
    <asp:Literal ID="ltlScript" runat="server" />
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblTemplateDisplayName" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtTemplateDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200"  />
                <asp:RequiredFieldValidator ID="rfvTemplateDisplayName" runat="server" ControlToValidate="txtTemplateDisplayName" Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblTemplateCodeName" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtTemplateCodeName" runat="server" CssClass="TextBoxField" MaxLength="100"  />
                <asp:RequiredFieldValidator ID="rfvTemplateCodeName" runat="server" ControlToValidate="txtTemplateCodeName" Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top; padding-top:5px" class="FieldLabel">
                <asp:Label ID="lblTemplateDescription" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtTemplateDescription" runat="server" TextMode="MultiLine" CssClass="TextAreaField" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
