using System;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_Roles_Controls_RoleEdit : CMSAdminEditControl
{
    #region "Private Variables"

    private int mSiteId = 0;
    private int mGroupId = 0;
    private int mRoleId = 0;
    private Guid mGroupGuid = Guid.Empty;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the group GUID
    /// </summary>
    public Guid GroupGUID
    {
        get
        {
            return mGroupGuid;
        }
        set
        {
            mGroupGuid = value;
        }
    }


    /// <summary>
    /// Gets or sets the group ID for which the roles should be displayed (0 means all groups)
    /// </summary>
    public int GroupID
    {
        get
        {
            return mGroupId;
        }
        set
        {
            mGroupId = value;
        }
    }


    /// <summary>
    /// Gets or sets the site ID for which the roles should be displayed
    /// </summary>
    public int SiteID
    {
        get
        {
            return mSiteId;
        }
        set
        {
            mSiteId = value;
        }
    }


    /// <summary>
    /// Gets the role ID of currently processed role
    /// </summary>
    public int RoleID
    {
        get
        {
            return mRoleId;
        }
        set
        {
            mRoleId = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Do not edit code name in simple mode
        if (DisplayMode == ControlDisplayModeEnum.Simple)
        {
            plcCodeName.Visible = false;
        }

        rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");

        if (GroupID > 0)
        {
            plcIsPublic.Visible = true;
        }

        ReloadData(false);
    }


    /// <summary>
    /// Reloads textboxes with new data.
    /// </summary>
    public override void ReloadData(bool forceReload)
    {
        if ((!RequestHelper.IsPostBack()) || (forceReload))
        {
            RoleInfo ri = RoleInfoProvider.GetRoleInfo(ItemID);
            string roleName = string.Empty;
            if (ri != null)
            {
                LoadData(ri);
                SiteID = ri.SiteID;
                roleName = ri.RoleName;
            }
            else
            {
                txtRoleCodeName.Text = null;
                txtRoleDisplayName.Text = null;
                txtDescription.Text = null;
                chkIsDomain.Checked = false;
                chkIsAdmin.Checked = false;
            }
            bool displayIsDomain = ((roleName != RoleInfoProvider.EVERYONE) && (roleName != RoleInfoProvider.AUTHENTICATED) && (roleName != RoleInfoProvider.NOTAUTHENTICATED) && (GroupID == 0));
            plcIsDomain.Visible = displayIsDomain;
        }
    }


    /// <summary>
    /// Loads data of edited role from DB into TextBoxes.
    /// </summary>
    protected void LoadData(RoleInfo ri)
    {
        txtRoleCodeName.Text = ri.RoleName;
        txtRoleDisplayName.Text = ri.DisplayName;
        txtDescription.Text = ri.Description;
        chkIsAdmin.Checked = ri.RoleIsGroupAdministrator;
        chkIsDomain.Checked = ri.RoleIsDomain;
    }


    /// <summary>
    /// Saves data of edited role from TextBoxes into DB.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (!CheckPermissions("cms.roles", PERMISSION_MODIFY))
        {
            return;
        }

        // Generate code name in simple mode
        string codeName = txtRoleCodeName.Text.Trim();
        string displayName = txtRoleDisplayName.Text.Trim();
        if ((DisplayMode == ControlDisplayModeEnum.Simple) && (ItemID == 0))
        {
            codeName = ValidationHelper.GetCodeName(txtRoleDisplayName.Text, 50) + "_group_" + GroupGUID;
        }

        // Check whether required fields are not empty
        string errorMessage = new Validator().NotEmpty(displayName, ResHelper.GetString("general.requiresdisplayname"))
                                             .NotEmpty(codeName, ResHelper.GetString("general.requirescodename"))
                                             .IsCodeName(codeName, ResHelper.GetString("general.invalidcodename")).Result;
        if (errorMessage == string.Empty)
        {

            txtRoleCodeName.Text = codeName;
            txtRoleDisplayName.Text = displayName;
            SiteInfo si = SiteInfoProvider.GetSiteInfo(SiteID);
            if (si != null)
            {
                // Ensure safe role name
                if (UserInfoProvider.UseSafeRoleName)
                {
                    codeName = ValidationHelper.GetSafeUserName(codeName, si.SiteName);
                }

                // Check unique name
                RoleInfo ri = RoleInfoProvider.GetRoleInfo(codeName, si.SiteName);
                if ((ri == null) || (ri.RoleID == ItemID))
                {
                    bool newRole = false;
                    // Get object
                    if (ri == null)
                    {
                        ri = RoleInfoProvider.GetRoleInfo(ItemID);
                        if (ri == null)
                        {
                            ri = new RoleInfo();
                            // indicate this is new role and should be redirected after safe
                            newRole = true;
                        }
                    }

                    // Set the fields
                    ri.DisplayName = displayName;
                    ri.RoleName = codeName;
                    ri.RoleID = ItemID;
                    ri.Description = txtDescription.Text;
                    ri.SiteID = mSiteId;
                    ri.RoleIsDomain = chkIsDomain.Checked;

                    // If group id is present then it's group role
                    if (GroupID > 0)
                    {
                        ri.RoleGroupID = mGroupId;
                        ri.RoleIsGroupAdministrator = chkIsAdmin.Checked;
                    }


                    RoleInfoProvider.SetRoleInfo(ri);
                    ItemID = ri.RoleID;

                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    // if new group was created redirect to edit page
                    if (newRole)
                    {
                        RoleID = ri.RoleID;
                        RaiseOnSaved();
                    }
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Administration-Role_New.RoleExists");
                }
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }    

    }
}
