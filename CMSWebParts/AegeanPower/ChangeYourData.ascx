﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/AegeanPower/ChangeYourData.ascx.cs"
    Inherits="CMSWebParts_AegeanPower_ChangeYourData" %>
<%@ Register Src="~/CMSFormControls/Inputs/SecurityCode.ascx" TagName="SecurityCode" TagPrefix="cms" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cms" %>

<style>
.registration-textbox{color:#43474a;font-size:12px;font-family:Tahoma}
.registration-email{color:#43474a;font-size:12px;font-family:Tahoma;}
.ContentButton
{
    border: none;
    background: url('/App_Themes/AegeanPower/Images/submitbutton.png') no-repeat top left;
    width:102px;
    height:19px;
}
.classOfLabel{float:left;font-size:12px;width:120px;padding-right:10px;text-align:right;font-family:Tahoma;font-size:11px;}
</style>
<asp:Panel ID="pnlForm" runat="server" DefaultButton="btnOK">

        <div class="CUDContentBG">
             <%-- Email --%>
             <div style="height:35px;vertical-align:middle">
                <div class="classOfLabel">
                    <asp:Label ID="lblEmail" runat="server" AssociatedControlID="txtEmail" EnableViewState="false" />
                </div>
                <div style="float:left;width:160px;">
                    <cms:ExtendedTextBox ID="txtEmail" runat="server" CssClass="registration-textbox" MaxLength="100" Width="160" Visible="false"/>
                    <asp:Label ID="UserEmail" runat="server"></asp:Label>
                </div>
            </div>
             <%-- FristName --%>
             <div style="height:35px;vertical-align:middle">
                <div class="classOfLabel">
                    <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName" EnableViewState="false" />
                </div>
                <div style="float:left;width:160px;">
                    <cms:ExtendedTextBox ID="txtFirstName" Text="sjfoisd" EnableEncoding="true" runat="server" CssClass="registration-textbox"
                    MaxLength="100" Width="160"/>
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px; padding-right:10px;">*</div>
                <div>
                    <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ValidationGroup="pnlForm"
                    ControlToValidate="txtFirstName" Display="Dynamic" EnableViewState="false" />
                </div>
            </div>
             <%-- LastName --%>
             <div style="height:35px;vertical-align:middle">
                <div class="classOfLabel">
                    <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName" />
                </div>
                <div style="float:left;width:160px;">
                    <cms:ExtendedTextBox ID="txtLastName" EnableEncoding="true" runat="server" CssClass="registration-textbox"
                    MaxLength="100" Width="160" />
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px; padding-right:10px;">*</div>
                <div>
                    <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ValidationGroup="pnlForm"
                    ControlToValidate="txtLastName" Display="Dynamic" EnableViewState="false" />
                </div>
            </div>
             <%-- Old Password --%>
             <div style="height:35px;vertical-align:middle">
                <div class="classOfLabel">
                    <asp:Label ID="lblOldPassword" runat="server" AssociatedControlID="txtOldPassword" EnableViewState="false" />
                </div>
                <div style="float:left;width:160px;">
                    <asp:TextBox ID="txtOldPassword" runat="server" TextMode="Password" CssClass="registration-textbox"
                    MaxLength="100" Width="160" AutoComplete="off"  />
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px; padding-right:10px;">*</div>
                <div>
                    <asp:RequiredFieldValidator ID="rfvOldPassword" ValidationGroup="pnlForm" runat="server"
                    ControlToValidate="txtOldPassword" Display="Dynamic" EnableViewState="false" />
                </div>
            </div>
            <%-- New Password --%>
             <div style="height:35px;vertical-align:middle">
                <div class="classOfLabel">
                    <asp:Label ID="lblNewPassword" runat="server" AssociatedControlID="txtNewPassword" EnableViewState="false" />
                </div>
                <div style="float:left;width:160px;">
                    <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" CssClass="registration-textbox"
                    MaxLength="100" Width="160" />
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px; padding-right:10px;">*</div>
                <div>
                    <asp:RequiredFieldValidator ID="rfvNewPassword" ValidationGroup="pnlForm" runat="server"
                    ControlToValidate="txtNewPassword" Display="Dynamic" EnableViewState="false" />
                </div>
            </div>
             <%-- ConfirmPassword --%>
             <div style="height:35px;vertical-align:middle">
                <div class="classOfLabel">
                    <asp:Label ID="lblConfirmPassword" runat="server" AssociatedControlID="txtConfirmPassword" EnableViewState="false" />
                </div>
                <div style="float:left;width:160px;">
                    <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" CssClass="registration-textbox"
                    MaxLength="100" Width="160" />
                </div>
                <div style="float:left;color:#7AC142;font-size:12px;font-weight:bold;padding-left:10px; padding-right:10px;">*</div>
                <div>
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" ValidationGroup="pnlForm" runat="server"
                    ControlToValidate="txtConfirmPassword" Display="Dynamic" EnableViewState="false" />
                </div>
            </div>
            
                
             
             <%-- Button --%>
                <div style="height:24px;float:left;color:#094595;font-size:12px;font-weight:bold;width:182px;padding:2px 10px 0px 0px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px;width:102px;height:24px;">
                    <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" ValidationGroup="pnlForm"
                    CssClass="ContentButton" EnableViewState="false" />                 
                </div>
                <div class="Clear"></div>

                <div style="margin-left:20px; float:left; width:230px;">
                    <asp:Label ID="lblError" runat="server" ForeColor="red" EnableViewState="false" />
                    <asp:Label ID="lblSucces" runat="server" ForeColor="green" Width="300px" EnableViewState="false"></asp:Label>
                    <asp:Label ID="lblText" runat="server" Visible="false" EnableViewState="false" />
                </div>
            </div>           
</asp:Panel>
