<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SystemTable_Frameset.aspx.cs"
    Inherits="CMSSiteManager_Development_SystemTables_SystemTable_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>System tables - Edit</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height:100%; 
        }
    </style>
</head>
    <frameset border="0" rows="102, *" id="rowsFrameset">
		<frame name="menu" src="SystemTable_Header.aspx?classid=<% Response.Write(Request.QueryString["classid"]); %> " scrolling="no" frameborder="0" noresize/>		
	    <frame name="content" src="SystemTable_Edit_Fields.aspx?classid=<% Response.Write(Request.QueryString["classid"]); %> " frameborder="0" noresize/>
	<noframes>
		<body>
		 <p id="p1">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		 </p>
		</body>
	</noframes>
</frameset>
</html>
