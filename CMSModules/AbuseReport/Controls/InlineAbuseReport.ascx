<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InlineAbuseReport.ascx.cs"
    Inherits="CMSModules_AbuseReport_Controls_InlineAbuseReport" %>
<cms:CMSPanel ID="ucWrapPanel" runat="server">
    <cms:LocalizedHyperlink ID="lnkText" runat="server" ResourceString="abuse.reportabuse"
        CssClass="InlineAbuseLink" EnableViewState="false" />
</cms:CMSPanel>
