using System;
using System.Data;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

using CMS.FormEngine;
using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.EventLog;
using CMS.UIControls;
using CMS.PortalControls;
using CMS.SiteProvider;
using CMS.SettingsProvider;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Widgets_Controls_WidgetProperties : CMSUserControl
{
    #region "Variables"

    protected string mAliasPath = null;
    protected string mZoneId = null;
    protected string mWidgetId = null;
    protected Guid mInstanceGUID = Guid.Empty;
    protected bool mWidgetIdChanged = false;
    protected string mBeforeFormDefinition = null;
    protected string mAfterFormDefinition = null;
    protected bool mIsNewWidget = false;


    /// <summary>
    /// Current page info
    /// </summary>
    PageInfo pi = null;


    /// <summary>
    /// Page template info
    /// </summary>
    PageTemplateInfo pti = null;


    /// <summary>
    /// Current widget (alias web part instance)
    /// </summary>
    WebPartInstance widgetInstance = null;


    /// <summary>
    /// Current page template
    /// </summary>
    PageTemplateInstance templateInstance = null;


    /// <summary>
    /// Zone instance.
    /// </summary>
    WebPartZoneInstance zone = null;


    /// <summary>
    /// Tree provider
    /// </summary>
    TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);

    #endregion


    #region "Events"

    /// <summary>
    /// On not allowed - security check
    /// </summary>
    public event EventHandler OnNotAllowed;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Page alias path
    /// </summary>
    public string AliasPath
    {
        get
        {
            return mAliasPath;
        }
        set
        {
            mAliasPath = value;
        }
    }


    /// <summary>
    /// Zone ID
    /// </summary>
    public string ZoneId
    {
        get
        {
            return mZoneId;
        }
        set
        {
            mZoneId = value;
        }
    }


    /// <summary>
    /// Web part ID
    /// </summary>
    public string WidgetId
    {
        get
        {
            return mWidgetId;
        }
        set
        {
            mWidgetId = value;
        }
    }


    /// <summary>
    /// Instance GUID
    /// </summary>
    public Guid InstanceGUID
    {
        get
        {
            return mInstanceGUID;
        }
        set
        {
            mInstanceGUID = value;
        }
    }


    /// <summary>
    /// True if the web part ID has changed
    /// </summary>
    public bool WidgetIdChanged
    {
        get
        {
            return mWidgetIdChanged;
        }
    }


    /// <summary>
    /// Before form definition
    /// </summary>
    public string BeforeFormDefinition
    {
        get
        {
            return mBeforeFormDefinition;
        }
        set
        {
            mBeforeFormDefinition = value;
        }
    }


    /// <summary>
    /// After form definition
    /// </summary>
    public string AfterFormDefinition
    {
        get
        {
            return mAfterFormDefinition;
        }
        set
        {
            mAfterFormDefinition = value;
        }
    }


    /// <summary>
    /// Whether is widget new or not.
    /// </summary>
    public bool IsNewWidget
    {
        get
        {
            return mIsNewWidget;
        }
        set
        {
            mIsNewWidget = value;
        }
    }

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // Setup basic form on live site
        formCustom.AllowMacroEditing = false;
        formCustom.IsLiveSite = this.IsLiveSite;

        // Load settings
        if (!String.IsNullOrEmpty(Request.Form[hdnIsNewWebPart.UniqueID]))
        {
            IsNewWidget = ValidationHelper.GetBoolean(Request.Form[hdnIsNewWebPart.UniqueID], false);
        }
        if (!String.IsNullOrEmpty(Request.Form[hdnInstanceGUID.UniqueID]))
        {
            InstanceGUID = ValidationHelper.GetGuid(Request.Form[hdnInstanceGUID.UniqueID], Guid.Empty);
        }

        if ((this.WidgetId != "") && (this.AliasPath != ""))
        {
            // Get pageinfo
            try
            {
                pi = PageInfoProvider.GetPageInfo(CMSContext.CurrentSiteName, AliasPath, CMSContext.PreferredCultureCode, null, CMSContext.CurrentSite.CombineWithDefaultCulture, tree.Connection);
            }
            catch (PageNotFoundException)
            {
                // Do not throw exception if page info not found (e.g. bad alias path)
            }

            if (pi == null)
            {
                lblInfo.Text = ResHelper.GetString("Widgets.Properties.aliasnotfound");
                lblInfo.Visible = true;
                pnlFormArea.Visible = false;
                return;
            }

            // Get template
            pti = pi.PageTemplateInfo;

            // Get template instance
            templateInstance = CMSPortalManager.GetTemplateInstanceForEditing(pi);

            // Get widget from instance
            WidgetInfo wi = null;
            if (!IsNewWidget)
            {
                // Get the instance of widget
                widgetInstance = templateInstance.GetWebPart(InstanceGUID, WidgetId);
                if (widgetInstance == null)
                {
                    lblInfo.Text = ResHelper.GetString("Widgets.Properties.WidgetNotFound");
                    lblInfo.Visible = true;
                    pnlFormArea.Visible = false;
                    return;
                }

                // Get widget info by widget name(widget type)
                wi = WidgetInfoProvider.GetWidgetInfo(widgetInstance.WebPartType);
            }
            // Widget instance hasn't created yet
            else
            {
                wi = WidgetInfoProvider.GetWidgetInfo(ValidationHelper.GetInteger(WidgetId, 0));
            }

            WebPartZoneInstance zone = templateInstance.GetZone(this.ZoneId);

            // WidgetInfo or zone can't be null
            if ((wi == null) || (zone == null))
            {
                return;
            }

            // Check security

            CurrentUserInfo currentUser = CMSContext.CurrentUser;

            switch (zone.WidgetZoneType)
            {
                // Group zone => Only group widgets and group admin
                case WidgetZoneTypeEnum.Group:
                    // Should always be, only group widget are allowed in group zone
                    if (!wi.WidgetForGroup || !currentUser.IsGroupAdministrator(pi.NodeGroupId))
                    {
                        if (OnNotAllowed != null)
                        {
                            OnNotAllowed(this, null);
                        }
                    }
                    break;

                // Widget must be allowed for editor zones
                case WidgetZoneTypeEnum.Editor:
                    if (!wi.WidgetForEditor)
                    {
                        if (OnNotAllowed != null)
                        {
                            OnNotAllowed(this, null);
                        }
                    }
                    break;

                // Widget must be allowed for user zones
                case WidgetZoneTypeEnum.User:
                    if (!wi.WidgetForUser)
                    {
                        if (OnNotAllowed != null)
                        {
                            OnNotAllowed(this, null);
                        }
                    }
                    break;
            }

            // Check security
            if ((zone.WidgetZoneType != WidgetZoneTypeEnum.Group) && !WidgetRoleInfoProvider.IsWidgetAllowed(wi, currentUser.UserID, currentUser.IsAuthenticated()))
            {
                if (OnNotAllowed != null)
                {
                    OnNotAllowed(this, null);
                }
            }


            // Get the form definition
            string wpProperties = "<form></form>";

            WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(wi.WidgetWebPartID);
            if (wpi != null)
            {
                wpProperties = FormHelper.MergeFormDefinitions(wpi.WebPartProperties, wi.WidgetProperties);
            }


            string beforeFormDefinition = null;
            if (zone.WidgetZoneType == WidgetZoneTypeEnum.User)
            {
                beforeFormDefinition = File.ReadAllText(Server.MapPath("~/CMSModules/Widgets/Controls/Widget_Properties.xml"));
            }

            // Get form schemas
            FormInfo fi = new FormInfo();

            if (!String.IsNullOrEmpty(beforeFormDefinition))
            {
                // Load before form schema
                fi.LoadXmlDefinition(beforeFormDefinition);

                // Load default form schema
                if (!String.IsNullOrEmpty(fi.GetXmlDefinition()))
                {
                    FormInfo dfi = new FormInfo();
                    dfi.LoadXmlDefinition(wpProperties);
                    // Combine with global schema, overwrite existing fields
                    fi.CombineWithForm(dfi, true, null, true);
                }
            }
            else
            {
                fi.LoadXmlDefinition(wpProperties);
            }

            // Show extended properties for editor
            if (zone.WidgetZoneType == WidgetZoneTypeEnum.Editor)
            {
                string afterForm = File.ReadAllText(Server.MapPath("~/CMSModules/Widgets/Controls/Widget_Edit_Properties.xml"));
                if (!String.IsNullOrEmpty(afterForm))
                {
                    FormInfo afterFi = new FormInfo(afterForm);
                    fi.CombineWithForm(afterFi, false, null, true);
                }
            }


            // Check if there are some editable properties
            FormFieldInfo[] ffi = fi.GetFields(true, false);
            if ((ffi == null) || (ffi.Length == 0))
            {
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("widgets.emptyproperties");
            }

            // Get datarows with required columns
            DataRow dr = fi.GetDataRow();

            // Load default values for new widget
            if (IsNewWidget)
            {
                fi.LoadDefaultValues(dr, true);

                // Overide default value and set title as widget display name
                DataHelper.SetDataRowValue(dr, "WidgetTitle", wi.WidgetDisplayName);
            }

            // Load values from existing widget
            LoadDataRowFromWidget(dr, widgetInstance);

            // Init HTML toolbar if exists
            InitHTMLToobar(fi);
            // Init the form
            InitForm(formCustom, dr, fi);

            // Set the context name
            formCustom.ControlContext.ContextName = CMS.SiteProvider.ControlContext.WIDGET_PROPERTIES;
        }
    }


    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        btnOnOK.Click += new EventHandler(btnOnOK_Click);
        btnOnApply.Click += new EventHandler(btnOnApply_Click);

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ApplyButton", ScriptHelper.GetScript(
            "function SetRefresh(refreshpage) { document.getElementById('" + hidRefresh.ClientID + "').value = refreshpage; } \n" +
            "function GetRefresh() { return document.getElementById('" + hidRefresh.ClientID + "').value == 'true'; } \n" +
            "function OnApplyButton(refreshpage) { SetRefresh(refreshpage); " + Page.ClientScript.GetPostBackEventReference(btnOnApply, "") + "} \n" +
            "function OnOKButton(refreshpage) { SetRefresh(refreshpage); " + Page.ClientScript.GetPostBackEventReference(btnOnOK, "") + "} \n"
        ));

        // Reload parent page after closing, if needed
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "UnloadRefresh", ScriptHelper.GetScript("window.isPostBack = false; window.onunload = function() { if (!window.isPostBack && GetRefresh()) { RefreshPage(); }};"));
        // Register css file with editor classes
        CSSHelper.RegisterCSSLink(this.Page, "~/CMSModules/Widgets/CSS/editor.css");
    }


    /// <summary>
    /// Control ID validation
    /// </summary>
    void formElem_OnItemValidation(object sender, ref string errorMessage)
    {
        Control ctrl = (Control)sender;
        if (ctrl.ID.ToLower() == "widgetcontrolid")
        {
            TextBox ctrlTextbox = (TextBox)ctrl;
            string newId = ctrlTextbox.Text;

            // Validate unique ID
            WebPartInstance existingPart = pti.GetWebPart(newId);
            if ((existingPart != null) && (existingPart != widgetInstance) && (existingPart.InstanceGUID != widgetInstance.InstanceGUID))
            {
                // Error - duplicit IDs
                errorMessage = ResHelper.GetString("Widgets.Properties.ErrorUniqueID");
            }
        }
    }


    /// <summary>
    /// Saves the given form
    /// </summary>
    /// <param name="form">Form to save</param>
    private bool SaveForm(BasicForm form)
    {
        if (form.Visible)
        {
            return form.SaveData("");
        }

        return true;
    }


    /// <summary>
    /// Saves widget properties.
    /// </summary>
    public bool Save()
    {
        // Save the data
        if ((pi != null) && (pti != null) && (templateInstance != null) && SaveForm(formCustom))
        {
            // Get the zone
            zone = templateInstance.EnsureZone(ZoneId);
            if (zone != null)
            {
                // Add new widget
                if (IsNewWidget)
                {
                    AddWidget();
                }

                // Get basicform's datarow and update widget            
                SaveFormToWidget(formCustom);

                // Save the changes  
                CMSPortalManager.SaveTemplateChanges(pi, pti, templateInstance, zone.WidgetZoneType, CMSContext.ViewMode, tree);
            }

            // Reload the form (because of macro values set only by JS)            
            this.formCustom.ReloadData();

            // Clear the cached web part
            if (InstanceGUID != null)
            {
                CacheHelper.TouchKey("webpartinstance|" + InstanceGUID.ToString().ToLower());
            }

            return true;
        }

        return false;
    }


    /// <summary>
    /// Adds widget
    /// </summary>
    private void AddWidget()
    {
        int widgetID = ValidationHelper.GetInteger(WidgetId, 0);

        // Add web part to the currently selected zone under currently selected page
        if ((widgetID > 0) && !String.IsNullOrEmpty(ZoneId) && !String.IsNullOrEmpty(AliasPath))
        {
            // Get the web part by code name
            WidgetInfo wi = WidgetInfoProvider.GetWidgetInfo(widgetID);
            if (wi != null)
            {
                // Add the widget
                WebPartInstance newWidget = templateInstance.AddWidget(ZoneId, widgetID);
                if (newWidget != null)
                {
                    // Prepare the form info to get the default properties
                    FormInfo fi = new FormInfo(wi.WidgetProperties);

                    DataRow dr = fi.GetDataRow();
                    fi.LoadDefaultValues(dr);

                    newWidget.LoadProperties(dr);

                    // Add webpart to user's last recently used
                    CMSContext.CurrentUser.UserSettings.UpdateRecentlyUsedWidget(wi.WidgetName);

                    widgetInstance = newWidget;
                }
            }
        }
    }


    /// <summary>
    /// Saves the given DataRow data to the web part properties
    /// </summary>
    /// <param name="form">Form to save</param>
    private void SaveFormToWidget(BasicForm form)
    {
        if (form.Visible && (widgetInstance != null))
        {
            string oldId = widgetInstance.ControlID.ToLower();

            DataRow dr = form.DataRow;
            foreach (DataColumn column in dr.Table.Columns)
            {
                widgetInstance.MacroTable[column.ColumnName.ToLower()] = form.MacroTable[column.ColumnName.ToLower()];
                widgetInstance.SetValue(column.ColumnName, dr[column]);

                // If name changed, move the content
                if (column.ColumnName.ToLower() == "widgetcontrolid")
                {
                    try
                    {
                        string newId = ValidationHelper.GetString(dr[column], "").ToLower();

                        // Name changed, move the document content if present
                        if (!String.IsNullOrEmpty(newId) && (newId != oldId))
                        {
                            mWidgetIdChanged = true;
                            WidgetId = newId;
                            // Get the content
                            string currentContent = (string)(pi.EditableWebParts[oldId]);
                            if (currentContent != null)
                            {
                                TreeNode node = DocumentHelper.GetDocument(pi.DocumentId, tree);

                                // Move the content in the page info
                                pi.EditableWebParts[oldId] = null;
                                pi.EditableWebParts[newId] = currentContent;

                                // Update the document
                                node.SetValue("DocumentContent", pi.GetContentXml());
                                DocumentHelper.UpdateDocument(node, tree);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider ev = new EventLogProvider();
                        ev.LogEvent("Content", "CHANGEWIDGET", ex);
                    }
                }
            }
        }
    }


    /// <summary>
    /// Loads the data row data from given web part instance
    /// </summary>
    /// <param name="dr">DataRow to fill</param>
    /// <param name="widget">Source web part</param>
    private void LoadDataRowFromWidget(DataRow dr, WebPartInstance widget)
    {
        foreach (DataColumn column in dr.Table.Columns)
        {
            try
            {
                object value = widgetInstance.GetValue(column.ColumnName);
                if (column.DataType == typeof(decimal))
                {
                    value = ValidationHelper.GetDouble(value, 0, "en-us");
                }
                DataHelper.SetDataRowValue(dr, column.ColumnName, value);
            }
            catch
            {
            }
        }
    }


    /// <summary>
    /// Initializes the form
    /// </summary>
    /// <param name="form">Form</param>
    /// <param name="dr">Datarow with the data</param>
    /// <param name="fi">Form info</param>
    private void InitForm(BasicForm form, DataRow dr, FormInfo fi)
    {
        form.DataRow = dr;
        if (widgetInstance != null)
        {
            form.MacroTable = widgetInstance.MacroTable;
        }
        else
        {
            form.MacroTable = new Hashtable();
        }

        form.SubmitButton.Visible = false;
        form.SiteName = CMSContext.CurrentSiteName;
        form.FormInformation = fi;
        form.ShowPrivateFields = true;
        form.OnItemValidation += new BasicForm.OnItemValidationEventHandler(formElem_OnItemValidation);
    }


    /// <summary>
    /// Initializes the HTML toolbar
    /// </summary>
    /// <param name="form">Form information</param>
    private void InitHTMLToobar(FormInfo form)
    {
        // Display / hide the HTML editor toolbar area
        if (form.UsesHtmlArea())
        {
            plcToolbar.Visible = true;
        }
    }


    /// <summary>
    /// Saves the widget properties and closes the window.
    /// </summary>
    protected void btnOnOK_Click(object sender, EventArgs e)
    {
        // Save widget properties
        if (Save())
        {
            bool refresh = ValidationHelper.GetBoolean(hidRefresh.Value, false);


            string script = "";
            if (WidgetIdChanged || refresh)
            {
                script = "RefreshPage(); \n";
            }

            // Close the window
            ltlScript.Text += ScriptHelper.GetScript(script + "top.window.close();");
        }
    }


    /// <summary>
    /// Saves the widget properties
    /// </summary>
    protected void btnOnApply_Click(object sender, EventArgs e)
    {
        if (Save())
        {
            hdnIsNewWebPart.Value = "false";
            hdnInstanceGUID.Value = widgetInstance.InstanceGUID.ToString();

            if (WidgetIdChanged)
            {
                ltlScript.Text += ScriptHelper.GetScript("ChangeWidget('" + ZoneId + "', '" + WidgetId + "', '" + AliasPath + "'); RefreshPage();");
            }
        }
    }


    protected override void Render(HtmlTextWriter writer)
    {
        base.Render(writer);
    }
}
