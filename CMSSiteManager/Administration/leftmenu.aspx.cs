using System;
using System.Data;
using System.IO;
using System.Collections;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.LicenseProvider;
using CMS.SettingsProvider;

public partial class CMSSiteManager_Administration_leftmenu : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize TreeView
        this.TreeViewAdministration.ImageSet = TreeViewImageSet.Custom;
        this.TreeViewAdministration.ExpandImageToolTip = ResHelper.GetString("General.Expand");
        this.TreeViewAdministration.CollapseImageToolTip = ResHelper.GetString("General.Collapse");
        if (CultureHelper.IsUICultureRTL())
        {
            this.TreeViewAdministration.LineImagesFolder = GetImageUrl("RTL/Design/Controls/Tree", false, true);
        }
        else
        {
            this.TreeViewAdministration.LineImagesFolder = GetImageUrl("Design/Controls/Tree", false, true);
        }
        this.TreeViewAdministration.Nodes.Clear();

        // Create root
        TreeNode rootNode = new TreeNode();
        rootNode.Text = "<span class=\"ContentTreeSelectedItem\" onclick=\"ShowDesktopContent('administration.aspx', this);\"><img src=\"" + GetImageUrl("General/DefaultRoot.png") + "\" style=\"border:none;height:10px;width:1px;\" /><span class=\"Name\">" + ResHelper.GetString("Administration-LeftMenu.Administration") + "</span></span>";
        rootNode.Expanded = true;
        rootNode.NavigateUrl = "#";
        this.TreeViewAdministration.Nodes.Add(rootNode);

        ArrayList items = new ArrayList();

        // Get the UIElements
        DataSet ds = UIElementInfoProvider.GetRootChildUIElements("CMS.Administration");
        if (!DataHelper.DataSourceIsEmpty(ds))
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                UIElementInfo element = new UIElementInfo(dr);                
                bool add = CMSAdministrationPage.IsAdministrationUIElementAvailable(element.ElementName);               
                if (add)
                {
                    object[] itemProperties = new object[3];

                    // Ensure target URL
                    string targetUrl = UrlHelper.ResolveUrl(element.ElementTargetURL);
                    targetUrl = UrlHelper.RemoveParameterFromUrl(targetUrl, "siteid");

                    // Ensure icon URL
                    string iconUrl = element.ElementIconPath;
                    if (!String.IsNullOrEmpty(iconUrl))
                    {
                        if (!ValidationHelper.IsURL(iconUrl))
                        {
                            iconUrl = GetImageUrl(iconUrl);
                            // Try to get default icon if requested icon not found
                            if (!File.Exists(Server.MapPath(iconUrl)))
                            {
                                iconUrl = GetImageUrl("CMSModules/list.png");
                            }
                        }
                    }
                    else
                    {
                        iconUrl = GetImageUrl("CMSModules/list.png");
                    }

                    // Initialize and add element to collection
                    itemProperties[0] = targetUrl;
                    itemProperties[1] = ResHelper.LocalizeString(element.ElementCaption);
                    itemProperties[2] = iconUrl;

                    items.Add(itemProperties);
                }
            }
        }

        // Add permanent modules
        AddToCollection(
            ResHelper.GetString("Administration-LeftMenu.Avatars"),
            "~/CMSModules/Avatars/Avatar_list.aspx",
            "Objects/CMS_Avatar/list.png", items);

        AddToCollection(
            ResHelper.GetString("Administration-LeftMenu.Badges"),
            "~/CMSModules/Badges/Badges_List.aspx",
            "Objects/CMS_Badge/list.png", items);

        AddToCollection(
            ResHelper.GetString("Administration-LeftMenu.BadWords"),
            "~/CMSModules/BadWords/BadWords_List.aspx",
            "Objects/Badwords_Word/list.png", items);

        AddToCollection(
             ResHelper.GetString("Administration-LeftMenu.EmailQueue"),
             "~/CMSModules/EmailQueue/EmailQueue_Frameset.aspx",
             "CMSModules/CMS_EmailQueue/list.png", items);

        AddToCollection(
             ResHelper.GetString("Administration-LeftMenu.RecycleBin"),
             "~/CMSSiteManager/Administration/RecycleBin/default.aspx",
             "CMSModules/CMS_RecycleBin/list.png", items);

        AddToCollection(
             ResHelper.GetString("srch.index.title"),
             "~/CMSModules/SmartSearch/SearchIndex_List.aspx",
             "Objects/CMS_SearchIndex/list.png", items);

        AddToCollection(
             ResHelper.GetString("Administration-LeftMenu.System"),
             "~/CMSSiteManager/Administration/System/System_Frameset.aspx",
             "CMSModules/CMS_System/list.png", items);

        if (LicenseHelper.IsFeautureAvailableInUI(FeatureEnum.Webfarm))
        {
            AddToCollection(
                 ResHelper.GetString("Administration-LeftMenu.WebFarm"),
                 "~/CMSSiteManager/Administration/WebFarm/WebFarm_Frameset.aspx",
                 "Objects/CMS_WebFarmServer/list.png", items);
        }

        // Sort the collection
        ObjectArrayComparer comparer = new ObjectArrayComparer();
        items.Sort(comparer);

        // Build the tree
        foreach (object node in items)
        {
            object[] nodeArray = (object[])node;
            AddNodeToTree(Convert.ToString(nodeArray[1]), Convert.ToString(nodeArray[0]), Convert.ToString(nodeArray[2]), rootNode);
        }
    }


    /// <summary>
    /// Add module to tree menu.
    /// </summary>
    /// <param name="caption">Module code name</param>
    /// <param name="url">Resource string of tree item</param>
    /// <param name="icon">URL to module</param>
    /// <param name="collection">Collection to fill</param>
    protected void AddToCollection(string caption, string url, string icon, ArrayList collection)
    {
        object[] itemProperties = new object[3];

        itemProperties[0] = url;
        itemProperties[1] = ResHelper.LocalizeString(caption);
        itemProperties[2] = GetImageUrl(icon);

        collection.Add(itemProperties);
    }


    /// <summary>
    /// Add module to tree menu.
    /// </summary>
    /// <param name="caption">Module code name</param>
    /// <param name="url">Resource string of tree item</param>
    /// <param name="icon">URL to module</param>
    /// <param name="rootNode">Parent node</param>
    protected void AddNodeToTree(string caption, string url, string icon, TreeNode rootNode)
    {
        TreeNode newNode = new TreeNode();
        newNode.Text = "<span class=\"ContentTreeItem\" onclick=\"ShowDesktopContent('" + UrlHelper.ResolveUrl(url) + "', this);\"><img class=\"TreeItemImage\" src=\"" + icon + "\" alt=\"\" /><span class=\"Name\">" + caption + "</span></span>";
        newNode.NavigateUrl = "#";
        rootNode.ChildNodes.Add(newNode);
    }
}
