// Register the related command.
FCKCommands.RegisterCommand( 'InsertUserControls', new FCKDialogCommand( 'InsertUserControls', FCKLang.InsertUserControlsTitle, FCKPlugins.Items['InsertUserControls'].Path + 'fck_InsertUserControls.aspx?' + FCKConfig.DialogParameters, 550, 400 ) ) ;

// Create the "Placeholder" toolbar button.
var oInsertUserControls = new FCKToolbarButton( 'InsertUserControls', FCKLang.InsertUserControlsButton ) ;
oInsertUserControls.IconPath = FCKPlugins.Items['InsertUserControls'].Path + 'fckInlineControls.gif' ;
FCKToolbarItems.RegisterItem( 'InsertUserControls', oInsertUserControls ) ;
