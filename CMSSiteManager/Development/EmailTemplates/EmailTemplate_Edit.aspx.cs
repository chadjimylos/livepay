using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.EmailEngine;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.FileManager;
using CMS.UIControls;

public partial class CMSSiteManager_Development_EmailTemplates_EmailTemplate_Edit : SiteManagerPage
{
    protected int templateId = 0;
    protected int siteId = 0;

    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize required field validators.
        RequiredFieldValidatorDisplayName.ErrorMessage = ResHelper.GetString("EmailTemplate_Edit.FillDisplayNameField");
        RequiredFieldValidatorCodeName.ErrorMessage = ResHelper.GetString("EmailTemplate_Edit.FillCodeNameField");
        // Initialize other controls.
        lblDisplayName.ResourceString = "General.DisplayName";
        lblDisplayName.ShowRequiredMark = true;
        lblDisplayName.DisplayColon = true;
        lblCodeName.ResourceString = "General.CodeName";
        lblCodeName.ShowRequiredMark = true;
        lblCodeName.DisplayColon = true;
        lblSubject.ResourceString = "General.Subject";
        lblSubject.DisplayColon = true;
        lblText.ResourceString = "EmailTemplate_Edit.HTMLVersion";
        lblText.DisplayColon = true;
        lblTextPlain.ResourceString = "EmailTemplate_Edit.PlainTextVersion";
        lblTextPlain.DisplayColon = true;
        lblFrom.ResourceString = "General.FromEmail";
        lblFrom.DisplayColon = true;
        lblCc.ResourceString = "EmailTemplate_Edit.Cc";
        lblCc.DisplayColon = true;
        lblBcc.ResourceString = "EmailTemplate_Edit.Bcc";
        lblBcc.DisplayColon = true;

        // Register macro scripts
        this.RegisterMacroScript();

        // Save button
        string[,] actions = new string[1, 9];
        actions[0, 0] = HeaderActions.TYPE_SAVEBUTTON;
        actions[0, 1] = ResHelper.GetString("general.save");
        actions[0, 2] = null;
        actions[0, 3] = null;
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        actions[0, 6] = "lnksave_click";
        actions[0, 7] = null;
        actions[0, 8] = "true";

        this.CurrentMaster.HeaderActions.LinkCssClass = "ContentSaveLinkButton";
        this.CurrentMaster.HeaderActions.Actions = actions;
        this.CurrentMaster.HeaderActions.ActionPerformed += new CommandEventHandler(HeaderActions_ActionPerformed);

        AttachmentTitle.TitleText = ResHelper.GetString("general.attachments");
        
        if (!RequestHelper.IsPostBack())
        {
            // Set focus to textbox 'txtDisplayName'.
            txtDisplayName.Focus();
        }

        string currentTemplate = ResHelper.GetString("EmailTemplate_Edit.CurrentTemplate");

        // Get 'templateid' from querystring.
        templateId = QueryHelper.GetInteger("templateid", 0);
        if (templateId > 0)
        {
            // Get email template info of specified 'templateid'.
            EmailTemplateInfo templateInfo = EmailTemplateProvider.GetEmailTemplate(templateId);
            // Get SiteID of the template.
            siteId = templateInfo.TemplateSiteID;
            currentTemplate = templateInfo.TemplateDisplayName;

            // Setup macro resolvers
            macroSelectorElm.Resolver = MacroSelector.GetEmailTemplateResolver(templateInfo.TemplateName);
            macroSelectorPlain.Resolver = MacroSelector.GetEmailTemplateResolver(templateInfo.TemplateName);

            // Fill the form.
            if (!RequestHelper.IsPostBack())
            {
                // Load data to the form.
                LoadData(templateInfo);
                // Show message that the email template was created or updated successfully.
                if (Request.QueryString["saved"] == "1")
                {
                    lblInfo.Visible = true;
                    lblInfo.ResourceString = "General.ChangesSaved";
                }

                // Show attachment list
                AttachmentList.Visible = true;
                AttachmentTitle.Visible = true;
            }
        }
        else
        {
            // Setup macro resolvers
            macroSelectorElm.Resolver = MacroSelector.GetEmailTemplateResolver(null);
            macroSelectorPlain.Resolver = MacroSelector.GetEmailTemplateResolver(null);
        }
        // Get 'siteid' from querystring.

        if (Request.QueryString["siteid"] != null)
        {
            siteId = QueryHelper.GetInteger("siteid", 0);
        }

        // Initialize page title control.
        string emailTemplates = ResHelper.GetString("EmailTemplate_Edit.EmailTemplates");

        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = emailTemplates;
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/EmailTemplates/EmailTemplate_List.aspx?siteid=" + siteId.ToString();
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentTemplate;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        // Init page title
        string imgUrl = "";
        string pageTitle = null;
        if (templateId > 0)
        {
            imgUrl = GetImageUrl("Objects/CMS_EmailTemplate/object.png");
            pageTitle = ResHelper.GetString("EmailTemplate_Edit.Title");
        }
        else
        {
            imgUrl = GetImageUrl("Objects/CMS_EmailTemplate/new.png");
            pageTitle = ResHelper.GetString("EmailTemplate_Edit.TitleNew");
        }

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = pageTitle;
        this.CurrentMaster.Title.TitleImage = imgUrl;
        this.CurrentMaster.Title.HelpTopicName = "newedit_e_mail_template";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    // Registers scripts for proper "Insert macro" functionality
    private void RegisterMacroScript()
    {
        string script =
            // Inserts the text to caret position (in text area)
            "function InsertMacroPlain(text, obj) { InsertMacroGeneral(text, document.getElementById('" + this.txtPlainText.ClientID + "')); }" +
            "function InsertMacro(text, obj) { InsertMacroGeneral(text, document.getElementById('" + this.txtText.ClientID + "')); }" +
            "function InsertMacroGeneral(text, obj) {\n" +
            "   if (obj != null) {\n" +
            "      if (document.selection) {\n" +
            // IE
            "           obj.focus();\n" +
            "           var orig = obj.value.replace(/\\r\\n/g, \"\\n\");\n" +
            "           var range = document.selection.createRange();\n" +
            "           if (range.parentElement() != obj) {\n" +
            "               return false;\n" +
            "           }\n" +
            "           range.text = text;\n" +
            "           var actual = tmp = obj.value.replace(/\\r\\n/g, \"\\n\");\n" +
            "           for (var diff = 0; diff < orig.length; diff++) {\n" +
            "               if(orig.charAt(diff) != actual.charAt(diff)) break;\n" +
            "           }\n" +
            "           for (var index = 0, start = 0; tmp.match(text) && (tmp = tmp.replace(text, \"\")) && index <= diff; index = start + text.length) \n" +
            "           {\n" +
            "               start = actual.indexOf(text, index);\n" +
            "           }\n" +
            "       } else {\n" +
            // Firefox
            "               var start = obj.selectionStart;\n" +
            "               var end   = obj.selectionEnd;\n" +
            "               obj.value = obj.value.substr(0, start) + text + obj.value.substr(end, obj.value.length);\n" +
            "       }\n" +
            "       if (start != null) {\n" +
            "           setCaretTo(obj, start + text.length);\n" +
            "       } else {\n" +
            "           obj.value += text;\n" +
            "       }\n" +
            "   }\n" +
            "}\n" +

            // Sets the location of the cursor in specified object to given position
            "function setCaretTo(obj, pos) {	\n" +
                "if (obj != null) {\n" +
                    "if (obj.createTextRange) {\n" +
                        "var range = obj.createTextRange();\n" +
                        "range.move('character', pos);\n" +
                        "range.select();\n" +
                    "} else if(obj.selectionStart) {\n" +
                        "obj.focus();\n" +
                        "obj.setSelectionRange(pos, pos);\n" +
                    "}\n" +
                "}\n" +
            "}\n";

     ScriptHelper.RegisterClientScriptBlock(this.Page,typeof(string), "MacroInsert2", ScriptHelper.GetScript(script));
    }


    /// <summary>
    /// Load data of edited email template from DB into textboxes.
    /// </summary>
    /// <param name="templateInfo">EmailTemplateInfo object.</param>
    protected void LoadData(EmailTemplateInfo templateInfo)
    {
        txtDisplayName.Text = templateInfo.TemplateDisplayName;
        txtCodeName.Text = templateInfo.TemplateName;
        txtSubject.Text = templateInfo.TemplateSubject;
        txtBcc.Text = templateInfo.TemplateBcc;
        txtCc.Text = templateInfo.TemplateCc;
        txtFrom.Text = templateInfo.TemplateFrom;
        txtText.Text = templateInfo.TemplateText;
        txtPlainText.Text = templateInfo.TemplatePlainText;

        // Init attachment storage
        AttachmentList.ObjectID = templateInfo.TemplateID;
        AttachmentList.ObjectType = EmailObjectType.EMAILTEMPLATE;
        AttachmentList.Category = MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE;
        AttachmentList.AllowEdit = true;

        if (siteId != 0)
        {
            AttachmentList.SiteID = siteId;
        }
    }


    /// <summary>
    /// Save button action
    /// </summary>
    protected void HeaderActions_ActionPerformed(object sender, CommandEventArgs e)
    {
        switch (e.CommandName.ToLower())
        {
            case "lnksave_click":
                txtDisplayName.Text = txtDisplayName.Text.Trim();
                txtCodeName.Text = txtCodeName.Text.Trim();
                txtSubject.Text = txtSubject.Text.Trim();
                // Find whether required fields are not empty.
                string result = new Validator().NotEmpty(txtDisplayName.Text, ResHelper.GetString("EmailTemplate_Edit.FillDisplayNameField")).NotEmpty(txtCodeName.Text, ResHelper.GetString("EmailTemplate_Edit.FillCodeNameField"))
                    .IsCodeName(txtCodeName.Text, ResHelper.GetString("general.invalidcodename"))
                    .Result;

                if ((this.txtFrom.Text != "") && !ValidationHelper.AreEmails(this.txtFrom.Text))
                {
                    result = ResHelper.GetString("EmailTemplate_Edit.InvalidFrom");
                }
                else if ((this.txtBcc.Text != "") && !ValidationHelper.AreEmails(this.txtBcc.Text))
                {
                    result = ResHelper.GetString("EmailTemplate_Edit.InvalidBcc");
                }
                else if ((this.txtCc.Text != "") && !ValidationHelper.AreEmails(this.txtCc.Text))
                {
                    result = ResHelper.GetString("EmailTemplate_Edit.InvalidCc");
                }

                if (result == "")
                {

                    string siteName = null;
                    if (siteId != 0)
                    {
                        siteName = SiteInfoProvider.GetSiteInfo(siteId).SiteName;
                    }

                    EmailTemplateInfo templateInfo = EmailTemplateProvider.GetEmailTemplate(txtCodeName.Text, siteName);

                    // Find if codename of the email template is unique for the site.
                    if ((templateInfo == null) || (templateInfo.TemplateID == templateId) || ((templateInfo.TemplateSiteID == 0) && (siteId > 0)))
                    {
                        // Get object
                        if (templateInfo == null)
                        {
                            templateInfo = EmailTemplateProvider.GetEmailTemplate(templateId);
                            if (templateInfo == null)
                            {
                                templateInfo = new EmailTemplateInfo();
                            }
                        }

                        templateInfo.TemplateID = templateId;
                        templateInfo.TemplateDisplayName = txtDisplayName.Text;
                        templateInfo.TemplateName = txtCodeName.Text;
                        templateInfo.TemplateSubject = txtSubject.Text;
                        templateInfo.TemplateFrom = txtFrom.Text;
                        templateInfo.TemplateBcc = txtBcc.Text;
                        templateInfo.TemplateCc = txtCc.Text;
                        templateInfo.TemplateText = txtText.Text;
                        templateInfo.TemplatePlainText = txtPlainText.Text;
                        templateInfo.TemplateSiteID = siteId;

                        // Save EmailTemplateInfo
                        EmailTemplateProvider.SetEmailTemplate(templateInfo);

                        UrlHelper.Redirect("EmailTemplate_Edit.aspx?templateid=" + templateInfo.TemplateID.ToString() + "&saved=1");
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.ResourceString = "EmailTemplate_Edit.UniqueCodeName";
                    }
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = result;
                }
                break;
        }
    }
}
