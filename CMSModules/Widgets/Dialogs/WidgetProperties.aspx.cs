using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using CMS.ExtendedControls;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.PortalEngine;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.PortalControls;

public partial class CMSModules_Widgets_Dialogs_WidgetProperties : CMSModalPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        string widgetId = QueryHelper.GetString("widgetid", String.Empty);
        string aliasPath = QueryHelper.GetString("aliasPath", String.Empty);
        string zoneId = QueryHelper.GetString("zoneid", String.Empty);
        Guid instanceGUID = QueryHelper.GetGuid("instanceguid", Guid.Empty);
        bool isNewWidget = QueryHelper.GetBoolean("isnew", false);

        // Set page title 
        if (isNewWidget)
        {
            this.Page.Title = ResHelper.GetString("widgets.propertiespage.titlenew");
        }
        else
        {
            this.Page.Title = ResHelper.GetString("widgets.propertiespage.title");
        }


        if ((widgetId != "") && (aliasPath != ""))
        {
            // Get pageinfo
            PageInfo pi = null;
            try
            {
                pi = PageInfoProvider.GetPageInfo(CMSContext.CurrentSiteName, aliasPath, CMSContext.PreferredCultureCode, null, CMSContext.CurrentSite.CombineWithDefaultCulture, tree.Connection);
            }
            catch (PageNotFoundException)
            {
                // Do not throw exception if page info not found (e.g. bad alias path)
            }

            if (pi == null)
            {
                return;
            }

            // Get template
            PageTemplateInfo pti = pi.PageTemplateInfo;

            // Get template instance
            PageTemplateInstance templateInstance = CMSPortalManager.GetTemplateInstanceForEditing(pi);

            // Get widget from instance
            WidgetInfo wi = null;
            if (!isNewWidget)
            {
                // Get the instance of widget
                WebPartInstance widgetInstance = templateInstance.GetWebPart(instanceGUID, widgetId);
                if (widgetInstance == null)
                {
                    return;
                }

                // Get widget info by widget name(widget type)
                wi = WidgetInfoProvider.GetWidgetInfo(widgetInstance.WebPartType);
            }
            // Widget instance hasn't created yet
            else
            {
                wi = WidgetInfoProvider.GetWidgetInfo(ValidationHelper.GetInteger(widgetId, 0));
            }


            if (wi != null)
            {
                WebPartZoneInstance zone = templateInstance.GetZone(zoneId);
                if (zone != null)
                {
                    CurrentUserInfo currentUser = CMSContext.CurrentUser;

                    switch (zone.WidgetZoneType)
                    {
                        // Group zone => Only group widgets and group admin
                        case WidgetZoneTypeEnum.Group:
                            // Should always be, only group widget are allowed in group zone
                            if (!wi.WidgetForGroup || !currentUser.IsGroupAdministrator(pi.NodeGroupId))
                            {
                                RedirectToAccessDenied(ResHelper.GetString("widgets.security.notallowed"));
                            }
                            break;

                        // Widget must be allowed for editor zones
                        case WidgetZoneTypeEnum.Editor:
                            if (!wi.WidgetForEditor)
                            {
                                RedirectToAccessDenied(ResHelper.GetString("widgets.security.notallowed"));
                            }
                            break;

                        // Widget must be allowed for user zones
                        case WidgetZoneTypeEnum.User:
                            if (!wi.WidgetForUser)
                            {
                                RedirectToAccessDenied(ResHelper.GetString("widgets.security.notallowed"));
                            }
                            break;
                    }

                    if ((zone.WidgetZoneType != WidgetZoneTypeEnum.Group) && !WidgetRoleInfoProvider.IsWidgetAllowed(wi, currentUser.UserID, currentUser.IsAuthenticated()))
                    {
                        RedirectToAccessDenied(ResHelper.GetString("widgets.security.notallowed"));
                    }
                }

                // If all ok, set up frames
                this.frameHeader.Attributes.Add("src", "widgetproperties_header.aspx" + Request.Url.Query);
                this.frameContent.Attributes.Add("src", "widgetproperties_properties_frameset.aspx" + Request.Url.Query);
            }
        }
    }
}
