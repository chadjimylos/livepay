using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;

public partial class CMSFormControls_Inputs_EncodingTextBox : FormEngineUserControl
{
    #region "Public properties"

    /// <summary>
    /// Gets or sets encoded textbox value.
    /// </summary>
    public override object Value
    {
        get
        {
            return HTMLHelper.HTMLEncode(txtValue.Text);
        }
        set
        {
            txtValue.Text = HttpUtility.HtmlDecode(ValidationHelper.GetString(value, String.Empty));
        }
    }

    #endregion


    #region "Methods"


    protected void Page_Load(object sender, EventArgs e)
    {
    }

    #endregion
}
