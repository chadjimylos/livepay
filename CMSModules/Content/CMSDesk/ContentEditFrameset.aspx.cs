using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.GlobalHelper;
using CMS.FormEngine;
using CMS.PortalEngine;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_ContentEditFrameset : CMSContentPage
{
    protected string viewpage = "Edit/editframeset.aspx";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Current Node ID
        int nodeId = QueryHelper.GetInteger("nodeid", 0);

        ViewModeEnum currentMode = ViewModeEnum.Edit;

        // Get the node
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        tree.CombineWithDefaultCulture = false;
        TreeNode node = tree.SelectSingleNode(nodeId, CMSContext.PreferredCultureCode);
        if (node != null)
        {
            currentMode = CMSContext.ViewMode;

            // Check the product mode
            if (currentMode == ViewModeEnum.Product)
            {
                bool showProductTab = false;

                // Product tab has to be allowed in settings and also the doc. type has to be marked as a product
                DataClassInfo classObj = DataClassInfoProvider.GetDataClass(node.NodeClassName);
                if (classObj != null)
                {
                    showProductTab = SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".ProductTabEnabled") && classObj.ClassIsProduct;
                }

                if (!showProductTab)
                {
                    currentMode = ViewModeEnum.Properties;
                    CMSContext.ViewMode = ViewModeEnum.Properties;
                }
            }

            // Get the name for automatic title
            string name = (node.NodeAliasPath == "/" ? CMSContext.CurrentSite.DisplayName : node.DocumentName);
            if (!String.IsNullOrEmpty(name))
            {
                this.ltlScript.Text = ScriptHelper.GetTitleScript(name);
            }
        }
        else
        {
            // Document does not exist -> redirect to new culture version creation dialog
            UrlHelper.Redirect("New/NewCultureVersion.aspx" + Request.Url.Query);
        }

        switch (ValidationHelper.GetString(Request.QueryString["action"], "edit").ToLower())
        {
            case "properties":
                viewpage = "Properties/default.aspx";
                break;

            case "product":
                viewpage = ResolveUrl("~/CMSModules/Ecommerce/Content/Product/Product_Selection.aspx");
                break;

            default:
                // Set the radio buttons
                switch (currentMode)
                {
                    case ViewModeEnum.Product:
                        viewpage = ResolveUrl("~/CMSModules/Ecommerce/Content/Product/Product_Selection.aspx");
                        break;

                    case ViewModeEnum.Properties:
                        viewpage = "Properties/default.aspx";
                        break;
                }
                break;
        }

        viewpage += Request.Url.Query;
    }
}
