<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Blogs/PostArchive.ascx.cs"
    Inherits="CMSWebParts_Blogs_PostArchive" %>
<cms:CMSRepeater ID="rptPostArchive" runat="server" OrderBy="BlogMonthStartingDate DESC"
    ClassNames="cms.blogmonth" OnItemDataBound="rptPostArchive_ItemDataBound" />
