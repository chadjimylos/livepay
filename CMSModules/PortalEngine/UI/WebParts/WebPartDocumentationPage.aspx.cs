using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.FormEngine;
using CMS.UIControls;
using CMS.PortalControls;

public partial class CMSModules_PortalEngine_UI_WebParts_WebPartDocumentationPage : MessagePage
{
    #region "Variables"

    string webPartId = "";

    string plImg = "";
    string minImg = "";


    // WebPartInstance webPart = null;
    WebPartInfo wpi = null;

    ContextResolver resolver = null;

    #endregion


    /// <summary>
    /// Generate documentation page 
    /// </summary>
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Get current resolver
        resolver = CMSContext.CurrentResolver.CreateChild();

        plImg = GetImageUrl("CMSModules/CMS_PortalEngine/WebpartProperties/plus.png");
        minImg = GetImageUrl("CMSModules/CMS_PortalEngine/WebpartProperties/minus.png");

        this.CurrentMaster.Title.TitleText = ResHelper.GetString("WebPartDocumentDialog.Documentation");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_PortalEngine/Documentation.png");

        webPartId = QueryHelper.GetString("webPartId", String.Empty);
        wpi = WebPartInfoProvider.GetWebPartInfo(webPartId);

        // Check whether webpart was found
        if (wpi != null)
        {
            this.CurrentMaster.Title.TitleText += " &nbsp; - &nbsp; " + wpi.WebPartDisplayName;

            // Get WebPart image
            DataSet ds = MetaFileInfoProvider.GetMetaFiles(wpi.WebPartID, PortalObjectType.WEBPART);

            // Set image url of exists
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                MetaFileInfo mtfi = new MetaFileInfo(ds.Tables[0].Rows[0]);
                if (mtfi != null)
                {
                    if (mtfi.MetaFileImageWidth > 385)
                    {
                        imgTeaser.Width = 385;
                    }

                    imgTeaser.ImageUrl = ResolveUrl("~/CMSPages/GetMetaFile.aspx?fileguid=" + mtfi.MetaFileGUID.ToString());
                }
            }
            else
            {
                // Set default image 
                imgTeaser.ImageUrl = GetImageUrl("CMSModules/CMS_PortalEngine/WebpartProperties/imagenotavailable.png");
            }

            // Additional image information
            imgTeaser.ToolTip = wpi.WebPartDisplayName;
            imgTeaser.AlternateText = wpi.WebPartDisplayName;

            // Set description of webpart
            ltrDescription.Text = wpi.WebPartDescription;

            // Get description from parent weboart if webpart is inherited
            if ((wpi.WebPartDescription == null || wpi.WebPartDescription == "") && (wpi.WebPartParentID > 0))
            {
                WebPartInfo pwpi = WebPartInfoProvider.GetWebPartInfo(wpi.WebPartParentID);
                if (pwpi != null)
                {
                    ltrDescription.Text = pwpi.WebPartDescription;
                }
            }

            FormInfo fi = null;

            // Generate properties
            if (wpi != null)
            {
                // Get form info from parent if webpart is inherited
                if (wpi.WebPartParentID != 0)
                {
                    WebPartInfo pwpi = WebPartInfoProvider.GetWebPartInfo(wpi.WebPartParentID);
                    if (pwpi != null)
                    {
                        fi = GetWebPartProperties(pwpi);
                    }
                }
                else
                {
                    fi = GetWebPartProperties(wpi);
                }                                
            }

            // Generate properties
            if (fi != null)
            {
                GenerateProperties(fi);
            }

            // Generate documentation text
            if (wpi.WebPartDocumentation == null || wpi.WebPartDocumentation.Trim() == "")
            {
                if (wpi.WebPartParentID != 0)
                {
                    WebPartInfo pwpi = WebPartInfoProvider.GetWebPartInfo(wpi.WebPartParentID);
                    if (pwpi != null && pwpi.WebPartDocumentation.Trim() != "")
                    {
                        ltlContent.Text = HTMLHelper.ResolveUrls(pwpi.WebPartDocumentation, null);
                    }
                    else
                    {
                        ltlContent.Text = "<br /><div style=\"padding-left:5px; font-weight: bold;\">" + ResHelper.GetString("WebPartDocumentation.DocumentationText") + "</div><br />";
                    }
                }
                else
                {
                    ltlContent.Text = "<br /><div style=\"padding-left:5px; font-weight: bold;\">" + ResHelper.GetString("WebPartDocumentation.DocumentationText") + "</div><br />";
                }
            }
            else
            {
                ltlContent.Text = HTMLHelper.ResolveUrls(wpi.WebPartDocumentation, null);
            }
        }
    }


    protected override void OnLoad(EventArgs e)
    {
        RegisterModalDialogScript();

        // Disable caching
        Response.Cache.SetNoStore();

        base.OnLoad(e);
    }


    protected FormInfo GetWebPartProperties(WebPartInfo wpi)
    {
        // Create form info
        FormInfo fi = new FormInfo();

        if (wpi != null)
        {
            // Before form                
            string before = PortalHelper.GetWebPartProperties((WebPartTypeEnum)wpi.WebPartType, PropertiesPosition.Before);

            // Load before properties
            fi.LoadXmlDefinition(before);

            // Load properties
            FormInfo cfi = new FormInfo();
            cfi.LoadXmlDefinition(wpi.WebPartProperties);

            // Combine
            fi.CombineWithForm(cfi, true, null, true);

            // After form
            FormInfo afi = new FormInfo();
            string after = PortalHelper.GetWebPartProperties((WebPartTypeEnum)wpi.WebPartType, PropertiesPosition.After);

            // Load before properties
            afi.LoadXmlDefinition(after);
            fi.CombineWithForm(afi, false, null, true, false);            
        }

        return fi;
    }


    /// <summary>
    /// Generate properties    
    /// </summary>
    protected void GenerateProperties(FormInfo fi)
    {
        // Generate script to display and hide  properties groups
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ShowHideProperties", ScriptHelper.GetScript("" +
            "function ShowHideProperties(id){" +
            "var obj = document.getElementById(id);" +
            "var objlink = document.getElementById(id+'_link');" +
            "if ((obj != null)&&(objlink != null)){" +
            "if (obj.style.display == 'none' ) { obj.style.display=''; objlink.innerHTML = '<img border=\"0\" src=\"" + minImg + "\" >'; } " +
            "else {obj.style.display='none'; objlink.innerHTML = '<img border=\"0\" src=\"" + plImg + "\" >';}" +
            "objlink.blur();}}"));  

        // Get defintion elements
        ArrayList infos = fi.GetFormElements(true, false);

        bool isOpenSubTable = false;
        bool firstLoad = true;

        string currentGuid = "";
        string newCategory = null;

        // Check all items in object array
        foreach (object contrl in infos)
        {
            // Generate row for form category
            if (contrl is FormCategoryInfo)
            {
                // Load castegory info
                FormCategoryInfo fci = contrl as FormCategoryInfo;
                if (fci != null)
                {
                    if (isOpenSubTable)
                    {
                        ltrProperties.Text += "<tr class=\"PropertyBottom\"><td class=\"PropertyLeftBottom\">&nbsp;</td><td colspan=\"2\" class=\"Center\">&nbsp;</td><td class=\"PropertyRightBottom\">&nbsp;</td></tr></table>";
                        isOpenSubTable = false;
                    }

                    if (currentGuid == "")
                    {
                        currentGuid = Guid.NewGuid().ToString().Replace("-", "_");
                    }

                    // Generate properties content
                    newCategory = "<br />" +
                        "<table cellpadding=\"0\" cellspacing=\"0\" class=\"CategoryTable\">" +
                        "<tr>" +
                        "<td class=\"CategoryLeftBorder\">&nbsp;</td>" +
                        "<td class=\"CategoryTextCell\">" + fci.CategoryCaption + "</td>" +
                        "<td class=\"HiderCell\">" +
                        "<a id=\"" + currentGuid + "_link\" href=\"#\" onclick=\"ShowHideProperties('" + currentGuid + "'); return false;\">" +
                        "<img border=\"0\" src=\"" + minImg + "\" ></a>" +
                        "</td>" +
                        "<td class=\"CategoryRightBorder\">&nbsp;</td>" +
                        "</tr></table>";
                }
            }
            else
            {
                // Get form field info
                FormFieldInfo ffi = contrl as FormFieldInfo;

                if (ffi != null)
                {
                    if (newCategory != null)
                    {
                        ltrProperties.Text += newCategory;
                        newCategory = null;

                        firstLoad = false;
                    }
                    else if (firstLoad)
                    {
                        if (currentGuid == "")
                        {
                            currentGuid = Guid.NewGuid().ToString().Replace("-", "_");
                        }

                        // Generate properties content
                        firstLoad = false;
                        ltrProperties.Text += "<br />" +
                        "<table cellpadding=\"0\" cellspacing=\"0\" class=\"CategoryTable\">" +
                        "<tr>" +
                        "<td class=\"CategoryLeftBorder\">&nbsp;</td>" +
                        "<td class=\"CategoryTextCell\">Default</td>" +
                        "<td class=\"HiderCell\">" +
                        "<a id=\"" + currentGuid + "_link\" href=\"#\" onclick=\"ShowHideProperties('" + currentGuid + "'); return false;\">" +
                        "<img border=\"0\" src=\"" + minImg + "\" ></a>" +
                        "</td>" +
                        "<td class=\"CategoryRightBorder\">&nbsp;</td>" +
                        "</tr></table>";
                    }

                    if (!isOpenSubTable)
                    {
                        isOpenSubTable = true;
                        ltrProperties.Text += "" +
                            "<table cellpadding=\"8\" cellspacing=\"0\" id=\"" + currentGuid + "\" class=\"PropertiesTable\" >";
                        currentGuid = "";
                    }

                    string doubleDot = "";
                    if (!ffi.Caption.EndsWith(":"))
                    {
                        doubleDot = ":";
                    }

                    ltrProperties.Text += "" +
                        "<tr>" +
                        "<td class=\"PropertyLeftBorder\" >&nbsp;</td>" +
                        "<td class=\"PropertyContent\" style=\"width:200px;\">" + ffi.Caption + doubleDot + "</td>" +
                        "<td>" + HTMLHelper.HTMLEncode(resolver.ResolveMacros(DataHelper.GetNotEmpty(ffi.Description, ResHelper.GetString("WebPartDocumentation.DescriptionNoneAvailable")))) + "</td>" +
                        "<td class=\"PropertyRightBorder\">&nbsp;</td>" +
                        "</tr>";
                }
            }
        }

        if (isOpenSubTable)
        {
            ltrProperties.Text += "<tr class=\"PropertyBottom\"><td class=\"PropertyLeftBottom\">&nbsp;</td><td colspan=\"2\" class=\"Center\">&nbsp;</td><td class=\"PropertyRightBottom\">&nbsp;</td></tr></table>";
        }
    }


    /// <summary>
    /// Disable handler base tag
    /// </summary>
    protected override void OnInit(EventArgs e)
    {
        UseBaseTagForHandlerPage = false;
        base.OnInit(e);
    }
}
