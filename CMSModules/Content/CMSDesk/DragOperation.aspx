<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DragOperation.aspx.cs" Inherits="CMSModules_Content_CMSDesk_DragOperation"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Content - Drop" %>

<%@ Register Src="~/CMSModules/Content/Controls/DragOperation.ascx" TagName="DragOperation" TagPrefix="cms" %>
<asp:Content ID="cnt1" ContentPlaceHolderID="plcContent" runat="server">
    <cms:DragOperation runat="server" id="opDrag" />
</asp:Content>
