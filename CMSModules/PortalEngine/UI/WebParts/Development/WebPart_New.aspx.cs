using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.FormEngine;
using CMS.PortalEngine;
using CMS.UIControls;
using CMS.ExtendedControls.Dialogs;

public partial class CMSModules_PortalEngine_UI_WebParts_Development_WebPart_New : SiteManagerPage
{
    #region "Variables"

    string[,] pageTitleTabs = new string[2, 3];

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Setup page title text and image
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Development-WebPart_Edit.TitleNew");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_WebPart/new.png");

        this.CurrentMaster.Title.HelpTopicName = "new_web_part";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Initialize
        btnOk.Text = ResHelper.GetString("general.ok");
        rfvWebPartDisplayName.ErrorMessage = ResHelper.GetString("Development-WebPart_Edit.ErrorDisplayName");
        rfvWebPartName.ErrorMessage = ResHelper.GetString("Development-WebPart_Edit.ErrorWebPartName");

        radNewWebPart.Text = ResHelper.GetString("developmentwebparteditnewwepart");
        radInherited.Text = ResHelper.GetString("Development-WebPart_Edit.Inherited");
        lblWebpartList.Text = ResHelper.GetString("DevelopmentWebPartEdit.InheritedWebPart");

        pageTitleTabs[0, 0] = ResHelper.GetString("Development-WebPart_Edit.WebParts");
        pageTitleTabs[0, 1] = ResolveUrl("WebPart_Frameset.aspx");
        pageTitleTabs[0, 2] = "_parent";
        pageTitleTabs[1, 0] = ResHelper.GetString("Development-WebPart_Edit.New");
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;

        FileSystemDialogConfiguration config = new FileSystemDialogConfiguration();
        config.DefaultPath = "CMSWebParts";
        config.AllowedExtensions = "ascx";
        config.ShowFolders = false;
        FileSystemSelector.DialogConfig = config;
        FileSystemSelector.AllowEmptyValue = true;
        FileSystemSelector.SelectedPathPrefix = "~/CMSWebParts/";
    }


    /// <summary>
    /// Handles radio buttons change
    /// </summary>
    protected void radNewWebPart_CheckedChanged(object sender, EventArgs e)
    {
        plcFileName.Visible = radNewWebPart.Checked;
        plcWebparts.Visible = radInherited.Checked;
    }


    /// <summary>
    /// Create new web part
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Validate the text box fields
        string errorMessage = new Validator().IsCodeName(txtWebPartName.Text, ResHelper.GetString("general.invalidcodename")).Result;

        // Check file name
        if (errorMessage == String.Empty)
        {
            string webpartPath = GetWebPartPhysicalPath(FileSystemSelector.Value.ToString());

            if (!radInherited.Checked)
            {
                errorMessage = new Validator().IsFileName(Path.GetFileName(webpartPath), ResHelper.GetString("WebPart_Clone.InvalidFileName")).Result;
            }
        }

        if (errorMessage != String.Empty)
        {
            lblError.Text = errorMessage;
            lblError.Visible = true;
            return;
        }

        WebPartInfo wi = new WebPartInfo();

        // Check if new name is unique
        WebPartInfo webpart = WebPartInfoProvider.GetWebPartInfo(txtWebPartName.Text);
        if (webpart != null)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Development.WebParts.WebPartNameAlreadyExist").Replace("%%name%%", txtWebPartName.Text);
            return;
        }


        string filename = FileSystemSelector.Value.ToString().Trim();
        if (filename.ToLower().StartsWith("~/cmswebparts/"))
        {
            filename = filename.Substring("~/cmswebparts/".Length);
        }

        wi.WebPartDisplayName = txtWebPartDisplayName.Text.Trim();
        wi.WebPartFileName = filename;
        wi.WebPartName = txtWebPartName.Text.Trim();
        wi.WebPartCategoryID = ValidationHelper.GetInteger(Request.QueryString["parentid"], 0);
        wi.WebPartDescription = "";

        // Inherited webpart
        if (radInherited.Checked)
        {
            // Check if is selected webpart and isn't category item
            if (ValidationHelper.GetInteger(webpartSelector.Value, 0) <= 0)
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("WebPartNew.InheritedCategory");
                return;
            }

            wi.WebPartParentID = ValidationHelper.GetInteger(webpartSelector.Value, 0);

            // Create empty default values definition
            wi.WebPartProperties = "<defaultvalues></defaultvalues>";
        }
        else
        {
            // Check if filename was added
            if (FileSystemSelector.Value.ToString().Trim() == "")
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Development-WebPart_Edit.ErrorFileName");

                return;
            }
            else
            {
                wi.WebPartProperties = "<form></form>";
                wi.WebPartParentID = 0;
            }
        }


        WebPartInfoProvider.SetWebPartInfo(wi);

        // refresh web part tree
        ScriptHelper.RegisterStartupScript(this, typeof(string), "reloadframe", ScriptHelper.GetScript(
        "parent.frames['webparttree'].location.replace('WebPart_Tree.aspx?webpartid=" + wi.WebPartID + "');" +
        "location.replace('WebPart_Edit_Frameset.aspx?webpartid=" + wi.WebPartID + "')"
        ));

        pageTitleTabs[1, 0] = wi.WebPartDisplayName;
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        plcTable.Visible = false;
    }


    private string GetWebPartPhysicalPath(string webpartPath)
    {
        webpartPath = webpartPath.Trim();

        if (webpartPath.StartsWith("~/"))
        {
            return Server.MapPath(webpartPath);
        }

        string fileName = webpartPath.Trim('/').Replace('/', '\\');
        return Path.Combine(Server.MapPath(WebPartInfoProvider.WebPartsDirectory), fileName);
    }
}
