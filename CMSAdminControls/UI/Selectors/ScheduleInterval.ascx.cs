using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Scheduler;
using CMS.UIControls;

public partial class CMSAdminControls_UI_Selectors_ScheduleInterval : CMSUserControl
{
    protected string mScheduleInterval = null;

    #region "'Public properties'"

    /// <summary>
    /// Public property.
    /// </summary>
    public string ScheduleInterval
    {
        get
        {
            mScheduleInterval = EncodeInterval();

            return mScheduleInterval;
        }
        set
        {
            mScheduleInterval = value;

            DecodeInterval(mScheduleInterval);
        }
    }

    #endregion // Public properties

    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        EnsureChildControls();
    }


    protected override void CreateChildControls()
    {
        base.CreateChildControls();
        lblError.Text = "";
        LabelInit();
        if (!RequestHelper.IsPostBack())
        {
            ControlInit();
            ValidatorInit();
        }
    }


    /// <summary>
    /// On selected index changed.
    /// </summary>
    protected void DrpPeriod_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        switch (drpPeriod.SelectedIndex)
        {
            case 0: // Minute
                pnlEvery.Visible = true;
                pnlBetween.Visible = true;
                pnlDays.Visible = true;
                pnlMonth.Visible = false;
                lblEveryPeriod.Text = ResHelper.GetString("ScheduleInterval.Period.Minute");
                break;

            case 1: // Hour
                pnlEvery.Visible = true;
                pnlBetween.Visible = true;
                pnlDays.Visible = true;
                pnlMonth.Visible = false;
                lblEveryPeriod.Text = ResHelper.GetString("ScheduleInterval.Period.Hour");
                break;

            case 2: // Day
                pnlEvery.Visible = true;
                pnlBetween.Visible = false;
                pnlDays.Visible = true;
                pnlMonth.Visible = false;
                lblEveryPeriod.Text = ResHelper.GetString("ScheduleInterval.Period.Day");
                break;

            case 3: // Week
                pnlEvery.Visible = true;
                pnlBetween.Visible = false;
                pnlDays.Visible = false;
                pnlMonth.Visible = false;
                lblEveryPeriod.Text = ResHelper.GetString("ScheduleInterval.Period.Week");
                break;

            case 4: // Month
                pnlEvery.Visible = false;
                pnlBetween.Visible = false;
                pnlDays.Visible = false;
                pnlMonth.Visible = true;
                break;

            case 5: // Once
                pnlEvery.Visible = false;
                pnlBetween.Visible = false;
                pnlDays.Visible = false;
                pnlMonth.Visible = false;
                break;
        }

        // set default values to textboxes and checkboxlists
        dateTimePicker.SelectedDateTime = DateTime.Now;
        txtEvery.Text = "1";
        txtFromHours.Text = "00";
        txtFromMinutes.Text = "00";
        txtToHours.Text = "23";
        txtToMinutes.Text = "59";
        foreach (ListItem li in chkWeek.Items)
        {
            li.Selected = true;
        }
        foreach (ListItem li in chkWeekEnd.Items)
        {
            li.Selected = true;
        }
        drpMonth1.SelectedIndex = 0;
        drpMonth2.SelectedIndex = 0;
        drpMonth3.SelectedIndex = 0;
    }


    /// <summary>
    /// Initialization of labels.
    /// </summary>
    protected void LabelInit()
    {
        // Panel Period
        lblPeriod.Text = ResHelper.GetString("ScheduleInterval.Period");
        lblStart.Text = ResHelper.GetString("ScheduleInterval.Start");

        // Panel Every
        lblEvery.Text = ResHelper.GetString("ScheduleInterval.Every");

        // Panel Between
        lblBetween.Text = ResHelper.GetString("ScheduleInterval.Between");
        lblAnd.Text = ResHelper.GetString("ScheduleInterval.And");

        // Panel Days
        lblDays.Text = ResHelper.GetString("ScheduleInterval.Days");

        // Panel Months
        lblMonth1.Text = ResHelper.GetString("ScheduleInterval.Months.OfTheMonth(s)");
        lblMonth2.Text = lblMonth1.Text;

        // text box color
        dateTimePicker.DateTimeTextBox.ForeColor = System.Drawing.Color.Black;
        txtEvery.ForeColor = System.Drawing.Color.Black;
        txtFromHours.ForeColor = System.Drawing.Color.Black;
        txtFromMinutes.ForeColor = System.Drawing.Color.Black;
        txtToHours.ForeColor = System.Drawing.Color.Black;
        txtToMinutes.ForeColor = System.Drawing.Color.Black;
    }


    /// <summary>
    /// Initialization of controls.
    /// </summary>
    protected void ControlInit()
    {
        ListItem li = null;
        // Panel Period
        // dropdownlist initialization
        if (!RequestHelper.IsPostBack())
        {
            li = new ListItem(ResHelper.GetString("ScheduleInterval.Period.Minute"), SchedulingHelper.PERIOD_MINUTE);
            drpPeriod.Items.Add(li);
            li = new ListItem(ResHelper.GetString("ScheduleInterval.Period.Hour"), SchedulingHelper.PERIOD_HOUR);
            drpPeriod.Items.Add(li);
            li = new ListItem(ResHelper.GetString("ScheduleInterval.Period.Day"), SchedulingHelper.PERIOD_DAY);
            drpPeriod.Items.Add(li);
            li = new ListItem(ResHelper.GetString("ScheduleInterval.Period.Week"), SchedulingHelper.PERIOD_WEEK);
            drpPeriod.Items.Add(li);
            li = new ListItem(ResHelper.GetString("ScheduleInterval.Period.Month"), SchedulingHelper.PERIOD_MONTH);
            drpPeriod.Items.Add(li);
            li = new ListItem(ResHelper.GetString("ScheduleInterval.Period.Once"), SchedulingHelper.PERIOD_ONCE);
            drpPeriod.Items.Add(li);
        }

        // datetimepicker initialization
        dateTimePicker.DateTimeTextBox.CssClass = "EditingFormCalendarTextBox";
        dateTimePicker.SelectedDateTime = DateTime.Now;

        // Panel Every
        lblEvery.Text = ResHelper.GetString("ScheduleInterval.Every");
        lblEveryPeriod.Text = ResHelper.GetString("ScheduleInterval.Period.Minute");
        txtEvery.Text = "1";

        // Panel Between
        lblBetween.Text = ResHelper.GetString("ScheduleInterval.Between");
        lblAnd.Text = ResHelper.GetString("ScheduleInterval.And");
        txtFromHours.Text = "00";
        txtFromMinutes.Text = "00";
        txtToHours.Text = "23";
        txtToMinutes.Text = "59";

        // Panel Days
        lblDays.Text = ResHelper.GetString("ScheduleInterval.Days");

        // checkboxlist and dropdownlist initialization
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Days.Monday"), DayOfWeek.Monday.ToString());
        li.Selected = true;
        chkWeek.Items.Add(li);
        drpMonth3.Items.Add(li);
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Days.Tuesday"), DayOfWeek.Tuesday.ToString());
        li.Selected = true;
        chkWeek.Items.Add(li);
        drpMonth3.Items.Add(li);
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Days.Wednesday"), DayOfWeek.Wednesday.ToString());
        li.Selected = true;
        chkWeek.Items.Add(li);
        drpMonth3.Items.Add(li);
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Days.Thursday"), DayOfWeek.Thursday.ToString());
        li.Selected = true;
        chkWeek.Items.Add(li);
        drpMonth3.Items.Add(li);
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Days.Friday"), DayOfWeek.Friday.ToString());
        li.Selected = true;
        chkWeek.Items.Add(li);
        drpMonth3.Items.Add(li);
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Days.Saturday"), DayOfWeek.Saturday.ToString());
        li.Selected = true;
        chkWeekEnd.Items.Add(li);
        drpMonth3.Items.Add(li);
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Days.Sunday"), DayOfWeek.Sunday.ToString());
        li.Selected = true;
        chkWeekEnd.Items.Add(li);
        drpMonth3.Items.Add(li);

        // Panel Months
        // radiobutton initialization
        radMonth1.Text = ResHelper.GetString("ScheduleInterval.Period.Day");
        radMonth2.Text = ResHelper.GetString("ScheduleInterval.Months.The");
        // Dropdownlist initialization
        for (int i = 1; i <= 31; i++)
        {
            li = new ListItem(i.ToString());
            drpMonth1.Items.Add(li);
        }

        lblMonth1.Text = ResHelper.GetString("ScheduleInterval.Months.OfTheMonth(s)");
        lblMonth2.Text = lblMonth1.Text;
        //dropdownlist initialization
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Months.First"), SchedulingHelper.MONTHS_FIRST);
        drpMonth2.Items.Add(li);
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Months.Second"), SchedulingHelper.MONTHS_SECOND);
        drpMonth2.Items.Add(li);
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Months.Third"), SchedulingHelper.MONTHS_THIRD);
        drpMonth2.Items.Add(li);
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Months.Fourth"), SchedulingHelper.MONTHS_FOURTH);
        drpMonth2.Items.Add(li);
        li = new ListItem(ResHelper.GetString("ScheduleInterval.Months.Last"), SchedulingHelper.MONTHS_LAST);
        drpMonth2.Items.Add(li);

        /*foreach (ListItem item in drpMonth2.Items)
        {
            item.Value = item.Text.ToLower();
        }*/
        /*  foreach (ListItem item in drpMonth3.Items)
          {
              item.Value = item.Text.ToLower();
          }*/

        drpMonth2.Enabled = false;
        drpMonth3.Enabled = false;
    }


    /// <summary>
    /// Initialization of validators.
    /// </summary>
    protected void ValidatorInit()
    {
        string error = ResHelper.GetString("ScheduleInterval.WrongFormat");
        string empty = ResHelper.GetString("ScheduleInterval.ErrorEmpty");
        // 'Every' panel validators
        rfvEvery.ErrorMessage = empty;
        rvEvery.MinimumValue = "0";
        rvEvery.MaximumValue = "10000";
        rvEvery.ErrorMessage = error;
        // 'Between' panel validators
        rfvFromHours.ErrorMessage = empty;
        rvFromHours.MinimumValue = "0";
        rvFromHours.MaximumValue = "23";
        rvFromHours.ErrorMessage = error;
        rfvFromMinutes.ErrorMessage = empty;
        rvFromMinutes.MinimumValue = "0";
        rvFromMinutes.MaximumValue = "59";
        rvFromMinutes.ErrorMessage = error;
        rfvToHours.ErrorMessage = empty;
        rvToHours.MinimumValue = "0";
        rvToHours.MaximumValue = "23";
        rvToHours.ErrorMessage = error;
        rfvToMinutes.ErrorMessage = empty;
        rvToMinutes.MinimumValue = "0";
        rvToMinutes.MaximumValue = "59";
        rvToMinutes.ErrorMessage = error;
    }


    /// <summary>
    /// On 1st radio button change.
    /// </summary>
    protected void radMonth1_CheckedChanged(object sender, EventArgs e)
    {
        radMonth2.Checked = false;
        drpMonth1.Enabled = true;
        drpMonth2.Enabled = false;
        drpMonth3.Enabled = false;
    }


    /// <summary>
    /// On 2nd radio button change.
    /// </summary>
    protected void radMonth2_CheckedChanged(object sender, EventArgs e)
    {
        radMonth1.Checked = false;
        drpMonth1.Enabled = false;
        drpMonth2.Enabled = true;
        drpMonth3.Enabled = true;
    }


    public bool CheckIntervalPreceedings()
    {
        switch (drpPeriod.SelectedIndex)
        {
            case 0: // Minute
            case 1: // Hour
                TimeSpan start = new TimeSpan(Convert.ToInt32(txtFromHours.Text), Convert.ToInt32(txtFromMinutes.Text), 0);
                TimeSpan end = new TimeSpan(Convert.ToInt32(txtToHours.Text), Convert.ToInt32(txtToMinutes.Text), 0); ;
                return (start.CompareTo(end) < 0);
            default: return true;
        }
    }


    public bool CheckOneDayMinimum()
    {
        switch (drpPeriod.SelectedIndex)
        {
            case 0: // Minute
            case 1: // Hour
            case 2: // Day
                return ((chkWeek.SelectedItem != null)||(chkWeekEnd.SelectedItem != null));
            default: return true;
        }
    }


    /// <summary>
    /// Creates schedule interval string.
    /// </summary>
    protected string EncodeInterval()
    {
        //string intervalCode = null;
        TaskInterval ti = new TaskInterval();

        string error = ResHelper.GetString("ScheduleInterval.WrongFormat");
        string empty = ResHelper.GetString("ScheduleInterval.ErrorEmpty");
        string result = "";

        try
        {
            ti.Period = drpPeriod.SelectedValue;
            ti.StartTime = dateTimePicker.SelectedDateTime;

            switch (drpPeriod.SelectedIndex)
            {
                case 0: // Minute
                case 1: // Hour
                    result = new Validator().NotEmpty(txtFromHours.Text, empty).IsInteger(txtFromHours.Text, error).NotEmpty(txtFromMinutes.Text, empty).IsInteger(txtFromMinutes.Text, error).NotEmpty(txtToHours.Text, empty).IsInteger(txtToHours.Text, error).NotEmpty(txtToMinutes.Text, empty).IsInteger(txtToMinutes.Text, error).NotEmpty(txtEvery.Text, empty).IsInteger(txtEvery.Text, error).Result;
                    if (result == "")
                    {
                        if ((Convert.ToInt32(txtFromHours.Text) >= 0) && (Convert.ToInt32(txtFromHours.Text) <= 23) && (Convert.ToInt32(txtFromMinutes.Text) >= 0) && (Convert.ToInt32(txtFromMinutes.Text) <= 59))
                        {
                            if ((Convert.ToInt32(txtToHours.Text) >= 0) && (Convert.ToInt32(txtToHours.Text) <= 23) && (Convert.ToInt32(txtToMinutes.Text) >= 0) && (Convert.ToInt32(txtToMinutes.Text) <= 59))
                            {
                                if ((Convert.ToInt32(txtEvery.Text) >= 0) && (Convert.ToInt32(txtEvery.Text) <= 10000))
                                {
                                    DateTime time = new DateTime();
                                    ti.Every = Convert.ToInt32(txtEvery.Text);
                                    time = new DateTime(1, 1, 1, Convert.ToInt32(txtFromHours.Text), Convert.ToInt32(txtFromMinutes.Text), 0);
                                    ti.BetweenStart = time;
                                    time = new DateTime(1, 1, 1, Convert.ToInt32(txtToHours.Text), Convert.ToInt32(txtToMinutes.Text), 0);
                                    ti.BetweenEnd = time;

                                    foreach (ListItem item in chkWeek.Items)
                                    {
                                        if (item.Selected)
                                        {
                                            ti.Days.Add(item.Value);
                                        }
                                    }
                                    foreach (ListItem item in chkWeekEnd.Items)
                                    {
                                        if (item.Selected)
                                        {
                                            ti.Days.Add(item.Value);
                                        }
                                    }
                                }
                                else
                                {
                                    lblError.Text = error;
                                    txtEvery.ForeColor = System.Drawing.Color.Red;
                                }
                            }
                            else
                            {
                                lblError.Text = error;
                                txtToHours.ForeColor = System.Drawing.Color.Red;
                                txtToMinutes.ForeColor = System.Drawing.Color.Red;
                            }
                        }
                        else
                        {
                            lblError.Text = error;
                            txtFromHours.ForeColor = System.Drawing.Color.Red;
                            txtFromMinutes.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    break;

                case 2: // Day
                    result = new Validator().NotEmpty(txtEvery.Text, empty).IsInteger(txtEvery.Text, error).Result;
                    if (result == "")
                    {
                        if ((Convert.ToInt32(txtEvery.Text) >= 0) && (Convert.ToInt32(txtEvery.Text) <= 10000))
                        {
                            ti.Every = Convert.ToInt32(txtEvery.Text);
                            // Days
                            foreach (ListItem item in chkWeek.Items)
                            {
                                if (item.Selected)
                                {
                                    ti.Days.Add(item.Value);
                                }
                            }
                            foreach (ListItem item in chkWeekEnd.Items)
                            {
                                if (item.Selected)
                                {
                                    ti.Days.Add(item.Value);
                                }
                            }
                        }
                        else
                        {
                            lblError.Text = error;
                            txtEvery.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    break;

                case 3: // Week
                    result = new Validator().NotEmpty(txtEvery.Text, empty).IsInteger(txtEvery.Text, error).Result;
                    if (result == "")
                    {
                        if ((Convert.ToInt32(txtEvery.Text) >= 0) && (Convert.ToInt32(txtEvery.Text) <= 10000))
                        {
                            ti.Every = Convert.ToInt32(txtEvery.Text);
                        }
                        else
                        {
                            lblError.Text = error;
                            txtEvery.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    break;

                case 4: // Month
                    if (radMonth1.Checked)
                    {
                        ti.Order = drpMonth1.SelectedValue;
                    }
                    else
                    {
                        ti.Order = drpMonth2.SelectedValue;
                        ti.Day = drpMonth3.SelectedValue;
                    }
                    break;

                case 5: // Once
                    break;
            }
        }
        catch
        {
            lblError.Text = error;
            dateTimePicker.DateTimeTextBox.ForeColor = System.Drawing.Color.Red;
        }


        if (result != "")
        {
            lblError.Text = result;
            ti = new TaskInterval();
        }

        string enti = SchedulingHelper.EncodeInterval(ti);
        return enti;
    }


    /// <summary>
    /// Sets form with values from the schedule interval string.
    /// </summary>
    /// <param name="interval">Schedule interval string.</param>
    protected void DecodeInterval(string interval)
    {
        EnsureChildControls();

        TaskInterval ti = new TaskInterval();
        ti = SchedulingHelper.DecodeInterval(interval);

        if (interval != null)
        {
            // Period type
            drpPeriod.SelectedValue = ti.Period;
            DrpPeriod_OnSelectedIndexChanged(null, null);
            // start time
            dateTimePicker.SelectedDateTime = ti.StartTime;

            switch (drpPeriod.SelectedIndex)
            {
                case 0: // Minute
                case 1: // Hour
                    txtEvery.Text = ti.Every.ToString();
                    txtFromHours.Text = ti.BetweenStart.TimeOfDay.Hours >= 10 ? ti.BetweenStart.TimeOfDay.Hours.ToString() : "0" + ti.BetweenStart.TimeOfDay.Hours.ToString();
                    txtFromMinutes.Text = ti.BetweenStart.TimeOfDay.Minutes >= 10 ? ti.BetweenStart.TimeOfDay.Minutes.ToString() : "0" + ti.BetweenStart.TimeOfDay.Minutes.ToString();
                    txtToHours.Text = ti.BetweenEnd.TimeOfDay.Hours >= 10 ? ti.BetweenEnd.TimeOfDay.Hours.ToString() : "0" + ti.BetweenEnd.TimeOfDay.Hours.ToString();
                    txtToMinutes.Text = ti.BetweenEnd.TimeOfDay.Minutes >= 10 ? ti.BetweenEnd.TimeOfDay.Minutes.ToString() : "0" + ti.BetweenEnd.TimeOfDay.Minutes.ToString();


                    foreach (ListItem li in chkWeek.Items)
                    {
                        li.Selected = false;
                        foreach (string Day in ti.Days)
                        {
                            if (li.Value.ToLower() == Day.ToLower())
                            {
                                li.Selected = true;
                            }
                        }
                    }
                    foreach (ListItem li in chkWeekEnd.Items)
                    {
                        li.Selected = false;
                        foreach (string Day in ti.Days)
                        {
                            if (li.Value.ToLower() == Day.ToLower())
                            {
                                li.Selected = true;
                            }
                        }
                    }

                    break;

                case 2: // Day
                    txtEvery.Text = ti.Every.ToString();

                    foreach (ListItem li in chkWeek.Items)
                    {
                        li.Selected = false;
                        foreach (string Day in ti.Days)
                        {
                            if (li.Value.ToLower() == Day.ToLower())
                            {
                                li.Selected = true;
                            }
                        }
                    }
                    foreach (ListItem li in chkWeekEnd.Items)
                    {
                        li.Selected = false;
                        foreach (string Day in ti.Days)
                        {
                            if (li.Value.ToLower() == Day.ToLower())
                            {
                                li.Selected = true;
                            }
                        }
                    }
                    break;

                case 3: // Week
                    txtEvery.Text = ti.Every.ToString();
                    break;

                case 4: // Month
                    if (ti.Day == "")
                    {
                        drpMonth1.SelectedValue = ti.Order.ToLower();
                        radMonth1.Checked = true;
                        radMonth1_CheckedChanged(null, null);
                    }
                    else
                    {
                        drpMonth2.SelectedValue = ti.Order.ToLower();
                        radMonth2.Checked = true;
                        radMonth2_CheckedChanged(null, null);
                        drpMonth3.SelectedValue = ti.Day;
                    }
                    break;
            }
        }
    }
}
