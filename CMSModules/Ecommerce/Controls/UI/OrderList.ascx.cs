using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Controls_UI_OrderList : CMSAdminListControl
{
    protected int customerId;
    protected bool isAdmin = false;
    private int siteId;

    protected void Page_Load(object sender, EventArgs e)
    {
        isAdmin = (CMSContext.CurrentUser != null) && (CMSContext.CurrentUser.IsGlobalAdministrator);

        if (!isAdmin)
        {
            this.plcSiteFilter.Visible = false;
            siteSelector.StopProcessing = true;
            siteId = CMSContext.CurrentSiteID;
        }
        else
        {
            // Set site selector
            siteSelector.AllowAll = true;

            if (!RequestHelper.IsPostBack())
            {
                siteId = CMSContext.CurrentSiteID;
                siteSelector.Value = siteId;
            }
            else
            {
                siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);
            }
        }

        // Set filter controls
        lblStatus.Text = ResHelper.GetString("OrderList.StatusLabel");
        lblSites.Text = ResHelper.GetString("OrderList.SitesLabel");
        lblOrderID.Text = ResHelper.GetString("OrderList.OrderIdLabel");
        lblCustomerFirstName.Text = ResHelper.GetString("OrderList.CustomerFirstName");
        lblCustomerLastName.Text = ResHelper.GetString("OrderList.CustomerLastName");

        btnFilter.Text = ResHelper.GetString("general.show");

        customerId = QueryHelper.GetInteger("customerId", 0);

        gridElem.IsLiveSite = this.IsLiveSite;
        gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.GridView.RowDataBound += new GridViewRowEventHandler(GridView_RowDataBound);
        gridElem.ImageDirectoryPath = GetImageUrl("Design/Controls/UniGrid/Actions/", IsLiveSite, true);
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
        gridElem.WhereCondition = GetWhereCondition();

        if (customerId > 0)
        {
            this.plcCustomerFilter.Visible = false;
        }
    }

    private string GetWhereCondition()
    {
        string where = "";

        // status dropdownlist
        if (statusElem.OrderStatusID > 0)
        {
            where += " OrderStatusID = " + statusElem.OrderStatusID + " AND";
        }

        // site filter
        if (siteId > 0)
        {
            where += " OrderSiteID = " + siteId + " AND";
        }

        if (txtOrderId.Text.Trim() != "")
        {
            where += " OrderID = " + ValidationHelper.GetInteger(txtOrderId.Text.Trim(), 0) + " AND";
        }

        if (customerId > 0)
        {
            where += " CustomerID = " + customerId + " AND";
        }
        else
        {
            if (txtCustomerFirstName.Text.Trim() != "")
            {
                where += " CustomerFirstName LIKE '%" + txtCustomerFirstName.Text.Replace("'", "''").Trim() + "%' AND";
            }

            if (txtCustomerLastName.Text.Trim() != "")
            {
                where += " CustomerLastName LIKE '%" + txtCustomerLastName.Text.Replace("'", "''").Trim() + "%' AND";
            }
        }

        if (where != "")
        {
            where = where.Remove(where.Length - 4); // 4 = " AND".Length
        }

        return where;
    }


    void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string color = ValidationHelper.GetString(((DataRowView)(e.Row.DataItem)).Row["StatusColor"], "");
            if (color != "")
            {
                e.Row.Style.Add("background-color", color);
            }
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (customerId > 0)
        {
            gridElem.GridView.Columns[2].Visible = false;
            gridElem.GridView.Columns[3].Visible = false;
        }
    }


    object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView dr = null;
        switch (sourceName.ToLower())
        {
            case "fullname":
                dr = (DataRowView)parameter;
                return HTMLHelper.HTMLEncode(dr["CustomerFirstName"] + " " + dr["CustomerLastName"]);

            case "totalprice":
                dr = (DataRowView)parameter;
                return String.Format(dr["CurrencyFormatString"].ToString(), dr["OrderTotalPrice"]);

            case "statusdisplayname":
                return HTMLHelper.HTMLEncode(ResHelper.LocalizeString(Convert.ToString(parameter)));
            //break;
        }
        return parameter;
    }


    /// <summary>
    /// Handles the gridElem's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        int orderId = ValidationHelper.GetInteger(actionArgument, 0);
        OrderInfo oi = null;
        OrderStatusInfo osi = null;

        switch (actionName.ToLower())
        {
            case "edit":
                string redirectToUrl = "Order_Edit.aspx?orderID=" + orderId.ToString();
                if (customerId > 0)
                {
                    redirectToUrl += "&customerId=" + customerId.ToString();
                }
                UrlHelper.Redirect(redirectToUrl);
                break;


            case "delete":
                // check 'EcommerceModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
                {
                    AccessDenied("CMS.Ecommerce", "EcommerceModify");
                }

                // delete OrderInfo object from database
                OrderInfoProvider.DeleteOrderInfo(orderId);
                break;

            case "previous":
                // check 'EcommerceModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
                {
                    AccessDenied("CMS.Ecommerce", "EcommerceModify");
                }

                oi = OrderInfoProvider.GetOrderInfo(orderId);
                if (oi != null)
                {
                    osi = OrderStatusInfoProvider.GetPreviousEnabledStatus(oi.OrderStatusID);
                    if (osi != null)
                    {
                        oi.OrderStatusID = osi.StatusID;
                        // Save order status changes
                        OrderInfoProvider.SetOrderInfo(oi);
                    }
                }
                break;

            case "next":
                // check 'EcommerceModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
                {
                    AccessDenied("CMS.Ecommerce", "EcommerceModify");
                }

                oi = OrderInfoProvider.GetOrderInfo(orderId);
                if (oi != null)
                {
                    osi = OrderStatusInfoProvider.GetNextEnabledStatus(oi.OrderStatusID);
                    if (osi != null)
                    {
                        oi.OrderStatusID = osi.StatusID;
                        // Save order status changes
                        OrderInfoProvider.SetOrderInfo(oi);
                    }
                }
                break;
        }
    }
}
