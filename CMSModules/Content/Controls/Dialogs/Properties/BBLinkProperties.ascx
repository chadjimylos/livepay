<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BBLinkProperties.ascx.cs"
    Inherits="CMSModules_Content_Controls_Dialogs_Properties_BBLinkProperties" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/General/WidthHeightSelector.ascx" TagPrefix="cms"
    TagName="WidthHeightSelector" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/General/URLSelector.ascx" TagPrefix="cms" TagName="URLSelector" %>
<div class="BBLinkProperties" enableviewstate="false">
    <asp:Panel runat="server" ID="pnlEmpty" Visible="true" CssClass="DialogInfoArea">
        <asp:Label runat="server" ID="lblEmpty" EnableViewState="false"/>
    </asp:Panel>
    <ajaxToolkit:TabContainer ID="pnlTabs" runat="server" CssClass="DialogElementHidden">
        <ajaxToolkit:TabPanel ID="tabGeneral" runat="server">
            <ContentTemplate>
                <cms:URLSelector runat="server" ID="urlSelectElem" />
                <cms:CMSButton ID="btnHidden" runat="server" CssClass="HiddenButton" EnableViewState="false" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
</div>
