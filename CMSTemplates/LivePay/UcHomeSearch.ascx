﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UcHomeSearch.ascx.vb" Inherits="CMSTemplates_LivePay_UcHomeSearch" %>
<script language="javascript" type="text/javascript" >
    var event_c;
    var obj_c;

    function handle_keydownHome(e) {
       
        document.getElementById('<%=btnSearch.ClientId %>').focus();
        $('#' + '<%=btnSearch.ClientId %>').click()
    }



    function RebindRpt(CachValue) {
        
        if (CheckAction(event_c, obj_c)) {
            CachValue = CachValue.replace(/[']/gi, "@@@@")
            setTimeout("ExecSearch('" + CachValue + "')", 500)
        } else {
            ControlRows(event_c, obj_c)
        }
    }

    function ExecSearch(CachValue) {
        var RealValue = document.getElementById('<%=txtSearch.ClientID %>').value
        RealValue = RealValue.replace(/[']/gi, "@@@@") 

        if (RealValue == CachValue) {
            var text = document.getElementById('<%=txtSearch.clientID %>').value;
            text = text.replace(/[']/gi, "@@@@") 
            if (text != null && text.length > 0) {
              
                GetHomeMerchantData(RealValue)

            } else {
                ShowHideQuickRes(false, 0)
            }
        }
    }

    function GetHomeMerchantData(val) {
        var x;
        if (window.ActiveXObject) {
            x = new ActiveXObject("Microsoft.XMLHTTP");
        }
        else {
            x = new XMLHttpRequest();
        }
        val = escape(val)
        x.open("GET", "/CMSTemplates/LivePay/xmlHttpMerchants.aspx?MerchantKeyWord=" + val + "&RecordsNo=3&FormPage=home&lang=" + '<%=request("lang") %>', true);
       
        x.onreadystatechange = function () {
            if (x.readyState == 4) {
                var Results = x.responseText;
                var RowCount = Results.split("#]")[0]
                Results = Results.split("#]")[1]

                if (RowCount > 0) {
                   
                    document.getElementById('QuickSrcResAll').innerHTML = Results;
                    ShowHideQuickRes(true, RowCount);
                } else {
              
                    ShowHideQuickRes(false, 0);
                }

            }
        }
        x.send(null);
    }

    

    function SetFocus() {
        var elem = document.getElementById('<%=txtSearch.clientID %>');
        var caretPos = elem.value.length
        if (elem != null) {
            if (elem.createTextRange) {
                var range = elem.createTextRange();
                range.move('character', caretPos);
                range.select();
            }
            else {
                elem.setSelectionRange(caretPos, caretPos);
                elem.focus();
                // Workaround for FF overflow no scroll problem 
                // Trigger a "space" keypress. 
                var evt = document.createEvent("KeyboardEvent");
                evt.initKeyEvent("keypress", true, true, null, false, false, false, false, 0, 32);
                elem.dispatchEvent(evt);
                // Trigger a "backspace" keypress. 
                evt = document.createEvent("KeyboardEvent");
                evt.initKeyEvent("keypress", true, true, null, false, false, false, false, 8, 0);
                elem.dispatchEvent(evt);

            }
        }
    }


    function ShowHideQuickRes(IsVisible, RowCount) {
        
        document.getElementById('ViewMoreResults').style.display = 'none';
        if (IsVisible) {
//            if (RowCount == 1)
//                document.getElementById('QuickSrcResMain').className = 'QuickSrcResMain_1'
//            if (RowCount == 2)
//                document.getElementById('QuickSrcResMain').className = 'QuickSrcResMain_2'
//            if (RowCount > 2)
//                document.getElementById('QuickSrcResMain').className = 'QuickSrcResMain'
            
            document.getElementById('QuickResDiv').style.display = '';
            if (RowCount > 2) {
                document.getElementById('ViewMoreResults').style.display = '';
                var val = document.getElementById('<%=txtSearch.clientID %>').value
                document.getElementById('lnkViewMoreResults').href = 'Search.aspx?searchtext=' + val;
            }
            ControlRows(event_c,obj_c)
        }
        else {
            document.getElementById('QuickResDiv').style.display = 'none';
        }
    }


    function ControlRows(e,obj) {
        var keynum = 0;
        var QuickResDiv = document.getElementById('QuickResDiv')
        if (window.event) { keynum = e.keyCode; }  // IE (sucks)
        else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera
      
        if (keynum == 38) { // up
            //Move selection up
            if (obj.value.length > 0 && QuickResDiv.style.display!='none') {
                
                ChangeSelection(-1)
            } 
        }
        if (keynum == 40) { // down
            //Move selection down
            if (obj.value.length > 0 && QuickResDiv.style.display != 'none') {
                ChangeSelection(1)
            }
        }
        if (keynum == 13) { // enter
            if (obj.value.length > 0) {
                var IsSelected = false
                var SelectedObj = null;
                var AllHtml = $('.QuickSrcResTitle').get()

                $.each(AllHtml, function (index, obj) { //-- Check If Menu Is Selected
                    if ($(obj).attr("IsSelected") == 'true') {
                        IsSelected = true
                        SelectedObj = obj
                    }
                });

                if (IsSelected == false) { // if nothing is selected go to search page
                    if (window.event) {
                        return handle_keydownHome(event);
                    }
                    else if (e.which) {
                        return handle_keydownHome(e);
                    }    // Netscape/Firefox/Opera
                } else {

                    var Href = $(SelectedObj).find("a").attr("href")
                    location.href = Href
                }
            }
        }
    }

    function ChangeSelection(GoTo) {
        
        var LastSelected = -1
        //QuickSrcResTitle
        var IsSelected = false
        var AllHtml = $('.QuickSrcResTitle').get()
        
        $.each(AllHtml, function (index, obj) { //-- Check If Menu Is Selected
            if ($(obj).attr("IsSelected") == 'true') {
                IsSelected = true
                LastSelected = index
            }
        });

        if (IsSelected == false) { //-- If Not Selected Select Firstone
            $.each(AllHtml, function (index, obj) {
                if (index == 0) {
                    $(obj).attr("IsSelected", "true")
                    $(obj).css("background-color", "#d2d6db")
                }
            });
        } else {
            $.each(AllHtml, function (index, obj) {
                var GetNextSelection = LastSelected + GoTo
              
                if (GetNextSelection > -1 && GetNextSelection < 3) { 
                $(obj).attr("IsSelected", "false")
                $(obj).css("background-color", "transparent")

                if (index == GetNextSelection) {
                    $(obj).attr("IsSelected", "true")
                    $(obj).css("background-color", "#d2d6db")
                }
            }
        });
        }

}

function CheckAction(e, obj) {
    var keynum = 0;
    if (window.event) { keynum = e.keyCode; }  // IE (sucks)
    else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

    var IsSearching = true

    if (keynum == 38) { // up
        IsSearching = false
    }

    if (keynum == 40) { // down
        IsSearching = false
    }

    if (keynum == 13) { // enter
        IsSearching = false
    }

    return IsSearching
}

function Setent(e) {
    var keynum = 0;
    if (window.event) { keynum = e.keyCode; }  // IE (sucks)
    else if (e.which) { keynum = e.which; }    // Netscape/Firefox/Opera

    if (keynum == 13) {
        return false
    } else {
        return true
    }
    
    
}
</script>



<asp:UpdatePanel ID="upnl" runat="server" UpdateMode="Always"  >
<ContentTemplate >
    <div class="QuickSrcMain">
        <div style="padding-top:40px">
            <div class="QuickSrcTitleTop"><%= ResHelper.LocalizeString("{$=Καλώς ήρθατε στην υπηρεσία|en-us=Welcome to Live-Pay$}")%></div>
            <div class="QuickSrcTitleTopSec"><%= ResHelper.LocalizeString("{$=ηλεκτρονικών πληρωμών και εισπράξεων,|en-us=the online bill payment center$}")%> </div>
            <div class="QuickSrcTitleTopThird"><%=ResHelper.LocalizeString("{$=Live-Pay!|en-us=Live-Pay!$}") %></div>
            <div class="QuickSrcMainSrcDiv">
                <div class="QuickSrcMainSrcDivBG">
                    <div style="text-align:left;" >
                        <div style="color:White;font-size:12px;padding:7px 0px 0px 9px"><%= ResHelper.LocalizeString("{$=Άμεση Πληρωμή Λογαριασμού|en-us=Direct Payment$}")%></div>
                        <div class="QuickSrcTxt" style="padding-top:12px;padding-left:13px"><asp:TextBox BorderWidth="0" BackColor="Transparent"  autocomplete="off" cssClass="SearchTxt" onkeydown="return Setent(event)" onkeyup="event_c = event; obj_c = this;RebindRpt(this.value);" ID="txtSearch" runat="server" ></asp:TextBox></div>
                        <div class="QuickSrcBtn" style="padding-top:10px"><asp:ImageButton ImageUrl="/app_themes/LivePay/btnSearchOk.gif" runat="server" ID="btnSearch" /></div>
                        <div class="Clear"></div>
                        <div class="QuickSrcResTopMain" id="QuickResDiv" style="display:none">
                            <div id="QuickSrcResMain">
                                <div class="AutoCom-Top">&nbsp;</div>
                                <div class="AutoCom-Center">
                                    <div class="QuickSrcResAll" id="QuickSrcResAll">
                                    </div>
                                     <div class="QuickSrcResViewMore" id="ViewMoreResults" style="display:none">
                                     <div class="QuickSrcResTitleUnder"><a href="#" id="lnkViewMoreResults" >» <%= ResHelper.LocalizeString("{$=Δείτε Περισσότερα Αποτελέσματα|en-us=See more results$}")%></a></div>
                                     <div class="QuickSrcResTitleSmall"><%= ResHelper.LocalizeString("{$=Εμφανίζονται τα πρώτα  -3- αποτελέσματα|en-us=First -3- results$}")%></div>
                                     </div> 
                                 </div>
                                 <div class="AutoCom-Bottom">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</ContentTemplate>
</asp:UpdatePanel>

