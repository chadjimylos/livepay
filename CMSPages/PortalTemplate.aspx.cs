using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSPages_PortalTemplate : PortalPage
{
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Init the page components
        this.PageManager = this.manPortal;
        this.manPortal.SetMainPagePlaceholder(this.plc);
        this.plc.CacheMinutes = CacheHelper.CachePageInfoMinutes(CMSContext.CurrentSiteName);

        this.ManagersContainer = this.plcManagers;
        this.LogsContainer = this.plcLogs;

        this.ScriptManagerControl = this.manScript;
        this.ScriptManagerControl.AsyncPostBackTimeout = 130;

        this.Server.ScriptTimeout = 130;
        
        // Include debug if enabled
        if (CMSFunctions.AnyLiveDebugEnabled)
        {
            Control debug = LoadControl("~/CMSAdminControls/Debug/Debug.ascx");
            debug.ID = "dbg";

            this.plcLogs.Controls.Add(debug);
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Init the header tags
        //this.ltlTags.Text = this.HeaderTags;
        this.ltlTags.Text = this.HeaderTags.Replace("<meta http-equiv=\"pragma\" content=\"no-cache\" />", "");
    }
}
