using System;
using System.IO;
using System.Web;
using System.Web.UI;

using CMS.CMSHelper;
using CMS.FormEngine;
using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.PortalEngine;
using CMS.SettingsProvider;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;

using CultureInfo = System.Globalization.CultureInfo;

public partial class CMSModules_Content_CMSDesk_Edit_Edit : CMSContentPage
{
    #region "Protected variables"

    protected int nodeId = 0;
    protected bool newdocument = false;
    protected bool newculture = false;
    protected int templateId = 0;
    protected int classId = 0;
    protected bool mShowToolbar = false;
    protected bool confirmChanges = false;
    protected DataClassInfo ci = null;
    protected INewProductControl ucNewProduct = null;
    protected TreeNode node = null;
    protected TreeProvider tree = null;

    #endregion


    /// <summary>
    /// Returns true if the changes should be saved
    /// </summary>
    public bool SaveChanges
    {
        get
        {
            return ValidationHelper.GetBoolean(HttpContext.Current.Request.Params["saveChanges"], false);
        }
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
        {
            ltlScript.Text = "<script type=\"text/javascript\">  //<![CDATA[ spellURL = \"" + CMSContext.ResolveDialogUrl("~/CMSFormControls/LiveSelectors/SpellCheck.aspx") + "\"; function SpellCheck() { checkSpelling(spellURL); } var offsetValue = 7; //]]> </script>";
        }
        else
        {
            ltlScript.Text = "<script type=\"text/javascript\">  //<![CDATA[ spellURL = \"" + CMSContext.ResolveDialogUrl("~/CMSModules/Content/CMSDesk/Edit/SpellCheck.aspx") + "\"; function SpellCheck() { checkSpelling(spellURL); } var offsetValue = 7; //]]> </script>";
        }

        // Register scripts
        ScriptHelper.RegisterCompletePageScript(this);
        ScriptHelper.RegisterProgress(this);
        ScriptHelper.RegisterDialogScript(this);

        confirmChanges = SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSConfirmChanges");

        tree = new TreeProvider(CMSContext.CurrentUser);
        formElem.TreeProvider = tree;

        // Current nodeID
        nodeId = QueryHelper.GetInteger("nodeid", 0);

        // ClassID, used when creating new document                
        classId = QueryHelper.GetInteger("classid", 0);

        // TemplateID, used when Use template selection is enabled in actual class
        templateId = QueryHelper.GetInteger("templateid", 0);


        // Analyze the action parameter
        switch (QueryHelper.GetString("action", "").ToLower())
        {
            case "new":
                // Check if document type is allowed under parent node
                if ((nodeId > 0) && (classId > 0))
                {
                    // Get the node                    
                    TreeNode node = tree.SelectSingleNode(nodeId);
                    DataClassInfo dci = DataClassInfoProvider.GetDataClass(classId);
                    if ((node == null) || (dci == null))
                    {
                        throw new Exception("[Content.Edit]: Given node or document class cannot be found!");
                    }

                    // Check allowed document type
                    if (!DataClassInfoProvider.IsChildClassAllowed(ValidationHelper.GetInteger(node.GetValue("NodeClassID"), 0), classId))
                    {
                        ltlScript.Text = ScriptHelper.GetScript("window.parent.parent.location.replace('NotAllowed.aspx?action=child')");
                    }

                    if (!CMSContext.CurrentUser.IsAuthorizedToCreateNewDocument(node, dci.ClassName))
                    {
                        ltlScript.Text = ScriptHelper.GetScript("window.parent.parent.location.replace('NotAllowed.aspx?action=new')");
                    }
                }

                newdocument = true;
                break;

            case "newculture":
                newculture = true;
                break;
        }

        // Get the node
        if (newculture || newdocument)
        {
            node = DocumentHelper.GetDocument(nodeId, TreeProvider.ALL_CULTURES, tree);
            if (newculture)
            {
                DocumentHelper.ClearWorkflowInformation(node);
            }
        }
        else
        {
            node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, tree);
        }

        if (!confirmChanges)
        {
            ltlScript.Text += ScriptHelper.GetScript("confirmChanges = false;");
        }

        // If node found, init the form
        if (node != null)
        {
            // CMSForm initialization
            formElem.NodeId = node.NodeID;
            formElem.CultureCode = CMSContext.PreferredCultureCode;

            // Set the form mode
            if (newdocument)
            {
                ci = DataClassInfoProvider.GetDataClass(classId);
                if (ci == null)
                {
                    throw new Exception("[Content/Edit.aspx]: Class ID '" + classId + "' not found.");
                }

                if (ci.ClassName.ToLower() == "cms.blog")
                {
                    if (!LicenseHelper.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Blogs, VersionActionEnum.Insert))
                    {
                        RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.bloglicenselimits"), ""));
                    }
                }

                if (!LicenseHelper.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Documents, VersionActionEnum.Insert))
                {
                    RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.documentslicenselimits"), ""));
                }

                // Check if need template selection, if so, then redirect to template selection page
                if ((ci.ClassShowTemplateSelection) && (templateId == 0) && (ci.ClassName.ToLower() != "cms.menuitem"))
                {
                    UrlHelper.Redirect("~/CMSModules/Content/CMSDesk/TemplateSelection.aspx" + HttpContext.Current.Request.Url.Query);
                }

                // Set default template ID
                formElem.DefaultPageTemplateID = templateId > 0 ? templateId : ci.ClassDefaultPageTemplateID;

                formElem.FormMode = FormModeEnum.Insert;
                string newClassName = ci.ClassName;
                formElem.FormName = newClassName + ".default";
            }
            else if (newculture)
            {
                if (node.NodeClassName.ToLower() == "cms.blog")
                {
                    if (!LicenseHelper.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Blogs, VersionActionEnum.Insert))
                    {
                        RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.bloglicenselimits"), ""));
                    }
                }

                if (!LicenseHelper.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Documents, VersionActionEnum.Insert))
                {
                    RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.documentslicenselimits"), ""));
                }

                formElem.FormMode = FormModeEnum.InsertNewCultureVersion;
                // Default data document ID
                formElem.CopyDefaultDataFromDocumentId = ValidationHelper.GetInteger(Request.QueryString["sourcedocumentid"], 0);

                ci = DataClassInfoProvider.GetDataClass(node.NodeClassName);
                formElem.FormName = node.NodeClassName + ".default";
            }
            else
            {
                formElem.FormMode = FormModeEnum.Update;
                ci = DataClassInfoProvider.GetDataClass(node.NodeClassName);
            }
            formElem.Visible = true;

            // Display / hide the FCK editor toolbar area
            FormInfo fi = new FormInfo();
            fi.LoadXmlDefinition(ci.ClassFormDefinition);

            if (fi.UsesHtmlArea())
            {
                // Add script to display toolbar
                if (formElem.HtmlAreaToolbarLocation.ToLower() == "out:fcktoolbar")
                {
                    mShowToolbar = true;

                    formElem.HtmlAreaToolbarLocation = "Out:parent(FCKToolbar)";
                }
            }

            ReloadForm();
        }

        ltlScript.Text += ScriptHelper.GetScript(
            "function SaveDocument(nodeId, createAnother) { document.getElementById('hidAnother').value = createAnother; " + (confirmChanges ? "AllowSubmit(); " : "") + ClientScript.GetPostBackEventReference(btnSave, null) + "; } \n" +
            "function Approve(nodeId) { SubmitAction(); " + ClientScript.GetPostBackEventReference(btnApprove, null) + "; } \n" +
            "function Reject(nodeId) { SubmitAction(); " + ClientScript.GetPostBackEventReference(btnReject, null) + "; } \n" +
            "function CheckIn(nodeId) { " + ClientScript.GetPostBackEventReference(btnCheckIn, null) + "; } \n" +
            (confirmChanges ? "var confirmSave='" + ResHelper.GetString("Content.ConfirmSave") + "'; var confirmLeave='" + ResHelper.GetString("Content.ConfirmLeave") + "'; \n" : "")
            );
    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);

        formElem.lblInfo.Text = "";

        if (RequestHelper.IsPostBack())
        {
            ltlScript.Text += ScriptHelper.GetScript("ClearFCKToolbar();");
        }

        if (newdocument && SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".ProductTabEnabled"))
        {
            InitializeProductControls();
        }
        else
        {
            // Hide all product controls
            plcNewProduct.Visible = false;
        }
    }


    private void ReloadForm()
    {
        lblWorkflowInfo.Text = "";

        // Disable the form for root
        if ((node != null) && (node.NodeAliasPath == "/") && (classId == 0))
        {
            formElem.Enabled = false;
        }

        if ((node != null) && !newdocument && !newculture)
        {
            // Check read permissions
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
            }
            // Check modify permissions
            else if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
            {
                formElem.Enabled = false;
                lblWorkflowInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
            }
            else
            {
                // Setup the workflow information
                WorkflowManager wm = new WorkflowManager(tree);
                WorkflowInfo wi = wm.GetNodeWorkflow(node);
                if ((wi != null) && (!newculture))
                {
                    // Get current step info, do not update document
                    WorkflowStepInfo si = wm.GetStepInfo(node, false) ??
                                          wm.GetFirstWorkflowStep(node);

                    bool allowApproval = true;
                    bool canApprove = wm.CanUserApprove(node, CMSContext.CurrentUser);
                    string stepName = si.StepName.ToLower();
                    if (!(canApprove || (stepName == "edit") || (stepName == "published") || (stepName == "archived")))
                    {
                        formElem.Enabled = false;
                    }

                    // Check-in, Check-out
                    if (VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName))
                    {
                        if (node.GetValue("DocumentCheckedOutByUserID") == null)
                        {
                            // If not checked out, add the check-out information
                            if (canApprove || (stepName == "edit") || (stepName == "published") || (stepName == "archived"))
                            {
                                lblWorkflowInfo.Text = ResHelper.GetString("EditContent.DocumentCheckedIn");
                            }
                            formElem.Enabled = newculture;
                        }
                        else
                        {
                            // If checked out by current user, add the check-in button
                            int checkedOutBy = ValidationHelper.GetInteger(node.GetValue("DocumentCheckedOutByUserID"), 0);
                            if (checkedOutBy == CMSContext.CurrentUser.UserID)
                            {
                                // Document is checked out
                                lblWorkflowInfo.Text = ResHelper.GetString("EditContent.DocumentCheckedOut");
                            }
                            else
                            {
                                // Checked out by somebody else
                                lblWorkflowInfo.Text = ResHelper.GetString("EditContent.DocumentCheckedOutByAnother");
                                formElem.Enabled = newculture;
                            }
                            allowApproval = false;
                        }
                    }

                    // Document approval
                    if (allowApproval)
                    {
                        switch (si.StepName.ToLower())
                        {
                            case "edit":
                            case "published":
                            case "archived":
                                break;

                            default:
                                // If the user is authorized to perform the step, display the approve and reject buttons
                                if (!canApprove)
                                {
                                    lblWorkflowInfo.Text += " " + ResHelper.GetString("EditContent.NotAuthorizedToApprove");
                                }
                                break;
                        }
                        lblWorkflowInfo.Text += " " + String.Format(ResHelper.GetString("EditContent.CurrentStepInfo"), si.StepDisplayName);
                    }
                }
            }
        }
        pnlWorkflowInfo.Visible = (lblWorkflowInfo.Text != "");
    }


    /// <summary>
    /// Adds the alert message to the output request window
    /// </summary>
    /// <param name="message">Message to display</param>
    private void AddAlert(string message)
    {
        ltlScript.Text += ScriptHelper.GetAlertScript(message);
    }


    /// <summary>
    /// Adds the script to the output request window
    /// </summary>
    /// <param name="script">Script to add</param>
    public override void AddScript(string script)
    {
        ltlScript.Text += ScriptHelper.GetScript(script);
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        SaveDocument();
    }


    private void SaveDocument()
    {
        if (nodeId > 0)
        {
            // Validate the form first
            if (formElem.BasicForm.ValidateData())
            {
                bool createAnother = newdocument && ValidationHelper.GetBoolean(Request.Params["hidAnother"], false);
                bool autoCheck = false;
                bool useWorkflow = false;
                VersionManager vm = null;
                if (!newdocument && !newculture)
                {
                    autoCheck = !VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName);
                    WorkflowManager workflowMan = new WorkflowManager(tree);

                    // Get the document
                    node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, tree);

                    // Check if the document uses workflow
                    if (workflowMan.GetNodeWorkflow(node) != null)
                    {
                        useWorkflow = true;
                    }
                }

                try
                {
                    // If not using check-in/check-out, check out automatically
                    if (autoCheck & useWorkflow)
                    {
                        if (node != null)
                        {
                            // Check out
                            vm = new VersionManager(tree);
                            vm.CheckOut(node);
                        }
                    }

                    // tree node is returned from CMSForm.Save method
                    if ((node != null) && !newdocument && !newculture)
                    {
                        formElem.Save(node);
                    }
                    else
                    {
                        // Document was not saved yet
                        node = null;

                        // Product should be created -> save document only when product data are valid              
                        if ((chkCreateProduct.Checked) && (ucNewProduct != null))
                        {
                            if (ucNewProduct.ValidateData())
                            {
                                node = formElem.Save();
                            }
                        }
                        // Product should not be created -> save document
                        else
                        {
                            node = formElem.Save();
                        }
                    }

                    if (node != null)
                    {
                        // Create product only if the doc. type can be product
                        if ((newdocument) && (chkCreateProduct.Checked) && (ucNewProduct != null) && (ucNewProduct.ClassObj.ClassIsProduct))
                        {
                            // Create product
                            ucNewProduct.Node = node;

                            IInfoObject skuObj = ucNewProduct.SaveData();
                            if (skuObj != null)
                            {
                                // Asssign new product to the document
                                node.NodeSKUID = skuObj.ObjectID;
                                DocumentHelper.UpdateDocument(node, tree, true);
                            }
                        }

                        // Check in the document
                        if (autoCheck & useWorkflow)
                        {
                            if (vm != null)
                            {
                                vm.CheckIn(node, null, null);
                            }
                            ReloadForm();
                        }

                        int newNodeId = node.NodeID;
                        if (newdocument || newculture)
                        {
                            if (createAnother)
                            {
                                AddScript("     PassiveRefresh(" + node.NodeParentID + ", " + node.NodeParentID + "); CreateAnother();");
                            }
                            else
                            {
                                // Document tree is refreshed and new document is displayed
                                AddScript("     RefreshTree(" + newNodeId + "," + newNodeId + "); SelectNode(" + newNodeId + ");");
                            }
                        }
                        else
                        {
                            AddScript("     PassiveRefresh(" + nodeId + ", " + node.NodeParentID + ");");

                            // Reload the form
                            string oldInfo = formElem.lblInfo.Text;
                            formElem.LoadForm();
                            formElem.lblInfo.Text = oldInfo;

                            formElem.lblInfo.Text += "<br />";
                        }

                        // If not menuitem type, switch to form mode to keep editing the form
                        if (!TreePathUtils.IsMenuItemType(node.NodeClassName))
                        {
                            CMSContext.ViewMode = ViewModeEnum.EditForm;
                        }
                    }
                }
                catch (Exception ex)
                {
                    AddAlert(ResHelper.GetString("ContentRequest.SaveFailed") + " : " + ex.Message);
                }
            }
        }
    }


    protected void btnApprove_Click(object sender, EventArgs e)
    {
        if (node != null)
        {
            // Validate the form first
            if (formElem.BasicForm.ValidateData())
            {
                // Save the changes first
                bool autoCheck = !VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName);
                if (autoCheck)
                {
                    if (SaveChanges)
                    {
                        SaveDocument();
                    }
                    else
                    {
                        // No version exists, create one
                        if (node.DocumentCheckedOutVersionHistoryID == 0)
                        {
                            // Check out the document
                            VersionManager vm = new VersionManager(tree);
                            vm.CheckOut(node);
                            // Check in the document
                            vm.CheckIn(node, null, null);
                        }

                        formElem.BasicForm.LoadControlValues();
                    }
                }

                // Approve the document - Go to next workflow step
                WorkflowManager wm = new WorkflowManager(tree);

                // Get original step
                WorkflowStepInfo originalStep = wm.GetStepInfo(node);

                // Approve the document
                WorkflowStepInfo nextStep = wm.MoveToNextStep(node, "");
                if (nextStep != null)
                {
                    string nextStepName = nextStep.StepName.ToLower();
                    string originalStepName = originalStep.StepName.ToLower();

                    bool published = (nextStepName == "published");
                    if (published)
                    {
                        formElem.LoadForm();
                    }

                    // Send workflow e-mails
                    if (SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSSendWorkflowEmails"))
                    {
                        if (nextStepName == "published")
                        {
                            // Publish e-mails
                            wm.SendWorkflowEmails(node, CMSContext.CurrentUser, originalStep, nextStep, WorkflowActionEnum.Published, "");
                        }
                        else
                        {
                            // Approve e-mails
                            wm.SendWorkflowEmails(node, CMSContext.CurrentUser, originalStep, nextStep, WorkflowActionEnum.Approved, "");
                        }
                    }
                    ltlScript.Text += ScriptHelper.GetScript("PassiveRefresh(" + nodeId + ", " + node.NodeParentID + ");");

                    // Ensure correct message is displayed
                    if (nextStepName == "published")
                    {
                        formElem.lblInfo.Text += " " + ResHelper.GetString("workflowstep.customtopublished");
                    }
                    else if (originalStepName == "edit" && nextStepName != "published")
                    {
                        formElem.lblInfo.Text += " " + ResHelper.GetString("workflowstep.edittocustom");
                    }
                    else if (originalStepName != "edit" && nextStepName != "published" && nextStepName != "archived")
                    {
                        formElem.lblInfo.Text += " " + ResHelper.GetString("workflowstep.customtocustom");
                    }
                    else
                    {
                        formElem.lblInfo.Text += " " + ResHelper.GetString("ContentEdit.WasApproved");
                    }
                }
                else
                {
                    // Workflow has been removed
                    AddScript("     SelectNode(" + nodeId + ");");
                    formElem.lblInfo.Text += " " + ResHelper.GetString("ContentEdit.WasApproved");
                }
            }
        }

        ReloadForm();
    }


    protected void btnReject_Click(object sender, EventArgs e)
    {
        // Reject the document - Go to previous workflow step
        if (node != null)
        {
            // Validate the form first
            if (formElem.BasicForm.ValidateData())
            {
                // Save the document first
                bool autoCheck = !VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName);
                if (autoCheck)
                {
                    if (SaveChanges)
                    {
                        SaveDocument();
                    }
                    else
                    {
                        formElem.BasicForm.LoadControlValues();
                        ltlScript.Text += ScriptHelper.GetScript("PassiveRefresh(" + nodeId + ", " + node.NodeParentID + ");");
                    }
                }

                WorkflowManager wm = new WorkflowManager(tree);

                // Get original step
                WorkflowStepInfo originalStep = wm.GetStepInfo(node);

                // Reject the document
                WorkflowStepInfo previousStep = wm.MoveToPreviousStep(node, "");

                if (previousStep != null)
                {
                    // Send workflow e-mails
                    if (SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSSendWorkflowEmails"))
                    {
                        wm.SendWorkflowEmails(node, CMSContext.CurrentUser, originalStep, previousStep, WorkflowActionEnum.Rejected, "");
                    }

                    ltlScript.Text += ScriptHelper.GetScript("PassiveRefresh(" + nodeId + ", " + node.NodeParentID + ");");
                }
                else
                {
                    // Workflow has been removed
                    AddScript("     SelectNode(" + nodeId + ");");
                }

                formElem.lblInfo.Text += " " + ResHelper.GetString("ContentEdit.WasRejected");
            }
        }
        ReloadForm();
    }


    protected void btnCheckIn_Click(object sender, EventArgs e)
    {
        // Check in the document
        if (node != null)
        {
            // Validate the form first
            if (formElem.BasicForm.ValidateData())
            {
                // Save the document first
                if (SaveChanges || !confirmChanges)
                {
                    SaveDocument();
                }
                else
                {
                    formElem.BasicForm.LoadControlValues();
                    ltlScript.Text += ScriptHelper.GetScript("PassiveRefresh(" + nodeId + ", " + node.NodeParentID + ");");
                }

                VersionManager verMan = new VersionManager(tree);

                // Check in the document        
                verMan.CheckIn(node, null, null);

                formElem.lblInfo.Text += " " + ResHelper.GetString("ContentEdit.WasCheckedIn");
            }
        }

        ReloadForm();
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (formElem.Enabled)
        {
            // Add the toolbar script
            if (mShowToolbar)
            {
                ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.TOOLBAR_SCRIPT_KEY, ScriptHelper.ToolbarScript);
            }

            // Add the shortcuts script
            ScriptHelper.RegisterShortcuts(this);
        }

        // Register the scripts
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "SpellCheck_" + ClientID, ltlScript.Text);
    }


    private void InitializeProductControls()
    {

        // Load NewProduct control
        if (File.Exists(Server.MapPath("~/CMSModules/Ecommerce/Content/Product/NewProduct.ascx")))
        {
            try
            {
                // Try to load product control
                Control ctrl = Page.LoadControl("~/CMSModules/Ecommerce/Content/Product/NewProduct.ascx");
                ctrl.ID = "ucNewProduct";
                pnlNewProduct.Controls.Add(ctrl);

                // Initialize product control
                ucNewProduct = (INewProductControl)ctrl;
            }
            catch { }
        }

        if (ucNewProduct == null)
        {
            plcNewProduct.Visible = false;
            return;
        }

        // Initialize product controls
        chkCreateProduct.Text = ResHelper.GetString("NewDocument.CreateProduct");
        chkCreateProduct.Attributes["onclick"] = "ShowHideSKUControls()";
        ucNewProduct.ClassID = classId;

        // Register script to show / hide SKU controls
        string script =
            "function ShowHideSKUControls() { \n" +
            "   var checkbox = document.getElementById('" + chkCreateProduct.ClientID + "'); \n" +
            "   var panel = document.getElementById('" + pnlNewProduct.ClientID + "'); \n" +
            "   if (panel != null) { if ((checkbox != null) && (checkbox.checked)) { panel.style.display = 'block'; } else { panel.style.display = 'none'; }} \n" +
            "} \n";

        AddScript(script);

        if (!RequestHelper.IsPostBack())
        {
            if ((ucNewProduct.ClassObj == null) || ucNewProduct.ClassObj.ClassCreateSKU || !ucNewProduct.ClassObj.ClassIsProduct)
            {
                // Hide checkbox when SKU should be created automatically or the doc. type is not product
                chkCreateProduct.Visible = false;
            }
            else
            {
                // Show checkbox to enable to enter SKU data
                chkCreateProduct.Checked = false;
            }
        }

        AddScript("ShowHideSKUControls();");
    }
}

