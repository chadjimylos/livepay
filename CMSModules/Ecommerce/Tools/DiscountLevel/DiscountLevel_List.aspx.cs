using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_DiscountLevel_DiscountLevel_List : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Page title
        this.CurrentMaster.HeaderActions.HelpTopicName = "discount_level_list";
        this.CurrentMaster.HeaderActions.HelpName = "helpTopic";

        // New item link
        string[,] actions = new string[1, 7];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("DiscountLevel_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("DiscountLevel_New.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_DiscountLevel/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        // Unigrid
        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "discountvalue":
                DataRowView row = (DataRowView)parameter;
                double value = ValidationHelper.GetDouble(row["DiscountLevelValue"], 0);
                return value.ToString() + "%";
            case "disclevelenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }
        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("DiscountLevel_Edit_Frameset.aspx?discountLevelId=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // Check 'EcommerceModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
            }

            int id = ValidationHelper.GetInteger(actionArgument, 0);

            // Check dependencies
            if (DiscountLevelInfoProvider.CheckDependencies(id))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                return;
            }

            // Delete DiscountlLevelInfo object from database
            DiscountLevelInfoProvider.DeleteDiscountLevelInfo(id);
        }
    }
}
