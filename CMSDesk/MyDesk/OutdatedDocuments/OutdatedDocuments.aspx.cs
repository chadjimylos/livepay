using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.SiteProvider;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSDesk_MyDesk_OutdatedDocuments_OutdatedDocuments : CMSMyDeskPage
{
    #region "Private variables"

    private UserInfo currentUserInfo = null;
    private SiteInfo currentSiteInfo = null;
    private TimeZoneInfo usedTimeZone = null;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check UIProfile
        if ((CMSContext.CurrentUser == null) || (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.MyDesk", "OutdatedDocs")))
        {
            RedirectToUINotAvailable();
        }

        string strDays = ResHelper.GetString("MyDesk.OutdatedDocuments.Days");
        string strWeeks = ResHelper.GetString("MyDesk.OutdatedDocuments.Weeks");
        string strMonths = ResHelper.GetString("MyDesk.OutdatedDocuments.Months");
        string strYears = ResHelper.GetString("MyDesk.OutdatedDocuments.Years");

        // Setup page title text and image
        CurrentMaster.Title.TitleText = ResHelper.GetString("MyDesk.OutdatedDocumentsTitle");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_MyDesk/OutdatedDocuments/module.png");

        CurrentMaster.Title.HelpTopicName = "outdated_documents";
        CurrentMaster.Title.HelpName = "helpTopic";

        // Initialize controls
        lblFilter.Text = ResHelper.GetString("MyDesk.OutdatedDocuments.Filter");

        if (!RequestHelper.IsPostBack())
        {
            // Fill the dropdown list
            drpFilter.Items.Add(strDays);
            drpFilter.Items.Add(strWeeks);
            drpFilter.Items.Add(strMonths);
            drpFilter.Items.Add(strYears);

            // Load default value
            txtFilter.Text = "1";
            drpFilter.SelectedValue = strYears;

            // Bind dropdown lists
            BindDropDowns();
        }

        object[,] parameters = new object[3, 3];
        parameters[0, 0] = "@UserID";
        parameters[0, 1] = CMSContext.CurrentUser.UserID;
        parameters[1, 0] = "@SiteID";
        parameters[1, 1] = CMSContext.CurrentSite.SiteID;

        DateTime olderThan = DateTime.Now;
        int dateTimeValue = ValidationHelper.GetInteger(txtFilter.Text, 0);
        if (drpFilter.SelectedValue == strDays)
        {
            olderThan = olderThan.AddDays(-dateTimeValue);
        }
        else if (drpFilter.SelectedValue == strWeeks)
        {
            olderThan = olderThan.AddDays(-dateTimeValue * 7);
        }
        else if (drpFilter.SelectedValue == strMonths)
        {
            olderThan = olderThan.AddMonths(-dateTimeValue);
        }
        else if (drpFilter.SelectedValue == strYears)
        {
            olderThan = olderThan.AddYears(-dateTimeValue);
        }

        parameters[2, 0] = "@OlderThan";
        parameters[2, 1] = olderThan;

        // Add where condition
        if (!string.IsNullOrEmpty(txtDocumentName.Text))
        {
            ugOutdatedDocs.WhereCondition = SqlHelperClass.AddWhereCondition(ugOutdatedDocs.WhereCondition, GetWhereCondition("DocumentName", drpDocumentName, txtDocumentName));
        }
        if (!string.IsNullOrEmpty(txtDocumentType.Text))
        {
            ugOutdatedDocs.WhereCondition = SqlHelperClass.AddWhereCondition(ugOutdatedDocs.WhereCondition, GetWhereCondition("ClassDisplayName", drpDocumentType, txtDocumentType));
        }
        ugOutdatedDocs.QueryParameters = parameters;
        ugOutdatedDocs.HideControlForZeroRows = true;
        ugOutdatedDocs.OnExternalDataBound += ugOutdatedDocs_OnExternalDataBound;
    }


    /// <summary>
    /// Binds filter dropdown lists with conditions
    /// </summary>
    private void BindDropDowns()
    {
        string[] conditions = { "LIKE", "NOT LIKE", "=", "<>" };
        drpDocumentName.DataSource = conditions;
        drpDocumentType.DataSource = conditions;
        drpDocumentName.DataBind();
        drpDocumentType.DataBind();
    }


    private static string GetWhereCondition(string column, ListControl drpCondition, ITextControl valueBox)
    {
        string condition = drpCondition.SelectedValue.Replace("'", "''");
        string value = valueBox.Text.Replace("'", "''");
        value = TextHelper.LimitLength(value, 100);

        string where = column + " " + condition + " ";
        if (string.IsNullOrEmpty(value))
        {
            where = string.Empty;
        }
        else
        {
            switch (condition)
            {
                case "LIKE":
                case "NOT LIKE":
                    where += "N'%" + value + "%'";
                    break;

                case "=":
                case "<>":
                    where += "N'" + value + "'";
                    break;

                default:
                    where = string.Empty;
                    break;
            }
        }
        return where;
    }

    /// <summary>
    /// External data binding handler
    /// </summary>
    protected object ugOutdatedDocs_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        lblInfo.Visible = false;
        int nodeId = 0;
        string culture = string.Empty;
        DataRowView data = null;
        sourceName = sourceName.ToLower();
        switch (sourceName)
        {
            case "classname":
                data = (DataRowView)parameter;
                nodeId = ValidationHelper.GetInteger(data["NodeID"], 0);
                culture = ValidationHelper.GetString(data["DocumentCulture"], string.Empty);
                string className = ValidationHelper.GetString(data["ClassName"], string.Empty);
                string imageUrl = GetDocumentTypeIconUrl(className);
                string toReturn = "<img src=\"" + imageUrl + "\" border=\"0\" />";
                // Check permissions
                if (IsUserAuthorizedPerContent())
                {
                    toReturn = "<a href=\"javascript: SelectItem(" + nodeId + ", '" + culture + "');\">" + toReturn + "</a>";
                }

                return toReturn;

            case "documentname":
                data = (DataRowView)parameter;
                string name = ValidationHelper.GetString(data["DocumentName"], string.Empty);
                nodeId = ValidationHelper.GetInteger(data["NodeID"], 0);
                culture = ValidationHelper.GetString(data["DocumentCulture"], string.Empty);

                if (name == string.Empty)
                {
                    name = "-";
                }
                string result = string.Empty;
                if (IsUserAuthorizedPerContent())
                {
                    result = "<a href=\"javascript: SelectItem(" + nodeId + ", '" + culture + "');\">" + HTMLHelper.HTMLEncode(TextHelper.LimitLength(name, 50)) + "</a>";
                    bool isLink = (data["NodeLinkedNodeID"] != DBNull.Value);
                    if (isLink)
                    {
                        result += UIHelper.GetDocumentMarkImage(this, DocumentMarkEnum.Link);
                    }
                }
                else
                {
                    result = "<span>" + HTMLHelper.HTMLEncode(TextHelper.LimitLength(name, 50)) + "</span>";
                }
                return result;

            case "documentnametooltip":
                data = (DataRowView)parameter;
                return UniGridFunctions.DocumentNameTooltip(data);

            case "stepdisplayname":
                string stepName = ValidationHelper.GetString(parameter, string.Empty);
                if (stepName == string.Empty)
                {
                    stepName = "-";
                }
                return stepName;

            case "culture":
                DataRowView drv = (DataRowView)parameter;

                // Add icon
                return UniGridFunctions.DocumentCultureFlag(drv, this.Page);

            case "modifiedwhen":
            case "modifiedwhentooltip":
                if (string.IsNullOrEmpty(parameter.ToString()))
                {
                    return "";
                }
                else
                {
                    DateTime modifiedWhen = ValidationHelper.GetDateTime(parameter, DateTimeHelper.ZERO_TIME);
                    if (currentUserInfo == null)
                    {
                        currentUserInfo = CMSContext.CurrentUser;
                    }
                    if (currentSiteInfo == null)
                    {
                        currentSiteInfo = CMSContext.CurrentSite;
                    }

                    if (sourceName == "modifiedwhen")
                    {
                        return TimeZoneHelper.GetCurrentTimeZoneDateTimeString(modifiedWhen,
                            currentUserInfo, currentSiteInfo, out usedTimeZone);
                    }
                    else
                    {
                        if (TimeZoneHelper.TimeZonesEnabled() && (usedTimeZone == null))
                        {
                            TimeZoneHelper.GetCurrentTimeZoneDateTimeString(modifiedWhen,
                                                        currentUserInfo, currentSiteInfo, out usedTimeZone);
                        }
                        return TimeZoneHelper.GetGMTLongStringOffset(usedTimeZone);
                    }
                }
        }

        return parameter;
    }
}
