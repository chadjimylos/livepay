-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_CMS_Resource_RemoveDependences]
	@ResourceID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;    
    DELETE FROM [CMS_ResourceSite] WHERE ResourceID = @ResourceID;
	
	-- Permissions
    DELETE FROM [Forums_ForumRoles]				WHERE PermissionID IN (SELECT PermissionID FROM [CMS_Permission] WHERE ResourceID = @ResourceID);
    DELETE FROM [CMS_RolePermission]			WHERE PermissionID IN (SELECT PermissionID FROM [CMS_Permission] WHERE ResourceID = @ResourceID);
    DELETE FROM [Community_GroupRolePermission]	WHERE PermissionID IN (SELECT PermissionID FROM [CMS_Permission] WHERE ResourceID = @ResourceID);
    DELETE FROM [Media_LibraryRolePermission]	WHERE PermissionID IN (SELECT PermissionID FROM [CMS_Permission] WHERE ResourceID = @ResourceID);
    DELETE FROM [CMS_WidgetRole]				WHERE PermissionID IN (SELECT PermissionID FROM [CMS_Permission] WHERE ResourceID = @ResourceID);
	DELETE FROM [CMS_Permission] WHERE ResourceID = @ResourceID;
    -- UI elements
    DELETE FROM [CMS_RoleUIElement] WHERE ElementID IN (SELECT ElementID FROM CMS_UIElement WHERE ElementResourceID = @ResourceID);
    DELETE FROM [CMS_UIElement] WHERE ElementResourceID = @ResourceID;
	COMMIT TRANSACTION;
END
