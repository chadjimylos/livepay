CREATE TABLE [CMS_Layout] (
		[LayoutID]                        int NOT NULL IDENTITY(1, 1),
		[LayoutCodeName]                  nvarchar(100) NOT NULL,
		[LayoutDisplayName]               nvarchar(200) NOT NULL,
		[LayoutDescription]               nvarchar(max) NOT NULL,
		[LayoutCode]                      nvarchar(max) NOT NULL,
		[LayoutCheckedOutFilename]        nvarchar(450) NULL,
		[LayoutCheckedOutByUserID]        int NULL,
		[LayoutCheckedOutMachineName]     nvarchar(50) NULL,
		[LayoutVersionGUID]               nvarchar(50) NULL,
		[LayoutGUID]                      uniqueidentifier NOT NULL,
		[LayoutLastModified]              datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Layout]
	ADD
	CONSTRAINT [PK_CMS_Layout]
	PRIMARY KEY
	([LayoutID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_Layout]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Layout_LayoutCheckedOutByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_Layout]
			ADD CONSTRAINT [FK_CMS_Layout_LayoutCheckedOutByUserID_CMS_User]
			FOREIGN KEY ([LayoutCheckedOutByUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_CMS_Layout_LayoutDisplayName]
	ON [CMS_Layout] ([LayoutDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Layout_LayoutCheckedOutByUserID]
	ON [CMS_Layout] ([LayoutCheckedOutByUserID])
	ON [PRIMARY]
