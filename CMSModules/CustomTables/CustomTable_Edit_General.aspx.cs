using System;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_CustomTables_CustomTable_Edit_General : CMSCustomTablesPage
{
    #region "Private fields"

    private int mClassId = 0;
    private DataClassInfo mCurrentClass = null;

    #endregion


    #region "Private properties"

    /// <summary>
    /// ID of the current class
    /// </summary>
    private int ClassID
    {
        get
        {
            if (mClassId == 0)
            {
                mClassId = QueryHelper.GetInteger("customtableid", 0);
            }

            return mClassId;
        }
    }


    /// <summary>
    /// Indicates whether the changes were saved
    /// </summary>
    private bool WasSaved
    {
        get
        {
            return !String.IsNullOrEmpty(QueryHelper.GetString("saved", String.Empty));
        }
    }


    /// <summary>
    /// Get the info on current class
    /// </summary>
    private DataClassInfo CurrentClass
    {
        get
        {
            if (mCurrentClass == null)
            {
                if (ClassID > 0)
                {
                    mCurrentClass = DataClassInfoProvider.GetDataClass(ClassID);
                }
            }

            return mCurrentClass;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize controls
        SetupControl();

        if (!RequestHelper.IsPostBack())
        {
            // Fills the existing class data
            LoadData();


            if (WasSaved)
            {
                // Display information on success to the user                                
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("general.changessaved");
            }
        }
    }

    #endregion


    #region "Event handling"

    protected void btnOk_Click(object sender, EventArgs e)
    {
        if (CurrentClass != null)
        {
            // Validate the form entries
            string errMsg = ValidateForm();
            if (errMsg == String.Empty)
            {
                CurrentClass.ClassDisplayName = txtDisplayName.Text;
                CurrentClass.ClassName = txtCodeNameNamespace.Text + "." + txtCodeNameName.Text;

                mCurrentClass.ClassNewPageURL = txtNewPage.Text;
                mCurrentClass.ClassViewPageUrl = txtViewPage.Text;
                mCurrentClass.ClassEditingPageURL = txtEditingPage.Text;
                mCurrentClass.ClassListPageURL = txtListPage.Text;

                DataClassInfoProvider.SetDataClass(CurrentClass);

                string editUrl = "~/CMSModules/CustomTables/CustomTable_Edit_General.aspx?customtableid=" + ClassID + "&saved=1";

                UrlHelper.Redirect(editUrl);
            }
            else
            {
                // Display error message to the user
                lblError.Visible = true;
                lblError.Text = errMsg;
            }
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes the controls on the page
    /// </summary>
    private void SetupControl()
    {
        // Set the validators' error messages
        rfvDisplayName.ErrorMessage = ResHelper.GetString("sysdev.class_edit_gen.displayname");
        rfvCodeNameNamespace.ErrorMessage = ResHelper.GetString("sysdev.class_edit_gen.namespace");
        rfvCodeNameName.ErrorMessage = ResHelper.GetString("sysdev.class_edit_gen.name");
    }


    /// <summary>
    /// Obtain the class data and fill the appropriate fields
    /// </summary>
    private void LoadData()
    {
        if (CurrentClass != null)
        {
            txtDisplayName.Text = CurrentClass.ClassDisplayName;

            // Fill class name info
            int classNameIndex = CurrentClass.ClassName.IndexOf('.');
            txtCodeNameNamespace.Text = CurrentClass.ClassName.Substring(0, classNameIndex);
            txtCodeNameName.Text = CurrentClass.ClassName.Substring(classNameIndex + 1);
            lblTableNameValue.Text = CurrentClass.ClassTableName;

            txtNewPage.Text = mCurrentClass.ClassNewPageURL;
            txtViewPage.Text = mCurrentClass.ClassViewPageUrl;
            txtEditingPage.Text = mCurrentClass.ClassEditingPageURL;
            txtListPage.Text = mCurrentClass.ClassListPageURL;
        }
    }


    /// <summary>
    /// Validates entries
    /// </summary>
    /// <returns>Returns empty string on success and error message otherwise</returns>
    private string ValidateForm()
    {
        string errMsg = String.Empty;

        if (CurrentClass != null)
        {
            // Validate using validators
            errMsg = new Validator().NotEmpty(txtCodeNameName.Text, rfvCodeNameName.ErrorMessage).NotEmpty(txtCodeNameNamespace.Text, rfvCodeNameNamespace.ErrorMessage).
                NotEmpty(txtDisplayName.Text, rfvDisplayName.ErrorMessage).IsCodeName(txtCodeNameNamespace.Text, ResHelper.GetString("general.invalidcodename")).
                IsCodeName(txtCodeNameName.Text, ResHelper.GetString("general.invalidcodename")).Result;

            string classFullName = txtCodeNameNamespace.Text + "." + txtCodeNameName.Text;

            // Check if class with specified code name already exist
            if (DataClassInfoProvider.GetDataClass(classFullName) != null)
            {
                if (CurrentClass.ClassName != classFullName)
                {
                    errMsg = ResHelper.GetString("sysdev.class_edit_gen.codenameunique");
                }
            }
        }

        return errMsg;
    }

    #endregion
}
