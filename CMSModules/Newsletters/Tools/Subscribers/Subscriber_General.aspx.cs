using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.FormControls;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Subscribers_Subscriber_General : CMSNewsletterPage
{
    protected int subscriberId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize dataform
        formCustomFields.OnAfterValidate += new DataForm.OnAfterValidateEventHandler(formCustomFields_OnAfterValidate);
        formCustomFields.OnAfterSave += new DataForm.OnAfterSaveEventHandler(formCustomFields_OnAfterSave);
        formCustomFields.OnBeforeValidate += new DataForm.OnBeforeValidateEventHandler(formCustomFields_OnBeforeValidate);

        // Get subscriber ID from querystring
        subscriberId = QueryHelper.GetInteger("subscriberid", 0);
        if (subscriberId > 0)
        {
            if (!RequestHelper.IsPostBack())
            {
                // Get subscriber info
                Subscriber subscriberObj = SubscriberProvider.GetSubscriber(subscriberId);
                if (subscriberObj != null)
                {
                    // Fill editing form only if subscriber is of standard type
                    if (DataHelper.IsEmpty(subscriberObj.SubscriberType) && subscriberObj.SubscriberRelatedID == 0)
                    {
                        formCustomFields.ItemID = subscriberObj.SubscriberID;

                        // Show that the subscriber was created or updated successfully
                        if (Request.QueryString["saved"] == "1")
                        {
                            lblInfo.Visible = true;
                            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                        }
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = String.Format(ResHelper.GetString("Subscriber_Edit.NotStandardSubscriber"), subscriberObj.SubscriberType);
                        formCustomFields.Visible = false;
                    }
                }
                else
                {
                    formCustomFields.Visible = false;
                }
            }
        }
        else
        {
            formCustomFields.Visible = false;
        }
    }


    /// <summary>
    /// DataForm's OnBeforeValidate event handler.
    /// </summary>
    protected void formCustomFields_OnBeforeValidate()
    {
        // Check permissions
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "managesubscribers"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "managesubscribers");
        }
    }


    /// <summary>
    /// DataForm's OnAfterValidate event handler.
    /// </summary>
    protected void formCustomFields_OnAfterValidate()
    {
        // Get subscriber's email
        Control ctrl = formCustomFields.BasicForm.FindControl("SubscriberEmail");
        if ((ctrl != null) && (ctrl is TextBox))
        {
            string mail = ((TextBox)ctrl).Text;
            Subscriber subscriberObj = SubscriberProvider.GetSubscriber(subscriberId);

            if (subscriberObj != null)
            {
                // Check the email changes
                if ((subscriberObj.SubscriberEmail.ToLower() != mail.ToLower().Trim()) && (SubscriberProvider.EmailExists(mail.Trim())))
                {
                    formCustomFields.BasicForm.StopProcessing = true;
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Subscriber_Edit.EmailAlreadyExists");
                }
            }
            else
            {
                formCustomFields.BasicForm.StopProcessing = true;
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Subscriber_Edit.SubscriberDoesNotExists");
            }
        }
    }


    /// <summary>
    /// DataForm's OnAfterSave event handler.
    /// </summary>
    protected void formCustomFields_OnAfterSave()
    {
        if (!formCustomFields.BasicForm.StopProcessing)
        {
            // Save with provider to log synchronization
            Subscriber subscriberObj = SubscriberProvider.GetSubscriber(subscriberId);
            subscriberObj.SubscriberFullName = subscriberObj.SubscriberFirstName + " " + subscriberObj.SubscriberLastName;
            SubscriberProvider.SetSubscriber(subscriberObj);
            
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (formCustomFields.BasicForm != null)
        {
            formCustomFields.BasicForm.SubmitButton.CssClass = "SubmitButton";
        }
    }
}
