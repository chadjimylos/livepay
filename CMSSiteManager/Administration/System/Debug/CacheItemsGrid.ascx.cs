﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using CMS.UIControls;
using CMS.Controls;
using CMS.GlobalHelper;

public partial class CMSSiteManager_Administration_System_Debug_CacheItemsGrid : CMSUserControl, IUniPageable
{
    #region "Variables"

    protected int mTotalItems = 0;

    protected bool mShowDummyItems = false;

    protected List<string> mAllItems = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// All cache items array
    /// </summary>
    public List<string> AllItems
    {
        get
        {
            return mAllItems;
        }
        set
        {
            mAllItems = value;
        }
    }


    /// <summary>
    /// If true, grid shows the dummy items
    /// </summary>
    public bool ShowDummyItems
    {
        get
        {
            return mShowDummyItems;
        }
        set
        {
            mShowDummyItems = value;
        }
    }

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.pagerItems.PagedControl = this;
        this.pagerItems.UniPager.PageControl = "cacheItemsGrid";
    }


    public void ReloadData()
    {
        if (this.ShowDummyItems)
        {
            this.lblKey.Text = ResHelper.GetString("Administration-System.CacheInfoDummyKey");
            this.plcData.Visible = false;
        }
        else
        {
            this.lblKey.Text = ResHelper.GetString("Administration-System.CacheInfoKey");
            this.lblData.Text = ResHelper.GetString("Administration-System.CacheInfoData");
            this.plcData.Visible = true;
        }

        // Build the table
        StringBuilder sb = new StringBuilder();

        // Prepare the indexes for paging
        int pageSize = this.pagerItems.CurrentPageSize;
        
        int startIndex = (this.pagerItems.CurrentPage - 1) * pageSize + 1;
        int endIndex = startIndex + pageSize;

        // Process all items
        int i = 0;
        bool all = (endIndex <= startIndex);
        
        if (ShowDummyItems)
        {
            // Process dummy keys
            foreach (string key in mAllItems)
            {
                object value = HttpRuntime.Cache[key];
                if (value == CacheHelper.DUMMY_KEY)
                {
                    i++;
                    if (all || (i >= startIndex) && (i < endIndex))
                    {
                        // Add the key row
                        string cssClass = ((i % 2) == 0) ? "OddRow" : "EvenRow";

                        sb.Append("<tr class=\"");
                        sb.Append(cssClass);
                        sb.Append("\"><td style=\"white-space: nowrap;\">");
                        sb.Append(key.Replace("&", "&amp;"));
                        sb.Append("</td></tr>");
                    }
                }
            }

        }
        else
        {
            // Process only normal keys
            foreach (string key in mAllItems)
            {
                object value = HttpRuntime.Cache[key];
                if (value != CacheHelper.DUMMY_KEY)
                {
                    // Normal keys (cached data)
                    i++;
                    if (all || (i >= startIndex) && (i < endIndex))
                    {
                        // Add the key row
                        string cssClass = ((i % 2) == 0) ? "OddRow" : "EvenRow";

                        sb.Append("<tr class=\"");
                        sb.Append(cssClass);
                        sb.Append("\"><td style=\"white-space: nowrap;\"><span title=\"" + key.Replace("&", "&amp;") + "\">");
                        string keyTag = TextHelper.LimitLength(key, 100);
                        sb.Append(keyTag.Replace("&", "&amp;"));
                        sb.Append("</span></td><td style=\"white-space: nowrap;\">");
                        if (value != null)
                        {
                            sb.Append("<a href=\"System_ViewObject.aspx?source=cache&amp;key=");
                            sb.Append(Server.UrlEncode(key));
                            sb.Append("\" target=\"_blank\"><img src=\"");
                            sb.Append(ResolveUrl(GetImageUrl("Design/Controls/UniGrid/Actions/View.png")));
                            sb.Append("\" style=\"border: none;\" alt=\"");
                            sb.Append(ResHelper.GetString("General.View"));
                            sb.Append("\" />");
                            sb.Append("</a> ");
                            if ((value == null) || (value == DBNull.Value))
                            {
                                sb.Append("null");
                            }
                            else
                            {
                                sb.Append(HttpUtility.HtmlEncode(DataHelper.GetObjectString(value, 100)));
                            }
                        }
                        else
                        {
                            sb.Append("null");
                        }
                        sb.Append("</td></tr>");
                    }
                }
            }
        }

        mTotalItems = i;
        this.plcItems.Visible = (i > 0);

        ltlCacheInfo.Text = sb.ToString();

        // Call page binding event
        if (OnPageBinding != null)
        {
            OnPageBinding(this, null);
        }
    }


    #region "IUniPageable Members"

    /// <summary>
    /// Pager data item object
    /// </summary>
    public object PagerDataItem
    {
        get
        {
            return null;
        }
        set
        {
        }
    }


    /// <summary>
    /// Occurs when the control bind data
    /// </summary>
    public event EventHandler<EventArgs> OnPageBinding;


    /// <summary>
    /// Occurs when the pager change the page and current mode is postback => reload data
    /// </summary>
    public event EventHandler<EventArgs> OnPageChanged;


    /// <summary>
    /// Evokes control databind
    /// </summary>
    public void ReBind()
    {
        if (OnPageChanged != null)
        {
            OnPageChanged(this, null);
        }

        this.ReloadData();
    }


    /// <summary>
    /// Gets or sets the number of result. Enables proceed "fake" datasets, where number 
    /// of results in the dataset is not correspondent to the real number of results
    /// This property must be equal -1 if should be disabled
    /// </summary>
    public int PagerForceNumberOfResults
    {
        get
        {
            return mTotalItems;
        }
        set
        {
        }
    }

    #endregion
}
