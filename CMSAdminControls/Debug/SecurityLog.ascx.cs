using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSAdminControls_Debug_SecurityLog : SecurityLog
{
    #region "Variables"

    protected string cmsVersion = null;
    protected int index = 0;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        this.EnableViewState = false;
        this.Visible = true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.Visible = false;

        if (this.Log != null)
        {
            // Get the log table
            DataTable dt = this.Log.LogTable;
            if (!DataHelper.DataSourceIsEmpty(dt))
            {
                this.Visible = true;

                cmsVersion = ResHelper.GetString("Footer.Version") + "&nbsp;" + CMSContext.SYSTEM_VERSION + "&nbsp;" + ResHelper.GetString("Footer.Build") + "&nbsp;" + CMSContext.FullSystemVersion;

                int index = 1;
                gridSec.Columns[index++].HeaderText = ResHelper.GetString("SecurityLog.UserName");
                gridSec.Columns[index++].HeaderText = ResHelper.GetString("SecurityLog.Operation");
                gridSec.Columns[index++].HeaderText = ResHelper.GetString("SecurityLog.Result");
                gridSec.Columns[index++].HeaderText = ResHelper.GetString("SecurityLog.Resource");
                gridSec.Columns[index++].HeaderText = ResHelper.GetString("SecurityLog.Name");
                gridSec.Columns[index++].HeaderText = ResHelper.GetString("SecurityLog.Site");
                gridSec.Columns[index++].HeaderText = ResHelper.GetString("SecurityLog.Context");

                if (LogStyle != "")
                {
                    this.ltlInfo.Text = "<div style=\"padding: 2px; font-weight: bold; background-color: #eecccc; border-bottom: solid 1px #ff0000;\">" + ResHelper.GetString("SecurityLog.Info") + "</div>";
                }

                gridSec.DataSource = dt;
                gridSec.DataBind();
            }
        }
    }


    protected string GetIndex(object ind)
    {
        int indent = ValidationHelper.GetInteger(ind, 0);
        if (indent == 0)
        {
            return (++index).ToString();
        }
        else
        {
            return null;
        }
    }


    protected string GetBeginIndent(object ind)
    {
        int indent = ValidationHelper.GetInteger(ind, 0);
        string result = "";
        for (int i = 0; i < indent; i++)
        {
            result += "&gt;"; //"<div style=\"padding-left: 10px;\">";
        }

        return result;
    }


    protected string GetEndIndent(object ind)
    {
        int indent = ValidationHelper.GetInteger(ind, 0);
        string result = "";
        /*
        for (int i = 0; i < indent; i++)
        {
            result += "</div>";
        }*/

        return result;
    }


    protected string GetResult(object result, object ind)
    {
        if ((result == null) || (result == DBNull.Value))
        {
            return null;
        }

        string stringResult = "";
        int indent = ValidationHelper.GetInteger(ind, 0);
        if (indent == 0)
        {
            stringResult = "<strong>";

            switch (result.ToString().ToLower())
            {
                case "true":
                case "allowed":
                    stringResult += "<span style=\"color: green;\">" + result + "</span>";
                    break;

                case "false":
                case "denied":
                    stringResult += "<span style=\"color: red;\">" + result + "</span>";
                    break;

                default:
                    stringResult += result.ToString();
                    break;
            }
        }
        else
        {
            stringResult += result.ToString();
        }

        if (indent == 0)
        {
            stringResult += "</strong>";
        }

        return stringResult;
    }
}
