﻿<%@ Page Title="" Language="C#" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master"
    AutoEventWireup="true" CodeFile="Default.aspx.cs" EnableEventValidation="false"
    Inherits="CMSFormControls_LiveSelectors_QuicklyInsertImage_Default" %>

<%@ Register Src="~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploaderControl.ascx"
    TagName="DirectFileUploader" TagPrefix="cms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">

    <script type="text/javascript" language="javascript">
        //<![CDATA[
        window.uploaderFocused = false;
        //]]>
    </script>

    <cms:DirectFileUploader ID="fileUploaderElem" runat="server" />
</asp:Content>
