using System;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSModules_ImportExport_SiteManager_NewSite_DefineSiteStructure_menu : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the dialog script
        ScriptHelper.RegisterDialogScript(this);

        // Initialize content management menu
        menuLeft.Groups = new string[,] { { ResHelper.GetString("ContentMenu.ContentManagement"), "~/CMSAdminControls/UI/UniMenu/NewSite/NewSiteMenu.ascx", null } };
    }
}
