<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DiscountLevelSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_FormControls_DiscountLevelSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%DiscountLevelDisplayName%}"
            ReturnColumnName="DiscountLevelID" ObjectType="ecommerce.discountlevel" ResourcePrefix="discountlevelselector"
            SelectionMode="SingleDropDownList" AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
