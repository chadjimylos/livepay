<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MyProfile_Edit.aspx.cs" Inherits="CMSDesk_MyDesk_MyProfile_MyProfile_Edit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Untitled Page</title>
    <script type="text/javascript">
        var IsCMSDesk = true;
    </script>
</head>
<frameset border="0" rows="72, *" id="rowsFrameset">
    <frame name="myProfileMenu" src="MyProfile_Header.aspx" frameborder="0" scrolling="no" noresize="auto" />
    <frame name="myProfileContent" src="MyProfile_MyDetails.aspx" frameborder="0" />
    <noframes>
        <body>
            <p id="p1">
                This HTML frameset displays multiple Web pages. To view this frameset, use a Web
                browser that supports HTML 4.0 and later.
            </p>
        </body>
    </noframes>
</frameset>
</html>
