using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Cultures_Culture_New : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        rfvCultureName.ErrorMessage = ResHelper.GetString("Culture_New.ErrorEmptyCultureName");
        rfvCultureCode.ErrorMessage = ResHelper.GetString("Culture_New.ErrorEmptyCultureCode");
        rfvCultureShortName.ErrorMessage = ResHelper.GetString("Culture_New.ErrorEmptyCultureShortName");

        // Init breadcrumbs
        string cultures = ResHelper.GetString("Culture_New.Cultures");
        string currentCulture = ResHelper.GetString("Culture_New.CurrentCulture");
        string title = ResHelper.GetString("Culture_New.CurrentCulture");

        string[,] breadcrumbs = new string[2, 3];
        breadcrumbs[0, 0] = cultures;
        breadcrumbs[0, 1] = "~/CMSSiteManager/Development/Cultures/Culture_List.aspx";
        breadcrumbs[0, 2] = "";
        breadcrumbs[1, 0] = currentCulture;
        breadcrumbs[1, 1] = "";
        breadcrumbs[1, 2] = "";

        // Init master page properties (page title)
        this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
        this.CurrentMaster.Title.TitleText = title;
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Culture/new.png");
        this.CurrentMaster.Title.HelpTopicName = "general_tabnew";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Handles btnOK's OnClick event - Save culture info
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Validate the input
        string result = new Validator().NotEmpty(txtCultureName.Text, rfvCultureName.ErrorMessage).NotEmpty(txtCultureCode.Text, rfvCultureCode.ErrorMessage).Result;

        if (txtCultureCode.Text.Length > 10)
        {
            result = ResHelper.GetString("Culture.MaxLengthError");
        }

        // Validate the culture code
        try
        {
            System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(txtCultureCode.Text);
        }
        catch
        {
            result = ResHelper.GetString("Culture.ErrorNoGlobalCulture");
        }

        if (result == "")
        {
            // Check if culture already exists
            CultureInfo ci = CultureInfoProvider.GetCultureInfo(txtCultureCode.Text);
            if (ci == null)
            {
                // Save culture info
                ci = new CultureInfo();
                ci.CultureName = txtCultureName.Text;
                ci.CultureCode = txtCultureCode.Text;
                ci.CultureShortName = txtCultureShortName.Text;
                CultureInfoProvider.SetCultureInfo(ci);

                UrlHelper.Redirect("Culture_Edit_Frameset.aspx?cultureID=" + ci.CultureID + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Culture_New.CultureExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }
}
