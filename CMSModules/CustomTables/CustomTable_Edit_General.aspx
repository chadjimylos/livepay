<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="CustomTable_Edit_General.aspx.cs" Inherits="CMSModules_CustomTables_CustomTable_Edit_General"
    Title="Custom table edit - General" Theme="Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <table>
        <tr>
            <td colspan="4">
                <asp:Label ID="lblInfo" runat="server" Visible="false" CssClass="InfoLabel" EnableViewState="false" />
                <asp:Label ID="lblError" runat="server" Visible="false" CssClass="ErrorLabel" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:LocalizedLabel ID="lblDisplayName" runat="server" CssClass="FieldLabel" EnableViewState="false"
                    ResourceString="customtable.newwizzard.DisplayName" />
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" ControlToValidate="txtDisplayName"
                    runat="server" Display="Dynamic" />
            </td>
        </tr>
        <tr>
            <td>
                <cms:LocalizedLabel ID="lblCodeName" runat="server" CssClass="FieldLabel" EnableViewState="false"
                    ResourceString="customtable.newwizzard.FullCodeName" />
            </td>
            <td>
                <asp:TextBox ID="txtCodeNameNamespace" runat="server" CssClass="TextBoxField" MaxLength="49" />
            </td>
            <td>
                .
            </td>
            <td>
                <asp:TextBox ID="txtCodeNameName" runat="server" CssClass="TextBoxField" MaxLength="50" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td>
                <cms:LocalizedLabel ID="lblNamespace" runat="server" ResourceString="customtable.newwizzard.NamespaceName"
                    EnableViewState="false" /><br />
                <asp:RequiredFieldValidator ID="rfvCodeNameNamespace" ControlToValidate="txtCodeNameNamespace"
                    runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                <cms:LocalizedLabel ID="lblName" runat="server" ResourceString="customtable.newwizzard.CodeName"
                    EnableViewState="false" /><br />
                <asp:RequiredFieldValidator ID="rfvCodeNameName" ControlToValidate="txtCodeNameName"
                    runat="server" Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblTableName" EnableViewState="false" ResourceString="class.TableName" />
            </td>
            <td>
                <asp:Label runat="server" ID="lblTableNameValue" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblNewPage" EnableViewState="false" ResourceString="customtable.edit.NewPage" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNewPage" CssClass="TextBoxField" MaxLength="200" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblEditingPage" EnableViewState="false" ResourceString="customtable.edit.EditingPage" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtEditingPage" CssClass="TextBoxField" MaxLength="200" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblViewPage" EnableViewState="false" ResourceString="customtable.edit.ViewPage" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtViewPage" CssClass="TextBoxField" MaxLength="200" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblListPage" EnableViewState="false" ResourceString="customtable.edit.ListPage" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtListPage" CssClass="TextBoxField" MaxLength="200" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
            <td colspan="3">
                <cms:LocalizedButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOk_Click"
                    ResourceString="general.ok" EnableViewState="false" />
            </td>
        </tr>
    </table>
</asp:Content>
