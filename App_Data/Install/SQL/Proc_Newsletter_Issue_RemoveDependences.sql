-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Newsletter_Issue_RemoveDependences]
	@IssueID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
	-- Newsletter_Emails
    DELETE FROM Newsletter_Emails WHERE EmailNewsletterIssueID = @IssueID;
	COMMIT TRANSACTION;
END
