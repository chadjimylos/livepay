using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.Polls;
using CMS.UIControls;

public partial class CMSModules_Polls_Tools_Polls_View : CMSPollsPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get PollID from querystring
        int pollId = QueryHelper.GetInteger("pollid", 0);

        // Get poll object
        PollInfo pi = PollInfoProvider.GetPollInfo(pollId);
        if (pi != null)
        {
            pollElem.PollCodeName = pi.PollCodeName;
            pollElem.CountType = CountTypeEnum.Percentage;
            pollElem.ShowGraph = true;
            pollElem.ShowResultsAfterVote = true;
            // Check permissions during voting if user hasn't got 'Modify' permission
            pollElem.CheckPermissions = (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.polls", "Modify"));
            pollElem.CheckVoted = false;
            pollElem.HideWhenNotAuthorized = false;
            pollElem.CheckOpen = false;
            pollElem.IsLiveSite = false;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        int textLength = pollElem.ButtonText.Length;
            if (textLength > 15)
            {
                pollElem.VoteButton.CssClass = "XLongSubmitButton";
            }
            else if (textLength > 8)
            {
                pollElem.VoteButton.CssClass = "LongSubmitButton";
            }
            else
            {
                pollElem.VoteButton.CssClass = "SubmitButton";
            }
    }
}
