<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="CMSModules_Ecommerce_Tools_Reports_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>E-commerce - Reports</title>
</head>
    <frameset border="0" cols="305,*" id="colsFramesetEcommReports" runat="server">
        <frame name="ecommreportstree" src="../../../../CMSModules/Reporting/Tools/Ecommerce/Reports_Tree.aspx" scrolling="no" frameborder="0" runat="server"/>
        <frame name="ecommreports" src="../../../../CMSDesk/blank.htm" scrolling="auto" frameborder="0" runat="server"/>
		<noframes>
			<p id="p1">
				This HTML frameset displays multiple Web pages. To view this frameset, use a 
				Web browser that supports HTML 4.0 and later.
			</p>
		</noframes>    
    </frameset>
</html>
