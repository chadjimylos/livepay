using System;
using System.Text;
using System.Web;
using System.Web.UI;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.ExtendedControls;
using CMS.CMSHelper;

public partial class CMSModules_Content_Controls_Attachments_DirectFileUploader_DirectFileUploader : DirectFileUploader
{
    #region "Variables"

    private Guid mAttachmentGUID = Guid.Empty;
    private Guid mAttachmentGroupGUID = Guid.Empty;
    private Guid mFormGUID = Guid.Empty;
    private string mIFrameUrl = null;
    private bool? mIsSupportedBrowser = null;
    private MediaSourceEnum mSourceType = MediaSourceEnum.DocumentAttachments;

    #endregion


    #region "Properties"

    /// <summary>
    /// Gets or sets type of the content uploaded by the control.
    /// </summary>
    public override MediaSourceEnum SourceType
    {
        get
        {
            return this.mSourceType;
        }
        set
        {
            this.mSourceType = value;
        }
    }


    /// <summary>
    /// Gets or sets node site name
    /// </summary>
    public override string IFrameUrl
    {
        get
        {
            if (this.mIFrameUrl == null)
            {
                this.mIFrameUrl = GetIframeUrl();
            }
            return mIFrameUrl;
        }
    }


    /// <summary>
    /// Indicates whether the control is displayed.
    /// </summary>
    public override bool Visible
    {
        get
        {
            return base.Visible;
        }
        set
        {
            base.Visible = value;
            uploaderFrame.Visible = value;
            if (IsSupportedBrowser)
            {
                imgLoad.Visible = value;
            }
        }
    }


    /// <summary>
    /// Control key.
    /// </summary>
    private string ControlKey
    {
        get
        {
            return ControlGroup + "_" + ParentElemID;
        }
    }


    /// <summary>
    /// Iframe CSS class.
    /// </summary>
    private string IFrameCSSClass
    {
        get
        {
            return "DFUframe_" + ControlGroup;
        }
    }


    /// <summary>
    /// Iframe CSS class.
    /// </summary>
    private string PanelCSSClass
    {
        get
        {
            return "DFUpanel_" + ControlGroup;
        }
    }


    /// <summary>
    /// Indicates if supported browser.
    /// </summary>
    public bool IsSupportedBrowser
    {
        get
        {
            if (mIsSupportedBrowser == null)
            {
                string browser = DataHelper.GetNotEmpty(CMSContext.GetBrowserClass(), "").ToLower();
                mIsSupportedBrowser = browser.Contains("ie") || browser.Contains("gecko");
            }
            return mIsSupportedBrowser.Value;
        }
    }

    #endregion


    #region "Methods"

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            if (!RequestHelper.IsPostBack() || ForceLoad)
            {
                LoadIFrame();
            }

            if (ControlGroup != null)
            {
                // First instance of the control is loaded
                RequestStockHelper.Add("DFUInstanceLoaded_" + ControlKey, true);

                string script =
                    "function DFULoadIframes_" + ControlKey + "(){\n" +
                    "   var iframe = document.getElementById('" + uploaderFrame.ClientID + "');\n" +
                    "   if (iframe!=null){\n" +
                    "       var iframes = $j('iframe." + IFrameCSSClass + "');\n" +
                    "       for(var i = 0;i<iframes.length;i++){\n" +
                    "           var currIframe = iframes[i];\n" +
                    // Remove fake img because of IE6 bug
                    "             var imgs = currIframe.parentNode.parentNode.getElementsByTagName('img');" +
                    "             if ((imgs != null) &&(imgs[0] != null)) {" +
                    "                 currIframe.parentNode.parentNode.removeChild(imgs[0]);" +
                    "             }" +
                    // Duplicate iframe HTML code
                    "             var originalHtml = iframe.contentWindow.document.childNodes[1].innerHTML;\n" +
                    "             originalHtml = originalHtml.replace(/action=[^\\s]+/, 'action=' + DFUframes[currIframe.id]);\n" +
                    "             currIframe.contentWindow.document.write(originalHtml);\n" +
                    "             currIframe.contentWindow.document.close();\n" +
                    "             currIframe.style.display = '';\n" +
                    "       }\n" +
                    "   }\n" +
                    "}\n";
                ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "DFUIframesLoader_" + ControlKey, ScriptHelper.GetScript(script));
                if (RequestHelper.IsAsyncPostback())
                {
                    ScriptHelper.RegisterStartupScript(this, typeof(string), "DFUIframesLoader_" + ControlKey, ScriptHelper.GetScript(script));
                }
            }
        }
    }


    /// <summary>
    /// Loads data of the control.
    /// </summary>
    public override void ReloadData()
    {
        this.mIFrameUrl = null;

        LoadIFrame();
    }


    /// <summary>
    /// Loads inner IFrame content.
    /// </summary>
    private void LoadIFrame()
    {
        // Temporary image
        if (IsSupportedBrowser)
        {

            imgLoad.Visible = true;
            imgLoad.ImageUrl = ImageUrl;
        }

        // Set iframe attributes
        if ((ControlGroup == null) || !IsSupportedBrowser || !ValidationHelper.GetBoolean(RequestStockHelper.GetItem("DFUInstanceLoaded_" + ControlKey), false))
        {
            uploaderFrame.Attributes.Add("src", ResolveUrl(this.IFrameUrl));
            if ((ControlGroup != null) && IsSupportedBrowser)
            {
                uploaderFrame.Attributes.Add("onload", "DFULoadIframes_" + ControlKey + "();");
            }
            imgLoad.Visible = false;
        }
        else
        {
            uploaderFrame.Attributes.Add("style", "display:none;");
            uploaderFrame.Attributes.Add("class", IFrameCSSClass);
            string script =
                "if(!window.DFUframes) {var DFUframes = new Object();}\n" +
                "if(!window.DFUpanels) {var DFUpanels = new Object();}\n" +
                "DFUframes['" + uploaderFrame.ClientID + "'] = " + ScriptHelper.GetString(ResolveUrl(this.IFrameUrl)) + ";\n";
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "DFUArrays_" + ClientID, ScriptHelper.GetScript(script));
            if (RequestHelper.IsAsyncPostback())
            {
                ScriptHelper.RegisterStartupScript(this, typeof(string), "DFUArrays_" + ClientID, ScriptHelper.GetScript(script));
            }
        }
        uploaderFrame.Attributes.Add("allowtransparency", "true");
        uploaderFrame.Attributes.Add("width", ImageWidth.ToString());
        uploaderFrame.Attributes.Add("height", ImageHeight.ToString());
        containerDiv.Attributes.Add("style", "height:" + ImageHeight + "px;");
    }


    /// <summary>
    /// Generates iframe URL
    /// </summary>
    /// <returns>URL for iframe</returns>
    private string GetIframeUrl()
    {
        // Create URL for iframe
        StringBuilder sb = new StringBuilder();
        if (ImageWidth != 0)
        {
            sb.Append("&imagewidth=" + ImageWidth);
        }
        if (ImageHeight != 0)
        {
            sb.Append("&imageheight=" + ImageHeight);
        }
        if (!string.IsNullOrEmpty(ImageUrl))
        {
            sb.Append("&imageurl=" + Server.UrlEncode(ImageUrl));
        }
        if (!string.IsNullOrEmpty(ImageUrlOver))
        {
            sb.Append("&imageurlover=" + Server.UrlEncode(ImageUrlOver));
        }
        if (!string.IsNullOrEmpty(LoadingImageUrl))
        {
            sb.Append("&loadingimageurl=" + Server.UrlEncode(LoadingImageUrl));
        }
        if (!string.IsNullOrEmpty(InnerDivHtml))
        {
            sb.Append("&innerdivhtml=" + Server.UrlEncode(InnerDivHtml));
        }
        if (!string.IsNullOrEmpty(InnerDivClass))
        {
            sb.Append("&innerdivclass=" + Server.UrlEncode(InnerDivClass));
        }
        if (!string.IsNullOrEmpty(InnerLoadingDivHtml))
        {
            sb.Append("&innerloadingdivhtml=" + Server.UrlEncode(InnerLoadingDivHtml));
        }
        if (!string.IsNullOrEmpty(InnerLoadingDivClass))
        {
            sb.Append("&innerloadingdivclass=" + Server.UrlEncode(InnerLoadingDivClass));
        }
        if (AttachmentGUID != Guid.Empty)
        {
            sb.Append("&attachmentguid=" + AttachmentGUID);
        }
        if (AttachmentGroupGUID != Guid.Empty)
        {
            sb.Append("&attachmentgroupguid=" + AttachmentGroupGUID);
        }
        if (!string.IsNullOrEmpty(AttachmentGUIDColumnName))
        {
            sb.Append("&attachmentguidcolumnname=" + AttachmentGUIDColumnName);
        }
        if (ResizeToWidth != 0)
        {
            sb.Append("&autoresize_width=" + ResizeToWidth);
        }
        if (ResizeToHeight != 0)
        {
            sb.Append("&autoresize_height=" + ResizeToHeight);
        }
        if (ResizeToMaxSideSize != 0)
        {
            sb.Append("&autoresize_maxsidesize=" + ResizeToMaxSideSize);
        }
        if (FormGUID != Guid.Empty)
        {
            sb.Append("&formguid=" + FormGUID);
        }
        if (DocumentID != 0)
        {
            sb.Append("&documentid=" + DocumentID);
        }
        if (NodeParentNodeID != 0)
        {
            sb.Append("&parentid=" + NodeParentNodeID);
        }
        if (!string.IsNullOrEmpty(NodeClassName))
        {
            sb.Append("&classname=" + NodeClassName);
        }
        if (InsertMode)
        {
            sb.Append("&insertmode=1");
        }
        if (OnlyImages)
        {
            sb.Append("&onlyimages=1");
        }
        if (RaiseOnClick)
        {
            sb.Append("&click=1");
        }
        if (AllowedExtensions != null)
        {
            sb.Append("&allowedextensions=" + Server.UrlEncode(AllowedExtensions));
        }
        if (!string.IsNullOrEmpty(ParentElemID))
        {
            sb.Append("&parentelemid=" + ParentElemID);
        }
        if (!IsLiveSite)
        {
            sb.Append("&islive=0");
        }
        sb.Append("&source=" + CMSDialogHelper.GetMediaSource(this.SourceType));
        if (!CheckPermissions)
        {
            sb.Append("&checkperm=0");
        }
        sb.Append("&frameid=" + uploaderFrame.ClientID);

        // If library information specified - pass it to the inner control (for new media file creation)
        if ((this.LibraryID > 0) && (this.LibraryFolderPath != null))
        {
            sb.Append("&libraryid=" + this.LibraryID);
            sb.Append("&path=" + Server.UrlEncode(this.LibraryFolderPath));

            sb.Append("&mediafileid=" + this.MediaFileID);
            if (this.IsMediaThumbnail)
            {
                sb.Append("&ismediathumbnail=1");
            }
            if (!string.IsNullOrEmpty(this.MediaFileName))
            {
                sb.Append("&filename=" + HTMLHelper.HTMLEncode(this.MediaFileName));
            }
        }

        if (this.IncludeNewItemInfo)
        {
            sb.Append("&includeinfo=1");
        }

        if (this.NodeSiteName != null)
        {
            sb.Append("&sitename=" + NodeSiteName);
        }

        if (!string.IsNullOrEmpty(AfterSaveJavascript))
        {
            sb.Append("&aftersave=" + Server.UrlEncode(AfterSaveJavascript).Replace("&", "%26"));
        }

        // Append validation hash
        sb.Replace("&", "?", 0, 1);
        string validationHash = QueryHelper.GetHash(sb.ToString());
        if (CMSContext.CurrentUser.IsAuthenticated())
        {
            sb.Insert(0, "~/CMSModules/Content/Attachments/DirectFileUploader/FileUploader.aspx");
        }
        else
        {
            sb.Insert(0, "~/CMSModules/Content/Attachments/DirectFileUploader/PublicFileUploader.aspx");
        }
        sb.Append("&hash=" + validationHash);
        return sb.ToString();
    }

    #endregion
}
