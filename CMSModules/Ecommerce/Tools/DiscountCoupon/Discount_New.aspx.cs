using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_DiscountCoupon_Discount_New : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        rfvCouponCode.ErrorMessage = ResHelper.GetString("DiscounCoupon_Edit.errorCode");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("DiscounCoupon_Edit.errorDisplay");


        // control initializations				
        lblDiscountCouponCode.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponCodeLabel");
        lblDiscountCouponDisplayName.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponDisplayNameLabel");
        btnOk.Text = ResHelper.GetString("General.OK");

        
        lblDiscountCouponValidTo.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponValidToLabel");
        lblDiscountCouponValidFrom.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponValidFromLabel");
        lblDiscountCouponCode.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponCodeLabel");
        lblDiscountCouponValue.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponAbsoluteValueLabel");

        radFixed.Text = ResHelper.GetString("DiscounCoupon_Edit.radFixed");
        radPercentage.Text = ResHelper.GetString("DiscounCoupon_Edit.radPercentage");

        rfvCouponCode.ErrorMessage = ResHelper.GetString("DiscounCoupon_Edit.errorCode");

        dtPickerDiscountCouponValidTo.SupportFolder = "~/CMSAdminControls/Calendar";
        dtPickerDiscountCouponValidFrom.SupportFolder = "~/CMSAdminControls/Calendar";

        string currentDiscountCoupon = ResHelper.GetString("DiscounCoupon_Edit.NewItemCaption");


        // Page title
        this.CurrentMaster.Title.HelpTopicName = "new_coupongeneral_tab";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Initializes page title breadcrumbs control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("DiscounCoupon_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/DiscountCoupon/Discount_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentDiscountCoupon;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        string errorMessage = new Validator().NotEmpty(txtDiscountCouponDisplayName.Text, ResHelper.GetString("DiscounCoupon_Edit.errorDisplay")).NotEmpty(txtDiscountCouponCode.Text, ResHelper.GetString("DiscounCoupon_Edit.errorCode")).Result;

        // Discount coupon must be positive number
        if (errorMessage == "")
        {
            if (!ValidationHelper.IsPositiveNumber(txtDiscountCouponValue.Text.Trim()))
            {
                errorMessage = ResHelper.GetString("DiscountCoupon_Edit.errorValue");
            }
        }

        // from/to date validation
        if (errorMessage == "")
        {
            if ((dtPickerDiscountCouponValidFrom.SelectedDateTime != DateTime.MinValue) &&
            (dtPickerDiscountCouponValidTo.SelectedDateTime != DateTime.MinValue) &&
            (dtPickerDiscountCouponValidFrom.SelectedDateTime >= dtPickerDiscountCouponValidTo.SelectedDateTime))
            {
                errorMessage = ResHelper.GetString("General.DateOverlaps");
            }
        }

        if (errorMessage == "")
        {
            // discountCouponCode must to be unique
            DiscountCouponInfo discountCouponObj = DiscountCouponInfoProvider.GetDiscountCouponInfo(txtDiscountCouponCode.Text.Trim());

            // if discountCouponCode value is unique														
            if ((discountCouponObj == null))
            {
                // create new item -> insert
                discountCouponObj = new DiscountCouponInfo();

                discountCouponObj.DiscountCouponCode = txtDiscountCouponCode.Text.Trim();
                discountCouponObj.DiscountCouponDisplayName = txtDiscountCouponDisplayName.Text.Trim();
                discountCouponObj.DiscountCouponIsExcluded = false;

                discountCouponObj.DiscountCouponIsFlatValue = true;

                if (radPercentage.Checked)
                {
                    discountCouponObj.DiscountCouponIsFlatValue = false;
                }

                discountCouponObj.DiscountCouponValue = ValidationHelper.GetDouble(txtDiscountCouponValue.Text.Trim(), 0.0);
                discountCouponObj.DiscountCouponCode = txtDiscountCouponCode.Text.Trim();
                discountCouponObj.DiscountCouponValidFrom = dtPickerDiscountCouponValidFrom.SelectedDateTime;
                discountCouponObj.DiscountCouponValidTo = dtPickerDiscountCouponValidTo.SelectedDateTime;

                DiscountCouponInfoProvider.SetDiscountCouponInfo(discountCouponObj);

                UrlHelper.Redirect("Discount_Edit_Frameset.aspx?discountid=" + Convert.ToString(discountCouponObj.DiscountCouponID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("DiscounCoupon_Edit.DiscountCouponCodeExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
