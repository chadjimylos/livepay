<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DropDownAegeanPower.ascx.cs" Inherits="CMSFormControls_DropDownAegeanPower" %>
<script language="javascript" type="text/javascript">
    function ClearNewJob(obj) {
        if (obj.value !=null) {
            obj.value = '';
        }
    }
</script>

<asp:UpdatePanel ID="Update" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
<ContentTemplate>
 
    <asp:DropDownList runat="server" ID="drpJob" OnSelectedIndexChanged="Index_Changed" AutoPostBack="true" CssClass="DropDownField">
    </asp:DropDownList>
    <asp:TextBox TextMode="MultiLine" ID="txtNewJob" runat="server" Visible="false" class="FormOtherPosition" onclick="ClearNewJob(this)"></asp:TextBox>
</ContentTemplate>
</asp:UpdatePanel>
