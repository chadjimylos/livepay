CREATE PROCEDURE [Proc_CMS_SettingsCategory_MoveCategoryDown]
	@CategoryID int
AS
BEGIN
	DECLARE @MaxCategoryOrder int
	SET @MaxCategoryOrder = (SELECT TOP 1 CategoryOrder FROM CMS_SettingsCategory ORDER BY CategoryOrder DESC);
	/* Move the next step(s) up */
	UPDATE CMS_SettingsCategory SET CategoryOrder = CategoryOrder - 1 WHERE CategoryOrder = (SELECT CategoryOrder FROM CMS_SettingsCategory WHERE CategoryID = @CategoryID) + 1 
	/* Move the current step down */
	UPDATE CMS_SettingsCategory SET CategoryOrder = CategoryOrder + 1 WHERE CategoryID = @CategoryID AND CategoryOrder < @MaxCategoryOrder
END
