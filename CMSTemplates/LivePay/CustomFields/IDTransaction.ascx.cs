using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;
using System.Data.SqlClient;

public partial class CMSFormControls_IDTransaction : LivePayFormEngineUserControl
{
    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            base.Enabled = value;
            this.IDTransaction.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            if (IsEmpty())
            {
                return "keno";
            }
            return this.IDTransaction.Text;
        }
        set
        {
            this.IDTransaction.Text = (string)value;
        }
    }


    /// <summary>
    /// Clears current value
    /// </summary>
    //public void Clear()
    //{
    //    IDTransaction.Text = "";
    //}


    /// <summary>
    /// Returns true if the number is empty
    /// </summary>
    public bool IsEmpty()
    {
        return (DataHelper.IsEmpty(this.IDTransaction.Text));
    }


    /// <summary>
    /// Returns true if user control is valid.
    /// </summary>
    public override bool IsValid()
    {
        if (IsEmpty() && this.Mandatory)
        {
            return false;
        }

        // Find control on page.
        //Control txtPrce = this.Page.FindControl("updPanel");
        //plc_lt_ContentPlaceHolder_ContentPlaceHolder_ContentPlaceHolder_lt_ContentPlaceHolder_pageplaceholder_pageplaceholder_lt_UCRegisteredUsers_UCRegisteredUsers_userControlElem_formUserControl_3_IDTransaction
        //Response.Write(this.Page.Controls[1].Controls[3].Controls[0].Controls[0]    .ID);
        //if (txtPrce != null)
        //{
        //    // Get control's parent.
        //    Control myControl2 = txtPrce.Parent;
        //    Response.Write("Parent of the text box is : " + myControl2.ID);
        //}
        //else
        //{
        //    Response.Write("Control not found");
        //}

        
        //TextBox txtPrce = this.Page.FindControl("Tab1_txtPrice") as TextBox;

        //Response.Write("price -> " + txtPrce.Text);

        Validator val = new Validator();
        string result = val.IsRegularExp(IDTransaction.Text, @"\d{9}\s\d{11}\s\d{3,11}", "error").Result;

        if (result != "")
        {
            return false;
        }

        string[] arrIdTransaction = IDTransaction.Text.Split(' ');

        string taxID = arrIdTransaction[0];
        string transID = arrIdTransaction[1];
        string price = Right("00000000000" + arrIdTransaction[2], 11);

        if (!ValidatorTaxID(taxID))
            return false;

        Int16 date = Convert.ToInt16(Mid(transID, 3, 4));
        Int16 currentDate = Convert.ToInt16(DateTime.Now.ToString("MMdd"));

        //Response.Write(date);
        if (date < currentDate)
            return false;

        //Response.Write("date -> " + date + "  |");
        
        string cd = Right(transID, 2);
        transID = Left(transID, 9);

        string Val_Code = taxID + transID + price + cd;

        //Response.Write(Val_Code);

        int Y = IntXmod11(Val_Code);
        //Response.Write(Y);

        if (Y != 1)
            return false;
        else
            return true;

        //028004819 01011261545 173250

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CustomCloud_div.InnerHtml = string.Concat("<a id='CustomCloud_", this.RowCustomField, "' href='#'></a>");

        cv_IDTransaction.Attributes.Add("ControlValidated", IDTransaction.ClientID);
        cv_IDTransaction.Attributes.Add("IsCustomValidator", String.Concat("CustomCloud_", this.RowCustomField));
        cv_IDTransaction.Attributes.Add("CustomValidatorErrorMessage", this.ValidationError);


    }

    private int IntXmod11(string IntX)
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnectionString"].ToString();
        int mod11 = 0;
        string queryString = " DECLARE @int Decimal(38,0) " +
                             " SET @int = " + IntX +
                             " SELECT @int % 11 ";
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryString, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    mod11 = Convert.ToInt32(reader[0].ToString());
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }
        }

        return mod11;

    }


    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }
}

