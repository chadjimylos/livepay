﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.ExtendedControls;

public partial class CMSInlineControls_YouTubeControl : InlineUserControl
{
    #region "Properties"

    /// <summary>
    /// Url of youtube media.
    /// </summary>
    public string Url
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Url"), "");
        }
        set
        {
            this.SetValue("Url", value);
        }
    }


    /// <summary>
    /// Enable full screen for youtube player.
    /// </summary>
    public bool Fs
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("Fs"), false);
        }
        set
        {
            this.SetValue("Fs", value);
        }
    }


    /// <summary>
    /// Gets or sets the value which indicates whether the video should be played in HD by default.
    /// </summary>
    public bool PlayInHD
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("hd"), false);
        }
        set
        {
            this.SetValue("hd", value);
        }
    }


    /// <summary>
    /// Enable auto play for youtube player.
    /// </summary>
    public bool AutoPlay
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("AutoPlay"), false);
        }
        set
        {
            this.SetValue("AutoPlay", value);
        }
    }


    /// <summary>
    /// Enable loop for youtube player.
    /// </summary>
    public bool Loop
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("Loop"), false);
        }
        set
        {
            this.SetValue("Loop", value);
        }
    }


    /// <summary>
    /// Enable relative videos in youtube player.
    /// </summary>
    public bool Rel
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("Rel"), false);
        }
        set
        {
            this.SetValue("Rel", value);
        }
    }


    /// <summary>
    /// Enable delayed cookies for youtube player.
    /// </summary>
    public bool Cookies
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("Cookies"), false);
        }
        set
        {
            this.SetValue("Cookies", value);
        }
    }


    /// <summary>
    /// Show border around youtube player.
    /// </summary>
    public bool Border
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("Border"), false);
        }
        set
        {
            this.SetValue("Border", value);
        }
    }


    /// <summary>
    /// Color 1 for youtube player.
    /// </summary>
    public string Color1
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Color1"), "#666666");
        }
        set
        {
            this.SetValue("Color1", value);
        }
    }


    /// <summary>
    /// Color 2 for youtube player.
    /// </summary>
    public string Color2
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("Color2"), "#efefef");
        }
        set
        {
            this.SetValue("Color2", value);
        }
    }


    /// <summary>
    /// Width of youtube player.
    /// </summary>
    public int Width
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("Width"), 0);
        }
        set
        {
            this.SetValue("Width", value);
        }
    }


    /// <summary>
    /// Height of youtube player.
    /// </summary>
    public int Height
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("Height"), 0);
        }
        set
        {
            this.SetValue("Height", value);
        }
    }


    /// <summary>
    /// Control parameter
    /// </summary>
    public override string Parameter
    {
        get
        {
            return this.Url;
        }
        set
        {
            this.Url = value;
        }
    }

    #endregion

    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        YouTubeVideoParameters ytParams = new YouTubeVideoParameters();
        ytParams.Url = ResolveUrl(this.Url).Replace("\"", "\\\"");
        ytParams.FullScreen = this.Fs;
        ytParams.PlayInHD = this.PlayInHD;
        ytParams.AutoPlay = this.AutoPlay;
        ytParams.Loop = this.Loop;
        ytParams.RelatedVideos = this.Rel;
        ytParams.Delayed = this.Cookies;
        ytParams.Border = this.Border;
        ytParams.Color1 = this.Color1;
        ytParams.Color2 = this.Color2;
        ytParams.Width = this.Width;
        ytParams.Height = this.Height;

        this.ltlYouTube.Text = MediaHelper.GetYouTubeVideo(ytParams);

        if (!ScriptHelper.IsClientScriptBlockRegistered(Page, "YouTubeUnload"))
        {
            string script = ScriptHelper.GetScript("window.onbeforeunload = function(){\n var ytEmbeds = document.getElementsByTagName('embed'); if (ytEmbeds.length > 0) {\n for (var i = 0; i < ytEmbeds.length; i++){\n ytEmbeds[i].parentNode.removeChild(ytEmbeds[i]);}}}");
            ScriptHelper.RegisterClientScriptBlock(this.Page, typeof(Page), "YouTubeUnload", script);
        }
    }

    #endregion
}
