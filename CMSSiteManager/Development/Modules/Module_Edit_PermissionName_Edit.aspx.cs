using System;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Modules_Module_Edit_PermissionName_Edit : SiteManagerPage
{
    #region "Variables"

    PermissionNameInfo currentPermission = null;
    protected int permissionId = 0;
    protected string permissionName = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        permissionId = QueryHelper.GetInteger("permissionid", 0);

        if (permissionId > 0)
        {
            currentPermission = PermissionNameInfoProvider.GetPermissionNameInfo(permissionId);

            if (!RequestHelper.IsPostBack())
            {
                if (currentPermission != null)
                {
                    tbPermissionCodeName.Text = currentPermission.PermissionName;
                    tbPermissionDisplayName.Text = currentPermission.PermissionDisplayName;
                    txtPermissionDescription.Text = currentPermission.PermissionDescription;
                    chkPermissionDisplayInMatrix.Checked = currentPermission.PermissionDisplayInMatrix;
                }

                // shows that the permission was created or updated successfully
                if (QueryHelper.GetBoolean("saved", false))
                {
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
            }

            if (currentPermission != null)
            {
                permissionName = currentPermission.PermissionDisplayName;
            }
        }
        else
        {
            permissionName = ResHelper.GetString("Module_Edit_PermissionName_Edit.NewPermission");
        }

        string permissionNames = ResHelper.GetString("Module_Edit_PermissionName_Edit.PermissionNames");

        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = permissionNames;
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/Modules/Module_Edit_PermissionNames.aspx?moduleId=" + Request.QueryString["moduleID"];
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = permissionName;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.HelpTopicName = "newedit_permission";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        lblPermissionDescription.Text = ResHelper.GetString("Administration-Module_Edit_PermissionName_Edit.PermissionDescription");
        lbPermissionDisplayName.Text = ResHelper.GetString("Administration-Module_Edit_PermissionName_Edit.PermissionDisplayName");
        lbPermissionCodeName.Text = ResHelper.GetString("Administration-Module_Edit_PermissionName_Edit.PermissionCodeName");
        lblPermissionDisplayInMatrix.Text = ResHelper.GetString("Administration-Module_Edit_PermissionName_Edit.DisplayInMatrix");

        rfvPermissionDisplayName.ErrorMessage = ResHelper.GetString("Administration-Module_Edit_PermissionName_Edit.ErrorEmptyPermissionDisplayName");
        rfvPermissionCodeName.ErrorMessage = ResHelper.GetString("Administration-Module_Edit_PermissionName_Edit.ErrorEmptyPermissionCodeName");

        btnOk.Text = ResHelper.GetString("general.ok");
    }


    /// <summary>
    /// Handles btnOK's OnClick event - Update or save permission info
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Finds whether required fields are not empty
        string result = new Validator().NotEmpty(tbPermissionDisplayName.Text, ResHelper.GetString("Administration-Module_Edit_PermissionName_Edit.ErrorEmptyPermissionDisplayName")).NotEmpty(tbPermissionCodeName.Text, ResHelper.GetString("Administration-Module_Edit_PermissionName_Edit.ErrorEmptyPermissionCodeName"))
            .IsCodeName(tbPermissionCodeName.Text, ResHelper.GetString("general.invalidcodename")).Result;

        if (result == "")
        {
            int resourceId = QueryHelper.GetInteger("moduleid", 0);
            if ((resourceId <= 0) && (currentPermission != null))
            {
                resourceId = currentPermission.ResourceId;
            }

            string resourceName = "";

            ResourceInfo ri = ResourceInfoProvider.GetResourceInfoById(resourceId);
            if (ri != null)
            {
                resourceName = ri.ResourceName;
            }

            PermissionNameInfo pni = PermissionNameInfoProvider.GetPermissionNameInfo(tbPermissionCodeName.Text, resourceName, null);

            if ((pni == null) || (pni.PermissionId == permissionId))
            {
                pni = new PermissionNameInfo();
                pni.PermissionId = permissionId;
                pni.PermissionName = tbPermissionCodeName.Text;
                pni.PermissionDisplayName = tbPermissionDisplayName.Text;
                pni.PermissionDescription = txtPermissionDescription.Text;
                pni.PermissionDisplayInMatrix = chkPermissionDisplayInMatrix.Checked;
                pni.ClassId = 0;
                pni.ResourceId = resourceId;

                // Update or save permission info
                PermissionNameInfoProvider.SetPermissionInfo(pni);

                UrlHelper.Redirect("Module_Edit_PermissionName_Edit.aspx?moduleID=" + pni.ResourceId + "&permissionID=" + pni.PermissionId + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Administration-Module_Edit_PermissionName_Edit.UniqueCodeName");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }
}
