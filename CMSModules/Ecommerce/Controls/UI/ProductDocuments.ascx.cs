using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.Ecommerce;
using CMS.TreeEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Controls_UI_ProductDocuments : FormEngineUserControl
{
    #region "Private variables"

    private int mProductId = 0;
    private SKUInfo product = null;
    private string productNameLocalized = "";
    private string selectedSite = null;
    private SiteInfo siteInfo = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// ID of the current product
    /// </summary>
    public int ProductID
    {
        get
        {
            return mProductId;
        }
        set
        {
            mProductId = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Initializes the controls
        SetupControl();
    }


    #region "Private methods"

    /// <summary>
    /// Initializes the controls on the page
    /// </summary>
    private void SetupControl()
    {
        // Get current product info
        product = SKUInfoProvider.GetSKUInfo(ProductID);
        if (product != null)
        {
            // Get no data message text
            productNameLocalized = ResHelper.LocalizeString(product.SKUName);

            uniGridDocs.OnExternalDataBound += uniGridDocs_OnExternalDataBound;
            uniGridDocs.WhereCondition = GetWhereCondition();

            // Allow site selector for global admins
            if (CMSContext.CurrentUser.IsGlobalAdministrator)
            {
                // Preselect 'all sites'
                selectedSite = !RequestHelper.IsPostBack() ? TreeProvider.ALL_SITES : ValidationHelper.GetString(filterDocuments.SelectedSite, String.Empty);
            }
            else
            {
                filterDocuments.LoadSites = false;
                filterDocuments.SitesPlaceHolder.Visible = false;
            }
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        string noDataMessage = string.Format(ResHelper.GetString("ProductDocuments.Documents.nodata"), HTMLHelper.HTMLEncode(productNameLocalized));
        if (filterDocuments.FilterIsSet)
        {
            noDataMessage = ResHelper.GetString("ProductDocuments.Documents.noresults");
        }
        else if (selectedSite != TreeProvider.ALL_SITES)
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(selectedSite);
            if (si != null)
            {
                noDataMessage = string.Format(ResHelper.GetString("ProductDocuments.Documents.nodataforsite"), HTMLHelper.HTMLEncode(productNameLocalized), HTMLHelper.HTMLEncode(si.DisplayName));
            }
        }
        lblInfo.Visible = uniGridDocs.IsEmpty;
        lblInfo.Text = noDataMessage;

        // Hide site column
        if (filterDocuments.SelectedSite != TreeProvider.ALL_SITES)
        {
            uniGridDocs.GridView.Columns[4].Visible = false;
        }
    }


    private string GetWhereCondition()
    {
        selectedSite = CMSContext.CurrentUser.IsGlobalAdministrator ? filterDocuments.SelectedSite : CMSContext.CurrentSiteName;
        string where = "(SKUID=" + ProductID + " AND DocumentNodeID IN (SELECT NodeID FROM CMS_Tree WHERE NodeSKUID=" + ProductID + "))";
        where = SqlHelperClass.AddWhereCondition(where, filterDocuments.WhereCondition);
        return where;
    }


    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private static object GetDocumentLink(string documentName, SiteInfo si, int nodeId, string cultureCode)
    {
        // If the document path is empty alter it with the default '/'
        if (string.IsNullOrEmpty(documentName))
        {
            documentName = "-";
        }

        string url = GetDocumentUrl(si, nodeId, cultureCode);
        string toReturn = HTMLHelper.HTMLEncode(documentName);

        if (!string.IsNullOrEmpty(url))
        {
            toReturn = "<a href=\"" + url + "\" target=\"_blank\">" + toReturn + "</a>";
        }
        return toReturn;
    }


    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private object GetDocumentIconLink(SiteInfo si, int nodeId, string cultureCode, string className)
    {
        string url = GetDocumentUrl(si, nodeId, cultureCode);
        string toReturn = "<img style=\"border-style:none;\" src=\"" + GetDocumentTypeIconUrl(className) + "\" alt=\"" + className + "\" />";

        if (!string.IsNullOrEmpty(url))
        {
            return "<a href=\"" + url + "\" target=\"_blank\">" + toReturn + "</a>";
        }
        return toReturn;
    }


    /// <summary>
    /// Generates URL for document
    /// </summary>
    private static string GetDocumentUrl(SiteInfo si, int nodeId, string cultureCode)
    {
        string url = string.Empty;
        if (si.Status == SiteStatusEnum.Running)
        {
            if (si.SiteName != CMSContext.CurrentSiteName)
            {
                // Make url for site in form 'http(s)://sitedomain/application/cmsdesk'.
                url = UrlHelper.GetApplicationUrl(si.DomainName) + "/cmsdesk/default.aspx?section=content&nodeid=" + nodeId + "&culture=" + cultureCode;
            }
            // Check permissions for current site
            else if (CMSPage.IsUserAuthorizedPerContent())
            {
                // Make url for current site (with port)
                url = UrlHelper.ResolveUrl("~/cmsdesk/default.aspx?section=content&nodeid=" + nodeId + "&culture=" + cultureCode);
            }
        }
        return url;
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }

    #endregion


    #region "Event handlers"

    protected object uniGridDocs_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView drv = GetDataRowView(sender as DataControlFieldCell);
        string siteName = ValidationHelper.GetString(drv["SiteName"], string.Empty);
        string documentCulture = null;
        int nodeId = 0;

        // Get document site
        siteInfo = SiteInfoProvider.GetSiteInfo(siteName);

        switch (sourceName.ToLower())
        {
            case "documentname":
                nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                documentCulture = ValidationHelper.GetString(drv["DocumentCulture"], string.Empty);
                string documentName = ValidationHelper.GetString(drv["DocumentName"], string.Empty);

                // Generate default link for the document
                return GetDocumentLink(documentName, siteInfo, nodeId, documentCulture);

            case "site":
                return siteInfo.DisplayName;

            case "classname":
                string className = ValidationHelper.GetString(drv["ClassName"], string.Empty);
                nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                documentCulture = ValidationHelper.GetString(drv["DocumentCulture"], string.Empty);
                return GetDocumentIconLink(siteInfo, nodeId, documentCulture, className);

            case "documentnametooltip":
                return UniGridFunctions.DocumentNameTooltip(drv);

            case "culture":
                return UniGridFunctions.DocumentCultureFlag(drv, Page);
        }
        return parameter;
    }

    #endregion
}
