using System;
using System.Web.UI;

using CMS.TreeEngine;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.PortalEngine;
using CMS.PortalControls;
using CMS.UIControls;
using CMS.LicenseProvider;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Edit_EditTabs : CMSContentPage, ICallbackEventHandler
{
    #region "Variables"

    private ViewModeEnum currentMode;
    private CurrentUserInfo currentUser = null;
    private int nodeId = 0;


    private bool showProductTab = false;
    private bool isMasterPage = false;
    private bool isPortalPage = false;
    private bool authorizedPerDesign = false;
    private bool designPermissionRequired = false;

    private int pageTabIndex = -1;
    private int designTabIndex = -1;
    private int formTabIndex = -1;
    private int productTabIndex = -1;
    private int masterTabIndex = -1;
    private int propertiesTabIndex = -1;

    private string tabQuery = null;

    #endregion


    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        this["TabControl"] = tabsModes;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RequestHelper.IsPostBack())
        {
            chkWebParts.Checked = PortalHelper.DisplayContentInDesignMode;
        }

        tabQuery = QueryHelper.GetString("tab", null);

        chkWebParts.Attributes.Add("onclick", "SaveSettings()");
        chkWebParts.Text = ResHelper.GetString("EditTabs.DisplayContent");

        ltlScript.Text += ScriptHelper.GetScript("function SaveSettings() { __theFormPostData = ''; WebForm_InitCallback(); " + ClientScript.GetCallbackEventReference(this, "'save'", "RefreshContent", null) + "; }");

        // Initialize tabs
        tabsModes.OnTabCreated += tabModes_OnTabCreated;
        tabsModes.SelectedTab = 0;
        tabsModes.UrlTarget = "contenteditview";

        // Process the page mode
        currentUser = CMSContext.CurrentUser;

        // Current Node ID
        nodeId = QueryHelper.GetInteger("nodeid", 0);

        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        TreeNode node = tree.SelectSingleNode(nodeId);
        if (node != null)
        {
            // Product tab
            DataClassInfo classObj = DataClassInfoProvider.GetDataClass(node.NodeClassName);
            if (classObj != null)
            {
                showProductTab = SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".ProductTabEnabled") && classObj.ClassIsProduct;
            }

            // Initialize required variables
            authorizedPerDesign = currentUser.IsAuthorizedPerResource("CMS.Content", "Design");
            isPortalPage = IsPortalPage(node);
            isMasterPage = (node.NodeAliasPath == "/") && isPortalPage;
            
            // Show master page tab for all portal master pages
            if (!isMasterPage && isPortalPage)
            {
                PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(node.DocumentPageTemplateID);
                if (pti != null)
                {
                    if (pti.ShowAsMasterTemplate)
                    {
                        isMasterPage = true;
                    }
                }
            }


            UpdateViewMode(node, ViewModeEnum.Edit);

            // Get the page mode
            currentMode = CMSContext.ViewMode;

            // If not global administrator, do not allow design mode
            if (!currentUser.IsGlobalAdministrator && (currentMode == ViewModeEnum.Design))
            {
                CMSContext.ViewMode = ViewModeEnum.Edit;
            }
        }
    }


    string[] tabModes_OnTabCreated(UIElementInfo element, string[] parameters, int tabIndex)
    {
        switch (element.ElementName.ToLower())
        {
            case "page":
                pageTabIndex = tabIndex;
                break;

            case "design":
                if (isPortalPage && authorizedPerDesign)
                {
                    designTabIndex = tabIndex;
                }
                else
                {
                    if (!authorizedPerDesign)
                    {
                        designPermissionRequired = true;
                    }
                    return null;
                }
                break;

            case "editform":
                formTabIndex = tabIndex;
                break;

            case "product":
                if (showProductTab && LicenseHelper.IsFeautureAvailableInUI(FeatureEnum.Ecommerce, ModuleEntry.ECOMMERCE))
                {
                    productTabIndex = tabIndex;
                }
                else
                {
                    return null;
                }
                break;

            case "masterpage":
                if (isMasterPage && authorizedPerDesign)
                {
                    masterTabIndex = tabIndex;
                }
                else
                {
                    if (!authorizedPerDesign)
                    {
                        designPermissionRequired = true;
                    }
                    return null;
                }
                break;

            case "properties":
                parameters[2] += (tabQuery != null ? "&tab=" + tabQuery : "");
                propertiesTabIndex = tabIndex;
                break;
        }

        return parameters;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        SelectTab();
    }


    /// <summary>
    /// Indicates if specified node represents portal page
    /// </summary>
    /// <param name="node">Tree node.</param>    
    private static bool IsPortalPage(TreeNode node)
    {
        if (node != null)
        {
            // Get page template information
            PageInfo pi = PageInfoProvider.GetPageInfo(CMSContext.CurrentSiteName, node.NodeAliasPath, node.DocumentCulture, node.DocumentUrlPath, false, null);
            if ((pi != null) && (pi.PageTemplateInfo != null))
            {
                return pi.PageTemplateInfo.IsPortal;
            }
        }
        return false;
    }


    private void SelectTab()
    {
        switch (QueryHelper.GetString("action", "edit").ToLower())
        {
            // New dialog / new page form
            case "new":
            case "newculture":
                tabsModes.SelectedTab = pageTabIndex;
                break;

            case "properties":
                // New document culture
                tabsModes.SelectedTab = propertiesTabIndex;
                break;

            case "product":
                tabsModes.SelectedTab = productTabIndex;
                break;

            default:
                // Set the radio buttons
                switch (currentMode)
                {
                    case ViewModeEnum.Edit:
                        tabsModes.SelectedTab = pageTabIndex;
                        break;

                    case ViewModeEnum.Design:
                        tabsModes.SelectedTab = designTabIndex;
                        ltlScript.Text += ScriptHelper.GetScript("SetTabsContext('design');");
                        break;

                    case ViewModeEnum.EditForm:
                        tabsModes.SelectedTab = formTabIndex;
                        break;

                    case ViewModeEnum.Product:
                        tabsModes.SelectedTab = productTabIndex;
                        break;

                    case ViewModeEnum.Properties:
                        tabsModes.SelectedTab = propertiesTabIndex;
                        break;
                }
                break;
        }

        // Selected tab (for first load)
        if (tabsModes.SelectedTab == -1)
        {
            tabsModes.SelectedTab = 0;
        }

        // Call the script for tab which is selected
        if (!tabsModes.TabsEmpty)
        {
            int selectedTab = (tabsModes.SelectedTab < 0 ? 0 : tabsModes.SelectedTab);
            ScriptHelper.RegisterStartupScript(Page, typeof(string), "FirstTabSelection", ScriptHelper.GetScript(" redir('" + tabsModes.Tabs[selectedTab, 2] + "','" + tabsModes.UrlTarget + "'); " + tabsModes.Tabs[selectedTab, 1]));
        }
        else if (designPermissionRequired)
        {
            ScriptHelper.RegisterStartupScript(Page, typeof(string), "FirstTabSelection", ScriptHelper.GetScript(" redir('" + UrlHelper.ResolveUrl("~/CMSDesk/accessdenied.aspx") + "?resource=CMS.Content&permission=Design','" + tabsModes.UrlTarget + "');"));
        }
        else
        {
            ScriptHelper.RegisterStartupScript(Page, typeof(string), "FirstTabSelection", ScriptHelper.GetScript(" redir('" + UrlHelper.ResolveUrl("~/CMSMessages/Information.aspx") + "?message=" + ResHelper.GetString("uiprofile.uinotavailable") + "','" + tabsModes.UrlTarget + "'); "));
        }
    }


    #region "Callback handling"

    public void RaiseCallbackEvent(string eventArgument)
    {
        if (eventArgument == "save")
        {
            PortalHelper.DisplayContentInDesignMode = chkWebParts.Checked;
        }
    }


    public string GetCallbackResult()
    {
        return "";
    }

    #endregion
}
