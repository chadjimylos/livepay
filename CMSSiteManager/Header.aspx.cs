using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.MembershipProvider;

public partial class CMSSiteManager_Header : SiteManagerPage
{
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        this["TabControl"] = BasicTabControlHeader;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Facebook Connect sign out
        if (CMSContext.CurrentUser.IsAuthenticated())
        {
            if (QueryHelper.GetInteger("logout", 0) > 0)
            {
                btnSignOut_Click(this, EventArgs.Empty);
                return;
            }
        }

        lblVersion.Text = "v" + CMSContext.FullSystemSuffixVersion;

        lnkCmsDesk.Visible = false;
        if (CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            lnkCmsDesk.Visible = true;
        }

        pnlSignOut.BackImageUrl = GetImageUrl("Design/Buttons/SignOutButton.png");
        lblSignOut.Text = ResHelper.GetString("signoutbutton.signout");

        lblUser.Text = ResHelper.GetString("Header.User");
        lblUserInfo.Text = HTMLHelper.HTMLEncode(CMSContext.CurrentUser.FullName);
        lnkCmsDesk.Text = ResHelper.GetString("Header.CMSDesk");
        lnkCmsDesk.NavigateUrl = "~/CMSDesk/default.aspx";
        lnkCmsDesk.Target = "_parent";

        lnkCmsDeskLogo.NavigateUrl = "~/CMSSiteManager/default.aspx";
        lnkCmsDeskLogo.Target = "_parent";

        string[,] tabs = new string[5 + 0,4];
        tabs[0, 0] = "&nbsp;" + ResHelper.GetString("general.sites") + "&nbsp;";
        tabs[0, 2] = "sites/site_list.aspx";
        tabs[1, 0] = "&nbsp;" + ResHelper.GetString("Header.Administration") + "&nbsp;";
        tabs[1, 2] = "administration/default.aspx";
        tabs[2, 0] = "&nbsp;" + ResHelper.GetString("Header.Settings") + "&nbsp;";
        tabs[2, 2] = "settings/default.aspx";
        tabs[3, 0] = "&nbsp;" + ResHelper.GetString("Header.Development") + "&nbsp;";
        tabs[3, 2] = "development/default.aspx";
        tabs[4, 0] = "&nbsp;" + ResHelper.GetString("Header.Licenses") + "&nbsp;";
        tabs[4, 2] = "licenses/License_List.aspx";


        BasicTabControlHeader.Tabs = tabs;

        string section = ValidationHelper.GetString(Request.QueryString["section"], "sites").ToLower();
        int selectedTab = 0;
        switch (section)
        {
            case "sites":
                selectedTab = 0;
                break;

            case "administration":
                selectedTab = 1;
                break;

            case "settings":
                selectedTab = 2;
                break;

            case "development":
                selectedTab = 3;
                break;

            case "licenses":
                selectedTab = 4;
                break;

            default:
                selectedTab = 0;
                break;
        }

        BasicTabControlHeader.SelectedTab = selectedTab;
        BasicTabControlHeader.UrlTarget = "cmsdesktop";

        lnkSignOut.OnClientClick = FacebookConnectHelper.FacebookConnectInitForSignOut(CMSContext.CurrentSiteName, ltlFBConnectScript);
    }


    protected void btnSignOut_Click(object sender, EventArgs e)
    {
        SignOut();
        ltlScript.Text += ScriptHelper.GetScript("parent.location.replace('" + Request.ApplicationPath.TrimEnd('/') + "/default.aspx');");
    }
}
