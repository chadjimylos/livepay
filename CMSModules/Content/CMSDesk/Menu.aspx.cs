using System;
using System.Text;

using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSModules_Content_CMSDesk_Menu : CMSContentPage
{
    #region "Protected variables"

    protected int selectedNodeId = 0;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the dialog script
        ScriptHelper.RegisterDialogScript(this);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ContentMenuScripts", ScriptHelper.GetScript("var contentDir = '" + ResolveUrl("~/CMSModules/Content/CMSDesk/") + "';"));

        ltlData.Text += "<input type=\"hidden\" id=\"imagesUrl\" value=\"" + GetImageUrl("CMSModules/CMS_Content/Menu/", false, true) + "\" />";

        // Check (and ensure) the proper content culture
        CheckPreferredCulture(true);

        // Setup selected node ID
        selectedNodeId = QueryHelper.GetInteger("nodeid", 0);

        // Initialize menu
        contentMenu.Groups = new string[,] { { ResHelper.GetString("ContentMenu.ContentManagement"), "~/CMSAdminControls/UI/UniMenu/Content/ContentMenu.ascx", "ContentMenuGroup" }, { ResHelper.GetString("ContentMenu.ViewMode"), "~/CMSAdminControls/UI/UniMenu/Content/ModeMenu.ascx", "ContentMenuGroup" }, { ResHelper.GetString("contentmenu.language"), "~/CMSModules/Content/Controls/LanguageMenu.ascx", "ContentMenuGroup" }, { ResHelper.GetString("contentmenu.other"), "~/CMSAdminControls/UI/UniMenu/Content/OtherMenu.ascx", "ContentMenuGroup" } };

        ScriptHelper.RegisterTitleScript(this, ResHelper.GetString("cmsdesk.ui.content"));
    }

    #endregion
}
