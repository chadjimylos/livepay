CREATE TABLE [COM_CurrencyExchangeRate] (
		[ExchagneRateID]               int NOT NULL IDENTITY(1, 1),
		[ExchangeRateToCurrencyID]     int NOT NULL,
		[ExchangeRateValue]            float NOT NULL,
		[ExchangeTableID]              int NOT NULL,
		[ExchangeRateGUID]             uniqueidentifier NOT NULL,
		[ExchangeRateLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_CurrencyExchangeRate]
	ADD
	CONSTRAINT [PK_COM_CurrencyExchangeRate]
	PRIMARY KEY
	([ExchagneRateID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_CurrencyExchangeRate]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Currency]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_CurrencyExchangeRate_ExchangeRateToCurrencyID_COM_Currency]') IS NULL
BEGIN
		ALTER TABLE [COM_CurrencyExchangeRate]
			ADD CONSTRAINT [FK_COM_CurrencyExchangeRate_ExchangeRateToCurrencyID_COM_Currency]
			FOREIGN KEY ([ExchangeRateToCurrencyID]) REFERENCES [COM_Currency] ([CurrencyID])
END
IF OBJECT_ID(N'[COM_CurrencyExchangeRate]') IS NOT NULL
	AND OBJECT_ID(N'[COM_ExchangeTable]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_CurrencyExchangeRate_ExchangeTableID_COM_ExchangeTable]') IS NULL
BEGIN
		ALTER TABLE [COM_CurrencyExchangeRate]
			ADD CONSTRAINT [FK_COM_CurrencyExchangeRate_ExchangeTableID_COM_ExchangeTable]
			FOREIGN KEY ([ExchangeTableID]) REFERENCES [COM_ExchangeTable] ([ExchangeTableID])
END
CREATE INDEX [IX_COM_CurrencyExchangeRate_ExchangeRateToCurrencyID]
	ON [COM_CurrencyExchangeRate] ([ExchangeRateToCurrencyID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_CurrencyExchangeRate_ExchangeTableID]
	ON [COM_CurrencyExchangeRate] ([ExchangeTableID])
	ON [PRIMARY]
