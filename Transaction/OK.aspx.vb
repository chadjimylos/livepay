Imports System.Data
Imports System.Data.SqlClient
Public Class OK
    Inherits Web.UI.Page

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Ref As String = Request("ref")

        UpdateMerchantRefStatus(Ref)

        'SendConfirmationEmail(orderID)
    End Sub

    Sub UpdateMerchantRefStatus(ByVal MerchantRef As String)
        Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()

        Dim objConnection As New SqlConnection(strConnection)
        objConnection.Open()

        Dim objCommand As New SqlCommand("AegeanPower_UpdateMerchantRefStatus", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@MerchantRef", MerchantRef)
        objCommand.Parameters.AddWithValue("@Successful", 1)

        objCommand.ExecuteNonQuery()
        objConnection.Close()
    End Sub
End Class
