using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Reporting;
using CMS.EventLog;
using CMS.CMSHelper;
using CMS.DataEngine;

public partial class CMSModules_Reporting_Controls_ReportValue : AbstractReportControl
{
    private string mReportValueName = "";

    /// <summary>
    /// Value name
    /// </summary>
    public string ReportValueName
    {
        get
        {
            return mReportValueName;
        }
        set
        {
            mReportValueName = value;
        }
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData(bool forceLoad)
    {
        try
        {
            // Set report value name
            this.ReportValueName = this.Parameter;

            // Load value info object
            ReportValueInfo rvi = ReportValueInfoProvider.GetReportValueInfo(this.ReportValueName);
            if (rvi != null)
            {
                this.QueryIsStoredProcedure = rvi.ValueQueryIsStoredProcedure;
                this.QueryText = CMSContext.CurrentResolver.ResolveMacros(rvi.ValueQuery);
            }

            // Only use base parameters in case of stored procedure
            if (this.QueryIsStoredProcedure)
            {
                this.AllParameters = SpecialFunctions.ConvertDataRowToParams(this.ReportParameters, null);
            }

            // Load data
            DataSet ds = this.LoadData();

            // If datasource is emptry, create empty dataset
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                // Set literal text
                string value = rvi.ValueFormatString;
                if ((value == null) || (value == ""))
                {
                    value = ValidationHelper.GetString(ds.Tables[0].Rows[0][0], "");
                }
                else
                {
                    value = string.Format(value, ds.Tables[0].Rows[0].ItemArray);
                }

                lblValue.Text = HTMLHelper.HTMLEncode(ResolveMacros(value));
            }
        }
        catch (Exception ex)
        {
            // Display error message, if data load fail
            lblError.Visible = true;
            lblError.Text = "[ReportValue.ascx] Error loading the data: " + ex.Message;
            EventLogProvider ev = new EventLogProvider();
            ev.LogEvent("Report value", "E", ex);
        }
    }
}
