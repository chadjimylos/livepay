<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlternativeFormFieldEditor.ascx.cs"
    Inherits="CMSModules_AdminControls_Controls_Class_AlternativeFormFieldEditor" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/FieldEditor.ascx" TagName="FieldEditor" TagPrefix="cms" %>    

<asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" />
<cms:FieldEditor ID="fieldEditor" runat="server" />
