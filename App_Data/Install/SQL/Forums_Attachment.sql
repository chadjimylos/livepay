CREATE TABLE [Forums_Attachment] (
		[AttachmentID]                int NOT NULL IDENTITY(1, 1),
		[AttachmentFileName]          nvarchar(200) NOT NULL,
		[AttachmentFileExtension]     nvarchar(10) NOT NULL,
		[AttachmentBinary]            varbinary(max) NULL,
		[AttachmentGUID]              uniqueidentifier NOT NULL,
		[AttachmentLastModified]      datetime NOT NULL,
		[AttachmentMimeType]          nvarchar(100) NOT NULL,
		[AttachmentFileSize]          int NOT NULL,
		[AttachmentImageHeight]       int NULL,
		[AttachmentImageWidth]        int NULL,
		[AttachmentPostID]            int NOT NULL,
		[AttachmentSiteID]            int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Forums_Attachment]
	ADD
	CONSTRAINT [PK_Forums_Attachment]
	PRIMARY KEY
	([AttachmentID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Forums_Attachment]
	ADD
	CONSTRAINT [DEFAULT_Forums_Attachment_AttachmentPostID]
	DEFAULT ((0)) FOR [AttachmentPostID]
IF OBJECT_ID(N'[Forums_Attachment]') IS NOT NULL
	AND OBJECT_ID(N'[Forums_ForumPost]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_Attachment_AttachmentPostID_Forums_ForumPost]') IS NULL
BEGIN
		ALTER TABLE [Forums_Attachment]
			ADD CONSTRAINT [FK_Forums_Attachment_AttachmentPostID_Forums_ForumPost]
			FOREIGN KEY ([AttachmentPostID]) REFERENCES [Forums_ForumPost] ([PostId])
END
IF OBJECT_ID(N'[Forums_Attachment]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_Attachment_AttachmentSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Forums_Attachment]
			ADD CONSTRAINT [FK_Forums_Attachment_AttachmentSiteID_CMS_Site]
			FOREIGN KEY ([AttachmentSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE UNIQUE INDEX [IX_Forums_Attachment_AttachmentGUID]
	ON [Forums_Attachment] ([AttachmentSiteID], [AttachmentGUID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Forums_Attachment_AttachmentPostID]
	ON [Forums_Attachment] ([AttachmentPostID])
	ON [PRIMARY]
