using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.IDataConnectionLibrary;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_Query_Edit : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string queryName = ResHelper.GetString("DocumentType_Edit_Query_Edit.NewQueryName");

        // Gets classID from querystring, used when creating new query
        int classId = QueryHelper.GetInteger("documenttypeid", 0);
        queryEdit.ClassID = classId;

        // Page and queryparam for refresh
        queryEdit.RefreshPageURL = "DocumentType_Edit_Query_Edit.aspx";
        queryEdit.QueryParamID = "?queryid";

        // Gets queryID from querystring
        int queryId = QueryHelper.GetInteger("queryid", 0);
        if (queryId > 0)
        {
            queryEdit.QueryID = queryId;
            Query query = QueryProvider.GetQuery(queryId);

            if (query != null)
            {
                // Get information on actual query for URLs
                classId = query.QueryClassId;
                queryName = query.QueryName;
            }
        }

        this.queryEdit.RefreshPageURL = "~/CMSSiteManager/Development/DocumentTypes/DocumentType_Edit_Query_Edit.aspx";

        // Initializes PageTitle
        string queries = ResHelper.GetString("DocumentType_Edit_Query_Edit.Queries");

        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = queries;
        pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/DocumentTypes/DocumentType_Edit_Query_List.aspx?documenttypeid=" + classId.ToString();
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = queryName;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }
}
