<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Container_List.aspx.cs" Inherits="CMSModules_PortalEngine_UI_WebContainers_Container_List"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Theme="Default" Title="Web part container" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Container_List.xml" OrderBy="ContainerDisplayName" />
</asp:Content>
