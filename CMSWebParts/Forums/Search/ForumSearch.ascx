<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Forums/Search/ForumSearch.ascx.cs"
    Inherits="CMSWebParts_Forums_Search_ForumSearch" %>
<asp:Panel ID="pnlForumSearch" runat="server" DefaultButton="btnGo">
    <cms:LocalizedLabel ID="lblSearch" runat="server" AssociatedControlID="txtSearch" EnableViewState="false" />
    <asp:TextBox ID="txtSearch" runat="server" />
    <cms:CMSButton ID="btnGo" runat="server" OnClick="btnGo_Click" CssClass="XShortButton" EnableViewState="false" /><br />
    <asp:HyperLink runat="server" ID="lnkAdvanceSearch" Visible="false" EnableViewState="false" />
</asp:Panel>
