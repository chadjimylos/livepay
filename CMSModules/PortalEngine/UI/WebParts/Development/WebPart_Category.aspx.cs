using System;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_WebParts_Development_WebPart_Category : SiteManagerPage
{
    protected int categoryId;
    protected int parentCategoryId;

    string[,] pageTitleTabs = new string[2, 3];

    protected void Page_Load(object sender, EventArgs e)
    {
        // initialize
        lblCategoryName.Text = ResHelper.GetString("General.CategoryName");
        lblCategoryDisplayName.Text = ResHelper.GetString("General.CategoryDisplayName");
        btnOk.Text = ResHelper.GetString("general.ok");
        rfvCategoryName.ErrorMessage = ResHelper.GetString("General.RequiresCodeName");
        rfvCategoryDisplayName.ErrorMessage = ResHelper.GetString("General.RequiresDisplayName");

        pageTitleTabs[0, 0] = ResHelper.GetString("Development-WebPart_Category.Category");
        pageTitleTabs[0, 1] = ResolveUrl("WebPart_Frameset.aspx");
        pageTitleTabs[0, 2] = "_parent";
        pageTitleTabs[1, 0] = ResHelper.GetString("Development-WebPart_Category.New");
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";        

        // Get category id
        categoryId = ValidationHelper.GetInteger(Request.QueryString["categoryID"], 0);
        parentCategoryId = ValidationHelper.GetInteger(Request.QueryString["parentID"], 0);

        string categoryName = "";
        string categoryDisplayName = "";
        string categoryImagePath = "" ;
        string pageTitleText = "";
        string pageTitleImage = "";

            if (categoryId > 0)
            {
                WebPartCategoryInfo ci = WebPartCategoryInfoProvider.GetWebPartCategoryInfoById(categoryId);
                if (ci != null)
                {
                    categoryName = ci.CategoryName;
                    categoryDisplayName = ci.CategoryDisplayName;
                    categoryImagePath = ci.CategoryImagePath ;
                    parentCategoryId = ci.CategoryParentID;

                    // If it's root category hide category name textbox
                    if (parentCategoryId == 0)
                    {
                        plcCategoryName.Visible = false;
                    }

                    pageTitleTabs[1, 0] = ci.CategoryDisplayName;
                    pageTitleText = ResHelper.GetString("Development-WebPart_Category.Title");
                    pageTitleImage = GetImageUrl("Objects/CMS_WebPartCategory/object.png");                                  
                }
            }
            else
            {
                pageTitleText = ResHelper.GetString("Development-WebPart_Category.TitleNew");
                pageTitleImage = GetImageUrl("Objects/CMS_WebPartCategory/new.png");
            }

        if (!RequestHelper.IsPostBack())
        {
            txtCategoryDisplayName.Text = categoryDisplayName;
            txtCategoryName.Text = categoryName;
            txtCategoryImagePath.Text = categoryImagePath;
        }

        // Initialize the master page
        InitializeMasterPage(pageTitleText, pageTitleImage, pageTitleTabs);
    }


    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    private void InitializeMasterPage(string pageTitleText, string pageTitleImage, string[,] pageTitleTabs)
    {
        this.CurrentMaster.Title.TitleText = pageTitleText;
        this.CurrentMaster.Title.TitleImage = pageTitleImage;
        this.CurrentMaster.Title.HelpTopicName = "new_categoryproperties";
        this.CurrentMaster.Title.HelpName = "helpTopic";        

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// Creates new or updates category name
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        string displayName = txtCategoryDisplayName.Text;
        string codeName = txtCategoryName.Text;
        string imagePath = txtCategoryImagePath.Text;

        string result = new Validator().NotEmpty(displayName, ResHelper.GetString("General.RequiresDisplayName")).NotEmpty(codeName, ResHelper.GetString("General.RequiresCodeName")).Result;

        // If it's root category don't validate codename
        if (parentCategoryId != 0)
        {
            // Validate the codename
            if (!ValidationHelper.IsCodeName(codeName))
            {
                result = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
            }
        }

        // Check codename uniqness
        if ((categoryId == 0) && (WebPartCategoryInfoProvider.GetWebPartCategoryInfoByCodeName(codeName) != null))
        {
            result = ResHelper.GetString("General.CodeNameExists");
        }

        if (result == "")
        {
            WebPartCategoryInfo ci;

            ltlScript.Text = "<script type=\"text/javascript\">";
            lblInfo.Visible = true;
            if (categoryId > 0)
            {
                ci = WebPartCategoryInfoProvider.GetWebPartCategoryInfoById(categoryId);
                ci.CategoryDisplayName = displayName;                
                ci.CategoryName = codeName;                
                ci.CategoryImagePath = imagePath;

                try
                {
                    WebPartCategoryInfoProvider.SetWebPartCategoryInfo(ci);
                    ltlScript.Text += "parent.frames['webparttree'].location.href = 'WebPart_Tree.aspx?categoryid=" + Request.QueryString["categoryID"] + "';";
                }
                catch(Exception ex)
                {
                    lblInfo.Visible = false;
                    lblError.Visible = true;
                    lblError.Text = ex.Message.Replace("%%name%%", displayName);
                }
            }
            else
            {
                ci = new WebPartCategoryInfo();
                ci.CategoryDisplayName = displayName;
                ci.CategoryName = codeName;
                ci.CategoryParentID = parentCategoryId;
                ci.CategoryImagePath = imagePath;

                try
                {
                    WebPartCategoryInfoProvider.SetWebPartCategoryInfo(ci);
                    ltlScript.Text += "parent.frames['webparttree'].location.href = 'WebPart_Tree.aspx?categoryid=" + ci.CategoryID + "';";
                    ltlScript.Text += "this.location.href = 'WebPart_Category.aspx?categoryid=" + ci.CategoryID + "';";
                }
                catch(Exception ex)
                {
                    lblInfo.Visible = false;
                    lblError.Visible = true;
                    lblError.Text = ex.Message.Replace("%%name%%", displayName);
                }
            }

            ltlScript.Text += "</script>";
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            pageTitleTabs[1, 0] = ci.CategoryDisplayName;
        }
        else
        {
            lblInfo.Visible = false;
            lblError.Visible = true;
            lblError.Text = result;
        }
    }
}
