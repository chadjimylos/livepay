<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Board_Edit_Security.aspx.cs"
    Inherits="CMSModules_MessageBoards_Tools_Boards_Board_Edit_Security" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Theme="default" Title="Message board - security" %>

<%@ Register Src="~/CMSModules/MessageBoards/Controls/Boards/BoardSecurity.ascx" TagName="BoardSecurity" TagPrefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:BoardSecurity ID="boardSecurity" runat="server" />
</asp:Content>
