using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_History : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int orderId = ValidationHelper.GetInteger(Request.QueryString["orderid"], 0);

        this.gridElem.DataSource = OrderStatusUserInfoProvider.GetOrderStatusList(orderId);
        this.gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        this.gridElem.GridView.RowDataBound += new GridViewRowEventHandler(gridElem_RowDataBound);
    }


    protected void gridElem_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int orderStatId = ValidationHelper.GetInteger(((DataRowView)(e.Row.DataItem)).Row["ToStatusID"], 0);

            OrderStatusInfo osi = OrderStatusInfoProvider.GetOrderStatusInfo(orderStatId);
            if (osi != null)
            {
                e.Row.Style.Add("background-color", osi.StatusColor);
            }
        }
    }


    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        // Localize status display name
        if (sourceName.ToLower() == "tostatusdisplayname")
        {
            return HTMLHelper.HTMLEncode(ResHelper.LocalizeString(Convert.ToString(parameter)));
        }
        else if (sourceName.ToLower() == "formattedusername")
        {
            return HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(Convert.ToString(parameter)));
        }

        return HTMLHelper.HTMLEncode(parameter.ToString());
    }
}
