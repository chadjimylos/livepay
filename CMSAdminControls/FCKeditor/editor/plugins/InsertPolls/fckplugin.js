// Register the related command.
FCKCommands.RegisterCommand( 'InsertPolls', new FCKDialogCommand( 'InsertPolls', FCKLang.InsertPollsTitle, FCKPlugins.Items['InsertPolls'].Path + '../../../../../CMSModules/Polls/Controls/FCKEditor/fck_InsertPolls.aspx?' + FCKConfig.DialogParameters, 600, 420 ) ) ;

// Create the "Placeholder" toolbar button.
var oInsertPolls = new FCKToolbarButton( 'InsertPolls', FCKLang.InsertPollsButton ) ;
oInsertPolls.IconPath = FCKPlugins.Items['InsertPolls'].Path + 'fckPolls.gif' ;
FCKToolbarItems.RegisterItem( 'InsertPolls', oInsertPolls ) ;
