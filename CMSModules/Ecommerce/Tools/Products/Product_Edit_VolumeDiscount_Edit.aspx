<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_Edit_VolumeDiscount_Edit.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Ecommerce_Tools_Products_Product_Edit_VolumeDiscount_Edit"
    Theme="Default" Title="Product edit - volume discount edit" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel" style="height: 32px">
                <asp:Label runat="server" ID="lblVolumeDiscountMinCount" EnableViewState="false" />
            </td>
            <td style="height: 32px">
                <asp:TextBox ID="txtVolumeDiscountMinCount" runat="server" CssClass="TextBoxField"
                    MaxLength="9" EnableViewState="false" />
                &nbsp;&nbsp; <span runat="server" id="spanMinCount">
                    <asp:RequiredFieldValidator ID="rfvMinCount" runat="server" ControlToValidate="txtVolumeDiscountMinCount"
                        Display="Dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvMinCount" runat="server" ControlToValidate="txtVolumeDiscountMinCount"
                        MinimumValue="1" Type="Integer" Display="Dynamic" MaximumValue="999999999" EnableViewState="false"></asp:RangeValidator>
                </span>
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblVolumeDiscountValue" EnableViewState="false" />
            </td>
            <td>
                <asp:RadioButton ID="radDiscountRelative" runat="server" GroupName="group1" EnableViewState="false" />
                <asp:RadioButton ID="radDiscountAbsolute" runat="server" GroupName="group1" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
            </td>
            <td>
                <asp:TextBox ID="txtVolumeDiscountValue" runat="server" CssClass="TextBoxField" MaxLength="10"
                    EnableViewState="false" />
                &nbsp;&nbsp; <span runat="server" id="spanValue">
                    <asp:RequiredFieldValidator ID="rfvDiscountValue" runat="server" ControlToValidate="txtVolumeDiscountValue"
                        Display="Dynamic" EnableViewState="false"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvDiscountValue" runat="server" ControlToValidate="txtVolumeDiscountValue"
                        Type="Double" Display="Dynamic" MaximumValue="9999999999" MinimumValue="0" EnableViewState="false"></asp:RangeValidator>
                </span>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
</asp:Content>
