<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletter_Issue_Send.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_Send" Theme="Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Tools - Newsletter issue send</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>
    <base target="_self" />

    <script type="text/javascript">
        //<![CDATA[
        var wopener = parent.wopener;

        function RefreshPage() {
            wopener.RefreshPage();
        }
        //]]>
    </script>

</head>
<body class="<%=mBodyClass%>" onunload="RefreshPage();">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="manScript" runat="server" />
    <asp:Panel runat="server" ID="pnlBody" CssClass="TabsPageBody">
        <asp:Panel runat="server" ID="pnlContainer" CssClass="TabsPageContainer">
            <asp:Panel runat="server" ID="pnlScroll" CssClass="TabsPageScrollArea2">
                <asp:Panel runat="server" ID="pnlTab" CssClass="TabsPageContent">
                    <asp:Panel runat="server" ID="pnlContent" CssClass="PageContent" DefaultButton="btnSend">
                        <asp:Label ID="lblError" runat="server" EnableViewState="false" Visible="false" CssClass="ErrorLabel" />
                        <asp:Label ID="lblSent" runat="server" EnableViewState="false" /><br />
                        <br />
                        <table>
                            <tr>
                                <td>
                                    <cms:LocalizedRadioButton ID="radSendNow" runat="server" GroupName="Send" AutoPostBack="true"
                                        OnCheckedChanged="radGroupSend_CheckedChanged" ResourceString="Newsletter_Issue_Send.SendNow" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cms:LocalizedRadioButton ID="radSchedule" runat="server" GroupName="Send" AutoPostBack="true"
                                        ResourceString="Newsletter_Issue_Send.Schedule" OnCheckedChanged="radGroupSend_CheckedChanged" />
                                    <div class="UnderRadioContent">
                                        <table>
                                            <tr>
                                                <td>
                                                    <cms:LocalizedLabel ID="lblDateTime" runat="server" AssociatedControlID="calendarControl"
                                                        ResourceString="Newsletter_Issue_Send.DateTime" />
                                                </td>
                                                <td>
                                                    <cms:DateTimePicker ID="calendarControl" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <cms:LocalizedRadioButton ID="radSendToSpecified" runat="server" GroupName="Send"
                                        AutoPostBack="true" ResourceString="Newsletter_Issue_Send.SendToSpecified" OnCheckedChanged="radGroupSend_CheckedChanged" />
                                    <div class="UnderRadioContent">
                                        <table>
                                            <tr>
                                                <td>
                                                    <cms:LocalizedLabel ID="lblEmail" runat="server" Text="Label" EnableViewState="false"
                                                        ResourceString="Newsletter_Issue_Send.Email" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSendToSpecified" runat="server" Enabled="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="UnderRadioContent">
                                        <cms:LocalizedButton ID="btnSend" runat="server" CssClass="SubmitButton" OnClick="btnSend_Click"
                                            ResourceString="Newsletter_Issue_Send.Send" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
