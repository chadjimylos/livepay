CREATE TABLE [CMS_Transformation] (
		[TransformationID]                        int NOT NULL IDENTITY(1, 1),
		[TransformationName]                      nvarchar(100) NOT NULL,
		[TransformationCode]                      nvarchar(max) NOT NULL,
		[TransformationType]                      nvarchar(50) NOT NULL,
		[TransformationClassID]                   int NOT NULL,
		[TransformationCheckedOutByUserID]        int NULL,
		[TransformationCheckedOutMachineName]     nvarchar(100) NULL,
		[TransformationCheckedOutFilename]        nvarchar(450) NULL,
		[TransformationVersionGUID]               nvarchar(50) NULL,
		[TransformationGUID]                      uniqueidentifier NOT NULL,
		[TransformationLastModified]              datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Transformation]
	ADD
	CONSTRAINT [PK_CMS_Transformation]
	PRIMARY KEY
	NONCLUSTERED
	([TransformationID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_Transformation]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Transformation_TransformationClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_Transformation]
			ADD CONSTRAINT [FK_CMS_Transformation_TransformationClassID_CMS_Class]
			FOREIGN KEY ([TransformationClassID]) REFERENCES [CMS_Class] ([ClassID])
END
IF OBJECT_ID(N'[CMS_Transformation]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Transformation_TransformationCheckedOutByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_Transformation]
			ADD CONSTRAINT [FK_CMS_Transformation_TransformationCheckedOutByUserID_CMS_User]
			FOREIGN KEY ([TransformationCheckedOutByUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE CLUSTERED INDEX [IX_CMS_Transformation_TransformationClassID_TransformationName]
	ON [CMS_Transformation] ([TransformationClassID], [TransformationName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Transformation_TransformationCheckedOutByUserID]
	ON [CMS_Transformation] ([TransformationCheckedOutByUserID])
	ON [PRIMARY]
