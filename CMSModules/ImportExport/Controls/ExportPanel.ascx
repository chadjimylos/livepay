<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExportPanel.ascx.cs" Inherits="CMSModules_ImportExport_Controls_ExportPanel" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/Trees/ObjectTree.ascx" TagName="ObjectTree" TagPrefix="cms" %>
<%@ Register Src="ExportGridView.ascx" TagName="ExportGridView" TagPrefix="cms" %>

<script type="text/javascript">
    //<![CDATA[

    function setDivBorder() {
        if (rtl == 'True') {
            document.getElementById("exTreeDiv").style.borderLeft = 'solid 1px #dddddd';
        }
        else {
            document.getElementById("exTreeDiv").style.borderRight = 'solid 1px #dddddd';
        }
    }

    function exSetDivPosition() {
        var intY = document.getElementById("exTreeDiv").scrollTop;
        document.getElementById("hdnexDivScrollBar").value = intY;
    }

    function exGetDivPosition() {
        var intY = document.getElementById('hdnexDivScrollBar').value;
        document.getElementById("exTreeDiv").scrollTop = intY;
    }

    // Hook onload handler
    if (window.onload != null) {
        var oldOnload = window.onload;
        window.onload = function(e) { oldOnload(e); exGetDivPosition(); setDivBorder(); }
    }
    else {
        window.onload = function(e) { exGetDivPosition(); setDivBorder(); }
    }
    //]]>
</script>

<asp:Literal ID="ltlDefaultValues" runat="server" />
<asp:Panel ID="pnlGrid" runat="Server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 250px; vertical-align: top;">
                <div id="exTreeDiv" style="width: 250px; height: 387px; padding: 5px; overflow: auto;"
                    onclick="exSetDivPosition();" onunload="exSetDivPosition();">
                    <cms:ObjectTree ID="objectTree" runat="server" UsePostback="true" UseImages="true"
                        EnableViewState="true" />
                </div>
                <input type="hidden" id="hdnexDivScrollBar" name="hdnexDivScrollBar" value="<%= mScrollPosition %>" />
            </td>
            <td style="vertical-align: top;">
                <div style="height: 387px; padding: 5px 15px 5px 15px; overflow: auto;">
                    <div style="width: 97%;">
                        <div class="Wizard">
                            <cms:PageTitle ID="titleElem" runat="server" EnableViewState="false" /></div>
                        <asp:Panel runat="server" ID="pnlError" CssClass="WizardHeaderLine" Visible="false"
                            BackColor="transparent">
                            <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" />
                        </asp:Panel>
                        <asp:PlaceHolder ID="plcPanel" runat="server">
                            <asp:PlaceHolder ID="plcControl" runat="Server" />
                            <br />
                            <cms:ExportGridView ID="gvObjects" runat="server" />
                        </asp:PlaceHolder>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
