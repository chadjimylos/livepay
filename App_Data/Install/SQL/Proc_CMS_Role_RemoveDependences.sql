CREATE PROCEDURE [Proc_CMS_Role_RemoveDependences]
    @RoleID int
AS
BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
    SET NOCOUNT ON;
    
    BEGIN TRANSACTION;
    
    DELETE FROM CMS_UserRole WHERE RoleID=@RoleID;
    DELETE FROM CMS_RolePermission WHERE RoleID=@RoleID; 
    DELETE FROM CMS_ACLItem WHERE RoleID=@RoleID;
    DELETE FROM CMS_WorkflowStepRoles WHERE RoleID=@RoleID; 
    DELETE FROM Forums_ForumRoles WHERE RoleID = @RoleID;
    DELETE FROM Polls_PollRoles WHERE RoleID = @RoleID;
    DELETE FROM Media_LibraryRolePermission WHERE RoleID = @RoleID;
    DELETE FROM Community_GroupRolePermission WHERE RoleID = @RoleID;
    DELETE FROM CMS_WidgetRole WHERE RoleID = @RoleID;
    -- Message boards
    DELETE FROM Board_Role WHERE RoleID = @RoleID;
    -- UIProfiles
    DELETE FROM CMS_RoleUIElement WHERE RoleID = @RoleID;
    COMMIT TRANSACTION;
END
