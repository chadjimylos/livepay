using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_Edit_Billing : CMSEcommercePage
{
    protected int orderId = 0;
    protected int customerId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AddressChange", ScriptHelper.GetScript("   function AddressChange(AddressId) { if (AddressId > 0) { document.getElementById('" + hdnAddress.ClientID + "').value = AddressId; " + ClientScript.GetPostBackEventReference(btnChange, "") + " } } "));

        orderId = ValidationHelper.GetInteger(Request.QueryString["orderId"], 0);

        lblCurrency.Text = ResHelper.GetString("OrderBilling.Currency");
        lblPayment.Text = ResHelper.GetString("OrderBilling.Payment");
        lblBillingAddress.Text = ResHelper.GetString("OrderBilling.Address");
        lblPaymentResult.Text = ResHelper.GetString("OrderBilling.lblPaymentResult");

        btnEdit.Text = ResHelper.GetString("general.edit");
        btnNew.Text = ResHelper.GetString("general.new");
        btnOk.Text = ResHelper.GetString("general.ok");

        addressElem.DropDownSingleSelect.AutoPostBack = true;
        addressElem.DropDownSingleSelect.SelectedIndexChanged += new EventHandler(DropDownSingleSelect_SelectedIndexChanged);

        LoadData();

        btnNew.OnClientClick = "AddAddress('" + customerId + "'); return false;";
        btnEdit.OnClientClick = "EditAddress('" + customerId + "','" + addressElem.AddressID + "'); return false;";
    }


    protected void DropDownSingleSelect_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdnAddress.Value = addressElem.AddressID.ToString();
    }


    protected void LoadData()
    {
        OrderInfo oi = OrderInfoProvider.GetOrderInfo(orderId);
        if (oi != null)
        {
            customerId = oi.OrderCustomerID;
            string paymentResultValue = oi.OrderPaymentResult.GetFormattedPaymentResultString();

            if (paymentResultValue.Trim() == String.Empty)
            {
                paymentResultValue = ResHelper.GetString("general.na");
            }

            lblPaymentResultValue.Text = paymentResultValue;


            drpPayment.ShippingOptionID = oi.OrderShippingOptionID;
            drpPayment.AddNoneRecord = true;

            addressElem.CustomerID = customerId;

            if (!UrlHelper.IsPostback())
            {
                drpPayment.PaymentID = oi.OrderPaymentOptionID;
                drpCurrency.CurrencyID = oi.OrderCurrencyID;
                addressElem.AddressID = oi.OrderBillingAddressID;
            }
        }
    }


    protected void btnOk_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        // Check whether some address is selected
        int addressId = addressElem.AddressID;
        if (addressId <= 0)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Order_Edit_Billing.NoAddress");
            return;
        }

        OrderInfo oi = OrderInfoProvider.GetOrderInfo(orderId);

        if (oi != null)
        {
            oi.OrderBillingAddressID = addressId;
            oi.OrderCurrencyID = drpCurrency.CurrencyID;
            oi.OrderPaymentOptionID = drpPayment.PaymentID;

            // Load the shopping cart to process the data
            ShoppingCartInfo sci = ShoppingCartInfoProvider.GetShoppingCartInfoFromOrder(orderId);
            if (sci != null)
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(oi.OrderSiteID);
                if (si != null)
                {
                    sci.SiteName = si.SiteName;
                }
                else
                {
                    sci.SiteName = CMSContext.CurrentSiteName;
                }
                sci.ExchangeRate = CurrencyInfoProvider.GetLastValidExchangeRate(oi.OrderCurrencyID);
                sci.ShoppingCartCurrencyID = oi.OrderCurrencyID;
                ShoppingCartInfoProvider.EvaluateShoppingCartContent(sci);

                // Update shipping charge, total price, total tax
                oi.OrderTotalPrice = sci.TotalPrice;
                oi.OrderTotalShipping = sci.TotalShipping;
                oi.OrderTotalTax = sci.TotalTax;

                // Delete shopping cart info (including all items) from database
                ShoppingCartInfoProvider.DeleteShoppingCartInfo(sci.ShoppingCartID);
            }

            OrderInfoProvider.SetOrderInfo(oi);

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }
    }
}
