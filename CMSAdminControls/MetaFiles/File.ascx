<%@ Control Language="C#" AutoEventWireup="true" CodeFile="File.ascx.cs" Inherits="CMSAdminControls_MetaFiles_File" %>
<div class="Uploader">
    <cms:Uploader ID="uploader" runat="server" BorderStyle="none" RequireDeleteConfirmation="true" />
    <asp:Label ID="lblError" runat="server" Visible="false" CssClass="ErrorLabel" EnableViewState="false" />
</div>
