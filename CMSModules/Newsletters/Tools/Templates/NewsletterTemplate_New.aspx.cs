using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Templates_NewsletterTemplate_New : CMSNewsletterPage
{
    protected int siteId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        siteId = CMS.CMSHelper.CMSContext.CurrentSite.SiteID;

        // Set title
        this.CurrentMaster.Title.HelpTopicName = "new_newsletter2";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Check 'Manage templates' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "managetemplates"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "managetemplates");
        }

        siteId = ValidationHelper.GetInteger(Request.QueryString["siteid"], 0);

        rfvTemplateDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvTemplateName.ErrorMessage = ResHelper.GetString("NewsletterTemplate_Edit.ErrorEmptyName");
        lblTemplateType.Text = ResHelper.GetString("NewsletterTemplate_Edit.TemplateType");
        btnOk.Text = ResHelper.GetString("General.OK");

        string currentEmailTemplate = ResHelper.GetString("NewsletterTemplate_Edit.NewItemCaption");

        // Initializes page title control		
        string[,] tabs = new string[2, 3];
        tabs[0, 0] = ResHelper.GetString("NewsletterTemplate_Edit.ItemListLink");
        tabs[0, 1] = "~/CMSModules/Newsletters/Tools/Templates/NewsletterTemplate_List.aspx?siteid=" + siteId.ToString();
        tabs[0, 2] = "newslettersContent";
        tabs[1, 0] = currentEmailTemplate;
        tabs[1, 1] = "";
        tabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = tabs;

        if (!RequestHelper.IsPostBack())
        {
            // Fill drop down list with newsletter types
            ListItem newItem = new ListItem(ResHelper.GetString("NewsletterTemplate_Edit.DrpIssue"), "0");
            drpTemplateType.Items.Add(newItem);
            newItem = new ListItem(ResHelper.GetString("NewsletterTemplate_Edit.DrpSubscription"), "1");
            drpTemplateType.Items.Add(newItem);
            newItem = new ListItem(ResHelper.GetString("NewsletterTemplate_Edit.DrpUnsubscription"), "2");
            drpTemplateType.Items.Add(newItem);
            drpTemplateType.SelectedIndex = 0;
        }
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        //// Check 'Manage templates' permission
        //if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "managetemplates"))
        //{
        //    RedirectToAccessDenied("cms.newsletter", "managetemplates");
        //}

        string errorMessage = new Validator().NotEmpty(txtTemplateDisplayName.Text, ResHelper.GetString("general.requiresdisplayname")).NotEmpty(txtTemplateName.Text, ResHelper.GetString("NewsletterTemplate_Edit.ErrorEmptyName")).Result;

        if (!ValidationHelper.IsCodeName(txtTemplateName.Text))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

        if (errorMessage == "")
        {
            // TemplateName must to be unique
            EmailTemplate emailTemplateObj = EmailTemplateProvider.GetEmailTemplate(txtTemplateName.Text.Trim(), siteId);

            // If templateName value is unique														
            if (emailTemplateObj == null)
            {
                string type;
                int typeValue = Convert.ToInt32(drpTemplateType.SelectedValue);
                switch (typeValue)
                {
                    case 1:
                        type = "S"; // Subscription
                        break;

                    case 2:
                        type = "U"; // Unsubscription
                        break;
                    default:
                        type = "I"; // Issue
                        break;
                }

                // Create new item -> insert
                emailTemplateObj = new EmailTemplate();
                emailTemplateObj.TemplateType = type;
                emailTemplateObj.TemplateBody = "";
                emailTemplateObj.TemplateName = txtTemplateName.Text.Trim();
                emailTemplateObj.TemplateHeader = "<html>\n<head>\n</head>\n<body>";
                emailTemplateObj.TemplateFooter = "</body>\n</html>";
                emailTemplateObj.TemplateDisplayName = txtTemplateDisplayName.Text.Trim();
                emailTemplateObj.TemplateSiteID = siteId;

                EmailTemplateProvider.SetEmailTemplate(emailTemplateObj);

                UrlHelper.Redirect("NewsletterTemplate_Edit.aspx?templateid=" + Convert.ToString(emailTemplateObj.TemplateID) + "&saved=1&siteid=" + siteId.ToString());
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("NewsletterTemplate_Edit.TemplateNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
