<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebPart_Category.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_PortalEngine_UI_WebParts_Development_WebPart_Category"
    Theme="Default" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label ID="lblInfo" runat="server" /><asp:Label EnableViewState="false"
        ForeColor="red" ID="lblError" runat="server" Visible="false" />
    <asp:Literal ID="ltlScript" runat="server" />
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCategoryDisplayName" runat="server" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCategoryDisplayName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvCategoryDisplayName" runat="server" ErrorMessage="RequiredFieldValidator"
                    ControlToValidate="txtCategoryDisplayName"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <asp:PlaceHolder runat="server" ID="plcCategoryName">
            <tr>
                <td class="FieldLabel">
                    <asp:Label ID="lblCategoryName" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtCategoryName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                    <asp:RequiredFieldValidator ID="rfvCategoryName" runat="server" ErrorMessage="RequiredFieldValidator"
                        ControlToValidate="txtCategoryName"></asp:RequiredFieldValidator>
                </td>
            </tr>
        </asp:PlaceHolder>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel ID="lblCategoryImagePath" runat="server" EnableViewState="false" ResourceString="webpartcategory.imagepath" DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtCategoryImagePath" runat="server" CssClass="TextBoxField" MaxLength="450" />                
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOK_Click" CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
