<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="TagGroup_Edit_Tags.aspx.cs" Inherits="CMSSiteManager_Development_TagGroups_TagGroup_Edit_Tags"
    Title="Tag group - Tags" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="Server">
    <asp:Label ID="lblInfo" runat="server" Visible="false" CssClass="InfoLabel" EnableViewState="false" />
    <cms:UniGrid ID="gridTags" runat="server" OrderBy="TagName" IsLiveSite="false" Columns="TagID, TagName, TagCount" />
</asp:Content>
