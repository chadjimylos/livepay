using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSAdminControls_Debug_CacheLog : CacheLog
{
    protected string cmsVersion = null;
    protected int index = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.EnableViewState = false;
        this.Visible = true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.Visible = false;

        if (this.Log != null)
        {
            // Get the log table
            DataTable dt = this.Log.LogTable;
            if (!DataHelper.DataSourceIsEmpty(dt))
            {
                this.Visible = true;

                cmsVersion = ResHelper.GetString("Footer.Version") + "&nbsp;" + CMSContext.SYSTEM_VERSION + "&nbsp;" + ResHelper.GetString("Footer.Build") + "&nbsp;" + CMSContext.FullSystemVersion;

                gridCache.Columns[1].HeaderText = ResHelper.GetString("CacheLog.Operation");
                gridCache.Columns[2].HeaderText = ResHelper.GetString("CacheLog.Data");
                gridCache.Columns[3].HeaderText = ResHelper.GetString("CacheLog.Context");

                if (LogStyle != "")
                {
                    this.ltlInfo.Text = "<div style=\"padding: 2px; font-weight: bold; background-color: #eecccc; border-bottom: solid 1px #ff0000;\">" + ResHelper.GetString("CacheLog.Info") + "</div>";
                }

                gridCache.DataSource = dt;
                gridCache.DataBind();
            }
        }
    }


    /// <summary>
    /// Gets the item index
    /// </summary>
    protected int GetIndex()
    {
        return ++index;
    }


    /// <summary>
    /// Gets the item value
    /// </summary>
    protected string GetValue(object key, object value)
    {
        string result = TextHelper.EnsureLineEndings(HttpUtility.HtmlEncode(DataHelper.GetObjectString(value)), ", ");

        if (!String.IsNullOrEmpty(result))
        {
            result = "<a href=\"" + ResolveUrl("~/CMSSiteManager/Administration/System/Debug/System_ViewObject.aspx?source=cache&amp;key=") + HttpUtility.UrlEncode((string)key) + "\" target=\"_blank\"><img src=\"" + ResolveUrl(GetImageUrl("Design/Controls/UniGrid/Actions/View.png")) + "\" style=\"float: right; border: none;\" alt=\"" + ResHelper.GetString("General.View") + "\" /></a>" + result;
        }

        return result;
    }
}
