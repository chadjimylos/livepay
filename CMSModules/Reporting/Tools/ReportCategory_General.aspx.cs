using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Reporting;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Reporting_Tools_ReportCategory_General : CMSReportingPage
{
    protected int categoryID = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Title = "Report category edit";

        rfvCodeName.ErrorMessage = ResHelper.GetString("ReportCategory_Edit.ErrorCodeName");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("ReportCategory_Edit.ErrorDisplayName");

        // control initializations				
        lblCategoryDisplayName.Text = ResHelper.GetString("ReportCategory_Edit.CategoryDisplayNameLabel");
        lblCategoryCodeName.Text = ResHelper.GetString("ReportCategory_Edit.CategoryCodeNameLabel");

        btnOk.Text = ResHelper.GetString("General.OK");

        string currentReportCategory = ResHelper.GetString("ReportCategory_Edit.NewItemCaption");

        // get reportCategory id from querystring		
        categoryID = ValidationHelper.GetInteger(Request.QueryString["CategoryID"], 0);
        if (categoryID > 0)
        {
            ReportCategoryInfo reportCategoryObj = ReportCategoryInfoProvider.GetReportCategoryInfo(categoryID);
            if (reportCategoryObj != null)
            {
                currentReportCategory = reportCategoryObj.CategoryDisplayName;

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(reportCategoryObj);

                    // show that the reportCategory was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
        }
    }


    /// <summary>
    /// Load data of editing reportCategory.
    /// </summary>
    /// <param name="reportCategoryObj">ReportCategory object.</param>
    protected void LoadData(ReportCategoryInfo reportCategoryObj)
    {
        txtCategoryDisplayName.Text = reportCategoryObj.CategoryDisplayName;
        txtCategoryCodeName.Text = reportCategoryObj.CategoryCodeName;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'Read' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
        {
            RedirectToAccessDenied("cms.reporting", "Modify");
        }

        string errorMessage = new Validator().NotEmpty(txtCategoryCodeName.Text, ResHelper.GetString("ReportCategory_Edit.ErrorCodeName")).NotEmpty(txtCategoryDisplayName.Text, ResHelper.GetString("ReportCategory_Edit.ErrorDisplayName")).Result;

        if ((errorMessage == "") && (!ValidationHelper.IsCodeName(txtCategoryCodeName.Text.Trim())))
        {
            errorMessage = ResHelper.GetString("ReportCategory_Edit.InvalidCodeName");
        }

        if (errorMessage == "")
        {
            ReportCategoryInfo rcCodeNameCheck = ReportCategoryInfoProvider.GetReportCategoryInfo(txtCategoryCodeName.Text.Trim());
            //check reportCategory codename
            if ((rcCodeNameCheck == null) || (rcCodeNameCheck.CategoryID == categoryID))
            {
                ReportCategoryInfo reportCategoryObj = ReportCategoryInfoProvider.GetReportCategoryInfo(categoryID);

                // if reportCategory doesnt already exist, create new one
                if (reportCategoryObj == null)
                {
                    reportCategoryObj = new ReportCategoryInfo();
                }
                reportCategoryObj.CategoryDisplayName = txtCategoryDisplayName.Text.Trim();
                reportCategoryObj.CategoryCodeName = txtCategoryCodeName.Text.Trim();

                ReportCategoryInfoProvider.SetReportCategoryInfo(reportCategoryObj);

                UrlHelper.Redirect("ReportCategory_General.aspx?CategoryID=" + Convert.ToString(reportCategoryObj.CategoryID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("ReportCategory_Edit.ReportCategoryAlreadyExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
