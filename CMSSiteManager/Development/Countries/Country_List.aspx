<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Country_List.aspx.cs" Inherits="CMSSiteManager_Development_Countries_Country_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Countries" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
    <cms:UniGrid runat="server" ID="UniGrid" GridName="Country_List.xml" OrderBy="CountryDisplayName"
        Columns="CountryID, CountryDisplayName" IsLiveSite="false" />
</asp:Content>
