using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.UIControls;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.SettingsProvider;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.Blogs;

public partial class CMSModules_Blogs_Tools_Blogs_Blogs_List : CMSBlogsPage
{
    #region "Variables"

    private CurrentUserInfo currentUser = null;
    private bool manageBlogs = false;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check the current user
        currentUser = CMSContext.CurrentUser;
        if (currentUser == null)
        {
            return;
        }

        // Check 'Manage' permission
        if (currentUser.IsAuthorizedPerResource("cms.blog", "Manage"))
        {
            manageBlogs = true;
        }

        if (!RequestHelper.IsPostBack())
        {
            this.drpBlogs.Items.Add(new ListItem(ResHelper.GetString("general.selectall"), "##ALL##"));
            this.drpBlogs.Items.Add(new ListItem(ResHelper.GetString("blog.selectmyblogs"), "##MYBLOGS##"));
        }

        // No cms.blog doc. type
        if (DataClassInfoProvider.GetDataClass("cms.blog") == null)
        {
            RedirectToInformation(ResHelper.GetString("blog.noblogdoctype"));
        }

        // Set the page title when existing category is being edited
        this.CurrentMaster.DisplayControlsPanel = true;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("myblogs.blogs.header");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Blog/object.png");
        this.CurrentMaster.Title.HelpTopicName = "blogs_list";

        this.gridBlogs.OnDataReload += new UniGrid.OnDataReloadEventHandler(gridBlogs_OnDataReload);
        this.gridBlogs.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        DataClassInfo dci = DataClassInfoProvider.GetDataClass("cms.blogpost");
        string classId = "";
        string script = "";

        if (dci != null)
        {
            classId = dci.ClassID.ToString();
        }

        // Get script to redirect to new blog post page        
        script += "function NewPost(parentId, culture) { \n";
        script += "     if (parentId != 0) { \n";
        script += "         parent.parent.parent.location.href = \"" + ResolveUrl("~/CMSDesk/default.aspx") + "?section=content&action=new&nodeid=\" + parentId + \"&classid=" + classId + "&culture=\" + culture;";
        script += "}} \n";

        // Generate javascript code
        ltlScript.Text = ScriptHelper.GetScript(script);
    }

    #endregion


    #region "UniGrid Events"


    protected DataSet gridBlogs_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        totalRecords = -1;
        if (drpBlogs.SelectedValue == "##MYBLOGS##")
        {
            // Get owned blogs
            return BlogHelper.GetBlogs(CMSContext.CurrentSiteName, currentUser.UserID, null, "BlogID, BlogName, NodeID, DocumentCulture", completeWhere);
        }
        else
        {
            if ((currentUser.IsGlobalAdministrator) || (manageBlogs))
            {
                // Get all blogs
                return BlogHelper.GetBlogs(CMSContext.CurrentSiteName, 0, null, "BlogID, BlogName, NodeID, DocumentCulture", completeWhere);
            }
            else
            {
                // Get owned or managed blogs
                return BlogHelper.GetBlogs(CMSContext.CurrentSiteName, currentUser.UserID, currentUser.UserName, "BlogID, BlogName, NodeID, DocumentCulture", completeWhere);
            }
        }
    }

    #endregion
}
