﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExportToPDF.aspx.vb" Inherits="CMSTemplates_LivePay_ExportToPDFNew" EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<style>

/*# Pop Up #*/
.PopUpMainTable{text-align:left}
.PopTopLeft{width:17px;height:35px;background-image:url('/App_Themes/LivePay/PopUp/LeftTop.png');background-repeat:no-repeat;}
.PopTopCenter{background-image:url('/App_Themes/LivePay/PopUp/TopRpt.png');background-repeat:repeat-x;width:580px;height:35px;}
.PopTopRight{width:15px;height:35px;background-image:url('/App_Themes/LivePay/PopUp/RightTop.png');background-repeat:no-repeat;}
.PopTopClose{background-image:url('/App_Themes/LivePay/PopUp/TopRpt.png');background-repeat:repeat-x;width:12px;height:35px;}
.PopCenterLeft{background-image:url('/App_Themes/LivePay/PopUp/LeftRpt.png');background-repeat:repeat-y;width:17px;min-height:35px}
.PopCenter{background-color:#effbff;min-height:35px;padding-left:10px;padding-right:10px}
.PopCenterRight{background-image:url('/App_Themes/LivePay/PopUp/RightRpt.png');background-repeat:repeat-y;width:17px;min-height:35px}
.PopBotCenter{background-image:url('/App_Themes/LivePay/PopUp/BottomRpt.png');background-repeat:repeat-x;min-width:300px;height:17px}
.PopBotLeft{width:17px;height:17px;background-image:url('/App_Themes/LivePay/PopUp/LeftBottom.png');background-repeat:no-repeat;}
.PopBotRight{width:15px;height:17px;background-image:url('/App_Themes/LivePay/PopUp/RightBottom.png');background-repeat:no-repeat;}
.PopTopTitle{line-height:26px;border-bottom:1px solid #cae8f2;font-size:14px;color:#094595;font-family:tahoma;font-weight:bold;padding-top:20px;width:580px}
.PopTopTitleSmall{line-height:26px;border-bottom:1px solid #cae8f2;font-size:14px;color:#094595;font-family:tahoma;font-weight:bold;padding-top:20px;width:424px}
.PopTopTitleUnder{font-size:1px;height:1px;background-color:white}
.PopContent{padding-top:10px;width:500px}
.PopTopSep{font-size:1px;height:7px}
.PopTopAlertBG{width:577px;height:27px;background-image:url('/App_Themes/LivePay/PopUp/TopAlertBg.png');background-repeat:no-repeat;}
.PopTopAlertText{font-size:11px;color:#444444;font-weight:bold;padding-left:40px}
.PopTopLogo{width:146px;text-align:right;border-bottom:1px solid #cae8f2;}

.PopTopCancelBG{width:577px;height:27px;background-image:url('/App_Themes/LivePay/PopUp/TopCancelBg.png');background-repeat:no-repeat;}
.PopTopCancelText{font-size:11px;color:#dc2c09;font-weight:bold;padding-left:40px}


.PopTransRecTitle{text-align:right;width:150px;color:#43474a;font-size:12px;font-family:tahoma;line-height:25px}
.PopTransDetTitle{width:200px;text-align:right;color:#43474a;font-size:12px;font-family:tahoma;line-height:25px;}
.PopTransDetTitle2{width:210px;text-align:right;color:#43474a;font-size:12px;font-family:tahoma;line-height:25px;}
.PopTransDetValue{width:334px;padding-left:15px;color:#43474a;font-size:12px;font-family:tahoma;line-height:25px;font-weight:bold;}
.PopTransDetNotes{width:334px;padding-left:15px;color:#43474a;font-size:12px;font-family:tahoma;font-weight:bold;}
.PopTransDetButtons{padding-top:15px}
.PopTransDEtBotDescr{color:#43474a;font-size:11px;width:580px;font-style:italic;padding-top:15px;padding-bottom:10px}
.PopTransREp{color:#43474a;font-size:12px;font-family:tahoma;font-weight:bold;}
.PopTransReportButtons{padding-top:15px;padding-left:115px}
.MerchSrcTopDLLDiv{float:left;color:#094595;font-size:12px;padding-top:10px;padding-left:15px}
.MerchSrcHeaderLeft{padding-left:3px;border-left:1px solid #96c2ff;border-right:1px solid #e7e7e7;widht:50px}
.MerchSrcHeaderSimple{padding-left:3px;border-right:1px solid #e7e7e7}
.MerchSrcItemSimple{padding-left:3px;border-right:1px solid #e7e7e7}
.MerchSrcItemLeft{padding-left:3px;border-left:1px solid #96c2ff;;border-right:1px solid #e7e7e7;widht:50px}
.MerchSrcBtnSearch{float:left;text-align:right;width:450px;padding-top:15px;}
.SrHistPayrbl label{padding-right:15px;padding-left:5px}
.SrHistPayBG{background-image:url('/app_themes/LivePay/SearchHistPayBG.png');background-color:#f1f1f1;background-repeat:no-repeat;width:669px;}
.SrHistPayTitle{color:white;font-size:14px;font-weight:bold;padding-top:8px;padding-left:15px}
.SrHistPaySrcTitle{color:#0a4da2;font-size:12px;font-weight:bold;padding-top:17px;padding-left:15px}
.SrHistPaySrcDescr{color:#000000;font-size:12px;padding-top:7px;padding-left:15px}
.SrHistPaySrcDtmTitle{float:left;color:#094595;font-size:12px;font-weight:bold;padding-top:12px;padding-left:65px}
.SrHistPaySrcDtmRBL{float:left;color:#094595;font-size:12px;padding-top:5px;padding-left:15px}
.SrHistPaySrcDtmFrom{float:left;color:#094595;font-size:12px;padding-top:15px;padding-left:152px}
.SrHistPaySrcFrom{float:left;padding-top:12px;padding-left:10px}
.SrHistPaySrcDtmTo{float:left;color:#094595;font-size:12px;padding-top:15px;padding-left:55px}
.SrHistPaySrcTo{float:left;padding-top:12px;padding-left:10px}
.SrHistPayResTitle{float:left;color:#094595;font-size:11px;padding-top:10px;padding-left:10px;width:120px}
.SrHistPayResBtnSearch{float:left;text-align:right;width:450px;padding-top:25px;}

.SrHistPayTableTitle{float:left;width:118px;text-align:right;color:#094595;font-size:12px;font-weight:bold;padding-right:25px;padding-top:3px}
.SrHistPayTableFrom{float:left;color:#094595;font-size:12px;padding-top:3px}
.SrHistPayTableTxt{float:left;padding-left:10px}
.SrHistPayTableTo{float:left;color:#094595;font-size:12px;padding-top:3px;padding-left:54px}
.SrHistPayTableTitleMerch{float:left;width:218px;color:#094595;font-size:12px;font-weight:bold;padding-right:20px;padding-top:3px;padding-left:55px;}
.SrHistPayTableTitleMerchRpt{float:left;width:158px;color:#094595;font-size:12px;font-weight:bold;padding-right:10px;padding-top:3px;padding-left:55px;}
.SrHistPayTableTitleSec{float:left;width:112px;color:#094595;font-size:12px;font-weight:bold;padding-right:20px;padding-top:3px;padding-left:55px;}
.SrHistPayTableToSec{float:left;color:#094595;font-size:12px;padding-top:3px;padding-left:40px}
.SrHistPayTableToMerc{float:left;color:#094595;font-size:12px;padding-top:3px;padding-left:54px}

.SrcHisPayGridHeader{padding-left:10px;border-left:1px solid #96c2ff;}
.SrcHisPayGridItem{padding-left:10px;border-left:1px solid #96c2ff;}
.SrcHisPayGridHeaderLast{border-right:1px solid #96c2ff;}
.SrcHisPayGridItemLast{border-right:1px solid #96c2ff;;width:30px}
.SrHistPayBigForm{padding-top:10px}
.SrcHisPayDDL{color:#43474a;font-size:12px;font-family:tahoma}
.SrcHisPayTxt{border:1px solid #3b6db4;color:#43474a;font-size:12px;font-family:tahoma}

.SrcHisPayGridHeaderPrice{width:33px;text-aling:center;padding-left:3px}
.SrcHisPayGridItemPrice{text-align:right;width:33px}
.SrcHisPayMainGrid{padding-left:10px;padding-top:10px;padding-top:expression(5);BACKGROUND-COLOR: #f1f1f1}
.SrcHisPayGridBGTop{background-image:url('../app_themes/LivePay/SrcHistPayGridTop.png');background-repeat:no-repeat;widht:642px;BACKGROUND-COLOR: #f1f1f1}
.SrcHisPayGridBGTop650{background-image:url('../app_themes/LivePay/SrcHistPayGridTop650.png');background-repeat:no-repeat;widht:650px;BACKGROUND-COLOR: #f1f1f1}
.SrcHisPayGridTopTitle{color:White;font-size:12px;font-weight:bold;padding-top:8px;padding-left:10px;padding-bottom:7px}
.SrcHisPayGridDiv{width:640px;BACKGROUND-COLOR: #f1f1f1}
.SrcHisPayGridBGBottom{background-image:url('../app_themes/LivePay/SrcHistPayGridBottom.png');background-repeat:no-repeat;widht:643px;height:7px;}
.SrcHisPayHeaderGrid{border-left:1px solid #96c2ff;border-right:1px solid #96c2ff;}
.SrcHisPayPager{background-image:url('../app_themes/LivePay/SrcHistPayGridBottom.png');background-repeat:no-repeat;widht:643px}
.SrcHisPayPager650{background-image:url('../app_themes/LivePay/SrcHistPayGridBottom650.png');background-repeat:no-repeat;widht:643px}
.SrcHisPayPagerEmpty{background-image:url('../app_themes/LivePay/GridEmptyBG.png');background-repeat:no-repeat;widht:100%px;height:25px}
.SrcHisPayPagerEmpty650{background-image:url('../app_themes/LivePay/GridEmptyBG650.png');background-repeat:no-repeat;widht:100%px;height:25px}
.SrcHisPayPagerSpace{padding-top:2px}
.SearchTable{}

.SrchHisTable_MainTitle{float:left;width:180px;border-right:1px solid #f8f6f6;height:90px;line-height:18px;}
.SrchHisTable_Main_Title{float:left;font-size:12px;color:#43474a;padding-top:10px;width:130px}
.SrchHisTable_Main_Value{float:left;font-size:12px;color:#0a4da2;font-weight:bold;padding-top:10px}
.SrchHisTable_Main_SecTitle{float:left;font-size:12px;color:#43474a;width:130px}
.SrchHisTable_Main_SecValue{float:left;font-size:12px;color:#0a4da2;font-weight:bold}
.SrchHisTable_LastVal{float:left;border-left:1px solid #e3e3e3;padding-left:10px;height:90px;line-height:18px;width:240px;}
.SrchHisTable_LastValTitle{font-size:12px;color:#43474a;padding-top:10px;}
.SrchHisTable_LastValValue{font-size:11px;color:#4f85d1;font-weight:bold;padding-top:10px}
</style>
<body>


    <form id="form1" runat="server">
    
    <div id="divRep1" runat="server" visible="false" >
     <table cellpadding="0" cellspacing="0" border="0" style="padding-bottom:10px">
     <tr>
     <td><img src="/App_Themes/LivePay/TopLeft.png" /></td>
     <td style="background-image:url('/App_Themes/LivePay/TopCenterSmall.png');background-repeat:repeat-x;width:286px;height:87px"></td>
     <td><img src="/App_Themes/LivePay/TopRight_NOEFG.png" /></td>
     </tr>
     <tr style="display:none">
        <td colspan="3" style="font-weight:bold;color:#0a4da2;font-size:15px"><%=ResHelper.LocalizeString("{$=Πρόσφατες Κινήσεις|en-us=Recent Transactions$}") %></td>
     </tr>
    </table>
        <asp:GridView  ID="GridViewPayments" runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Names="arial" HeaderStyle-Font-Size="14px" HeaderStyle-Font-Bold="true" RowStyle-Font-Names="arial" RowStyle-Font-Size="12px" >
            <Columns>
                <asp:BoundField HeaderStyle-Width="150px" DataField="transactiondDte" ItemStyle-HorizontalAlign="Center"  />
                 <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"   HeaderStyle-Width="150px" >
                    <ItemTemplate>
                        <asp:Label ID="lblmerchantId" style="padding-left:3px" runat="server" Text='<%#Eval("merchantId") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple"   HeaderStyle-Width="110px" >
                    <ItemTemplate>
                        <asp:Label ID="lblCardNumber" style="padding-left:3px" runat="server" Text=' <%# "xxxxxxxx-xxxx-" & Right(Replace(Eval("CardNumber")," ",""),4) %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField  HeaderStyle-CssClass="MerchSrcHeaderSimple" ItemStyle-HorizontalAlign="Right"  HeaderStyle-Width="60px" >
                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("transactioAamount") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField  HeaderStyle-Width="120px" ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:Label ID="lbltransactionStatus" runat="server" Text='<%#GetTransactionStatusDescrPay(Eval("TransactionStatus")) %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
              <asp:TemplateField  HeaderStyle-Width="70px" ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:Label ID="lbltransactionType" runat="server" Text='<%#Eval("TransactionType") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

    <div id="divRep2" runat="server" visible="false" >
    <table cellpadding="0" cellspacing="0" border="0" style="padding-bottom:10px">
     <tr>
     <td><img src="/App_Themes/LivePay/TopLeft.png" /></td>
     <td style="background-image:url('/App_Themes/LivePay/TopCenterSmall.png');background-repeat:repeat-x;width:524px;height:87px"></td>
     <td><img src="/App_Themes/LivePay/TopRight_NOEFG.png" /></td>
     </tr>
     <tr style="display:none">
        <td colspan="3" style="font-weight:bold;color:#0a4da2;font-size:15px"><%=ResHelper.LocalizeString("{$=Συναλλαγές Ανοικτού Πακέτου|en-us=Current Batch Transactions$}") %></td>
     </tr>
    </table>
        <asp:GridView  ID="GridViewMerchantsPayments" runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Names="arial" HeaderStyle-Font-Size="14px" HeaderStyle-Font-Bold="true" RowStyle-Font-Names="arial" RowStyle-Font-Size="12px" >
            <Columns>
                <asp:TemplateField HeaderStyle-Width="90px"  HeaderStyle-CssClass="MerchSrcHeaderLeft MerchSrcHeader"   >
                    <ItemTemplate>
                        <asp:label ID="lblBoxNo" style="padding-left:3px" runat="server" Text='<%#Eval("BatchID") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="90px" HeaderStyle-CssClass="MerchSrcHeaderSimple"    ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:label ID="lblCloseDtm" runat="server" Text='<%#Eval("BatchDate").toString().split(" ")(0) & "<br />" & Eval("BatchDate").toString().split(" ")(1) %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="85px" HeaderStyle-CssClass="MerchSrcHeaderSimple"    ItemStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:label ID="lblBoxStatus" runat="server" Text='<%#IIF(Eval("BatchClosed")=true,"Κλειστό","Ανοιχτό") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="90px" HeaderStyle-CssClass="MerchSrcHeaderSimple"    >
                    <ItemTemplate>
                        <asp:label ID="lblTransCode" style="padding-left:3px" runat="server" Text='' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="90px" HeaderStyle-CssClass="MerchSrcHeaderSimple "  ItemStyle-HorizontalAlign="Center"  >
                    <ItemTemplate>
                        <asp:label ID="lblTransDtm" runat="server" Text='<%#Eval("transactiondDte").toString().split(" ")(0) & "<br />" & Eval("transactiondDte").toString().split(" ")(1) %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField  HeaderStyle-Width="160px" HeaderStyle-CssClass="MerchSrcHeaderSimple" ItemStyle-HorizontalAlign="Center"  >
                    <ItemTemplate>
                        <asp:label ID="lblCustName" runat="server" Text='<%#Eval("CustomerName") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="60px" HeaderStyle-CssClass="MerchSrcHeaderSimple" ItemStyle-HorizontalAlign="Right"   >
                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server" Text='<%#Eval("transactioAamount") %>' />&nbsp;&nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="70px" HeaderStyle-CssClass="MerchSrcHeaderSimple"   ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:label ID="lblTransType" runat="server" Text='<%#Eval("transactionType") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="70px" HeaderStyle-CssClass="MerchSrcHeaderSimple"   ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:label ID="lblInstallments" runat="server" Text='<%#Eval("Installments") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderStyle-Width="90px" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblTransStatus" runat="server" Text='<%#GetTransactionStatusDescr(Eval("TransactionStatus")) %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>

                 <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo1" runat="server" Text='<%#Eval("info1") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo2" runat="server" Text='<%#Eval("info2") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo3" runat="server" Text='<%#Eval("info3") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false"  HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo4" runat="server" Text='<%#Eval("info4") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo5" runat="server" Text='<%#Eval("info5") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo6" runat="server" Text='<%#Eval("info6") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo7" runat="server" Text='<%#Eval("info7") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo8" runat="server" Text='<%#Eval("info8") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo9" runat="server" Text='<%#Eval("info9") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                       <asp:label ID="lblInfo10" runat="server" Text='<%#Eval("info10") %>' ></asp:label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

    <div id="DivMerchantTransDetails" runat="server" visible="false" >
    <table cellpadding="0" cellspacing="0" border="0" style="padding-bottom:10px">
     <tr>
     <td><img src="/App_Themes/LivePay/TopLeft.png" /></td>
     <td style="background-image:url('/App_Themes/LivePay/TopCenterSmall.png');background-repeat:repeat-x;width:235px;height:87px"></td>
     <td><img src="/App_Themes/LivePay/TopRight_NOEFG.png" /></td>
     </tr>
    </table>
         <table cellpadding="0" cellspacing="0" border="0" class="PopUpMainTable">
            <tr>
	        <td class="PopTopLeft">&nbsp;</td>
                <td class="PopTopCenter">&nbsp;</td>
                <td class="PopTopClose">&nbsp;</td>
	        <td class="PopTopRight">&nbsp;</td>  
            </tr>
            <tr>
	        <td class="PopCenterLeft">&nbsp;</td>
	        <td colspan="2" class="PopCenter">
                <table cellpadding="0" cellspacing="0">
                    <tr><td class="PopTopTitle" id="PageTitle" runat="server"></td></tr>
                    <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>

                    <tr><td class="PopContent">
                        <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center;font-size:12px" >
                            <tr runat="server" id="trTransCode">
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Κωδικός Συναλλαγής |en-us=Transaction Code $}") %></td>
                                <td class="PopTransDetValue" id="tdTransCode" runat="server"></td>
                            </tr>
                            <tr>
                                <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ημερομηνία Συναλλαγής |en-us=Transaction Date$}")%></td>
                                <td class="PopTransDetValue" id="tdTransDtm" runat="server"></td>
                            </tr>
                             <tr id="FieldCancelDTM" runat="server" visible="false">
                                <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ημερομηνία Ακύρωσης Συναλλαγής|en-us=Cancellation Date$}")%></td>
                                <td class="PopTransDetValue" id="tdCancelDTM" runat="server"></td>
                            </tr>
                            <tr id="TRPrice" runat="server">
                                <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ποσό |en-us=Amount $}")%> (&euro;)</td>
                                <td class="PopTransDetValue"  ID="tdPrice" runat="server" ></td>
                            </tr>
                            <tr>
                                <td class="PopTransDetTitle"><%= ResHelper.LocalizeString("{$=Κατάσταση Συναλλαγής |en-us=Transaction Status $}")%></td>
                                <td class="PopTransDetValue"  id="tdTransStatus" runat="server"></td>
                            </tr>
                            <tr id="AuthorisationCodeTR" runat="server" visible="false">
                                <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Authorisation Code|en-us=Authorisation Code$}")%></td>
                                <td class="PopTransDetValue" id="tdAuthorisationCode" runat="server"></td>
                            </tr>
                            <tr>
                                <td colspan="2" width="544px">
                                    <table cellpadding="0" cellspacing="0" id="tblCustomFields" runat="server" style="font-size:12px">
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td></tr>
                    <tr><td class="PopTopTitle"><%= ResHelper.LocalizeString("{$=Στοιχεία Πακέτου |en-us=Batch Details$}")%></td></tr>
                    <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>
                    <tr><td class="PopContent">
                        <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center;font-size:12px" >
                           <tr id="FieldBatchNo" runat="server">
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Αριθμός Πακέτου |en-us=Batch Number $}") %></td>
                                <td class="PopTransDetValue" id="tdBoxNo" runat="server"></td>
                            </tr>
                            <tr id="FieldBatchCloseDtm" runat="server">
                                <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Ημερομηνία Κλεισίματος Πακέτου |en-us=Batch Submission Date $}")%></td>
                                <td class="PopTransDetValue" id="tdBoxCloseDtm" runat="server"></td>
                            </tr>
                            <tr id="FieldBatchStatus" runat="server">
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Κατάσταση Πακέτου |en-us=Batch Status $}") %></td>
                                <td class="PopTransDetValue" id="tdBoxStatus" runat="server"></td>
                            </tr>
                          </table>
                    </td></tr>

                <asp:PlaceHolder runat="server" ID="ph_Invoice" Visible="false">
                    <tr><td class="PopTopTitle"><%=ResHelper.LocalizeString("{$=Στοιχεία Τιμολόγησεις |en-us=Invoice Details $}") %></td></tr>
                    <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>
                    <tr><td class="PopContent">
                        <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center" >
                           <tr id="TrCompanyName" runat="server">
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Επωνυμία Επιχείρησης|en-us=Company Name $}")%></td>
                                <td class="PopTransDetValue" id="tdCompanyName" runat="server"></td>
                            </tr>
                            <tr id="TrProfession" runat="server">
                                <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Επάγγελμα |en-us=Profession $}")%></td>
                                <td class="PopTransDetValue" id="tdProfession" runat="server"></td>
                            </tr>
                            <tr id="TrAddress" runat="server">
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Διεύθυνση |en-us=Address $}") %></td>
                                <td class="PopTransDetValue" id="tdAddress" runat="server"></td>
                            </tr>
                            <tr id="TrPostalCode" runat="server">
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Τ.Κ.|en-us=Postal Code $}")%></td>
                                <td class="PopTransDetValue" id="tdPostalCode" runat="server"></td>
                            </tr>
                            <tr id="TrCity" runat="server">
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Πόλη |en-us=City $}") %></td>
                                <td class="PopTransDetValue" id="tdCity" runat="server"></td>
                            </tr>
                            <tr id="TrVATNumber" runat="server">
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=ΑΦΜ |en-us=VAT Number$}") %></td>
                                <td class="PopTransDetValue" id="tdVATNumber" runat="server"></td>
                            </tr>
                            <tr id="TrTaxOffice" runat="server">
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=ΔΟΥ |en-us=Tax office $}")%></td>
                                <td class="PopTransDetValue" id="tdTaxOffice" runat="server"></td>
                            </tr>
                        </table>
                    </td></tr>
                </asp:PlaceHolder>

                    <tr><td class="PopTopTitle"><%= ResHelper.LocalizeString("{$=Στοιχεία Πελάτη |en-us=Customer Information$}")%></td></tr>
                    <tr><td class="PopTopTitleUnder">&nbsp;</td></tr>
                    <tr><td>
                        <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center;font-size:12px" >
                            <tr>
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Ονοματεπώνυμο Πελάτη |en-us=Customer Full Name $}") %></td>
                                <td class="PopTransDetValue" id="tdCustFullName" runat="server"></td>
                            </tr>
                            
                            <tr>
                                <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Τύπος Κάρτας |en-us=Card Type $}")%></td>
                                <td class="PopTransDetValue" id="tdCardType" runat="server"></td>
                            </tr>
                            <tr>
                                <td class="PopTransDetTitle" ><%= ResHelper.LocalizeString("{$=Αριθμός Κάρτας |en-us=Card Number$}")%></td>
                                <td class="PopTransDetValue" id="tdCardNo" runat="server"></td>
                            </tr>
                             <tr>
                                <td class="PopTransDetTitle" >E-mail</td>
                                <td class="PopTransDetValue" id="tdEmail" runat="server"></td>
                            </tr>
                             <tr>
                                <td class="PopTransDetTitle" ><%=ResHelper.LocalizeString("{$=Τηλέφωνο Επικοινωνίας |en-us=Phone Number$}") %></td>
                                <td class="PopTransDetValue" id="tdContactPhone" runat="server"></td>
                            </tr>
                             <tr>
                                <td class="PopTransDetTitle"><%=ResHelper.LocalizeString("{$=Σημειώσεις |en-us=Comments $}") %></td>
                                <td class="PopTransDetNotes" id="tdNotes" runat="server">-</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="PopTransDEtBotDescr"><%= ResHelper.LocalizeString("{$=Για οποιαδήποτε πληροφορία καλέστε στο 210 - 9555019 ή αποστείλατε e-mail στο ebanking@eurobank.gr  |en-us=For more information, please call our support department at 210 - 9555019 or e-mail us at ebanking@eurobank.gr $}")%></td>
                            </tr>
                            
                             <tr>
                                <td colspan="2" class="PopContent">
                                    <table cellpadding="0" cellspacing="0" width="577px" style="text-align:left;" >
                                       <tr id="Tr1" runat="server">
                                            <td colspan="2" class="PopTransRecTitle" id="td_AcknowledgementTransaction" runat="server" style="width:100%;text-align:left;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                        </table>
                    </td></tr>
                </table>
            </td>
	        <td class="PopCenterRight">&nbsp;</td>
            </tr>
            <tr>
	        <td class="PopBotLeft">&nbsp;</td>
	        <td colspan="2" class="PopBotCenter"></td>
	        <td class="PopBotRight">&nbsp;</td>
            </tr>
        </table>
        
    </div> 

 
    <div id="DivTransReceipt" runat="server" visible="false">
    <table cellpadding="0" cellspacing="0" border="0" style="padding-bottom:10px">
     <tr>
     <td><img src="/App_Themes/LivePay/TopLeft.png" /></td>
     <td style="background-image:url('/App_Themes/LivePay/TopCenterSmall.png');background-repeat:repeat-x;width:227px;height:87px"></td>
     <td><img src="/App_Themes/LivePay/TopRight_NOEFG.png" /></td>
     </tr>
    </table>
            <table cellpadding="0" cellspacing="0" border="0" class="PopUpMainTable">
            <tr>
	        <td class="PopTopLeft">&nbsp;</td>
                <td class="PopTopCenter">&nbsp;</td>
                <td class="PopTopClose">&nbsp;</td>
	        <td class="PopTopRight">&nbsp;</td>  
            </tr>
            <tr>
	        <td class="PopCenterLeft">&nbsp;</td>
	        <td colspan="2" class="PopCenter">
                <table cellpadding="0" cellspacing="0">
                    <tr><td class="PopTopTitleSmall" id="TdCompanyTitle" runat="server"></td>
                    <td class="PopTopLogo">
                    <asp:Image runat="server" BorderWidth="0" BackColor="Transparent" ID="imgLogo" EnableViewState="false" />
                    </tr>
                    <tr><td class="PopTopTitleUnder"  colspan="2">&nbsp;</td></tr>
                    <tr><td class="PopContent" colspan="2">
                        <table cellpadding="0" cellspacing="0" style="font-size:12px">
                            <tr>
                                <td class="PopTransRecTitle" runat="server" id="tdTransDateTime"></td>
                                <td class="PopTransDetValue" id="tdDtm" runat="server"></td>
                            </tr>
                            <tr>
                                <td class="PopTransRecTitle"><%=ResHelper.LocalizeString("{$=Πληρωμή προς |en-us=Payment to$}") %></td>
                                <td class="PopTransDetValue" id="tdCompPay" runat="server"></td>
                            </tr>
                            <tr runat="server" id="trPayCode">
                                <td class="PopTransRecTitle"><%= ResHelper.LocalizeString("{$=Κωδικός Συναλλαγής |en-us=Payment Code $}")%></td>
                                <td class="PopTransDetValue" id="tdPayCode" runat="server"></td>
                            </tr>
                            <tr style="display:none">
                                <td class="PopTransRecTitle"><%= ResHelper.LocalizeString("{$=Κωδικός Πελάτη |en-us=Customer Code $}")%></td>
                                <td class="PopTransDetValue" id="tdCustCode" runat="server"></td>
                            </tr>
                            <tr>
                                <td colspan="2" width="100%">
                                    <table width="577px" cellpadding="0" cellspacing="0" id="tblCustomFields_Receipt" runat="server" style="font-size:12px">
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="PopTransRecTitle"><%= ResHelper.LocalizeString("{$=Ποσό |en-us=Amount $}")%> (&euro;)</td>
                                <td class="PopTransDetValue" id="tdPrice_Receipt" runat="server"></td>
                            </tr>
                        <asp:PlaceHolder runat="server" ID="plc_Commission" Visible="false" EnableViewState="false">
                            <tr>
                                <td class="PopTransRecTitle"><%= ResHelper.LocalizeString("{$=Προμήθεια (&euro;)|en-us=Commission (&euro;)$}")%></td>
                                <td class="PopTransDetValue" id="tdCommission_Receipt" runat="server"></td>
                            </tr>
                        </asp:PlaceHolder>
                            <tr>
                                <td class="PopTransRecTitle"><%= ResHelper.LocalizeString("{$=Κάρτα |en-us=Card $}")%></td>
                                <td class="PopTransDetValue" id="tdCardNo_Receipt" runat="server"></td>
                            </tr>
                            
                        <asp:PlaceHolder runat="server" ID="plc_Installments" Visible="false" EnableViewState="false">
                            <tr>
                                <td class="PopTransRecTitle"><%= ResHelper.LocalizeString("{$=Αριθμός δόσεων |en-us=Installments $}")%></td>
                                <td class="PopTransDetValue" id="tdInstallments_Receipt" runat="server"></td>
                            </tr>
                        </asp:PlaceHolder>                   

                        <asp:PlaceHolder runat="server" ID="ph_Receipt_Invoice" Visible="false">
                            <tr><td colspan="2" class="PopTopTitle"><%=ResHelper.LocalizeString("{$=Στοιχεία Τιμολόγησεις |en-us=Invoice Details $}") %></td></tr>
                            <tr><td colspan="2" class="PopTopTitleUnder">&nbsp;</td></tr>
                            <tr><td colspan="2" class="PopContent">
                                <table cellpadding="0" cellspacing="0" width="577px" style="background-position:left center" >
                                   <tr id="TrRecCompanyName" runat="server">
                                        <td class="PopTransRecTitle" ><%=ResHelper.LocalizeString("{$=Επωνυμία Επιχείρησης|en-us=Company Name $}")%></td>
                                        <td class="PopTransDetValue" id="tdRecCompanyName" runat="server"></td>
                                    </tr>
                                    <tr id="TrRecProfession" runat="server">
                                        <td class="PopTransRecTitle" ><%= ResHelper.LocalizeString("{$=Επάγγελμα |en-us=Profession $}")%></td>
                                        <td class="PopTransDetValue" id="tdRecProfession" runat="server"></td>
                                    </tr>
                                    <tr id="TrRecAddress" runat="server">
                                        <td class="PopTransRecTitle" ><%=ResHelper.LocalizeString("{$=Διεύθυνση |en-us=Address $}") %></td>
                                        <td class="PopTransDetValue" id="tdRecAddress" runat="server"></td>
                                    </tr>
                                    <tr id="TrRecPostalCode" runat="server">
                                        <td class="PopTransRecTitle" ><%=ResHelper.LocalizeString("{$=Τ.Κ.|en-us=Postal Code $}")%></td>
                                        <td class="PopTransDetValue" id="tdRecPostalCode" runat="server"></td>
                                    </tr>
                                    <tr id="TrRecCity" runat="server">
                                        <td class="PopTransRecTitle" ><%=ResHelper.LocalizeString("{$=Πόλη |en-us=City $}") %></td>
                                        <td class="PopTransDetValue" id="tdRecCity" runat="server"></td>
                                    </tr>
                                    <tr id="TrRecVATNumber" runat="server">
                                        <td class="PopTransRecTitle" ><%=ResHelper.LocalizeString("{$=ΑΦΜ |en-us=VAT Number$}") %></td>
                                        <td class="PopTransDetValue" id="tdRecVATNumber" runat="server"></td>
                                    </tr>
                                    <tr id="TrRecTaxOffice" runat="server">
                                        <td class="PopTransRecTitle" ><%=ResHelper.LocalizeString("{$=ΔΟΥ |en-us=Tax office $}")%></td>
                                        <td class="PopTransDetValue" id="tdRecTaxOffice" runat="server"></td>
                                    </tr>
                                </table>
                            </td></tr>
                        </asp:PlaceHolder>
                            
                        </table>
                    </td></tr>
                </table>
            </td>
	        <td class="PopCenterRight">&nbsp;</td>
            </tr>
            <tr>
	        <td class="PopBotLeft">&nbsp;</td>
	        <td colspan="2" class="PopBotCenter"></td>
	        <td class="PopBotRight">&nbsp;</td>
            </tr>
        </table>
    </div>
    
    </form>
</body>
</html>
