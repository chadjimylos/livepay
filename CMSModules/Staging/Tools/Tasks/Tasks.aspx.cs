using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Threading;
using System.Security.Principal;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Staging;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.PortalEngine;
using CMS.UIControls;
using CMS.DataEngine;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Staging_Tools_Tasks_Tasks : CMSStagingPage
{
    #region "Variables"

    protected static Hashtable mLogs = new Hashtable();
    protected static Hashtable mErrors = new Hashtable();
    protected static Hashtable mInfos = new Hashtable();

    protected IDataClass mEventLog = null;

    protected bool allowView = true;

    private int serverId = 0;
    private string eventCode = null;
    private string eventType = null;

    protected string viewImage = string.Empty;
    protected string deleteImage = string.Empty;
    protected string syncImage = string.Empty;

    protected string viewTooltip = string.Empty;
    protected string deleteTooltip = string.Empty;
    protected string syncTooltip = string.Empty;

    protected string syncCurrent = null;
    protected string syncSubtree = null;

    string aliasPath = "/";

    int currentSiteId = 0;
    string currentSiteName = null;

    protected CurrentUserInfo currentUser = null;
    protected GeneralConnection mConnection = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Connection
    /// </summary>
    public GeneralConnection Connection
    {
        get
        {
            return mConnection ?? (mConnection = ConnectionHelper.GetConnection());
        }
    }


    /// <summary>
    /// Current Error
    /// </summary>
    public string CurrentError
    {
        get
        {
            return ValidationHelper.GetString(mErrors["SyncError_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mErrors["SyncError_" + ctlAsync.ProcessGUID] = value;
        }
    }


    /// <summary>
    /// Current Info
    /// </summary>
    public string CurrentInfo
    {
        get
        {
            return ValidationHelper.GetString(mInfos["SyncInfo_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mInfos["SyncInfo_" + ctlAsync.ProcessGUID] = value;
        }
    }


    /// <summary>
    /// Current log context
    /// </summary>
    public LogContext CurrentLog
    {
        get
        {
            return EnsureLog();
        }
    }


    /// <summary>
    /// Creates new event log
    /// </summary>
    private IDataClass EventLog
    {
        get
        {
            if (mEventLog == null)
            {
                try
                {
                    // New event declaration
                    mEventLog = DataClassFactory.NewDataClass("CMS.EventLog", Connection);

                    mEventLog.SetValue("EventType", eventType);
                    mEventLog.SetValue("EventTime", DateTime.Now);
                    mEventLog.SetValue("Source", "Staging");
                    mEventLog.SetValue("EventCode", eventCode);
                    mEventLog.SetValue("UserID", currentUser.UserID);
                    mEventLog.SetValue("UserName", TextHelper.LimitLength(currentUser.UserName, 250, string.Empty));
                    mEventLog.SetValue("EventDescription", string.Empty);
                    mEventLog.SetValue("IPAddress", TextHelper.LimitLength(HTTPHelper.GetUserHostAddress(), 100));
                    mEventLog.SetValue("EventMachineName", TextHelper.LimitLength(HTTPHelper.MachineName, 100));
                    mEventLog.SetValue("SiteID", currentSiteId);
                }
                catch
                {
                    // Unable to log into eventlog
                }
            }
            return mEventLog;
        }
        set
        {
            mEventLog = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Register script for pendingCallbacks repair
        ScriptHelper.FixPendingCallbacks(Page);

        // Get site info
        currentSiteId = CMSContext.CurrentSiteID;
        currentSiteName = CMSContext.CurrentSiteName;

        // Initialize current user for the async actions
        currentUser = CMSContext.CurrentUser;

        if (CausedPostback(btnSyncComplete))
        {
            SyncComplete();
        }
        else
        {
            if (!RequestHelper.IsCallback())
            {
                serverId = QueryHelper.GetInteger("serverid", 0);
                int nodeId = QueryHelper.GetInteger("nodeid", 0);

                aliasPath = "/";

                // Get the document node
                if (nodeId > 0)
                {
                    TreeProvider tree = new TreeProvider(currentUser);
                    TreeNode node = tree.SelectSingleNode(nodeId, TreeProvider.ALL_CULTURES);
                    if (node != null)
                    {
                        aliasPath = node.NodeAliasPath;
                    }
                }

                // Setup title
                titleElem.TitleText = ResHelper.GetString("Synchronization.Title");
                titleElem.TitleImage = GetImageUrl("CMSModules/CMS_Staging/synchronization.png");

                if (!CausedPostback(btnCurrent, btnSubtree, btnSyncSelected, btnSyncAll))
                {
                    // Check 'Manage servers' permission
                    if (!currentUser.IsAuthorizedPerResource("cms.staging", "ManageTasks"))
                    {
                        RedirectToAccessDenied("cms.staging", "ManageTasks");
                    }

                    // Register the dialog script
                    ScriptHelper.RegisterDialogScript(this);

                    ltlScript.Text +=
                        ScriptHelper.GetScript("function ConfirmDeleteTask(taskId) { return confirm(" +
                                               ScriptHelper.GetString(ResHelper.GetString("Tasks.ConfirmDelete")) + "); }");
                    ltlScript.Text +=
                        ScriptHelper.GetScript("function CompleteSync(){" +
                                               Page.ClientScript.GetPostBackEventReference(btnSyncComplete, null) + "}");


                    tasksUniGrid.OnExternalDataBound += tasksUniGrid_OnExternalDataBound;
                    tasksUniGrid.OnAction += tasksUniGrid_OnAction;
                    tasksUniGrid.OnDataReload += tasksUniGrid_OnDataReload;

                    viewImage = GetImageUrl("Design/Controls/UniGrid/Actions/View.png");
                    deleteImage = GetImageUrl("Design/Controls/UniGrid/Actions/Delete.png");
                    syncImage = GetImageUrl("Design/Controls/UniGrid/Actions/Synchronize.png");

                    btnSyncSelected.OnClientClick = "return !IsSelectionEmpty_" + tasksUniGrid.ClientID + "();";

                    syncTooltip = ResHelper.GetString("Tasks.SyncTooltip");
                    deleteTooltip = ResHelper.GetString("general.delete");
                    viewTooltip = ResHelper.GetString("general.view");

                    syncCurrent = ResHelper.GetString("Tasks.SyncCurrent");
                    syncSubtree = ResHelper.GetString("Tasks.SyncSubtree");

                    imgCurrent.ImageUrl = GetImageUrl("CMSModules/CMS_Staging/synccurrent.png");
                    imgSubtree.ImageUrl = GetImageUrl("CMSModules/CMS_Staging/syncsubtree.png");

                    btnDeleteAll.Text = ResHelper.GetString("Tasks.DeleteAll");
                    btnDeleteSelected.Text = ResHelper.GetString("Tasks.DeleteSelected");
                    btnSyncAll.Text = ResHelper.GetString("Tasks.SyncAll");
                    btnSyncSelected.Text = ResHelper.GetString("Tasks.SyncSelected");

                    btnDeleteAll.OnClientClick = "return confirm(" +
                                                 ScriptHelper.GetString(ResHelper.GetString("Tasks.ConfirmDeleteAll")) +
                                                 ");";
                    btnDeleteSelected.OnClientClick = "return confirm(" +
                                                      ScriptHelper.GetString(ResHelper.GetString("general.confirmdelete")) +
                                                      ");";

                    pnlContent.Visible = true;
                    btnCancel.Attributes.Add("onclick", "CancelAction(); return false;");
                    btnCancel.Text = ResHelper.GetString("General.Cancel");

                    pnlLog.Visible = false;
                }
            }

        }

        ctlAsync.OnFinished += ctlAsync_OnFinished;
        ctlAsync.OnError += ctlAsync_OnError;
        ctlAsync.OnRequestLog += ctlAsync_OnRequestLog;
        ctlAsync.OnCancel += ctlAsync_OnCancel;
    }


    protected override void OnPreRender(EventArgs e)
    {
        lblError.Visible = (lblError.Text != string.Empty);
        lblInfo.Visible = (lblInfo.Text != string.Empty);
        base.OnPreRender(e);
    }

    #endregion


    #region "Grid events"

    protected DataSet tasksUniGrid_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        DataSet ds = TaskInfoProvider.SelectDocumentTaskList(currentSiteId, serverId, aliasPath, null, currentOrder, 0, "TaskID, TaskSiteID, TaskDocumentID, TaskNodeAliasPath, TaskTitle, TaskTime, TaskType, TaskObjectType, TaskObjectID, TaskRunning, (SELECT COUNT(*) FROM Staging_Synchronization WHERE SynchronizationTaskID = TaskID AND SynchronizationErrorMessage IS NOT NULL AND (SynchronizationServerID = @ServerID OR (@ServerID = 0 AND (@TaskSiteID = 0 OR SynchronizationServerID IN (SELECT ServerID FROM Staging_Server WHERE ServerSiteID = @TaskSiteID AND ServerEnabled=1))))) AS FailedCount", currentOffset, currentPageSize, ref totalRecords);
        if (!DataHelper.DataSourceIsEmpty(ds))
        {
            pnlTasksGrid.Visible = true;
            pnlFooter.Visible = true;
        }
        else
        {
            if (lblInfo.Text == string.Empty)
            {
                lblInfo.Text = ResHelper.GetString("Tasks.NoTasks");
            }
            pnlFooter.Visible = false;
            pnlTasksGrid.Visible = false;
        }
        return ds;
    }


    protected object tasksUniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView dr = null;
        switch (sourceName.ToLower())
        {
            case "tasktitle":
                dr = (DataRowView)parameter;
                return GetDocumentLink(dr["TaskDocumentID"], HTMLHelper.HTMLEncode(TextHelper.LimitLength(dr["TaskTitle"].ToString(), 100)), dr["TaskType"]);

            case "tasktime":
                return DateTime.Parse(parameter.ToString()).ToString();

            case "taskresult":
                dr = (DataRowView)parameter;
                return GetResultLink(dr["FailedCount"], dr["TaskID"]);

            case "taskview":
                ImageButton viewBtn = (ImageButton)sender;
                if (allowView)
                {
                    string taskId = ((DataRowView)((GridViewRow)parameter).DataItem).Row["TaskID"].ToString();
                    string url = ResolveUrl("~/CMSModules/Staging/Tools/Tasks/View.aspx?taskid=") + taskId;
                    viewBtn.OnClientClick = "window.open('" + url + "'); return false;";
                    return viewBtn;
                }
                viewBtn.Visible = false;
                return viewBtn;
        }
        return parameter.ToString();
    }


    protected void tasksUniGrid_OnAction(string actionName, object actionArgument)
    {
        int taskId = ValidationHelper.GetInteger(actionArgument, 0);
        switch (actionName.ToLower())
        {
            case "synchronize":
                try
                {
                    // Run task synchronization
                    string result = StagingHelper.RunSynchronization(taskId, serverId, true, currentSiteId);


                    if (!string.IsNullOrEmpty(result))
                    {
                        lblError.Text = ResHelper.GetString("Tasks.SynchronizationFailed");
                    }
                    else
                    {
                        lblInfo.Text = ResHelper.GetString("Tasks.SynchronizationOK");
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = ResHelper.GetString("Tasks.SynchronizationFailed") + " " + ex.Message;
                }
                break;

            case "delete":
                // Delete task
                SynchronizationInfoProvider.DeleteSynchronizationInfo(taskId, serverId, currentSiteId);
                break;
        }
    }

    #endregion


    #region "Grid helper methods"

    /// <summary>
    /// Returns the result link for the synchronization log
    /// </summary>
    /// <param name="failedCount">Failed items count</param>
    /// <param name="taskId">Task ID</param>
    protected string GetResultLink(object failedCount, object taskId)
    {
        int count = ValidationHelper.GetInteger(failedCount, 0);
        if (count > 0)
        {
            string logUrl = "log.aspx?taskid=" + taskId + "&serverId=" + serverId;
            return "<a target=\"_blank\" href=\"" + logUrl + "\" onclick=\"modalDialog('" + logUrl + "', 'tasklog', 700, 500); return false;\">" + ResHelper.GetString("Tasks.ResultFailed") + "</a>";
        }
        else
        {
            return string.Empty;
        }
    }


    /// <summary>
    /// Returns link for document view
    /// </summary>
    /// <param name="documentId">Document ID</param>
    /// <param name="taskTitle">Task title</param>
    /// <param name="taskType">Type of the task</param>
    protected string GetDocumentLink(object documentId, object taskTitle, object taskType)
    {
        string title = ValidationHelper.GetString(taskTitle, string.Empty);
        string type = ValidationHelper.GetString(taskType, string.Empty).ToLower();
        int docId = ValidationHelper.GetInteger(documentId, 0);

        if ((type != "deletedoc") && (type != "deleteallculutres"))
        {
            string viewMode = ViewModeCode.LiveSite.ToString();

            // For publish tasks display document in preview mode
            if ((type == "publishdoc") || (type == "archivedoc"))
            {
                viewMode = ViewModeCode.Preview.ToString();
            }

            // Get document url
            string docUrl = ResolveUrl(CMSContext.GetDocumentUrl(docId)) + "?viewmode=" + viewMode;
            return "<a target=\"_blank\" href=\"" + docUrl + "\">" + HTMLHelper.HTMLEncode(title) + "</a>";
        }
        return title;
    }

    #endregion


    #region "Button handling"

    protected void btnSyncAll_Click(object sender, EventArgs e)
    {
        titleElem.TitleText = ResHelper.GetString("Synchronization.Title");
        RunAsync(SynchronizeAll);
    }


    private void SyncComplete()
    {
        titleElem.TitleText = ResHelper.GetString("Synchronization.Title");
        RunAsync(SynchronizeComplete);
    }


    protected void btnSyncSelected_Click(object sender, EventArgs e)
    {
        titleElem.TitleText = ResHelper.GetString("Synchronization.Title");
        ArrayList list = tasksUniGrid.SelectedItems;
        if (list.Count > 0)
        {
            ctlAsync.Parameter = list;
            RunAsync(SynchronizeSelected);
        }
    }


    protected void btnDeleteAll_Click(object sender, EventArgs e)
    {
        titleElem.TitleText = ResHelper.GetString("Synchronization.DeletingTasksTitle");
        RunAsync(DeleteAll);
    }


    protected void btnDeleteSelected_Click(object sender, EventArgs e)
    {
        titleElem.TitleText = ResHelper.GetString("Synchronization.DeletingTasksTitle");
        if (tasksUniGrid.SelectedItems.Count > 0)
        {
            ctlAsync.Parameter = tasksUniGrid.SelectedItems;
            RunAsync(DeleteSelected);
        }
    }


    protected void btnCurrent_Click(object sender, EventArgs e)
    {
        RunAsync(SynchronizeCurrent);
    }


    protected void btnSubtree_Click(object sender, EventArgs e)
    {
        RunAsync(SynchronizeSubtree);
    }

    #endregion


    #region "Async methods"

    public void SynchronizeComplete(object parameter)
    {
        string result = null;
        eventCode = "SYNCCOMPLETE";

        try
        {

            int sid = serverId;
            if (sid <= 0)
            {
                sid = SynchronizationInfoProvider.ENABLED_SERVERS;
            }

            AddLog(ResHelper.GetString("Synchronization.LoggingTasks"));

            // Synchronize root node
            ArrayList tasks = DocumentHelper.LogSynchronization(CMSContext.CurrentSiteName, "/", TaskTypeEnum.UpdateDocument, null, sid, false, false);

            AddLog(ResHelper.GetString("Synchronization.RunningTasks"));

            // Run the synchronization
            foreach (TaskInfo task in tasks)
            {
                AddLog(string.Format(ResHelper.GetAPIString("synchronization.running", "Processing '{0}' task"), HTMLHelper.HTMLEncode(task.TaskTitle)));
                result += StagingHelper.RunSynchronization(task.TaskID, serverId, true, currentSiteId);
            }

            AddLog(ResHelper.GetString("Synchronization.LoggingTasks"));

            // Synchronize subnodes
            tasks = DocumentHelper.LogSynchronization(CMSContext.CurrentSiteName, "/%", TaskTypeEnum.UpdateDocument, null, sid, false, false);

            AddLog(ResHelper.GetString("Synchronization.RunningTasks"));

            // Run the synchronization
            foreach (TaskInfo task in tasks)
            {
                AddLog(string.Format(ResHelper.GetAPIString("synchronization.running", "Processing '{0}' task"), HTMLHelper.HTMLEncode(task.TaskTitle)));
                result += StagingHelper.RunSynchronization(task.TaskID, serverId, true, currentSiteId);
            }


            if (!string.IsNullOrEmpty(result))
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, null);
            }
            else
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationOK");
                AddLog(CurrentInfo);
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, result);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            FinalizeEventLog();
        }
    }


    public void SynchronizeAll(object parameter)
    {
        string result = string.Empty;
        eventCode = "SYNCALLDOCS";

        try
        {
            AddLog(ResHelper.GetString("Synchronization.RunningTasks"));

            // Process all records
            DataSet ds = TaskInfoProvider.SelectDocumentTaskList(currentSiteId, serverId, aliasPath, null, null, -1, "TaskID,TaskTitle");
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string taskTitle = ValidationHelper.GetString(row["TaskTitle"], string.Empty);
                    AddLog(string.Format(ResHelper.GetAPIString("synchronization.running", "Processing '{0}' task"), HTMLHelper.HTMLEncode(taskTitle)));

                    // Get ID
                    int taskId = ValidationHelper.GetInteger(row["TaskID"], 0);
                    if (taskId > 0)
                    {
                        result += StagingHelper.RunSynchronization(taskId, serverId, true, currentSiteId);
                    }
                }
            }

            if (result != string.Empty)
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, null);
            }
            else
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationOK");
                AddLog(CurrentInfo);
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, result);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            FinalizeEventLog();
        }
    }


    protected void SynchronizeSubtree(object parameter)
    {
        string result = string.Empty;
        eventCode = "SYNCSUBTREE";

        try
        {
            // Synchronize current node first
            result = SynchronizeCurrentInternal(false, "SYNCSUBTREE", false);

            if (result == string.Empty)
            {
                // Synchronize subnodes
                int sid = serverId;
                if (sid <= 0)
                {
                    sid = SynchronizationInfoProvider.ENABLED_SERVERS;
                }

                AddLog(ResHelper.GetString("Synchronization.LoggingTasks"));

                // Get the tasks
                ArrayList tasks = DocumentHelper.LogSynchronization(currentSiteName, aliasPath.TrimEnd('/') + "/%", TaskTypeEnum.UpdateDocument, null, sid, false, false);

                AddLog(ResHelper.GetString("Synchronization.RunningTasks"));

                // Run the synchronization
                foreach (TaskInfo task in tasks)
                {
                    AddLog(string.Format(ResHelper.GetAPIString("synchronization.running", "Processing '{0}' task"), HTMLHelper.HTMLEncode(task.TaskTitle)));
                    result += StagingHelper.RunSynchronization(task.TaskID, serverId, true, currentSiteId);
                }
            }

            if (result != string.Empty)
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, null);
            }
            else
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationOK");
                AddLog(CurrentInfo);
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, result);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            FinalizeEventLog();
        }
    }


    public void SynchronizeSelected(object parameter)
    {
        if (parameter == null)
        {
            return;
        }

        string result = string.Empty;
        eventCode = "SYNCSELECTEDDOCS";
        ArrayList list = (ArrayList)parameter;
        try
        {
            AddLog(ResHelper.GetString("Synchronization.RunningTasks"));

            foreach (object taskIdObj in list)
            {
                int taskId = ValidationHelper.GetInteger(taskIdObj, 0);
                if (taskId > 0)
                {
                    TaskInfo task = TaskInfoProvider.GetTaskInfo(taskId);
                    if (task != null)
                    {
                        AddLog(string.Format(ResHelper.GetAPIString("synchronization.running", "Processing '{0}' task"), HTMLHelper.HTMLEncode(task.TaskTitle)));
                        result += StagingHelper.RunSynchronization(taskId, serverId, true, currentSiteId);
                    }
                }
            }

            if (result != string.Empty)
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, null);
            }
            else
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationOK");
                AddLog(CurrentInfo);
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, result);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            FinalizeEventLog();
        }
    }


    /// <summary>
    /// Synchronizes the current document
    /// </summary>
    private void SynchronizeCurrent(object parameter)
    {
        SynchronizeCurrentInternal(parameter, "SYNCCURRENTDOC", true);
    }


    /// <summary>
    /// Internal method for synchronizing current document
    /// </summary>
    /// <param name="parameter">Parameter</param>
    /// <param name="eventCodeForLog">Event code to set</param>
    /// <param name="finalizeEventLog">Indicates whether to finalize eventlog</param>
    /// <returns>Result of synchronization</returns>
    private string SynchronizeCurrentInternal(object parameter, string eventCodeForLog, bool finalizeEventLog)
    {
        string result = string.Empty;
        eventCode = eventCodeForLog;
        bool finish = ValidationHelper.GetBoolean(parameter, true);

        int sid = serverId;
        if (sid <= 0)
        {
            sid = SynchronizationInfoProvider.ENABLED_SERVERS;
        }

        AddLog(ResHelper.GetString("Synchronization.LoggingTasks"));

        try
        {
            // Get the tasks
            ArrayList tasks = DocumentHelper.LogSynchronization(currentSiteName, aliasPath, TaskTypeEnum.UpdateDocument, null, sid, false, false);

            AddLog(ResHelper.GetString("Synchronization.RunningTasks"));

            // Run the synchronization
            foreach (TaskInfo task in tasks)
            {
                AddLog(string.Format(ResHelper.GetAPIString("synchronization.running", "Processing '{0}' task"), HTMLHelper.HTMLEncode(task.TaskTitle)));
                result += StagingHelper.RunSynchronization(task.TaskID, serverId, true, currentSiteId);
            }

            if (finish)
            {
                if (result != string.Empty)
                {
                    CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                    AddErrorLog(CurrentError, null);
                }
                else
                {
                    CurrentInfo = ResHelper.GetString("Tasks.SynchronizationOK");
                    AddLog(CurrentInfo);
                }
            }
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.SynchronizationCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed");
                AddErrorLog(CurrentError, result);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.SynchronizationFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            if (finalizeEventLog)
            {
                FinalizeEventLog();
            }
        }
        return result;
    }


    /// <summary>
    /// Deletes selected tasks
    /// </summary>
    protected void DeleteSelected(object parameter)
    {
        if (parameter == null)
        {
            return;
        }

        eventCode = "DELETESELECTEDTASKS";
        ArrayList list = (ArrayList)parameter;
        try
        {
            AddLog(ResHelper.GetString("Synchronization.DeletingTasks"));

            foreach (string taskIdString in list)
            {
                int taskId = ValidationHelper.GetInteger(taskIdString, 0);
                if (taskId > 0)
                {
                    TaskInfo task = TaskInfoProvider.GetTaskInfo(taskId);

                    if (task != null)
                    {
                        AddLog(string.Format(ResHelper.GetAPIString("deletion.running", "Deleting '{0}' task"), HTMLHelper.HTMLEncode(task.TaskTitle)));
                        SynchronizationInfoProvider.DeleteSynchronizationInfo(task, serverId, currentSiteId);
                    }
                }
            }

            CurrentInfo = ResHelper.GetString("Tasks.DeleteOK");
            AddLog(CurrentInfo);
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.DeletionCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.DeletionFailed");
                AddErrorLog(CurrentError);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.DeletionFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            FinalizeEventLog();
        }
    }


    /// <summary>
    /// Deletes all tasks
    /// </summary>
    protected void DeleteAll(object parameter)
    {
        eventCode = "DELETEALLTASKS";
        try
        {
            AddLog(ResHelper.GetString("Synchronization.DeletingTasks"));
            // Get the tasks
            DataSet ds = TaskInfoProvider.SelectDocumentTaskList(currentSiteId, serverId, aliasPath, null, null, -1, null);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    TaskInfo task = new TaskInfo(row);
                    if (task != null)
                    {
                        AddLog(string.Format(ResHelper.GetAPIString("deletion.running", "Deleting '{0}' task"), HTMLHelper.HTMLEncode(task.TaskTitle)));
                        SynchronizationInfoProvider.DeleteSynchronizationInfo(task, serverId, currentSiteId);
                    }
                }
            }

            CurrentInfo = ResHelper.GetString("Tasks.DeleteOK");
            AddLog(CurrentInfo);
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                CurrentInfo = ResHelper.GetString("Tasks.DeletionCanceled");
                AddLog(CurrentInfo);
            }
            else
            {
                CurrentError = ResHelper.GetString("Tasks.DeletionFailed");
                AddErrorLog(CurrentError);
            }
        }
        catch (Exception ex)
        {
            CurrentError = ResHelper.GetString("Tasks.DeletionFailed") + ": " + ex.Message;
            AddErrorLog(CurrentError);
        }
        finally
        {
            FinalizeEventLog();
        }
    }

    #endregion


    #region "Async processing"

    protected void ctlAsync_OnRequestLog(object sender, EventArgs e)
    {
        ctlAsync.Log = CurrentLog.Log;
    }


    protected void ctlAsync_OnError(object sender, EventArgs e)
    {
        tasksUniGrid.ResetSelection();

        lblError.Text = CurrentError;
        lblInfo.Text = CurrentInfo;
    }


    protected void ctlAsync_OnFinished(object sender, EventArgs e)
    {
        tasksUniGrid.ResetSelection();

        lblError.Text = CurrentError;
        lblInfo.Text = CurrentInfo;
    }


    protected void ctlAsync_OnCancel(object sender, EventArgs e)
    {
        tasksUniGrid.ResetSelection();

        ltlScript.Text += ScriptHelper.GetScript("var __pendingCallbacks = new Array();");

        lblError.Text = CurrentError;
        lblInfo.Text = CurrentInfo;
    }


    protected void RunAsync(AsyncAction action)
    {
        pnlLog.Visible = true;
        CurrentLog.Close();
        EnsureLog();
        CurrentError = string.Empty;
        CurrentInfo = string.Empty;
        EventLog = null;
        eventType = "I";
        pnlContent.Visible = false;

        ctlAsync.RunAsync(action, WindowsIdentity.GetCurrent());
    }


    #endregion


    #region "Log handling"

    /// <summary>
    /// Adds the log information
    /// </summary>
    /// <param name="newLog">New log information</param>
    protected void AddLog(string newLog)
    {
        EnsureLog();
        LogContext.AppendLine(newLog);
        AddEventLog(CurrentLog.Log);
    }


    /// <summary>
    /// Adds the log error
    /// </summary>
    /// <param name="newLog">New log information</param>
    protected void AddErrorLog(string newLog)
    {
        AddErrorLog(newLog, null);
    }


    /// <summary>
    /// Adds the log error
    /// </summary>
    /// <param name="newLog">New log information</param>
    /// <param name="errorMessage">Error message</param>
    protected void AddErrorLog(string newLog, string errorMessage)
    {
        LogContext.AppendLine(newLog);

        string logMessage = CurrentLog.Log;
        if (errorMessage != null)
        {
            logMessage = errorMessage + "<br />" + logMessage;
        }
        eventType = "E";

        AddEventLog(logMessage);
    }



    /// <summary>
    /// Adds message to event log object and updates event type
    /// </summary>
    /// <param name="logMessage">Message to log</param>
    protected void AddEventLog(string logMessage)
    {
        // If there is an EventLog
        if (EventLog != null)
        {
            EventLog.SetValue("EventDescription", logMessage);
            EventLog.SetValue("EventType", eventType);
        }
    }


    /// <summary>
    /// Updates event log
    /// </summary>
    protected void FinalizeEventLog()
    {
        // If there is an EventLog
        if (EventLog != null)
        {
            // Insert a new event
            mEventLog.Insert();
        }
    }


    /// <summary>
    /// Ensures the logging context
    /// </summary>
    protected LogContext EnsureLog()
    {
        LogContext log = LogContext.EnsureLog(ctlAsync.ProcessGUID);
        log.Reversed = true;
        log.LineSeparator = "<br />";
        return log;
    }

    #endregion


    #region "Helper methods"

    private bool CausedPostback(params Control[] controls)
    {
        foreach (Control control in controls)
        {
            string uniqueID = control.UniqueID;
            bool toReturn = (Request.Form[uniqueID] != null) || ((Request.Form["__EVENTTARGET"] != null) && Request.Form["__EVENTTARGET"].Equals(uniqueID)) || ((Request.Form[uniqueID + ".x"] != null) && (Request.Form[uniqueID + ".y"] != null));
            if (toReturn)
            {
                return true;
            }
        }
        return false;
    }

    #endregion
}