<%@ Page ValidateRequest="false" Language="C#" AutoEventWireup="true" CodeFile="WebPart_Edit_Properties.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_PortalEngine_UI_WebParts_Development_WebPart_Edit_Properties"
    Theme="Default" EnableEventValidation="false" %>

<%@ Register Src="DefaultValueEditor.ascx" TagName="DefaultValueEditor" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/FieldEditor.ascx" TagName="FieldEditor" TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:FieldEditor ID="FieldEditor" runat="server" DisplaySourceFieldSelection="false" />
    <asp:Label runat="server" ID="lblInfo" Visible="false" EnableViewState="false" />
    <cms:DefaultValueEditor ID="DefaultValueEditor1" runat="server" Visible="false" />
</asp:Content>
