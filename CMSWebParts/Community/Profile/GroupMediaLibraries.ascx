<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Community/Profile/GroupMediaLibraries.ascx.cs" Inherits="CMSWebParts_Community_Profile_GroupMediaLibraries" %>
<%@ Register Src="~/CMSModules/MediaLibrary/Controls/LiveControls/MediaLibraries.ascx" TagName="MediaLibraries"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/System/PermissionMessage.ascx" TagName="PermissionMessage"
    TagPrefix="cms" %>
<cms:PermissionMessage ID="messageElem" runat="server" Visible="false" EnableViewState="false" />
<cms:MediaLibraries ID="librariesElem" runat="server" />