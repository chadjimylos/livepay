<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ExportGridView.ascx.cs"
    Inherits="CMSModules_ImportExport_Controls_ExportGridView" %>

<script type="text/javascript">
//<![CDATA[
    // Check specified checkboxes
    function egvSelectCheckboxes(checkbox,classname)
    {
        var checkboxes = document.getElementsByTagName("input");
        for(var i = 0;i<checkboxes.length;i++)
        {
            if(checkboxes[i].className == classname)
            {
                if(checkbox.checked)
                {
                    checkboxes[i].checked=true;
                }
                else
                {
                    checkboxes[i].checked=false;
                }
            }
                
        }
        return false;
    }

    // Change checkboxes states
    function egvChangeCheckboxes(className, state, siteObject, clientId)
    {
        function egvModify(state)
        {
            var checkboxes = document.getElementsByTagName("input");
            for(var i = 0;i<checkboxes.length;i++)
            {
                if(checkboxes[i].className == className)
                {
                    checkboxes[i].checked = state;
                }
            }
        }
        
        // Selct all / Unselect all
        if(state!=-1)
        {
            var checked = (state==1);
            egvModify(checked);
        }
        // Select default
        else
        {
            egvModify(false);
            if((exportDefaultObjectsValues!=null) && (exportDefaultSiteObjectsValues!=null))
            {
                if(siteObject)
                {
                    var codeNames = exportDefaultSiteObjectsValues[className];
                    if(codeNames!=null)
                    {
                        for(var i = 0;i<codeNames.length;i++)
                        {
                            var element = document.getElementById(codeNames[i]);
                            if(element!=null)
                            {
                                element.checked = true;
                            }
                        }
                    }
                }
                else
                {
                    var codeNames = exportDefaultObjectsValues[className];
                    if(codeNames!=null)
                    {
                        for(var i = 0;i<codeNames.length;i++)
                        {
                            var element = document.getElementById(codeNames[i]);
                            if(element!=null)
                            {
                                element.checked = true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
//]]>
</script>

<asp:PlaceHolder ID="plcGrid" runat="server">
    <asp:Panel ID="pnlGrid" runat="server">
        <asp:PlaceHolder runat="server" ID="plcObjects">
            <asp:Panel ID="pnlLinks" runat="server">
                &nbsp;
                <asp:LinkButton ID="btnAll" runat="Server" />&nbsp;
                <asp:LinkButton ID="btnNone" runat="Server" />&nbsp;
                <asp:LinkButton ID="btnDefault" runat="Server" />
                <br />
                <br />
            </asp:Panel>
            <asp:Label ID="lblCategoryCaption" runat="Server" />
            <asp:GridView ID="gvObjects" runat="server" AutoGenerateColumns="False" GridLines="Horizontal"
                CssClass="UniGridGrid" CellPadding="1" Width="100%">
                <HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="UniGridHead" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle Width="50" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" Wrap="false" />
                        <ItemTemplate>
                            <input type="checkbox" class='<%# ValidationHelper.GetIdentificator(ObjectType) %>'
                                id='<%# preposition + ValidationHelper.GetIdentificator(Eval(codeNameColumnName)).ToLower().Replace("'","\'") %>'
                                name='<%# preposition + HttpUtility.HtmlEncode(ValidationHelper.GetString(Eval(codeNameColumnName), "")) %>' <%# IsChecked(Eval(codeNameColumnName)) %> />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle Width="100%" />
                        <ItemStyle Wrap="false" />
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" ToolTip='<%# HttpUtility.HtmlEncode(ValidationHelper.GetString(Eval(codeNameColumnName), "")) %>' Text='<%# HttpUtility.HtmlEncode(TextHelper.LimitLength(GetName(Eval(codeNameColumnName), Eval(displayNameColumnName)), 75)) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="OddRow" />
                <AlternatingRowStyle CssClass="EvenRow" />
            </asp:GridView>
        </asp:PlaceHolder>
        <asp:Label ID="lblNoData" runat="Server" /><br />
        <asp:PlaceHolder runat="server" ID="plcTasks">
            <br />
            <strong>
                <asp:Label ID="lblTasks" runat="Server" /></strong>
            <br />
            <br />
                &nbsp;
                <asp:LinkButton ID="btnAllTasks" runat="Server" />&nbsp;
                <asp:LinkButton ID="btnNoneTasks" runat="Server" /><br />
            <br />
            <asp:GridView ID="gvTasks" runat="server" AutoGenerateColumns="False" GridLines="Horizontal"
                CssClass="UniGridGrid" CellPadding="1" Width="100%">
                <HeaderStyle HorizontalAlign="Left" Wrap="False" CssClass="UniGridHead" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle Width="50" HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <input type="checkbox" class='export_task' id='<%# taskPreposition + Eval("TaskID") %>'
                                name='<%# taskPreposition + Eval("TaskID") %>' <%# IsTaskChecked(Eval("TaskID")) %> />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle Width="100%" />
                        <ItemStyle Wrap="false" />
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" ToolTip='<%# HttpUtility.HtmlEncode(ValidationHelper.GetString(Eval("TaskTitle"), "")) %>' Text='<%# HttpUtility.HtmlEncode(TextHelper.LimitLength(ValidationHelper.GetString(Eval("TaskTitle"), ""), 60)) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TaskType">
                        <ItemStyle Wrap="false" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TaskTime">
                        <ItemStyle Wrap="false" />
                    </asp:BoundField>
                </Columns>
                <RowStyle CssClass="OddRow" />
                <AlternatingRowStyle CssClass="EvenRow" />
            </asp:GridView>
        </asp:PlaceHolder>
    </asp:Panel>
    <br />
    <br />
</asp:PlaceHolder>