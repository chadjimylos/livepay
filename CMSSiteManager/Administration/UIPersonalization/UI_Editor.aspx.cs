using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.SiteProvider;
using CMS.GlobalHelper;

public partial class CMSSiteManager_Administration_UIPersonalization_UI_Editor : CMSUIPersonalizationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.editElem.SiteID = this.GetSiteID(QueryHelper.GetString("siteid", ""));
        ResourceInfo ri = ResourceInfoProvider.GetResourceInfo("CMS.WYSIWYGEditor");
        if (ri != null)
        {
            this.editElem.ResourceID = ri.ResourceId;
            this.editElem.IsLiveSite = false;
        }
    }
}
