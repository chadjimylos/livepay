using System;
using System.Web.UI;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;
using CMS.FormControls;

public partial class CMSModules_Content_CMSDesk_Properties_MetaData : CMSPropertiesPage
{
    #region "Protected variables"

    protected int nodeId = 0;
    protected TreeNode node = null;
    protected TreeProvider tree = null;
    protected bool hasModifyPermission = true;
    protected string siteName = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Indicates whether the asynchronous postback occurs on the page.
    /// </summary>
    private bool IsAsyncPostback
    {
        get
        {
            return ScriptManager.GetCurrent(this.Page).IsInAsyncPostBack;
        }
    }

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        this.tagGroupSelectorElem.StopProcessing = this.pnlUITags.IsHidden;
        this.tagSelectorElem.StopProcessing = this.pnlUITags.IsHidden;

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.MetaData"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.MetaData");
        }

        // Redirect to information page when no UI elements displayed
        if (this.pnlUIPage.IsHidden && this.pnlUITags.IsHidden)
        {
            RedirectToUINotAvailable();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        UIContext.PropertyTab = PropertyTabEnum.Metadata;

        // Register the scripts
        ScriptHelper.RegisterProgress(this.Page);

        tagGroupSelectorElem.AddNoneItemsRecord = false;
        tagGroupSelectorElem.UseAutoPostback = true;
        tagGroupSelectorElem.UseGroupNameForSelection = false;
        tagSelectorElem.IsLiveSite = false;

        // Get the document
        nodeId = QueryHelper.GetInteger("nodeid", 0);
        tree = new TreeProvider(CMSContext.CurrentUser);
        node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, tree);

        if (node != null)
        {
            SiteInfo si = SiteInfoProvider.GetSiteInfo(node.NodeSiteID);
            if (si != null)
            {
                siteName = si.SiteName;
            }
        }

        this.menuElem.Node = node;

        SetCheckBoxes();

        ltlScript.Text = "";

        lblPageDescription.Text = ResHelper.GetString("PageProperties.Decription");
        lblPageKeywords.Text = ResHelper.GetString("PageProperties.Keywords");
        lblPageTitle.Text = ResHelper.GetString("PageProperties.Title");
        lblTagSelector.Text = ResHelper.GetString("PageProperties.Tags");
        lblTagGroupSelector.Text = ResHelper.GetString("PageProperties.TagGroup");
        chkDescription.Text = ResHelper.GetString("Metadata.Inherit");
        chkKeyWords.Text = ResHelper.GetString("Metadata.Inherit");
        chkTitle.Text = ResHelper.GetString("Metadata.Inherit");
        chkTagGroupSelector.Text = ResHelper.GetString("Metadata.Inherit");

        pnlPageSettings.GroupingText = ResHelper.GetString("content.metadata.pagesettings");
        pnlTags.GroupingText = ResHelper.GetString("content.metadata.tags");

        btnOk.Text = ResHelper.GetString("general.ok");

        menuElem.OnBeforeCheckIn += new EventHandler(menuElem_OnBeforeCheckIn);
        menuElem.OnBeforeApprove += new EventHandler(menuElem_OnBeforeApprove);
        menuElem.OnBeforeReject += new EventHandler(menuElem_OnBeforeReject);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (menuElem.AllowEdit && menuElem.AllowSave && hasModifyPermission)
        {
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
                "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>"
            );
            pnlForm.Enabled = true;
        }
        else
        {
            pnlForm.Enabled = false;
        }

        lblWorkflowInfo.Text = menuElem.WorkflowInfo;
        lblWorkflowInfo.Visible = (lblWorkflowInfo.Text != "");

        // Get the document with valid data if under the workflow
        if (IsPostBack && lblWorkflowInfo.Visible)
        {
            menuElem.ReloadMenu(false);
        }

        if (!IsAsyncPostback)
        {
            ReloadData();
        }

        if (!this.pnlUITags.IsHidden)
        {
            if (!this.tagGroupSelectorElem.UniSelector.HasData)
            {
                // Hide tag controls
                lblTagGroupSelector.Visible = false;
                tagGroupSelectorElem.Visible = false;
                chkTagGroupSelector.Visible = false;
                lblTagSelector.Visible = false;
                tagSelectorElem.Visible = false;

                // Show tag info
                lblTagInfo.Text = ResHelper.GetString("PageProperties.TagsInfo");
                lblTagInfo.Visible = true;
            }
            else
            {
                // Set the tag selector
                int tagGroup = ValidationHelper.GetInteger(this.tagGroupSelectorElem.Value, 0);
                if (tagGroup <= 0)
                {
                    this.tagSelectorElem.Enabled = false;
                    this.tagSelectorElem.Value = "";
                }
                else
                {
                    this.tagSelectorElem.SetValue("GroupID", tagGroup);
                }
            }
        }
    }

    #endregion


    #region "Protected methods"

    protected void SetCheckBoxes()
    {
        if (RequestHelper.IsPostBack())
        {
            return;
        }

        if ((node != null) && (node.NodeAliasPath == "/"))
        {
            chkDescription.Visible = false;
            chkKeyWords.Visible = false;
            chkTitle.Visible = false;
            chkTagGroupSelector.Visible = false;
        }
    }


    private void ReloadData()
    {
        if (node != null)
        {
            bool isRoot = (node.NodeAliasPath == "/");

            // Check read permissions
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
            }
            // Check modify permissions
            else if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
            {
                hasModifyPermission = false;
                pnlForm.Enabled = false;
                tagSelectorElem.Enabled = false;
                lblWorkflowInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
            }

            // Load the inherited values
            TreeNode tmpNode = node.Clone();

            tmpNode.SetValue("DocumentPageTitle", DBNull.Value);
            tmpNode.SetValue("DocumentPageKeyWords", DBNull.Value);
            tmpNode.SetValue("DocumentPageDescription", DBNull.Value);
            tmpNode.SetValue("DocumentTagGroupID", DBNull.Value);
            SiteInfo si = SiteInfoProvider.GetSiteInfo(node.NodeSiteID);
            if (si != null)
            {
                tmpNode.LoadInheritedValues(new string[] { "DocumentPageTitle", "DocumentPageKeyWords", "DocumentPageDescription", "DocumentTagGroupID" }, SiteInfoProvider.CombineWithDefaultCulture(si.SiteName));
            }

            if (!this.pnlUIPage.IsHidden)
            {
                // Page title
                if (node.GetValue("DocumentPageTitle") != null)
                {
                    txtTitle.Text = node.GetValue("DocumentPageTitle").ToString();
                }
                else
                {
                    if (!isRoot)
                    {
                        txtTitle.Enabled = false;
                        chkTitle.Checked = true;
                        txtTitle.Text = ValidationHelper.GetString(tmpNode.GetValue("DocumentPageTitle"), "");
                    }
                }

                // Page key words
                if (node.GetValue("DocumentPageKeyWords") != null)
                {
                    txtKeywords.Text = node.GetValue("DocumentPageKeyWords").ToString();
                }
                else
                {
                    if (!isRoot)
                    {
                        txtKeywords.Enabled = false;
                        chkKeyWords.Checked = true;
                        txtKeywords.Text = ValidationHelper.GetString(tmpNode.GetValue("DocumentPageKeyWords"), "");
                    }
                }

                // Page description
                if (node.GetValue("DocumentPageDescription") != null)
                {
                    txtDescription.Text = node.GetValue("DocumentPageDescription").ToString();
                }
                else
                {
                    if (!isRoot)
                    {
                        txtDescription.Enabled = false;
                        chkDescription.Checked = true;
                        txtDescription.Text = ValidationHelper.GetString(tmpNode.GetValue("DocumentPageDescription"), "");
                    }
                }
            }

            if (!this.pnlUITags.IsHidden)
            {
                // Tag group
                if (node.GetValue("DocumentTagGroupID") != null)
                {
                    object tagGroupId = node.GetValue("DocumentTagGroupID");
                    tagGroupSelectorElem.Value = tagGroupId;
                }
                else
                {
                    if (!isRoot)
                    {
                        // Get the inherited tag group
                        int tagGroup = ValidationHelper.GetInteger(tmpNode.GetValue("DocumentTagGroupID"), 0);
                        if (tagGroup > 0)
                        {
                            tagGroupSelectorElem.Value = tagGroup;
                        }
                        else
                        {
                            tagGroupSelectorElem.AddNoneItemsRecord = true;
                        }
                        tagGroupSelectorElem.Enabled = false;
                        chkTagGroupSelector.Checked = true;
                    }
                    else
                    {
                        tagGroupSelectorElem.AddNoneItemsRecord = true;
                    }
                }

                // Tags
                tagSelectorElem.Value = node.DocumentTags;
            }
        }
    }

    #endregion


    #region "Control handling"

    void menuElem_OnBeforeCheckIn(object sender, EventArgs e)
    {
        btnOK_Click(sender, e);
        lblInfo.Text += "<br />" + ResHelper.GetString("ContentEdit.WasCheckedIn");
        lblInfo.Visible = true;
    }


    void menuElem_OnBeforeReject(object sender, EventArgs e)
    {
        btnOK_Click(sender, e);
    }


    void menuElem_OnBeforeApprove(object sender, EventArgs e)
    {
        btnOK_Click(sender, e);
    }


    /// <summary>
    /// Updates node properties in database
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (node != null)
        {
            bool autoCheck = !VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName);
            VersionManager vm = null;
            bool useWorkflow = false;
            WorkflowManager workflowMan = new WorkflowManager(tree);

            // Test if the document uses workflow
            if (workflowMan.GetNodeWorkflowScope(node) != null)
            {
                useWorkflow = true;
            }

            // If not using check-in/check-out, check out automatically
            if (autoCheck & useWorkflow)
            {
                if (node != null)
                {
                    // Check out
                    vm = new VersionManager(tree);
                    vm.CheckOut(node);
                }
            }

            // Update the data
            if (node != null)
            {
                if (!this.pnlUIPage.IsHidden)
                {
                    node.SetValue("DocumentPageTitle", null);
                    if (!chkTitle.Checked)
                    {
                        node.SetValue("DocumentPageTitle", txtTitle.Text);
                    }

                    node.SetValue("DocumentPageKeyWords", null);
                    if (!chkKeyWords.Checked)
                    {
                        node.SetValue("DocumentPageKeyWords", txtKeywords.Text);
                    }

                    node.SetValue("DocumentPageDescription", null);
                    if (!chkDescription.Checked)
                    {
                        node.SetValue("DocumentPageDescription", txtDescription.Text);
                    }
                }

                if (!this.pnlUITags.IsHidden)
                {
                    node.SetValue("DocumentTagGroupID", null);
                    if (!chkTagGroupSelector.Checked)
                    {
                        // If is tag group id
                        if (ValidationHelper.GetInteger(tagGroupSelectorElem.Value, 0) > 0)
                        {
                            node.SetValue("DocumentTagGroupID", tagGroupSelectorElem.Value);
                        }
                        else
                        {
                            node.SetValue("DocumentTagGroupID", DBNull.Value);
                        }
                    }

                    if (ValidationHelper.GetInteger(tagGroupSelectorElem.Value, 0) > 0)
                    {
                        node.SetValue("DocumentTags", tagSelectorElem.Value);
                    }
                    else
                    {
                        node.SetValue("DocumentTags", DBNull.Value);
                    }
                }

                DocumentHelper.UpdateDocument(node, tree);

                // Check in the document
                if (autoCheck & useWorkflow)
                {
                    if (vm != null)
                    {
                        vm.CheckIn(node, null, null);
                    }
                }
            }

            menuElem.Node = node;
            menuElem.ReloadMenu(false);

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");

            // Reload tags
            if (node != null)
            {
                tagSelectorElem.Value = node.DocumentTags;
            }
        }
    }


    protected void chkTitle_CheckedChanged(object sender, EventArgs e)
    {
        if (chkTitle.Checked)
        {
            txtTitle.Enabled = false;
            if (!String.IsNullOrEmpty(siteName))
            {
                txtTitle.Text = ValidationHelper.GetString(node.GetInheritedValue("DocumentPageTitle", SiteInfoProvider.CombineWithDefaultCulture(siteName)), "");
            }
        }
        else
        {
            txtTitle.Enabled = true;
        }
    }


    protected void chkDescription_CheckedChanged(object sender, EventArgs e)
    {
        if (chkDescription.Checked)
        {
            txtDescription.Enabled = false;
            if (!String.IsNullOrEmpty(siteName))
            {
                txtDescription.Text = ValidationHelper.GetString(node.GetInheritedValue("DocumentPageDescription", SiteInfoProvider.CombineWithDefaultCulture(siteName)), "");
            }
        }
        else
        {
            txtDescription.Enabled = true;
        }
    }


    protected void chkKeyWords_CheckedChanged(object sender, EventArgs e)
    {
        if (chkKeyWords.Checked)
        {
            txtKeywords.Enabled = false;
            if (!String.IsNullOrEmpty(siteName))
            {
                txtKeywords.Text = ValidationHelper.GetString(node.GetInheritedValue("DocumentPageKeyWords", SiteInfoProvider.CombineWithDefaultCulture(siteName)), "");
            }
        }
        else
        {
            txtKeywords.Enabled = true;
        }
    }


    protected void chkTagGroupSelector_CheckedChanged(object sender, EventArgs e)
    {
        if (chkTagGroupSelector.Checked)
        {
            tagGroupSelectorElem.Enabled = false;

            if (!String.IsNullOrEmpty(siteName))
            {
                int value = ValidationHelper.GetInteger(node.GetInheritedValue("DocumentTagGroupID", SiteInfoProvider.CombineWithDefaultCulture(siteName)), 0);
                if (value == 0)
                {
                    tagGroupSelectorElem.AddNoneItemsRecord = true;
                }
                tagGroupSelectorElem.Value = value;
                tagGroupSelectorElem.ReloadData();
            }
        }
        else
        {
            tagGroupSelectorElem.Enabled = true;
        }
    }

    #endregion
}
