﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.EmailEngine;
using CMS.EventLog;
using CMS.DataEngine;
using CMS.WebAnalytics;
using CMS.LicenseProvider;
using CMS.PortalEngine;
using CMS.SettingsProvider;

public partial class CMSWebParts_AegeanPower_ChangeYourData: CMSAbstractWebPart
{
    #region "Text properties"

    /// <summary>
    /// Gets or sets the Skin ID
    /// </summary>
    public override string SkinID
    {
        get
        {
            return base.SkinID;
        }
        set
        {
            base.SkinID = value;
            SetSkinID(value);
        }
    }


    /// <summary>
    /// Gets or sets the first name text
    /// </summary>
    public string FirstNameText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("FirstNameText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.FirstName$}"));
        }
        set
        {
            this.SetValue("FirstNameText", value);
            lblFirstName.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the last name text
    /// </summary>
    public string LastNameText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("LastNameText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.LastName$}"));
        }
        set
        {
            this.SetValue("LastNameText", value);
            lblLastName.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the e-mail text
    /// </summary>
    public string EmailText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("EmailText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Email$}"));
        }
        set
        {
            this.SetValue("EmailText", value);
            lblEmail.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the password text
    /// </summary>
    public string OldPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("OldPasswordText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.OldPassword$}"));
        }
        set
        {
            this.SetValue("OldPasswordText", value);
            lblOldPassword.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the password text
    /// </summary>
    public string NewPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("NewPasswordText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.NewPassword$}"));
        }
        set
        {
            this.SetValue("PasswordText", value);
            lblNewPassword.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the confirmation password text
    /// </summary>
    public string ConfirmPasswordText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ConfirmPasswordText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.ConfirmPassword$}"));
        }
        set
        {
            this.SetValue("ConfirmPasswordText", value);
            lblConfirmPassword.Text = value;
        }
    }


    /// <summary>
    /// Gets or sets the button text
    /// </summary>
    public string ButtonText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ButtonText"), ResHelper.LocalizeString("{$Webparts_Membership_RegistrationForm.Button$}"));
        }
        set
        {
            this.SetValue("ButtonText", value);
            btnOk.Text = value;
        }
    }

    /// <summary>
    /// Gets or sets registration approval page URL
    /// </summary>

    #endregion


    #region "Registration properties"

    /// <summary>
    /// Gets or sets the password minimal length
    /// </summary>
    public int PasswordMinLength
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("PasswordMinLength"), 0);
        }
        set
        {
            this.SetValue("PasswordMinLength", 0);
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }

    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do not process
            rfvFirstName.Enabled = false;
            rfvOldPassword.Enabled = false;
            rfvNewPassword.Enabled = false;
            rfvConfirmPassword.Enabled = false;
            rfvLastName.Enabled = false;
        }
        else
        {
            UserEmail.Text = CMSContext.CurrentUser.Email;
            txtFirstName.Text = CMSContext.CurrentUser.FirstName;
            txtLastName.Text = CMSContext.CurrentUser.LastName;

            // Set texts
            lblFirstName.Text = this.FirstNameText;
            lblLastName.Text = this.LastNameText;
            lblEmail.Text = this.EmailText;
            lblOldPassword.Text = this.OldPasswordText;
            lblNewPassword.Text = this.NewPasswordText;
            lblConfirmPassword.Text = this.ConfirmPasswordText;
            btnOk.Text = this.ButtonText;
            // Set required field validators texts
            rfvFirstName.ErrorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.rfvFirstName");
            rfvLastName.ErrorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.rfvLastName");
            rfvOldPassword.ErrorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.rfvPassword");
            rfvNewPassword.ErrorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.rfvPassword");
            rfvConfirmPassword.ErrorMessage = ResHelper.GetString("Webparts_Membership_RegistrationForm.rfvConfirmPassword");

            // Set SkinID
            if (!this.StandAlone && (this.PageCycle < PageCycleEnum.Initialized))
            {
                SetSkinID(this.SkinID);
            }

        }
    }


    /// <summary>
    /// Sets SkinID
    /// </summary>
    void SetSkinID(string skinId)
    {
        if (skinId != "")
        {
            lblFirstName.SkinID = skinId;
            lblLastName.SkinID = skinId;
            lblEmail.SkinID = skinId;
            lblOldPassword.SkinID = skinId;
            lblNewPassword.SkinID = skinId;
            lblConfirmPassword.SkinID = skinId;
            txtFirstName.SkinID = skinId;
            txtLastName.SkinID = skinId;
            //txtEmail.SkinID = skinId;
            txtOldPassword.SkinID = skinId;
            txtNewPassword.SkinID = skinId;
            txtConfirmPassword.SkinID = skinId;
            btnOk.SkinID = skinId;
        }
    }


    /// <summary>
    /// OK click handler (Proceed registration)
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        //string correctcode = CMSContext.CurrentUser.UserPasswordFormat;
        if ((this.PageManager.ViewMode == ViewModeEnum.Design) || (this.HideOnCurrentPage) || (!this.IsVisible))
        {
            // Do not process
        }
        else
        {
            #region "Banned IPs"

            // Ban IP addresses which are blocked for registration
            if (!BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.Registration))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("banip.ipisbannedregistration");
                return;
            }

            #endregion

            #region "Check username"
            UserInfo user = CMSContext.CurrentUser;
            if (user != null)
            {
                user.FirstName = txtFirstName.Text;
                user.LastName = txtLastName.Text;
                user.FullName = txtFirstName.Text + " " + txtLastName.Text;
                UserInfoProvider.SetUserInfo(user);
            }
            #endregion

            #region "Check Email & password"
                // Get current user info object
                CurrentUserInfo ui = CMSContext.CurrentUser;
                // Get current site info object
                CurrentSiteInfo si = CMSContext.CurrentSite;
                
                if ((ui != null) && (si != null))
                {
                    //UserInfo user = CMSContext.CurrentUser;

                    //user.FirstName = txtFirstName.Text;
                    string userName = ui.UserName;
                    string siteName = si.SiteName;

                    //ui.SetValue("FirstName", txtFirstName.Text);
                    //ui.SetValue("LastName", txtLastName.Text);


                    // new password correctly filled
                    if (txtConfirmPassword.Text == txtNewPassword.Text)
                    {
                        // Old password match
                        if (UserInfoProvider.AuthenticateUser(userName, txtOldPassword.Text.Trim(), siteName) != null)
                        {
                            UserInfoProvider.SetPassword(userName, txtNewPassword.Text.Trim());
                            //lblError.Visible = true;
                            //lblError.Text = ResHelper.LocalizeString("{$=Η ενημέρωση του προφίλ σας έγινε με επιτυχία.|en-us=Η ενημέρωση του προφίλ σας έγινε με επιτυχία$}");//ResHelper.GetString("ChangePassword.ChangesSaved");
                            lblSucces.Visible = true;
                            lblSucces.Text = ResHelper.LocalizeString("{$=Η ενημέρωση του προφίλ σας έγινε με επιτυχία.|en-us=Η ενημέρωση του προφίλ σας έγινε με επιτυχία$}");
                        }
                        else
                        {
                            lblError.Visible = true;
                            lblError.Text = ResHelper.LocalizeString("{$=Δεν ταιριάζει ο κωδικός πρόσβασης.|en-us=Δεν ταιριάζει ο κωδικός πρόσβασης.$}"); //ResHelper.GetString("ChangePassword.ErrorOldPassword");
                        }
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.LocalizeString("{$=Δεν ταιριάζουν οι κωδικοί πρόσβασης.|en-us=Δεν ταιριάζουν οι κωδικοί πρόσβασης.$}");
                    }
                    //UserInfoProvider.SetUserInfo(user);
                }
            
            #endregion
        }
    }

    #endregion
}
