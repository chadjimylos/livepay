<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SecurityAddRoles.ascx.cs"
    Inherits="CMSModules_Membership_FormControls_Roles_SecurityAddRoles" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="usRoles" runat="server" IsLiveSite="false" ObjectType="cms.role"
            ResourcePrefix="addroles" SelectionMode="MultipleButton" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
