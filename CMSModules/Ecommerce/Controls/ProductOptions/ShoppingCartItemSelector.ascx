<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShoppingCartItemSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_Controls_ProductOptions_ShoppingCartItemSelector" %>
<cms:CMSUpdatePanel ID="upnlAjax" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlContainer" runat="server" CssClass="CartItemSelectorContainer">
            <asp:Panel ID="pnlSelectors" runat="server" CssClass="CartItemSelector" />
            <asp:Panel ID="pnlPrice" runat="server" CssClass="TotalPriceContainer" EnableViewState="false">
                <asp:Label ID="lblPrice" runat="server" CssClass="TotalPriceLabel" EnableViewState="false" />
                <asp:Label ID="lblPriceValue" runat="server" CssClass="TotalPrice" EnableViewState="false" />
            </asp:Panel>
            <asp:Panel ID="pnlButton" runat="server" CssClass="AddToCartContainer"
                EnableViewState="false">
                <div class="FloatRight">
                    <asp:ImageButton ID="btnWishlist" runat="server" Visible="false" CssClass="AddToWishlistImageButton" />
                    <asp:LinkButton ID="lnkWishlist" runat="server" Visible="false" CssClass="AddToWishlistLink" />
                    <cms:LocalizedLabel ID="lblUnits" runat="server" Visible="false" ResourceString="ecommerce.shoppingcartcontent.skuunits" />
                    <asp:TextBox ID="txtUnits" runat="server" Visible="false" CssClass="AddToCartTextBox"
                        MaxLength="9" />
                    <cms:CMSButton ID="btnAdd" runat="server" Visible="false" CssClass="SubmitButton AddToCartButton" />
                    <asp:ImageButton ID="btnAddImage" runat="server" Visible="false" CssClass="AddToCartImageButton"
                        OnClick="btnAddImage_Click" />
                    <asp:LinkButton ID="lnkAdd" runat="server" Visible="false" CssClass="AddToCartLink" />
                </div>
            </asp:Panel>
        </asp:Panel>
        <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
