<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebFarm_Server_List.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Web farm server - List"
    Inherits="CMSSiteManager_Administration_WebFarm_WebFarm_Server_List" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="plcBeforeContent" runat="server">
    <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false" />
        <cms:UniGrid runat="server" ID="UniGrid" GridName="WebFarmServers_List.xml" OrderBy="ServerDisplayName"
            IsLiveSite="false" />
    </asp:Panel>
</asp:Content>
