using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.UIControls;

public partial class CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_Documents : SiteManagerPage
{
    #region "Private variables"

    private int documentTypeId = 0;
    private SiteInfo siteInfo = null;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        filterDocuments.OnSiteSelectionChanged += UniSelector_OnSelectionChanged;
        filterDocuments.ClassPlaceHolder.Visible = false;

        // Get current page template ID
        documentTypeId = QueryHelper.GetInteger("documenttypeid", 0);

        ucDocuments.OnExternalDataBound += ucDocuments_OnExternalDataBound;
        ucDocuments.ZeroRowsText = ResHelper.GetString("DocumentType_Edit_General.Documents.nodata");
        ucDocuments.OnDataReload += new UniGrid.OnDataReloadEventHandler(ucDocuments_OnDataReload);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (filterDocuments.SelectedSite != TreeProvider.ALL_SITES)
        {
            ucDocuments.GridView.Columns[1].Visible = false;
        }
    }

    #endregion


    #region "Events handling"

    protected object ucDocuments_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView drv = GetDataRowView(sender as DataControlFieldCell);
        string siteName = ValidationHelper.GetString(drv["SiteName"], string.Empty);

        // Get document site
        siteInfo = SiteInfoProvider.GetSiteInfo(siteName);

        switch (sourceName.ToLower())
        {
            case "documentname":
                // If the document path is empty alter it with the default '/'
                string documentName = TextHelper.LimitLength(ValidationHelper.GetString(drv["DocumentName"], string.Empty), 50);
                int nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                // Generate link to the document
                return GetDocumentLink(documentName, siteInfo, nodeId);

            case "documentnametooltip":
                drv = (DataRowView)parameter;
                return UniGridFunctions.DocumentNameTooltip(drv);

            case "sitename":
                return HTMLHelper.HTMLEncode(siteInfo.DisplayName);

            default:
                return parameter;
        }
    }


    protected DataSet ucDocuments_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        TreeProvider tp = new TreeProvider(CMSContext.CurrentUser);
        tp.UseCache = false;
        string selectedSite = filterDocuments.SelectedSite;

        string where = "ClassName IN (SELECT ClassName FROM CMS_Class WHERE ClassID =" + documentTypeId + ")";
        where = SqlHelperClass.AddWhereCondition(where, completeWhere);
        where = SqlHelperClass.AddWhereCondition(where, filterDocuments.WhereCondition);

        // Get documents
        DataSet ds = tp.SelectNodes(selectedSite, "/%", TreeProvider.ALL_CULTURES, true, null, where, "DocumentName, SiteName", -1, false, currentTopN, "DocumentName, DocumentNamePath, NodeID, DocumentCulture, SiteName");

        totalRecords = DataHelper.GetItemsCount(ds);
        return ds;
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        pnlUpdate.Update();
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private static object GetDocumentLink(string documentName, SiteInfo si, int nodeId)
    {
        if (string.IsNullOrEmpty(documentName))
        {
            documentName = "-";
        }

        if (si.Status == SiteStatusEnum.Running)
        {
            string url = string.Empty;
            if (si.SiteName != CMSContext.CurrentSiteName)
            {
                // Make url for site in form 'http(s)://sitedomain/application/cmsdesk'.
                url = UrlHelper.GetApplicationUrl(si.DomainName) + "/cmsdesk/default.aspx?section=content&nodeid=" + nodeId;
            }
            else
            {
                // Make url for current site (with port)
                url = UrlHelper.ResolveUrl("~/cmsdesk/default.aspx?section=content&nodeid=" + nodeId);
            }
            return "<a href=\"" + url + "\" target=\"_blank\">" + HTMLHelper.HTMLEncode(documentName) + "</a>";
        }
        return HTMLHelper.HTMLEncode(documentName);
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }

    #endregion
}
