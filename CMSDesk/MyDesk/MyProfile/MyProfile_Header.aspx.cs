using System;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.LicenseProvider;

public partial class CMSDesk_MyDesk_MyProfile_MyProfile_Header : CMSMyProfilePage
{
    private int selectedTabIndex = 0;

    protected void Page_PreInit(object sender, EventArgs e)
    {
        this["TabControl"] = tabElem;

        pnlLeft.CssClass = "TabsLeft";
        pnlRight.CssClass = "TabsRight";        
    }

    
    protected void Page_Load(object sender, EventArgs e)
    {
        this.titleElem.TitleText = ResHelper.GetString("MyDesk.MyProfileTitle");
        this.titleElem.TitleImage = GetImageUrl("CMSModules/CMS_MyDesk/MyProfile/module.png");
        this.titleElem.HelpName = "helpTopic";
        this.titleElem.HelpTopicName = "my_profile_details";

        // Initialize tabs
        this.tabElem.OnTabCreated += new UITabs.TabCreatedEventHandler(tabElem_OnTabCreated);
        this.tabElem.SelectedTab = 0;
        this.tabElem.UrlTarget = "myProfileContent";
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        this.tabElem.SelectedTab = QueryHelper.GetInteger("selectedtab", 0);

        // Call the script for tab which is selected
        if (!this.tabElem.TabsEmpty)
        {
            ltlScript.Text += ScriptHelper.GetScript(" redir('" + this.tabElem.Tabs[selectedTabIndex, 2] + Request.Url.Query + "','" + this.tabElem.UrlTarget + "'); " + this.tabElem.Tabs[selectedTabIndex, 1]);
        }
        else
        {
            ltlScript.Text += ScriptHelper.GetScript(" redir('" + UrlHelper.ResolveUrl("~/CMSMessages/Information.aspx") + "?message=" + ResHelper.GetString("uiprofile.uinotavailable") + "','" + this.tabElem.UrlTarget + "'); ");
        }
    }


    string[] tabElem_OnTabCreated(CMS.SiteProvider.UIElementInfo element, string[] parameters, int tabIndex)
    {
        switch (element.ElementName.ToLower())
        {
            case "myprofile.notifications":
                if (!LicenseHelper.IsFeautureAvailableInUI(FeatureEnum.Notifications, ModuleEntry.NOTIFICATIONS))
                {
                    return null;
                }
                break;
        }

        return parameters;
    }
}
