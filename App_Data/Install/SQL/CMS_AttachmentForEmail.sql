CREATE TABLE [CMS_AttachmentForEmail] (
		[EmailID]          int NOT NULL,
		[AttachmentID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_AttachmentForEmail]
	ADD
	CONSTRAINT [PK_CMS_AttachmentForEmail]
	PRIMARY KEY
	([EmailID], [AttachmentID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_AttachmentForEmail]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_EmailAttachment]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_AttachmentForEmail_AttachmentID_CMS_EmailAttachment]') IS NULL
BEGIN
		ALTER TABLE [CMS_AttachmentForEmail]
			ADD CONSTRAINT [FK_CMS_AttachmentForEmail_AttachmentID_CMS_EmailAttachment]
			FOREIGN KEY ([AttachmentID]) REFERENCES [CMS_EmailAttachment] ([AttachmentID])
END
IF OBJECT_ID(N'[CMS_AttachmentForEmail]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Email]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_AttachmentForEmail_EmailID_CMS_Email]') IS NULL
BEGIN
		ALTER TABLE [CMS_AttachmentForEmail]
			ADD CONSTRAINT [FK_CMS_AttachmentForEmail_EmailID_CMS_Email]
			FOREIGN KEY ([EmailID]) REFERENCES [CMS_Email] ([EmailID])
END
