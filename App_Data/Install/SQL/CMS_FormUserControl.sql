CREATE TABLE [CMS_FormUserControl] (
		[UserControlID]                      int NOT NULL IDENTITY(1, 1),
		[UserControlDisplayName]             nvarchar(200) NOT NULL,
		[UserControlCodeName]                nvarchar(200) NOT NULL,
		[UserControlFileName]                nvarchar(400) NOT NULL,
		[UserControlForText]                 bit NOT NULL,
		[UserControlForLongText]             bit NOT NULL,
		[UserControlForInteger]              bit NOT NULL,
		[UserControlForDecimal]              bit NOT NULL,
		[UserControlForDateTime]             bit NOT NULL,
		[UserControlForBoolean]              bit NOT NULL,
		[UserControlForFile]                 bit NOT NULL,
		[UserControlShowInBizForms]          bit NOT NULL,
		[UserControlDefaultDataType]         nvarchar(50) NOT NULL,
		[UserControlDefaultDataTypeSize]     int NULL,
		[UserControlShowInDocumentTypes]     bit NULL,
		[UserControlShowInSystemTables]      bit NULL,
		[UserControlShowInWebParts]          bit NULL,
		[UserControlShowInReports]           bit NULL,
		[UserControlGUID]                    uniqueidentifier NOT NULL,
		[UserControlLastModified]            datetime NOT NULL,
		[UserControlForGuid]                 bit NULL,
		[UserControlShowInCustomTables]      bit NULL,
		[UserControlForVisibility]           bit NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_FormUserControl]
	ADD
	CONSTRAINT [PK_CMS_FormUserControl]
	PRIMARY KEY
	NONCLUSTERED
	([UserControlID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_FormUserControl]
	ADD
	CONSTRAINT [DEFAULT_cms_FormUserControl_UserControlForGuid]
	DEFAULT ((0)) FOR [UserControlForGuid]
ALTER TABLE [CMS_FormUserControl]
	ADD
	CONSTRAINT [DEFAULT_cms_FormUserControl_UserControlForVisibility]
	DEFAULT ((0)) FOR [UserControlForVisibility]
ALTER TABLE [CMS_FormUserControl]
	ADD
	CONSTRAINT [DEFAULT_cms_FormUserControl_UserControlShowInCustomTables]
	DEFAULT ((0)) FOR [UserControlShowInCustomTables]
CREATE CLUSTERED INDEX [IX_CMS_FormUserControl_UserControlDisplayName]
	ON [CMS_FormUserControl] ([UserControlDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
