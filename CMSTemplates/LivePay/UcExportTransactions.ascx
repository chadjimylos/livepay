﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UcExportTransactions.ascx.vb" Inherits="CMSTemplates_LivePay_UcExportTransactions" %>
<%@ Register Src="~/CMSTemplates/LivePay/CustomFields/Date.ascx" TagName="date" TagPrefix="uc1" %>
<style>
    .row{margin:5px 0px}
    .fleft{float:left}
    .filter_name {width:100px; float:left; padding:3px 0px}
    .filter input, .filter select {
        border: 1px solid #ADAAA0;
        color: #000;
        margin: 0 0 0 3px;
        padding: 3px 5px;
        width: 134px;
    }
    .filter select {
        border: 1px solid #ADAAA0;
        color: #000;
        margin: 0 0 0 3px;
        padding: 2px 3px;
        width: 134px;
    }
    .filter a{text-decoration:underline; color:#000; font-weight:bold}
    .filter .error{color:Red;}
</style>
<div class="filter">
    <div class="row">
        <div class="filter_name">date from</div>
        <div><uc1:date runat="server" ID="ucDateFrom" /></div>
    </div>
    <div class="row">
        <div class="filter_name">date to</div>
        <div><uc1:date runat="server" ID="ucDateTo" /></div>
    </div>
    <div class="row">
        <div class="filter_name">Client IP</div>
        <div class="fleft"><asp:DropDownList CssClass="ExtraSmallDropDown" ID="drpFilterClientIP" runat="server" /></div>
        <div><asp:TextBox runat="server" ID="tbClientIP" MaxLength="15" /></div>
    </div>
    <div class="row">
        <div><asp:LinkButton runat="server" ID="lnkExportExcel" Text="Export Excel" /></div>
    </div>
</div>