<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Product_Edit_Options.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Products_Product_Edit_Options" Theme="Default" %>

<%@ Register Src="~/CMSModules/Ecommerce/Controls/UI/ProductOptions.ascx"
    TagName="ProductOptions" TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Product options</title>
    <style type="text/css">
		body
		{
			margin: 0px;
			padding: 0px;
			height:100%; 
			width: 100%;
            overflow: hidden;
		}
	</style>
</head>
<body class="<%=mBodyClass%>">
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="pnlBody" CssClass="TabsPageBody">
            <asp:Panel runat="server" ID="pnlContainer" CssClass="TabsPageContainer">
                <cms:ProductOptions ID="ucOptions" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
