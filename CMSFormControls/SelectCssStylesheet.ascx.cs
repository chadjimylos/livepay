using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.CMSHelper;

public partial class CMSFormControls_SelectCssStylesheet : CMS.FormControls.FormEngineUserControl
{
    #region "Private variables"

    private string mStylesheetCodeName = String.Empty;
    private bool mAddNoneRecord = true;
    private int mSiteId = 0;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the enabled state of the control
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return base.Enabled;
        }
        set
        {
            EnsureChildControls();
            base.Enabled = value;
            this.usStyleSheet.Enabled = value;
        }
    }


    /// <summary>
    /// Indicates whether "none record" should be added to the dropdownlist
    /// </summary>
    public bool AddNoneRecord
    {
        get
        {
            return mAddNoneRecord;
        }
        set
        {
            mAddNoneRecord = value;
        }
    }


    /// <summary>
    /// Gets the currene UniSelector instance
    /// </summary>
    public UniSelector CurrentSelector
    {
        get
        {
            EnsureChildControls();
            return this.usStyleSheet;
        }
    }


    /// <summary>
    /// Css stylesheet code name.
    /// </summary>
    public string StylesheetCodeName
    {
        get
        {
            EnsureChildControls();
            return Convert.ToString(usStyleSheet.Value);
        }
        set
        {
            EnsureChildControls();
            usStyleSheet.Value = value;
        }
    }


    /// <summary>
    /// Gets the current drop down control
    /// </summary>
    public DropDownList CurrentDropDown
    {
        get
        {
            EnsureChildControls();
            return usStyleSheet.DropDownSingleSelect;
        }
    }


    /// <summary>
    /// Get or sets stylesheet name.
    /// </summary>
    public override object Value
    {
        get
        {
            EnsureChildControls();
            return Convert.ToString(usStyleSheet.Value);
        }
        set
        {
            EnsureChildControls();
            usStyleSheet.Value = value;
        }
    }


    /// <summary>
    /// Gets ClientID of the dropdownlist with stylesheets
    /// </summary>
    public override string ValueElementID
    {
        get
        {
            EnsureChildControls();
            return usStyleSheet.ClientID;
        }
    }


    /// <summary>
    /// Gets or sets the site id. If set, only stylesheets of the site are displayed.
    /// </summary>
    public int SiteId
    {
        get
        {
            return mSiteId;
        }
        set
        {
            mSiteId = value;
        }
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Add "none record" if required
        if (this.AddNoneRecord && (usStyleSheet.SpecialFields == null))
        {
            usStyleSheet.SpecialFields = new string[1, 2] { { ResHelper.GetString("SelectCssStylesheet.NoneRecord"), String.Empty } };
        }

        // If site specified, restrict to stylesheets assigned to the site
        if (this.SiteId > 0)
        {
            usStyleSheet.WhereCondition = SqlHelperClass.AddWhereCondition(usStyleSheet.WhereCondition, "StylesheetID IN (SELECT StylesheetID FROM CMS_CssStylesheetSite WHERE SiteID = " + this.SiteId + ")");
        }

        // Check if user can edit the stylesheet
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        bool design = currentUser.IsAuthorizedPerResource("CMS.Content", "Design");
        bool uiElement = currentUser.IsAuthorizedPerUIElement("CMS.Content", new string[] { "Properties", "Properties.General", "General.Design" }, CMSContext.CurrentSiteName);

        if (design && uiElement && this.usStyleSheet.ReturnColumnName.Equals("StylesheetID", StringComparison.InvariantCultureIgnoreCase))
        {
            usStyleSheet.DropDownSingleSelect.CssClass = "SelectorDropDown";
            usStyleSheet.EditItemPageUrl = "~/CMSSiteManager/Development/CssStylesheets/CssStylesheet_General.aspx?cssstylesheetid=##ITEMID##&editonlycode=true";
        }
    }


    /// <summary>
    /// Reloads the selector's data
    /// </summary>
    /// <param name="forceReload">Indicates whether data should be forcibly reloaded</param>
    public void Reload(bool forceReload)
    {
        usStyleSheet.Reload(forceReload);        
    }


    /// <summary>
    /// Creates child controls and loads update panel container if it is required
    /// </summary>
    protected override void CreateChildControls()
    {
        // If selector is not defined load updat panel container
        if (usStyleSheet == null)
        {
            this.pnlUpdate.LoadContainer();
        }

        // Call base method
        base.CreateChildControls();
    }

    #endregion
}

