using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.FormEngine;
using CMS.LicenseProvider;
using CMS.DirectoryUtilities;
using CMS.FormControls;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.WorkflowEngine;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Ecommerce_Controls_UI_ProductEdit : FormEngineUserControl
{
    private string mProductName = "";
    private double mProductWeight = 0;
    private double mProductHeight = 0;
    private double mProductWidth = 0;
    private double mProductDepth = 0;
    private string mProductImagePath = "";
    private string mProductDescription = "";
    private double mProductPrice = 0;

    private int mOptionCategoryId = 0;
    private int mProductId = 0;
    private int mNodeId = 0;
    private bool mShowOkButton = true;

    private bool hasAttachmentImagePath = true;


    #region "Public properties"

    /// <summary>
    /// True if the OK button is enabled
    /// </summary>
    public bool ButtonEnabled
    {
        get
        {
            return btnOk.Enabled;
        }
        set
        {
            btnOk.Enabled = value;
        }
    }


    /// <summary>
    /// Product name
    /// </summary>
    public string ProductName
    {
        get
        {
            return mProductName;
        }
        set
        {
            mProductName = value;
        }
    }


    /// <summary>
    /// Product price
    /// </summary>
    public double ProductPrice
    {
        get
        {
            return mProductPrice;
        }
        set
        {
            mProductPrice = value;
        }
    }


    /// <summary>
    /// Product weight
    /// </summary>
    public double ProductWeight
    {
        get
        {
            return mProductWeight;
        }
        set
        {
            mProductWeight = value;
        }
    }


    /// <summary>
    /// Product height
    /// </summary>
    public double ProductHeight
    {
        get
        {
            return mProductHeight;
        }
        set
        {
            mProductHeight = value;
        }
    }


    /// <summary>
    /// Product width
    /// </summary>
    public double ProductWidth
    {
        get
        {
            return mProductWidth;
        }
        set
        {
            mProductWidth = value;
        }
    }


    /// <summary>
    /// Product depth
    /// </summary>
    public double ProductDepth
    {
        get
        {
            return mProductDepth;
        }
        set
        {
            mProductDepth = value;
        }
    }


    /// <summary>
    /// Product image path
    /// </summary>
    public string ProductImagePath
    {
        get
        {
            return mProductImagePath;
        }
        set
        {
            mProductImagePath = value;
        }
    }


    /// <summary>
    /// Product description
    /// </summary>
    public string ProductDescription
    {
        get
        {
            return mProductDescription;
        }
        set
        {
            mProductDescription = value;
        }
    }


    /// <summary>
    /// ProductID
    /// </summary>
    public int ProductID
    {
        get
        {
            return mProductId;
        }
        set
        {
            mProductId = value;
        }
    }


    /// <summary>
    /// NodeID
    /// </summary>
    public int NodeID
    {
        get
        {
            return mNodeId;
        }
        set
        {
            mNodeId = value;
        }
    }


    /// <summary>
    /// Option category ID
    /// </summary>
    public int OptionCategoryID
    {
        get
        {
            return mOptionCategoryId;
        }
        set
        {
            mOptionCategoryId = value;
        }
    }


    /// <summary>
    /// Show OK button.
    /// </summary>
    public bool ShowOkButton
    {
        get
        {
            return mShowOkButton;
        }
        set
        {
            mShowOkButton = value;
        }
    }


    /// <summary>
    /// Indicates whether form is enabled or not.
    /// </summary>
    public bool FormEnabled
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["FormEnabled"], true);
        }
        set
        {
            this.txtSKUAvailableInDays.Enabled = value;

            htmlTemplateBody.Enabled = value;

            //this.txtSKUDescription.Enabled = value;
            this.txtSKUName.Enabled = value;
            this.txtSKUNumber.Enabled = value;
            this.txtSKUPrice.Enabled = value;
            this.chkSKUEnabled.Enabled = value;
            this.departmentElem.Enabled = value;
            this.internalStatusElem.Enabled = value;
            this.manufacturerElem.Enabled = value;
            this.publicStatusElem.Enabled = value;
            this.supplierElem.Enabled = value;
            this.imgSelect.Enabled = false;
            this.txtSKUWeight.Enabled = value;
            this.txtSKUHeight.Enabled = value;
            this.txtSKUWidth.Enabled = value;
            this.txtSKUDepth.Enabled = value;
            this.txtSKUAvailableItems.Enabled = value;
            this.chkSKUSellOnlyAvailable.Enabled = value;
            this.ucMetaFile.Enabled = value;

            ViewState["FormEnabled"] = value;
        }
    }


    /// <summary>
    /// Error message describing error while input data
    /// </summary>
    public string ErrorMessage
    {
        get
        {
            return lblError.Text.Trim();
        }
        set
        {
            lblError.Text = value;
            lblError.Visible = (lblError.Text.Trim() != "");
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'EcommerceModify' permission
        if ((CMSContext.CurrentSite != null) && (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify")))
        {
            ucMetaFile.Enabled = false;
        }

        htmlTemplateBody.AutoDetectLanguage = false;
        htmlTemplateBody.DefaultLanguage = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        htmlTemplateBody.EditorAreaCSS = FormHelper.GetHtmlEditorAreaCss(CMSContext.CurrentSiteName);
        htmlTemplateBody.ToolbarSet = "Basic";

        // control initializations				
        lblSKUInternalStatusID.Text = ResHelper.GetString("com_SKU_edit_general.SKUInternalStatusIDLabel");
        lblSKUDepartmentID.Text = ResHelper.GetString("com_SKU_edit_general.SKUDepartmentIDLabel");
        lblSKUManufacturerID.Text = ResHelper.GetString("com_SKU_edit_general.SKUManufacturerIDLabel");
        lblSKUName.Text = ResHelper.GetString("com_SKU_edit_general.SKUNameLabel");
        lblSKUNumber.Text = ResHelper.GetString("com_SKU_edit_general.SKUNumberLabel");
        lblSKUAvailableInDays.Text = ResHelper.GetString("com_SKU_edit_general.SKUAvailableInDaysLabel");
        lblSKUPublicStatusID.Text = ResHelper.GetString("com_SKU_edit_general.SKUPublicStatusIDLabel");
        lblSKUSupplierID.Text = ResHelper.GetString("com_SKU_edit_general.SKUSupplierIDLabel");
        lblSKUPrice.Text = ResHelper.GetString("com_SKU_edit_general.SKUPriceLabel");
        lblSKUImagePath.Text = ResHelper.GetString("NewProduct.lblSKUImagePath");
        lblSKUMetaFile.Text = ResHelper.GetString("NewProduct.lblSKUMetaFile");
        lblSKUWeight.Text = ResHelper.GetString("NewProduct.lblSKUWeight");
        lblSKUHeight.Text = ResHelper.GetString("NewProduct.lblSKUHeight");
        lblSKUWidth.Text = ResHelper.GetString("NewProduct.lblSKUWidth");
        lblSKUDepth.Text = ResHelper.GetString("NewProduct.lblSKUDepth");
        lblSKUSellOnlyAvailable.Text = ResHelper.GetString("NewProduct.lblSKUSellOnlyAvailable");
        lblSKUAvailableItems.Text = ResHelper.GetString("NewProduct.lblSKUAvailableItems");
        lblPackage.Text = ResHelper.GetString("NewProduct.PackageCategory");
        lblInventory.Text = ResHelper.GetString("NewProduct.InventoryCategory");

        txtSKUPrice.ValidationErrorMessage = ResHelper.GetString("com_SKU_edit_general.PriceErrorFormat");
        txtSKUPrice.EmptyErrorMessage = ResHelper.GetString("com_SKU_edit_general.PriceError");
        rfvSKUName.ErrorMessage = ResHelper.GetString("com_SKU_edit_general.NameError");
        //rfvSKUNumber.ErrorMessage = ResHelper.GetString("com_SKU_edit_general.NumberError");
        rvSKUAvailableInDays.ErrorMessage = ResHelper.GetString("newproduct.availabilitywrongformat");

        rvSKUDepth.ErrorMessage = ResHelper.GetString("newproduct.invalidvalue");
        rvSKUHeight.ErrorMessage = ResHelper.GetString("newproduct.invalidvalue");
        rvSKUWeight.ErrorMessage = ResHelper.GetString("newproduct.invalidvalue");
        rvSKUWidth.ErrorMessage = ResHelper.GetString("newproduct.invalidvalue");

        if (!mShowOkButton)
        {
            btnOk.Visible = false;
        }

        btnOk.Text = ResHelper.GetString("General.OK");

        // Get current product info
        SKUInfo skuObj = SKUInfoProvider.GetSKUInfo(mProductId);

        if (skuObj != null)
        {
            string imagePath = skuObj.SKUImagePath;
            if (string.IsNullOrEmpty(imagePath) || imagePath.ToLower().StartsWith("~/getmetafile/"))
            {
                hasAttachmentImagePath = false;
            }
            if (this.OptionCategoryID == 0)
            {
                this.OptionCategoryID = skuObj.SKUOptionCategoryID;
            }
        }
        else
        {
            hasAttachmentImagePath = false;

            this.manufacturerElem.DisplayItems = StatusEnum.Enabled;
            this.supplierElem.DisplayItems = StatusEnum.Enabled;
            this.internalStatusElem.DisplayItems = StatusEnum.Enabled;
            this.publicStatusElem.DisplayItems = StatusEnum.Enabled;
        }

        CurrentUserInfo cui = CMSContext.CurrentUser;
        if ((cui != null) && (!cui.IsGlobalAdministrator))
        {
            departmentElem.UserID = cui.UserID;
        }

        if (!RequestHelper.IsPostBack())
        {
            if (mProductId > 0)
            {
                if (skuObj != null)
                {
                    LoadData(skuObj);

                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
        }
        else if (skuObj != null)
        {
            // Reload product description after postback
            htmlTemplateBody.ResolvedValue = skuObj.SKUDescription;
        }


        if (CMS.CMSEcommerce.SKUInfoProvider.UseMetaFileForProductImage && !hasAttachmentImagePath)
        {
            // Display meta file uploader    
            plcMetaFile.Visible = true;
            ucMetaFile.ObjectID = mProductId;
            ucMetaFile.ObjectType = ECommerceObjectType.SKU;
            ucMetaFile.Category = MetaFileInfoProvider.OBJECT_CATEGORY_IMAGE;
        }
        else
        {
            // Display image selector
            plcImagePath.Visible = true;
        }
    }   


    /// <summary>
    /// Load data of editing ecommerceEcommerce.
    /// </summary>
    /// <param name="skuiObj">EcommerceEcommerce object.</param>
    protected void LoadData(SKUInfo skuiObj)
    {
        //// load values
        txtSKUNumber.Text = skuiObj.SKUNumber;
        txtSKUName.Text = skuiObj.SKUName;
        htmlTemplateBody.ResolvedValue = skuiObj.SKUDescription;
        //txtSKUDescription.Text = skuiObj.SKUDescription;
        txtSKUPrice.Value = skuiObj.SKUPrice;
        chkSKUEnabled.Checked = skuiObj.SKUEnabled;
        txtSKUAvailableInDays.Text = Convert.ToString(skuiObj.SKUAvailableInDays);
        imgSelect.Value = skuiObj.SKUImagePath;
        txtSKUWeight.Text = (skuiObj.SKUWeight > 0) ? skuiObj.SKUWeight.ToString() : "";
        txtSKUWidth.Text = (skuiObj.SKUWidth > 0) ? skuiObj.SKUWidth.ToString() : "";
        txtSKUHeight.Text = (skuiObj.SKUHeight > 0) ? skuiObj.SKUHeight.ToString() : "";
        txtSKUDepth.Text = (skuiObj.SKUDepth > 0) ? skuiObj.SKUDepth.ToString() : "";
        txtSKUAvailableItems.Text = skuiObj.SKUAvailableItems.ToString();
        chkSKUSellOnlyAvailable.Checked = skuiObj.SKUSellOnlyAvailable;

        departmentElem.DepartmentID = skuiObj.SKUDepartmentID;
        manufacturerElem.ManufacturerID = skuiObj.SKUManufacturerID;
        supplierElem.SupplierID = skuiObj.SKUSupplierID;
        publicStatusElem.PublicStatusID = skuiObj.SKUPublicStatusID;
        internalStatusElem.InternalStatusID = skuiObj.SKUInternalStatusID;
    }


    /// <summary>
    /// Action when button is clicked
    /// </summary>
    public int OnBtnOkClick()
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            CMSPage.RedirectToCMSDeskAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        btnOk.CausesValidation = FormEnabled;

        // Form validation
        string errorMessage = ValidateForm();

        bool newItem = false;

        if (errorMessage == "")
        {
            // Get SKUInfo
            SKUInfo skuiObj = SKUInfoProvider.GetSKUInfo(mProductId);
            if (skuiObj == null)
            {
                newItem = true;

                // create new item -> insert
                skuiObj = new SKUInfo();
            }


            skuiObj.SKUAvailableInDays = ValidationHelper.GetInteger(txtSKUAvailableInDays.Text.Trim(), 0);
            skuiObj.SKUPrice = txtSKUPrice.Value;
            skuiObj.SKUWeight = ValidationHelper.GetDouble(txtSKUWeight.Text.Trim(), 0);
            skuiObj.SKUHeight = ValidationHelper.GetDouble(txtSKUHeight.Text.Trim(), 0);
            skuiObj.SKUWidth = ValidationHelper.GetDouble(txtSKUWidth.Text.Trim(), 0);
            skuiObj.SKUDepth = ValidationHelper.GetDouble(txtSKUDepth.Text.Trim(), 0);
            skuiObj.SKUAvailableItems = ValidationHelper.GetInteger(txtSKUAvailableItems.Text.Trim(), 0);

            skuiObj.SKUName = txtSKUName.Text.Trim();
            skuiObj.SKUNumber = txtSKUNumber.Text.Trim();
            skuiObj.SKUDescription = htmlTemplateBody.ResolvedValue; //txtSKUDescription.Text.Trim();
            skuiObj.SKUEnabled = chkSKUEnabled.Checked;
            skuiObj.SKUInternalStatusID = internalStatusElem.InternalStatusID;
            skuiObj.SKUDepartmentID = departmentElem.DepartmentID;
            skuiObj.SKUManufacturerID = manufacturerElem.ManufacturerID;
            skuiObj.SKUPublicStatusID = publicStatusElem.PublicStatusID;
            skuiObj.SKUSupplierID = supplierElem.SupplierID;
            skuiObj.SKUSellOnlyAvailable = chkSKUSellOnlyAvailable.Checked;
            if (!newItem)
            {
                // Update sku image path
                UpdateSKUImagePath(skuiObj);
            }

            // When creating new product option
            if ((this.ProductID == 0) && (this.OptionCategoryID > 0))
            {
                skuiObj.SKUOptionCategoryID = this.OptionCategoryID;
            }

            if ((newItem) && (!SKUInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Ecommerce, VersionActionEnum.Insert)))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("EcommerceProduct.VersionCheck");

                return 0;
            }

            SKUInfoProvider.SetSKUInfo(skuiObj);
            if (newItem)
            {
                if (CMS.CMSEcommerce.SKUInfoProvider.UseMetaFileForProductImage)
                {
                    // Upload metafile and update sku image path
                    ucMetaFile.ObjectID = skuiObj.SKUID;
                    ucMetaFile.UploadFile();
                    UpdateSKUImagePath(skuiObj);
                    SKUInfoProvider.SetSKUInfo(skuiObj);
                }
                else
                {
                    skuiObj.SKUImagePath = this.imgSelect.Value;
                }
            }

            if ((mNodeId > 0) && (mProductId == 0))
            {
                TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                TreeNode node = tree.SelectSingleNode(mNodeId, TreeProvider.ALL_CULTURES);
                node.NodeSKUID = skuiObj.SKUID;
                //node.Update();
                DocumentHelper.UpdateDocument(node, tree);

                // Ensure new SKU values
                SKUInfoProvider.SetSKUInfo(skuiObj);
            }

            // Update the price format
            this.txtSKUPrice.Value = skuiObj.SKUPrice;

            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            lblInfo.Visible = true;

            return ValidationHelper.GetInteger(skuiObj.SKUID, 0);
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }

        return 0;
    }


    private string ValidateForm()
    {
        string errorMessage = new Validator().NotEmpty(txtSKUName.Text, rfvSKUName.ErrorMessage).Result;
        if (errorMessage == "")
        {
            if ((txtSKUAvailableInDays.Text.Trim() != "") && !ValidationHelper.IsInteger(txtSKUAvailableInDays.Text.Trim()))
            {
                errorMessage = ResHelper.GetString("com_SKU_edit_general.DaysErrorFormat");
            }

            if (departmentElem.DepartmentID <= 0)
            {
                errorMessage = ResHelper.GetString("com_SKU_edit_general.EmptyDepartment");
            }

            // Validate price
            errorMessage = txtSKUPrice.ValidatePrice(this.OptionCategoryID > 0);

            // Validate department
            if ((errorMessage == "") && (departmentElem.DepartmentID == 0))
            {
                // Department not selected
                errorMessage = ResHelper.GetString("NewProduct.SKUDepartmentNotSelected");
            }

            string temp = "";

            // Validate weight
            temp = txtSKUWeight.Text.Trim();
            double skuWeight = ValidationHelper.GetDouble(temp, 0);
            if (temp != "")
            {
                if ((skuWeight == 0) || (!ValidationHelper.IsPositiveNumber(temp)))
                {
                    errorMessage = ResHelper.GetString("NewProduct.WeightWrongFormat");
                }
            }

            // Validate height
            temp = txtSKUHeight.Text.Trim();
            double skuHeight = ValidationHelper.GetDouble(temp, 0);
            if (temp != "")
            {
                if ((skuHeight == 0) || (!ValidationHelper.IsPositiveNumber(temp)))
                {
                    errorMessage = ResHelper.GetString("NewProduct.HeightWrongFormat");
                }
            }

            // Validate width
            temp = txtSKUWidth.Text.Trim();
            double skuWidth = ValidationHelper.GetDouble(temp, 0);
            if (temp != "")
            {
                if ((skuWidth == 0) || (!ValidationHelper.IsPositiveNumber(temp)))
                {
                    errorMessage = ResHelper.GetString("NewProduct.WidthWrongFormat");
                }
            }

            // Validate depth
            temp = txtSKUDepth.Text.Trim();
            double skuDepth = ValidationHelper.GetDouble(temp, 0);
            if (temp != "")
            {
                if ((skuDepth == 0) || (!ValidationHelper.IsPositiveNumber(temp)))
                {
                    errorMessage = ResHelper.GetString("NewProduct.DepthWrongFormat");
                }
            }

            // Validate available items
            if (txtSKUAvailableItems.Text.Trim() != "")
            {
                if (!ValidationHelper.IsInteger(txtSKUAvailableItems.Text.Trim()))
                {
                    errorMessage = ResHelper.GetString("com_SKU_edit_general.SKUAvailableItemsFormat");
                }
            }

            // If global meta files should be stored in filesystem
            if (ucMetaFile.Visible && (ucMetaFile.PostedFile != null) && MetaFileInfoProvider.StoreFilesInFileSystem(null))
            {
                // Get product image path
                string path = MetaFileInfoProvider.GetFilesFolderPath(null);

                // Check permission for image folder
                if (!DirectoryHelper.CheckPermissions(path))
                {
                    errorMessage = String.Format(ResHelper.GetString("NewProduct.AccessDeniedToPath"), path);
                }
            }
        }

        return errorMessage;
    }


    private void UpdateSKUImagePath(SKUInfo skuObj)
    {
        if (CMS.CMSEcommerce.SKUInfoProvider.UseMetaFileForProductImage && !hasAttachmentImagePath)
        {
            // Update product image path according to its meta file
            DataSet ds = MetaFileInfoProvider.GetMetaFiles(ucMetaFile.ObjectID, skuObj.TypeInfo.ObjectType);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                MetaFileInfo metaFile = new MetaFileInfo(ds.Tables[0].Rows[0]);
                skuObj.SKUImagePath = MetaFileInfoProvider.GetMetaFileUrl(metaFile.MetaFileGUID, metaFile.MetaFileName);
            }
            else
            {
                skuObj.SKUImagePath = "";
            }
        }
        else
        {
            // Update product image path from the image selector
            skuObj.SKUImagePath = imgSelect.Value;
        }
    }


    public event EventHandler OnOkClickHandler;

    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            CMSPage.RedirectToCMSDeskAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        // Update data only if the form is enabled
        if (FormEnabled)
        {
            this.ProductID = OnBtnOkClick();
        }

        if (this.ProductID > 0)
        {
            if (OnOkClickHandler != null)
            {
                OnOkClickHandler(this, null);
            }
        }
    }


    /// <summary>
    /// Sets values to the form fields.
    /// </summary>
    public void SetValues()
    {
        txtSKUName.Text = mProductName;
        txtSKUHeight.Text = (mProductHeight > 0) ? mProductHeight.ToString() : "";
        txtSKUWeight.Text = (mProductWeight > 0) ? mProductWeight.ToString() : "";
        txtSKUWidth.Text = (mProductWidth > 0) ? mProductWidth.ToString() : "";
        txtSKUDepth.Text = (mProductDepth > 0) ? mProductDepth.ToString() : "";
        //txtSKUPrice.Text = (mProductPrice > 0) ? mProductPrice.ToString() : "";
        txtSKUPrice.Value = mProductPrice;
        htmlTemplateBody.ResolvedValue = mProductDescription;
        imgSelect.Value = mProductImagePath;
    }
}
