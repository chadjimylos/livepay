using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Header : CMSNewsletterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Newsletters.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Newsletter_Newsletter/object.png");

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }
    }


    /// <summary>
    /// Initializes menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string [,] tabs = new string[6, 4];
        tabs[0, 0] = ResHelper.GetString("Newsletters.Newsletters");
        tabs[0, 2] = "Newsletters/Newsletter_List.aspx";
        tabs[1, 0] = ResHelper.GetString("Newsletters.Subscribers");
        tabs[1, 2] = "Subscribers/Subscriber_List.aspx";
        tabs[2, 0] = ResHelper.GetString("Newsletters.Templates");
        tabs[2, 2] = "Templates/NewsletterTemplate_List.aspx";
        tabs[3, 0] = ResHelper.GetString("Newsletters.EmailQueue");
        tabs[3, 2] = "EmailQueue/NewsletterEmailQueue.aspx";
        tabs[4, 0] = ResHelper.GetString("Newsletters.ImportSubscribers");
        tabs[4, 2] = "ImportExportSubscribers/Subscriber_Import.aspx";
        tabs[5, 0] = ResHelper.GetString("Newsletters.ExportSubscribers");
        tabs[5, 2] = "ImportExportSubscribers/Subscriber_Export.aspx";

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "newslettersContent";
    }
}
