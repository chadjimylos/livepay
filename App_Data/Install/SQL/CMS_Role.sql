CREATE TABLE [CMS_Role] (
		[RoleID]                       int NOT NULL IDENTITY(1, 1),
		[RoleDisplayName]              nvarchar(100) NOT NULL,
		[RoleName]                     nvarchar(100) NOT NULL,
		[RoleDescription]              nvarchar(max) NULL,
		[SiteID]                       int NOT NULL,
		[RoleGUID]                     uniqueidentifier NOT NULL,
		[RoleLastModified]             datetime NOT NULL,
		[RoleGroupID]                  int NULL,
		[RoleIsGroupAdministrator]     bit NULL,
		[RoleIsDomain]                 bit NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Role]
	ADD
	CONSTRAINT [PK_CMS_Role]
	PRIMARY KEY
	NONCLUSTERED
	([RoleID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_Role]
	ADD
	CONSTRAINT [DF_CMS_Role_RoleGroupID]
	DEFAULT ((1)) FOR [RoleGroupID]
ALTER TABLE [CMS_Role]
	ADD
	CONSTRAINT [DF_CMS_Role_RoleIsGroupAdministrator]
	DEFAULT ((0)) FOR [RoleIsGroupAdministrator]
IF OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[Community_Group]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Role_RoleGroupID_Community_Group]') IS NULL
BEGIN
		ALTER TABLE [CMS_Role]
			ADD CONSTRAINT [FK_CMS_Role_RoleGroupID_Community_Group]
			FOREIGN KEY ([RoleGroupID]) REFERENCES [Community_Group] ([GroupID])
END
IF OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Role_SiteID_CMS_SiteID]') IS NULL
BEGIN
		ALTER TABLE [CMS_Role]
			ADD CONSTRAINT [FK_CMS_Role_SiteID_CMS_SiteID]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE INDEX [IX_CMS_Role_RoleGroupID]
	ON [CMS_Role] ([RoleGroupID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_Role_SiteID_RoleName_RoleDisplayName]
	ON [CMS_Role] ([SiteID], [RoleName], [RoleDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
