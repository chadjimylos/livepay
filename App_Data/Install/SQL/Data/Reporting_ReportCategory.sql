SET IDENTITY_INSERT [Reporting_ReportCategory] ON
INSERT INTO [Reporting_ReportCategory] ([CategoryID], [CategoryLastModified], [CategoryDisplayName], [CategoryCodeName], [CategoryGUID]) VALUES (12, '20090127 09:19:01', N'Document reports', N'DocumentReports', '763c75ba-f2a2-40fd-a9a9-0a23fe6caeea')
INSERT INTO [Reporting_ReportCategory] ([CategoryID], [CategoryLastModified], [CategoryDisplayName], [CategoryCodeName], [CategoryGUID]) VALUES (13, '20080313 09:35:00', N'E-commerce', N'EcommerceReports', '10595f9b-8a32-464e-85c6-33404ebfcd67')
INSERT INTO [Reporting_ReportCategory] ([CategoryID], [CategoryLastModified], [CategoryDisplayName], [CategoryCodeName], [CategoryGUID]) VALUES (21, '20081130 15:19:50', N'Forums', N'Forums', '0517a811-fbfa-4aeb-a9ae-8a58ce23e12e')
INSERT INTO [Reporting_ReportCategory] ([CategoryID], [CategoryLastModified], [CategoryDisplayName], [CategoryCodeName], [CategoryGUID]) VALUES (19, '20080822 11:13:22', N'Membership', N'Membership', '413a5a4c-8b8a-4328-a693-c56e1de76b83')
INSERT INTO [Reporting_ReportCategory] ([CategoryID], [CategoryLastModified], [CategoryDisplayName], [CategoryCodeName], [CategoryGUID]) VALUES (4, '20080410 17:23:28', N'Web Analytics', N'WebAnalytics', '8d48afa7-5164-4ec1-94e7-19c18956f79c')
SET IDENTITY_INSERT [Reporting_ReportCategory] OFF
