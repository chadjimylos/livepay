using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Forums;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Forums_Controls_Subscriptions_SubscriptionEdit : CMSAdminEditControl
{
    #region "Variables"

    private int mForumId;
    private int mSubscriptionId;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the ID of the forum to edit.
    /// </summary>
    public int ForumID
    {
        get
        {
            return this.mForumId;
        }
        set
        {
            this.mForumId = value;
        }
    }


    /// <summary>
    /// Gets or sets the ID of the subscription to edit.
    /// </summary>
    public int SubscriptionID
    {
        get
        {
            return this.mSubscriptionId;
        }
        set
        {
            this.mSubscriptionId = value;
        }
    }


    /// <summary>
    /// Returns if checkbox "Send confirmation email" is checked or not
    /// </summary>
    public bool SendEmailConfirmation
    {
        get
        {
            return this.chkSendConfirmationEmail.Checked;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get query keys
        bool saved = QueryHelper.GetBoolean("saved", false);
        bool chkChecked = QueryHelper.GetBoolean("checked", true);

        // Get resource strings        
        this.rfvEmail.ErrorMessage = ResHelper.GetString("ForumSubscription_Edit.emailErrorMsg");
        this.btnOk.Text = ResHelper.GetString("General.OK");

        bool process = true;
        if (!this.Visible || StopProcessing)
        {
            this.EnableViewState = false;
            process = false;
        }

        this.rfvSubscriptionEmail.ErrorMessage = ResHelper.GetString("ForumSubscription_Edit.EnterSomeEmail");
        this.rfvEmail.ValidationExpression = @"^([a-zA-Z0-9_\-\+]+(\.[a-zA-Z0-9_\-\+]+)*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+)*$";
        
        if (!this.IsLiveSite && !RequestHelper.IsPostBack() && process)
        {
            ReloadData();
        }

        if (!RequestHelper.IsPostBack())
        {
            chkSendConfirmationEmail.Checked = true;
        }

        if (!chkChecked)
        {
            chkSendConfirmationEmail.Checked = false;
        }

        if (saved)
        {
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }
    }


    /// <summary>
    /// Reloads the form data.
    /// </summary>
    public override void ReloadData()
    {
        ClearForm();
        ForumSubscriptionInfo forumSubscriptionObj = ForumSubscriptionInfoProvider.GetForumSubscriptionInfo(this.mSubscriptionId);
        if (forumSubscriptionObj != null)
        {
            txtSubscriptionEmail.Text = Server.HtmlEncode(forumSubscriptionObj.SubscriptionEmail);
        }
    }


    /// <summary>
    /// Clears the form fields to default values.
    /// </summary>
    public override void ClearForm()
    {
        this.txtSubscriptionEmail.Text = "";
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (!CheckPermissions("cms.forums", CMSAdminControl.PERMISSION_MODIFY))
        {
            return;
        }

        string errorMessage = new Validator().NotEmpty(txtSubscriptionEmail.Text, ResHelper.GetString("ForumSubscription_Edit.EnterSomeEmail")).Result;
        
        if (errorMessage == "")
        {

            // Check that e-mail is not already subscribed
            if (ForumSubscriptionInfoProvider.IsSubscribed(txtSubscriptionEmail.Text.Trim(), this.mForumId, 0))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("ForumSubscibe.SubscriptionExists");
                return;
            }

            ForumSubscriptionInfo forumSubscriptionObj = ForumSubscriptionInfoProvider.GetForumSubscriptionInfo(this.mSubscriptionId);

            if (forumSubscriptionObj == null)
            {
                forumSubscriptionObj = new ForumSubscriptionInfo();
                forumSubscriptionObj.SubscriptionForumID = this.mForumId;
                forumSubscriptionObj.SubscriptionGUID = Guid.NewGuid();
            }

            forumSubscriptionObj.SubscriptionEmail = txtSubscriptionEmail.Text.Trim();
            if (ValidationHelper.IsEmail(forumSubscriptionObj.SubscriptionEmail))
            {
                ForumSubscriptionInfoProvider.SetForumSubscriptionInfo(forumSubscriptionObj, chkSendConfirmationEmail.Checked, null, null);

                this.mSubscriptionId = forumSubscriptionObj.SubscriptionID;

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");

                this.RaiseOnSaved();
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("ForumSubscription_Edit.EmailIsNotValid");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
