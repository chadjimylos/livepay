using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.TreeEngine;

public partial class CMSSiteManager_Development_Categories_Category_Edit_Documents : SiteManagerPage
{
    #region "Variables"

    private int mCategoryID = 0;
    private SiteInfo siteInfo = null;

    #endregion


    #region "Page methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Initializes the controls
        filterDocuments.OnSiteSelectionChanged += UniSelector_OnSelectionChanged;
        // Get current page template ID
        mCategoryID = QueryHelper.GetInteger("categoryID", 0);
        CategoryInfo ci = CategoryInfoProvider.GetCategoryInfo(mCategoryID);
        if (ci != null)
        {
            uniGridDocs.OnDataReload += new UniGrid.OnDataReloadEventHandler(uniGridDocs_OnDataReload);
            uniGridDocs.OnExternalDataBound += uniGridDocs_OnExternalDataBound;
            uniGridDocs.ZeroRowsText = ResHelper.GetString("Category_Edit.Documents.nodata");
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (filterDocuments.SelectedSite != TreeProvider.ALL_SITES)
        {
            uniGridDocs.GridView.Columns[4].Visible = false;
        }
    }



    protected DataSet uniGridDocs_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        string site = filterDocuments.SelectedSite;
        string where = "(DocumentID IN (SELECT CMS_DocumentCategory.DocumentID FROM CMS_DocumentCategory WHERE CategoryID = " + mCategoryID + "))";
        where = SqlHelperClass.AddWhereCondition(where, completeWhere);
        where = SqlHelperClass.AddWhereCondition(where, filterDocuments.WhereCondition);

        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        DataSet ds = tree.SelectNodes(site, TreeProvider.ALL_DOCUMENTS, TreeProvider.ALL_CULTURES, false, null, where,
            "DocumentName, SiteName", -1, true, currentTopN, "DocumentCulture,DocumentName,DocumentNamePath,NodeID,NodeClassID,SiteName,ClassName");

        totalRecords = DataHelper.GetItemsCount(ds);
        return ds;
    }


    protected object uniGridDocs_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView drv = GetDataRowView(sender as DataControlFieldCell);
        string siteName = ValidationHelper.GetString(drv["SiteName"], string.Empty);
        string documentCulture = null;
        int nodeId = 0;

        // Get document site
        siteInfo = SiteInfoProvider.GetSiteInfo(siteName);

        switch (sourceName.ToLower())
        {
            case "documentname":
                // Generate link to the document
                string documentName = TextHelper.LimitLength(ValidationHelper.GetString(drv["DocumentName"], string.Empty), 50);
                nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                documentCulture = ValidationHelper.GetString(drv["DocumentCulture"], string.Empty);
                return GetDocumentLink(documentName, siteInfo, nodeId, documentCulture);

            case "documentnametooltip":
                drv = (DataRowView)parameter;
                return UniGridFunctions.DocumentNameTooltip(drv);

            case "nodeclassid":
            case "nodeclassidtooltip":
                int nodeClassId = ValidationHelper.GetInteger(drv["NodeClassID"], 0);
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(nodeClassId);
                string result = (dci != null) ? dci.ClassDisplayName : string.Empty;
                if (sourceName.ToLower() == "nodeclassid")
                {
                    result = TextHelper.LimitLength(result, 50);
                }
                result = HTMLHelper.HTMLEncode(result);
                return result;

            case "sitename":
                return HTMLHelper.HTMLEncode(siteInfo.DisplayName);

            case "culture":
                return UniGridFunctions.DocumentCultureFlag(drv, this.Page);

            case "icon":
                string className = ValidationHelper.GetString(drv["ClassName"], string.Empty);
                nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                documentCulture = ValidationHelper.GetString(drv["DocumentCulture"], string.Empty);
                return GetDocumentIconLink(siteInfo, nodeId, documentCulture, className);

            default:
                return parameter;
        }
    }


    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        uniGridDocs.ReloadData();
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }


    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private static object GetDocumentLink(string documentName, SiteInfo si, int nodeId, string cultureCode)
    {
        // If the document path is empty alter it with the default '/'
        if (string.IsNullOrEmpty(documentName))
        {
            documentName = "-";
        }

        string url = GetDocumentUrl(si, nodeId, cultureCode);
        string toReturn = HTMLHelper.HTMLEncode(documentName);

        if (!string.IsNullOrEmpty(url))
        {
            toReturn = "<a href=\"" + url + "\" target=\"_blank\">" + toReturn + "</a>";
        }
        return toReturn;
    }


    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private object GetDocumentIconLink(SiteInfo si, int nodeId, string cultureCode, string className)
    {
        string url = GetDocumentUrl(si, nodeId, cultureCode);
        string toReturn = "<img style=\"border-style:none;\" src=\"" + GetDocumentTypeIconUrl(className) + "\" alt=\"" + className + "\" />";

        if (!string.IsNullOrEmpty(url))
        {
            return "<a href=\"" + url + "\" target=\"_blank\">" + toReturn + "</a>";
        }
        return toReturn;
    }


    /// <summary>
    /// Generates URL for document
    /// </summary>
    private static string GetDocumentUrl(SiteInfo si, int nodeId, string cultureCode)
    {
        string url = string.Empty;

        if (si.Status == SiteStatusEnum.Running)
        {
            if (si.SiteName != CMSContext.CurrentSiteName)
            {
                // Make url for site in form 'http(s)://sitedomain/application/cmsdesk'.
                url = UrlHelper.GetApplicationUrl(si.DomainName) + "/cmsdesk/default.aspx?section=content&nodeid=" + nodeId + "&culture=" + cultureCode;
            }
            else
            {
                // Make url for current site (with port)
                url = UrlHelper.ResolveUrl("~/cmsdesk/default.aspx?section=content&nodeid=" + nodeId + "&culture=" + cultureCode);
            }
        }
        return url;
    }

    #endregion
}
