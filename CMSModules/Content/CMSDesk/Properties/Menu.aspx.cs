using System;

using CMS.GlobalHelper;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.CMSHelper;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Properties_Menu : CMSPropertiesPage
{
    #region "Variables"

    protected int nodeId = 0;
    protected bool hasModifyPermission = true;
    private TreeNode node = null;
    private TreeProvider tree = null;

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.Menu"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.Menu");
        }

        // Redirect to information page when no UI elements displayed
        if (this.pnlUIActions.IsHidden && this.pnlUIBasicProperties.IsHidden && this.pnlUIDesign.IsHidden)
        {
            RedirectToUINotAvailable();
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the scripts
        ScriptHelper.RegisterProgress(this.Page);
        
        UIContext.PropertyTab = PropertyTabEnum.Menu;

        nodeId = QueryHelper.GetInteger("nodeid", 0);

        // Get the document
        tree = new TreeProvider(CMSContext.CurrentUser);
        node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, tree);

        PreRender += Page_PreRender;

        radInactive.Attributes.Add("onclick", "enableTextBoxes('inactive')");
        radStandard.Attributes.Add("onclick", "enableTextBoxes('')");
        radUrl.Attributes.Add("onclick", "enableTextBoxes('url')");
        radJavascript.Attributes.Add("onclick", "enableTextBoxes('java')");

        pnlBasicProperties.GroupingText = ResHelper.GetString("content.menu.basic");
        pnlActions.GroupingText = ResHelper.GetString("content.menu.actions");
        pnlDesign.GroupingText = ResHelper.GetString("content.menu.design");

        menuElem.OnBeforeCheckIn += new EventHandler(menuElem_OnBeforeCheckIn);
        menuElem.OnBeforeApprove += new EventHandler(menuElem_OnBeforeApprove);
        menuElem.OnBeforeReject += new EventHandler(menuElem_OnBeforeReject);
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (menuElem.AllowEdit && menuElem.AllowSave && hasModifyPermission)
        {
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
                "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>"
            );
            pnlForm.Enabled = true;
        }
        else
        {
            pnlForm.Enabled = false;
        }

        lblWorkflowInfo.Text = menuElem.WorkflowInfo;
        lblWorkflowInfo.Visible = (lblWorkflowInfo.Text != string.Empty);

        // Get the document with valid data if under the workflow
        if (lblWorkflowInfo.Visible)
        {
            node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, tree);
        }

        // Reload data
        if (!RequestHelper.IsPostBack())
        {
            ReloadData();
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Reload data
    /// </summary>
    private void ReloadData()
    {
        if (node != null)
        {
            // Check read permissions
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
            }
            // Check modify permissions
            else if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
            {
                hasModifyPermission = false;
                pnlForm.Enabled = false;
                lblWorkflowInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
            }

            txtMenuCaption.Text = node.DocumentMenuCaption;
            txtMenuItemStyle.Text = node.DocumentMenuStyle;
            txtMenuItemImage.Text = node.DocumentMenuItemImage;
            txtMenuItemLeftImage.Text = node.DocumentMenuItemLeftImage;
            txtMenuItemRightImage.Text = node.DocumentMenuItemRightImage;

            if (node.GetValue("DocumentMenuItemHideInNavigation") != null)
            {
                chkShowInNavigation.Checked = !(Convert.ToBoolean(node.GetValue("DocumentMenuItemHideInNavigation")));
            }
            else
            {
                chkShowInNavigation.Checked = false;
            }


            chkShowInSitemap.Checked = Convert.ToBoolean(node.GetValue("DocumentShowInSiteMap"));

            txtCssClass.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuClass"), "");

            txtMenuItemStyleMouseOver.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuStyleOver"), "");
            txtCssClassMouseOver.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuClassOver"), "");
            txtMenuItemImageMouseOver.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuItemImageOver"), "");
            txtMenuItemLeftImageMouseOver.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuItemLeftImageOver"), "");
            txtMenuItemRightImageMouseOver.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuItemRightImageOver"), "");

            txtMenuItemStyleHighlight.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuStyleHighlighted"), "");
            txtCssClassHighlight.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuClassHighlighted"), "");
            txtMenuItemImageHighlight.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuItemImageHighlighted"), "");
            txtMenuItemLeftImageHighlight.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuItemLeftImageHighlighted"), "");
            txtMenuItemRightImageHighlight.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuItemRightImageHighlighted"), "");


            //Menu Action
            SetRadioActions(0);

            // Menu action priority low to high !
            if (ValidationHelper.GetString(node.GetValue("DocumentMenuJavascript"), "") != "")
            {
                txtJavaScript.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuJavascript"), "");
                SetRadioActions(2);
            }

            if (ValidationHelper.GetString(node.GetValue("DocumentMenuRedirectUrl"), "") != "")
            {
                txtUrlInactive.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuRedirectUrl"), "");
                txtUrl.Text = ValidationHelper.GetString(node.GetValue("DocumentMenuRedirectUrl"), "");
                SetRadioActions(3);
            }

            if (ValidationHelper.GetBoolean(node.GetValue("DocumentMenuItemInactive"), false))
            {
                SetRadioActions(1);
            }
        }
    }


    /// <summary>
    /// Set radio buttons for menu action
    /// </summary>
    private void SetRadioActions(int action)
    {
        radInactive.Checked = false;
        radStandard.Checked = false;
        radUrl.Checked = false;
        radJavascript.Checked = false;

        txtJavaScript.Enabled = false;
        txtUrl.Enabled = false;

        switch (action)
        {
            case 1:
                {
                    ltlScript.Text = ScriptHelper.GetScript("enableTextBoxes('inactive');");
                    radInactive.Checked = true;
                    break;
                }
            case 2:
                {
                    ltlScript.Text = ScriptHelper.GetScript("enableTextBoxes('java');");
                    radJavascript.Checked = true;
                    txtJavaScript.Enabled = true;
                    break;
                }
            case 3:
                {
                    ltlScript.Text = ScriptHelper.GetScript("enableTextBoxes('url');");
                    radUrl.Checked = true;
                    txtUrl.Enabled = true;
                    break;
                }
            default:
                {
                    ltlScript.Text = ScriptHelper.GetScript("enableTextBoxes('');");
                    radStandard.Checked = true;
                    break;
                }
        }
    }

    #endregion


    #region "Button handling"

    protected void menuElem_OnBeforeCheckIn(object sender, EventArgs e)
    {
        btnOK_Click(sender, e);
        lblInfo.Text += "<br />" + ResHelper.GetString("ContentEdit.WasCheckedIn");
        lblInfo.Visible = true;
    }


    protected void menuElem_OnBeforeReject(object sender, EventArgs e)
    {
        btnOK_Click(sender, e);
    }


    protected void menuElem_OnBeforeApprove(object sender, EventArgs e)
    {
        btnOK_Click(sender, e);
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        TreeNode node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, tree);

        if (node != null)
        {
            bool autoCheck = !VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName);
            VersionManager vm = null;
            bool useWorkflow = false;
            WorkflowManager workflowMan = new WorkflowManager(tree);

            // Get the document
            node = DocumentHelper.GetDocument(nodeId, CMSContext.PreferredCultureCode, tree);

            // Test if the document uses workflow
            if (workflowMan.GetNodeWorkflowScope(node) != null)
            {
                useWorkflow = true;
            }

            // If not using check-in/check-out, check out automatically
            if (autoCheck & useWorkflow)
            {
                if (node != null)
                {
                    // Check out
                    vm = new VersionManager(tree);
                    vm.CheckOut(node);
                }
            }

            // Update the data
            if (node != null)
            {
                if (!this.pnlUIBasicProperties.IsHidden)
                {
                    node.DocumentMenuCaption = txtMenuCaption.Text.Trim();
                    node.SetValue("DocumentMenuItemHideInNavigation", !chkShowInNavigation.Checked);
                    node.SetValue("DocumentShowInSiteMap", chkShowInSitemap.Checked);
                }

                if (!this.pnlUIDesign.IsHidden)
                {
                    node.DocumentMenuItemImage = txtMenuItemImage.Text.Trim();
                    node.DocumentMenuItemLeftImage = txtMenuItemLeftImage.Text.Trim();
                    node.DocumentMenuItemRightImage = txtMenuItemRightImage.Text.Trim();
                    node.DocumentMenuStyle = txtMenuItemStyle.Text.Trim();
                    node.SetValue("DocumentMenuClass", txtCssClass.Text.Trim());

                    node.SetValue("DocumentMenuStyleOver", txtMenuItemStyleMouseOver.Text.Trim());
                    node.SetValue("DocumentMenuClassOver", txtCssClassMouseOver.Text.Trim());
                    node.SetValue("DocumentMenuItemImageOver", txtMenuItemImageMouseOver.Text.Trim());
                    node.SetValue("DocumentMenuItemLeftImageOver", txtMenuItemLeftImageMouseOver.Text.Trim());
                    node.SetValue("DocumentMenuItemRightImageOver", txtMenuItemRightImageMouseOver.Text.Trim());

                    node.SetValue("DocumentMenuStyleHighlighted", txtMenuItemStyleHighlight.Text.Trim());
                    node.SetValue("DocumentMenuClassHighlighted", txtCssClassHighlight.Text.Trim());
                    node.SetValue("DocumentMenuItemImageHighlighted", txtMenuItemImageHighlight.Text.Trim());
                    node.SetValue("DocumentMenuItemLeftImageHighlighted", txtMenuItemLeftImageHighlight.Text.Trim());
                    node.SetValue("DocumentMenuItemRightImageHighlighted", txtMenuItemRightImageHighlight.Text.Trim());
                }
            }

            if (!this.pnlUIActions.IsHidden)
            {
                // Menu action
                txtJavaScript.Enabled = false;
                txtUrl.Enabled = false;
                
                if (radStandard.Checked)
                {
                    if (node != null)
                    {
                        node.SetValue("DocumentMenuRedirectUrl", "");
                        node.SetValue("DocumentMenuJavascript", "");
                        node.SetValue("DocumentMenuItemInactive", false);
                    }
                }

                if (radInactive.Checked)
                {
                    ltlScript.Text = ScriptHelper.GetScript("enableTextBoxes('inactive');");
                    if (node != null)
                    {
                        node.SetValue("DocumentMenuRedirectUrl", txtUrlInactive.Text);
                        node.SetValue("DocumentMenuJavascript", txtJavaScript.Text);
                        node.SetValue("DocumentMenuItemInactive", true);
                    }
                }

                if (radJavascript.Checked)
                {
                    txtJavaScript.Enabled = true;
                    txtUrl.Enabled = false;
                    if (node != null)
                    {
                        node.SetValue("DocumentMenuRedirectUrl", "");
                        node.SetValue("DocumentMenuJavascript", txtJavaScript.Text);
                        node.SetValue("DocumentMenuItemInactive", false);
                    }
                }

                if (radUrl.Checked)
                {
                    txtJavaScript.Enabled = false;
                    txtUrl.Enabled = true;
                    if (node != null)
                    {
                        node.SetValue("DocumentMenuRedirectUrl", txtUrl.Text);
                        node.SetValue("DocumentMenuJavascript", "");
                        node.SetValue("DocumentMenuItemInactive", false);
                    }
                }
            }

            DocumentHelper.UpdateDocument(node, tree);

            // Check in the document
            if (autoCheck & useWorkflow)
            {
                if (vm != null)
                {
                    vm.CheckIn(node, null, null);
                }
            }

            menuElem.ReloadMenu();
            string refreshMenuScript = "RefreshTree(" + nodeId + ", " + nodeId + ");";
            
            ScriptHelper.RegisterStartupScript(this, typeof(string), "refreshMenu", ScriptHelper.GetScript(refreshMenuScript));

            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            lblInfo.Visible = true;
        }
    }

    #endregion
}
