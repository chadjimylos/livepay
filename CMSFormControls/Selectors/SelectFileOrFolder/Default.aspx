﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="Default" CodeFile="Default.aspx.cs"
    Inherits="CMSFormControls_Selectors_SelectFileOrFolder_Default" EnableEventValidation="false" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head id="Head1" runat="server">
    <title>Insert</title>
</head>
<frameset rows="36, *, 43" border="0" id="rowsFrameset">
		<frame name="insertHeader" src="Header.aspx<%=Request.Url.Query%>" scrolling="no" frameborder="0" noresize="noresize" id="menu" />
	    <frame name="insertContent" src="Content.aspx<%=Request.Url.Query%>" scrolling="no" frameborder="0" id="content" />
	    <frame name="insertFooter" src="Footer.aspx<%=Request.Url.Query%>" scrolling="no" frameborder="0" noresize="noresize" id="footer" />
	</frameset>
<noframes>
    <body>
        <p id="p1">
            This HTML frameset displays multiple Web pages. To view this frameset, use a Web
            browser that supports HTML 4.0 and later.
        </p>
    </body>
</noframes>
</html>
