-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Staging_Task_RemoveDependences]
	@TaskID int
AS
BEGIN
    -- Staging_SyncLog
	DELETE FROM Staging_SyncLog WHERE SyncLogTaskID = @TaskID;
	-- Staging_Synchronization
	DELETE FROM Staging_Synchronization WITH(HOLDLOCK) WHERE SynchronizationTaskID = @TaskID;
END
