using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_InternalStatus_InternalStatus_List : CMSEcommerceConfigurationPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Pagetitle
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("InternalStatus_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_InternalStatus/object.png");
        this.CurrentMaster.Title.HelpTopicName = "internal_status_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("InternalStatus_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("InternalStatus_Edit.aspx");
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_InternalStatus/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        gridElem.OnAction += new OnActionEventHandler(gridElem_OnAction);
        gridElem.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridElem_OnExternalDataBound);
        gridElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void gridElem_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("InternalStatus_Edit.aspx?statusid=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // check 'ConfigurationModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
            }

            if (InternalStatusInfoProvider.CheckDependencies(ValidationHelper.GetInteger(actionArgument, 0)))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                return;
            }

            // delete InternalStatusInfo object from database
            InternalStatusInfoProvider.DeleteInternalStatusInfo(ValidationHelper.GetInteger(actionArgument, 0));
        }
    }


    protected object gridElem_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "internalstatusenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }

        return parameter;
    }
}
