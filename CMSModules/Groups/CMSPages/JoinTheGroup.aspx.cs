using System;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.Community;

public partial class CMSModules_Groups_CMSPages_JoinTheGroup : CMSLiveModalPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentMaster.Title.TitleText = ResHelper.GetString("Groups.JoinTheGroup");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Groups/jointhegroup.png");
        Title = ResHelper.GetString("Groups.JoinTheGroup");

        if (CommunityContext.CurrentGroup != null)
        {
            GroupJoin1.Group = CommunityContext.CurrentGroup;
        }
    }
}
