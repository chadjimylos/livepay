<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BizForm_Edit_General.aspx.cs"
    Inherits="CMSModules_BizForms_Tools_BizForm_Edit_General" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="BizForm General" Theme="Default" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblDisplayName" runat="server" EnableViewState="False" />
            </td>
            <td>
                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName"
                    Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCodeName" runat="server" EnableViewState="False" />
            </td>
            <td>
                <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="100"
                    ReadOnly="True" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblTableName" runat="server" EnableViewState="False" />
            </td>
            <td>
                <asp:TextBox ID="txtTableName" runat="server" CssClass="TextBoxField" MaxLength="100"
                    ReadOnly="True" Enabled="False" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" class="FieldLabel">
                <asp:Label ID="lblAfterSubmited" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                &nbsp;<asp:RadioButton ID="radDisplay" runat="server" GroupName="grpAfterSubmited"
                    AutoPostBack="True" OnCheckedChanged="radDisplay_CheckedChanged" />
            </td>
            <td style="height: 24px">
                <asp:TextBox ID="txtDisplay" runat="server" MaxLength="200" CssClass="TextBoxField" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                &nbsp;<asp:RadioButton ID="radRedirect" runat="server" GroupName="grpAfterSubmited"
                    AutoPostBack="True" OnCheckedChanged="radRedirect_CheckedChanged" />
            </td>
            <td>
                <asp:TextBox ID="txtRedirect" runat="server" MaxLength="200" CssClass="TextBoxField" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                &nbsp;<asp:RadioButton ID="radClear" runat="server" GroupName="grpAfterSubmited"
                    AutoPostBack="True" OnCheckedChanged="radClear_CheckedChanged" />
            </td>
            <td style="height: 24px">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                &nbsp;<asp:RadioButton ID="radContinue" runat="server" GroupName="grpAfterSubmited"
                    AutoPostBack="True" OnCheckedChanged="radClear_CheckedChanged" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="width: 140px">
                <asp:Label ID="lblButtonText" runat="server" EnableViewState="False" />
            </td>
            <td style="height: 24px">
                <asp:TextBox ID="txtButtonText" runat="server" MaxLength="200" CssClass="TextBoxField" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="width: 140px">
                <asp:Label ID="lblSubmitButtonImage" runat="server" EnableViewState="False" />
            </td>
            <td style="height: 24px">
                <asp:TextBox ID="txtSubmitButtonImage" runat="server" MaxLength="200" CssClass="TextBoxField" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="width: 140px">
            </td>
            <td>
                <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOk_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
