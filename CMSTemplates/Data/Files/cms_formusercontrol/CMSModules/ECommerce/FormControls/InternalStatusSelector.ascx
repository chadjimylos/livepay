<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InternalStatusSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_FormControls_InternalStatusSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%InternalStatusDisplayName%}"
            ObjectType="ecommerce.internalstatus" ResourcePrefix="internalstatusselector"
            SelectionMode="SingleDropDownList" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
