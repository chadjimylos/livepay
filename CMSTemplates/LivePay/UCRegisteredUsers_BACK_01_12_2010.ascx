﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCRegisteredUsers.ascx.vb" Inherits="CMSTemplates_LivePay_UCRegisteredUsers" %>
<%@ Register Src="~/CMSTemplates/livepay/PopUp/UCReceipt.ascx" TagName="Receipt" TagPrefix="uc"  %>
<script language="javascript" type="text/javascript" >
    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    var CardBelongToOther_User = 0;

    var IsDirectPage = '<%= CMSContext.CurrentUser.IsPublic() %>'

    //---- For UnReg Users
    var Tab2_PhoneErrorMessage = ''
    var Tab2_EmailErrorMessage = ''
    //---- For UnReg Users

    var Tab1_PriceErrorMessage = ''
    var Tab1_PriceErrorMessageLimit = ''
    //--Start---For New Card---
    var FullNameErrorMessage = ''
    var NewCardCVV2ErrorMessage = ''
    var NewCardFriendlyErrorMessage = ''
    var SavedCardCVV2ErrorMessage = ''
    var SaveCardTopErrorMessage = ''
    var CardTypeTopErrorMessage = ''
    var EmptySelectedCardErrorMessage = ''
    var EmptyNewCardErrorMessage = ''
    var CardBelongToOtherUserErrorMessage = ''

    var Tab2_ChlIWantInvoice = '<%=Tab2_ChlIWantInvoice.ClientID %>'
    var Inv_CompanyNameErrorMessage = ''
    var Inv_OccupationErrorMessage = ''
    var Inv_AddressErrorMessage = ''
    var Inv_TKErrorMessage = ''
    var Inv_CityErrorMessage = ''
    var Inv_AFMErrorMessage = ''
    var Inv_DOYErrorMessage = ''
    var CompleteErrorMessage = ''

    var Tab2_NotValidCardErrorMessage = ''

    var TermsOfUse = ''

    var MerchantMaxTransErrorMessage = ''
    var UsertMaxTransSessionErrorMessage = ''


    //--- CardMessages 
    var Tab2_CardNoSupportFromMerchant = '' //'Η κάρτα δεν υποστηρίζετε από την εταιρία'
    var Tab2_CardMaxLimitPerDay = ''        //'Η κάρτα έχει ξεπεράσει τις μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για την εταιρία'
    var Tab2_CardMaxAmountLimitPerDay = ''  //'Το αθροιστικό ποσό της καρτας ξεπερνά το Μέγιστο επιτρεπόμενο ποσό συναλλαγών ανά Ημέρα για την εταιρία'
    var Tab2_MerchantLimitPerDay = ''       //'Η εταιρία έχει ξεπεράσει<br>τις μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα'



    var NewCard_Tab2_txtCardNo = '<%=Tab2_txtCardNo.ClientID %>'
    var NewCard_NotValidCard = '<%=NewCard_HiddenNotValidCard.ClientID %>'
    var NewCard_NotValidMerchant = '<%=NewCard_HiddenNotValidMerchant.ClientID %>'
    var Tab2_txtFriendName = '<%=Tab2_txtFriendName.ClientID %>'
    //--End---For New Card---

    //--Start--For Saved Cards
    var Tab2_ddlSavedCard = '<%=Tab2_ddlSavedCard.ClientID %>'
    var NotValidCard = '<%=HiddenNotValidCard.ClientID %>'
    var NotValidMerchant = '<%=HiddenNotValidMerchant.ClientID %>'
    //--End--For Saved Cards


    



    var chkCardForSave = '<%=Tab2_ChkSaveDetails.ClientID %>'
    var HiddenChoice = '<%=HiddenChoice.ClientID %>'
    var CompanyName = '';
    var MerchantValidCards = '';
    var _MerchantID = '<%=HiddenMerchantID.ClientID %>'
    var MinimumTransactionAmount = '<%=HiddenMinimumTransactionAmount.ClientID %>';
    var MaximumTransactionAmount = '<%=HiddenMaximumTransactionAmount.ClientID %>';
    var MaximumTransactionsPerDayCard = ''; //-- ok
    var MaximumTransactionsPerDayGlobally = ''; //-- ok
    var MaximumSumTransactionsPerDayInCard = ''; //-- ok
    var MaxTransactionsPerSessionPerCard = 0;


    function AllCardsValidation(Type) {
        if (document.getElementById(HiddenChoice).value == 2) {
            NewCard_CheckValidCard(true, false, Type)
        } else {
            
            CheckValidCard(Type)
        }
         
   }

    function NewCard_CheckValidCard(IsFromValidation, ForChoice,FinalValidation) {
       
        
        //--- For xml
        var xmlhttp;
        if (window.ActiveXObject)
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        else
            xmlhttp = new XMLHttpRequest();

        var TransCountPerMerchantPerCard = 0; //--MaximumTransactionsPerDayCard  ---μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για τον συγκεκριμένο έμπορο
        var TransCountPerMerchant = 0;  //--MaximumTransactionsPerDayGlobally -- Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα gia ton emporo
        var TransSumAmountPerCardPerMerchant = 0; //---MaximumSumTransactionsPerDayInCard -- Το ποσό να μην ξεπερνά το Μέγιστο επιτρεπόμενο ποσό συναλλαγών ανά Ημέρα ανά κάρτα
        var TheCardIsNotValid = true;
        var GetNewCardType = 0;
        //--- For xml
        var CompanyNameValue = CompanyName.value
        var MerchantValidCardsValue = MerchantValidCards.value
        var CardObj = document.getElementById(NewCard_Tab2_txtCardNo);
        var CardObjValue = CardObj.value;
        var ShowCloud = false;

        if (document.getElementById(HiddenChoice).value == 2 ) {    //--- Selected Choice "New Card"
            //----Get Limits Per Card and Merchant
            document.getElementById('Tab2_btnNextCheckNewCard').disabled = true
            document.getElementById('Tab2_NextBtnInvoiceCheckNewCard').disabled = true
            PleaseWait("SavedCardsPlswaitNewCard", true); 
                xmlhttp.open("GET", "/CMSTemplates/LivePay/xmlhttp.aspx?MerchantID=" + MerchantID.value + "&UserCardID=0&CardNo=" + CardObjValue, true);
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4) {
                        var AllLimits = xmlhttp.responseText;
                       
                        TransCountPerMerchantPerCard = GetLimit(AllLimits, 0)
                        TransCountPerMerchant = GetLimit(AllLimits, 1)
                        TransSumAmountPerCardPerMerchant = GetLimit(AllLimits, 2)
                        TheCardIsNotValid = GetLimit(AllLimits, 4)
                        CardBelongToOther_User = GetLimit(AllLimits, 5)
                        document.getElementById('<%=CardBelongToOtherUser.ClientID %>').value = GetLimit(AllLimits, 5)

                        //NotValidCardErrorMessage
                        GetNewCardType = '[' + GetLimit(AllLimits, 3) + ']'

                        if (IsFromValidation) {
                            if (CardObjValue.length == 0) {
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = EmptySelectedCardErrorMessage
                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                NewCardErrorsType('ok', 'ok');
                            }
                        }

                        if (CardObjValue.length > 0) {
                            if (CardObjValue.length != 16) {
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = EmptyNewCardErrorMessage
                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                              
                                NewCardErrorsType('ok', 'ok');
                            }


                            if (CardBelongToOther_User != '0' && document.getElementById(chkCardForSave).checked == true) { //--- IF the Card Belong to other User

                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = CardBelongToOtherUserErrorMessage
                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                document.getElementById('<%=NewCard_HiddenCardBelongToOtherUser.ClientID %>').value = ''
                            } else {
                                document.getElementById('<%=NewCard_HiddenCardBelongToOtherUser.ClientID %>').value = 'ok'
                            }




                            if (TheCardIsNotValid == 'False' && ShowCloud == false) {            //------Check if the Card Is Valid number
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = Tab2_NotValidCardErrorMessage
                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                NewCardErrorsType('', 'ok');
                            }


                            if (MerchantValidCardsValue.indexOf(GetNewCardType) == -1 && ShowCloud == false) {            //------Check if the Card Is Valid for the Merchant

                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = Tab2_CardNoSupportFromMerchant
                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                NewCardErrorsType('', 'ok');
                            }

                            if (parseInt(TransCountPerMerchantPerCard) > parseInt(MaximumTransactionsPerDayCard.value) && ShowCloud == false) { //------Check if μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για την εταιρία
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = Tab2_CardMaxLimitPerDay
                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                NewCardErrorsType('', 'ok');
                            }

                            if (parseFloat(TransSumAmountPerCardPerMerchant) > parseFloat(MaximumSumTransactionsPerDayInCard.value) && ShowCloud == false) { //------Check if Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα του εμπορου
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = Tab2_CardMaxAmountLimitPerDay
                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, true, true, true, '')
                                }
                                NewCardErrorsType('', 'ok');
                            }

                            if (parseInt(TransCountPerMerchant) > parseInt(MaximumTransactionsPerDayGlobally.value) && ShowCloud == false) { //------Check if Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα του εμπορου
                                if (ForChoice != true) {
                                    ShowCloud = true;
                                    CloudContent = Tab2_MerchantLimitPerDay
                                    CreateTheCloud('#SavedCardMsgNewCard', CloudContent, false, true, true, '')
                                }
                                NewCardErrorsType('', 'ok');
                            }
                        }
                        PleaseWait("SavedCardsPlswaitNewCard", false)
                        document.getElementById('Tab2_btnNextCheckNewCard').disabled = false
                        document.getElementById('Tab2_NextBtnInvoiceCheckNewCard').disabled = false
                        

                        if (FinalValidation=='1') {
                            document.getElementById('<%=Tab2_btnNext.ClientID%>').click();
                        }

                        if (FinalValidation == '2') {
                            document.getElementById('<%=Tab2_NextBtnInvoice.ClientID%>').click();
                        }
                        

                    }
                    //PleaseWait("SavedCardsPlswaitNewCard", false)
                }
                xmlhttp.send(null);
                //PleaseWait("SavedCardsPlswaitNewCard", false)
                //--END--Get Limits Per Card and Merchant
        }
        
        if (ShowCloud) {
           
        } else {
            
            document.getElementById(NewCard_NotValidMerchant).value = 'ok';
            document.getElementById(NewCard_NotValidCard).value = 'ok';
            $('#SavedCardMsgNewCard').poshytip('hide');
        }
    }

    function NewCardErrorsType(Card,Merch) {
        document.getElementById(NewCard_NotValidCard).value = Card;
        document.getElementById(NewCard_NotValidMerchant).value = Merch;
    }

    function CheckValidCard(FinalValidation) {
        //--- For xml
        var xmlhttp;
        if (window.ActiveXObject)
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        else
            xmlhttp = new XMLHttpRequest();

        var TransCountPerMerchantPerCard = 0; //--MaximumTransactionsPerDayCard  ---μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για τον συγκεκριμένο έμπορο
        var TransCountPerMerchant = 0;  //--MaximumTransactionsPerDayGlobally -- Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα gia ton emporo
        var TransSumAmountPerCardPerMerchant = 0; //---MaximumSumTransactionsPerDayInCard -- Το ποσό να μην ξεπερνά το Μέγιστο επιτρεπόμενο ποσό συναλλαγών ανά Ημέρα ανά κάρτα 
      
        //--- For xml
        var CompanyNameValue = CompanyName.value
        var MerchantValidCardsValue = MerchantValidCards.value
        var DDL = document.getElementById(Tab2_ddlSavedCard);
        var DDLIndex = DDL.selectedIndex;
        var DDLText = DDL.options[DDLIndex].text;
        var DDLValue = DDL.value;
        var CardTypeFormat = '[' + GetSelectedCardType(DDLValue,'_',1) + ']';
        var ShowCloud = false;

        if (FinalValidation == '1' || FinalValidation == '2') {
            if (DDLValue == '') {
                ShowCloud = true;
                CloudContent = EmptySelectedCardErrorMessage
                CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
            }
        }
        if (document.getElementById(HiddenChoice).value == 1 && DDLValue != '') {
            if (MerchantValidCardsValue.indexOf(CardTypeFormat) == -1) {            //------Check if the Card Is Valid for the Merchant
                ShowCloud = true;
                CloudContent = Tab2_CardNoSupportFromMerchant
                CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
            } else {                                                               //------Check if the Card Is Valid for the Merchant
                //----Get Limits Per Card and Merchant
              
                
                xmlhttp.open("GET", "/CMSTemplates/LivePay/xmlhttp.aspx?MerchantID=" + MerchantID.value + "&UserCardID=" + GetSelectedCardType(DDLValue, '_', 0), true);
                xmlhttp.onreadystatechange = function(){
                    if (xmlhttp.readyState == 4) {
                        var AllLimits = xmlhttp.responseText;
                        TransCountPerMerchantPerCard = GetLimit(AllLimits, 0)
                        TransCountPerMerchant = GetLimit(AllLimits, 1)
                        TransSumAmountPerCardPerMerchant = GetLimit(AllLimits, 2)
                       

                        if (parseInt(TransCountPerMerchantPerCard) > parseInt(MaximumTransactionsPerDayCard.value)) { //------Check if μέγιστες επιτρεπόμενες συναλλαγές ανά ημέρα για την εταιρία
                            ShowCloud = true;
                            CloudContent = Tab2_CardMaxLimitPerDay
                            CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
                        }

                        if (parseFloat(TransSumAmountPerCardPerMerchant) > parseFloat(MaximumSumTransactionsPerDayInCard.value) && ShowCloud == false) { //------Check if Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα του εμπορου
                            ShowCloud = true;
                            CloudContent = Tab2_CardMaxAmountLimitPerDay
                            CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, true, false, false)
                        }

                        if (parseInt(TransCountPerMerchant) > parseInt(MaximumTransactionsPerDayGlobally.value) && ShowCloud == false) { //------Check if Μέγιστες επιτρεπόμενες συναλλαγές ανά Ημέρα του εμπορου
                            ShowCloud = true;
                            CloudContent = Tab2_MerchantLimitPerDay
                            CreateTheCloud_SavedCard('#SavedCardMsg', CloudContent, false, false, false)
                        }

                        PleaseWait("SavedCardsPlswait", false)
                        if (FinalValidation == '1') {
                            document.getElementById('<%=Tab2_btnNext.ClientID%>').click();
                        }

                        if (FinalValidation == '2') {
                            document.getElementById('<%=Tab2_NextBtnInvoice.ClientID%>').click();
                        }
                    }
                }
                xmlhttp.send(null);
                PleaseWait("SavedCardsPlswait", true)
                //--END--Get Limits Per Card and Merchant
            }
        }

        if (ShowCloud) {
        } else {
            document.getElementById(NotValidCard).value = 'ok';
            document.getElementById(NotValidMerchant).value = 'ok';  
            $('#SavedCardMsg').poshytip('hide');
        }
    }


    function SetCustomFieldsValues(objTxt, objSpan, Records) {
        var CustomFieldsContainer = document.getElementById('<%=CustomFields.ClientID %>')
        var AllSelect = CustomFieldsContainer.getElementsByTagName('input');
        var ICount = 0;
        for (i = 0; i < AllSelect.length; i++) {
            if (AllSelect[i].id.indexOf(objTxt) != -1) {
                document.getElementById(objSpan + ICount).innerHTML = AllSelect[i].value;
                ICount = ICount + 1;
            }
        }
    }


    function ShowReceipt() {
        $.blockUI({ css: { border: '0px', backgroundColor: 'transparent', cursor: 'default' }, message: $('#ReceiptPopUp') });
    }

    function CheckToComplete() {
        var IsAccepted = document.getElementById('<%=Tab3_chkTermsAccept.ClientID%>').checked
        if (IsAccepted) {
            return true
        } else {
            alert(CompleteErrorMessage);
            return false   
        }
    }

    var DefaultOffsetTop = 0
    function OpenCustomCloud(str, ObjStr, Object, ObjOpener, Show) {
        var Cloud = document.getElementById(Object)
        var CloudContent = document.getElementById(ObjStr)
        if (Show) {
            var LinkOpener = document.getElementById(ObjOpener)
            CloudContent.innerHTML = str
            Cloud.style.display = ''
            if (document.all) {
//                Cloud.style.top = (ObjOpener.offsetTop - Cloud.offsetHeight + 15) + 'px'
//                Cloud.style.left = (ObjOpener.offsetLeft + ObjOpener.offsetWidth + 15) + 'px'
            } else {
                //Cloud.style.top = (ObjOpener.offsetTop - Cloud.offsetHeight) + 'px'
                Cloud.style.left = (ObjOpener.offsetLeft + ObjOpener.offsetWidth) + 'px'
            }
        } else {
            Cloud.style.display = 'none'
        }
    }

</script>
<script src="/CMSScripts/LivePay/RegisteredUsers.js" type="text/javascript"></script>

<style type="text/css">
div.growlUI { background: url(check48.png) no-repeat 10px 10px;width:200px; }
div.growlUI h1, div.growlUI h2 {font-size:12px;color: white;text-align: left;width:200px}
.test
{
    display:none;
}
</style>


<asp:UpdatePanel ID="updPanel" runat="server" UpdateMode="Conditional">
<ContentTemplate>

<div id="ReceiptPopUp" style="display:none">
        <uc:Receipt ID="Receipt" runat="server" />
</div>
 <div class="RegUserTopMain" style="height:10px">
         <img src="/App_Themes/LivePay/RegisteredUsers/RegUserTopBG.png" />
     </div> 
 <div class="RegUsersMainPage">
    <div id="Tab1" style="display:;">
        <div class="PL10">
            <div class="Tab1MainBG">
                <div id="DivTabsTab1">
                    <div class="RegTopTab1btn"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 1: Στοιχεία Πληρωμής|en-us=ΒΗΜΑ 1: Στοιχεία Πληρωμής$}") %></div>
                    <div class="RegTopTab2_3btn" ><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 2: Στοιχεία Χρέωσης|en-us=ΒΗΜΑ 2: Στοιχεία Χρέωσης$}") %></div>
                    <div class="RegTopTab2_3btn"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 3: Επιβεβαίωση|en-us=ΒΗΜΑ 3: Επιβεβαίωση$}") %></div>
                    <div class="Clear"></div>
                </div>
                <div id="DivFormTab1" class="PL10PT30">
                    <div id="CustomFields" runat="server">
                    </div>
                    <div class="RegUsersTab1CustomTitle"><%=ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}") %></div>
                    <div class="RegUsersTab2_TxtDiv">
                    <a id="Tab1_PriceMsg" href="#"></a>
                    <asp:TextBox cssclass="RegUsersTab1CustomTxt" onkeyup="FixMoney(this,false,true)" onblur="FixMoney(this,true,true)" width="260px" id="Tab1_txtPrice" runat="server" ></asp:TextBox>
                    <div class="HideIt"><asp:CustomValidator ID="Tab1_ReqFldVal_Price" IsNumber="yes" ClientValidationFunction="validateControls" ValidateEmptyText="true" runat="server"   ControlToValidate="Tab1_txtPrice" text="*" ValidationGroup="Tab1_PayForm"/>
                    </div> </div> 
                    <div class="Clear"></div>
                    <div class="RegUsersTab1CustomTitle"><%=ResHelper.LocalizeString("{$=Σημειώσεις|en-us=Σημειώσεις$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox cssclass="RegUsersTab1CustomTxt" width="260px" Rows="4" TextMode="MultiLine" id="Tab1_txtNotes" runat="server" ></asp:TextBox></div>
                    <div class="Clear"></div>
                    <div style="float:left;width:140px;text-align:right;">&nbsp;</div>
                    <div class="RegUsersTab2_TxtDiv"><asp:ImageButton ID="Tab1_BtnNext" ValidationGroup="Tab1_PayForm"   runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/BtnNext.png" /></div>
                    <div class="Clear"></div>
                   
                </div>
            </div>
            <div><img src="/App_Themes/LivePay/RegisteredUsers/Tab1WhiteBottom.png" /></div>
        </div>
    </div>
    <div id="Tab2" style="display:none">
        <div class="PL10">
            <div class="Tab2MainBG">
                <div id="DivTabsTab2">
                    <div class="RegTopTab1btn" ><%= ResHelper.LocalizeString("{$=ΒΗΜΑ 1: Στοιχεία Πληρωμής|en-us=ΒΗΜΑ 1: Στοιχεία Πληρωμής$}")%></div>
                    <div class="RegTopTab2_3btn" ><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 2: Στοιχεία Χρέωσης|en-us=ΒΗΜΑ 2: Στοιχεία Χρέωσης$}") %></div>
                    <div class="RegTopTab2_3btn" ><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 3: Επιβεβαίωση|en-us=ΒΗΜΑ 3: Επιβεβαίωση$}") %></div>
                    <div class="Clear"></div>
                </div>
                <div id="DivFormTab2" class="PL10PT30">
                    <div id="ForRegUsersTop" runat="server">
                        <div class="Tab2RAdChoice1">
                            <asp:RadioButton Checked="true" cssclass="RegUserRadioA" ID="Tab2_RbSavedCard"  runat="server" />
                        </div>
                        <div class="RegUserMainSavedCards">
                         <a id="SavedCardMsg" href="#"></a><a id="SavedCardMsgSec" href="#"></a><div id="SavedCardsPlswait" class="PleaseWait"><img src="/App_Themes/LivePay/LoadingProgressBar.gif" /></div><asp:DropDownList ID="Tab2_ddlSavedCard" runat="server" cssclass="REgUserDDL" width="230px" ></asp:DropDownList>
                         <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_SavedCard" ErrorMessage="CVV2" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" isDDLSavedCards="yes" IsSavedCards="yes" ControlToValidate="Tab2_ddlSavedCard" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                         <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_iddenNotValidCard" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsCardHidden="yes" IsSavedCards="yes" ControlToValidate="HiddenNotValidCard" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                         <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_iddenNotValidMerchant" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsCardHidden="yes" IsSavedCards="yes" ControlToValidate="HiddenNotValidMerchant" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                     
                        </div>
                        <div class="RegUserCVVTopTitle">CVV2</div> 
                        <div class="RegUserCVVTitleB"><a id="SavedCardCVV2Msg" href="#"></a><asp:TextBox autocomplete="off" cssclass="RegUsersTab1CustomTxt" width="110px" id="Tab2_txtCVV2Top" MaxLength="3" runat="server" ></asp:TextBox></div> 
                        <div class="Tab2CVV2Div" onmouseover="ShowClound('CVV2Cloud',this)" onmouseout="HideClound('CVV2Cloud')"><img  src="/App_Themes/LivePay/InfoBtn.png" />
                            <div style="display:none;" class="RegUsersMainClouds" id="CVV2Cloud"><img src="/App_Themes/LivePay/RegisteredUsers/Tab1CloudCustCode.png" /></div>
                            <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_CVV2Top" ErrorMessage="CVV2" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsNumber="yes" LengthLimit="3" CheckLength="yes" IsSavedCards="yes" ControlToValidate="Tab2_txtCVV2Top" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        </div>
                        <div class="Clear"></div> 
                        <div style="padding-top:4px"><div style="height:2px;background-color:#f1f1f1;width:650;font-size:1px;">&nbsp;</div></div>
                    </div>
                    <div class="RegUsersTab2_RB2Div" id="Tab2_RB2Div">
                        <div id="Tab2_RbNewCardDivCont" runat="server"><asp:RadioButton cssclass="RegUserRadioA" ID="Tab2_RbNewCard" runat="server" /></div>
                    </div>
                    
                    
                    <div class="RegUserTitleA"><%=ResHelper.LocalizeString("{$=Τύπος Κάρτας|en-us=Τύπος Κάρτας$}") %></div>
                    <div class="RegUsersTab2_TxtDivSec"><a id="CardTypeMsg" href="#"></a><asp:DropDownList cssclass="REgUserDDL" width="264px" id="Tab2_ddlCardType" runat="server" ></asp:DropDownList>
                         <div class="HideIt"><asp:CustomValidator id="Tab2_ReqFldVal_CardType"  runat="server" ValidationGroup="Tab2_PayForm" ValidateEmptyText="true" IsCardType="yes" ControlToValidate = "Tab2_ddlCardType" ClientValidationFunction="validateControlsTabB" ></asp:CustomValidator></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" >(*)</div>
                    <div class="Clear"></div>
                    <div class="RegUserTitleA"><%=ResHelper.LocalizeString("{$=Αριθμός Κάρτας|en-us=Αριθμός Κάρτας$}") %></div>
                    <div class="RegUsersTab2_TxtDivSec">
                    <a id="SavedCardMsgNewCard" href="#"></a><div id="SavedCardsPlswaitNewCard" class="PleaseWait"><img src="/App_Themes/LivePay/LoadingProgressBar.gif" /></div>
                    <asp:TextBox cssclass="RegUsersTab1CustomTxt" autocomplete="off" width="260px" id="Tab2_txtCardNo" onkeyup="ClearCharcters(this,false)" onblur="ClearCharcters(this,true)" runat="server" MaxLength="16" ></asp:TextBox>
                         <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_NewCardBelognToOtherUser" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" isCVVChecked="yes" runat="server"  ControlToValidate="NewCard_HiddenCardBelongToOtherUser"  text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_NewCardNotValCard" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" isCVVChecked="yes" runat="server"  ControlToValidate="NewCard_HiddenNotValidCard"  text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_NewCardNotValMerchant" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" isCVVChecked="yes" runat="server" isTest="yes" ControlToValidate="NewCard_HiddenNotValidMerchant" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_CardNo" runat="server" ValidationGroup="Tab2_PayForm" ValidateEmptyText="true" IsNewCardNo="yes" LengthLimit="16" CheckLength="yes" IsNumber="yes" ClientValidationFunction="validateControlsTabB"  ControlToValidate="Tab2_txtCardNo" text="*" /></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" >(*)</div>
                    <div class="Clear"></div>
                    <div class="RegUserTitleB"><%=ResHelper.LocalizeString("{$=Ονοματεπώνυμο Κατόχου|en-us=Ονοματεπώνυμο Κατόχου$}") %></div> 
                    <div class="RegUsersTab2_TxtDivSec"><a id="NewCardFullNameMsg" href="#"></a><asp:TextBox autocomplete="off" cssclass="RegUsersTab1CustomTxt" MaxLength="150" width="260px" id="Tab2_txtFullNameOwner" runat="server" ></asp:TextBox>
                        <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_FullNameOwner" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsFullName="yes" ControlToValidate="Tab2_txtFullNameOwner" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" >(*)</div>
                    <div class="Clear"></div>
                    <div class="RegUserTitleB"><%=ResHelper.LocalizeString("{$=Ημερομηνία<Br />Λήξης|en-us=Ημερομηνία<Br />Λήξης$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:DropDownList cssclass="REgUserDDL" width="112px" id="Tab2_DdlCardMonth" runat="server" ></asp:DropDownList>
                        <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_CardMonth" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server"  ControlToValidate="Tab2_DdlCardMonth" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="RegUsersMainDivCloud" >(*)</div>
                    <div class="RegUsersTab2_TxtDiv"><asp:DropDownList cssclass="REgUserDDL" width="112px" id="Tab2_DdlCardYear" runat="server" ></asp:DropDownList>
                        <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_CardYear" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" ControlToValidate="Tab2_DdlCardYear" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        
                    </div>
                    <div class="RegUsersMainDivCloud" >(*)</div>
                    <div class="Clear"></div>
                    <div class="RegUserTitleB">CVV2</div>
                    <div class="RegUserNewCVV2DIV"><a id="NewCardCVVMsg" href="#"></a><asp:TextBox MaxLength="3" autocomplete="off" cssclass="RegUsersTab1CustomTxt" width="108px" id="Tab2_txtCVV2" runat="server" ></asp:TextBox>
                        <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_CVV2" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" IsNewCardCVV="yes" IsNumber="yes" LengthLimit="3" CheckLength="yes" ErrorMessage="CVV2" runat="server" ControlToValidate="Tab2_txtCVV2" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        
                    </div>
                    <div class="Tab2CVV2Div" >(*)</div>
                    <div class="Clear"></div>
                    <div id="ForRegUsersChoiceToSave" runat="server">
                        <div class="RegUsersSeprSaveCard">&nbsp;</div>
                        <div class="FLEFT"><asp:CheckBox ID="Tab2_ChkSaveDetails" cssclass="RegUSerControlA" runat="server" Text="Αποθήκευση Στοιχείων Κάρτας για Μελλοντική Χρήση" /></div>
                        <div class="Clear"></div>
                        <div style="min-height:34px">
                            <div id="friendlyNameMainDiv" style="display:none">
                                <div class="RegUserTitleB"><%=ResHelper.LocalizeString("{$=Φιλική Ονομασία|en-us=Φιλική Ονομασία$}") %></div>
                                <div class="RegUsersTab2_TxtDivSec" style="padding-top:10px"><a id="NewCardFriendly" href="#"></a><asp:TextBox autocomplete="off" cssclass="RegUsersTab1CustomTxt" width="260px" id="Tab2_txtFriendName" runat="server" MaxLength="30" ></asp:TextBox>
                                    <div class="HideIt"><asp:CustomValidator ID="Tab2_ReqFldVal_FriendName" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsFriendlyName="yes" ControlToValidate="Tab2_txtFriendName" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                                </div>
                                <div class="RegUsersMainDivCloud" style="padding-top:12px" >(*)</div>
                                 <div class="Clear"></div>
                            </div>
                        </div>
                    </div>
                    <div id="ForUnRegUsers_PhoneMail" runat="server" style="display:none">
                        <div style="padding-top:4px"><div style="height:2px;background-color:#f1f1f1;width:650;font-size:1px;">&nbsp;</div></div>
                        <div style="padding-top:5px;">
                        <%=ResHelper.LocalizeString("{$=Σε περίπτωση που θέλετε να παραλάβετε το αποδεικτικό της συναλλαγής, συμπληρώστε το email σας|en-us=Σε περίπτωση που θέλετε να παραλάβετε το αποδεικτικό της συναλλαγής, συμπληρώστε το email σας$}") %>
                    </div>
                        <div class="RegUsersTab2_PhoneDiv" id="Tab2_PhoneDiv">
                            <div class="RegUserTitleA"><%= ResHelper.LocalizeString("{$=Τηλέφωνο|en-us=Τηλέφωνο$}")%></div>
                            <div class="RegUsersTab2_TxtDivSec"><a id="UnRegUserPhoneMsg" href="#"></a><asp:TextBox autocomplete="off" MaxLength="10"  cssclass="RegUsersTab1CustomTxt" width="260px" id="Tab2_txtPhone" runat="server" ></asp:TextBox>
                                <div class="HideIt"><asp:CustomValidator ID="UnRegUserPhoneValid" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsPhone="yes" IsNumber="yes" LengthLimit="10" CheckLength="yes" IsUnRegField="yes" ControlToValidate="Tab2_txtPhone" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                            </div>
                            <div class="RegUsersMainDivCloud" style="padding-top:12px" >(*)</div>
                        <div class="Clear"></div>
                        </div>
                        <div class="RegUserTitleA"><%= ResHelper.LocalizeString("{$=E-mail|en-us=E-mail$}")%></div>
                        <div class="RegUsersTab2_TxtDivSec"><a id="UnRegUserEmailMsg" href="#"></a><asp:TextBox autocomplete="off" cssclass="RegUsersTab1CustomTxt" MaxLength="150" width="260px" id="Tab2_txtEmail" runat="server" ></asp:TextBox>
                        <div class="HideIt"><asp:CustomValidator ID="UnRegUserEmailValid" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsUnRegField="yes" IsEmail="yes" ControlToValidate="Tab2_txtEmail" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                        </div>
                        <div class="Clear"></div>
                    </div>
                     
                   <div style="padding-top:4px"><div style="height:2px;background-color:#f1f1f1;width:650;font-size:1px;">&nbsp;</div></div>
                    <div class="RegUsersTab2_BtnNextDiv" id="Tab2_BtnNextDiv">
                        <div style="float:left;width:128px;">&nbsp;</div>
                        <div class="FLEFT"><asp:ImageButton ID="Tab2_btnReturn" OnClientClick="ShowHideTabs('','none','none');return false"  runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/btnReturn.png" /></div>
                        <div style="float:left;padding-left:10px">
                        <img id="Tab2_btnNextCheckNewCard" style="cursor:pointer" src="/App_Themes/LivePay/RegisteredUsers/BtnNext.png" onclick="AllCardsValidation('1')" /> 
                        <asp:ImageButton style="display:none" ID="Tab2_btnNext" ValidationGroup="Tab2_PayForm" runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/BtnNext.png" />
                        </div>
                        <div class="Clear"></div>
                        
                    </div>
                    <div class="REgUsersBottomMust">
                        <%=ResHelper.LocalizeString("{$=Τα πεδία με τον αστερίσκο (*) είναι υποχρεωτικά|en-us=Τα πεδία με τον αστερίσκο (*) είναι υποχρεωτικά$}") %>
                    </div>
                    <div>
                        <asp:CheckBox ID="Tab2_ChlIWantInvoice" onclick="ShowHideInvoice(this)" cssclass="RegUSerControlA" runat="server"  />
                    </div>
                </div>       
                <div id="InvoiceDiv" style="padding-left:10px;background-color:White;padding-bottom:5px;display:none;">
                    <div class="RegUserTitleA"><%=ResHelper.LocalizeString("{$=Επωνυμία Επιχείρησης|en-us=Επωνυμία Επιχείρησης$}") %></div>
                    <div class="RegUsersTab2_TxtDivSec"><a id="Inv_CompanyNameMsg" href="#"></a><asp:TextBox autocomplete="off" CssClass="RegUsersTab1CustomTxt" MaxLength="200" width="260px" id="Tab2Inv_CompanyName" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvCompanyName" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_CompanyName="yes" IsInvoice="yes" ControlToValidate="Tab2Inv_CompanyName" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div> 
                    <div class="Clear"></div>
                    <div class="RegUserTitleB"><%=ResHelper.LocalizeString("{$=Επάγγελμα|en-us=Επάγγελμα$}") %></div>
                    <div class="RegUsersTab2_TxtDivSec"><a id="Inv_OccupationMsg" href="#"></a><asp:TextBox autocomplete="off" CssClass="RegUsersTab1CustomTxt" MaxLength="150" width="260px" id="Tab2Inv_Occupation" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvOccupation" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_Occupation="yes" IsInvoice="yes" ControlToValidate="Tab2Inv_Occupation" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="Clear"></div>

                    <div class="RegUserTitleB"><%=ResHelper.LocalizeString("{$=Διεύθυνση|en-us=Διεύθυνση$}") %></div>
                    <div class="RegUsersTab2_TxtDivSec"><a id="Inv_AddressMsg" href="#"></a><asp:TextBox autocomplete="off" CssClass="RegUsersTab1CustomTxt" MaxLength="200" width="260px" id="Tab2Inv_Address" runat="server" ></asp:TextBox>
                      <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvAddress" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_Address="yes" IsInvoice="yes" ControlToValidate="Tab2Inv_Address" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="Clear"></div>

                    <div class="RegUserTitleB"><%=ResHelper.LocalizeString("{$=Πόλη|en-us=Πόλη$}") %></div>
                    <div class="RegUsersTab2_TxtDivSec"><a id="Inv_CityMsg" href="#"></a><asp:TextBox autocomplete="off" CssClass="RegUsersTab1CustomTxt" width="122px" MaxLength="100" id="Tab2Inv_City" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvCity" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_City="yes"  IsInvoice="yes" ControlToValidate="Tab2Inv_City" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="Clear"></div>
                    <div class="RegUserTitleB"><%=ResHelper.LocalizeString("{$=Τ.Κ.|en-us=Τ.Κ.$}") %></div>
                    <div class="RegUsersTab2_TxtDivSec" ><a id="Inv_TKMsg" href="#"></a><asp:TextBox autocomplete="off" MaxLength="5" CssClass="RegUsersTab1CustomTxt" width="60px" id="Tab2Inv_TK" runat="server" ></asp:TextBox>
                        <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvTK" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_TK="yes" IsInvoice="yes" IsNumber="yes" LengthLimit="5" CheckLength="yes" ControlToValidate="Tab2Inv_TK" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="Clear"></div>
                    

                    <div class="RegUserTitleB"><%=ResHelper.LocalizeString("{$=ΑΦΜ|en-us=ΑΦΜ$}") %></div>
                    <div class="RegUsersTab2_TxtDivSec"><a id="Inv_AFMMsg" href="#"></a><asp:TextBox autocomplete="off" MaxLength="9" CssClass="RegUsersTab1CustomTxt" width="200px" id="Tab2Inv_AFM" runat="server"  ></asp:TextBox>
                        <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvAFM" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_AFM="yes" IsInvoice="yes" IsNumber="yes" LengthLimit="9" CheckLength="yes" ControlToValidate="Tab2Inv_AFM" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    </div>
                    <div class="Clear"></div>
                     <div class="RegUserTitleB"><%=ResHelper.LocalizeString("{$=ΔΟΥ|en-us=ΔΟΥ$}") %></div>
                    <div class="RegUsersTab2_TxtDivSec"><a id="Inv_DOYMsg" href="#"></a><asp:DropDownList cssclass="REgUserDDL" ID="ddlDoy" runat="server" ></asp:DropDownList>
                    <div style="display:none"><asp:CustomValidator ID="Tab2_CustVal_InvDOY" ClientValidationFunction="validateControlsTabB" ValidateEmptyText="true" runat="server" IsInv_DOYMsg="yes" IsInvoice="yes" ControlToValidate="ddlDoy" text="*" ValidationGroup="Tab2_PayForm"/></div> 
                    
                    </div>
                    <div class="Clear"></div>
                     <div class="RegUsersTab2_BtnNextDiv">
                        <div style="float:left;width:128px;">&nbsp;</div>
                        <div class="FLEFT">
                         
                        <asp:ImageButton ID="ImageButton2" OnClientClick="ShowHideTabs('','none','none');return false"  runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/btnReturn.png" />
                        </div>
                        <div style="float:left;padding-left:10px">
                        <img id="Tab2_NextBtnInvoiceCheckNewCard" style="cursor:pointer" src="/App_Themes/LivePay/RegisteredUsers/BtnNext.png" onclick="AllCardsValidation('2')" />
                        <asp:ImageButton ID="Tab2_NextBtnInvoice" style="display:none" ValidationGroup="Tab2_PayForm" runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/BtnNext.png" />
                        </div>
                        <div class="Clear"></div>
                    </div>
                </div>
                <div><img src="/App_Themes/LivePay/RegisteredUsers/Tab1WhiteBottom.png" /></div>
            </div>
        </div>
    </div>

    
    <div id="Tab3" style="display:none">
        <div class="PL10">
            <div style="background-image:url('/App_Themes/LivePay/RegisteredUsers/Tab3BG.png');background-repeat:no-repeat;width:650px;min-height:340px;background-color:White">
                <div id="DivTabsTab3">
                    <div class="RegTopTab1btn"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 1: Στοιχεία Πληρωμής|en-us=ΒΗΜΑ 1: Στοιχεία Πληρωμής$}") %></div>
                    <div class="RegTopTab2_3btn"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 2: Στοιχεία Χρέωσης|en-us=ΒΗΜΑ 2: Στοιχεία Χρέωσης$}") %></div>
                    <div class="RegTopTab2_3btn"><%=ResHelper.LocalizeString("{$=ΒΗΜΑ 3: Επιβεβαίωση|en-us=ΒΗΜΑ 3: Επιβεβαίωση$}") %></div>
                    <div class="Clear"></div>
                </div>
                <div id="DivFormTab3" style="padding-left:10px;padding-top:20px">
                    <div style="font-size:11px;font-family:Tahoma;color:#43474a"><%=ResHelper.LocalizeString("{$=Διαβάστε προσεχτικά τα παρακάτω και πατήστε το κουμπί <b>Ολοκλήρωση</b> προκειμένου να πραγματοποιήσετε την Πληρωμή|en-us=Διαβάστε προσεχτικά τα παρακάτω και πατήστε το κουμπί <b>Ολοκλήρωση</b> προκειμένου να πραγματοποιήσετε την Πληρωμή$}") %></div>
                    <div class="RegUserTab3Title"><%= ResHelper.LocalizeString("{$=Πληρωμή προς|en-us=Πληρωμή προς$}")%></div>
                    <div style="float:left;padding-left:15px;padding-top:24px"><asp:Label ID="Tab3_lblPayTo" runat="server" CssClass="RegUSerTab3Control" ></asp:Label></div>
                    <div class="Clear"></div>
                    <div id="Tab3_CustomFields" runat="server">
                    </div>
                    <div class="RegUserTab3TitleB"><%= ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}")%></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:Label ID="Tab3_lblPrice" runat="server" CssClass="RegUSerTab3Control" ></asp:Label></div>
                    <div class="Clear"></div>
                    <div class="RegUserTab3TitleB"><%= ResHelper.LocalizeString("{$=Κάρτα|en-us=Κάρτα$}")%></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:Label ID="Tab3_lblCard" runat="server" CssClass="RegUSerTab3Control" ></asp:Label></div>
                    <div class="Clear"></div>
                    <div class="RegUserTab3TitleB"><%=ResHelper.LocalizeString("{$=Όροι <br />Συναλλαγής|en-us=Οροι<br />Χρήσης$}") %></div>
                    <div class="RegUsersTab2_TxtDiv"><asp:TextBox ReadOnly="true"  cssclass="RegUsersTab1CustomTxt" TextMode="MultiLine" Rows="7" width="320px" id="Tab3_lblTerms" runat="server" ></asp:TextBox></div>
                    <div class="Clear"></div>
                    <div style="float:left;color:#43474a;font-size:12px;width:140px;">&nbsp;</div>
                    <div style="float:left;padding-left:11px;"><asp:CheckBox ID="Tab3_chkTermsAccept" cssclass="RegUSerControlA" runat="server" Text="Αποδέχομαι τους όρους συναλλαγής" /></div>
                    <div class="Clear"></div>
                     <div style="padding-top:12px;">
                        <div style="float:left;width:155px;">&nbsp;</div>
                        <div class="FLEFT"><asp:ImageButton ID="Tab3_btnReturn"  OnClientClick="ShowHideTabs('none','','none');return false" runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/btnReturn.png" /></div>
                        <div style="float:left;padding-left:10px"><asp:ImageButton ID="Tab3_btnComplete" OnClientClick="return CheckToComplete()" runat="server" ImageUrl="/App_Themes/LivePay/RegisteredUsers/btnComplete.png" /></div>
                        <div class="Clear"></div>
                    </div>
                </div>
                 <div><img src="/App_Themes/LivePay/RegisteredUsers/Tab1WhiteBottom.png" /></div>
            </div>
        </div>
    </div>
     <div class="RegUserBotMain" style="height:10px">
         <img src="/App_Themes/LivePay/RegisteredUsers/RegUserBotBG.png" />
     </div> 
 </div>

    <asp:TextBox ID="HiddenChoice" runat="server" Text="1" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenCompanyName" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenMerchantValidCards" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenMerchantID" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenMinimumTransactionAmount" runat="server" style="display:none"></asp:TextBox> 
    <asp:TextBox ID="HiddenMaximumTransactionAmount" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenMaximumTransactionsPerDayCard" runat="server" style="display:none"></asp:TextBox> 
    <asp:TextBox ID="HiddenMaximumTransactionsPerDayGlobally" runat="server" style="display:none"></asp:TextBox> 
    <asp:TextBox ID="HiddenMaximumSumTransactionsPerDayInCard" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenMaxTransactionsPerSessionPerCard" runat="server" style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenNotValidCard" runat="server" Text="ok"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="HiddenNotValidMerchant" runat="server" Text="ok"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="NewCard_HiddenCardBelongToOtherUser" runat="server" Text="ok"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="NewCard_HiddenNotValidCard" runat="server" Text="ok"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="NewCard_HiddenNotValidMerchant" runat="server" Text="ok"  style="display:none"></asp:TextBox>
    <asp:TextBox ID="CardBelongToOtherUser" runat="server"  style="display:none"></asp:TextBox>
    
<asp:Literal ID="lit_JsScript" runat="server" ></asp:Literal>
</ContentTemplate>
</asp:UpdatePanel>
  <script language="javascript" type="text/javascript" >
     
      FixChromeHeight()
      function FixChromeHeight() {
          var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
          if (is_chrome) {
//              document.getElementById('Tab2_PhoneDiv').className = 'RegUsersTab2_PhoneDivChrome'
//              document.getElementById('Tab2_BtnNextDiv').className = 'RegUsersTab2_BtnNextDivChrome'
              document.getElementById('Tab2_RB2Div').className = 'RegUsersTab2_RB2DivChrome'
          }

      }

      CompanyName = document.getElementById('<%=HiddenCompanyName.ClientID %>')
      MerchantValidCards = document.getElementById('<%=HiddenMerchantValidCards.ClientID %>')
      MaximumTransactionsPerDayCard = document.getElementById('<%=HiddenMaximumTransactionsPerDayCard.ClientID %>')
      MaximumTransactionsPerDayGlobally = document.getElementById('<%=HiddenMaximumTransactionsPerDayGlobally.ClientID %>')
      MaximumSumTransactionsPerDayInCard = document.getElementById('<%=HiddenMaximumSumTransactionsPerDayInCard.ClientID %>')
      MaxTransactionsPerSessionPerCard = document.getElementById('<%=HiddenMaxTransactionsPerSessionPerCard.ClientID %>')
      MerchantID = document.getElementById('<%=HiddenMerchantID.ClientID %>')
     
     
  </script>