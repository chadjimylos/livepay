CREATE TABLE [CMS_Tree] (
		[NodeID]                        int NOT NULL IDENTITY(1, 1),
		[NodeAliasPath]                 nvarchar(450) NOT NULL,
		[NodeName]                      nvarchar(100) NOT NULL,
		[NodeAlias]                     nvarchar(50) NOT NULL,
		[NodeClassID]                   int NOT NULL,
		[NodeParentID]                  int NOT NULL,
		[NodeLevel]                     int NOT NULL,
		[NodeACLID]                     int NULL,
		[NodeSiteID]                    int NOT NULL,
		[NodeGUID]                      uniqueidentifier NOT NULL,
		[NodeOrder]                     int NULL,
		[IsSecuredNode]                 bit NULL,
		[NodeCacheMinutes]              int NULL,
		[NodeSKUID]                     int NULL,
		[NodeDocType]                   nvarchar(max) NULL,
		[NodeHeadTags]                  nvarchar(max) NULL,
		[NodeBodyElementAttributes]     nvarchar(max) NULL,
		[NodeInheritPageLevels]         nvarchar(200) NULL,
		[NodeChildNodesCount]           int NULL,
		[RequiresSSL]                   int NULL,
		[NodeLinkedNodeID]              int NULL,
		[NodeOwner]                     int NULL,
		[NodeCustomData]                nvarchar(max) NULL,
		[NodeGroupID]                   int NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Tree]
	ADD
	CONSTRAINT [PK_CMS_Tree]
	PRIMARY KEY
	NONCLUSTERED
	([NodeID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_ACL]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Tree_NodeACLID_CMS_ACL]') IS NULL
BEGIN
		ALTER TABLE [CMS_Tree]
			ADD CONSTRAINT [FK_CMS_Tree_NodeACLID_CMS_ACL]
			FOREIGN KEY ([NodeACLID]) REFERENCES [CMS_ACL] ([ACLID])
END
IF OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Tree_NodeClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_Tree]
			ADD CONSTRAINT [FK_CMS_Tree_NodeClassID_CMS_Class]
			FOREIGN KEY ([NodeClassID]) REFERENCES [CMS_Class] ([ClassID])
END
IF OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[Community_Group]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Tree_NodeGroupID_Community_Group]') IS NULL
BEGIN
		ALTER TABLE [CMS_Tree]
			ADD CONSTRAINT [FK_CMS_Tree_NodeGroupID_Community_Group]
			FOREIGN KEY ([NodeGroupID]) REFERENCES [Community_Group] ([GroupID])
END
IF OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Tree_NodeLinkedNodeID_CMS_Tree]') IS NULL
BEGIN
		ALTER TABLE [CMS_Tree]
			ADD CONSTRAINT [FK_CMS_Tree_NodeLinkedNodeID_CMS_Tree]
			FOREIGN KEY ([NodeLinkedNodeID]) REFERENCES [CMS_Tree] ([NodeID])
END
IF OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Tree_NodeOwner_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_Tree]
			ADD CONSTRAINT [FK_CMS_Tree_NodeOwner_CMS_User]
			FOREIGN KEY ([NodeOwner]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Tree_NodeSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_Tree]
			ADD CONSTRAINT [FK_CMS_Tree_NodeSiteID_CMS_Site]
			FOREIGN KEY ([NodeSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Tree_NodeSKUID_COM_SKU]') IS NULL
BEGIN
		ALTER TABLE [CMS_Tree]
			ADD CONSTRAINT [FK_CMS_Tree_NodeSKUID_COM_SKU]
			FOREIGN KEY ([NodeSKUID]) REFERENCES [COM_SKU] ([SKUID])
END
CREATE INDEX [IX_CMS_Tree_IsSecuredNode_RequiresSSL_NodeCacheMinutes]
	ON [CMS_Tree] ([IsSecuredNode], [RequiresSSL], [NodeCacheMinutes])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Tree_NodeACLID]
	ON [CMS_Tree] ([NodeACLID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Tree_NodeAliasPath]
	ON [CMS_Tree] ([NodeAliasPath])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Tree_NodeClassID]
	ON [CMS_Tree] ([NodeClassID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Tree_NodeGroupID]
	ON [CMS_Tree] ([NodeGroupID])
	ON [PRIMARY]
CREATE UNIQUE CLUSTERED INDEX [IX_CMS_Tree_NodeID]
	ON [CMS_Tree] ([NodeID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Tree_NodeID_NodeSiteID]
	ON [CMS_Tree] ([NodeID], [NodeSiteID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Tree_NodeLevel]
	ON [CMS_Tree] ([NodeLevel])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Tree_NodeLinkedNodeID]
	ON [CMS_Tree] ([NodeLinkedNodeID])
	INCLUDE ([NodeID], [NodeClassID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Tree_NodeOwner]
	ON [CMS_Tree] ([NodeOwner])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Tree_NodeParentID_NodeAlias_NodeName]
	ON [CMS_Tree] ([NodeParentID], [NodeAlias], [NodeName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE UNIQUE INDEX [IX_CMS_Tree_NodeSiteID_NodeGUID]
	ON [CMS_Tree] ([NodeSiteID], [NodeGUID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Tree_NodeSKUID]
	ON [CMS_Tree] ([NodeSKUID])
	ON [PRIMARY]
