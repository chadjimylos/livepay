using System;

using CMS.GlobalHelper;
using CMS.Polls;
using CMS.UIControls;

public partial class CMSModules_Polls_Tools_Polls_Header : CMSPollsPage
{
    protected int pollId;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get the poll ID
        pollId = QueryHelper.GetInteger("pollId", 0);
        
        PollInfo pi = PollInfoProvider.GetPollInfo(pollId);
        if (pi != null)
        {
            if (!RequestHelper.IsPostBack())
            {
                InitalizeMenu();
            }

            // Initialize the page title
            string[,] breadcrumbs = new string[2, 3];
            breadcrumbs[0, 0] = ResHelper.GetString("Polls_Edit.itemlistlink");
            breadcrumbs[0, 1] = "~/CMSModules/Polls/Tools/Polls_List.aspx";
            breadcrumbs[0, 2] = "_parent";
            breadcrumbs[1, 0] = pi.PollDisplayName;
            breadcrumbs[1, 1] = "";
            breadcrumbs[1, 2] = "";

            this.CurrentMaster.Title.TitleText = ResHelper.GetString("Polls_Edit.headercaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Polls_Poll/object.png");
            this.CurrentMaster.Title.Breadcrumbs = breadcrumbs;
            this.CurrentMaster.Title.HelpTopicName = "general_tab4";
            this.CurrentMaster.Title.HelpName = "helpTopic";
        }
    }


    /// <summary>
    /// Initializes edit menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string[,] tabs = new string[5, 4];
        tabs[0, 0] = ResHelper.GetString("general.general");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab4');";
        tabs[0, 2] = "Polls_General.aspx?pollId=" + pollId;
        tabs[1, 0] = ResHelper.GetString("Polls_Edit.Answers");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'answer_list');";
        tabs[1, 2] = "Polls_Answer_List.aspx?pollId=" + pollId;
        tabs[2, 0] = ResHelper.GetString("general.security");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'security3');";
        tabs[2, 2] = "Polls_Security.aspx?pollId=" + pollId;
        tabs[3, 0] = ResHelper.GetString("general.sites");
        tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'sites');";
        tabs[3, 2] = "Polls_Sites.aspx?pollId=" + pollId;
        tabs[4, 0] = ResHelper.GetString("general.view");
        tabs[4, 1] = ""; // "SetHelpTopic('helpTopic', 'view');";
        tabs[4, 2] = "Polls_View.aspx?pollId=" + pollId;

        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "pollContent";
    }
}
