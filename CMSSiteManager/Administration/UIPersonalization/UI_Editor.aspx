<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UI_Editor.aspx.cs" Inherits="CMSSiteManager_Administration_UIPersonalization_UI_Editor"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" Title="UI Personalization - Dialogs" %>

<%@ Register Src="~/CMSSiteManager/Administration/UIPersonalization/UIPersonalization.ascx"  TagName="UIPersonalization" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <div style="position: absolute; width: 100%; height: 100%; top: 0px; bottom: 0px;">
        <cms:UIPersonalization runat="server" ID="editElem" />
    </div>
</asp:Content>
