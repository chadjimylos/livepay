using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.MessageBoard;
using CMS.UIControls;
using CMS.Community;

public partial class CMSModules_Groups_Tools_MessageBoards_Boards_Board_List : CMSGroupPage
{
    private int mGroupId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.mGroupId = QueryHelper.GetInteger("groupid", 0);

        this.boardList.IsLiveSite = false;
        this.boardList.GroupID = mGroupId;
        this.boardList.OnAction += new CommandEventHandler(boardList_OnAction);
        this.boardList.OnCheckPermissions += new CMSAdminControl.CheckPermissionsEventHandler(boardList_OnCheckPermissions);
        this.boardList.GridName = "~/CMSModules/Groups/Tools/MessageBoards/Boards/Board_List.xml";

        // Initialize the controls
        InitializeMasterPage();
    }


    void boardList_OnAction(object sender, CommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "edit":
                int boardId = ValidationHelper.GetInteger(e.CommandArgument, 0);

                // Create a target site URL and pass the category ID as a parameter
                string editUrl = "Board_Edit.aspx?boardid=" + boardId.ToString() + ((this.mGroupId > 0) ? "&groupid=" + this.mGroupId : "");
                UrlHelper.Redirect(editUrl);
                break;

            default:
                break;
        }
    }


    void boardList_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        // Check 'Manage' permission
        CheckPermissions(this.boardList.GroupID, CMSAdminControl.PERMISSION_MANAGE);
    }


    /// <summary>
    /// Initializes the controls on the page
    /// </summary>
    private void InitializeMasterPage()
    {
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Board_Board/object.png");
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("board.header.boards");
        this.CurrentMaster.Title.HelpTopicName = "board_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }
}
