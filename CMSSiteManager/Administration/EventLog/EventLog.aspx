<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventLog.aspx.cs" Inherits="CMSSiteManager_Administration_EventLog_EventLog"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="SiteManager - Event log"
    Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSSiteManager/Administration/EventLog/EventFilter.ascx" TagName="EventFilter"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcBeforeContent" runat="server">
    <asp:Panel ID="PanelTop" runat="server" CssClass="PageHeaderLine">
        <table style="width: 100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <asp:Panel ID="pnlSites" runat="server">
                        <asp:Label runat="server" ID="lblSite" EnableViewState="false" />
                        <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" />
                    </asp:Panel>
                </td>
                <td class="TextRight">
                    <cms:CMSButton runat="server" ID="btnClearLog" OnClick="ClearLogButton_Click" CssClass="LongButton"
                        EnableViewState="false" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
<asp:Content ID="plcContent" ContentPlaceHolderID="plcContent" runat="server">
    <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cms:EventFilter runat="server" ID="cntFilter" />
            <br />
            <cms:UniGrid runat="server" ID="gridEvents" GridName="EventLog.xml" OrderBy="EventTime DESC"
                IsLiveSite="false" />
        </ContentTemplate>
    </cms:CMSUpdatePanel>
</asp:Content>
