<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Security.aspx.cs" Inherits="CMSModules_Content_CMSDesk_Properties_Security"
    Theme="Default" Title="Content properties - security tab" %>

<%@ Register Src="~/CMSModules/Membership/FormControls/Roles/securityAddRoles.ascx"
    TagName="AddRoles" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Membership/FormControls/Users/securityAddUsers.ascx"
    TagName="AddUsers" TagPrefix="cms" %>
<%@ Register TagPrefix="cms" Namespace="CMS.UIControls" Assembly="CMS.UIControls" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" enableviewstate="false">
    <title>Properties - Security</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>
</head>
<body class="VerticalTabsBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptMan" runat="server" />
    <asp:Panel ID="pnlBody" runat="server" CssClass="VerticalTabsPageBody">
        <asp:Panel runat="server" ID="pnlMenu" CssClass="ContentEditMenu" EnableViewState="false">
            <table width="100%">
                <tr>
                    <td>
                        <div style="height: 24px; padding: 5px;">
                        </div>
                    </td>
                    <td class="TextRight">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlPageContent" runat="server" CssClass="PageContent">
            <asp:Label ID="lblPermission" runat="server" CssClass="InfoLabel" EnableViewState="false"
                Visible="false" />
            <cms:UIPlaceHolder ID="pnlUIPermissionsPart" runat="server" ModuleName="CMS.Content"
                ElementName="Security.Permissions">
                <asp:Panel ID="pnlPermissionsPart" runat="server" CssClass="NodePermissions">
                    <asp:Panel ID="pnlPermissionsPartBox" CssClass="NodePermissionBox" runat="server">
                        <asp:Label ID="lblLicenseInfo" runat="server" Visible="False" EnableViewState="false" />
                        <asp:Panel ID="pnlPermissions" runat="server">
                            <asp:Label ID="lblInheritanceInfo" CssClass="InfoLabel" runat="server" EnableViewState="false" />
                            <asp:LinkButton ID="lnkInheritance" runat="server" OnClick="lnkInheritance_Click"
                                EnableViewState="false" />
                            <br />
                            <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td style="vertical-align: bottom;">
                                                <asp:Label ID="lblUsersRoles" CssClass="FormGroupHeader" runat="server" EnableViewState="false" />
                                            </td>
                                            <td style="vertical-align: bottom;">
                                                <asp:Label ID="lblInfo" runat="server" EnableViewState="false" /><br />
                                                <asp:Label ID="lblAccessRights" CssClass="FormGroupHeader" runat="server" EnableViewState="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="vertical-align: top;">
                                                <asp:Panel ID="pnlFilter" runat="server" DefaultButton="btnFilter">
                                                    <asp:TextBox ID="txtFilter" runat="server" CssClass="SelectorTextBox" Width="180" />&nbsp;<cms:LocalizedButton
                                                        ID="btnFilter" runat="server" CssClass="ContentButton" ResourceString="general.search" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr style="vertical-align: top;">
                                            <td style="width: 300px;">
                                                <asp:ListBox ID="lstOperators" runat="server" AutoPostBack="True" CssClass="PermissionsListBox"
                                                    OnSelectedIndexChanged="lstOperators_SelectedIndexChanged" />
                                                <br />
                                                <br />
                                                <table style="border: collapse;" cellpadding="0" cellspacing="0" width="280">
                                                    <tr>
                                                        <td>
                                                            <cms:AddUsers ID="addUsers" runat="server" />
                                                        </td>
                                                        <td>
                                                            <cms:AddRoles ID="addRoles" runat="server" />
                                                        </td>
                                                        <td>
                                                            <cms:LocalizedButton ID="btnRemoveOperator" runat="server" CssClass="ContentButton"
                                                                ResourceString="general.remove" OnClick="btnRemoveOperator_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="text-align: right; vertical-align: middle;">
                                                <asp:Panel ID="pnlAccessRights" runat="server">
                                                    <table class="UniGridGrid" cellpadding="3" cellspacing="0" border="1" style="border-collapse: collapse;"
                                                        rules="rows">
                                                        <tr class="UniGridHead">
                                                            <th style="width: 100px;">
                                                                &nbsp;
                                                            </th>
                                                            <th style="width: 50px;">
                                                                <asp:Label ID="lblAllow" runat="server" EnableViewState="false" />
                                                            </th>
                                                            <th style="width: 50px;">
                                                                <asp:Label ID="lblDeny" runat="server" EnableViewState="false" />
                                                            </th>
                                                        </tr>
                                                        <tr class="EvenRow">
                                                            <td class="TableRowHeader">
                                                                <asp:Label ID="lblFullControl" runat="server" EnableViewState="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkFullControlAllow" runat="server" AutoPostBack="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkFullControlDeny" runat="server" AutoPostBack="false" />
                                                            </td>
                                                        </tr>
                                                        <tr class="OddRow">
                                                            <td class="TableRowHeader">
                                                                <asp:Label ID="lblRead" runat="server" EnableViewState="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkReadAllow" runat="server" AutoPostBack="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkReadDeny" runat="server" AutoPostBack="false" />
                                                            </td>
                                                        </tr>
                                                        <tr class="EvenRow">
                                                            <td class="TableRowHeader">
                                                                <asp:Label ID="lblModify" runat="server" EnableViewState="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkModifyAllow" runat="server" AutoPostBack="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkModifyDeny" runat="server" AutoPostBack="false" />
                                                            </td>
                                                        </tr>
                                                        <tr class="OddRow">
                                                            <td class="TableRowHeader">
                                                                <asp:Label ID="lblCreate" runat="server" EnableViewState="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkCreateAllow" runat="server" AutoPostBack="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkCreateDeny" runat="server" AutoPostBack="false" />
                                                            </td>
                                                        </tr>
                                                        <tr class="EvenRow">
                                                            <td class="TableRowHeader">
                                                                <asp:Label ID="lblDelete" runat="server" EnableViewState="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkDeleteAllow" runat="server" AutoPostBack="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkDeleteDeny" runat="server" AutoPostBack="false" />
                                                            </td>
                                                        </tr>
                                                        <tr class="OddRow">
                                                            <td class="TableRowHeader">
                                                                <asp:Label ID="lblDestroy" runat="server" EnableViewState="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkDestroyAllow" runat="server" AutoPostBack="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkDestroyDeny" runat="server" AutoPostBack="false" />
                                                            </td>
                                                        </tr>
                                                        <tr class="EvenRow">
                                                            <td class="TableRowHeader">
                                                                <asp:Label ID="lblExploreTree" runat="server" EnableViewState="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkExploreTreeAllow" runat="server" AutoPostBack="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkExploreTreeDeny" runat="server" AutoPostBack="false" />
                                                            </td>
                                                        </tr>
                                                        <tr class="OddRow">
                                                            <td class="TableRowHeader">
                                                                <asp:Label ID="lblManagePermissions" runat="server" EnableViewState="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkManagePermissionsAllow" runat="server" AutoPostBack="false" />
                                                            </td>
                                                            <td class="TableCell">
                                                                <asp:CheckBox ID="chkManagePermissionsDeny" runat="server" AutoPostBack="false" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOK_Click"
                                                        EnableViewState="false" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </cms:CMSUpdatePanel>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <br />
            </cms:UIPlaceHolder>
            <asp:Panel ID="pnlAccessPart" runat="server" CssClass="NodePermissions" GroupingText="Access">
                <asp:Panel ID="pnlAccessBox" CssClass="NodePermissionBox" runat="server">
                    <table width="100%">
                        <tr>
                            <cms:UIPlaceHolder ID="pnlUIAuth" runat="server" ModuleName="CMS.Content" ElementName="Security.Authentication">
                                <td>
                                    <asp:Label ID="lblAccessInfo" runat="server" CssClass="InfoLabel" Visible="False"
                                        EnableViewState="false" />
                                    <asp:Label ID="lblReqAuthent" runat="server" EnableViewState="false" />
                                    <br />
                                </td>
                            </cms:UIPlaceHolder>
                            <cms:UIPlaceHolder ID="pnlUISsl" runat="server" ModuleName="CMS.Content" ElementName="Security.SSL">
                                <td>
                                    <asp:Label ID="lblAccesInfo2" runat="server" CssClass="InfoLabel" Text="&nbsp;" Visible="False"
                                        EnableViewState="false" />
                                    <asp:Label ID="lblReqSSL" runat="server" EnableViewState="false" />
                                    <br />
                                </td>
                            </cms:UIPlaceHolder>
                        </tr>
                        <tr>
                            <cms:UIPlaceHolder ID="pnlUIAuthBtns" runat="server" ModuleName="CMS.Content" ElementName="Security.Authentication">
                                <td style="vertical-align: top;">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:RadioButton GroupName="reqAuth" ID="radYes" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButton GroupName="reqAuth" ID="radNo" runat="server" />
                                            </td>
                                        </tr>
                                        <asp:PlaceHolder ID="plcAuthParent" runat="server">
                                            <tr>
                                                <td>
                                                    <asp:RadioButton GroupName="reqAuth" ID="radParent" runat="server" />
                                                </td>
                                            </tr>
                                        </asp:PlaceHolder>
                                    </table>
                                </td>
                            </cms:UIPlaceHolder>
                            <cms:UIPlaceHolder ID="pnlUISslBtns" runat="server" ModuleName="CMS.Content" ElementName="Security.SSL">
                                <td style="vertical-align: top;">
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:RadioButton GroupName="reqSSL" ID="radYesSSL" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButton GroupName="reqSSL" ID="radNoSSL" runat="server" />
                                            </td>
                                        </tr>
                                        <asp:PlaceHolder ID="plcSSLParent" runat="server">
                                            <tr>
                                                <td>
                                                    <asp:RadioButton GroupName="reqSSL" ID="radParentSSL" runat="server" />
                                                </td>
                                            </tr>
                                        </asp:PlaceHolder>
                                        <tr>
                                            <td>
                                                <asp:RadioButton GroupName="reqSSL" ID="radNeverSSL" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </cms:UIPlaceHolder>
                            <td style="text-align: right; vertical-align: bottom;">
                                <cms:CMSButton ID="btnRadOk" runat="server" CssClass="SubmitButton" OnClick="btnRadOk_Click"
                                    EnableViewState="false" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="pnlInheritance" runat="server" Visible="False">
                <asp:PlaceHolder runat="server" ID="plcRestore">
                    <asp:LinkButton ID="lnkRestoreInheritance" runat="server" OnClick="lnkRestoreInheritance_Click"
                        EnableViewState="false" /><br />
                    <br />
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="plcBreakCopy">
                    <asp:LinkButton ID="lnkBreakWithCopy" runat="server" OnClick="lnkBreakWithCopy_Click"
                        EnableViewState="false" /><br />
                    <br />
                </asp:PlaceHolder>
                <asp:PlaceHolder runat="server" ID="plcBreakClear">
                    <asp:LinkButton ID="lnkBreakWithClear" runat="server" OnClick="lnkBreakWithClear_Click"
                        EnableViewState="false" /><br />
                    <br />
                </asp:PlaceHolder>
                <br />
                <cms:CMSButton ID="btnCancel" runat="server" CssClass="SubmitButton" OnClick="btnCancel_Click"
                    EnableViewState="false" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
</html>
