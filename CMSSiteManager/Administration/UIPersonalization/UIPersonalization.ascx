﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UIPersonalization.ascx.cs"
    Inherits="CMSSiteManager_Administration_UIPersonalization_UIPersonalization" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/HeaderActions.ascx" tagname="HeaderActions" tagprefix="cms" %>
<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Membership/FormControls/Roles/selectrole.ascx" TagName="RoleSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/SelectModule.ascx" TagName="ModuleSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/UIProfiles/UIElementCheckBoxTree.ascx" TagName="UITree" TagPrefix="cms" %>

<cms:CMSUpdatePanel runat="server" ID="pnlUpdate" UpdateMode="Always">
    <ContentTemplate>
        <cms:LocalizedLabel runat="server" ID="lblNoSiteInfo" EnableViewState="false" Visible="false"
            ResourceString="uiprofile.nosite" CssClass="InfoLabel PageContent" />
        <asp:PlaceHolder runat="server" ID="plcContent">
            <asp:Panel ID="pnlActions" runat="server" CssClass="PageHeaderLine" Style="height: auto;">
                <table>
                    <asp:PlaceHolder runat="server" ID="plcSite">
                        <tr>
                            <td>
                                <cms:LocalizedLabel runat="server" ID="lblSite" EnableViewState="false" ResourceString="general.site"
                                    DisplayColon="true" />
                            </td>
                            <td>
                                <cms:SiteSelector runat="server" ID="selectSite" AllowAll="false" AllowEmpty="false"
                                    OnlyRunningSites="false" />
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                    <tr>
                        <td>
                            <cms:LocalizedLabel runat="server" ID="lblRole" EnableViewState="false" ResourceString="objecttype.cms_role"
                                DisplayColon="true" />
                        </td>
                        <td>
                            <cms:RoleSelector runat="server" ID="selectRole" ShowSiteFilter="false" UseCodeNameForSelection="false"
                                AllowEmpty="false" />
                        </td>
                    </tr>
                    <asp:PlaceHolder runat="server" ID="plcModule">
                        <tr>
                            <td>
                                <cms:LocalizedLabel runat="server" ID="lblModule" EnableViewState="false" ResourceString="objecttype.cms_resource"
                                    DisplayColon="true" />
                            </td>
                            <td>
                                <cms:ModuleSelector runat="server" ID="selectModule" />
                            </td>
                        </tr>
                    </asp:PlaceHolder>
                </table>
            </asp:Panel>
            <cms:LocalizedLabel runat="server" ID="lblNoRoleInfo" EnableViewState="false" Visible="false"
                ResourceString="uiprofile.norole" CssClass="InfoLabel PageContent" />
            <asp:Panel ID="pnlAdditionalControls" runat="server" CssClass="PageHeaderLine" Style="height: auto;"
                EnableViewState="false">
                <cms:HeaderActions ID="actionsElem" runat="server" />
            </asp:Panel>
            <asp:Panel ID="pnlTree" runat="server" CssClass="UIPersonalizationTreeMedium">
                <cms:LocalizedLabel ID="lblDisabled" runat="server" EnableViewState="false" CssClass="PageHeaderLine"
                    ResourceString="uiprofile.disabled" />
                <cms:LocalizedLabel ID="lblInfo" runat="server" EnableViewState="false" CssClass="PageHeaderLine" Visible="false" />
                <div class="ContentTree">
                    <div class="UIPersonalizationTreeArea">
                        <cms:UITree runat="server" ID="treeElem" />
                    </div>
                </div>
            </asp:Panel>
        </asp:PlaceHolder>
    </ContentTemplate>
</cms:CMSUpdatePanel>
