using System;
using System.Collections;

using CMS.GlobalHelper;
using CMS.CMSImportExport;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSModules_ImportExport_Controls_ImportConfiguration : CMSUserControl
{
    private SiteImportSettings mSettings = null;

    #region "Properties"

    /// <summary>
    /// Import settings
    /// </summary>
    public SiteImportSettings Settings
    {
        get
        {
            return mSettings;
        }
        set
        {
            mSettings = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!StopProcessing)
        {
            imgRefresh.ImageUrl = GetImageUrl("CMSModules/CMS_EmailQueue/refresh.png");

            if (!Page.IsCallback)
            {
                lblImports.Text = ResHelper.GetString("ImportConfiguration.Imports");

                radAll.Text = ResHelper.GetString("ImportConfiguration.All");
                radNew.Text = ResHelper.GetString("ImportConfiguration.Date");

                // Load imports list
                LoadImports();
            }
        }
    }


    /// <summary>
    /// Refresh button click handler.
    /// </summary>
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        // Reload the list
        lstImports.Items.Clear();
        LoadImports();

        // Update panel
        pnlUpdate.Update();

        // Select first item
        if (lstImports.Items.Count > 0)
        {
            lstImports.SelectedIndex = 0;
        }
    }


    // Load imports lsit
    private void LoadImports()
    {
        if (lstImports.Items.Count == 0)
        {
            // Get import packages
            ArrayList files = ImportProvider.GetImportFilesList();
            if (files.Count != 0)
            {
                lstImports.Enabled = true;
                lstImports.DataSource = files;
                lstImports.DataBind();
            }
            else
            {
                lstImports.Enabled = false;
            }
        }

        if (!RequestHelper.IsPostBack())
        {
            try
            {
                lstImports.SelectedIndex = 0;
            }
            catch
            {
            }
        }
    }


    /// <summary>
    /// Initialize control
    /// </summary>
    public void InitControl()
    {
        // Could be used in the future:)
    }


    /// <summary>
    /// Apply control settings
    /// </summary>
    public bool ApplySettings()
    {
        string result = null;

        // File is not selected, inform the user
        if (lstImports.SelectedItem == null)
        {
            result = ResHelper.GetString("ImportConfiguration.FileError");
        }

        if (string.IsNullOrEmpty(result))
        {
            try
            {
                // Set current user information
                Settings.CurrentUser = CMSContext.CurrentUser;
                // Set source filename
                Settings.SourceFilePath = Server.MapPath(ImportProvider.IMPORT_RELATIVE_PATH + "/" + lstImports.SelectedValue);

                // Unpack the files
                Settings.TemporaryFilesCreated = false;

                // Init default values
                Settings.ImportType = radAll.Checked ? ImportTypeEnum.All : ImportTypeEnum.New;
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                return false;
            }
        }
        else
        {
            lblError.Text = result;
            return false;
        }

        return true;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        lblError.Visible = (!string.IsNullOrEmpty(lblError.Text));
    }
}
