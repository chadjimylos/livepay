<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Membership/Users/UserPublicProfile.ascx.cs"
    Inherits="CMSWebParts_Membership_Users_UserPublicProfile" %>

<asp:Label ID="lblError" CssClass="ErrorLabel" runat="server" Visible="false" EnableViewState="false" />
<asp:PlaceHolder ID="plcContent" runat="server">
    <cms:DataForm ID="formElem" runat="server" IsLiveSite="true" />
    <asp:Label ID="lblNoProfile" runat="Server" CssClass="NoProfile" Visible="false"
        EnableViewState="false" />
</asp:PlaceHolder>
