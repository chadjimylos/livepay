CREATE TABLE [CMS_Site] (
		[SiteID]                               int NOT NULL IDENTITY(1, 1),
		[SiteName]                             nvarchar(100) NOT NULL,
		[SiteDisplayName]                      nvarchar(200) NOT NULL,
		[SiteDescription]                      nvarchar(max) NULL,
		[SiteStatus]                           nvarchar(20) NOT NULL,
		[SiteDomainName]                       nvarchar(400) NOT NULL,
		[SiteDefaultStylesheetID]              int NULL,
		[SiteDefaultVisitorCulture]            nvarchar(50) NULL,
		[SiteDefaultEditorStylesheet]          int NULL,
		[SiteStoreAllowAnonymousCustomers]     bit NULL,
		[SiteStoreShippingFreeLimit]           float NULL,
		[SiteInvoiceTemplate]                  nvarchar(max) NULL,
		[SiteDefaultCountryID]                 int NULL,
		[SiteShowTaxRegistrationID]            bit NULL,
		[SiteShowOrganizationID]               bit NULL,
		[SiteSendStoreEmailsFrom]              nvarchar(100) NULL,
		[SiteSendOrderNotificationTo]          nvarchar(100) NULL,
		[SiteUseExtraCompanyAddress]           bit NULL,
		[SiteSendOrderNotification]            bit NULL,
		[SiteSendPaymentNotification]          bit NULL,
		[SiteCheckoutProcess]                  nvarchar(max) NULL,
		[SiteGUID]                             uniqueidentifier NOT NULL,
		[SiteLastModified]                     datetime NOT NULL,
		[SiteRequireOrgTaxRegIDs]              bit NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Site]
	ADD
	CONSTRAINT [PK_CMS_Site]
	PRIMARY KEY
	NONCLUSTERED
	([SiteID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Country]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Site_SiteDefaultCountryID_CMS_Country]') IS NULL
BEGIN
		ALTER TABLE [CMS_Site]
			ADD CONSTRAINT [FK_CMS_Site_SiteDefaultCountryID_CMS_Country]
			FOREIGN KEY ([SiteDefaultCountryID]) REFERENCES [CMS_Country] ([CountryID])
END
IF OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_CssStylesheet]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Site_SiteDefaultEditorStylesheet_CMS_CssStylesheet]') IS NULL
BEGIN
		ALTER TABLE [CMS_Site]
			ADD CONSTRAINT [FK_CMS_Site_SiteDefaultEditorStylesheet_CMS_CssStylesheet]
			FOREIGN KEY ([SiteDefaultEditorStylesheet]) REFERENCES [CMS_CssStylesheet] ([StylesheetID])
END
IF OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_CssStylesheet]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Site_SiteDefaultStylesheetID_CMS_CssStylesheet]') IS NULL
BEGIN
		ALTER TABLE [CMS_Site]
			ADD CONSTRAINT [FK_CMS_Site_SiteDefaultStylesheetID_CMS_CssStylesheet]
			FOREIGN KEY ([SiteDefaultStylesheetID]) REFERENCES [CMS_CssStylesheet] ([StylesheetID])
END
CREATE INDEX [IX_CMS_Site_SiteDefaultCountryID]
	ON [CMS_Site] ([SiteDefaultCountryID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Site_SiteDefaultEditorStylesheet]
	ON [CMS_Site] ([SiteDefaultEditorStylesheet])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Site_SiteDefaultStylesheetID]
	ON [CMS_Site] ([SiteDefaultStylesheetID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_Site_SiteDisplayName]
	ON [CMS_Site] ([SiteDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Site_SiteDomainName_SiteStatus]
	ON [CMS_Site] ([SiteDomainName], [SiteStatus])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Site_SiteName]
	ON [CMS_Site] ([SiteName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
