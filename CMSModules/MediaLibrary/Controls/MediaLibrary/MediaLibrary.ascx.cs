using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.MediaLibrary;
using CMS.UIControls;
using CMS.SiteProvider;
using CMS.ExtendedControls;
using CMS.SettingsProvider;
using CMS.EventLog;

using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;

public partial class CMSModules_MediaLibrary_Controls_MediaLibrary_MediaLibrary : CMSAdminItemsControl
{
    #region "Constants"

    private const string PERCENT_ESC_CHAR = "|";

    private const string UPDT_SCRIPT_LIST = "var origName = $j(\"#hdnFileOrigName\").attr('value'); if((origName!=null) && (origName!='')) { $j(\"#\" + origName).attr('id', '@ID@'); $j(\"#hdnFileOrigName\").attr('value', '@ID@'); }" +
             "$j(\"#@ID@ input[id$='action_select']\").attr('onclick', '').unbind('click').click(function () { @ACTION@ });" +
             "$j(\"#@ID@ input[id$='action_edit']\").attr('onclick', '').unbind('click').click(function () { @EDITACTION@ });" +
             "$j(\"#@ID@ div[class='DialogListItem']\").attr('onclick', '').unbind('click').click(function () { @ACTION@ });" +
             "var img = $j(\"#@ID@ div[class='DialogListItem'] img\"); var imgHdn = $j(\"#@ID@ div[class='DialogListItem'] input[type='hidden']\");" +
             " if(imgHdn.length > 0) { var imgHdnSrc = imgHdn.attr('value'); imgHdnSrc = '@THUMBURL@'; imgHdn.attr('value', imgHdnSrc); }" +
             " else { img.attr('id', @IMGID@); img.after('<input type=\"hidden\" id=\"' + @IMGID@ + '_src\" value=\"@THUMBURL@\" />'); } " +
             "if(typeof hover == 'function') { hover(@IMGID@, @THUMBWIDTH@, @THUMBHEIGHT@, true); }" +
              "$j(\"#@ID@ span[class='DialogListItemName'] a\").text(@NEWNAME@); " +
             "$j(\"#@ID@ td:has(div[class='DialogListItemNameRow']) img\").attr('src', '@NEWEXTURL@'); " +
             "$j(\"#@ID@ td:has(div[class='DialogListItem']) + td\").text('@NEWEXT@'); $j(\"#@ID@ td:has(div[class='DialogListItem']) + td + td\").text('@NEWSIZE@');" +
             "$j(\"#@ID@ td:has(div[class='DialogListItem']) + td + td + td\").text('@NEWMODIFIEDWHEN@');" +
             "var chkSelect = $j(\"#@ID@ input[type='checkbox']\"); if((chkSelect!=null) && (chkSelect.length>0)) { var origSelect = chkSelect.attr('onclick'); " +
             "var origSelPart = MatchSelectPart(origSelect); if((origSelPart!=null) && (origSelPart!='')) { chkSelect.attr('onclick', origSelPart + \"'@NEWFILENAME@');\").unbind('click').click(function () { eval(origSelPart + \"'@NEWFILENAME@');\"); }); }}";

    private const string UPDT_SCRIPT_TILES = "var origName = $j(\"#hdnFileOrigName\").attr('value'); if((origName!=null) && (origName!='')) { $j(\"#\" + origName).attr('id', '@ID@'); $j(\"#hdnFileOrigName\").attr('value', '@ID@'); }" +
             "$j(\"#@ID@ div[class='DialogTileItemActions'] input[id$='btnSelect']\").attr('onclick', '').unbind('click').click(function () { @ACTION@ }); " +
             "$j(\"#@ID@ div[class='DialogTileItemActions'] input[id$='btnContentEdit']\").attr('onclick', '').unbind('click').click(function () { @EDITACTION@ });" +
             "$j(\"#@ID@ div[class='DialogTileItemBox']\").attr('onclick', '').unbind('click').click(function () { @ACTION@ }); var img = $j(\"#@ID@ '.DialogTileItemImage' img\"); " +
             "var imgSrc = '@URL@?maxsidesize=@MAXSIDESIZE@&chset=@NEWGUID@'; img.attr('src', imgSrc); " +
             "var imgHdn = $j(\"#@ID@ '.DialogTileItemImage' input[type='hidden']\"); if(imgHdn.length > 0) { var imgHdnSrc = '@THUMBURL@';" +
             " imgHdn.attr('value', imgHdnSrc); } else { img.attr('id', @IMGID@); img.after('<input type=\"hidden\" id=\"' + @IMGID@ + '_src\" value=\"@THUMBURL@\" />'); }" +
             " if(typeof hover == 'function') { hover(@IMGID@, @THUMBWIDTH@, @THUMBHEIGHT@, true); }" +
             "$j(\"#@ID@ span[id$='lblFileName']\").text(@NEWNAME@);" +
             "$j(\"#@ID@ div[class='DialogTileItemBox'] div[class='DialogTileItemInfo'] div[class='DialogTileItemInfoGreyText'] span[id$='lblTypeValue']\").text('@NEWEXT@'); " +
             "$j(\"#@ID@ div[class='DialogTileItemBox'] div[class='DialogTileItemInfo'] div[class='DialogTileItemInfoGreyText'] span[id$='lblSizeValue']\").text('@NEWSIZE@');" +
             "$j(\"#@ID@ input[id$='hdnItemName']\").attr('value', '@NEWFILENAME@');";

    private const string UPDT_SCRIPT_THUMBS = "var origName = $j(\"#hdnFileOrigName\").attr('value'); if((origName!=null) && (origName!='')) { $j(\"#\" + origName).attr('id', '@ID@'); $j(\"#hdnFileOrigName\").attr('value', '@ID@'); }" +
                "$j(\"#@ID@ div[class='DialogThumbnailActions'] input[id$='btnSelect']\").attr('onclick', '').unbind('click').click(function () { @ACTION@ }); " +
                "$j(\"#@ID@ div[class='DialogThumbnailActions'] input[id$='btnContentEdit']\").attr('onclick', '').unbind('click').click(function () { @EDITACTION@ });" +
                "$j(\"#@ID@ div[class='DialogThumbnailItemBox']\").attr('onclick', '').unbind('click').click(function () { @ACTION@ }); var img = $j(\"#@ID@ '.DialogThumbnailItemImage' img\"); " +
                "$j(\"#@ID@ span[id$='lblFileName']\").text(@NEWNAME@);" +
                "var imgSrc = '@PREVURL@?width=@WIDTH@&height=@HEIGHT@&chset=@NEWGUID@'; img.attr('src', imgSrc); $j(\"div[id='@ID@'] + span + input[id$='hdnItemName']\").attr('value', '@NEWFILENAME@');";

    #endregion


    #region "Variables"

    private bool wasLoaded = false;
    private bool copyMoveInProgress = false;
    private bool mRenderUpdateControl = false;
    private string mUpdateScript = null;
    private MediaFileInfo mMediaFileToRender = null;

    private bool mDisplayFilesCount = false;
    private bool mControlReloaded = false;

    private MediaLibraryInfo mLibraryInfo = null;
    private SiteInfo mLibrarySiteInfo = null;
    private Hashtable mFileList = null;

    private bool mDisplayOnlyImportedFiles = false;
    private string mLibraryRootFolder = null;
    private string mFolderPath = "";
    private string mLibraryPath = "";

    private int mAutoResizeWidth = 0;
    private int mAutoResizeHeight = 0;
    private int mAutoResizeMaxSideSize = 0;

    private string mSortDirection = "ASC";
    private string mSortColumns = "FileName";

    private string mCurrentAction = null;

    // Copy/move variables
    private string mAction = null;
    private string mFiles = null;
    private bool mAllFiles = false;
    private string mCopyMovePath = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// ID of the currently processed library.
    /// </summary>
    public int LibraryID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["MediaLibraryID"], 0);
        }
        set
        {
            mLibrarySiteInfo = null;
            mLibraryRootFolder = null;
            ViewState["MediaLibraryID"] = value;
        }
    }


    /// <summary>
    /// Current media library info.
    /// </summary>
    public MediaLibraryInfo LibraryInfo
    {
        get
        {
            // If media library already obtained and the current library ID in the ViewState is valid in the context
            if (mLibraryInfo != null && (mLibraryInfo.LibraryID == LibraryID))
            {
                return mLibraryInfo;
            }

            mLibraryInfo = MediaLibraryInfoProvider.GetMediaLibraryInfo(LibraryID);
            return mLibraryInfo;
        }
        set
        {
            mLibraryInfo = value;
        }
    }


    /// <summary>
    /// Info on site related to the current media library.
    /// </summary>
    public SiteInfo LibrarySiteInfo
    {
        get
        {
            if ((LibraryInfo != null) && (mLibrarySiteInfo == null))
            {
                mLibrarySiteInfo = SiteInfoProvider.GetSiteInfo(LibraryInfo.LibrarySiteID);
            }
            return mLibrarySiteInfo;
        }
    }


    /// <summary>
    /// Returns media library root folder path.
    /// </summary>
    public string LibraryRootFolder
    {
        get
        {
            if ((LibrarySiteInfo != null) && (mLibraryRootFolder == null))
            {
                mLibraryRootFolder = MediaLibraryHelper.GetMediaRootFolderPath(LibrarySiteInfo.SiteName);
            }
            return mLibraryRootFolder;
        }
    }


    /// <summary>
    /// Indicates if file count should be displayed in folder tree.
    /// </summary>
    public bool DisplayFilesCount
    {
        get
        {
            return mDisplayFilesCount;
        }
        set
        {
            mDisplayFilesCount = value;
        }
    }


    /// <summary>
    /// Gets or sets the value which determines whether only imported media files are displayed.
    /// </summary>
    public bool DisplayOnlyImportedFiles
    {
        get
        {
            return mDisplayOnlyImportedFiles;
        }
        set
        {
            mDisplayOnlyImportedFiles = value;
        }
    }


    /// <summary>
    /// Gets library relative url path.
    /// </summary>
    public string LibraryPath
    {
        get
        {
            if (String.IsNullOrEmpty(mLibraryPath))
            {
                if (LibraryInfo != null)
                {
                    mLibraryPath = LibraryRootFolder + LibraryInfo.LibraryFolder;
                }
            }
            return mLibraryPath;
        }
    }


    /// <summary>
    /// Indicates whether the UI should process all of the actions required.
    /// </summary>
    public bool ShouldProcess
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["ShouldProcess"], false);
        }
        set
        {
            ViewState["ShouldProcess"] = value;
        }
    }


    /// <summary>
    /// Action control is displayed for.
    /// </summary>
    public string Action
    {
        get
        {
            return mAction;
        }
        set
        {
            mAction = value;
        }
    }


    /// <summary>
    /// Gets or sets a folder path of the media library.
    /// </summary>
    public string FolderPath
    {
        get
        {
            return mFolderPath.Replace("|", "/");
        }
        set
        {
            mFolderPath = ((value == null) ? "" : value);
            folderActions.FolderPath = mFolderPath;
            LastFolderPath = mFolderPath;
        }
    }


    /// <summary>
    /// Gets or sets a folder path of the media library.
    /// </summary>
    public string CopyMovePath
    {
        get
        {
            return mCopyMovePath;
        }
        set
        {
            mCopyMovePath = value;
        }
    }


    /// <summary>
    /// Set of file names action is related to.
    /// </summary>
    public string Files
    {
        get
        {
            return mFiles;
        }
        set
        {
            mFiles = value;
        }
    }


    /// <summary>
    /// Indicates whether all available files should be processed.
    /// </summary>
    public bool AllFiles
    {
        get
        {
            return mAllFiles;
        }
        set
        {
            mAllFiles = value;
        }
    }

    #endregion


    #region "Private properties"

    /// <summary>
    /// Current action name.
    /// </summary>
    private string CurrentAction
    {
        get
        {
            if (mCurrentAction == null)
            {
                mCurrentAction = hdnAction.Value.Trim().ToLower();
            }
            return mCurrentAction;
        }
    }


    /// <summary>
    /// Current action name.
    /// </summary>
    private string CurrentArgument
    {
        get
        {
            return this.hdnArgument.Value;
        }
        set
        {
            this.hdnArgument.Value = value;
        }
    }


    /// <summary>
    /// Update script.
    /// </summary>
    private string UpdateScript
    {
        get
        {
            return mUpdateScript;
        }
        set
        {
            mUpdateScript = value;
        }
    }


    /// <summary>
    /// Indicates whether the DirectFileUploader control is required to be rendered.
    /// </summary>
    private bool RenderUpdateControl
    {
        get
        {
            return mRenderUpdateControl;
        }
        set
        {
            mRenderUpdateControl = value;
        }
    }


    /// <summary>
    /// Media file info that is related to required DirectFileUploader rendering.
    /// </summary>
    private MediaFileInfo MediaFileToRender
    {
        get
        {
            return mMediaFileToRender;
        }
        set
        {
            mMediaFileToRender = value;
        }
    }


    /// <summary>
    /// Indicates whether the control was already reloaded.
    /// </summary>
    private bool ControlReloaded
    {
        get
        {
            return mControlReloaded;
        }
        set
        {
            mControlReloaded = value;
        }
    }


    /// <summary>
    /// Indicates if full listing mode is enabled. This mode enables navigation to child and parent folders/documents from current view.
    /// </summary>
    private bool IsFullListingMode
    {
        get
        {
            return mediaView.IsFullListingMode;
        }
        set
        {
            mediaView.IsFullListingMode = value;
        }
    }


    /// <summary>
    /// Gets or sets selected item to colorize.
    /// </summary>
    private string ItemToColorize
    {
        get
        {
            return ValidationHelper.GetString(ViewState["ItemToColorize"], "");
        }
        set
        {
            ViewState["ItemToColorize"] = value;
        }
    }


    /// <summary>
    /// GUID of the recently edited file.
    /// </summary>
    private Guid LastEditFileGuid
    {
        get
        {
            return ValidationHelper.GetGuid(ViewState["LastEditFileGuid"], Guid.Empty);
        }
        set
        {
            ViewState["LastEditFileGuid"] = value;
        }
    }



    /// <summary>
    /// Indicates whether the image was recently edited.
    /// </summary>
    private bool IsEditImage
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["IsEditImage"], false);
        }
        set
        {
            ViewState["IsEditImage"] = value;
        }
    }


    /// <summary>
    /// Gets or sets last searched value.
    /// </summary>
    private string LastSearchedValue
    {
        get
        {
            return hdnLastSearchedValue.Value;
        }
        set
        {
            hdnLastSearchedValue.Value = value;
            pnlUpdateViewHidden.Update();
        }
    }


    /// <summary>
    /// Gets or sets last selected folder path.
    /// </summary>
    private string LastFolderPath
    {
        get
        {
            return hdnLastSelectedPath.Value.Replace("|", "/");
        }
        set
        {
            hdnLastSelectedPath.Value = value;
            pnlUpdateViewHidden.Update();
        }
    }


    /// <summary>
    /// Gets width files are resized to on upload.
    /// </summary>
    private int AutoResizeWidth
    {
        get
        {
            if ((LibrarySiteInfo != null) && (mAutoResizeWidth == 0))
            {
                mAutoResizeWidth = SettingsKeyProvider.GetIntValue(LibrarySiteInfo.SiteName + ".CMSAutoResizeImageWidth");
            }
            return mAutoResizeWidth;
        }
    }


    /// <summary>
    /// Gets height files are resized to on upload.
    /// </summary>
    private int AutoResizeHeight
    {
        get
        {
            if ((LibrarySiteInfo != null) && (mAutoResizeHeight == 0))
            {
                mAutoResizeHeight = SettingsKeyProvider.GetIntValue(LibrarySiteInfo.SiteName + ".CMSAutoResizeImageHeight");
            }
            return mAutoResizeHeight;
        }
    }


    /// <summary>
    /// Gets max side size files are resized to on upload.
    /// </summary>
    private int AutoResizeMaxSideSize
    {
        get
        {
            if ((LibrarySiteInfo != null) && (mAutoResizeMaxSideSize == 0))
            {
                mAutoResizeMaxSideSize = SettingsKeyProvider.GetIntValue(LibrarySiteInfo.SiteName + ".CMSAutoResizeImageMaxSideSize");
            }
            return mAutoResizeMaxSideSize;
        }
    }


    /// <summary>
    /// Paths of the files to be imported
    /// </summary>
    private string ImportFilePaths
    {
        get
        {
            return ValidationHelper.GetString(ViewState["MediaFilePaths"], "");
        }
        set
        {
            ViewState["MediaFilePaths"] = value;
        }
    }


    /// <summary>
    /// Overall number of the files to be imported
    /// </summary>
    private int ImportFilesNumber
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["MediaFilesNumber"], 0);
        }
        set
        {
            ViewState["MediaFilesNumber"] = value;
        }
    }


    /// <summary>
    /// Index of the currently processed file
    /// </summary>
    private int ImportCurrFileIndex
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["MediaCurrFileIndex"], 0);
        }
        set
        {
            ViewState["MediaCurrFileIndex"] = value;
        }
    }


    /// <summary>
    /// Gets or sets list of files from the currently selected folder.
    /// </summary>
    private Hashtable FileList
    {
        get
        {
            return mFileList;
        }
        set
        {
            mFileList = value;
        }
    }


    /// <summary>
    /// Gets or sets direction in which data should be ordered.
    /// </summary>
    private string SortDirection
    {
        get
        {
            return mSortDirection;
        }
        set
        {
            mSortDirection = value;
        }
    }


    /// <summary>
    /// Gets or sets sort columns data are ordered by.
    /// </summary>
    public string SortColumns
    {
        get
        {
            return mSortColumns;
        }
        set
        {
            mSortColumns = value;
        }
    }


    /// <summary>
    /// Indicates whether the control is displayed as part of the copy/move dialog.
    /// </summary>
    private bool IsCopyMoveLinkDialog
    {
        get
        {
            return ((Action == "copy") || (Action == "move") || (Action == "link") || (Action == "linkdoc"));
        }
    }

    #endregion


    #region "Page events"

    protected override void OnInit(EventArgs e)
    {
        // Make sure view control is set as soon as possible
        mediaView.SourceType = MediaSourceEnum.MediaLibraries;

        base.OnInit(e);
    }


    protected override void OnPreRender(EventArgs e)
    {
        if (ShouldProcess)
        {
            // High-light item being edited
            if (ItemToColorize != "")
            {
                ColorizeRow(ItemToColorize.ToLower());
            }

            // If full-listing mode is on
            bool rootHasMore = (LastFolderPath == "") && ((CurrentAction == "select") || (!wasLoaded && CurrentAction == "")) && folderTree.RootHasMore;
            if (!IsCopyMoveLinkDialog && (IsFullListingMode || rootHasMore))
            {
                IsFullListingMode = (rootHasMore ? true : IsFullListingMode);

                string closeLink = "<span class=\"ListingClose\" style=\"cursor: pointer;\" onclick=\"SetAction('closelisting', ''); RaiseHiddenPostBack(); return false;\">" + ResHelper.GetString("general.close") + "</span>";
                string docNamePath = "<span class=\"ListingPath\">" + GetFullFilePath(LastFolderPath).Replace('\\', '/') + "</span>";

                string listingMsg = string.Format(ResHelper.GetString("media.libraryui.listingInfo"), docNamePath, closeLink);
                mediaView.DisplayListingInfo(listingMsg);
            }
            mediaView.ShowParentButton = (IsFullListingMode && (LastFolderPath != ""));

            // Simulate library root selection
            if (!wasLoaded && mediaView.Visible)
            {
                string path = GetFullFilePath(LastFolderPath);
                HandleFolderAction(path, false, false, !IsCopyMoveLinkDialog, true);
            }

            // Pre-load edit folder control when loading - applied only when displaying in CMSDesk
            if (Action == null)
            {
                if (!UrlHelper.IsPostback())
                {
                    DisplayFolderProperties();
                }
            }
            else if (!copyMoveInProgress && (CurrentAction != "search") && !(UrlHelper.IsPostback() && (CurrentAction == "")))
            {
                // Otherwise load copy/move properties
                DisplayCopyMoveProperties();
            }

            pnlUpdateSelectors.Update();

            // When import was recent action and DirectFileUploader needs to be rendered
            if (RenderUpdateControl)
            {
                string newIFrameHtml = mediaView.GetDirectFileUploaderHTML(MediaFileToRender);
                newIFrameHtml = newIFrameHtml.Replace("\r\n", "").Replace("\n", "");

                UpdateScript = UpdateScript.Replace("@NEWIFRAMEHTML@", newIFrameHtml);

                // Run update script
                ScriptHelper.RegisterStartupScript(Page, typeof(Page), "UpdateEditScript", ScriptHelper.GetScript(UpdateScript));
            }

            if (IsCopyMoveLinkDialog && !RequestHelper.IsPostBack())
            {
                // Hide media elemnents
                mediaView.StopProcessing = true;
                mediaView.Visible = false;

                // Disable New folder button
                ScriptHelper.RegisterStartupScript(Page, typeof(Page), "DisableNewFolderOnLoad", ScriptHelper.GetScript("if( typeof(DisableNewFolderBtn) == 'function' ) { DisableNewFolderBtn(); }"));
            }
        }

        base.OnPreRender(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!StopProcessing)
        {
            InitializeDesignScripts();

            // Initialize control by default when in CMSDesk or displayed on LiveSite and already reloaded
            if ((!IsLiveSite || ShouldProcess) && !ControlReloaded)
            {
                InitializeControl();
            }
        }
        else
        {
            Visible = false;
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes control.
    /// </summary>
    private void InitializeControl()
    {
        Visible = true;
        ShouldProcess = true;

        RaiseOnCheckPermissions(PERMISSION_READ, this);

        // Initialize inner controls
        SetupControls();

        // Special treatment for specific actions
        HandleSpecialActions();
    }


    /// <summary>
    /// Initializes all the nested controls
    /// </summary>
    private void SetupControls()
    {
        // Full-listing mode when copy/move dialog
        if (IsCopyMoveLinkDialog)
        {
            IsFullListingMode = IsCopyMoveLinkDialog;
        }

        // Sub-menu actions control
        if (LibraryInfo != null)
        {
            folderActions.LibraryID = LibraryID;
            folderActions.IsLiveSite = IsLiveSite;
            folderActions.OnCheckPermissions += folderActions_OnCheckPermissions;

            string delConfirmation = ScriptHelper.GetString(ResHelper.GetString("media.folder.delete.confirmation"));

            folderActions.DeleteScript = "if(confirm(" + string.Format(delConfirmation, "##FOLDERPATH##") + ") == true) { SetAction('delete', ''); RaiseHiddenPostBack(); } return false;";
        }

        // Folder tree control
        folderTree.OnCheckPermissions += folderTree_OnCheckPermissions;
        folderTree.DisplayFilesCount = DisplayFilesCount;

        // Initialize controlling scripts
        InitializeControlScripts();

        // Menu element initialization
        InitializeMenuElem();

        // Initialize view element
        InitializeViewElem();

        if (!UrlHelper.IsPostback() && (LibraryInfo != null))
        {
            // Folder tree initialization
            InitializeTree();
        }
    }


    /// <summary>
    /// Reloads control and its content
    /// </summary>
    public void ReloadControl()
    {
        SetupControls();

        if (mediaView.Visible)
        {
            LoadData(true);
        }
    }


    /// <summary>
    /// Loads all files for the view control.
    /// </summary>
    private void LoadData()
    {
        LoadData(false);
    }


    /// <summary>
    /// Loads all files for the view control.
    /// </summary>
    /// 
    private void LoadData(bool forceSetup)
    {
        mediaView.StopProcessing = false;

        LoadDataSource(LastSearchedValue);
        mediaView.Reload(forceSetup);

        wasLoaded = true;
    }


    /// <summary>
    /// Returns set of files in the file system.
    /// </summary>
    private DataSet GetFileSystemDataSource(string where)
    {
        if (!string.IsNullOrEmpty(where))
        {
            fileSystemDataSource.WhereCondition = where;
        }

        if (string.IsNullOrEmpty(LastFolderPath))
        {
            fileSystemDataSource.Path = LibraryPath + "/";
        }
        else
        {
            fileSystemDataSource.Path = LibraryPath + "/" + MediaLibraryHelper.EnsurePath(LastFolderPath) + "/";
        }
        fileSystemDataSource.Path = fileSystemDataSource.Path.Replace("/", "\\").Replace("|", "\\");

        return (DataSet)fileSystemDataSource.GetDataSource();
    }


    /// <summary>
    /// Loads all files for the view control.
    /// </summary>
    /// <param name="searchText">Text to filter loaded files.</param>
    private void LoadDataSource(string searchText)
    {
        // Load media files data
        if ((LastFolderPath != null) && (LibraryID > 0))
        {
            string err = CheckPermissions();
            if (err == "")
            {
                DataSet result = null;

                if (!IsCopyMoveLinkDialog)
                {
                    // Get only imported files if required
                    if (DisplayOnlyImportedFiles)
                    {
                        string normFolderPath = NormalizeForSql(this.FolderPath);

                        // Create WHERE condition
                        string where = "FilePath LIKE '" + MediaLibraryHelper.EnsurePath(normFolderPath).Trim('/') +
                            "%' {escape '" + PERCENT_ESC_CHAR + "'} AND FilePath NOT LIKE '" + MediaLibraryHelper.EnsurePath(normFolderPath).Trim('/') +
                            "_%/%' {escape '" + PERCENT_ESC_CHAR + "'} AND FileLibraryID = " + LibraryID;
                        
                        if (!string.IsNullOrEmpty(searchText))
                        {
                            searchText = NormalizeForSql(searchText);

                            where += " AND (FileName LIKE N'%" + searchText.Replace("'", "''") + "%' {escape '" + PERCENT_ESC_CHAR + "'})";
                        }

                        string columns = "FileID, FileGUID, FileName, FilePath, FileExtension, FileImageWidth, FileImageHeight, FileTitle, FileSize, FileModifiedWhen, FileSiteID";
                        int topN = mediaView.CurrentTopN;

                        // Get all files from current folder and pass it to the media view control
                        result = MediaFileInfoProvider.GetMediaFiles(where, null, topN, columns);
                    }
                    else
                    {
                        try
                        {
                            string where = "";
                            if (!string.IsNullOrEmpty(searchText))
                            {
                                where += "(FileName LIKE '%" + searchText.Replace("'", "''") + "%')";
                            }
                            result = GetFileSystemDataSource(where);
                        }
                        catch (Exception ex) 
                        {
                            EventLogProvider eventLog = new EventLogProvider();
                            eventLog.LogEvent("Media library", "READOBJ", ex);
                        }
                    }
                }
                else
                {
                    result = CreateEmptyDataSet();
                }

                // Sort DataSet containing folders as well as files
                SortMixedDataSet(result, SortDirection, SortColumns);

                // If folders should be displayed as well include them in the DataSet
                if (IsFullListingMode || IsCopyMoveLinkDialog)
                {
                    // Add folder
                    IncludeFolders(result, searchText);
                }

                mediaView.DataSource = result;
            }
            else
            {
                lblNoLibraries.ResourceString = "";
                lblNoLibraries.CssClass = "ErrorLabel";
                lblNoLibraries.Text = err;

                // Display error
                DisplayError(err);
            }
        }
    }


    /// <summary>
    /// Creates new DataSet containing all the required columns ready to be filled with data.
    /// </summary>
    private DataSet CreateEmptyDataSet()
    {
        DataSet result = new DataSet();
        if (result != null)
        {
            DataTable table = new DataTable("files");

            // Create columns
            DataColumn column = new DataColumn("FileName");
            table.Columns.Add(column);
            column = new DataColumn("FilePath");
            table.Columns.Add(column);
            column = new DataColumn("Directory");
            table.Columns.Add(column);
            column = new DataColumn("Created");
            table.Columns.Add(column);
            column = new DataColumn("Modified");
            table.Columns.Add(column);
            column = new DataColumn("Extension");
            table.Columns.Add(column);
            column = new DataColumn("FileURL");
            table.Columns.Add(column);
            column = new DataColumn("Size");
            table.Columns.Add(column);

            // Add table to data set
            result.Tables.Add(table);
        }

        return result;
    }


    /// <summary>
    /// Ensures sorting of data set containing both files and folders.
    /// </summary>
    /// <param name="ds">DateSet to sort.</param>
    /// <param name="direction">Direction in which data should be sorted.</param>
    /// <param name="expression">Expresion used to sort the data.</param>
    private static void SortMixedDataSet(DataSet ds, string direction, string expression)
    {
        if ((ds != null) && !string.IsNullOrEmpty(direction) && !string.IsNullOrEmpty(expression) && !DataHelper.IsEmpty(ds))
        {
            string orderBy = expression.Trim() + " " + direction;

            DataHelper.SortDataTable(ds.Tables[0], orderBy);
            DataTable sortedResult = ds.Tables[0].DefaultView.ToTable();
            ds.Tables.Remove("files");
            ds.Tables.Add(sortedResult);
        }
    }


    /// <summary>
    /// Imports folder details into given set of data.
    /// </summary>
    /// <param name="ds">DataSet folders information should be imported to.</param>
    private void IncludeFolders(DataSet ds, string searchText)
    {
        // Data object specified
        if ((ds != null) && (ds.Tables["files"] != null))
        {
            // Get path to the currently selected folder
            string dirPath = LibraryRootFolder.TrimEnd('\\') + "\\" + LibraryInfo.LibraryFolder + ((LastFolderPath != "") ? "\\" + LastFolderPath : "");
            dirPath = dirPath.Replace('/', '\\');
            if (Directory.Exists(dirPath))
            {
                // Get directories in the current path
                string[] dirs = Directory.GetDirectories(dirPath);
                if (dirs != null)
                {
                    int lastFolderIndex = 0;

                    string hiddenFolder = MediaLibraryHelper.GetMediaFileHiddenFolder(LibrarySiteInfo.SiteName);
                    foreach (string folderPath in dirs)
                    {
                        if (!folderPath.EndsWith(hiddenFolder))
                        {
                            // Get directory info object to access additional information
                            DirectoryInfo dirInfo = new DirectoryInfo(folderPath);
                            if (dirInfo != null)
                            {
                                if (!DisplayOnlyImportedFiles)
                                {
                                    DataRow dirRow = ds.Tables["files"].NewRow();

                                    bool includeFolder = true;
                                    string fileName = Path.GetFileName(dirInfo.FullName).ToLower();
                                    if (!string.IsNullOrEmpty(searchText) && !fileName.Contains(searchText.ToLower()))
                                    {
                                        includeFolder = false;
                                    }

                                    // Insert new row
                                    if (includeFolder)
                                    {
                                        // Fill new row with data
                                        dirRow["FileName"] = Path.GetFileName(dirInfo.FullName);
                                        dirRow["FilePath"] = dirPath;
                                        dirRow["Directory"] = dirInfo.Name;
                                        dirRow["Created"] = dirInfo.CreationTime;
                                        dirRow["Modified"] = dirInfo.LastWriteTime;
                                        dirRow["Extension"] = "<dir>";

                                        ds.Tables["files"].Rows.InsertAt(dirRow, lastFolderIndex);

                                        lastFolderIndex++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    /// <summary>
    /// Performs actions necessary to select particular item from a list.
    /// </summary>
    private void SelectMediaItem(string argument)
    {
        if (!string.IsNullOrEmpty(argument) && (LibraryInfo != null))
        {
            string[] argArr = argument.Split('|');
            if (argArr.Length >= 2)
            {
                // Get file name
                string fileName = "";
                if (DisplayOnlyImportedFiles)
                {
                    // File name + file extension
                    fileName = argArr[1] + argArr[3];
                }
                else
                {
                    // File name - already contains extension
                    fileName = argArr[0];
                }

                // Get media file path
                string filePath = CreateFilePath(fileName);

                // Try to get media file info based on its path
                FileInfo fi = new FileInfo(MediaFileInfoProvider.GetMediaFilePath(LibrarySiteInfo.SiteName, LibraryInfo.LibraryFolder, filePath));
                if (fi != null)
                {
                    string path = MediaLibraryHelper.EnsurePath(filePath);
                    string where = "(FileLibraryID = " + LibraryID + ") AND (FilePath = N'" + path.TrimStart('/').Replace("'", "''") + "')";

                    // Try to get file from DB
                    DataSet ds = MediaFileInfoProvider.GetMediaFiles(where, null);
                    if (!DataHelper.DataSourceIsEmpty(ds))
                    {
                        // If in DB get file info
                        MediaFileInfo fileInfo = new MediaFileInfo(ds.Tables[0].Rows[0]);
                        if (fileInfo != null)
                        {
                            DisplayEditProperties(fileInfo);
                        }
                    }

                    // FileGUID for imported and FileName for non-imported media file
                    ItemToColorize = EnsureFileName(argArr[0]);
                }
            }
        }

        wasLoaded = true;
    }


    /// <summary>
    /// Loads folder tree.
    /// </summary>
    private void InitializeTree()
    {
        // Initialize folder tree control
        folderTree.CustomClickForMoreFunction = "SetAction('clickformore##TYPE##', '##NODEVALUE##'); SetLastViewAction('list'); RaiseHiddenPostBack();";
        folderTree.CustomSelectFunction = "SetAction('folderselect', '##NODEVALUE##'); RaiseHiddenPostBack();";
        folderTree.RootFolderPath = LibraryRootFolder;
        folderTree.MediaLibraryFolder = LibraryInfo.LibraryFolder;

        folderTree.ReloadData();
        pnlUpdateTree.Update();
    }


    /// <summary>
    /// Ensures folder path comming from JavaScript to keep normalized form.
    /// </summary>
    /// <param name="path">Path to ensure.</param>
    private string EnsureFolderPath(string path)
    {
        char separator = folderTree.PathSeparator;

        return (!string.IsNullOrEmpty(path)) ? path.Replace('/', separator).Replace("\\\\", separator.ToString()).TrimStart(separator) : "";
    }


    /// <summary>
    /// Ensures that given path is selected when available in the tree.
    /// </summary>
    /// <param name="path">Path to select.</param>
    private void SelectTreePath(string path)
    {
        SelectTreePath(path, false);
    }


    /// <summary>
    /// Ensures that given path is selected when available in the tree.
    /// </summary>
    /// <param name="path">Path to select.</param>
    /// <param name="forceReload">Indicates if the reload on folder tree should be called explicitly.</param>
    private void SelectTreePath(string path, bool forceReload)
    {
        folderTree.PathToSelect = path;

        if (forceReload)
        {
            InitializeTree();
        }
    }

    #endregion


    #region "Initialization methods"

    /// <summary>
    /// Initializes all the script required for communication between controls.
    /// </summary>
    private void InitializeControlScripts()
    {
        ScriptHelper.RegisterJQuery(Page);

        // SetAction function setting action name and passed argument
        string setAction = "function SetAction(action, argument) {                                              " +
                           "    var hdnAction = document.getElementById('" + hdnAction.ClientID + "');     " +
                           "    var hdnArgument = document.getElementById('" + hdnArgument.ClientID + "'); " +
                          @"    if ((hdnAction != null) && (hdnArgument != null)) {                             
                                   if (action != null) {                                                       
                                       hdnAction.value = action;                                               
                                   }                                                                           
                                   if (argument != null) {                                                     
                                       hdnArgument.value = argument;                                           
                                   }                                                                           
                               }                                                                               
                            }                                                                                   
                                                                                                               
                            function SelectRootFolder() {                                                       
                                var rootFolder = document.getElementById('0');                                  
                                if(rootFolder != null) {                                                        
                                    if(typeof(SelectFolder) == 'function') {                                    
                                        SelectFolder(0, rootFolder);                                            
                                    }                                                                           
                                }                                                                               
                            }                                                                                   
                            
                            function MatchSelectPart(selectText) {
                                if (selectText != null) {
                                    var regExp = new RegExp(/SelectionClick.*\(this,/i);
                                    var m = regExp.exec(selectText);
                                    if (m != null) {
                                        return m[0];
                                    } 
                                }        
                                return null;
                            }

                            function GetOriginalFullName(selectText) {
                                if (selectText != null) {                                           " +
                           "        var regExp = new RegExp(/\".*\"/i);                             " +
                          @"        var m = regExp.exec(selectText);
                                    if (m != null) {                                                " +
                           "           var result = m[0].replace('\"', '').replace('\"', '');       " +
                          @"           return result;
                                   } 
                               }        
                               return null;
                           }
                           ";

        // Get reffernce causing postback to hidden button
        string postBackRef = ControlsHelper.GetPostBackEventReference(hdnButton, "");

        // RaiseOnAction function causing postback to the hidden button
        string raiseOnAction = "function RaiseHiddenPostBack(){" + postBackRef + ";}";

        // Prepare for upload
        string refreshType = CMSDialogHelper.GetMediaSource(MediaSourceEnum.MediaLibraries);

        string initRefresh = @" function InitRefresh_" + refreshType + "(message, fullRefresh, itemInfo, action) {      " +
                             @"  if((message != null) && (message != ''))                                   
                                     {                                                                          
                                        window.alert(message);                                                  
                                     }                                                                          
                                     else                                                                       
                                     {                                                                          
                                        SetAction('libraryfilecreated', itemInfo);                                 
                                        RaiseHiddenPostBack();                                                  
                                     }                                                                          
                                   }    
                                                                        
                                function InitRefresh_LibraryUpdate(message, fullRefresh, itemInfo, action) {      " +
                             @"  if((message != null) && (message != ''))                                   
                                     {                                                                          
                                        window.alert(message);                                                  
                                     }                                                                          
                                     else                                                                       
                                     {                                                                          
                                        SetAction('libraryfileupdated', itemInfo);                                 
                                        RaiseHiddenPostBack();                                                  
                                     }                                                                          
                                   }                                                                                    ";

        setAction += initRefresh;

        string imgEditRefresh = @" function imageEdit_Refresh(guid){
                                    SetAction('edit', guid);                                                    
                                    RaiseHiddenPostBack();
                                }                                                                                       ";

        setAction += imgEditRefresh;

        ltlScript.Text = ScriptHelper.GetScript(setAction + raiseOnAction);
    }


    /// <summary>
    /// Initializes view element.
    /// </summary>
    private void InitializeViewElem()
    {
        mediaView.LibraryID = LibraryID;
        mediaView.IsLiveSite = IsLiveSite;
        mediaView.IsCopyMoveLinkDialog = IsCopyMoveLinkDialog;

        // Generate permanent URLs whenever node GUID output required        
        mediaView.UsePermanentUrls = false;
        mediaView.ListReloadRequired += mediaView_ListReloadRequired;
        mediaView.GetInformation += mediaView_GetInformation;
        mediaView.ListViewControl.OnBeforeSorting += ListViewControl_OnBeforeSorting;
        mediaView.ListViewControl.GridView.RowDataBound += GridView_RowDataBound;
        mediaView.ListViewControl.DataSourceIsSorted = true;

        // Set media properties
        mediaView.SelectableContent = SelectableContentEnum.AllFiles;
        mediaView.ViewMode = menuElem.SelectedViewMode;
        mediaView.SourceType = MediaSourceEnum.MediaLibraries;
        pnlInfo.Visible = false;

        // Set autoresize parameters
        mediaView.ResizeToHeight = AutoResizeHeight;
        mediaView.ResizeToMaxSideSize = AutoResizeMaxSideSize;
        mediaView.ResizeToWidth = AutoResizeWidth;

        // Single file import initialization
        fileImport.SaveRequired += fileImport_SaveRequired;
        fileImport.Action += fileImport_Action;

        fileEdit.Action += fileEdit_Action;

        // Multiple file import initialization
        multipleImport.Action += multipleImport_Action;
        multipleImport.SaveRequired += multipleImport_SaveRequired;
        multipleImport.GetItemUrl += multipleImport_GetItemUrl;

        // If folder was changed reset current page index for control displaying content
        switch (this.CurrentAction)
        {
            case "folderselect":
            case "clickformorefolder":
            case "morefolderselect":
            case "parentselect":
            case "clickformorelink":
                ResetPageIndex();
                break;

            default:
                break;
        }
    }


    /// <summary>
    /// Initialize design jQuery scripts
    /// </summary>
    private void InitializeDesignScripts()
    {
        ScriptHelper.RegisterJQuery(Page);
        CMSDialogHelper.RegisterDialogHelper(Page);
        StringBuilder sb = new StringBuilder();
        sb.Append("setTimeout('InitializeDesign();',200);");
        sb.Append("$j(window).unbind('resize').bind('resize',function() { InitializeDesign(); });");
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "designScript", ScriptHelper.GetScript(sb.ToString()));
    }


    /// <summary>
    /// Initializes menu element.
    /// </summary>
    private void InitializeMenuElem()
    {
        menuElem.LibraryFolderPath = LastFolderPath.Trim('/');

        menuElem.ResizeToHeight = AutoResizeHeight;
        menuElem.ResizeToMaxSideSize = AutoResizeMaxSideSize;
        menuElem.ResizeToWidth = AutoResizeWidth;

        menuElem.DisplayMode = DisplayMode;
        menuElem.IsCopyMoveLinkDialog = IsCopyMoveLinkDialog;
        menuElem.IsLiveSite = IsLiveSite;
        menuElem.LibraryID = LibraryID;
        menuElem.SourceType = MediaSourceEnum.MediaLibraries;

        // Hide Fullscreen button for Live site
        menuElem.AllowFullscreen = !IsLiveSite;
        menuElem.UpdateViewMenu();

        plcFolderActions.Visible = !IsCopyMoveLinkDialog;
    }


    /// <summary>
    /// Initializes control used to perform copy/move action.
    /// </summary>
    private void InitialzeCopyMoveProperties()
    {
        folderCopyMove.Action = Action;
        folderCopyMove.AllFiles = AllFiles;
        folderCopyMove.Files = Files;
        folderCopyMove.FolderPath = CopyMovePath;
        folderCopyMove.IsLiveSite = IsLiveSite;
        folderCopyMove.MediaLibraryID = LibraryID;

        // this.folderCopyMove.NewPath is initialized on 'copymoveselect' action in hdnBtn_Click       

        folderCopyMove.ReloadData();
    }

    #endregion


    #region "Action handler methods"

    /// <summary>
    /// Handles special actions that require initialization as soon as possible.
    /// </summary>
    private void HandleSpecialActions()
    {
        if (this.CurrentAction == "massaction")
        {
            // Remember mass action so other controls are aware of
            FolderPath = LastFolderPath;

            if (menuElem.SelectedViewMode == DialogViewModeEnum.ListView)
            {
                LoadData(false);
            }
        }
        else if (CurrentAction == "select")
        {
            HandleSelectAction(this.CurrentArgument);
        }
    }


    /// <summary>
    /// Handles actions occuring when some text is searched.
    /// </summary>
    /// <param name="argument">Argument holding information on searched text.</param>
    private void HandleSearchAction(string argument)
    {
        LastSearchedValue = argument;
        FolderPath = LastFolderPath;

        // Load new data filtered by searched text and reload view control's content
        LoadData();

        pnlUpdateView.Update();

        // Keep focus in search text box
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "SetSearchFocus", ScriptHelper.GetScript("SetSearchFocus();"));
    }


    /// <summary>
    /// Handles actions occuring when some item is selected.
    /// </summary>
    /// <param name="argument">Argument holding information on selected item.</param>
    private void HandleSelectAction(string argument)
    {
        // Create new selected media item and pass it to the properties dialog
        SelectMediaItem(argument);
    }


    /// <summary>
    /// Hadnles actions occuring when new library file was created.
    /// </summary>
    /// <param name="argument">Argument holding information on new file path.</param>
    private void HandleFileCreatedAction(string argument)
    {
        mediaView.ResetListSelection();

        string[] argArr = argument.Split('|');
        if (argArr.Length == 2)
        {
            string path = argArr[1];
            int mediaFileId = ValidationHelper.GetInteger(argArr[0], 0);

            MediaFileInfo fileInfo = MediaFileInfoProvider.GetMediaFileInfo(mediaFileId);
            if (fileInfo != null)
            {
                path = path.Replace('|', '/');
                path = GetFullFilePath(path);

                SelectTreePath(path);

                // Reload tree if files count needs to be refreshed
                if (DisplayFilesCount)
                {
                    InitializeTree();
                }

                // Select file upload target location            
                string selectPath = EnsureFolderPath(path);
                folderTree.SelectPath(selectPath);
                pnlUpdateTree.Update();

                // Update current folder path information
                FolderPath = GetFilePath(path);

                // Initialize file edit control
                DisplayEditProperties(fileInfo);

                // Highlight newly created file
                ItemToColorize = (DisplayOnlyImportedFiles ? fileInfo.FileGUID.ToString() : EnsureFileName(Path.GetFileName(fileInfo.FilePath)));
                ColorizeRow(ItemToColorize);
            }

            // Load new data and reload view control's content
            LoadData();

            pnlUpdateView.Update();
        }
    }


    /// <summary>
    /// Hadnles actions occuring when existing library file was updated.
    /// </summary>
    /// <param name="argument">Argument holding information on updated file path.</param>
    private void HandleFileUpdatedAction(string argument)
    {
        mediaView.ResetListSelection();

        string[] argArr = argument.Split('|');
        if (argArr.Length == 2)
        {
            string path = argArr[1];
            int mediaFileId = ValidationHelper.GetInteger(argArr[0], 0);

            MediaFileInfo mfi = MediaFileInfoProvider.GetMediaFileInfo(mediaFileId);
            if (mfi != null)
            {
                path = GetFullFilePath(path.Replace('>','\\'));

                // Update current folder path information
                FolderPath = GetFilePath(path);

                ItemToColorize = (DisplayOnlyImportedFiles ? mfi.FileGUID.ToString() : EnsureFileName(Path.GetFileName(mfi.FilePath)));

                // Initialize file edit control
                DisplayEditProperties(mfi);

                // Get URLs
                string url = mediaView.GetItemUrl(LibrarySiteInfo, mfi.FileGUID, mfi.FileName, mfi.FileExtension, mfi.FilePath, false, 0, 0, 0);

                UpdateSelectScript(mfi, url, true);

                // Highlight newly created file                
                ColorizeRowDelayed(ItemToColorize);
            }
        }

        wasLoaded = true;

        ClearActionElems();
    }


    /// <summary>
    /// Handles actions related to the media folder.
    /// </summary>
    /// <param name="folderPath">Argument of handled action.</param>
    /// <param name="isNewFolder">Indicates whether the action is related to the new folder created action.</param>
    /// <param name="displayNormal">Indicates whether the properties should be minimized.</param>
    private void HandleFolderAction(string folderPath, bool isNewFolder)
    {
        HandleFolderAction(folderPath, isNewFolder, true);
    }


    /// <summary>
    /// Handles actions related to the media folder.
    /// </summary>
    /// <param name="folderPath">Argument of handled action.</param>
    /// <param name="isNewFolder">Indicates whether the action is related to the new folder created action.</param>
    /// <param name="displayNormal">Indicates whether the properties should be minimized.</param>
    private void HandleFolderAction(string folderPath, bool isNewFolder, bool displayNormal)
    {
        HandleFolderAction(folderPath, isNewFolder, displayNormal, true);
    }


    /// <summary>
    /// Handles actions related to the media folder.
    /// </summary>
    /// <param name="folderPath">Argument of handled action.</param>
    /// <param name="isNewFolder">Indicates whether the action is related to the new folder created action.</param>
    /// <param name="displayNormal">Indicates whether the properties should be minimized.</param>
    /// <param name="selectAndDisplay">Indicates whether the folder should be selected and displayed.</param>
    private void HandleFolderAction(string folderPath, bool isNewFolder, bool displayNormal, bool selectAndDisplay)
    {
        HandleFolderAction(folderPath, isNewFolder, displayNormal, selectAndDisplay, selectAndDisplay);
    }


    /// <summary>
    /// Handles actions related to the media folder.
    /// </summary>
    /// <param name="folderPath">Argument of handled action.</param>
    /// <param name="isNewFolder">Indicates whether the action is related to the new folder created action.</param>
    /// <param name="displayNormal">Indicates whether the properties should be minimized.</param>
    /// <param name="select">Indicates whether the folder should be selected.</param>
    /// <param name="display">Indicates whether the content of folder should be displayed.</param>
    private void HandleFolderAction(string folderPath, bool isNewFolder, bool displayNormal, bool select, bool display)
    {
        // Update information on currently selected folder path
        FolderPath = GetFilePath(folderPath);

        // Reload tree if new folder was created     
        if (isNewFolder)
        {
            InitializeTree();
        }

        if (select)
        {
            string selectPath = EnsureFolderPath(folderPath);
            folderTree.SelectPath(selectPath);
        }

        // Update menu
        menuElem.LibraryID = LibraryID;
        menuElem.LibraryFolderPath = (!isNewFolder) ? LastFolderPath.Trim('/') : LastFolderPath;
        menuElem.UpdateActionsMenu();

        if (display)
        {
            // Load new data and reload view control's content
            LoadData(true);
            pnlUpdateView.Update();
        }

        if (displayNormal)
        {
            DisplayNormal();
        }
    }


    /// <summary>
    /// Handles folder edit section.
    /// </summary>
    private void HandleFolderEdit()
    {
        string newPath = folderEdit.ProcessFolderAction();
        if (newPath != null)
        {
            newPath = MediaLibraryHelper.EnsurePath(newPath);

            SelectTreePath(newPath);

            HandleFolderAction(newPath, true);
        }

        pnlUpdateProperties.Update();

        wasLoaded = true;
    }


    /// <summary>
    /// Handles attachment edit action.
    /// </summary>
    /// <param name="argument">Media file path comming from the view control.</param>
    private void HandleEditInitialization(string argument)
    {
        IsEditImage = true;

        if (!string.IsNullOrEmpty(argument))
        {
            string filePath = CreateFilePath(argument);

            MediaFileInfo mfi = MediaFileInfoProvider.GetMediaFileInfo(LibrarySiteInfo.SiteName, filePath, LibraryInfo.LibraryFolder);
            if (mfi != null)
            {
                LastEditFileGuid = mfi.FileGUID;

                string parameters = "mediafileguid=" + mfi.FileGUID + "&sitename=" + LibrarySiteInfo.SiteName + "&refresh=1";

                ScriptHelper.RegisterStartupScript(Page, typeof(Page), "EditLibraryUI", ScriptHelper.GetScript("EditImage('" + parameters + "');"));
            }
        }

        wasLoaded = true;

        ClearActionElems();
    }


    /// <summary>
    /// Handles attachment edit action.
    /// </summary>
    /// <param name="argument">Media file GUID comming from the view control.</param>
    private void HandleEdit(string argument)
    {
        IsEditImage = true;

        if (!string.IsNullOrEmpty(argument))
        {
            string[] argArr = argument.Split('|');

            Guid fileGuid = ValidationHelper.GetGuid(argArr[0], Guid.Empty);
            if (fileGuid != Guid.Empty)
            {
                MediaFileInfo mfi = MediaFileInfoProvider.GetMediaFileInfo(fileGuid, LibrarySiteInfo.SiteName);
                if (mfi != null)
                {
                    string url = mediaView.GetItemUrl(LibrarySiteInfo, mfi.FileGUID, mfi.FileName, mfi.FileExtension, mfi.FilePath, false, 0, 0, 0);

                    string fileName = Path.GetFileName(mfi.FilePath);

                    string fileId = (DisplayOnlyImportedFiles ? mfi.FileGUID.ToString() : EnsureFileName(fileName));

                    if (LastEditFileGuid == mfi.FileGUID && fileEdit.Visible && (fileEdit.FileID == mfi.FileID))
                    {
                        DisplayEditProperties(mfi);

                        ItemToColorize = EnsureFileName(Path.GetFileName(mfi.FilePath));
                    }

                    // Update select action to reflect changes made during editing
                    UpdateSelectScript(mfi, url);
                }
            }
            else
            {
                ItemToColorize = EnsureFileName(ValidationHelper.GetString(argument, ""));
            }
        }

        wasLoaded = true;

        ClearActionElems();
    }


    /// <summary>
    /// Handles delete action.
    /// </summary>
    /// <param name="argument">Media file GUID comming from the view control.</param>
    private void HandleFolderDelete(string argument)
    {
        // Check 'Folder delete' permission
        if (MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "folderdelete"))
        {
            if (!string.IsNullOrEmpty(argument))
            {
                // Get path and parent path
                string path = argument;
                string parentPath = GetParentFullPath(path);

                // Delete folder
                MediaLibraryInfoProvider.DeleteMediaLibraryFolder(LibrarySiteInfo.SiteName, LibraryID, path, false);

                SelectTreePath(parentPath);

                // Reload tree and select parent folders
                HandleFolderAction(parentPath, true);
            }

            ClearActionElems();
        }
        else
        {
            DisplayError(MediaLibraryHelper.GetAccessDeniedMessage("folderdelete"));
        }
    }


    /// <summary>
    /// Handles file delete action.
    /// </summary>
    /// <param name="argument">path to the file to delete.</param>
    private void HandleDeleteFile(string argument)
    {
        // Check 'File create' permission
        if (MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "filedelete"))
        {
            if (!string.IsNullOrEmpty(argument))
            {
                string filePath = CreateFilePath(argument);

                // Delete media file
                MediaFileInfo mfi = MediaFileInfoProvider.GetMediaFileInfo(LibraryID, filePath);
                if (mfi != null)
                {
                    // Delete media file info and file
                    MediaFileInfoProvider.DeleteMediaFileInfo(mfi);
                }
                else
                {
                    // Delete file
                    MediaFileInfoProvider.DeleteMediaFile(LibrarySiteInfo.SiteID, LibraryID, filePath, false);
                }
                this.fileEdit.FileID = 0;
                this.pnlUpdateProperties.Update();

                // Reload folder content
                string path = GetFullFilePath(LastFolderPath);

                SelectTreePath(path, !DisplayFilesCount);

                HandleFolderAction(path, DisplayFilesCount);

                // If deleted item was currently selected one
                if (ItemToColorize == EnsureFileName(Path.GetFileName(filePath)))
                {
                    DisplayFolderProperties();
                }
            }

            wasLoaded = true;
        }
        else
        {
            DisplayError(MediaLibraryHelper.GetAccessDeniedMessage("filedelete"));
        }
    }


    /// <summary>
    /// Handles importing single file.
    /// </summary>
    /// <param name="argument"></param>
    private void HandleImportFile(string argument)
    {
        if (!string.IsNullOrEmpty(argument))
        {
            string filePath = CreateFilePath(argument);
            string path = MediaFileInfoProvider.GetMediaFilePath(LibrarySiteInfo.SiteName, LibraryInfo.LibraryFolder, filePath);

            // Try to get file info
            FileInfo fi = new FileInfo(path);
            if (fi != null)
            {
                plcEditFolder.Visible = false;
                plcFileEdit.Visible = false;
                plcMultipleImport.Visible = false;

                plcImportFile.Visible = true;

                fileImport.SetupTexts();
                fileImport.FilePath = CreateFilePath(argument);
                fileImport.LibraryID = LibraryID;
                fileImport.DisplayForm(fi);

                pnlUpdateProperties.Update();

                // FileGUID for imported and FileName for non-imported media file
                ItemToColorize = EnsureFileName(fi.Name);
            }
        }

        ClearActionElems();
        wasLoaded = true;
    }


    /// <summary>
    /// Handles display more action.
    /// </summary>
    private void HandleDisplayMore(string argument)
    {
        // Set display mode
        IsFullListingMode = true;

        argument = argument.Replace('|', '/');
        HandleFolderAction(argument, false);

        DisplayFolderProperties();
        pnlUpdateProperties.Update();

        // Update Menu element
        menuElem.SelectedViewMode = DialogViewModeEnum.ListView;
        menuElem.UpdateViewMenu();
        pnlUpdateMenu.Update();

        ClearActionElems();
    }


    /// <summary>
    /// Handles copy/move action.
    /// </summary>
    private void HandleCopyMoveAction()
    {
        copyMoveInProgress = true;

        InitialzeCopyMoveProperties();

        folderCopyMove.IsLoad = !folderTree.FolderSelected;
        folderCopyMove.PerformAction();

        pnlUpdateProperties.Update();
    }

    #endregion


    #region "Mass action methods"

    /// <summary>
    /// Handles actions comming from mass action control.
    /// </summary>
    /// <param name="argument">Type of an action occuring and files context.</param>
    private void HandleMassAction(string argument)
    {
        string[] argArr = argument.Split('|');
        if (argArr.Length == 2)
        {
            string actionType = argArr[0];
            bool allFiles = (argArr[1].ToLower() == "all");

            // If some items are selected
            if (LibraryInfo != null)
            {
                // Library directory on disk
                string libPath = LibraryRootFolder + LibraryInfo.LibraryFolder;
                // File directory on disk
                string fileDir = libPath + "\\" + LastFolderPath;

                StringBuilder builder = new StringBuilder();

                object[] selectedItems = mediaView.SelectedItems.ToArray();

                switch (actionType.ToLower())
                {
                    case "copy":
                        string copyFiles = "";
                        string copyFolder = "";
                        if (!allFiles)
                        {
                            if (mediaView.SelectedItems.Count > 0)
                            {
                                foreach (string fileName in selectedItems)
                                {
                                    builder.Append(fileName);
                                    builder.Append("|");
                                }

                                copyFiles = HttpUtility.UrlPathEncode(builder.ToString()).Replace("\'", "\\\'").Replace("&", "%26").Replace("#", "%23");
                            }
                            else
                            {
                                DisplayError(ResHelper.GetString("general.noitems"));
                                break;
                            }
                        }

                        copyFolder = HttpUtility.UrlPathEncode(MediaLibraryHelper.EnsurePath(LastFolderPath)).Replace("'", "%27").Replace("&", "%26").Replace("#", "%23");

                        string url = GetCopyMoveDialogUrl();
                        url += "?action=copy&folderpath=" + copyFolder + "&libraryid=" + LibraryID + "&files=" + copyFiles + (allFiles ? "&allFiles=1" : "");
                        // Add security hash
                        url = UrlHelper.AddParameterToUrl(url, "hash", QueryHelper.GetHash(url, false));
                        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "modalCopy", ScriptHelper.GetScript("modalDialog('" + ResolveUrl(url) + "', 'Copy files', '90%', '70%');"));
                        break;

                    case "move":
                        string moveFiles = "";
                        string moveFolder = "";
                        if (!allFiles)
                        {
                            if (mediaView.SelectedItems.Count > 0)
                            {
                                foreach (string fileName in selectedItems)
                                {
                                    builder.Append(fileName);
                                    builder.Append("|");
                                }

                                moveFiles = HttpUtility.UrlPathEncode(builder.ToString()).Replace("'", "%27").Replace("&", "%26").Replace("#", "%23");
                            }
                            else
                            {
                                DisplayError(ResHelper.GetString("general.noitems"));
                                break;
                            }
                        }

                        moveFolder = HttpUtility.UrlPathEncode(MediaLibraryHelper.EnsurePath(LastFolderPath)).Replace("'", "%27").Replace("&", "%26").Replace("#", "%23");

                        url = GetCopyMoveDialogUrl();
                        url += "?action=move&folderpath=" + moveFolder + "&libraryid=" + LibraryID + "&files=" + moveFiles + (allFiles ? "&allFiles=1" : "");
                        // Add security hash
                        url = UrlHelper.AddParameterToUrl(url, "hash", QueryHelper.GetHash(url, false));
                        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "modalMove", ScriptHelper.GetScript("modalDialog('" + ResolveUrl(url) + "', 'Move files', '90%', '70%');"));
                        break;

                    case "delete":
                        // Check 'File delete' permission
                        if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "filedelete"))
                        {
                            DisplayError(MediaLibraryHelper.GetAccessDeniedMessage("filedelete"));
                        }
                        else
                        {
                            bool displayFolder = false;

                            try
                            {
                                if (!allFiles)
                                {
                                    if (mediaView.SelectedItems.Count > 0)
                                    {
                                        foreach (string fileName in selectedItems)
                                        {
                                            MassDeleteFile(fileName);

                                            string filePath = CreateFilePath(fileName);
                                            if (ItemToColorize == EnsureFileName(Path.GetFileName(filePath)))
                                            {
                                                displayFolder = true;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DisplayError(ResHelper.GetString("general.noitems"));
                                        break;
                                    }
                                }
                                else
                                {
                                    displayFolder = true;

                                    DataSet files = GetFileSystemDataSource(null);
                                    if (!DataHelper.IsEmpty(files))
                                    {
                                        foreach (DataRow file in files.Tables[0].Rows)
                                        {
                                            string fileName = ValidationHelper.GetString(file["FileName"], "");

                                            MassDeleteFile(fileName);
                                        }
                                    }
                                }

                                FileList = null;

                                // Reload data
                                string path = GetFullFilePath(LastFolderPath);

                                SelectTreePath(path, !DisplayFilesCount);

                                HandleFolderAction(path, DisplayFilesCount);

                                // If deleted item was currently selected one
                                if (displayFolder)
                                {
                                    DisplayFolderProperties();
                                }
                            }
                            catch (Exception ex)
                            {
                                DisplayError(ResHelper.GetString("media.delete.failed") + ": " + ex.Message);
                            }
                            finally
                            {
                                mediaView.ResetListSelection();
                            }
                        }
                        break;

                    case "import":
                        // Check 'File create' permission
                        if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "filecreate"))
                        {
                            DisplayError(MediaLibraryHelper.GetAccessDeniedMessage("filecreate"));
                        }

                        int numberOfFiles = 0;

                        if (!allFiles)
                        {
                            if (mediaView.SelectedItems.Count > 0)
                            {
                                // Get the list of files and store it in the ViewState
                                foreach (string fileName in selectedItems)
                                {
                                    // Filter only files without record in the DB
                                    if (FileIsNotInDatabase(fileName) == null)
                                    {
                                        numberOfFiles++;
                                        builder.Append(fileName);
                                        builder.Append("|");
                                    }
                                }
                            }
                            else
                            {
                                DisplayError(ResHelper.GetString("general.noitems"));
                                break;
                            }
                        }
                        else
                        {
                            DataSet files = GetFileSystemDataSource(null);
                            foreach (DataRow fileRow in files.Tables[0].Rows)
                            {
                                // Filter only files without record in the DB
                                if (FileIsNotInDatabase(fileRow["FileName"].ToString()) == null)
                                {
                                    numberOfFiles++;
                                    builder.Append(fileRow["FileName"].ToString());
                                    builder.Append("|");
                                }
                            }
                        }

                        string filePaths = builder.ToString();
                        if ((filePaths != "") && (filePaths != "|"))
                        {
                            ImportFilePaths = filePaths;
                            ImportFilesNumber = numberOfFiles;
                            ImportCurrFileIndex = 1;

                            DisplayMultipleImportProperties();
                        }
                        else
                        {
                            DisplayError(ResHelper.GetString("media.file.import.allalreadyimported"));
                        }

                        mediaView.ResetListSelection();
                        break;

                    default:
                        DisplayError(ResHelper.GetString("media.mass.noaction"));
                        break;
                }
            }
        }

        wasLoaded = true;
        ClearActionElems();
    }


    /// <summary>
    /// Performs file delete action.
    /// </summary>
    /// <param name="fileName">Name fo the file to delete.</param>
    private void MassDeleteFile(string fileName)
    {
        string filePath = CreateFilePath(fileName);

        // Delete media file
        MediaFileInfo mfi = MediaFileInfoProvider.GetMediaFileInfo(LibraryID, filePath);
        if (mfi != null)
        {
            // Delete media file info and file
            MediaFileInfoProvider.DeleteMediaFileInfo(mfi);
        }
        else
        {
            // Delete file
            MediaFileInfoProvider.DeleteMediaFile(LibrarySiteInfo.SiteID, LibraryID, filePath, false);
        }
    }


    /// <summary>
    /// Returns true if file information is not in database.
    /// </summary>
    /// <param name="fileName">File name</param>
    private DataRow FileIsNotInDatabase(string fileName)
    {
        if (FileList == null)
        {
            string where = "FilePath LIKE N'" + MediaLibraryHelper.EnsurePath(LastFolderPath.Replace("'", "''").Replace("[", "[[]").Replace("%", "[%]")).Trim('/') +
                "%' AND FilePath NOT LIKE N'" + MediaLibraryHelper.EnsurePath(LastFolderPath.Replace("'", "''").Replace("[", "[[]").Replace("%", "[%]")).Trim('/') +
                "_%/%' AND FileLibraryID = " + LibraryID;

            if (!string.IsNullOrEmpty(LastSearchedValue))
            {
                where += " AND ((FileName LIKE N'%" + LastSearchedValue.Replace("'", "''") + "%') OR (FileExtension LIKE N'%" + LastSearchedValue.Replace("'", "''") + "%'))";
            }

            string columns = "FileID, FilePath, FileGUID, FileName, FileExtension, FileImageWidth, FileImageHeight, FileTitle, FileSize, FileLibraryID, FileSiteID";

            // Get all files from current folder
            DataSet ds = MediaFileInfoProvider.GetMediaFiles(where, "FileName", 0, columns);
            if (ds != null)
            {
                FileList = new Hashtable();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    FileList[row["FilePath"].ToString()] = row;
                }
            }
        }

        if (FileList != null)
        {
            if (String.IsNullOrEmpty(LastFolderPath))
            {
                if (FileList.Contains(fileName))
                {
                    return (FileList[fileName] as DataRow);
                }
            }
            else
            {
                string filePath = MediaLibraryHelper.EnsurePath(LastFolderPath).Trim('/') + "/" + fileName;
                if (FileList.Contains(filePath))
                {
                    return (FileList[filePath] as DataRow);
                }
            }
        }

        return null;
    }

    #endregion


    #region "Event handlers"

    /// <summary>
    /// Behaves as mediator in communication line between control taking action and the rest of the same level controls.
    /// </summary>
    protected void hdnButton_Click(object sender, EventArgs e)
    {
        // Get information on action causing postback        
        string argument = this.CurrentArgument;

        switch (CurrentAction)
        {
            case "search":
                folderCopyMove.IsLoad = false;

                HandleSearchAction(argument);
                break;

            case "select":
                wasLoaded = true;
                ClearActionElems();
                // Handled in the Page_Load
                break;

            case "libraryfilecreated":
                HandleFileCreatedAction(argument);
                break;

            case "libraryfileupdated":
                HandleFileUpdatedAction(argument);
                break;

            case "clickformorefolder":
            case "clickformorelink":
                folderCopyMove.IsLoad = false;

                mediaView.ResetListSelection();

                ResetSearchFilter();
                HandleDisplayMore(argument);

                PerformCopyMoveInit();
                break;

            case "closelisting":
                IsFullListingMode = false;
                folderTree.CloseListing = true;
                folderCopyMove.IsLoad = false;

                // Reload folder
                string path = GetFullFilePath(LastFolderPath);

                SelectTreePath(path);

                HandleFolderAction(path, true);
                break;

            case "parentselect":
                folderCopyMove.IsLoad = false;

                path = GetParentFullPath(LastFolderPath);

                ResetSearchFilter();

                SelectTreePath(path);

                HandleFolderAction(path, true);

                DisplayFolderProperties();

                PerformCopyMoveInit();

                ClearActionElems();
                break;

            case "folderselect":
                folderCopyMove.IsLoad = false;

                ResetSearchFilter();

                if (IsCopyMoveLinkDialog)
                {
                    ItemToColorize = "";
                }

                argument = argument.Replace('|', '/');
                HandleFolderAction(argument, false);

                mediaView.ResetListSelection();

                // Only when folder isn't changed in copy/move dialog
                if (Action == null)
                {
                    DisplayFolderProperties();
                }

                PerformCopyMoveInit();
                ClearActionElems();
                break;

            case "morefolderselect":
                folderCopyMove.IsLoad = false;

                string folderPath = CreateFilePath(argument);
                folderPath = GetFullFilePath(folderPath);

                ResetSearchFilter();

                SelectTreePath(folderPath);

                HandleFolderAction(folderPath, true);

                DisplayFolderProperties();

                PerformCopyMoveInit();

                ClearActionElems();
                break;

            case "folderedit":
                HandleFolderEdit();
                break;

            case "delete":
                HandleFolderDelete(LastFolderPath);
                DisplayFolderProperties();
                break;

            case "newfolder":
                argument = argument.Replace('|', '/').Replace("//", "/").TrimEnd('.');
                ResetSearchFilter();

                SelectTreePath(argument, true);

                HandleFolderAction(argument, true);

                if (!IsCopyMoveLinkDialog)
                {
                    DisplayFolderProperties();
                }
                else
                {
                    folderCopyMove.IsLoad = false;

                    path = GetFilePath(argument);
                    ItemToColorize = EnsureFileName(path);
                    folderCopyMove.NewPath = path;
                }
                break;

            case "copymovefolder":
                argument = argument.Replace('|', '/').Replace("//", "/");
                argument = GetFullFilePath(argument);

                mediaView.ResetListSelection();

                SelectTreePath(argument);

                HandleFolderAction(argument, true);

                DisplayFolderProperties();
                break;

            case "copymoveselect":
                wasLoaded = true;
                ItemToColorize = EnsureFileName(argument);
                folderCopyMove.NewPath = CreateFilePath(argument);
                folderCopyMove.IsLoad = false;
                break;

            case "copymove":
                HandleCopyMoveAction();
                break;

            case "copymovefinished":
                argument = argument.Replace('|', '/');
                path = GetFullFilePath(argument);

                // Clear selection
                this.mediaView.ResetListSelection();
                // Clear searched text
                ResetSearchFilter();
                // Select target path
                SelectTreePath(path);
                // Load data from target path
                HandleFolderAction(path, true);
                //Display properties of target folder
                DisplayFolderProperties();
                break;

            case "massaction":
                HandleMassAction(argument);
                break;

            case "editlibraryui":
                HandleEditInitialization(argument);
                break;

            case "edit":
                HandleEdit(argument);
                break;

            case "deletefile":
                HandleDeleteFile(argument);
                break;

            case "importfile":
                HandleImportFile(argument);
                break;

            default:
                ColorizeLastSelectedRow();
                pnlUpdateView.Update();
                break;
        }
    }


    /// <summary>
    /// Handles event occuring when inner media view control requires load of the data.
    /// </summary>
    private void mediaView_ListReloadRequired()
    {
        LoadData();
    }


    void fileImport_Action(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "save":
                MediaFileInfo mfi = (actionArgument as MediaFileInfo);
                if (mfi != null)
                {
                    string url = mediaView.GetItemUrl(LibrarySiteInfo, mfi.FileGUID, mfi.FileName, mfi.FileExtension, mfi.FilePath, false, 0, 0, 0);

                    // Update item view
                    UpdateSelectScript(mfi, url, true);

                    // Display item properties
                    DisplayEditProperties(mfi);

                    ItemToColorize = (DisplayOnlyImportedFiles ? mfi.FileGUID.ToString() : EnsureFileName(Path.GetFileName(mfi.FilePath)));

                    // Highlight newly created file                
                    ColorizeRowDelayed(ItemToColorize);
                }
                break;

            case "error":
                fileImport.SetupTexts();
                break;
        }

        wasLoaded = true;
    }


    protected MediaFileInfo fileImport_SaveRequired(FileInfo file, string title, string desc, string name, string filePath)
    {
        MediaFileInfo mfi = SaveNewFile(file, title, desc, name, filePath);

        string url = mediaView.GetItemUrl(LibrarySiteInfo, mfi.FileGUID, mfi.FileName, mfi.FileExtension, mfi.FilePath, false, 0, 0, 0);
        string argument = mfi.FileName + "." + mfi.FileExtension.TrimStart('.') + "|" + mfi.FileExtension + "|" + mfi.FilePath + "|" + url + "|" + mfi.FileSize;

        HandleSelectAction(argument);

        return mfi;
    }


    protected void multipleImport_Action(string actionName, object actionArgument)
    {
        if (!string.IsNullOrEmpty(actionName))
        {
            switch (actionName.ToLower().Trim())
            {
                case "importnofiles":
                    DisplayError(ResHelper.GetString("general.noitems"));
                    break;

                case "importcancel":
                    DisplayFolderProperties();
                    DisplayNormal();
                    break;

                case "reloaddata":
                    string path = GetFullFilePath(LastFolderPath);
                    HandleFolderAction(path, false);
                    break;

                case "singlefileimported":
                    multipleImport.SetupTexts();
                    wasLoaded = true;
                    break;

                case "importerror":
                    string msg = ValidationHelper.GetString(actionArgument, "");
                    multipleImport.DisplayError(msg);
                    wasLoaded = true;
                    break;
            }
        }

        pnlUpdateProperties.Update();
    }


    protected MediaFileInfo multipleImport_SaveRequired(FileInfo file, string title, string desc, string name, string filePath)
    {
        return SaveNewFile(file, title, desc, name, filePath);
    }


    protected string multipleImport_GetItemUrl(string fileName)
    {
        DataSet fileData = GetFileSystemDataSource("FileName LIKE '" + DataHelper.EscapeLikeQueryPatterns(fileName, true, true, true) + "'");

        if (!DataHelper.IsEmpty(fileData))
        {
            return UrlHelper.GetAbsoluteUrl(fileData.Tables[0].Rows[0]["FileURL"].ToString());
        }

        return "";
    }


    protected object mediaView_GetInformation(string type, object parameter)
    {
        switch (type.ToLower())
        {
            case "fileisnotindatabase":
                string fileName = ValidationHelper.GetString(parameter, "");
                return FileIsNotInDatabase(fileName);

            case "siteidrequired":
                return LibrarySiteInfo.SiteID;

            default:
                return null;
        }
    }


    private void fileList_OnNotAllowed(string permissionType, CMSAdminControl sender)
    {
        RaiseOnCheckPermissions(permissionType, sender);
    }


    private void fileList_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        RaiseOnCheckPermissions(permissionType, sender);
    }


    private void folderDelete_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        RaiseOnCheckPermissions(permissionType, sender);
    }


    private void folderEdit_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        RaiseOnCheckPermissions(permissionType, sender);
    }


    private void folderTree_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        RaiseOnCheckPermissions(permissionType, sender);
    }


    private void folderActions_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        RaiseOnCheckPermissions(permissionType, sender);
    }


    private void fileEdit_Action(string actionName, object actionArgument)
    {
        ItemToColorize = EnsureFileName((string)actionArgument);
    }


    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (e.Row.DataItem as DataRowView);
            if ((drv != null) && drv.DataView.Table.Columns.Contains("Extension") && (drv["Extension"].ToString().ToLower() == "<dir>"))
            {
                // Hide selection check box for folder items
                CheckBox selectBox = e.Row.Cells[1].FindControl("itemBox") as CheckBox;
                if (selectBox != null)
                {
                    selectBox.Visible = false;
                }
            }
        }
    }


    protected void ListViewControl_OnBeforeSorting(object sender, EventArgs e)
    {
        GridViewSortEventArgs sortArg = (e as GridViewSortEventArgs);
        if (sortArg != null)
        {
            SortDirection = (mediaView.ListViewControl.SortDirect.ToLower().EndsWith("desc") ? "DESC" : "ASC");

            string sortExpr = (sortArg.SortExpression == "FileSize") ? "Size" : sortArg.SortExpression;
            SortColumns = sortExpr;
        }
    }

    #endregion


    #region "Helper methods"

    /// <summary>
    /// Returns URL of the dialog to open.
    /// </summary>
    private string GetCopyMoveDialogUrl()
    {
        string result = "~/CMSModules/MediaLibrary/CMSPages/SelectFolder.aspx";
        if (!IsLiveSite)
        {
            result = "~/CMSModules/MediaLibrary/Tools/FolderActions/SelectFolder.aspx";
        }

        return result;
    }


    /// <summary>
    /// Performs actions required to reload control and its data.
    /// </summary>
    public override void ReloadData()
    {
        ControlReloaded = true;

        // Display default
        ClearForm();

        // Force control initialization
        InitializeControl();
    }


    /// <summary>
    /// Ensures all the actions to set control in its default state.
    /// </summary>
    public override void ClearForm()
    {
        FolderPath = "";
        InitializeTree();

        DisplayFolderProperties();
    }


    /// <summary>
    /// Stores new media file info into the DB
    /// </summary>
    /// <param name="fi">Info on file to be stored</param>
    /// <param name="description">Description of new media file</param>
    /// <param name="name">Name of new media file</param>
    private MediaFileInfo SaveNewFile(FileInfo fi, string title, string description, string name, string filePath)
    {
        string path = MediaLibraryHelper.EnsurePath(filePath);
        string fileName = name;
        string origFileName = Path.GetFileNameWithoutExtension(fi.Name);

        string fullPath = fi.FullName;
        string extension = UrlHelper.GetSafeFileName(fi.Extension, CMSContext.CurrentSiteName);
        int fileSize = ValidationHelper.GetInteger(fi.Length, 0);

        // Check if filename is changed ad move file if necessary
        if (fileName + extension != fi.Name)
        {
            string oldPath = path;
            fullPath = MediaLibraryHelper.EnsureUniqueFileName(Path.GetDirectoryName(fullPath) + "\\" + fileName + extension);
            path = MediaLibraryHelper.EnsurePath(Path.GetDirectoryName(path) + "/" + Path.GetFileName(fullPath)).TrimStart('/');
            MediaFileInfoProvider.MoveMediaFile(CMSContext.CurrentSiteName, LibraryID, oldPath, path, true);
            fileName = Path.GetFileNameWithoutExtension(fullPath);
        }

        // Create media file info
        MediaFileInfo fileInfo = new MediaFileInfo(fullPath, LibraryInfo.LibraryID, MediaLibraryHelper.EnsurePath(Path.GetDirectoryName(path)));

        fileInfo.FileTitle = title;
        fileInfo.FileDescription = description;

        // Save media file info
        MediaFileInfoProvider.ImportMediaFileInfo(fileInfo);

        // Save FileID in ViewState
        fileEdit.FileID = fileInfo.FileID;
        fileEdit.FilePath = fileInfo.FilePath;

        return fileInfo;
    }


    /// <summary>
    /// Updates JavaScript used to cause asynchronous post-back when item is selected in view.
    /// </summary>
    /// <param name="mediaFile">Updated media file.</param>
    /// <param name="url">URL of updated media file.</param>
    private void UpdateSelectScript(MediaFileInfo mediaFile, string url)
    {
        UpdateSelectScript(mediaFile, url, false);
    }


    /// <summary>
    /// Updates JavaScript used to cause asynchronous post-back when item is selected in view.
    /// </summary>
    /// <param name="mediaFile">Updated media file.</param>
    /// <param name="url">URL of updated media file.</param>
    /// <param name="afterImport">Indicates whether the update is result of recent import action.</param>
    private void UpdateSelectScript(MediaFileInfo mediaFile, string url, bool afterImport)
    {
        string fileName = Path.GetFileName(mediaFile.FilePath);
        // Escape apostrof from URL
        url = url.Replace("'", "%27");

        string argSet = "";
        if (DisplayOnlyImportedFiles)
        {
            argSet = mediaFile.FileGUID + "|" + mediaFile.FileName + "|" + mediaFile.FilePath + "|" + mediaFile.FileExtension +
                "|" + mediaFile.FileImageWidth + "|" + mediaFile.FileImageHeight + "|" + mediaFile.FileTitle + "|" + mediaFile.FileSize +
                "|" + mediaFile.FileID;
        }
        else
        {
            argSet = fileName + "|" + mediaFile.FileExtension + "|" + url + "|" + mediaFile.FileSize;
        }

        string origFileId = null;

        // Ensure name data
        if (afterImport && (mediaFile.RelatedData != null))
        {
            origFileId = EnsureFileName(Path.GetFileName(mediaFile.RelatedData.ToString()));
        }
        else if (!afterImport && (mediaFile.RelatedData != null))
        {
            origFileId = EnsureFileName(Path.GetFileName(mediaFile.FilePath.ToString()));
        }
        else
        {
            origFileId = null;
        }

        string origScript = "";
        string thumbScript = "";
        string editImgUrl = "";

        string fileId = (DisplayOnlyImportedFiles ? mediaFile.FileGUID.ToString() : Path.GetFileName(mediaFile.FilePath));
        fileId = EnsureFileName(fileId);
        string imgId = Guid.NewGuid().ToString();
        string newSelectAction = "ColorizeRow('" + fileId + "'); SetSelectAction(" + ScriptHelper.GetString(argSet + "|" + url) + "); return false;";
        string newEditAction = "$j(\"#hdnFileOrigName\").attr('value', '" + fileId + "'); SetAction('editlibraryui', '" + Path.GetFileName(mediaFile.FilePath) + "'); RaiseHiddenPostBack(); return false;";
        string newDeleteAction = "if(DeleteMediaFileConfirmation() == false){return false;} SetAction('deletefile','" + fileName + "'); RaiseHiddenPostBack(); return false;";
        string newViewAction = "window.open('@URL@'); return false;";
        string newIFrameUrl = UrlHelper.ResolveUrl(mediaView.GetUpdateIFrameUrl(mediaFile.DataClass.DataRow));
        string newName = mediaFile.FileName.TrimEnd('.') + "." + mediaFile.FileExtension.TrimStart('.').ToLower();
        string imgDeletePath = "Design/Controls/UniGrid/Actions/Delete.png";
        string imgNextPath = "Design/Controls/UniGrid/Actions/Next.png";

        bool isImage = ImageHelper.IsImage(mediaFile.FileExtension);
        if (!isImage)
        {
            editImgUrl = "Design/Controls/UniGrid/Actions/Editdisabled.png";
        }
        else
        {
            editImgUrl = "Design/Controls/UniGrid/Actions/Edit.png";
        }

        // Modified when
        string newModifiedWhen = "";
        string fullFileName = mediaFile.FileName + "." + mediaFile.FileExtension.TrimStart('.').ToLower();

        DataSet file = GetFileSystemDataSource("FileName LIKE '" + fullFileName + "'");
        if (!DataHelper.IsEmpty(file) && (file.Tables[0] != null) && (file.Tables[0].Rows.Count > 0) && (file.Tables[0].Rows[0] != null))
        {
            newModifiedWhen = CMSContext.ConvertDateTime(ValidationHelper.GetDateTime(file.Tables[0].Rows[0]["Modified"], DataHelper.DATETIME_NOT_SELECTED), this).ToString();
        }

        if (afterImport && (origFileId != null))
        {
            MediaFileToRender = mediaFile;
            RenderUpdateControl = true;
        }

        // Get thumbnail dimensions
        int[] dims = ImageHelper.EnsureImageDimensions(0, 0, 200, mediaFile.FileImageWidth, mediaFile.FileImageHeight);
        string thumbUrl = mediaView.GetItemUrl(LibrarySiteInfo, mediaFile.FileGUID, mediaFile.FileName, mediaFile.FileExtension, mediaFile.FilePath, true, dims[1], dims[0], 0);
        thumbUrl = UrlHelper.UpdateParameterInUrl(thumbUrl, "chset", Guid.NewGuid().ToString());

        string updateScript = "";
        if (menuElem.SelectedViewMode == DialogViewModeEnum.ListView)
        {
            newName = Path.GetFileNameWithoutExtension(newName);

            updateScript = UPDT_SCRIPT_LIST;
            updateScript = updateScript.Replace("@THUMBWIDTH@", dims[0].ToString()).Replace("@THUMBHEIGHT@", dims[1].ToString());
            updateScript = updateScript.Replace("@NEWMODIFIEDWHEN@", newModifiedWhen);

            string extUrl = UrlHelper.ResolveUrl(GetFileIconUrl(mediaFile.FileExtension, "list"));
            updateScript = updateScript.Replace("@NEWEXTURL@", extUrl);

            // Additional element manipulation when import has taken place
            if (afterImport)
            {
                string addScript = "$j(\"#@ID@ input[id$='action_select']\").attr('style', 'border-width: 0px; margin: 0px 3px;').attr('src', '" + GetImageUrl(imgNextPath, IsLiveSite) + "').attr('title'," + ScriptHelper.GetString(ResHelper.GetString("general.select")) + ");" +
                    "$j(\"#@ID@ input[id$='action_edit']\").attr('style', 'border-width: 0px; margin: 0px 3px;').attr('src', '" + GetImageUrl(editImgUrl, IsLiveSite) + "')" + (isImage ? ".removeAttr('disabled')" : "") + ";" +
                    "$j(\"#@ID@ input[id$='action_view']\").attr('style', 'border-width: 0px; margin: 0px 3px;').attr('src', '" + GetImageUrl("Design/Controls/UniGrid/Actions/View.png", IsLiveSite) + "').removeAttr('disabled');" +                    
                    "$j(\"#@ID@ input[id$='action_import']\").remove(); " +
                    "$j(\"#@ID@ div[class='DialogListItemNameRow'] span a\").text('" + Path.GetFileNameWithoutExtension(fileName) + "');" +
                    "$j(\"#@ID@ iframe[id$='uploaderFrame']\").attr('src', " + ScriptHelper.GetString(newIFrameUrl) + ");";

                if (origFileId != null)
                {
                    origScript = "$j(\"#hdnFileOrigName\").attr('value', '" + origFileId + "'); ";
                    addScript += "$j(\"#@ID@ td[class='UnigridSelection'] + td div\").html('@NEWIFRAMEHTML@');";

                    thumbScript = "$j(\"#@ID@ div[class='DialogListItemNameRow'] img\").replaceWith('<img alt=\"\" id=\"" + imgId + "\" src=\"" + extUrl + "\"/><input type=\"hidden\" id=\"" + imgId + "_src\" value=\"" + ResolveUrl(thumbUrl) + "\" />');";
                }
                updateScript = origScript + thumbScript + updateScript + addScript;
            }
            updateScript += "$j(\"#@ID@ input[id$='action_delete']\").attr('style', 'border-width: 0px; margin: 0px 3px;').attr('src', '" + GetImageUrl(imgDeletePath, IsLiveSite) + "').removeAttr('disabled').attr('onclick', '').unbind('click').click(function () { @DELACTION@ });";
            updateScript += "$j(\"#@ID@ input[id$='action_view']\").attr('onclick', '').unbind('click').click(function () { " + newViewAction + " });";
        }
        else if (menuElem.SelectedViewMode == DialogViewModeEnum.TilesView)
        {
            string previewUrl = mediaView.GetItemUrl(LibrarySiteInfo, mediaFile.FileGUID, mediaFile.FileName, mediaFile.FileExtension, mediaFile.FilePath, true, 0, 0, 48);
            previewUrl = UrlHelper.UpdateParameterInUrl(previewUrl, "chset", Guid.NewGuid().ToString());

            updateScript = UPDT_SCRIPT_TILES;
            updateScript = updateScript.Replace("@THUMBWIDTH@", dims[0].ToString()).Replace("@THUMBHEIGHT@", dims[1].ToString());
            updateScript = updateScript.Replace("@MAXSIDESIZE@", "48");

            // Additional element manipulation when import has taken place
            if (afterImport)
            {
                string addScript = "$j(\"#@ID@ div[class='DialogTileItemActions'] input[id$='btnSelect']\").attr('style', 'border-width: 0px;').attr('src', '" + GetImageUrl(imgNextPath, IsLiveSite) + "');" +
                    "$j(\"#@ID@ div[class='DialogTileItemActions'] input[id$='btnContentEdit']\").attr('style', 'border-width: 0px;').attr('src', '" + GetImageUrl(editImgUrl) + "')" + (isImage ? ".removeAttr('disabled')" : "") + ";" +
                    "$j(\"#@ID@ div[class='DialogTileItemActions'] input[id$='btnView']\").attr('style', 'border-width: 0px;').attr('src', '" + GetImageUrl("Design/Controls/UniGrid/Actions/View.png", IsLiveSite) + "').removeAttr('disabled');" +                    
                    "$j(\"#@ID@ div[class='DialogTileItemActions'] input[id$='btnImport']\").remove();" +
                    "$j(\"#@ID@ div[class='DialogTileItemActions'] input[id$='btnView']\").attr('onclick', '').unbind('click').click(function () { " + newViewAction + " });" +
                    "$j(\"#@ID@ div[class='DialogTileItemInfo'] span[id$='lblFileName']\").text('" + Path.GetFileName(fileName) + "');" +
                    "$j(\"#@ID@ iframe[id$='uploaderFrame']\").attr('src', '" + newIFrameUrl + "');";

                if (origFileId != null)
                {
                    origScript = "$j(\"#hdnFileOrigName\").attr('value', '" + origFileId + "'); ";
                    addScript += "$j(\"#@ID@ div[class='DialogTileItemActions'] td:has(div[id$='pnlDisabledUpdate'])\").replaceWith('<td>@NEWIFRAMEHTML@</td>');";

                    thumbScript = "$j(\"#@ID@ div[class='DialogTileItemImage'] img\").replaceWith('<img alt=\"\" id=\"" + imgId + "\" src=\"" + previewUrl + "\"/><input type=\"hidden\" id=\"" + imgId + "_src\" value=\"" + ResolveUrl(thumbUrl) + "\" />');";
                }                
                updateScript = origScript + thumbScript + updateScript + addScript;
            }
            updateScript += "$j(\"#@ID@ div[class='DialogTileItemActions'] input[id$='btnDelete']\").attr('style', 'border-width: 0px;').attr('src', '" + GetImageUrl(imgDeletePath, IsLiveSite) + "').removeAttr('disabled').attr('onclick', '').unbind('click').click(function () { @DELACTION@ });";
            updateScript += "$j(\"#@ID@ div[class='DialogTileItemActions'] input[id$='btnView']\").attr('onclick', '').unbind('click').click(function () { " + newViewAction + " });";

            if (!isImage)
            {
                previewUrl = GetFileIconUrl(mediaFile.FileExtension, "");
            }

            updateScript += "$j(\"#@ID@ div[class='DialogTileItemImage'] img\").attr('src', '" + previewUrl + "');";
        }
        else
        {
            int[] tileDims = CMSDialogHelper.GetThumbImageDimensions(mediaFile.FileImageHeight, mediaFile.FileImageWidth, 95, 160);

            string previewUrl = "";
            if (!isImage)
            {
                previewUrl = GetFileIconUrl(mediaFile.FileExtension, "");
            }
            else
            {
                previewUrl = mediaView.GetItemUrl(LibrarySiteInfo, mediaFile.FileGUID, mediaFile.FileName, mediaFile.FileExtension, mediaFile.FilePath, true, tileDims[0], tileDims[1], 0);
            }
            previewUrl = UrlHelper.UpdateParameterInUrl(previewUrl, "chset", Guid.NewGuid().ToString());

            updateScript = UPDT_SCRIPT_THUMBS;
            updateScript = updateScript.Replace("@WIDTH@", tileDims[1].ToString()).Replace("@HEIGHT@", tileDims[0].ToString());

            // Additional element manipulation when import has taken place
            if (afterImport)
            {
                string addScript = "$j(\"#@ID@ div[class='DialogThumbnailActions'] input[id$='btnSelect']\").attr('style', 'border-width: 0px;').attr('src', '" + GetImageUrl(imgNextPath, IsLiveSite) + "');" +
                    "$j(\"#@ID@ div[class='DialogThumbnailActions'] input[id$='btnContentEdit']\").attr('style', 'border-width: 0px;').attr('src', '" + GetImageUrl(editImgUrl, IsLiveSite) + "')" + (isImage ? ".removeAttr('disabled')" : "") + ";" +
                    "$j(\"#@ID@ div[class='DialogThumbnailActions'] input[id$='btnView']\").attr('style', 'border-width: 0px;').attr('src', '" + GetImageUrl("Design/Controls/UniGrid/Actions/View.png", IsLiveSite) + "').removeAttr('disabled');" +                    
                    "$j(\"#@ID@ div[class='DialogThumbnailActions'] input[id$='btnImport']\").remove();" +
                    "$j(\"#@ID@ div[class='DialogThumbnailItemInfo'] span[id$='lblFileName']\").text('" + Path.GetFileName(fileName) + "');" +
                    "$j(\"#@ID@ iframe[id$='uploaderFrame']\").attr('src', '" + newIFrameUrl + "');" +
                    "$j(\"#@ID@ table[class='DialogThumbnailItemImage'] img\").attr('src', '" + previewUrl + "');";

                if (origFileId != null)
                {
                    origScript = "$j(\"#hdnFileOrigName\").attr('value', '" + origFileId + "'); ";
                    addScript += "$j(\"#@ID@ div[class='DialogThumbnailActions'] td:has(div[id$='pnlDisabledUpdate'])\").replaceWith('<td>@NEWIFRAMEHTML@</td>');";
                }                
                updateScript = origScript + updateScript + addScript;
            }
            updateScript += "$j(\"#@ID@ div[class='DialogThumbnailActions'] input[id$='btnDelete']\").attr('style', 'border-width: 0px;').attr('src', '" + GetImageUrl(imgDeletePath, IsLiveSite) + "').removeAttr('disabled').attr('onclick', '').unbind('click').click(function () { @DELACTION@ });";
            updateScript = updateScript.Replace("@PREVURL@", previewUrl);
            updateScript += "$j(\"#@ID@ div[class='DialogThumbnailActions'] input[id$='btnView']\").attr('onclick', '').unbind('click').click(function () { " + newViewAction + " });";
        }
        updateScript = updateScript.Replace("@EDITACTION@", newEditAction).Replace("@DELACTION@", newDeleteAction);
        updateScript = updateScript.Replace("@ID@", fileId).Replace("@ACTION@", newSelectAction).Replace("@NEWGUID@", Guid.NewGuid().ToString());
        updateScript = updateScript.Replace("@URL@", url).Replace("@NEWNAME@", ScriptHelper.GetString(newName)).Replace("@NEWFILENAME@", fullFileName);

        // Update List & Tiles view specific information
        if ((menuElem.SelectedViewMode == DialogViewModeEnum.ListView) || (menuElem.SelectedViewMode == DialogViewModeEnum.TilesView))
        {
            // Update size after conversion
            long size = ValidationHelper.GetLong(mediaFile.FileSize, 0);
            updateScript = updateScript.Replace("@NEWSIZE@", DataHelper.GetSizeString(size)).Replace("@NEWEXT@", mediaFile.FileExtension.ToLower());
            updateScript = updateScript.Replace("@THUMBURL@", thumbUrl);

            if (RenderUpdateControl)
            {
                updateScript = updateScript.Replace("@IMGID@", "'" + imgId + "'");
            }
            else
            {
                updateScript = updateScript.Replace("@IMGID@", "img.attr('id')");
            }

            if (!isImage)
            {
                // Hide Tooltip for non-image files
                updateScript += " if(img.length > 0) { img.unbind('mouseover'); }";
            }

            // Ensure hover function
            ScriptHelper.RegisterStartupScript(Page, typeof(Page), "UpdateHoverScript", ScriptHelper.GetScript(EnsureHoverFunction()));
        }

        updateScript = "function UpdateSelectScript() {" + updateScript + "} setTimeout('UpdateSelectScript()', 300);";

        if (!RenderUpdateControl)
        {
            // Run update script
            ScriptHelper.RegisterStartupScript(Page, typeof(Page), "UpdateEditScript", ScriptHelper.GetScript(updateScript));
        }
        else
        {
            UpdateScript = updateScript;
        }
    }


    /// <summary>
    /// Ensures given file name in the way it is usable as ID.
    /// </summary>
    /// <param name="fileName">Name of the file to ensure.</param>
    private string EnsureFileName(string fileName)
    {
        if (!string.IsNullOrEmpty(fileName))
        {
            char[] specialChars = "#;&,.+*~':\"!^$[]()=>|/\\-%@`".ToCharArray();
            foreach (char specialChar in specialChars)
            {
                fileName = fileName.Replace(specialChar, '_');
            }
            return fileName.Replace(" ", "").ToLower();
        }

        return fileName;
    }

    
    /// <summary>
    /// Ensures that the text can be used in the WHERE condition.
    /// </summary>
    /// <param name="text">Text to ensure.</param>
    private string NormalizeForSql(string text)
    {
        if (!String.IsNullOrEmpty(text) && text.Contains("%"))
        {
            text = text.Replace("%", PERCENT_ESC_CHAR + "%");
        }

        return text;
    }


    /// <summary>
    /// Returns file path without library root folder.
    /// </summary>
    /// <param name="argument">Original file path.</param>
    private string GetFilePath(string argument)
    {
        if (!string.IsNullOrEmpty(argument))
        {
            int rootFolderNameIndex = argument.IndexOf('/');

            if (rootFolderNameIndex > -1)
            {
                argument = argument.Substring(rootFolderNameIndex);
            }
            else if (argument.StartsWith(LibraryInfo.LibraryFolder))
            {
                argument = argument.Remove(0, LibraryInfo.LibraryFolder.Length);
            }

            return argument.TrimStart('/');
        }

        return "";
    }


    /// <summary>
    /// Returns full file path including library folder.
    /// </summary>
    /// <param name="path">File path to get full path for.</param>
    private string GetFullFilePath(string path)
    {
        if ((LibraryInfo != null) && (path != null))
        {
            return (LibraryInfo.LibraryFolder + '/' + path).TrimEnd('/');
        }

        return "";
    }


    /// <summary>
    /// Gets folder path of the parent of the folder specified by its path.
    /// </summary>
    /// <param name="path">Path of the folder.</param
    private string GetParentFullPath(string path)
    {
        if (!string.IsNullOrEmpty(path))
        {
            path = MediaLibraryHelper.EnsurePath(path);

            int lastSlash = path.LastIndexOf('/');
            if (lastSlash > -1)
            {
                path = path.Substring(0, lastSlash).Trim('/');
            }
            else
            {
                path = "";
            }

            if (LibraryInfo != null)
            {
                path = LibraryInfo.LibraryFolder + '/' + path;
            }
        }

        return path.TrimEnd('/');
    }


    /// <summary>
    /// Gets file path based on its file name, recently selected folder path and library folder.
    /// </summary>
    /// <param name="fileName">Name of the file (including extension).</param>
    private string CreateFilePath(string fileName)
    {
        string filePath = "";
        if (String.IsNullOrEmpty(LastFolderPath))
        {
            // If root folder of library
            filePath = fileName;
        }
        else
        {
            filePath = LastFolderPath + "\\" + fileName;
        }

        return filePath.Replace('/', '\\').Replace('|', '\\');
    }


    /// <summary>
    /// Highlights item specified by its ID.
    /// </summary>
    /// <param name="itemId">String representation of item ID.</param>
    private void ColorizeRow(string itemId)
    {
        // Keep item selected
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "ColorizeSelectedRow", ScriptHelper.GetScript("function tryColorizeRow(itemId) { if(typeof(ColorizeRow) == 'function'){ ColorizeRow(itemId); } else { setTimeout(\"tryColorizeRow('\" + itemId + \"')\", 500); } }; tryColorizeRow('" + itemId + "');"));
    }


    /// <summary>
    /// Highlights item specified by its ID.
    /// </summary>
    /// <param name="itemId">String representation of item ID.</param>
    private void ColorizeRowDelayed(string itemId)
    {
        // Keep item selected
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "ColorizeSelectedRowDelayed", ScriptHelper.GetScript("function tryColorizeRow(itemId) { if(typeof(ColorizeRow) == 'function'){ setTimeout(\"tryColorizeRow('\" + itemId + \"')\", 500); } else { setTimeout(\"tryColorizeRow('\" + itemId + \"')\", 500); } }; tryColorizeRow('" + itemId + "');"));
    }


    /// <summary> 
    /// Highlights row recently selected.
    /// </summary>
    private void ColorizeLastSelectedRow()
    {
        // Keep item selected
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "ColorizeLastSelectedRow", ScriptHelper.GetScript("if(typeof(ColorizeLastRow) == 'function'){ ColorizeLastRow(); }"));
    }


    /// <summary>
    /// Checks permissions for the current user taking specified action.
    /// </summary>
    /// <param name="action">Action permissions are checked for.</param>
    private string CheckPermissions()
    {
        if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, CMSAdminControl.PERMISSION_READ) &&
            !MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "LibraryAccess"))
        {
            return ResHelper.GetString("media.security.noaccess");
        }

        return "";
    }


    /// <summary>
    /// Initializes and displays file info properties.
    /// </summary>
    /// <param name="fileInfo">File to edit.</param>
    private void DisplayEditProperties(MediaFileInfo fileInfo)
    {
        plcFileEdit.Visible = true;

        plcEditFolder.Visible = false;
        folderEdit.StopProcessing = true;

        plcMultipleImport.Visible = false;
        plcImportFile.Visible = false;
        plcCopyMove.Visible = false;
        folderCopyMove.StopProcessing = true;

        fileEdit.StopProcessing = false;
        fileEdit.FileID = fileInfo.FileID;
        fileEdit.FileInfo = fileInfo;
        fileEdit.FilePath = fileInfo.FilePath;
        fileEdit.FolderPath = LastFolderPath;
        fileEdit.LibraryInfo = LibraryInfo;
        fileEdit.LibrarySiteInfo = LibrarySiteInfo;
        fileEdit.MediaLibraryID = LibraryID;
        fileEdit.ReloadControl();

        pnlUpdateProperties.Update();
    }


    /// <summary>
    /// Initializes and displays the folder info properties.
    /// </summary>
    private void DisplayFolderProperties()
    {
        plcEditFolder.Visible = true;

        plcFileEdit.Visible = false;
        fileEdit.StopProcessing = true;

        plcMultipleImport.Visible = false;
        plcImportFile.Visible = false;
        plcCopyMove.Visible = false;
        folderCopyMove.StopProcessing = true;

        folderEdit.UseViewStateProperties = true;
        folderEdit.DisplayCancel = false;
        folderEdit.Action = "edit";
        folderEdit.CustomScript = "SetAction('folderedit', ''); RaiseHiddenPostBack(); return false;";
        folderEdit.IsLiveSite = IsLiveSite;
        folderEdit.LibraryID = LibraryID;
        folderEdit.LibraryFolder = LibraryInfo.LibraryFolder;
        folderEdit.FolderPath = LastFolderPath;

        // Special treatment for root folder
        if (LastFolderPath == "")
        {
            folderEdit.Enabled = false;
            folderEdit.FolderPath = LibraryInfo.LibraryFolder;
        }
        else
        {
            folderEdit.Enabled = true;
        }
        folderEdit.ReloadData();

        ItemToColorize = "";

        pnlUpdateProperties.Update();
    }


    /// <summary>
    /// Initializes and displays the multiple import properties.
    /// </summary>
    private void DisplayMultipleImportProperties()
    {
        plcEditFolder.Visible = false;
        plcFileEdit.Visible = false;
        fileEdit.StopProcessing = true;
        plcImportFile.Visible = false;
        plcCopyMove.Visible = false;
        folderCopyMove.StopProcessing = true;

        plcMultipleImport.Visible = true;

        // Initialize multiple files import control
        multipleImport.ImportCurrFileIndex = ImportCurrFileIndex;
        multipleImport.ImportFilePaths = ImportFilePaths;
        multipleImport.ImportFilesNumber = ImportFilesNumber;
        multipleImport.RootFolderPath = LibraryRootFolder;
        multipleImport.FolderPath = LastFolderPath;
        multipleImport.IsLiveSite = IsLiveSite;
        multipleImport.LibraryID = LibraryID;
        multipleImport.LibrarySiteInfo = LibrarySiteInfo;
        multipleImport.SetupTexts();
        multipleImport.SetupImport(true);

        // Display multiple files import control in full size
        DisplayFull();

        pnlUpdateProperties.Update();
    }


    /// <summary>
    /// Initializes and displays file info properties.
    /// </summary>
    /// <param name="fileInfo">File to edit.</param>
    private void DisplayCopyMoveProperties()
    {
        plcCopyMove.Visible = true;
        folderCopyMove.Visible = true;

        folderEdit.StopProcessing = true;
        plcFileEdit.Visible = false;
        plcEditFolder.Visible = false;
        plcMultipleImport.Visible = false;
        plcImportFile.Visible = false;

        InitialzeCopyMoveProperties();

        pnlUpdateProperties.Update();
    }


    /// <summary>
    /// Displays error message.
    /// </summary>
    /// <param name="err">Text of the error message.</param>
    private void DisplayError(string err)
    {
        lblInfo.Visible = false;

        pnlInfo.Visible = true;
        lblError.Text = err;
        lblError.Visible = true;

        pnlUpdateViewInfo.Update();
    }


    /// <summary>
    /// Displays properties in full size.
    /// </summary>
    private void DisplayFull()
    {
        // Change CSS class so properties are displayed in full size
        divDialogView.Attributes["class"] = "DialogElementHidden";
        divDialogResizer.Attributes["class"] = "DialogElementHidden";
        divDialogProperties.Attributes["class"] = "DialogPropertiesFullSize";

        pnlUpdateContent.Update();
    }


    /// <summary>
    /// Displays properties in default size.
    /// </summary>
    private void DisplayNormal()
    {
        if (divDialogView.Attributes["class"] == "DialogElementHidden")
        {
            // Change CSS class so properties are displayed in normal size
            divDialogView.Attributes["class"] = "DialogViewContent MediaViewContent";
            divDialogResizer.Attributes["class"] = "DialogResizerVLine";
            divDialogProperties.Attributes["class"] = "DialogProperties";

            // Initialize resizers
            ScriptHelper.RegisterStartupScript(Page, typeof(string), "initresizers", ScriptHelper.GetScript("InitResizers();"));

            pnlUpdateContent.Update();
        }
    }


    /// <summary>
    /// Clears hidden control elements for the future use.
    /// </summary>
    private void ClearActionElems()
    {
        hdnAction.Value = "";
        hdnArgument.Value = "";
    }


    /// <summary>
    /// Ensures that filter is no more applied.
    /// </summary>
    private void ResetSearchFilter()
    {
        mediaView.ResetSearch();
        LastSearchedValue = "";
    }


    /// <summary>
    /// Ensures first page is displayed in the control displaying the content.
    /// </summary>
    private void ResetPageIndex()
    {
        mediaView.ResetPageIndex();
    }


    /// <summary>
    /// Performs copy/move new path initialization if necessary.
    /// </summary>
    private void PerformCopyMoveInit()
    {
        if (IsCopyMoveLinkDialog)
        {
            wasLoaded = true;
            folderCopyMove.NewPath = LastFolderPath;
            folderCopyMove.IsLoad = false;
        }
    }


    /// <summary>
    /// Gets JavaScript code for hover functionality.
    /// </summary>
    private string EnsureHoverFunction()
    {
        StringBuilder sb = new StringBuilder();

        // If jQuery not loaded
        sb.Append("if (typeof jQuery == 'undefined') { \n");
        sb.Append("var jQueryCore=document.createElement('script'); \n");
        sb.Append("jQueryCore.setAttribute('type','text/javascript'); \n");
        sb.Append("jQueryCore.setAttribute('src', '" + ResolveUrl("~/CMSScripts/jquery/jquery-core.js") + "'); \n");
        sb.Append("setTimeout('document.body.appendChild(jQueryCore)',100); setTimeout('loadTooltip()',200); \n");
        sb.Append(" \n}");

        // If jQuery tooltip plugin not loaded
        sb.Append("var jQueryTooltips=document.createElement('script'); \n");
        sb.Append("function loadTooltip() { \n");
        sb.Append("if (typeof jQuery == 'undefined') { setTimeout('loadTooltip()',200); return;} \n");
        sb.Append("if (typeof jQuery.fn.tooltip == 'undefined') { \n");
        sb.Append("jQueryTooltips.setAttribute('type','text/javascript'); \n");
        sb.Append("jQueryTooltips.setAttribute('src', '" + ResolveUrl("~/CMSScripts/jquery/jquery-tooltips.js") + "'); \n");
        sb.Append("setTimeout('document.body.appendChild(jQueryTooltips)',100); \n");
        sb.Append(" \n}");
        sb.Append(" \n}");

        sb.Append("function hover(imgID, width, height, sizeInUrl) { \n");

        // Timer for loading jQuery or tooltip plugin
        sb.Append(" if ((typeof jQuery == 'undefined')||(typeof jQuery.fn.tooltip == 'undefined')) {\n");
        sb.Append(" setTimeout(\"loadTooltip();hover('\"+imgID+\"',\"+width+\",\"+height+\",\"+sizeInUrl+\")\",100); return;\n");
        sb.Append(" }\n");

        // Tooltip definition
        sb.Append(" $j('img[id=' + imgID + ']').tooltip({ \n");
        sb.Append("     delay: 0, \n");
        sb.Append("     track: true, \n");
        sb.Append("     showBody: \" - \", \n");
        sb.Append("     showBody: \" - \", \n");
        sb.Append("     extraClass: \"ImageExtraClass\", \n");
        sb.Append("     showURL: false, \n");

        if (IsLiveSite)
        {
            if (CultureHelper.IsPreferredCultureRTL())
            {
                sb.Append("     positionLeft: true, \n");
                sb.Append("     left: -15, \n");
            }
        }
        else
        {
            if (CultureHelper.IsUICultureRTL())
            {
                sb.Append("     positionLeft: true, \n");
                sb.Append("     left: -15, \n");
            }
        }

        sb.Append("     bodyHandler: function() { \n");
        sb.Append("         var hidden = $j(\"#\" + this.id + \"_src\"); \n");
        sb.Append("         var source = this.src; \n");
        sb.Append("         if (hidden[0] != null) { \n");
        sb.Append("             source = hidden[0].value; \n");
        sb.Append("         } \n");
        sb.Append("         var hoverDiv = $j(\"<div/>\"); \n");
        sb.Append("         var hoverImg = $j(\"<img/>\").attr(\"class\", \"ImageTooltip\").attr(\"src\", source); \n");
        sb.Append("         hoverImg.css({'width' : width, 'height' : height}); \n");
        sb.Append("         hoverDiv.append(hoverImg); \n");
        sb.Append("         return hoverDiv;\n");
        sb.Append("     } \n");
        sb.Append(" }); \n");
        sb.Append("} \n");

        return sb.ToString();
    }

    #endregion
}
