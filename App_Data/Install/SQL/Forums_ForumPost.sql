CREATE TABLE [Forums_ForumPost] (
		[PostId]                                 int NOT NULL IDENTITY(1, 1),
		[PostForumID]                            int NOT NULL,
		[PostParentID]                           int NULL,
		[PostIDPath]                             nvarchar(450) NOT NULL,
		[PostLevel]                              int NOT NULL,
		[PostSubject]                            nvarchar(450) NOT NULL,
		[PostUserID]                             int NULL,
		[PostUserName]                           nvarchar(200) NOT NULL,
		[PostUserMail]                           nvarchar(100) NULL,
		[PostText]                               nvarchar(max) NULL,
		[PostTime]                               datetime NOT NULL,
		[PostApprovedByUserID]                   int NULL,
		[PostThreadPosts]                        int NULL,
		[PostThreadLastPostUserName]             nvarchar(200) NULL,
		[PostThreadLastPostTime]                 datetime NULL,
		[PostUserSignature]                      nvarchar(max) NULL,
		[PostGUID]                               uniqueidentifier NOT NULL,
		[PostLastModified]                       datetime NOT NULL,
		[PostApproved]                           bit NULL,
		[PostIsLocked]                           bit NULL,
		[PostIsAnswer]                           int NULL,
		[PostStickOrder]                         int NOT NULL,
		[PostViews]                              int NULL,
		[PostLastEdit]                           datetime NULL,
		[PostInfo]                               nvarchar(max) NULL,
		[PostAttachmentCount]                    int NULL,
		[PostType]                               int NULL,
		[PostThreadPostsAbsolute]                int NULL,
		[PostThreadLastPostUserNameAbsolute]     nvarchar(200) NULL,
		[PostThreadLastPostTimeAbsolute]         datetime NULL,
		[PostQuestionSolved]                     bit NULL,
		[PostIsNotAnswer]                        int NULL
)
ON [PRIMARY]
ALTER TABLE [Forums_ForumPost]
	ADD
	CONSTRAINT [PK_Forums_ForumPost]
	PRIMARY KEY
	NONCLUSTERED
	([PostId])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Forums_ForumPost]
	ADD
	CONSTRAINT [DEFAULT_Forums_ForumPost_PostAttachmentCount]
	DEFAULT ((0)) FOR [PostAttachmentCount]
ALTER TABLE [Forums_ForumPost]
	ADD
	CONSTRAINT [DEFAULT_Forums_ForumPost_PostIsLocked]
	DEFAULT ((0)) FOR [PostIsLocked]
ALTER TABLE [Forums_ForumPost]
	ADD
	CONSTRAINT [DEFAULT_Forums_ForumPost_PostQuestionSolved]
	DEFAULT ((0)) FOR [PostQuestionSolved]
ALTER TABLE [Forums_ForumPost]
	ADD
	CONSTRAINT [DEFAULT_Forums_ForumPost_PostUserName]
	DEFAULT ('') FOR [PostUserName]
IF OBJECT_ID(N'[Forums_ForumPost]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumPost_PostApprovedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumPost]
			ADD CONSTRAINT [FK_Forums_ForumPost_PostApprovedByUserID_CMS_User]
			FOREIGN KEY ([PostApprovedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Forums_ForumPost]') IS NOT NULL
	AND OBJECT_ID(N'[Forums_Forum]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumPost_PostForumID_Forums_Forum]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumPost]
			ADD CONSTRAINT [FK_Forums_ForumPost_PostForumID_Forums_Forum]
			FOREIGN KEY ([PostForumID]) REFERENCES [Forums_Forum] ([ForumID])
END
IF OBJECT_ID(N'[Forums_ForumPost]') IS NOT NULL
	AND OBJECT_ID(N'[Forums_ForumPost]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumPost_PostParentID_Forums_ForumPost]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumPost]
			ADD CONSTRAINT [FK_Forums_ForumPost_PostParentID_Forums_ForumPost]
			FOREIGN KEY ([PostParentID]) REFERENCES [Forums_ForumPost] ([PostId])
END
IF OBJECT_ID(N'[Forums_ForumPost]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumPost_PostUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumPost]
			ADD CONSTRAINT [FK_Forums_ForumPost_PostUserID_CMS_User]
			FOREIGN KEY ([PostUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_Forums_ForumPost_PostApproved]
	ON [Forums_ForumPost] ([PostApproved])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Forums_ForumPost_PostApprovedByUserID]
	ON [Forums_ForumPost] ([PostApprovedByUserID])
	ON [PRIMARY]
CREATE INDEX [IX_Forums_ForumPost_PostForumID]
	ON [Forums_ForumPost] ([PostForumID])
	ON [PRIMARY]
CREATE UNIQUE CLUSTERED INDEX [IX_Forums_ForumPost_PostIDPath]
	ON [Forums_ForumPost] ([PostIDPath])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Forums_ForumPost_PostLevel]
	ON [Forums_ForumPost] ([PostLevel])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Forums_ForumPost_PostParentID]
	ON [Forums_ForumPost] ([PostParentID])
	ON [PRIMARY]
CREATE INDEX [IX_Forums_ForumPost_PostUserID]
	ON [Forums_ForumPost] ([PostUserID])
	ON [PRIMARY]
