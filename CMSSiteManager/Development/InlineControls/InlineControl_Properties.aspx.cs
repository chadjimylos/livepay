using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.FormEngine;

public partial class CMSSiteManager_Development_InlineControls_InlineControl_Properties : SiteManagerPage
{
    int inlineControlID = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get inline control  ID
        this.inlineControlID = ValidationHelper.GetInteger(Request.QueryString["inlinecontrolID"], 0);
        this.FieldEditor.Visible = false;

        if (this.inlineControlID > 0)
        {
            // Get inline control info
            InlineControlInfo inlineControlObj = InlineControlInfoProvider.GetInlineControlInfo(inlineControlID);

            // Check if info object exists
            if (inlineControlObj != null)
            {
                // Initialize FieldEditor
                this.FieldEditor.OnAfterDefinitionUpdate += new OnAfterDefinitionUpdateEventHandler(editor_OnAfterDefinitionUpdate);
                this.FieldEditor.Mode = FieldEditorModeEnum.General;
                this.FieldEditor.DisplayedControls = FieldEditorControlsEnum.Controls;
                this.FieldEditor.FormDefinition = inlineControlObj.ControlProperties;
                this.FieldEditor.Visible = true;
            }
        }        
    }
    

    /// <summary>
    /// OnAfterDefinitionUpdate handler.
    /// </summary>
    void editor_OnAfterDefinitionUpdate()
    {
        InlineControlInfo inlineControlObj = InlineControlInfoProvider.GetInlineControlInfo(inlineControlID);
        if (inlineControlObj != null)
        {
            // Get new parameters changed by fieldeditor
            inlineControlObj.ControlProperties = this.FieldEditor.FormDefinition;

            // Update parameters in database
            InlineControlInfoProvider.SetInlineControlInfo(inlineControlObj);
        }
    }
}
