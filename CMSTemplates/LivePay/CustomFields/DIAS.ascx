<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DIAS.ascx.cs" Inherits="CMSFormControls_DIAS" %>
<script type="text/javascript">

    var ObjDias = {
        maxLength: 20,
        //typePayment:0,
        organismCode: 000,
        value:'0000000000000000000'
    };


    function checkDIAS(sender, args) {

        var dias = args.Value;
        var bIsValid = false;

        CMSObjDias.value = dias;
        padWithZero(CMSObjDias);

        $.extend(true, ObjDias, CMSObjDias);

        var PedeTeleftea = Right(ObjDias.value, 5);
        var organismCode = Left(PedeTeleftea, 3);
        //var typePayment = PedeTeleftea.substr(3, 1);
        var controlDigit = PedeTeleftea.substr(4, 1);

        //if (organismCode == ObjDias.organismCode && typePayment == ObjDias.typePayment) {
        if (organismCode == ObjDias.organismCode) {
                bIsValid = true;
        }

            var cd = 0; //Control Digit

        if (bIsValid) {
            cd = modulus11(ObjDias.value, ObjDias.maxLength);
            if (controlDigit == cd) {
                bIsValid = true;
            } else { bIsValid = false; }
        }

        var IsCutomValid = sender.getAttribute('IsCustomValidator');
        var CustomErrorMess = sender.getAttribute('CustomValidatorErrorMessage');
        if (IsCutomValid != null) {
            if (IsCutomValid.indexOf('CustomCloud_') != -1) {
                if (!bIsValid) {
                    Tab1_ValidationCloud('#' + IsCutomValid, CustomErrorMess, true, '')
                } else {
                    Tab1_ValidationCloud('#' + IsCutomValid, '', false, '')
                }
            }

        }
        args.IsValid = bIsValid;
    }
</script>

<div runat="server" id="CustomCloud_div"></div>

<asp:TextBox runat="server" cssclass="RegUsersTab1CustomTxt" width="260px" onkeyup="padWithZero(this)" ID="DIAS" />


<div class="HideIt">
    <asp:CustomValidator ID="cv_DIAS" 
                        ClientValidationFunction="checkDIAS" runat="server"   
                        ControlToValidate="DIAS" ValidationGroup="Tab1_PayForm" />
</div>