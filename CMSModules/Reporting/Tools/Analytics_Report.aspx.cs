using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.FormEngine;
using CMS.WebAnalytics;
using CMS.Reporting;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.SiteProvider;

public partial class CMSModules_Reporting_Tools_Analytics_Report : CMSWebAnalyticsPage
{
    private bool isSaved = false;

    public string mSave = null;
    public string mPrint = null;
    public string mDeleteData = null;
    public string reportName = "";

    const int YEAR_REP = 0;
    const int MONTH_REP = 1;
    const int WEEK_REP = 2;
    const int DAY_REP = 3;
    const int HOUR_REP = 4;


    /// <summary>
    /// Extracts report code name from report code names list.
    /// </summary>
    /// <param name="rep">Requested report (0 - year, 1 - month, 2 - week, 3 - day, 4 - hour)</param>
    /// <param name="reportCodeName">Report code name list (delimited by semicolons)</param>
    private string GetReportCodeName(int rep, string reportCodeName)
    {
        string[] reports = reportCodeName.Split(';');
        if ((reports.GetLength(0) < 5) || (rep < 0) || (rep > 4))
        {
            return "";
        }

        return reports[rep];
    }


    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Check the license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), "") != "")
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Reporting);
        }

        // Check site availability
        if (!ResourceSiteInfoProvider.IsResourceOnSite("CMS.Reporting", CMSContext.CurrentSiteName))
        {
            RedirectToResourceNotAvailableOnSite("CMS.Reporting");
        }

        CurrentUserInfo user = CMSContext.CurrentUser;

        // Check 'Read' permission
        if (!user.IsAuthorizedPerResource("CMS.Reporting", "Read"))
        {
            RedirectToAccessDenied("CMS.Reporting", "Read");
        }

        int year = QueryHelper.GetInteger("year", 0);
        int month = QueryHelper.GetInteger("month", 0);
        int week = QueryHelper.GetInteger("week", 0);
        int day = QueryHelper.GetInteger("day", 0);

        DateTime fromDate = DateTime.MinValue;
        DateTime toDate = DateTime.MinValue;

        string statCodeName = QueryHelper.GetString("statCodeName", String.Empty);
        string dataCodeName = QueryHelper.GetString("dataCodeName", String.Empty);
        string reportCodeName = QueryHelper.GetString("reportCodeName", String.Empty);

        if (year > 0)
        {
            if (week > 0)
            {
                // Week report
                reportName = GetReportCodeName(DAY_REP, reportCodeName);
                fromDate = DateTimeHelper.GetWeekStart(new DateTime(year, month, day), AnalyticsHelper.AnalyticsCulture);
                toDate = fromDate.AddDays(7);
            }
            else if (month > 0)
            {
                if (day > 0)
                {
                    // Day report
                    reportName = GetReportCodeName(HOUR_REP, reportCodeName);
                    fromDate = new DateTime(year, month, day, 0, 0, 0);
                    toDate = fromDate.AddDays(1);
                }
                else
                {
                    // Month report
                    reportName = GetReportCodeName(DAY_REP, reportCodeName);
                    fromDate = new DateTime(year, month, 1);
                    toDate = fromDate.AddMonths(1);
                }
            }
            else
            {
                // Year report
                reportName = GetReportCodeName(MONTH_REP, reportCodeName);
                fromDate = new DateTime(year, 1, 1);
                toDate = fromDate.AddYears(1);
            }

            DataTable dtp = new DataTable();

            dtp.Columns.Add("FromDate", typeof(DateTime));
            dtp.Columns.Add("ToDate", typeof(DateTime));
            dtp.Columns.Add("CodeName", typeof(string));
            dtp.Columns.Add("FromDayFirst", typeof(int));
            dtp.Columns.Add("ToDayFirst", typeof(int));
            dtp.Columns.Add("ToDaySecond", typeof(int));
            dtp.Columns.Add("FirstCategory", typeof(string));
            dtp.Columns.Add("SecondCategory", typeof(string));

            int fromDayFirst = fromDate.Day;
            int toDayFirst = toDate.AddDays(-1).Day;
            int toDaySecond = 0;
            if (fromDayFirst > toDayFirst)
            {
                toDaySecond = toDayFirst;
                toDayFirst = DateTime.DaysInMonth(fromDate.Year, fromDate.Month);
            }

            object[] parameters = new object[8];

            parameters[0] = fromDate;
            parameters[1] = toDate;
            parameters[2] = dataCodeName;
            parameters[3] = fromDayFirst;
            parameters[4] = toDayFirst;
            parameters[5] = toDaySecond;
            parameters[6] = HitLogProvider.VISITORS_FIRST;
            parameters[7] = HitLogProvider.VISITORS_RETURNING;

            dtp.Rows.Add(parameters);
            dtp.AcceptChanges();
            ucDisplayReport.ReportName = reportName;

            if (ucDisplayReport.ReportInfo == null)
            {
                lblError.Visible = true;
                lblError.Text = String.Format(ResHelper.GetString("Analytics_Report.ReportDoesnotExist"), reportName);
            }
            else
            {
                ucDisplayReport.LoadFormParameters = false;
                ucDisplayReport.DisplayFilter = false;
                ucDisplayReport.ReportParameters = dtp.Rows[0];
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), "") != "")
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.WebAnalytics);
        }

        // Check 'Read' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.WebAnalytics", "Read"))
        {
            RedirectToCMSDeskAccessDenied("CMS.WebAnalytics", "Read");
        }

        mSave = ResHelper.GetString("general.save");
        mPrint = ResHelper.GetString("Analytics_Report.Print");
        mDeleteData = ResHelper.GetString("Analytics_Report.ManageData");

        imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
        imgPrint.ImageUrl = GetImageUrl("General/print.png");
        imgManageData.ImageUrl = GetImageUrl("CMSModules/CMS_Reporting/managedata.png");


        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY,
            ScriptHelper.DialogScript);

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string),
            "myModalDialog",
            ScriptHelper.GetScript("function myModalDialog(url, name, width, height) { " +
            "win = window; " +
            "var dHeight = height; var dWidth = width; " +
            "if (( document.all )&&(navigator.appName != 'Opera')) { " +
            "try { win = wopener.window; } catch (e) {} " +
            "if ( parseInt(navigator.appVersion.substr(22, 1)) < 7 ) { dWidth += 4; dHeight += 58; }; " +
            "dialog = win.showModalDialog(url, this, 'dialogWidth:' + dWidth + 'px;dialogHeight:' + dHeight + 'px;resizable:yes;scroll:yes'); " +
            "} else { " +
            "oWindow = win.open(url, name, 'height=' + dHeight + ',width=' + dWidth + ',toolbar=no,directories=no,menubar=no,modal=yes,dependent=yes,resizable=yes,scroll=yes,scrollbars=yes'); oWindow.opener = this; oWindow.focus(); } } "));

        this.lnkPrint.OnClientClick = "myModalDialog('Analytics_Print.aspx?reportname=" + reportName + "&parameters=" + HttpUtility.HtmlEncode(GetQueryStringParameters()) + "&UILang=" + System.Globalization.CultureInfo.CurrentUICulture.IetfLanguageTag + "', 'Print report" + ucDisplayReport.ReportName + "', 800, 700); return false;";

        string statCodeName = QueryHelper.GetString("statCodeName", String.Empty);

        // Check 'ManageData' permission
        if (CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.WebAnalytics", "ManageData"))
        {
            this.lnkDeleteData.OnClientClick = "modalDialog('" +
            ResolveUrl("~\\CMSModules\\WebAnalytics\\Tools\\Analytics_ManageData.aspx") +
            "?statcodename=" + statCodeName + "', 'AnalyticsManageData', 450, 250); return false; ";
            this.lnkDeleteData.Visible = true;
        }
    }


    /// <summary>
    /// VerifyRenderingInServerForm
    /// </summary>
    /// <param name="control">Control</param>
    public override void VerifyRenderingInServerForm(Control control)
    {
        if (!isSaved)
        {
            base.VerifyRenderingInServerForm(control);
        }
    }


    /// <summary>
    /// Handles lnkSave click event.
    /// </summary>
    protected void lnkSave_Click(object sender, EventArgs e)
    {
        // Check web analytics save permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.WebAnalytics", "SaveReports"))
        {
            RedirectToCMSDeskAccessDenied("CMS.WebAnalytics", "SaveReports");
        }

        isSaved = true;

        if (ucDisplayReport.SaveReport() > 0)
        {
            lblInfo.Visible = true;
            lblInfo.Text = String.Format(ResHelper.GetString("Analytics_Report.ReportSavedTo"), ucDisplayReport.ReportInfo.ReportDisplayName + " - " + DateTime.Now.ToString());
        }

        isSaved = false;
    }


    /// <summary>
    /// Generates query string parameters.
    /// </summary>
    protected string GetQueryStringParameters()
    {
        string result = "";
        IFormatProvider culture = DateTimeHelper.DefaultIFormatProvider;

        if (this.ucDisplayReport.ReportParameters != null)
        {
            // Build the results array
            foreach (DataColumn col in this.ucDisplayReport.ReportParameters.Table.Columns)
            {
                if ((col.DataType.Name.ToLower() == "datetime") && ((ValidationHelper.GetDateTime(this.ucDisplayReport.ReportParameters[col.ColumnName], DataHelper.DATETIME_NOT_SELECTED)) != DataHelper.DATETIME_NOT_SELECTED))
                {
                    result += col.ColumnName + ";" + ((DateTime)this.ucDisplayReport.ReportParameters[col.ColumnName]).ToString(culture) + ";";
                }
                else
                {
                    result += col.ColumnName + ";" + Convert.ToString(this.ucDisplayReport.ReportParameters[col.ColumnName]) + ";";
                }
            }

            // Removes last semicolon (we can't use trimend because of situation when last parametr doesn't have value)
            if (result.EndsWith(";"))
            {
                result = result.Substring(0, result.Length - 1);
            }
        }

        return result;
    }
}
