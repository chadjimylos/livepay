﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="xmlHttpMerchants.aspx.vb" Inherits="CMSTemplates_LivePay_xmlHttpMerchants" %>
<%@ OutputCache Location="None"  %>

<asp:Repeater runat="server" id="rptSearchSec">
<ItemTemplate>
    <div class="QuickSrcResTitle">
	    <div style="position:absolute;margin-left:0px;margin-left:expression(-10)">»</div> <a style="margin-left:10px;display:block" href="<%# Eval("nodealiasPath") & ".aspx"%>" class="Chand" ><%# Eval("DiscreetTitle") %></a>
    </div>
    <div class="SuggestBorderLine"></div>
</ItemTemplate>
</asp:Repeater>


<asp:Repeater runat="server" id="rptSearchSec_Form" visible="false">
<ItemTemplate>
    <div class="QuickSrcResTitle" >
       »  <a onclick="SetSuggestValue('<%#Eval("DiscreetTitle").ToString().Replace("'","\'")%>','<%# Eval("LivePayID") %>')" class="Chand"><%# Eval("DiscreetTitle") %></a>
    </div>
    <div class="SuggestBorderLine"></div>
</ItemTemplate>
</asp:Repeater>
