<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectQuery.ascx.cs" Inherits="CMSFormControls_Classes_SelectQuery" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>
<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" FilterControl="~/CMSFormControls/Filters/DocTypeFilter.ascx"
            DisplayNameFormat="{%QueryName%}" AllowEditTextBox="true" ReturnColumnName="QueryName"
            ObjectType="cms.query" ResourcePrefix="queryselector" SelectionMode="SingleTextBox" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
