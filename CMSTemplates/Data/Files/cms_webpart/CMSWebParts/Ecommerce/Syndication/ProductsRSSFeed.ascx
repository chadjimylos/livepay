﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Ecommerce/Syndication/ProductsRSSFeed.ascx.cs" Inherits="CMSWebParts_Ecommerce_Syndication_ProductsRSSFeed" %>
<%@ Register TagPrefix="cms" Namespace="CMS.Ecommerce" Assembly="CMS.Ecommerce" %>
<cms:ProductsDataSource runat="server" ID="srcProducts" />
<cms:RSSFeed ID="rssFeed" runat="server" />