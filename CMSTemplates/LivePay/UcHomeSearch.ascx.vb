﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.SettingsProvider
Imports CMS.SiteProvider
Imports CMS.DataEngine

Partial Class CMSTemplates_LivePay_UcHomeSearch
    Inherits CMSUserControl
    Public searchText As String

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        btnSearch.ImageUrl = ResHelper.LocalizeString("{$=/app_themes/LivePay/btnSearchOk.gif|en-us=/app_themes/LivePay/btnSearchOk_en-us.png$}")
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        searchText = ResHelper.LocalizeString("{$=Πληκτρολογήστε το Όνομα Πληρωμής|en-us=Enter the Payment Name$}")
        Me.txtSearch.Text = searchText
        Me.txtSearch.Attributes.Add("onfocus", "if(this.value=='" & searchText & "'){this.value='';}")
        Me.txtSearch.Attributes.Add("onBlur", "if (this.value == '') {this.value = '" & searchText & "';}")
        GetFont()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click

        If txtSearch.Text = ResHelper.LocalizeString("{$=Πληκτρολογήστε το Όνομα Πληρωμής|en-us=Enter the Payment Name$}") Then
            txtSearch.Text = String.Empty
        End If
        Dim key As String = StripHTMLFunctions.StripTags(txtSearch.Text)
        If key.Length > 0 Then
            key = Replace(key, "'", String.Empty)
            key = key.Replace("""", String.Empty)
            key = key.Replace("<", "&lt;")
            key = key.Replace(">", "&gt;")
            key = Server.UrlEncode(key)
            If key <> String.Empty Then
                Response.Redirect(String.Concat("~/Search.aspx?searchtext=", key, ""))
            End If
        End If
    End Sub

    Private Sub GetFont()
    End Sub
End Class
