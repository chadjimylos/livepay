using System;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_UICultures_UICulture_New : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        rfvUICultureCode.ErrorMessage = ResHelper.GetString("UICultures_New.EmptyCultureCode");
        rfvUICultureName.ErrorMessage = ResHelper.GetString("UICultures_New.EmptyCultureName");

        InitializeMasterPage();
    }


    /// <summary>
    /// Initializes the master page elements.
    /// </summary>
    private void InitializeMasterPage()
    {
        string uiCultures = ResHelper.GetString("UICultures.UICultures");
        string currentUICulture = ResHelper.GetString("UICultures.NewUICulture");
        const string UICulturesURL = "~/CMSSiteManager/Development/UICultures/UICulture_List.aspx";

        // Initializes page breadcrumbs
        string[,] tabs = new string[2, 3];
        tabs[0, 0] = uiCultures;
        tabs[0, 1] = UICulturesURL;
        tabs[0, 2] = "";
        tabs[1, 0] = currentUICulture;
        tabs[1, 1] = "";
        tabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = tabs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("UICultures.NewUICulture");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_UICulture/new.png");

        // Set help icon
        this.CurrentMaster.Title.HelpTopicName = "new_culturegeneral_tab";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Find whether required fields are not empty
        string result = new Validator().NotEmpty(txtUICultureCode.Text, ResHelper.GetString("UICultures_New.EmptyCultureCode")).NotEmpty(txtUICultureName.Text, ResHelper.GetString("UICultures_New.EmptyCultureName")).Result;

        try
        {
            System.Globalization.CultureInfo objekt = new System.Globalization.CultureInfo(txtUICultureCode.Text);
        }
        catch
        {
            result = ResHelper.GetString("UICulture.ErrorNoGlobalCulture");
        }

        if (result == "")
        {
            try
            {
                UICultureInfo ui = UICultureInfoProvider.GetUICultureInfo(txtUICultureCode.Text);
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("UICulture.UICultureAlreadyExist");
            }
            catch 
            {
                int uiCultureId = SaveNewUICulture();

                if (uiCultureId > 0)
                {
                    UrlHelper.Redirect("UICulture_Frameset.aspx?uicultureid=" + uiCultureId + "&update=1");
                }            
            }            
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }


    protected int SaveNewUICulture()
    {
        UICultureInfo ui = new UICultureInfo();
        ui.UICultureCode = txtUICultureCode.Text;
        ui.UICultureName = txtUICultureName.Text;
        UICultureInfoProvider.SetUICultureInfo(ui);
        return ui.UICultureID;
    }
}
