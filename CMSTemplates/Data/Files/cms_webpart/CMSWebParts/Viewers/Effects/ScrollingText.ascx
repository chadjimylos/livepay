<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Viewers/Effects/ScrollingText.ascx.cs"
    Inherits="CMSWebParts_Viewers_Effects_ScrollingText" %>
<asp:Label ID="lblNoData" runat="server" Visible="false" />
<asp:Literal runat="server" ID="ltlBefore" EnableViewState="false" />
<cms:CMSRepeater ID="repItems" runat="server" EnableViewState="true" OnItemDataBound="repItems_ItemDataBound" />
<asp:Literal runat="server" ID="ltlAfter" EnableViewState="false" />
