<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShoppingCartPaymentShipping.ascx.cs"
    Inherits="CMSModules_Ecommerce_Controls_ShoppingCart_ShoppingCartPaymentShipping" %>
<div class="BlockContent">
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label ID="lblTitle" runat="server" CssClass="BlockTitle" EnableViewState="false" />
    <table cellspacing="0" cellpadding="10" border="0">
        <asp:PlaceHolder ID="plcShipping" runat="server">
            <tr>
                <td colspan="2">
                    <cms:LocalizedLabel ID="lblFreeShippingInfo" runat="server" ResourceString="ecommerce.order.freeshipping"
                        Visible="false" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" style="width: 100px">
                    <asp:Label ID="lblShipping" runat="server" CssClass="ContentLabel" EnableViewState="false" />
                </td>
                <td>
                    <cms:LocalizedDropDownList ID="drpShipping" runat="server" CssClass="DropDownField"
                        AutoPostBack="true" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="plcPayment" runat="server">
            <tr>
                <td class="FieldLabel" style="width: 100px">
                    <asp:Label ID="lblPayment" runat="server" CssClass="ContentLabel" EnableViewState="false" />
                </td>
                <td>
                    <cms:LocalizedDropDownList ID="drpPayment" runat="server" CssClass="DropDownField" />
                </td>
            </tr>
        </asp:PlaceHolder>
    </table>
    <%--    <asp:Panel id="PaymentGatewayCustomData" runat="server" CssClass="PaymentGatewayDataContainer" />--%>
</div>
