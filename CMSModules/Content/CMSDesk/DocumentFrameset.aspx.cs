using System;

using CMS.TreeEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_DocumentFrameset : CMSContentPage
{
    #region "Private & protected variables"

    protected string viewpage = "~/CMSModules/Content/CMSDesk/blank.htm";
    private int nodeId = 0;
    private TreeProvider mTree = null;
    private TreeNode mTreeNode = null;

    #endregion


    #region "Private properties"

    private TreeProvider Tree
    {
        get
        {
            if (mTree == null)
            {
                mTree = new TreeProvider(CMSContext.CurrentUser);
            }
            return mTree;
        }
    }


    private TreeNode TreeNode
    {
        get
        {
            return mTreeNode;
        }
        set
        {
            mTreeNode = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        int classId = 0;
        bool checkCulture = false;
        nodeId = QueryHelper.GetInteger("nodeid", 0);

        switch (QueryHelper.GetString("action", "edit").ToLower())
        {
            // New dialog / new page form
            case "new":
                classId = QueryHelper.GetInteger("classid", 0);
                if (classId <= 0)
                {
                    // Get by class name if specified
                    string className = QueryHelper.GetString("classname", string.Empty);
                    if (className != string.Empty)
                    {
                        DataClassInfo ci = DataClassInfoProvider.GetDataClass(className);
                        if (ci != null)
                        {
                            classId = ci.ClassID;
                        }
                    }
                }

                if (classId > 0)
                {
                    viewpage = "Edit/EditFrameset.aspx";

                    // Check if document type is allowed under parent node
                    if (nodeId > 0)
                    {
                        // Get the node
                        TreeNode = Tree.SelectSingleNode(nodeId, TreeProvider.ALL_CULTURES);
                        if (TreeNode != null)
                        {
                            if (!DataClassInfoProvider.IsChildClassAllowed(ValidationHelper.GetInteger(TreeNode.GetValue("NodeClassID"), 0), classId))
                            {
                                viewpage = "NotAllowed.aspx?action=child";
                            }
                        }
                    }
                }
                else
                {
                    viewpage = "New/new.aspx";
                }
                break;

            case "delete":
                // Delete dialog
                viewpage = "Delete.aspx";
                break;

            case "preview":
                // Preview mode
                viewpage = "View/preview.aspx";

                TreeNode = Tree.SelectSingleNode(nodeId, TreeProvider.ALL_CULTURES);
                if (TreeNode != null)
                {
                    classId = ValidationHelper.GetInteger(TreeNode.GetValue("NodeClassID"), 0);

                    DataClassInfo ci = DataClassInfoProvider.GetDataClass(classId);
                    if (ci != null)
                    {
                        if (!string.IsNullOrEmpty(ci.ClassPreviewPageUrl))
                        {
                            viewpage = ci.ClassPreviewPageUrl;
                        }
                    }
                }
                break;

            case "listing":
                // Listing mode
                viewpage = "View/listing.aspx";

                TreeNode = Tree.SelectSingleNode(nodeId, TreeProvider.ALL_CULTURES);
                if (TreeNode != null)
                {
                    classId = ValidationHelper.GetInteger(TreeNode.GetValue("NodeClassID"), 0);

                    DataClassInfo ci = DataClassInfoProvider.GetDataClass(classId);
                    if (ci != null)
                    {
                        if (!string.IsNullOrEmpty(ci.ClassListPageURL))
                        {
                            viewpage = ci.ClassListPageURL;
                        }
                    }
                }
                break;

            case "livesite":
                // Live site mode
                viewpage = "View/livesite.aspx";
                break;

            case "newculture":
                // New document culture
                viewpage = "Edit/EditFrameset.aspx";
                break;

            default:
                // Edit mode
                viewpage = "Edit/EditFrameset.aspx";
                checkCulture = true;
                break;
        }

        // If culture version should be checked, check
        if (checkCulture)
        {
            // Check (and ensure) the proper content culture
            CheckPreferredCulture(true);

            // Get the node
            Tree.CombineWithDefaultCulture = false;
            TreeNode = Tree.SelectSingleNode(nodeId);
            if (TreeNode == null)
            {
                // Document does not exist -> redirect to new culture version creation dialog
                viewpage = "Edit/NewCultureVersion.aspx";
            }
        }

        // Apply the additional transformations to the view page URL
        viewpage = UrlHelper.AppendQuery(viewpage, Request.Url.Query);
        viewpage = ResolveUrl(viewpage);
    }

    #endregion


    #region "Overidden methods"

    /// <summary>
    /// Adds the script to the output request window
    /// </summary>
    /// <param name="script">Script to add</param>
    public override void AddScript(string script)
    {
        ltlScript.Text += ScriptHelper.GetScript(script);
    }

    #endregion
}
