using System;

using CMS.GlobalHelper;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_Edit : CMSNewsletterPage
{
    protected int issueId = 0;
    protected Issue issue = null;
    protected string mSave = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        chkShowInArchive.Text = ResHelper.GetString("NewsletterTemplate_Edit.ShowInArchive");

        mSave = ResHelper.GetString("general.save");

        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Newsletter", "AuthorIssues"))
        {
            // Disable Save button if user is not authorized
            lnkSave.Enabled = false;
            imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/savedisabled.png");
        }
        else
        {
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditShortcuts",
                "<script type=\"text/javascript\" src=\"" + ResolveUrl("~/CMSScripts/shortcuts.js") + "\"></script>"
            );

            imgSave.ImageUrl = GetImageUrl("CMSModules/CMS_Content/EditMenu/save.png");
            lnkSave.Attributes.Add("onclick", "SaveDocument(); return false;");

            lblInfo.Text = ResHelper.GetString("general.changessaved");
            lblInfo.Style.Add("display", "none");
            lblError.Text = ResHelper.GetString("NewsletterContentEditor.SubjectRequired");
            lblError.Style.Add("display", "none");

            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "MsgInfo", ScriptHelper.GetScript(
                "function MsgInfo(err) { \n" +
                "    if (err == 0) { \n" +
                "         document.getElementById('" + lblError.ClientID + "').style.display = \"none\"; \n" +
                "         document.getElementById('" + lblInfo.ClientID + "').style.display = \"block\"; \n" +
                "    } \n" +
                "    if (err == 1) { \n" +
                "         document.getElementById('" + lblInfo.ClientID + "').style.display = \"none\"; \n" +
                "         document.getElementById('" + lblError.ClientID + "').style.display = \"block\"; \n" +
                "    } \n" +
                "} \n"));

            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "GetSubject",
                ScriptHelper.GetScript(
                "function SaveDocument() \n" +
                "{ \n" +
                "    ClearToolbar(); \n" +
                "    window.frames['iframeContent'].SaveContent(false, document.getElementById('" + txtSubject.ClientID + "').value, " +
                "                                                      document.getElementById('" + chkShowInArchive.ClientID + "').checked ); \n" +
                "    document.getElementById('" + lblInfo.ClientID + "').style.display = \"block\"; \n" +
                "    RefreshPage(); \n" +
                "} \n"));
        }

        // Get issue ID from query string
        issueId = QueryHelper.GetInteger("issueid", 0);
        // Get edited issue object
        issue = IssueProvider.GetIssue(issueId);
        if (issue == null)
        {
            return;
        }

        // Check whether issue has been sent
        bool isSent = (issue.IssueMailoutTime == DateTimeHelper.ZERO_TIME ? false : (issue.IssueMailoutTime < DateTime.Now));
        if (isSent)
        {
            lblSent.Text = ResHelper.GetString("Newsletter_Issue_Header.AlreadySent");
        }
        else
        {
            lblSent.Text = ResHelper.GetString("Newsletter_Issue_Header.NotSentYet");
        }

        // Get newsletter
        Newsletter news = NewsletterProvider.GetNewsletter(issue.IssueNewsletterID);
        if (news != null)
        {
            if (news.NewsletterType == "D")
            {
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("Newsletter_Issue_Edit.CannotBeEdited");
                contentHeader.Visible = false;
                contentBody.Visible = false;
                contentFooter.Visible = false;
                pnlMenu.Visible = false;
                return;
            }
            else
            {
                contentBody.IssueID = issueId;
                txtSubject.Text = issue.IssueSubject;
                chkShowInArchive.Checked = issue.IssueShowInNewsletterArchive;
            }
        }

        AttachmentTitle.TitleText = ResHelper.GetString("general.attachments");
        AttachmentList.ObjectID = issueId;
        AttachmentList.SiteID = CMSContext.CurrentSite.SiteID;
        AttachmentList.AllowPasteAttachments = true;
        AttachmentList.ObjectType = NewsletterObjectType.NEWSLETTERISSUE;
        AttachmentList.Category = MetaFileInfoProvider.OBJECT_CATEGORY_ISSUE;
        AttachmentList.AllowEdit = lnkSave.Enabled;

        // Save changes before uploading new attachment
        AttachmentList.UploadOnClickScript = "SaveDocument();";
        AttachmentList.OnAfterUpload += new EventHandler(AttachmentList_OnAfterUpload);
    }


    protected override void OnPreRender(EventArgs e)
    {
        lblInfo.Visible = (lblInfo.Text != String.Empty);
        lblError.Visible = (lblError.Text != String.Empty);
        base.OnPreRender(e);
    }


    void AttachmentList_OnAfterUpload(object sender, EventArgs e)
    {
        // Show info message after upload/delete
        lblInfo.Style.Remove("display");
        lblInfo.Style.Add("display", "block");
    }

}
