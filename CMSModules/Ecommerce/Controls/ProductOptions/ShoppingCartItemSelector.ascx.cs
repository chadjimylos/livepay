using System;
using System.Data;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

using CMS.Ecommerce;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.URLRewritingEngine;
using CMS.DataEngine;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Controls_ProductOptions_ShoppingCartItemSelector : CMSUserControl
{
    #region "Variables"

    private bool mSKUEnabled = true;
    private bool mDataLoaded = false;

    private string mAddToCartImageButton = "";
    private string mAddToCartLinkText = "";

    private string mAddToWishlistImageButton = "";
    private string mAddToWishlistLinkText = "";

    private string mImageFolder = "";
    private int mDefaultQuantity = 1;

    private string mShoppingCartUrl = "";
    private string mWishlistUrl = "";

    private bool mShowUnitsTextBox = false;
    private bool mShowProductOptions = false;
    private bool mShowWishlistLink = false;
    private bool mShowPriceIncludingTax = false;
    private bool mShowTotalPrice = false;
    private bool mDialogMode = false;

    private ShoppingCartInfo mLocalShoppingCartObj = null;
    private SKUInfo mProductInfo = null;

    private Hashtable mProductOptionPrice = new Hashtable();
    private Hashtable mProductOptionDepartment = new Hashtable();

    /// <summary>
    /// Fires when "Add to shopping cart" button is clicked, overrides original action
    /// </summary>
    public event EventHandler OnAddToShoppingCart;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Product ID (SKU ID).
    /// </summary>
    public int SKUID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["SKUId"], 0);
        }
        set
        {
            ViewState["SKUId"] = value;
        }
    }


    /// <summary>
    /// Indicates whether current product (SKU) is enabled, TRUE - button/link for adding product to the shopping cart is rendered, otherwise it is not rendered.
    /// </summary>
    public bool SKUEnabled
    {
        get
        {
            return mSKUEnabled;
        }
        set
        {
            mSKUEnabled = value;
            InitializeControls();
        }
    }


    /// <summary>
    /// File name of the image which is used as a source for image button to add product to the shopping cart, default image folder is '~/App_Themes/(Skin_Folder)/Images/ShoppingCart/'.
    public string AddToCartImageButton
    {
        get
        {
            return mAddToCartImageButton;
        }
        set
        {
            mAddToCartImageButton = value;
        }
    }


    /// <summary>
    /// Simple string or localizable string of the link to add product to the shopping cart.
    /// </summary>
    public string AddToCartLinkText
    {
        get
        {
            return mAddToCartLinkText;
        }
        set
        {
            mAddToCartLinkText = value;
        }
    }


    /// <summary>
    /// Indicates if textbox for entering number of units to add to the shopping cart should be displayed, if it is hidden number of units is equal to 1.
    /// </summary>
    public bool ShowUnitsTextBox
    {
        get
        {
            return mShowUnitsTextBox;
        }
        set
        {
            mShowUnitsTextBox = value;
        }
    }


    /// <summary>
    /// Indicates if product options of the current product should be displayed.
    /// </summary>
    public bool ShowProductOptions
    {
        get
        {
            return mShowProductOptions;
        }
        set
        {
            mShowProductOptions = value;
        }
    }


    /// <summary>
    /// Indicates if "Add to Wishlist" link should be displayed.
    /// </summary>
    public bool ShowWishlistLink
    {
        get
        {
            return mShowWishlistLink;
        }
        set
        {
            mShowWishlistLink = value;
        }
    }


    /// <summary>
    /// Default quantity when adding product to the shopping cart.
    /// </summary>
    public int DefaultQuantity
    {
        get
        {
            return mDefaultQuantity;
        }
        set
        {
            mDefaultQuantity = value;
        }
    }


    /// <summary>
    /// Show total price.
    /// </summary>
    public bool ShowTotalPrice
    {
        get
        {
            return mShowTotalPrice;
        }
        set
        {
            mShowTotalPrice = value;
        }
    }


    /// <summary>
    /// Quantity of the specified product to add to the shopping cart.
    /// </summary>
    public int Quantity
    {
        get
        {
            if (this.ShowUnitsTextBox)
            {
                return ValidationHelper.GetInteger(txtUnits.Text.Trim(), this.DefaultQuantity);
            }
            else
            {
                return this.DefaultQuantity;
            }
        }
    }


    /// <summary>
    /// Product options selected by inner selectors (string of SKUIDs separated byt the comma).
    /// </summary>
    public string ProductOptions
    {
        get
        {
            string options = "";

            // Get product options from selectors
            foreach (Control selector in pnlSelectors.Controls)
            {
                if (selector is ProductOptionSelector)
                {
                    options += ((ProductOptionSelector)selector).GetSelectedSKUOptions() + ",";
                }
            }
            return options.TrimEnd(',');
        }
    }


    /// <summary>
    /// Image folder, default image folder is '~/App_Themes/(Skin_Folder)/Images/ShoppingCart/'.
    /// </summary>
    public string ImageFolder
    {
        get
        {
            return mImageFolder;
        }
        set
        {
            mImageFolder = value;
        }
    }


    /// <summary>
    /// Shopping cart url. By default Shopping cart url from SiteManager settings is returned.
    /// </summary>
    public string ShoppingCartUrl
    {
        get
        {
            if (mShoppingCartUrl == "")
            {
                mShoppingCartUrl = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSShoppingCartURL");
            }
            return mShoppingCartUrl;
        }
        set
        {
            mShoppingCartUrl = value;
        }
    }


    /// <summary>
    /// Wishlist url. By default WIshlist url from SiteManager settings is returned.
    /// </summary>
    public string WishlistUrl
    {
        get
        {
            if (mWishlistUrl == "")
            {
                mWishlistUrl = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSWishlistURL");
            }
            return mWishlistUrl;
        }
        set
        {
            mWishlistUrl = value;
        }
    }


    /// <summary>
    /// File name of the image which is used as a source for image button to add product to the wishlist, default image folder is '~/App_Themes/(Skin_Folder)/Images/ShoppingCart/'.
    public string AddToWishlistImageButton
    {
        get
        {
            return mAddToWishlistImageButton;
        }
        set
        {
            mAddToWishlistImageButton = value;
        }
    }


    /// <summary>
    /// Simple string or localizable string of the link to add product to the wishlist.
    /// </summary>
    public string AddToWishlistLinkText
    {
        get
        {
            return mAddToWishlistLinkText;
        }
        set
        {
            mAddToWishlistLinkText = value;
        }
    }


    /// <summary>
    /// Shopping cart object required for product options price formatting. If it is not set current shopping cart from CMS context is used.
    /// </summary>
    public ShoppingCartInfo LocalShoppingCartObj
    {
        get
        {
            return mLocalShoppingCartObj;
        }
        set
        {
            mLocalShoppingCartObj = value;
        }
    }


    /// <summary>
    /// TRUE - product option price is displayed including tax, FALSE - product option price is displayed without tax
    /// </summary>
    public bool ShowPriceIncludingTax
    {
        get
        {
            return mShowPriceIncludingTax;
        }
        set
        {
            mShowPriceIncludingTax = value;
        }
    }


    public SKUInfo ProductInfo
    {
        get
        {
            if (mProductInfo == null)
            {
                this.mProductInfo = SKUInfoProvider.GetSKUInfo(this.SKUID);
            }
            return mProductInfo;
        }
    }


    /// <summary>
    /// Indicates if shopping car item selector is modal dialog.
    /// </summary>
    public bool DialogMode
    {
        get
        {
            return this.mDialogMode;
        }
        set
        {
            this.mDialogMode = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        ReloadData();
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (this.SKUID > 0)
        {
            InitializeControls();
            if (!mDataLoaded)
            {
                ReloadData();
            }

            // Get count of the product options            
            if ((this.ShowTotalPrice) &&
                (OptionCategoryInfoProvider.GetSKUOptionsCount(this.SKUID) > 0))
            {
                if (!RequestHelper.IsPostBack())
                {
                    // Count and show total price with options
                    CalculateTotalPrice();
                }

                // Show total price container
                if (this.ShowPriceIncludingTax)
                {
                    lblPrice.Text = ResHelper.GetString("ShoppingCartItemSelector.TotalPriceIncludeTax");
                }
                else
                {
                    lblPrice.Text = ResHelper.GetString("ShoppingCartItemSelector.TotalPriceWithoutTax");
                }
                pnlPrice.Visible = true;
            }
            else
            {
                // Hide total price container
                pnlPrice.Visible = false;
            }

            if (DialogMode)
            {
                pnlButton.CssClass += " PageFooterLine";
            }
        }

        lblUnits.Style.Add("display", "none");
    }


    /// <summary>
    /// Reloads shopping cart item selector data
    /// </summary>
    public void ReloadData()
    {
        if (this.SKUID > 0)
        {
            DebugHelper.SetContext("ShoppingCartItemSelector");

            InitializeControls();

            if (this.ShowProductOptions)
            {
                LoadProductOptions();
            }

            // Get count of the product options            
            if ((this.ShowTotalPrice) &&
                (OptionCategoryInfoProvider.GetSKUOptionsCount(this.SKUID) > 0))
            {
                // Count and show total price with options
                CalculateTotalPrice();
            }
            mDataLoaded = true;
            DebugHelper.ReleaseContext();
        }
    }


    /// <summary>
    /// Initializes controls
    /// </summary>
    private void InitializeControls()
    {
        if (this.lnkAdd == null)
        {
            this.upnlAjax.LoadContainer();
        }
        if (this.SKUEnabled)
        {
            // Display / hide units textbox with default quantity
            if (this.ShowUnitsTextBox)
            {
                txtUnits.Visible = true;
                lblUnits.Visible = true;
                lblUnits.AssociatedControlID = "txtUnits";
                if (txtUnits.Text.Trim() == "")
                {
                    txtUnits.Text = this.DefaultQuantity.ToString();
                }
            }

            // Display button/link for adding product to the shopping cart            
            if (this.AddToCartLinkText != "")
            {
                // Link button
                lnkAdd.Visible = true;
                lnkAdd.Text = ResHelper.LocalizeString(this.AddToCartLinkText);
                lnkAdd.ToolTip = ResHelper.LocalizeString(this.AddToCartLinkText);
                lnkAdd.Click += new EventHandler(lnkAdd_Click);
            }
            else if (this.AddToCartImageButton != "")
            {
                if (this.ImageFolder == "")
                {
                    // Set default image folder
                    this.ImageFolder = GetImageUrl("ShoppingCart/");
                }

                // Image button
                btnAddImage.Visible = true;
                btnAddImage.ImageUrl = this.ImageFolder.TrimEnd('/') + "/" + this.AddToCartImageButton;
                btnAddImage.AlternateText = ResHelper.GetString("ShoppingCart.AddToShoppingCartToolTip");
                btnAddImage.ToolTip = ResHelper.GetString("ShoppingCart.AddToShoppingCartToolTip");
            }
            else
            {
                // Classic button
                btnAdd.Visible = true;
                btnAdd.Text = ResHelper.GetString("ShoppingCart.AddToShoppingCart");
                btnAdd.Click += new EventHandler(btnAdd_Click);
            }
        }

        // Display "Add to Wishlist" link        
        if (this.AddToWishlistLinkText != "")
        {
            lnkWishlist.Visible = true;
            lnkWishlist.Text = ResHelper.LocalizeString(this.AddToWishlistLinkText);
            lnkWishlist.ToolTip = ResHelper.LocalizeString(this.AddToWishlistLinkText);
            lnkWishlist.Click += new EventHandler(lnkWishlist_Click);
        }
        // Display "Add to Wishlist" image button 
        else if (this.AddToWishlistImageButton != "")
        {
            if (this.ImageFolder == "")
            {
                // Set default image folder
                this.ImageFolder = GetImageUrl("ShoppingCart/");
            }

            // Image button
            btnWishlist.Visible = true;
            btnWishlist.ImageUrl = this.ImageFolder.TrimEnd('/') + "/" + this.AddToWishlistImageButton;
            btnWishlist.AlternateText = ResHelper.GetString("ShoppingCart.AddToWishlistToolTip");
            btnWishlist.ToolTip = ResHelper.GetString("ShoppingCart.AddToWishlistToolTip");
            btnWishlist.Click += new ImageClickEventHandler(btnWishlist_Click);
        }
    }

    void lnkWishlist_Click(object sender, EventArgs e)
    {
        AddProductToWishlist();
    }

    void btnWishlist_Click(object sender, ImageClickEventArgs e)
    {
        AddProductToWishlist();
    }


    void lnkAdd_Click(object sender, EventArgs e)
    {
        AddProductToShoppingCart();
    }


    protected void btnAddImage_Click(object sender, ImageClickEventArgs e)
    {
        AddProductToShoppingCart();
    }


    void btnAdd_Click(object sender, EventArgs e)
    {
        AddProductToShoppingCart();
    }


    private void AddProductToWishlist()
    {
        SessionHelper.SetValue("ShoppingCartUrlReferrer", UrlHelper.CurrentURL);
        UrlHelper.Redirect(this.WishlistUrl + "?productid=" + this.SKUID);
    }


    /// <summary>
    /// Adds specified quantity of product to the shopping cart.
    /// </summary>
    private void AddProductToShoppingCart()
    {
        // Fire action which is run instead of the original action
        if (OnAddToShoppingCart != null)
        {
            OnAddToShoppingCart(this, null);
        }
        else
        {
            if (!this.ShowProductOptions)
            {
                // If product has some enabled option categories
                if (!DataHelper.DataSourceIsEmpty(OptionCategoryInfoProvider.GetSKUOptionCategories(this.SKUID, "CategoryEnabled = 1")))
                {
                    UrlHelper.Redirect("~/CMSPages/GetProduct.aspx?productid=" + this.SKUID);
                }
            }

            // Build shopping cart url
            StringBuilder url = new StringBuilder();
            url.Append(this.ShoppingCartUrl);
            url.Append("?productid=" + this.SKUID);
            url.Append("&quantity=" + this.Quantity);
            if (this.ProductOptions != "")
            {
                url.Append("&options=" + this.ProductOptions);
            }

            SessionHelper.SetValue("ShoppingCartUrlReferrer", UrlHelper.CurrentURL);

            // Add product to the shopping cart
            UrlHelper.Redirect(url.ToString());
        }
    }


    /// <summary>
    /// Loads product options
    /// </summary>
    private void LoadProductOptions()
    {
        // Get all option categories assigned to the current product
        DataSet dsCategories = OptionCategoryInfoProvider.GetSKUOptionCategories(this.SKUID, "CategoryEnabled = 1");
        if (!DataHelper.DataSourceIsEmpty(dsCategories))
        {
            foreach (DataRow dr in dsCategories.Tables[0].Rows)
            {
                try
                {
                    // Load control for selection product options
                    ProductOptionSelector selector = (ProductOptionSelector)LoadControl("~/CMSModules/Ecommerce/Controls/ProductOptions/ProductOptionSelector.ascx");

                    selector.ID = "opt" + ValidationHelper.GetInteger(dr["CategoryID"], 0);
                    selector.LocalShoppingCartObj = this.LocalShoppingCartObj;
                    selector.ShowPriceIncludingTax = this.ShowPriceIncludingTax;

                    // Set option category data to the selector
                    selector.OptionCategoryDataRow = dr;

                    // Add option price hashtable
                    foreach (DictionaryEntry entry in selector.ProductOptionPrice)
                    {
                        this.mProductOptionPrice.Add(entry.Key, entry.Value);
                    }

                    // Add option department hashtable
                    foreach (DictionaryEntry entry in selector.ProductOptionDepartment)
                    {
                        this.mProductOptionDepartment.Add(entry.Key, entry.Value);
                    }

                    if (this.ShowTotalPrice)
                    {
                        // Add Index change handler
                        ((ListControl)(selector.SelectionControl)).AutoPostBack = true;
                        ((ListControl)(selector.SelectionControl)).SelectedIndexChanged += new EventHandler(Selector_SelectedIndexChanged);
                    }

                    // Add control to the collection
                    this.pnlSelectors.Controls.Add(selector);
                }
                catch
                {
                }
            }
        }

        // Show panel only when some selectors loaded
        this.pnlSelectors.Visible = (pnlSelectors.Controls.Count > 0);
    }


    /// <summary>
    /// Selector index change handler
    /// </summary>
    /// <param name="sender">Sender</param>
    /// <param name="e">Arguments</param>
    private void Selector_SelectedIndexChanged(object sender, EventArgs e)
    {
        CalculateTotalPrice();
    }


    /// <summary>
    /// Calculate total price with product options prices
    /// </summary>
    private void CalculateTotalPrice()
    {
        // Main price of product
        double price = 0;
        if (this.ShowPriceIncludingTax)
        {
            price = EcommerceFunctions.GetPrice(this.ProductInfo.SKUPrice, this.ProductInfo.SKUDepartmentID, this.LocalShoppingCartObj, this.ProductInfo.SKUID);
        }
        else
        {
            price = EcommerceFunctions.GetPrice(this.ProductInfo.SKUPrice, this.ProductInfo.SKUDepartmentID, this.LocalShoppingCartObj);
        }

        // Add all product options prices
        if (this.ProductOptions != "")
        {
            string[] options = this.ProductOptions.Split(',');
            foreach (string optionId in options)
            {
                if (optionId != "")
                {
                    // Add product option price
                    double optionPrice = Convert.ToDouble(this.mProductOptionPrice[Convert.ToInt32(optionId)]);
                    int optionDepartment = Convert.ToInt32(this.mProductOptionDepartment[Convert.ToInt32(optionId)]);
                    if (this.ShowPriceIncludingTax)
                    {
                        price += EcommerceFunctions.GetPrice(optionPrice, optionDepartment, this.LocalShoppingCartObj, Convert.ToInt32(optionId));
                    }
                    else
                    {
                        price += EcommerceFunctions.GetPrice(optionPrice, optionDepartment, this.LocalShoppingCartObj);
                    }
                }
            }
        }

        // Write formatted price
        CurrencyInfo currency = null;
        if (this.LocalShoppingCartObj != null)
        {
            currency = this.LocalShoppingCartObj.CurrencyInfoObj;
        }
        if (currency == null)
        {
            if (ECommerceContext.CurrentShoppingCart != null)
            {
                currency = ECommerceContext.CurrentShoppingCart.CurrencyInfoObj;
            }
        }
        if (currency == null)
        {
            currency = CurrencyInfoProvider.GetMainCurrency();
        }
        lblPriceValue.Text = CurrencyInfoProvider.GetFormatedPrice(price, currency);
    }
}
