using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SiteProvider;
using CMS.ExtendedControls;

public partial class CMSSiteManager_Administration_Users_User_Edit_Categories : CMSUsersPage
{
    protected int userId = 0;

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        this.CurrentMaster.PanelBody.CssClass = "";
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        userId = QueryHelper.GetInteger("userid", 0);
        this.categoriesEditElem.UserID = userId;

        // Check that only global administrator can edit global administrator's accouns
        UserInfo ui = UserInfoProvider.GetUserInfo(categoriesEditElem.UserID);
        if (!CheckGlobalAdminEdit(ui))
        {
            categoriesEditElem.Visible = false;
            lblError.Text = ResHelper.GetString("Administration-User_List.ErrorGlobalAdmin");
            lblError.Visible = true;
        }

        this.categoriesEditElem.AdministrationMode = true;
        this.categoriesEditElem.ZeroRowsText = ResHelper.GetString("general.nodatafound");
        this.categoriesEditElem.OnCheckPermissions += new CMSAdminControl.CheckPermissionsEventHandler(categoriesEditElem_OnCheckPermissions);

        Panel pnlContent = ControlsHelper.GetChildControl(Page, typeof(Panel), "pnlContent") as Panel;
        if (pnlContent != null)
        {
            pnlContent.CssClass = "";
        }
    }

    
    protected void categoriesEditElem_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        CurrentUserInfo cui = CMSContext.CurrentUser;
        if ((cui == null) || ((userId != cui.UserID) && !cui.IsAuthorizedPerResource("CMS.Users", permissionType)))
        {
            RedirectToCMSDeskAccessDenied("CMS.Users", permissionType);
        }
    }
}
