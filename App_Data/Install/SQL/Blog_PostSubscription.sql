CREATE TABLE [Blog_PostSubscription] (
		[SubscriptionID]                 int NOT NULL IDENTITY(1, 1),
		[SubscriptionPostDocumentID]     int NOT NULL,
		[SubscriptionUserID]             int NULL,
		[SubscriptionEmail]              nvarchar(250) NULL,
		[SubscriptionLastModified]       datetime NOT NULL,
		[SubscriptionGUID]               uniqueidentifier NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Blog_PostSubscription]
	ADD
	CONSTRAINT [PK_Blog_PostSubscription]
	PRIMARY KEY
	([SubscriptionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Blog_PostSubscription]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Document]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Blog_PostSubscription_SubscriptionPostDocumentID_CMS_Document]') IS NULL
BEGIN
		ALTER TABLE [Blog_PostSubscription]
			ADD CONSTRAINT [FK_Blog_PostSubscription_SubscriptionPostDocumentID_CMS_Document]
			FOREIGN KEY ([SubscriptionPostDocumentID]) REFERENCES [CMS_Document] ([DocumentID])
END
IF OBJECT_ID(N'[Blog_PostSubscription]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Blog_PostSubscription_SubscriptionUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Blog_PostSubscription]
			ADD CONSTRAINT [FK_Blog_PostSubscription_SubscriptionUserID_CMS_User]
			FOREIGN KEY ([SubscriptionUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_Blog_PostSubscription_SubscriptionPostDocumentID]
	ON [Blog_PostSubscription] ([SubscriptionPostDocumentID])
	ON [PRIMARY]
CREATE INDEX [IX_Blog_PostSubscription_SubscriptionUserID]
	ON [Blog_PostSubscription] ([SubscriptionUserID])
	ON [PRIMARY]
