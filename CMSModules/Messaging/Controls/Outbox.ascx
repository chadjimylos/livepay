<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Outbox.ascx.cs" Inherits="CMSModules_Messaging_Controls_Outbox" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Messaging/Controls/SendMessage.ascx" TagName="SendMessage"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Messaging/Controls/ViewMessage.ascx" TagName="ViewMessage"
    TagPrefix="cms" %>
<cms:CMSUpdatePanel ID="pnlOutbox" runat="server">
    <ContentTemplate>
        <asp:Label ID="lblInfo" runat="Server" CssClass="Info" Visible="false" EnableViewState="false" />
        <asp:Label ID="lblError" runat="Server" CssClass="Error" Visible="false" EnableViewState="false" />
        <asp:Panel ID="pnlBackToList" runat="Server" CssClass="BackToList" Visible="false">
            <cms:LocalizedLinkButton ID="btnBackToList" runat="server" EnableViewState="false"
                ResourceString="Messaging.BackToList" />
            <br />
            <br />
        </asp:Panel>
        <asp:Panel ID="pnlNew" runat="server" CssClass="NewPanel" Visible="false">
            <div class="NewMessageHeader">
                <cms:LocalizedLabel ID="lblNewMessageHeader" runat="server" EnableViewState="false"
                    ResourceString="Messaging.NewMessage" />
            </div>
            <cms:SendMessage ID="ucSendMessage" runat="server" />
        </asp:Panel>
        <asp:Panel ID="pnlView" runat="server" CssClass="ViewPanel" Visible="false">
            <asp:Panel ID="pnlActions" CssClass="MessageActionsPanel" runat="server">
                <cms:LocalizedLinkButton ID="btnForward" runat="Server" EnableViewState="false" ResourceString="Messaging.Forward" />&nbsp;
                <cms:LocalizedLinkButton ID="btnDelete" runat="Server" EnableViewState="false" ResourceString="Messaging.Delete" />
            </asp:Panel>
            <br />
            <div class="ViewMessageHeader">
                <cms:LocalizedLabel ID="lblViewMessageHeader" runat="server" EnableViewState="false"
                    ResourceString="Messaging.ViewMessage" />
            </div>
            <cms:ViewMessage ID="ucViewMessage" runat="server" StopProcessing="true" />
        </asp:Panel>
        <asp:Panel ID="pnlList" runat="server" CssClass="ListPanel">
            <asp:Panel ID="pnlGeneralActions" runat="Server" CssClass="GeneralActions">
                <table cellpadding="0" cellspacing="0" style="border-style: none;">
                    <tr>
                        <td>
                            <cms:LocalizedLinkButton ID="btnNewMessage" runat="Server" CssClass="NewMessage"
                                EnableViewState="false" ResourceString="Messaging.NewMessage" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <cms:LocalizedLinkButton ID="btnDeleteAll" runat="Server" CssClass="DeleteAll" EnableViewState="false"
                                ResourceString="Messaging.DeleteAll" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <br />
            <cms:UniGrid runat="server" ID="outboxGrid" GridName="~/CMSModules/Messaging/Controls/Outbox.xml"
                DelayedReload="true" OrderBy="MessageSent DESC" />
            <br />
            <div class="FooterInfo">
                <asp:Label ID="lblFooter" runat="Server" EnableViewState="false" />
            </div>
        </asp:Panel>
    </ContentTemplate>
</cms:CMSUpdatePanel>
