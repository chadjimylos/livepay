<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReportGraph.ascx.cs" Inherits="CMSModules_Reporting_Controls_ReportGraph" %>
<asp:Image runat="server" ID="imgGraph" AlternateText="Report graph" EnableViewState="false" />

<asp:Label runat="server" ID="lblError" EnableViewState="false" CssClass="InlineControlError" Visible="false" />
