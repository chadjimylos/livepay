using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Reporting;
using CMS.UIControls;

public partial class CMSModules_Reporting_Tools_Report_new : CMSReportingPage
{
    int categoryId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
        {
            RedirectToAccessDenied("cms.reporting", "Modify");
        }

        // control initializations				
        rfvReportDisplayName.ErrorMessage = ResHelper.GetString("Report_New.EmptyDisplayName");
        rfvReportName.ErrorMessage = ResHelper.GetString("Report_New.EmptyCodeName");

        lblReportDisplayName.Text = ResHelper.GetString("Report_New.DisplayNameLabel");
        lblReportName.Text = ResHelper.GetString("Report_New.NameLabel");
        lblReportAccess.Text = ResHelper.GetString("Report_New.ReportAccessLabel");

        btnOk.Text = ResHelper.GetString("General.OK");

        categoryId = ValidationHelper.GetInteger(Request.QueryString["categoryId"], 0);

        InitializeMasterPage();
	}


    /// <summary>
    /// Initializes Master Page.
    /// </summary>
    protected void InitializeMasterPage()
    {
        // Initializes page title label
        string[,] tabs = new string[2, 3];
        tabs[0, 0] = ResHelper.GetString("Report_New.ReportList");
        tabs[0, 1] = "~/CMSModules/Reporting/Tools/Report_List.aspx?categoryid=" + categoryId;
        tabs[0, 2] = "";
        tabs[1, 0] = ResHelper.GetString("Report_New.NewReport");
        tabs[1, 1] = "";
        tabs[1, 2] = "";
        CurrentMaster.Title.Breadcrumbs = tabs;

        // Initialize title and help
        Title = "Report new";
        CurrentMaster.Title.HelpName = "helpTopic";
        CurrentMaster.Title.HelpTopicName = "new_report";
    }

   
    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
        {
            RedirectToAccessDenied("cms.reporting", "Modify");
        }
        

        string errorMessage = new Validator().NotEmpty(txtReportDisplayName.Text.Trim(), rfvReportDisplayName.ErrorMessage).NotEmpty(txtReportName.Text.Trim(), rfvReportName.ErrorMessage).Result;

        if ((errorMessage == "") && (!ValidationHelper.IsCodeName(txtReportName.Text.Trim())))
        {
            errorMessage = ResHelper.GetString("general.invalidcodename");
        }

        if (ReportCategoryInfoProvider.GetReportCategoryInfo(categoryId) == null)
        {
            errorMessage = ResHelper.GetString("Report_General.InvalidReportCategory");
        }

        ReportAccessEnum reportAccess = ReportAccessEnum.All;
        if (!chkReportAccess.Checked)
        {
            reportAccess = ReportAccessEnum.Authenticated;
        }

        if (errorMessage == "")
        {
            //if report with given name already exists show error message
            if ( ReportInfoProvider.GetReportInfo(txtReportName.Text.Trim()) != null)
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Report_New.ReportAlreadyExists");
                return;
            }

            ReportInfo ri = new ReportInfo();

            ri.ReportDisplayName = txtReportDisplayName.Text.Trim();
            ri.ReportName = txtReportName.Text.Trim();
            ri.ReportCategoryID = categoryId;
            ri.ReportLayout = "";
            ri.ReportParameters = "";
            ri.ReportAccess = reportAccess;

            ReportInfoProvider.SetReportInfo(ri);

            UrlHelper.Redirect("Report_Edit.aspx?reportId=" + ri.ReportID + "&saved=1&categoryId="+categoryId);
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
