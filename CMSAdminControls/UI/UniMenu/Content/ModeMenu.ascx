<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModeMenu.ascx.cs" Inherits="CMSAdminControls_UI_UniMenu_Content_ModeMenu" %>
<%@ Register Src="~/CMSAdminControls/UI/UniMenu/UniMenuButtons.ascx" TagName="UniMenuButtons"
    TagPrefix="cms" %>
<cms:UniMenuButtons ID="buttonsBig" runat="server" EnableViewState="false" AllowSelection="true" />
