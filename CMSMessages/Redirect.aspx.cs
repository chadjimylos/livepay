using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.UIControls;

public partial class CMSMessages_Redirect : MessagePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.titleElem.TitleText = ResHelper.GetString("Redirect.Header");
        this.titleElem.TitleImage = GetImageUrl("Others/Messages/redirect.png");
        this.lblInfo.Text = ResHelper.GetString("Redirect.Info");
        string url = QueryHelper.GetText("url", String.Empty);
        string target = QueryHelper.GetText("target", String.Empty);
        string frame = QueryHelper.GetText("frame", String.Empty);
        bool urlIsRelative = url.StartsWith("~/");

        string script = String.Empty;
        url = ResolveUrl(url);

        // Information about the target page
        this.lnkTarget.Text = url;
        this.lnkTarget.NavigateUrl = url;
        this.lnkTarget.Target = target;

        // Generate redirect script
        if (urlIsRelative && frame.ToLower(CultureHelper.EnglishCulture) == "top")
        {
            script += "if (self.location!=top.location) { top.location = '" + url + "'; }";
        }
        else if ((target == String.Empty)&&(url != String.Empty))
        {
            script += "if (IsCMSDesk()) { window.open('" + url + "'); } else { document.location = '" + url + "'; }";
        }

        this.ltlScript.Text += ScriptHelper.GetScript(script);
    }
}
