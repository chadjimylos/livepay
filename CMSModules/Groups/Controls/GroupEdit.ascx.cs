using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Community;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.LicenseProvider;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Groups_Controls_GroupEdit : CMSAdminEditControl
{
    #region "Variables"

    private int mGroupId = 0;
    private int mSiteId = 0;
    private bool mHideWhenGroupIsNotSupplied = false;
    private bool mDisplayAdvanceOptions = false;
    private GroupInfo gi = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Determines whether to hide the content of the control when GroupID is not supplied.
    /// </summary>
    public bool HideWhenGroupIsNotSupplied
    {
        get
        {
            return this.mHideWhenGroupIsNotSupplied;
        }
        set
        {
            this.mHideWhenGroupIsNotSupplied = value;

        }
    }


    /// <summary>
    /// Current group ID
    /// </summary>
    public int GroupID
    {
        get
        {
            if (mGroupId <= 0)
            {
                this.mGroupId = ValidationHelper.GetInteger(this.GetValue("GroupID"), 0);
            }

            return this.mGroupId;
        }
        set
        {
            this.mGroupId = value;
        }
    }


    /// <summary>
    /// Current site ID
    /// </summary>
    public int SiteID
    {
        get
        {
            if (this.mSiteId <= 0)
            {
                this.mSiteId = ValidationHelper.GetInteger(this.GetValue("SiteID"), 0);
            }

            return this.mSiteId;
        }
        set
        {
            this.mSiteId = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether should be displayed advance fields like codename display name
    /// or document selector
    /// </summary>
    public bool DisplayAdvanceOptions
    {
        get
        {
            return this.mDisplayAdvanceOptions;
        }
        set
        {
            this.mDisplayAdvanceOptions = value;
            this.plcGroupLocation.Visible = value;
            this.plcAdvanceOptions.Visible = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Visible)
        {
            this.EnableViewState = false;
        }

        if (this.StopProcessing)
        {
            // Do nothing
            this.Visible = false;
            this.groupPictureEdit.StopProcessing = true;
            this.groupPageURLElem.StopProcessing = true;
        }
        else
        {
            this.plcGroupLocation.Visible = this.DisplayAdvanceOptions;
            this.plcAdvanceOptions.Visible = this.DisplayAdvanceOptions;


            RaiseOnCheckPermissions(CMSAdminControl.PERMISSION_READ, this);

            if (this.StopProcessing)
            {
                return;
            }

            if ((this.GroupID == 0) && this.HideWhenGroupIsNotSupplied)
            {
                this.Visible = false;
                return;
            }

            InitializeForm();

            gi = GroupInfoProvider.GetGroupInfo(this.GroupID);
            if (gi != null)
            {
                if (!RequestHelper.IsPostBack())
                {
                    // Handle existing Group editing - prepare the data
                    if (this.GroupID > 0)
                    {
                        HandleExistingGroup();
                    }
                }

                this.groupPictureEdit.GroupInfo = gi;
            }

            groupPictureEdit.IsLiveSite = this.IsLiveSite;
            groupPageURLElem.IsLiveSite = this.IsLiveSite;
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        SaveData();
    }


    #region "Public methods"

    /// <summary>
    /// Updates the current Group or creates new if no GroupID is present.
    /// </summary>
    public void SaveData()
    {
        if (!CheckPermissions("cms.groups", CMSAdminControl.PERMISSION_MANAGE, this.GroupID))
        {
            return;
        }

        // Trim display name and code name
        string displayName = this.txtDisplayName.Text.Trim();
        string codeName = ValidationHelper.GetCodeName(this.txtCodeName.Text.Trim(), null, 89, true, true);
        this.txtCodeName.Text = codeName;

        // Validate form entries
        string errorMessage = ValidateForm(displayName, codeName);
        if (errorMessage == "")
        {
            txtCodeName.Text = codeName;
            txtDisplayName.Text = displayName;

            GroupInfo group = null;

            try
            {
                bool newGroup = false;
                // Update existing item
                if ((this.GroupID) > 0 && (gi != null))
                {
                    group = gi;
                }
                else
                {
                    group = new GroupInfo();
                    newGroup = true;
                }

                if (group != null)
                {
                    if (this.DisplayAdvanceOptions)
                    {
                        // Update Group fields
                        group.GroupDisplayName = displayName;
                        group.GroupName = codeName;
                        group.GroupNodeGUID = ValidationHelper.GetGuid(this.groupPageURLElem.Value, Guid.Empty);
                    }
                    group.GroupDescription = this.txtDescription.Text;
                    group.GroupAccess = GetGroupAccess();
                    group.GroupSiteID = this.SiteID;
                    group.GroupApproveMembers = GetGroupApproveMembers();
                    group.GroupSendJoinLeaveNotification = this.chkJoinLeave.Checked;
                    group.GroupSendWaitingForApprovalNotification = this.chkWaitingForApproval.Checked;
                    this.groupPictureEdit.UpdateGroupPicture(group);

                    // If new group was created 
                    if (newGroup)
                    {
                        // Set columns GroupCreatedByUserID and GroupApprovedByUserID to current user
                        CurrentUserInfo user = CMSContext.CurrentUser;
                        if (user != null)
                        {
                            group.GroupCreatedByUserID = user.UserID;
                            group.GroupApprovedByUserID = user.UserID;
                            group.GroupApproved = true;
                        }
                    }

                    // Save Group in the database
                    GroupInfoProvider.SetGroupInfo(group);
                    this.groupPictureEdit.GroupInfo = group;

                    // Display information on success
                    this.lblInfo.Text = ResHelper.GetString("general.changessaved");
                    this.lblInfo.Visible = true;

                    // If new group was created 
                    if (newGroup)
                    {
                        this.GroupID = group.GroupID;
                        this.RaiseOnSaved();
                    }
                }
            }
            catch (Exception ex)
            {
                // Display error message
                this.lblError.Text = ResHelper.GetString("general.erroroccurred") + " " + ex.Message;
                this.lblError.Visible = true;
            }
        }
        else
        {
            // Display error message
            this.lblError.Text = errorMessage;
            this.lblError.Visible = true;
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes the contols in the form
    /// </summary>
    private void InitializeForm()
    {
        // Initialize errors
        this.rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        this.rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");

        // Initialize buttons
        this.btnSave.Text = ResHelper.GetString("general.ok");
    }


    /// <summary>
    /// Returns correct number according to radiobutton selection
    /// </summary>
    private SecurityAccessEnum GetGroupAccess()
    {
        if (this.radSiteMembers.Checked)
        {
            return SecurityAccessEnum.AuthenticatedUsers;
        }
        else if (this.radGroupMembers.Checked)
        {
            return SecurityAccessEnum.GroupMembers;
        }
        else
        {
            return SecurityAccessEnum.AllUsers;
        }
    }


    /// <summary>
    /// Returns correct number according to radiobutton selection
    /// </summary>
    private GroupApproveMembersEnum GetGroupApproveMembers()
    {
        if (this.radMembersApproved.Checked)
        {
            return GroupApproveMembersEnum.ApprovedCanJoin;
        }
        else if (this.radMembersInvited.Checked)
        {
            return GroupApproveMembersEnum.InvitedWithoutApproval;
        }
        else
        {
            return GroupApproveMembersEnum.AnyoneCanJoin;
        }
    }


    /// <summary>
    /// Fills the data into form for specified Group
    /// </summary>
    private void HandleExistingGroup()
    {
        if (gi != null)
        {
            this.txtDisplayName.Text = gi.GroupDisplayName;
            this.txtCodeName.Text = gi.GroupName;
            this.txtDescription.Text = gi.GroupDescription;
            if (this.DisplayAdvanceOptions)
            {
                this.groupPageURLElem.Value = gi.GroupNodeGUID.ToString();
            }
            this.chkJoinLeave.Checked = gi.GroupSendJoinLeaveNotification;
            this.chkWaitingForApproval.Checked = gi.GroupSendWaitingForApprovalNotification;

            UserInfo ui = null;

            // Display created by user name
            if (gi.GroupCreatedByUserID != 0)
            {
                plcCreatedBy.Visible = true;

                ui = UserInfoProvider.GetUserInfo(gi.GroupCreatedByUserID);
                if (ui != null)
                {
                    this.lblCreatedByValue.Text = HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(ui.UserName, this.IsLiveSite));
                }
            }

            // Display approved by user name
            if (gi.GroupApprovedByUserID != 0)
            {
                plcApprovedBy.Visible = true;

                if (gi.GroupApprovedByUserID != gi.GroupCreatedByUserID)
                {
                    ui = UserInfoProvider.GetUserInfo(gi.GroupApprovedByUserID);
                }

                if (ui != null)
                {
                    this.lblApprovedByValue.Text = HTMLHelper.HTMLEncode(Functions.GetFormattedUserName(ui.UserName, this.IsLiveSite));
                }
            }

            switch (gi.GroupAccess)
            {
                case SecurityAccessEnum.AllUsers:
                    this.radAnybody.Checked = true;
                    break;

                case SecurityAccessEnum.AuthenticatedUsers:
                    this.radSiteMembers.Checked = true;
                    break;

                case SecurityAccessEnum.GroupMembers:
                    this.radGroupMembers.Checked = true;
                    break;
            }

            switch (gi.GroupApproveMembers)
            {
                case GroupApproveMembersEnum.AnyoneCanJoin:
                    this.radMembersAny.Checked = true;
                    break;

                case GroupApproveMembersEnum.ApprovedCanJoin:
                    this.radMembersApproved.Checked = true;
                    break;

                case GroupApproveMembersEnum.InvitedWithoutApproval:
                    this.radMembersInvited.Checked = true;
                    break;
            }
        }
    }


    /// <summary>
    /// Checks whether the given code name is unique
    /// </summary>
    private bool CodeNameIsUnique(string codeName, int currentGroupId)
    {

        string where = "GroupName = N'" + codeName.Replace("'", "''") + "'";

        // Filter out current group
        if (currentGroupId > 0)
        {
            where = SqlHelperClass.AddWhereCondition(where, "GroupId <> " + currentGroupId);
        }

        if (this.SiteID > 0)
        {
            where = SqlHelperClass.AddWhereCondition(where, "GroupSiteID = " + this.SiteID);
        }


        return DataHelper.DataSourceIsEmpty(GroupInfoProvider.GetGroups(where, null));
    }


    /// <summary>
    /// Validates the form entries
    /// </summary>
    /// <param name="codeName">Code name to validate</param>)
    /// <param name="displayName">Display name to validate</param>
    private string ValidateForm(string displayName, string codeName)
    {
        string errorMessage = new Validator().NotEmpty(codeName, this.rfvCodeName.ErrorMessage)
                                             .NotEmpty(displayName, this.rfvDisplayName.ErrorMessage)
                                             .IsCodeName(codeName, ResHelper.GetString("general.errorcodenameinidentificatorformat"), true).Result;

        if (errorMessage != "")
        {
            return errorMessage;
        }

        // Validate uniqueness
        if (!CodeNameIsUnique(codeName, this.GroupID))
        {
            errorMessage = ResHelper.GetString("general.uniquecodenameerror");
        }

        // Validate file input
        if (!this.groupPictureEdit.IsValid())
        {
            errorMessage = this.groupPictureEdit.ErrorMessage;
        }

        return errorMessage;
    }

    #endregion
}
