using System;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Web;


using CMS.PortalControls;
using CMS.SettingsProvider;
using CMS.MembershipProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.PortalEngine;

public partial class CMSWebParts_Membership_FacebookConnect_FacebookConnectLogon : CMSAbstractWebPart
{
    #region "Constants"

    protected const string COOKIE_NAME = "webauthtoken";
    protected const string SESSION_NAME_USERDATA = "facebookid";
    protected const string CONFIRMATION_URLPARAMETER = "confirmed";

    #endregion


    #region "Public properties"

    /// <summary>
    /// Get or sets sign in text
    /// </summary>
    public string SignInText
    {
        get
        {
            return ValidationHelper.GetString(GetValue("SignInText"), "");
        }
        set
        {
            SetValue("SignInText", value);
        }
    }


    /// <summary>
    /// Get or sets size of Facebook Connect login button
    /// Allowed values: 'small', 'medium', 'large', 'xlarge'
    /// </summary>
    public string Size
    {
        get
        {
            return ValidationHelper.GetString(GetValue("Size"), "medium");
        }
        set
        {
            SetValue("Size", value);
        }
    }


    /// <summary>
    /// Get or sets length of Facebook Connect login button
    /// Allowed values: 'short', 'long'
    /// </summary>
    public string Length
    {
        get
        {
            return ValidationHelper.GetString(GetValue("Length"), "short");
        }
        set
        {
            SetValue("Length", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether to show sign out link
    /// </summary>
    public bool LatestVersion
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("LatestVersion"), false);
        }
        set
        {
            SetValue("LatestVersion", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether to show sign out link
    /// </summary>
    public bool ShowSignOut
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("ShowSignOut"), false);
        }
        set
        {
            SetValue("ShowSignOut", value);
        }
    }


    /// <summary>
    /// Get or sets sign out text
    /// </summary>
    public string SignOutText
    {
        get
        {
            return ValidationHelper.GetString(GetValue("SignOutText"), "");
        }
        set
        {
            SetValue("SignOutText", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that buttons will be used instead of links
    /// </summary>
    public bool ShowAsButton
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("ShowAsButton"), false);
        }
        set
        {
            SetValue("ShowAsButton", value);
        }
    }


    /// <summary>
    /// Get or sets sign out button image URL
    /// </summary>
    public string SignOutImageURL
    {
        get
        {
            return DataHelper.GetNotEmpty(GetValue("SignOutImageURL"), null);
        }
        set
        {
            SetValue("SignOutImageURL", value);
        }
    }


    /// <summary>
    /// Returns attribute activating "latest version" mode if "latest version" mode is requested.
    /// </summary>
    protected string LatestVersionAttribute
    {
        get
        {
            return (LatestVersion ? "v=\"2\"" : "");
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (StopProcessing)
        {
            this.Visible = false;
        }
        else
        {
            if (QueryHelper.GetInteger("logout", 0) > 0)
            {
                // Sign out from CMS
                FormsAuthentication.SignOut();
                CMSContext.ClearShoppingCart();
                CMSContext.CurrentUser = null;
                Response.Cache.SetNoStore();
                UrlHelper.Redirect(UrlHelper.RemoveParameterFromUrl(UrlHelper.CurrentURL, "logout"));
                return;
            }

            string currentSiteName = CMSContext.CurrentSiteName;
            if (!String.IsNullOrEmpty(currentSiteName) && SettingsKeyProvider.GetBoolValue(currentSiteName + ".CMSEnableFacebookConnect"))
            {
                // Check Facebook Connect settings
                if (!FacebookConnectHelper.FacebookIsAvailable(currentSiteName))
                {
                    // Display warning message in "Design mode"
                    if (DisplayMessage())
                    {
                        return;
                    }

                    this.Visible = false;
                    return;
                }

                // Try to retrieve return URL from query
                string returnUrl = QueryHelper.GetString("returnurl", null);

                // Init Facebook Connect
                if (this.Page is ContentPage)
                {
                    // Adding XML namespace
                    ((ContentPage)this.Page).XmlNamespace = FacebookConnectHelper.GetFacebookXmlNamespace();
                }
                // Init FB connect
                ltlScript.Text = FacebookConnectHelper.GetFacebookInitScriptForSite(currentSiteName);
                // Return URL
                string currentUrl = UrlHelper.AddParameterToUrl(UrlHelper.CurrentURL, "logout", "1");
                string additionalScript = "window.location.href=" + ScriptHelper.GetString(UrlHelper.GetAbsoluteUrl(currentUrl)) + "; return false;";
                // Logout script for FB connect
                string logoutScript = FacebookConnectHelper.GetFacebookLogoutScriptForSignOut(UrlHelper.CurrentURL, FacebookConnectHelper.GetFacebookApiKey(currentSiteName), additionalScript);

                string facebookUserId = "";
                bool facebookCookiesValid = FacebookConnectHelper.GetFacebookSessionInfo(currentSiteName, out facebookUserId) == FacebookValidationEnum.ValidSignature;

                // If user is already authenticated 
                if (CMSContext.CurrentUser.IsAuthenticated())
                {
                    // Is user logged in using Facebook Connect?
                    if (!facebookCookiesValid || ((CMSContext.CurrentUser.UserSettings != null) &&
                        (CMSContext.CurrentUser.UserSettings.UserFacebookID != facebookUserId)))
                    {  // no, user is not logged in by Facebook Connect
                        logoutScript = additionalScript;
                    }

                    // Hide Facebook Connect button
                    plcFBButton.Visible = false;

                    // If signout should be visible and user has FacebookID registered
                    if (ShowSignOut && !String.IsNullOrEmpty(CMSContext.CurrentUser.UserSettings.UserFacebookID))
                    {
                        // If only text is set use text/button link
                        if (!String.IsNullOrEmpty(SignOutText))
                        {
                            // Button link
                            if (ShowAsButton)
                            {
                                btnSignOut.OnClientClick = logoutScript;
                                btnSignOut.Text = SignOutText;
                                btnSignOut.Visible = true;
                            }
                            // Text link
                            else
                            {
                                lnkSignOutLink.Text = SignOutText;
                                lnkSignOutLink.Visible = true;
                                lnkSignOutLink.Attributes.Add("onclick", logoutScript);
                                lnkSignOutLink.Attributes.Add("style", "cursor:pointer;");
                            }
                        }
                        // Image link
                        else
                        {
                            string signOutImageUrl = SignOutImageURL;
                            // Use default image if none is specified
                            if (String.IsNullOrEmpty(signOutImageUrl))
                            {
                                signOutImageUrl = GetImageUrl("Others/FacebookConnect/signout.gif");
                            }
                            imgSignOut.ImageUrl = ResolveUrl(signOutImageUrl);
                            imgSignOut.Visible = true;
                            imgSignOut.AlternateText = ResHelper.GetString("webparts_membership_signoutbutton.signout");
                            lnkSignOutImageBtn.Visible = true;
                            lnkSignOutImageBtn.Attributes.Add("onclick", logoutScript);
                            lnkSignOutImageBtn.Attributes.Add("style", "cursor:pointer;");
                        }
                    }
                    else
                    {
                        Visible = false;
                    }
                }
                // Sign In
                else
                {
                    if ((QueryHelper.GetInteger(CONFIRMATION_URLPARAMETER, 0) > 0) && facebookCookiesValid)
                    {
                        if (!String.IsNullOrEmpty(facebookUserId))
                        {
                            UserInfo ui = UserInfoProvider.GetUserInfoByFacebookConnectID(facebookUserId);
                            if (ui != null)
                            {
                                // Claimed Facebook ID is in DB

                                // Login existing user
                                if ((ui != null) && ui.Enabled)
                                {
                                    // Ban IP addresses which are blocked for login
                                    BannedIPInfoProvider.CheckIPandRedirect(currentSiteName, BanControlEnum.Login);

                                    // Create autentification cookie
                                    UserInfoProvider.SetAuthCookieWithUserData(ui.UserName, true, Session.Timeout, new string[] { "facebooklogon" });
                                    UserInfoProvider.SetPreferredCultures(ui);

                                    // Redirect user
                                    if (String.IsNullOrEmpty(returnUrl))
                                    {
                                        returnUrl = UrlHelper.RemoveParameterFromUrl(UrlHelper.CurrentURL, CONFIRMATION_URLPARAMETER);
                                    }
                                    UrlHelper.Redirect(returnUrl);
                                }
                                // Otherwise is user disabled
                                else
                                {
                                    HttpCookie loginCookie = new HttpCookie(COOKIE_NAME);
                                    loginCookie.Expires = DateTime.Now.AddYears(-10);
                                    Response.Cookies.Add(loginCookie);
                                }
                            }
                            else
                            {
                                // Claimed Facebook ID not found  = save new user

                                // Check whether additional user info page is set
                                string additionalInfoPage = SettingsKeyProvider.GetStringValue(currentSiteName + ".CMSRequiredFacebookPage").Trim();

                                // No page set, user can be created
                                if (String.IsNullOrEmpty(additionalInfoPage))
                                {
                                    // Check if IP address is not banned for possible registration
                                    bool create = BannedIPInfoProvider.IsAllowed(currentSiteName, BanControlEnum.Registration);

                                    // Register new user
                                    ui = UserInfoProvider.AuthenticateFacebookConnectUser(facebookUserId, currentSiteName, create, true);

                                    // If user was found or successfuly created
                                    if (ui != null)
                                    {
                                        // Check license limitation
                                        if (!CheckLicenseLimit(ui))
                                        {
                                            // Disable user
                                            ui.Enabled = false;
                                            UserInfoProvider.SetUserInfo(ui);
                                        }

                                        // If user is enabled
                                        if (ui.Enabled)
                                        {
                                            // Ban IP addresses which are blocked for login
                                            BannedIPInfoProvider.CheckIPandRedirect(currentSiteName, BanControlEnum.Login);

                                            // Create authentification cookie
                                            UserInfoProvider.SetAuthCookieWithUserData(ui.UserName, true, Session.Timeout, new string[] { "facebooklogon" });
                                            UserInfoProvider.SetPreferredCultures(ui);
                                        }
                                        // Otherwise is user disabled
                                        else
                                        {
                                            HttpCookie loginCookie = new HttpCookie(COOKIE_NAME);
                                            loginCookie.Expires = DateTime.Now.AddYears(-10);

                                            Response.Cookies.Add(loginCookie);
                                        }
                                    }
                                    // Must be banned for registration, otherwise would be created
                                    else
                                    {
                                        BannedIPInfoProvider.CheckIPandRedirect(currentSiteName, BanControlEnum.Registration);

                                        HttpCookie loginCookie = new HttpCookie(COOKIE_NAME);
                                        loginCookie.Expires = DateTime.Now.AddYears(-10);

                                        Response.Cookies.Add(loginCookie);
                                    }

                                    // Redirect 
                                    if (!String.IsNullOrEmpty(returnUrl))
                                    {
                                        UrlHelper.Redirect(UrlHelper.GetAbsoluteUrl(returnUrl));
                                    }
                                    else
                                    {
                                        UrlHelper.Redirect(UrlHelper.RemoveParameterFromUrl(UrlHelper.CurrentURL, CONFIRMATION_URLPARAMETER));
                                    }
                                }
                                // Additional information page is set
                                else
                                {
                                    // Store user object in session for additional info page
                                    Session[SESSION_NAME_USERDATA] = facebookUserId;
                                    
                                    // Redirect to additional info page
                                    string targetURL = UrlHelper.GetAbsoluteUrl(additionalInfoPage);

                                    if (!String.IsNullOrEmpty(returnUrl))
                                    {
                                        // Add return URL to parameter
                                        targetURL = UrlHelper.AddParameterToUrl(targetURL, "returnurl", HttpUtility.UrlEncode(returnUrl));
                                    }
                                    UrlHelper.Redirect(targetURL);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                // Show warning message in "Design mode"
                this.Visible = DisplayMessage();
            }
        }
    }


    /// <summary>
    /// Checks if newly created user doesn't interfere with site limits.
    /// </summary>
    /// <param name="ui">User info to check</param>
    /// <returns>Returns true if all limits are OK</returns>
    private bool CheckLicenseLimit(UserInfo ui)
    {
        // Check limitations for Global administrator
        if (ui.IsGlobalAdministrator)
        {
            if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.GlobalAdmininistrators, VersionActionEnum.Insert, false))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("License.MaxItemsReachedGlobal");
                return false;
            }
        }

        // Check limitations for editors
        if (ui.IsEditor)
        {
            if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Editors, VersionActionEnum.Insert, false))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("License.MaxItemsReachedEditor");
                return false;
            }
        }

        // Check limitations for site members
        if (!UserInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.SiteMembers, VersionActionEnum.Insert, false))
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("License.MaxItemsReachedSiteMember");
            return false;
        }

        return true;
    }


    /// <summary>
    /// Displays warning message in "Design mode".
    /// </summary>
    private bool DisplayMessage()
    {
        // Error label is displayed in Design mode when Facebook Connect is disabled
        if (CMSContext.ViewMode == ViewModeEnum.Design)
        {
            string parameter = null;
            if (CMSContext.CurrentUser.IsGlobalAdministrator)
            {
                parameter = "<a href=\"" + UrlHelper.GetAbsoluteUrl("~/CMSSiteManager/default.aspx?section=settings") + "\" target=\"_top\">SiteManager -> Settings -> Facebook Connect</a>";
            }
            else
            {
                parameter = "SiteManager -> Settings -> Facebook Connect";
            }
            lblError.Text = String.Format(ResHelper.GetString("mem.facebook.disabled"), parameter);
            lblError.Visible = true;
            plcFBButton.Visible = false;
            imgSignOut.Visible = false;
            lnkSignOutImageBtn.Visible = false;
            lnkSignOutLink.Visible = false;
        }

        return lblError.Visible;
    }

    #endregion
}
