CREATE TABLE [CMS_BannedIP] (
		[IPAddressID]                int NOT NULL IDENTITY(1, 1),
		[IPAddress]                  nvarchar(100) NOT NULL,
		[IPAddressRegular]           nvarchar(200) NOT NULL,
		[IPAddressAllowed]           bit NOT NULL,
		[IPAddressAllowOverride]     bit NOT NULL,
		[IPAddressBanReason]         nvarchar(450) NULL,
		[IPAddressBanType]           nvarchar(100) NOT NULL,
		[IPAddressBanEnabled]        bit NULL,
		[IPAddressSiteID]            int NULL,
		[IPAddressGUID]              uniqueidentifier NOT NULL,
		[IPAddressLastModified]      datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_BannedIP]
	ADD
	CONSTRAINT [PK_CMS_BannedIP]
	PRIMARY KEY
	NONCLUSTERED
	([IPAddressID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_BannedIP]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_BannedIP_IPAddressSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_BannedIP]
			ADD CONSTRAINT [FK_CMS_BannedIP_IPAddressSiteID_CMS_Site]
			FOREIGN KEY ([IPAddressSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_CMS_BannedIP_IPAddressSiteID_IPAddress]
	ON [CMS_BannedIP] ([IPAddress], [IPAddressSiteID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
