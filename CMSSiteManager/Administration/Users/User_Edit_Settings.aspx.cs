using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_Users_User_Edit_Settings : CMSUsersPage
{
    private UserInfo ui;


    /// <summary>
    /// Handles onInit, fill timezone dropdownlist
    /// </summary>    
    protected override void OnInit(EventArgs e)
    {
        timeZone.ReloadData();
        base.OnInit(e);
    }


    /// <summary>
    /// Handles Page Load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        //Load labels
        btnOk.Text = ResHelper.GetString("general.ok");
        lblActivatedByUser.Text = ResHelper.GetString("adm.user.lblActivatedByUser");
        lblActivationDate.Text = ResHelper.GetString("adm.user.lblActivationDate");
        lblCampaign.Text = ResHelper.GetString("adm.user.lblCampaign");
        lblMessageNotifEmail.Text = ResHelper.GetString("adm.user.lblMessageNotifEmail");
        lblNickName.Text = ResHelper.GetString("adm.user.lblNickName");
        lblRegInfo.Text = ResHelper.GetString("adm.user.lblRegInfo");
        lblTimeZone.Text = ResHelper.GetString("adm.user.lblTimeZone");
        lblURLReferrer.Text = ResHelper.GetString("adm.user.lblURLReferrer");
        lblUserPicture.Text = ResHelper.GetString("adm.user.lblUserPicture");
        lblUserSignature.Text = ResHelper.GetString("adm.user.lblUserSignature");
        lblWaitingForActivation.Text = ResHelper.GetString("adm.user.lblWaitingForActivation");
        lblUserLiveID.Text = ResHelper.GetString("adm.user.lblUserLiveID");
        lblUserOpenID.ResourceString = "adm.user.lblOpenID";

        lblBadge.Text = ResHelper.GetString("adm.user.lblBadge");
        lblUserShowSplashScreen.Text = ResHelper.GetString("adm.user.lblUserShowSplashScreen");
        lblUserActivityPoints.Text = ResHelper.GetString("adm.user.lblUserActivityPoints");
        lblUserForumPosts.Text = ResHelper.GetString("adm.user.lblUserForumPosts");
        lblUserBlogPosts.Text = ResHelper.GetString("adm.user.lblUserBlogPosts");
        lblUserBlogComments.Text = ResHelper.GetString("adm.user.lblUserBlogComments");
        lblUserGender.Text = ResHelper.GetString("adm.user.lblUserGender");
        lblUserDateOfBirth.Text = ResHelper.GetString("adm.user.lblUserDateOfBirth");
        lblUserMessageBoardPosts.Text = ResHelper.GetString("adm.user.lblUserMessageBoardPosts");

        if (!RequestHelper.IsPostBack())
        {
            rbtnlGender.Items.Add(new ListItem(ResHelper.GetString("general.unknown"), "0"));
            rbtnlGender.Items.Add(new ListItem(ResHelper.GetString("general.male"), "1"));
            rbtnlGender.Items.Add(new ListItem(ResHelper.GetString("general.female"), "2"));
        }

        // Get userid from query string
        int userId = QueryHelper.GetInteger("userID", 0);

        // Check that only global administrator can edit global administrator's accouns
        ui = UserInfoProvider.GetUserInfo(userId);
        if (!CheckGlobalAdminEdit(ui))
        {
            plcTable.Visible = false;
            lblError.Text = ResHelper.GetString("Administration-User_List.ErrorGlobalAdmin");
            lblError.Visible = true;
        }

        UserPictureFormControl.IsLiveSite = false;

        //Load user data
        LoadData();
    }


    /// <summary>
    /// Loads data of edited user from DB
    /// </summary>
    protected void LoadData()
    {
        //Check if user exists
        if (ui != null)
        {
            if (!RequestHelper.IsPostBack())
            {
                if ((ui.UserSettings != null) && (ui.UserSettings.UserActivatedByUserID > 0))
                {
                    UserInfo user = UserInfoProvider.GetUserInfo(ui.UserSettings.UserActivatedByUserID);
                    if (user != null)
                    {
                        lblUserFullName.Text = HTMLHelper.HTMLEncode(user.FullName);
                    }
                }

                if (String.IsNullOrEmpty(lblUserFullName.Text))
                {
                    lblUserFullName.Text = ResHelper.GetString("general.na");
                }

                activationDate.SelectedDateTime = ui.UserSettings.UserActivationDate;
                txtCampaign.Text = ui.UserCampaign;
                txtMessageNotifEmail.Text = ui.UserMessagingNotificationEmail;
                txtNickName.Text = ui.UserNickName;
                //txtCustomData.Text = ui.UserCustomData.GetData();
                //txtPreferences.Text = ui.UserSettings.UserPreferences.Value;
                LoadRegInfo(ui.UserSettings);
                timeZone.Value = ui.UserSettings.UserTimeZoneID;
                txtURLReferrer.Text = ui.UserURLReferrer;
                txtUserSignature.Text = ui.UserSignature;
                txtUserDescription.Text = ui.UserSettings.UserDescription;
                chkWaitingForActivation.Checked = ui.UserSettings.UserWaitingForApproval;
                badgeSelector.Value = ui.UserSettings.UserBadgeID;
                txtUserLiveID.Text = ui.UserSettings.WindowsLiveID;
                txtFacebookUserID.Text = ui.UserSettings.UserFacebookID;
                txtOpenID.Text = OpenIDUserInfoProvider.GetOpenIDByUserID(ui.UserID);
                chkUserShowSplashScreen.Checked = ui.UserSettings.UserShowSplashScreen;
                txtUserActivityPoints.Text = ui.UserSettings.UserActivityPoints.ToString();
                lblUserForumPostsValue.Text = ui.UserSettings.UserForumPosts.ToString();
                lblUserBlogPostsValue.Text = ui.UserSettings.UserBlogPosts.ToString();
                lblUserBlogCommentsValue.Text = ui.UserSettings.UserBlogComments.ToString();
                rbtnlGender.SelectedValue = ui.UserSettings.UserGender.ToString();
                dtUserDateOfBirth.SelectedDateTime = ui.UserSettings.UserDateOfBirth;
                lblUserMessageBoardPostsValue.Text = ui.UserSettings.UserMessageBoardPosts.ToString();
            }

            // Load user picture, even for postback
            SetUserPictureArea(ui);
        }
    }


    /// <summary>
    /// Displays user's registration information
    /// </summary>
    /// <param name="ui">User info</param>
    protected void LoadRegInfo(UserSettingsInfo usi)
    {
        if ((usi.UserRegistrationInfo != null) && (usi.UserRegistrationInfo.ColumnNames != null))
        {
            plcUserLastLogonInfo.Controls.Add(new LiteralControl("<br />"));
            foreach (string column in usi.UserRegistrationInfo.ColumnNames)
            {
                Label lbl = new Label();
                lbl.Text = HTMLHelper.HTMLEncode(TextHelper.LimitLength(usi.UserRegistrationInfo[column], 80, "...")) + "<br />";
                lbl.ToolTip = HTMLHelper.HTMLEncode(column + " - " + usi.UserRegistrationInfo[column]);
                plcUserLastLogonInfo.Controls.Add(lbl);
            }

            plcUserLastLogonInfo.Controls.Add(new LiteralControl("<br />"));
        }
        else
        {
            plcUserLastLogonInfo.Controls.Add(new LiteralControl(ResHelper.GetString("general.na") + "<br />"));
        }
    }


    /// <summary>
    /// Saves data of edited user from TextBoxes into DB.
    /// </summary>
    protected void ButtonOK_Click(object sender, EventArgs e)
    {
        // Check "modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Users", "Modify"))
        {
            RedirectToAccessDenied("CMS.Users", "Modify");
        }

        string errorMessage = "";

        // If some email address is set check the format
        if (txtMessageNotifEmail.Text != String.Empty)
        {
            errorMessage = new Validator().IsEmail(txtMessageNotifEmail.Text, ResHelper.GetString("adm.user.messageemailincorrect")).Result;
        }

        if (!UserPictureFormControl.IsValid())
        {
            errorMessage = UserPictureFormControl.ErrorMessage;
        }


        // Clean from empty strings
        txtNickName.Text = txtNickName.Text.Trim();
        txtUserSignature.Text = txtUserSignature.Text.Trim();
        txtUserDescription.Text = txtUserDescription.Text.Trim();
        txtMessageNotifEmail.Text = txtMessageNotifEmail.Text.Trim();


        if ((errorMessage == string.Empty) && (ui != null))
        {
            ui.UserSettings.UserActivationDate = activationDate.SelectedDateTime;
            ui.UserCampaign = txtCampaign.Text;
            ui.UserMessagingNotificationEmail = txtMessageNotifEmail.Text;
            ui.UserNickName = txtNickName.Text;
            ui.UserSettings.WindowsLiveID = txtUserLiveID.Text;
            ui.UserSettings.UserFacebookID = txtFacebookUserID.Text;

            ui.UserSettings.UserTimeZoneID = ValidationHelper.GetInteger(timeZone.Value, 0);
            ui.UserURLReferrer = txtURLReferrer.Text;
            ui.UserSignature = txtUserSignature.Text;
            ui.UserSettings.UserDescription = txtUserDescription.Text;

            ui.UserSettings.UserWaitingForApproval = chkWaitingForActivation.Checked;
            ui.UserSettings.UserBadgeID = ValidationHelper.GetInteger(badgeSelector.Value, 0);

            ui.UserSettings.UserShowSplashScreen = chkUserShowSplashScreen.Checked;

            ui.UserSettings.UserActivityPoints = ValidationHelper.GetInteger(txtUserActivityPoints.Text, 0);
            ui.UserSettings.UserGender = ValidationHelper.GetInteger(rbtnlGender.SelectedValue, 0);
            ui.UserSettings.UserDateOfBirth = dtUserDateOfBirth.SelectedDateTime;

            //xml data - don't save for now
            //ui.UserCustomData.Value = txtCustomData.Text;
            //ui.UserSettings.UserRegistrationInfo.Value = txtRegInfo.Text;

            //Set user picture to DB
            UserPictureFormControl.UpdateUserPicture(ui);
            UserInfoProvider.SetUserInfo(ui);

            // Update OpenID value
            UpdateOpenID(ui);

            //Update user picture on the page
            SetUserPictureArea(ui);

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    /// <summary>
    /// Updates OpenID for given user.
    /// </summary>
    private void UpdateOpenID(UserInfo ui)
    {
        if (ui != null)
        {
            string oldOpenID = OpenIDUserInfoProvider.GetOpenIDByUserID(ui.UserID);
            string newOpenID = txtOpenID.Text.Trim();

            // Update or delete given OpenID related to user
            OpenIDUserInfoProvider.UpdateOpenIDUserInfo(oldOpenID, newOpenID, ui.UserID);
        }
    }


    /// <summary>
    /// Set user picture control
    /// </summary>    
    private void SetUserPictureArea(UserInfo ui)
    {
        UserPictureFormControl.UserInfo = ui;
        UserPictureFormControl.MaxSideSize = 100;
    }
}
