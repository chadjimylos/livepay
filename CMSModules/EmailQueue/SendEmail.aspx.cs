using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.EmailEngine;
using CMS.IEmailEngine;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.ExtendedControls;
using CMS.EventLog;

public partial class CMSModules_EmailQueue_SendEmail : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize validators
        revFrom.ErrorMessage = ResHelper.GetString("System_Email.ErrorEmail");
        rfvFrom.ErrorMessage = ResHelper.GetString("System_Email.EmptyEmail"); ;
        revFrom.ValidationExpression = ValidationHelper.EmailRegExp.ToString();
        revTo.ErrorMessage = revFrom.ErrorMessage;
        rfvTo.ErrorMessage = rfvFrom.ErrorMessage;
        revTo.ValidationExpression = @"^([A-Za-z0-9_\-\+]+(?:\.[A-Za-z0-9_\-\+]+)*@[A-Za-z0-9_-]+(?:\.[A-Za-z0-9_-]+)+(;)?)+$";
        
        // Initialize uploader
        uploader.AddButtonImagePath = GetImageUrl("CMSModules/CMS_EmailQueue/addattachment.png");
        pnlAttachments.GroupingText = ResHelper.GetString("general.attachments");
        
        // Initialize the text editor
        htmlText.AutoDetectLanguage = false;
        htmlText.DefaultLanguage = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        htmlText.ToolbarSet = "SimpleEdit"; 
        htmlText.MediaDialogConfig.UseFullURL = true;
        htmlText.LinkDialogConfig.UseFullURL = true;
        htmlText.QuickInsertConfig.UseFullURL = true;

        // Get e-mail format from global settings
        EmailFormatEnum emailFormat = EmailHelper.GetEmailFormat(SettingsKeyProvider.GetStringValue("CMSEmailFormat"), EmailFormatEnum.Html);
        switch (emailFormat)
        {
            case EmailFormatEnum.Html:
                plcPlainText.Visible = false;
                break;

            case EmailFormatEnum.PlainText:
                plcText.Visible = false;
                break;
        }
    }


    /// <summary>
    /// On btnSend click.
    /// </summary>
    protected void btnSend_Click(object sender, EventArgs e)
    {
        txtFrom.Text = txtFrom.Text.Trim();
        txtTo.Text = txtTo.Text.Trim();

        string result = new Validator().NotEmpty(txtFrom.Text, ResHelper.GetString("System_Email.EmptyEmail")).NotEmpty(txtTo.Text, ResHelper.GetString("System_Email.EmptyEmail")).Result;
        if (result == "")
        {
            // Validate e-mail addresses
            if (ValidationHelper.IsEmail(txtFrom.Text) && ValidationHelper.AreEmails(txtTo.Text))
            {
                // Send the e-mail
                try
                {
                    SendEmail();

                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("System_Email.EmailSent");
                }
                catch (Exception ex)
                {
                    lblError.Visible = true;
                    lblError.Text = ex.Message;
                    lblError.ToolTip = EventLogProvider.GetExceptionLogMessage(ex);
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("System_Email.ErrorEmail");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }


    /// <summary>
    /// Sends the e-mail.
    /// </summary>
    protected void SendEmail()
    {
        EmailMessage msg = new EmailMessage();

        // Set message properties
        msg.From = txtFrom.Text;
        msg.Recipients = txtTo.Text;
        msg.CcRecipients = txtCc.Text;
        msg.BccRecipients = txtBcc.Text;
        msg.Subject = txtSubject.Text;
        if (plcText.Visible)
        {
            msg.Body = htmlText.ResolvedValue;
        }
        if (plcPlainText.Visible)
        {
            msg.PlainTextBody = txtPlainText.Text;
        }

        // Add the attachments
        HttpPostedFile[] attachments = this.uploader.PostedFiles;
        foreach (HttpPostedFile att in attachments)
        {
            msg.Attachments.Add(new Attachment(att.InputStream, att.FileName));
        }

        // Send e-mail
        EmailSender.SendEmail(msg);
    }
}
