using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Modules_Module_Edit_PermissionNames : SiteManagerPage
{    

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Request.QueryString["moduleID"] != null)&&(Request.QueryString["moduleID"].ToString() != ""))
        {        
    
            unigridPermissionList.WhereCondition = "ResourceID = " + Request.QueryString["moduleID"];
            unigridPermissionList.OnAction += new OnActionEventHandler(UniGridPermissionList_OnAction);
            unigridPermissionList.ZeroRowsText = ResHelper.GetString("general.nodatafound");

            // New item link
            string[,] actions = new string[1, 6];
            actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
            actions[0, 1] = ResHelper.GetString("Administration-Module_Edit_PermissionNames.NewPermission");
            actions[0, 2] = null;
            actions[0, 3] = ResolveUrl("Module_Edit_PermissionName_Edit.aspx?moduleID=" + Request.QueryString["moduleID"]);
            actions[0, 4] = null;
            actions[0, 5] = GetImageUrl("Objects/CMS_Module/add.png");
            this.CurrentMaster.HeaderActions.Actions = actions;
        }
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that threw event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGridPermissionList_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("Module_Edit_PermissionName_Edit.aspx?moduleID=" + Request.QueryString["moduleID"] + "&permissionID=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {            
            PermissionNameInfoProvider.DeletePermissionInfo(Convert.ToInt32(actionArgument));        
        }
    }
}
