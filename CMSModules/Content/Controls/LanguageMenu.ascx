<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LanguageMenu.ascx.cs"
    Inherits="CMSModules_Content_Controls_LanguageMenu" %>
<%@ Register Src="~/CMSFormControls/Cultures/SiteCultureSelector.ascx" TagName="SiteCultureSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/UniMenu/UniMenuButtons.ascx" TagName="UniMenuButtons"
    TagPrefix="cms" %>
<asp:Panel runat="server" ID="pnlLang" CssClass="ContentMenuLang">
    <cms:SiteCultureSelector runat="server" ID="cultureSelector" IsLiveSite="false" />
</asp:Panel>
<cms:UniMenuButtons ID="buttons" runat="server" EnableViewState="false" AllowSelection="true" />
