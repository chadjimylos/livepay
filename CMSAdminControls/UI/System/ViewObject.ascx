<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewObject.ascx.cs" Inherits="CMSAdminControls_UI_System_ViewObject" %>
<asp:Literal runat="server" ID="ltlData" EnableViewState="false" />
<div style="width: 90%;">
    <asp:PlaceHolder ID="pnlContent" runat="server" />
</div>
