CREATE TABLE [Newsletter_Subscriber] (
		[SubscriberID]               int NOT NULL IDENTITY(1, 1),
		[SubscriberEmail]            nvarchar(400) NULL,
		[SubscriberFirstName]        nvarchar(200) NULL,
		[SubscriberLastName]         nvarchar(200) NULL,
		[SubscriberSiteID]           int NOT NULL,
		[SubscriberGUID]             uniqueidentifier NOT NULL,
		[SubscriberCustomData]       nvarchar(max) NULL,
		[SubscriberType]             nvarchar(100) NULL,
		[SubscriberRelatedID]        int NULL,
		[SubscriberLastModified]     datetime NOT NULL,
		[SubscriberFullName]         nvarchar(440) NULL
)
ON [PRIMARY]
ALTER TABLE [Newsletter_Subscriber]
	ADD
	CONSTRAINT [PK_Newsletter_Subscriber]
	PRIMARY KEY
	NONCLUSTERED
	([SubscriberID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Newsletter_Subscriber]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_Subscriber_SubscriberSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_Subscriber]
			ADD CONSTRAINT [FK_Newsletter_Subscriber_SubscriberSiteID_CMS_Site]
			FOREIGN KEY ([SubscriberSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_Newsletter_Subscriber_SubscriberSiteID_SubscriberFullName]
	ON [Newsletter_Subscriber] ([SubscriberSiteID], [SubscriberFullName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Newsletter_Subscriber_SubscriberType_SubscriberRelatedID]
	ON [Newsletter_Subscriber] ([SubscriberType], [SubscriberRelatedID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
