<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LargeTextAreaDesigner.aspx.cs"
    Inherits="CMSFormControls_Selectors_LargeTextAreaDesigner" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master" Title="Edit text" %>


<%@ Register Src="~/CMSAdminControls/UI/Selectors/MacroSelector.ascx" TagName="MacroSelector"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">

    <script type="text/javascript">
        //<![CDATA[
        // Inserts the text to caret position (in text area)
        function InsertMacroPlain(text) {
            if (txtElem != null) {
                if (document.selection) {
                    // IE
                    txtElem.focus();
                    var orig = txtElem.value.replace(/\r\n/g, "\n");
                    var range = document.selection.createRange();
                    if (range.parentElement() != txtElem) {
                        return false;
                    }
                    range.text = text;
                    var actual = tmp = txtElem.value.replace(/\r\n/g, "\n");
                    for (var diff = 0; diff < orig.length; diff++) {
                        if (orig.charAt(diff) != actual.charAt(diff)) break;
                    }
                    for (var index = 0, start = 0; tmp.match(text) && (tmp = tmp.replace(text, "")) && index <= diff; index = start + text.length) {
                        start = actual.indexOf(text, index);
                    }
                } else {
                    // Firefox
                    var start = txtElem.selectionStart;
                    var end = txtElem.selectionEnd;
                    txtElem.value = txtElem.value.substr(0, start) + text + txtElem.value.substr(end, txtElem.value.length);
                }
                if (start != null) {
                    setCaretTo(txtElem, start + text.length);
                } else {
                    txtElem.value += text;
                }
            }
        }

        // Sets the location of the cursor in specified object to given position
        function setCaretTo(obj, pos) {
            if (obj != null) {
                if (obj.createTextRange) {
                    var range = obj.createTextRange();
                    range.move('character', pos);
                    range.select();
                } else if (obj.selectionStart) {
                    obj.focus();
                    obj.setSelectionRange(pos, pos);
                }
            }
        };
        //]]>
    </script>

    <asp:Panel runat="server" ID="pnlContent" CssClass="PageContent">
        <cms:ExtendedTextArea ID="txtLargeArea" runat="server" TextMode="MultiLine" Height="430px"
            CssClass="FullTextArea" />
        <br />
        <br />
        <cms:MacroSelector ID="macroSelectorElem" runat="server" ShowExtendedControls="true" />
    </asp:Panel>
    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
</asp:Content>
<asp:Content ID="cntFooter" ContentPlaceHolderID="plcFooter" runat="server">
    <div class="FloatRight">
        <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" /><cms:CMSButton
            ID="btnCancel" runat="server" CssClass="SubmitButton" />
        <asp:Literal ID="ltlOk" runat="server" EnableViewState="false" />
    </div>
</asp:Content>
