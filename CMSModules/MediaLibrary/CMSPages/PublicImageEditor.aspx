<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PublicImageEditor.aspx.cs" Inherits="CMSModules_MediaLibrary_CMSPages_PublicImageEditor"
    Theme="Default" MasterPageFile="~/CMSMasterPages/LiveSite/Dialogs/ModalDialogPage.master"
    Title="Edit image" %>

<%@ Register Src="~/CMSModules/MediaLibrary/Controls/MediaLibrary/ImageEditor.ascx"
    TagName="ImageEditor" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <div class="PageContent">
        <cms:ImageEditor ID="imageEditor" runat="server" EnableViewState="true" />
    </div>
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:LocalizedButton ID="btnClose" runat="server" OnClientClick="Close(); return false;"
            CssClass="SubmitButton" ResourceString="general.close" />
    </div>
</asp:Content>
