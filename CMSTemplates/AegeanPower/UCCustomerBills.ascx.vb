﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports CMS.Controls

Partial Class CMSTemplates_AegeanPower_UCCustomerBills
    Inherits CMSUserControl

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If

        If Not StopProcessing AndAlso (RequestHelper.IsPostBack()) Then
            ReloadDataAfterPostBack()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        SetAttributes()
        Bind()
    End Sub

    Public Overloads Sub ReloadDataAfterPostBack()
    End Sub

    Private Sub SetAttributes()
        SetUserProviders()
        hiddenYear.Text = Now.Year
    End Sub

    Private Sub SetUserProviders()
        Dim UserID As Integer = CMSContext.CurrentUser.UserID
        Dim dt As DataTable = AegeanPower_DBConnection.GetUserProviders(UserID)
        rptProviders.DataSource = dt
        rptProviders.DataBind()

        If dt.Rows.Count < 4 Then
            BillRptCenterMain.Attributes.Add("class", "BillRptCenterMainSmall")
        Else
            BillRptCenterMain.Attributes.Add("class", "BillRptCenterMain")
        End If
        If dt.Rows.Count > 0 Then

            hiddenProviderNumber.Text = dt.Rows(0)("Provider_Number").ToString
            LlbProviderNo.InnerText = dt.Rows(0)("Provider_Number").ToString
            rptTopValue.InnerHtml = dt.Rows(0)("Provider_Number").ToString
        Else
            hiddenProviderNumber.Text = 0
            LlbProviderNo.InnerHtml = "Δεν Βρέθηκε"
            rptTopValue.InnerHtml = "Δεν Βρέθηκε"
        End If
        
    End Sub

    Private Sub Bind()
        Dim ProviderNo As String = hiddenProviderNumber.Text
        Dim DataYear As Integer = hiddenYear.Text
        Dim ViewType As Integer = hiddenViewType.Text
        If ProviderNo = 0 Then
            LlbProviderNo.InnerText = "Δεν Βρέθηκε"
            rptTopValue.InnerHtml = "Δεν Βρέθηκε"
            DivNoActiveAFM.Visible = True
        Else
            LlbProviderNo.InnerText = ProviderNo
            rptTopValue.InnerHtml = ProviderNo
        End If
        
        Dim dt As DataTable = AegeanPower_DBConnection.GetCustomerBills(ProviderNo, DataYear, ViewType, 1)
        GvBills.DataSource = dt
        GvBills.DataBind()

        If ViewType = 0 Then
            LnkViewAllBills.CssClass = "BillsGvShortLnks_Bold"
            LnkViewLastBill.CssClass = "BillsGvShortLnks"

            BillTopGvLnkAllBill.Attributes.Add("class", "BillViewTypeAllDiv")
            BillTopGvLnkLastBill.Attributes.Add("class", "BillViewTypeLastDiv")
        Else
            BillTopGvLnkAllBill.Attributes.Add("class", "BillViewTypeAllDivSel")
            BillTopGvLnkLastBill.Attributes.Add("class", "BillViewTypeLastDivSel")
            LnkViewAllBills.CssClass = "BillsGvShortLnks"
            LnkViewLastBill.CssClass = "BillsGvShortLnks_Bold"
        End If

        PagerMain.Visible = False
        PagerLeft.Visible = False
        PagerCenter.Visible = False
        PagerRight.Visible = False
        PagerlnkLastYear.Text = String.Empty
        PagerLnkNextYear.Text = String.Empty
        If dt.Rows.Count > 0 AndAlso ViewType = 0 Then
            Dim LastYear As Integer = dt.Rows(0)("LastYear").ToString
            Dim NextYear As Integer = dt.Rows(0)("NextYear").ToString
            If LastYear <> 0 Then
                PagerMain.Visible = True
                PagerLeft.Visible = True
                PagerlnkLastYear.Text = LastYear
            Else
                PagerCenter.Visible = True
            End If
            If NextYear <> 0 Then
                PagerMain.Visible = True
                PagerRight.Visible = True
                PagerLnkNextYear.Text = NextYear
            End If
            'PagerlnkLastYear
        End If
    End Sub

    Protected Sub GvBills_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvBills.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lblIssueDate As Label = DirectCast(e.Row.FindControl("lblIssueDate"), Label)
            Dim lblAmmount As Label = DirectCast(e.Row.FindControl("lblAmmount"), Label)
            Dim lblPayment_Code As Label = DirectCast(e.Row.FindControl("lblPayment_Code"), Label)
            Dim imgPayTheBill As HtmlImage = DirectCast(e.Row.FindControl("imgPayTheBill"), HtmlImage)
            If Not IsNothing(lblIssueDate) Then
                Dim dtm As Date = lblIssueDate.Text
                lblIssueDate.Text = String.Concat("<b>", dtm.ToString("MMMM"), "</b>", " ", Year(dtm))
                Dim Amount As Integer = Replace(lblAmmount.Text, " &euro;", String.Empty) * 100
                Dim PaymentCodeEncrypt As String = CryptoFunctions.EncryptString(lblPayment_Code.Text)
                PaymentCodeEncrypt = Server.UrlEncode(PaymentCodeEncrypt)
                Dim AmountEncrypt As String = CryptoFunctions.EncryptString(Amount)
                AmountEncrypt = Server.UrlEncode(AmountEncrypt)
                imgPayTheBill.Attributes.Add("onclick", "window.open('/Transaction/Initiation.aspx?lang=GR&Am=" & AmountEncrypt & "&PC=" & PaymentCodeEncrypt & "','','')")
            End If
        End If
    End Sub

    Protected Sub PagerlnkLastYear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PagerlnkLastYear.Click
        Bind()
    End Sub

    Protected Sub PagerLnkNextYear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles PagerLnkNextYear.Click
        Bind()
    End Sub

    Protected Sub LnkViewAllBills_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkViewAllBills.Click
        Bind()
    End Sub

    Protected Sub LnkViewLastBill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkViewLastBill.Click
        Bind()
    End Sub

    Protected Sub lnkRptProvider_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Bind()
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

End Class