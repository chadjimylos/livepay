using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Polls;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.SiteProvider;

public partial class CMSModules_Polls_Controls_PollNew : CMSAdminEditControl
{
    #region "Variables"

    private int mGroupID = 0;
    private int mSiteID = 0;
    private Guid mGroupGUID = Guid.Empty;
    private string mLicenseError = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the site ID for which the poll should be created
    /// </summary>
    public int SiteID
    {
        get
        {
            if (this.mSiteID <= 0)
            {
                this.mSiteID = CMSContext.CurrentSiteID;
            }

            return this.mSiteID;
        }
        set
        {
            this.mSiteID = value;
        }
    }


    /// <summary>
    /// Gets or sets the group ID for which the poll should be created
    /// </summary>
    public int GroupID
    {
        get
        {
            return this.mGroupID;
        }
        set
        {
            this.mGroupID = value;
        }
    }


    /// <summary>
    /// Gets or sets the group GUID for which the poll should be created
    /// </summary>
    public Guid GroupGUID
    {
        get
        {
            return this.mGroupGUID;
        }
        set
        {
            this.mGroupGUID = value;
        }
    }


    /// <summary>
    /// Gets or sets license error message
    /// </summary>
    public string LicenseError
    {
        get
        {
            return mLicenseError;
        }
        set
        {
            mLicenseError = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Hide code name editing in simple mode
        if (DisplayMode == ControlDisplayModeEnum.Simple)
        {
            plcCodeName.Visible = false;
        }

        // Init the labels
        rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvQuestion.ErrorMessage = ResHelper.GetString("Polls_New.QuestionError");
        rfvMaxLength.ErrorMessage = ResHelper.GetString("general.errortexttoolong");

        lblTitle.Text = ResHelper.GetString("Polls_New.TitleLabel");
        lblQuestion.Text = ResHelper.GetString("Polls_New.QuestionLabel");
        btnOk.Text = ResHelper.GetString("General.OK");

        // Init breadcrumbs
        string[,] breadcrumbs = new string[2, 3];

        if (!RequestHelper.IsPostBack())
        {
            this.ClearForm();
        }
    }


    /// <summary>
    /// Resets all boxes.
    /// </summary>
    public override void ClearForm()
    {
        txtCodeName.Text = null;
        txtDisplayName.Text = null;
        txtTitle.Text = null;
        txtQuestion.Text = null;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (!CheckPermissions("cms.polls", CMSAdminControl.PERMISSION_MODIFY))
        {
            return;
        }

        // Generate code name in simple mode
        string codeName = txtCodeName.Text.Trim();
        if (DisplayMode == ControlDisplayModeEnum.Simple)
        {
            codeName = ValidationHelper.GetCodeName(txtDisplayName.Text.Trim(), null, null);
        }

        // Perform validation
        string errorMessage = new Validator().NotEmpty(codeName, rfvCodeName.ErrorMessage)
            .NotEmpty(txtDisplayName.Text, rfvDisplayName.ErrorMessage).NotEmpty(txtQuestion.Text.Trim(), rfvQuestion.ErrorMessage).Result;

        // Check CodeName for identificator format
        if (!ValidationHelper.IsCodeName(codeName))
        {
            errorMessage = ResHelper.GetString("General.ErrorCodeNameInIdentificatorFormat");
        }

        if (string.IsNullOrEmpty(errorMessage))
        {
            bool isnew = false;

            // Create new 
            PollInfo pollObj = new PollInfo();
            pollObj.PollAllowMultipleAnswers = false;
            pollObj.PollAccess = SecurityAccessEnum.AllUsers;

            // Check if codename already exists on a group or is a global
            PollInfo pi = null;

            if (this.GroupID == 0)
            {
                pi = PollInfoProvider.GetPollInfo(codeName);
            }
            else
            {
                pi = PollInfoProvider.GetPollInfo(codeName, this.SiteID, this.GroupID);
            }

            if (pi == null)
            {
                // Set the fields
                pollObj.PollCodeName = codeName;
                pollObj.PollDisplayName = txtDisplayName.Text.Trim();
                pollObj.PollTitle = txtTitle.Text.Trim();
                pollObj.PollQuestion = txtQuestion.Text.Trim();
                if (this.GroupID > 0)
                {
                    if (this.SiteID <= 0)
                    {
                        lblError.Text = ResHelper.GetString("polls.nositeid");
                        lblError.Visible = true;
                        return;
                    }

                    pollObj.PollGroupID = this.GroupID;
                    pollObj.PollSiteID = this.SiteID;
                }

                // Save the object
                PollInfoProvider.SetPollInfo(pollObj);
                this.ItemID = pollObj.PollID;
                isnew = true;

                // Add poll to current site
                if (CMSContext.CurrentSite != null)
                {
                    if (PollInfoProvider.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Polls, VersionActionEnum.Insert))
                    {
                        if ((pollObj.PollGroupID == 0) && (pollObj.PollSiteID == 0))
                        {
                            // Bind only global polls to current site
                            PollInfoProvider.AddPollToSite(pollObj.PollID, CMSContext.CurrentSiteID);
                        }
                    }
                    else
                    {
                        this.LicenseError = ResHelper.GetString("LicenseVersion.Polls");
                    }
                }

                // Redirect to edit mode
                if ((isnew) && (!lblError.Visible))
                {
                    RaiseOnSaved();
                }
            }
            else
            {
                // Error message - code name already exists
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("polls.codenameexists");
            }
        }
        else
        {
            // Error message - validation
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
