<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Category_Edit.aspx.cs" Inherits="CMSSiteManager_Development_Categories_Category_Edit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" enableviewstate="false">
    <title>Categories</title>
</head>
<frameset border="0" rows="102, *" id="rowsFrameset">
		<frame name="menu" src="Category_Edit_Header.aspx?categoryID=<%=Request.QueryString["categoryID"] %> " scrolling="no" frameborder="0" noresize="noresize" />		
	    <frame name="content" src="Category_Edit_General.aspx?categoryID=<%=Request.QueryString["categoryID"]%>&saved=<%=Request.QueryString["saved"]%>  " frameborder="0" noresize="noresize" />
	<noframes>
    <body>
        <p id="p2">
            This HTML frameset displays multiple Web pages. To view this frameset, use a Web
            browser that supports HTML 4.0 and later.
        </p>
    </body>
</noframes>
</frameset>
</html>
