﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports ESB

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://livepay.gr/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class Bridge
    Inherits System.Web.Services.WebService

    Dim cl As POSServiceSoapClient
    Dim cInfo As ClientInfo
    Dim rInfo As RequestInfo
    Dim headers As EFGTransportHeaders

#Region "PRIVATE OBJECTS"
    Sub PrepareESB()
        If (cl IsNot Nothing) Then Return

        cl = New POSServiceSoapClient()
        cInfo = New ClientInfo()
        cInfo.customerID = 10
        cInfo.customerIDSpecified = True
        cInfo.entityID = 26
        cInfo.entityIDSpecified = True
        cInfo.channel = Channel.LivePay
        cInfo.channelSpecified = True
        cInfo.userid = System.Configuration.Configurationmanager.AppSettings("ESBUserID") 
        cInfo.virtualUserid = System.Configuration.Configurationmanager.AppSettings("ESBVirtualUserID") 
        rInfo = New RequestInfo()
        rInfo.businessTransactionID = "bbbbbbbbbbbbbbbb"

        If (headers Is Nothing) Then
            Dim s As New signon()
            s.userName = System.Configuration.Configurationmanager.AppSettings("ESBVirtualUserID") 
            s.passwd = System.Configuration.Configurationmanager.AppSettings("ESBVirtualUserPassword") 
            headers = New EFGTransportHeaders()
            headers.Password = s.passwd
            headers.UserName = s.userName
            Dim sresp As signonResp = cl.signon(headers, cInfo, rInfo, s)
            headers.EFGToken = sresp.efgToken
        End If
    End Sub

    Sub LogInputCall(ByVal obj As Object)
        'Try
        Dim str As New IO.StreamWriter("D:\LivepayLogs\log.xml", True)
        str.WriteLine() : str.WriteLine()
        str.WriteLine(Now)
        str.Close()

        Dim objStreamWriter As New IO.MemoryStream
        Dim x As New System.Xml.Serialization.XmlSerializer(obj.GetType)
        x.Serialize(objStreamWriter, obj)

        Dim i As New IO.FileStream("D:\LivepayLogs\log.xml", IO.FileMode.Append)
        i.Write(objStreamWriter.ToArray, 0, objStreamWriter.ToArray.Length)
        i.Close()
        objStreamWriter.Close()
        'Catch ex As Exception
        'End Try
    End Sub
#End Region

#Region "PUBLIC OBJECTS"
    Public Structure PaymentObj
        Dim CardNumber As String
        Dim CardType As ESB.CardEnum
        Dim CardCvv2 As String
        Dim CardExpiryDate As String
        Dim CustomerComments As String
        Dim CustomerEmail As String
        Dim CustomerId As String
        Dim CustomerName As String
        Dim CustomerTelephone As String
        Dim TransactionId As Integer
        Dim TransactionAmount As Double
        Dim Installments As Integer
        Dim Info1 As String
        Dim Info2 As String
        Dim Info3 As String
        Dim Info4 As String
        Dim Info5 As String
        Dim Info6 As String
        Dim Info7 As String
        Dim Info8 As String
        Dim Info9 As String
        Dim Info10 As String
        Dim MerchantId As String
        Dim [Public] As Boolean
    End Structure

    Public Structure PaymentResponse
        Dim Result As Boolean
        Dim ErrorCode As String
        Dim ErrorMessage As String
        Dim ErrorDetails As String
    End Structure

    Public Structure CommissionResponse
        Dim Result As Double
        Dim ErrorCode As String
        Dim ErrorMessage As String
        Dim ErrorDetails As String
    End Structure

    Public Structure CloseTransactionServiceResponse
        Dim Result As Boolean
        Dim ErrorCode As String
        Dim ErrorMessage As String
        Dim ErrorDetails As String
    End Structure

    Public Structure TransactionDetailsResponse
        Dim TransactionDetails As TransactionDetailsInfo
        Dim ErrorCode As String
        Dim ErrorMessage As String
        Dim ErrorDetails As String
    End Structure

    Public Structure MerchantStatisticsResponse
        Dim MerchantStatistics As MerchantStatisticInfo
        Dim ErrorCode As String
        Dim ErrorMessage As String
        Dim ErrorDetails As String
    End Structure

    Public Structure GetTransactionsObj
        Dim PageNumber As Integer
        Dim PageSize As Integer
        Dim AmountFrom As Double
        Dim amountTo As Double
        Dim BatchClosed As Boolean
        Dim BatchClosedSpecified As Boolean
        Dim BatchDateFrom As DateTime
        Dim BatchDateTo As DateTime
        Dim BatchIdFrom As Long
        Dim BatchIdTo As Long
        Dim CardNumber As String
        Dim CustomerId As String
        Dim CustomerName As String
        Dim MerchantId As String
        Dim OriginalTransactionID As Integer
        Dim TransactionDateFrom As DateTime
        Dim TransactionDateTo As DateTime
        Dim TransactionResult As TxnResultEnum?()
        Dim TransactionResultSpecified As Boolean
        Dim TransactionType As TxnTypeEnum?()
        Dim TransactionTypeSpecified As Boolean
        Dim info1 As String
        Dim info2 As String
        Dim info3 As String
        Dim info4 As String
        Dim info5 As String
    End Structure

    Public Structure GetTransactionsResponse
        Dim Transactions() As TransactionInfo
        Dim TotalRecords As Integer
        Dim ErrorCode As String
        Dim ErrorMessage As String
        Dim ErrorDetails As String
    End Structure

    Public Structure RefundTransactionServiceResponse
        Dim Result As Boolean
        Dim ErrorCode As String
        Dim ErrorMessage As String
        Dim ErrorDetails As String
    End Structure

    Public Structure SetAutoCloseBatchTimeServiceResponse
        Dim Result As Boolean
        Dim ErrorCode As String
        Dim ErrorMessage As String
        Dim ErrorDetails As String
    End Structure

    Public Structure CancelTransactionServiceResponse
        Dim Result As Boolean
        Dim ErrorCode As String
        Dim ErrorMessage As String
        Dim ErrorDetails As String
    End Structure
#End Region

    <WebMethod()> _
    Public Function GetLogs(ByVal Level As LevelEnum) As LogInfo()
        PrepareESB()
        Dim input As getLogs = New getLogs()
        input.systemId = System.Configuration.ConfigurationManager.AppSettings("SystemID") 'must be a 2 digit number
        input.dateFrom = DateTime.Now
        input.dateFromSpecified = True
        input.dateTo = DateTime.Now
        input.dateToSpecified = True
        input.level = Level
        input.levelSpecified = True
        Dim res As LogInfo() = cl.getLogs(headers, cInfo, rInfo, input)
        Return res
    End Function

    <WebMethod()> _
    Public Function CloseTransaction(ByVal MerchantID As String) As CloseTransactionServiceResponse
        PrepareESB()
        Dim input As New closeTransaction()
        input.systemId = System.Configuration.ConfigurationManager.AppSettings("SystemID")
        input.merchantId = MerchantID

        Dim res As closeTransactionResp = cl.closeTransaction(headers, cInfo, rInfo, input)
        Dim r As New CloseTransactionServiceResponse
        r.Result = res.result
        r.ErrorCode = res.errorCode
        r.ErrorMessage = res.errorMessage
        Return r
    End Function

    <WebMethod()> _
    Function GetCommission(ByVal CardNumber As String, ByVal ServiceCode As String, ByVal Amount As String) As CommissionResponse
        PrepareESB()
        Dim input As New getPaymentCommission
        input.cardNumber = CardNumber
        input.amount = Amount
        input.amountSpecified = True
        input.serviceCode = ServiceCode

        LogInputCall(input)

        Dim p As New CommissionResponse

        Try
            Dim res As getPaymentCommissionResp = cl.getPaymentCommission(headers, cInfo, rInfo, input)
            p.Result = res.commissionAmount
            p.ErrorCode = 0
        Catch ex As Exception
            Try
                Dim err As WSServiceError = DirectCast(ex, System.ServiceModel.FaultException(Of ESB.WSServiceError)).Detail
                p.ErrorCode = err.rc
                p.ErrorMessage = err.text
            Catch ex2 As Exception
                p.ErrorCode = 300
                p.ErrorMessage = ex2.ToString
            End Try
        End Try
        Return p
    End Function

    <WebMethod()> _
    Function MakePayment(ByVal PaymentObject As PaymentObj) As PaymentResponse
        PrepareESB()
        Dim input As New makePayment()
        input.systemId = System.Configuration.ConfigurationManager.AppSettings("SystemID")
        input.cardNumber = PaymentObject.CardNumber
        input.cardType = PaymentObject.CardType
        input.cardTypeSpecified = True
        input.cardCvv2 = PaymentObject.CardCvv2
        input.cardExpiryDate = PaymentObject.CardExpiryDate
        input.customerComments = PaymentObject.CustomerComments
        input.customerEmail = PaymentObject.CustomerEmail
        input.customerId = PaymentObject.CustomerId
        input.customerName = PaymentObject.CustomerName
        input.customerTelephone = PaymentObject.CustomerTelephone
        input.transactionId = PaymentObject.TransactionId
        input.transactionAmount = PaymentObject.TransactionAmount
        input.transactionAmountSpecified = True
        input.installments = IIf(PaymentObject.Installments > 0, PaymentObject.Installments, 0)
        input.installmentsSpecified = True
        input.info1 = PaymentObject.Info1
        input.info2 = PaymentObject.Info2
        input.info3 = PaymentObject.Info3
        input.info4 = PaymentObject.Info4
        input.info5 = PaymentObject.Info5
        input.info6 = PaymentObject.Info6
        input.info7 = PaymentObject.Info7
        input.info8 = PaymentObject.Info8
        input.info9 = PaymentObject.Info9
        input.info10 = PaymentObject.Info10
        input.merchantId = PaymentObject.MerchantId

        input.public = PaymentObject.Public
        input.publicSpecified = True

        LogInputCall(input)

        Dim p As New PaymentResponse

        Try
            Dim res As makePaymentResp = cl.makePayment(headers, cInfo, rInfo, input)
            p.Result = res.result
            p.ErrorCode = res.errorCode
            p.ErrorMessage = res.errorMessage
            p.ErrorDetails = res.errorCode
        Catch ex As Exception
            Dim err As WSServiceError = DirectCast(ex, System.ServiceModel.FaultException(Of ESB.WSServiceError)).Detail
            p.ErrorCode = err.applErrorCode
            p.ErrorMessage = err.text

        End Try
        Return p
    End Function

    <WebMethod()> _
    Function GetTransactionDetails(ByVal TransactionID As String, ByVal SystemType As ESB.SystemEnum) As TransactionDetailsResponse
        PrepareESB()
        Dim input As New getTransactionDetails()
        input.transactionId = TransactionID
        input.systemId = System.Configuration.ConfigurationManager.AppSettings("SystemID")
        input.system = SystemType
        input.systemSpecified = True

        LogInputCall(input)

        Dim p As New TransactionDetailsResponse
        Try
            Dim res As getTransactionDetailsResp = cl.getTransactionDetails(headers, cInfo, rInfo, input)
            p.TransactionDetails = res.transactionDetailsInfo
        Catch ex As Exception
            Try
                Dim err As WSServiceError = DirectCast(ex, System.ServiceModel.FaultException(Of ESB.WSServiceError)).Detail
                p.ErrorCode = err.rc
                p.ErrorMessage = err.text
            Catch ex2 As Exception
                p.ErrorCode = 1
                p.ErrorMessage = ex2.ToString
            End Try

        End Try
        Return p
    End Function

    <WebMethod()> _
    Function GetMerchantStatistics(ByVal GetTransactionsObject As GetTransactionsObj) As MerchantStatisticsResponse
        PrepareESB()
        Dim input As getMerchantStatistics = New getMerchantStatistics()
        input.systemId = System.Configuration.ConfigurationManager.AppSettings("SystemID")

        If GetTransactionsObject.AmountFrom > -1 Then input.amountFrom = GetTransactionsObject.AmountFrom
        input.amountFromSpecified = (GetTransactionsObject.AmountFrom > -1)

        If GetTransactionsObject.amountTo > -1 Then input.amountTo = GetTransactionsObject.amountTo
        input.amountToSpecified = (GetTransactionsObject.amountTo > -1)

        If GetTransactionsObject.BatchClosedSpecified Then
            input.batchClosed = input.batchClosed
            input.batchClosedSpecified = True
        End If

        'input.batchClosed = False
        'input.batchClosedSpecified = True

        If GetTransactionsObject.BatchDateFrom > DateTime.MinValue Then input.batchDateFrom = GetTransactionsObject.BatchDateFrom
        input.batchDateFromSpecified = (GetTransactionsObject.BatchDateFrom > DateTime.MinValue)

        If GetTransactionsObject.BatchDateTo > DateTime.MinValue Then input.batchDateTo = GetTransactionsObject.BatchDateTo
        input.batchDateToSpecified = (GetTransactionsObject.BatchDateTo > DateTime.MinValue)


        If GetTransactionsObject.BatchIdFrom > 0 Then input.batchIdFrom = GetTransactionsObject.BatchIdFrom
        input.batchIdFromSpecified = (GetTransactionsObject.BatchIdFrom > 0)

        If GetTransactionsObject.BatchIdTo > 0 Then input.batchIdTo = GetTransactionsObject.BatchIdTo
        input.batchIdToSpecified = (GetTransactionsObject.BatchIdTo > 0)

        If GetTransactionsObject.CardNumber IsNot Nothing Then input.cardNumber = GetTransactionsObject.CardNumber
        If GetTransactionsObject.CustomerId IsNot Nothing Then input.customerId = GetTransactionsObject.CustomerId
        If GetTransactionsObject.CustomerName IsNot Nothing Then input.customerName = GetTransactionsObject.CustomerName
        If GetTransactionsObject.MerchantId IsNot Nothing Then input.merchantId = GetTransactionsObject.MerchantId

        If GetTransactionsObject.TransactionDateFrom > DateTime.MinValue Then input.transactionDateFrom = GetTransactionsObject.TransactionDateFrom
        input.transactionDateFromSpecified = (GetTransactionsObject.TransactionDateFrom > DateTime.MinValue)

        If GetTransactionsObject.TransactionDateTo > DateTime.MinValue Then input.transactionDateTo = GetTransactionsObject.TransactionDateTo
        input.transactionDateToSpecified = (GetTransactionsObject.TransactionDateTo > DateTime.MinValue)

        If GetTransactionsObject.TransactionResultSpecified Then input.transactionResult = GetTransactionsObject.TransactionResult

        If GetTransactionsObject.TransactionTypeSpecified Then input.transactionType = GetTransactionsObject.TransactionType

        Dim p As New MerchantStatisticsResponse
        Try
            Dim res As getMerchantStatisticsResp = cl.getMerchantStatistics(headers, cInfo, rInfo, input)
            p.MerchantStatistics = res.merchantStatisticInfo
        Catch ex As Exception
            Dim err As WSServiceError = DirectCast(ex, System.ServiceModel.FaultException(Of ESB.WSServiceError)).Detail
            p.ErrorCode = err.rc
            p.ErrorMessage = err.text
        End Try
        Return p
    End Function

    <WebMethod()> _
    Public Function GetTransactions(ByVal GetTransactionsObject As GetTransactionsObj) As GetTransactionsResponse
        PrepareESB()

        Dim input As getTransactions = New getTransactions()
        input.systemId = System.Configuration.ConfigurationManager.AppSettings("SystemID")

        input.pageSize = GetTransactionsObject.PageSize
        input.pageSizeSpecified = True

        input.pageNumber = GetTransactionsObject.PageNumber
        input.pageNumberSpecified = (GetTransactionsObject.PageNumber > -1)

        If GetTransactionsObject.AmountFrom > -1 Then input.amountFrom = GetTransactionsObject.AmountFrom
        input.amountFromSpecified = (GetTransactionsObject.AmountFrom > -1)

        If GetTransactionsObject.amountTo > -1 Then input.amountTo = GetTransactionsObject.amountTo
        input.amountToSpecified = (GetTransactionsObject.amountTo > -1)

        If GetTransactionsObject.BatchClosedSpecified Then
            input.batchClosed = input.batchClosed
            input.batchClosedSpecified = True
        End If

        'input.batchClosed = False
        'input.batchClosedSpecified = True

        If GetTransactionsObject.OriginalTransactionID > 0 Then input.originalTransactionId = GetTransactionsObject.OriginalTransactionID

        If GetTransactionsObject.BatchDateFrom > DateTime.MinValue Then input.batchDateFrom = GetTransactionsObject.BatchDateFrom
        input.batchDateFromSpecified = (GetTransactionsObject.BatchDateFrom > DateTime.MinValue)

        If GetTransactionsObject.BatchDateTo > DateTime.MinValue Then input.batchDateTo = GetTransactionsObject.BatchDateTo
        input.batchDateToSpecified = (GetTransactionsObject.BatchDateTo > DateTime.MinValue)


        If GetTransactionsObject.BatchIdFrom > 0 Then input.batchIdFrom = GetTransactionsObject.BatchIdFrom
        input.batchIdFromSpecified = (GetTransactionsObject.BatchIdFrom > 0)

        If GetTransactionsObject.BatchIdTo > 0 Then input.batchIdTo = GetTransactionsObject.BatchIdTo
        input.batchIdToSpecified = (GetTransactionsObject.BatchIdTo > 0)

        If GetTransactionsObject.CardNumber IsNot Nothing Then input.cardNumber = GetTransactionsObject.CardNumber
        If GetTransactionsObject.CustomerId IsNot Nothing Then input.customerId = GetTransactionsObject.CustomerId
        If GetTransactionsObject.CustomerName IsNot Nothing Then input.customerName = GetTransactionsObject.CustomerName
        If GetTransactionsObject.MerchantId IsNot Nothing Then input.merchantId = GetTransactionsObject.MerchantId

        If GetTransactionsObject.TransactionDateFrom > DateTime.MinValue Then input.transactionDateFrom = GetTransactionsObject.TransactionDateFrom
        input.transactionDateFromSpecified = (GetTransactionsObject.TransactionDateFrom > DateTime.MinValue)

        If GetTransactionsObject.TransactionDateTo > DateTime.MinValue Then input.transactionDateTo = GetTransactionsObject.TransactionDateTo
        input.transactionDateToSpecified = (GetTransactionsObject.TransactionDateTo > DateTime.MinValue)

        If GetTransactionsObject.TransactionResultSpecified Then input.transactionResult = GetTransactionsObject.TransactionResult

        If GetTransactionsObject.TransactionTypeSpecified Then input.transactionType = GetTransactionsObject.TransactionType

        If GetTransactionsObject.info1 <> String.Empty Then input.info1 = GetTransactionsObject.info1
        If GetTransactionsObject.info2 <> String.Empty Then input.info1 = GetTransactionsObject.info2
        If GetTransactionsObject.info3 <> String.Empty Then input.info1 = GetTransactionsObject.info3
        If GetTransactionsObject.info4 <> String.Empty Then input.info1 = GetTransactionsObject.info4
        If GetTransactionsObject.info5 <> String.Empty Then input.info1 = GetTransactionsObject.info5

        LogInputCall(input)

        Dim p As New GetTransactionsResponse
        Try
            Dim res As getTransactionsResp = cl.getTransactions(headers, cInfo, rInfo, input)
            p.TotalRecords = res.totalRecordsCount
            p.Transactions = res.transactions

        Catch ex As Exception
            Try
                Dim err As WSServiceError = DirectCast(ex, System.ServiceModel.FaultException(Of ESB.WSServiceError)).Detail
                p.ErrorCode = err.rc
                p.ErrorMessage = err.text
            Catch ex2 As Exception
                Dim err As System.ServiceModel.CommunicationException = DirectCast(ex, System.ServiceModel.CommunicationException)
                p.ErrorCode = 0
                p.ErrorMessage = err.Message
            End Try
        End Try

        Return p

    End Function

    <WebMethod()> _
    Public Function RefundTransaction(ByVal OriginalTransactionID As Integer, ByVal TransactionID As Integer, ByVal TransactionAmount As Double) As RefundTransactionServiceResponse
        PrepareESB()
        Dim input As New refundTransaction()
        input.systemId = System.Configuration.ConfigurationManager.AppSettings("SystemID")
        input.originalTransactionId = OriginalTransactionID
        input.transactionId = TransactionID
        input.transactionAmount = TransactionAmount
        input.transactionAmountSpecified = (TransactionAmount > 0)

        LogInputCall(input)

        Dim p As New RefundTransactionServiceResponse
        Try
            Dim res As refundTransactionResp = cl.refundTransaction(headers, cInfo, rInfo, input)
            p.Result = res.result
            p.ErrorCode = res.errorCode
            p.ErrorMessage = res.errorMessage
        Catch ex As Exception
            Dim err As WSServiceError = DirectCast(ex, System.ServiceModel.FaultException(Of ESB.WSServiceError)).Detail
            p.ErrorCode = err.rc
            p.ErrorMessage = err.text
        End Try
        Return p

    End Function

    <WebMethod()> _
    Public Function SetAutoCloseBatchTime(ByVal MerchantID As String, ByVal Time As DateTime) As SetAutoCloseBatchTimeServiceResponse
        PrepareESB()
        Dim input As New setAutoCloseBatchTime()
        input.systemId = System.Configuration.ConfigurationManager.AppSettings("SystemID")
        input.merchantId = MerchantID
        input.time = Time

        Dim p As New SetAutoCloseBatchTimeServiceResponse
        Try
            Dim res As setAutoCloseBatchTimeResp = cl.setAutoCloseBatchTime(headers, cInfo, rInfo, input)
            p.Result = res.result
            p.ErrorCode = res.errorCode
            p.ErrorMessage = res.errorMessage
        Catch ex As Exception
            Dim err As WSServiceError = DirectCast(ex, System.ServiceModel.FaultException(Of ESB.WSServiceError)).Detail
            p.ErrorCode = err.rc
            p.ErrorMessage = err.text
        End Try
        Return p
    End Function

    <WebMethod()> _
    Public Function CancelTransaction(ByVal OriginalTransactionID As Integer, ByVal TransactionID As Integer) As CancelTransactionServiceResponse
        PrepareESB()
        Dim input As New cancelTransaction()
        input.systemId = System.Configuration.ConfigurationManager.AppSettings("SystemID")
        input.originalTransactionId = OriginalTransactionID
        input.transactionId = TransactionID

        LogInputCall(input)

        Dim p As New CancelTransactionServiceResponse
        Try
            Dim res As cancelTransactionResp = cl.cancelTransaction(headers, cInfo, rInfo, input)
            p.Result = res.result
            p.ErrorCode = res.errorCode
            p.ErrorMessage = res.errorMessage
        Catch ex As Exception
            Dim err As WSServiceError = DirectCast(ex, System.ServiceModel.FaultException(Of ESB.WSServiceError)).Detail
            p.ErrorCode = err.rc
            p.ErrorMessage = err.text
        End Try
        Return p
    End Function
End Class