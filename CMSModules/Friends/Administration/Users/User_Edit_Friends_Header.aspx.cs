using System;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_Friends_Administration_Users_User_Edit_Friends_Header : CMSUsersPage
{
    #region "Variables"

    protected int userId = 0;
    protected CurrentUserInfo currentUser = null;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        userId = QueryHelper.GetInteger("userId", 0);
        currentUser = CMSContext.CurrentUser;

        // Check 'read' permissions
        if (!currentUser.IsAuthorizedPerResource("CMS.Friends", "Read") && (currentUser.UserID != userId))
        {
            RedirectToAccessDenied("CMS.Friends", "Read");
        }

        // Check license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), string.Empty) != string.Empty)
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Friends);
        }

        CurrentMaster.Title.TitleText = ResHelper.GetString("friends.friends");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Friends/module.png");
        CurrentMaster.Title.HelpTopicName = "friends_myfriends";
        CurrentMaster.Title.HelpName = "helpTopic";

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }
    }


    #region "Private methods"

    /// <summary>
    /// Initializes menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string[,] tabs = new string[4, 4];
        tabs[0, 0] = ResHelper.GetString("friends.userfriends");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'friends_myfriends');";
        tabs[0, 2] = "User_Edit_Friends_Approved.aspx" + Request.Url.Query;

        tabs[1, 0] = ResHelper.GetString("friends.userwaitingforapproval");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'friends_waitingforapproval');";
        tabs[1, 2] = "User_Edit_Friends_ToApprove.aspx" + Request.Url.Query;

        tabs[2, 0] = ResHelper.GetString("friends.userrejectedfriendships");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'friends_rejectedfriendships');";
        tabs[2, 2] = "User_Edit_Friends_Rejected.aspx" + Request.Url.Query;

        tabs[3, 0] = ResHelper.GetString("friends.userrequestedfriendships");
        tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'friends_requestedfriendships');";
        tabs[3, 2] = "User_Edit_Friends_Requested.aspx" + Request.Url.Query;
        CurrentMaster.Tabs.UrlTarget = "friendsContent";
        CurrentMaster.Tabs.Tabs = tabs;
    }

    #endregion
}
