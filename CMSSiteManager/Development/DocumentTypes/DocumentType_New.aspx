<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentType_New.aspx.cs"
    Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_New" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Document Types - New Document Type" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/NewClassWizard.ascx" TagName="NewDocWizard" TagPrefix="cms" %>
    
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:NewDocWizard ID="newDocWizard" runat="server" />
</asp:Content>
