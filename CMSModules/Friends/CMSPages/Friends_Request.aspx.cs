using System;

using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Friends_CMSPages_Friends_Request : CMSLiveModalPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), string.Empty) != string.Empty)
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Friends);
        }

        int userId = QueryHelper.GetInteger("userid", 0);
        int requestedUserId = QueryHelper.GetInteger("requestid", 0);
        CurrentMaster.Title.TitleText = ResHelper.GetString("friends.addnewfriend");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Friends/request.png");

        FriendsRequest.UserID = userId;
        FriendsRequest.RequestedUserID = requestedUserId;
        FriendsRequest.IsLiveSite = true;

        if (requestedUserId != 0)
        {
            string fullUserName = String.Empty;

            UserInfo requestedUser = UserInfoProvider.GetFullUserInfo(requestedUserId);
            if (requestedUser != null)
            {
                fullUserName = UserInfoProvider.GetFullUserName(Functions.GetFormattedUserName(requestedUser.UserName, true), requestedUser.UserNickName);
            }

            Page.Title = string.Format(ResHelper.GetString("friends.requestfriendshipwith"), HTMLHelper.HTMLEncode(fullUserName));
            CurrentMaster.Title.TitleText = Page.Title;
        }
    }
}
