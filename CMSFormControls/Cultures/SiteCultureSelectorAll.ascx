<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SiteCultureSelectorAll.ascx.cs"
    Inherits="CMSFormControls_Cultures_SiteCultureSelectorAll" %>
<%@ Register Src="~/CMSFormControls/Cultures/SiteCultureSelector.ascx" TagName="SiteCultureSelector"
    TagPrefix="cms" %>
<cms:SiteCultureSelector ID="SiteCultureSelector" runat="server" AddDefaultRecord="false"
    AddAllRecord="true" />
