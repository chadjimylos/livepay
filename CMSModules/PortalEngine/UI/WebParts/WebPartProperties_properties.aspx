<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebPartProperties_properties.aspx.cs"
    Inherits="CMSModules_PortalEngine_UI_WebParts_WebPartProperties_properties" Theme="default"
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Src="~/CMSModules/PortalEngine/Controls/WebParts/WebpartProperties.ascx" TagName="WebpartProperties" TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Web part properties</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
            width: 100%;
            overflow: hidden;
        }
        .FCKToolbar
        {
            border-bottom: solid 1px #eeeeee;
            line-height: 0px;
        }
    </style>

    <script type="text/javascript">
    //<![CDATA[
        var wopener = parent.wopener;

        function ChangeWebPart(zoneId, webPartId, aliasPath)
        {
            if ( parent.ChangeWebPart ) {                
                parent.ChangeWebPart(zoneId, webPartId, aliasPath);
            }
        }

        function RefreshPage() {            
            if (wopener.RefreshPage) {                                              
                wopener.RefreshPage();
            }
        }
    //]]>
    </script>

    <base target="_self" />
</head>
<body class="<%=mBodyClass%> WebpartProperties">
    <form id="form1" runat="server" onsubmit="window.isPostBack = true; return true;">
        <asp:Panel runat="server" ID="pnlBody" CssClass="TabsPageBody">
            <asp:ScriptManager ID="manScript" runat="server" />
            <cms:WebpartProperties ID="webPartProperties" runat="server" />
        </asp:Panel>
    </form>
</body>
</html>
