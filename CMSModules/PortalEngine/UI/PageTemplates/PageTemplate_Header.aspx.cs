using System;

using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_Header : CMSEditTemplatePage
{
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        // Change master page for dialog
        if (ValidationHelper.GetInteger(Request.QueryString["nobreadcrumbs"], 0) == 1)
        {
            this.MasterPageFile = "~/CMSMasterPages/UI/Dialogs/TabsHeader.master";
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        int pageTemplateId = QueryHelper.GetInteger("templateid", 0);

        if (pageTemplateId != 0)
        {
            // Initialize page title
            string templates = ResHelper.GetString("Administration-PageTemplate_Header.Templates");
            string title = ResHelper.GetString("Administration-PageTemplate_Header.TemplateProperties");
            PageTemplateInfo pti = PageTemplateInfoProvider.GetPageTemplateInfo(pageTemplateId);
            string currentPageTemplate = pti.DisplayName;
            string[,] pageTitleTabs;

            if (ValidationHelper.GetInteger(Request.QueryString["nobreadcrumbs"], 0) < 1)
            {
                pageTitleTabs = new string[2, 3];
                pageTitleTabs[0, 0] = templates;
                pageTitleTabs[0, 1] = ResolveUrl("PageTemplate_Frameset.aspx");
                pageTitleTabs[0, 2] = "frameMain";
                pageTitleTabs[1, 0] = currentPageTemplate;
                pageTitleTabs[1, 1] = "";
                pageTitleTabs[1, 2] = "";
            }
            else
            {
                pageTitleTabs = new string[2, 3];
                pageTitleTabs[0, 0] = templates;
                pageTitleTabs[0, 1] = "";
                pageTitleTabs[0, 2] = "";
                pageTitleTabs[1, 0] = currentPageTemplate;
                pageTitleTabs[1, 1] = "";
                pageTitleTabs[1, 2] = "";
            }
            this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
            this.CurrentMaster.Title.TitleText = title;
            this.CurrentMaster.Title.HelpTopicName = "general_tab12";
            this.CurrentMaster.Title.HelpName = "helpTopic";

            if (pti.IsReusable)
            {
                this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_PageTemplates/pagetemplate.png");
            }
            else
            {
                this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_PageTemplates/adhoc.png");
            }

            // Initialize menu
            string[,] tabs = new string[8, 4];

            tabs[0, 0] = ResHelper.GetString("general.general");
            tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab12');";
            tabs[0, 2] = "PageTemplate_General.aspx?templateid=" + pageTemplateId;

            if (pti.IsReusable)
            {
                tabs[1, 0] = ResHelper.GetString("general.sites");
                tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'sites_tab2');";
                tabs[1, 2] = "PageTemplate_Sites.aspx?templateid=" + pageTemplateId;

                tabs[2, 0] = ResHelper.GetString("pagetemplate.edit.scopes");
                tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'page_templates_scopes');";
                tabs[2, 2] = "Scopes/PageTemplateScopes_List.aspx?templateid=" + pageTemplateId;
            }

            if ((pti.IsPortal))
            {
                tabs[3, 0] = ResHelper.GetString("Administration-PageTemplate_Header.Layouts");
                tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'layout');";
                tabs[3, 2] = "PageTemplate_Layouts.aspx?templateid=" + pageTemplateId;

                tabs[4, 0] = ResHelper.GetString("Administration-PageTemplate_Header.WebParts");
                tabs[4, 1] = ""; // "SetHelpTopic('helpTopic', 'web_parts');";
                tabs[4, 2] = "PageTemplate_WebParts.aspx?templateid=" + pageTemplateId;

                if (ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSShowTemplateASPXTab"], false))
                {
                    tabs[6, 0] = ResHelper.GetString("Administration-PageTemplate_Header.ASPX");
                    tabs[6, 1] = ""; // "SetHelpTopic('helpTopic', 'page_templates_aspx_code');";
                    tabs[6, 2] = "PageTemplate_ASPX.aspx?templateid=" + pageTemplateId;
                }
            }

            tabs[5, 0] = ResHelper.GetString("Administration-PageTemplate_Header.Header");
            tabs[5, 1] = ""; // "SetHelpTopic('helpTopic', 'header');";
            tabs[5, 2] = "PageTemplate_HeaderTab.aspx?templateid=" + pageTemplateId;

            tabs[7, 0] = ResHelper.GetString("general.documents");
            tabs[7, 1] = ""; // "SetHelpTopic('helpTopic', 'page_templates_documents');";
            tabs[7, 2] = "PageTemplate_Documents.aspx?templateid=" + pageTemplateId;
            
            this.CurrentMaster.Tabs.Tabs = tabs;
            this.CurrentMaster.Tabs.UrlTarget = "pt_edit_content";
        }
    }
}
