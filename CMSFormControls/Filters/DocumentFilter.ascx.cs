using System;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.Controls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.TreeEngine;

public partial class CMSFormControls_Filters_DocumentFilter : CMSAbstractBaseFilterControl
{
    #region "Variables"

    private bool mAllowSiteAutopostback = true;
    private bool mIncludeSiteCondition = false;
    private bool mLoadSites = false;
    private string mWhereCondition = null;
    private bool mEnableSiteSelector = false;
    
    #endregion


    #region "Properties"

    /// <summary>
    /// Gets sites placeholder
    /// </summary>
    public PlaceHolder SitesPlaceHolder
    {
        get
        {
            return plcSites;
        }
    }


    /// <summary>
    /// Gets document name path placeholder
    /// </summary>
    public PlaceHolder PathPlaceHolder
    {
        get
        {
            return plcPath;
        }
    }


    /// <summary>
    /// Gets class placeholder
    /// </summary>
    public PlaceHolder ClassPlaceHolder
    {
        get
        {
            return plcClass;
        }
    }


    /// <summary>
    /// Determines whether to load site selector
    /// </summary>
    public bool LoadSites
    {
        get
        {
            return mLoadSites;
        }
        set
        {
            mLoadSites = value;
        }
    }


    /// <summary>
    /// Determines whether to include condition for site to the resulting WHERE condition.
    /// </summary>
    public bool IncludeSiteCondition
    {
        get
        {
            return mIncludeSiteCondition;
        }
        set
        {
            mIncludeSiteCondition = value;
        }

    }


    /// <summary>
    /// Determines whether the site selector DDL has autopostback option on or off.
    /// </summary>
    public bool AllowSiteAutopostback
    {
        get
        {
            return mAllowSiteAutopostback;
        }
        set
        {
            mAllowSiteAutopostback = value;
        }
    }


    /// <summary>
    /// Returns selected site name
    /// </summary>
    public string SelectedSite
    {
        get
        {
            return ValidationHelper.GetString(siteSelector.Value, string.Empty);
        }
    }

    public bool EnableSiteSelector {
        get {
            return mEnableSiteSelector;
        }
        set {
            mEnableSiteSelector = value;
        }
    }


    /// <summary>
    /// Where condition.
    /// </summary>
    public override string WhereCondition
    {
        get
        {
            if (mWhereCondition == null)
            {
                // Add where conditions from filters
                mWhereCondition = base.WhereCondition;

                mWhereCondition = SqlHelperClass.AddWhereCondition(mWhereCondition, classFilter.WhereCondition);
                mWhereCondition = SqlHelperClass.AddWhereCondition(mWhereCondition, nameFilter.WhereCondition);

                if (IncludeSiteCondition && !String.IsNullOrEmpty(siteSelector.SiteName) && siteSelector.SiteName != "##ALL##")
                {
                    mWhereCondition = SqlHelperClass.AddWhereCondition(mWhereCondition, "SiteName = '" + siteSelector.SiteName.Replace("'", "''") + "'");
                }

                base.WhereCondition = mWhereCondition;

            }
            return mWhereCondition;
        }
        set
        {
            base.WhereCondition = value;
        }
    }


    /// <summary>
    /// Determines whether filter is set
    /// </summary>
    public bool FilterIsSet
    {
        get
        {
            return nameFilter.FilterIsSet || classFilter.FilterIsSet;
        }
    }

    public event EventHandler OnSiteSelectionChanged;

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        siteSelector.DropDownSingleSelect.Width = new Unit(305);

        if (LoadSites)
        {
            LoadSiteSelector();
        }
    }

    /// <summary>
    /// Loads the drop-down list with all the available sites
    /// </summary>
    private void LoadSiteSelector()
    {
        // Get all user sites
        if (CMSContext.CurrentUser.IsGlobalAdministrator || this.EnableSiteSelector)
        {
            // Set site selector
            siteSelector.DropDownSingleSelect.AutoPostBack = AllowSiteAutopostback;
            siteSelector.AllowAll = false;
            siteSelector.UniSelector.SpecialFields = new string[1, 2] { { ResHelper.GetString("general.selectall"), TreeProvider.ALL_SITES } };
            siteSelector.UniSelector.OnSelectionChanged += OnSiteSelectionChanged;

            // Preselect all
            if (!RequestHelper.IsPostBack())
            {
                siteSelector.Value = TreeProvider.ALL_SITES;
            }
        }
        else
        {
            plcSites.Visible = false;
        }
    }

    #endregion
}
