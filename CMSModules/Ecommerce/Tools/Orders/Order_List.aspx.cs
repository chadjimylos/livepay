using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Orders_Order_List : CMSEcommercePage
{
    protected int customerId;

    protected void Page_Load(object sender, EventArgs e)
    {
        string[,] actions = new string[1, 6];

        customerId = ValidationHelper.GetInteger(Request.QueryString["customerId"], 0);
        if (customerId > 0)
        {
            actions[0, 3] += "?customerId=" + customerId.ToString();
        }

        // New item link
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Order_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("Order_New.aspx") + "?customerid=" + customerId.ToString();
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("CMSModules/CMS_Ecommerce/addorder.png");
        this.CurrentMaster.HeaderActions.Actions = actions;
        this.CurrentMaster.HeaderActions.HelpTopicName = "orders_list";
        this.CurrentMaster.HeaderActions.HelpName = "helpTopic";
    }
}
