<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InternalStatus_Edit.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_InternalStatus_InternalStatus_Edit"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Internal status - Edit" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblInternalStatusDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtInternalStatusDisplayName" runat="server" CssClass="TextBoxField"
                    MaxLength="200" EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ValidationGroup="Internal"
                    Display="Dynamic" ControlToValidate="txtInternalStatusDisplayName" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblInternalStatusName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtInternalStatusName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ValidationGroup="Internal"
                    Display="Dynamic" ControlToValidate="txtInternalStatusName" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblInternalStatusEnabled" EnableViewState="false"
                    ResourceString="general.enabled" DisplayColon="true" />
            </td>
            <td>
                <asp:CheckBox ID="chkInternalStatusEnabled" runat="server" CssClass="CheckBoxMovedLeft"
                    Checked="true" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" ValidationGroup="Internal" />
            </td>
        </tr>
    </table>
</asp:Content>
