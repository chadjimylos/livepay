<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectBizForm.ascx.cs"
    Inherits="CMSModules_BizForms_FormControls_SelectBizForm" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>
<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" ResourcePrefix="bizformselect" ObjectType="cms.form"
            OrderBy="FormName" AllowEditTextBox="true" SelectionMode="SingleTextBox" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
