<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditingFormControl.aspx.cs"
    Inherits="CMSAdminControls_EditingFormControl" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master" Title="Edit field value" %>

<%@ Register Src="~/CMSAdminControls/UI/Selectors/MacroSelector.ascx" TagPrefix="cc1"
    TagName="MacroSelector" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">

    <script type="text/javascript">
        //<![CDATA[
        // Sets the given text to textarea
        function initTextArea(text) {
            if (txtElem != null) {
                txtElem.value = text;
            }
        }

        // Inserts the text to caret position (in text area)
        function insertAtCursorPosition(text) {
            if (text != "") {
                if (txtElem != null) {
                    if (document.selection) {
                        // IE
                        txtElem.focus();
                        var orig = txtElem.value.replace(/\r\n/g, "\n");
                        var range = document.selection.createRange();

                        if (range.parentElement() != txtElem) {
                            return false;
                        }

                        range.text = text;
                        var actual = tmp = txtElem.value.replace(/\r\n/g, "\n");

                        for (var diff = 0; diff < orig.length; diff++) {
                            if (orig.charAt(diff) != actual.charAt(diff)) break;
                        }

                        for (var index = 0, start = 0; tmp.match(text) && (tmp = tmp.replace(text, "")) && index <= diff; index = start + text.length) {
                            start = actual.indexOf(text, index);
                        }

                    } else if (txtElem.selectionStart) {
                        // Firefox
                        var start = txtElem.selectionStart;
                        var end = txtElem.selectionEnd;

                        txtElem.value = txtElem.value.substr(0, start) + text + txtElem.value.substr(end, txtElem.value.length);
                    }

                    if (start != null) {
                        setCaretTo(txtElem, start + text.length);
                    } else {
                        txtElem.value += text;
                    }
                }
            }
        }

        // Sets the location of the cursor in specified object to given position
        function setCaretTo(obj, pos) {

            if (obj != null) {
                if (obj.createTextRange) {
                    var range = obj.createTextRange();
                    range.move('character', pos);
                    range.select();
                } else if (obj.selectionStart) {
                    obj.focus();
                    obj.setSelectionRange(pos, pos);
                }
            }
        }

        // Sets the content of the TextArea to given selector
        function setValueToParent(selId, controlPanelId, selPanelId) {
            if (txtElem != null) {
                wopener.setNestedControlValue(selId, controlPanelId, txtElem.value, selPanelId);
            }
        }

        // Removes macro
        function removeMacro(selId, controlPanelId, selPanelId) {
            wopener.setNestedControlValue(selId, controlPanelId, '', selPanelId);
        }
        //]]>
    </script>

    <asp:Panel runat="server" ID="pnlContent" CssClass="PageContent">
        <textarea style="width: 97%; height: 100px;" cols="5" rows="4" id="txtArea" runat="server" />
        <br />
        <br />
        <cc1:MacroSelector ID="macroElem" runat="server" JavaScripFunction="insertAtCursorPosition"
            ShowExtendedControls="true" />
    </asp:Panel>
    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
</asp:Content>
<asp:Content ID="cntFooter" ContentPlaceHolderID="plcFooter" runat="server">
    <div class="FloatLeft">
        <cms:CMSButton ID="btnRemove" runat="server" CssClass="LongSubmitButton" EnableViewState="false" />
    </div>
    <div class="FloatRight">
        <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" EnableViewState="false" />
        <cms:CMSButton ID="btnCancel" runat="server" CssClass="SubmitButton" OnClientClick="window.close(); return false;"
            EnableViewState="false" />
    </div>
</asp:Content>
