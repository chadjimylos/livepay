<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StoreSettings_CheckoutProcess.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_StoreSettings_StoreSettings_CheckoutProcess"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" %>

<%@ Register Src="~/CMSModules/ECommerce/FormControls/CheckoutProcess.ascx" TagName="CheckoutProcess"
    TagPrefix="cms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <asp:ScriptManager ID="manScript" runat="server" />
    <cms:CheckoutProcess ID="ucCheckoutProcess" runat="server" />
</asp:Content>
