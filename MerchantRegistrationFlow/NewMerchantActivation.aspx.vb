﻿Imports System.Collections.Generic
Imports System.Data.Linq
Namespace Kentico_MerchantActivationWorkflow
    Public Class NewMerchantActivation
        Inherits System.Web.UI.Page

        Public ReadOnly Property MerchantId As Guid?
            Get
                Dim GuidString As String = Me.Page.Request.QueryString("guid")

                Try
                    Dim MerchantIdGuid As Guid? = New Guid(GuidString)
                    Return MerchantIdGuid
                Catch ex As Exception
                    Return Nothing
                End Try

            End Get
        End Property

        Public ReadOnly Property BankId As String
            Get
                Dim _BankId = Me.Page.Request.QueryString("bankid")
                Return _BankId
            End Get
        End Property

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not Me.IsPostBack Then



                Dim API = New HellasPayInternalService.InternalAPIClient()

                Dim BankAccount = API.GetMerchantBankAccount(Me.MerchantId, Me.BankId)

                If (BankAccount IsNot Nothing) Then
                	Main.Visible = False
                	FailedPanel.Visible = True
                Else
                	Me.MerchantStatement.Text = API.GetMerchantById(Me.MerchantId).Statement

                	Me.MID.Text = ""
                	Me.TID1.Text = ""
	                Me.TID2.Text = ""
              	End If
            End If
        End Sub

        Protected Function LinkUrl(ByVal merchant As HellasPayInternalService.Merchant) As String
            Return "https://www.hellaspay.gr/MerchantRegistrationFlow/euronetactivation.aspx?guid=" + merchant.MerchantId.ToString()
        End Function


        Protected Sub Save_Click(sender As Object, e As EventArgs)
            Dim ApiClient As New HellasPayInternalService.InternalAPIClient()
            Dim BankAccount As New HellasPayInternalService.MerchantBankAccount

            BankAccount.BankId = Me.BankId
            BankAccount.MerchantId = Me.MerchantId
            BankAccount.MID = Me.MID.Text
            BankAccount.TID1 = Me.TID1.Text
            BankAccount.TID2 = Me.TID2.Text
            BankAccount.IsActive = True
	    BankAccount.LastUpdated = Now

            Dim Success = ApiClient.UpdateMerchantBankAccount(BankAccount)

            Main.Visible = False

            If Success Then
                SuccessPanel.Visible = True

                BankAccount.Bank = ApiClient.GetBankById(BankAccount.BankId)

                Dim merchant As HellasPayInternalService.Merchant = ApiClient.GetMerchantById(Me.MerchantId)
                Dim EmailToList As New List(Of String)()
                Dim EmailCCList As New List(Of String)()
                Dim EmailSubject As String = "Euronet Merchant Activation : " + merchant.Person.CompanyName

                EmailToList.Add("jlefas@realize.gr")
                EmailCCList.Add("makis@realize.gr")


                Dim EmailBody As String = _
                "<img src='https://www.hellaspay.gr/Images/logoHellasPay' /><BR/><BR/>                                      " + "<br/>" + _
                "Να ανοιχθεί νέο VTID με τα παρακάτω στοιχεία:                                                              " + "<br/>" + _
                "                                                                                                           " + "<br/>" + _
                "&nbsp;&nbsp; Όνομα εμπόρου : <br/>&nbsp;&nbsp;&nbsp;&nbsp; " + merchant.Person.CompanyName + "<br/>              " + "<br/>" + _
                "&nbsp;&nbsp;&nbsp;&nbsp; " + BankAccount.Bank.Name + " MID : " + BankAccount.MID + "                           " + "<br/>" + _
                "&nbsp;&nbsp;&nbsp;&nbsp; " + BankAccount.Bank.Name + " TID1: " + BankAccount.TID1 + "                          " + "<br/>" + _
                "&nbsp;&nbsp;&nbsp;&nbsp; " + BankAccount.Bank.Name + " TID2: " + BankAccount.TID2 + "                          " + "<br/>" + _
                "                                                                                                           " + "<br/>" + _
                "Μόλις ολοκληρωθεί, παρακαλώ ενημερώστε μας ακολουθώντας το παρακάτω link                                   " + "<br/>" + _
                "&nbsp;&nbsp; <a href='" + LinkUrl(merchant) + "' >" + LinkUrl(merchant) + " </a>                             " + "<br/>" + _
                "                                                                                                           " + "<br/>" + _
                "Ευχαριστούμε πολύ"

                ' SEND MAIL HERE
                Dim cms As New HellasPayCmsService
                Dim emailOptions As New EmailOptions

                emailOptions.From = "info@hellaspay.gr"
                emailOptions.SendTo = "james@realize.gr"
                emailOptions.Bcc = "makis@realize.gr"
                emailOptions.Subject = "VTID Request For " + merchant.Person.CompanyName
                emailOptions.Body = EmailBody

                Dim MailSend As Boolean = cms.sendEmail(emailOptions)
                MailSend = MailSend
            Else
                FailedPanel.Visible = True
            End If

        End Sub
    End Class
End Namespace