<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="Tags_Documents.aspx.cs" Inherits="CMSSiteManager_Development_TagGroups_Tags_Documents"
    Title="Tag group - Tags documents" Theme="Default" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<%@ Register Src="~/CMSFormControls/Filters/DocumentFilter.ascx" TagName="DocumentFilter"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" runat="Server">
    <cms:DocumentFilter ID="filterDocuments" runat="server" LoadSites="true" />
    <br />
    <br />
    <cms:LocalizedLabel ID="lblTitle" runat="server" ResourceString="TagGroup_Edit.Documents.used"
        DisplayColon="true" EnableViewState="false" />
    <br />
    <br />
    <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cms:UniGrid ID="uniGridDocs" runat="server" GridName="Tags_Documents.xml" IsLiveSite="false" />
        </ContentTemplate>
    </cms:CMSUpdatePanel>
</asp:Content>
