<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PageEditFrameset.aspx.cs"
    Inherits="CMSModules_Content_CMSDesk_MasterPage_PageEditFrameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Page edit frameset</title>

    <script type="text/javascript">
    //<![CDATA[
        var IsCMSDesk = true;
    //]]>
    </script>

</head>

<script src="<%= ResolveUrl("~/CMSScripts/progress.js") %>" type="text/javascript"></script>

<frameset border="0" rows="43,*">
		<frame name="masterpageheader" src="PageEditHeader.aspx<%=Request.Url.Query%>" scrolling="no" noresize="noresize" frameborder="0" />
		<frame name="masterpagecontent" src="PageEdit.aspx<%=Request.Url.Query%>" scrolling="yes" frameborder="0" />
		<noframes>
			<p id="p1">
				This HTML frameset displays multiple Web pages. To view this frameset, use a 
				Web browser that supports HTML 4.0 and later.
			</p>
		</noframes>
	</frameset>
</html>
