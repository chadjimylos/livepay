using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.IO;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.Community;
using CMS.UIControls;

public partial class CMSWebParts_Community_Profile_GroupProfile : CMSAbstractWebPart
{
    #region "Private variables"

    bool mDisplayMessage = false;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets group name to specify group members.
    /// </summary>
    public string GroupName
    {
        get
        {
            string groupName = ValidationHelper.GetString(this.GetValue("GroupName"), "");
            if ((string.IsNullOrEmpty(groupName) || groupName == GroupInfoProvider.CURRENT_GROUP) && (CommunityContext.CurrentGroup != null))
            {
                return CommunityContext.CurrentGroup.GroupName;
            }
            return groupName;
        }
        set
        {
            this.SetValue("GroupName", value);
        }
    }


    /// <summary>
    /// Gets or sets message which should be displayed if user hasn't permissions.
    /// </summary>
    public string NoPermissionMessage
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("NoPermissionMessage"), "");
        }
        set
        {
            this.SetValue("NoPermissionMessage", value);
            this.messageElem.ErrorMessage = value;
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            this.SetContext();

            this.messageElem.ErrorMessage = this.NoPermissionMessage;
            this.messageElem.IsLiveSite = true;
            this.groupProfileElem.IsLiveSite = true;
            this.groupProfileElem.RedirectToAccessDeniedPage = false;
            this.groupProfileElem.OnCheckPermissions += new CMS.UIControls.CMSAdminControl.CheckPermissionsEventHandler(groupProfileElem_OnCheckPermissions);

            this.groupProfileElem.HideWhenGroupIsNotSupplied = true;

            GroupInfo gi = GroupInfoProvider.GetGroupInfo(this.GroupName, CMSContext.CurrentSiteName); 

            if (gi != null)
            {
                this.groupProfileElem.GroupID = gi.GroupID;
            }
            else
            {
                groupProfileElem.StopProcessing = true;
                groupProfileElem.Visible = false;
                mDisplayMessage = true;
            }

            this.ReleaseContext();
        }
    }


   
    /// <summary>
    /// Group profile - check permissions
    /// </summary>
    void groupProfileElem_OnCheckPermissions(string permissionType, CMSAdminControl sender)
    {
        if (!(CMSContext.CurrentUser.IsGroupAdministrator(this.groupProfileElem.GroupID) || CMSContext.CurrentUser.IsAuthorizedPerResource("cms.groups", permissionType)))
        {
            if (sender != null)
            {
                sender.StopProcessing = true;
            }
            groupProfileElem.StopProcessing = true;
            groupProfileElem.Visible = false;
            messageElem.ErrorMessage = NoPermissionMessage;
            mDisplayMessage = true;
        }
    }

    
    /// <summary>
    /// Render override
    /// </summary>
    /// <param name="writer">writer</param>
    protected override void Render(HtmlTextWriter writer)
    {
        if (!mDisplayMessage)
        {
            messageElem.Visible = false;
        }
        
        base.Render(writer);
    }
}
