using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;

public partial class CMSModules_Content_CMSDesk_Properties_ViewVersion : CMSPropertiesPage
{
    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        this.btnClose.Text = ResHelper.GetString("General.Close");

        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.Title.HelpTopicName = "versions";
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Content.ViewVersion");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_RecycleBin/viewversion.png");

        // Register tooltip script
        ScriptHelper.RegisterTooltip(this.Page);

        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // Redirect to information page when no UI elements displayed
        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.Versions"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.Versions");
        }
    }

    #endregion
}
