using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Customers_Customer_New : CMSEcommercePage
{
    protected int customerid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // control initializations				
        lblCustomerLastName.Text = ResHelper.GetString("Customers_Edit.CustomerLastNameLabel");
        lblCustomerFirstName.Text = ResHelper.GetString("Customers_Edit.CustomerFirstNameLabel");
        lblCustomerCompany.Text = ResHelper.GetString("Customers_Edit.CustomerCompanyLabel");
        lblCountry.Text = ResHelper.GetString("Customers_Edit.CustomerCountry");
        lblUserName.Text = ResHelper.GetString("general.username");
        lblPassword1.Text = ResHelper.GetString("Customer_Edit_Login_Edit.lblPassword1");
        lblPassword2.Text = ResHelper.GetString("Customer_Edit_Login_Edit.lblPassword2");
        lblCustomerData.Text = ResHelper.GetString("customers_edit.customerdata");
        lblUserData.Text = ResHelper.GetString("customers_edit.userdata");
        rqvUserName.ErrorMessage = ResHelper.GetString("Customer_Edit_Login_Edit.rqvUserName");
        rqvPassword1.ErrorMessage = ResHelper.GetString("Customer_Edit_Login_Edit.rqvPassword1");
        rqvPassword2.ErrorMessage = ResHelper.GetString("Customer_Edit_Login_Edit.rqvPassword2");
        btnOk.Text = ResHelper.GetString("General.OK");

        string currentCustomer = ResHelper.GetString("Customers_Edit.NewItemCaption");

        // get customer id from querystring		
        customerid = ValidationHelper.GetInteger(Request.QueryString["customerid"], 0);
        if (customerid > 0)
        {
            CustomerInfo customerObj = CustomerInfoProvider.GetCustomerInfo(customerid);
            if (customerObj != null)
            {
                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    LoadData(customerObj);

                    // show that the customer was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
        }
        else
        {
            if (!RequestHelper.IsPostBack())
            {
                CountryInfo ci = CountryInfoProvider.GetCountryInfo("usa");
                if (ci != null)
                {
                    this.drpCountry.CountryID = ci.CountryID;
                }
            }
        }

        InitializeMasterPage(currentCustomer);

        InitializeProductControls();
    }


    /// <summary>
    /// Initializes master page elements
    /// </summary>
    private void InitializeMasterPage(string currentCustomer)
    {
        this.CurrentMaster.Title.HelpTopicName = "new_customer";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Customers_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Customers/Customer_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentCustomer;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// Load data of editing customer.
    /// </summary>
    /// <param name="customerObj">Customer object.</param>
    protected void LoadData(CustomerInfo customerObj)
    {
        txtCustomerLastName.Text = customerObj.CustomerLastName;
        txtCustomerFirstName.Text = customerObj.CustomerFirstName;
        txtCustomerCompany.Text = customerObj.CustomerCompany;
        drpCountry.CountryID = customerObj.CustomerCountryID;
        drpCountry.StateID = customerObj.CustomerStateID;
        chkCustomerEnabled.Enabled = customerObj.CustomerEnabled;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        string errorMessage = "";

        if ((txtCustomerCompany.Text == null || txtCustomerCompany.Text.Trim() == "") &&
            ((txtCustomerFirstName.Text == null || txtCustomerFirstName.Text.Trim() == "") ||
             (txtCustomerLastName.Text == null || txtCustomerLastName.Text.Trim() == "")))
        {
            errorMessage = ResHelper.GetString("Customers_Edit.errorInsert");
        }

        if (errorMessage == "")
        {
            if (this.chkCreateLogin.Checked)
            {
                errorMessage = new Validator().NotEmpty(txtUserName.Text, ResHelper.GetString("Customer_Edit_Login_Edit.rqvUserName")).NotEmpty(txtPassword1.Text, ResHelper.GetString("Customer_Edit_Login_Edit.rqvPassword1")).NotEmpty(txtPassword2.Text, ResHelper.GetString("Customer_Edit_Login_Edit.rqvPassword2")).Result;

                if (txtPassword1.Text != txtPassword2.Text)
                {
                    errorMessage = ResHelper.GetString("Customer_Edit_Login_Edit.DifferentPasswords");
                }
            }

            if (errorMessage == "")
            {
                // If user already has the record in the CMS_User table            
                UserInfo ui = UserInfoProvider.GetUserInfo(txtUserName.Text.Trim());
                if (ui != null)
                {
                    errorMessage = ResHelper.GetString("Customer_Edit_Login_Edit.UserExist");
                }

                if (errorMessage == "")
                {
                    CustomerInfo customerObj = CustomerInfoProvider.GetCustomerInfo(customerid);

                    // if customer doesn't already exist, create new one
                    if (customerObj == null)
                    {
                        customerObj = new CustomerInfo();
                    }

                    customerObj.CustomerLastName = txtCustomerLastName.Text.Trim();
                    customerObj.CustomerFirstName = txtCustomerFirstName.Text.Trim();
                    customerObj.CustomerCompany = txtCustomerCompany.Text.Trim();
                    customerObj.CustomerCountryID = drpCountry.CountryID;
                    customerObj.CustomerStateID = drpCountry.StateID;
                    customerObj.CustomerEnabled = chkCustomerEnabled.Checked;
                    //customerObj.CustomerCreated = ucCustomerCreated.SelectedDateTime;

                    CustomerInfoProvider.SetCustomerInfo(customerObj);

                    // if create login checked
                    if (this.chkCreateLogin.Checked)
                    {
                        ui = new UserInfo();
                        ui.UserName = txtUserName.Text.Trim();
                        ui.FullName = customerObj.CustomerFirstName + " " + customerObj.CustomerLastName;
                        ui.IsGlobalAdministrator = false;
                        ui.SetValue("UserEnabled", true);
                        UserInfoProvider.SetUserInfo(ui);
                        UserInfoProvider.SetPassword(ui.UserName, txtPassword1.Text);
                        UserInfoProvider.AddUserToSite(ui.UserName, CMSContext.CurrentSiteName);

                        customerObj.CustomerUserID = ui.UserID;

                        CustomerInfoProvider.SetCustomerInfo(customerObj);
                    }

                    UrlHelper.Redirect("Customer_Edit_Frameset.aspx?customerid=" + Convert.ToString(customerObj.CustomerID) + "&saved=1");
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = errorMessage;
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = errorMessage;
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    private void InitializeProductControls()
    {
        // Initialize login controls
        lblCreateLogin.Text = ResHelper.GetString("Customers_Edit.CreateLogin");
        chkCreateLogin.Attributes["onclick"] = "ShowHideLoginControls()";

        // Register script to show / hide SKU controls
        string script =
            "function ShowHideLoginControls() { \n" +
            "   checkbox = document.getElementById('" + this.chkCreateLogin.ClientID + "'); \n" +
            "   line1 = document.getElementById('loginLine1'); \n" +
            "   line2 = document.getElementById('loginLine2'); \n" +
            "   line3 = document.getElementById('loginLine3'); \n" +
            "   if ((checkbox != null) && (checkbox.checked)) { \n" +
            "       line1.style.display = '';" +
            "       line2.style.display = '';" +
            "       line3.style.display = '';" +
            "   } else { \n" +
            "       line1.style.display = 'none';" +
            "       line2.style.display = 'none';" +
            "       line3.style.display = 'none';" +
            "   } \n" +
            "} \n";

        this.ltlScript.Text += ScriptHelper.GetScript(script);

        this.ltlScript.Text += ScriptHelper.GetScript("ShowHideLoginControls()");
    }
}
