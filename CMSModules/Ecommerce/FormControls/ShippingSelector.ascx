<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ShippingSelector.ascx.cs"
    Inherits="CMSModules_Ecommerce_FormControls_ShippingSelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" DisplayNameFormat="{%ShippingOptionDisplayName%}"
            ReturnColumnName="ShippingOptionID" ObjectType="ecommerce.shippingoption" ResourcePrefix="shippingselector"
            SelectionMode="SingleDropDownList" AllowEmpty="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
