﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Partial Class CMSTemplates_LivePay_UCDirectPayment
    Inherits CMSUserControl


#Region "Variables"

    ''' <summary>
    ''' My property
    ''' </summary>
    Private mMyProperty As String = Nothing

#End Region

#Region "Properties"


    ''' <summary>
    ''' Gets or sets value of MyProperty
    ''' </summary>
    Public Property MyProperty() As String
        Get
            Return mMyProperty
        End Get
        Set(ByVal value As String)
            mMyProperty = value
        End Set
    End Property

#End Region

#Region "Methods"
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Tab1_BtnNext.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/BtnNext.png|en-us=/App_Themes/LivePay/RegisteredUsers/BtnNext_en-us.png$}")
        Tab2_btnNext.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/BtnNext.png|en-us=/App_Themes/LivePay/RegisteredUsers/BtnNext_en-us.png$}")
        Tab2_btnReturn.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnReturn.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnReturn_en-us.png$}")
        ImageButton1.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/BtnNext.png|en-us=/App_Themes/LivePay/RegisteredUsers/BtnNext_en-us.png$}")
        ImageButton2.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnReturn.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnReturn_en-us.png$}")
        Tab3_btnComplete.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnComplete.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnComplete_en-us.png$}")
        Tab3_btnReturn.ImageUrl = ResHelper.LocalizeString("{$=/App_Themes/LivePay/RegisteredUsers/btnReturn.png|en-us=/App_Themes/LivePay/RegisteredUsers/btnReturn_en-us.png$}")
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        SetLang()
    End Sub

    Private Sub SetLang()
        Tab1_Val_CustomerCode.ErrorMessage = ResHelper.LocalizeString("{$=Κωδικός Πελάτη|en-us=Customer Code$}")
        Tab1_ReqFldVal_AcountNo.ErrorMessage = ResHelper.LocalizeString("{$=Αριθμός Λογαριασμού|en-us=Account Number$}")
        Tab1_ReqFldVal_Price.ErrorMessage = ResHelper.LocalizeString("{$=Ποσό|en-us=Amount$}")
        Tab1_ReqularFldVal_Price.ErrorMessage = ResHelper.LocalizeString("{$=Ποσό|en-us=Amount$}")
        Tab1_ValSumm.HeaderText = ResHelper.LocalizeString("{$=Παρακαλώ συμπληρώστε τα ακόλουθα πεδία:|en-us=Please complete the following fields:$}")


       
        Tab2_ReqFldVal_CardType.ErrorMessage = ResHelper.LocalizeString("{$=Τύπος Κάρτας|en-us=Card Type$}")
        Tab2_ReqFldVal_CardNo.ErrorMessage = ResHelper.LocalizeString("{$=Αριθμός Κάρτας|en-us=Card Number$}")
        Tab2_ReqFldVal_FullNameOwner.ErrorMessage = ResHelper.LocalizeString("{$=Ονοματεπώνυμο Κατόχου|en-us=Card Holder Name$}")
        Tab2_ReqFldVal_CardMonth.ErrorMessage = ResHelper.LocalizeString("{$=Ημερομηνία Λήξης(μήνας)|en-us=Expiration Date (month)$}")
        Tab2_ReqFldVal_CardYear.ErrorMessage = ResHelper.LocalizeString("{$=Ημερομηνία Λήξης(έτος)|en-us=Expiration Date (year)$}")
        Tab2_ChlIWantInvoice.Text = ResHelper.LocalizeString("{$=Επιθυμώ να μου αποσταλεί τιμολόγιο|en-us=I wish to have an Invoice issued$}")
        Tab3_chkTermsAccept.Text = ResHelper.LocalizeString("{$=Αποδέχομαι τους όρους συναλλαγής|en-us=I accept the Payment Terms of Use$}")
    End Sub
#End Region
End Class
