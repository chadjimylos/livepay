using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Text;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.EcommerceProvider;
using CMS.CMSHelper;
using CMS.URLRewritingEngine;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.ExtendedControls;

public partial class CMSModules_Ecommerce_Controls_ShoppingCart_ShoppingCartContent : ShoppingCartStep
{
    #region "Private properties"

    protected Button btnReload = null;
    protected Button btnAddProduct = null;
    protected HiddenField hidProductID = null;
    protected HiddenField hidQuantity = null;
    protected HiddenField hidOptions = null;

    protected Nullable<bool> mEnableEditMode = null;

    #endregion


    /// <summary>
    /// Child control creation.
    /// </summary>
    protected override void CreateChildControls()
    {
        // Add product button
        this.btnAddProduct = new CMSButton();
        this.btnAddProduct.Attributes["style"] = "display: none";
        this.Controls.Add(this.btnAddProduct);
        this.btnAddProduct.Click += new EventHandler(btnAddProduct_Click);

        // Add the hidden fields for productId, quantity and product options
        this.hidProductID = new HiddenField();
        this.hidProductID.ID = "hidProductID";
        this.Controls.Add(this.hidProductID);

        this.hidQuantity = new HiddenField();
        this.hidQuantity.ID = "hidQuantity";
        this.Controls.Add(this.hidQuantity);

        this.hidOptions = new HiddenField();
        this.hidOptions.ID = "hidOptions";
        this.Controls.Add(this.hidOptions);
    }


    /// <summary>
    /// Page pre-render.
    /// </summary>
    protected void Page_PreRender(object sender, EventArgs e)
    {
        // Register the dialog scripts
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "AddProductScript",
            ScriptHelper.GetScript(
                "function setProduct(val) { document.getElementById('" + this.hidProductID.ClientID + "').value = val; } \n" +
                "function setQuantity(val) { document.getElementById('" + this.hidQuantity.ClientID + "').value = val; } \n" +
                "function setOptions(val) { document.getElementById('" + this.hidOptions.ClientID + "').value = val; } \n" +
                "function AddProduct(productIDs, quantities, options) { \n" +
                    "setProduct(productIDs); \n" +
                    "setQuantity(quantities); \n" +
                    "setOptions(options); \n" +
                    this.Page.ClientScript.GetPostBackEventReference(this.btnAddProduct, null) +
                ";} \n"
            ));
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

        // Hide columns with identifiers
        gridData.Columns[0].Visible = false;
        gridData.Columns[1].Visible = false;
        gridData.Columns[2].Visible = false;
        gridData.Columns[3].Visible = false;
    }


    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lblTitle.Text = ResHelper.GetString("ShoppingCart.CartContent");
        //bool currencySelected = false;

        if ((ShoppingCartInfoObj != null) && (ShoppingCartInfoObj.CountryID == 0) && (CMSContext.CurrentSite != null))
        {
            ShoppingCartInfoObj.CountryID = CMSContext.CurrentSite.SiteDefaultCountryID;
        }

        // Initialization
        imgNewItem.ImageUrl = GetImageUrl("Objects/Ecommerce_OrderItem/add.png");
        lblCurrency.Text = ResHelper.GetString("Ecommerce.ShoppingCartContent.Currency");
        lblCoupon.Text = ResHelper.GetString("Ecommerce.ShoppingCartContent.Coupon");
        lnkNewItem.Text = ResHelper.GetString("Ecommerce.ShoppingCartContent.AddItem");
        btnUpdate.Text = ResHelper.GetString("Ecommerce.ShoppingCartContent.BtnUpdate");
        btnEmpty.Text = ResHelper.GetString("Ecommerce.ShoppingCartContent.BtnEmpty");
        btnEmpty.OnClientClick = "return confirm(" + ScriptHelper.GetString(ResHelper.GetString("Ecommerce.ShoppingCartContent.EmptyConfirm")) + ");";
        //this.TitleText = ResHelper.GetString("order_new.cartcontent.title");
        lnkNewItem.OnClientClick = "OpenAddProductDialog('" + GetCMSDeskShoppingCartSessionName() + "');return false;";
        gridData.Columns[4].HeaderText = ResHelper.GetString("general.remove");
        gridData.Columns[5].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.SKUName");
        gridData.Columns[6].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.SKUUnits");
        gridData.Columns[7].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.UnitPrice");
        gridData.Columns[8].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.UnitDiscount");
        gridData.Columns[9].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.Tax");
        gridData.Columns[10].HeaderText = ResHelper.GetString("Ecommerce.ShoppingCartContent.Subtotal");

        // Registre product price detail javascript
        StringBuilder script = new StringBuilder();
        script.Append("function ShowProductPriceDetail(cartItemGuid, sessionName){");
        script.Append("if (sessionName != \"\"){sessionName =  \"&cart=\" + sessionName;}");
        string detailUrl = "~/CMSModules/Ecommerce/Controls/ShoppingCart/ShoppingCartSKUPriceDetail.aspx";
        if (this.IsLiveSite)
        {
            detailUrl = "~/CMSModules/Ecommerce/CMSPages/ShoppingCartSKUPriceDetail.aspx";
        }
        script.Append("modalDialog('" + ResolveUrl(detailUrl) + "?itemguid=' + cartItemGuid + sessionName, 'ProductPriceDetail', 550, 500);}");
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ProductPriceDetail", ScriptHelper.GetScript(script.ToString()));

        script = new StringBuilder();
        script.Append("function OpenOrderItemDialog(cartItemGuid, sessionName){");
        script.Append("if (sessionName != \"\"){sessionName =  \"&cart=\" + sessionName;}");
        script.Append("modalDialog('" + ResolveUrl("~/CMSModules/Ecommerce/Controls/ShoppingCart/OrderItemEdit.aspx") + "?itemguid=' + cartItemGuid + sessionName, 'OrderItemEdit', 440, 280);}");
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "OrderItemEdit", ScriptHelper.GetScript(script.ToString()));

        // Hide "add product" link if it is live site order
        if (!this.ShoppingCartControl.IsInternalOrder)
        {
            pnlNewItem.Visible = false;

            this.ShoppingCartControl.ButtonBack.Text = ResHelper.GetString("Ecommerce.CartContent.ButtonBackText");
            this.ShoppingCartControl.ButtonBack.CssClass = "LongButton";
            this.ShoppingCartControl.ButtonNext.Text = ResHelper.GetString("Ecommerce.CartContent.ButtonNextText");

            if (!this.ShoppingCartControl.IsCurrentStepPostBack)
            {
                int productId = QueryHelper.GetInteger("productId", 0);
                int quantity = QueryHelper.GetInteger("quantity", 0);
                string[] options = QueryHelper.GetString("options", String.Empty).Split(',');

                AddProducts(productId, quantity, GetIntOptions(options));
            }
        }

        // Set sending order notification when editing existing order according to the site settings
        if (this.ShoppingCartControl.CheckoutProcessType == CheckoutProcessEnum.CMSDeskOrderItems)
        {
            if (!this.ShoppingCartControl.IsCurrentStepPostBack)
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(this.ShoppingCartInfoObj.ShoppingCartSiteID);
                if (si != null)
                {
                    chkSendEmail.Checked = si.SiteSendOrderNotification;
                }
            }
            chkSendEmail.Visible = true;
            chkSendEmail.Text = ResHelper.GetString("ShoppingCartContent.SendEmail");
        }

        if (this.ShoppingCartControl.CheckoutProcessType == CheckoutProcessEnum.CMSDeskOrderItems)
        {
            this.ShoppingCartControl.ButtonBack.Visible = false;
            this.ShoppingCartInfoObj.CheckAvailableItems = false;

            if ((!ExistAnotherStepsExceptPayment) && (this.ShoppingCartControl.PaymentGatewayProvider == null))
            {
                this.ShoppingCartControl.ButtonNext.Text = ResHelper.GetString("General.OK");
            }
            else
            {
                this.ShoppingCartControl.ButtonNext.Text = ResHelper.GetString("general.next");
            }
        }

        // fill dropdownlist
        if (!this.ShoppingCartControl.IsCurrentStepPostBack)
        {
            if ((this.ShoppingCartInfoObj.CartItems.Count > 0) || this.ShoppingCartControl.IsInternalOrder)
            {
                //this.ShoppingCartControl.IsCurrentStepPostBack = true;
                drpCurrency.Items.Clear();
                DataSet ds = null;
                if (this.ShoppingCartControl.CheckoutProcessType == CheckoutProcessEnum.CMSDeskOrderItems)
                {
                    ds = CurrencyInfoProvider.GetAllCurrencies();
                }
                else
                {
                    ds = CurrencyInfoProvider.GetCurrenciesWithExchangeRate();
                }
                if (!DataHelper.IsEmpty(ds))
                {
                    drpCurrency.DataTextField = "CurrencyDisplayName";
                    drpCurrency.DataValueField = "CurrencyID";
                    drpCurrency.DataSource = ds;
                    drpCurrency.DataBind();
                }

                // Add site default currency if it is not already included in currency dropdown list
                if (CMSContext.CurrentSite != null)
                {
                    CurrencyInfo defaultCurrency = CurrencyInfoProvider.GetCurrencyInfo(CMSContext.CurrentSite.SiteDefaultCurrencyID);
                    if (defaultCurrency != null)
                    {
                        bool isIncluded = false;
                        for (int i = 0; i < drpCurrency.Items.Count; i++)
                        {
                            if (drpCurrency.Items[i].Value == defaultCurrency.CurrencyID.ToString())
                            {
                                isIncluded = true;
                                break;
                            }
                        }
                        if (isIncluded == false)
                        {
                            ListItem siteDefaultCurrencyItem = new ListItem(defaultCurrency.CurrencyDisplayName, defaultCurrency.CurrencyID.ToString());
                            drpCurrency.Items.Add(siteDefaultCurrencyItem);
                        }
                    }
                }

                // Show/Hide dropdownlist with currencies
                pnlCurrency.Visible = (drpCurrency.Items.Count > 1);


                if (this.ShoppingCartInfoObj != null)
                {
                    if (ShoppingCartInfoObj.ShoppingCartCurrencyID == 0)
                    {
                        // select customer preffered currency                    
                        if (ShoppingCartInfoObj.CustomerInfoObj != null)
                        {
                            ShoppingCartInfoObj.ShoppingCartCurrencyID = ShoppingCartInfoObj.CustomerInfoObj.CustomerPreferredCurrencyID;
                            // select shopping cart currency in dropdown list
                            ListItem li = drpCurrency.Items.FindByValue(ShoppingCartInfoObj.ShoppingCartCurrencyID.ToString());
                            if (li != null)
                            {
                                li.Selected = true;
                            }
                            else
                            {
                                ShoppingCartInfoObj.ShoppingCartCurrencyID = 0;
                            }
                        }
                    }

                    if (ShoppingCartInfoObj.ShoppingCartCurrencyID == 0)
                    {
                        if (CMSContext.CurrentSite != null)
                        {
                            ShoppingCartInfoObj.ShoppingCartCurrencyID = CMSContext.CurrentSite.SiteDefaultCurrencyID;
                        }
                    }
                    if (ShoppingCartInfoObj.ShoppingCartCurrencyID > 0)
                    {
                        // select shopping cart currency in dropdown list
                        ListItem li = drpCurrency.Items.FindByValue(ShoppingCartInfoObj.ShoppingCartCurrencyID.ToString());
                        if (li != null)
                        {
                            li.Selected = true;
                        }
                    }

                    if (ShoppingCartInfoObj.ShoppingCartDiscountCouponID > 0)
                    {
                        // fill textbox with discount coupon code
                        DiscountCouponInfo dci = DiscountCouponInfoProvider.GetDiscountCouponInfo(ShoppingCartInfoObj.ShoppingCartDiscountCouponID);
                        if (dci != null)
                        {
                            if (this.ShoppingCartInfoObj.IsCreatedFromOrder || dci.IsValid)
                            {
                                txtCoupon.Text = dci.DiscountCouponCode;
                            }
                            else
                            {
                                this.ShoppingCartInfoObj.ShoppingCartDiscountCouponID = 0;
                            }
                        }
                    }
                }
            }

            ReloadData();
        }

        // Turn on available items checking after content is loaded
        ShoppingCartInfoObj.CheckAvailableItems = true;

        // Change style according to enabled state of the currency
        EcommerceFunctions.MarkEnabledAndDisabledItems(drpCurrency, "CurrencyID", "CurrencyEnabled");
    }


    /// <summary>
    /// On btnUpdate click event.
    /// </summary>
    protected void btnUpdate_Click1(object sender, EventArgs e)
    {
        if (this.ShoppingCartInfoObj != null)
        {
            this.ShoppingCartInfoObj.ShoppingCartCurrencyID = ValidationHelper.GetInteger(drpCurrency.SelectedValue, 0);

            if (this.ShoppingCartInfoObj.CartItems != null)
            {
                // Skip if method was called by btnAddProduct
                if (sender != this.btnAddProduct)
                {
                    foreach (GridViewRow row in gridData.Rows)
                    {
                        // Get shopping cart item Guid
                        Guid cartItemGuid = ValidationHelper.GetGuid(((Label)row.Cells[1].Controls[1]).Text, Guid.Empty);

                        // Try to find shopping cart item in the list
                        ShoppingCartItemInfo cartItem = this.ShoppingCartInfoObj.GetShoppingCartItemInfo(cartItemGuid);
                        if (cartItem != null)
                        {
                            // If product and its product options should be removed
                            if (((CheckBox)row.Cells[4].Controls[1]).Checked && (sender != null))
                            {
                                // Remove product and its product option from list
                                this.ShoppingCartInfoObj.RemoveShoppingCartItem(cartItemGuid);

                                if (!this.ShoppingCartControl.IsInternalOrder)
                                {
                                    // Delete product from database
                                    ShoppingCartItemInfoProvider.DeleteShoppingCartItem(cartItemGuid);
                                }
                            }
                            // If product units has changed
                            else if (!cartItem.IsProductOption)
                            {
                                // Get number of units
                                int itemUnits = ValidationHelper.GetInteger(((TextBox)(row.Cells[6].Controls[1])).Text.Trim(), 0);
                                if ((itemUnits > 0) && (cartItem.CartItemUnits != itemUnits))
                                {
                                    // Update units of the parent product
                                    cartItem.CartItemUnits = itemUnits;

                                    // Update units of the child product options
                                    foreach (ShoppingCartItemInfo option in cartItem.ProductOptions)
                                    {
                                        option.CartItemUnits = itemUnits;
                                    }

                                    if (!this.ShoppingCartControl.IsInternalOrder)
                                    {
                                        ShoppingCartItemInfoProvider.SetShoppingCartItemInfo(cartItem);

                                        // Update product options in database
                                        foreach (ShoppingCartItemInfo option in cartItem.ProductOptions)
                                        {
                                            ShoppingCartItemInfoProvider.SetShoppingCartItemInfo(option);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }

            if (txtCoupon.Text.Trim() != "")
            {
                // Get discount info
                DiscountCouponInfo dci = DiscountCouponInfoProvider.GetDiscountCouponInfo(txtCoupon.Text.Trim());
                // Do not validate coupon when editing existing order and coupon code was not changed, otherwise process validation
                if ((dci != null) && (((this.ShoppingCartInfoObj.IsCreatedFromOrder) && (ShoppingCartInfoObj.ShoppingCartDiscountCouponID == dci.DiscountCouponID)) || dci.IsValid))
                {
                    ShoppingCartInfoObj.ShoppingCartDiscountCouponID = dci.DiscountCouponID;
                    if (!this.ShoppingCartInfoObj.IsDiscountCouponApplied)
                    {
                        // Discount coupon code is valid but not applied to any product of the shopping cart
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("ShoppingCart.DiscountCouponNotApplied");
                    }
                }
                else
                {
                    ShoppingCartInfoObj.ShoppingCartDiscountCouponID = 0;

                    // Discount coupon is not valid
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("ecommerce.error.couponcodeisnotvalid");
                }
            }
            else
            {
                ShoppingCartInfoObj.ShoppingCartDiscountCouponID = 0;
            }

            ReloadData();
        }
    }


    /// <summary>
    /// On btnEmpty click event.
    /// </summary>
    protected void btnEmpty_Click1(object sender, EventArgs e)
    {
        if (this.ShoppingCartInfoObj != null)
        {
            ShoppingCartInfoProvider.EmptyShoppingCart(this.ShoppingCartInfoObj);
            ReloadData();
        }
    }


    /// <summary>
    /// Validates this step.
    /// </summary>
    public override bool IsValid()
    {
        // Check modify permissions
        if (this.ShoppingCartControl.CheckoutProcessType == CheckoutProcessEnum.CMSDeskOrderItems)
        {
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
            {
                CMSEcommercePage.RedirectToCMSDeskAccessDenied("CMS.Ecommerce", "EcommerceModify");
            }
        }

        // allow to go to the next step only if shopping cart contains some products
        bool IsValid = (ShoppingCartInfoObj.CartItems.Count > 0);

        if (!IsValid)
        {
            HideCartContentWhenEmpty();
        }

        if (this.ShoppingCartInfoObj.IsCreatedFromOrder)
        {
            IsValid = true;
        }

        if (!IsValid)
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Ecommerce.Error.InsertSomeProducts");
        }

        return IsValid;
    }


    /// <summary>
    /// Process this step.
    /// </summary>
    public override bool ProcessStep()
    {
        // Shopping cart units are already saved in database (on "Update" or on "btnAddProduct_Click" actions) 

        bool isOK = false;

        if (ShoppingCartInfoObj != null)
        {
            // Check available items before "Check out"
            ShoppingCartInfoProvider.EvaluateShoppingCartContent(ShoppingCartInfoObj);

            if (ShoppingCartInfoObj.ItemsNotAvailable)
            {
                // Show error message with new data
                ReloadData();
            }
            //else if (this.ShoppingCartInfoObj.IsCreatedFromOrder)
            else if (this.ShoppingCartControl.CheckoutProcessType == CheckoutProcessEnum.CMSDeskOrderItems)
            {
                // indicates wheter order saving process is successful
                isOK = true;

                try
                {
                    ShoppingCartInfoProvider.SetOrder(this.ShoppingCartInfoObj);
                }
                catch
                {
                    isOK = false;
                }

                if (isOK)
                {
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");

                    // Send order notification when editing existing order
                    if (this.ShoppingCartControl.CheckoutProcessType == CheckoutProcessEnum.CMSDeskOrderItems)
                    {
                        if (chkSendEmail.Checked)
                        {
                            OrderInfoProvider.SendOrderNotificationToAdministrator(this.ShoppingCartInfoObj);
                            OrderInfoProvider.SendOrderNotificationToCustomer(this.ShoppingCartInfoObj);
                        }
                    }
                    // Send order notification emails when on the live site
                    else if (CMSContext.CurrentSite.SiteSendOrderNotification)
                    {
                        OrderInfoProvider.SendOrderNotificationToAdministrator(this.ShoppingCartInfoObj);
                        OrderInfoProvider.SendOrderNotificationToCustomer(this.ShoppingCartInfoObj);
                    }


                    if (this.ShoppingCartControl.CheckoutProcessType == CheckoutProcessEnum.CMSDeskOrderItems)
                    {
                        ShoppingCartInfoObj.ReloadOrderSKUItems();
                        ShoppingCartInfoObj.ClearShoppingCartHashtables();
                    }
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Ecommerce.OrderPreview.ErrorOrderSave");
                }
            }
            // go to the next step
            else
            {
                // Save other options
                if (!this.ShoppingCartControl.IsInternalOrder)
                {
                    ShoppingCartInfoProvider.SetShoppingCartInfo(ShoppingCartInfoObj);
                }
                isOK = true;
            }
        }

        return isOK;
    }


    /// <summary>
    /// Add product event handler
    /// </summary>
    void btnAddProduct_Click(object sender, EventArgs e)
    {
        // Get strings with productIDs and quantities separated by ';'
        string productIDs = ValidationHelper.GetString(this.hidProductID.Value, "");
        string quantities = ValidationHelper.GetString(this.hidQuantity.Value, "");
        string options = ValidationHelper.GetString(this.hidOptions.Value, "");

        // Add new products to shopping cart
        if ((productIDs != "") && (quantities != ""))
        {
            string[] arrID = productIDs.TrimEnd(';').Split(';');
            string[] arrQuant = quantities.TrimEnd(';').Split(';');
            int[] intOptions = null;
            if (options != "")
            {
                intOptions = GetIntOptions(options.Split(','));
            }

            lblError.Text = "";

            for (int i = 0; i < arrID.Length; i++)
            {
                int skuId = ValidationHelper.GetInteger(arrID[i], 0);

                SKUInfo skuInfo = SKUInfoProvider.GetSKUInfo(skuId);
                if (skuInfo != null)
                {
                    bool cartItemAvailable = true;

                    if (!skuInfo.SKUEnabled)
                    {
                        ltlAlert.Text = ScriptHelper.GetAlertScript(string.Format(ResHelper.GetString("Ecommerce.CartContent.ProductDisabled"), skuInfo.SKUName));
                        cartItemAvailable = false;
                    }
                    else
                    {
                        int quant = ValidationHelper.GetInteger(arrQuant[i], 0);

                        int orderItems = 0;
                        // Get product items from existing order before update
                        if (this.ShoppingCartInfoObj.IsCreatedFromOrder && ShoppingCartInfoObj.OrderSKUItems.Contains(skuInfo.SKUID))
                        {
                            orderItems = ValidationHelper.GetInteger(ShoppingCartInfoObj.OrderSKUItems[skuInfo.SKUID], 0);
                        }

                        // Check available items
                        if (skuInfo.SKUSellOnlyAvailable && (skuInfo.SKUAvailableItems + orderItems < quant + ValidationHelper.GetInteger(this.ShoppingCartInfoObj.CartSKUItems[skuInfo.SKUID], 0)))
                        {
                            lblError.Visible = true;
                            lblError.Text += string.Format(ResHelper.GetString("Ecommerce.AddProduct.ItemsNotAvailable"), ResHelper.LocalizeString(skuInfo.SKUName), skuInfo.SKUAvailableItems);
                            cartItemAvailable = false;
                        }

                        // UNCOMENT FOLLOWING CODE IF YOU WANT TO CHECK PRODUCT OPTIONS FOR AVAILABILITY BEFORE ITEM IS INSERTED TO THE SHOPPING CART
                        //else
                        //{
                        //    // Check availability of the product options
                        //    foreach (int optionId in intOptions)
                        //    {
                        //        SKUInfo optionSKU = SKUInfoProvider.GetSKUInfo(optionId);
                        //        if (optionSKU != null)
                        //        {
                        //            // If product option is not available
                        //            if (!optionSKU.SKUEnabled)
                        //            {
                        //                ltlAlert.Text = ScriptHelper.GetAlertScript(string.Format(ResHelper.GetString("Ecommerce.CartContent.ProductDisabled"), optionSKU.SKUName));
                        //                cartItemAvailable = false;
                        //            }
                        //            else
                        //            {
                        //                // Check available items
                        //                if (optionSKU.SKUSellOnlyAvailable && (optionSKU.SKUAvailableItems < quant + ValidationHelper.GetInteger(this.ShoppingCartInfoObj.CartSKUItems[skuInfo.SKUID], 0)))
                        //                {
                        //                    lblError.Visible = true;
                        //                    lblError.Text += string.Format(ResHelper.GetString("Ecommerce.AddProduct.ItemsNotAvailable"), ResHelper.LocalizeString(optionSKU.SKUName), optionSKU.SKUAvailableItems);
                        //                    cartItemAvailable = false;
                        //                }
                        //            }
                        //        }
                        //    }                                                        
                        //}

                        if (cartItemAvailable)
                        {
                            // Add product to the shopping cart
                            this.ShoppingCartInfoObj.AddShoppingCartItem(skuId, quant, intOptions);

                            // Show empty button
                            btnEmpty.Visible = true;
                        }
                    }
                }
            }
            // Update values in table
            btnUpdate_Click1(this.btnAddProduct, e);
        }

        // Hide cart content when empty
        if (DataHelper.DataSourceIsEmpty(ShoppingCartInfoObj.ShoppingCartContentTable))
        {
            HideCartContentWhenEmpty();
        }
    }


    /// <summary>
    /// Transforms array of string values to array of integer values.
    /// </summary>
    /// <param name="options">Array with string values.</param>
    private static int[] GetIntOptions(string[] options)
    {
        if (options != null)
        {
            // Get product options as integer values
            int[] intOptions = new int[options.Length];
            for (int i = 0; i < options.Length; i++)
            {
                intOptions[i] = ValidationHelper.GetInteger(options[i], 0);
            }
            return intOptions;
        }
        return null;
    }


    // Displays total price
    protected void DisplayTotalPrice()
    {
        pnlPrice.Visible = true;
        lblTotalPriceValue.Text = CurrencyInfoProvider.GetFormatedPrice(ShoppingCartInfoObj.RoundedTotalPrice, ShoppingCartInfoObj.CurrencyInfoObj);
        lblTotalPrice.Text = ResHelper.GetString("ecommerce.cartcontent.totalpricelabel");

        lblShippingPrice.Text = ResHelper.GetString("ecommerce.cartcontent.shippingpricelabel");
        lblShippingPriceValue.Text = CurrencyInfoProvider.GetFormatedPrice(ShoppingCartInfoObj.TotalShipping, ShoppingCartInfoObj.CurrencyInfoObj);
    }


    /// <summary>
    /// Adds product to shopping cart.
    /// </summary>
    /// <param name="skuId">Product SKUID.</param>
    /// <param name="quantity">Product quantity.</param>
    /// <param name="options">Product options' SKUIDs.</param>
    protected void AddProducts(int productId, int quantity, int[] options)
    {
        // Add new products to shopping cart
        if ((productId > 0) && (quantity > 0))
        {
            // Get actual SKU info object from database
            SKUInfo skuObj = SKUInfoProvider.GetSKUInfo(productId);
            if (skuObj != null)
            {
                int orderItems = 0;
                // Get product items from existing order before update
                if (this.ShoppingCartInfoObj.IsCreatedFromOrder && ShoppingCartInfoObj.OrderSKUItems.Contains(skuObj.SKUID))
                {
                    orderItems = ValidationHelper.GetInteger(ShoppingCartInfoObj.OrderSKUItems[skuObj.SKUID], 0);
                }

                // Check whether SKU is enabled
                if (!skuObj.SKUEnabled)
                {
                    lblError.Visible = true;
                    lblError.Text = string.Format(ResHelper.GetString("Ecommerce.CartContent.ProductDisabled"), ResHelper.LocalizeString(skuObj.SKUName));
                }
                // Check available items
                else if (skuObj.SKUSellOnlyAvailable && ((skuObj.SKUAvailableItems + orderItems) < quantity + ValidationHelper.GetInteger(this.ShoppingCartInfoObj.CartSKUItems[skuObj.SKUID], 0)))
                {
                    lblError.Visible = true;
                    lblError.Text = string.Format(ResHelper.GetString("Ecommerce.AddProduct.ItemsNotAvailable"), skuObj.SKUName, skuObj.SKUAvailableItems);
                }
                else
                {

                    // On the live site
                    if (!ShoppingCartControl.IsInternalOrder)
                    {
                        bool updateCart = false;

                        // Assign current shopping cart to current user
                        CurrentUserInfo ui = CMSContext.CurrentUser;
                        if (!ui.IsPublic())
                        {
                            this.ShoppingCartInfoObj.UserInfoObj = ui;
                            updateCart = true;
                        }

                        // Shopping cart is not saved yet
                        if (ShoppingCartInfoObj.ShoppingCartID == 0)
                        {
                            updateCart = true;
                        }

                        // Update shopping cart when required
                        if (updateCart)
                        {
                            ShoppingCartInfoProvider.SetShoppingCartInfo(ShoppingCartInfoObj);
                        }

                        // Add item to the shopping cart
                        ShoppingCartItemInfo product = this.ShoppingCartInfoObj.AddShoppingCartItem(productId, quantity, options);

                        // Update shopping cart item in database
                        ShoppingCartItemInfoProvider.SetShoppingCartItemInfo(product);

                        // Update product options in database
                        foreach (ShoppingCartItemInfo option in product.ProductOptions)
                        {
                            ShoppingCartItemInfoProvider.SetShoppingCartItemInfo(option);
                        }

                    }
                    // In CMSDesk
                    else
                    {
                        ShoppingCartItemInfo product = this.ShoppingCartInfoObj.AddShoppingCartItem(productId, quantity, options);
                    }
                }
            }

            // avoid adding the same product after page refresh

            if (lblError.Text == "")
            {
                // Remove add parameters from URL
                string newUrl = UrlHelper.RemoveParameterFromUrl(URLRewriter.CurrentURL, "quantity");
                newUrl = UrlHelper.RemoveParameterFromUrl(newUrl, "productid");
                newUrl = UrlHelper.RemoveParameterFromUrl(newUrl, "options");
                UrlHelper.Redirect(newUrl);
            }
        }
    }


    /// <summary>
    /// Hides cart content controls when no items are in shopping cart.
    /// </summary>
    protected void HideCartContentWhenEmpty()
    {
        pnlNewItem.Visible = this.ShoppingCartControl.IsInternalOrder;
        pnlPrice.Visible = false;
        btnEmpty.Visible = false;

        if (!this.ShoppingCartControl.IsInternalOrder)
        {
            pnlCurrency.Visible = false;
            plcCoupon.Visible = false;
            gridData.Visible = false;
            this.ShoppingCartControl.ButtonNext.Visible = false;
            lblError.Visible = (lblError.Text != "");
            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("Ecommerce.ShoppingCartEmpty") + "<br />";
        }
    }


    /// <summary>
    /// Reloads the form data
    /// </summary>
    protected void ReloadData()
    {
        gridData.DataSource = ShoppingCartInfoObj.ShoppingCartContentTable;

        // Hide coupon placeholder when no coupons are defined
        if (DiscountCouponInfoProvider.GetDiscountCouponCount() == 0)
        {
            plcCoupon.Visible = false;
        }

        // Bind data
        gridData.DataBind();

        if (!DataHelper.DataSourceIsEmpty(ShoppingCartInfoObj.ShoppingCartContentTable))
        {
            // Display total price
            DisplayTotalPrice();
            // Display/hide discount column
            gridData.Columns[8].Visible = this.ShoppingCartInfoObj.IsDiscountApplied;
        }
        else
        {
            // Hide some items
            HideCartContentWhenEmpty();
        }
    }


    /// <summary>
    /// Returns price detail link
    /// </summary>
    protected string GetPriceDetailLink(object value)
    {
        if ((this.ShoppingCartControl.EnableProductPriceDetail) && (this.ShoppingCartInfoObj.ShoppingCartContentTable != null))
        {
            Guid cartItemGuid = ValidationHelper.GetGuid(value, Guid.Empty);
            if (cartItemGuid != Guid.Empty)
            {
                // Add product price detail
                return "<img src=\"" + GetImageUrl("Design/Controls/UniGrid/Actions/detail.png") + "\" onclick=\"javascript: ShowProductPriceDetail('" + cartItemGuid.ToString() + "','" + GetCMSDeskShoppingCartSessionName() + "')\" alt=\"" + ResHelper.GetString("ShoppingCart.ProductPriceDetail") + "\" class=\"ProductPriceDetailImage\" style=\"cursor:pointer;\" />";
            }
        }

        return "";
    }


    /// <summary>
    /// Returns shopping cart session name
    /// </summary>
    private string GetCMSDeskShoppingCartSessionName()
    {
        switch (this.ShoppingCartControl.CheckoutProcessType)
        {
            case CheckoutProcessEnum.CMSDeskOrder:
                return "CMSDeskNewOrderShoppingCart";

            case CheckoutProcessEnum.CMSDeskCustomer:
                return "CMSDeskNewCustomerOrderShoppingCart";

            case CheckoutProcessEnum.CMSDeskOrderItems:
                return "CMSDeskOrderItemsShoppingCart";

            case CheckoutProcessEnum.LiveSite:
            case CheckoutProcessEnum.Custom:
            default:
                return "";
        }
    }


    protected void drpCurrency_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnUpdate_Click1(null, null);
    }


    public override void ButtonBackClickAction()
    {
        if ((!this.ShoppingCartControl.IsInternalOrder) && (this.ShoppingCartControl.CurrentStepIndex == 0))
        {
            // Continue shopping
            UrlHelper.Redirect(this.ShoppingCartControl.PreviousPageUrl);
        }
        else
        {
            // Standard action
            base.ButtonBackClickAction();
        }
    }


    /// <summary>
    /// Indicates whether there are another checkout process steps after the current step, except payment
    /// </summary>
    private bool ExistAnotherStepsExceptPayment
    {
        get
        {
            return (this.ShoppingCartControl.CurrentStepIndex + 2 <= this.ShoppingCartControl.CheckoutProcessSteps.Count - 1);
        }
    }


    /// <summary>
    /// Displays product error message in shopping cart content table
    /// </summary>
    /// <param name="skuErrorMessage">Error message to be displayed.</param>
    protected static string DisplaySKUErrorMessage(object skuErrorMessage)
    {
        string err = ValidationHelper.GetString(skuErrorMessage, "");
        if (err != "")
        {
            return "<br /><span class=\"ItemsNotAvailable\">" + err + "</span>";
        }
        return "";
    }


    /// <summary>
    /// Returns formated value string.
    /// </summary>
    /// <param name="value">Value to format.</param>
    protected string GetFormattedValue(object value)
    {
        double price = ValidationHelper.GetDouble(value, 0);
        return CurrencyInfoProvider.GetFormatedValue(price, this.ShoppingCartInfoObj.CurrencyInfoObj);
    }


    /// <summary>
    /// Returns formatted and localized SKU name.
    /// </summary>
    /// <param name="skuID">SKU ID.</param>
    /// <param name="value">SKU name.</param>
    /// <param name="parentItemGuid">Product option parent product GUID.</param>    
    protected string GetSKUName(object skuID, object value, object parentItemGuid)
    {
        string name = ResHelper.LocalizeString((string)value);

        // If it is a product option
        if (IsProductOption(parentItemGuid))
        {
            return "<span style=\"font-size:90%\"> - " + HTMLHelper.HTMLEncode(name) + "</span>";
        }
        // If it is a parent product
        else
        {
            // On the live site
            if (!this.ShoppingCartControl.IsInternalOrder)
            {
                return "<a href=\"" + ResolveUrl("~/CMSPages/GetProduct.aspx") + "?productid=" + skuID.ToString() + "\" class=\"CartProductDetailLink\">" + HTMLHelper.HTMLEncode(name) + "</a>";
            }
            // In CMSDesk
            else
            {
                return HTMLHelper.HTMLEncode(name);
            }
        }
    }


    /// <summary>
    /// Determines whether it is a preduct option or not.
    /// </summary>
    /// <param name="parentProductGuid">Parent product GUID.</param>
    protected static bool IsProductOption(object parentProductGuid)
    {
        return (ValidationHelper.GetGuid(parentProductGuid, Guid.Empty) != Guid.Empty);
    }


    protected string GetEditOrderItemLink(object value)
    {
        // When editing existing order and order item editing is enabled -> show edit button
        if ((this.ShoppingCartControl.CheckoutProcessType == CheckoutProcessEnum.CMSDeskOrderItems)
            && CMS.CMSEcommerce.ShoppingCartInfoProvider.EnableOrderItemEditing)
        {
            Guid cartItemGuid = ValidationHelper.GetGuid(value, Guid.Empty);
            if (cartItemGuid != Guid.Empty)
            {
                // Add product price detail
                return "<img src=\"" + GetImageUrl("Design/Controls/UniGrid/Actions/edit.png") + "\" onclick=\"javascript: OpenOrderItemDialog('" + cartItemGuid.ToString() + "','" + GetCMSDeskShoppingCartSessionName() + "')\" alt=\"" + ResHelper.GetString("ShoppingCart.EditOrderItem") + "\" class=\"OrderItemEditLink\" style=\"cursor:pointer;\" />";
            }
        }

        return "";
    }


    /// <summary>
    /// Returns formatted product option units, if it is not product option returns empty string.
    /// </summary>
    /// <param name="skuUnits">SKU units.</param>
    /// <param name="parentProductGuid">Parent product ID.</param>
    protected static string GetProductOptionUnits(object skuUnits, object parentProductGuid)
    {
        if (IsProductOption(parentProductGuid))
        {
            return "<span style=\"padding-left:2px;\">" + Convert.ToString(skuUnits) + "</span>";
        }
        return "";
    }
}

