<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategorySelector.ascx.cs"
    Inherits="CMSModules_Content_FormControls_Categories_CategorySelector" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" />
    </ContentTemplate>
</cms:CMSUpdatePanel>