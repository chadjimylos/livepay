using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;

using CMS.CMSHelper;
using CMS.FormEngine;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.OutputFilter;
using CMS.SiteProvider;

/// <summary>
/// Global CMS Functions
/// </summary>
public static class CMSFunctions
{
    #region "Variables"

    public static bool mAsyncInit = true;

    /// <summary>
    /// If true, some debug is enabled
    /// </summary>
    private static bool? mAnyDebugEnabled = null;

    /// <summary>
    /// If true, some debug is enabled on live site
    /// </summary>
    private static bool? mAnyLiveDebugEnabled = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Returns true if any debug is enabled
    /// </summary>
    public static bool AnyDebugEnabled
    {
        get
        {
            if (mAnyDebugEnabled == null)
            {
                mAnyDebugEnabled =
                    SqlHelperClass.DebugQueries ||
                    CacheHelper.DebugCache ||
                    OutputHelper.DebugOutput ||
                    SecurityHelper.DebugSecurity ||
                    MacroResolver.DebugMacros ||
                    RequestHelper.DebugRequests;
            }

            return mAnyDebugEnabled.Value;
        }
        set
        {
            mAnyDebugEnabled = value;
        }
    }


    /// <summary>
    /// Returns true if any LiveDebug is enabled
    /// </summary>
    public static bool AnyLiveDebugEnabled
    {
        get
        {
            if (mAnyLiveDebugEnabled == null)
            {
                mAnyLiveDebugEnabled =
                    SqlHelperClass.DebugQueriesLive ||
                    CacheHelper.DebugCacheLive ||
                    SecurityHelper.DebugSecurityLive ||
                    MacroResolver.DebugMacros ||
                    RequestHelper.DebugRequestsLive;
            }

            return mAnyLiveDebugEnabled.Value;
        }
        set
        {
            mAnyLiveDebugEnabled = value;
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Initializes the system
    /// </summary>
    public static void Init()
    {
        // Init CMS environment
        CMSContext.Init(mAsyncInit, true);

        // Register the events
        RegisterEvents();
    }


    /// <summary>
    /// Waits until the initialization is completed
    /// </summary>
    public static void WaitForInitialization()
    {
        if (mAsyncInit)
        {
            // Wait until all modules are ready
            while (!CMSContext.ModulesReady)
            {
                Thread.Sleep(20);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="password"></param>
    /// <returns></returns>
    public static bool CheckPassword(string password)
    {

        int upper = 0;
        int lower = 0;
        int numbers = 0;
        int other = 0;

        if (password.Length > 6)
        {
            for (int i = 0; i <= password.Length - 1; i++)
            {
                if (char.IsLetter(password[i]))
                {
                    if (char.IsUpper(password[i]))
                    {
                        upper += 1;
                    }
                    else
                    {
                        lower += 1;
                    }
                }
                else if (char.IsNumber(password[i]))
                {
                    numbers += 1;
                }
                else
                {
                    other += 1;
                }
            }

            int Alphas = upper + lower;
            if (numbers >= 1 && Alphas >= 1){
                return true;
            }else{
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Registers the events
    /// </summary>
    private static void RegisterEvents()
    {
        // Page events
        // PreInit
        CMSPage.OnBeforePagePreInit += CMSPageEvents.BeforePagePreInit;
        CMSPage.OnAfterPagePreInit += CMSPageEvents.AfterPagePreInit;

        // Init
        CMSPage.OnBeforePageInit += CMSPageEvents.BeforePageInit;
        CMSPage.OnAfterPageInit += CMSPageEvents.AfterPageInit;

        // Load
        CMSPage.OnBeforePageLoad += CMSPageEvents.BeforePageLoad;
        CMSPage.OnAfterPageLoad += CMSPageEvents.AfterPageLoad;

        // PreRender
        CMSPage.OnBeforePagePreRender += CMSPageEvents.BeforePagePreRender;
        CMSPage.OnAfterPagePreRender += CMSPageEvents.AfterPagePreRender;

        // Render
        CMSPage.OnBeforePageRender += CMSPageEvents.BeforePageRender;
        CMSPage.OnAfterPageRender += CMSPageEvents.AfterPageRender;


        // Control events
        // Init
        CMSUserControl.OnBeforeUserControlInit += CMSUserControlEvents.BeforeUserControlInit;
        CMSUserControl.OnAfterUserControlInit += CMSUserControlEvents.AfterUserControlInit;

        // Load
        CMSUserControl.OnBeforeUserControlLoad += CMSUserControlEvents.BeforeUserControlLoad;
        CMSUserControl.OnAfterUserControlLoad += CMSUserControlEvents.AfterUserControlLoad;

        // PreRender
        CMSUserControl.OnBeforeUserControlPreRender += CMSUserControlEvents.BeforeUserControlPreRender;
        CMSUserControl.OnAfterUserControlPreRender += CMSUserControlEvents.AfterUserControlPreRender;

        // Render
        CMSUserControl.OnBeforeUserControlRender += CMSUserControlEvents.BeforeUserControlRender;
        CMSUserControl.OnAfterUserControlRender += CMSUserControlEvents.AfterUserControlRender;
    }

    #endregion
}
