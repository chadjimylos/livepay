using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_ProductOptions_OptionCategory_New : CMSEcommercePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Field validator error messages initialization
        rfvCategoryName.ErrorMessage = ResHelper.GetString("general.requirescodename");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");

        // Control initializations				
        btnOk.Text = ResHelper.GetString("General.OK");

        // Initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("optioncategory_edit.itemlistlink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/ProductOptions/OptionCategory_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = ResHelper.GetString("optioncategory_list.newitemcaption");
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.HelpTopicName = "new_option_category";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        // Check input value from textboxs
        string errorMessage = new Validator().NotEmpty(txtDisplayName.Text, ResHelper.GetString("general.requiresdisplayname"))
            .NotEmpty(txtCategoryName.Text, ResHelper.GetString("general.requirescodename"))
            .IsIdentificator(txtCategoryName.Text, ResHelper.GetString("optioncategory_new.errorNotIdentificator")).Result;

        if (errorMessage == "")
        {
            // Category code name must to be unique
            OptionCategoryInfo optionCategoryObj = OptionCategoryInfoProvider.GetOptionCategoryInfo(txtCategoryName.Text.Trim());

            // If category code name value is unique														
            if (optionCategoryObj == null)
            {
                // Create, fill and set OptionCategoryInfo object
                optionCategoryObj = new OptionCategoryInfo();
                optionCategoryObj.CategoryDisplayName = txtDisplayName.Text.Trim();
                optionCategoryObj.CategoryName = txtCategoryName.Text.Trim();
                optionCategoryObj.CategorySelectionType = OptionCategorySelectionTypeEnum.Dropdownlist;
                optionCategoryObj.CategoryEnabled = true;
                optionCategoryObj.CategoryDefaultRecord = "";
                optionCategoryObj.CategoryDefaultOptions = "";
                OptionCategoryInfoProvider.SetOptionCategoryInfo(optionCategoryObj);

                UrlHelper.Redirect("OptionCategory_Edit.aspx?categoryId=" + Convert.ToString(optionCategoryObj.CategoryID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("optioncategory_new.errorExistingCodeName");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
