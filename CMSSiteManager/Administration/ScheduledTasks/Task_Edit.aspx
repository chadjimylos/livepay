<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Task_Edit.aspx.cs" Inherits="CMSSiteManager_Administration_ScheduledTasks_Task_Edit"
    Theme="Default" ValidateRequest="false" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Scheduled tasks - Task Edit" %>

<%@ Register Src="~/CMSAdminControls/UI/Selectors/ScheduleInterval.ascx" TagName="ScheduleInterval"
    TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td>
                <asp:Label runat="server" ID="lblTaskDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtTaskDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtTaskDisplayName"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblTaskName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtTaskName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtTaskName"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblTaskAssemblyName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtTaskAssemblyName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvAssembly" runat="server" ControlToValidate="txtTaskAssemblyName"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblTaskClass" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtTaskClass" runat="server" CssClass="TextBoxField" MaxLength="200" />
                <asp:RequiredFieldValidator ID="rfvClass" runat="server" ControlToValidate="txtTaskClass"
                    Display="Dynamic"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblTaskInterval" EnableViewState="false" />
            </td>
            <td>
                <cms:ScheduleInterval ID="ScheduleInterval1" runat="server" />
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblTaskData" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtTaskData" runat="server" TextMode="MultiLine" CssClass="TextAreaField" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblTaskEnabled" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox ID="chkTaskEnabled" runat="server" Checked="true" CssClass="CheckBoxMovedLeft" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblTaskDeleteAfterLastRun" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox ID="chkTaskDeleteAfterLastRun" runat="server" Checked="false" CssClass="CheckBoxMovedLeft" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label runat="server" ID="lblServerName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtServerName" runat="server" CssClass="TextBoxField" MaxLength="100" /><br />
                <asp:CheckBox ID="chkAllServers" runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
