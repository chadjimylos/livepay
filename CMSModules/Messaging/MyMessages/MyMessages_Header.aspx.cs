using System;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.Messaging;

public partial class CMSModules_Messaging_MyMessages_MyMessages_Header : CMSMyMessagesPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check 'read' permissions
        if (QueryHelper.GetInteger("userid", 0) != CMSContext.CurrentUser.UserID)
        {
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Messaging", "Read"))
            {
                RedirectToAccessDenied("CMS.Messaging", "Read");
            }
        }
        CurrentMaster.Title.TitleText = ResHelper.GetString("mydesk.mymessages");
        CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Messaging/module.png");
        CurrentMaster.Title.HelpTopicName = "my_messages_inbox";
        CurrentMaster.Title.HelpName = "helpTopic";

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }
    }


    #region "Private methods"

    /// <summary>
    /// Initializes menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string[,] tabs = new string[4, 4];
        tabs[0, 0] = ResHelper.GetString("MyMessages.Inbox");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'my_messages_inbox');";
        tabs[0, 2] = "MyMessages_Inbox.aspx" + Request.Url.Query;

        tabs[1, 0] = ResHelper.GetString("MyMessages.Outbox");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'my_messages_outbox');";
        tabs[1, 2] = "MyMessages_Outbox.aspx" + Request.Url.Query;

        tabs[2, 0] = ResHelper.GetString("MyMessages.ContactList");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'my_messages_contactlist');";
        tabs[2, 2] = "MyMessages_ContactList.aspx" + Request.Url.Query;

        tabs[3, 0] = ResHelper.GetString("MyMessages.IgnoreList");
        tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'my_messages_ignorelist');";
        tabs[3, 2] = "MyMessages_IgnoreList.aspx" + Request.Url.Query;
        CurrentMaster.Tabs.UrlTarget = "messagesContent";
        CurrentMaster.Tabs.Tabs = tabs;
    }

    #endregion
}
