<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Role_New.aspx.cs" Inherits="CMSSiteManager_Administration_Roles_Role_New"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Roles - New Role" Theme="Default" %>

<%@ Register Src="~/CMSSiteManager/Administration/Roles/Controls/RoleEdit.ascx" TagName="RoleEdit" TagPrefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:RoleEdit Id="roleEditElem" runat="server" />
</asp:Content>
