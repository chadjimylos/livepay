using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Blogs;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.URLRewritingEngine;
using CMS.UIControls;
using CMS.SiteProvider;
using CMS.SettingsProvider;

public partial class CMSModules_Blogs_Controls_NewSubscription : CMSUserControl
{
    #region "Private variables"

    private BlogProperties mBlogProperties = null;
    int mDocumentId = 0;

    #endregion


    #region "Public properties"
    
    /// <summary>
    /// Document ID
    /// </summary>
    public int DocumentID
    {
        get 
        { 
            return mDocumentId; 
        }
        set 
        {
            mDocumentId = value; 
        }
    }


    /// <summary>
    /// Properties passed from the upper control
    /// </summary>
    public BlogProperties BlogProperties 
    {
        get 
        {
            return this.mBlogProperties;
        }
        set 
        {
            this.mBlogProperties = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        string valGroup = this.UniqueID;

        lblEmail.ResourceString = "blog.subscription.email";
        btnOk.ResourceString = "blog.subscription.subscribe";
        btnOk.ValidationGroup = valGroup;

        rfvEmailRequired.ErrorMessage = ResHelper.GetString("blog.subscription.noemail");
        rfvEmailRequired.ValidationGroup = valGroup;
                
        this.revEmailValid.ValidationGroup = valGroup;
        this.revEmailValid.ErrorMessage = ResHelper.GetString("general.correctemailformat");
        this.revEmailValid.ValidationExpression = ValidationHelper.EmailRegExp.ToString();
    }


    /// <summary>
    /// Pre-fill user e-mail
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (!IsPostBack)
        {
            // Pre-fill user e-mail address to empty textbox for the first time
            if ((txtEmail.Text.Trim() == "") && (CMSContext.CurrentUser != null))
            {
                txtEmail.Text = CMSContext.CurrentUser.Email;
            }
        }
    }


    /// <summary>
    /// OK click handler
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check banned IP
        if (!BannedIPInfoProvider.IsAllowed(CMSContext.CurrentSiteName, BanControlEnum.AllNonComplete))
        {
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("General.BannedIP");
            return;
        }

        // Check input fields
        string email = txtEmail.Text.Trim();
        string result = new Validator().NotEmpty(email, rfvEmailRequired.ErrorMessage)
            .IsEmail(email, ResHelper.GetString("general.correctemailformat")).Result;

        // Try to subscribe new subscriber
        if (result == "")
        {
            if (this.DocumentID > 0)
            {
                BlogPostSubscriptionInfo bpsi = BlogPostSubscriptionInfoProvider.GetBlogPostSubscriptionInfo(email, this.DocumentID);

                // Check for duplicit subscriptions
                if (bpsi == null)
                {
                    bpsi = new BlogPostSubscriptionInfo();
                    bpsi.SubscriptionPostDocumentID = this.DocumentID;
                    bpsi.SubscriptionEmail = email;

                    // Update user id for logged users (except the public users)
                    if ((CMSContext.CurrentUser != null) && (!CMSContext.CurrentUser.IsPublic()))
                    {
                        bpsi.SubscriptionUserID = CMSContext.CurrentUser.UserID;
                    }

                    BlogPostSubscriptionInfoProvider.SetBlogPostSubscriptionInfo(bpsi);

                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("blog.subscription.beensubscribed");

                    // Clear form after successful subscription
                    txtEmail.Text = "";
                }
                else
                {
                    result = ResHelper.GetString("blog.subscription.emailexists");
                }
            }
            else
            {
                result = ResHelper.GetString("general.invalidid");
            }
        }

        if (result != String.Empty)
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }

    #endregion
}
