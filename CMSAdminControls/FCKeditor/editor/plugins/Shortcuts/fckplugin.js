﻿// Shortcut save command
var SCSaveCommand = function()
{
	this.Name = 'ShortcutSave';
}

SCSaveCommand.prototype.Execute = function()
{
    if (!window.disableShortcuts && (window.allowShortcuts || window.parent.allowShortcuts))
    {
        if (window.SaveDocument)
        {
            SaveDocument();
        }
        else if ((window.parent != null) && window.parent.SaveDocument)
        {
            window.parent.SaveDocument();
        }
    }
}

SCSaveCommand.prototype.GetState = function()
{
	return FCK_TRISTATE_OFF ;
}

FCKCommands.RegisterCommand( 'ShortcutSave', new SCSaveCommand( 'ShortcutSave') ) ;
