<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletter_Iframe_Edit.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_Iframe_Edit" Theme="Default"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Edit issue</title>
</head>
<body>

    <script type="text/javascript">
        //<![CDATA[
        //set id of curently focused region to focusedRegionID (declared in LoadRegionList() in code behind)
        function RememberFocusedRegion() {
            for (i = 0; i < regions.length; i++) //regions array is declared in LoadRegionList() in code behind
            {
                if (window.FCKeditorAPI != null) {
                    var oEditor = FCKeditorAPI.GetInstance(regions[i]);
                    if ((oEditor != null) && (oEditor.HasFocus)) {
                        focusedRegionID = regions[i];
                        break;
                    } 
                }
            }
        }

        // Insert desired HTML at the current cursor position of the FCK editor
        function InsertHTML(htmlString) {
            if (focusedRegionID == '') //focusedRegionID is declared in LoadRegionList() in code behind
            {
                RememberFocusedRegion();
            }

            if (focusedRegionID != '') //focusedRegionID is declared in LoadRegionList() in code behind
            {
                if (window.FCKeditorAPI != null) {
                    // Get the editor instance that we want to interact with.
                    var oEditor = FCKeditorAPI.GetInstance(focusedRegionID);
                    if (oEditor != null) {
                        // Check the active editing mode.
                        if (oEditor.EditMode == FCK_EDITMODE_WYSIWYG) {
                            // Insert the desired HTML.
                            oEditor.InsertHtml(htmlString);
                        }
                        else {
                            alert('You must be on WYSIWYG mode!');
                        } 
                    }
                }
            }
            return false;
        }

        //]]>
    </script>

    <form id="form1" runat="server">
    <asp:Label runat="server" ID="lblInfo" EnableViewState="false" Visible="false" />
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    <asp:PlaceHolder ID="plcContent" runat="server" />
    <cms:CMSButton runat="server" ID="btnHidden" EnableViewState="false" CssClass="HiddenButton"
        OnClick="btnHidden_Click" />
    <asp:HiddenField runat="server" ID="hdnIssueId" />
    <asp:HiddenField runat="server" ID="hdnNext" />
    <asp:HiddenField ID="hdnNewsletterSubject" runat="server" />
    <asp:HiddenField ID="hdnNewsletterShowInArchive" runat="server" />
    &nbsp;
    <asp:Literal runat="server" ID="ltlScript2" EnableViewState="false" />
    </form>
</body>
</html>
