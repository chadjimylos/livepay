CREATE TABLE [Community_Friend] (
		[FriendID]                  int NOT NULL IDENTITY(1, 1),
		[FriendRequestedUserID]     int NOT NULL,
		[FriendUserID]              int NOT NULL,
		[FriendRequestedWhen]       datetime NOT NULL,
		[FriendComment]             nvarchar(max) NULL,
		[FriendApprovedBy]          int NULL,
		[FriendApprovedWhen]        datetime NULL,
		[FriendRejectedBy]          int NULL,
		[FriendRejectedWhen]        datetime NULL,
		[FriendGUID]                uniqueidentifier NOT NULL,
		[FriendStatus]              int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Community_Friend]
	ADD
	CONSTRAINT [PK_Community_Friend]
	PRIMARY KEY
	([FriendID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Community_Friend]
	ADD
	CONSTRAINT [DEFAULT_Community_Friend_FriendStatus]
	DEFAULT ((0)) FOR [FriendStatus]
IF OBJECT_ID(N'[Community_Friend]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Friend_FriendApprovedBy_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Community_Friend]
			ADD CONSTRAINT [FK_CMS_Friend_FriendApprovedBy_CMS_User]
			FOREIGN KEY ([FriendApprovedBy]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Community_Friend]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Friend_FriendRejectedBy_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Community_Friend]
			ADD CONSTRAINT [FK_CMS_Friend_FriendRejectedBy_CMS_User]
			FOREIGN KEY ([FriendRejectedBy]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Community_Friend]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Friend_FriendRequestedUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Community_Friend]
			ADD CONSTRAINT [FK_CMS_Friend_FriendRequestedUserID_CMS_User]
			FOREIGN KEY ([FriendRequestedUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Community_Friend]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Friend_FriendUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Community_Friend]
			ADD CONSTRAINT [FK_CMS_Friend_FriendUserID_CMS_User]
			FOREIGN KEY ([FriendUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_Community_Friend_FriendApprovedBy]
	ON [Community_Friend] ([FriendApprovedBy])
	ON [PRIMARY]
CREATE INDEX [IX_Community_Friend_FriendRejectedBy]
	ON [Community_Friend] ([FriendRejectedBy])
	ON [PRIMARY]
CREATE INDEX [IX_Community_Friend_FriendRequestedUserID_FriendStatus]
	ON [Community_Friend] ([FriendRequestedUserID], [FriendStatus])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Community_Friend_FriendUserID_FriendStatus]
	ON [Community_Friend] ([FriendUserID], [FriendStatus])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
