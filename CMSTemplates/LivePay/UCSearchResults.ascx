﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSearchResults.ascx.vb" Inherits="CMSTemplates_LivePay_UCSearchResults" %>
<div style="margin-top:-10px;padding-bottom:10px;padding-left:20px" id="divResults" runat="server" visible="false">
<%= ResHelper.LocalizeString("{$=Η αναζήτηση σας επέστρεψε <b>|en-us=The search return <b>$}")%>
<asp:Label ID="lblResults" runat="server" ></asp:Label>
<%= ResHelper.LocalizeString("{$=</b> αποτελέσματα|en-us=</b> results$}")%>
</div>
<asp:UpdatePanel ID="SrcUpdatePanel" runat="server" UpdateMode="Always">
<ContentTemplate >
 <asp:GridView ID="GVMerchants" GridLines="None" ShowHeader="false" runat="server" PageSize="20" AutoGenerateColumns="false" AllowPaging="true" Width="690" >
 
  <Columns >
    <asp:TemplateField    >
        <ItemTemplate>
            <div id="GvRow" runat="server" class="SeachResults-new"><div style="color:#094595;">&nbsp;&rsaquo;&rsaquo;&nbsp;&nbsp;<a style="text-decoration:none" href="<%# CMS.CMSHelper.CMSContext.GetUrl(Eval("nodealiasPath")) %>"><%# Eval("DiscreetTitle")%></a></div></div>
        </ItemTemplate>
    </asp:TemplateField>
  </Columns>
  <EmptyDataTemplate >
    <div><%= ResHelper.LocalizeString("{$=Η αναζήτηση σας δεν επέστρεψε κάποιο αποτέλεσμα.<br>Παρακαλώ δοκιμάστε πάλι ή επιλέξτε από το μενού 'Διαθέσιμες Πληρωμές' στα αριστερά.|en-us=Your query did not return any results.<br>Please try again or pick a payment from the 'Available Payments' menu on the left.$}")%></div>
  </EmptyDataTemplate>
   <PagerTemplate >
    <div class="SrcHisPayPagerSpace" >
        <div>
            <div class="GridPagerNoLinkDivLeftBefore"><asp:linkbutton ID="lnkPrev" CssClass="PagerText" onclick="ChangePageByLinkNumber_Before" runat="server"><%= ResHelper.LocalizeString("{$=&laquo; Προηγούμενη|en-us=&laquo; Previous$}")%></asp:linkbutton>&nbsp;</div>
            <div class="GridPagerNoLinkDivLeftFirst"><asp:linkbutton ID="lnkFirstPage" CssClass="PagerText" onclick="ChangePageByLinkNumber_First" runat="server"><%= ResHelper.LocalizeString("{$=Πρωτη|en-us=First$}")%></asp:linkbutton><font color="#adadad">&nbsp;|&nbsp;</font></div>
            <div class="GridPagerNoLinkDivCenterSrc" id="lblPageIndex" runat="server">&nbsp;</div>
            <div class="GridPagerNoLinkDivRightLast"><font color="#adadad">&nbsp;|&nbsp;</font><asp:linkbutton ID="lnkLastPage" CssClass="PagerText" onclick="ChangePageByLinkNumber_Last" runat="server"><%= ResHelper.LocalizeString("{$=Τελευταία|en-us=Last$}")%></asp:linkbutton>&nbsp;</div>
            <div class="GridPagerNoLinkDivRightNext">&nbsp;<asp:linkbutton ID="lnkNext" CssClass="PagerText"  onclick="ChangePageByLinkNumber_Next" runat="server"><%=ResHelper.LocalizeString("{$=Επόμενη &raquo;|en-us=Next &raquo;$}") %></asp:linkbutton></div>
       </div>
      </div>
    </PagerTemplate>
  </asp:GridView> 
   <asp:LinkButton ID="HiddenPageBtn" style="display:none" runat="server" Text="0"></asp:LinkButton>
    <asp:TextBox ID="HiddenPage"  style="display:none" runat="server" Text="1"></asp:TextBox>
    <asp:TextBox ID="HiddenSearchType"  style="display:none" runat="server" Text="0"></asp:TextBox>
    <div id="BottomDiv" style="display:none" runat="server" class="SrcHisPayGridBGBottom"></div> 
</ContentTemplate>
</asp:UpdatePanel>

    <script language="javascript" type="text/javascript" >
     function GoToPage(no) {
         document.getElementById('<%=HiddenPage.ClientID %>').value = no;
         document.getElementById('<%=HiddenPageBtn.ClientID %>').innerHTML = no;
         var GetID = '<%=HiddenPageBtn.ClientID %>'
         GetID = GetID.replace(/[_]/gi, "$")
         __doPostBack(GetID, '')
     }
 </script>