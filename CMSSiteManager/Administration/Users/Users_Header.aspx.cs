using System;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.LicenseProvider;

public partial class CMSSiteManager_Administration_Users_Users_Header : CMSUsersPage
{
    #region "Private fields"

    private string mSiteId = null;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "Users administration";

        mSiteId = QueryHelper.GetString("siteid", null);

        // Initialize the master page elements
        InitializeMasterPage();
    }


    /// <summary>
    /// Initializes the master page elements
    /// </summary>
    private void InitializeMasterPage()
    {
        // Set the master page title
        CurrentMaster.Title.TitleText = ResHelper.GetString("general.users");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_User/object.png");

        string query = GetQueryParameters();

        // Set the tabs
        string[,] tabs = new string[4, 8];
        
        tabs[0, 0] = ResHelper.GetString("general.users");
        tabs[0, 2] = "User_List.aspx" + query;

        if (SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSRegistrationAdministratorApproval"))
        {
            tabs[1, 0] = ResHelper.GetString("administration.users_header.myapproval");
            tabs[1, 2] = "General/User_WaitingForApproval.aspx" + query;
        }

        tabs[2, 0] = ResHelper.GetString("administration.users_header.massemails");
        tabs[2, 2] = "General/User_MassEmail.aspx" + query;

        if (LicenseHelper.IsFeautureAvailableInUI(FeatureEnum.OnlineUsers))
        {
            tabs[3, 0] = ResHelper.GetString("administration.users_header.onlineusers");
            tabs[3, 2] = "General/User_Online.aspx" + query;
        }

        CurrentMaster.Tabs.UrlTarget = "usersContent";
        CurrentMaster.Tabs.Tabs = tabs;
    }


    private string GetQueryParameters()
    {
        if (!string.IsNullOrEmpty(mSiteId))
        {
            return "?siteid=" + mSiteId;
        }

        return "";
    }
}
