-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_CMS_WebFarmTask_RemoveDependences]
	@Id int
AS
BEGIN
	
	-- CMS_WebFarmServerTask
    DELETE FROM CMS_WebFarmServerTask WHERE TaskID = @Id;
	
	DELETE FROM CMS_WebFarmTask WHERE TaskEnabled = 1 AND TaskID NOT IN (SELECT TaskID FROM CMS_WebFarmServerTask);
END
