<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Subscriber_Export.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_ImportExportSubscribers_Subscriber_Export" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Tools - Newsletter subscribers" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>


<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label ID="lblSelectSub" runat="server" CssClass="ContentLabel" EnableViewState="false" />
    <br />
    <br />
    <cms:UniSelector ID="usNewsletters" runat="server" IsLiveSite="false" ObjectType="Newsletter.Newsletter"
        SelectionMode="Multiple" ResourcePrefix="newsletterselect" />
    <br />
    <br />
    <asp:Label ID="lblExportedSub" runat="server" CssClass="ContentLabel" EnableViewState="false" />
    <br />
    <br />
    <asp:TextBox ID="txtExportSub" runat="server" TextMode="MultiLine" CssClass="TextAreaLarge"
        Enabled="false" EnableViewState="false" />
    <br />
    <br />
    <cms:CMSButton ID="btnExport" runat="server" CssClass="LongSubmitButton" EnableViewState="false" />
</asp:Content>
