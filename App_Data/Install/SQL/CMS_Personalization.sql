CREATE TABLE [CMS_Personalization] (
		[PersonalizationID]               int NOT NULL IDENTITY(1, 1),
		[PersonalizationGUID]             uniqueidentifier NOT NULL,
		[PersonalizationLastModified]     datetime NOT NULL,
		[PersonalizationUserID]           int NULL,
		[PersonalizationDocumentID]       int NOT NULL,
		[PersonalizationWebParts]         nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Personalization]
	ADD
	CONSTRAINT [PK_CMS_Personalization]
	PRIMARY KEY
	NONCLUSTERED
	([PersonalizationID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_Personalization]
	ADD
	CONSTRAINT [DEFAULT_CMS_Personalization_PersonalizationDocumentID]
	DEFAULT ((0)) FOR [PersonalizationDocumentID]
ALTER TABLE [CMS_Personalization]
	ADD
	CONSTRAINT [DEFAULT_CMS_Personalization_PersonalizationGUID]
	DEFAULT ('00000000-0000-0000-0000-000000000000') FOR [PersonalizationGUID]
ALTER TABLE [CMS_Personalization]
	ADD
	CONSTRAINT [DEFAULT_CMS_Personalization_PersonalizationLastModified]
	DEFAULT ('9/2/2008 5:36:59 PM') FOR [PersonalizationLastModified]
IF OBJECT_ID(N'[CMS_Personalization]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Document]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Personalization_PersonalizationDocumentID_CMS_Document]') IS NULL
BEGIN
		ALTER TABLE [CMS_Personalization]
			ADD CONSTRAINT [FK_CMS_Personalization_PersonalizationDocumentID_CMS_Document]
			FOREIGN KEY ([PersonalizationDocumentID]) REFERENCES [CMS_Document] ([DocumentID])
END
IF OBJECT_ID(N'[CMS_Personalization]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Personalization_PersonalizationUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_Personalization]
			ADD CONSTRAINT [FK_CMS_Personalization_PersonalizationUserID_CMS_User]
			FOREIGN KEY ([PersonalizationUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE CLUSTERED INDEX [IX_CMS_Personalization_PersonalizationUserID_PersonalizationDocumentID]
	ON [CMS_Personalization] ([PersonalizationUserID], [PersonalizationDocumentID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
