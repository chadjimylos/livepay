<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UserNotifications.ascx.cs"
    Inherits="CMSModules_Notifications_Controls_UserNotifications" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>
<asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
    Visible="false" />
<div class="UserNotifications">
    <cms:UniGrid ID="gridElem" runat="server" GridName="~/CMSModules/Notifications/Controls/UserNotifications.xml"
        OrderBy="SubscriptionTime" IsLiveSite="false" Columns="SubscriptionID, SubscriptionTarget, SubscriptionTime, SubscriptionEventDisplayName" />
</div>
