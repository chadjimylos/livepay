<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Country_New.aspx.cs" Inherits="CMSSiteManager_Development_Countries_Country_New"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="New country" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCountryDisplayName" EnableViewState="false" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtCountryDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvCountryDisplayName" runat="server" ControlToValidate="txtCountryDisplayName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblCountryCodeName" EnableViewState="false" runat="server" />
            </td>
            <td>
                <asp:TextBox ID="txtCountryCodeName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="rfvCountryCodeName" runat="server" ControlToValidate="txtCountryCodeName"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
