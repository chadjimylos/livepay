using System;
using System.Web;
using System.Web.UI;
using System.IO;

using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.MediaLibrary;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_MediaLibrary_Controls_Dialogs_DirectFileUploader_FileUpload : FileUpload
{
    #region "Button handling"

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        lblError.Visible = (lblError.Text != "");
    }

    protected void btnOK_Click(object senderObject, EventArgs e)
    {
        if (!ucFileUpload.HasFile)
        {
            lblError.Text = ResHelper.GetString("attach.selectfile");
        }
        else
        {
            switch (this.SourceType)
            {
                case MediaSourceEnum.MediaLibraries:
                    HandleLibrariesUpload();
                    break;

                default:
                    break;
            }
        }
    }


    /// <summary>
    /// Provides operations necessary to create and store new library file.
    /// </summary>
    private void HandleLibrariesUpload()
    {
        MediaFileInfo mediaFile = null;

        string message = string.Empty;

        // Get related library info
        MediaLibraryInfo mli = MediaLibraryInfoProvider.GetMediaLibraryInfo(this.LibraryID);
        if (mli != null)
        {
            try
            {
                #region "Check permissions"

                CurrentUserInfo currUser = CMSContext.CurrentUser;

                // Not a global admin
                if (!currUser.IsGlobalAdministrator)
                {
                    // Group library
                    bool isGroupLibrary = (mli.LibraryGroupID > 0);
                    if (!(isGroupLibrary && currUser.IsGroupAdministrator(mli.LibraryGroupID)))
                    {
                        // Checked resource name
                        string resource = (isGroupLibrary) ? "CMS.Groups" : "CMS.MediaLibrary";

                        // Check 'CREATE' & 'MANAGE' permissions
                        if (!(currUser.IsAuthorizedPerResource(resource, CMSAdminControl.PERMISSION_MANAGE) || MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(mli, "FileCreate")))
                        {
                            throw new Exception(ResHelper.GetString("media.security.nofilecreate"));
                        }
                    }
                }

                #endregion

                // No file for upload specified
                if (!ucFileUpload.HasFile)
                {
                    throw new Exception(ResHelper.GetString("media.newfile.errorempty"));
                }

                // Check the allowed extensions
                CheckAllowedExtensions();

                // Create new media file record
                mediaFile = new MediaFileInfo(ucFileUpload.PostedFile, this.LibraryID, this.LibraryFolderPath, this.ResizeToWidth, this.ResizeToHeight, this.ResizeToMaxSideSize);

                mediaFile.FileDescription = "";

                // Save the new file info
                MediaFileInfoProvider.SetMediaFileInfo(mediaFile);
            }
            catch (Exception ex)
            {
                // Creation of new media file failed
                message = ex.Message;
                lblError.Text = message;
            }
            finally
            {
                if (string.IsNullOrEmpty(message))
                {
                    // Create media file info string
                    string mediaInfo = "";
                    if ((mediaFile != null) && (mediaFile.FileID > 0) && (this.IncludeNewItemInfo))
                    {
                        mediaInfo = mediaFile.FileID.ToString() + "|" + this.LibraryFolderPath.Replace('\\', '>');
                    }

                    // Ensure message text
                    message = HTMLHelper.EnsureLineEnding(message, " ");

                    // Call function to refresh parent window                                                     
                    ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "refresh", ScriptHelper.GetScript("if ((wopener.parent != null) && (wopener.parent.InitRefresh_" + ParentElemID + " != null)){wopener.parent.InitRefresh_" + ParentElemID + "(" + ScriptHelper.GetString(message.Trim()) + ", false" + ((mediaInfo != "") ? ", '" + mediaInfo + "'" : "") + (InsertMode ? ", 'insert'" : ", 'update'") + ");}window.close();"));
                }
            }
        }
    }


    /// <summary>
    /// Checks if file is allowed according current settings.
    /// </summary>
    protected void CheckAllowedExtensions()
    {
        if ((AllowedExtensions != null) && (ucFileUpload.PostedFile != null))
        {
            string fileName = ucFileUpload.PostedFile.FileName.ToLower();

            string extension = Path.GetExtension(fileName).TrimStart('.');
            if ((AllowedExtensions != "") || OnlyImages)
            {
                if (OnlyImages && !ImageHelper.IsImage(extension))
                {
                    throw new Exception(string.Format(ResHelper.GetString("attach.quickinsert.onlyimages")));
                }

                if ((AllowedExtensions != "") && !AllowedExtensions.ToLower().Contains(extension))
                {
                    throw new Exception(string.Format(ResHelper.GetString("attach.notallowedextension"), extension, AllowedExtensions.TrimEnd(';').Replace(";", ", ")));
                }
            }
        }
    }

    #endregion
}
