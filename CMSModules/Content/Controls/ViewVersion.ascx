﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewVersion.ascx.cs" Inherits="CMSModules_Content_Controls_ViewVersion" %>

<script type="text/javascript">
    //<![CDATA[
    function TipImage(imageWidth, url, attachmentName) {
        Tip('<div style=\'width:' + imageWidth + 'px;text-align:center;\'><img src=\'' + url + '\' alt=\'' + attachmentName + '\' /></div>');
    }
    //]]>
</script>

<asp:Panel ID="pnlControl" runat="server">
    <style type="text/css">
        .UnsortedLeft
        {
            width: 50%;
            float: left;
        }
        .UnsortedRight
        {
            width: 50%;
            float: right;
        }
        .PageContent .NotIncluded, .PageContent .HTMLNotIncluded
        {
            display: inline;
            visibility: hidden;
        }
        td
        {
            vertical-align: top;
        }
    </style>
    <asp:ScriptManager ID="ucScriptManager" runat="server" />
    <asp:Panel ID="pnlAdditionalControls" runat="server" CssClass="PageHeaderLine">
        <cms:LocalizedLabel ID="lblCompareTo" runat="server" ResourceString="content.compareto"
            DisplayColon="true" EnableViewState="false" />
        <cms:LocalizedDropDownList ID="drpCompareTo" runat="server" CssClass="DropDownField"
            AutoPostBack="true" />
    </asp:Panel>
</asp:Panel>
<asp:Panel ID="pnlBody" runat="server">
    <div class="PageContent">
        <asp:Table ID="tblDocument" runat="server" GridLines="Horizontal" CellPadding="3"
            CellSpacing="0" CssClass="UniGridGrid" />
        <br />
        <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" />
    </div>
</asp:Panel>
