<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentType_Edit.aspx.cs" Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_Edit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Document Types - Edit</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height:100%; 
        }
    </style>
</head>
    <frameset border="0" rows="102, *" id="rowsFrameset">
		<frame name="menu" src="DocumentType_Edit_Header.aspx?documenttypeid=<% Response.Write(Request.QueryString["documenttypeid"]); %> " scrolling="no" frameborder="0" noresize/>		
	    <frame name="content" src="DocumentType_Edit_General.aspx?documenttypeid=<% Response.Write(Request.QueryString["documenttypeid"]); %> " frameborder="0" noresize/>
	<noframes>
		<body>
		 <p id="p1">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		 </p>
		</body>
	</noframes>
</frameset>
</html>
