CREATE PROCEDURE [Proc_CMS_SettingsKey_SelectGeneration]
	@KeyLoadGeneration int
AS
SELECT SiteName, KeyName, KeyValue FROM CMS_SettingsKey LEFT OUTER JOIN CMS_Site ON
	CMS_SettingsKey.SiteID = CMS_Site.SiteID WHERE KeyLoadGeneration = @KeyLoadGeneration
