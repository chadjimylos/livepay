<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Friends_Approve.aspx.cs"
    Title="Approve friendship" Inherits="CMSModules_Friends_CMSPages_Friends_Approve"
    Theme="default" MasterPageFile="~/CMSMasterPages/LiveSite/Dialogs/ModalSimplePage.master" %>

<%@ Register Src="~/CMSModules/Friends/Controls/Friends_Approve.ascx" TagName="FriendsApprove"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <div class="PageContent">
        <cms:FriendsApprove ID="FriendsApprove" runat="server" Visible="true" />
    </div>
</asp:Content>
