using System.IO;
using System;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Security.Principal;
using System.Data;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.LicenseProvider;
using CMS.CMSImportExport;
using CMS.TreeEngine;
using CMS.SettingsProvider;
using CMS.DirectoryUtilities;
using CMS.IDataConnectionLibrary;
using CMS.ExtendedControls;
using CMS.DataEngine;
using CMS.UIControls;
using CMS.VirtualPathHelper;

#region "InstallInfo"

/// <summary>
/// Installation info
/// </summary>
[Serializable]
public class InstallInfo
{
    #region "Variables"

    public const string SEPARATOR = "<#>";

    private const string LOG = "I" + SEPARATOR + SEPARATOR + SEPARATOR;

    // Deletion log
    private string mInstallLog = LOG;
    private string mScriptsFullPath = null;
    private string mConnectionString = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Keep information about installation progress
    /// </summary>
    public string InstallLog
    {
        get
        {
            return mInstallLog;
        }
        set
        {
            mInstallLog = value;
        }
    }


    /// <summary>
    /// Connection string
    /// </summary>
    public string ConnectionString
    {
        get
        {
            return mConnectionString;
        }

        set
        {
            mConnectionString = value;
        }
    }


    /// <summary>
    /// Scripts full path
    /// </summary>
    public string ScriptsFullPath
    {
        get
        {
            return mScriptsFullPath;
        }

        set
        {
            mScriptsFullPath = value;
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Constructor
    /// </summary>
    public InstallInfo()
    {
    }


    /// <summary>
    /// Clear log
    /// </summary>
    public void ClearLog()
    {
        InstallLog = LOG;
    }


    /// <summary>
    /// Gets limited progress log for callback
    /// </summary>
    /// <param name="reqMessageLength">Requested message part length</param>
    /// <param name="reqErrorLength">Requested error part length</param>
    /// <param name="reqWarningLength">Requested warning part length</param>
    public string GetLimitedProgressLog(int reqMessageLength, int reqErrorLength, int reqWarningLength)
    {
        if (mInstallLog != null)
        {
            string[] parts = mInstallLog.Split(new string[] { SEPARATOR }, StringSplitOptions.None);

            if (parts.Length != 4)
            {
                return "F" + SEPARATOR + "Wrong internal log." + SEPARATOR + SEPARATOR;
            }

            string message = parts[1];
            string error = parts[2];
            string warning = parts[3];

            int messageLength = message.Length;
            if (reqMessageLength > messageLength)
            {
                reqMessageLength = messageLength;
            }

            int errorLength = error.Length;
            if (reqErrorLength > errorLength)
            {
                reqErrorLength = errorLength;
            }

            int warningLength = warning.Length;
            if (reqWarningLength > warningLength)
            {
                reqWarningLength = warningLength;
            }

            return parts[0] + SEPARATOR + message.Substring(0, messageLength - reqMessageLength) + SEPARATOR + parts[2].Substring(0, errorLength - reqErrorLength) + SEPARATOR + parts[3].Substring(0, warningLength - reqWarningLength);
        }
        return "F" + SEPARATOR + "Internal error." + SEPARATOR + SEPARATOR;
    }

    #endregion
}

#endregion


public partial class CMSInstall_install : CMSPage, ICallbackEventHandler
{
    #region "Consts"

    private const string WWAG_KEY = "CMSWWAGInstallation";

    #endregion


    #region "Variables"

    private static Hashtable mInstallInfos = new Hashtable();

    private static Hashtable mManagers = new Hashtable();

    private string hostName = HttpContext.Current.Request.Url.Host.ToLower();

    #endregion


    #region "Properties"

    /// <summary>
    /// Install info
    /// </summary>
    public InstallInfo InstallInfo
    {
        get
        {
            string key = "instInfos_" + Session.SessionID;
            if (mInstallInfos[key] == null)
            {
                InstallInfo instInfo = new InstallInfo();
                mInstallInfos[key] = instInfo;
            }
            return (InstallInfo)mInstallInfos[key];
        }
        set
        {
            string key = "instInfos_" + Session.SessionID;
            mInstallInfos[key] = value;
        }
    }


    /// <summary>
    /// Authentication type
    /// </summary>
    public SQLServerAuthenticationModeEnum authenticationType
    {
        get
        {
            if (ViewState["authentication"] == null)
            {
                if (RequestHelper.IsPostBack())
                {
                    throw new Exception("Connection information was lost!");
                }
            }
            return (SQLServerAuthenticationModeEnum)ViewState["authentication"];
        }
        set
        {
            ViewState["authentication"] = value;
        }
    }


    /// <summary>
    /// Database name
    /// </summary>
    public string Database
    {
        get
        {
            return ValidationHelper.GetString(ViewState["Database"], "");
        }
        set
        {
            ViewState["Database"] = value;
        }
    }


    /// <summary>
    /// Import manager
    /// </summary>
    public ImportManager ImportManager
    {
        get
        {
            string key = "imManagers_" + Session.SessionID;
            if (mManagers[key] == null)
            {
                SiteImportSettings imSettings = new SiteImportSettings(CMSContext.CurrentUser);
                imSettings.ImportType = ImportTypeEnum.All;
                imSettings.CopyFiles = false;
                imSettings.EnableSearchTasks = false;
                ImportManager im = new ImportManager(imSettings);
                mManagers[key] = im;
            }
            return (ImportManager)mManagers[key];
        }
        set
        {
            string key = "imManagers_" + Session.SessionID;
            mManagers[key] = value;
        }
    }


    /// <summary>
    /// New site domain
    /// </summary>
    public string Domain
    {
        get
        {
            return ValidationHelper.GetString(ViewState["Domain"], "");
        }

        set
        {
            ViewState["Domain"] = value;
        }
    }


    /// <summary>
    /// New site site name
    /// </summary>
    public string SiteName
    {
        get
        {
            return ValidationHelper.GetString(ViewState["SiteName"], "");
        }

        set
        {
            ViewState["SiteName"] = value;
        }
    }


    /// <summary>
    /// Connection string
    /// </summary>
    public string ConnectionString
    {
        get
        {
            if (ViewState["connString"] == null)
            {
                ViewState["connString"] = "";
            }
            return (string)ViewState["connString"];
        }

        set
        {
            ViewState["connString"] = value;
        }
    }


    /// <summary>
    /// Step index
    /// </summary>
    public int StepIndex
    {
        get
        {
            if (ViewState["stepIndex"] == null)
            {
                ViewState["stepIndex"] = 1;
            }
            return (int)ViewState["stepIndex"];
        }

        set
        {
            ViewState["stepIndex"] = value;
        }
    }


    private string mResult
    {
        get
        {
            if (ViewState["result"] == null)
            {
                if (RequestHelper.IsPostBack())
                {
                    throw new Exception("Information was lost!");
                }
            }
            return (string)ViewState["result"];
        }
        set
        {
            ViewState["result"] = value;
        }
    }


    private bool mDisplayLog
    {
        get
        {
            if (ViewState["displLog"] == null)
            {
                if (RequestHelper.IsPostBack())
                {
                    throw new Exception("Information was lost!");
                }
                return false;
            }
            return (bool)ViewState["displLog"];
        }
        set
        {
            ViewState["displLog"] = value;
        }
    }


    private bool CreateDBObjects
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["CreateDBObjects"], true);
        }
        set
        {
            ViewState["CreateDBObjects"] = value;
        }
    }


    private int PreviousStep
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["PreviousStep"], 0);
        }
        set
        {
            ViewState["PreviousStep"] = value;
        }
    }


    private int ActualStep
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["ActualStep"], 0);
        }
        set
        {
            ViewState["ActualStep"] = value;
        }
    }


    private int StepOperation
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["StepOperation"], 0);
        }
        set
        {
            ViewState["StepOperation"] = value;
        }
    }

    #endregion


    #region "Step wizard buttons"

    /// <summary>
    /// Previous button
    /// </summary>
    public LocalizedButton PreviousButton
    {
        get
        {
            return wzdInstaller.FindControl("StepNavigationTemplateContainerID").FindControl("StepPrevButton") as LocalizedButton;
        }
    }


    /// <summary>
    /// Next button
    /// </summary>
    public LocalizedButton NextButton
    {
        get
        {
            return wzdInstaller.FindControl("StepNavigationTemplateContainerID").FindControl("StepNextButton") as LocalizedButton;
        }
    }

    #endregion


    protected void Page_Load(Object sender, EventArgs e)
    {
        // Register script for pendingCallbacks repair
        ScriptHelper.FixPendingCallbacks(this.Page);

        SetBrowserClass(false);

        if (!IsCallback)
        {
            ucAsyncControl.OnFinished += worker_OnFinished;
            ucDBAsyncControl.OnFinished += workerDB_OnFinished;

            // Javascript functions
            string jsFunctions =
                "var iMessageText = '';\n" +
                "var iErrorText = '';\n" +
                "var iWarningText = '';\n" +
                "var getBusy = false; \n" +
                "function GetInstallState(argument)\n" +
                "{ if (getBusy) return; getBusy = true; setTimeout(\"getBusy = false;\", 2000); if(window.Activity){window.Activity();} var arg = argument + ';' + iMessageText.length + ';' + iErrorText.length + ';' + iWarningText.length; return " + Page.ClientScript.GetCallbackEventReference(this, "arg", "SetInstallStateMssg", "arg", true) + " } \n";

            jsFunctions +=
                "function SetInstallStateMssg(rValue, context)\n" +
                "{\n" +
                "   getBusy = false; \n" +
                "   if (rValue != '')\n" +
                "   {\n" +
                "       var args = context.split(';');\n" +
                "       var values = rValue.split('" + AbstractImportExportSettings.SEPARATOR + "');\n" +
                "       var messageElement = document.getElementById('lblProgress');\n" +
                "       var errorElement = document.getElementById('" + lblError.ClientID + "');\n" +
                "       var warningElement = document.getElementById('" + lblWarning.ClientID + "');\n" +
                "       var messageText = iMessageText;\n" +
                "       messageText = values[1] + messageText.substring(messageText.length - args[2]);\n" +
                "       if(messageText.length > iMessageText.length){ iMessageText = messageElement.innerHTML = messageText; }\n" +
                "       var errorText = iErrorText;\n" +
                "       errorText = values[2] + errorText.substring(errorText.length - args[3]);\n" +
                "       if(errorText.length > iErrorText.length){ iErrorText = errorElement.innerHTML = errorText; }\n" +
                "       var warningText = iWarningText;\n" +
                "       warningText = values[3] + warningText.substring(warningText.length - args[4]);\n" +
                "       if(warningText.length > iWarningText.length){ iWarningText = warningElement.innerHTML = warningText; }\n" +
                "       if((values == '') || (values[0] == 'F'))\n" +
                "       {\n" +
                "           StopInstallStateTimer();\n" +
                "           if(values[2] != '')\n" +
                "           {\n" +
                "               BTN_Disable('" + NextButton.ClientID + "');\n" +
                "               BTN_Enable('" + PreviousButton.ClientID + "');\n" +
                "           }\n" +
                "           else\n" +
                "           {\n" +
                "               BTN_Disable('" + NextButton.ClientID + "');\n" +
                "               BTN_Disable('" + PreviousButton.ClientID + "');\n" +
                "           }\n" +
                "       }\n" +
                "   }\n" +
                "}\n";

            // Register the script to perform get flags for showing buttons retrieval callback
            ScriptHelper.RegisterClientScriptBlock(this, GetType(), "InstallFunctions", ScriptHelper.GetScript(jsFunctions));

            lblVersion.Text = ResHelper.GetFileString("install.Version") + "&nbsp;" + CMSContext.SYSTEM_VERSION + "&nbsp;" + ResHelper.GetString("install.Build") + "&nbsp;" + typeof(TreeProvider).Assembly.GetName().Version.Major + "." + typeof(TreeProvider).Assembly.GetName().Version.Minor + "." + typeof(TreeProvider).Assembly.GetName().Version.Build;

            //StartHelp.Tooltip = ResHelper.GetFileString("install.tooltip");
            //StartHelp.TopicName = "DBInstall_Step1";
            //Help.Tooltip = ResHelper.GetFileString("install.tooltip");
            //Help.IconUrl = GetImageUrl("Others/LogonForm/HelpButton.png");
            //StartHelp.IconUrl = GetImageUrl("Others/LogonForm/HelpButton.png");
            Response.Cache.SetNoStore();

            btnPermissionTest.Click += btnPermissionTest_Click;
            btnPermissionSkip.Click += btnPermissionSkip_Click;
            btnPermissionContinue.Click += btnPermissionContinue_Click;

            // If the connection string is set, redirect
            if (!RequestHelper.IsPostBack())
            {
                if ((SettingsHelper.ConnectionStrings["CMSConnectionString"] != null) && (SettingsHelper.ConnectionStrings["CMSConnectionString"].ConnectionString != ""))
                {
                    UrlHelper.Redirect("~/default.aspx");
                }

                // Initialize progress bars
                InitProgressBars();

                bool checkPermission = QueryHelper.GetBoolean("checkpermission", true);
                bool testAgain = QueryHelper.GetBoolean("testagain", false);

                string dir = HttpContext.Current.Server.MapPath("~/");

                // Do not test write permissions in WWAG mode
                if (!ValidationHelper.GetBoolean(SettingsHelper.AppSettings[WWAG_KEY], false))
                {
                    if (!DirectoryHelper.CheckPermissions(dir) && checkPermission)
                    {
                        pnlWizard.Visible = false;
                        imgHeader.Visible = false;
                        pnlPermission.Visible = true;
                        pnlButtons.Visible = true;

                        lblPermission.Text = string.Format(ResHelper.GetFileString("Install.lblPermission"), WindowsIdentity.GetCurrent().Name, dir);
                        btnPermissionSkip.Text = ResHelper.GetFileString("Install.btnPermissionSkip");
                        btnPermissionTest.Text = ResHelper.GetFileString("Install.btnPermissionTest");

                        // Show troubleshoot link
                        //hlpTroubleshoot.Visible = true;
                        //hlpTroubleshoot.Text = ResHelper.GetFileString("Install.ErrorPermissions");
                        //hlpTroubleshoot.TopicName = "DiskPermissions";
                        return;
                    }

                    if (testAgain)
                    {
                        pnlWizard.Visible = false;
                        pnlPermission.Visible = false;
                        pnlButtons.Visible = false;
                        pnlPermissionSuccess.Visible = true;
                        lblPermissionSuccess.Text = ResHelper.GetFileString("Install.lblPermissionSuccess");
                        btnPermissionContinue.Text = ResHelper.GetFileString("Install.btnPermissionContinue");
                        return;
                    }
                }
            }

            pnlWizard.Visible = true;
            pnlPermission.Visible = false;
            pnlButtons.Visible = false;

            if (!RequestHelper.IsPostBack())
            {
                if (HttpContext.Current != null)
                {
                    txtServerName.Text = HTTPHelper.MachineName;
                }
                authenticationType = SQLServerAuthenticationModeEnum.SQLServerAuthentication;
                string webDirectory = UrlHelper.WebApplicationVirtualPath.Replace("/", "");
                if ((webDirectory == "") || (webDirectory == "~"))
                {
                    webDirectory = "VivaCMS";
                }
                txtNewDatabaseName.Text = webDirectory;

                wzdInstaller.ActiveStepIndex = 0;
            }
            else
            {
                if (ViewState["install.password"] == null)
                    ViewState["install.password"] = txtDBPassword.Text.Trim();
            }

            // Load the strings
            mDisplayLog = false;
            lblError.Text = "";
            //hlpTroubleshoot.Visible = false;

            lblExistingDatabaseName.Text = ResHelper.GetFileString("Install.lblExistingDatabaseName");
            chkCreateDatabaseObjects.Text = ResHelper.GetFileString("Install.chkCreateDatabaseObjects");
            lblDBPassword.Text = ResHelper.GetFileString("Install.lblPassword");
            lblDBUsername.Text = ResHelper.GetFileString("Install.lblUsername");
            lblServerName.Text = ResHelper.GetFileString("Install.lblServername");
            lblCompleted.Text = ResHelper.GetFileString("Install.DBSetupOK");
            lblLog.Text = ResHelper.GetFileString("Install.lblLog");
            radCreateNew.Text = ResHelper.GetFileString("Install.radCreateNew");
            radUseExisting.Text = ResHelper.GetFileString("Install.radUseExisting");
            lblNewDatabaseName.Text = ResHelper.GetFileString("Install.lblNewDatabaseName");
            lblMediumTrustInfo.Text = ResHelper.GetFileString("Install.MediumTrustInfo");

            radSQLAuthentication.Text = ResHelper.GetFileString("Install.radSQlAuthentication");
            radWindowsAuthentication.Text = ResHelper.GetFileString("Install.radWindowsAuthentication") + "<br /><span class=\"InstallAccountName\">" + String.Format(ResHelper.GetFileString("Install.Account"), WindowsIdentity.GetCurrent().Name) + "</span>";

            lblSQLServer.Text = ResHelper.GetFileString("Install.lblSQLServer");
            lblDatabase.Text = ResHelper.GetFileString("Install.lblDatabase");
            ltlScript.Text = ScriptHelper.GetScript(
                "function NextStep(btnNext,elementDiv)\n" +
                "{\n" +
                "   btnNext.disabled=true;\n" +
                "   try{BTN_Disable('" + PreviousButton.ClientID + "');}catch(err){}\n" +
                    ClientScript.GetPostBackEventReference(btnHiddenNext, null) +
                "}\n" +
                "function PrevStep(btnPrev,elementDiv)\n" +
                "{" +
                "   btnPrev.disabled=true;\n" +
                "   try{BTN_Disable('" + NextButton.ClientID + "');}catch(err){}\n" +
                    ClientScript.GetPostBackEventReference(btnHiddenBack, null) +
                "}\n"
                );
            mResult = "";

            // Sets connection string panel
            lblConnectionString.Text = ResHelper.GetFileString("Install.lblConnectionString");

            wzdInstaller.StartNextButtonText = ResHelper.GetFileString("general.next") + " >";
            wzdInstaller.FinishCompleteButtonText = ResHelper.GetFileString("Install.Finish");
            wzdInstaller.FinishPreviousButtonText = ResHelper.GetFileString("Install.BackStep");
            wzdInstaller.StepNextButtonText = ResHelper.GetFileString("general.next") + " >";
            wzdInstaller.StepPreviousButtonText = ResHelper.GetFileString("Install.BackStep");

            // Show WWAG dialog instead of license dialog (if running in WWAG mode)
            if (ValidationHelper.GetBoolean(SettingsHelper.AppSettings[WWAG_KEY], false))
            {
                ucLicenseDialog.Visible = false;
                ucWagDialog.Visible = true;
            }
        }

        wzdInstaller.ActiveStepChanged += wzdInstaller_ActiveStepChanged;

        NextButton.Attributes.Remove("disabled");
        PreviousButton.Attributes.Remove("disabled");
    }


    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (SqlHelperClass.IsConnectionStringInitialized)
        {
            ucSiteCreationDialog.StopProcessing = false;
            ucSiteCreationDialog.ReloadData();
        }

        SetFields();
        // Display the log if result filled
        if (mDisplayLog)
        {
            txtLog.Text = mResult;
            pnlLog.Visible = true;
        }
        else
        {
            pnlLog.Visible = false;
        }

        InitializeHeader(wzdInstaller.ActiveStepIndex);
        EnsureDefaultButton();

        if (!IsPostBack)
        {
            ltlInstallScript.Text += ScriptHelper.GetScript("var imgSelected = new Image(); imgSelected.src = '" + GetImageUrl("/Others/Install/SiteSelector.png") + "';");
        }
    }


    void wzdInstaller_ActiveStepChanged(object sender, EventArgs e)
    {
        switch (wzdInstaller.ActiveStepIndex)
        {
            // Finish step
            case 7:
                // Set current user default culture of the site
                CMSContext.PreferredCultureCode = SettingsKeyProvider.GetStringValue(SiteName + ".CMSDefaultCultureCode");

                // Initialize virtual path provider
                VirtualPathHelper.Init(this);

                // Check medium trust
                if (!SettingsKeyProvider.UsingVirtualPathProvider)
                {
                    btnWebSite.Text = ResHelper.GetFileString("Install.lnkMediumTrust");
                    lblMediumTrustInfo.Visible = true;
                }
                else
                {
                    btnWebSite.Text = ResHelper.GetFileString("Install.lnkWebsite");
                }
                break;
        }
    }


    void btnPermissionContinue_Click(object sender, EventArgs e)
    {
        UrlHelper.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }


    void btnPermissionSkip_Click(object sender, EventArgs e)
    {
        UrlHelper.Redirect(Request.Url.GetLeftPart(UriPartial.Path) + "?checkpermission=0");
    }


    void btnPermissionTest_Click(object sender, EventArgs e)
    {
        UrlHelper.Redirect(Request.Url.GetLeftPart(UriPartial.Path) + "?testagain=1");
    }


    private void SetFields()
    {
        txtDBPassword.Enabled = radSQLAuthentication.Checked;
        txtDBUsername.Enabled = radSQLAuthentication.Checked;
        txtExistingDatabaseName.Enabled = radUseExisting.Checked;
        if (radCreateNew.Checked)
        {
            chkCreateDatabaseObjects.Checked = true;
            chkCreateDatabaseObjects.Enabled = false;
        }
        else
        {
            chkCreateDatabaseObjects.Enabled = true;
        }
        txtNewDatabaseName.Enabled = radCreateNew.Checked;
    }


    protected void btnWebSite_onClick(object sender, EventArgs e)
    {
        if (!SettingsKeyProvider.UsingVirtualPathProvider)
        {
            UserInfo ui = UserInfoProvider.GetUserInfo("administrator");
            if (ui != null)
            {
                FormsAuthentication.SetAuthCookie("administrator", false);
                UserInfoProvider.SetPreferredCultures(ui);
            }
            UrlHelper.Redirect("~/cmssitemanager/default.aspx?section=administration");
        }
        else
        {
            UrlHelper.Redirect(ResolveUrl("~/default.aspx"));
        }
    }


    protected void btnHiddenBack_onClick(object sender, EventArgs e)
    {
        StepOperation = -1;
        if (wzdInstaller.ActiveStepIndex == 8)
        {
            StepIndex = 2;
            wzdInstaller.ActiveStepIndex = 1;
        }
        else
        {
            StepIndex--;
            wzdInstaller.ActiveStepIndex--;
        }
    }


    protected void btnHiddenNext_onClick(object sender, EventArgs e)
    {
        StepOperation = 1;
        StepIndex++;

        switch (wzdInstaller.ActiveStepIndex)
        {
            case 0:
                ViewState["install.password"] = txtDBPassword.Text.Trim();

                // Set the authentication type
                authenticationType = radWindowsAuthentication.Checked ? SQLServerAuthenticationModeEnum.WindowsAuthentication : SQLServerAuthenticationModeEnum.SQLServerAuthentication;

                // Check the server name
                if (txtServerName.Text.Trim() == "")
                {
                    HandleError(ResHelper.GetFileString("Install.ErrorServerEmpty"));
                    return;
                }
                // Check if it is possible to connect to the database
                string res = ConnectionHelper.TestConnection(authenticationType, txtServerName.Text.Trim(), "", txtDBUsername.Text.Trim(), ViewState["install.password"].ToString());
                if (!string.IsNullOrEmpty(res))
                {
                    HandleError(res, "Install.ErrorSqlTroubleshoot", "SQLError");
                    return;
                }
                else
                {
                    wzdInstaller.ActiveStepIndex = 1;
                }
                break;

            case 1:
            case 8:
                // Get database name
                Database = radCreateNew.Checked ? txtNewDatabaseName.Text.Trim() : txtExistingDatabaseName.Text.Trim();

                ConnectionString = ConnectionHelper.GetConnectionString(authenticationType, txtServerName.Text.Trim(), Database, txtDBUsername.Text.Trim(), ViewState["install.password"].ToString(), 240, false);

                if (Database == "")
                {
                    HandleError(ResHelper.GetFileString("Install.ErrorDBNameEmpty"));
                    return;
                }

                if (radUseExisting.Checked)
                {
                    // Use existing database
                    if (!ConnectionHelper.DatabaseExists(ConnectionString))
                    {
                        HandleError(string.Format(ResHelper.GetFileString("Install.ErrorDatabseDoesntExist"), Database));
                        return;
                    }

                    if (wzdInstaller.ActiveStepIndex != 8)
                    {
                        string collation = null;
                        // Check database collation
                        if (!ConnectionHelper.CheckDatabaseCollation(ConnectionString, ref collation))
                        {
                            lblCollation.Text = ResHelper.GetString("install.databasecollation");
                            rbChangeCollation.Text = string.Format(ResHelper.GetString("install.changecollation"), ConnectionHelper.DatabaseCollation);
                            rbLeaveCollation.Text = string.Format(ResHelper.GetString("install.leavecollation"), collation);
                            wzdInstaller.ActiveStepIndex = 8;
                            return;
                        }
                    }
                    else
                    {
                        // Change database collation
                        if (rbChangeCollation.Checked)
                        {
                            ConnectionHelper.ChangeDatabaseCollation(ConnectionString, Database);
                        }
                    }
                }
                else
                {
                    // Create a new database
                    if (!CreateDatabase())
                    {
                        HandleError(string.Format(ResHelper.GetFileString("Install.ErrorCreateDB"), txtNewDatabaseName.Text));
                        return;
                    }
                    else
                    {
                        txtExistingDatabaseName.Text = txtNewDatabaseName.Text;
                        radCreateNew.Checked = false;
                        radUseExisting.Checked = true;
                    }
                }

                // Run SQL script
                if (chkCreateDatabaseObjects.Checked)
                {
                    InstallInfo.ScriptsFullPath = Server.MapPath("~/App_Data/Install/SQL");
                    InstallInfo.ConnectionString = ConnectionString;
                    InstallInfo.ClearLog();

                    ucDBAsyncControl.RunAsync(InstallDatabase, WindowsIdentity.GetCurrent());

                    NextButton.Attributes.Add("disabled", "true");
                    PreviousButton.Attributes.Add("disabled", "true");
                    wzdInstaller.ActiveStepIndex = 2;

                    ltlInstallScript.Text = ScriptHelper.GetScript("StartInstallStateTimer('DB');");
                }
                else
                {
                    CreateDBObjects = chkCreateDatabaseObjects.Checked;

                    // Check the DB connection
                    pnlLog.Visible = false;

                    // Set connection string
                    if (SettingsHelper.SetConnectionString(Server.MapPath("~/web.config"), "CMSConnectionString", ConnectionString))
                    {
                        SqlHelperClass.ConnectionString = ConnectionString;

                        // If this is installation to existing BD and objects are not created
                        // Add license keys
                        bool licensesAdded = true;

                        if (CreateDBObjects && (ucSiteCreationDialog.CreationType != CMSInstall_Controls_SiteCreationDialog.CreationTypeEnum.ExistingSite))
                        {
                            licensesAdded = AddTrialLicenseKeys(ConnectionString);
                        }

                        if (licensesAdded)
                        {
                            if ((hostName != "localhost") && (hostName != "127.0.0.1"))
                            {
                                // Check if license key for current domain is present
                                LicenseKeyInfo lki = LicenseKeyInfoProvider.GetLicenseKeyInfo(hostName);
                                wzdInstaller.ActiveStepIndex = (lki == null) ? 4 : 5;
                            }
                            else
                            {
                                wzdInstaller.ActiveStepIndex = 5;
                            }
                        }
                        else
                        {
                            wzdInstaller.ActiveStepIndex = 4;
                            ucLicenseDialog.SetLicenseExpired();
                        }
                    }
                    else
                    {
                        string connStringDisplay = ConnectionHelper.GetConnectionString(authenticationType, txtServerName.Text.Trim(), Database, txtDBUsername.Text.Trim(), ViewState["install.password"].ToString(), 240, true);
                        wzdInstaller.ActiveStepIndex = 3;
                        string message = ResHelper.GetFileString("Install.ConnectionStringError") + " <br/><br/><strong>&lt;add name=\"CMSConnectionString\" connectionString=\"" + connStringDisplay + "\"/&gt;</strong><br/><br/>";
                        lblErrorConnMessage.Text = message;

                        // Show troubleshoot link
                        //hlpTroubleshoot.Visible = true;
                        //hlpTroubleshoot.TopicName = "DiskPermissions";
                        //hlpTroubleshoot.Text = ResHelper.GetFileString("Install.ErrorPermissions");
                    }
                }
                break;

            // After DB install
            case 2:
                break;

            // After connection string save error
            case 3:
                // Restart application to ensure connection string update
                try
                {
                    // Try to restart applicatin by unload app domain
                    HttpRuntime.UnloadAppDomain();
                }
                catch
                {
                }


                // If connectionstrings don't match
                if ((SettingsHelper.ConnectionStrings["CMSConnectionString"] == null) ||
                   (SettingsHelper.ConnectionStrings["CMSConnectionString"].ConnectionString == null) ||
                   (SettingsHelper.ConnectionStrings["CMSConnectionString"].ConnectionString.Trim() == "") ||
                   (SettingsHelper.ConnectionStrings["CMSConnectionString"].ConnectionString != ConnectionString))
                {
                    HandleError(ResHelper.GetFileString("Install.ErrorAddConnString"));
                    return;
                }
                else
                {
                    // If this is installation to existing DB and objects are not created
                    // Add license keys
                    bool licensesAdded = true;

                    if (CreateDBObjects && (ucSiteCreationDialog.CreationType != CMSInstall_Controls_SiteCreationDialog.CreationTypeEnum.ExistingSite))
                    {
                        licensesAdded = AddTrialLicenseKeys(ConnectionString);
                    }

                    if (licensesAdded)
                    {
                        if ((hostName != "localhost") && (hostName != "127.0.0.1"))
                        {
                            wzdInstaller.ActiveStepIndex = 4;
                        }
                        else
                        {
                            wzdInstaller.ActiveStepIndex = 5;
                        }
                    }
                    else
                    {
                        wzdInstaller.ActiveStepIndex = 4;
                        ucLicenseDialog.SetLicenseExpired();
                    }
                }
                break;

            // After license entering
            case 4:
                try
                {
                    if (ucLicenseDialog.Visible)
                    {
                        ucLicenseDialog.SetLicenseKey();
                        wzdInstaller.ActiveStepIndex = 5;
                    }
                    else
                    {
                        if (ucWagDialog.ProcessRegistration(ConnectionString))
                        {
                            wzdInstaller.ActiveStepIndex = 5;
                        }
                    }
                }
                catch (Exception ex)
                {
                    HandleError(ex.Message);
                    return;
                }
                break;

            // Site creation
            case 5:
                switch (ucSiteCreationDialog.CreationType)
                {
                    case CMSInstall_Controls_SiteCreationDialog.CreationTypeEnum.Template:
                        {
                            if (ucSiteCreationDialog.TemplateName == "")
                            {
                                HandleError(ResHelper.GetFileString("install.notemplate"));
                                return;
                            }

                            // Settings preparation
                            SiteImportSettings settings = new SiteImportSettings(CMSContext.CurrentUser);
                            settings.ImportType = ImportTypeEnum.All;
                            settings.CopyFiles = false;
                            settings.EnableSearchTasks = false;

                            if (HttpContext.Current != null)
                            {
                                const string www = "www.";
                                if (hostName.StartsWith(www))
                                {
                                    hostName = hostName.Remove(0, www.Length);
                                }

                                if (!HttpContext.Current.Request.Url.IsDefaultPort)
                                {
                                    hostName += ":" + HttpContext.Current.Request.Url.Port;
                                }

                                settings.SiteDomain = hostName;
                                Domain = hostName;
                            }

                            // Create site
                            WebTemplateInfo ti = WebTemplateInfoProvider.GetWebTemplateInfoByCodeName(ucSiteCreationDialog.TemplateName);
                            if (ti == null)
                            {
                                HandleError("[Install]: Template not found.");
                                return;
                            }

                            settings.SiteName = ti.WebTemplateName;
                            settings.SiteDisplayName = ti.WebTemplateDisplayName;

                            if (HttpContext.Current != null)
                            {
                                settings.SourceFilePath = HttpContext.Current.Server.MapPath(ti.WebTemplateFileName);
                                settings.WebsitePath = HttpContext.Current.Server.MapPath("~/");
                            }

                            settings.SetSettings(ImportExportHelper.SETTINGS_DELETE_SITE, true);
                            settings.SetSettings(ImportExportHelper.SETTINGS_DELETE_TEMPORARY_FILES, false);

                            SiteName = settings.SiteName;

                            // Init the Mimetype helper (required for the Import)
                            MimeTypeHelper.LoadMimeTypes();

                            // Import the site asynchronously
                            ImportManager.Settings = settings;

                            ucAsyncControl.RunAsync(ImportManager.Import, WindowsIdentity.GetCurrent());

                            NextButton.Attributes.Add("disabled", "true");
                            PreviousButton.Attributes.Add("disabled", "true");
                            wzdInstaller.ActiveStepIndex = 6;

                            ltlInstallScript.Text = ScriptHelper.GetScript("StartInstallStateTimer('IM');");
                        }
                        break;

                    // Else redirect to the site import
                    case CMSInstall_Controls_SiteCreationDialog.CreationTypeEnum.ExistingSite:
                        {
                            UserInfo ui = UserInfoProvider.GetUserInfo("administrator");
                            if (ui != null)
                            {
                                FormsAuthentication.SetAuthCookie("administrator", false);
                                UserInfoProvider.SetPreferredCultures(ui);
                            }
                            UrlHelper.Redirect("~/cmssitemanager/default.aspx?section=sites&action=import");
                        }
                        break;

                    // Else redirect to the new site wizard
                    case CMSInstall_Controls_SiteCreationDialog.CreationTypeEnum.NewSiteWizard:
                        {
                            UserInfo ui = UserInfoProvider.GetUserInfo("administrator");
                            if (ui != null)
                            {
                                FormsAuthentication.SetAuthCookie("administrator", false);
                                UserInfoProvider.SetPreferredCultures(ui);
                            }
                            UrlHelper.Redirect("~/cmssitemanager/default.aspx?section=sites&action=new");
                        }
                        break;
                }
                break;

            default:
                wzdInstaller.ActiveStepIndex++;
                break;
        }
    }


    void worker_OnFinished(object sender, EventArgs e)
    {
        // If the import finished without error
        if ((ImportManager.ImportStatus != CMS.CMSImportExport.ProcessStatus.Error) && (ImportManager.ImportStatus != CMS.CMSImportExport.ProcessStatus.Restarted))
        {

            // Get all indexes and create rebuild tasks
            DataSet ds = SearchIndexInfoProvider.GetSearchIndexes(null, null);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                // Loop thru all indexes and create rebuild tasks
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    SearchTaskInfoProvider.CreateTask(SearchTaskTypeEnum.Rebuild, null, null, Convert.ToString(dr["IndexName"]));
                }
            }

            wzdInstaller.ActiveStepIndex = 7;
        }
        else
        {
            string log = ImportManager.Settings.ProgressLog;
            string[] messages = log.Split(new string[] { InstallInfo.SEPARATOR }, StringSplitOptions.None);
            lblError.Text = messages[2];
            ltlProgress.Text = "<span id=\"lblProgress\" >" + messages[1] + "</span>";
            NextButton.Enabled = false;
        }
    }


    void workerDB_OnFinished(object sender, EventArgs e)
    {
        CreateDBObjects = chkCreateDatabaseObjects.Checked;

        // Check the DB connection
        pnlLog.Visible = false;

        // Set connection string
        if (SettingsHelper.SetConnectionString(Server.MapPath("~/web.config"), "CMSConnectionString", ConnectionString))
        {
            SqlHelperClass.ConnectionString = ConnectionString;

            // If this is installation to existing BD and objects are not created
            // Add license keys
            bool licensesAdded = true;

            if (CreateDBObjects && (ucSiteCreationDialog.CreationType != CMSInstall_Controls_SiteCreationDialog.CreationTypeEnum.ExistingSite))
            {
                licensesAdded = AddTrialLicenseKeys(ConnectionString);
            }

            if (licensesAdded)
            {
                if ((hostName != "localhost") && (hostName != "127.0.0.1"))
                {
                    // Check if license key for current domain is present
                    LicenseKeyInfo lki = LicenseKeyInfoProvider.GetLicenseKeyInfo(hostName);
                    wzdInstaller.ActiveStepIndex = lki == null ? 4 : 5;
                }
                else
                {
                    wzdInstaller.ActiveStepIndex = 5;
                }
            }
            else
            {
                wzdInstaller.ActiveStepIndex = 4;
                ucLicenseDialog.SetLicenseExpired();
            }

            // Request meta file
            try
            {
                System.Net.WebClient client = new System.Net.WebClient();
                string url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath.TrimEnd('/') + "/CMSPages/GetMetaFile.aspx";
                client.DownloadData(url);
                client.Dispose();
            }
            catch
            {
            }
        }
        else
        {
            string connStringDisplay = ConnectionHelper.GetConnectionString(authenticationType, txtServerName.Text.Trim(), Database, txtDBUsername.Text.Trim(), ViewState["install.password"].ToString(), 240, true);
            wzdInstaller.ActiveStepIndex = 3;
            string message = ResHelper.GetFileString("Install.ConnectionStringError") + " <br/><br/><strong>&lt;add name=\"CMSConnectionString\" connectionString=\"" + connStringDisplay + "\"/&gt;</strong><br/><br/>";
            lblErrorConnMessage.Text = message;

            // Show troubleshoot link
            //hlpTroubleshoot.Visible = true;
            //hlpTroubleshoot.TopicName = "DiskPermissions";
            //hlpTroubleshoot.Text = ResHelper.GetFileString("Install.ErrorPermissions");
        }
    }


    protected void wzdInstaller_PreviousButtonClick(object sender, WizardNavigationEventArgs e)
    {
        --StepIndex;
        wzdInstaller.ActiveStepIndex -= 1;
    }


    private bool AddTrialLicenseKeys(string connectionString)
    {
        // Skip creation of trial licence keys if running in WWAG mode
        if (ValidationHelper.GetBoolean(SettingsHelper.AppSettings[WWAG_KEY], false))
        {
            return false;
        }

        string licenseKey = ValidationHelper.GetString(SettingsHelper.AppSettings["CMSTrialKey"], "");
        if (licenseKey != "")
        {
            IDataConnection conn = SqlHelperClass.GetConnection(connectionString);

            // Delete all existing license keys
            string query = "DELETE FROM CMS_LicenseKey";
            conn.ExecuteQuery(query, null, QueryTypeEnum.SQLQuery);

            // License key for 'localhost' and '127.0.0.1' domain
            LicenseKeyInfo licenseInfo = new LicenseKeyInfo();
            licenseInfo.LoadLicense(licenseKey, "localhost");

            if (licenseInfo.ValidationResult != LicenseValidationEnum.Expired)
            {
                query = "INSERT INTO CMS_LicenseKey (LicenseDomain, LicenseKey, LicenseEdition, LicenseExpiration, LicenseServers) VALUES ('localhost', '" + licenseKey + "','" + LicenseKeyInfoProvider.EditionToChar(licenseInfo.Edition) + "','" + licenseInfo.ExpirationDateReal.ToShortDateString() + "', 0)";
                conn.ExecuteQuery(query, null, QueryTypeEnum.SQLQuery);

                licenseInfo = new LicenseKeyInfo();
                licenseInfo.LoadLicense(licenseKey, "127.0.0.1");

                query = "INSERT INTO CMS_LicenseKey (LicenseDomain, LicenseKey, LicenseEdition, LicenseExpiration, LicenseServers) VALUES ('127.0.0.1', '" + licenseKey + "','" + LicenseKeyInfoProvider.EditionToChar(licenseInfo.Edition) + "','" + licenseInfo.ExpirationDateReal.ToShortDateString() + "', 0)";
                conn.ExecuteQuery(query, null, QueryTypeEnum.SQLQuery);
            }
            else
            {
                return false;
            }
        }
        else
        {
            lblError.Text = ResHelper.GetFileString("Install.ErrorTrialLicense");
            return false;
        }

        return true;
    }


    private void InitializeHeader(int index)
    {
        //Help.Visible = true;
        //StartHelp.Visible = true;

        string imgName = null;

        lblHeader.Text = ResHelper.GetFileString("Install.Step") + " - ";

        switch (index)
        {
            case 0:
                imgName = "header_connection.png";
                //Help.TopicName = "DBInstall_StepConnection";
                //StartHelp.TopicName = "DBInstall_StepConnection";
                lblHeader.Text += ResHelper.GetFileString("Install.Step0");
                break;

            case 1:
                imgName = "header_db.png";
                //Help.TopicName = "DBInstall_StepDB";
                //StartHelp.TopicName = "DBInstall_StepDB";
                lblHeader.Text += ResHelper.GetFileString("Install.Step1");
                break;

            case 2:
                imgName = "header_db.png";
                //Help.Visible = false;
                //StartHelp.Visible = false;
                lblHeader.Text += ResHelper.GetFileString("Install.Step2");
                break;

            case 3:
                imgName = "header_db.png";
                //Help.Visible = false;
                //StartHelp.Visible = false;
                lblHeader.Text += ResHelper.GetFileString("Install.Step3");
                break;

            case 4:
                imgName = "header_db.png";
                //Help.TopicName = "DBInstall_StepLicense";
                //StartHelp.TopicName = "DBInstall_StepLicense";
                lblHeader.Text += ResHelper.GetFileString("Install.Step4");
                break;

            case 5:
                imgName = "header_site.png";
                //Help.TopicName = "DBInstall_StepSite";
                //StartHelp.TopicName = "DBInstall_StepSite";
                lblHeader.Text += ResHelper.GetFileString("Install.Step5");
                break;

            case 6:
                imgName = "header_site.png";
                //Help.Visible = false;
                //StartHelp.Visible = false;
                lblHeader.Text += ResHelper.GetFileString("Install.Step6");
                break;

            case 7:
                imgName = "header_finish.png";
                //Help.TopicName = "DBInstall_StepFinish";
                //StartHelp.TopicName = "DBInstall_StepFinish";
                lblHeader.Text += ResHelper.GetFileString("Install.Step7");
                break;

            case 8:
                imgName = "header_db.png";
                //Help.TopicName = "DBInstall_StepCollation";
                //StartHelp.TopicName = "DBInstall_StepCollation";
                lblHeader.Text += ResHelper.GetFileString("Install.Step8");
                break;
        }

        // Calculate step number
        if (PreviousStep == index)
        {
            StepOperation = 0;
        }
        ActualStep += StepOperation;
        lblHeader.Text = string.Format(lblHeader.Text, ActualStep + 1);
        PreviousStep = index;

        imgHeader.ImageUrl = GetImageUrl("Others/Install/" + imgName);
        imgHeader.AlternateText = "Header";
    }


    private void InitProgressBars()
    {
        ltlProgress.Text = "<span id=\"lblProgress\" ></span>";
        ltlDBProgress.Text = "<span id=\"lblProgress\" ></span>";
    }


    private void EnsureDefaultButton()
    {
        if (wzdInstaller.ActiveStep != null)
        {
            if (wzdInstaller.ActiveStep.StepType == WizardStepType.Start)
            {
                Page.Form.DefaultButton =
                    wzdInstaller.FindControl("StartNavigationTemplateContainerID").FindControl("StepNextButton").
                        UniqueID;
            }
            else if (wzdInstaller.ActiveStep.StepType == WizardStepType.Step)
            {
                Page.Form.DefaultButton =
                    wzdInstaller.FindControl("StepNavigationTemplateContainerID").FindControl("StepNextButton").UniqueID;
            }
            else if (wzdInstaller.ActiveStep.StepType == WizardStepType.Finish)
            {
                Page.Form.DefaultButton =
                    wzdInstaller.FindControl("FinishNavigationTemplateContainerID").FindControl("StepFinishButton").
                        UniqueID;
            }
        }
    }

    #region "Installation methods"

    public bool CreateDatabase()
    {
        try
        {
            string message = ResHelper.GetFileString("Installer.LogCreatingDatabase") + " " + txtNewDatabaseName.Text.Trim();
            AddResult(message);
            LogProgressState(LogStatusEnum.Info, message);

            string pass = Convert.ToString(ViewState["install.password"]) + "";
            string connectionString = ConnectionHelper.GetConnectionString(authenticationType, txtServerName.Text.Trim(), "", txtDBUsername.Text.Trim(), pass, 240, false);

            // Create the query
            string query = "CREATE DATABASE [" + txtNewDatabaseName.Text.Trim() + "] COLLATE " + ConnectionHelper.DatabaseCollation;

            // Create the database
            IDataConnection conn = SqlHelperClass.GetConnection(connectionString);
            conn.AllowTransactions = false;
            conn.ExecuteQuery(query, null, QueryTypeEnum.SQLQuery);

            return true;
        }
        catch (Exception ex)
        {
            mDisplayLog = true;
            string message = ResHelper.GetFileString("Intaller.LogErrorCreateDB") + " " + ex.Message;
            AddResult(message);
            LogProgressState(LogStatusEnum.Error, message);
        }
        return false;
    }


    private void InstallDatabase(object parameter)
    {
        if (!InstallDatabase(InstallInfo.ConnectionString, InstallInfo.ScriptsFullPath))
        {
            throw new Exception("[InsrtallDatabase]: Error during database creation.");
        }
    }


    /// <summary>
    /// Performs the database installation
    /// </summary>
    /// <param name="connectionString">Connection string to the database</param>
    /// <param name="scriptsFolder">Folder with the database scripts</param>
    public bool InstallDatabase(string connectionString, string scriptsFolder)
    {
        IDataConnection conn = SqlHelperClass.GetConnection(connectionString);
        string applicationVirtualPath = UrlHelper.WebApplicationVirtualPath;
        // *******************  SQL  *******************************

        // Create database objects 
        string result = ProceedSQLScripts("defaultDBObjects.txt", conn, scriptsFolder, applicationVirtualPath);
        if (result != "")
        {
            mDisplayLog = true;
            string message = ResHelper.GetFileString("Installer.LogErrorCreateDBObjects") + " " + result;
            AddResult(message);
            LogProgressState(LogStatusEnum.Error, message);
            return false;
        }
        // Insert default data
        result = ProceedSQLScripts(@"defaultData.txt", conn, scriptsFolder, applicationVirtualPath);
        if (result != "")
        {
            mDisplayLog = true;
            string message = ResHelper.GetFileString("Installer.LogErrorDefaultData") + " " + HttpUtility.HtmlEncode(result);
            AddResult(message);
            LogProgressState(LogStatusEnum.Error, message);
            return false;
        }
        if (File.Exists(scriptsFolder + @"\webproject.txt"))
        {
            result = ProceedSQLScripts("webproject.txt", conn, scriptsFolder, applicationVirtualPath);
            if (result != "")
            {
                mDisplayLog = true;
                string message = ResHelper.GetFileString("Installer.LogErrorDefaultData") + " " + result;
                AddResult(message);
                LogProgressState(LogStatusEnum.Error, message);
                return false;
            }
        }

        LogProgressState(LogStatusEnum.Finish, ResHelper.GetFileString("Installer.DBInstallFinished"));
        return true;
    }


    /// <summary>
    /// Runs the SQL scripts listed in the given file
    /// </summary>
    /// <param name="fileName">FileName with the scripts to run</param>
    /// <param name="conn">SQL connection</param>
    /// <param name="scriptsFolder">Folder with the SQL scripts</param>
    /// <param name="virtDirName">Application virtual directory name</param>
    private string ProceedSQLScripts(string fileName, IDataConnection conn, string scriptsFolder, string virtDirName)
    {
        string scripts = "";

        try
        {
            // Read all script names into scripts string
            StreamReader str = File.OpenText(scriptsFolder + "\\" + fileName);
            while (str.Peek() > -1)
            {
                string line = str.ReadLine();
                if (line.Trim() != "")
                {
                    scripts += line + ";";
                }
            }
            if (scripts.Trim() != "")
            {
                scripts = scripts.Substring(0, scripts.Length - 1);
            }
            if (scripts == null)
            {
                return null;
            }
            str.Close();

            string[] arrScripts = scripts.Split(';');

            // Trim the virtual directory name
            virtDirName = virtDirName.TrimEnd('/');

            // Execute SQL scripts
            for (int i = 0; i <= arrScripts.GetUpperBound(0); i++)
            {
                // Run query
                string scriptName = arrScripts[i].Trim();
                if (!scriptName.StartsWith("//") && (scriptName != ""))
                {
                    // Log script
                    string message = ResHelper.GetFileString("Installer.LogExecutingScript") + " " + scriptName.ToLower();
                    AddResult(message);
                    LogProgressState(LogStatusEnum.Info, message);

                    str = File.OpenText(scriptsFolder + @"\" + scriptName);
                    string query = str.ReadToEnd();
                    str.Close();
                    if (query != "")
                    {
                        // Get the script
                        if (arrScripts[i].ToLower().Trim().EndsWith("_data.sql"))
                        {
                            if (virtDirName + "" != "")
                            {
                                query = query.Replace("/VivaCMS/files", virtDirName + "/files");
                                query = query.Replace("/CorporateSite/", virtDirName + "/");
                            }
                            else
                            {
                                query = query.Replace("/VivaCMS/files", "/files");
                                query = query.Replace("/CorporateSite/", "/");
                            }
                        }

                        // Run the query
                        conn.ExecuteQuery(query, null, QueryTypeEnum.SQLQuery);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

        return "";
    }

    #endregion


    #region "Error handling methods"

    protected void HandleError(string message, WizardNavigationEventArgs e)
    {
        if (StepIndex > 1)
        {
            --StepIndex;
        }
        lblError.Text = message;
        e.Cancel = true;
    }


    protected void HandleError(string message)
    {
        if (StepIndex > 1)
        {
            --StepIndex;
        }
        lblError.Text = message;
    }


    protected void HandleError(string message, string resourceString, string topic)
    {
        if (StepIndex > 1)
        {
            --StepIndex;
        }
        lblError.Text = message;
        //hlpTroubleshoot.Visible = true;
        //hlpTroubleshoot.Text = ResHelper.GetFileString(resourceString);
        //hlpTroubleshoot.TopicName = topic;
    }

    #endregion


    #region "Logging methods"

    /// <summary>
    /// Appends the result string to the result message
    /// </summary>
    /// <param name="result">String to append</param>
    public void AddResult(string result)
    {
        mResult = result + "\n" + mResult;
    }


    /// <summary>
    /// Log progress state
    /// </summary>
    /// <param name="type">Type of the message</param>
    /// <param name="message">Message to be logged</param>
    public void LogProgressState(LogStatusEnum type, string message)
    {
        string[] status = InstallInfo.InstallLog.Split(new string[] { InstallInfo.SEPARATOR }, StringSplitOptions.None);

        // Wrong format of the internal status
        if (status.Length != 4)
        {
            InstallInfo.InstallLog = "F" + InstallInfo.SEPARATOR + "Wrong internal log." + InstallInfo.SEPARATOR + InstallInfo.SEPARATOR;
        }

        switch (type)
        {
            case LogStatusEnum.Info:
                status[0] = "I";
                status[1] = message + "<br />" + status[1];
                break;

            case LogStatusEnum.Error:
                status[0] = "F";
                status[2] += "<strong>" + ResHelper.GetFileString("Global.ErrorSign") + "</strong>" + message + "<br />";
                break;

            case LogStatusEnum.Warning:
                status[3] += "<strong>" + ResHelper.GetFileString("Global.Warning") + "</strong>" + message + "<br />";
                break;

            case LogStatusEnum.Finish:
                status[0] = "F";
                status[1] = "<strong>" + message + "</strong><br /><br />" + status[1];
                break;
        }

        InstallInfo.InstallLog = status[0] + InstallInfo.SEPARATOR + status[1] + InstallInfo.SEPARATOR + status[2] + InstallInfo.SEPARATOR + status[3];
    }

    #endregion


    #region "ICallbackEventHandler Members"

    public string GetCallbackResult()
    {
        return hdnState.Value;
    }


    public void RaiseCallbackEvent(string eventArgument)
    {
        // Get arguments
        string[] args = eventArgument.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        bool cancel = ValidationHelper.GetBoolean(args[0], false);
        bool import = (args[1] == "IM");
        int messageLength = 0;
        int errorLength = 0;
        int warningLength = 0;

        if (args.Length == 5)
        {
            messageLength = ValidationHelper.GetInteger(args[2], 0);
            errorLength = ValidationHelper.GetInteger(args[3], 0);
            warningLength = ValidationHelper.GetInteger(args[4], 0);
        }

        if (import)
        {
            try
            {
                // Cancel
                if (cancel)
                {
                    ImportManager.Settings.Cancel();
                }

                hdnState.Value = ImportManager.Settings.GetLimitedProgressLog(messageLength, errorLength, warningLength);
            }
            catch
            {
                ImportManager.Settings.LogProgressState(LogStatusEnum.Finish, ResHelper.GetString("SiteImport.Applicationrestarted"));
                hdnState.Value = ImportManager.Settings.GetLimitedProgressLog(messageLength, errorLength, warningLength);
            }
        }
        else
        {
            try
            {
                hdnState.Value = InstallInfo.GetLimitedProgressLog(messageLength, errorLength, warningLength);
            }
            catch
            {
                LogProgressState(LogStatusEnum.Finish, ResHelper.GetFileString("SiteImport.Applicationrestarted"));
                hdnState.Value = InstallInfo.GetLimitedProgressLog(messageLength, errorLength, warningLength);
            }
        }
    }

    #endregion
}

