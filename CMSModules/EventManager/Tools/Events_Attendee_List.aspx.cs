using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.EventManager;
using CMS.LicenseProvider;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.SiteProvider;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_EventManager_Tools_Events_Attendee_List : CMSEventManagerPage
{
    protected int eventNodeId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Reload();
    }


    /// <summary>
    /// Reloads control.
    /// </summary>
    public void Reload()
    {
        eventNodeId = QueryHelper.GetInteger("eventId", 0);

        // Script for UniGri's edit action 
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "EditAttendee",
            ScriptHelper.GetScript("function EditAttendee(attendeeId){" +
            "location.replace('Events_Attendee_Edit.aspx?attendeeid=' + attendeeId + '&eventnodeid=" + eventNodeId + "'); }"));

        // Refresh parent frame header
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "RefreshHeader",
            ScriptHelper.GetScript("function RefreshHeader() { " +
            "parent.frames['eventsHeader'].location.replace(parent.frames['eventsHeader'].location); } \n"));

        UniGrid.WhereCondition = "AttendeeEventNodeId = " + eventNodeId;
        UniGrid.OnAction += new OnActionEventHandler(UniGrid_OnAction);
        UniGrid.ZeroRowsText = ResHelper.GetString("Events_List.NoAttendees");
        UniGrid.HideControlForZeroRows = false;

        // New item link
        string[,] actions = new string[1, 6];
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("Events_Attendee_List.NewItemCaption");
        actions[0, 2] = null;
        actions[0, 3] = ResolveUrl("Events_Attendee_Edit.aspx?eventnodeid=" + eventNodeId.ToString());
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("CMSModules/CMS_EventManager/addattendee.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGrid_OnAction(string actionName, object actionArgument)
    {
        // Check 'Modify' permission (because of delete action in unigrid)
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.eventmanager", "Modify"))
        {
            RedirectToAccessDenied("cms.eventmanager", "Modify");
        }

        if (actionName == "delete")
        {
            EventAttendeeInfoProvider.DeleteEventAttendeeInfo(ValidationHelper.GetInteger(actionArgument, 0));
            // Refresh parent frame header
            ltlScript.Text = ScriptHelper.GetScript("RefreshHeader();");
            this.Reload();
        }
        else if (actionName == "sendemail")
        {
            // Resend invitation email
            TreeProvider mTree = new TreeProvider(CMSContext.CurrentUser);
            TreeNode node = mTree.SelectSingleNode(eventNodeId);

            EventAttendeeInfo eai = EventAttendeeInfoProvider.GetEventAttendeeInfo(ValidationHelper.GetInteger(actionArgument, 0));

            if (node != null && node.NodeClassName.ToLower() == "cms.bookingevent" && eai != null)
            {
                EventProvider.SendInvitation(CMSContext.CurrentSiteName, node.DataRow, eai.DataClass.DataRow, TimeZoneHelper.ServerTimeZone);

                lblInfo.Text = ResHelper.GetString("eventmanager.invitationresend");
                lblInfo.Visible = true;
            }
        }
    }
}
