using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.PortalControls;
using CMS.PortalEngine;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.URLRewritingEngine;

public partial class CMSWebParts_Widgets_WidgetActions : CMSAbstractWebPart
{
    #region "Variables"

    private WidgetZoneTypeEnum zoneType = WidgetZoneTypeEnum.None;
    private PageInfo pi = null;
    private TreeProvider mTreeProvider = null;
    private bool resetAllowed = true;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets widget zone type
    /// </summary>
    public string WidgetZoneType
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("WidgetZoneType"), String.Empty);
        }
        set
        {
            this.SetValue("WidgetZoneType", value);
        }
    }


    /// <summary>
    /// Gets or sets widget zone type
    /// </summary>
    public string WidgetZoneID
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("WidgetZoneID"), String.Empty);
        }
        set
        {
            this.SetValue("WidgetZoneID", value);
        }
    }


    /// <summary>
    /// Gets or sets text for add button
    /// </summary>
    public string AddButtonText
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("AddButtonText"), String.Empty);
        }
        set
        {
            this.SetValue("AddButtonText", value);
        }
    }


    /// <summary>
    /// Gets or sets text for reset button
    /// </summary>
    public string ResetButtonText
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("ResetButtonText"), String.Empty);
        }
        set
        {
            this.SetValue("ResetButtonText", value);
        }
    }


    /// <summary>
    /// Enables or disables reset button
    /// </summary>
    public bool DisplayResetButton
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("DisplayResetButton"), true);
        }
        set
        {
            this.SetValue("DisplayResetButton", value);
        }
    }


    /// <summary>
    /// Enables or disables add widget button
    /// </summary>
    public bool DisplayAddButton
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("DisplayAddButton"), true);
        }
        set
        {
            this.SetValue("DisplayAddButton", value);
        }
    }


    /// <summary>
    /// Enables or disables confirmation for reset button
    /// </summary>
    public bool ResetConfirmationRequired
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("ResetConfirmationRequired"), true);
        }
        set
        {
            this.SetValue("ResetConfirmationRequired", value);
        }
    }


    /// <summary>
    /// Return instance of tree provider
    /// </summary>
    public TreeProvider TreeProvider
    {
        get
        {
            if (mTreeProvider == null)
            {
                mTreeProvider = new TreeProvider(CMSContext.CurrentUser);
            }
            return mTreeProvider;
        }
        set
        {
            mTreeProvider = value;
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            // Nothing to render, nothing to do
            if ((!this.DisplayAddButton && !this.DisplayResetButton) || this.PagePlaceholder.UsingDefaultPageTemplate)
            {
                this.Visible = false;
                return;
            }

            CurrentUserInfo currentUser = CMSContext.CurrentUser;
            zoneType = WidgetZoneTypeCode.ToEnum(this.WidgetZoneType);

            // Check security
            if (((zoneType == WidgetZoneTypeEnum.Group) && !currentUser.IsGroupAdministrator(CMSContext.CurrentPageInfo.NodeGroupId))
                || ((zoneType == WidgetZoneTypeEnum.Editor) && (!currentUser.IsEditor || !currentUser.IsAuthorizedPerResource("CMS.Content", "Modify")))
                || ((zoneType == WidgetZoneTypeEnum.User) && !currentUser.IsAuthenticated()))
            {
                this.Visible = false;
                resetAllowed = false;
                return;
            }

            // Displaying - Editor zone only in edit mode, User/Group zone only in Live site/Preview mode
            if (((zoneType == WidgetZoneTypeEnum.Editor) && (CMSContext.ViewMode != ViewModeEnum.Edit)) ||
                (((zoneType == WidgetZoneTypeEnum.User) || (zoneType == WidgetZoneTypeEnum.Group)) && ((CMSContext.ViewMode != ViewModeEnum.LiveSite) && (CMSContext.ViewMode != ViewModeEnum.Preview))))
            {
                this.Visible = false;
                resetAllowed = false;
                return;
            }


            // If use checkin checkout enabled, check if document is checkout by current user
            if (VersionManager.UseCheckInCheckOut(CMSContext.CurrentSiteName))
            {
                TreeNode currentNode = DocumentHelper.GetDocument(CMSContext.CurrentDocument.DocumentID, this.TreeProvider);
                if (currentNode != null)
                {
                    // Check if node is under workflow
                    WorkflowManager wm = new WorkflowManager(this.TreeProvider);
                    WorkflowInfo wi = wm.GetNodeWorkflow(currentNode);
                    if (wi != null)
                    {
                        int checkedOutBy = ValidationHelper.GetInteger(currentNode.GetValue("DocumentCheckedOutByUserID"), 0);
                        if (checkedOutBy != CMSContext.CurrentUser.UserID)
                        {
                            this.Visible = false;
                            resetAllowed = false;
                            return;
                        }
                    }
                }
            }

            // Find widget zone
            pi = CMSContext.CurrentPageInfo;
            WebPartZoneInstance zoneInstance = null;

            if (pi != null)
            {
                PageTemplateInfo pti = pi.PageTemplateInfo;

                // ZodeID specified directly
                if (!String.IsNullOrEmpty(this.WidgetZoneID))
                {
                    zoneInstance = pti.GetZone(this.WidgetZoneID);
                }

                // Zone not find or specified zone is not of correct type
                if ((zoneInstance == null) || (zoneInstance.WidgetZoneType != zoneType))
                {
                    zoneInstance = null;
                    
                    // Get current placeholder and try find first suitable zone
                    CMSPagePlaceholder placeholder = PortalHelper.FindParentPlaceholder(this);
                    if (placeholder != null)
                    {
                       ArrayList zones = placeholder.WebPartZones;
                        if (zones != null)
                        {
                            foreach(CMSWebPartZone zone in zones)
                            {
                                if ((zone.ZoneInstance != null) && (zone.ZoneInstance.WidgetZoneType == zoneType))
                                {
                                    zoneInstance = zone.ZoneInstance;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            // No suitable zones on the page, nothing to do
            if (zoneInstance == null)
            {
                this.Visible = false;
                resetAllowed = false;
                return;
            }

            // Adding is enabled
            if (this.DisplayAddButton)
            {
                lnkAddWidget.Visible = true;
                lnkAddWidget.Text = DataHelper.GetNotEmpty(this.AddButtonText, ResHelper.GetString("widgets.addwidget"));

                string script = "NewWidget('" + HttpUtility.UrlEncode(zoneInstance.ZoneID) + "', '" + HttpUtility.UrlEncode(pi.NodeAliasPath) + "'); return false;";
                lnkAddWidget.Attributes.Add("onclick", script);
            }

            // Reset is enabled
            if (this.DisplayResetButton)
            {
                btnReset.Visible = true;
                btnReset.Text = DataHelper.GetNotEmpty(this.ResetButtonText, ResHelper.GetString("widgets.resettodefault"));
                btnReset.Click += new EventHandler(btnReset_Click);

                // Add confirmation if required
                if (this.ResetConfirmationRequired)
                {
                    btnReset.Attributes.Add("onclick", "if (!confirm('" + ResHelper.GetString("widgets.resetzoneconfirmtext") + "')) return false;");
                }
            }

            // Set the panel css clas with dependence on actions zone type
            switch (zoneType)
            {
                // Editor
                case WidgetZoneTypeEnum.Editor:
                    pnlWidgetActions.CssClass = "EditorWidgetActions";
                    break;

                // User
                case WidgetZoneTypeEnum.User:
                    pnlWidgetActions.CssClass = "UserWidgetActions";
                    break;

                // Group
                case WidgetZoneTypeEnum.Group:
                    pnlWidgetActions.CssClass = "GroupWidgetActions";
                    break;
            }
        }
    }


    /// <summary>
    /// Handles reset button click. Resets zones of specified type to default settings
    /// </summary>
    protected void btnReset_Click(object sender, EventArgs e)
    {
        // Security check
        if (!DisplayResetButton || !resetAllowed)
        {
            return;
        }

        if ((zoneType == WidgetZoneTypeEnum.Editor) || (zoneType == WidgetZoneTypeEnum.Group))
        {
            // Clear document webparts/group webparts
            TreeNode node = DocumentHelper.GetDocument(pi.DocumentId, this.TreeProvider);

            if (zoneType == WidgetZoneTypeEnum.Editor)
            {
                node.SetValue("DocumentWebParts", String.Empty);
            }
            else if (zoneType == WidgetZoneTypeEnum.Group)
            {
                node.SetValue("DocumentGroupWebParts", String.Empty);
            }

            // Save the document
            DocumentHelper.UpdateDocument(node, this.TreeProvider);
        }
        else if (zoneType == WidgetZoneTypeEnum.User)
        {
            // Delete user personalization info
            PersonalizationInfo up = PersonalizationInfoProvider.GetUserPersonalization(CMSContext.CurrentUser.UserID, pi.DocumentId);
            PersonalizationInfoProvider.DeletePersonalizationInfo(up);
        }

        // Make redirect to see changes after load
        string url = URLRewriter.CurrentURL;
        UrlHelper.Redirect(url);
    }

    #endregion
}
