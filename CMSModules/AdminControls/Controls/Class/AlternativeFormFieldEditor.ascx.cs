using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.FormEngine;
using CMS.UIControls;


public delegate void OnAlternativeFieldEditorSaveEventHandler();


public partial class CMSModules_AdminControls_Controls_Class_AlternativeFormFieldEditor : CMSUserControl
{
    protected int mAlternativeFormId = 0;
    protected FieldEditorControlsEnum mDisplayedControls = FieldEditorControlsEnum.None;


    #region "Public properties"


    /// <summary>
    /// Indicates if system fields (node and document fields) are enabled
    /// </summary>
    public bool EnableSystemFields
    {
        get
        {
            return fieldEditor.EnableSystemFields;
        }
        set
        {
            fieldEditor.EnableSystemFields = value;
        }
    }


    /// <summary>
    /// Indicates if field visibility selector should be displayed.
    /// </summary>
    public bool ShowFieldVisibility
    {
        get
        {
            return fieldEditor.ShowFieldVisibility;
        }
        set
        {
            fieldEditor.ShowFieldVisibility = value;
        }
    }


    /// <summary>
    /// Form id (with alterantive form definition)
    /// </summary>
    public int AlternativeFormID
    {
        get
        {
            return mAlternativeFormId;
        }
        set
        {
            mAlternativeFormId = value;
        }
    }


    /// <summary>
    /// Specify set of controls which should be offered for field types
    /// </summary>
    public FieldEditorControlsEnum DisplayedControls
    {
        get
        {
            return mDisplayedControls;
        }
        set
        {
            mDisplayedControls = value;
        }
    }

    #endregion


    #region "Public events"

    public event OnAlternativeFieldEditorSaveEventHandler OnBeforeSave;
    public event OnAlternativeFieldEditorSaveEventHandler OnAfterSave;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        AlternativeFormInfo afi = AlternativeFormInfoProvider.GetAlternativeFormInfo(mAlternativeFormId);

        if (afi != null)
        {
            DataClassInfo dci = DataClassInfoProvider.GetDataClass(afi.FormClassID);
            if (dci != null)
            {
                string formDef = dci.ClassFormDefinition;

                if (afi.FormCoupledClassID > 0)
                {
                    // If coupled class is defined combine form definitions
                    DataClassInfo coupledDci = DataClassInfoProvider.GetDataClass(afi.FormCoupledClassID);
                    if (coupledDci != null)
                    {
                        formDef = FormHelper.MergeFormDefinitions(formDef, coupledDci.ClassFormDefinition, true);
                    }
                }

                // Merge class and alternative form definitions
                formDef = FormHelper.MergeFormDefinitions(formDef, afi.FormDefinition);

                // Initialize field editor mode and load form definition
                fieldEditor.Mode = FieldEditorModeEnum.AlternativeFormDefinition;
                fieldEditor.FormDefinition = formDef;
                // Specify set of controls which should be offered for field types
                fieldEditor.DisplayedControls = mDisplayedControls;

                // Handle definition update (move up, move down, delete, OK button)
                fieldEditor.OnAfterDefinitionUpdate += new OnAfterDefinitionUpdateEventHandler(fieldEditor_OnAfterDefinitionUpdate);
            }
            else
            {
                ShowErrorMessage();
            }
        }
    }


    /// <summary>
    /// After form definition update event handler.
    /// </summary>
    void fieldEditor_OnAfterDefinitionUpdate()
    {
        // Perform OnBeforeSave if defined
        if (OnBeforeSave != null)
        {
            OnBeforeSave();
        }

        // Stop processing if set from outside
        if (StopProcessing)
        {
        	return;
        }

        // Get alternative form info object and data class info object
        AlternativeFormInfo afi = AlternativeFormInfoProvider.GetAlternativeFormInfo(mAlternativeFormId);

        if (afi != null)
        {
            DataClassInfo dci = DataClassInfoProvider.GetDataClass(afi.FormClassID);

            if (dci != null)
            {
                string formDefinition = dci.ClassFormDefinition;

                if (afi.FormCoupledClassID > 0)
                {
                    // Combine form definitions of class and its coupled class
                    DataClassInfo coupledDci = DataClassInfoProvider.GetDataClass(afi.FormCoupledClassID);
                    if (coupledDci != null)
                    {
                        formDefinition = FormHelper.MergeFormDefinitions(formDefinition, coupledDci.ClassFormDefinition, true);
                    }
                }

                // Compare original and alternative form definitions - store differences only
                afi.FormDefinition = FormHelper.GetFormDefinitionDifference(formDefinition, fieldEditor.FormDefinition);
                // Update alternative form info in database
                AlternativeFormInfoProvider.SetAlternativeFormInfo(afi);
            }
            else
            {
                ShowErrorMessage();
            }
        }

        // Perform OnAfterSave if defined
        if (OnAfterSave != null)
        {
            OnAfterSave();
        }
    }


    /// <summary>
    /// Shows invalid id message.
    /// </summary>
    void ShowErrorMessage()
    {
        lblError.Visible = true;
        lblError.Text = ResHelper.GetString("general.invalidid");
    }
}
