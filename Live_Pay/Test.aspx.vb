﻿Imports System.Xml

Partial Class Live_Pay_Test
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Xml As String = String.Empty
        Xml = "<geonames>"
        For i As Integer = 0 To 10
            Xml = String.Concat(Xml, "<geoname>")
            Xml = String.Concat(Xml, "<name><![CDATA[&lt;u&gt;N&lt;/u&gt;ikos", i, "]]></name>")
            Xml = String.Concat(Xml, "<lat>", i, "</lat>")
            Xml = String.Concat(Xml, "<lng>", i, "</lng>")
            Xml = String.Concat(Xml, "<geonameId>", i, "</geonameId>")
            Xml = String.Concat(Xml, "<countryCode>", i, "</countryCode>")
            Xml = String.Concat(Xml, "<countryName>", i, "</countryName>")
            Xml = String.Concat(Xml, "<fcl>", i, "</fcl>")
            Xml = String.Concat(Xml, "<fcode>", i, "</fcode>")
            Xml = String.Concat(Xml, "</geoname>")
        Next
        Xml = String.Concat(Xml, "</geonames>")
        Response.ContentType = "text/xml"
        Response.Write("<?xml version='1.0' encoding='utf-8'?>")
        Response.Write(Xml)
    End Sub
End Class
