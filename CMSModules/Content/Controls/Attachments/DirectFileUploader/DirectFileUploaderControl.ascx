<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DirectFileUploaderControl.ascx.cs"
    Inherits="CMSModules_Content_Controls_Attachments_DirectFileUploader_DirectFileUploaderControl" %>
<asp:PlaceHolder ID="plcUploader" runat="server">
    <asp:Literal ID="ltlCss" runat="server" EnableViewState="false" />
    <asp:Literal ID="ltlInnerDiv" runat="server" EnableViewState="false" />
    <asp:Panel ID="pnlLoading" runat="server">
        <asp:Image ID="imgLoading" runat="server" EnableViewState="false" /><asp:Label ID="lblLoading"
            runat="server" EnableViewState="false" />
    </asp:Panel>
    <label id="container" runat="server" unselectable="on">
        <cms:CMSFileUpload ID="ucFileUpload" runat="server" />
    </label>
    <cms:CMSButton ID="btnHidden" runat="server" OnClick="btnHidden_Click" EnableViewState="false" />
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
</asp:PlaceHolder>
<asp:PlaceHolder ID="plcStandard" runat="server" Visible="false">
    <asp:Panel ID="pnlStandard" runat="server">
        <asp:Image ID="imgStandard" runat="server" EnableViewState="false" /><asp:Label ID="lblStandard"
            runat="server" EnableViewState="false" />
    </asp:Panel>
</asp:PlaceHolder>
