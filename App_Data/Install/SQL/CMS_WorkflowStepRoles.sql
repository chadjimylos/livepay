CREATE TABLE [CMS_WorkflowStepRoles] (
		[StepID]     int NOT NULL,
		[RoleID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_WorkflowStepRoles]
	ADD
	CONSTRAINT [PK_CMS_WorkflowStepRoles]
	PRIMARY KEY
	([StepID], [RoleID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_WorkflowStepRoles]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WorkflowStepRoles_RoleID_CMS_Role]') IS NULL
BEGIN
		ALTER TABLE [CMS_WorkflowStepRoles]
			ADD CONSTRAINT [FK_CMS_WorkflowStepRoles_RoleID_CMS_Role]
			FOREIGN KEY ([RoleID]) REFERENCES [CMS_Role] ([RoleID])
END
IF OBJECT_ID(N'[CMS_WorkflowStepRoles]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_WorkflowStep]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_WorkflowStepRoles_StepID_CMS_WorkflowStep]') IS NULL
BEGIN
		ALTER TABLE [CMS_WorkflowStepRoles]
			ADD CONSTRAINT [FK_CMS_WorkflowStepRoles_StepID_CMS_WorkflowStep]
			FOREIGN KEY ([StepID]) REFERENCES [CMS_WorkflowStep] ([StepID])
END
