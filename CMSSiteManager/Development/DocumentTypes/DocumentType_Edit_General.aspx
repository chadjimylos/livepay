<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentType_Edit_General.aspx.cs"
    Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_General" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Document Type Edit - General" %>

<%@ Register Src="~/CMSAdminControls/UI/Selectors/LoadGenerationSelector.ascx" TagName="LoadGenerationSelector"
    TagPrefix="uc1" %>
<%@ Register Src="~/CMSModules/PortalEngine/FormControls/PageTemplates/selectpagetemplate.ascx" TagName="selectpagetemplate"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblDisplayName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbDisplayName" CssClass="TextBoxField" MaxLength="100"
                    EnableViewState="false" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDisplayName" runat="server"
                    EnableViewState="false" ControlToValidate="tbDisplayName" Display="dynamic" />
            </td>
        </tr>
        <tr>
            <td style="vertical-align: text-top; padding-top: 5px" class="FieldLabel">
                <asp:Label runat="server" ID="lblFullCodeName" EnableViewState="false" />
            </td>
            <td>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:TextBox runat="server" ID="tbNamespaceName" CssClass="TextBoxField" MaxLength="49"
                                EnableViewState="false" />
                        </td>
                        <td>
                            .
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="tbCodeName" CssClass="TextBoxField" MaxLength="50"
                                EnableViewState="false" />
                        </td>
                    </tr>
                    <tr style="color: Green">
                        <td>
                            <asp:Label runat="server" ID="lblNamespaceName" EnableViewState="false" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lblCodeName" EnableViewState="false" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorNamespaceName" runat="server"
                                EnableViewState="false" ControlToValidate="tbNamespaceName" Display="dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorNameSpaceName" runat="server"
                                EnableViewState="false" Display="dynamic" ControlToValidate="tbNamespaceName" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCodeName" runat="server" EnableViewState="false"
                                ControlToValidate="tbCodeName" Display="dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorCodeName" runat="server"
                                EnableViewState="false" Display="dynamic" ControlToValidate="tbCodeName" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblTableName" EnableViewState="false" />
            </td>
            <td>
                <asp:Label runat="server" ID="lblTableNameValue" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblNewPage" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtNewPage" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblViewPage" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtViewPage" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblEditingPage" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbEditingPage" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblPreviewPage" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtPreviewPage" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblListPage" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox runat="server" ID="tbListPage" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblClassUsePublishFromTo" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkClassUsePublishFromTo" CssClass="EditingFormCheckBox"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblTemplateSelection" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkTemplateSelection" CssClass="EditingFormCheckBox"
                    OnCheckedChanged="chkTemplateSelection_CheckedChanged" AutoPostBack="true" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblDefaultTemplate" EnableViewState="false" />
            </td>
            <td>
                <cms:selectpagetemplate ID="templateDefault" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblIsMenuItem" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkIsMenuItem" CssClass="EditingFormCheckBox" EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblIsProduct" EnableViewState="false" />
            </td>
            <td>
                <asp:CheckBox runat="server" ID="chkIsProduct" CssClass="EditingFormCheckBox" EnableViewState="false" />
            </td>
        </tr>
        <asp:PlaceHolder runat="server" ID="plcLoadGeneration">
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblLoadGeneration" EnableViewState="false" />
                </td>
                <td>
                    <uc1:LoadGenerationSelector ID="drpGeneration" runat="server" />
                </td>
            </tr>
        </asp:PlaceHolder>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" CssClass="SubmitButton"
                    EnableViewState="false" />
            </td>
        </tr>
    </table>
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
</asp:Content>
