﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCHistoryOfRecords.ascx.vb" Inherits="CMSTemplates_AegeanPower_UCCustomerBills" %>
<style type="text/css">


</style>

<script language="javascript" type="text/javascript">
    function SetViewType(val) {
        document.getElementById('<%=hiddenViewType.ClientID %>').value = val
    }

    function SetYearData(obj) {
        document.getElementById('<%=hiddenYear.ClientID %>').value = obj.innerHTML
    }

    function SetProviderData(obj) {
        document.getElementById('<%=hiddenProviderNumber.ClientID %>').value = obj.innerHTML
    }

    function ShowHidePrvRpt() {
        if (document.getElementById('ProviderRptMain').style.display == '') {
            document.getElementById('ProviderRptMain').style.display = 'none'
        } else {
            document.getElementById('ProviderRptMain').style.display = ''
        }
    }

</script>


<asp:UpdatePanel ID="updpnl" runat="server" UpdateMode="Always" >
<ContentTemplate>
<div class="BillPageMain">
    <div style="float:left; width:490px;" > 
        <div class="BillBgBlue">
            <div class="BillViewTypeAllDiv" id="BillTopGvLnkAllBill" runat="server" ><asp:LinkButton OnClientClick="SetViewType(0)" CssClass="BillsGvShortLnks_Bold" ID="LnkViewAllBills" runat="server"><%=ResHelper.LocalizeString("{$=Πλήρης λίστα|en-us=Πλήρης λίστα$}") %></asp:LinkButton></div>
            <div class="BillViewTypeAllPointer"><img src="/App_Themes/AegeanPower/Images/Bills/GreenPointer.png" /></div>
            <div class="BillViewTypeLastDiv" id="BillTopGvLnkLastBill" runat="server" ><asp:LinkButton OnClientClick="SetViewType(1)" CssClass="BillsGvShortLnks" ID="LnkViewLastBill" runat="server"><%=ResHelper.LocalizeString("{$=Τρέχων Λογαριασμός|en-us=Τρέχων Λογαριασμός$}") %></asp:LinkButton></div>
            <div class="BillViewTypeLastPointer"><img src="/App_Themes/AegeanPower/Images/Bills/GreenPointer.png" /></div>
            
                <div class="Fleft BillsGvShortLnks" ><%=ResHelper.LocalizeString("{$=Αριθμός Παροχής:|en-us=Αριθμός Παροχής:$}") %></div>
                <div class="Fleft BillPrvField" > 
                <div id="ProviderRptMain" class="BillDDLPRV" style="display:none">
                        <div class="BillRptTop">
                            <div class="BillRptPrvTopValue" id="rptTopValue" runat="server"></div>
                        </div>
                        <div class="BillRptCenterMain" id="BillRptCenterMain" runat="server">
                            <asp:Repeater ID="rptProviders" runat="server">
                            <ItemTemplate>
                                <div class="BillRptPrvLnkDiv"><div class="BillRptPrvLnkDivIn"><asp:LinkButton OnClick="lnkRptProvider_Click" ID="lnkRptProvider" CssClass="BillRptPrvLink" OnClientClick="SetProviderData(this)" runat="server" Text='<%#Eval("Provider_Number") %>'></asp:LinkButton></div></div>
                                <div class="BillRptPrvSep">&nbsp;</div>
                            </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="BillRptBottom" ></div>
                    </div>

                    <div  class="BillBgPrvNoTxt">
                        <div ID="LlbProviderNo" runat="server" class="BillsProvNumLbl"></div>
                    </div>
                    
                 </div>
                 <div class="Fleft PT4"><img onclick="ShowHidePrvRpt()" class="Chant" src="/App_Themes/AegeanPower/Images/Bills/GreenDDLPointer.png" /></div>
                <div class="Clear"></div>
            
        </div>
        <div class="PT1">
            <asp:GridView ID="GvBills" runat="server" AutoGenerateColumns="false" GridLines="None">
            <AlternatingRowStyle CssClass="BillsGv_RowB" />
            <RowStyle  CssClass="BillsGv_RowA" />
                <Columns>

                    <asp:TemplateField HeaderText="Ημ/νία" HeaderStyle-Width="74px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblIssueDate" runat="server" Text='<%#Eval("FormInserted") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Μετρητής"  HeaderStyle-Width="82px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblSelectCounter" CssClass="BillGv_Price" runat="server" Text='<%#Eval("SelectCounter") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Ένδειξη (kWh)"  HeaderStyle-Width="81px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblIndication" CssClass="BillGv_ExpDate" runat="server" Text='<%#Eval("Indication") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Νυχτερινή ένδειξη (kWh)"  HeaderStyle-Width="82px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblNightlifeMeterReading" CssClass="BillGv_ExpDate" runat="server" Text='<%#Eval("NightlifeMeterReading") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Άεργος ένδειξη (kWh)"  HeaderStyle-Width="79px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblReactiveMeterReading" CssClass="BillGv_ExpDate" runat="server" Text='<%#Eval("ReactiveMeterReading") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Μέγιστη Ζήτηση (kW) "  HeaderStyle-Width="90px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblMaximumDemand" CssClass="BillGv_ExpDate" runat="server" Text='<%#Eval("MaximumDemand") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--<asp:TemplateField HeaderText="Ποσό"  HeaderStyle-Width="115px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblAmmount" CssClass="BillGv_Price" runat="server" Text='<%#Eval("Amount") & " &euro;" %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Ημ/νία Λήξης" HeaderStyle-Width="120px" HeaderStyle-CssClass="BillsGv_Title">
                        <ItemTemplate>
                            <asp:Label ID="lblExpDate" CssClass="BillGv_ExpDate" runat="server" Text='<%#Eval("ExpDate") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="Επιλογές" ItemStyle-CssClass="BillsGv_RowA_Last" HeaderStyle-Width="67px" HeaderStyle-CssClass="BillsGv_Title_Last">
                        <ItemTemplate>
                           <div>
                              <div class="Fleft PL10" ><img src="/App_Themes/AegeanPower/Images/Bills/CardIcon.png" /></div>
                              <div class="Fleft PL10" ><img src="/App_Themes/AegeanPower/Images/Bills/PDFIcon.png" /></div>
                              <div class="Clear"></div>
                           </div>
                        </ItemTemplate>
                    </asp:TemplateField>--%>

                </Columns>
                <EmptyDataTemplate >
                    <div style="padding:10px 0px 5px 10px;"><%= ResHelper.LocalizeString("{$=Δεν βρέθηκε Ιστορικό καταχωρήσεων.|en-us=History records not found.$}")%></div>
                </EmptyDataTemplate>
            </asp:GridView>

        </div>
        <div class="BillBgBlue" id="PagerMain" runat="server" visible="false">
            <div id="PagerLeft" runat="server"  visible="false">
                <div class="Fleft Bill_GvPagerPointLeft" ><img src="/App_Themes/AegeanPower/Images/Bills/GreenPointerLeft.png" id="PagerLeftPointer" runat="server" /></div>
                <div class="Fleft Width438"><asp:LinkButton CssClass="BillsGvShortLnks" OnClientClick="SetYearData(this)" ID="PagerlnkLastYear" runat="server" ></asp:LinkButton></div>
            </div>
            <div class="Fleft Width448" id="PagerCenter" runat="server" visible="false">&nbsp;</div>
            <div id="PagerRight" runat="server" visible="false">
                <div class="Fleft"><asp:LinkButton CssClass="BillsGvShortLnks" OnClientClick="SetYearData(this)" ID="PagerLnkNextYear" runat="server" ></asp:LinkButton></div>
                <div class="Fleft Bill_GvPagerPointRight" ><img src="/App_Themes/AegeanPower/Images/Bills/GreenPointer.png" /></div>
            </div> 
            
            <div class="Clear"></div>
        </div>
        
    </div>
</div>
<div style="display:none">
<asp:TextBox id="hiddenViewType" runat="server" Text="0" ></asp:TextBox>
<asp:TextBox id="hiddenYear" runat="server"  ></asp:TextBox>
<asp:TextBox id="hiddenProviderNumber" runat="server"  ></asp:TextBox>
</div>
</ContentTemplate>
</asp:UpdatePanel>