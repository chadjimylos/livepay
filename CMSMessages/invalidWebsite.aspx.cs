using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.URLRewritingEngine;
using CMS.UIControls;
using CMS.LicenseProvider;

public partial class CMSMessages_invalidWebsite : MessagePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string mPrefix = "http://";

        this.titleElem.TitleText = ResHelper.GetString("Message.InvalidWebSite");
        this.titleElem.TitleImage = GetImageUrl("Others/Messages/denied.png");

        string mDomain = HttpContext.Current.Request.Url.Host;
        if (HttpContext.Current.Request.Url.Port != 80)
        {
            mDomain = mDomain + ":" + HttpContext.Current.Request.Url.Port.ToString();
        }

        if (this.Page.Request.Url.OriginalString.StartsWith("https://"))
        {
            mPrefix = "https://";
        }

        lblMessage.Text = ResHelper.GetString("Message.TextInvalidWebSite") + " ";
        lblMessageUrl.Text = mPrefix + mDomain + HttpUtility.HtmlEncode(URLRewriter.CurrentURL);

        lblInfo1.Text = ResHelper.GetString("Message.InfoInvalidWebSite1") + " ";
        lnkSiteManager.Text = ResHelper.GetString("Message.LinkInvalidWebSite");
        lblInfo2.Text = " " + ResHelper.GetString("Message.InfoInvalidWebSite2") + " ";
        lblInfoDomain.Text = mDomain;

        if (LicenseHelper.CurrentLicenseInfo == null)
        {
            pnlLicense.Visible = true;
        }
    }
}
