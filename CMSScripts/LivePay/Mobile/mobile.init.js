﻿
$(function () {
    //initInputs();

    mobileInit();
    

    //SetTheZoomInCalendar();
});

function mobileInit() {
   
    resizefooter();

    $(window).resize(resizeLogo);

    ReplaceImages();
    FindInfoImg();
    iconStep();
    drp();
    $('input:checkbox').customCheckbox();
}


function ChangeIPhoneTitle(Title) {
}

function logoResize() {
    var header_height = $('#headerlogo').height();
    $('#logoeurobank').css({ "width": ((header_height - 3) * 2) + "px", "position": "absolute","top" : "0", "right": "0" });
    $('#logolivepay').css({ "width": ((header_height - 3) * 2) + "px", "position": "absolute", "top": "0", "left": "0" });
}

    function ShowTerms() {
        var popup = $('div#TextTerms');
        //popup.fadeIn("slow");

        //$('div#TextTerms').click(function () {
        $('div#TextTerms').toggle();
        //});

    }

    function buttonResize() {
      
        var width_ = $('.btn_home a').width() + 80
        $('.btn_home').css({ "padding-left": (($(window).width() - width_) / 2) + "px" }); 
        $('[id$=btnNewPayment]').css({ "zoom": "1.5", "padding-left": (($(window).width() - 900) / 2) + "px", "width": "300px" });
    }

    function resizeLogo() {
        logoResize();
        buttonResize();
    }
// Resize Footer
    function resizefooter() {
        var _windowHeight = $(window).height();
        var _Height = 0;

        if (mobileDivice.isAndroid())
            _Height = _windowHeight - 232;
        else if (mobileDivice.isIphone)
            _Height = _windowHeight - 250;

        var _content = $("div.mobile_content");
        _content.removeAttr('style');
        var _contentHeight = $("div.mobile_content").height();
        if (_Height > _contentHeight) {
            _content.css({ 'height': _Height + 'px' });
            //-moz-transform: scale(1.5); -webkit-transform: scale(1.5);
        }
        logoResize();
        buttonResize();
}


// clear inputs
function initInputs() {
    $('input:text, input:password, textarea').each(function () {
        var _input = $(this);
        if (_input.val().length && _input.val() != _input.attr('title')) return;
        _input.data('val', _input.attr('title')).removeAttr('title');
        if (!$('.form-input').length) {
            _input.val(_input.data('val'));
        }

        //        _input.bind('focus', function () {
        //            _input.parent().addClass('text_focus').parent().addClass('focus');
        //            if (_input.val() == _input.data('val')) _input.val('');
        //        }).bind('blur', function () {
        //            _input.parent().removeClass('text_focus').parent().removeClass('focus');
        //            if (_input.val() == '') _input.val(_input.data('val'));
        //        });

        //_input.unbind('blur').unbind('keyup');

        //        if (_input.is(':password')) {
        //            if (!$('.update-form').length) {
        //                var _fake = $('<input type="text" />').addClass('fakeInput').val(_input.data('val')).insertAfter(_input.val('').hide());
        //                _fake.bind('focus', function () {
        //                    _input.addClass('focus').parent().addClass('parent-focus');
        //                    _fake.hide();
        //                    _input.show().focus();
        //                });
        //                _input.bind('blur', function () {
        //                    _input.removeClass('focus').parent().removeClass('parent-focus');
        //                    if (!_input.val().length) {
        //                        _input.hide();
        //                        _fake.show();
        //                    }
        //                });
        //            }
        //        } else {
        //            _input.bind('focus', function () {
        //                _input.addClass('focus').parent().addClass('parent-focus');
        //                if (_input.val() == _input.data('val')) _input.val('');
        //            }).bind('blur', function () {
        //                _input.removeClass('focus').parent().removeClass('parent-focus');
        //                if (_input.val() == '') _input.val(_input.data('val'));
        //            });
        //        }
    });
}

// custom checkboxes module
$.fn.customCheckbox = function (_options) {
    var _options = $.extend({
        checkboxStructure: '<div class="checkboxArea"></div>',
        checkboxDisabled: 'disabled',
        checkboxDefault: 'checkboxArea',
        checkboxChecked: 'checked'
    }, _options);
    return this.each(function () {
        var checkbox = $(this);
        if (!checkbox.hasClass('outtaHere') && checkbox.is(':checkbox')) {
            var replaced = $(_options.checkboxStructure);
            this._replaced = replaced;
            if (checkbox.is(':disabled')) replaced.addClass(_options.checkboxDisabled);
            else if (checkbox.is(':checked')) replaced.addClass(_options.checkboxChecked);
            else replaced.addClass(_options.checkboxDefault);

            replaced.click(function () {
                if (checkbox.is(':checked')) checkbox.removeAttr('checked');
                else checkbox.attr('checked', 'checked');
                changeCheckbox(checkbox);
                checkbox.triggerHandler("click");
            });
            checkbox.click(function () {
                changeCheckbox(checkbox);
            });

            replaced.insertBefore(checkbox);
            checkbox.addClass('outtaHere');
        }
    });
    function changeCheckbox(_this) {
        _this.change();
        if (_this.is(':checked')) {
            _this.get(0)._replaced.addClass(_options.checkboxChecked);
            //if (_this.get(0)._replaced.parent().hasClass('chkPBPRegister'))
            //$('.pnlPBPStatus').removeClass('hide');
        }
        else {
            _this.get(0)._replaced.removeClass().addClass(_options.checkboxDefault);
            //if (_this.get(0)._replaced.parent().hasClass('chkPBPRegister'))
            //$('.pnlPBPStatus').addClass('hide');
        }
    }
}


function ReplaceImages() {
    var tab1_next = $('[id$=Tab1_BtnNext]');
    var tab2_prev = $('[id$=Tab2_btnReturn]');
    var tab2_next = $('#Tab2_btnNextCheckNewCard');
    var tab2_prev_company = $('[id$=ImageButton2]');
    var tab2_next_company = $('#Tab2_NextBtnInvoiceCheckNewCard');
    var tab3_prev = $('[id$=Tab3_btnReturn]');
    var tab3_Complete = $('[id$=Tab3_btnComplete]');


    if (tab1_next != null)
        tab1_next.attr('src', '/App_Themes/LivePay/Mobile/BtnNext.gif').css({'margin-top': '20px', 'margin-right':'10%', 'zoom' : '1.5'});

    if (tab2_prev != null)
        tab2_prev.attr('src', '/App_Themes/LivePay/Mobile/BtnPrev.gif').css({ 'zoom': '1.5' });

    if (tab2_next != null)
        tab2_next.attr('src', '/App_Themes/LivePay/Mobile/BtnNext.gif').css({ 'zoom': '1.5' }).parent().css({ 'float': 'right', 'margin-right': '10%' });

    if (tab2_prev_company != null)
        tab2_prev_company.attr('src', '/App_Themes/LivePay/Mobile/BtnPrev.gif').css({ 'zoom': '1.5' });

    if (tab2_next_company != null)
        tab2_next_company.attr('src', '/App_Themes/LivePay/Mobile/BtnNext.gif').css({ 'zoom': '1.5' }).parent().css({ 'float': 'right', 'margin-right': '10%' });
    
    if (tab3_prev != null)
        tab3_prev.attr('src', '/App_Themes/LivePay/Mobile/BtnPrev.gif').css({ 'zoom': '1.5' });

    if (tab3_Complete != null)
        tab3_Complete.attr('src', '/App_Themes/LivePay/Mobile/BtnComplete.gif').css({'zoom':'1.5'}).parent().css({'float':'right','padding-bottom':'40px'});


}

function FindInfoImg() {
    $('img[src$="InfoBtn.png"]').each(function (index) {
        img = $(this);
        img.attr('src', '/App_Themes/LivePay/mobile/InfoBtn.gif').parent().css({ 'zoom': '1.5' }).append("<div>  </div>");
    });
}

function iconStep() {
    var Steps = $('div.mobile_content').before("<div><img width='100%' id='viewstepicon'/></div>");
    var viewstepicon = $('#viewstepicon')
    if ($('#Tab1').css('display') != 'none' && $('#Tab1').css('display') != null) {
        //Steps.before("<div><img width='100%' name='step1' alt='Step 1' src='/App_Themes/LivePay/mobile/headerTab1.gif'></div>");
        //$('div.mobile_content').first().before("<div><img width='100%' name='step1' alt='Step 1' src='/App_Themes/LivePay/mobile/headerTab1.gif'></div>");
        viewstepicon.attr({
            src: "/App_Themes/LivePay/mobile/headerTab1.gif",
            title: "step 1",
            alt: "step 1"
        })
    } else if ($('#Tab2').css('display') != 'none' && $('#Tab1').css('display') != null) {
        viewstepicon.attr({
            src: "/App_Themes/LivePay/mobile/headerTab2.gif",
            title: "step 2",
            alt: "step 2"
        })
    } else if ($('#Tab3').css('display') != 'none' && $('#Tab1').css('display') != null) {
        viewstepicon.attr({
            src: "/App_Themes/LivePay/mobile/headerTab3.gif",
            title: "step 3",
            alt: "step 3"
        })
    }


}

function drp() {
    $('[id$=Tab2_DdlCardMonth]').css({ 'width': '370px' });
    $('[id$=Tab2_ddlCardType]').css({ 'width': '550px' });
    $('[id$=Tab2_DdlCardYear]').css({ 'width': '200px' });
}

function SetTheZoomInCalendar() {
    if (DetectIphoneOrIpod() == true || DetectAndroid() == true) {
        if (DetectIphoneOrIpod() == true) {
             
            //$('.ui-state-default').css("zoom", "3")
          

        } else {
           
            //$('.ui-state-default').css("zoom", "2")
        }
    }
}


function SetZoom_() {
    if (DetectIphoneOrIpod()) {
        $("input[type=text], textarea, select").mouseover(zoomDisable).mousedown(zoomEnable);
    } else {
        if (DetectAndroid()) {
           $("input[type=text], textarea, select").click(function () { ttt(event) })
           WebSettings.setBuiltInZoomControls(false);
        }
    }
}

function zoomDisable() {
    $('head meta[name=viewport]').remove();
    $('head').prepend('<meta name="viewport" content="user-scalable=0" />');
}
function zoomEnable() {

    $('head meta[name=viewport]').remove();
    $('head').prepend('<meta name="viewport" content="user-scalable=1" />');
}

function ttt(e) {
    if (!e) var e = window.event;
     $('head meta[name=viewport]').remove();
 
    

}