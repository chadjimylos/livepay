﻿function submitFormWithEnter(myfield, e) {
    var keycode;
    if (window.event) {
        keycode = window.event.keyCode;
    }
    else if (e) {
        keycode = e.which;
    }
    else {
        return true;
    }

    if (keycode == 13) {
        myfield.form.submit();
        return false;
    }
    else {
        return true;
    }
}

var W3CDOM = (document.createElement && document.getElementsByTagName);

window.onload = function initFileUploads() {
    $('.level2:not(:has(li))').css('display', 'none');

    if (!W3CDOM) return;
    var fakeFileUpload = document.createElement('div');
    fakeFileUpload.className = 'fakefile';
    fakeFileUpload.appendChild(document.createElement('input'));
    var image = document.createElement('img');
    image.src = '../../App_Themes/EurobankEquities/Images/browse.gif';
    fakeFileUpload.appendChild(image);
    var x = document.getElementsByTagName('input');
    for (var i = 0; i < x.length; i++) {
        if (x[i].type != 'file') continue;
        if (x[i].parentNode.className != 'fileinputs') continue;
        x[i].className = 'file hidden';
        var clone = fakeFileUpload.cloneNode(true);
        x[i].parentNode.appendChild(clone);
        x[i].relatedElement = clone.getElementsByTagName('input')[0];
        x[i].onchange = x[i].onmouseout = function () {
            this.relatedElement.value = this.value;
        }
    }
}


$(document).ready(function () {
    if (window.location.href.search("cms") != -1) {

        $(".DailyCommentGRLink").click(function (event) {
            event.preventDefault();

        });
    }

    $(".auto .jCarouselLite").jCarouselLite({
        auto: 1500,
        visible: 1,
        speed: 2000,
        pauseOnHover: true
    });
    $(".jCarouselLite").width(200);

    $('dl.faqOne').each(function () {
        var Container = $(this);

        var Headers = $('dt', Container);

        Headers.click(function () {
            var ActiveHeader = Container.find('dt.open');
            var CurrentHeader = $(this);
            var ActiveDescription = ActiveHeader.next('dd');
            var CurrentDescription = CurrentHeader.next('dd');
            CurrentDescription.slideDown(130, function () {
                CurrentHeader.removeClass('closed').addClass('open');
            });
            ActiveDescription.slideUp(130, function () {
                ActiveHeader.removeClass('open').addClass('closed');
            });
        });
    });

    $('ul').parent('li').addClass('down_li');

});

function setfavorite() {

    var title = 'Eurobank Equities'
    var url = 'http://eurobankequities.realize.gr/EurobankEquities';
    if (window.sidebar) // firefox
    {
        window.sidebar.addPanel(title, url, "");
    }

    else if (document.all)// ie
    {
        window.external.AddFavorite(url, title);
    }
    else {
        alert("Η προσθήκη στα αγαπημένα δεν γίνεται αυτόματα από αυτόν τον browser. Για να το προσθέσεις στα αγαπημένα σου πάτα ctl + d");
    }
}

function homepage(obj) {


    if (document.all) {
        obj.style.behavior = 'url(#default#homepage)'; obj.setHomePage('http://eurobankequities.realize.gr/EurobankEquities');
    }


    else {
        alert('Στο μενού Εργαλεία (Tools) διάλεξε Επιλογές (Options). Πήγαινε στο κυρίως τμήμα (Main tab). Στο τμήμα Εκκίνηση (Startup) πληκτρολογήστε http://www.oroskopos.gr/ στο πεδίο Αρχική σελίδα (Home Page).Από το μενού Οταν ξεκινάει το Firefox (When Firefox starts) επέλεξε Εμφάνισε την αρχική μου σελίδα (Show my home page). Πάτα OK.');
    }
}