<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectSite.ascx.cs" Inherits="CMSFormControls_Sites_SelectSite" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>
   
<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="usSites" runat="server" ObjectType="cms.site" SelectionMode="SingleTextBox" AllowEditTextBox="true" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
