<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ViewMessage.ascx.cs" Inherits="CMSModules_Messaging_Controls_ViewMessage" %>
<%@ Register Src="~/CMSModules/Messaging/Controls/MessageUserButtons.ascx" TagName="MessageUserButtons"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/UserPicture.ascx" TagName="UserPicture" TagPrefix="cms" %>
<asp:Panel ID="pnlViewMessage" runat="server" CssClass="ViewMessage" Visible="false">
    <table border="0" cellspacing="0" cellpadding="0" class="HeaderTable">
        <tr>
            <td class="ImageCell" style="vertical-align: top;">
                <cms:UserPicture ID="ucUserPicture" runat="server" Visible="false" Height="100" Width="100"
                    KeepAspectRatio="true" />
            </td>
            <td style="vertical-align: top;" class="InfoCell">
                <table>
                    <tr>
                        <td class="FieldCaption">
                            <asp:Label ID="lblFromCaption" runat="Server" EnableViewState="false" />
                        </td>
                        <td class="Field">
                            <asp:Label ID="lblFrom" runat="Server" EnableViewState="false" /><cms:RTLFix IsLiveSite="false" runat="server" ID="rtlFix" /><cms:MessageUserButtons
                                ID="ucMessageUserButtons" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldCaption">
                            <asp:Label ID="lblDateCaption" runat="Server" EnableViewState="false" />
                        </td>
                        <td class="Field">
                            <asp:Label ID="lblDate" runat="Server" EnableViewState="false" />
                        </td>
                    </tr>
                    <tr>
                        <td class="FieldCaption">
                            <asp:Label ID="lblSubjectCaption" runat="Server" EnableViewState="false" />
                        </td>
                        <td class="Field">
                            <asp:Label ID="lblSubject" runat="Server" EnableViewState="false" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="vertical-align: top;">
                <div class="Body">
                    <asp:Label ID="lblBody" runat="server" EnableViewState="false" />
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>
