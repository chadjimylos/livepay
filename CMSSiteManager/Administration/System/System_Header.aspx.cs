using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.UIControls;
using CMS.Controls;

public partial class CMSSiteManager_Administration_System_System_Header : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Pagetitle
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Administration-System.Header");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_System/module.png");
        this.CurrentMaster.Title.HelpTopicName = "general_tab10";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }
    }


    /// <summary>
    /// Initializes menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string[,] tabs = new string[6, 4];

        tabs[0, 0] = ResHelper.GetString("general.general");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab10');";
        tabs[0, 2] = "System.aspx";
        tabs[2, 0] = ResHelper.GetString("general.email");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'email_tab');";
        tabs[2, 2] = "System_Email.aspx";
        tabs[3, 0] = ResHelper.GetString("Administration-System.Files");
        tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'files_tab');";
        tabs[3, 2] = "System_Files.aspx";
        tabs[4, 0] = ResHelper.GetString("Administration-System.Deployment");
        tabs[4, 1] = ""; // "SetHelpTopic('helpTopic', 'deployment_tab');";
        tabs[4, 2] = "System_Deployment.aspx";

        // Debug tab
        tabs[5, 0] = ResHelper.GetString("Administration-System.Debug");
        tabs[5, 1] = ""; // "SetHelpTopic('helpTopic', 'debug_tab');";
        tabs[5, 2] = "Debug/System_DebugFrameset.aspx";

        this.CurrentMaster.Tabs.UrlTarget = "systemContent";
        this.CurrentMaster.Tabs.Tabs = tabs;
    }
}
