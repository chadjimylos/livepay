﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.SettingsProvider
Imports CMS.DataEngine
Imports System.Globalization
Imports System.Threading

Partial Class CMSTemplates_LivePay_XmlHttpData
    Inherits System.Web.UI.Page

    Protected ReadOnly Property Lang() As String
        Get
            Dim sLang As String = "el-gr"
            If Not String.IsNullOrEmpty(Request("lang")) Then
                sLang = Request("lang")
            End If
            Return sLang
        End Get
    End Property

   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.ContentType = "text/plain"
        Response.Write("[")
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Me.Lang)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo(Me.Lang)
        If Request("DataType") = "DistrictResutls" Then
            Dim ds_District As New DataSet
            Dim strConnection_District As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()
            Dim cn_District As GeneralConnection
            cn_District = ConnectionHelper.GetConnection(strConnection_District, 53)
            Dim parameters_country As Object(,) = New Object(0, 3) {}
            ds_District = cn_District.ExecuteQuery("Eurobank.LivePay.GetDistricts", parameters_country)

            Dim dtCountry As DataTable = ds_District.Tables(0)



            Response.Write("{""Text"": """ & ResHelper.LocalizeString("{$=Επιλέξτε Νομό|en-us=Enter District$}") & "" & """,""Value"": ""0""}")

            For i As Integer = 0 To dtCountry.Rows.Count - 1
                Dim dtRow As DataRow = dtCountry.Rows(i)
                Response.Write(",{""Text"": """ & dtRow("prefectureName").ToString & "" & """,""Value"": """ & dtRow("prefectureid").ToString & """}")
            Next
        End If
        If Request("DataType") = "AreaResults" Then
            Dim ds_District As New DataSet
            Dim strConnection_District As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()
            Dim cn_District As GeneralConnection
            cn_District = ConnectionHelper.GetConnection(strConnection_District, 53)
            Dim parameters_country As Object(,) = New Object(1, 2) {}
            parameters_country(0, 0) = "@DataType"
            parameters_country(0, 1) = "1"
            If Not String.IsNullOrEmpty(Request("DistrictID")) Then
                parameters_country(1, 0) = "@DistrictID"

                parameters_country(1, 1) = CInt(Request("DistrictID").ToString)

            End If
            ds_District = cn_District.ExecuteQuery("Eurobank.LivePay.GetDistricts", parameters_country)
            Dim dtCountry As DataTable = ds_District.Tables(0)
            Response.Write("{""Text"": """ & ResHelper.LocalizeString("{$=Επιλέξτε Περιοχή|en-us=Enter Area$}") & "" & """,""Value"": ""0""}")
            For i As Integer = 0 To dtCountry.Rows.Count - 1
                Dim dtRow As DataRow = dtCountry.Rows(i)
                Response.Write(",{""Text"": """ & dtRow(1).ToString & "" & """,""Value"": """ & dtRow(0).ToString & """}")
            Next
        End If


        Response.Write("]")
    End Sub
End Class
