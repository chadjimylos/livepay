<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectCulture.ascx.cs"
    Inherits="CMSSiteManager_Settings_Controls_SelectCulture" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" IsLiveSite="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>