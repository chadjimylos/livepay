﻿using System;
using System.Web.UI;
using System.Data;

using CMS.UIControls;
using CMS.ExtendedControls.Dialogs;
using CMS.GlobalHelper;

public partial class CMSModules_Content_Controls_Dialogs_FileSystemSelector_FileSystemView : CMSUserControl
{
    #region "Private variables"

    private FileSystemDialogConfiguration mConfig = null;
    private string mStartingPath = "";
    private string mSearchText = "";

    protected string mSaveText = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the value which determineds whtether to show the Parent button or not.
    /// </summary>
    public bool ShowParentButton
    {
        get
        {
            return this.plcParentButton.Visible;
        }
        set
        {
            this.plcParentButton.Visible = value;
        }
    }


    /// <summary>
    /// Gets or sets ID of the parent of the curently selected node.
    /// </summary>
    public string NodeParentID
    {
        get
        {
            return ValidationHelper.GetString(this.hdnLastNodeParentID.Value, "");
        }
        set
        {
            this.hdnLastNodeParentID.Value = value.ToString();
        }
    }


    /// <summary>
    /// Gets or sets text of the information label.
    /// </summary>
    public string InfoText
    {
        get
        {
            return this.innermedia.InfoText;
        }
        set
        {
            this.innermedia.InfoText = value;
        }
    }


    /// <summary>
    /// Gets current dialog configuration.
    /// </summary>
    public FileSystemDialogConfiguration Config
    {
        get
        {
            if (mConfig == null)
            {
                mConfig = new FileSystemDialogConfiguration();
            }
            return this.mConfig;
        }
        set
        {
            this.mConfig = value;
        }
    }


    /// <summary>
    /// Indicates whether the content tree is displaying more than max tree nodes.
    /// </summary>
    public bool IsDisplayMore
    {
        get
        {
            return this.innermedia.IsDisplayMore;
        }
        set
        {
            this.innermedia.IsDisplayMore = value;
        }
    }


    /// <summary>
    /// Get or sets starting path of control
    /// </summary>
    public string StartingPath
    {
        get
        {
            return mStartingPath;
        }
        set
        {
            if (value.StartsWith("~"))
            {
                mStartingPath = Server.MapPath(value);
            }
            else
            {
                mStartingPath = value;
            }
        }
    }


    /// <summary>
    /// Search text to filter data
    /// </summary>
    public string SearchText
    {
        get
        {
            mSearchText = ValidationHelper.GetString(ViewState["SearchText"], "");
            return mSearchText;
        }
        set
        {
            mSearchText = value;
            ViewState["SearchText"]=mSearchText;
        }
    }

    #endregion


    #region "Control events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // If processing the request should not continue
        if (this.StopProcessing)
        {
            this.Visible = false;
        }
        else
        {
            // Initialize controls
            SetupControls();
        }
    }


    /// <summary>
    /// Loads control's content.
    /// </summary>
    public void Reload()
    {
        // Initialize controls
        SetupControls();

        ReloadData();
    }


    /// <summary>
    /// Displays listing info message.
    /// </summary>
    /// <param name="infoMsg">Info message to display.</param>
    public void DisplayListingInfo(string infoMsg)
    {
        if (!string.IsNullOrEmpty(infoMsg))
        {
            this.plcListingInfo.Visible = true;
            this.lblListingInfo.Text = infoMsg;
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes all nested controls.
    /// </summary>
    private void SetupControls()
    {
        InitializeControlScripts();

        // Initialize inner view control
        this.innermedia.FileSystemPath = this.StartingPath;

        // Set grid definition
        string gridName = "~/CMSModules/Content/Controls/Dialogs/FileSystemSelector/FileSystemView.xml";
        this.innermedia.ListViewControl.GridName = gridName;

        // Set inner control binding columns
        this.innermedia.FileIdColumn = "path";
        this.innermedia.FileNameColumn = "name";
        this.innermedia.FileExtensionColumn = "type";
        this.innermedia.FileSizeColumn = "size";
        this.innermedia.SearchText = this.SearchText;

        // Register for inner media events
        this.innermedia.GetArgumentSet += new CMSModules_Content_Controls_Dialogs_FileSystemSelector_InnerFileSystemView.OnGetArgumentSet(innermedia_GetArgumentSet);

        // Parent directory button
        if ((this.ShowParentButton) && (!string.IsNullOrEmpty(this.NodeParentID)))
        {
            this.plcParentButton.Visible = true;
            this.imgParent.ImageUrl = "~/App_Themes/Default/Images/CMSModules/CMS_Content/Dialogs/parent.gif";
            this.mSaveText = ResHelper.GetString("dialogs.mediaview.parentfolder");
            this.btnParent.OnClientClick = "SelectNode('" + this.NodeParentID.Replace("\\", "\\\\").Replace("'", "\\'") + "');SetParentAction('" + this.NodeParentID.Replace("\\", "\\\\").Replace("'", "\\'") + "'); return false;";
        }
        this.innermedia.Configuration = this.Config;

    }


    /// <summary>
    /// Initializes scrips used by the control.
    /// </summary>
    private void InitializeControlScripts()
    {
        string script = @"
                            function SetSelectAction(argument) {
                                // Raise select action
                                SetAction('select', argument);
                                RaiseHiddenPostBack();
                            }
                            function SetParentAction(argument) {
                                // Raise select action
                                SetAction('parentselect', argument);
                                RaiseHiddenPostBack();
                            }";

        ScriptManager.RegisterStartupScript(this, this.GetType(), "DialogsSelectAction", script, true);
    }


    /// <summary>
    /// Loads data from data source property.
    /// </summary>
    private void ReloadData()
    {
        this.innermedia.Reload(true);
    }

    #endregion


    #region "Inner media view event handlers"

    /// <summary>
    /// Returns argument set according passed DataRow and flag indicating whether the set is obtained for selected item.
    /// </summary>
    /// <param name="dr">DataRow with all the item data.</param>
    /// <param name="isSelected">Indicates whether the set is required for an selected item.</param>
    string innermedia_GetArgumentSet(DataRow dr)
    {
        // Return required argument set
        return GetArgumentSet(dr);
    }

    #endregion


    #region "Helper methods"

    /// <summary>
    /// Returns argument set for the passed file data row.
    /// </summary>
    /// <param name="dr">Data row object holding all the data on current file.</param>
    private string GetArgumentSet(DataRow dr)
    {
        // Common information for both content & attachments
        string result = dr[this.innermedia.FileIdColumn].ToString().Replace("\\", "\\\\") + ";" + DataHelper.GetSizeString(ValidationHelper.GetLong(dr[this.innermedia.FileSizeColumn], 0)) + ";" + dr["isfile"].ToString();

        return result;
    }


    /// <summary>
    /// Ensures no item is selected.
    /// </summary>
    public void ResetSearch()
    {
        this.dialogSearch.ResetSearch();
        this.SearchText = "";
    }

    #endregion
}
