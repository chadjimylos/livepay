﻿var elemMinimize = document.getElementById('imgMinimize');
var elemMaximize = document.getElementById('imgMaximize');

var elemMinimizeAll = document.getElementById('imgMinimizeAll');
var elemMaximizeAll = document.getElementById('imgMaximizeAll');

var elemBorder = document.getElementById('resizerBorder');
var originalSizeElem = document.getElementById('originalSize');

var minSizes;
if (window.minSize != undefined) {
    minSizes = minSize.split(';');
}

var originalSizes;
if (window.originalSizeElem != undefined) {
    originalSizes = originalSizeElem.value.split(';');
}

function ResizerGetParent() {
    var par = window;
    var level = parentLevel;

    while (level > 0) {
        par = par.parent;
        level--;
    }

    return par;
}

function Minimize() {
    if (window.parentLevel == undefined) { return; }

    var index = 0;
    var par = ResizerGetParent();
	// Minimalize only if not minimized
	if (elemMinimize.style.display != 'none') {
		for (index = 0; index < minSizes.length; index++) {
			var minSize = minSizes[index];
			if (minSize && (minSize != '')) {
				var fs = par.document.getElementById(framesetName);
				if (fs) {
					if (resizeVertical) {
						originalSizes[index] = fs.rows;
						fs.rows = minSize;
					}
					else {
						originalSizes[index] = fs.cols;
						fs.cols = minSize;
					}
					
					elemMinimize.style.display = 'none';
					elemMaximize.style.display = 'inline';
					elemBorder.style.display = 'block';
				}
			}
			
			par = par.parent;
		}
	}
}

function Maximize() {
    if (window.parentLevel == undefined) { return; }

    var index = 0;
    var par = ResizerGetParent();
	// Maximize only if not maximized
	if (elemMaximize.style.display != 'none') {
		for (index = 0; index < minSizes.length; index++) {
			var originalSize = originalSizes[index];
			if (originalSize && (originalSize != '')) {
				var fs = par.document.getElementById(framesetName);
				if (fs) {
					if (resizeVertical) {
						fs.rows = originalSize;
					}
					else {
						if (/\%$/.test(originalSize)) {
							originalSize = originalSize.replace(/[0-9]+,/, '*,');
						}
						fs.cols = originalSize;
					}
					
					elemMinimize.style.display = 'inline';
					elemMaximize.style.display = 'none';
					elemBorder.style.display = 'none';
				}
			}
			
			par = par.parent;
		}
	}
}

function MinimizeAll(wnd) {
    if (wnd == null) {
        elemMinimizeAll.style.display = 'none';
        elemMaximizeAll.style.display = 'inline';

        wnd = top.window;
    }

    if (wnd.Minimize) {
        wnd.Minimize();
    }
    else {
        for (var i = 0; i < wnd.frames.length; i++) {
            MinimizeAll(wnd.frames[i]);
        }
    }
}

function MaximizeAll(wnd) {
    if (wnd == null) {
        elemMinimizeAll.style.display = 'inline';
        elemMaximizeAll.style.display = 'none';

        wnd = top.window;
        window.requestWindow = window;
    }

    if (wnd.Maximize) {
        wnd.Maximize();
    }
    else {
        for (var i = 0; i < wnd.frames.length; i++) {
            if (window.requestWindow != wnd) {
                MaximizeAll(wnd.frames[i]);
            }
        }
    }
}

function GetLeftButton() {
    var s = navigator.userAgent.toLowerCase() + '';
    if ((s.indexOf('gecko/') >= 0) || (s.indexOf('opera/') >= 0)) {
        return 0;
    }
    else {
        return 1;
    }
}

var lastResizeX = 0;
var currentFrameSize = 0;

var resizingFrame = false;
var oldBodyMove = null;

function StopResizeFrame(ev) {
    resizingFrame = false;
    return false;
}

function StartResizeFrame(ev) {
    if (ev == null) {
        ev = window.event;
    }

    if (ev.button == GetLeftButton()) {
        lastResizeX = ev.clientX;
        currentFrameSize = document.body.clientWidth;
   
        resizingFrame = true;
    }
    //return false;
}

function ResizeFrame(ev) {
    if (resizingFrame) {
        if (ev == null) {
            ev = window.event;
        }
        if (ev.button == GetLeftButton()) {
            var rtl = (document.body.className.indexOf('RTL') >= 0);
            var newX = ev.clientX;

            var fs = window.parent.document.getElementById(framesetName);
            if (fs) {
                var changeX = newX - lastResizeX;
                if (rtl) {
                    currentFrameSize -= changeX;
                    fs.cols = fs.cols.replace(/[^,]+$/, currentFrameSize);
                } else {
                    currentFrameSize += changeX;
                    fs.cols = fs.cols.replace(/^[^,]+/, currentFrameSize);
                }
            }

            if (!rtl) {
                lastResizeX = newX;
            }
        }
        else {
            resizingFrame = false;
        }
    }

    if (oldBodyMove != null) {
        oldBodyMove(ev);
    }
}

function InitFrameResizer(elem) {
    if (elem != null) {
        if (!elem.resizerInitialized) {
            elem.resizerInitialized = true;

            elem.onmousedown = StartResizeFrame;
            elem.onmouseup = StopResizeFrame;

            oldBodyMove = document.body.onmousemove; 
            document.body.onmousemove = ResizeFrame;
        }
    }
}
