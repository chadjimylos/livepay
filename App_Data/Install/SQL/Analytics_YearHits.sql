CREATE TABLE [Analytics_YearHits] (
		[HitsID]               int NOT NULL IDENTITY(1, 1),
		[HitsStatisticsID]     int NOT NULL,
		[HitsStartTime]        datetime NOT NULL,
		[HitsEndTime]          datetime NOT NULL,
		[HitsCount]            int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Analytics_YearHits]
	ADD
	CONSTRAINT [PK_Analytics_YearHits]
	PRIMARY KEY
	NONCLUSTERED
	([HitsID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Analytics_YearHits]') IS NOT NULL
	AND OBJECT_ID(N'[Analytics_Statistics]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Analytics_YearHits_HitsStatisticsID_Analytics_Statistics]') IS NULL
BEGIN
		ALTER TABLE [Analytics_YearHits]
			ADD CONSTRAINT [FK_Analytics_YearHits_HitsStatisticsID_Analytics_Statistics]
			FOREIGN KEY ([HitsStatisticsID]) REFERENCES [Analytics_Statistics] ([StatisticsID])
END
CREATE INDEX [IX_Analytics_WeekYearHits_HitsStatisticsID]
	ON [Analytics_YearHits] ([HitsStatisticsID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_Analytics_YearHits_HitsStartTime_HitsEndTime]
	ON [Analytics_YearHits] ([HitsStartTime] DESC, [HitsEndTime] DESC)
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
