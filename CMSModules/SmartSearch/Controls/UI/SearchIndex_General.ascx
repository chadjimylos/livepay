<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SearchIndex_General.ascx.cs"
    Inherits="CMSModules_SmartSearch_Controls_UI_SearchIndex_General" %>
<%@ Register src="SearchIndex_StopWordsCustomAnalyzer.ascx" tagname="StopCustomControl" tagprefix="cms" %>
<asp:Panel ID="pnlBody" runat="server">
    <asp:Panel ID="pnlAction" runat="server" CssClass="PageHeaderLine">
        <table cellpadding="0" cellspacing="0">
            <tbody>
                <tr>
                    <td>
                        <asp:Image ID="imgRebuild" runat="server" CssClass="NewItemImage" EnableViewState="false" />
                        <asp:LinkButton ID="btnRebuild" CssClass="NewItemLink" runat="server" OnClick="btnRebuild_Click" EnableViewState="false">LinkButton</asp:LinkButton>
                    </td>
                    <td style="width: 20px;" />
                    <td>
                        <asp:Image ID="imgOptimize" runat="server" CssClass="NewItemImage" EnableViewState="false" />
                        <asp:LinkButton ID="btnOptimize" CssClass="NewItemLink" runat="server" OnClick="btnOptimize_Click" EnableViewState="false">LinkButton</asp:LinkButton>
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <asp:Panel runat="server" ID="pnlDisabled" CssClass="DisabledInfoPanel" Visible="false">
            <cms:LocalizedLabel runat="server" ID="lblDisabled" EnableViewState="false" ResourceString="srch.searchdisabledinfo"
                CssClass="InfoLabel"></cms:LocalizedLabel>
        </asp:Panel>
        <table width="100%">
            <tr>
                <td style="width: 100%;">
                    <table>
                        <tr>
                            <td class="FieldLabel">
                                <cms:LocalizedLabel runat="server" ID="lblDisplayName" EnableViewState="false" ResourceString="general.displayname"
                                    DisplayColon="true" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                                <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" Display="Dynamic"
                                    ControlToValidate="txtDisplayName" ValidationGroup="Required" EnableViewState="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <cms:LocalizedLabel runat="server" ID="lblCodeName" EnableViewState="false" ResourceString="general.codename"
                                    DisplayColon="true" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="200" />
                                <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" Display="Dynamic" ControlToValidate="txtCodeName"
                                    ValidationGroup="Required" EnableViewState="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <cms:LocalizedLabel runat="server" ID="lblAnalyzer" EnableViewState="false" ResourceString="srch.index.analyzer"
                                    DisplayColon="true" />
                            </td>
                            <td>
                                <asp:DropDownList ID="drpAnalyzer" runat="server" CssClass="DropDownField">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <cms:StopCustomControl runat="server" ID="stopCustomControl" />
                        <tr>
                            <td class="FieldLabel">
                                <cms:LocalizedLabel runat="server" ID="lblType" EnableViewState="false" ResourceString="srch.index.type"
                                    DisplayColon="true" />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblTypeValue" EnableViewState="false"></asp:Label>
                            </td>
                        </tr>
                        <%--      
// Community indexing is not yet supported
  <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblCommunity" EnableViewState="false" ResourceString="srch.index.community"
                    DisplayColon="true" />
            </td>
            <td>
                <asp:CheckBox ID="chkCommunity" runat="server" />
            </td>
        </tr>--%>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                                    CssClass="SubmitButton" ValidationGroup="Required" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top">
                    <div style="padding-right: 30px;">
                        <asp:Panel runat="server" ID="pnlInfo">
                            <div style="padding: 10px">
                                <table cellspacing="2" cellpadding="2">
                                    <tr>
                                        <td class="FieldLabel">
                                            <cms:LocalizedLabel runat="server" ID="lblNumberOfItemsText" EnableViewState="false"
                                                ResourceString="srch.general.numberofitems" DisplayColon="true" />
                                        </td>
                                        <td class="FieldLabel">
                                            <cms:LocalizedLabel runat="server" ID="lblNumberOfItemsValue" EnableViewState="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            <cms:LocalizedLabel runat="server" ID="lblIndexFileSizeText" EnableViewState="false"
                                                ResourceString="srch.general.filesize" DisplayColon="true" />
                                        </td>
                                        <td class="FieldLabel">
                                            <cms:LocalizedLabel runat="server" ID="lblIndexFileSizeValue" EnableViewState="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            <cms:LocalizedLabel runat="server" ID="lblIndexStatusText" EnableViewState="false"
                                                ResourceString="srch.index.indexstatus" DisplayColon="true" />
                                        </td>
                                        <td class="FieldLabel">
                                            <cms:LocalizedLabel runat="server" ID="lblIndexStatusValue" EnableViewState="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            <cms:LocalizedLabel runat="server" ID="lblIndexIsOptimizedText" EnableViewState="false"
                                                ResourceString="srch.index.isoptimized" DisplayColon="true" />
                                        </td>
                                        <td class="FieldLabel">
                                            <cms:LocalizedLabel runat="server" ID="lblIndexIsOptimizedValue" EnableViewState="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="FieldLabel">
                                            <cms:LocalizedLabel runat="server" ID="lblLastRebuildTimeText" EnableViewState="false"
                                                ResourceString="srch.index.lastrebuild" DisplayColon="true" />
                                        </td>
                                        <td class="FieldLabel">
                                            <cms:LocalizedLabel runat="server" ID="lblLastRebuildTimeValue" EnableViewState="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                    </div>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>
