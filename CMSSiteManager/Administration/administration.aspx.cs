using System;
using System.IO;
using System.Data;
using System.Collections;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.LicenseProvider;
using CMS.SettingsProvider;
using CMS.SiteProvider;

public partial class CMSSiteManager_Administration_administration : SiteManagerPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Initialize page
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Header.Administration");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_Administration/module.png");

        this.guide.OnGuideItemCreated += new CMSAdminControls_UI_UIProfiles_UIGuide.GuideItemCreatedEventHandler(guide_OnGuideItemCreated);
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Hardcoded modules
        object[] row;

        row = new object[5];
        row[0] = GetImageUrl("Objects/CMS_Avatar/object.png");
        row[1] = ResHelper.GetString("Administration-LeftMenu.Avatars");
        row[2] = ResolveUrl("~/CMSModules/Avatars/Avatar_List.aspx");
        row[3] = ResHelper.GetString("cms.avatar.description");
        this.guide.GuideParameters.Add(row);

        row = new object[5];
        row[0] = GetImageUrl("Objects/CMS_Badge/object.png");
        row[1] = ResHelper.GetString("Administration-LeftMenu.Badges");
        row[2] = ResolveUrl("~/CMSModules/Badges/Badges_List.aspx");
        row[3] = ResHelper.GetString("badges.description");
        this.guide.GuideParameters.Add(row);

        row = new object[5];
        row[0] = GetImageUrl("Objects/Badwords_Word/object.png");
        row[1] = ResHelper.GetString("Administration-LeftMenu.BadWords");
        row[2] = ResolveUrl("~/CMSModules/BadWords/BadWords_List.aspx");
        row[3] = ResHelper.GetString("cms.badwords.Description");
        this.guide.GuideParameters.Add(row);

        row = new object[5];
        row[0] = GetImageUrl("CMSModules/CMS_EmailQueue/module.png");
        row[1] = ResHelper.GetString("Administration-LeftMenu.EmailQueue");
        row[2] = ResolveUrl("~/CMSModules/EmailQueue/EmailQueue_Frameset.aspx");
        row[3] = ResHelper.GetString("emailqueue.description");
        this.guide.GuideParameters.Add(row);

        row = new object[5];
        row[0] = GetImageUrl("CMSModules/CMS_RecycleBin/module.png");
        row[1] = ResHelper.GetString("Administration-LeftMenu.RecycleBin");
        row[2] = ResolveUrl("~/CMSSiteManager/Administration/RecycleBin/default.aspx");
        row[3] = ResHelper.GetString("Administration-LeftMenu.RecycleBinDescription");
        this.guide.GuideParameters.Add(row);

        row = new object[5];
        row[0] = GetImageUrl("Objects/CMS_SearchIndex/object.png");
        row[1] = ResHelper.GetString("srch.index.title");
        row[2] = ResolveUrl("~/CMSModules/SmartSearch/SearchIndex_List.aspx");
        row[3] = ResHelper.GetString("Administration-LeftMenu.SmartSearchDescription");
        this.guide.GuideParameters.Add(row);

        row = new object[5];
        row[0] = GetImageUrl("CMSModules/CMS_System/module.png");
        row[1] = ResHelper.GetString("Administration-LeftMenu.System");
        row[2] = ResolveUrl("~/CMSSiteManager/Administration/System/System_Frameset.aspx");
        row[3] = ResHelper.GetString("Administration-LeftMenu.SystemDescription");
        this.guide.GuideParameters.Add(row);

        if (LicenseHelper.IsFeautureAvailableInUI(FeatureEnum.Webfarm))
        {
            row = new object[5];
            row[0] = GetImageUrl("Objects/CMS_WebFarmServer/object.png");
            row[1] = ResHelper.GetString("Administration-LeftMenu.WebFarm");
            row[2] = ResolveUrl("~/CMSSiteManager/Administration/WebFarm/WebFarm_Frameset.aspx");
            row[3] = ResHelper.GetString("Administration-LeftMenu.WebFarmDescription");
            this.guide.GuideParameters.Add(row);
        }

        // Sort the collection
        ObjectArrayComparer comparer = new ObjectArrayComparer();
        this.guide.GuideParameters.Sort(comparer);
    }


    object[] guide_OnGuideItemCreated(UIElementInfo uiElement, object[] defaultItem)
    {
        if (!CMSAdministrationPage.IsAdministrationUIElementAvailable(uiElement.ElementName))
        {
            return null;
        }

        // Ensure default icon
        string iconUrl = GetImageUrl(ValidationHelper.GetString(defaultItem[0], ""));
        if (!ValidationHelper.IsURL(iconUrl) && !File.Exists(Server.MapPath(iconUrl)))
        {
            iconUrl = UIHelper.GetImageUrl(this.Page, "/Images/CMSModules/module.png");
        }

        // Remove siteid parameter from URL
        defaultItem[2] = UrlHelper.RemoveParameterFromUrl(defaultItem[2].ToString(), "siteid");
        return defaultItem;
    }
}
