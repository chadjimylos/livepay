<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BaseImageEditor.ascx.cs"
    Inherits="CMSAdminControls_ImageEditor_BaseImageEditor" %>

<script type="text/javascript">
    //<![CDATA[
    function Close() {
        window.close();
    }

    function PassiveRefresh() {
        if (wopener.PassiveRefresh) {
            wopener.PassiveRefresh();
            Refresh();
        }
    }

    function Refresh() {
        if (wopener.UpdatePage) {
            wopener.UpdatePage();
        }
        else {
            wopener.location.replace(wopener.location);
        }
    }

    function InitRefresh(clientId, fullRefresh, guid, action) {
        setTimeout("if (wopener.InitRefresh_" + clientId + "){ wopener.InitRefresh_" + clientId + "('', " + fullRefresh + ", '" + guid + "', '" + action + "'); } else { if(" + fullRefresh + "){ FullRefresh('" + clientId + "','" + guid + "'); } else { Refresh(); } }", 0);
    }

    // Refresh using postback
    function FullRefresh(clientId, guid) {
        setTimeout("if (wopener.FullPageRefresh_" + clientId + "){ wopener.FullPageRefresh_" + clientId + "('" + guid + "')}else{Refresh();}", 0);
    }

    // Refresh the parent
    function FolderRefresh(path, filename) {
        if (wopener.RefreshPage) {
            wopener.RefreshPage(path, 'edit', filename);
        }
    }
    //]]>
</script>

<div>
    <asp:ScriptManager ID="manScript" runat="server" />
    <table id="tblMain" class="ImageEditorTable" cellpadding="0" cellspacing="0">
        <tr>
            <td class="EditorMenuMain">
                <ajaxToolkit:Accordion ID="ajaxAccordion" runat="Server" CssClass="ImageEditorMain"
                    ContentCssClass="ImageEditorSub" HeaderCssClass="MenuHeaderItem" HeaderSelectedCssClass="MenuHeaderItemSelected">
                    <Panes>
                        <ajaxToolkit:AccordionPane ID="pnlAccordion1" runat="server">
                            <Header>
                                <div class="HeaderInner">
                                    <cms:LocalizedLabel ID="lblResize" runat="server" EnableViewState="false" ResourceString="img.resize" />
                                </div>
                            </Header>
                            <Content>
                                <cms:LocalizedLabel ID="lblValidationFailedResize" runat="server" EnableViewState="false"
                                    CssClass="ErrorLabel" ResourceString="img.errors.resize" />
                                <cms:CMSUpdatePanel ID="pnlAjax" runat="server" EnableViewState="false" UpdateMode="Always">
                                    <ContentTemplate>
                                        <table class="ImageEditorTable">
                                            <tr>
                                                <td class="LabelResize">
                                                    <cms:LocalizedRadioButton ID="radByPercentage" runat="server" GroupName="Resize"
                                                        Checked="true" AutoPostBack="true" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtResizePercent" runat="server" AutoPostBack="true" CssClass="ImageEditorTextBox"
                                                        MaxLength="3" />
                                                    %
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <cms:LocalizedRadioButton ID="radByAbsolute" runat="server" GroupName="Resize" AutoPostBack="true" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cms:LocalizedLabel ID="lblResizeWidth" runat="server" EnableViewState="false" ResourceString="img.width"
                                                        DisplayColon="true" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtResizeWidth" runat="server" Enabled="false" CssClass="ImageEditorTextBox"
                                                        MaxLength="4" />
                                                    px
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cms:LocalizedLabel ID="lblResizeHeight" runat="server" EnableViewState="false" ResourceString="img.height"
                                                        DisplayColon="true" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtResizeHeight" runat="server" Enabled="false" CssClass="ImageEditorTextBox"
                                                        MaxLength="4" />
                                                    px
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <cms:LocalizedCheckBox ID="chkMaintainRatio" runat="server" AutoPostBack="true" OnCheckedChanged="chkMaintainRatioChanged"
                                                        Enabled="false" Checked="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </cms:CMSUpdatePanel>
                                <table class="ImageEditorTable">
                                    <tr>
                                        <tr>
                                            <td class="LabelResize">
                                            </td>
                                            <td>
                                                <cms:LocalizedButton ID="btnResize" runat="server" OnClick="btnResizeClick" CssClass="SubmitButton"
                                                    ResourceString="general.ok" />
                                            </td>
                                        </tr>
                                </table>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                        <ajaxToolkit:AccordionPane ID="pnlAccordion2" runat="server">
                            <Header>
                                <div class="HeaderInner">
                                    <cms:LocalizedLabel ID="lblRotation" runat="server" EnableViewState="false" ResourceString="img.rotation" />
                                </div>
                            </Header>
                            <Content>
                                <table class="ImageEditorTable">
                                    <tr>
                                        <td class="Image">
                                            <asp:ImageButton ID="btnRotate90Left" runat="server" OnClick="btnRotate90LeftClick" />
                                        </td>
                                        <td>
                                            <cms:LocalizedLinkButton ID="lblRotate90Left" runat="server" EnableViewState="false"
                                                OnClick="btnRotate90LeftClick" ResourceString="img.rotate90left" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="Divider">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Image">
                                            <asp:ImageButton ID="btnRotate90Right" runat="server" OnClick="btnRotate90RightClick" />
                                        </td>
                                        <td>
                                            <cms:LocalizedLinkButton ID="lblRotate90Right" runat="server" EnableViewState="false"
                                                OnClick="btnRotate90RightClick" ResourceString="img.rotate90right" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="Divider">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Image">
                                            <asp:ImageButton ID="btnFlipHorizontal" runat="server" OnClick="btnFlipHorizontalClick" />
                                        </td>
                                        <td>
                                            <cms:LocalizedLinkButton ID="lblFlipHorizontal" runat="server" EnableViewState="false"
                                                OnClick="btnFlipHorizontalClick" ResourceString="img.fliphorizontal" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="Divider">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="Image">
                                            <asp:ImageButton ID="btnFlipVertical" runat="server" OnClick="btnFlipVerticalClick" />
                                        </td>
                                        <td>
                                            <cms:LocalizedLinkButton ID="lblFlipVertical" runat="server" EnableViewState="false"
                                                OnClick="btnFlipVerticalClick" ResourceString="img.flipvertical" />
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                        <ajaxToolkit:AccordionPane ID="pnlAccordion3" runat="server">
                            <Header>
                                <div class="HeaderInner">
                                    <cms:LocalizedLabel ID="lblConvert" runat="server" EnableViewState="false" ResourceString="img.convert" />
                                </div>
                            </Header>
                            <Content>
                                <cms:LocalizedLabel ID="lblQualityFailed" runat="server" EnableViewState="false"
                                    CssClass="ErrorLabel" ResourceString="img.errors.quality" />
                                <cms:CMSUpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <table class="ImageEditorTable">
                                            <tr>
                                                <td class="LabelConvert">
                                                    <cms:LocalizedLabel ID="lblFrom" runat="server" ResourceString="img.from" DisplayColon="true"
                                                        EnableViewState="false" />
                                                </td>
                                                <td>
                                                    <cms:LocalizedLabel ID="lblActualFormat" runat="server" EnableViewState="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cms:LocalizedLabel ID="lblTo" runat="server" EnableViewState="false" ResourceString="img.to"
                                                        DisplayColon="true" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="drpConvert" runat="server" AutoPostBack="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <cms:LocalizedLabel ID="lblQuality" runat="server" EnableViewState="false" ResourceString="img.quality"
                                                        DisplayColon="true" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtQuality" runat="server" Text="100" Enabled="false" CssClass="ImageEditorTextBox"
                                                        MaxLength="3" />
                                                    %
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </cms:CMSUpdatePanel>
                                <table class="ImageEditorTable">
                                    <tr>
                                        <tr>
                                            <td class="LabelConvert">
                                            </td>
                                            <td>
                                                <cms:CMSButton ID="btnConvert" runat="server" OnClick="btnConvertClick" CssClass="SubmitButton" />
                                            </td>
                                        </tr>
                                </table>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                        <ajaxToolkit:AccordionPane ID="pnlAccordion4" runat="server">
                            <Header>
                                <div class="HeaderInner">
                                    <cms:LocalizedLabel ID="lblTrim" runat="server" EnableViewState="false" ResourceString="img.trim" />
                                </div>
                            </Header>
                            <Content>
                                <cms:LocalizedLabel ID="ValidationFailedTrim" runat="server" CssClass="ErrorLabel"
                                    EnableViewState="false" ResourceString="img.errors.trim" />
                                <table class="ImageEditorTable">
                                    <tr>
                                        <td class="LabelTrim">
                                            <cms:LocalizedLabel ID="lblWidth" runat="server" EnableViewState="false" ResourceString="img.width"
                                                DisplayColon="true" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtWidthTrim" runat="server" CausesValidation="true" CssClass="ImageEditorTextBox"
                                                MaxLength="5" />
                                            px
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cms:LocalizedLabel ID="lblHeight" runat="server" EnableViewState="false" ResourceString="img.height"
                                                DisplayColon="true" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtHeightTrim" runat="server" CssClass="ImageEditorTextBox" MaxLength="5" />
                                            px
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <table id="tblTrim" class="ImageEditorInnerTable">
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="btnTopLeft" runat="server" ValidationGroup="Trim" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnTopCenter" runat="server" ValidationGroup="Trim" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnTopRight" runat="server" ValidationGroup="Trim" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="btnMiddleLeft" runat="server" ValidationGroup="Trim" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnMiddleCenter" runat="server" ValidationGroup="Trim" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnMiddleRight" runat="server" ValidationGroup="Trim" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:ImageButton ID="btnBottomLeft" runat="server" ValidationGroup="Trim" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnBottomCenter" runat="server" ValidationGroup="Trim" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton ID="btnBottomRight" runat="server" ValidationGroup="Trim" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <cms:LocalizedButton ID="btnTrim" runat="server" OnClick="btnTrimClick" CssClass="SubmitButton" ResourceString="general.ok" RenderScript="true" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="hidTrim" runat="server" />
                            </Content>
                        </ajaxToolkit:AccordionPane>
                        <ajaxToolkit:AccordionPane ID="pnlAccordion5" runat="server">
                            <Header>
                                <div class="HeaderInner">
                                    <cms:LocalizedLabel ID="lblColor" runat="server" EnableViewState="false" ResourceString="img.color" />
                                </div>
                            </Header>
                            <Content>
                                <table class="ImageEditorTable">
                                    <tr>
                                        <td class="Image">
                                            <asp:ImageButton ID="btnGrayscale" runat="server" OnClick="btnGrayscaleClick" />
                                        </td>
                                        <td>
                                            <cms:LocalizedLinkButton ID="lblBtnGrayscale" runat="server" EnableViewState="false"
                                                OnClick="btnGrayscaleClick" ResourceString="img.grayscale" />
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                        <ajaxToolkit:AccordionPane ID="pnlAccordion6" runat="server">
                            <Header>
                                <div class="HeaderInner">
                                    <cms:LocalizedLabel ID="lblProperties" runat="server" EnableViewState="false" ResourceString="img.properties" />
                                </div>
                            </Header>
                            <Content>
                                <cms:LocalizedLabel ID="lblFileNameValidationFailed" runat="server" EnableViewState="false"
                                    CssClass="ErrorLabel" ResourceString="img.errors.filename" />
                                <table id="tblProperties" class="ImageEditorTable">
                                    <tr>
                                        <td>
                                            <cms:LocalizedLabel ID="lblFileName" runat="server" EnableViewState="false" ResourceString="general.filename"
                                                DisplayColon="true" />
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFileName" runat="server" MaxLength="245" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cms:LocalizedLabel ID="lblExtensionText" runat="server" EnableViewState="false"
                                                ResourceString="img.extension" DisplayColon="true" />
                                        </td>
                                        <td>
                                            <cms:LocalizedLabel ID="lblExtensionValue" runat="server" EnableViewState="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cms:LocalizedLabel ID="lblImageSizeText" runat="server" EnableViewState="false"
                                                ResourceString="img.imagesize" DisplayColon="true" />
                                        </td>
                                        <td>
                                            <cms:LocalizedLabel ID="lblImageSizeValue" runat="server" EnableViewState="false" />
                                            kB
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cms:LocalizedLabel ID="lblWidthText" runat="server" EnableViewState="false" ResourceString="img.width"
                                                DisplayColon="true" />
                                        </td>
                                        <td>
                                            <cms:LocalizedLabel ID="lblWidthValue" runat="server" EnableViewState="false" />
                                            px
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cms:LocalizedLabel ID="lblHeightText" runat="server" EnableViewState="false" ResourceString="img.height"
                                                DisplayColon="true" />
                                        </td>
                                        <td>
                                            <cms:LocalizedLabel ID="lblHeightValue" runat="server" EnableViewState="false" />
                                            px
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>
                                            <cms:CMSButton ID="btnChangeFileName" runat="server" OnClick="btnChangeFileNameClick"
                                                CssClass="SubmitButton" />
                                        </td>
                                    </tr>
                                </table>
                            </Content>
                        </ajaxToolkit:AccordionPane>
                    </Panes>
                </ajaxToolkit:Accordion>
                <div class="buttonClose">
                    <cms:LocalizedLabel ID="lblLoadFailed" runat="server" CssClass="ErrorLabel" />
                </div>
            </td>
            <td>
                <iframe id="frame1" scrolling="auto" runat="server" frameborder="0" width="99%" height="550px"
                    class="ImageEditorFrame" />
                <asp:Image ID="imgMain" runat="server" Visible="false" />
            </td>
        </tr>
    </table>
</div>
<asp:Literal ID="ltlScript" runat="server" />