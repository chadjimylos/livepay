<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MultipleCategoriesSelector.ascx.cs" Inherits="CMSModules_Membership_FormControls_Users_MultipleCategoriesSelector" %>
<%@ Register Src="~/CMSAdminControls/Categories/MultipleCategoriesSelector.ascx" TagName="MultipleCategoriesSelector"
    TagPrefix="cms" %>
<div style="border: 1px solid #cccccc; padding: 5px;">
    <cms:MultipleCategoriesSelector ID="categorySelector" runat="server" FormControlMode="true" AdministrationMode="false" />
</div>
