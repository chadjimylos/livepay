using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SiteProvider;
using CMS.LicenseProvider;
using CMS.SettingsProvider;

public partial class CMSModules_Reporting_Tools_Ecommerce_Reports_Tree : CMSEcommercePage
{
    string[,] reports = { {"{$Ecommerce.NumberOfOrdersPerYear_caption$}", "EcommNumberOfOrdersPerYear", "Objects/Ecommerce_Order/list.png"},
                          {"{$Ecommerce.NumberOfOrdersPerMonth_caption$}", "EcommNumberOfOrdersPerMonth", "Objects/Ecommerce_Order/list.png"},
                          {"{$Ecommerce.NumberOfOrdersPerDay_caption$}", "EcommNumberOfOrdersPerDay", "Objects/Ecommerce_Order/list.png"},
                          {"{$Ecommerce.SalesPerYear_caption$}", "EcommSalesPerYear", "CMSModules/CMS_Ecommerce/sales.png"},
                          {"{$Ecommerce.SalesPerMonth_caption$}", "EcommSalesPerMonth", "CMSModules/CMS_Ecommerce/sales.png"},
                          {"{$Ecommerce.SalesPerDay_caption$}", "EcommSalesPerDay", "CMSModules/CMS_Ecommerce/sales.png"},
                          {"{$Ecommerce.Inventory_caption$}", "EcommInventory", "Objects/Ecommerce_SKU/list.png"},
                          {"{$Ecommerce.Top100CustByVolOfSales_caption$}","EcommTop100CustByVolOfSales", "Objects/Ecommerce_Customer/list.png"}
                         };


    protected void Page_Load(object sender, EventArgs e)
    {
        // Check the license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), "") != "")
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Reporting);
        }

        // Check site availability
        if (!ResourceSiteInfoProvider.IsResourceOnSite("CMS.Reporting", CMSContext.CurrentSiteName))
        {
            RedirectToResourceNotAvailableOnSite("CMS.Reporting");
        }

        CurrentUserInfo user = CMSContext.CurrentUser;

        // Check 'Read' permission
        if (!user.IsAuthorizedPerResource("CMS.Reporting", "Read"))
        {
            RedirectToAccessDenied("CMS.Reporting", "Read");
        }

        treeStats.LineImagesFolder = GetImageUrl("Design/Controls/Tree");

        // Clear all nodes
        this.treeStats.Nodes.Clear();

        // Create root node
        TreeNode rootNode = new TreeNode(ResHelper.GetString("Ecommerce_Reports.Root"));
        rootNode.Expanded = true;
        rootNode.NavigateUrl = "#";

        treeStats.Nodes.Add(rootNode);

        for (int i = 0; i < reports.GetLength(0); i++)
        {
            TreeNode childNode = new TreeNode();
            childNode.Text = "<span class=\"ContentTreeItem\" onclick=\"ShowDesktopContent('Reports.aspx?reportCodeName=" +
                reports[i, 1] + "', this);\"><span class=\"Name\">" +
                           ResHelper.LocalizeString(reports[i, 0]) + "</span></span>";
            childNode.NavigateUrl = "#";
            childNode.ImageUrl = GetImageUrl(reports[i, 2]);
            rootNode.ChildNodes.Add(childNode);
        }
    }
}
