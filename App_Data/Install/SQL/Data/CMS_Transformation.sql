SET IDENTITY_INSERT [CMS_Transformation] ON
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'AtomItem', N'Ascx', 1115, N'<entry>
  <title><![CDATA[<%# Eval("DocumentName", true) %>]]></title>
  <link href="<%# GetAbsoluteUrl(GetDocumentUrlForFeed(), Eval("SiteName")) %>" />
  <id>urn:uuid:<%# Eval("NodeGUID") %></id>
  <published><%# GetAtomDateTime(Eval("DocumentCreatedWhen")) %></published>
  <updated><%# GetAtomDateTime(Eval("DocumentModifiedWhen")) %></updated>
  <author>
    <name><%# Eval("NodeOwnerFullName") %></name>
  </author>
  <summary><![CDATA[<%# Eval("NodeAliasPath",true) %>]]></summary>
</entry>', '20100309 17:14:42', 'ce31d65b-f97e-4967-824c-c65bb10543ad', NULL, 1095, N'559b6901-48f4-4939-b2c4-43512db501bf', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'Attachment', N'Ascx', 676, N'<%@ Register TagPrefix="cc1" Namespace="CMS.GlobalHelper" Assembly="CMS.GlobalHelper" %>
<div>
<a target="_blank" href="<%# GetAttachmentUrl(Eval("AttachmentName"), Eval("NodeAliasPath")) %>">
<img style="border: none;" src="<%# IfCompare(ImageHelper.IsImage((string)Eval("AttachmentExtension")), true, GetAttachmentIconUrl(Eval("AttachmentExtension"), null), GetAttachmentUrl(Eval("AttachmentName"), Eval("NodeAliasPath"))) %>?maxsidesize=150" alt="<%# Eval("AttachmentName", true) %>" />
</a>
<%# IfCompare(ImageHelper.IsImage((string)Eval("AttachmentExtension")), true, "<br />" + ResHelper.GetString("attach.openfile"), "") %>
<br />
<%# Eval("AttachmentName") %>
<br />
</div>', '20090612 12:57:57', '6eba21c8-7c1a-49a6-937e-974627c9224b', NULL, 1095, N'43b754e8-9a72-4f50-baf1-5ba6c6b31098', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'AttachmentLightbox', N'Ascx', 713, N'<%@ Register TagPrefix="cc1" Namespace="CMS.GlobalHelper" Assembly="CMS.GlobalHelper" %>
<a style="text-decoration: none;" href="<%# GetAttachmentUrl(Eval("AttachmentName"), Eval("NodeAliasPath")) %>" rel="lightbox[attachments]" rev="<%# Eval("AttachmentID") %>" title="<%# Eval("AttachmentName", true) %>">
<img style="border: none;" src="<%# IfCompare(ImageHelper.IsImage((string)Eval("AttachmentExtension")), true, GetAttachmentIconUrl(Eval("AttachmentExtension"), null), GetAttachmentUrl(Eval("AttachmentName"), Eval("NodeAliasPath"))) %>?maxsidesize=150" alt="<%# Eval("AttachmentName", true) %>" />
</a>', '20100420 13:38:17', 'f0460e74-2509-4b1e-8665-5e68cf947e86', NULL, 1095, N'5a7b7239-1efb-42b2-b1af-9403fff18270', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'AttachmentLightboxDetail', N'Ascx', 714, N'<%@ Register TagPrefix="cc1" Namespace="CMS.GlobalHelper" Assembly="CMS.GlobalHelper" %>
<%# IfCompare(ImageHelper.IsImage((string)Eval("AttachmentExtension")), true,
"<div style=\"text-align:center;width: 350px;\"><div style=\"font-size: 11px;line-height: 12px;position:relative;z-index:1000;margin:auto;width:140px;\"><a target=\"_blank\" href=\"" + GetAttachmentUrl(Eval("AttachmentName"), Eval("NodeAliasPath")) + "\"><img style=\"border: none;\" src=\"" + GetAttachmentIconUrl(Eval("AttachmentExtension"), null) + "\" alt=\"" + Eval("AttachmentName") + "\" /></a><p>" + ResHelper.GetString("attach.openfile") + "</p></div></div>",
"<img src=\"" + GetAttachmentUrl(Eval("AttachmentName"), Eval("NodeAliasPath")) + "?maxsidesize=1000\" alt=\"" + Eval("AttachmentName", true) + "\" />") %>', '20100420 13:34:17', 'fdad8271-1293-4444-9151-e74af81dcda6', NULL, 1095, N'cc44709f-8f26-4816-8132-8c921907bb65', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'AttachmentList', N'Ascx', 841, N'<div>
<img src="<%# GetAttachmentIconUrl(Eval("AttachmentExtension"), "List") %>" alt="<%# Eval("AttachmentName") %>" />
&nbsp;
<a target="_blank" href="<%# GetAttachmentUrl(Eval("AttachmentName"), Eval("NodeAliasPath")) %>">
<%# Eval("AttachmentName") %>
</a>
</div>', '20090612 12:01:19', '4455961a-e81f-419b-8a68-19d1cdcef2a1', NULL, 1095, N'9f22c355-e78f-4d46-85cd-1c1afc465aec', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'CategoryList', N'Ascx', 415, N'<asp:HyperLink ID="lnkCategoryList" runat="server" EnableViewState="false"></asp:HyperLink><br />', '20080929 12:53:32', '4b6c6f44-ac26-45c1-920f-df48f92e94d0', NULL, 1095, N'14958877-9799-476e-b74e-7c61124095de', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'CMSDeskSmartSearchResults', N'Ascx', 915, N'<div style="margin-bottom: 30px;">
	<%-- Search result image --%>
        <div style="border: solid 1px #eeeeee; width: 90px; height:90px; margin-right: 5px;" class="LeftAlign">
           <img src="<%# GetSearchImageUrl(UIHelper.GetImageUrl(this.Page, "CMSModules/CMS_SmartSearch/no_image.gif"),90) %>" alt="" />
        </div>
        <div class="LeftAlign">
            <%-- Search result title --%>
            <div style="text-align: left;">
                <a style="font-weight: bold" href=''<%# "javascript:SelectItem(" + ValidationHelper.GetString(GetSearchValue("nodeId"), "") + ", \""+ ValidationHelper.GetString(GetSearchValue("DocumentCulture"), "") + "\")" %>''>
                    <%# SearchHighlight(HTMLHelper.HTMLEncode(DataHelper.GetNotEmpty(Eval("Title"), "/")),"<span style=\"font-weight:bold;\">","</span>") %> (<%#ValidationHelper.GetString(GetSearchValue("DocumentCulture"), "")%>)
                </a>
            </div>
            <%-- Search result content --%>
            <div style="margin-top: 5px; width: 590px;min-height:40px">
                <%# SearchHighlight(HTMLHelper.HTMLEncode(TextHelper.LimitLength(HttpUtility.HtmlDecode(HTMLHelper.StripTags(GetSearchedContent(DataHelper.GetNotEmpty(Eval("Content"), "")), false, " ")), 280, "...")),"<span style=\"background-color: #FEFF8F\">","</span>") %><br />
            </div>
            <%-- Relevance, URL, Creattion --%>
            <div style="margin-top: 5px;">
                <%-- Relevance --%>
                <div title="<%# "Relevance: " + Convert.ToInt32(ValidationHelper.GetDouble(Eval("Score"),0.0)*100)  + "%" %>"
                    style="width: 50px; border: solid 1px #aaaaaa; margin-top: 7px; margin-right: 6px;
                    float: left; color: #0000ff; font-size: 2pt; line-height: 4px; height: 4px;">
                    <div style="<%# "background-color:#a7d3a7;width:"+ Convert.ToString(Convert.ToInt32((ValidationHelper.GetDouble(Eval("Score"),0.0)/2)*100))  + "px;height:4px;line-height: 4px;"%>">
                    </div>
                </div>
                <%-- URL --%>
                <span style="color: #008000">
                    <%# TextHelper.BreakLine(SearchHighlight(SearchResultUrl(true),"<strong>","</strong>"),75,"<br />") %>
                </span>
                <%-- Creation --%>
                <span style="padding-left:5px;;color: #888888; font-size: 9pt">
                    <%# GetDateTimeString(ValidationHelper.GetDateTime(Eval("Created"), DateTimeHelper.ZERO_TIME), true) %>
                </span>
            </div>
        </div>
        <div style="clear: both">
        </div>
    </div>', '20100510 12:59:26', 'aadecc4c-1909-4804-b0bb-990c093fc4e0', NULL, 1095, N'8eaacc23-a2c7-44ed-803a-47368cbfeec7', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'CMSDeskSQLSearchResults', N'Ascx', 916, N'<div style="margin-bottom: 30px;">
	<%-- Search result image --%>
        <div style="margin-right: 5px;" class="LeftAlign">
           <img src="<%# UIHelper.GetDocumentTypeIconUrl(this.Page, ValidationHelper.GetString(DataBinder.Eval(((System.Web.UI.WebControls.RepeaterItem)(Container)).DataItem, "ClassName"), "")) %>" alt="" />
        </div>
        <div class="LeftAlign">
            <%-- Search result title --%>
            <div>
    		<a style="font-weight: bold" href="<%# "javascript:SelectItem(" + Eval("NodeID") + ", \''" + Eval("DocumentCulture") + "\'')" %>"><%# IfEmpty(Eval("NodeName"), "/", HTMLHelper.HTMLEncode(ValidationHelper.GetString(Eval("NodeName"), null))) %> (<%# Eval("DocumentCulture") %>)</a>
            </div>
	<div style="margin-top: 5px;">
<%-- URL --%>
                <span style="color: #008000">
                    <%#  GetAbsoluteUrl(GetDocumentUrl()) %>
                </span>
                <%-- Creation --%>
                <span style="padding-left:5px;;color: #888888; font-size: 9pt">
                    <%# GetDateTimeString(ValidationHelper.GetDateTime(Eval("DocumentCreatedWhen"), DateTimeHelper.ZERO_TIME), true) %>
                </span>
  	    </div>
	</div>
<div style="clear: both">
</div>
</div>', '20100510 13:16:23', '914377c1-21ad-443e-956f-0b9646e5520b', NULL, 1095, N'44f1c179-1c22-4479-bf3e-3af477149ff8', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'GoogleSiteMap', N'Ascx', 668, N'<url>
<loc><%# GetAbsoluteUrl(GetDocumentUrl()) %></loc>
<lastmod><%# GetDateTime("DocumentModifiedWhen", "yyyy-MM-dd") %></lastmod>
</url>', '20090105 12:12:09', '8d5a6991-b2bf-436d-ba82-5159eb98fc71', NULL, 1095, N'a35a06a0-3b27-4e7a-af56-0afa011ba5bb', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'Newsletter_Archive', N'Ascx', 153, N'<%#  FormatDateTime(Eval("IssueMailoutTime"),"d") %> - <a href="~/CMSModules/Newsletters/CMSPages/GetNewsletterIssue.aspx?issueId=<%# Eval("IssueID")%>" target="_blank"><%# Eval("IssueSubject") %></a> <br />', '20081105 14:09:42', '62116172-2118-4676-98eb-373c79fa6cc6', NULL, 1095, N'66200e87-f563-49f1-ad73-35cb5824e7cf', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'OnLineUsers', N'Ascx', 559, N'<%# Eval("UserName", true) %>&nbsp;', '20090122 21:35:31', '3b16b39e-3020-485d-8bae-6e7617e13894', NULL, 1095, N'a24c061e-54c0-4f8d-8572-62edf6373b22', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'Print', N'Ascx', 283, N'<h3>Print transformation is missing</h3>
<div>Transformation of current document type is missing. You have to define the tranformation in the CMS Site Manager Development section.</div>', '20080421 16:10:12', 'dfba986c-38de-431c-89df-8ad3e8c7b451', NULL, 1095, N'e0b0c999-89a8-4d31-8d35-29dcb4df7693', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'RelatedDocuments', N'Ascx', 15, N'<strong><a href="<%# ResolveUrl(GetUrl( Eval("NodeAliasPath"), null)) %>">
<%# Eval("DocumentName") %></a></strong>
<br />', '20081010 11:57:13', 'b96f0a40-8fb6-4ed5-8eaa-309867a18283', NULL, 1095, N'51048ab0-3dfb-4c20-8be5-fe71e00b1edf', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'RSSItem', N'Ascx', 1019, N'<item>
     <guid isPermaLink="false"><%# Eval("NodeGUID") %></guid>
     <title><![CDATA[<%# Eval("DocumentName", true) %>]]></title>
     <description><![CDATA[<%# Eval("NodeAliasPath",true) %>]]></description>
     <pubDate><%# GetRSSDateTime(Eval("DocumentCreatedWhen")) %></pubDate>
     <link><![CDATA[<%# GetAbsoluteUrl(GetDocumentUrlForFeed(), Eval("SiteName")) %>]]></link>     	
</item>', '20100305 10:24:31', 'e38315ed-fcef-46a9-b82a-b33d6902649b', NULL, 1095, N'f1347f18-5849-4c31-92b8-fa6d78e2658b', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'SearchResults', N'Ascx', 20, N'<div class="SearchResult">
  <div class="ResultTitle">
    <a href="<%# GetDocumentUrl()%>"><%# IfEmpty(Eval("SearchResultName",true), "/", Eval("SearchResultName",true)) %></a>
  </div>
  <div class="ResultPath">
    Path: <%# Eval("DocumentNamePath",true) %><br />
  </div>
</div>', '20081205 13:57:06', 'a3c543c8-b855-4acd-9c03-9302a02f8f74', NULL, 1095, N'fc5f0777-f3b9-422b-b0b8-cb98a6a04cc0', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'SmartSearchResults', N'Ascx', 801, N'<div style="margin-bottom: 30px;">
        <%-- Search result title --%>
        <div>
            <a style="font-weight: bold" href=''<%# SearchResultUrl(true) %>''>
                <%# SearchHighlight(HTMLHelper.HTMLEncode(DataHelper.GetNotEmpty(Eval("Title"), "/")),"<span style=\"font-weight:bold;\">","</span>") %>
            </a>
        </div>
        <%-- Search result content --%>
        <div style="margin-top: 5px; width: 590px;">
            <%# SearchHighlight(HTMLHelper.HTMLEncode(TextHelper.LimitLength(HttpUtility.HtmlDecode(HTMLHelper.StripTags(GetSearchedContent(DataHelper.GetNotEmpty(Eval("Content"), "")), false, " ")), 280, "...")),"<span style=\"background-color: #FEFF8F\">","</span>") %><br />
        </div>
        <%-- Relevance, URL, Creattion --%>
        <div style="margin-top: 5px;">
            <%-- Relevance --%>
            <div title="<%# "Relevance: " + Convert.ToInt32(ValidationHelper.GetDouble(Eval("Score"),0.0)*100)  + "%" %>"
                style="width: 50px; border: solid 1px #aaaaaa; margin-top: 7px; margin-right: 6px; float: left; color: #0000ff; font-size: 2pt; line-height: 4px; height: 4px;">
                <div style=''<%# "background-color:#a7d3a7;width:"+ Convert.ToString(Convert.ToInt32((ValidationHelper.GetDouble(Eval("Score"),0.0)/2)*100))  + "px;height:4px;line-height: 4px;"%>''>
                </div>
            </div>
            <%-- URL --%>
            <span style="color: #008000">
                <%# SearchHighlight(SearchResultUrl(true),"<strong>","</strong>")%>
            </span>
            <%-- Creation --%>
            <span style="color: #888888; font-size: 9pt">
                <%# GetDateTimeString(ValidationHelper.GetDateTime(Eval("Created"), DateTimeHelper.ZERO_TIME), true) %>
            </span>
        </div>
    </div>', '20091127 10:34:22', '63b96721-59ae-4462-a842-10fedfc282e1', NULL, 1095, N'b1da4b77-ea43-4143-9135-cabe8709b396', N'')
INSERT INTO [CMS_Transformation] ([TransformationCheckedOutFilename], [TransformationName], [TransformationType], [TransformationID], [TransformationCode], [TransformationLastModified], [TransformationGUID], [TransformationCheckedOutByUserID], [TransformationClassID], [TransformationVersionGUID], [TransformationCheckedOutMachineName]) VALUES (N'', N'SmartSearchResultsWithImages', N'Ascx', 846, N'<div style="margin-bottom: 30px;">
	<%-- Search result image --%>
        <div style="float: left; border: solid 1px #eeeeee; width: 90px; height:90px; margin-right: 5px;">
           <img src="<%# GetSearchImageUrl("~/App_Themes/Default/Images/CMSModules/CMS_SmartSearch/no_image.gif",90) %>" alt="" />
        </div>
        <div style="float: left">
            <%-- Search result title --%>
            <div>
                <a style="font-weight: bold" href=''<%# SearchResultUrl(true) %>''>
                    <%# SearchHighlight(HTMLHelper.HTMLEncode(DataHelper.GetNotEmpty(Eval("Title"), "/")),"<span style=\"font-weight:bold;\">","</span>") %>
                </a>
            </div>
            <%-- Search result content --%>
            <div style="margin-top: 5px; width: 590px;min-height:40px">
                <%# SearchHighlight(HTMLHelper.HTMLEncode(TextHelper.LimitLength(HttpUtility.HtmlDecode(HTMLHelper.StripTags(GetSearchedContent(DataHelper.GetNotEmpty(Eval("Content"), "")), false, " ")), 280, "...")),"<span style=\"background-color: #FEFF8F\">","</span>") %><br />
            </div>
            <%-- Relevance, URL, Creattion --%>
            <div style="margin-top: 5px;">
                <%-- Relevance --%>
                <div title="<%# "Relevance: " + Convert.ToInt32(ValidationHelper.GetDouble(Eval("Score"),0.0)*100)  + "%" %>"
                    style="width: 50px; border: solid 1px #aaaaaa; margin-top: 7px; margin-right: 6px;
                    float: left; color: #0000ff; font-size: 2pt; line-height: 4px; height: 4px;">
                    <div style="<%# "background-color:#a7d3a7;width:"+ Convert.ToString(Convert.ToInt32((ValidationHelper.GetDouble(Eval("Score"),0.0)/2)*100))  + "px;height:4px;line-height: 4px;"%>">
                    </div>
                </div>
                <%-- URL --%>
                <span style="color: #008000">
                    <%# TextHelper.BreakLine(SearchHighlight(SearchResultUrl(true),"<strong>","</strong>"),75,"<br />") %>
                </span>
                <%-- Creation --%>
                <span style="padding-left:5px;;color: #888888; font-size: 9pt">
                    <%# GetDateTimeString(ValidationHelper.GetDateTime(Eval("Created"), DateTimeHelper.ZERO_TIME), true) %>
                </span>
            </div>
        </div>
        <div style="clear: both">
        </div>
    </div>', '20091222 11:29:46', 'fa253f7c-6705-4995-bb27-96c0042dee5b', NULL, 1095, N'b9abeed8-ae15-4e27-8927-d4646f64fbbc', N'')
SET IDENTITY_INSERT [CMS_Transformation] OFF
