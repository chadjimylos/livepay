<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Customer_Edit_General.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Customers_Customer_Edit_General" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Theme="Default" Title="Customer properties" %>

<%@ Register Src="~/CMSModules/ECommerce/FormControls/PaymentSelector.ascx" TagName="PaymentSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/ECommerce/FormControls/ShippingSelector.ascx" TagName="ShippingSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/ECommerce/FormControls/CurrencySelector.ascx" TagName="CurrencySelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/CountrySelector.ascx" TagName="CountrySelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/ECommerce/FormControls/DiscountLevelSelector.ascx"
    TagName="DiscountLevelSelector" TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerFirstName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCustomerFirstName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerLastName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCustomerLastName" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerCompany" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCustomerCompany" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblOrganizationID" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtOraganizationID" runat="server" CssClass="TextBoxField" MaxLength="50"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblTaxRegistrationID" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtTaxRegistrationID" runat="server" CssClass="TextBoxField" MaxLength="50"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblCustomerEmail" DisplayColon="true" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCustomerEmail" runat="server" CssClass="TextBoxField" MaxLength="200"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerPhone" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCustomerPhone" runat="server" CssClass="TextBoxField" MaxLength="50"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblCustomerFax" DisplayColon="true" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtCustomerFax" runat="server" CssClass="TextBoxField" MaxLength="50"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerPreferredCurrency" EnableViewState="false" />
            </td>
            <td>
                <cms:CurrencySelector ID="drpCurrency" runat="server" AddNoneRecord="true" IsLiveSite="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerPrefferedPaymentOption" EnableViewState="false" />
            </td>
            <td>
                <cms:PaymentSelector ID="drpPayment" runat="server" AddNoneRecord="true" IsLiveSite="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerPreferredShippingOption" EnableViewState="false" />
            </td>
            <td>
                <cms:ShippingSelector ID="drpShipping" runat="server" AddNoneRecord="true" IsLiveSite="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel" style="vertical-align: top; padding-top: 6px">
                <asp:Label runat="server" ID="lblCustomerCountry" EnableViewState="false" />
            </td>
            <td>
                <cms:CountrySelector ID="drpCountry" runat="server" AddNoneRecord="true" UseCodeNameForSelection="false"
                    AddSelectCountryRecord="false" IsLiveSite="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblCustomerEnabled" EnableViewState="false"
                    ResourceString="general.enabled" DisplayColon="true" />
            </td>
            <td>
                <asp:CheckBox ID="chkCustomerEnabled" runat="server" CssClass="CheckBoxMovedLeft"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblCustomerDiscountLevel" EnableViewState="false" />
            </td>
            <td>
                <cms:DiscountLevelSelector ID="drpDiscountLevel" runat="server" AddNoneRecord="true"
                    UseCodeNameForSelection="false" IsLiveSite="false" />
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                    CssClass="SubmitButton" />
            </td>
        </tr>
    </table>
</asp:Content>
