<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BoardSubscriptions.ascx.cs"
    Inherits="CMSModules_MessageBoards_Controls_Boards_BoardSubscriptions" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<cms:UniGrid ID="boardSubscriptions" runat="server" GridName="~/CMSModules/MessageBoards/Tools/Boards/BoardSubscriptions.xml"
    Columns="SubscriptionID, SubscriptionEmail, UserName" OrderBy="SubscriptionEmail" />
