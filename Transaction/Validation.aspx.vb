Option Strict Off
Imports System.Data
Imports System.Data.SqlClient
Partial Class VALIDATION
    Inherits Web.UI.Page

    Dim theRef As String = ""

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        theRef = Request("Ref")
        Try
            AegeanPower_DBConnection.TransactionLogAdd(theRef, "Validation Initiated")

            Dim theShop As String = Request("Shop")

            Dim theAmount As String = Request("Amount").Replace(".", ",")
            Dim theCurrency As String = Request("Currency")


            If theShop <> "" And theRef <> "" And theAmount <> "" And theCurrency <> "" Then
                If CInt(theCurrency) <> 978 Then
                    okOrBad.Text = "[BAD]"
                    AegeanPower_DBConnection.TransactionLogAdd(theRef, "Validation Error: Invalid Currency sent by APACS(" & theCurrency & ")")
                Else
                    Dim correctAmount As Double = GetAmountByMerchantRef(theRef)
                    theAmount = CDbl(theAmount)
                    If IsDBNull(correctAmount) Then
                        okOrBad.Text = "[BAD]"
                        AegeanPower_DBConnection.TransactionLogAdd(theRef, "Validation Error: No such RefID located in SHOP_Validation(" & theRef & ")")
                    Else
                        If theAmount <> correctAmount Then
                            okOrBad.Text = "[BAD]"
                            AegeanPower_DBConnection.TransactionLogAdd(theRef, "Validation Error: DB Amount and APACS Amount do not match. (" & theAmount & ", " & correctAmount & ",MerchantRef:" & theRef & ")")
                        Else
                            okOrBad.Text = "[OK]"
                            AegeanPower_DBConnection.TransactionLogAdd(theRef, "Validation OK.")
                        End If
                    End If
                End If
            Else
                okOrBad.Text = "[BAD]"
                AegeanPower_DBConnection.TransactionLogAdd(theRef, "Validation Error: Invalid QueryString sent by APACS (" & Request.QueryString.ToString & ").")
            End If
        Catch ex As Exception
            okOrBad.Text = "[BAD]"
            AegeanPower_DBConnection.TransactionLogAdd(theRef, "Validation Exception: " & ex.ToString)
        End Try
    End Sub

    Function GetAmountByMerchantRef(ByVal MerchantRef As String) As String
        Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()

        Dim objConnection As New SqlConnection(strConnection)
        objConnection.Open()

        Dim objCommand As New SqlCommand("AegeanPower_GetAmountByMerchantRef", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@MerchantRef", MerchantRef)

        Dim amount As String = objCommand.ExecuteScalar
        objConnection.Close()
        Return amount
    End Function

End Class
