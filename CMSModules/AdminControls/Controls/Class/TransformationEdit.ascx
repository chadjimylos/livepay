<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TransformationEdit.ascx.cs"
    Inherits="CMSModules_AdminControls_Controls_Class_TransformationEdit" %>
<asp:Panel ID="pnlCheckOutInfo" runat="server" CssClass="InfoLabel" EnableViewState="false">
    <asp:Label runat="server" ID="lblCheckOutInfo" />
</asp:Panel>
<br />
<asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
    Visible="false" />
<table style="width: 100%">
    <tr>
        <td class="FieldLabel" style="width: 15%">
            <asp:Label runat="server" ID="lblTransformationName" />
        </td>
        <td colspan="2">
            <asp:TextBox runat="server" ID="tbTransformationName" CssClass="TextBoxField" MaxLength="100" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidatorTransformationName" runat="server"
                ControlToValidate="tbTransformationName" Display="dynamic" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblTransformationType" EnableViewState="false" />
        </td>
        <td colspan="2">
            <asp:DropDownList runat="server" ID="drpTransformationType" AutoPostBack="true" OnSelectedIndexChanged="drpTransformationType_SelectedIndexChanged"
                EnableViewState="false" CssClass="ShortDropDownList" />
        </td>
    </tr>
    <tr>
        <td style="vertical-align: text-top" class="FieldLabel">
            <asp:Label runat="server" ID="lblTransformationCode" EnableViewState="false" />
        </td>
        <td colspan="2">
            <div style="width=100%">
            <asp:PlaceHolder ID="plcTransformationCode" runat="server">
                <asp:DropDownList runat="server" ID="drpTransformationCode" AutoPostBack="false"
                    CssClass="ShortDropDownList" EnableViewState="false" />
            </asp:PlaceHolder>
            <cms:CMSButton runat="server" ID="btnDefaultTransformation" OnClick="btnDefaultTransformation_Click"
                CausesValidation="false" EnableViewState="false" CssClass="XXLongButton" />
            </div>
            <asp:Literal runat="server" ID="ltlDirectives" EnableViewState="false" />
            <asp:PlaceHolder runat="server" ID="plcVirtualInfo" Visible="false">
                <br />
                <asp:Label runat="server" ID="lblVirtualInfo" CssClass="ErrorLabel" EnableViewState="false" />
            </asp:PlaceHolder>
            <cms:ExtendedTextArea runat="server" ID="tbTransformationText" TextMode="multiline"
                CssClass="TextAreaHigh" Width="99%" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td style="width: 100%">
            <cms:LocalizedHyperlink ID="lnkHelp" runat="server" EnableViewState="false" />
        </td>
    </tr>
</table>
