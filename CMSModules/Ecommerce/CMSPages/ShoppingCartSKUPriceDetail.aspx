<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShoppingCartSKUPriceDetail.aspx.cs"
    Inherits="CMSModules_Ecommerce_CMSPages_ShoppingCartSKUPriceDetail" Theme="Default"
    MasterPageFile="~/CMSMasterPages/LiveSite/Dialogs/ModalDialogPage.master" Title="Shopping cart - Product price detail" %>

<%@ Register Src="~/CMSModules/Ecommerce/Controls/ShoppingCart/ShoppingCartSKUPriceDetail.ascx"
    TagName="ShoppingCartSKUPriceDetail" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <div class="PageContent">
        <cms:ShoppingCartSKUPriceDetail ID="ucSKUPriceDetail" runat="server" />
    </div>

    <script type="text/javascript">
        //<![CDATA[       
        function Close() {
            top.window.close();
        }
        //]]>
    </script>

</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:CMSButton ID="btnClose" runat="server" CssClass="ContentButton" EnableViewState="false" />
    </div>
</asp:Content>
