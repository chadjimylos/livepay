<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BizForm_New.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="BizForms - New BizForm"
    Inherits="CMSModules_BizForms_Tools_BizForm_New" Theme="Default"%>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
	<asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false" Visible="false" />
	<asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false" Visible="false" />
	<table style="vertical-align:top">
		<tr>
			<td class="FieldLabel"><asp:Label runat="server" ID="lblFormDisplayName" EnableViewState="false" /></td>
			<td>
			    <asp:TextBox ID="txtFormDisplayName" runat="server" CssClass="TextBoxField" MaxLength="100" />
			    <asp:RequiredFieldValidator ID="rfvFormDisplayName" runat="server" ControlToValidate="txtFormDisplayName" />
		    </td>
		</tr>
		<tr>
			<td class="FieldLabel"><asp:Label runat="server" ID="lblFormName" EnableViewState="false" /></td>
			<td>
			    <asp:TextBox ID="txtFormName" runat="server" CssClass="TextBoxField" MaxLength="100" />
			    <asp:RequiredFieldValidator ID="rfvFormName" runat="server" ControlToValidate="txtFormName" />
		    </td>						
		</tr>
		<tr>
			<td class="FieldLabel"><asp:Label runat="server" ID="lblTableName" EnableViewState="false" /></td>
			<td>
			    <asp:TextBox ID="txtTableName" runat="server" CssClass="TextBoxField" MaxLength="100" />
                <asp:RequiredFieldValidator ID="rfvTableName" runat="server" ControlToValidate="txtTableName" />
            </td>                            
		</tr>					
		<tr>
			<td></td>
			<td><cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false" CssClass="SubmitButton" /></td>
		</tr>
	</table>
</asp:Content>
