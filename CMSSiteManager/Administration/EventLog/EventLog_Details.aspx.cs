using System;
using System.Data;
using System.Collections;
using System.Web;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.EventLog;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSSiteManager_Administration_EventLog_EventLog_Details : CMSEventLogPage
{
    #region "Protected variables"

    protected int eventId = 0;
    protected int prevId = 0;
    protected int nextId = 0;
    protected EventLogProvider eventProvider = new EventLogProvider();

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (QueryHelper.ValidateHash("hash", "eventid"))
        {
            string title = ResHelper.GetString("EventLogDetails.Header");
            this.Page.Title = title;
            this.CurrentMaster.Title.TitleText = title;
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_EventLog/detail.png");

            // Get the ORDER BY column and starting event ID
            string orderBy = QueryHelper.GetString("orderby", "EventID DESC");
            string whereCondition = HttpUtility.UrlDecode(QueryHelper.GetString("where", "")).Replace("%26", "&").Replace("%23", "#");

            eventId = QueryHelper.GetInteger("eventid", 0);

            if (!RequestHelper.IsPostBack())
            {
                // Get EventID value
                LoadData();
            }

            lnkExport.Visible = true;
            lnkExport.Text = ResHelper.GetString("EventLogDetails.Export");
            lnkExport.NavigateUrl = ResolveUrl("GetEventDetail.aspx?eventid=" + eventId);
            int siteID = QueryHelper.GetInteger("siteid", 0) ;
            if (siteID > 0)
            {
                lnkExport.NavigateUrl = UrlHelper.AddParameterToUrl(lnkExport.NavigateUrl, "siteid", siteID.ToString());
            }

            lnkExport.Target = "_blank";

            // Initialize next/previous buttons
            int[] prevNext = eventProvider.GetPreviousNext(eventId, whereCondition, orderBy);
            if (prevNext != null)
            {
                prevId = prevNext[0];
                nextId = prevNext[1];

                btnPrevious.Enabled = (prevId != 0);
                btnNext.Enabled = (nextId != 0);

                btnPrevious.Click += btnPrevious_Click;
                btnNext.Click += btnNext_Click;
            }

            // Set button caption
            btnNext.Text = ResHelper.GetString("general.next") + " >";
            btnPrevious.Text = "< " + ResHelper.GetString("general.back");

            RegisterModalPageScripts();
        }
    }

    #endregion


    #region "Button handling"

    protected void btnPrevious_Click(object sender, EventArgs e)
    {
        // Redirect to previous
        UrlHelper.Redirect(UrlHelper.UpdateParameterInUrl(UrlHelper.CurrentURL, "eventId", "" + prevId));
    }


    protected void btnNext_Click(object sender, EventArgs e)
    {
        // Redirect to next
        UrlHelper.Redirect(UrlHelper.UpdateParameterInUrl(UrlHelper.CurrentURL, "eventId", "" + nextId));
    }

    #endregion


    #region "Protected methods"

    /// <summary>
    /// Loads data of specific EventLog from DB.
    /// </summary>
    protected void LoadData()
    {
        IDataClass ev = eventProvider.GetEventData(eventId);
        DataRow dr = null;
        if (ev != null)
        {
            dr = ev.DataRow;
        }

        if (dr != null)
        {
            string eventType = ValidationHelper.GetString(dr["EventType"], "");

            // Rewrite event type text.
            lblEventTypeValue.Text = EventLogHelper.GetEventTypeText(eventType);

            lblEventIDValue.Text = ValidationHelper.GetString(dr["EventID"], "");
            lblEventTimeValue.Text = ValidationHelper.GetString(dr["EventTime"], "");
            lblSourceValue.Text = HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["Source"], ""));
            lblEventCodeValue.Text = HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["EventCode"], ""));

            lblUserIDValue.Text = ValidationHelper.GetString(dr["UserID"], "");
            plcUserID.Visible = (lblUserIDValue.Text != "");

            lblUserNameValue.Text = HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["UserName"], ""));
            plcUserName.Visible = (lblUserNameValue.Text != "");

            lblIPAddressValue.Text = ValidationHelper.GetString(dr["IPAddress"], "");

            lblNodeIDValue.Text = ValidationHelper.GetString(dr["NodeID"], "");
            plcNodeID.Visible = (lblNodeIDValue.Text != "");

            lblNodeNameValue.Text = HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["DocumentName"], ""));
            plcNodeName.Visible = (lblNodeNameValue.Text != "");

            string description = HTMLHelper.StripTags(ValidationHelper.GetString(dr["EventDescription"], "").Replace("<br />", "\r\n").Replace("<br/>", "\r\n"));
            lblEventDescriptionValue.Text = HTMLHelper.EnsureLineEnding(HTMLHelper.HTMLEncode(description), "<br />");

            if (!DataHelper.IsEmpty(dr["SiteID"]))
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(Convert.ToInt32(dr["SiteID"]));
                if (si != null)
                {
                    lblSiteNameValue.Text = HTMLHelper.HTMLEncode(si.DisplayName);
                }
            }
            else
            {
                plcSite.Visible = false;
            }

            lblMachineNameValue.Text = HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["EventMachineName"], ""));
            lblEventUrlValue.Text = HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["EventUrl"], ""));
            lblUrlReferrerValue.Text = HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["EventUrlReferrer"], ""));
            lblUserAgentValue.Text = HTMLHelper.HTMLEncode(ValidationHelper.GetString(dr["EventUserAgent"], ""));
        }
    }

    #endregion
}
