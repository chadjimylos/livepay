<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StoreSettings_General.aspx.cs"
    Inherits="CMSModules_Ecommerce_Tools_Configuration_StoreSettings_StoreSettings_General"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <div class="PageContent">
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
            Visible="false" />
        <table style="vertical-align: top">
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblCurrentMainCurrency" EnableViewState="false" />
                </td>
                <td>
                    <asp:Label ID="lblMainCurrency" runat="server" EnableViewState="false" />&nbsp;
                </td>
                <td style="text-align: right;">
                    <cms:CMSButton ID="btnChangeCurrency" runat="server" CssClass="ContentButton" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblDefaultCountry" EnableViewState="false" />
                </td>
                <td colspan="2">
                    <cms:LocalizedDropDownList ID="drpCountry" runat="server" DataTextField="CountryDisplayName"
                        DataValueField="CountryID" CssClass="DropDownField">
                    </cms:LocalizedDropDownList>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblShippingFreeLimit" EnableViewState="false" />
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtShippingFreeLimit" runat="server" CssClass="TextBoxField" MaxLength="10"
                        EnableViewState="false" />
                    <asp:RangeValidator ID="rvShippingFreeLimit" runat="server" ControlToValidate="txtShippingFreeLimit"
                        MaximumValue="9999999999" MinimumValue="0" Type="Double" Display="Dynamic" ValidationGroup="Internal"
                        EnableViewState="false"></asp:RangeValidator>
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblAllowAnonymous" EnableViewState="false" />
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="chkAllowAnonymous" runat="server" CssClass="CheckBoxMovedLeft"
                        Checked="false" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel">
                    <asp:Label runat="server" ID="lblUseCompanyAddress" EnableViewState="false" />
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="chkUseCompanyAddress" runat="server" CssClass="CheckBoxMovedLeft"
                        Checked="false" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td class="FieldLabel" style="width: 240px">
                    <asp:Label runat="server" ID="lblRequireIDs" EnableViewState="false" />
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="chkRequireIDs" runat="server" CssClass="CheckBoxMovedLeft" Checked="false"
                        AutoPostBack="true" OnCheckedChanged="chkRequreIDs_CheckedChanged" />
                </td>
            </tr>
            <asp:Panel runat="server" ID="pnlIDs">
                <tr>
                    <td class="FieldLabel">
                        <asp:Label runat="server" ID="lblOrganizationID" EnableViewState="false" />
                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="chkOrganizationID" runat="server" CssClass="CheckBoxMovedLeft"
                            Checked="false" EnableViewState="false" />
                    </td>
                </tr>
                <tr>
                    <td class="FieldLabel">
                        <asp:Label runat="server" ID="lblTaxRegistrationID" EnableViewState="false" />
                    </td>
                    <td colspan="2">
                        <asp:CheckBox ID="chkTaxRegistrationID" runat="server" CssClass="CheckBoxMovedLeft"
                            Checked="false" EnableViewState="false" />
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <cms:CMSButton runat="server" ID="btnOk" OnClick="btnOK_Click" EnableViewState="false"
                        CssClass="SubmitButton" ValidationGroup="Internal" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
