<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MacroSelector.ascx.cs"
    Inherits="CMSAdminControls_UI_Selectors_MacroSelector" %>
<cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional" RenderMode="Inline">
    <ContentTemplate>
        <table cellspacing="0" cellspacing="0">
            <tr>
                <td style="vertical-align: top;">
                    <asp:Panel ID="pnlMacroType" runat="server">
                        <cms:LocalizedLabel ID="lblMacroType" runat="server" EnableViewState="false" ResourceString="macroselector.type" /><br />
                        <asp:DropDownList ID="drpMacroType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpMacroType_SelectedIndexChanged"
                            CssClass="SmallDropDown" />
                    </asp:Panel>
                </td>
                <td style="vertical-align: top;" colspan="2">
                    <asp:Panel ID="pnlDrop1" runat="server">
                        <cms:LocalizedLabel ID="lblMacroData" runat="server" EnableViewState="false" ResourceString="macroselector.data" /><br />
                        <asp:DropDownList ID="drpDataTable" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpDataTable_SelectedIndexChanged"
                            CssClass="SmallDropDown" Width="310" />
                    </asp:Panel>
                    <asp:Panel ID="pnlTextBox" runat="server" CssClass="FloatLeft">
                        <cms:LocalizedLabel ID="lblMacroText" runat="server" EnableViewState="false" ResourceString="macroselector.text" /><br />
                        <asp:TextBox ID="txtInput" runat="server" CssClass="SmallTextBox" />
                    </asp:Panel>
                </td>
                <td style="white-space: nowrap; vertical-align: top;">
                    <asp:Panel ID="pnlDrop2" runat="server" CssClass="FloatLeft">
                        <br />
                        <asp:DropDownList ID="drpDataColumn" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpDataColumn_SelectedIndexChanged"
                            CssClass="SmallDropDown" />
                    </asp:Panel>
                    <asp:Panel ID="pnlTextDataBox" runat="server" CssClass="FloatLeft">
                        <cms:LocalizedLabel ID="lblMacroDataText" runat="server" EnableViewState="false"
                            ResourceString="macroselector.text" /><br />
                        <asp:TextBox ID="txtInputData" runat="server" CssClass="SmallTextBox" />
                    </asp:Panel>
                </td>
                <td style="vertical-align: top;">
                    <asp:Panel ID="pnlInsert" runat="server" CssClass="FloatLeft">
                        <br />
                        <cms:LocalizedButton ID="btnInsert" runat="server" CssClass="ContentButton" OnClick="btnInsert_Click"
                            ResourceString="macroselector.insert" />
                    </asp:Panel>
                </td>
            </tr>
            <asp:Panel ID="pnlExtend" runat="server" Visible="false">
                <tr>
                    <td style="vertical-align: top;">
                        <cms:LocalizedLabel ID="lblDefault" runat="server" EnableViewState="false" ResourceString="macroselector.default" /><br />
                        <asp:TextBox ID="txtDefault" runat="server" CssClass="SmallTextBox" />
                    </td>
                    <td style="vertical-align: top;">
                        <cms:LocalizedLabel ID="lblCulture" runat="server" EnableViewState="false" ResourceString="macroselector.culture" /><br />
                        <asp:TextBox ID="txtCulture" runat="server" CssClass="SmallTextBox" />
                    </td>
                    <td style="vertical-align: top;">
                        <cms:LocalizedLabel ID="lblFormat" runat="server" EnableViewState="false" ResourceString="macroselector.format" /><br />
                        <asp:TextBox ID="txtFormat" runat="server" CssClass="SmallTextBox" />
                    </td>
                    <td style="vertical-align: top;">
                        <asp:Panel ID="pnlBottomInsert" runat="server" CssClass="FloatLeft">
                            <br />
                            <cms:LocalizedButton ID="btnBottomInsert" runat="server" CssClass="LongButton" OnClick="btnInsert_Click"
                                ResourceString="macroselector.insertmacro" />
                        </asp:Panel>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </asp:Panel>
        </table>
        <asp:Literal ID="ltlType" runat="server" Visible="false" />
        <asp:Literal ID="ltlPattern" runat="server" Visible="false" />
        <asp:Label ID="lblError" runat="server" EnableViewState="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
