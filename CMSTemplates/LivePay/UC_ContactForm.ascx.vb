﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls

Imports CMS.PortalControls
Imports CMS.GlobalHelper
Imports CMS.TreeEngine
Imports CMS.CMSHelper
Imports CMS.SiteProvider
Imports CMS.EmailEngine
Imports CMS.EventLog
Imports CMS.DataEngine
Imports CMS.WebAnalytics
Imports CMS.LicenseProvider
Imports CMS.PortalEngine
Imports CMS.SettingsProvider
Imports System.Text.RegularExpressions


Imports System.Data.SqlClient

Imports CMS.UIControls
Imports CMS.FormEngine


Partial Class CMSTemplates_LivePay_UC_ContactForm
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim lang As String = CMSContext.CurrentDocument.DocumentCulture

            btnOK.ImageUrl = ResHelper.LocalizeString("{$=/app_themes/LivePay/Contact/btnSend.png|en-us=/app_themes/LivePay/Contact/btnSend_en-us.png$}")

            lbl_FullName.Text = ResHelper.LocalizeString("{$=Ονοματεπώνυμο|en-us=FullName$}")
            RequiredFullName.Text = ResHelper.LocalizeString("{$=Παρακαλώ συμπληρώστε το ονοματεπώνυμό σας|en-us=Please fill in your name$}")
            revtxtEmailInput.Text = ResHelper.LocalizeString("{$=Παρακαλώ εισάγετε ένα έγκυρο e-mail|en-us=Please enter a valid e-mail address$}")
            RequiredtxtEmailInput.Text = ResHelper.LocalizeString("{$=Παρακαλώ εισάγετε ένα έγκυρο e-mail|en-us=Please enter a valid e-mail address$}")

            lbl_Category.Text = ResHelper.LocalizeString("{$=Κατηγορία|en-us=Category$}")
            rfvCategory.Text = ResHelper.LocalizeString("{$=Παρακαλώ επιλέξτε κατηγορία|en-us=Please select a category$}")
            lbl_Descr.Text = ResHelper.LocalizeString("{$=Περιγραφή|en-us=Description$}")
            RequiredDescr.Text = ResHelper.LocalizeString("{$=Παρακαλώ εισάγετε το μήνυμά σας|en-us=Please enter your message$}")
            lblCaptcha.Text = ResHelper.LocalizeString("{$=Συμπληρώστε τους παραπάνω χαρακτήρες|en-us=Fill in the above mentioned characters$}")
            RequiredCaptcha.Text = ResHelper.LocalizeString("{$=Παρακαλώ συμπληρώστε τους παραπάνω χαρακτήρες|en-us=Please fill in the characters shown above$}")
            Comments.Text = ResHelper.LocalizeString("{$=Τα πεδία με τον αστερίσκο (*) είναι υποχρεωτικά|en-us=Fields marked with an asterisk (*) are required$}")

            
            If lang = "el-GR" Then
                ValidationSummary.HeaderText = "Παρακαλώ, συμπληρώστε όλα τα υποχρεωτικά πεδία."
                lbl_CaptchaError.Text = "Οι χαρακτήρες που καταχωρίσατε δεν αντιστοιχούν στην επαλήθευση λέξης. Δοκιμάστε ξανά."

                Category.Items.Insert(0, New ListItem("Επιλογή", "0"))
                Category.Items.Insert(1, New ListItem("Ερώτηση", "1"))
                Category.Items.Insert(2, New ListItem("Παράπονο", "2"))
                Category.Items.Insert(3, New ListItem("Πρόταση", "3"))
            Else
                ValidationSummary.HeaderText = "The entered values cannot be saved. Please see the fields below for details"
                lbl_CaptchaError.Text = "Captcha verification failed."

                Category.Items.Insert(0, New ListItem("Select", "0"))
                Category.Items.Insert(1, New ListItem("Question", "1"))
                Category.Items.Insert(2, New ListItem("Complaint", "2"))
                Category.Items.Insert(3, New ListItem("Proposal", "3"))
            End If

            
            Category.SelectedValue = 0
        End If



    End Sub

    Protected Sub btnOK_Click(sender As Object, e As ImageClickEventArgs) Handles btnOK.Click
        Dim lang As String = CMSContext.CurrentDocument.DocumentCulture

        scCaptcha.ValidateCaptcha(txtCaptcha.Text.ToUpper())
        If Not scCaptcha.UserValidated Then
            lbl_CaptchaError.Visible = True
            txtCaptcha.Text = String.Empty
            Return
            Exit Sub

        Else
            txtCaptcha.Text = String.Empty

            Dim bizFormName As String = "ContactForm"
            Dim siteName As String = CMSContext.CurrentSiteName
            Dim bfi As BizFormInfo = BizFormInfoProvider.GetBizFormInfo(bizFormName, siteName)
            If Not IsNothing(bfi) Then
                Dim dci As DataClassInfo = DataClassInfoProvider.GetDataClass(bfi.FormClassID)
                If Not IsNothing(dci) Then
                    Dim genConn As GeneralConnection = ConnectionHelper.GetConnection()
                    Dim formRecord As DataClass = New DataClass(dci.ClassName, genConn)

                    formRecord.SetValue("FormInserted", DateTime.Now)
                    formRecord.SetValue("FormUpdated", DateTime.Now)
                    formRecord.SetValue("FullName", FullName.Text.Replace("'", ""))
                    formRecord.SetValue("Email", txtEmailInput.Text.Replace("'", ""))
                    formRecord.SetValue("Category", Category.SelectedValue.Replace("'", ""))
                    formRecord.SetValue("Descr", Descr.Text.Replace("'", ""))
		    formRecord.SetValue("txtComment", "")		    

                    formRecord.Insert()
                    bfi.FormItems += 1
                    BizFormInfoProvider.SetBizFormInfo(bfi)
                    Dim Content As IDataClass = DataClassFactory.NewDataClass(dci.ClassName, formRecord.ID, genConn)
                    Dim sendFromEmail As String = bfi.FormSendFromEmail
                    Dim sendToEmail As String = bfi.FormSendToEmail
                    Dim sendEmailtoAdmin As Boolean = ((Not String.IsNullOrEmpty(sendFromEmail)) AndAlso (sendToEmail <> ""))
                    If sendEmailtoAdmin Then
                        If Not formRecord.IsEmpty Then
                            Dim row As DataRow = Content.DataRow
                            Dim bizform_ As New CMS.FormControls.BizForm()
                            bizform_.SendNotificationEmail(sendFromEmail, sendToEmail, row, bfi, Content)
                        End If
                    End If

                End If

                If lang = "el-GR" Then
                    Response.Redirect("~/contactsuccess.aspx")
                Else
                    Response.Redirect("~/contactsuccess.aspx?lang=en-US")
                End If

            End If
        End If

    End Sub
End Class
