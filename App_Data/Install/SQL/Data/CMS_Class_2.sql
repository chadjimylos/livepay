set identity_insert [cms_class] on
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Newsletter - Subscriber', N'', N'', 0, N'', N'', N'', 1, N'', N'', N'<form><field column="SubscriberID" fieldcaption="" visible="false" defaultvalue="" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" columnsize="" fielddescription="" regularexpression="" minstringlength="" maxstringlength="" minnumericvalue="" maxnumericvalue="" mindatetimevalue="" maxdatetimevalue="" validationerrormessage="" captionstyle="" inputcontrolstyle="" fileextensions="" publicfield="false" guid="83c8689b-22e0-4b81-a1f0-ae6ff8808fba" /><field column="SubscriberEmail" fieldcaption="{$General.Email$}" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="400" regularexpression="^[a-zA-Z0-9_\-\+]+(\.[a-zA-Z0-9_\-\+]+)*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$" validationerrormessage="{$NewsletterSubscription.ErrorInvalidEmail$}" publicfield="false" spellcheck="true" guid="c13eb8fa-f65d-4052-8f91-92fc00c53679" visibility="none" ismacro="false" /><field column="SubscriberFirstName" fieldcaption="{$SubscribeForm.FirstName$}" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="e02a73e0-3722-47ba-be59-aae58cae2c49" /><field column="SubscriberLastName" fieldcaption="{$SubscribeForm.LastName$}" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="d3020a22-0412-443c-8925-0d0936813c55" /><field column="SubscriberSiteID" fieldcaption="" visible="false" defaultvalue="" columntype="integer" fieldtype="label" allowempty="false" isPK="false" system="true" columnsize="" fielddescription="" regularexpression="" minstringlength="" maxstringlength="" minnumericvalue="" maxnumericvalue="" mindatetimevalue="" maxdatetimevalue="" validationerrormessage="" captionstyle="" inputcontrolstyle="" fileextensions="" publicfield="false" guid="2bfe7ce0-e15c-4cef-bc53-c0cecabbcf52" /><field column="SubscriberGUID" fieldcaption="SubscriberGUID" visible="false" defaultvalue="" columntype="file" fieldtype="label" allowempty="false" isPK="false" system="true" columnsize="" fielddescription="" regularexpression="" minstringlength="" maxstringlength="" minnumericvalue="" maxnumericvalue="" mindatetimevalue="" maxdatetimevalue="" validationerrormessage="" captionstyle="" inputcontrolstyle="" fileextensions="" publicfield="false" spellcheck="true" guid="4e52e79d-6de4-4fda-86cd-e22150b496c0" /><field column="SubscriberRelatedID" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6a33bca3-a330-4bd4-8e35-bffa9a14d941" /><field column="SubscriberLastModified" visible="false" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0b576d02-188a-4e93-8900-7197bf863eb5" /><field column="SubscriberType" visible="false" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="6d688ffb-acac-4b92-89f8-35689d3da781" /></form>', '20091118 11:00:20', 908, N'newsletter.subscriber', 0, 0, 0, '01c9a7a3-beb3-48ef-a2b2-2cd56a0f074f', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Newsletter_Subscriber', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Newsletter_Subscriber">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SubscriberID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SubscriberEmail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriberFirstName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriberLastName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriberSiteID" type="xs:int" />
              <xs:element name="SubscriberGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="SubscriberCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriberType" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriberRelatedID" type="xs:int" minOccurs="0" />
              <xs:element name="SubscriberLastModified" type="xs:dateTime" />
              <xs:element name="SubscriberFullName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Newsletter_Subscriber" />
      <xs:field xpath="SubscriberID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'ScheduledTask', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TaskID" fieldcaption="TaskID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="4419f729-75ac-4eb7-a4d4-bdcdb8d03884" /><field column="TaskName" fieldcaption="TaskName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e9eabd43-9133-4cd9-8012-aefa513e87b2" /><field column="TaskDisplayName" fieldcaption="TaskDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a3f95b42-bd02-4db6-b1aa-0d340952a27e" /><field column="TaskAssemblyName" fieldcaption="TaskAssemblyName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4c567e00-bf7a-42e8-878b-a5befde1faed" /><field column="TaskClass" fieldcaption="TaskClass" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c7a260a0-d909-4e9e-b1d3-bcbda7eb9076" /><field column="TaskInterval" fieldcaption="TaskInterval" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="02900d9c-c032-4081-9532-a48c339dbbce" /><field column="TaskData" fieldcaption="TaskData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a8fd0828-3456-483d-9845-f93c6d9fc9fd" /><field column="TaskLastRunTime" fieldcaption="TaskLastRunTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c1c5fbe3-cf9b-4055-b663-18898c5cae18" /><field column="TaskNextRunTime" fieldcaption="TaskNextRunTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="72b48b10-53ca-4934-8be4-c3044af2be7d" /><field column="TaskProgress" fieldcaption="TaskProgress" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b541b89c-ea55-4085-9a0d-9c9c33cf64e5" /><field column="TaskLastResult" fieldcaption="TaskLastResult" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="feb262dd-3a25-4301-8b31-6d068f0a75a1" /><field column="TaskEnabled" fieldcaption="TaskEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="dbd5d77f-f7f4-4da4-87f8-9027cbcca5e1" /><field column="TaskSiteID" fieldcaption="TaskSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e4bc2c60-9893-4c41-a3c9-45395365d776" /><field column="TaskDeleteAfterLastRun" fieldcaption="TaskDeleteAfterLastRun" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="564b2be3-fa3f-4517-8cf3-f36bff312e6b" /><field column="TaskServerName" fieldcaption="TaskServerName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a86cf79a-156b-4075-a378-58875eb715ef" /><field column="TaskGUID" fieldcaption="TaskGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6db955f1-124a-4a2b-ad66-596dd53936ea" /><field column="TaskLastModified" fieldcaption="TaskLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5a02bac7-ee29-497a-a42d-22f1201e1b1c" /><field column="TaskExecutions" fieldcaption="TaskExecutions" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5f1cbc53-2a56-415b-b194-fdf91a48a8ad" /></form>', '20081231 08:50:07', 909, N'cms.ScheduledTask', 0, 0, 0, '57b78754-4137-4220-b736-8c16cbbe6c87', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_ScheduledTask', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_ScheduledTask">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TaskID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="TaskName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskAssemblyName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskClass" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskInterval">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1000" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskData">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskLastRunTime" type="xs:dateTime" minOccurs="0" />
              <xs:element name="TaskNextRunTime" type="xs:dateTime" minOccurs="0" />
              <xs:element name="TaskProgress" type="xs:int" minOccurs="0" />
              <xs:element name="TaskLastResult" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskEnabled" type="xs:boolean" />
              <xs:element name="TaskSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="TaskDeleteAfterLastRun" type="xs:boolean" minOccurs="0" />
              <xs:element name="TaskServerName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="TaskLastModified" type="xs:dateTime" />
              <xs:element name="TaskExecutions" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_ScheduledTask" />
      <xs:field xpath="TaskID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'NewsletterSubscriberNewsletter', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="SubscriberID" fieldcaption="SubscriberID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="812e73dc-fd64-4e4d-8edc-b83ee7991a33" /><field column="NewsletterID" fieldcaption="NewsletterID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b19b8699-2c2f-4001-b802-04cdaa87e01e" /><field column="SubscribedWhen" fieldcaption="SubscribedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="882d41c1-10a3-4c89-9b11-19cb98f88807" /></form>', '20081229 09:07:13', 912, N'Newsletter.SubscriberNewsletter', 0, 0, 0, 'e73eba16-4a8d-445d-a1f1-ac3d610b855e', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Newsletter_SubscriberNewsletter', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Newsletter_SubscriberNewsletter">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SubscriberID" type="xs:int" />
              <xs:element name="NewsletterID" type="xs:int" />
              <xs:element name="SubscribedWhen" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Newsletter_SubscriberNewsletter" />
      <xs:field xpath="SubscriberID" />
      <xs:field xpath="NewsletterID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Newsletter - Newsletter', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="NewsletterID" fieldcaption="NewsletterID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="f015e086-c0c6-4c72-bbaa-7a7461f4e5ff" /><field column="NewsletterDisplayName" fieldcaption="NewsletterDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7af312ef-f89f-4480-bafd-22f924b7517f" /><field column="NewsletterName" fieldcaption="NewsletterName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ab201dec-dd17-45e1-8057-306ed113b8ed" /><field column="NewsletterType" fieldcaption="NewsletterType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5d898e33-0f0d-462e-b051-91228c9b4729" /><field column="NewsletterSubscriptionTemplateID" fieldcaption="NewsletterSubscriptionTemplateID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="902c9fb3-08fd-453d-9cad-7f2b1ef40201" /><field column="NewsletterUnsubscriptionTemplateID" fieldcaption="NewsletterUnsubscriptionTemplateID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0fceb734-921a-4434-bede-0aed0ed35823" /><field column="NewsletterSenderName" fieldcaption="NewsletterSenderName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2eaf8900-a508-4c38-a013-c8d243d2f96c" /><field column="NewsletterSenderEmail" fieldcaption="NewsletterSenderEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8499a3ed-40b3-4ea7-bbf7-4ac62e2a835c" /><field column="NewsletterDynamicSubject" fieldcaption="NewsletterDynamicSubject" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b489b91a-31a1-4c1a-9208-255f78324403" /><field column="NewsletterDynamicURL" fieldcaption="NewsletterDynamicURL" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="58bc4a29-4c40-4b2d-acba-197115902e38" /><field column="NewsletterDynamicScheduledTaskID" fieldcaption="NewsletterDynamicScheduledTaskID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="21c10b0f-e449-4c7b-b5ad-7f9fba1c9705" /><field column="NewsletterTemplateID" fieldcaption="NewsletterTemplateID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="95bd18af-6e22-4c94-baaa-7f20254b60c2" /><field column="NewsletterSiteID" fieldcaption="NewsletterSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9048b776-a74a-4551-b7c5-cee34128b9ee" /><field column="NewsletterGUID" fieldcaption="NewsletterGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5c08f195-4b00-46b3-a733-a629c8206380" /><field column="NewsletterUnsubscribeUrl" fieldcaption="NewsletterUnsubscribeUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4b99c0d8-704a-4d1f-ba23-9924ffdb0f3e" /><field column="NewsletterBaseUrl" fieldcaption="NewsletterBaseUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2e08adfd-1256-4946-abb1-7bc104bf95c3" /><field column="NewsletterLastModified" fieldcaption="NewsletterLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2dd1c825-0c1c-4ea8-84d0-64a3bff43f32" /><field column="NewsletterUseEmailQueue" fieldcaption="NewsletterUseEmailQueue" visible="true" defaultvalue="false" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1dfa055c-6214-44b1-ac7d-92ed36d0eb68" /></form>', '20100308 19:28:00', 913, N'newsletter.newsletter', 0, 0, 0, '22ecf2f7-865a-4a1e-bbaa-5da680489b39', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Newsletter_Newsletter', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Newsletter_Newsletter">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="NewsletterID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="NewsletterDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NewsletterName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NewsletterType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="5" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NewsletterSubscriptionTemplateID" type="xs:int" />
              <xs:element name="NewsletterUnsubscriptionTemplateID" type="xs:int" />
              <xs:element name="NewsletterSenderName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NewsletterSenderEmail">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NewsletterDynamicSubject" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="500" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NewsletterDynamicURL" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="500" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NewsletterDynamicScheduledTaskID" type="xs:int" minOccurs="0" />
              <xs:element name="NewsletterTemplateID" type="xs:int" minOccurs="0" />
              <xs:element name="NewsletterSiteID" type="xs:int" />
              <xs:element name="NewsletterGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="NewsletterUnsubscribeUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1000" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NewsletterBaseUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="500" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="NewsletterLastModified" type="xs:dateTime" />
              <xs:element name="NewsletterUseEmailQueue" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Newsletter_Newsletter" />
      <xs:field xpath="NewsletterID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Newsletter - Emails', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="EmailID" fieldcaption="EmailID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="8a400d2b-7570-430f-b380-39bef865cbd4" /><field column="EmailNewsletterIssueID" fieldcaption="EmailNewsletterIssueID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="578a62a2-28aa-4e74-ae40-e3bbe1ff2445" /><field column="EmailSubscriberID" fieldcaption="EmailSubscriberID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="56aebfe8-f488-43e1-baf1-7b42e5935fb7" /><field column="EmailSiteID" fieldcaption="EmailSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c07c5d6c-4898-4e70-8c41-112da8032f06" /><field column="EmailLastSendResult" fieldcaption="EmailLastSendResult" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="27b30e71-7cb9-482c-b6b3-efdc45109bf3" /><field column="EmailLastSendAttempt" fieldcaption="EmailLastSendAttempt" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="bfbe1ae6-2430-41ab-90ed-a648c0db3946" /><field column="EmailSending" fieldcaption="EmailSending" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6dcfdb16-3b71-4a22-94d6-27fb374a537f" /><field column="EmailGUID" fieldcaption="EmailGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a35bf542-8ab5-4e7a-a000-6e84be3b4265" /><field column="EmailLastModified" fieldcaption="EmailLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="30cefcfa-1ae9-45dc-ac26-5e57b4427de3" /></form>', '20100308 19:38:10', 919, N'newsletter.emails', 0, 0, 0, '551d4df2-d429-4a32-906b-c5821a04f7da', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Newsletter_Emails', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Newsletter_Emails">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="EmailID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="EmailNewsletterIssueID" type="xs:int" />
              <xs:element name="EmailSubscriberID" type="xs:int" />
              <xs:element name="EmailSiteID" type="xs:int" />
              <xs:element name="EmailLastSendResult" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="EmailLastSendAttempt" type="xs:dateTime" minOccurs="0" />
              <xs:element name="EmailSending" type="xs:boolean" minOccurs="0" />
              <xs:element name="EmailGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="EmailLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Newsletter_Emails" />
      <xs:field xpath="EmailID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'WebPartContainer', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="ContainerID" fieldcaption="ContainerID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="b2c2b404-fd6f-4fb1-bb93-4f6926ac3e37" /><field column="ContainerDisplayName" fieldcaption="ContainerDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="790388db-652f-4b65-9740-65ec50bb1f35" /><field column="ContainerName" fieldcaption="ContainerName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="62939e60-f1b3-4164-b587-94aa5b9a1c5f" /><field column="ContainerTextBefore" fieldcaption="ContainerTextBefore" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f2c087fc-b7ea-4513-a14e-89b5769d2b09" /><field column="ContainerTextAfter" fieldcaption="ContainerTextAfter" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b8c67f27-9765-4371-a48d-8281e220c530" /><field column="ContainerGUID" fieldcaption="ContainerGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ba1c27b5-3f48-45d4-bdfc-62471238b7f7" /><field column="ContainerLastModified" fieldcaption="ContainerLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c35ecd48-2d39-4daa-ba7e-5087dfee2c1f" /></form>', '20081229 09:07:40', 929, N'cms.WebPartContainer', 0, 0, 0, 'efe592b2-9b49-4997-8572-8419674cad0c', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_WebPartContainer', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WebPartContainer">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ContainerID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ContainerDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ContainerName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ContainerTextBefore" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ContainerTextAfter" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ContainerGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="ContainerLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WebPartContainer" />
      <xs:field xpath="ContainerID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'LicenseKey', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="LicenseKeyID" fieldcaption="LicenseKeyID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="d2e30d80-9086-4d4b-a508-842eefa2aa1a" /><field column="LicenseDomain" fieldcaption="LicenseDomain" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cc354cd7-2b35-4a58-9ab4-a7f4d4428881" /><field column="LicenseKey" fieldcaption="LicenseKey" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="13e758e9-06e7-4074-8805-13aa3acd6d02" /><field column="LicenseEdition" fieldcaption="LicenseEdition" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d7884a96-2d2f-4e1b-9e06-2063cf08bff0" /><field column="LicenseExpiration" fieldcaption="LicenseExpiration" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b50fdb22-682e-4f36-abe6-2962376c45a0" /><field column="LicensePackages" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="993a19f5-15b7-4b89-8844-0b5520a1c57c" visibility="none" ismacro="false" fieldcaption="LicensePackages" /><field column="LicenseServers" fieldcaption="LicenseServers" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="135c8059-2619-431b-8fd1-682b5cf5c332" visibility="none" ismacro="false" /></form>', '20091211 15:01:08', 930, N'cms.LicenseKey', 0, 0, 0, '029aa7db-a6e2-4996-b1c7-38f7bbd838ee', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_LicenseKey', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_LicenseKey">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="LicenseKeyID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="LicenseDomain">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LicenseKey">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LicenseEdition" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LicenseExpiration" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LicensePackages" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LicenseServers" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_LicenseKey" />
      <xs:field xpath="LicenseKeyID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'WebFarmServer', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ServerID" fieldcaption="ServerID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="aa45d15a-369b-4d52-b349-50a6dd88bf99" /><field column="ServerDisplayName" fieldcaption="ServerDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0faeba3a-902c-498b-86f1-adc10aecc480" /><field column="ServerName" fieldcaption="ServerName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2b918771-1f4f-4b7e-8079-a9bc77580211" /><field column="ServerURL" fieldcaption="ServerURL" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="46b24382-3b88-46b8-b1f8-9a04ddbf09d3" /><field column="ServerGUID" fieldcaption="ServerGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4a6c0fb1-566d-4a9f-b780-a6777aab8ac5" /><field column="ServerLastModified" fieldcaption="ServerLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="08311544-ceb1-4df6-b40a-c7c7eb4b8048" /><field column="ServerEnabled" fieldcaption="ServerEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a7318ab5-b31f-4c39-a7e5-6f3fe9dff9c1" /></form>', '20100112 11:48:02', 1093, N'cms.WebFarmServer', 0, 0, 0, 'bb652b81-0792-4191-9d32-e7b8f2bd6e4f', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_WebFarmServer', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WebFarmServer">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ServerID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ServerDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerURL">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2000" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ServerLastModified" type="xs:dateTime" />
              <xs:element name="ServerEnabled" type="xs:boolean" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WebFarmServer" />
      <xs:field xpath="ServerID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'WebFarmTask', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="TaskID" fieldcaption="TaskID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="5e8df106-ca79-494f-bcf1-d0b016afbad9" /><field column="TaskType" fieldcaption="TaskType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8f272cff-24c4-48d6-8199-d96f0b3a4c49" /><field column="TaskTextData" fieldcaption="TaskTextData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="35ac3782-f563-4df0-af7e-989db67a7ec7" /><field column="TaskBinaryData" fieldcaption="TaskBinaryData" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="93186034-dfa3-49ac-8d6c-3c7c503e088c" /><field column="TaskCreated" fieldcaption="TaskCreated" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2468f2df-6070-4941-82f0-04c1f9aa91d0" /><field column="TaskEnabled" fieldcaption="TaskEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e53d2ff4-2a03-41d3-95b9-8808a4a4e4af" /><field column="TaskTarget" fieldcaption="TaskTarget" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="92590b46-2f18-4de4-8a83-3e670dc72cce" /><field column="TaskMachineName" fieldcaption="TaskMachineName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0febed42-1c4f-4abe-86f8-b2cb7cf9a25e" /><field column="TaskGUID" visible="false" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9783e58c-39eb-43c8-ae5a-90f11a6797e5" /></form>', '20100308 11:25:56', 1094, N'cms.WebFarmTask', 0, 0, 0, '44576ef8-a9b8-44e6-ba9d-4643f7de17cf', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_WebFarmTask', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WebFarmTask">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TaskID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="TaskType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskTextData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskBinaryData" type="xs:base64Binary" minOccurs="0" />
              <xs:element name="TaskCreated" type="xs:dateTime" minOccurs="0" />
              <xs:element name="TaskEnabled" type="xs:boolean" minOccurs="0" />
              <xs:element name="TaskTarget" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskMachineName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WebFarmTask" />
      <xs:field xpath="TaskID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'DocumentName', NULL, N'Root', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form></form>', '20100510 13:16:23', 1095, N'CMS.Root', 1, 0, 0, 'a585aea3-10b5-4b74-9aad-747fcce72493', N'', NULL, 1, N'DocumentCreatedWhen', N'', N'', N'DocumentContent', 0, 0, N'', N'', 0, N'<search></search>', 0)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'ForumGroup', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="GroupID" fieldcaption="GroupID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="50672b5d-28e7-4921-bd0e-531d708afbaa" /><field column="GroupSiteID" fieldcaption="GroupSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43e2efce-a5a9-4a0d-b6b4-64e44431bb89" /><field column="GroupName" fieldcaption="GroupName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="39177fe2-d6d5-47ef-80b4-f40c81543e7f" /><field column="GroupDisplayName" fieldcaption="GroupDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="992f03b3-2866-48b1-ad27-7f4d60346a4c" /><field column="GroupOrder" fieldcaption="GroupOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c72aa204-d1e2-452f-b210-6b5abadbe7e0" /><field column="GroupDescription" fieldcaption="GroupDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="eb32d397-4c3c-4dc6-a2a5-613980598049" /><field column="GroupGUID" fieldcaption="GroupGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e752f0f3-3a4a-401a-b10f-077c2ae9baa8" /><field column="GroupLastModified" fieldcaption="GroupLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3fe0188c-52cc-44aa-8eee-a152d8ae2a0b" /><field column="GroupBaseUrl" fieldcaption="GroupBaseUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="c244605b-0c2b-4c2e-b9dc-cd07122c911b" /><field column="GroupUnsubscriptionUrl" fieldcaption="GroupUnsubscriptionUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="026dd06a-ec26-425d-be05-ec62ca2d63f3" /><field column="GroupGroupID" fieldcaption="GroupGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ddb4b979-4274-4305-95b5-c2f17aff74a9" /><field column="GroupAuthorEdit" fieldcaption="GroupAuthorEdit" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="90fff474-5d5f-4c9b-a373-86aa3e3179c8" /><field column="GroupAuthorDelete" fieldcaption="GroupAuthorDelete" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="52355416-f982-40e5-b0df-d335d75527d0" /><field column="GroupType" fieldcaption="GroupType" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e3ec2d3d-65f4-430a-8447-3503ce4a9423" /><field column="GroupIsAnswerLimit" fieldcaption="GroupIsAnswerLimit" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="16dd655d-8417-45fa-82d2-15ffc7166c73" /><field column="GroupImageMaxSideSize" fieldcaption="GroupImageMaxSideSize" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f8629a16-3009-4650-9a5a-3bcc3503f9cd" /><field column="GroupDisplayEmails" fieldcaption="GroupDisplayEmails" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="68f84af3-38cf-40e7-93a3-69d95945aafa" /><field column="GroupRequireEmail" fieldcaption="GroupRequireEmail" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="79eaf33a-37e4-498c-8691-b3af44f0d0dc" /><field column="GroupHTMLEditor" fieldcaption="GroupHTMLEditor" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9f4bddb6-ef29-4a93-b7c0-1e9e3b504245" /><field column="GroupUseCAPTCHA" fieldcaption="GroupUseCAPTCHA" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2300337e-0edc-4adf-94c7-5867e9b39c02" /><field column="GroupAttachmentMaxFileSize" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2e00eb00-2368-45ab-bd0e-e9af4652d095" /><field column="GroupDiscussionActions" fieldcaption="GroupDiscussionActions" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a156aa0a-4df4-4efb-b7c6-69e5307d9985" /></form>', '20100205 13:31:55', 1121, N'Forums.ForumGroup', 0, 0, 0, 'ae01ac82-45ae-441f-85e3-fda3e45ef85f', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Forums_ForumGroup', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Forums_ForumGroup">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="GroupID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="GroupSiteID" type="xs:int" />
              <xs:element name="GroupName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GroupDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GroupOrder" type="xs:int" minOccurs="0" />
              <xs:element name="GroupDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GroupGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="GroupLastModified" type="xs:dateTime" />
              <xs:element name="GroupBaseUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GroupUnsubscriptionUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="GroupGroupID" type="xs:int" minOccurs="0" />
              <xs:element name="GroupAuthorEdit" type="xs:boolean" minOccurs="0" />
              <xs:element name="GroupAuthorDelete" type="xs:boolean" minOccurs="0" />
              <xs:element name="GroupType" type="xs:int" minOccurs="0" />
              <xs:element name="GroupIsAnswerLimit" type="xs:int" minOccurs="0" />
              <xs:element name="GroupImageMaxSideSize" type="xs:int" minOccurs="0" />
              <xs:element name="GroupDisplayEmails" type="xs:boolean" minOccurs="0" />
              <xs:element name="GroupRequireEmail" type="xs:boolean" minOccurs="0" />
              <xs:element name="GroupHTMLEditor" type="xs:boolean" minOccurs="0" />
              <xs:element name="GroupUseCAPTCHA" type="xs:boolean" minOccurs="0" />
              <xs:element name="GroupAttachmentMaxFileSize" type="xs:int" minOccurs="0" />
              <xs:element name="GroupDiscussionActions" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Forums_ForumGroup" />
      <xs:field xpath="GroupID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Forum', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ForumID" fieldcaption="ForumID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="a5a72580-8e59-4c28-9ed1-68260005d5ec" /><field column="ForumGroupID" fieldcaption="ForumGroupID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="da47afe9-4cee-4122-a356-fdf161909434" /><field column="ForumName" fieldcaption="ForumName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="d44035ca-f757-4b21-ab86-2496940def23" /><field column="ForumDisplayName" fieldcaption="ForumDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="cbcfba2f-7033-418e-a983-207674c0b40c" /><field column="ForumDescription" fieldcaption="ForumDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="80033e42-d650-4b5e-87fe-1ee020efc671" /><field column="ForumOrder" fieldcaption="ForumOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1ace9535-c2d2-4bc9-90cc-a777926a7190" /><field column="ForumDocumentID" fieldcaption="ForumDocumentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4f5d483c-a9d4-4be0-b997-3cf8172c434a" /><field column="ForumOpen" fieldcaption="ForumOpen" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="29d695a8-f165-44c0-977c-bfe1952e4864" /><field column="ForumModerated" fieldcaption="ForumModerated" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2a84972b-9988-4560-8fd9-bd60fcd05e76" /><field column="ForumDisplayEmails" fieldcaption="ForumDisplayEmails" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="401fe8d8-bd7a-4671-aa42-9188b881b9af" /><field column="ForumRequireEmail" fieldcaption="ForumRequireEmail" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="036c72c8-c97b-41d8-9f15-a3550a2c8dd7" /><field column="ForumAccess" fieldcaption="ForumAccess" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4e1fe9df-0be3-41ab-8418-45481e35d136" /><field column="ForumThreads" fieldcaption="ForumThreads" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="56fa4909-066d-4135-b56f-4c2e751d57c3" /><field column="ForumPosts" fieldcaption="ForumPosts" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e866e09f-15cc-43e2-b16c-31c643ddf8e6" /><field column="ForumLastPostTime" fieldcaption="ForumLastPostTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="50bf7d4f-258a-4f24-8bed-9272753c7b5a" /><field column="ForumLastPostUserName" fieldcaption="ForumLastPostUserName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="68e87a3c-eb53-4f1e-aa59-0dde3dba94b0" visibility="none" ismacro="false" /><field column="ForumBaseUrl" fieldcaption="ForumBaseUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="f2789b2b-0187-45a3-b51d-ebd2b01b58fa" /><field column="ForumAllowChangeName" fieldcaption="ForumAllowChangeName" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a1b4cee6-4eff-41a6-9925-8167361bfbb5" /><field column="ForumHTMLEditor" fieldcaption="ForumHTMLEditor" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fa99d5b2-81da-4d20-988a-84fd8dfeb08f" /><field column="ForumUseCAPTCHA" fieldcaption="ForumUseCAPTCHA" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="efc0612a-7161-42e5-b4e9-77fbd182b2e8" /><field column="ForumGUID" fieldcaption="ForumGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="626e3139-da8d-4d78-8d0b-217a5ba4c66d" /><field column="ForumLastModified" fieldcaption="ForumLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e6af3cf1-871c-434d-a8ea-ca33c21bb372" /><field column="ForumUnsubscriptionUrl" fieldcaption="ForumUnsubscriptionUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="0a317c54-44e3-4f35-8762-9b83d2bb64dc" /><field column="ForumIsLocked" fieldcaption="ForumIsLocked" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1e79145e-05e7-45f6-bddb-d3040108b386" /><field column="ForumSettings" fieldcaption="ForumSettings" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6460cdfc-8e5c-4286-b5ba-5b40f1ca6e6e" /><field column="ForumAuthorEdit" fieldcaption="ForumAuthorEdit" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7128e991-75dd-4730-9a81-ab995016d9b7" /><field column="ForumAuthorDelete" fieldcaption="ForumAuthorDelete" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0c03904c-0369-4ff4-ae26-603433572553" /><field column="ForumType" fieldcaption="ForumType" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="65c15999-91c6-48ed-9b85-58fbd76e2168" /><field column="ForumIsAnswerLimit" fieldcaption="ForumIsAnswerLimit" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="71f079d1-33ad-43ba-9803-f05d121ce042" /><field column="ForumImageMaxSideSize" fieldcaption="ForumImageMaxSideSize" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d9dd0e96-655b-4bdd-9358-92bbb755eeb8" /><field column="ForumLastPostTimeAbsolute" fieldcaption="ForumLastPostTimeAbsolute" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ff990f14-94d5-4a78-ae1a-e0a462d689d7" /><field column="ForumLastPostUserNameAbsolute" fieldcaption="ForumLastPostUserNameAbsolute" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="10473eee-e635-4929-9a58-d06f4940114f" /><field column="ForumPostsAbsolute" fieldcaption="ForumPostsAbsolute" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4bd75ccf-9dfc-426d-8ce8-bcbe4f7638a0" /><field column="ForumThreadsAbsolute" fieldcaption="ForumThreadsAbsolute" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b86bebb0-db09-421e-92d3-fe5acb10838f" /><field column="ForumAttachmentMaxFileSize" visible="false" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d4909e25-1604-4dd6-89f5-3a44f697b9c3" /><field column="ForumDiscussionActions" fieldcaption="ForumDiscussionActions" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b7fa1080-90f2-4f08-a7d2-75417dea8550" /><field column="ForumSiteID" fieldcaption="Forum site ID" visible="false" columntype="integer" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9dd53caf-bf89-4ec7-92a9-3414595e9649" visibility="none" /></form>', '20100217 11:42:21', 1122, N'Forums.Forum', 0, 0, 0, '32a7f4fb-dfa5-4394-a120-6af690f0f2e2', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Forums_Forum', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Forums_Forum">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ForumID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ForumGroupID" type="xs:int" />
              <xs:element name="ForumName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ForumDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ForumDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ForumOrder" type="xs:int" minOccurs="0" />
              <xs:element name="ForumDocumentID" type="xs:int" minOccurs="0" />
              <xs:element name="ForumOpen" type="xs:boolean" />
              <xs:element name="ForumModerated" type="xs:boolean" />
              <xs:element name="ForumDisplayEmails" type="xs:boolean" minOccurs="0" />
              <xs:element name="ForumRequireEmail" type="xs:boolean" minOccurs="0" />
              <xs:element name="ForumAccess" type="xs:int" />
              <xs:element name="ForumThreads" type="xs:int" />
              <xs:element name="ForumPosts" type="xs:int" />
              <xs:element name="ForumLastPostTime" type="xs:dateTime" minOccurs="0" />
              <xs:element name="ForumLastPostUserName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ForumBaseUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ForumAllowChangeName" type="xs:boolean" minOccurs="0" />
              <xs:element name="ForumHTMLEditor" type="xs:boolean" minOccurs="0" />
              <xs:element name="ForumUseCAPTCHA" type="xs:boolean" minOccurs="0" />
              <xs:element name="ForumGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ForumLastModified" type="xs:dateTime" />
              <xs:element name="ForumUnsubscriptionUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ForumIsLocked" type="xs:boolean" minOccurs="0" />
              <xs:element name="ForumSettings" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ForumAuthorEdit" type="xs:boolean" minOccurs="0" />
              <xs:element name="ForumAuthorDelete" type="xs:boolean" minOccurs="0" />
              <xs:element name="ForumType" type="xs:int" minOccurs="0" />
              <xs:element name="ForumIsAnswerLimit" type="xs:int" minOccurs="0" />
              <xs:element name="ForumImageMaxSideSize" type="xs:int" minOccurs="0" />
              <xs:element name="ForumLastPostTimeAbsolute" type="xs:dateTime" minOccurs="0" />
              <xs:element name="ForumLastPostUserNameAbsolute" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ForumPostsAbsolute" type="xs:int" minOccurs="0" />
              <xs:element name="ForumThreadsAbsolute" type="xs:int" minOccurs="0" />
              <xs:element name="ForumAttachmentMaxFileSize" type="xs:int" minOccurs="0" />
              <xs:element name="ForumDiscussionActions" type="xs:int" minOccurs="0" />
              <xs:element name="ForumSiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Forums_Forum" />
      <xs:field xpath="ForumID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'PostSubject', NULL, N'ForumPost', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="PostId" fieldcaption="PostId" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="00a5f4ac-2e67-465b-bd5c-e6848c10559a" /><field column="PostForumID" fieldcaption="PostForumID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="924d3b8c-3110-48b4-961a-08bae5b5adc0" /><field column="PostParentID" fieldcaption="PostParentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7ec61a89-dd67-4694-8734-6887ad02b162" /><field column="PostIDPath" fieldcaption="PostIDPath" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="3f10250c-688d-4f5e-8b6d-68e5055872e3" /><field column="PostLevel" fieldcaption="PostLevel" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a5f89ef5-0988-402e-996e-e93409d0b22a" /><field column="PostSubject" fieldcaption="PostSubject" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="fc8be85a-2904-4098-a7c0-35ea946ae16d" /><field column="PostUserID" fieldcaption="PostUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6e8e10ba-6ff2-4393-b6cc-37d4e3a3687b" /><field column="PostUserName" fieldcaption="PostUserName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="8373dd18-fe7b-4ead-a3b6-9c295605385f" visibility="none" ismacro="false" /><field column="PostUserMail" fieldcaption="PostUserMail" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="c2213d50-2c8f-41b0-9151-41f0b84f37db" /><field column="PostText" fieldcaption="PostText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="33c6eb92-51f3-420d-82cd-5e3b4f54361a" /><field column="PostTime" fieldcaption="PostTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="95aa7b00-47d2-4119-af78-6ba0003eede2" /><field column="PostApprovedByUserID" fieldcaption="PostApprovedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ac69c3a5-53a2-4113-b78c-31d41d34868a" /><field column="PostThreadPosts" fieldcaption="PostThreadPosts" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6290317c-5293-44e1-81fb-b85c85f69f60" /><field column="PostThreadLastPostUserName" fieldcaption="PostThreadLastPostUserName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="17985059-abff-4992-a38d-0ce54ff426dd" visibility="none" ismacro="false" /><field column="PostThreadLastPostTime" fieldcaption="PostThreadLastPostTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="989327c7-6997-4a50-a234-10ec7a5ad132" /><field column="PostUserSignature" fieldcaption="PostUserSignature" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="47a93b85-74db-447b-a10f-2df627566203" /><field column="PostGUID" fieldcaption="PostGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b98f1f62-a46e-450b-b61d-68b13e0bff63" /><field column="PostLastModified" fieldcaption="PostLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b0d89e7c-6427-4f90-9ef9-1cd819cc3669" /><field column="PostApproved" fieldcaption="PostApproved" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0bb49d7c-346d-4da4-ac4e-70cd971fec5d" /><field column="PostIsLocked" fieldcaption="PostIsLocked" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2560b6ed-9156-4b51-81d6-8b44165c8334" /><field column="PostIsAnswer" fieldcaption="PostIsAnswer" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ac84a32c-33eb-4404-88cd-843cd5f4475a" /><field column="PostStickOrder" fieldcaption="PostStickOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5dcc65fd-dba4-4e7f-b899-b5cfcbbe23e6" /><field column="PostViews" fieldcaption="PostViews" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c586bf90-5b68-488d-a6a6-6bda0f1a6e27" /><field column="PostLastEdit" fieldcaption="PostLastEdit" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a00788a0-53ec-4f73-bf9a-47f7c1fa1940" /><field column="PostInfo" fieldcaption="PostInfo" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a879820c-e3cd-4467-82a6-c4d28f15e637" /><field column="PostAttachmentCount" fieldcaption="PostAttachmentCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="71cc9ff2-e54e-4536-8248-3205e7320931" defaultvalue="0" /><field column="PostType" fieldcaption="PostType" visible="true" columntype="integer" fieldtype="radiobuttons" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9157042f-693a-4e03-9b2c-dcc84404f34a"><settings><repeatdirection>vertical</repeatdirection><options><item value="0" text="Normal" /><item value="1" text="Answer" /></options></settings></field><field column="PostThreadPostsAbsolute" fieldcaption="PostThreadPostsAbsolute" visible="false" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7470dc26-3ee4-4027-bdc1-aedd64b35715" /><field column="PostThreadLastPostUserNameAbsolute" fieldcaption="PostThreadLastPostUserNameAbsolute" visible="false" columntype="text" fieldtype="label" allowempty="true" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="17951f5f-5f61-4cc8-b63b-82f58815c826" /><field column="PostThreadLastPostTimeAbsolute" fieldcaption="PostThreadLastPostTimeAbsolute" visible="false" columntype="datetime" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0af83503-acaf-47a1-b6f6-19c85308a9fb" /><field column="PostQuestionSolved" fieldcaption="PostQuestionSolved" visible="true" defaultvalue="false" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6e4af8f2-aae7-4df1-b977-2cd341ada9f0" /><field column="PostIsNotAnswer" fieldcaption="PostIsNotAnswer" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="34fe9c34-2c65-456b-a285-fcbc84b7650a" /></form>', '20100414 08:22:22', 1123, N'Forums.ForumPost', 0, 0, 0, 'f40c961c-3d47-4e6a-997e-b127db2520c1', N'', NULL, 0, N'PostTime', N'', N'', N'PostText', 0, 0, N'Forums_ForumPost', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Forums_ForumPost">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="PostId" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="PostForumID" type="xs:int" />
              <xs:element name="PostParentID" type="xs:int" minOccurs="0" />
              <xs:element name="PostIDPath">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PostLevel" type="xs:int" />
              <xs:element name="PostSubject">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PostUserID" type="xs:int" minOccurs="0" />
              <xs:element name="PostUserName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PostUserMail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PostText" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PostTime" type="xs:dateTime" />
              <xs:element name="PostApprovedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="PostThreadPosts" type="xs:int" minOccurs="0" />
              <xs:element name="PostThreadLastPostUserName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PostThreadLastPostTime" type="xs:dateTime" minOccurs="0" />
              <xs:element name="PostUserSignature" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PostGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="PostLastModified" type="xs:dateTime" />
              <xs:element name="PostApproved" type="xs:boolean" minOccurs="0" />
              <xs:element name="PostIsLocked" type="xs:boolean" minOccurs="0" />
              <xs:element name="PostIsAnswer" type="xs:int" minOccurs="0" />
              <xs:element name="PostStickOrder" type="xs:int" />
              <xs:element name="PostViews" type="xs:int" minOccurs="0" />
              <xs:element name="PostLastEdit" type="xs:dateTime" minOccurs="0" />
              <xs:element name="PostInfo" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PostAttachmentCount" type="xs:int" minOccurs="0" />
              <xs:element name="PostType" type="xs:int" minOccurs="0" />
              <xs:element name="PostThreadPostsAbsolute" type="xs:int" minOccurs="0" />
              <xs:element name="PostThreadLastPostUserNameAbsolute" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="PostThreadLastPostTimeAbsolute" type="xs:dateTime" minOccurs="0" />
              <xs:element name="PostQuestionSolved" type="xs:boolean" minOccurs="0" />
              <xs:element name="PostIsNotAnswer" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Forums_ForumPost" />
      <xs:field xpath="PostId" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'<search><item tokenized="False" name="PostId" content="False" searchable="True" id="e4795a52-a0c3-4040-b4d9-07a57294e05b" /><item tokenized="False" name="PostForumID" content="False" searchable="True" id="075d7293-802f-476d-aac6-af0abae45816" /><item tokenized="False" name="PostParentID" content="False" searchable="True" id="4bac1bd7-1885-497a-8f15-0fc6015dfa93" /><item tokenized="False" name="PostIDPath" content="False" searchable="True" id="76d587b1-0711-419e-ac79-a4305a3e1c38" /><item tokenized="False" name="PostLevel" content="False" searchable="True" id="c515659a-41e8-4e0d-9bfd-a8706a4cec54" /><item tokenized="True" name="PostSubject" content="True" searchable="False" id="d8fdc784-f5d5-412c-8885-b4c4ff0c5974" /><item tokenized="False" name="PostUserID" content="False" searchable="True" id="0f9a0b1e-731c-4432-bd1b-d9bc1bdddae2" /><item tokenized="True" name="PostUserName" content="True" searchable="False" id="c5939d48-d9cb-4d74-bfe3-5e5d4e4b200f" /><item tokenized="True" name="PostUserMail" content="True" searchable="False" id="32dde000-eed5-4091-8bc9-8acca85ac1b3" /><item tokenized="True" name="PostText" content="True" searchable="False" id="20650fce-9586-4dac-87e2-042eabd757f8" /><item tokenized="False" name="PostTime" content="False" searchable="True" id="e3c5b2c8-00cc-43b0-b1cd-16cae14e6102" /><item tokenized="False" name="PostApprovedByUserID" content="False" searchable="True" id="781b680e-5ee9-485a-a260-2652245919df" /><item tokenized="False" name="PostThreadPosts" content="False" searchable="True" id="75949b92-d698-4053-99e4-43a277a9ec1c" /><item tokenized="False" name="PostThreadLastPostUserName" content="False" searchable="False" id="c55984d7-8f15-40ca-bd4b-8b152eb3ed7a" /><item tokenized="False" name="PostThreadLastPostTime" content="False" searchable="False" id="acd19d37-9350-4661-b2b8-fdd16cde9050" /><item tokenized="False" name="PostUserSignature" content="False" searchable="False" id="6235fc61-99c0-4b32-8759-e87c981e9a65" /><item tokenized="False" name="PostGUID" content="False" searchable="False" id="c16877d3-a7d2-48e4-86d2-c39014f324db" /><item tokenized="False" name="PostLastModified" content="False" searchable="True" id="4270f74e-7c3b-405c-b953-4eb710cdb86f" /><item tokenized="False" name="PostApproved" content="False" searchable="True" id="ef64c5f6-6b82-4070-b814-4dbd575b29e2" /><item tokenized="False" name="PostIsLocked" content="False" searchable="True" id="84eb64c7-849d-4621-bc08-84d693b98cab" /><item tokenized="False" name="PostIsAnswer" content="False" searchable="True" id="88c1b2a0-4a86-42c4-ac35-54955c146b83" /><item tokenized="False" name="PostStickOrder" content="False" searchable="True" id="c610c446-b9bb-4e09-a9b9-c9cf0cf47bb3" /><item tokenized="False" name="PostViews" content="False" searchable="True" id="3133a944-d966-472a-8c87-9e28865eb634" /><item tokenized="False" name="PostLastEdit" content="False" searchable="True" id="5b7c2424-423a-4367-9053-1fb5751e4be8" /><item tokenized="False" name="PostInfo" content="False" searchable="False" id="a6d1e097-2511-4e4f-8a65-a18c9c7f0701" /><item tokenized="False" name="PostAttachmentCount" content="False" searchable="True" id="fb76529e-3521-4987-b1f5-9c2b22cf2afe" /><item tokenized="False" name="PostType" content="False" searchable="True" id="6fbf9437-e4ce-4636-88d3-5a5225d5da58" /><item tokenized="False" name="PostThreadPostsAbsolute" content="False" searchable="False" id="e1e78549-804d-486d-808d-0d9088dd4c9f" /><item tokenized="False" name="PostThreadLastPostUserNameAbsolute" content="False" searchable="False" id="703bb080-71c4-41ab-bcca-ad88374bc5ef" /><item tokenized="False" name="PostThreadLastPostTimeAbsolute" content="False" searchable="False" id="34738a31-7dbb-45b8-af3d-7249fb54857b" /><item tokenized="False" name="PostQuestionSolved" content="False" searchable="True" id="abacdb5a-597a-4e0d-882c-b4f86c15adb4" /><item tokenized="False" name="PostIsNotAnswer" content="False" searchable="True" id="ef75eb43-e6da-448d-b7bc-714b44e6f5ea" /></search>', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'ForumSubscription', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="SubscriptionID" fieldcaption="SubscriptionID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="3fc5afd4-d80f-4c8e-bff1-203798e7142a" /><field column="SubscriptionUserID" fieldcaption="SubscriptionUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="dd5652e2-0d24-4e79-bbca-41508456c966" /><field column="SubscriptionEmail" fieldcaption="SubscriptionEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="01531690-2ab8-4ef5-97a3-db33bf517da4" /><field column="SubscriptionForumID" fieldcaption="SubscriptionForumID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1f53c06d-dfed-46f0-b77e-7ce173d0f2e5" /><field column="SubscriptionPostID" fieldcaption="SubscriptionPostID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="aafad6cf-698b-41f7-97b7-21d4988bd61a" /><field column="SubscriptionGUID" fieldcaption="SubscriptionGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="446281b6-cf2d-4f6a-95a1-bda6552a2b74" /><field column="SubscriptionLastModified" fieldcaption="SubscriptionLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f417d26b-a105-4dbd-848a-7451dcb6d6b7" /></form>', '20090922 10:15:59', 1124, N'Forums.ForumSubscription', 0, 0, 0, 'c1ddbb88-8e9a-4b77-b7c1-cb331ed94083', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Forums_ForumSubscription', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Forums_ForumSubscription">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SubscriptionID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SubscriptionUserID" type="xs:int" minOccurs="0" />
              <xs:element name="SubscriptionEmail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SubscriptionForumID" type="xs:int" />
              <xs:element name="SubscriptionPostID" type="xs:int" minOccurs="0" />
              <xs:element name="SubscriptionGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="SubscriptionLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Forums_ForumSubscription" />
      <xs:field xpath="SubscriptionID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Country', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="CountryID" fieldcaption="CountryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="1c2b75ec-e2ec-4c27-b9f3-b1ad4c86747b" /><field column="CountryDisplayName" fieldcaption="CountryDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="714fce0f-86fb-4b48-84bd-7e028eecf62d" /><field column="CountryName" fieldcaption="CountryName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7360fc6f-444b-4293-a60e-678920a9635f" /><field column="CountryGUID" fieldcaption="CountryGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="eafdf9a4-1213-4ab7-ad24-e67684cfa34e" /><field column="CountryLastModified" fieldcaption="CountryLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="28acb0c7-a1e6-4b19-9037-b53147d9cf02" /></form>', '20091103 08:49:17', 1125, N'cms.country', 0, 0, 0, '7e651b6d-e59d-4d72-93c3-2d3adb2a6c6b', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'cms_country', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="cms_country">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CountryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="CountryDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CountryName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CountryGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="CountryLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//cms_country" />
      <xs:field xpath="CountryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'State', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="StateID" fieldcaption="StateID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="9d25f72b-8b2f-4635-a0c0-49f1a46369ce" /><field column="StateDisplayName" fieldcaption="StateDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="153f4f19-df99-4cb5-8089-cdfc25260a4f" /><field column="StateName" fieldcaption="StateName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f773552d-012b-4d94-9935-dab01da9b535" /><field column="StateCode" fieldcaption="StateCode" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="33e36f31-a6ad-4e19-a83c-b4d900cc07db" /><field column="CountryID" fieldcaption="CountryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c76ec985-2879-4e92-aebd-edf82909e58b" /><field column="StateGUID" fieldcaption="StateGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="216850cd-1080-4bc7-a794-2c0f2fa848a0" /><field column="StateLastModified" fieldcaption="StateLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="014db439-c6cd-4774-9a1b-d2243fdff59c" /></form>', '20090903 12:41:30', 1126, N'cms.state', 0, 0, 0, '6d8f4d1f-3ac1-4388-94d6-14d68e972f5d', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_State', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_State">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="StateID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="StateDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StateName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StateCode" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CountryID" type="xs:int" />
              <xs:element name="StateGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="StateLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_State" />
      <xs:field xpath="StateID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Staging - synchronization', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="SynchronizationID" fieldcaption="SynchronizationID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="a98996fd-850a-48a3-862e-ed43833d3780" /><field column="SynchronizationTaskID" fieldcaption="SynchronizationTaskID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="82d4f3ab-07fb-42ff-9a7c-5f398e1c52cd" /><field column="SynchronizationServerID" fieldcaption="SynchronizationServerID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5ed8bcb4-d204-46e7-b6e3-9a282fc888ea" /><field column="SynchronizationLastRun" fieldcaption="SynchronizationLastRun" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f85de1a4-a651-4150-97f5-8af2c850964c" /><field column="SynchronizationErrorMessage" fieldcaption="SynchronizationErrorMessage" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0a3c3f13-ce60-45e4-8276-539e56d79972" /></form>', '20100415 14:32:30', 1131, N'staging.synchronization', 0, 0, 0, '235368da-5b9f-4a38-b0f6-b8baf22ecd0f', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Staging_Synchronization', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Staging_Synchronization">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SynchronizationID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SynchronizationTaskID" type="xs:int" />
              <xs:element name="SynchronizationServerID" type="xs:int" />
              <xs:element name="SynchronizationLastRun" type="xs:dateTime" />
              <xs:element name="SynchronizationErrorMessage" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Staging_Synchronization" />
      <xs:field xpath="SynchronizationID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Staging - server', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ServerID" fieldcaption="ServerID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="adc25c7d-4b9d-4272-9bb5-83577cf832d3" /><field column="ServerName" fieldcaption="ServerName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="56e84c75-7ef5-46ac-b851-792532748a56" /><field column="ServerDisplayName" fieldcaption="ServerDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ccbfd081-f598-4bcd-86c5-0b0659d1d823" columnsize="440" visibility="none" ismacro="false" /><field column="ServerSiteID" fieldcaption="ServerSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3a5aac85-f6a7-462c-995c-f87088fccdc7" /><field column="ServerURL" fieldcaption="ServerURL" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5e032c90-c2ab-43d8-bb9c-9c43ebfcddc8" /><field column="ServerEnabled" fieldcaption="ServerEnabled" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4726b612-34b9-464b-8a35-17ef4acc9c26" /><field column="ServerAuthentication" fieldcaption="ServerAuthentication" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="df28276a-53b0-4899-b35e-3bce1bf0cfb8" /><field column="ServerUsername" fieldcaption="ServerUsername" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7d0a229d-7ac4-4377-aaf4-953e995ed9a8" /><field column="ServerPassword" fieldcaption="ServerPassword" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b2bb8d87-584d-4e88-bff1-ebc6e96eff4a" /><field column="ServerX509ClientKeyID" fieldcaption="ServerX509ClientKeyID" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2c24dc63-7eed-4fc9-a343-24c89827ce81" /><field column="ServerX509ServerKeyID" fieldcaption="ServerX509ServerKeyID" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8b2d88a3-111f-4a03-8128-e31abaff562f" /><field column="ServerGUID" fieldcaption="ServerGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="85c57a10-f71b-4bcc-903a-8e55bd1038a2" /><field column="ServerLastModified" fieldcaption="ServerLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="40a0bfef-1603-4a94-90c0-d48e769f3fe7" /></form>', '20100325 18:30:23', 1132, N'staging.server', 0, 0, 0, 'ae6f2aaa-9dbc-47f4-b365-91167e71bbd0', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'staging_server', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="staging_server">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ServerID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ServerName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="440" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerSiteID" type="xs:int" />
              <xs:element name="ServerURL">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerEnabled" type="xs:boolean" />
              <xs:element name="ServerAuthentication">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="20" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerUsername" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerPassword" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerX509ClientKeyID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerX509ServerKeyID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ServerGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ServerLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//staging_server" />
      <xs:field xpath="ServerID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Staging - synclog', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="SyncLogID" fieldcaption="SyncLogID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="73496e9f-301f-4c88-ade6-a71805827354" /><field column="SyncLogTaskID" fieldcaption="SyncLogTaskID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b5fca3fc-d6cf-4bde-8932-6977593b6805" /><field column="SyncLogServerID" fieldcaption="SyncLogServerID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3ca24d4b-3a08-4dea-ba9a-740d761c6510" /><field column="SyncLogTime" fieldcaption="SyncLogTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7c7c95c2-b5ee-4bc3-b86f-17428f87b3d5" /><field column="SyncLogError" fieldcaption="SyncLogError" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a694b613-4fe6-4cda-a30b-55c553e9e3f0" /></form>', '20091012 07:52:16', 1133, N'staging.synclog', 0, 0, 0, 'abf6512d-23fe-4657-a767-0417f41cf96b', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'staging_synclog', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="staging_synclog">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SyncLogID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SyncLogTaskID" type="xs:int" />
              <xs:element name="SyncLogServerID" type="xs:int" />
              <xs:element name="SyncLogTime" type="xs:dateTime" />
              <xs:element name="SyncLogError" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//staging_synclog" />
      <xs:field xpath="SyncLogID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Staging - task', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="TaskID" fieldcaption="TaskID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="5a76cc96-89cf-4a50-94e6-7514e2d49fdc" /><field column="TaskSiteID" fieldcaption="TaskSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fa037649-546c-4e8c-b6ba-b58233ac4cfb" /><field column="TaskDocumentID" fieldcaption="TaskDocumentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="229ffc3b-f78f-4982-a159-fd0789c9875e" /><field column="TaskNodeID" fieldcaption="TaskNodeID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="df505950-031a-4d7d-bfa7-0f0dbea0a732" visibility="none" ismacro="false" /><field column="TaskNodeAliasPath" fieldcaption="TaskNodeAliasPath" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="44f99d9f-cf85-4f4e-9f6a-b38466e32e4c" /><field column="TaskTitle" fieldcaption="TaskTitle" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ec039fe8-3da7-4c0f-a8f7-4f8d8c501eea" /><field column="TaskData" fieldcaption="TaskData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="582a8ed2-17d6-46e7-af57-d1552a5905c4" /><field column="TaskTime" fieldcaption="TaskTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fe5c0b3e-381b-4813-896f-8a24119c8912" /><field column="TaskType" fieldcaption="TaskType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4c934e50-ede0-429b-9243-7951ae0c7022" /><field column="TaskObjectType" fieldcaption="TaskObjectType" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ca7447f8-e178-4ef1-8a8a-d373c2364422" /><field column="TaskObjectID" fieldcaption="TaskObjectID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c27c108d-ecda-45ce-a085-fce64d313eed" /><field column="TaskRunning" fieldcaption="TaskRunning" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="17eafbfa-0395-4cc3-9ad3-0afdbea9c19e" /></form>', '20100322 15:43:49', 1134, N'staging.task', 0, 0, 0, 'a79db106-b9f8-44ee-a172-f8c77403aebb', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Staging_Task', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Staging_Task">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TaskID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="TaskSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="TaskDocumentID" type="xs:int" minOccurs="0" />
              <xs:element name="TaskNodeAliasPath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskTitle">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskData">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="2147483647" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskTime" type="xs:dateTime" />
              <xs:element name="TaskType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskObjectType" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TaskObjectID" type="xs:int" minOccurs="0" />
              <xs:element name="TaskRunning" type="xs:boolean" minOccurs="0" />
              <xs:element name="TaskNodeID" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Staging_Task" />
      <xs:field xpath="TaskID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
set identity_insert [cms_class] off
