<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DocumentGroupSelector.ascx.cs"
    Inherits="CMSAdminControls_UI_Selectors_DocumentGroupSelector" %>
<asp:TextBox ID="txtGroups" runat="server" EnableViewState="false" ReadOnly="true"
    CssClass="SelectorTextBox" /><cms:LocalizedButton ID="btnChange" runat="server" ResourceString="general.change"
    CssClass="ContentButton" />
<asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
<cms:CMSButton runat="server" ID="btnHidden" CssClass="HiddenButton" OnClick="btnHidden_Click" />
