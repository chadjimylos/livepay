using System;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_WebParts_Development_WebPart_Edit_Header : SiteManagerPage
{
    protected int webpartid;

    /// <summary>
    /// Page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        webpartid = QueryHelper.GetInteger("webpartid", 0);

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }
        
        WebPartInfo wpi = WebPartInfoProvider.GetWebPartInfo(webpartid);

        if (wpi != null)
        {
            string currentWebPartName = wpi.WebPartDisplayName;

            // Initialize master page
            InitializeMasterPage(currentWebPartName);
        }
    }


    /// <summary>
    /// Initialize master page
    /// </summary>
    private void InitializeMasterPage(string currentWebPartName)
    {
        this.Title = "WebParts - List";

        // Set the master page title
        this.CurrentMaster.Title.HelpTopicName = "general_tab17";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Initializes page title
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Development-WebPart_Edit.WebParts");
        pageTitleTabs[0, 1] = ResolveUrl("WebPart_Frameset.aspx");
        pageTitleTabs[0, 2] = "frameMain";
        pageTitleTabs[1, 0] = currentWebPartName;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// Initializes edit menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string[,] tabs = new string[4, 4];
        tabs[0, 0] = ResHelper.GetString("General.General");
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab17');";
        tabs[0, 2] = "WebPart_Edit_General.aspx?webpartid=" + webpartid;
        tabs[1, 0] = ResHelper.GetString("WebParts.Properties");
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'properties_tab2');";
        tabs[1, 2] = "WebPart_Edit_Properties.aspx?webpartid=" + webpartid;
        tabs[2, 0] = ResHelper.GetString("WebParts.Layout");
        tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'layout_tab');";
        tabs[2, 2] = "WebPart_Edit_Layout.aspx?webpartid=" + webpartid;
        tabs[3, 0] = ResHelper.GetString("WebParts.Documentation");
        tabs[3, 1] = ""; // "SetHelpTopic('helpTopic', 'documentation_tab');";
        tabs[3, 2] = "WebPart_Edit_Documentation.aspx?webpartid=" + webpartid;

        this.CurrentMaster.Tabs.UrlTarget = "webparteditcontent";
        this.CurrentMaster.Tabs.Tabs = tabs;
    }
}
