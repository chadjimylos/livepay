CREATE TABLE [CMS_CssStylesheetSite] (
		[StylesheetID]     int NOT NULL,
		[SiteID]           int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_CssStylesheetSite]
	ADD
	CONSTRAINT [PK_CMS_CssStylesheetSite]
	PRIMARY KEY
	([StylesheetID], [SiteID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_CssStylesheetSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_CssStylesheetSite_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_CssStylesheetSite]
			ADD CONSTRAINT [FK_CMS_CssStylesheetSite_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[CMS_CssStylesheetSite]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_CssStylesheet]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_CssStylesheetSite_StylesheetID_CMS_CssStylesheet]') IS NULL
BEGIN
		ALTER TABLE [CMS_CssStylesheetSite]
			ADD CONSTRAINT [FK_CMS_CssStylesheetSite_StylesheetID_CMS_CssStylesheet]
			FOREIGN KEY ([StylesheetID]) REFERENCES [CMS_CssStylesheet] ([StylesheetID])
END
