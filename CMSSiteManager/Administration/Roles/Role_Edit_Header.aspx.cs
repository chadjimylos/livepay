using System;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.CMSHelper;

public partial class CMSSiteManager_Administration_Roles_Role_Edit_Header : CMSRolesPage
{
    private int roleId = 0;
    private int siteId = 0;
    protected RoleInfo role = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        roleId = QueryHelper.GetInteger("roleid", 0);

        string roleListUrl = "~/CMSSiteManager/Administration/Roles/Role_List.aspx";

        if (!string.IsNullOrEmpty(Request.QueryString["selectedsiteid"]))
        {
            siteId = GetSiteID( QueryHelper.GetString("selectedsiteid", ""));
            roleListUrl += "?selectedsiteid=" + siteId;
        }
        else if (!string.IsNullOrEmpty(Request.QueryString["siteid"]))
        {
            siteId = GetSiteID(QueryHelper.GetString("siteid", ""));
            roleListUrl += "?siteid=" + siteId;
        }

        string currentRole = "";
        role = RoleInfoProvider.GetRoleInfo(roleId);
        if (role != null)
        {
            currentRole = role.DisplayName;
        }

        // Initialize PageTitle breadcrumbs
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("general.roles");
        pageTitleTabs[0, 1] = roleListUrl;
        pageTitleTabs[0, 2] = "_parent";
        pageTitleTabs[1, 0] = currentRole;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        CurrentMaster.Title.TitleText = ResHelper.GetString("Administration-Role_Edit.Title");
        CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Role/object.png");
        CurrentMaster.Title.HelpTopicName = "general_tab9";
        CurrentMaster.Title.HelpName = "title";

        // Register script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ShowContent", ScriptHelper.GetScript("function ShowContent(contentLocation) { parent.frames['content'].location.href= contentLocation; }"));

        // Tabs
        InitalizeTabs();
    }


    /// <summary>
    /// Initializes tabs
    /// </summary>
    protected void InitalizeTabs()
    {
        string roleName = "";
        if(role != null)
        {
            roleName = role.RoleName ;
        }

        bool displayUsers = ((roleName != RoleInfoProvider.EVERYONE) && (roleName != RoleInfoProvider.AUTHENTICATED) && (roleName != RoleInfoProvider.NOTAUTHENTICATED));

        string generalString = ResHelper.GetString("general.general");
        string usersString = ResHelper.GetString("general.users");

        string[,] tabs = new string[2, 4];
        tabs[0, 0] = generalString;
        tabs[0, 1] = ""; // "SetHelpTopic('title', 'general_tab9');";
        tabs[0, 2] = "Role_Edit_General.aspx?roleid=" + roleId+"&siteId="+siteId;
        if (displayUsers)
        {
            tabs[1, 0] = usersString;
            tabs[1, 1] = ""; // "SetHelpTopic('title', 'users_tab');";
            tabs[1, 2] = "Role_Edit_Users.aspx?roleid=" + roleId + "&siteId=" + siteId;
        }
        CurrentMaster.Tabs.UrlTarget = "content";
        CurrentMaster.Tabs.Tabs = tabs;
    }
}