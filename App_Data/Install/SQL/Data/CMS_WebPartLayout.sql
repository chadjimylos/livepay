SET IDENTITY_INSERT [CMS_WebPartLayout] ON
INSERT INTO [CMS_WebPartLayout] ([WebPartLayoutGUID], [WebPartLayoutDescription], [WebPartLayoutCode], [WebPartLayoutCheckedOutByUserID], [WebPartLayoutCodeName], [WebPartLayoutCheckedOutFilename], [WebPartLayoutLastModified], [WebPartLayoutVersionGUID], [WebPartLayoutDisplayName], [WebPartLayoutID], [WebPartLayoutWebPartID], [WebPartLayoutCheckedOutMachineName]) VALUES ('3239ff22-011a-4bc9-b35d-7d967dcc7b47', N'', N'<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Search/cmscompletesearchdialog.ascx.cs" Inherits="CMSWebParts_Search_cmscompletesearchdialog" %>
<div class="SearchDialog">
    <cms:CMSSearchDialog ID="srchDialog" runat="server" />
</div>
<div class="SearchResults">
    <cms:CMSSearchResults ID="srchResults" runat="server" FilterName="SearchDialog" />
</div>', NULL, N'filter', NULL, '20080819 17:14:18', N'3a647b0a-e59f-48d1-a11f-9da026413835', N'filter', 121, 173, NULL)
INSERT INTO [CMS_WebPartLayout] ([WebPartLayoutGUID], [WebPartLayoutDescription], [WebPartLayoutCode], [WebPartLayoutCheckedOutByUserID], [WebPartLayoutCodeName], [WebPartLayoutCheckedOutFilename], [WebPartLayoutLastModified], [WebPartLayoutVersionGUID], [WebPartLayoutDisplayName], [WebPartLayoutID], [WebPartLayoutWebPartID], [WebPartLayoutCheckedOutMachineName]) VALUES ('c023897f-17cf-46d1-a71f-ff9104fd23f5', N'', N'<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Ecommerce/ShoppingCart/ShoppingCartMiniPreviewWebPart.ascx.cs"
    Inherits="CMSWebParts_Ecommerce_ShoppingCart_ShoppingCartMiniPreviewWebPart" %>
<table cellspacing="0">
    <tr>
        <td colspan="3" style="padding-right: 5px; height:38px; padding-left:15px;">
<asp:Literal runat="server" ID="ltlRTLFix" text="<%# rtlFix %>" EnableViewState="false"></asp:Literal> 
<asp:PlaceHolder ID="plcShoppingCart" runat="server" EnableViewState="false">
                <asp:HyperLink ID="lnkShoppingCart" runat="server" CssClass="ShoppingCartLink"></asp:HyperLink>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="plcMyAccount" runat="server" EnableViewState="false">|&nbsp;<asp:HyperLink ID="lnkMyAccount"
                runat="server" CssClass="ShoppingCartLink"></asp:HyperLink>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="plcMyWishlist" runat="server" EnableViewState="false">|&nbsp;<asp:HyperLink ID="lnkMyWishlist"
                runat="server" CssClass="ShoppingCartLink"></asp:HyperLink>
            </asp:PlaceHolder>
        </td>
    </tr>
    <asp:PlaceHolder ID="plcTotalPrice" runat="server" EnableViewState="false">
    <tr>
	<td align="left" style="padding-left:15px; width:22%;">
	    <asp:Image ID="imgCartIcon" runat="server" CssClass="ShoppingCartIcon" />
	</td>
        <td align="left" style="font-weight:bold;">
            <asp:Label ID="lblTotalPriceTitle" runat="server" Text="" CssClass="SmallTextLabel"></asp:Label>
        </td>
        <td align="right" style="padding-right: 5px;font-weight:bold;">
            <asp:Label ID="lblTotalPriceValue" runat="server" Text="" CssClass="SmallTextLabel"></asp:Label>
        </td>
    </tr>
    </asp:PlaceHolder>
</table>', NULL, N'EcommerceSite', N'', '20100317 08:50:12', N'5deb91a2-e255-48d9-9d59-6c67d91b0b11', N'Ecommerce site', 115, 221, N'')
INSERT INTO [CMS_WebPartLayout] ([WebPartLayoutGUID], [WebPartLayoutDescription], [WebPartLayoutCode], [WebPartLayoutCheckedOutByUserID], [WebPartLayoutCodeName], [WebPartLayoutCheckedOutFilename], [WebPartLayoutLastModified], [WebPartLayoutVersionGUID], [WebPartLayoutDisplayName], [WebPartLayoutID], [WebPartLayoutWebPartID], [WebPartLayoutCheckedOutMachineName]) VALUES ('100f8305-87b9-4d28-bd8f-08bd1d9497a5', N'', N'<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/Membership/Logon/LogonMiniForm.ascx.cs"
    Inherits="CMSWebParts_Membership_Logon_LogonMiniForm" %>
<asp:Login ID="loginElem" runat="server" DestinationPageUrl="~/Default.aspx" EnableViewState="false">
    <LayoutTemplate>
        <asp:Panel ID="pnlLogonMiniForm" runat="server" DefaultButton="btnLogon" EnableViewState="false">
            <cms:LocalizedLabel ID="lblUserName" runat="server" AssociatedControlID="UserName" EnableViewState="false" />
            <asp:TextBox ID="UserName" runat="server" CssClass="LogonField" EnableViewState="false" />
            <asp:RequiredFieldValidator ID="rfvUserNameRequired" runat="server" ControlToValidate="UserName"
                ValidationGroup="Login1" Display="Dynamic" EnableViewState="false">*</asp:RequiredFieldValidator>
            <cms:LocalizedLabel ID="lblPassword" runat="server" AssociatedControlID="Password" EnableViewState="false" />
            <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="LogonField" EnableViewState="false" />
            <cms:LocalizedButton ID="btnLogon" ResourceString="LogonForm.LogOnButton" runat="server" CommandName="Login" ValidationGroup="Login1" EnableViewState="false" />
            <asp:ImageButton ID="btnImageLogon" runat="server" Visible="false" CommandName="Login"
                ValidationGroup="Login1" EnableViewState="false" /><br />
            <asp:Label ID="FailureText" CssClass="ErrorLabel" runat="server" EnableViewState="false" />
        </asp:Panel>
    </LayoutTemplate>
</asp:Login>', NULL, N'Block', NULL, '20100420 09:51:10', N'2246e216-1a2f-4fe8-b004-fcdcc249e5ea', N'Block', 124, 525, NULL)
SET IDENTITY_INSERT [CMS_WebPartLayout] OFF
