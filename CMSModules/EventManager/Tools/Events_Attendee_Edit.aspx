<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Events_Attendee_Edit.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Attendee details"
    Inherits="CMSModules_EventManager_Tools_Events_Attendee_Edit" Theme="Default" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <table style="vertical-align: top">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblFirstName" AssociatedControlID="txtFirstName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtFirstName" runat="server" CssClass="TextBoxField" MaxLength="100" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblLastName" AssociatedControlID="txtLastName" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtLastName" runat="server" CssClass="TextBoxField" MaxLength="100" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <cms:LocalizedLabel runat="server" ID="lblEmail" AssociatedControlID="txtEmail" EnableViewState="false"
                    ResourceString="general.email" DisplayColon="true" />
            </td>
            <td>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="TextBoxField" MaxLength="250" />
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" AssociatedControlID="txtPhone" ID="lblPhone" EnableViewState="false" />
            </td>
            <td>
                <asp:TextBox ID="txtPhone" runat="server" CssClass="TextBoxField" MaxLength="50" />&nbsp;
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <cms:CMSButton runat="server" ID="btnOk" EnableViewState="false" CssClass="SubmitButton"
                    OnClick="btnOK_Click" />
            </td>
        </tr>
    </table>

    <script type="text/javascript">
    //<![CDATA[
        // Refresh parent frame header
        function RefreshHeader(attendeeId, duplicit)
        {
            parent.frames['eventsHeader'].location.replace(parent.frames['eventsHeader'].location);
            location.replace(location + "&attendeeId=" + attendeeId + "&saved=1&duplicit=" + duplicit);
        }
    //]]>
    </script>

    <asp:Literal ID="ltlScript" runat="server" />
</asp:Content>
