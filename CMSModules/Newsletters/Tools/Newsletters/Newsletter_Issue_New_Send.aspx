<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Newsletter_Issue_New_Send.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_New_Send"
    Theme="default" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master"
    Title="Tools - Send new newsletter issue" %>

<%@ Register Src="~/CMSAdminControls/Wizard/Header.ascx" TagName="WizardHeader" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">

    <script type="text/javascript">
        //<![CDATA[
        function RefreshPage() {
            wopener.RefreshPage();
        }
        //]]>
    </script>

    <div style="padding: 10px;">
        <table class="GlobalWizard NewsletterWizard" border="0" cellpadding="0" cellspacing="0">
            <tr class="Top">
                <td class="Left">
                    &nbsp;
                </td>
                <td class="Center">
                    <cms:WizardHeader ID="ucHeader" runat="server" />
                </td>
                <td class="Right">
                    &nbsp;
                </td>
            </tr>
            <tr class="Middle">
                <td class="Center" colspan="3">
                    <div id="wzdBody">
                        <table class="Wizard" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse;">
                            <tbody>
                                <tr style="height: 100%;">
                                    <td>
                                        <div class="NewsletterWizardStep">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
                                                            Visible="false" />
                                                        <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
                                                            Visible="false" />
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:RadioButton ID="radSendNow" runat="server" GroupName="Send" AutoPostBack="true"
                                                                        OnCheckedChanged="radGroupSend_CheckedChanged" TextAlign="Right" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 25px;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RadioButton ID="radSchedule" runat="server" GroupName="Send" AutoPostBack="true"
                                                                        OnCheckedChanged="radGroupSend_CheckedChanged" />
                                                                    <div class="UnderRadioContent">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <cms:LocalizedLabel ID="lblDateTime" runat="server" AssociatedControlID="calendarControl"
                                                                                        ResourceString="Newsletter_Issue_Send.DateTime" />
                                                                                </td>
                                                                                <td>
                                                                                    <cms:DateTimePicker ID="calendarControl" runat="server" Enabled="false" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 25px;">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RadioButton ID="radSendLater" runat="server" GroupName="Send" AutoPostBack="true"
                                                                        OnCheckedChanged="radGroupSend_CheckedChanged" TextAlign="Right" />
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:Literal ID="ltlScript" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ButtonRow">
                                        <div id="buttonsDiv">
                                            <cms:CMSButton ID="btnFinish" runat="server" CssClass="SubmitButton" OnClick="btnFinish_Click" />
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            <tr class="Bottom">
                <td class="Left">
                    &nbsp;
                </td>
                <td class="Center">
                    &nbsp;
                </td>
                <td class="Right">
                    &nbsp;
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="cntFooter" runat="server" ContentPlaceHolderID="plcFooter">
    <div class="FloatRight">
        <cms:CMSButton ID="btnClose" runat="server" CssClass="SubmitButton" OnClientClick="window.close();RefreshPage();return false;" />
    </div>
</asp:Content>
