<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WaitingForApproval.aspx.cs"
    Inherits="CMSDesk_MyDesk_WaitingForApproval_WaitingForApproval" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="MyDesk - Documents waiting for my approval" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <cms:UniGrid ID="gridWaitingDocs" runat="server" GridName="WaitingForApproval_List.xml"
        OrderBy="DocumentName" IsLiveSite="false" />

    <script type="text/javascript">
        //<![CDATA[
        // Select item action
        function SelectItem(nodeId, culture) {
            if (nodeId != 0) {
                parent.parent.location.href = "../../default.aspx?section=content&action=edit&nodeid=" + nodeId + "&culture=" + culture;
            }
        }
        //]]>
    </script>

</asp:Content>
