<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Categories.aspx.cs" Inherits="CMSModules_Content_CMSDesk_Properties_Categories"
    Theme="Default" %>

<%@ Register Src="~/CMSAdminControls/Categories/MultipleCategoriesSelector.ascx" TagName="MultipleCategoriesSelector"
    TagPrefix="cms" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Categories</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>
</head>
<body class="VerticalTabsBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="pnlBody" CssClass="VerticalTabsPageBody">
        <asp:Panel runat="server" ID="pnlTab" CssClass="VerticalTabsPageContent">
            <asp:Panel runat="server" ID="pnlMenu" CssClass="ContentEditMenu">
                <table width="100%">
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkSave" runat="server" OnClick="lnkSave_Click" CssClass="MenuItemEdit">
                                <cms:CMSImage ID="imgSave" runat="server" />
                                <%=mSave%>
                            </asp:LinkButton>
                        </td>
                        <td class="TextRight">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent" Visible="false">
                <asp:Label ID="lblCategoryInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />
            </asp:Panel>
            <asp:Panel ID="pnlUserCatgerories" runat="server" Style="padding: 0px 10px;">
                <cms:MultipleCategoriesSelector ID="categoriesElem" runat="server" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    </form>
</body>
