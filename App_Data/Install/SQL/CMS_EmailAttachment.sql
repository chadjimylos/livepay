CREATE TABLE [CMS_EmailAttachment] (
		[AttachmentID]               int NOT NULL IDENTITY(1, 1),
		[AttachmentName]             nvarchar(255) NOT NULL,
		[AttachmentExtension]        nvarchar(50) NOT NULL,
		[AttachmentSize]             int NOT NULL,
		[AttachmentMimeType]         nvarchar(100) NOT NULL,
		[AttachmentBinary]           varbinary(max) NOT NULL,
		[AttachmentGUID]             uniqueidentifier NOT NULL,
		[AttachmentLastModified]     datetime NOT NULL,
		[AttachmentContentID]        nvarchar(255) NULL,
		[AttachmentSiteID]           int NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_EmailAttachment]
	ADD
	CONSTRAINT [PK_CMS_EmailAttachment]
	PRIMARY KEY
	([AttachmentID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
