CREATE TABLE [CMS_EventLog] (
		[EventID]              int NOT NULL IDENTITY(1, 1),
		[EventType]            nvarchar(5) NOT NULL,
		[EventTime]            datetime NOT NULL,
		[Source]               nvarchar(100) NOT NULL,
		[EventCode]            nvarchar(100) NOT NULL,
		[UserID]               int NULL,
		[UserName]             nvarchar(250) NULL,
		[IPAddress]            nvarchar(100) NULL,
		[NodeID]               int NULL,
		[DocumentName]         nvarchar(100) NULL,
		[EventDescription]     nvarchar(max) NULL,
		[SiteID]               int NULL,
		[EventUrl]             nvarchar(2000) NULL,
		[EventMachineName]     nvarchar(100) NULL,
		[EventUserAgent]       nvarchar(max) NULL,
		[EventUrlReferrer]     nvarchar(2000) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_EventLog]
	ADD
	CONSTRAINT [PK_CMS_EventLog]
	PRIMARY KEY
	NONCLUSTERED
	([EventID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_EventLog]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Event_NodeID_CMS_Tree]') IS NULL
BEGIN
		ALTER TABLE [CMS_EventLog]
			ADD CONSTRAINT [FK_CMS_Event_NodeID_CMS_Tree]
			FOREIGN KEY ([NodeID]) REFERENCES [CMS_Tree] ([NodeID])
END
IF OBJECT_ID(N'[CMS_EventLog]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Event_SiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_EventLog]
			ADD CONSTRAINT [FK_CMS_Event_SiteID_CMS_Site]
			FOREIGN KEY ([SiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[CMS_EventLog]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Event_UserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [CMS_EventLog]
			ADD CONSTRAINT [FK_CMS_Event_UserID_CMS_User]
			FOREIGN KEY ([UserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE CLUSTERED INDEX [IX_CMS_EventLog_EventTime]
	ON [CMS_EventLog] ([EventTime] DESC)
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_EventLog_NodeID]
	ON [CMS_EventLog] ([NodeID])
	ON [PRIMARY]
CREATE INDEX [IX_CMS_EventLog_SiteID_EventType_Source_EventCode]
	ON [CMS_EventLog] ([SiteID], [EventType], [Source], [EventCode])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_EventLog_UserID]
	ON [CMS_EventLog] ([UserID])
	ON [PRIMARY]
