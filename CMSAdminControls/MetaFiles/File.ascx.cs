using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FileManager;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSAdminControls_MetaFiles_File : CMSUserControl
{
    private bool mAlreadyUploadedDontDelete = false;

    #region "Properties"

    /// <summary>
    /// After metafile delete event
    /// </summary>
    public event EventHandler OnAfterDelete;


    /// <summary>
    /// Object id
    /// </summary>
    public int ObjectID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["ObjectID"], 0);
        }
        set
        {
            ViewState["ObjectID"] = value;
        }
    }


    /// <summary>
    /// Object type
    /// </summary>
    public string ObjectType
    {
        get
        {
            return ValidationHelper.GetString(ViewState["ObjectType"], "");
        }
        set
        {
            ViewState["ObjectType"] = value;
        }
    }


    /// <summary>
    /// Site name
    /// </summary>
    public int SiteID
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["SiteID"], 0);
        }
        set
        {
            ViewState["SiteID"] = value;
        }
    }


    /// <summary>
    /// Attachment category/group
    /// </summary>
    public string Category
    {
        get
        {
            return ValidationHelper.GetString(ViewState["Category"], "");
        }
        set
        {
            ViewState["Category"] = value;
        }
    }


    /// <summary>
    /// Returns true if saving of the file failed
    /// </summary>
    public bool SavingFailed
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["SavingFailed"], false);
        }
    }


    /// <summary>
    /// Returns true if deleting of the file failed
    /// </summary>
    public bool DeletingFailed
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["DeletingFailed"], false);
        }
    }


    /// <summary>
    /// Indicates if uploader is enabled
    /// </summary>
    public bool Enabled
    {
        get
        {
            return uploader.Enabled;
        }
        set
        {
            uploader.Enabled = value;
        }
    }


    /// <summary>
    /// Returns the currently posted file or null when no file posted
    /// </summary>
    public HttpPostedFile PostedFile
    {
        get
        {
            return uploader.PostedFile;
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Inits uploaded file name based on file name and GUID.
    /// </summary>
    /// <param name="fileName">File name</param>
    /// <param name="fileGuid">File GUID</param>
    public void InitUploader(string fileName, Guid fileGuid)
    {
        uploader.CurrentFileName = System.IO.Path.GetFileName(fileName);
        uploader.CurrentFileUrl = "~/CMSPages/GetMetaFile.aspx?fileguid=" + fileGuid.ToString();
    }


    /// <summary>
    /// Inits uploaded file name based on current properties.
    /// </summary>
    public void InitUploader()
    {
        uploader.CurrentFileName = String.Empty;
        uploader.CurrentFileUrl = String.Empty;

        if ((this.ObjectID > 0) && (this.ObjectType != "") && (this.Category != ""))
        {
            DataSet ds = MetaFileInfoProvider.GetMetaFiles(this.ObjectID, this.ObjectType, this.Category, null, null);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                MetaFileInfo mfi = new MetaFileInfo(ds.Tables[0].Rows[0]);
                InitUploader(mfi.MetaFileName, mfi.MetaFileGUID);

                // If the file is image display uploader's action button and allow image editing
                // Only global admin can edit metafile images
                if ((ImageHelper.IsSupportedByImageEditor(mfi.MetaFileExtension)) && (CMSContext.CurrentUser != null) && (CMSContext.CurrentUser.IsGlobalAdministrator))
                {
                    // Register dialog script
                    ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

                    // Dialog for selectiong roles
                    ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "OpenEditor",
                        ScriptHelper.GetScript("function OpenEditor(metafileGUID) { " +
                            "modalDialog('" + UrlHelper.GetFullApplicationUrl() + "/CMSModules/Content/CMSDesk/Edit/ImageEditor.aspx?metafileguid=' + metafileGUID, 'imageEditorDialog', 905, 670); " +
                            "return false; } "));

                    // Setup uploader's action button - it opens image editor when clicked
                    uploader.ActionButton.Attributes.Add("onclick", "OpenEditor('" + mfi.MetaFileGUID.ToString() + "'); return false;");
                    uploader.ActionButton.ToolTip = ResHelper.GetString("general.editimage");
                    uploader.ShowActionButton = true;
                }
                else
                {
                    // Hide uploader's action button
                    uploader.ShowActionButton = false;
                }
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Visible = false;
        lblError.Text = String.Empty;
        if (uploader.Enabled)
        {
            uploader.OnUploadFile += new EventHandler(uploader_OnUploadFile);
            uploader.OnDeleteFile += new EventHandler(uploader_OnDeleteFile);
        }

        InitUploader();
    }


    void uploader_OnUploadFile(object sender, EventArgs e)
    {
        UploadFile();
    }


    void uploader_OnDeleteFile(object sender, EventArgs e)
    {
        // Careful with upload and delete in on postback - ignore delete request
        if (mAlreadyUploadedDontDelete)
        {
            return;
        }

        try
        {
            DataSet ds = MetaFileInfoProvider.GetMetaFiles(ObjectID, ObjectType, Category, null, null);

            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    MetaFileInfo mfi = new MetaFileInfo(dr);
                    if ((mfi != null) && (mfi.MetaFileName.ToLower() == uploader.CurrentFileName.ToLower()))
                    {
                        MetaFileInfoProvider.DeleteMetaFileInfo(mfi.MetaFileID);
                    }
                }
            }
            // Execute after delete event
            if (OnAfterDelete != null)
            {
                OnAfterDelete(this, null);
            }

            InitUploader();
        }
        catch (Exception ex)
        {
            ViewState["DeletingFailed"] = true;
            lblError.Visible = true;
            lblError.Text = ex.Message;
            InitUploader();
        }
    }


    /// <summary>
    /// Uploads file.
    /// </summary>
    public void UploadFile()
    {
        if ((uploader.PostedFile != null) && (this.ObjectID > 0))
        {
            try
            {
                MetaFileInfo existing = null;

                // Check if uploaded file already exists and delete it
                DataSet ds = MetaFileInfoProvider.GetMetaFiles(this.ObjectID, this.ObjectType, this.Category, null, null);
                if (!DataHelper.DataSourceIsEmpty(ds))
                {
                    // Get existing record ID and delete it
                    existing = new MetaFileInfo(ds.Tables[0].Rows[0]);
                    MetaFileInfoProvider.DeleteMetaFileInfo(existing);
                }

                // Create new meta file
                MetaFileInfo mfi = new MetaFileInfo(uploader.PostedFile, this.ObjectID, this.ObjectType, this.Category);
                if (existing != null)
                {
                    // Preserve GUID
                    mfi.MetaFileGUID = existing.MetaFileGUID;
                }
                mfi.MetaFileSiteID = this.SiteID;

                // Save to the database
                MetaFileInfoProvider.SetMetaFileInfo(mfi);

                InitUploader();
            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message;
                ViewState["SavingFailed"] = true;
                InitUploader();
            }

            // File was uploaded, do not delete in one postback
            mAlreadyUploadedDontDelete = true;
        }
    }


    /// <summary>
    /// Clears the content (file name & file URL) of the control
    /// </summary>
    public void ClearControl()
    {
        this.uploader.CurrentFileName = "";
        this.uploader.CurrentFileUrl = "";
    }

    #endregion
}
