<%@ Page Language="C#" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="CMSSiteManager_Administration_RecycleBin_default"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" MaintainScrollPositionOnPostback="true"
    Title="Site Manager - Recycle bin" %>

<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSSiteManager/Administration/RecycleBin/Controls/RecycleBin.ascx"
    TagName="RecycleBin" TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcBeforeContent" runat="server">
    <asp:Panel runat="server" ID="pnlBody">
        <asp:Panel ID="pnlSiteSelector" runat="server" CssClass="PageHeaderLine">
            <asp:Label ID="lblSite" runat="server" />
            <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" />
        </asp:Panel>
        <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <cms:RecycleBin ID="recycleBin" runat="server" />
            </ContentTemplate>
        </cms:CMSUpdatePanel>
    </asp:Panel>
</asp:Content>
