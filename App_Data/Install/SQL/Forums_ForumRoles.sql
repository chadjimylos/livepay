CREATE TABLE [Forums_ForumRoles] (
		[ForumID]          int NOT NULL,
		[RoleID]           int NOT NULL,
		[PermissionID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Forums_ForumRoles]
	ADD
	CONSTRAINT [PK_Forums_ForumRoles]
	PRIMARY KEY
	([ForumID], [RoleID], [PermissionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Forums_ForumRoles]') IS NOT NULL
	AND OBJECT_ID(N'[Forums_Forum]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumRoles_ForumID_Forums_Forum]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumRoles]
			ADD CONSTRAINT [FK_Forums_ForumRoles_ForumID_Forums_Forum]
			FOREIGN KEY ([ForumID]) REFERENCES [Forums_Forum] ([ForumID])
END
IF OBJECT_ID(N'[Forums_ForumRoles]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Permission]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumRoles_PermissionID_CMS_Permission]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumRoles]
			ADD CONSTRAINT [FK_Forums_ForumRoles_PermissionID_CMS_Permission]
			FOREIGN KEY ([PermissionID]) REFERENCES [CMS_Permission] ([PermissionID])
END
IF OBJECT_ID(N'[Forums_ForumRoles]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Role]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Forums_ForumRoles_RoleID_CMS_Role]') IS NULL
BEGIN
		ALTER TABLE [Forums_ForumRoles]
			ADD CONSTRAINT [FK_Forums_ForumRoles_RoleID_CMS_Role]
			FOREIGN KEY ([RoleID]) REFERENCES [CMS_Role] ([RoleID])
END
