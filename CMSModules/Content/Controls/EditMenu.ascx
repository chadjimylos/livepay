<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EditMenu.ascx.cs" Inherits="CMSModules_Content_Controls_EditMenu" %>
<%@ Register TagPrefix="cms" Namespace="CMS.UIControls" Assembly="CMS.UIControls" %>
<%@ Register TagPrefix="cms" Namespace="CMS.skmMenuControl" Assembly="CMS.skmMenuControl" %>
<cms:Menu ID="menuElem" runat="server" EnableViewState="False" />
<asp:PlaceHolder runat="server" ID="plcScript">

    <script type="text/javascript" src="<%= ResolveUrl("~/CMSModules/Content/CMSDesk/Edit/editmenu.js") %>"></script>

</asp:PlaceHolder>
<cms:CMSButton ID="btnSave" runat="server" EnableViewState="false" OnClick="btnSave_Click" />
<cms:CMSButton ID="btnReject" runat="server" EnableViewState="false" OnClick="btnReject_Click" />
<cms:CMSButton ID="btnUndoCheckout" runat="server" EnableViewState="false" OnClick="btnUndoCheckout_Click" />
<cms:CMSButton ID="btnCheckOut" runat="server" EnableViewState="false" OnClick="btnCheckOut_Click" />
<cms:CMSButton ID="btnApprove" runat="server" EnableViewState="false" OnClick="btnApprove_Click" />
<cms:CMSButton ID="btnCheckIn" runat="server" EnableViewState="false" OnClick="btnCheckIn_Click" />
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />