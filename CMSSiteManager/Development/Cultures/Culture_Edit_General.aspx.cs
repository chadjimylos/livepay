using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_Cultures_Culture_Edit_General : SiteManagerPage
{
    protected int cultureId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblCultureName.Text = ResHelper.GetString("Culture_Edit_General.CultureName");
        lblCultureCode.Text = ResHelper.GetString("Culture_Edit_General.CultureCode");
        lblCultureShortName.Text = ResHelper.GetString("Culture_Edit_General.CultureShortName");
        btnOk.Text = ResHelper.GetString("general.ok");
        rfvCultureName.ErrorMessage = ResHelper.GetString("Culture_Edit_General.ErrorEmptyCultureName");
        rfvCultureCode.ErrorMessage = ResHelper.GetString("Culture_Edit_General.ErrorEmptyCultureCode");
        rfvCultureShortName.ErrorMessage = ResHelper.GetString("Culture_Edit_General.ErrorEmptyCultureShortName");

        cultureId = QueryHelper.GetInteger("cultureId", 0);
        if (!RequestHelper.IsPostBack())
        {
            LoadData();

            if (QueryHelper.GetBoolean("saved", false))
            {
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
        }
    }


    /// <summary>
    /// Handles btnOK's OnClick event - Update resource info
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // finds whether required fields are not empty
        string result = new Validator().NotEmpty(txtCultureName.Text, rfvCultureName.ErrorMessage).NotEmpty(txtCultureCode.Text, rfvCultureCode.ErrorMessage).Result;


        if (txtCultureCode.Text.Length > 10)
        {
            result = ResHelper.GetString("Culture.MaxLengthError");
        }

        try
        {
            System.Globalization.CultureInfo objekt = new System.Globalization.CultureInfo(txtCultureCode.Text);
        }
        catch
        {
            result = ResHelper.GetString("Culture.ErrorNoGlobalCulture");
        }

        if (result == "")
        {
            // finds if the culture code is unique
            CultureInfo ci = CultureInfoProvider.GetCultureInfo(txtCultureCode.Text);

            // if culture code already exists and it is just editing culture -> update
            if ((ci == null) || (ci.CultureID == cultureId))
            {
                UpdateCulture();
            }
            // if culture code already exists and it is another culture -> error
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Culture_New.CultureExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = result;
        }
    }


    /// <summary>
    /// Loads data of editing culture from DB into textboxes.
    /// </summary>
    protected void LoadData()
    {
        CultureInfo ci = CultureInfoProvider.GetCultureInfo(cultureId);

        txtCultureName.Text = ci.CultureName;
        txtCultureCode.Text = ci.CultureCode;
        txtCultureShortName.Text = ci.CultureShortName;
    }


    /// <summary>
    /// Update just editing culture
    /// </summary>
    protected void UpdateCulture()
    {
        CultureInfo ci = new CultureInfo();
        ci.CultureID = cultureId;
        ci.CultureName = txtCultureName.Text;
        ci.CultureCode = txtCultureCode.Text;
        ci.CultureShortName = txtCultureShortName.Text;
        CultureInfoProvider.SetCultureInfo(ci);

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}
