﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.FormControls;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.CMSHelper;
using CMS.ExtendedControls;

public partial class CMSModules_PortalEngine_Controls_WebParts_WebPartTree : CMSAdminControl
{
    #region "Variables"

    bool mSelectWebParts = false;
    bool mShowRecentlyUsed = false;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets whether webparts are shown in tree or not.
    /// </summary>
    public bool SelectWebParts
    {
        get
        {
            return this.mSelectWebParts;
        }
        set
        {
            this.mSelectWebParts = value;
        }
    }


    /// <summary>
    /// Gets or sets whether recently used link is shown or not.
    /// </summary>
    public bool ShowRecentlyUsed
    {
        get
        {
            return mShowRecentlyUsed;
        }
        set
        {
            mShowRecentlyUsed = value;
        }
    }


    /// <summary>
    /// Gets or sets selected item.
    /// </summary>
    public string SelectedItem
    {
        get
        {
            return treeElem.SelectedItem;
        }
        set
        {
            treeElem.SelectedItem = value;
        }
    }


    /// <summary>
    /// Gets or sets if use postback.
    /// </summary>
    public bool UsePostBack
    {
        get
        {
            return treeElem.UsePostBack;
        }
        set
        {
            treeElem.UsePostBack = value;
        }
    }


    /// <summary>
    /// Gets or sets select path.
    /// </summary>
    public string SelectPath
    {
        get
        {
            return treeElem.SelectPath;
        }
        set
        {
            treeElem.SelectPath = value;
            treeElem.ExpandPath = value;
        }
    }

    /// <summary>
    /// Indicates if the control should perform the operations
    /// </summary>
    public override bool StopProcessing
    {
        get
        {
            return base.StopProcessing;
        }
        set
        {
            base.StopProcessing = value;
            treeElem.StopProcessing = value;
        }
    }


    /// <summary>
    /// Indicates if control is used on live site
    /// </summary>
    public override bool IsLiveSite
    {
        get
        {
            return base.IsLiveSite;
        }
        set
        {
            base.IsLiveSite = value;
            treeElem.IsLiveSite = value;
        }
    }

    #endregion


    #region "Custom events"

    /// <summary>
    /// On selected item event handler.
    /// </summary>    
    public delegate void ItemSelectedEventHandler(string selectedValue);

    /// <summary>
    /// On selected item event handler.
    /// </summary>
    public event ItemSelectedEventHandler OnItemSelected;

    #endregion


    #region "Page and other events"

    /// <summary>
    /// Page_Load event.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            return;
        }

        // Create and set category provider
        UniTreeProvider categoryProvider = new UniTreeProvider();
        categoryProvider.DisplayNameColumn = "DisplayName";
        categoryProvider.IDColumn = "ObjectID";
        categoryProvider.LevelColumn = "ObjectLevel";
        categoryProvider.OrderColumn = "CategoryOrder";
        categoryProvider.ParentIDColumn = "ParentID";
        categoryProvider.PathColumn = "ObjectPath";
        categoryProvider.ValueColumn = "ObjectID";
        categoryProvider.ChildCountColumn = "CompleteChildCount";
        categoryProvider.QueryName = "cms.webpartcategory.selectallview";
        categoryProvider.ObjectTypeColumn = "ObjectType";
        categoryProvider.Columns = "DisplayName, ObjectID, ObjectLevel,CategoryOrder,ParentID, ObjectPath, CompleteChildCount,ObjectType,CategoryChildCount, CategoryImagePath";
        categoryProvider.ImageColumn = "CategoryImagePath";

        if (!SelectWebParts)
        {
            categoryProvider.WhereCondition = "ObjectType = 'webpartcategory'";
            categoryProvider.ChildCountColumn = "CategoryChildCount";
            categoryProvider.ObjectTypeColumn = "";
            treeElem.DefaultImagePath = GetImageUrl("Objects/CMS_WebPartCategory/list.png");
        }
        else
        {
            categoryProvider.OrderBy = "ObjectType DESC, DisplayName ASC";
            treeElem.OnGetImage += new CMSAdminControls_UI_Trees_UniTree.GetImageEventHandler(treeElem_OnGetImage);
        }

        // Set up tree 
        treeElem.ProviderObject = categoryProvider;        

        if (SelectWebParts)
        {
            treeElem.NodeTemplate = "<span id=\"##OBJECTTYPE##_##NODEID##\" onclick=\"SelectNode(##NODEID##,'##OBJECTTYPE##', ##PARENTNODEID##);\" name=\"treeNode\" class=\"ContentTreeItem\">##ICON## <span class=\"Name\">##NODENAME##</span></span>";
            treeElem.SelectedNodeTemplate = "<span id=\"##OBJECTTYPE##_##NODEID##\" onclick=\"SelectNode(##NODEID##,'##OBJECTTYPE##', ##PARENTNODEID##);\" name=\"treeNode\" class=\"ContentTreeItem ContentTreeSelectedItem\">##ICON## <span class=\"Name\">##NODENAME##</span></span>";
        }
        else
        {
            treeElem.NodeTemplate = "<span onclick=\"SelectNode(##NODEID##, this);\" class=\"ContentTreeItem\">##ICON## <span class=\"Name\">##NODENAME##</span></span>";
            treeElem.DefaultItemTemplate = "<span onclick=\"SelectNode('recentlyused', this);\" class=\"ContentTreeItem\">##ICON##<span class=\"Name\">##NODENAME##</span></span>";
            treeElem.SelectedDefaultItemTemplate = "<span onclick=\"SelectNode('recentlyused', this);\" class=\"ContentTreeItem ContentTreeSelectedItem\">##ICON##<span class=\"Name\">##NODENAME##</span></span>";
            treeElem.SelectedNodeTemplate = "<span onclick=\"SelectNode(##NODEID##, this);\" class=\"ContentTreeItem ContentTreeSelectedItem\">##ICON## <span class=\"Name\">##NODENAME##</span></span>";

            // Register jquery
            ScriptHelper.RegisterJQuery(this.Page);

            string js = "var selectedItem = $j('.ContentTreeSelectedItem');" +
                "function SelectNode(nodeid, sender){" +                
                "selectedItem.removeClass('ContentTreeSelectedItem'); " +
                "selectedItem.addClass('ContentTreeItem');" +
                "selectedItem = $j(sender);" +
                "selectedItem.removeClass('ContentTreeItem'); " +
                "selectedItem.addClass('ContentTreeSelectedItem'); " +
                "document.getElementById('" + this.treeElem.SelectedItemFieldId + "').value = nodeid;" +
                treeElem.GetOnSelectedItemBackEventReference() +
                "}";

            ScriptHelper.RegisterStartupScript(this.Page, typeof(string), "SelectTreeNode", ScriptHelper.GetScript(js));
        }

        // Add last recently used
        if (ShowRecentlyUsed)
        {
            treeElem.AddDefaultItem(ResHelper.GetString("webparts.recentlyused"), "recentlyused", ResolveUrl(GetImageUrl("Objects/CMS_WebPartCategory/recentlyused.png")));
        }

        // Setup event handler
        treeElem.OnItemSelected += new CMSAdminControls_UI_Trees_UniTree.ItemSelectedEventHandler(treeElem_OnItemSelected);
    }


    /// <summary>
    /// Page PreRender.
    /// </summary>
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (this.StopProcessing)
        {
            return;
        }

        // Load data
        if (!RequestHelper.IsPostBack())
        {
            treeElem.ReloadData();
        }
    }


    /// <summary>
    ///  On selected item event.
    /// </summary>
    /// <param name="selectedValue">Selected value.</param>
    protected void treeElem_OnItemSelected(string selectedValue)
    {
        if (OnItemSelected != null)
        {
            OnItemSelected(selectedValue);
        }
    }


    /// <summary>
    /// On get image event.
    /// </summary>
    /// <param name="node">Current node.</param>
    string treeElem_OnGetImage(UniTreeNode node)
    {
        if ((node != null) && (node.ItemData != null))
        {
            string objectType = string.Empty;

            DataRow dr = (DataRow)node.ItemData;
            if (dr != null)
            {
                objectType = ValidationHelper.GetString(dr["ObjectType"], "").ToLower();
            }

            // Return image path
            if (objectType == "webpart")
            {
                return GetImageUrl("Objects/CMS_WebPart/tree.png");
            }
            else if (objectType == "webpartcategory")
            {
                return GetImageUrl("Objects/CMS_WebPartCategory/list.png");
            }
        }
        return String.Empty;
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Reloads the tree data
    /// </summary>
    public override void ReloadData()
    {
        treeElem.ReloadData();
        base.ReloadData();
    }

    #endregion
}
