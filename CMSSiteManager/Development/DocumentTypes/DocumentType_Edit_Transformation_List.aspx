<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentType_Edit_Transformation_List.aspx.cs"
    Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_Transformation_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Document Type Edit - Transformation List" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/ClassTransformations.ascx" TagName="ClassTransformations" TagPrefix="cms" %>
    
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="plcContent">
    <cms:ClassTransformations ID="classTransformations" runat="server" />
</asp:Content>
