using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.FormControls;

public partial class CMSModules_Widgets_FormControls_SelectZoneType : FormEngineUserControl
{
    #region "Public properties"

    /// <summary>
    /// Gets or sets field value.
    /// </summary>
    public override object Value
    {
        get
        {
            EnsureChildControls();
            return rblOptions.SelectedValue;
        }
        set
        {
            EnsureChildControls();
            rblOptions.SelectedValue = ValidationHelper.GetString(value, "");
        }
    }

    #endregion


    #region "Methods"


    protected void Page_Load(object sender, EventArgs e)
    {
        EnsureChildControls();
    }


    protected override void CreateChildControls()
    {
        rblOptions.Items.Clear();

        ListItem li = null;

        li = new ListItem(ResHelper.GetString("widgets.zonetype.none"), "None");
        li.Attributes.Add("onclick", "ShowZoneTypeWarning();");
        rblOptions.Items.Add(li);

        li = new ListItem(ResHelper.GetString("widgets.zonetype.user"), "User");
        li.Attributes.Add("onclick", "ShowZoneTypeWarning();");
        rblOptions.Items.Add(li);

        li = new ListItem(ResHelper.GetString("widgets.zonetype.editor"), "Editor");
        li.Attributes.Add("onclick", "ShowZoneTypeWarning();");
        rblOptions.Items.Add(li);

        li = new ListItem(ResHelper.GetString("widgets.zonetype.group"), "Group");
        li.Attributes.Add("onclick", "ShowZoneTypeWarning();");
        rblOptions.Items.Add(li);
    }

    #endregion
}
