﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UC_ContactForm.ascx.vb" Inherits="CMSTemplates_LivePay_UC_ContactForm" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cms" %>

<%--<script>
    $(document).ready(function () {
        var ImageUrl = $('input[id$=btnOK]').attr("src")
        var LangType = $('label[id$=FullName_FieldLabel]').html()
        alert(LangType);
        if (LangType.toLowerCase().indexOf('fullname') > -1)
            $('input[id$=btnOK]').attr("src", "/app_themes/LivePay/Contact/btnSend_en-us.png")

    });
</script>--%>

<asp:ValidationSummary ID="ValidationSummary"
DisplayMode="List" CssClass="ErrorLabel" 
EnableClientScript="true"
runat="server" ValidationGroup="pnl_Contact"/>



<table>
    <tbody>
        <tr>
            <td class="ContactFieldTD"><asp:Label class="EditingFormLabel" runat="server" ID="lbl_FullName"></asp:Label>:</td>
            <td><div class="EditingFormControlNestedControl"><asp:TextBox runat="server" ID="FullName" CssClass="ContactTxtName" maxlength="100"></asp:TextBox></div></td>
            <td class="ContactStarTD">*</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="ContactErrorTD">
                <asp:RequiredFieldValidator ID="RequiredFullName" runat="server" ValidationGroup="pnl_Contact"
                            ControlToValidate="FullName" Display="Dynamic" EnableViewState="true" />
            </td>
        </tr>
        <tr>
            <td class="ContactFieldTD"><asp:Label runat="server" class="EditingFormLabel" ID="lbl_Email" Text="E-mail"></asp:Label>:</td>
            <td>
                <div class="EditingFormControlNestedControl">
				    <div class="ContactTxtEmail">
                        <asp:TextBox runat="server" ID="txtEmailInput" CssClass="ContactTxtEmail" maxlength="100"></asp:TextBox>
                    </div>
                </div>
            </td>
            <td class="ContactStarTD">*</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="ContactErrorTD">
                <asp:RegularExpressionValidator id="revtxtEmailInput" ValidationGroup="pnl_Contact" runat="server"
						ControlToValidate="txtEmailInput" ValidationExpression="^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$" Display="Dynamic" EnableViewState="true" /> 
                <asp:RequiredFieldValidator ID="RequiredtxtEmailInput" runat="server" ValidationGroup="pnl_Contact"
                            ControlToValidate="txtEmailInput" Display="Dynamic" EnableViewState="true" />
             
            </td>
        </tr>
        <tr>
            <td class="ContactFieldTD"><asp:Label runat="server" class="EditingFormLabel" ID="lbl_Category"></asp:Label>:</td>
            <td class="Contact-Category-TD">
                <div class="EditingFormControlNestedControl">
				    <asp:DropDownList CssClass="ContactDDL" runat="server" ID="Category"></asp:DropDownList>
			    </div>
            </td>
            <td class="ContactStarTD">*</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="ContactErrorTD">
                <asp:RequiredFieldValidator ID="rfvCategory" runat="server" 
                    ControlToValidate="Category" ValidationGroup="pnl_Contact" InitialValue="0" Display="Dynamic" EnableViewState="true"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="ContactFieldTD"><asp:Label runat="server" class="EditingFormLabel" ID="lbl_Descr"></asp:Label>:</td>
            <td>
                <div class="EditingFormControlNestedControl">
				    <asp:TextBox class="ContactTxtA" id="Descr" cols="20" rows="6" runat="server" TextMode="MultiLine"></asp:TextBox>
			    </div>
            </td>
            <td class="ContactStarTD">*</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2" class="ContactErrorTD">
                <asp:RequiredFieldValidator ID="RequiredDescr" runat="server" ValidationGroup="pnl_Contact"
                            ControlToValidate="Descr" Display="Dynamic" EnableViewState="true" />
            </td>
        </tr>
        <%-- Captchas --%>
        <tr>
            <td>&nbsp;</td>
            <td colspan="2">
            <asp:PlaceHolder runat="server" ID="plcCaptcha">
            <div>
                <div style="float:left" >
                    <asp:UpdatePanel runat="server" ID="RegUpdatePanel" UpdateMode="Always">
                        <ContentTemplate>
                            <cms:CaptchaControl ID="scCaptcha" runat="server" BorderWidth="1px" BorderColor="#3b6db4" BorderStyle="Solid"
	                            CaptchaBackgroundNoise="None"
	                            CaptchaLength="5" 
	                            CaptchaHeight="58"
	                            CaptchaWidth="236"
	                            Width="236"
	                            CaptchaLineNoise="Extreme" 
	                            CacheStrategy="HttpRuntime"
	                            CaptchaMaxTimeout="240" 
                            />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div style="float:left" >&nbsp;<asp:LinkButton ID="lnkRefresh" runat="server"><img border="0" alt="Close" src="/App_Themes/LivePay/UserRegistration/refresh.png"></asp:LinkButton></div> 
                <div class="Clear"></div>
            </div>
            <div style="height:30px;line-height:30px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;"><asp:Label ID="lblCaptcha" runat="server" AssociatedControlID="scCaptcha" EnableViewState="false"/></div>
                <div class="Clear"></div>
            </div>
             <div style="">
                <div style="float:left;">&nbsp;</div> 
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;">
                    <a id="CaptchaCloud" href="#"></a>
                    <asp:TextBox TabIndex="9" ID="txtCaptcha" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/>
                </div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">*</div>
                <div class="Clear"></div>
                <div class="ContactErrorTD">
                    <asp:Label runat="server" ID="lbl_CaptchaError" Visible="false"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredCaptcha" runat="server" ValidationGroup="pnl_Contact"
                            ControlToValidate="txtCaptcha" Display="Dynamic" EnableViewState="true"/>
                    <%--<asp:CustomValidator ID="UnRegUserCaptcha" ClientValidationFunction="RegValidationForm" ValidateEmptyText="true" runat="server" IsCaptcha="yes" ControlToValidate="txtCaptcha" text="*" ValidationGroup="pnl_Contact"/>--%>
                </div>
            </div>
            </asp:PlaceHolder>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="ContactComments"><asp:Label runat="server" ID="Comments"></asp:Label></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><asp:ImageButton runat="server" ID="btnOK" style="border-width:0px;" alt="OK" class="FormButton" ValidationGroup="pnl_Contact"/></td>
        </tr>
    </tbody>
</table>