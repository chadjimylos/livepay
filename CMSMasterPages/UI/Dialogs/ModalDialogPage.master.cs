using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using CMS.UIControls;

public partial class CMSMasterPages_UI_Dialogs_ModalDialogPage : CMSMasterPage
{
    #region "Properties"

    /// <summary>
    /// PageTitle control
    /// </summary>
    public override PageTitle Title
    {
        get
        {
            return titleElem;
        }
    }


    /// <summary>
    /// HeaderActions control
    /// </summary>
    public override HeaderActions HeaderActions
    {
        get
        {
            return actionsElem;
        }
    }


    /// <summary>
    /// Body panel
    /// </summary>
    public override Panel PanelBody
    {
        get
        {
            return pnlBody;
        }
    }


    /// <summary>
    /// Footer panel
    /// </summary>
    public override Panel PanelFooter
    {
        get
        {
            return pnlFooter;
        }
    }


    /// <summary>
    /// Body object
    /// </summary>
    public override HtmlGenericControl Body
    {
        get
        {
            return bodyElem;
        }
    }


    /// <summary>
    /// Prepared for specifying the additional HEAD elements
    /// </summary>
    public override Literal HeadElements
    {
        get
        {
            return ltlHeadElements;
        }
        set
        {
            ltlHeadElements = value;
        }
    }

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        PageStatusContainer = plcStatus;
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Hide actions panel if no actions are present and DisplayActionsPanel is false
        if (!DisplayActionsPanel)
        {
            if ((actionsElem.Actions == null) || (actionsElem.Actions.Length == 0))
            {
                pnlActions.Visible = false;
            }
        }

        // Display panel with additional controls place holder if required
        if (DisplayControlsPanel)
        {
            pnlAdditionalControls.Visible = true;
        }
        bodyElem.Attributes["class"] = mBodyClass;
    }
}
