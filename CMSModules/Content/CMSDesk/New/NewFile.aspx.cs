using System;
using System.Web;
using System.Web.UI.WebControls;
using System.IO;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.SettingsProvider;
using CMS.FormEngine;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.PortalEngine;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_New_NewFile : CMSContentPage
{
    #region "Variables"

    private int nodeId = 0;
    private bool isDialog = false;
    private int templateId = 0;
    private FormFieldInfo mFieldInfo = null;
    private int mResizeToWidth = 0;
    private int mResizeToHeight = 0;
    private int mResizeToMaxSideSize = 0;
    private string mAllowedExtensions = null;

    #endregion


    #region "Properties"


    /// <summary>
    /// Class field info
    /// </summary>
    public FormFieldInfo FieldInfo
    {
        get
        {
            if (mFieldInfo == null)
            {
                // Get document type ('cms.file') settings
                DataClassInfo dci = DataClassInfoProvider.GetDataClass("cms.file");
                if (dci == null)
                {
                    throw new Exception("[NewFile.aspx]: Class 'cms.file' is missing!");
                }
                // Load xml definition
                string def = dci.ClassFormDefinition;
                FormInfo fi = new FormInfo();
                fi.LoadXmlDefinition(def);

                // Get valid extensions from form field info
                mFieldInfo = fi.GetFormField("FileAttachment");
            }
            return mFieldInfo;
        }
    }


    /// <summary>
    /// Unique GUID
    /// </summary>
    public Guid Guid
    {
        get
        {
            Guid guid = ValidationHelper.GetGuid(ViewState["Guid"], Guid.Empty);
            if (guid == Guid.Empty)
            {
                guid = Guid.NewGuid();
                ViewState["Guid"] = guid;
            }
            return guid;
        }
    }


    /// <summary>
    /// Resize to width
    /// </summary>
    private int ResizeToWidth
    {
        get
        {
            return mResizeToWidth;
        }
        set
        {
            mResizeToWidth = value;
        }
    }


    /// <summary>
    /// Resize to height
    /// </summary>
    private int ResizeToHeight
    {
        get
        {
            return mResizeToHeight;
        }
        set
        {
            mResizeToHeight = value;
        }
    }


    /// <summary>
    /// Resize to maximal side size
    /// </summary>
    private int ResizeToMaxSideSize
    {
        get
        {
            return mResizeToMaxSideSize;
        }
        set
        {
            mResizeToMaxSideSize = value;
        }
    }


    /// <summary>
    /// Allowed extensions
    /// </summary>
    private string AllowedExtensions
    {
        get
        {
            return mAllowedExtensions;
        }
        set
        {
            mAllowedExtensions = value;
        }
    }


    /// <summary>
    /// Indicates if file uploader control should be used
    /// </summary>
    private bool UseFileUploader
    {
        get
        {
            return (FieldInfo.FieldType == FormFieldControlTypeEnum.UploadControl);
        }
    }

    #endregion


    protected override void OnPreInit(EventArgs e)
    {
        ((Panel)CurrentMaster.PanelBody.FindControl("pnlContent")).CssClass = "";
        base.OnPreInit(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
        {
            ltlSpellScript.Text = "<script type=\"text/javascript\">  //<![CDATA[ spellURL = \"" + CMSContext.ResolveDialogUrl("~/CMSFormControls/LiveSelectors/SpellCheck.aspx") + "\"; //]]> </script>";
        }
        else
        {
            ltlSpellScript.Text = "<script type=\"text/javascript\">  //<![CDATA[ spellURL = \"" + CMSContext.ResolveDialogUrl("~/CMSModules/Content/CMSDesk/Edit/SpellCheck.aspx") + "\"; //]]> </script>";
        }

        // Register progrees script
        ScriptHelper.RegisterProgress(Page);

        if (!LicenseHelper.LicenseVersionCheck(UrlHelper.GetCurrentDomain(), FeatureEnum.Documents, VersionActionEnum.Insert))
        {
            RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.documentslicenselimits"), ""));
        }

        // Check if need template selection, if so, then redirect to template selection page
        DataClassInfo ci = DataClassInfoProvider.GetDataClass("CMS.File");
        if (ci == null)
        {
            throw new Exception("[Content/Edit.aspx]: Class 'CMS.File' not found.");
        }

        if ((ci.ClassShowTemplateSelection) && (templateId == 0))
        {
            UrlHelper.Redirect("~/CMSModules/Content/CMSDesk/TemplateSelection.aspx" + HttpContext.Current.Request.Url.Query);
        }

        nodeId = ValidationHelper.GetInteger(Request.QueryString["nodeid"], 0);

        // Get the node
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
        TreeNode node = tree.SelectSingleNode(nodeId, TreeProvider.ALL_CULTURES);
        if (node != null)
        {
            if (!DataClassInfoProvider.IsChildClassAllowed(ValidationHelper.GetInteger(node.GetValue("NodeClassID"), 0), ci.ClassID))
            {
                lblError.Text = ResHelper.GetString("Content.ChildClassNotAllowed");
                pnlForm.Visible = false;
                return;
            }
        }

        if (!CMSContext.CurrentUser.IsAuthorizedToCreateNewDocument(node, ci.ClassName))
        {
            lblError.Text = ResHelper.GetString("Content.NotAuthorizedFile");
            pnlForm.Visible = false;
            return;
        }

        // Set request timeout
        Server.ScriptTimeout = AttachmentHelper.ScriptTimeout;

        // Indicates whether new file is added from the dialog window (HTML editor -> InsertImage -> BrowseServer -> Upload)
        isDialog = (ValidationHelper.GetString(Request.QueryString["dialog"], "") == "1");

        plcDirect.Visible = !UseFileUploader;
        plcUploader.Visible = UseFileUploader;

        InitializeProperties();

        // Init direct uploader
        if (!UseFileUploader)
        {
            ucDirectUploader.GUIDColumnName = FieldInfo.Name;
            ucDirectUploader.AllowDelete = FieldInfo.AllowEmpty;
            ucDirectUploader.FormGUID = Guid;
            ucDirectUploader.ResizeToHeight = ResizeToHeight;
            ucDirectUploader.ResizeToWidth = ResizeToWidth;
            ucDirectUploader.ResizeToMaxSideSize = ResizeToMaxSideSize;
            ucDirectUploader.AllowedExtensions = AllowedExtensions;
            ucDirectUploader.NodeParentNodeID = node.NodeParentID;
        }

        lblFileDescription.Text = ResHelper.GetString("NewFile.FileDescription");
        lblUploadFile.Text = ResHelper.GetString("NewFile.UploadFile");

        ltlScript.Text = ScriptHelper.GetScript("function SaveDocument(nodeId, createAnother) { document.getElementById('hidAnother').value = createAnother; " + ClientScript.GetPostBackEventReference(btnOk, null) + "; }");
    }


    protected void btnOk_Click(object sender, EventArgs e)
    {
        TreeNode node = null;
        TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);

        if ((UseFileUploader && !FileUpload.HasFile) || (!UseFileUploader && !ucDirectUploader.HasData()))
        {
            lblError.Text = ResHelper.GetString("NewFile.ErrorEmpty");
        }
        else
        {
            // Get file extension
            string fileExtension = UseFileUploader ? FileUpload.FileName : ucDirectUploader.AttachmentName;
            fileExtension = Path.GetExtension(fileExtension).TrimStart('.');

            // Check file extension
            if (IsExtensionAllowed(fileExtension))
            {
                bool newDocumentCreated = false;

                try
                {
                    if (UseFileUploader)
                    {
                        // Process file using file upload
                        node = ProcessFileUploader(tree);
                    }
                    else
                    {
                        // Process file using direct uploader
                        node = ProcessDirectUploader(tree);

                        // Save temporary attachments
                        DocumentHelper.SaveTemporaryAttachments(node, Guid, CMSContext.CurrentSiteName, tree);
                    }

                    newDocumentCreated = true;

                    // Create default SKU if configured
                    if (ModuleEntry.CheckModuleLicense(ModuleEntry.ECOMMERCE, UrlHelper.GetCurrentDomain(), FeatureEnum.Ecommerce, VersionActionEnum.Insert))
                    {
                        node.CreateDefaultSKU();
                    }

                    // Set additional values
                    if (!string.IsNullOrEmpty(fileExtension))
                    {
                        // Update document extensions if no custom are used
                        if (!node.DocumentUseCustomExtensions)
                        {
                            node.DocumentExtensions = "." + fileExtension;
                        }
                        node.SetValue("DocumentType", "." + fileExtension);
                    }

                    // Update the document
                    DocumentHelper.UpdateDocument(node, tree);

                    bool createAnother = ValidationHelper.GetBoolean(Request.Params["hidAnother"], false);

                    // Added from CMSDesk->Content
                    if (!isDialog)
                    {
                        if (createAnother)
                        {
                            ltlScript.Text += ScriptHelper.GetScript("PassiveRefresh(" + node.NodeParentID + ", " + node.NodeParentID + "); CreateAnother();");
                        }
                        else
                        {
                            ltlScript.Text += ScriptHelper.GetScript(
                                "   RefreshTree(" + node.NodeID + ", " + node.NodeID + "); SelectNode(" + node.NodeID + "); \n"
                            );
                        }
                    }
                    // Added from dialog window
                    else
                    {
                        if (createAnother)
                        {
                            txtFileDescription.Text = "";
                            ltlScript.Text += ScriptHelper.GetScript("FileCreated(" + node.NodeID + ", " + node.NodeParentID + ", false);");
                        }
                        else
                        {
                            ltlScript.Text += ScriptHelper.GetScript("FileCreated(" + node.NodeID + ", " + node.NodeParentID + ", true);");
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Delete the document if something failed
                    if (newDocumentCreated && (node != null) && (node.DocumentID > 0))
                    {
                        DocumentHelper.DeleteDocument(node, tree, false, true, true);
                    }
                    lblError.Text = ResHelper.GetString("NewFile.Failed") + ": " + ex.Message;
                }
            }
            else
            {
                lblError.Text = String.Format(ResHelper.GetString("NewFile.ExtensionNotAllowed"), fileExtension);
            }
        }
    }


    protected void InitializeProperties()
    {
        string autoresize = ValidationHelper.GetString(FieldInfo.Settings["autoresize"], "").ToLower();

        // Set custom settings
        if (autoresize == "custom")
        {
            if (FieldInfo.Settings["autoresize_width"] != null)
            {
                ResizeToWidth = ValidationHelper.GetInteger(FieldInfo.Settings["autoresize_width"], 0);
            }
            if (FieldInfo.Settings["autoresize_height"] != null)
            {
                ResizeToHeight = ValidationHelper.GetInteger(FieldInfo.Settings["autoresize_height"], 0);
            }
            if (FieldInfo.Settings["autoresize_maxsidesize"] != null)
            {
                ResizeToMaxSideSize = ValidationHelper.GetInteger(FieldInfo.Settings["autoresize_maxsidesize"], 0);
            }
        }
        // Set site settings
        else if (autoresize == "")
        {
            string siteName = CMSContext.CurrentSiteName;
            ResizeToWidth = ImageHelper.GetAutoResizeToWidth(siteName);
            ResizeToHeight = ImageHelper.GetAutoResizeToHeight(siteName);
            ResizeToMaxSideSize = ImageHelper.GetAutoResizeToMaxSideSize(siteName);
        }

        if (UseFileUploader)
        {
            string siteName = CMSContext.CurrentSiteName;
            string siteExtensions = SettingsKeyProvider.GetStringValue(siteName + ".CMSUploadExtensions");
            string allExtensions = siteExtensions;
            if (!string.IsNullOrEmpty(FieldInfo.FileExtensions))
            {
                allExtensions += ";" + FieldInfo.FileExtensions;
            }
            AllowedExtensions = allExtensions;
        }
        else
        {
            if (ValidationHelper.GetString(FieldInfo.Settings["extensions"], "") == "custom")
            {
                // Load allowed extensions
                AllowedExtensions = ValidationHelper.GetString(FieldInfo.Settings["allowed_extensions"], "");
            }
            else
            {
                // Use site settings
                string siteName = CMSContext.CurrentSiteName;
                AllowedExtensions = SettingsKeyProvider.GetStringValue(siteName + ".CMSUploadExtensions");
            }
        }
    }


    protected TreeNode ProcessFileUploader(TreeProvider tree)
    {
        TreeNode node = null;

        // Create new document
        string fileName = Path.GetFileNameWithoutExtension(FileUpload.FileName);

        node = new TreeNode("CMS.File", tree);
        node.DocumentCulture = CMSContext.PreferredCultureCode;
        node.DocumentName = fileName;

        // Load default values
        FormHelper.LoadDefaultValues(node);

        node.SetValue("FileDescription", txtFileDescription.Text);
        node.SetValue("FileName", fileName);
        node.SetValue("FileAttachment", Guid.Empty);

        // Set default template ID
        if (templateId > 0)
        {
            node.DocumentPageTemplateID = templateId;
        }
        else
        {
            // Get the class
            DataClassInfo ci = DataClassInfoProvider.GetDataClass("CMS.File");
            if (ci == null)
            {
                throw new Exception("[Content/Edit.aspx]: Class 'CMS.File' not found.");
            }
            node.DocumentPageTemplateID = ci.ClassDefaultPageTemplateID;
        }

        // Insert the document
        DocumentHelper.InsertDocument(node, nodeId, tree);

        // Add the file
        DocumentHelper.AddAttachment(node, "FileAttachment", FileUpload.PostedFile, tree, ResizeToWidth, ResizeToHeight, ResizeToMaxSideSize);

        return node;
    }


    protected TreeNode ProcessDirectUploader(TreeProvider tree)
    {
        TreeNode node = null;

        // Create new document
        string fileName = Path.GetFileNameWithoutExtension(ucDirectUploader.AttachmentName);
        string extension = Path.GetExtension(ucDirectUploader.AttachmentName);

        node = new TreeNode("CMS.File", tree);
        node.DocumentCulture = CMSContext.PreferredCultureCode;
        node.DocumentName = fileName;

        // Load default values
        FormHelper.LoadDefaultValues(node);

        node.SetValue("FileDescription", txtFileDescription.Text);
        node.SetValue("FileName", fileName);
        node.SetValue("FileAttachment", ucDirectUploader.Value);

        // Set default template ID
        if (templateId > 0)
        {
            node.DocumentPageTemplateID = templateId;
        }
        else
        {
            // Get the class
            DataClassInfo ci = DataClassInfoProvider.GetDataClass("CMS.File");
            if (ci == null)
            {
                throw new Exception("[Content/Edit.aspx]: Class 'CMS.File' not found.");
            }
            node.DocumentPageTemplateID = ci.ClassDefaultPageTemplateID;
        }

        // Insert the document
        DocumentHelper.InsertDocument(node, nodeId, tree);

        return node;
    }


    /// <summary>
    /// Determines whether file with specified extension can be uploaded.
    /// </summary>
    /// <param name="extension">File extension to check.</param>
    protected bool IsExtensionAllowed(string extension)
    {
        if (string.IsNullOrEmpty(AllowedExtensions))
        {
            return true;
        }


        // Remove starting dot from tested extension
        extension = extension.TrimStart('.').ToLower();

        string extensions = ";" + AllowedExtensions.ToLower() + ";";
        return ((extensions.Contains(";" + extension + ";")) || (extensions.Contains(";." + extension + ";")));
    }
}
