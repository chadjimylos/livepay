<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Subscriber_Subscriptions.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Subscribers_Subscriber_Subscriptions"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Newsletter subscribtions" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>


<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label ID="lblSelected" runat="server" CssClass="BoldInfoLabel" EnableViewState="false" />
    <cms:UniSelector ID="usNewsletters" runat="server" IsLiveSite="false" ObjectType="Newsletter.Newsletter"
        SelectionMode="Multiple" ResourcePrefix="newsletterselect" />
    <br />
    <asp:CheckBox ID="chkSendConfirmation" runat="server" TextAlign="Right" /><br />
</asp:Content>
