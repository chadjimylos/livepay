using System;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.ResourceManager;
using CMS.SettingsProvider;
using CMS.UIControls;

public partial class CMSSiteManager_Development_UICultures_UICulture_String_New : SiteManagerPage
{
    #region "Variables"

    int uiCultureID = 0;
    UICultureInfo uic = null;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Get UI culture ID from query string
        uiCultureID = QueryHelper.GetInteger("UIcultureID", 0);

        lblEnglishText.Text = String.Format(ResHelper.GetString("Administration-UICulture_String_New.EnglishText"), CultureHelper.DefaultUICulture);
        rfvKey.ErrorMessage = ResHelper.GetString("Administration-UICulture_String_New.EmptyKey");

        if (!RequestHelper.IsPostBack())
        {
            chkCustomString.Checked = ValidationHelper.GetBoolean(SettingsHelper.AppSettings["CMSCreateCustomStringsByDefault"], true);
        }

        // Get the culture info
        uic = UICultureInfoProvider.GetUICultureInfo(uiCultureID);

        // If default culture, do not show the default text
        if (uic.UICultureCode.Equals(CultureHelper.DefaultUICulture, StringComparison.InvariantCultureIgnoreCase))
        {
            txtEnglishText.Visible = false;
            lblEnglishText.Visible = false;
        }
        else
        {
            txtEnglishText.Visible = true;
            lblEnglishText.Visible = true;
        }

        InitializeMasterPage();
    }


    /// <summary>
    /// Initializes MasterPage
    /// </summary>
    protected void InitializeMasterPage()
    {
        // Initializes page title
        string[,] tabs = new string[2, 3];
        tabs[0, 0] = ResHelper.GetString("UICultures_Strings.Strings");
        tabs[0, 1] = "~/CMSSiteManager/Development/UICultures/UICulture_Strings_List.aspx?UIcultureID=" + uiCultureID;
        tabs[0, 2] = "";
        tabs[1, 0] = ResHelper.GetString("UICultures_Strings.NewString");
        tabs[1, 1] = "";
        tabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = tabs;

        this.CurrentMaster.Title.HelpName = "helpTopic";
        this.CurrentMaster.Title.HelpTopicName = "newedit_string";
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        try
        {
            //Trim the key before save
            txtKey.Text = txtKey.Text.Trim();

            string result = new Validator().NotEmpty(txtKey.Text, rfvKey.ErrorMessage).Result;

            if ((result == String.Empty) && (!ValidationHelper.IsCodeName(txtKey.Text)))
            {
                result = ResHelper.GetString("Administration-UICulture_String_New.InvalidCodeName");
            }

            if (result == string.Empty)
            {

                // Check if resource string already exists with given code name
                ResourceStringInfo ri = SqlResourceManager.GetResourceStringInfo(txtKey.Text.Trim());
                if (ri == null)
                {
                    ri = new ResourceStringInfo();
                    ri.StringKey = txtKey.Text.Trim();
                    ri.StringIsCustom = chkCustomString.Checked;
                    ri.UICultureCode = uic.UICultureCode;
                    ri.TranslationText = txtText.Text;
                    SqlResourceManager.SetResourceStringInfo(ri);

                    // If english text is set, store it
                    if (!string.IsNullOrEmpty(txtEnglishText.Text.Trim()))
                    {
                        ri.UICultureCode = CultureHelper.DefaultUICulture;     // !!!
                        ri.TranslationText = txtEnglishText.Text;
                        SqlResourceManager.SetResourceStringInfo(ri);
                    }

                    UrlHelper.Redirect("UICulture_Strings_Edit.aspx?uicultureID=" + uiCultureID + "&stringid=" + ri.StringId + "&saved=1&showBack=0");
                }
                else
                {
                    lblError.Text = String.Format(ResHelper.GetString("Administration-UICulture_String_New.StringExists"), txtKey.Text.Trim());
                    lblError.Visible = true;
                }
            }

            if (result != String.Empty)
            {
                lblError.Text = result;
                lblError.Visible = true;
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ResHelper.GetString("general.erroroccurred") + ex.Message;
            lblError.Visible = true;
        }
    }
}
