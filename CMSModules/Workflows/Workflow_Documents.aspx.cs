using System;
using System.Data;
using System.Collections;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.EventLog;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.TreeEngine;
using CMS.UIControls;
using CMS.WorkflowEngine;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSSiteManager_Development_Workflows_Workflow_Documents : CMSAdministrationSharedPage
{
    #region "Private variables & enums"

    private int workflowId = 0;
    private SiteInfo siteInfo = null;
    private CurrentUserInfo currentUser = null;
    private string currentCulture = CultureHelper.DefaultUICulture;
    private EventLogProvider mEventLog = null;
    private TreeProvider mTree = null;
    private static readonly Hashtable mErrors = new Hashtable();
    private static readonly Hashtable mInfos = new Hashtable();

    private const string GET_WORKFLOW_DOCS_WHERE = "DocumentWorkflowStepID IN (SELECT StepID FROM CMS_WorkflowStep WHERE StepWorkflowID = {0})";

    private enum Action
    {
        SelectAction = 0,
        PublishAndFinish = 1,
        RemoveWorkflow = 2
    }

    protected enum What
    {
        SelectedDocuments = 0,
        AllDocuments = 1
    }

    private What currentWhat = default(What);

    #endregion


    #region "Properties"

    /// <summary>
    /// Current log context
    /// </summary>
    public LogContext CurrentLog
    {
        get
        {
            return EnsureLog();
        }
    }


    private TreeProvider Tree
    {
        get
        {
            return mTree ?? (mTree = new TreeProvider(currentUser));
        }
    }


    private EventLogProvider EventLog
    {
        get
        {
            return mEventLog ?? (mEventLog = new EventLogProvider(Tree.Connection));
        }
    }


    /// <summary>
    /// Current Error
    /// </summary>
    private string CurrentError
    {
        get
        {
            return ValidationHelper.GetString(mInfos["WorkflowError_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mInfos["WorkflowError_" + ctlAsync.ProcessGUID] = value;
        }
    }


    /// <summary>
    /// Current Info
    /// </summary>
    private string CurrentInfo
    {
        get
        {
            return ValidationHelper.GetString(mErrors["WorkflowError_" + ctlAsync.ProcessGUID], string.Empty);
        }
        set
        {
            mErrors["WorkflowError_" + ctlAsync.ProcessGUID] = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        currentUser = CMSContext.CurrentUser;
        currentCulture = CultureHelper.PreferredUICulture;

        // Initialize events
        ctlAsync.OnFinished += ctlAsync_OnFinished;
        ctlAsync.OnError += ctlAsync_OnError;
        ctlAsync.OnRequestLog += ctlAsync_OnRequestLog;
        ctlAsync.OnCancel += ctlAsync_OnCancel;

        if (!RequestHelper.IsCallback())
        {
            // Set visibility of panels
            pnlContent.Visible = true;
            pnlLog.Visible = false;

            filterDocuments.OnSiteSelectionChanged += UniSelector_OnSelectionChanged;

            // Get current page template ID
            workflowId = QueryHelper.GetInteger("workflowid", 0);

            // Initialize unigrid
            ucDocuments.OnExternalDataBound += ucDocuments_OnExternalDataBound;
            ucDocuments.ZeroRowsText = ResHelper.GetString(filterDocuments.FilterIsSet ? "unigrid.filteredzerorowstext" : "workflowdocuments.nodata");
            ucDocuments.OnDataReload += ucDocuments_OnDataReload;

            // Create action script
            StringBuilder actionScript = new StringBuilder();
            actionScript.AppendLine("var tempSelection = '';");
            actionScript.AppendLine("function PerformAction(gridId, dropId)");
            actionScript.AppendLine("{");
            actionScript.AppendLine("   var gridElem = document.getElementById(gridId);");
            actionScript.AppendLine("   var label = document.getElementById('" + lblValidation.ClientID + "');");
            actionScript.AppendLine("   var items = gridElem.value;");
            actionScript.AppendLine("   var whatDrp = document.getElementById('" + drpWhat.ClientID + "');");
            actionScript.AppendLine("   var allSelected = " + (int)What.SelectedDocuments + ";");
            actionScript.AppendLine("   var action = document.getElementById(dropId).value;");
            actionScript.AppendLine("   if (action == '" + (int)Action.SelectAction + "')");
            actionScript.AppendLine("   {");
            actionScript.AppendLine("       label.innerHTML = '" + ResHelper.GetString("massaction.selectsomeaction") + "';");
            actionScript.AppendLine("       return false;");
            actionScript.AppendLine("   }");
            actionScript.AppendLine("   if(whatDrp.value == '" + (int)What.AllDocuments + "')");
            actionScript.AppendLine("   {");
            actionScript.AppendLine("       allSelected = " + (int)What.AllDocuments + ";");
            actionScript.AppendLine("   }");
            actionScript.AppendLine("   var items = gridElem.value;");
            actionScript.AppendLine("   if(!SelectionIsEmpty(gridElem) || whatDrp.value == '" + (int)What.AllDocuments + "')");
            actionScript.AppendLine("   {");
            actionScript.AppendLine("       var confirmed = false;");
            actionScript.AppendLine("       var confMessage = '';");
            actionScript.AppendLine("       switch(action)");
            actionScript.AppendLine("       {");
            actionScript.AppendLine("           case '" + (int)Action.PublishAndFinish + "':");
            actionScript.AppendLine("               confMessage = '" + ResHelper.GetString("workflowdocuments.confrimpublish") + "';");
            actionScript.AppendLine("               break;");
            actionScript.AppendLine("           case '" + (int)Action.RemoveWorkflow + "':");
            actionScript.AppendLine("               confMessage = '" + ResHelper.GetString("workflowdocuments.confirmremove") + "';");
            actionScript.AppendLine("               break;");
            actionScript.AppendLine("       }");
            actionScript.AppendLine("       return confirm(confMessage);");
            actionScript.AppendLine("   }");
            actionScript.AppendLine("   else");
            actionScript.AppendLine("   {");
            actionScript.AppendLine("       label.innerHTML = '" + ResHelper.GetString("documents.selectdocuments") + "';");
            actionScript.AppendLine("       return false;");
            actionScript.AppendLine("   }");
            actionScript.AppendLine("}");
            actionScript.AppendLine("function SelectionIsEmpty(gridElem)");
            actionScript.AppendLine("{");
            actionScript.AppendLine("   var items = gridElem.value;");
            actionScript.AppendLine("   return !(items != '' && items != '|');");
            actionScript.AppendLine("}");
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "actionScript", ScriptHelper.GetScript(actionScript.ToString()));

            // Add action to button
            btnOk.OnClientClick = "return PerformAction('" + ucDocuments.ClientID + "_hidSelection','" + drpAction.ClientID + "');";
            btnCancel.Text = ResHelper.GetString("general.cancel");

            // Initialize dropdown list with actions
            if (!RequestHelper.IsPostBack())
            {
                if (currentUser.IsGlobalAdministrator ||
                    currentUser.IsAuthorizedPerResource("CMS.Content", "manageworkflow"))
                {
                    drpAction.Items.Add(new ListItem(ResHelper.GetString("general." + Action.SelectAction), Convert.ToInt32(Action.SelectAction).ToString()));
                    drpAction.Items.Add(new ListItem(ResHelper.GetString("workflowdocuments." + Action.PublishAndFinish), Convert.ToInt32(Action.PublishAndFinish).ToString()));
                    drpAction.Items.Add(new ListItem(ResHelper.GetString("workflowdocuments." + Action.RemoveWorkflow), Convert.ToInt32(Action.RemoveWorkflow).ToString()));

                }

                drpWhat.Items.Add(new ListItem(ResHelper.GetString("contentlisting." + What.SelectedDocuments), Convert.ToInt32(What.SelectedDocuments).ToString()));
                drpWhat.Items.Add(new ListItem(ResHelper.GetString("contentlisting." + What.AllDocuments), Convert.ToInt32(What.AllDocuments).ToString()));
            }
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (filterDocuments.SelectedSite != TreeProvider.ALL_SITES)
        {
            ucDocuments.GridView.Columns[7].Visible = false;
        }
    }

    #endregion


    #region "Button handling"

    protected void btnCancel_OnClick(object sender, EventArgs e)
    {
        pnlLog.Visible = false;
        pnlContent.Visible = true;
    }


    protected void btnOk_OnClick(object sender, EventArgs e)
    {
        pnlLog.Visible = true;
        pnlContent.Visible = false;

        CurrentError = string.Empty;
        CurrentInfo = string.Empty;
        CurrentLog.Close();
        EnsureLog();

        int actionValue = ValidationHelper.GetInteger(drpAction.SelectedValue, 0);
        Action action = (Action)actionValue;

        int whatValue = ValidationHelper.GetInteger(drpWhat.SelectedValue, 0);
        currentWhat = (What)whatValue;

        ctlAsync.Parameter = currentUser;
        switch (action)
        {
            case Action.PublishAndFinish:
                // Publish and finish workflow
                titleElemAsync.TitleText = ResHelper.GetString("content.publishingdocuments");
                titleElemAsync.TitleImage = GetImageUrl("CMSModules/CMS_Workflows/publish.png");
                ctlAsync.RunAsync(PublishAndFinish, WindowsIdentity.GetCurrent());
                break;

            case Action.RemoveWorkflow:
                // Remove workflow
                titleElemAsync.TitleText = ResHelper.GetString("workflowdocuments.removingwf");
                titleElemAsync.TitleImage = GetImageUrl("CMSModules/CMS_Workflows/removeworkflow.png");
                ctlAsync.RunAsync(RemoveWorkflow, WindowsIdentity.GetCurrent());
                break;
        }
    }

    #endregion


    #region "Events handling"

    protected object ucDocuments_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView drv = GetDataRowView(sender as DataControlFieldCell);
        string siteName = ValidationHelper.GetString(drv["SiteName"], string.Empty);
        string documentCulture = null;
        int nodeId = 0;

        // Get document site
        siteInfo = SiteInfoProvider.GetSiteInfo(siteName);

        switch (sourceName.ToLower())
        {
            case "documentname":
                // Generate link to the document
                string documentName = TextHelper.LimitLength(ValidationHelper.GetString(drv["DocumentName"], string.Empty), 50);
                nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                documentCulture = ValidationHelper.GetString(drv["DocumentCulture"], string.Empty);
                return GetDocumentLink(documentName, siteInfo, nodeId, documentCulture);

            case "documentnametooltip":
                drv = (DataRowView)parameter;
                return UniGridFunctions.DocumentNameTooltip(drv);

            case "culture":
                return UniGridFunctions.DocumentCultureFlag(drv, this.Page);

            case "nodeclassid":
            case "nodeclassidtooltip":
                int nodeClassId = ValidationHelper.GetInteger(drv["NodeClassID"], 0);
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(nodeClassId);
                string result = (dci != null) ? dci.ClassDisplayName : string.Empty;
                if (sourceName.ToLower() == "nodeclassid")
                {
                    result = TextHelper.LimitLength(result, 50);
                }
                result = HTMLHelper.HTMLEncode(result);
                return result;

            case "sitename":
                return HTMLHelper.HTMLEncode(siteInfo.DisplayName);

            case "icon":
                string className = ValidationHelper.GetString(drv["ClassName"], string.Empty);
                nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                documentCulture = ValidationHelper.GetString(drv["DocumentCulture"], string.Empty);
                return GetDocumentIconLink(siteInfo, nodeId, documentCulture, className);

            case "stepname":
                string stepName = ValidationHelper.GetString(parameter, null);
                if (stepName != null)
                {
                    WorkflowStepInfo wsi = WorkflowStepInfoProvider.GetWorkflowStepInfo(stepName, workflowId);
                    if (wsi != null)
                    {
                        return wsi.StepDisplayName;
                    }
                }
                return "-";

            case "versionnumber":
                if (parameter == DBNull.Value)
                {
                    parameter = "-";
                }
                return parameter;

            default:
                return parameter;
        }
    }


    protected DataSet ucDocuments_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        string where = string.Format(GET_WORKFLOW_DOCS_WHERE, workflowId);
        where = SqlHelperClass.AddWhereCondition(where, completeWhere);
        where = SqlHelperClass.AddWhereCondition(where, filterDocuments.WhereCondition);
        string selectedSite = filterDocuments.SelectedSite;
        // Get documents
        DataSet nodes = DocumentHelper.GetDocuments(selectedSite, "/%", TreeProvider.ALL_CULTURES, false, null, where, currentOrder, -1, false, currentTopN, "DocumentID, DocumentName, DocumentNamePath, NodeID, DocumentCulture, SiteName, NodeClassID, ClassName, Published, VersionNumber, StepName", Tree);

        pnlFooter.Visible = !DataHelper.DataSourceIsEmpty(nodes);
        totalRecords = DataHelper.GetItemsCount(nodes);
        return nodes;
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        pnlUpdate.Update();
    }

    #endregion


    #region "Thread methods"

    private void PublishAndFinish(object parameter)
    {
        TreeProvider tree = Tree;
        TreeNode node = null;
        tree.AllowAsyncActions = false;

        try
        {
            // Begin log
            AddLog(ResHelper.GetString("content.preparingdocuments", currentCulture));

            ArrayList documentIds = ucDocuments.SelectedItems;

            // Prepare the where condition
            string where = string.Empty;
            if (currentWhat == What.SelectedDocuments)
            {
                // Get where for selected documents
                where = "DocumentID IN (";
                foreach (object id in documentIds)
                {
                    string documentId = ValidationHelper.GetString(id, string.Empty);
                    where += documentId.Replace("'", "''") + ",";
                }
                where = where.TrimEnd(',') + ")";
            }
            else
            {
                if (!string.IsNullOrEmpty(filterDocuments.WhereCondition))
                {
                    // Add filter condition
                    where = SqlHelperClass.AddWhereCondition(where, filterDocuments.WhereCondition);
                }
                // Select documents only under current workflow
                where = SqlHelperClass.AddWhereCondition(where, string.Format(GET_WORKFLOW_DOCS_WHERE, workflowId));
            }

            // Get the documents
            DataSet documents = DocumentHelper.GetDocuments(TreeProvider.ALL_SITES, "/%", TreeProvider.ALL_CULTURES, false, TreeProvider.ALL_CLASSNAMES, where, "NodeAliasPath DESC", -1, false, tree);

            if (!DataHelper.DataSourceIsEmpty(documents))
            {
                // Create instance of workflow manager class
                WorkflowManager wm = new WorkflowManager(tree);

                // Begin publishing
                AddLog(ResHelper.GetString("content.publishingdocuments", currentCulture));
                foreach (DataTable classTable in documents.Tables)
                {
                    foreach (DataRow nodeRow in classTable.Rows)
                    {
                        // Get the document
                        node = new TreeNode(nodeRow, nodeRow["ClassName"].ToString());

                        // Publish document
                        if (!Publish(node, wm))
                        {
                            // Add log record
                            AddLog(HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")"));
                        }
                        else
                        {
                            AddLog(string.Format(ResHelper.GetString("content.publishedalready"), HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")")));
                        }
                    }
                }
            }
            else
            {
                ctlAsync.RaiseError(null, null);
                AddError(ResHelper.GetString("content.nothingtopublish", currentCulture));
            }
            CurrentInfo += ResHelper.GetString("workflowdocuments.publishcomplete");
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                // When canceled
                AddError(ResHelper.GetString("content.publishcanceled", currentCulture));
                ctlAsync.RaiseError(null, null);
            }
            else
            {
                int siteId = (node != null) ? node.NodeSiteID : CMSContext.CurrentSiteID;
                // Log error
                LogExceptionToEventLog("PUBLISHDOC", "content.publishfailed", ex, siteId);
            }
        }
        catch (Exception ex)
        {
            int siteId = (node != null) ? node.NodeSiteID : CMSContext.CurrentSiteID;
            // Log error
            LogExceptionToEventLog("PUBLISHDOC", "content.publishfailed", ex, siteId);
        }
    }


    private void RemoveWorkflow(object parameter)
    {
        TreeProvider tree = Tree;
        VersionManager verMan = new VersionManager(tree);
        TreeNode node = null;
        // Custom logging
        tree.LogEvents = false;
        tree.AllowAsyncActions = false;

        try
        {
            // Begin log
            AddLog(ResHelper.GetString("content.preparingdocuments", currentCulture));

            ArrayList documentIds = ucDocuments.SelectedItems;

            // Prepare the where condition
            string where = string.Empty;
            if (currentWhat == What.SelectedDocuments)
            {
                // Get where for selected documents
                where = "DocumentID IN (";
                foreach (object id in documentIds)
                {
                    string documentId = ValidationHelper.GetString(id, string.Empty);
                    where += documentId.Replace("'", "''") + ",";
                }
                where = where.TrimEnd(',') + ")";
            }
            else
            {
                if (!string.IsNullOrEmpty(filterDocuments.WhereCondition))
                {
                    // Add filter condition
                    where = SqlHelperClass.AddWhereCondition(where, filterDocuments.WhereCondition);
                }
                // Select documents only under current workflow
                where = SqlHelperClass.AddWhereCondition(where, string.Format(GET_WORKFLOW_DOCS_WHERE, workflowId));
            }

            // Get the documents
            DataSet documents = DocumentHelper.GetDocuments(TreeProvider.ALL_SITES, "/%", TreeProvider.ALL_CULTURES, false, null, where, "NodeAliasPath DESC", -1, false, tree);

            if (!DataHelper.DataSourceIsEmpty(documents))
            {
                // Begin log
                AddLog(ResHelper.GetString("workflowdocuments.removingwf", currentCulture));

                foreach (DataTable classTable in documents.Tables)
                {
                    foreach (DataRow nodeRow in classTable.Rows)
                    {
                        // Get the document
                        node = new TreeNode(nodeRow, nodeRow["ClassName"].ToString(), tree);

                        // Destroy document history
                        verMan.DestroyDocumentHistory(node.DocumentID);

                        // Clear workflow
                        DocumentHelper.ClearWorkflowInformation(node);
                        node.Update();

                        // Add log record
                        AddLog(HTMLHelper.HTMLEncode(node.NodeAliasPath + " (" + node.GetValue("DocumentCulture") + ")"));

                        // Add record to eventlog
                        EventLog.LogEvent("I", DateTime.Now, "Content", "REMOVEDOCWORKFLOW", currentUser.UserID,
                             currentUser.UserName, node.NodeID, node.DocumentName,
                             HTTPHelper.GetUserHostAddress(), ResHelper.GetString("workflowdocuments.removeworkflowsuccess"),
                             node.NodeSiteID, HTTPHelper.GetAbsoluteUri());
                    }
                }
            }
            else
            {
                ctlAsync.RaiseError(null, null);
                AddError(ResHelper.GetString("workflowdocuments.nodocumentstoclear", currentCulture));
            }
            CurrentInfo += ResHelper.GetString("workflowdocuments.removecomplete");
        }
        catch (ThreadAbortException ex)
        {
            string state = ValidationHelper.GetString(ex.ExceptionState, string.Empty);
            if (state == CMSThread.ABORT_REASON_STOP)
            {
                // When canceled
                AddError(ResHelper.GetString("workflowdocuments.removingcanceled", currentCulture));
                ctlAsync.RaiseError(null, null);
            }
            else
            {
                int siteId = (node != null) ? node.NodeSiteID : CMSContext.CurrentSiteID;
                // Log error
                LogExceptionToEventLog("REMOVEDOCWORKFLOW", "workflowdocuments.removefailed", ex, siteId);
            }
        }
        catch (Exception ex)
        {
            int siteId = (node != null) ? node.NodeSiteID : CMSContext.CurrentSiteID;
            // Log error
            LogExceptionToEventLog("REMOVEDOCWORKFLOW", "workflowdocuments.removefailed", ex, siteId);
            ctlAsync.RaiseError(null, null);
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// When exception occures, log it to event log
    /// </summary>
    /// <param name="eventCode">Code of event</param>
    /// <param name="errorTitle">Message to log to async log</param>
    /// <param name="ex">Exception to log</param>
    /// <param name="siteId">ID of site</param>
    private void LogExceptionToEventLog(string eventCode, string errorTitle, Exception ex, int siteId)
    {
        AddError(ResHelper.GetString(errorTitle, currentCulture) + ": " + ex.Message);
        EventLog.LogEvent("E", DateTime.Now, "Content", eventCode, currentUser.UserID,
                          currentUser.UserName, 0, null,
                          HTTPHelper.GetUserHostAddress(), EventLogProvider.GetExceptionLogMessage(ex),
                          siteId, HTTPHelper.GetAbsoluteUri());
        ctlAsync.RaiseError(null, null);
    }


    /// <summary>
    /// Publishes document
    /// </summary>
    /// <param name="node">Node to publish</param>
    /// <param name="wm">Workflow manager</param>
    /// <returns>Whether node is already published</returns>
    private static bool Publish(TreeNode node, WorkflowManager wm)
    {
        bool toReturn = true;
        // Approve until the step is publish
        WorkflowStepInfo currentStep = wm.GetStepInfo(node);
        while ((currentStep != null) && (currentStep.StepName.ToLower() != "published"))
        {
            if (currentStep.StepName.ToLower() == "archived")
            {
                currentStep = wm.MoveToPreviousStep(node, string.Empty);
            }
            else
            {
                currentStep = wm.MoveToNextStep(node, string.Empty);
            }
            toReturn = false;
        }
        return toReturn;
    }


    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private static object GetDocumentLink(string documentName, SiteInfo si, int nodeId, string cultureCode)
    {
        // If the document name is empty alter it with the '-'
        if (string.IsNullOrEmpty(documentName))
        {
            documentName = "-";
        }

        string url = GetDocumentUrl(si, nodeId, cultureCode);
        string toReturn = HTMLHelper.HTMLEncode(documentName);

        if (!string.IsNullOrEmpty(url))
        {
            toReturn = "<a href=\"" + url + "\" target=\"_blank\">" + toReturn + "</a>";
        }
        return toReturn;
    }


    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private object GetDocumentIconLink(SiteInfo si, int nodeId, string cultureCode, string className)
    {
        string url = GetDocumentUrl(si, nodeId, cultureCode);
        string toReturn = "<img style=\"border-style:none;\" src=\"" + GetDocumentTypeIconUrl(className) + "\" alt=\"" + className + "\" />";

        if (!string.IsNullOrEmpty(url))
        {
            return "<a href=\"" + url + "\" target=\"_blank\">" + toReturn + "</a>";
        }
        return toReturn;
    }


    /// <summary>
    /// Generates URL for document
    /// </summary>
    private static string GetDocumentUrl(SiteInfo si, int nodeId, string cultureCode)
    {
        string url = string.Empty;

        if (si.Status == SiteStatusEnum.Running)
        {
            if (si.SiteName != CMSContext.CurrentSiteName)
            {
                // Make url for site in form 'http(s)://sitedomain/application/cmsdesk'.
                url = UrlHelper.GetApplicationUrl(si.DomainName) + "/cmsdesk/default.aspx?section=content&nodeid=" + nodeId + "&culture=" + cultureCode;
            }
            else
            {
                // Make url for current site (with port)
                url = UrlHelper.ResolveUrl("~/cmsdesk/default.aspx?section=content&nodeid=" + nodeId + "&culture=" + cultureCode);
            }
        }
        return url;
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }


    private void HandlePossibleError()
    {
        if (!string.IsNullOrEmpty(CurrentError))
        {
            lblError.Text = CurrentError;
            lblError.Visible = true;
        }
        if (!string.IsNullOrEmpty(CurrentInfo))
        {
            lblInfo.Text = CurrentInfo;
            lblInfo.Visible = true;
        }
        // Clear selection
        ucDocuments.ResetSelection();
        CurrentLog.Close();
    }


    /// <summary>
    /// Ensures the logging context
    /// </summary>
    protected LogContext EnsureLog()
    {
        LogContext log = LogContext.EnsureLog(ctlAsync.ProcessGUID);
        log.Reversed = true;
        log.LineSeparator = "<br />";
        return log;
    }


    /// <summary>
    /// Adds the log information
    /// </summary>
    /// <param name="newLog">New log information</param>
    protected void AddLog(string newLog)
    {
        EnsureLog();
        LogContext.AppendLine(newLog);
    }


    /// <summary>
    /// Adds the error to collection of errors
    /// </summary>
    /// <param name="error">Error message</param>
    protected void AddError(string error)
    {
        AddLog(error);
        CurrentError = (error + "<br />" + CurrentError);
    }

    #endregion


    #region "Handling async thread"

    private void ctlAsync_OnCancel(object sender, EventArgs e)
    {
        ltlScript.Text += ScriptHelper.GetScript("var __pendingCallbacks = new Array();");
        HandlePossibleError();
    }


    private void ctlAsync_OnRequestLog(object sender, EventArgs e)
    {
        ctlAsync.Log = CurrentLog.Log;
    }


    private void ctlAsync_OnError(object sender, EventArgs e)
    {
        HandlePossibleError();
    }


    private void ctlAsync_OnFinished(object sender, EventArgs e)
    {
        HandlePossibleError();
    }

    #endregion
}
