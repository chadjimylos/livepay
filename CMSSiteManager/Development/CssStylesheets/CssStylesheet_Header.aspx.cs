using System;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Development_CssStylesheets_CssStylesheet_Header : SiteManagerPage
{
    protected int cssstylesheetid;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["cssstylesheetid"]))
        {
            cssstylesheetid = Convert.ToInt32(Request.QueryString["cssstylesheetid"]);
        }

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }

        string cssStyleSheets = ResHelper.GetString("CssStylesheet.CssStylesheets");
        string title = ResHelper.GetString("CssStylesheet.CssStylesheetProperties");
        string currentCssStylesheetName = "";
        if (CssStylesheetInfoProvider.GetCssStylesheetInfo(cssstylesheetid) != null)
        {
            currentCssStylesheetName = CssStylesheetInfoProvider.GetCssStylesheetInfo(cssstylesheetid).StylesheetDisplayName;
        }
        const string cssStylesheetUrl = "~/CMSSiteManager/Development/CssStylesheets/CssStylesheet_List.aspx";
        
        // Initializes page title
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = cssStyleSheets;
        pageTitleTabs[0, 1] = cssStylesheetUrl;
        pageTitleTabs[0, 2] = "_parent";
        pageTitleTabs[1, 0] = currentCssStylesheetName;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = title;
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_CSSStylesheet/object.png");
        this.CurrentMaster.Title.HelpTopicName = "general_tab13";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Initializes edit menu
    /// </summary>
    protected void InitalizeMenu()
    {
        string generalString = ResHelper.GetString("General.General");
        string sitesString = ResHelper.GetString("general.sites");

        string[,] tabs = new string[8, 4];
        tabs[0, 0] = generalString;
        tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'general_tab13');";
        tabs[0, 2] = "CssStylesheet_General.aspx?cssstylesheetid=" + cssstylesheetid;
        tabs[1, 0] = sitesString;
        tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'sites_tab3');";
        tabs[1, 2] = "CssStylesheet_Sites.aspx?cssstylesheetid=" + cssstylesheetid;
        this.CurrentMaster.Tabs.Tabs = tabs;
        this.CurrentMaster.Tabs.UrlTarget = "Content";
    }
}
