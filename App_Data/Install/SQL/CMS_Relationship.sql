CREATE TABLE [CMS_Relationship] (
		[LeftNodeID]                 int NOT NULL,
		[RightNodeID]                int NOT NULL,
		[RelationshipNameID]         int NOT NULL,
		[RelationshipCustomData]     nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Relationship]
	ADD
	CONSTRAINT [PK_CMS_Relationship]
	PRIMARY KEY
	([LeftNodeID], [RightNodeID], [RelationshipNameID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[CMS_Relationship]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Relationship_LeftNodeID_CMS_Tree]') IS NULL
BEGIN
		ALTER TABLE [CMS_Relationship]
			ADD CONSTRAINT [FK_CMS_Relationship_LeftNodeID_CMS_Tree]
			FOREIGN KEY ([LeftNodeID]) REFERENCES [CMS_Tree] ([NodeID])
END
IF OBJECT_ID(N'[CMS_Relationship]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_RelationshipName]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Relationship_RelationshipNameID_CMS_RelationshipName]') IS NULL
BEGIN
		ALTER TABLE [CMS_Relationship]
			ADD CONSTRAINT [FK_CMS_Relationship_RelationshipNameID_CMS_RelationshipName]
			FOREIGN KEY ([RelationshipNameID]) REFERENCES [CMS_RelationshipName] ([RelationshipNameID])
END
IF OBJECT_ID(N'[CMS_Relationship]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Tree]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Relationship_RightNodeID_CMS_Tree]') IS NULL
BEGIN
		ALTER TABLE [CMS_Relationship]
			ADD CONSTRAINT [FK_CMS_Relationship_RightNodeID_CMS_Tree]
			FOREIGN KEY ([RightNodeID]) REFERENCES [CMS_Tree] ([NodeID])
END
