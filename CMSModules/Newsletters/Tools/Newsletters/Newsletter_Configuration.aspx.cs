using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Newsletter;
using CMS.GlobalHelper;
using CMS.Scheduler;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Configuration : CMSNewsletterPage
{
    protected int newsletterId = 0;
    protected bool isDynamic = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get newsletter ID from query string
        newsletterId = QueryHelper.GetInteger("newsletterid", 0);
        if (newsletterId == 0)
        {
            return;
        }

        rfvNewsletterDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
        rfvNewsletterName.ErrorMessage = ResHelper.GetString("Newsletter_Edit.ErrorEmptyName");
        rfvNewsletterSenderName.ErrorMessage = ResHelper.GetString("Newsletter_Edit.ErrorEmptySenderName");
        rfvNewsletterSenderEmail.ErrorMessage = ResHelper.GetString("Newsletter_Edit.ErrorEmptySenderEmail");

        lblNewsletterName.Text = ResHelper.GetString("Newsletter_Edit.NewsletterNameLabel");
        lblNewsletterDisplayName.Text = ResHelper.GetString("Newsletter_Edit.NewsletterDisplayNameLabel");
        lblNewsletterSenderName.Text = ResHelper.GetString("Newsletter_Edit.NewsletterSenderNameLabel");
        lblNewsletterDynamicURL.Text = ResHelper.GetString("Newsletter_Edit.NewsletterDynamicURLLabel");
        lblNewsletterSenderEmail.Text = ResHelper.GetString("Newsletter_Edit.NewsletterSenderEmailLabel");
        lblSubscriptionTemplate.Text = ResHelper.GetString("Newsletter_Edit.SubscriptionTemplate");
        lblUnsubscriptionTemplate.Text = ResHelper.GetString("Newsletter_Edit.UnsubscriptionTemplate");
        lblIssueTemplate.Text = ResHelper.GetString("Newsletter_Edit.NewsletterTemplate");
        lblDynamic.Text = ResHelper.GetString("Newsletter_Configuration.Dynamic");
        lblTemplateBased.Text = ResHelper.GetString("Newsletter_Configuration.TemplateBased");
        lblNewsletterDynamicURL.Text = ResHelper.GetString("Newsletter_Edit.SourcePageURL");
        lblSchedule.Text = ResHelper.GetString("Newsletter_Edit.Schedule");
        radPageTitle.Text = ResHelper.GetString("Newsletter_Configuration.PageTitleSubject");
        radFollowing.Text = ResHelper.GetString("Newsletter_Configuration.PageTitleFollowing");
        lblNewsletterBaseUrl.Text = ResHelper.GetString("Newsletter_Configuration.NewsletterBaseUrl");
        lblNewsletterUnsubscribeUrl.Text = ResHelper.GetString("Newsletter_Configuration.NewsletterUnsubscribeUrl");
        btnOk.Text = ResHelper.GetString("General.OK");

        if (!RequestHelper.IsPostBack())
        {
            if (QueryHelper.GetInteger("saved", 0) > 0)
            {
                // If user was redirected from newsletter_new.aspx, display the 'Changes were saved' message
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
        }

        Newsletter news = NewsletterProvider.GetNewsletter(newsletterId);
        if (news != null)
        {
            int siteId = news.NewsletterSiteID;
            subscriptionTemplate.WhereCondition = "TemplateType='S' AND TemplateSiteID=" + siteId;
            unsubscriptionTemplate.WhereCondition = "TemplateType='U' AND TemplateSiteID=" + siteId;
            issueTemplate.WhereCondition = "TemplateType='I' AND TemplateSiteID=" + siteId;
        }

        LoadData();
    }


    protected void LoadData()
    {
        Newsletter newsletterObj = NewsletterProvider.GetNewsletter(newsletterId);
        if (newsletterObj != null)
        {
            if (newsletterObj.NewsletterType == "D")
            {
                isDynamic = true;
                lblTemplateBased.Visible = false;
                lblDynamic.Visible = true;
                pnlDynamic.Visible = true;
                lblNewsletterDynamicURL.Visible = true;
                txtNewsletterDynamicURL.Visible = true;
                plcUrl.Visible = true;
                chkSchedule.Visible = true;
                lblSchedule.Visible = true;

                lblIssueTemplate.Visible = false;
                issueTemplate.Visible = false;
            }
            else
            {
                isDynamic = false;
                lblTemplateBased.Visible = true;
                lblDynamic.Visible = false;
                pnlDynamic.Visible = false;
                lblNewsletterDynamicURL.Visible = false;
                txtNewsletterDynamicURL.Visible = false;
                plcUrl.Visible = false;
                chkSchedule.Visible = false;
                lblSchedule.Visible = false;

                lblIssueTemplate.Visible = true;
                issueTemplate.Visible = true;
            }

            if (!RequestHelper.IsPostBack())
            {
                if (newsletterObj.NewsletterType == "D")
                {
                    plcInterval.Visible = true;
                }
                else
                {
                    plcInterval.Visible = false;
                }

                txtNewsletterDisplayName.Text = newsletterObj.NewsletterDisplayName;
                txtNewsletterName.Text = newsletterObj.NewsletterName;
                txtNewsletterSenderName.Text = newsletterObj.NewsletterSenderName;
                txtNewsletterSenderEmail.Text = newsletterObj.NewsletterSenderEmail;
                txtNewsletterBaseUrl.Text = newsletterObj.NewsletterBaseUrl;
                txtNewsletterUnsubscribeUrl.Text = newsletterObj.NewsletterUnsubscribeUrl;
                chkUseEmailQueue.Checked = newsletterObj.NewsletterUseEmailQueue;

                subscriptionTemplate.Value = newsletterObj.NewsletterSubscriptionTemplateID.ToString();
                unsubscriptionTemplate.Value = newsletterObj.NewsletterUnsubscriptionTemplateID.ToString();

                if (isDynamic)
                {
                    if ((newsletterObj.NewsletterDynamicSubject == null) || (newsletterObj.NewsletterDynamicSubject == ""))
                    {
                        radPageTitle.Checked = true;
                        radFollowing.Checked = false;
                        radPageTitle_CheckedChanged(null, null);
                    }
                    else
                    {
                        radPageTitle.Checked = false;
                        radFollowing.Checked = true;
                        radPageTitle_CheckedChanged(null, null);
                        txtSubject.Text = newsletterObj.NewsletterDynamicSubject;
                    }

                    txtNewsletterDynamicURL.Text = newsletterObj.NewsletterDynamicURL;

                    TaskInfo task = TaskInfoProvider.GetTaskInfo(newsletterObj.NewsletterDynamicScheduledTaskID);

                    if (task != null)
                    {
                        chkSchedule.Checked = true;
                        plcInterval.Visible = true;
                        ScheduleInterval1.ScheduleInterval = task.TaskInterval;
                    }
                    else
                    {
                        chkSchedule.Checked = false;
                        ScheduleInterval1.Visible = false;
                    }
                }
                else
                {
                    issueTemplate.Value = newsletterObj.NewsletterTemplateID.ToString();
                }
            }
            else
            {
                if (newsletterObj.NewsletterType == "D")
                {
                    if (chkSchedule.Checked)
                    {
                        ScheduleInterval1.Visible = true;
                    }
                    else
                    {
                        ScheduleInterval1.Visible = false;
                    }
                }
                else
                {
                    plcInterval.Visible = false;
                }
            }
        }
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check "configure" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.newsletter", "configure"))
        {
            RedirectToCMSDeskAccessDenied("cms.newsletter", "configure");
        }

        // ID of scheduled task which should be deleted
        int deleteScheduledTaskId = 0;
        string errorMessage = new Validator().NotEmpty(txtNewsletterDisplayName.Text, ResHelper.GetString("general.requiresdisplayname")).NotEmpty(txtNewsletterName.Text, ResHelper.GetString("Newsletter_Edit.ErrorEmptyName")).NotEmpty(txtNewsletterSenderName.Text, ResHelper.GetString("Newsletter_Edit.ErrorEmptySenderName")).NotEmpty(txtNewsletterSenderEmail.Text, ResHelper.GetString("Newsletter_Edit.ErrorEmptySenderEmail")).IsEmail(txtNewsletterSenderEmail.Text.Trim(), ResHelper.GetString("Newsletter_Edit.ErrorEmailFormat")).IsCodeName(txtNewsletterName.Text, ResHelper.GetString("general.invalidcodename")).Result;

        if (errorMessage == "")
        {
            // newsletterName must to be unique
            Newsletter newsletterObj = NewsletterProvider.GetNewsletter(txtNewsletterName.Text.Trim(), CMSContext.CurrentSite.SiteID);

            // if newsletterName value is unique														
            if ((newsletterObj == null) || (newsletterObj.NewsletterID == newsletterId))
            {
                if (newsletterObj == null)
                {
                    newsletterObj = NewsletterProvider.GetNewsletter(newsletterId);
                }

                newsletterObj.NewsletterDisplayName = txtNewsletterDisplayName.Text.Trim();
                newsletterObj.NewsletterName = txtNewsletterName.Text.Trim();
                newsletterObj.NewsletterSenderName = txtNewsletterSenderName.Text.Trim();
                newsletterObj.NewsletterSenderEmail = txtNewsletterSenderEmail.Text.Trim();
                newsletterObj.NewsletterBaseUrl = txtNewsletterBaseUrl.Text.Trim();
                newsletterObj.NewsletterUnsubscribeUrl = txtNewsletterUnsubscribeUrl.Text.Trim();
                newsletterObj.NewsletterUseEmailQueue = chkUseEmailQueue.Checked;

                int selectedValue = ValidationHelper.GetInteger(subscriptionTemplate.Value, 0);
                if (selectedValue != 0)
                {
                    newsletterObj.NewsletterSubscriptionTemplateID = selectedValue;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Newsletter_Edit.NoSubscriptionTemplateSelected");
                    return;
                }

                selectedValue = ValidationHelper.GetInteger(unsubscriptionTemplate.Value, 0);
                if (selectedValue != 0)
                {
                    newsletterObj.NewsletterUnsubscriptionTemplateID = selectedValue;
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Newsletter_Edit.NoUnsubscriptionTemplateSelected");
                    return;
                }

                if (isDynamic)
                {
                    newsletterObj.NewsletterDynamicURL = txtNewsletterDynamicURL.Text.Trim();
                    newsletterObj.NewsletterType = "D";
                    if (radFollowing.Checked)
                    {
                        newsletterObj.NewsletterDynamicSubject = txtSubject.Text;
                    }
                    else
                    {
                        newsletterObj.NewsletterDynamicSubject = "";
                    }

                    if (chkSchedule.Checked)
                    {
                        //set info for scheduled task
                        TaskInfo task = TaskInfoProvider.GetTaskInfo(ValidationHelper.GetInteger(newsletterObj.NewsletterDynamicScheduledTaskID, 0));
                        if (task == null)
                        {
                            task = new TaskInfo();
                            task.TaskAssemblyName = "CMS.Newsletter";
                            task.TaskClass = "CMS.Newsletter.DynamicNewsletterSender";
                            task.TaskEnabled = true;
                            task.TaskLastResult = "";
                            task.TaskSiteID = CMSContext.CurrentSite.SiteID;
                            task.TaskData = newsletterObj.NewsletterID.ToString();
                        }

                        if (!ScheduleInterval1.CheckOneDayMinimum())
                        {
                            //if problem occurred while setting schedule interval
                            lblError.Visible = true;
                            lblError.Text = ResHelper.GetString("Newsletter_Edit.NoDaySelected");
                            return;
                        }
                        else
                        {
                            task.TaskInterval = ScheduleInterval1.ScheduleInterval;
                        }

                        task.TaskNextRunTime = SchedulingHelper.GetNextTime(task.TaskInterval, new DateTime(), new DateTime());
                        task.TaskDisplayName = ResHelper.GetString("DynamicNewsletter.TaskName") + newsletterObj.NewsletterDisplayName;
                        task.TaskName = "DynamicNewsletter_" + newsletterObj.NewsletterName;
                        TaskInfoProvider.SetTaskInfo(task);
                        newsletterObj.NewsletterDynamicScheduledTaskID = task.TaskID;
                    }
                    else
                    {
                        if (newsletterObj.NewsletterDynamicScheduledTaskID > 0)
                        {
                            // Store task ID for deletion
                            deleteScheduledTaskId = newsletterObj.NewsletterDynamicScheduledTaskID;
                        }
                        newsletterObj.NewsletterDynamicScheduledTaskID = 0;
                        ScheduleInterval1.Visible = false;
                    }
                }
                else
                {
                    selectedValue = ValidationHelper.GetInteger(issueTemplate.Value, 0);
                    if (selectedValue != 0)
                    {
                        newsletterObj.NewsletterTemplateID = selectedValue;
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = ResHelper.GetString("Newsletter_Edit.NoEmailTemplateSelected");
                        return;
                    }
                    newsletterObj.NewsletterType = "T";
                }
                // Save changes to DB
                NewsletterProvider.SetNewsletter(newsletterObj);

                if (deleteScheduledTaskId > 0)
                {
                    // Delete scheduled task if schedule mail-outs were unchecked
                    TaskInfoProvider.DeleteTaskInfo(deleteScheduledTaskId);
                }

                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Newsletter_Edit.NewsletterNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    protected void radPageTitle_CheckedChanged(object sender, EventArgs e)
    {
        if (radFollowing.Checked)
        {
            txtSubject.Enabled = true;
        }
        else
        {
            txtSubject.Enabled = false;
        }
    }


    protected void radFollowing_CheckedChanged(object sender, EventArgs e)
    {
        if (radFollowing.Checked)
        {
            txtSubject.Enabled = true;
        }
        else
        {
            txtSubject.Enabled = false;
        }
    }


    protected void chkSchedule_CheckedChanged(object sender, EventArgs e)
    {
        if (chkSchedule.Checked)
        {
            ScheduleInterval1.Visible = true;
        }
        else
        {
            ScheduleInterval1.Visible = false;
        }
    }
}
