using System;

using CMS.UIControls;

public partial class CMSTemplates_BlankASPX_Blank : TemplateMasterPage
{
    protected override void CreateChildControls()
    {
        base.CreateChildControls();

        PageManager = CMSPageManager1;
    }


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        ((TemplatePage)Page).ManagersContainer = plcManagers;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        ltlTags.Text = HeaderTags;
    }
}
