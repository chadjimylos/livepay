CREATE TABLE [Newsletter_Newsletter] (
		[NewsletterID]                           int NOT NULL IDENTITY(1, 1),
		[NewsletterDisplayName]                  nvarchar(250) NOT NULL,
		[NewsletterName]                         nvarchar(250) NOT NULL,
		[NewsletterType]                         nvarchar(5) NOT NULL,
		[NewsletterSubscriptionTemplateID]       int NOT NULL,
		[NewsletterUnsubscriptionTemplateID]     int NOT NULL,
		[NewsletterSenderName]                   nvarchar(200) NOT NULL,
		[NewsletterSenderEmail]                  nvarchar(200) NOT NULL,
		[NewsletterDynamicSubject]               nvarchar(500) NULL,
		[NewsletterDynamicURL]                   nvarchar(500) NULL,
		[NewsletterDynamicScheduledTaskID]       int NULL,
		[NewsletterTemplateID]                   int NULL,
		[NewsletterSiteID]                       int NOT NULL,
		[NewsletterGUID]                         uniqueidentifier NOT NULL,
		[NewsletterUnsubscribeUrl]               nvarchar(1000) NULL,
		[NewsletterBaseUrl]                      nvarchar(500) NULL,
		[NewsletterLastModified]                 datetime NOT NULL,
		[NewsletterUseEmailQueue]                bit NULL
)
ON [PRIMARY]
ALTER TABLE [Newsletter_Newsletter]
	ADD
	CONSTRAINT [PK_Newsletter_Newsletter]
	PRIMARY KEY
	NONCLUSTERED
	([NewsletterID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Newsletter_Newsletter]
	ADD
	CONSTRAINT [DEFAULT_Newsletter_Newsletter_NewsletterUseEmailQueue]
	DEFAULT ((0)) FOR [NewsletterUseEmailQueue]
IF OBJECT_ID(N'[Newsletter_Newsletter]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_Newsletter_NewsletterSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_Newsletter]
			ADD CONSTRAINT [FK_Newsletter_Newsletter_NewsletterSiteID_CMS_Site]
			FOREIGN KEY ([NewsletterSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[Newsletter_Newsletter]') IS NOT NULL
	AND OBJECT_ID(N'[Newsletter_EmailTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_Newsletter_NewsletterSubscriptionTemplateID_Newsletter_EmailTemplate]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_Newsletter]
			ADD CONSTRAINT [FK_Newsletter_Newsletter_NewsletterSubscriptionTemplateID_Newsletter_EmailTemplate]
			FOREIGN KEY ([NewsletterSubscriptionTemplateID]) REFERENCES [Newsletter_EmailTemplate] ([TemplateID])
END
IF OBJECT_ID(N'[Newsletter_Newsletter]') IS NOT NULL
	AND OBJECT_ID(N'[Newsletter_EmailTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_Newsletter_NewsletterTemplateID_Newsletter_EmailTemplate]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_Newsletter]
			ADD CONSTRAINT [FK_Newsletter_Newsletter_NewsletterTemplateID_Newsletter_EmailTemplate]
			FOREIGN KEY ([NewsletterTemplateID]) REFERENCES [Newsletter_EmailTemplate] ([TemplateID])
END
IF OBJECT_ID(N'[Newsletter_Newsletter]') IS NOT NULL
	AND OBJECT_ID(N'[Newsletter_EmailTemplate]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_Newsletter_NewsletterUnsubscriptionTemplateID_Newsletter_EmailTemplate]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_Newsletter]
			ADD CONSTRAINT [FK_Newsletter_Newsletter_NewsletterUnsubscriptionTemplateID_Newsletter_EmailTemplate]
			FOREIGN KEY ([NewsletterUnsubscriptionTemplateID]) REFERENCES [Newsletter_EmailTemplate] ([TemplateID])
END
CREATE CLUSTERED INDEX [IX_Newsletter_Newsletter_NewsletterSiteID_NewsletterDisplayName]
	ON [Newsletter_Newsletter] ([NewsletterSiteID], [NewsletterDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE UNIQUE INDEX [IX_Newsletter_Newsletter_NewsletterSiteID_NewsletterName]
	ON [Newsletter_Newsletter] ([NewsletterSiteID], [NewsletterName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Newsletter_Newsletter_NewsletterSubscriptionTemplateID]
	ON [Newsletter_Newsletter] ([NewsletterSubscriptionTemplateID])
	ON [PRIMARY]
CREATE INDEX [IX_Newsletter_Newsletter_NewsletterTemplateID]
	ON [Newsletter_Newsletter] ([NewsletterTemplateID])
	ON [PRIMARY]
CREATE INDEX [IX_Newsletter_Newsletter_NewsletterUnsubscriptionTemplateID]
	ON [Newsletter_Newsletter] ([NewsletterUnsubscriptionTemplateID])
	ON [PRIMARY]
