using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Ecommerce_Controls_UI_ProductTaxes : CMSAdminControl
{
    #region "Variables"

    protected int mProductId = 0;
    protected string currentValues = string.Empty;

    #endregion


    #region "Properties"

    /// <summary>
    /// Product ID
    /// </summary>
    public int ProductID
    {
        get
        {
            return this.mProductId;
        }
        set
        {
            this.mProductId = value;
        }
    }


    /// <summary>
    /// Form enabled.
    /// </summary>
    public bool Enabled
    {
        get
        {
            return this.uniSelector.Enabled;
        }
        set
        {
            this.uniSelector.Enabled = value;
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        lblSiteTitle.Text = ResHelper.GetString("product_edit_tax.taxtitle");
        if (ProductID > 0)
        {
            DataSet ds = TaxClassInfoProvider.GetSKUTaxClasses(ProductID);
            if (!DataHelper.DataSourceIsEmpty(ds))
            {
                currentValues = String.Join(";", SqlHelperClass.GetStringValues(ds.Tables[0], "TaxClassID"));
            }

            if (!IsPostBack)
            {
                this.uniSelector.Value = currentValues;
            }
        }

        this.uniSelector.IconPath = GetObjectIconUrl("ecommerce.taxclass", "object.png");
        this.uniSelector.OnSelectionChanged += uniSelector_OnSelectionChanged;
    }


    protected void uniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        SaveItems();
    }


    protected void SaveItems()
    {
        // Remove old items
        string newValues = ValidationHelper.GetString(uniSelector.Value, null);
        string items = DataHelper.GetNewItemsInList(newValues, currentValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to user
                foreach (string item in newItems)
                {
                    int taxClassId = ValidationHelper.GetInteger(item, 0);
                    SKUTaxClassInfoProvider.RemoveTaxClassFromSKU(taxClassId, ProductID);
                }
            }
        }

        // Add new items
        items = DataHelper.GetNewItemsInList(currentValues, newValues, ';');
        if (!String.IsNullOrEmpty(items))
        {
            string[] newItems = items.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            if (newItems != null)
            {
                // Add all new items to user
                foreach (string item in newItems)
                {
                    int taxClassId = ValidationHelper.GetInteger(item, 0);
                    SKUTaxClassInfoProvider.AddTaxClassToSKU(taxClassId, ProductID);
                }
            }
        }

        lblInfo.Visible = true;
        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
    }
}
