﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MediaView.ascx.cs" Inherits="CMSModules_MediaLibrary_Controls_Dialogs_MediaView" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/LinkMediaSelector/Search.ascx"
    TagName="DialogSearch" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/LinkMediaSelector/InnerMediaView.ascx"
    TagName="InnerMediaView" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploader.ascx"
    TagName="DirectFileUploader" TagPrefix="cms" %>
<asp:PlaceHolder ID="plcListingInfo" runat="server" Visible="false" EnableViewState="false">
    <div class="DialogListingInfo">
        <asp:Label ID="lblListingInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"></asp:Label>
    </div>
</asp:PlaceHolder>
<div class="DialogViewContentTop">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td class="TextLeft">
                <asp:UpdatePanel ID="pnlUpdateDialog" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <cms:DialogSearch ID="dialogSearch" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <asp:PlaceHolder ID="plcParentButton" runat="server">
                <td class="TextRight">
                    <asp:LinkButton ID="btnParent" runat="server" CssClass="MenuItemEditSmall" EnableViewState="false">
                        <asp:Image ID="imgParent" runat="server" />
                        <span>
                            <% =mSaveText %>
                        </span>
                        <asp:HiddenField ID="hdnLastNodeParentID" runat="server" />
                    </asp:LinkButton>
                </td>
            </asp:PlaceHolder>
        </tr>
    </table>
</div>
<div class="DialogViewContentBottom">
    <asp:UpdatePanel ID="pnlUpdateView" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cms:InnerMediaView ID="innermedia" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
