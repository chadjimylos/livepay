using System;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.Messaging;
using CMS.UIControls;

public partial class CMSModules_Messaging_Controls_ViewMessage : CMSUserControl
{
    #region "Variables"

    private int mMessageId = 0;
    private MessageModeEnum mMessageMode = MessageModeEnum.Inbox;
    protected MessageInfo messageInfo = null;
    protected UserInfo currentUserInfo = null;
    protected UserInfo messageUserInfo = null;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Message ID
    /// </summary>
    public int MessageId
    {
        get
        {
            return mMessageId;
        }
        set
        {
            mMessageId = value;
        }
    }


    /// <summary>
    /// Message mode
    /// </summary>
    public MessageModeEnum MessageMode
    {
        get
        {
            return mMessageMode;
        }
        set
        {
            mMessageMode = value;
        }
    }


    /// <summary>
    /// Information text
    /// </summary>
    public string InformationText
    {
        get
        {
            return ucMessageUserButtons.InformationText;
        }
    }


    /// <summary>
    /// Error text
    /// </summary>
    public string ErrorText
    {
        get
        {
            return ucMessageUserButtons.ErrorText;
        }
    }

    #endregion


    #region "Page events"

    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        // Message id is set, display message details
        if (MessageId != 0)
        {
            // Find postback invoker
            string invokerName = Page.Request.Params.Get("__EVENTTARGET");
            // If postback was caused by user buttons
            if (invokerName.Contains(ucMessageUserButtons.UniqueID))
            {
                StopProcessing = false;
            }
            ReloadData();
        }
    }

    #endregion


    #region "Other methods"

    public void ReloadData()
    {
        if (StopProcessing)
        {
            // Do nothing
            ucMessageUserButtons.StopProcessing = true;
            ucUserPicture.StopProcessing = true;
        }
        else
        {
            ucMessageUserButtons.StopProcessing = false;
            ucUserPicture.StopProcessing = false;
            // Init message info
            messageInfo = MessageInfoProvider.GetMessageInfo(MessageId);

            if (messageInfo != null)
            {
                // Get current user info
                currentUserInfo = CMSContext.CurrentUser;
                // Get message user info
                if (MessageMode == MessageModeEnum.Inbox)
                {
                    messageUserInfo = UserInfoProvider.GetUserInfo(messageInfo.MessageSenderUserID);
                }
                else
                {
                    messageUserInfo = UserInfoProvider.GetUserInfo(messageInfo.MessageRecipientUserID);
                }

                // Display only to authorized user
                if ((currentUserInfo.UserID == messageInfo.MessageRecipientUserID) || (currentUserInfo.UserID == messageInfo.MessageSenderUserID) || currentUserInfo.IsGlobalAdministrator)
                {
                    pnlViewMessage.Visible = true;
                    lblDateCaption.Text = ResHelper.GetString("Messaging.Date");
                    lblSubjectCaption.Text = ResHelper.GetString("general.subject");
                    lblFromCaption.Text = (MessageMode == MessageModeEnum.Inbox) ? ResHelper.GetString("Messaging.From") : ResHelper.GetString("Messaging.To");

                    // Sender exists
                    if (messageUserInfo != null)
                    {
                        ucUserPicture.Visible = true;
                        ucUserPicture.UserID = messageUserInfo.UserID;
                        ucMessageUserButtons.RelatedUserId = messageUserInfo.UserID;
                        lblFrom.Text = HTMLHelper.HTMLEncode(UserInfoProvider.GetFullUserName(Functions.GetFormattedUserName(messageUserInfo.UserName, this.IsLiveSite), messageUserInfo.UserNickName));
                    }
                    else
                    {
                        ucMessageUserButtons.RelatedUserId = 0;
                        lblFrom.Text = HTMLHelper.HTMLEncode(messageInfo.MessageSenderNickName);
                    }
                    string body = messageInfo.MessageBody;
                    // Resolve macros
                    DiscussionMacroHelper dmh = new DiscussionMacroHelper();
                    body = dmh.ResolveMacros(body);

                    lblSubject.Text = HTMLHelper.HTMLEncodeLineBreaks(messageInfo.MessageSubject);
                    lblDate.Text = CMSContext.ConvertDateTime(messageInfo.MessageSent, this).ToString();
                    lblBody.Text = body;
                }
            }
        }
    }

    #endregion
}