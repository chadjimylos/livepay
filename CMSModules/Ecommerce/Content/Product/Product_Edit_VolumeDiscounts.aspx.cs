using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.FormControls;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;
using CMS.LicenseProvider;
using CMS.SiteProvider;

public partial class CMSModules_Ecommerce_Content_Product_Product_Edit_VolumeDiscounts : CMSContentPage
{
    protected int productId = 0;
    protected int nodeId = 0;

    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);

        this["TabControl"] = BasicTabControlMenu;
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        lblInfo.Visible = !string.IsNullOrEmpty(lblInfo.Text);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        CurrentUserInfo currentUser = CMSContext.CurrentUser;

        if (currentUser != null)
        {
            // Check the license
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Ecommerce);

            // Check site availability
            if (!ResourceSiteInfoProvider.IsResourceOnSite("CMS.Ecommerce", CMSContext.CurrentSiteName))
            {
                RedirectToResourceNotAvailableOnSite("CMS.Ecommerce");
            }

            // Check 'EcommerceRead' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceRead"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "EcommerceRead");
            }

            nodeId = ValidationHelper.GetInteger(Request.QueryString["nodeid"], 0);
            productId = ValidationHelper.GetInteger(Request.QueryString["productID"], 0);

            TreeProvider tree = new TreeProvider(currentUser);
            TreeNode node = null;
            node = DocumentHelper.GetDocument(nodeId, currentUser.PreferredCultureCode, tree);
            if (node != null)
            {
                // Check read permissions
                if (currentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) == AuthorizationResultEnum.Denied)
                {
                    RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
                }
                else
                {
                    // Check modify permissions
                    if (currentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Denied)
                    {
                        // disable form editing                                                            
                        DisableFormEditing();

                        // show access denied message
                        lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
                    }
                    // check User - Department association
                    else if (!currentUser.IsGlobalAdministrator)
                    {
                        SKUInfo skuObj = SKUInfoProvider.GetSKUInfo(productId);
                        if (skuObj != null)
                        {
                            if (!DepartmentInfoProvider.IsUserInDepartment(skuObj.SKUDepartmentID, currentUser.UserID))
                            {
                                // disable form editing                                                            
                                DisableFormEditing();

                                DepartmentInfo departmentObj = DepartmentInfoProvider.GetDepartmentInfo(skuObj.SKUDepartmentID);
                                if (departmentObj != null)
                                {
                                    // show access denied message
                                    lblInfo.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtperdepartment"), departmentObj.DepartmentName);
                                }
                            }
                        }
                    }

                    // Set unigrid properties
                    SetUnigridProperties();

                    // Set URL of new volume discount editing page
                    lnkNewItem.NavigateUrl = "javascript: modalDialog('" + ResolveUrl("~/CMSModules/Ecommerce/Content/Product/Product_Edit_VolumeDiscount_Edit.aspx") + "?ProductID=" + productId + "&dialog=1', 'Volume discounts', 500, 230);";
                    lnkNewItem.Text = ResHelper.GetString("Product_Edit_VolumeDiscount_List.NewItemCaption");
                    imgNewItem.ImageUrl = GetImageUrl("Objects/Ecommerce_VolumeDiscount/add.png");

                    // Register modal dialog script
                    ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);


                    // custom fields
                    DataForm df = new DataForm();
                    df.ClassName = "Ecommerce.SKU";
                    df.ItemID = productId;

                    int showCustomFields = (df.BasicForm.FormInformation.GetFormElements(true, false).Count <= 0 ? 0 : 1); ;

                    BasicTabControlMenu.Tabs = new string[5 + showCustomFields, 4];
                    int lastTabIndex = 0;

                    BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("general.general");
                    BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_General.aspx?productId=" + productId.ToString() + "&nodeId=" + nodeId.ToString();
                    lastTabIndex++;

                    if (showCustomFields != 0)
                    {
                        BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.CustomFields");
                        BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_CustomFields.aspx?productId=" + productId.ToString() + "&nodeId=" + nodeId.ToString();
                        lastTabIndex++;
                    }

                    BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.Tax");
                    BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_Tax.aspx?productId=" + productId.ToString() + "&nodeId=" + nodeId.ToString();
                    lastTabIndex++;

                    BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.VolumeDiscounts");
                    BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_VolumeDiscounts.aspx?productId=" + productId.ToString() + "&nodeId=" + nodeId.ToString();
                    BasicTabControlMenu.SelectedTab = lastTabIndex;
                    lastTabIndex++;

                    BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.Options");
                    BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_Options.aspx?productId=" + productId.ToString() + "&nodeId=" + nodeId.ToString();
                    lastTabIndex++;

                    BasicTabControlMenu.Tabs[lastTabIndex, 0] = ResHelper.GetString("Product_Edit_Header.Documents");
                    BasicTabControlMenu.Tabs[lastTabIndex, 2] = "Product_Edit_Documents.aspx?productId=" + productId + "&nodeId=" + nodeId;
                    lastTabIndex++;

                    BasicTabControlMenu.UrlTarget = "_self";

                    chkMarkDocAsProd.Text = ResHelper.GetString("Product_Selection.DocIsAsProd");
                }
            }
        }
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGrid_OnAction(string actionName, object actionArgument)
    {
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        if (actionName.ToLower() == "edit")
        {
            // javascript			
        }
        else if (actionName.ToLower() == "delete")
        {
            if (chkMarkDocAsProd.Checked)
            {
                // delete VolumeDiscountInfo object from database
                VolumeDiscountInfoProvider.DeleteVolumeDiscountInfo(Convert.ToInt32(actionArgument));
            }
        }
    }


    /// <summary>
    /// Handles the UniGrid's OnExternalDataBound event.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="sourceName">Source name.</param>
    /// <param name="parameter">Parameter.</param>
    protected object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "discountvalue":
                DataRowView row = (DataRowView)parameter;
                double value = ValidationHelper.GetDouble(row["VolumeDiscountValue"], 0);
                bool isFlat = ValidationHelper.GetBoolean(row["VolumeDiscountIsFlatValue"], false);
                // If value is relative, add "%" next to the value.
                if (isFlat)
                {
                    CurrencyInfo ci = CurrencyInfoProvider.GetMainCurrency();
                    return CurrencyInfoProvider.GetFormatedPrice(value, ci);
                }
                else
                {
                    return value.ToString() + "%";
                }

            case "edit":
                if (!chkMarkDocAsProd.Checked || !chkMarkDocAsProd.Enabled)
                {
                    // Disable editing
                    ((WebControl)sender).Attributes["onclick"] = "return false;";
                }
                else
                {
                    // Open modal dialog with volume discount edit form
                    int volumeDiscountId = ValidationHelper.GetInteger(((DataRowView)((GridViewRow)parameter).DataItem)[0], 0);
                    ((WebControl)sender).Attributes["onclick"] = "modalDialog('" + ResolveUrl("~/CMSModules/Ecommerce/Content/Product/Product_Edit_VolumeDiscount_Edit.aspx") + "?ProductID=" + productId + "&VolumeDiscountID=" + volumeDiscountId + "&dialog=1', 'Volume discounts', 500, 260); return false;";
                }
                break;

            case "delete":
                if (!chkMarkDocAsProd.Checked || !chkMarkDocAsProd.Enabled)
                {
                    // Disable deleting
                    ((WebControl)sender).Attributes["onclick"] = "return false;";
                }
                break;
        }
        return parameter;
    }


    protected void SetUnigridProperties()
    {
        // Set action handler
        UniGrid.OnAction += new OnActionEventHandler(UniGrid_OnAction);
        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);
        UniGrid.WhereCondition = "VolumeDiscountSKUID = " + productId;
    }


    protected void chkMarkDocAsProd_CheckedChanged(object sender, EventArgs e)
    {
        lnkNewItem.Enabled = chkMarkDocAsProd.Checked;
    }


    /// <summary>
    /// Disables form editing.
    /// </summary>
    protected void DisableFormEditing()
    {
        chkMarkDocAsProd.Enabled = false;
        lnkNewItem.Enabled = false;
    }
}

