using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.DataEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSWebParts_UserContributions_ContributionList : CMSAbstractWebPart
{
    #region "Document properties"

    /// <summary>
    /// Gets or sets the class names (document types) separated with semicolon, which should be displayed
    /// </summary>
    public string ClassNames
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("Classnames"), this.list.ClassNames), this.list.ClassNames);
        }
        set
        {
            this.SetValue("ClassNames", value);
            this.list.ClassNames = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether the default language version of the document 
    /// should be displayed if the document is not translated to the current language.
    /// </summary>
    public bool CombineWithDefaultCulture
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("CombineWithDefaultCulture"), this.list.CombineWithDefaultCulture);
        }
        set
        {
            this.SetValue("CombineWithDefaultCulture", value);
            this.list.CombineWithDefaultCulture = value;
        }
    }


    /// <summary>
    /// Gets or sets the culture version of the displayed content
    /// </summary>
    public string CultureCode
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("CultureCode"), this.list.CultureCode), this.list.CultureCode);
        }
        set
        {
            this.SetValue("CultureCode", value);
            this.list.CultureCode = value;
        }
    }


    /// <summary>
    /// Gets or sets the maximum nesting level. It specifies the number of sub-levels in the content tree 
    /// that should be included in the displayed content.
    /// </summary>
    public int MaxRelativeLevel
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("MaxRelativeLevel"), this.list.MaxRelativeLevel);
        }
        set
        {
            this.SetValue("MaxRelativeLevel", value);
            this.list.MaxRelativeLevel = value;
        }
    }


    /// <summary>
    /// Gets or sets the ORDER BY part of the SELECT query
    /// </summary>
    public string OrderBy
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("OrderBy"), this.list.OrderBy), this.list.OrderBy);
        }
        set
        {
            this.SetValue("OrderBy", value);
            this.list.OrderBy = value;
        }
    }


    /// <summary>
    /// Gets or sets the path to the document
    /// </summary>
    public string Path
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("Path"), null), null);
        }
        set
        {
            this.SetValue("Path", value);
            this.list.Path = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether to show only published documents
    /// </summary>
    public bool SelectOnlyPublished
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("SelectOnlyPublished"), this.list.SelectOnlyPublished);
        }
        set
        {
            this.SetValue("SelectOnlyPublished", value);
            this.list.SelectOnlyPublished = value;
        }
    }


    /// <summary>
    /// Gets or sets the codename of the site from which you want to display the content
    /// </summary>
    public string SiteName
    {
        get
        {
            return DataHelper.GetNotEmpty(ValidationHelper.GetString(this.GetValue("SiteName"), this.list.SiteName), this.list.SiteName);
        }
        set
        {
            this.SetValue("SiteName", value);
            this.list.SiteName = value;
        }
    }


    /// <summary>
    /// Gets or sets the WHERE part of the SELECT query
    /// </summary>
    public string WhereCondition
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("WhereCondition"), this.list.WhereCondition);
        }
        set
        {
            this.SetValue("WhereCondition", value);
            this.list.WhereCondition = value;
        }
    }

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the value that indicates whether the list of documents should be displayed
    /// </summary>
    public bool DisplayList
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("DisplayList"), this.list.DisplayList);
        }
        set
        {
            this.SetValue("DisplayList", value);
            this.list.DisplayList = value;
        }
    }


    /// <summary>
    /// Gets or sets the path for new created documents
    /// </summary>
    public string NewDocumentPath
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("NewDocumentPath"), this.list.NewDocumentPath);
        }
        set
        {
            this.SetValue("NewDocumentPath", value);
            this.list.NewDocumentPath = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether inserting new document is allowed
    /// </summary>
    public bool AllowInsert
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("AllowInsert"), this.list.AllowInsert);
        }
        set
        {
            this.SetValue("AllowInsert", value);
            this.list.AllowInsert = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether editing document is allowed
    /// </summary>
    public bool AllowEdit
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("AllowEdit"), this.list.AllowEdit);
        }
        set
        {
            this.SetValue("AllowEdit", value);
            this.list.AllowEdit = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether deleting document is allowed
    /// </summary>
    public bool AllowDelete
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("AllowDelete"), this.list.AllowDelete);
        }
        set
        {
            this.SetValue("AllowDelete", value);
            this.list.AllowDelete = value;
        }
    }


    /// <summary>
    /// Gets or sets the group of users which can work with the documents
    /// </summary>
    public UserContributionAllowUserEnum AllowUsers
    {
        get
        {
            object value = this.GetValue("AllowUsers");

            if (value == null)
            {
                return UserContributionAllowUserEnum.DocumentOwner;
            }
            else if (ValidationHelper.IsInteger(value))
            {
                return (UserContributionAllowUserEnum)(ValidationHelper.GetInteger(value, 2));
            }
            else
            {
                return (UserContributionAllowUserEnum)(value);
            }
        }
        set
        {
            this.SetValue("AllowUsers", value);
            this.list.AllowUsers = value;
        }
    }


    /// <summary>
    /// Gets or sets the page template the new items are assigned to
    /// </summary>
    public string NewItemPageTemplate
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("NewItemPageTemplate"), this.list.NewItemPageTemplate);
        }
        set
        {
            this.SetValue("NewItemPageTemplate", value);
            this.list.NewItemPageTemplate = value;
        }
    }


    /// <summary>
    /// Gets or sets the type of the child documents that are allowed to be created
    /// </summary>
    public string AllowedChildClasses
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("AllowedChildClasses"), this.list.AllowedChildClasses);
        }
        set
        {
            this.SetValue("AllowedChildClasses", value);
            this.list.AllowedChildClasses = value;
        }
    }


    /// <summary>
    /// Gets or sets alternative form name
    /// </summary>
    public string AlternativeFormName
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("AlternativeFormName"), this.list.AlternativeFormName);
        }
        set
        {
            this.SetValue("AlternativeFormName", value);
            this.list.AlternativeFormName = value;
        }
    }


    /// <summary>
    /// Gets or sets the message which is displayed after validation failed
    /// </summary>
    public string ValidationErrorMessage
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("ValidationErrorMessage"), this.list.ValidationErrorMessage);
        }
        set
        {
            this.SetValue("ValidationErrorMessage", value);
            this.list.ValidationErrorMessage = value;
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether the permissions are checked
    /// </summary>
    public bool CheckPermissions
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("CheckPermissions"), this.list.CheckPermissions);
        }
        set
        {
            this.SetValue("CheckPermissions", value);
            this.list.CheckPermissions = value;
        }
    }


    /// <summary>
    /// Gets or sets new item button label.
    /// </summary>
    public string NewItemButtonText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("NewItemButtonText"), this.list.NewItemButtonText);
        }
        set
        {
            this.SetValue("NewItemButtonText", value);
            this.list.NewItemButtonText = value;
        }
    }


    /// <summary>
    /// Gets or sets List button label.
    /// </summary>
    public string ListButtonText
    {
        get
        {
            return DataHelper.GetNotEmpty(this.GetValue("ListButtonText"), this.list.ListButtonText);
        }
        set
        {
            this.SetValue("ListButtonText", value);
            this.list.ListButtonText = value;
        }
    }

    #endregion


    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
        AttachEvents();
    }


    /// <summary>
    /// Reloads data for partial caching
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
        AttachEvents();
    }


    /// <summary>
    /// Event registration
    /// </summary>
    protected void AttachEvents()
    {
        this.list.EditForm.OnAfterApprove += new EventHandler(EditForm_OnAfterChange);
        this.list.EditForm.OnAfterReject += new EventHandler(EditForm_OnAfterChange);
        this.list.EditForm.OnAfterDelete += new EventHandler(EditForm_OnAfterChange);

        this.list.EditForm.CMSForm.OnAfterSave += new CMS.FormControls.CMSForm.OnAfterSaveEventHandler(CMSForm_OnAfterSave);

        this.list.OnAfterDelete += new EventHandler(EditForm_OnAfterChange);
    }


    void EditForm_OnAfterChange(object sender, EventArgs e)
    {
        CMSForm_OnAfterSave();
    }


    void CMSForm_OnAfterSave()
    {
        if (!this.StandAlone)
        {
            // Reload data after saving the document
            this.PagePlaceholder.ClearCache();
            this.PagePlaceholder.ReloadData();
        }
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            this.list.StopProcessing = true; // Do nothing
        }
        else
        {
            this.list.ControlContext = this.ControlContext;
            this.list.AllowEdit = this.AllowEdit;
            this.list.AllowInsert = this.AllowInsert;
            this.list.AllowDelete = this.AllowDelete;
            this.list.CheckPermissions = this.CheckPermissions;
            this.list.AllowedChildClasses = this.AllowedChildClasses;
            this.list.NewItemPageTemplate = this.NewItemPageTemplate;
            this.list.AllowUsers = this.AllowUsers;
            this.list.WhereCondition = this.WhereCondition;
            this.list.SiteName = this.SiteName;
            this.list.SelectOnlyPublished = this.SelectOnlyPublished;
            this.list.Path = this.Path;
            this.list.OrderBy = this.OrderBy;
            this.list.MaxRelativeLevel = this.MaxRelativeLevel;
            this.list.CultureCode = this.CultureCode;
            this.list.CombineWithDefaultCulture = this.CombineWithDefaultCulture;
            this.list.ClassNames = this.ClassNames;
            this.list.DisplayList = this.DisplayList;
            this.list.NewDocumentPath = this.NewDocumentPath;
            this.list.AlternativeFormName = this.AlternativeFormName;
            this.list.ValidationErrorMessage = this.ValidationErrorMessage;
            this.list.NewItemButtonText = this.NewItemButtonText;
            this.list.ListButtonText = this.ListButtonText;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        if (this.list.StopProcessing)
        {
            // Hide control if stop processing is set
            this.Visible = false;
        }
        base.OnPreRender(e);
    }
}
