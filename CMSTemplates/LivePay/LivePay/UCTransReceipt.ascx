﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCTransReceipt.ascx.vb" Inherits="CMSTemplates_LivePay_UCTransReceipt" %>
<script>
    function ExportDetails(url) {
        document.getElementById('<%=exportpopFrame.ClientID %>').src = url;
        return false
    }
</script>
<div style="display:none">
<iframe id="exportpopFrame" runat="server" ></iframe>
</div>
<div class="PT20">
     <div class="TransRecMainContent">
	<div class="TransRecTblTitle" ><%=ResHelper.LocalizeString("{$=Αποδεικτικό Συναλλαγής|en-us=Αποδεικτικό Συναλλαγής$}") %></div>
	<div class="TransRecTblTick" ><img id="imgResult" runat="server" src="/app_themes/LivePay/GreenTick.gif"></div>
	<div class="TransRecTblGreenTitle" id="MessageTitle" runat="server" ></div>
	<div class="Clear"></div>
	<div style="padding-top:20px;padding-left:20px" id="MainDivForm" runat="server">
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Ημερομηνία & Ώρα|en-us=Ημερομηνία & Ώρας$}") %></div>
	   <div class="TransRecRightRow" id="tdDateTime" runat="server" >Τετάρτη 31 Μαρτίου 2010, 17:45</div>
	   <div class="Clear"></div>
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Πληρωμή προς|en-us=Πληρωμή προς$}") %></div>
	   <div class="TransRecRightRow" id="tdMerchant" runat="server" ></div>
	   <div class="Clear"></div>
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Κωδικός Συναλλαγής|en-us=Κωδικός Συναλλαγής$}") %></div>
	   <div class="TransRecRightRow" id="tdTransCode" runat="server" ></div>
	   <div class="Clear"></div>
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Κωδικός Πελάτη|en-us=Κωδικός Πελάτη$}") %></div>
	   <div class="TransRecRightRow" id="tdCustCode" runat="server" ></div>
	   <div class="Clear"></div>
	   <div id="CustomFieldsPanel" runat="server">
       </div>
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}") %></div>
	   <div class="TransRecRightRow" id="tdPrice" runat="server" ></div>
	   <div class="Clear"></div>
	   <div class="TransRecLeftRow"><%=ResHelper.LocalizeString("{$=Κάρτα|en-us=Κάρτα$}") %></div>
	   <div class="TransRecRightRow" id="tdCard" runat="server" ></div>
	   <div class="Clear"></div>
	   <div class="PT10">
		<div class="FLEFT"><img class="Chand"  onclick="ExportDetails('/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=TransReceipt&lang=<%=CMSContext.CurrentDocumentCulture.CultureCode %>')" src="/app_themes/LivePay/btnSavePDF.gif" /></div>
		<div class="FLEFT PL10"><img class="Chand" onclick="ExportDetails('/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=TransReceipt&lang=<%=CMSContext.CurrentDocumentCulture.CultureCode %>')" src="/app_themes/LivePay/btnPrint.png" /></div>
        <div class="FLEFT PL10"><asp:ImageButton ImageUrl="/app_themes/LivePay/BtnSave.png" runat="server" ID="btnSave" /></div>
		<div class="Clear"></div>
	   </div>
	</div>
    <div style="line-height:22px;font-size:12px;color:#43474a;padding-top:20px;display:none;padding-left:10px" id="MainDivErrorContainer" runat="server">
    </div> 
      </div>
      <div><img src="/app_themes/LivePay/TransRecContentBGBot.png" /></div>
  </div>