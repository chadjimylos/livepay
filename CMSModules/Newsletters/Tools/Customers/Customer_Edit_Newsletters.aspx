<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Customer_Edit_Newsletters.aspx.cs"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Inherits="CMSModules_Newsletters_Tools_Customers_Customer_Edit_Newsletters"
    Theme="Default" Title="Customer newsletters" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>


<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblTitle" CssClass="InfoLabel" EnableViewState="false" />
    <cms:UniSelector ID="usNewsletters" runat="server" ObjectType="Newsletter.Newsletter"
        SelectionMode="Multiple" ResourcePrefix="newsletterselect" IsLiveSite="false" />
</asp:Content>
