<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewSiteType.ascx.cs" Inherits="CMSModules_ImportExport_Controls_NewSiteType" %>

<asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
<table>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="radBlank" Checked="true" GroupName="Type" />
        </td>
    </tr>
    <tr>
        <td>
            <asp:RadioButton runat="server" ID="radTemplate" GroupName="Type" />
        </td>
    </tr>
</table>
