using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.Reporting;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Reporting_Tools_ReportTable_Edit : CMSReportingModalPage
{
    protected ReportTableInfo tableInfo = null;
    protected int tableId;
    protected ReportInfo reportInfo = null;

    private bool newReportTable = true;


    /// <summary>
    /// PreRender event handler
    /// </summary>
    protected override void OnPreRender(EventArgs e)
    {
        // Register the dialog script
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
        this.AddNoCacheTag();
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        this.RegisterModalPageScripts();

        int reportId = ValidationHelper.GetInteger(Request.QueryString["reportid"], 0);

        if (reportId > 0)
        {
            reportInfo = ReportInfoProvider.GetReportInfo(reportId);
        }

        if (reportInfo != null) //must be valid reportid parameter
        {
            string tableName = ValidationHelper.GetString(Request.QueryString["itemname"], "");

            // try to load tableName from hidden field (adding new graph & preview)
            if (tableName == String.Empty)
            {
                tableName = txtNewTableHidden.Value;
            }

            if (ValidationHelper.IsCodeName(tableName))
            {
                tableInfo = ReportTableInfoProvider.GetReportTableInfo(tableName);
                tableId = tableInfo.TableID;

                // indicates that this is edit, not add new table action
                newReportTable = false;
            }


            rfvCodeName.ErrorMessage = ResHelper.GetString("general.requirescodename");
            rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");
            rfvQuery.ErrorMessage = ResHelper.GetString("Reporting_ReportTable_Edit.ErrorQuery");

            // Control initializations
            lblQuery.Text = ResHelper.GetString("Reporting_ReportTable_Edit.Query");
            lblStoredProcedure.Text = ResHelper.GetString("Reporting_ReportTable_Edit.StoredProcedure");
            lblSkinID.Text = ResHelper.GetString("Reporting_ReportTable_Edit.SkinID");            

            if (tableInfo != null)
            {
                this.CurrentMaster.Title.TitleText = ResHelper.GetString("Reporting_ReportTable_Edit.TitleText");
                this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Reporting_ReportTable/object.png");
            }
            else //new item
            {
                this.CurrentMaster.Title.TitleText = ResHelper.GetString("Reporting_ReportTable_Edit.NewItemTitleText");
                this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Reporting_ReportTable/new.png");
            }

            // set help key
            this.CurrentMaster.Title.HelpTopicName = "report_table_properties";

            if (!RequestHelper.IsPostBack())
            {
                DataHelper.FillListControlWithEnum(typeof(PagerButtons), drpPageMode, "PagerButtons.", null);
                // Preselect page numbers paging
                drpPageMode.SelectedValue = ((int)PagerButtons.Numeric).ToString();

                LoadData();
            }
        }
        else
        {
            pnlElements.Visible = false;
            btnOk.Visible = false;
            btnCancel.Visible = false;
            btnPreview.Visible = false;
            lblError.Visible = true;
            lblError.Text = ResHelper.GetString("Reporting_ReportTable_Edit.InvalidReportId");
        }
        btnOk.Text = ResHelper.GetString("General.OK");
        btnCancel.Text = ResHelper.GetString("General.Cancel");
        btnPreview.Text = ResHelper.GetString("General.SaveAndPreview");
    }


    protected void LoadData()
    {
        if (tableInfo != null)
        {
            txtDisplayName.Text = tableInfo.TableDisplayName;
            txtCodeName.Text = tableInfo.TableName;
            txtQuery.Text = tableInfo.TableQuery;
            chkStoredProcedure.Checked = tableInfo.TableQueryIsStoredProcedure;
            txtSkinID.Text = ValidationHelper.GetString(tableInfo.TableSettings["skinid"], "");
            txtPageSize.Text = ValidationHelper.GetInteger(tableInfo.TableSettings["pagesize"], 10).ToString();
            chkEnablePaging.Checked = ValidationHelper.GetBoolean(tableInfo.TableSettings["enablepaging"], false);
            drpPageMode.SelectedValue = ValidationHelper.GetString(tableInfo.TableSettings["pagemode"], "");
        }
    }


    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (Save())
        {
            ltlScript.Text += ScriptHelper.GetScript("wopener.ReloadPage();window.close();");
        }
    }


    protected void btnPreview_Click(object sender, EventArgs e)
    {
        if (Save())
        {
            if (newReportTable)
            {
                txtNewTableHidden.Value = tableInfo.TableName;
            }

            int windowWidth = 800;
            int windowHeight = 600;
            ltlScript.Text += ScriptHelper.GetScript("modalDialog('Report_Preview.aspx?type=table&id=" + tableInfo.TableID + "','ReportPreview'," + windowWidth + "," + windowHeight + ",'dependent=YES,dialog=YES,modal=YES,resizable=NO,scrollbars=YES,location=0,status=0,menubar=0,toolbar=0');");
        }
    }


    /// <summary>
    /// Save data
    /// </summary>
    /// <returns>return true if save was successfull</returns>
    protected bool Save()
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.reporting", "Modify"))
        {
            RedirectToAccessDenied("cms.reporting", "Modify");
        }

        string errorMessage = new Validator().NotEmpty(txtDisplayName.Text, rfvDisplayName.ErrorMessage).NotEmpty(txtCodeName.Text, rfvCodeName.ErrorMessage).NotEmpty(txtQuery.Text, rfvQuery.ErrorMessage).Result;

        if ((errorMessage == "") && (!ValidationHelper.IsIdentificator(txtCodeName.Text.Trim())))
        {
            errorMessage = ResHelper.GetString("general.erroridentificatorformat");
        }

        if ((errorMessage == "") && (txtPageSize.Text.Trim() != String.Empty) && (!ValidationHelper.IsInteger(txtPageSize.Text) || !ValidationHelper.IsPositiveNumber(txtPageSize.Text)))
        {
            errorMessage = ResHelper.GetString("Reporting_ReportTable_Edit.errorinvalidpagesize");
        }

        string fullName = reportInfo.ReportName + "." + txtCodeName.Text.Trim();
        ReportTableInfo codeNameCheck = ReportTableInfoProvider.GetReportTableInfo(fullName);
        if ((errorMessage == "") && (codeNameCheck != null) && (codeNameCheck.TableID != tableId))
            errorMessage = ResHelper.GetString("Reporting_ReportTable_Edit.ErrorCodeNameExist");

        if ((errorMessage == ""))
        {
            if (tableInfo == null) //new table
                tableInfo = new ReportTableInfo();

            tableInfo.TableDisplayName = txtDisplayName.Text.Trim();
            tableInfo.TableName = txtCodeName.Text.Trim();
            tableInfo.TableQuery = txtQuery.Text.Trim();
            tableInfo.TableQueryIsStoredProcedure = chkStoredProcedure.Checked;
            tableInfo.TableReportID = reportInfo.ReportID;

            tableInfo.TableSettings["SkinID"] = txtSkinID.Text.Trim();
            tableInfo.TableSettings["enablepaging"] = chkEnablePaging.Checked.ToString();
            tableInfo.TableSettings["pagesize"] = txtPageSize.Text;
            tableInfo.TableSettings["pagemode"] = drpPageMode.SelectedValue;

            ReportTableInfoProvider.SetReportTableInfo(tableInfo);
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
            return false;
        }

        return true;
    }
}
