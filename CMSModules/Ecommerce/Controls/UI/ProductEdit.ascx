<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProductEdit.ascx.cs" Inherits="CMSModules_Ecommerce_Controls_UI_ProductEdit" %>
<%@ Register Src="~/CMSAdminControls/MetaFiles/File.ascx" TagName="File" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/Controls/UI/PriceSelector.ascx" TagName="PriceSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/FormControls/DepartmentSelector.ascx" TagName="DepartmentSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/FormControls/ManufacturerSelector.ascx"
    TagName="ManufacturerSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/FormControls/SupplierSelector.ascx" TagName="SupplierSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/FormControls/PublicStatusSelector.ascx"
    TagName="PublicStatusSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/Ecommerce/FormControls/InternalStatusSelector.ascx"
    TagName="InternalStatusSelector" TagPrefix="cms" %>
<asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
    Visible="false" />
<table style="vertical-align: top; padding-top: 5px;">
    <tr>
        <td class="FieldLabel" style="height: 26px">
            <asp:Label runat="server" ID="lblSKUName" EnableViewState="false" />
        </td>
        <td style="height: 26px">
            <asp:TextBox ID="txtSKUName" runat="server" CssClass="TextBoxField" MaxLength="440"
                EnableViewState="false" />
            <asp:RequiredFieldValidator ID="rfvSKUName" runat="server" ControlToValidate="txtSKUName"
                Display="Dynamic" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <cms:LocalizedLabel runat="server" ID="lblSKUEnabled" EnableViewState="false" ResourceString="general.enabled"
                DisplayColon="true" />
        </td>
        <td>
            <asp:CheckBox ID="chkSKUEnabled" runat="server" CssClass="CheckBoxMovedLeft" Checked="true"
                EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUNumber" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtSKUNumber" runat="server" CssClass="TextBoxField" MaxLength="200"
                EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top; padding-top: 5px" class="FieldLabel">
            <cms:LocalizedLabel ID="lblSKUDescription" runat="server" EnableViewState="false"
                ResourceString="general.description" DisplayColon="true" />
        </td>
        <td>
            <cms:CMSHtmlEditor ID="htmlTemplateBody" runat="server" Width="400px" Height="300px" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" style="height: 26px">
            <asp:Label runat="server" ID="lblSKUPrice" EnableViewState="false" />
        </td>
        <td style="height: 26px">
            <cms:PriceSelector ID="txtSKUPrice" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUDepartmentID" EnableViewState="false" />
        </td>
        <td>
            <cms:DepartmentSelector runat="server" ID="departmentElem" AddAllItemsRecord="false"
                AddAllMyRecord="false" AddNoneRecord="false" UseDepartmentNameForSelection="false"
                IsLiveSite="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUManufacturerID" EnableViewState="false" />
        </td>
        <td>
            <cms:ManufacturerSelector runat="server" ID="manufacturerElem" AddAllItemsRecord="false"
                AddNoneRecord="true" IsLiveSite="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUSupplierID" EnableViewState="false" />
        </td>
        <td>
            <cms:SupplierSelector runat="server" ID="supplierElem" AddAllItemsRecord="false"
                AddNoneRecord="true" IsLiveSite="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUPublicStatusID" EnableViewState="false" />
        </td>
        <td>
            <cms:PublicStatusSelector runat="server" ID="publicStatusElem" AddAllItemsRecord="false"
                AddNoneRecord="true" UseStatusNameForSelection="false" IsLiveSite="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUInternalStatusID" EnableViewState="false" />
        </td>
        <td>
            <cms:InternalStatusSelector runat="server" ID="internalStatusElem" AddAllItemsRecord="false"
                AddNoneRecord="true" UseStatusNameForSelection="false" IsLiveSite="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUAvailableInDays" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtSKUAvailableInDays" runat="server" CssClass="TextBoxField" MaxLength="5"
                EnableViewState="false" />
            <asp:RangeValidator ID="rvSKUAvailableInDays" runat="server" ControlToValidate="txtSKUAvailableInDays"
                MaximumValue="99999" MinimumValue="-9999" Type="Integer" Display="Dynamic" EnableViewState="false" />
        </td>
    </tr>
    <asp:PlaceHolder ID="plcImagePath" runat="server" Visible="false">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblSKUImagePath" EnableViewState="false" />
            </td>
            <td>
                <cms:ImageSelector ID="imgSelect" runat="server" ImageHeight="50" ShowImagePreview="true"
                    ShowClearButton="true" UseImagePath="true" IsLiveSite="false" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="plcMetaFile" runat="server" Visible="false">
        <tr>
            <td class="FieldLabel">
                <asp:Label runat="server" ID="lblSKUMetaFile" EnableViewState="false" />
            </td>
            <td>
                <cms:File ID="ucMetaFile" runat="server" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td colspan="2" style="padding-top: 10px; font-weight: bold;">
            <asp:Label runat="server" ID="lblPackage" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel" style="height: 26px">
            <asp:Label runat="server" ID="lblSKUWeight" EnableViewState="false" />
        </td>
        <td style="height: 26px">
            <asp:TextBox ID="txtSKUWeight" runat="server" CssClass="TextBoxField" EnableViewState="false"
                MaxLength="10" />
            <asp:RangeValidator ID="rvSKUWeight" runat="server" Type="Double" ControlToValidate="txtSKUWeight"
                MaximumValue="9999999999" MinimumValue="0" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUHeight" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtSKUHeight" runat="server" CssClass="TextBoxField" EnableViewState="false"
                MaxLength="10" />
            <asp:RangeValidator ID="rvSKUHeight" runat="server" ControlToValidate="txtSKUHeight"
                MaximumValue="9999999999" MinimumValue="0" Type="Double" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUWidth" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtSKUWidth" runat="server" CssClass="TextBoxField" EnableViewState="false"
                MaxLength="10" />
            <asp:RangeValidator ID="rvSKUWidth" runat="server" ControlToValidate="txtSKUWidth"
                MaximumValue="9999999999" MinimumValue="0" Type="Double" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUDepth" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtSKUDepth" runat="server" CssClass="TextBoxField" EnableViewState="false"
                MaxLength="10" />
            <asp:RangeValidator ID="rvSKUDepth" runat="server" ControlToValidate="txtSKUDepth"
                MaximumValue="9999999999" MinimumValue="0" Type="Double" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-top: 10px; font-weight: bold;">
            <asp:Label runat="server" ID="lblInventory" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUAvailableItems" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtSKUAvailableItems" runat="server" CssClass="TextBoxField" EnableViewState="false"
                MaxLength="9" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label runat="server" ID="lblSKUSellOnlyAvailable" EnableViewState="false" />
        </td>
        <td>
            <asp:CheckBox ID="chkSKUSellOnlyAvailable" runat="server" CssClass="CheckBoxMovedLeft"
                Checked="false" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <cms:CMSButton runat="server" ID="btnOk" EnableViewState="false" CssClass="SubmitButton"
                OnClick="btnOK_Click" />
        </td>
    </tr>
</table>
