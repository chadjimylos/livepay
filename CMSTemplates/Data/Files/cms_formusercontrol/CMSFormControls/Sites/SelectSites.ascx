<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectSites.ascx.cs"
    Inherits="CMSFormControls_Sites_SelectSites" %>

<%@ Register Src="~/CMSFormControls/Sites/selectsite.ascx" TagName="SelectSite" TagPrefix="cms" %>

<cms:SelectSite ID="selectSite" runat="server" AllowMultipleSelection="true" />
