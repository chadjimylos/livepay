using System;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Collections;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSDesk_Tools_LeftMenu : CMSToolsPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        this.menuElem.RootTargetURL = ResolveUrl("~/CMSDesk/Tools/modules.aspx");
        this.menuElem.OnNodeCreated += new CMSAdminControls_UI_UIProfiles_UIMenu.NodeCreatedEventHandler(menuElem_OnNodeCreated);

        ScriptHelper.RegisterClientScriptBlock(this.Page, typeof(string), "AdministrationLoadItem", ScriptHelper.GetScript(
            "function LoadItem(elementName, elementUrl) \n" +
            "{ \n" +
            "    parent.frames['frameMain'].location.href = elementUrl; \n" +
            "} \n"));
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        // Call the script for tab which is selected
        if (this.menuElem.MenuEmpty)
        {
            ScriptHelper.RegisterStartupScript(this.Page, typeof(string), "FirstTabSelection", ScriptHelper.GetScript(" parent.frames['frameMain'].location.href = '" + UrlHelper.ResolveUrl("~/CMSMessages/Information.aspx") + "?message=" + ResHelper.GetString("uiprofile.uinotavailable") + "'; "));
        }
    }


    TreeNode menuElem_OnNodeCreated(UIElementInfo uiElement, TreeNode defaultNode)
    {
        if (uiElement != null)
        {
            if (!IsToolsUIElementAvailable(uiElement.ElementName))
            {
                return null;
            }
        }
        return defaultNode;
    }
}
