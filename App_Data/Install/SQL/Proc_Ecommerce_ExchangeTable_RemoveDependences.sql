-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Ecommerce_ExchangeTable_RemoveDependences]
	@ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
	-- COM_CurrencyExchangeRate
    DELETE FROM [COM_CurrencyExchangeRate] WHERE exchangetableId = @ID;
	COMMIT TRANSACTION;
END
