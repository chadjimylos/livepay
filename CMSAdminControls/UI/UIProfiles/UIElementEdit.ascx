﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UIElementEdit.ascx.cs"
    Inherits="CMSAdminControls_UI_UIProfiles_UIElementEdit" %>
<%@ Register Src="~/CMSAdminControls/UI/UIProfiles/SelectUIElement.ascx" TagName="SelectUIElement"
    TagPrefix="cms" %>
<cms:LocalizedLabel ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<cms:LocalizedLabel ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
    Visible="false" />
<table>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblDisplayName" runat="server" EnableViewState="false" DisplayColon="true"
                ResourceString="resource.ui.displayname" />
        </td>
        <td>
            <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="200"
                EnableViewState="false" />
            <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName"
                ValidationGroup="UIProfile" Display="Dynamic" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblName" runat="server" EnableViewState="false" DisplayColon="true"
                ResourceString="resource.ui.name" />
        </td>
        <td>
            <asp:TextBox ID="txtName" runat="server" CssClass="TextBoxField" MaxLength="200"
                EnableViewState="false" />
            <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName"
                ValidationGroup="UIProfile" Display="Dynamic" EnableViewState="false" />
        </td>
    </tr>
    <asp:PlaceHolder ID="plcParentElem" runat="server">
        <tr>
            <td>
                <cms:LocalizedLabel ID="lblParentElem" runat="server" EnableViewState="false" DisplayColon="true"
                    ResourceString="resource.ui.parent" />
            </td>
            <td>
                <cms:SelectUIElement ID="elemSelector" runat="server" CssClass="ContentDropdown"
                    UseElementNameForSelection="false" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblCustomElem" runat="server" EnableViewState="false" DisplayColon="true"
                ResourceString="resource.ui.customelem" />
        </td>
        <td>
            <asp:CheckBox ID="chkCustom" runat="server" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <strong>
                <cms:LocalizedLabel ID="lblMenuItems" runat="server" EnableViewState="false" ResourceString="uiprofiles.menuitem" />
            </strong>
        </td>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblCaption" runat="server" EnableViewState="false" DisplayColon="true"
                ResourceString="resource.ui.caption" />
        </td>
        <td>
            <asp:TextBox ID="txtCaption" runat="server" CssClass="TextBoxField" MaxLength="200"
                EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblTargetURL" runat="server" EnableViewState="false" DisplayColon="true"
                ResourceString="resource.ui.targeturl" />
        </td>
        <td>
            <asp:TextBox ID="txtTargetURL" runat="server" CssClass="TextBoxField" MaxLength="450"
                EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblIconPath" runat="server" EnableViewState="false" DisplayColon="true"
                ResourceString="resource.ui.iconpath" />
        </td>
        <td>
            <asp:TextBox ID="txtIconPath" runat="server" CssClass="TextBoxField" MaxLength="200"
                EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
        </td>
        <td>
            <cms:LocalizedButton ID="btnOk" runat="server" ResourceString="general.ok" CssClass="SubmitButton"
                OnClick="btnOk_Click" ValidationGroup="UIProfile" EnableViewState="false" />
        </td>
    </tr>
</table>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />