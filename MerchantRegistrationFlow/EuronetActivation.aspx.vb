﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Services
Imports CMS.SiteProvider
Imports CMS.EventLog


Namespace Kentico_MerchantActivationWorkflow
    Public Class EuronetActivation
        Inherits System.Web.UI.Page



        Public Function SetUserToRole(requesteduserGuid As Guid, rolename As String) As String

            Dim ev As New EventLogProvider()

            Dim res As String = String.Empty
            Dim theUser As UserInfo = Nothing
            '    If usn = "tHaNaSiS" AndAlso pass = "tH@n@S1s123!@#" Then
            Try
                theUser = UserInfoProvider.GetUserInfoByGUID(requesteduserGuid)
            Catch ex As Exception
                ev.LogEvent("E", "HellasPayCmsService - SetUserToRole - GetUserInfoByGUID", ex)
                res = "ERROR: " & ex.ToString()
            End Try

            If theUser IsNot Nothing Then
                Try
                    CMS.SiteProvider.UserInfoProvider.AddUserToRole(theUser.UserName, rolename, CMS.CMSHelper.CMSContext.CurrentSiteName)
                    Dim errMsg As String = String.Format("HellasPayCmsService - User {0} ({2}) was added to role {1} ", theUser.UserName, rolename, requesteduserGuid.ToString())

                    ev.LogEvent("I", errMsg, New Exception())
                Catch ex As Exception
                    ev.LogEvent("E", "HellasPayCmsService - SetUserToRole - AddUserToRole", ex)
                    res = ex.ToString()
                End Try
            Else

                Dim errMsg As String = "No user with such guid: " & requesteduserGuid.ToString()
                ev.LogEvent("E", "HellasPayCmsService - SetUserToRole - SetRole", New Exception(errMsg))
                res = "ERROR: " & errMsg
            End If
            '      Else
            '     res = "ERROR: Login Failed"
            '       End If
            Return res
        End Function



        Public Function RemoveUserFromRole(requesteduserGuid As Guid, rolename As String) As String

            Dim ev As New EventLogProvider()

            Dim res As String = String.Empty
            Dim theUser As UserInfo = Nothing
            '     If usn = "tHaNaSiS" AndAlso pass = "tH@n@S1s123!@#" Then
            Try
                theUser = UserInfoProvider.GetUserInfoByGUID(requesteduserGuid)
            Catch ex As Exception
                ev.LogEvent("E", "HellasPayCmsService - RemoveUserFromRole - GetUserInfoByGUID", ex)
                res = "ERROR: " & ex.ToString()
            End Try

            If theUser IsNot Nothing Then
                Try
                    CMS.SiteProvider.UserInfoProvider.RemoveUserFromRole(theUser.UserName, rolename, CMS.CMSHelper.CMSContext.CurrentSiteName)
                    Dim errMsg As String = String.Format("HellasPayCmsService - User {0} ({2}) was removed from role {1} ", theUser.UserName, rolename, requesteduserGuid.ToString())
                    ev.LogEvent("I", errMsg, New Exception())
                Catch ex As Exception
                    ev.LogEvent("E", "HellasPayCmsService - RemoveUserFromRole - RemoveUserFromRole", ex)
                    res = ex.ToString()
                End Try
            Else

                Dim errMsg As String = "No user with such guid: " & requesteduserGuid.ToString()
                ev.LogEvent("E", "HellasPayCmsService - RemoveUserFromRole ", New Exception(errMsg))
                res = "ERROR: " & errMsg
            End If
            '  Else
            '   res = "ERROR: Login Failed"
            '   End If
            Return res
        End Function




        Public ReadOnly Property MerchantId As Guid?
            Get
                Dim GuidSting As String = Me.Page.Request.QueryString("guid")

                Try
                    Dim MerchantIdGuid As Guid? = New Guid(GuidSting)
                    Return MerchantIdGuid
                Catch ex As Exception
                    Return Nothing
                End Try

            End Get
        End Property


        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            If Not Me.IsPostBack Then

                Dim API As New HellasPayInternalService.InternalAPIClient()
                Me.MerchantStatement.Text = API.GetMerchantById(Me.MerchantId).Statement

                Me.VTID.Text = ""
            End If
        End Sub





        Protected Sub Save_Click(sender As Object, e As EventArgs)
            Dim ApiClient = New HellasPayInternalService.InternalAPIClient()

            Dim Merchant = ApiClient.GetMerchantById(Me.MerchantId)

            Dim url = Me.Request.Url.AbsoluteUri.Split("?")(0).Replace("/euronetactivation.aspx", "")

            Dim templatePath = String.Format("~/MerchantRegistrationFlow/EuronetActivation_EmailTemplate_{0}.htm", Merchant.Person.Culture)
            templatePath = Server.MapPath(templatePath)

            Dim bodyText = System.IO.File.ReadAllText(templatePath)
            bodyText = bodyText.Replace("{%ClientName%}", Merchant.Person.LastName & " " & Merchant.Person.FirstName)

            Main.Visible = False

            Dim Success As Boolean = False

            If Merchant IsNot Nothing Then

                Merchant.EuronetTID1 = VTID.Text


                Dim StatusCrm = ApiClient.GetStatusCRM(Me.MerchantId)


                Dim cms As New HellasPayCmsService
                Dim emailOptions As New EmailOptions

                emailOptions.From = "info@hellaspay.gr"
                emailOptions.SendTo = "kkioussis@realize.gr" 'Merchant.Person.Email
                emailOptions.Bcc = "philosopher@realize.gr"
                emailOptions.Subject = "Hellas Pay: Oλοκλήρωση Ενεργοποίησης"
                emailOptions.Body = bodyText

                Dim MailSend As Boolean = cms.sendEmail(emailOptions)

                Me.Response.Write(MailSend.ToString + "<BR/>")

                StatusCrm.Status = "A"

                ApiClient.UpdateStatusCrm(StatusCrm)

                Merchant.Person.IsActive = 1
                Merchant.ePayUser = "HELLASPAYLIVELIVEHELLASPAYLIVE"
                Merchant.ePayPass = "kkn14n23127YX2xy23198249283ZxG"
                If String.IsNullOrEmpty(Merchant.Password) Then Merchant.Password = "123456"

                Dim s0 = RemoveUserFromRole(Merchant.MerchantId, "HellasPayMerchantWebPos")
                Dim s1 = RemoveUserFromRole(Merchant.MerchantId, "HellasPayMerchantCard")
                Dim s3 = SetUserToRole(Merchant.MerchantId, "HellasPayMerchantWebPos")
                Dim s4 = SetUserToRole(Merchant.MerchantId, "HellasPayMerchantCard")

                Success = ApiClient.UpdateMerchant(Merchant)
            End If

            If (Success) Then
                SuccessPanel.Visible = True
            Else
                FailedPanel.Visible = True
            End If

        End Sub
    End Class
End Namespace