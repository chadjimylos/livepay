CREATE PROCEDURE [Proc_COM_OptionCategory_RemoveDependencies]
	@CategoryID int
AS
BEGIN	
	DELETE FROM [COM_SKUOptionCategory] WHERE CategoryID = @CategoryID;	
END
