<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ContactIgnoreSearchBox.ascx.cs"
    Inherits="CMSModules_Messaging_Controls_ContactIgnoreSearchBox" %>
<asp:Panel ID="pnlFilter" runat="server" EnableViewState="false" CssClass="SearchList"
    Style="white-space: nowrap;">
    <asp:TextBox ID="txtSearch" runat="server" EnableViewState="false" CssClass="TextBoxField" />
</asp:Panel>
