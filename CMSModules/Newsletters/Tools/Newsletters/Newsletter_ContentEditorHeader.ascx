<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Newsletter_ContentEditorHeader.ascx.cs"
    Inherits="CMSModules_Newsletters_Tools_Newsletters_Newsletter_ContentEditorHeader" %>
<table width="99%" style="background-color: #E9F3FE; border-left: solid 1px #CCCCCC;
    border-right: solid 1px #CCCCCC; border-top: solid 1px #CCCCCC; margin-top: -1px;">
    <tr>
        <td>
            <div id="FCKEditorToolbar">
            </div>
        </td>
    </tr>
</table>

<script type="text/javascript">
    //<![CDATA[
    var toolbarElem = document.getElementById('FCKEditorToolbar');

    function ClearToolbar()
    {
        toolbarElem.innerHTML = '';
        toolbarElem.__FCKToolbarSet = null;
        toolbarElem.style.height = '';
    }
            
    function RememberFocusedRegion()
    {
        if ((window.frames['iframeContent'] != null) && (window.frames['iframeContent'].RememberFocusedRegion != null))
	    {  
	        window.frames['iframeContent'].RememberFocusedRegion();
	    }
    }
    //]]>
</script>

