using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSImportExport;
using CMS.SiteProvider;
using CMS.PortalEngine;

public partial class CMSModules_ImportExport_Controls_Import___objects__ : ImportExportControl
{
    /// <summary>
    /// Import settings
    /// </summary>
    public SiteImportSettings ImportSettings
    {
        get
        {
            if (this.Settings != null)
            {
                return (SiteImportSettings)this.Settings;
            }
            return null;
        }

        set
        {
        	 this.Settings = value; 
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.StopProcessing)
        {
            if ((ImportSettings != null) && CheckVersion() && (ImportSettings.IsIncluded(SiteObjectType.FORMUSERCONTROL, false) || ImportSettings.IsIncluded(SiteObjectType.INLINECONTROL, false) || ImportSettings.IsIncluded(PortalObjectType.WEBPART, false)))
            {
                plcOverwriteQueries.Visible = true;
                pnlWarning.Visible = true;
                lblWarning.Text = ResHelper.GetString("ImportObjects.WarningVersion");
            }

            this.lblInfo.Text = ResHelper.GetString("ImportObjects.Info");
            this.lblInfo2.Text = ResHelper.GetString("ImportObjects.Info2");
            this.lblSettings.Text = ResHelper.GetString("ImportObjects.Settings");
            this.lblSelection.Text = ResHelper.GetString("ImportObjects.Selection");

            this.lnkSelectAll.Text = ResHelper.GetString("ImportObjects.SelectAll");
            this.lnkSelectNone.Text = ResHelper.GetString("ImportObjects.SelectNone");
            this.lnkSelectNew.Text = ResHelper.GetString("ImportObjects.SelectNew");
            this.lnkSelectDefault.Text = ResHelper.GetString("ImportObjects.SelectDefault");

            this.chkCopyFiles.Text = ResHelper.GetString("ImportObjects.CopyFiles");
            this.chkOverwriteSystemQueries.Text = ResHelper.GetString("ImportObjects.OverwriteQueries");
            this.chkSkipOrfans.Text = ResHelper.GetString("ImportObjects.SkipOrfans");
            this.chkImportTasks.Text = ResHelper.GetString("ImportObjects.ImportTasks");
        }
    }


    /// <summary>
    /// Get settings
    /// </summary>
    public override void SaveSettings()
    {
        this.ImportSettings.SetSettings(ImportExportHelper.SETTINGS_DELETE_SITE, this.chkDeleteSite.Checked);
        this.ImportSettings.SetSettings(ImportExportHelper.SETTINGS_ADD_SITE_BINDINGS, this.chkBindings.Checked);
        this.ImportSettings.SetSettings(ImportExportHelper.SETTINGS_RUN_SITE, this.chkRunSite.Checked);
        this.ImportSettings.SetSettings(ImportExportHelper.SETTINGS_UPDATE_SITE_DEFINITION, this.chkUpdateSite.Checked);
        this.ImportSettings.SetSettings(ImportExportHelper.SETTINGS_OVERWRITE_SYSTEM_QUERIES, this.chkOverwriteSystemQueries.Checked);
        this.ImportSettings.SetSettings(ImportExportHelper.SETTINGS_SKIP_OBJECT_ON_TRANSLATION_ERROR, this.chkSkipOrfans.Checked);
        this.ImportSettings.SetSettings(ImportExportHelper.SETTINGS_TASKS, this.chkImportTasks.Checked);
        this.ImportSettings.CopyFiles = this.chkCopyFiles.Checked;
    }


    /// <summary>
    /// Reload data
    /// </summary>
    public override void ReloadData()
    {
        if (this.ImportSettings != null)
        {
            bool singleObject = ValidationHelper.GetBoolean(this.ImportSettings.GetInfo(ImportExportHelper.INFO_SINGLE_OBJECT), false);

            this.chkCopyFiles.Checked = this.ImportSettings.CopyFiles;
            this.chkBindings.Checked = ValidationHelper.GetBoolean(this.ImportSettings.GetSettings(ImportExportHelper.SETTINGS_ADD_SITE_BINDINGS), true);
            this.chkDeleteSite.Checked = ValidationHelper.GetBoolean(this.ImportSettings.GetSettings(ImportExportHelper.SETTINGS_DELETE_SITE), false);
            this.chkRunSite.Checked = ValidationHelper.GetBoolean(this.ImportSettings.GetSettings(ImportExportHelper.SETTINGS_RUN_SITE), !singleObject);
            this.chkUpdateSite.Checked = ValidationHelper.GetBoolean(this.ImportSettings.GetSettings(ImportExportHelper.SETTINGS_UPDATE_SITE_DEFINITION), !singleObject);
            this.chkOverwriteSystemQueries.Checked = ValidationHelper.GetBoolean(this.ImportSettings.GetSettings(ImportExportHelper.SETTINGS_OVERWRITE_SYSTEM_QUERIES), false);
            this.chkSkipOrfans.Checked = ValidationHelper.GetBoolean(this.ImportSettings.GetSettings(ImportExportHelper.SETTINGS_SKIP_OBJECT_ON_TRANSLATION_ERROR), false);
            this.chkImportTasks.Checked = ValidationHelper.GetBoolean(this.ImportSettings.GetSettings(ImportExportHelper.SETTINGS_TASKS), true);

            this.Visible = true;

            if (this.ImportSettings.TemporaryFilesCreated)
            {
                if (this.ImportSettings.SiteIsIncluded && !singleObject)
                {
                    this.plcSite.Visible = true;

                    if (this.ImportSettings.ExistingSite)
                    {
                        this.plcExistingSite.Visible = true;
                        this.chkUpdateSite.Text = ResHelper.GetString("ImportObjects.UpdateSite");
                    }
                    this.chkBindings.Text = ResHelper.GetString("ImportObjects.Bindings");
                    this.chkRunSite.Text = ResHelper.GetString("ImportObjects.RunSite");
                    this.chkDeleteSite.Text = ResHelper.GetString("ImportObjects.DeleteSite");
                }
                else
                {
                    this.plcSite.Visible = false;
                }
            }
        }
        else
        {
            this.Visible = false;
        }
    }


    protected void lnkSelectAll_Click(object sender, EventArgs e)
    {
        ImportTypeEnum importType = this.ImportSettings.ImportType;

        this.ImportSettings.ImportType = ImportTypeEnum.All;
        this.ImportSettings.LoadDefaultSelection(false);

        SaveSettings();

        this.ImportSettings.ImportType = importType;

        this.lblInfo.Text = ResHelper.GetString("ImportObjects.AllSelected");
    }


    protected void lnkSelectNone_Click(object sender, EventArgs e)
    {
        ImportTypeEnum importType = this.ImportSettings.ImportType;

        this.ImportSettings.ImportType = ImportTypeEnum.None;
        this.ImportSettings.LoadDefaultSelection(false);

        SaveSettings();

        this.ImportSettings.ImportType = importType;

        this.lblInfo.Text = ResHelper.GetString("ImportObjects.NoneSelected");
    }


    protected void lnkSelectDefault_Click(object sender, EventArgs e)
    {
        this.ImportSettings.LoadDefaultSelection(false);

        SaveSettings();

        this.lblInfo.Text = ResHelper.GetString("ImportObjects.DefaultSelected");
    }


    protected void lnkSelectNew_Click(object sender, EventArgs e)
    {
        ImportTypeEnum importType = this.ImportSettings.ImportType;

        this.ImportSettings.ImportType = ImportTypeEnum.New;
        this.ImportSettings.LoadDefaultSelection(false);

        SaveSettings();

        this.ImportSettings.ImportType = importType;

        this.lblInfo.Text = ResHelper.GetString("ImportObjects.NewSelected");
    }
}
