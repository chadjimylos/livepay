﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileSystemTree.ascx.cs"
    Inherits="CMSModules_Content_Controls_Dialogs_FileSystemSelector_FileSystemTree" %>
<asp:Label runat="server" ID="lblError"></asp:Label>
<asp:Panel runat="server" ID="pnlTree">
<asp:TreeView ID="treeFileSystem" runat="server" OnTreeNodePopulate="treeFileSystem_TreeNodePopulate"
    CssClass="ContentTree" Style="padding-top:5px;padding-left:5px">
</asp:TreeView>
</asp:Panel>
<asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
