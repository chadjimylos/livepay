<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BizForm_Edit_Autoresponder.aspx.cs"
    Inherits="CMSModules_BizForms_Tools_BizForm_Edit_Autoresponder" Theme="Default"
    ValidateRequest="false" EnableEventValidation="false" %>
<%@ Register src="~/CMSAdminControls/UI/PageElements/PageTitle.ascx" tagname="PageTitle" tagprefix="cms" %>

<%@ Register Src="~/CMSAdminControls/MetaFiles/FileList.ascx" TagName="FileList" TagPrefix="cms" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Bizform - Autoresponder</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            padding: 0px;
            height: 100%;
        }
    </style>
</head>
<body class="TabsBody <%=mBodyClass%>">
    <form id="form1" runat="server">
    <asp:Panel ID="pnlBody" runat="server" CssClass="TabsPageBody">
        <asp:Panel runat="server" ID="pnlScroll" CssClass="TabsPageScrollArea">
            <asp:Panel ID="pnlUsers" runat="server" CssClass="TabsPageContent">
                <asp:Panel runat="server" ID="pnlMenu" CssClass="PageHeaderLine">
                    <asp:LinkButton ID="lnkSave" runat="server" CssClass="ContentSaveLinkButton" OnClick="lnkSave_Click">
                        <asp:Image ID="imgSave" runat="server" CssClass="NewItemImage" />
                        <%=mSave%>
                    </asp:LinkButton>
                </asp:Panel>
                <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
                    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
                        Visible="false"></asp:Label>
                    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false"
                        Visible="false"></asp:Label>
                    <table cellpadding="0" border="0">
                        <tr>
                            <td style="padding-right: 5px;">
                                <asp:Label ID="lblEmailField" runat="server" EnableViewState="False" />
                            </td>
                            <td>
                                <asp:DropDownList ID="drpEmailField" runat="server" CssClass="DropDownField" AutoPostBack="true" />
                            </td>
                        </tr>
                        <asp:PlaceHolder ID="pnlCustomLayout" runat="server">
                            <tr>
                                <td style="padding-right: 5px;">
                                    <asp:Label ID="lblEmailFrom" runat="server" EnableViewState="False" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmailFrom" runat="server" MaxLength="200" CssClass="TextBoxField" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-right: 5px;">
                                    <cms:LocalizedLabel ID="lblEmailSubject" runat="server" EnableViewState="False" ResourceString="general.subject"
                                        DisplayColon="true" />
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmailSubject" runat="server" MaxLength="200" CssClass="TextBoxField" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <table>
                                        <tr>
                                            <td colspan="2" class="GenerateButtonPadding">
                                                <cms:CMSButton ID="btnGenerateLayout" runat="server" OnClientClick="SetContent(GenerateTableLayout()); return false;"
                                                    CssClass="XLongButton" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2" style="width: 550px; vertical-align: top;">
                                                <cms:CMSHtmlEditor ID="htmlEditor" runat="server" Width="550px" Height="300px" />
                                            </td>
                                            <td style="vertical-align: top; padding-left: 7px;" class="RightColumn">
                                                <asp:Label ID="lblAvailableFields" runat="server" EnableViewState="false" CssClass="AvailableFieldsTitle" />
                                                <asp:ListBox ID="lstAvailableFields" runat="server" CssClass="FieldsList" Width="152px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: bottom; padding-bottom: 3px;" class="RightColumn">
                                                <table cellspacing="0" cellpadding="1">
                                                    <tr>
                                                        <td>
                                                            <cms:CMSButton ID="btnInsertLabel" runat="server" CssClass="LongButton" OnClientClick="InsertAtCursorPosition('$$label:' + document.getElementById('lstAvailableFields').value + '$$'); return false;" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <cms:CMSButton ID="btnInsertInput" runat="server" CssClass="LongButton" OnClientClick="InsertAtCursorPosition('$$value:' + document.getElementById('lstAvailableFields').value + '$$'); return false;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                    </table>
                    <br />
                    <cms:PageTitle ID="AttachmentTitle" runat="server" Visible="false" TitleCssClass="SubTitleHeader" />
                    <br />
                    <cms:FileList ID="AttachmentList" runat="server" Visible="false" />
                    <asp:Literal ID="ltlConfirmDelete" runat="server" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    </form>

    <script type="text/javascript">
        //<![CDATA[
        // Insert desired HTML at the current cursor position of the FCK editor
        function InsertHTML(htmlString) {
            // Get the editor instance that we want to interact with.
            var oEditor = FCKeditorAPI.GetInstance("htmlEditor");

            // Check the active editing mode.
            if (oEditor.EditMode == FCK_EDITMODE_WYSIWYG) {
                // Insert the desired HTML.
                oEditor.InsertHtml(htmlString);
            }
            else
                alert('You must be on WYSIWYG mode!');
        }


        // Set content of the FCK editor - replace the actual one
        function SetContent(newContent) {
            // Get the editor instance that we want to interact with.
            var oEditor = FCKeditorAPI.GetInstance('htmlEditor');

            // Set the editor content (replace the actual one).
            oEditor.SetHTML(newContent);
        }

        function PasteImage(imageurl) {
            imageurl = '<img src="' + imageurl + '" />';
            return InsertHTML(imageurl);
        }

        // Returns HTML code with standard table layout
        function GenerateTableLayout() {
            var tableLayout = "";

            // indicates whether any row definition was added to the table
            var rowAdded = false;

            // list of attributes
            var list = document.getElementById("lstAvailableFields");

            // attributes count
            var optionsCount = list.options.length;

            for (var i = 0; i < optionsCount; i++) {
                tableLayout += "<tr><td>$$label:" + list.options[i].value + "$$</td><td>$$value:" + list.options[i].value + "$$</td></tr>";
                rowAdded = true;
            }

            if (rowAdded) {
                tableLayout = "<table><tbody>" + tableLayout + "</tbody></table>";
            }

            return tableLayout;
        }


        // Insert desired HTML at the current cursor position of the FCK editor if it is not already inserted 
        function InsertAtCursorPosition(htmlString) {
            InsertHTML(htmlString);
        }


        function ConfirmDelete() {
            return confirm(document.getElementById('confirmdelete').value);
        }
        //]]>
    </script>

</body>
</html>
