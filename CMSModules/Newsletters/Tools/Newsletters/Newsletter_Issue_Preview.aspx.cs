using System;

using CMS.GlobalHelper;
using CMS.Newsletter;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_Tools_Newsletters_Newsletter_Issue_Preview : CMSNewsletterPage
{
    protected int newsletterIssueId;

    protected void Page_Load(object sender, EventArgs e)
    {
        newsletterIssueId = ValidationHelper.GetInteger(Request.QueryString["issueid"], 0);
        if (newsletterIssueId == 0)
        {
            return;
        }
        Issue issue = IssueProvider.GetIssue(newsletterIssueId);
        bool isSent = (issue.IssueMailoutTime == DateTimeHelper.ZERO_TIME ? false : (issue.IssueMailoutTime < DateTime.Now));

        lblSent.Text = isSent
                           ? ResHelper.GetString("Newsletter_Issue_Header.AlreadySent")
                           : ResHelper.GetString("Newsletter_Issue_Header.NotSentYet");
    }
}

