<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MessageList.ascx.cs" Inherits="CMSModules_MessageBoards_Controls_Messages_MessageList" %>
<%@ Register Src="~/CMSAdminControls/UI/PageElements/HeaderActions.ascx" TagName="HeaderActions"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSModules/MessageBoards/FormControls/SelectBoard.ascx" TagName="BoardSelector"
    TagPrefix="cms" %>

<script type="text/javascript">
    //<![CDATA[       
    // Confirm mass delete
    function MassConfirm(dropdown, msg) {
        var drop = document.getElementById(dropdown);
        if (drop != null) {
            if (drop.value == "DELETE") {
                return confirm(msg);
            }
            return true;
        }
        return true;
    }
    //]]>
</script>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <asp:PlaceHolder ID="plcNewMessageGroups" runat="server" Visible="false">
            <div class="PageHeaderLine">
                <cms:HeaderActions ID="headerActions" runat="server"></cms:HeaderActions>
            </div>
        </asp:PlaceHolder>
        <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
        <table width="100%" class="BoardTable">
            <tr>
                <td>
                    <table>
                        <asp:PlaceHolder ID="pnlSite" runat="server" Visible="false">
                            <tr>
                                <td class="FieldLabel">
                                    <cms:LocalizedLabel ID="lblSiteName" runat="server" ResourceString="Board.MessageList.SiteName"
                                        EnableViewState="false" />
                                </td>
                                <td>
                                    <cms:SiteSelector ID="siteSelector" runat="server" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="plcBoard" runat="server">
                            <tr>
                                <td class="FieldLabel">
                                    <cms:LocalizedLabel ID="lblBoardName" runat="server" ResourceString="Board.MessageList.BoardName"
                                        EnableViewState="false" />
                                </td>
                                <td>
                                    <cms:BoardSelector ID="boardSelector" runat="server" AddAllItemsRecord="true" />
                                </td>
                            </tr>
                        </asp:PlaceHolder>
                        <tr>
                            <td class="FieldLabel">
                                <cms:LocalizedLabel ID="lblUserName" runat="server" AssociatedControlID="txtUserName"
                                    ResourceString="general.username" DisplayColon="true" EnableViewState="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtUserName" runat="server" CssClass="TextBoxField" EnableViewState="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <cms:LocalizedLabel ID="lblMessage" AssociatedControlID="txtMessage" runat="server"
                                    ResourceString="Board.MessageList.Message" EnableViewState="false" />
                            </td>
                            <td>
                                <asp:TextBox ID="txtMessage" runat="server" CssClass="TextBoxField" EnableViewState="false" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <cms:LocalizedLabel ID="lblApproved" AssociatedControlID="drpApproved" runat="server"
                                    ResourceString="Board.MessageList.Approved" EnableViewState="false" />
                            </td>
                            <td>
                                <asp:DropDownList ID="drpApproved" runat="server" CssClass="DropDownFieldSmall" />
                            </td>
                        </tr>
                        <tr>
                            <td class="FieldLabel">
                                <cms:LocalizedLabel ID="lblSpam" AssociatedControlID="drpSpam" runat="server" ResourceString="Board.MessageList.Spam"
                                    EnableViewState="false" />
                            </td>
                            <td>
                                <asp:DropDownList ID="drpSpam" runat="server" CssClass="DropDownFieldSmall" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <cms:CMSButton ID="btnFilter" runat="server" CssClass="ContentButton" EnableViewState="false" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <cms:UniGrid runat="server" ID="gridElem" GridName="~/CMSModules/MessageBoards/Tools/Messages/Message_List.xml"
                        OrderBy="MessageInserted DESC" Columns="MessageID, MessageUserName, MessageText, MessageApproved, MessageIsSpam, BoardDisplayName, MessageInserted" />
                </td>
            </tr>
            <tr>
                <td class="ItemsActions">
                    <cms:LocalizedLabel ID="lblActions" AssociatedControlID="drpActions" ResourceString="Board.MessageList.Actions"
                        runat="server" EnableViewState="false" />
                    <asp:DropDownList ID="drpActions" runat="server" CssClass="DropDownFieldSmall" />
                    <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOk_Clicked"
                        EnableViewState="false" />
                </td>
            </tr>
        </table>
        <cms:CMSButton ID="btnRefreshHdn" runat="server" Visible="false" OnCommand="btnRefreshHdn_Command" />
        <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
