CREATE TABLE [COM_ShoppingCart] (
		[ShoppingCartID]                    int NOT NULL IDENTITY(1, 1),
		[ShoppingCartGUID]                  uniqueidentifier NOT NULL,
		[ShoppingCartUserID]                int NULL,
		[ShoppingCartSiteID]                int NOT NULL,
		[ShoppingCartLastUpdate]            datetime NOT NULL,
		[ShoppingCartCurrencyID]            int NULL,
		[ShoppingCartPaymentOptionID]       int NULL,
		[ShoppingCartShippingOptionID]      int NULL,
		[ShoppingCartDiscountCouponID]      int NULL,
		[ShoppingCartBillingAddressID]      int NULL,
		[ShoppingCartShippingAddressID]     int NULL,
		[ShoppingCartCustomerID]            int NULL,
		[ShoppingCartNote]                  nvarchar(max) NULL,
		[ShoppingCartCompanyAddressID]      int NULL,
		[ShoppingCartCustomData]            nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [COM_ShoppingCart]
	ADD
	CONSTRAINT [PK_COM_ShoppingCart]
	PRIMARY KEY
	([ShoppingCartID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Address]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCart_ShoppingCartBillingAddressID_COM_Address]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCart]
			ADD CONSTRAINT [FK_COM_ShoppingCart_ShoppingCartBillingAddressID_COM_Address]
			FOREIGN KEY ([ShoppingCartBillingAddressID]) REFERENCES [COM_Address] ([AddressID])
END
IF OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Address]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCart_ShoppingCartCompanyAddressID_COM_Address]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCart]
			ADD CONSTRAINT [FK_COM_ShoppingCart_ShoppingCartCompanyAddressID_COM_Address]
			FOREIGN KEY ([ShoppingCartCompanyAddressID]) REFERENCES [COM_Address] ([AddressID])
END
IF OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Currency]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCart_ShoppingCartCurrencyID_COM_Currency]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCart]
			ADD CONSTRAINT [FK_COM_ShoppingCart_ShoppingCartCurrencyID_COM_Currency]
			FOREIGN KEY ([ShoppingCartCurrencyID]) REFERENCES [COM_Currency] ([CurrencyID])
END
IF OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Customer]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCart_ShoppingCartCustomerID_COM_Customer]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCart]
			ADD CONSTRAINT [FK_COM_ShoppingCart_ShoppingCartCustomerID_COM_Customer]
			FOREIGN KEY ([ShoppingCartCustomerID]) REFERENCES [COM_Customer] ([CustomerID])
END
IF OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[COM_DiscountCoupon]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCart_ShoppingCartDiscountCouponID_COM_DiscountCoupon]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCart]
			ADD CONSTRAINT [FK_COM_ShoppingCart_ShoppingCartDiscountCouponID_COM_DiscountCoupon]
			FOREIGN KEY ([ShoppingCartDiscountCouponID]) REFERENCES [COM_DiscountCoupon] ([DiscountCouponID])
END
IF OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[COM_PaymentOption]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCart_ShoppingCartPaymentOptionID_COM_PaymentOption]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCart]
			ADD CONSTRAINT [FK_COM_ShoppingCart_ShoppingCartPaymentOptionID_COM_PaymentOption]
			FOREIGN KEY ([ShoppingCartPaymentOptionID]) REFERENCES [COM_PaymentOption] ([PaymentOptionID])
END
IF OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[COM_Address]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCart_ShoppingCartShippingAddressID_COM_Address]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCart]
			ADD CONSTRAINT [FK_COM_ShoppingCart_ShoppingCartShippingAddressID_COM_Address]
			FOREIGN KEY ([ShoppingCartShippingAddressID]) REFERENCES [COM_Address] ([AddressID])
END
IF OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[COM_ShippingOption]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCart_ShoppingCartShippingOptionID_COM_ShippingOption]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCart]
			ADD CONSTRAINT [FK_COM_ShoppingCart_ShoppingCartShippingOptionID_COM_ShippingOption]
			FOREIGN KEY ([ShoppingCartShippingOptionID]) REFERENCES [COM_ShippingOption] ([ShippingOptionID])
END
IF OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCart_ShoppingCartSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCart]
			ADD CONSTRAINT [FK_COM_ShoppingCart_ShoppingCartSiteID_CMS_Site]
			FOREIGN KEY ([ShoppingCartSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[COM_ShoppingCart]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_ShoppingCart_ShoppingCartUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [COM_ShoppingCart]
			ADD CONSTRAINT [FK_COM_ShoppingCart_ShoppingCartUserID_CMS_User]
			FOREIGN KEY ([ShoppingCartUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_COM_ShoppingCart_ShoppingCartBillingAddressID]
	ON [COM_ShoppingCart] ([ShoppingCartBillingAddressID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_ShoppingCart_ShoppingCartCompanyAddressID]
	ON [COM_ShoppingCart] ([ShoppingCartCompanyAddressID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_ShoppingCart_ShoppingCartCurrencyID]
	ON [COM_ShoppingCart] ([ShoppingCartCurrencyID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_ShoppingCart_ShoppingCartCustomerID]
	ON [COM_ShoppingCart] ([ShoppingCartCustomerID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_ShoppingCart_ShoppingCartDiscountCouponID]
	ON [COM_ShoppingCart] ([ShoppingCartDiscountCouponID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_ShoppingCart_ShoppingCartLastUpdate]
	ON [COM_ShoppingCart] ([ShoppingCartLastUpdate])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_ShoppingCart_ShoppingCartPaymentOptionID]
	ON [COM_ShoppingCart] ([ShoppingCartPaymentOptionID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_ShoppingCart_ShoppingCartShippingAddressID]
	ON [COM_ShoppingCart] ([ShoppingCartShippingAddressID])
	ON [PRIMARY]
CREATE INDEX [IX_COM_ShoppingCart_ShoppingCartShippingOptionID]
	ON [COM_ShoppingCart] ([ShoppingCartShippingOptionID])
	ON [PRIMARY]
CREATE UNIQUE INDEX [IX_COM_ShoppingCart_ShoppingCartSiteID_ShoppingCartGUID]
	ON [COM_ShoppingCart] ([ShoppingCartSiteID], [ShoppingCartGUID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_COM_ShoppingCart_ShoppingCartUserID]
	ON [COM_ShoppingCart] ([ShoppingCartUserID])
	ON [PRIMARY]
