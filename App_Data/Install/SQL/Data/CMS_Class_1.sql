set identity_insert [cms_class] on
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'ACL', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="ACLID" fieldcaption="ACLID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="ed3bb1c3-2f77-4362-aaa2-32522e66c3f0" /><field column="ACLInheritedACLs" fieldcaption="ACLInheritedACLs" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="7db7efe1-669f-4181-98c5-6f137ac342fa" /><field column="ACLOwnerNodeID" fieldcaption="ACLOwnerNodeID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9da03250-37ab-48bd-bb1a-9864426beabc" /><field column="ACLGUID" fieldcaption="ACLGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="25d0fa3a-f1da-49f8-97c0-d2c67679c4f3" /><field column="ACLLastModified" fieldcaption="ACLLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1d55ce40-56bf-4319-94f2-c1d357df6047" /></form>', '20091110 13:48:14', 233, N'cms.acl', 0, 0, 0, '798885a1-331c-44aa-95c2-45ebb9a46a65', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_ACL', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_ACL">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ACLID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ACLInheritedACLs">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ACLOwnerNodeID" type="xs:int" />
              <xs:element name="ACLGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ACLLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_ACL" />
      <xs:field xpath="ACLID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Workflow history', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="WorkflowHistoryID" fieldcaption="WorkflowHistoryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="216f01a5-16fa-4a22-a0ff-e1eee5700bc4" /><field column="VersionHistoryID" fieldcaption="VersionHistoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ffc25364-a750-4ac1-a749-a7183e166e80" /><field column="StepID" fieldcaption="StepID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="da6cebcb-7dbe-45cf-8596-c175a68eb72e" /><field column="StepDisplayName" fieldcaption="StepDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9e9f09c3-97be-4f0e-88a6-aef997532d6e" /><field column="ApprovedByUserID" fieldcaption="ApprovedByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="51b7ad05-c0ae-42d1-ae89-b8f6bd3e6eb9" /><field column="ApprovedWhen" fieldcaption="ApprovedWhen" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e8bb8da1-4c12-4137-b98a-6ed43ca5da8a" /><field column="Comment" fieldcaption="Comment" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fd6fac2f-dcdd-4c7e-ad68-4255ede92001" /><field column="WasRejected" fieldcaption="WasRejected" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="366b4b7a-e22c-4d4c-9fb9-3df3b5efcb1b" /></form>', '20100303 10:24:07', 298, N'cms.workflowhistory', 0, 0, 0, 'accc303b-bfec-49fb-8d65-bef8984b7833', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_WorkflowHistory', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WorkflowHistory">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="WorkflowHistoryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="VersionHistoryID" type="xs:int" />
              <xs:element name="StepID" type="xs:int" minOccurs="0" />
              <xs:element name="StepDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ApprovedByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="ApprovedWhen" type="xs:dateTime" minOccurs="0" />
              <xs:element name="Comment" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WasRejected" type="xs:boolean" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WorkflowHistory" />
      <xs:field xpath="WorkflowHistoryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'PageTemplateCategory', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="CategoryID" fieldcaption="CategoryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="6049bf05-83a6-45c6-93e5-6981f476ec91" /><field column="CategoryDisplayName" fieldcaption="CategoryDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="eb6f8688-47b1-401a-b552-9cc53bb0b28f" /><field column="CategoryParentID" fieldcaption="CategoryParentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ced6a744-2400-473b-af23-d446df602f58" /><field column="CategoryName" fieldcaption="CategoryName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4cf24965-9b14-4f3f-86eb-316501e42f24" /><field column="CategoryGUID" fieldcaption="CategoryGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2356cad3-2cda-408b-a312-19432631758a" /><field column="CategoryLastModified" fieldcaption="CategoryLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4c42aad5-077d-496b-acb2-1a9b76bd2a15" /><field column="CategoryImagePath" visible="true" columntype="text" fieldtype="label" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="482243d1-3780-46c4-99cd-2dba52f201cf" visibility="none" ismacro="false" fieldcaption="CategoryImagePath" /><field column="CategoryChildCount" fieldcaption="CategoryChildCount" visible="true" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4f1a4640-8cc4-4b8a-824a-35e2cfe99703" visibility="none" ismacro="false" /><field column="CategoryTemplateChildCount" fieldcaption="CategoryTemplateChildCount" visible="true" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="358b657b-3aff-4580-9838-598a0f22fe9c" visibility="none" ismacro="false" /><field column="CategoryPath" fieldcaption="CategoryPath" visible="true" columntype="text" fieldtype="label" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="ba9b77a6-bfd8-4e14-93fb-8ddff52a098b" visibility="none" ismacro="false" /><field column="CategoryOrder" fieldcaption="CategoryOrder" visible="true" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3670fe8d-2a01-4879-9351-ed8a6cd16615" visibility="none" ismacro="false" /><field column="CategoryLevel" fieldcaption="CategoryLevel" visible="true" columntype="integer" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="085396ce-e649-40ae-8309-db9fa4632ae4" visibility="none" ismacro="false" /></form>', '20100120 10:24:12', 405, N'cms.pagetemplatecategory', 0, 0, 0, '92a1be18-bad3-4aac-91f0-064d30d452fd', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_PageTemplateCategory', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_PageTemplateCategory">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CategoryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="CategoryDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryParentID" type="xs:int" minOccurs="0" />
              <xs:element name="CategoryName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="CategoryLastModified" type="xs:dateTime" />
              <xs:element name="CategoryImagePath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryChildCount" type="xs:int" minOccurs="0" />
              <xs:element name="CategoryTemplateChildCount" type="xs:int" minOccurs="0" />
              <xs:element name="CategoryPath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryOrder" type="xs:int" minOccurs="0" />
              <xs:element name="CategoryLevel" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_PageTemplateCategory" />
      <xs:field xpath="CategoryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Layout', N'', N'', 1, N'', N'', N'', 0, N'', N'', N'<form><field column="LayoutID" fieldcaption="LayoutID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="ca2b14bd-12c8-47c4-8c2a-09a0a7bdc546" /><field column="LayoutCodeName" fieldcaption="LayoutCodeName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ee661fe6-8455-471e-8571-4aecad368b72" /><field column="LayoutDisplayName" fieldcaption="LayoutDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="09307aeb-2738-4b0e-b49e-5f76e48bb7a9" /><field column="LayoutDescription" fieldcaption="LayoutDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e316e3e3-2d28-46d2-87f0-da19bda6bdf5" /><field column="LayoutCode" fieldcaption="LayoutCode" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="10c4bb79-c4bd-44d6-a590-09f7b9bf9136" /><field column="LayoutCheckedOutFilename" fieldcaption="LayoutCheckedOutFilename" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ebd4efff-5d54-4753-8b21-e499061f462b" /><field column="LayoutCheckedOutByUserID" fieldcaption="LayoutCheckedOutByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="28099dd9-2a6c-4c96-94c7-33dcfd38ec29" /><field column="LayoutCheckedOutMachineName" fieldcaption="LayoutCheckedOutMachineName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6f9f0dc4-de0d-4b0d-8f7c-ec9c4df9f95d" /><field column="LayoutVersionGUID" fieldcaption="LayoutVersionGUID" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="69c017d1-a807-4f34-a55e-911a71b9fe4f" /><field column="LayoutGUID" fieldcaption="LayoutGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8458d6aa-1b12-491f-80f7-c8223ba9c1a7" /><field column="LayoutLastModified" fieldcaption="LayoutLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b2dc55fa-02c4-475f-9bf7-9e84558282a1" /></form>', '20091106 16:22:19', 410, N'cms.layout', 0, 0, 0, 'f0ba54c7-64ef-41d9-a4f4-0b792510414d', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Layout', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Layout">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="LayoutID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="LayoutCodeName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LayoutDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LayoutDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LayoutCode">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LayoutCheckedOutFilename" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LayoutCheckedOutByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="LayoutCheckedOutMachineName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LayoutVersionGUID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="LayoutGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="LayoutLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Layout" />
      <xs:field xpath="LayoutID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'WebPartCategory', N'', N'', 1, N'', N'', N'', 0, N'', N'', N'<form><field column="CategoryID" fieldcaption="CategoryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e79a9743-1548-4c03-a0be-bf4e59a59de0" /><field column="CategoryDisplayName" fieldcaption="CategoryDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="557c910c-3b7c-4ebc-9545-ef444d9c3755" /><field column="CategoryParentID" fieldcaption="CategoryParentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6bbbeeaa-b4bc-45f2-9fc8-293e5cf2da27" /><field column="CategoryName" fieldcaption="CategoryName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="fcb3e927-1a84-43db-8cc5-d838ad33d498" /><field column="CategoryGUID" fieldcaption="CategoryGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ea4e0ee4-0446-457a-be59-226738008cb7" /><field column="CategoryLastModified" fieldcaption="CategoryLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d1ee711f-e797-4042-a1e9-ba528b5f2ee7" /><field column="CategoryImagePath" fieldcaption="CategoryImagePath" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="ea9f0f5e-5496-4b19-85c3-f4dd5c4b57f1" /><field column="CategoryPath" fieldcaption="CategoryPath" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="5fbadd22-4c00-4bc8-9412-e6f1775406ef" /><field column="CategoryOrder" fieldcaption="CategoryOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a0a18eb8-cb03-4700-92dd-d3bc7afcd7ad" /><field column="CategoryLevel" fieldcaption="CategoryLevel" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4ddb3f47-6d98-49bc-b29f-61b71df562bc" /><field column="CategoryChildCount" fieldcaption="CategoryChildCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7bd82729-6151-450d-b9e8-f79c6907e603" /><field column="CategoryWebPartChildCount" fieldcaption="CategoryWebPartChildCount" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a4ae1551-c272-44b9-a63d-afc72d08730e" /></form>', '20100120 10:24:36', 424, N'cms.webpartcategory', 0, 0, 0, '920ce0d4-cb71-4a7b-bd25-af73d3323951', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_WebPartCategory', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WebPartCategory">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="CategoryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="CategoryDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryParentID" type="xs:int" minOccurs="0" />
              <xs:element name="CategoryName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="CategoryLastModified" type="xs:dateTime" />
              <xs:element name="CategoryImagePath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryPath">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="CategoryOrder" type="xs:int" minOccurs="0" />
              <xs:element name="CategoryLevel" type="xs:int" minOccurs="0" />
              <xs:element name="CategoryChildCount" type="xs:int" minOccurs="0" />
              <xs:element name="CategoryWebPartChildCount" type="xs:int" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WebPartCategory" />
      <xs:field xpath="CategoryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'WebPart', N'', N'', 1, N'', N'', N'', 0, N'', N'', N'<form><field column="WebPartID" fieldcaption="WebPartID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="d3a38f4f-3cab-4767-a9fb-dcd3295ad0e8" /><field column="WebPartName" fieldcaption="WebPartName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="1d4f4dfe-8290-4cf4-9ed1-2e39d97902ad" /><field column="WebPartDisplayName" fieldcaption="WebPartDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="79585e4d-5501-4cee-b37a-906c43f67fdb" /><field column="WebPartDescription" fieldcaption="WebPartDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="436ca21c-abcd-4175-b564-f554ff24349f" /><field column="WebPartFileName" fieldcaption="WebPartFileName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="db339e77-7066-4eb0-ac2a-fa9333dbb45c" /><field column="WebPartProperties" fieldcaption="WebPartProperties" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5ff2bc7b-8079-4b4d-ba30-7b2fa1aa5269" /><field column="WebPartCategoryID" fieldcaption="WebPartCategoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7f34b123-3e8d-4fa9-814d-5385d397efd8" /><field column="WebPartParentID" fieldcaption="WebPartParentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="84c73734-f65c-488a-a169-fd563cab6d40" /><field column="WebPartDocumentation" fieldcaption="WebPartDocumentation" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="43aa4a09-b2ea-4685-9edc-fb0ac2179df4" /><field column="WebPartGUID" fieldcaption="WebPartGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b55f8e33-b63c-4fe0-98ad-416d6d6b05e5" /><field column="WebPartLastModified" fieldcaption="WebPartLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c1ab9701-42aa-4cf1-aa79-70952c79c9a6" /><field column="WebPartType" fieldcaption="WebPartType" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5a01db9d-bc2e-4924-a8b9-48d5a0b88f08" /><field column="WebPartLoadGeneration" fieldcaption="WebPartLoadGeneration" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a650ce19-a7e6-4e79-8700-dc5405904902" /><field column="WebPartLastSelection" fieldcaption="WebPartLastSelection" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a881f02b-f419-43b9-addd-9e1bb636f31b" /></form>', '20090923 16:03:24', 425, N'cms.webpart', 0, 0, 0, '347a3154-07b4-4fd6-8b70-ce0391f76007', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_WebPart', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WebPart">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="WebPartID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="WebPartName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartFileName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartProperties">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartCategoryID" type="xs:int" />
              <xs:element name="WebPartParentID" type="xs:int" minOccurs="0" />
              <xs:element name="WebPartDocumentation" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebPartGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="WebPartLastModified" type="xs:dateTime" />
              <xs:element name="WebPartType" type="xs:int" minOccurs="0" />
              <xs:element name="WebPartLoadGeneration" type="xs:int" />
              <xs:element name="WebPartLastSelection" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WebPart" />
      <xs:field xpath="WebPartID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'UICulture', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="UICultureID" fieldcaption="UICultureID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="0e71eca6-a095-4fcb-838c-4bc9a10947f7" /><field column="UICultureName" fieldcaption="UICultureName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f845dfbb-a645-4f55-a68f-dc317f842802" /><field column="UICultureCode" fieldcaption="UICultureCode" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ef053470-1986-4676-9bf3-6b384d37d127" /><field column="UICultureGUID" fieldcaption="UICultureGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="597bcede-5b8e-43b5-b80a-125c049c671a" /><field column="UICultureLastModified" fieldcaption="UICultureLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3d7274f5-c67d-4819-8cf6-fb30c450c1f6" /></form>', '20081229 09:07:39', 490, N'cms.uiculture', 0, 0, 0, 'c5ac2fce-1d32-49b6-b36d-533610a50988', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_UICulture', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_UICulture">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="UICultureID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="UICultureName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UICultureCode">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UICultureGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="UICultureLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_UICulture" />
      <xs:field xpath="UICultureID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'WebTemplate', N'', N'', 1, N'', N'', N'', 0, N'', N'', N'<form><field column="WebTemplateID" fieldcaption="WebTemplateID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="bedd2cc2-8f3d-4693-9d70-7a79b1b559d4" /><field column="WebTemplateDisplayName" fieldcaption="WebTemplateDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a1640e39-619a-46d7-ba7c-84dfe8c6e544" /><field column="WebTemplateFileName" fieldcaption="WebTemplateFileName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="015f84fe-a23d-43c8-8f18-58a912bda714" /><field column="WebTemplateDescription" fieldcaption="WebTemplateDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c94c476d-f821-4d18-a625-44b1845b7f48" /><field column="WebTemplateGUID" fieldcaption="WebTemplateGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="8ce11f28-e4f7-4139-9ffc-6f069a8c4bc6" /><field column="WebTemplateLastModified" fieldcaption="WebTemplateLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="28416deb-c52c-477c-a27c-c4a29efe0811" /><field column="WebTemplateName" fieldcaption="WebTemplateName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ead12402-8ffc-413b-8d53-4ce290b7556a" /><field column="WebTemplateOrder" fieldcaption="WebTemplateOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c285f89e-b479-4bec-8bd8-f24ffdeb42e1" /><field column="WebTemplateLicenses" fieldcaption="WebTemplateLicenses" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b5634cf7-4ad7-4dde-b37c-df2975d0a04c" /></form>', '20091012 08:13:26', 500, N'cms.webtemplate', 0, 0, 0, '710b58bd-d39a-4f5c-93a8-42c8d886da2c', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_WebTemplate', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_WebTemplate">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="WebTemplateID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="WebTemplateDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebTemplateFileName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebTemplateDescription">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebTemplateGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="WebTemplateLastModified" type="xs:dateTime" />
              <xs:element name="WebTemplateName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="WebTemplateOrder" type="xs:int" />
              <xs:element name="WebTemplateLicenses">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_WebTemplate" />
      <xs:field xpath="WebTemplateID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Attachment', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="AttachmentID" fieldcaption="AttachmentID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="b985a545-89fe-4ff3-8dd2-337300735cc0" /><field column="AttachmentName" fieldcaption="AttachmentName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="255" publicfield="false" spellcheck="true" guid="2a47c2d0-d90b-4f7d-aff6-f372a9912c74" /><field column="AttachmentExtension" fieldcaption="AttachmentExtension" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="50" publicfield="false" spellcheck="true" guid="4c3bd725-76e9-445c-96cf-80f750bfefd8" /><field column="AttachmentSize" fieldcaption="AttachmentSize" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e1e4069a-1cce-4d77-bba8-bee75d823cd4" /><field column="AttachmentMimeType" fieldcaption="AttachmentMimeType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="656c281f-de55-4ae4-94ff-7fcdda523c6c" /><field column="AttachmentBinary" fieldcaption="AttachmentBinary" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fffa4cb8-cda7-4e8d-9f18-b713a6fda407" /><field column="AttachmentImageWidth" fieldcaption="AttachmentImageWidth" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="42f78c33-8aff-442c-8d86-be4150e2927e" /><field column="AttachmentImageHeight" fieldcaption="AttachmentImageHeight" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="52a66a03-154a-44e3-84ba-1328bc6329b0" /><field column="AttachmentDocumentID" fieldcaption="AttachmentDocumentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c05d18d4-cd4f-4855-998c-cec7d5a60fc6" /><field column="AttachmentGUID" fieldcaption="AttachmentGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7f35eb41-f171-41b8-83f9-9d6fe00cf407" /><field column="AttachmentLastHistoryID" fieldcaption="AttachmentLastHistoryID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="247ac0af-3dd1-4fae-b8b1-c43ff9589d6a" /><field column="AttachmentSiteID" fieldcaption="AttachmentSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3c5f0fd7-34c9-45d3-ba14-8b8e14b8f42a" /><field column="AttachmentLastModified" fieldcaption="AttachmentLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="91858b60-a074-4fe4-836c-36170849e389" /><field column="AttachmentIsUnsorted" fieldcaption="AttachmentIsUnsorted" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f07f10b2-bbab-46d0-ac1e-a5eb71e69579" /><field column="AttachmentOrder" fieldcaption="AttachmentOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="29d985ab-ad73-4a2e-b3cf-4d328a2921f0" /><field column="AttachmentGroupGUID" fieldcaption="AttachmentGroupGUID" visible="true" columntype="guid" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1d321bde-cea0-45cf-a4a3-0ef2c412b0b8" /><field column="AttachmentFormGUID" fieldcaption="AttachmentFormGUID" visible="true" columntype="guid" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b43b925b-a809-4697-9a18-a771af70e08c" /></form>', '20091012 09:40:00', 504, N'cms.attachment', 0, 0, 0, 'af2b10c3-0037-42e9-8b5a-e4e1a90b81e6', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Attachment', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Attachment">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="AttachmentID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="AttachmentName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="255" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentExtension">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentSize" type="xs:int" />
              <xs:element name="AttachmentMimeType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentBinary" type="xs:base64Binary" minOccurs="0" />
              <xs:element name="AttachmentImageWidth" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentImageHeight" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentDocumentID" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="AttachmentLastHistoryID" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentSiteID" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentLastModified" type="xs:dateTime" />
              <xs:element name="AttachmentIsUnsorted" type="xs:boolean" minOccurs="0" />
              <xs:element name="AttachmentOrder" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentGroupGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="AttachmentFormGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Attachment" />
      <xs:field xpath="AttachmentID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Attachment history', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="AttachmentHistoryID" fieldcaption="AttachmentHistoryID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="86c5ec11-c8d9-4563-9aa6-c34c7e309859" /><field column="AttachmentName" fieldcaption="AttachmentName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="255" publicfield="false" spellcheck="true" guid="632cc328-92ac-4f9b-8f8e-055fc684d60a" /><field column="AttachmentExtension" fieldcaption="AttachmentExtension" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="50" publicfield="false" spellcheck="true" guid="88804def-9871-4dd3-947e-36d91ed97682" /><field column="AttachmentSize" fieldcaption="AttachmentSize" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="00919c3c-e110-4eda-9669-85cb1e2c3a41" /><field column="AttachmentMimeType" fieldcaption="AttachmentMimeType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="100" publicfield="false" spellcheck="true" guid="38063b41-d910-4166-bc14-fdfe6d003c28" /><field column="AttachmentBinary" fieldcaption="AttachmentBinary" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="53774aec-ef12-4e40-93b2-dcb2b57cf8ce" /><field column="AttachmentImageWidth" fieldcaption="AttachmentImageWidth" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="1993f67a-0004-4365-ba55-320445401f91" /><field column="AttachmentImageHeight" fieldcaption="AttachmentImageHeight" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="dde1218e-27d0-4e6c-82c0-b79f7c9017be" /><field column="AttachmentDocumentID" fieldcaption="AttachmentDocumentID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c93b7183-23aa-4418-bf5e-2e064945c7ba" /><field column="AttachmentGUID" fieldcaption="AttachmentGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6da57418-94ce-4af9-a981-82fb58214236" /><field column="AttachmentIsUnsorted" fieldcaption="AttachmentIsUnsorted" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4fca9cf7-2b4a-48bc-90d7-13c93d20c3b7" /><field column="AttachmentOrder" fieldcaption="AttachmentOrder" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="02b696d5-7eaf-4f54-a96a-a22d4ecb4851" /><field column="AttachmentGroupGUID" fieldcaption="AttachmentGroupGUID" visible="true" columntype="guid" fieldtype="label" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a9ec6492-5ccb-44a8-be38-edb173e968f9" /></form>', '20091012 10:36:38', 516, N'cms.attachmenthistory', 0, 0, 0, '5d723ee7-8df3-4253-a0de-8cb8534a1e17', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_AttachmentHistory', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_AttachmentHistory">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="AttachmentHistoryID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="AttachmentName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="255" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentExtension">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentSize" type="xs:int" />
              <xs:element name="AttachmentMimeType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="AttachmentBinary" type="xs:base64Binary" minOccurs="0" />
              <xs:element name="AttachmentImageWidth" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentImageHeight" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentDocumentID" type="xs:int" />
              <xs:element name="AttachmentGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="AttachmentIsUnsorted" type="xs:boolean" minOccurs="0" />
              <xs:element name="AttachmentOrder" type="xs:int" minOccurs="0" />
              <xs:element name="AttachmentGroupGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_AttachmentHistory" />
      <xs:field xpath="AttachmentHistoryID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'RelationshipName', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="RelationshipNameID" fieldcaption="RelationshipNameID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="47839bd6-f19c-4cfd-b67f-1ca754694d46" /><field column="RelationshipDisplayName" fieldcaption="RelationshipDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6515b190-003a-44b6-b541-8814760de218" /><field column="RelationshipName" fieldcaption="RelationshipName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="42221f4a-30fa-47a6-bc80-3f99ee81f8a5" /><field column="RelationshipAllowedObjects" fieldcaption="RelationshipAllowedObjects" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="2a02c9d5-f0f9-4a19-be8d-9a007f4464ac" /><field column="RelationshipGUID" fieldcaption="RelationshipGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="03ad948a-2bb7-44b2-b580-b05abf3a2a8b" /><field column="RelationshipLastModified" fieldcaption="RelationshipLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ea7edf35-ed86-4cef-91c5-7bfdde27c389" /></form>', '20090819 18:55:56', 530, N'cms.relationshipname', 0, 0, 0, 'bcf36881-2644-4955-a591-7bc93157bf96', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_RelationshipName', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_RelationshipName">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="RelationshipNameID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="RelationshipDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="RelationshipName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="RelationshipAllowedObjects" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="RelationshipGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="RelationshipLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_RelationshipName" />
      <xs:field xpath="RelationshipNameID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'RelationshipNameSite', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="RelationshipNameID" fieldcaption="RelationshipNameID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e1f7d35d-e4b0-4df9-b63f-c855b306247c" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="556cd134-f2b3-4ff4-8bbe-d7a7dce76095" /></form>', '20081229 09:07:21', 536, N'CMS.RelationshipNameSite', 0, 0, 0, 'b9bc174e-8753-4d6d-bac1-f3ebb2b77ba8', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_RelationshipNameSite', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_RelationshipNameSite">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="RelationshipNameID" type="xs:int" />
              <xs:element name="SiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_RelationshipNameSite" />
      <xs:field xpath="RelationshipNameID" />
      <xs:field xpath="SiteID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Relationship', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="LeftNodeID" fieldcaption="LeftNodeID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="ace8cd3a-d954-4db1-8adb-895330e7f6b2" /><field column="RightNodeID" fieldcaption="RightNodeID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6ef2e0ec-a0bc-451e-ade1-1f1b17d10d79" /><field column="RelationshipNameID" fieldcaption="RelationshipNameID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f0ba6614-31ed-4544-8378-855c69df9e56" /><field column="RelationshipCustomData" fieldcaption="RelationshipCustomData" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f3f9e7ed-c3e0-4765-8c91-577db743b687" /></form>', '20090831 18:27:35', 537, N'CMS.Relationship', 0, 0, 0, '784809db-885c-4619-89fd-ef1c59066839', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Relationship', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Relationship">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="LeftNodeID" type="xs:int" />
              <xs:element name="RightNodeID" type="xs:int" />
              <xs:element name="RelationshipNameID" type="xs:int" />
              <xs:element name="RelationshipCustomData" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Relationship" />
      <xs:field xpath="LeftNodeID" />
      <xs:field xpath="RightNodeID" />
      <xs:field xpath="RelationshipNameID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'CSS stylesheet', N'', N'', 1, N'', N'', N'', 0, N'', N'', N'<form><field column="StylesheetID" fieldcaption="StylesheetID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e4954d9f-c7b4-4aed-900d-b4a41baad967" /><field column="StylesheetDisplayName" fieldcaption="StylesheetDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9f19d3a0-ff81-4fd7-818e-ca9c964e2c1d" /><field column="StylesheetName" fieldcaption="StylesheetName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f0a6eecf-5ba4-40de-8c0b-3a17dee2484f" /><field column="StylesheetText" fieldcaption="StylesheetText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7d418e04-4553-4ed3-9658-61f86e99abd7" /><field column="StylesheetCheckedOutByUserID" fieldcaption="StylesheetCheckedOutByUserID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="782d505d-1193-4850-99dd-f424669819fa" /><field column="StylesheetCheckedOutMachineName" fieldcaption="StylesheetCheckedOutMachineName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="18ee9d8c-942f-48fe-ab7a-e0472de39615" /><field column="StylesheetCheckedOutFileName" fieldcaption="StylesheetCheckedOutFileName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="eb22239d-2f29-41c3-b55a-c3ac8ac9212c" /><field column="StylesheetVersionGUID" fieldcaption="StylesheetVersionGUID" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a8b4a916-8414-4932-81bb-93200820f30b" /><field column="StylesheetGUID" fieldcaption="StylesheetGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="919f9210-e1c5-4722-a0b1-efffba6f4fcc" /><field column="StylesheetLastModified" fieldcaption="StylesheetLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f02b9848-d685-435e-b51b-9ad5896ea338" /></form>', '20090518 14:50:37', 555, N'cms.cssstylesheet', 0, 0, 0, 'e6b9c8bb-e87b-48ec-949e-9158f60ff91c', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_CssStyleSheet', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_CssStyleSheet">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="StylesheetID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="StylesheetDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StylesheetName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StylesheetText">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StylesheetCheckedOutByUserID" type="xs:int" minOccurs="0" />
              <xs:element name="StylesheetCheckedOutMachineName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StylesheetCheckedOutFileName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StylesheetVersionGUID" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="StylesheetGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" minOccurs="0" />
              <xs:element name="StylesheetLastModified" type="xs:dateTime" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_CssStyleSheet" />
      <xs:field xpath="StylesheetID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (0, N'', N'', NULL, N'Site Domain Alias', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="SiteDomainAliasID" fieldcaption="SiteDomainAliasID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="97dd2de6-e021-4be6-afc0-98bb257c2697" /><field column="SiteDomainAliasName" fieldcaption="SiteDomainAliasName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0128ef2c-a751-404e-a4ca-e635da780b90" /><field column="SiteID" fieldcaption="SiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6bcc14c5-26c8-4219-95fa-b17014252d3a" /><field column="SiteDefaultVisitorCulture" fieldcaption="SiteDefaultVisitorCulture" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a69982ab-2f9e-4c9d-a11a-caa1bee222aa" /><field column="SiteDomainGUID" fieldcaption="SiteDomainGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9f8ec7b9-bd18-4c2f-aa37-86cc2e848fa7" /><field column="SiteDomainLastModified" fieldcaption="SiteDomainLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9ddf6eaa-3970-4615-9000-e40513eaa8c3" /><field column="SiteDomainDefaultAliasPath" fieldcaption="SiteDomainDefaultAliasPath" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="8552c789-8c92-4639-8b27-22e5c07706ae" /><field column="SiteDomainRedirectUrl" fieldcaption="SiteDomainRedirectUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="f8cfa857-2f5c-465d-aa72-8d23841f0334" /></form>', '20091012 07:45:30', 661, N'cms.sitedomainalias', 0, 0, 0, 'a72f9763-8c28-4314-981e-6247776f8134', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_SiteDomainAlias', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_SiteDomainAlias">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SiteDomainAliasID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="SiteDomainAliasName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteID" type="xs:int" />
              <xs:element name="SiteDefaultVisitorCulture" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteDomainGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="SiteDomainLastModified" type="xs:dateTime" />
              <xs:element name="SiteDomainDefaultAliasPath" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="SiteDomainRedirectUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_SiteDomainAlias" />
      <xs:field xpath="SiteDomainAliasID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'FormUserControl', N'', N'', 1, N'', N'Null', N'', 0, N'', NULL, N'<form><field column="UserControlID" fieldcaption="UserControlID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="12a7dbac-256a-4620-8c83-ebef77554f29" /><field column="UserControlDisplayName" fieldcaption="UserControlDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="58a7d57f-08b3-4357-bf7c-1681268bf702" /><field column="UserControlCodeName" fieldcaption="UserControlCodeName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="200" publicfield="false" spellcheck="true" guid="c78acd92-a019-4830-8b20-56129d0d056f" /><field column="UserControlFileName" fieldcaption="UserControlFileName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="400" publicfield="false" spellcheck="true" guid="6928eca4-3189-4172-b8a5-53d28a4e6966" /><field column="UserControlForText" fieldcaption="UserControlForText" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="083e22af-aa8e-43ee-ba88-652dc8f24ba8" /><field column="UserControlForLongText" fieldcaption="UserControlForLongText" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ed4af8dc-0bc5-4628-a3ac-da94198f24d6" /><field column="UserControlForInteger" fieldcaption="UserControlForInteger" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a3b8e554-24f2-46fd-a547-6d31bf223066" /><field column="UserControlForDecimal" fieldcaption="UserControlForDecimal" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="58ce5d88-bd20-4c62-be7f-5174c98c20e3" /><field column="UserControlForDateTime" fieldcaption="UserControlForDateTime" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="0969400a-4930-4389-b6dd-3e70aa97ee91" /><field column="UserControlForBoolean" fieldcaption="UserControlForBoolean" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d60d1960-04a9-4857-8bdf-6fb82b425a8a" /><field column="UserControlForFile" fieldcaption="UserControlForFile" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="7b7c90e8-a739-4f82-baf9-6cb912f7fcb1" /><field column="UserControlShowInBizForms" fieldcaption="UserControlShowInBizForms" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="06591de6-0995-4cad-8931-2bb863c326f3" /><field column="UserControlDefaultDataType" fieldcaption="UserControlDefaultDataType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" columnsize="50" publicfield="false" spellcheck="true" guid="9ba27907-21f5-4b91-bef7-7bc775e5a17c" /><field column="UserControlDefaultDataTypeSize" fieldcaption="UserControlDefaultDataTypeSize" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="15fd80cc-e8e3-47ff-9892-cf8719ba1c2c" /><field column="UserControlShowInDocumentTypes" fieldcaption="UserControlShowInDocumentTypes" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="db50df09-ceff-498e-adf5-22f59ae02711" /><field column="UserControlShowInSystemTables" fieldcaption="UserControlShowInSystemTables" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="95dd83fb-ec56-41cb-908b-3ef96d173c98" /><field column="UserControlShowInWebParts" fieldcaption="UserControlShowInWebParts" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="597a5ea1-9775-4cb1-baa5-ce9c66d21d33" /><field column="UserControlShowInReports" fieldcaption="UserControlShowInReports" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="336640d5-2347-45f8-afcb-6b2a97d59dd0" /><field column="UserControlGUID" fieldcaption="UserControlGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="94342765-2117-4427-9fe9-ba8e89c657e4" /><field column="UserControlLastModified" fieldcaption="UserControlLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a74a847c-8357-434e-9096-7666c50bb870" /><field column="UserControlForGuid" fieldcaption="UserControlForGuid" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="5145d64a-c176-463a-89bb-19af77bb8e4d" /><field column="UserControlShowInCustomTables" fieldcaption="UserControlShowInCustomTables" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d3a2ed0b-5b90-48f2-8fe8-6e4aef81f626" /><field column="UserControlForVisibility" fieldcaption="UserControlForVisibility" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9e466fcd-eae2-45f9-bef2-f9ea11a18ea1" defaultvalue="false" /></form>', '20081229 09:07:07', 846, N'cms.formusercontrol', 0, 0, 0, 'ee83afe0-b6e8-442c-a643-13f670672466', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'cms_FormUserControl', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="cms_FormUserControl">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="UserControlID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="UserControlDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserControlCodeName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserControlFileName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserControlForText" type="xs:boolean" />
              <xs:element name="UserControlForLongText" type="xs:boolean" />
              <xs:element name="UserControlForInteger" type="xs:boolean" />
              <xs:element name="UserControlForDecimal" type="xs:boolean" />
              <xs:element name="UserControlForDateTime" type="xs:boolean" />
              <xs:element name="UserControlForBoolean" type="xs:boolean" />
              <xs:element name="UserControlForFile" type="xs:boolean" />
              <xs:element name="UserControlShowInBizForms" type="xs:boolean" />
              <xs:element name="UserControlDefaultDataType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="UserControlDefaultDataTypeSize" type="xs:int" minOccurs="0" />
              <xs:element name="UserControlShowInDocumentTypes" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserControlShowInSystemTables" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserControlShowInWebParts" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserControlShowInReports" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserControlGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="UserControlLastModified" type="xs:dateTime" />
              <xs:element name="UserControlForGuid" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserControlShowInCustomTables" type="xs:boolean" minOccurs="0" />
              <xs:element name="UserControlForVisibility" type="xs:boolean" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//cms_FormUserControl" />
      <xs:field xpath="UserControlID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'InlineControl', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="ControlID" fieldcaption="ControlID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="e85912e4-5a1a-4002-ad46-7ad5f2009a05" /><field column="ControlDisplayName" fieldcaption="ControlDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="fd811489-c34a-412e-868c-a5c07bf43734" /><field column="ControlName" fieldcaption="ControlName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="564e6a7f-b643-4061-b218-b1c43633b7c0" /><field column="ControlParameterName" fieldcaption="ControlParameterName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="02e9c06f-9187-44a6-9bab-72f9b9210b77" /><field column="ControlDescription" fieldcaption="ControlDescription" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b6697c07-7d62-4961-a809-d410953ab0f9" /><field column="ControlGUID" fieldcaption="ControlGUID" visible="true" columntype="guid" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9f3a30a4-7d73-411e-a758-a1a72a8ad018" /><field column="ControlLastModified" fieldcaption="ControlLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="db669571-0da4-4607-8e9e-9d2c7787e88b" /><field column="ControlFileName" fieldcaption="ControlFileName" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f0369391-048d-4717-8020-fc15e8fce2b3" /><field column="ControlProperties" fieldcaption="Control properties" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ead75d5b-0189-4836-8daa-6a6ea9dbf7d3" /></form>', '20081229 09:07:09', 877, N'cms.inlinecontrol', 0, 0, 0, 'e2be9ac2-a390-4619-99ae-b3ff811d11d1', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'CMS_InlineControl', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_InlineControl">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="ControlID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="ControlDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ControlName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ControlParameterName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="200" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ControlDescription" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ControlGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="ControlLastModified" type="xs:dateTime" />
              <xs:element name="ControlFileName" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="ControlProperties" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_InlineControl" />
      <xs:field xpath="ControlID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (1, N'', N'', NULL, N'Form', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="FormID" fieldcaption="FormID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="7e4045d0-83fe-48e2-8dff-17ba63371c98" /><field column="FormDisplayName" fieldcaption="FormDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a2f80fe3-4db8-4108-a1ba-72d87b300acd" /><field column="FormName" fieldcaption="FormName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9923899a-7913-4138-bc54-00b2d9d75826" /><field column="FormSendToEmail" fieldcaption="FormSendToEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e39d82b9-a055-4816-9a7d-ae091ad1b533" /><field column="FormSendFromEmail" fieldcaption="FormSendFromEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4233f725-e939-4a30-9f80-6f662b35c87a" /><field column="FormEmailSubject" fieldcaption="FormEmailSubject" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d5a09ea2-cd3f-43cd-9e16-34aae72e4693" /><field column="FormEmailTemplate" fieldcaption="FormEmailTemplate" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="70361f86-4cfa-4728-aa67-c130eb29cc53" /><field column="FormEmailAttachUploadedDocs" fieldcaption="FormEmailAttachUploadedDocs" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ee271037-5c60-4196-8220-0bf7fd4f0b24" /><field column="FormClassID" fieldcaption="FormClassID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4a6f6a49-d057-4c80-9b20-2b8e760c246f" /><field column="FormItems" fieldcaption="FormItems" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="72c682ed-264a-4bc2-9ce2-9aab8203dd61" /><field column="FormReportFields" fieldcaption="FormReportFields" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f20ee9b5-041b-44fe-a2f9-c9723a959201" /><field column="FormRedirectToUrl" fieldcaption="FormRedirectToUrl" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="15988158-84b2-4f52-8474-3b512d8c4e43" /><field column="FormDisplayText" fieldcaption="FormDisplayText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="97864ebf-e58c-4ccd-b1f7-c49801379509" /><field column="FormClearAfterSave" fieldcaption="FormClearAfterSave" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="414bfd02-1c2e-4a6c-80df-70158de74370" /><field column="FormSubmitButtonText" fieldcaption="FormSubmitButtonText" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="db2765b7-a1b6-464d-924b-7b7f7231d4a3" /><field column="FormSiteID" fieldcaption="FormSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="a695a92d-3d29-4aaf-b326-20db0876d681" /><field column="FormConfirmationEmailField" fieldcaption="FormConfirmationEmailField" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6b822565-28bf-4881-b75a-86565bd3f6a4" /><field column="FormConfirmationTemplate" fieldcaption="FormConfirmationTemplate" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b305f971-5cde-4f05-bc7e-f5f122432c89" /><field column="FormConfirmationSendFromEmail" fieldcaption="FormConfirmationSendFromEmail" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="38dfb147-f65e-4e82-b098-4b3cc09e7c7a" /><field column="FormConfirmationEmailSubject" fieldcaption="FormConfirmationEmailSubject" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="47e8f8d3-c8ba-4060-9d3b-ac44883febd8" /><field column="FormAccess" fieldcaption="FormAccess" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6655864e-d1c5-44e7-bdb9-d115fd50309b" /><field column="FormSubmitButtonImage" fieldcaption="FormSubmitButtonImage" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="839193cc-6353-4cd8-9b70-2315dce1f7de" /><field column="FormGUID" fieldcaption="FormGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="9e7c52e0-e181-4e28-a140-2c49aaf5640a" /><field column="FormLastModified" fieldcaption="FormLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f50a1f91-63ea-4a7c-a69a-8fb4574a93ba" /></form>', '20100308 17:05:55', 880, N'cms.form', 0, 0, 0, 'be2a91e3-b191-4a74-8888-a212b9a88c6d', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'CMS_Form', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="CMS_Form">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="FormID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="FormDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormSendToEmail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormSendFromEmail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormEmailSubject" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormEmailTemplate" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormEmailAttachUploadedDocs" type="xs:boolean" minOccurs="0" />
              <xs:element name="FormClassID" type="xs:int" />
              <xs:element name="FormItems" type="xs:int" />
              <xs:element name="FormReportFields" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormRedirectToUrl" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormDisplayText" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormClearAfterSave" type="xs:boolean" />
              <xs:element name="FormSubmitButtonText" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormSiteID" type="xs:int" />
              <xs:element name="FormConfirmationEmailField" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="100" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormConfirmationTemplate" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormConfirmationSendFromEmail" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="400" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormConfirmationEmailSubject" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormAccess" type="xs:int" minOccurs="0" />
              <xs:element name="FormSubmitButtonImage" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="255" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="FormGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="FormLastModified" type="xs:dateTime" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//CMS_Form" />
      <xs:field xpath="FormID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Newsletter - Email template', N'', N'', 0, N'', N'', N'', 0, N'', NULL, N'<form><field column="TemplateID" fieldcaption="TemplateID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="1fc8d3e4-5b6f-4a8b-9266-42cfdd739d2d" /><field column="TemplateDisplayName" fieldcaption="TemplateDisplayName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d3505546-d312-4dda-aacc-cf1dfcb7bab3" /><field column="TemplateName" fieldcaption="TemplateName" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d138a78c-5095-4778-a5a1-6c0e9d9c226e" /><field column="TemplateBody" fieldcaption="TemplateBody" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b06319a3-ed5a-4e28-b0d0-76e8db86ea6c" /><field column="TemplateSiteID" fieldcaption="TemplateSiteID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e8548e32-9ba1-4093-b00e-b6ab09d374e2" /><field column="TemplateHeader" fieldcaption="TemplateHeader" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="d5753a72-1424-4110-8557-1b54f68169c1" /><field column="TemplateFooter" fieldcaption="TemplateFooter" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="e8476b60-2e2f-48b6-87f6-5b6fe3ef426e" /><field column="TemplateType" fieldcaption="TemplateType" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="721c9b4a-60e2-4e9e-beef-8ca0e5612a89" /><field column="TemplateStylesheetText" fieldcaption="TemplateStylesheetText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="b9c74440-beca-4189-99a8-9bb431b3c20a" /><field column="TemplateGUID" fieldcaption="TemplateGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4bf16188-eaa3-4b2a-bc44-897706226bae" /><field column="TemplateLastModified" fieldcaption="TemplateLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="951b6ddb-4c52-4d94-a7dd-601f67b3658c" /><field column="TemplateSubject" fieldcaption="TemplateSubject" visible="true" columntype="text" fieldtype="textbox" allowempty="true" isPK="false" system="true" columnsize="450" publicfield="false" spellcheck="true" guid="0eaddd8f-5a59-4873-afb1-80a7156918a6" /></form>', '20081229 09:07:13', 905, N'newsletter.emailtemplate', 0, 0, 0, '5c6c85d4-63bc-4ca6-8b93-160d574716e4', N'', NULL, 0, NULL, N'', N'', NULL, 0, 0, N'Newsletter_EmailTemplate', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Newsletter_EmailTemplate">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="TemplateID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="TemplateDisplayName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateName">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="250" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateBody">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateSiteID" type="xs:int" />
              <xs:element name="TemplateHeader">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateFooter">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateType">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="50" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateStylesheetText" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="TemplateGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="TemplateLastModified" type="xs:dateTime" />
              <xs:element name="TemplateSubject" minOccurs="0">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Newsletter_EmailTemplate" />
      <xs:field xpath="TemplateID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, NULL, 1)
INSERT INTO [CMS_Class] ([ClassLoadGeneration], [ClassNodeAliasSource], [ClassSearchTitleColumn], [ClassSKUDefaultDepartmentID], [ClassDisplayName], [ClassSKUMappings], [ClassFormLayout], [ClassUsesVersioning], [ClassNodeNameSource], [ClassViewPageUrl], [ClassShowColumns], [ClassShowAsSystemTable], [ClassNewPageUrl], [ClassSearchImageColumn], [ClassFormDefinition], [ClassLastModified], [ClassID], [ClassName], [ClassIsMenuItemType], [ClassCreateSKU], [ClassShowTemplateSelection], [ClassGUID], [ClassListPageUrl], [ClassDefaultPageTemplateID], [ClassIsDocumentType], [ClassSearchCreationDateColumn], [ClassEditingPageUrl], [ClassPreviewPageUrl], [ClassSearchContentColumn], [ClassUsePublishFromTo], [ClassIsCustomTable], [ClassTableName], [ClassXmlSchema], [ClassIsCoupledClass], [ClassSearchSettings], [ClassIsProduct]) VALUES (2, N'', N'', NULL, N'Newsletter - Issue', N'', N'', 0, N'', N'', N'', 0, N'', N'', N'<form><field column="IssueID" fieldcaption="IssueID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="true" system="true" publicfield="false" spellcheck="true" guid="15b88ba7-8d58-48c4-9292-ea5f7784fb04" /><field column="IssueSubject" fieldcaption="IssueSubject" visible="true" columntype="text" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="00223c6b-e89b-4dcc-b489-5bbc48cd4a24" columnsize="450" visibility="none" /><field column="IssueText" fieldcaption="IssueText" visible="true" columntype="longtext" fieldtype="textarea" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="afc050d5-1b24-411f-a3dc-24453f493022" /><field column="IssueUnsubscribed" fieldcaption="IssueUnsubscribed" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="3f9a9455-3975-4ced-9400-13b2f8b0ef44" /><field column="IssueNewsletterID" fieldcaption="IssueNewsletterID" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="54d28cf3-3e2e-4b3b-a3f6-419cb217db55" /><field column="IssueTemplateID" fieldcaption="IssueTemplateID" visible="true" columntype="integer" fieldtype="textbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="f3101643-6599-4374-a7a6-c666834af21d" /><field column="IssueSentEmails" fieldcaption="IssueSentEmails" visible="true" columntype="integer" fieldtype="textbox" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="4e86e99f-0715-4f0e-b6eb-d69f2f84363e" /><field column="IssueMailoutTime" fieldcaption="IssueMailoutTime" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="6c176fdf-07c7-4dff-b21c-dfe85684160c" /><field column="IssueShowInNewsletterArchive" fieldcaption="IssueShowInNewsletterArchive" visible="true" columntype="boolean" fieldtype="checkbox" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="39607d2e-7a55-4331-a9e8-68b83d320a44" /><field column="IssueGUID" fieldcaption="IssueGUID" visible="true" columntype="file" fieldtype="uploadfile" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="ea71f981-a9e2-4019-a97d-e65dac96c9a1" /><field column="IssueLastModified" fieldcaption="IssueLastModified" visible="true" columntype="datetime" fieldtype="calendar" allowempty="true" isPK="false" system="true" publicfield="false" spellcheck="true" guid="cfdcc588-60b7-4909-9f2e-46f62e7d7615" /><field column="IssueSiteID" fieldcaption="IssueSiteID" visible="true" columntype="integer" fieldtype="label" allowempty="false" isPK="false" system="true" publicfield="false" spellcheck="true" guid="c3fd7ac6-621d-4e3d-b79d-4f493b053c21" visibility="none" /></form>', '20090910 14:21:30', 907, N'newsletter.issue', 0, 0, 0, '41f64b98-ae6d-4ba4-867a-0079ef839a1a', N'', NULL, 0, N'', N'', N'', N'', 0, 0, N'Newsletter_NewsletterIssue', N'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Newsletter_NewsletterIssue">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="IssueID" msdata:ReadOnly="true" msdata:AutoIncrement="true" type="xs:int" />
              <xs:element name="IssueSubject">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="450" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IssueText">
                <xs:simpleType>
                  <xs:restriction base="xs:string">
                    <xs:maxLength value="1073741823" />
                  </xs:restriction>
                </xs:simpleType>
              </xs:element>
              <xs:element name="IssueUnsubscribed" type="xs:int" />
              <xs:element name="IssueNewsletterID" type="xs:int" />
              <xs:element name="IssueTemplateID" type="xs:int" minOccurs="0" />
              <xs:element name="IssueSentEmails" type="xs:int" />
              <xs:element name="IssueMailoutTime" type="xs:dateTime" minOccurs="0" />
              <xs:element name="IssueShowInNewsletterArchive" type="xs:boolean" minOccurs="0" />
              <xs:element name="IssueGUID" msdata:DataType="System.Guid, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" type="xs:string" />
              <xs:element name="IssueLastModified" type="xs:dateTime" />
              <xs:element name="IssueSiteID" type="xs:int" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
    <xs:unique name="Constraint1" msdata:PrimaryKey="true">
      <xs:selector xpath=".//Newsletter_NewsletterIssue" />
      <xs:field xpath="IssueID" />
    </xs:unique>
  </xs:element>
</xs:schema>', 1, N'', 1)
set identity_insert [cms_class] off
