using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_ProductOptions_OptionCategory_Edit_General : CMSEcommercePage
{
    protected int categoryID = 0;

    private string SelectionType
    {
        get
        {
            return ValidationHelper.GetString(ViewState["SelectionType"], "");
        }
        set
        {
            ViewState["SelectionType"] = value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        // Field validator error messages initialization
        rfvCategoryName.ErrorMessage = ResHelper.GetString("general.requirescodename");
        rfvDisplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");

        // Control initializations
        lblCategoryName.Text = ResHelper.GetString("general.codename") + ResHelper.Colon;
        lblCategorySelectionType.Text = ResHelper.GetString("OptionCategory_Edit.CategorySelectionTypeLabel");
        lblCategoryDefaultOptions.Text = ResHelper.GetString("OptionCategory_Edit.CategoryDefaultOptionsLabel");
        lblCategoryDescription.Text = ResHelper.GetString("general.description") + ResHelper.Colon;
        lblDefaultRecord.Text = ResHelper.GetString("OptionCategory_Edit.CategoryDefaultRecord");
        lblNoOptions.Text = ResHelper.GetString("OptionCategory_Edit.NoProductOptions");
        btnOk.Text = ResHelper.GetString("General.OK");        
        
        string currentOptionCategory = ResHelper.GetString("OptionCategory_Edit.NewItemCaption");

        // Get option category information	
        categoryID = ValidationHelper.GetInteger(Request.QueryString["CategoryID"], 0);
        OptionCategoryInfo categObj = OptionCategoryInfoProvider.GetOptionCategoryInfo(categoryID);

        // Use default currency for price formatting
        productOptionSelector.UseDefaultCurrency = true;

        if (SelectionType != "")
        {
            if (categObj != null)
            {
                categObj.CategorySelectionType = GetOptionCategoryEnum(SelectionType);
                productOptionSelector.OptionCategoryDataRow = categObj.DataClass.DataRow;
            }
        }
        else
        {
            productOptionSelector.OptionCategoryId = categoryID;
        }

        if (categObj != null)
        {
            currentOptionCategory = categObj.CategoryName;

            // Fill editing form
            if (!RequestHelper.IsPostBack())
            {
                LoadData(categObj);

                // Show that the optionCategory was created or updated successfully
                if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                {
                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
            }
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        // Show information label when selection control is empty or contain only 'none'
        if (this.productOptionSelector.IsSelectionControlEmpty())
        {
            lblNoOptions.Visible = true;
        }
        else
        {
            lblNoOptions.Visible = false;
        }
        base.OnPreRender(e);
    }


    /// <summary>
    /// Load data of editing optionCategory.
    /// </summary>
    /// <param name="optionCategoryObj">OptionCategory object.</param>
    protected void LoadData(OptionCategoryInfo optionCategoryObj)
    {
        // Load data to controls
        txtDisplayName.Text = optionCategoryObj.CategoryDisplayName;
        txtCategoryName.Text = optionCategoryObj.CategoryName;

        drpCategorySelectionType.Items.Add(new ListItem(ResHelper.GetString("optioncategory_selectiontype.dropdownlist"), OptionCategorySelectionTypeEnum.Dropdownlist.ToString()));
        drpCategorySelectionType.Items.Add(new ListItem(ResHelper.GetString("optioncategory_selectiontype.radiobuttonsvertical"), OptionCategorySelectionTypeEnum.RadioButtonsVertical.ToString()));
        drpCategorySelectionType.Items.Add(new ListItem(ResHelper.GetString("optioncategory_selectiontype.radiobuttonshorizontal"), OptionCategorySelectionTypeEnum.RadioButtonsHorizontal.ToString()));
        drpCategorySelectionType.Items.Add(new ListItem(ResHelper.GetString("optioncategory_selectiontype.checkboxesvertical"), OptionCategorySelectionTypeEnum.CheckBoxesVertical.ToString()));
        drpCategorySelectionType.Items.Add(new ListItem(ResHelper.GetString("optioncategory_selectiontype.checkboxeshorizontal"), OptionCategorySelectionTypeEnum.CheckBoxesHorizontal.ToString()));
        drpCategorySelectionType.SelectedValue = optionCategoryObj.CategorySelectionType.ToString();

        this.chkCategoryDisplayPrice.Checked = optionCategoryObj.CategoryDisplayPrice;

        txtCategoryDecription.Text = optionCategoryObj.CategoryDescription;
        txtDefaultRecord.Text = optionCategoryObj.CategoryDefaultRecord;
        chkCategoryEnabled.Checked = optionCategoryObj.CategoryEnabled;
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        // Check input value from textboxs
        string errorMessage = new Validator().NotEmpty(txtDisplayName.Text, ResHelper.GetString("general.requiresdisplayname"))
            .NotEmpty(txtCategoryName.Text, ResHelper.GetString("general.requirescodename"))
            .IsIdentificator(txtCategoryName.Text, ResHelper.GetString("optioncategory_new.errorNotIdentificator")).Result;

        if (errorMessage == "")
        {
            // Category code name must to be unique
            OptionCategoryInfo optionCategoryObj = OptionCategoryInfoProvider.GetOptionCategoryInfo(txtCategoryName.Text.Trim());

            // If category code name value is unique														
            if ((optionCategoryObj == null) || (optionCategoryObj.CategoryID == categoryID))
            {
                // If optionCategory doesn't already exist, create new one
                if (optionCategoryObj == null)
                {
                    optionCategoryObj = OptionCategoryInfoProvider.GetOptionCategoryInfo(categoryID);
                    if (optionCategoryObj == null)
                    {
                        optionCategoryObj = new OptionCategoryInfo();
                    }
                }

                // Fill optionCategoryObj
                optionCategoryObj.CategoryID = categoryID;
                optionCategoryObj.CategoryDisplayName = txtDisplayName.Text.Trim();
                optionCategoryObj.CategoryName = txtCategoryName.Text.Trim();
                optionCategoryObj.CategorySelectionType = GetOptionCategoryEnum(drpCategorySelectionType.SelectedValue);
                optionCategoryObj.CategoryDefaultOptions = productOptionSelector.GetSelectedSKUOptions();
                optionCategoryObj.CategoryDescription = txtCategoryDecription.Text.Trim();
                optionCategoryObj.CategoryDefaultRecord = txtDefaultRecord.Text.Trim();
                optionCategoryObj.CategoryEnabled = chkCategoryEnabled.Checked;
                optionCategoryObj.CategoryDisplayPrice = chkCategoryDisplayPrice.Checked;

                // Save changes
                OptionCategoryInfoProvider.SetOptionCategoryInfo(optionCategoryObj);

                // If hidebreadcrumbs (is in modal window)
                if (ValidationHelper.GetBoolean(Request.QueryString["hidebreadcrumbs"], false))
                {
                    // Close window and refresh opener
                    ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "CloseAndRefresh", ScriptHelper.GetScript("parent.wopener.Refresh(); parent.window.close();"));
                }
                else
                {
                    // Normal save
                    UrlHelper.Redirect("OptionCategory_Edit_General.aspx?CategoryID=" + Convert.ToString(optionCategoryObj.CategoryID) + "&saved=1");
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("optioncategory_new.errorExistingCodeName");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }


    /// <summary>
    /// drpCategorySelectionType SelectedIndexChanged event handler.
    /// </summary>
    protected void drpCategorySelectionType_SelectedIndexChanged(object sender, EventArgs e)
    {
        OptionCategoryInfo categObj = OptionCategoryInfoProvider.GetOptionCategoryInfo(categoryID);
        if (categObj != null)
        {
            // Clone option category data
            OptionCategoryInfo tempCategObj = new OptionCategoryInfo(categObj, false);

            // Set new selection type from selection control
            tempCategObj.CategorySelectionType = GetOptionCategoryEnum(drpCategorySelectionType.SelectedValue);
            // Set previously selected options
            tempCategObj.CategoryDefaultOptions = productOptionSelector.GetSelectedSKUOptions();
            // Set display price option
            tempCategObj.CategoryDisplayPrice = this.chkCategoryDisplayPrice.Checked;

            // Remember selection type in viewstate
            this.SelectionType = drpCategorySelectionType.SelectedValue;

            // Reload selector
            productOptionSelector.OptionCategoryDataRow = tempCategObj.DataClass.DataRow;
            productOptionSelector.ReloadSelector();
        }
    }


    /// <summary>
    /// Handle display price selection made
    /// </summary>
    protected void chkCategoryDisplayPrice_CheckedChanged(object sender, EventArgs e)
    {
        OptionCategoryInfo categObj = OptionCategoryInfoProvider.GetOptionCategoryInfo(categoryID);
        if (categObj != null)
        {
            // Clone option category data
            OptionCategoryInfo tempCategObj = new OptionCategoryInfo(categObj, false);

            // Set new selection type from selection control
            tempCategObj.CategorySelectionType = GetOptionCategoryEnum(drpCategorySelectionType.SelectedValue);
            // Set previously selected options
            tempCategObj.CategoryDefaultOptions = productOptionSelector.GetSelectedSKUOptions();
            // Set display price option
            tempCategObj.CategoryDisplayPrice = this.chkCategoryDisplayPrice.Checked;

            // Remember selection type in viewstate
            this.SelectionType = drpCategorySelectionType.SelectedValue;

            // Reload selector
            productOptionSelector.OptionCategoryDataRow = tempCategObj.DataClass.DataRow;
            productOptionSelector.ReloadSelector();
        }
    }


    /// <summary>
    /// Returns option category enumeration type according to the string value.
    /// </summary>
    /// <param name="value">Option category string identifier.</param>
    private OptionCategorySelectionTypeEnum GetOptionCategoryEnum(string value)
    {
        if (value == OptionCategorySelectionTypeEnum.CheckBoxesHorizontal.ToString())
        {
            return OptionCategorySelectionTypeEnum.CheckBoxesHorizontal;
        }
        else if (value == OptionCategorySelectionTypeEnum.CheckBoxesVertical.ToString())
        {
            return OptionCategorySelectionTypeEnum.CheckBoxesVertical;
        }
        else if (value == OptionCategorySelectionTypeEnum.RadioButtonsHorizontal.ToString())
        {
            return OptionCategorySelectionTypeEnum.RadioButtonsHorizontal;
        }
        else if (value == OptionCategorySelectionTypeEnum.RadioButtonsVertical.ToString())
        {
            return OptionCategorySelectionTypeEnum.RadioButtonsVertical;
        }
        else
        {
            return OptionCategorySelectionTypeEnum.Dropdownlist;
        }
    }
}
