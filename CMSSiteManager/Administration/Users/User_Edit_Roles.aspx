<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User_Edit_Roles.aspx.cs"
    Inherits="CMSSiteManager_Administration_Users_User_Edit_Roles" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="User Edit - Roles" %>

<%@ Register Src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" TagName="UniSelector"
    TagPrefix="cms" %>
<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblErrorDeskAdmin" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:PlaceHolder ID="plcTable" runat="server"><strong>
        <cms:LocalizedLabel runat="server" ID="lblRolesInfo" DisplayColon="true" ResourceString="edituserroles.userroles"
            EnableViewState="false" CssClass="InfoLabel" />
    </strong>
        <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
            Visible="false" />
        <asp:PlaceHolder ID="plcSelectSite" runat="server">
            <cms:LocalizedLabel ID="LabelSelectSite" runat="server" ResourceString="Administration-User_Edit_Sites.SelectSite" />&nbsp;
            <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" />
            <br />
            <br />
        </asp:PlaceHolder>
        <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label runat="server" ID="lblError" Visible="false" CssClass="ErrorLabel" EnableViewState="false" />
                <cms:UniSelector ID="usRoles" runat="server" IsLiveSite="false" ObjectType="cms.role"
                    SelectionMode="Multiple" ResourcePrefix="addroles" />
            </ContentTemplate>
        </cms:CMSUpdatePanel>
    </asp:PlaceHolder>
</asp:Content>
