using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Net.Mail;
using System.IO;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.EmailEngine;
using CMS.DirectoryUtilities;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.DataEngine;
using CMS.UIControls;
using CMS.Controls;

public partial class CMSSiteManager_Administration_System_Debug_System_DebugViewState : CMSDebugPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.btnClear.Text = ResHelper.GetString("DebugViewState.ClearLog");

        ReloadData();
    }


    protected void ReloadData()
    {
        if (!CMSControlsHelper.DebugViewState)
        {
            this.lblInfo.Text = ResHelper.GetString("DebugViewState.NotConfigured");
        }
        else
        {
            this.plcLogs.Controls.Clear();

            for (int i = CMSControlsHelper.LastLogs.Count - 1; i >= 0; i--)
            {
                try
                {
                    // Get the log
                    RequestLog log = (RequestLog)CMSControlsHelper.LastLogs[i];
                    if (log != null)
                    {
                        // Load the table
                        DataTable dt = log.LogTable;
                        if (!DataHelper.DataSourceIsEmpty(dt))
                        {
                            // Load the control
                            ViewStateLog logCtrl = (ViewStateLog)LoadControl("~/CMSAdminControls/Debug/ViewState.ascx");
                            logCtrl.ID = "viewStateLog";
                            logCtrl.EnableViewState = false;
                            logCtrl.DisplayTotalSize = false;
                            logCtrl.Log = log;
                            logCtrl.LogStyle = "";

                            // Add to the output
                            this.plcLogs.Controls.Add(new LiteralControl("<div><strong>&nbsp;" + GetRequestLink(log.RequestURL, log.RequestGUID) + "</strong> (" + log.RequestTime.ToString("hh:mm:ss")  + ")<br /><br />"));
                            this.plcLogs.Controls.Add(logCtrl);
                            this.plcLogs.Controls.Add(new LiteralControl("</div><br /><br />"));
                        }
                    }
                }
                catch
                {
                }
            }
        }
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        CMSControlsHelper.LastLogs.Clear();
        ReloadData();
    }
}
