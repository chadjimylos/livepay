using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.SiteProvider;
using CMS.GlobalHelper;
using CMS.ResourceManager;
using CMS.UIControls;

public partial class CMSSiteManager_Development_UICultures_UICulture_Strings_List : SiteManagerPage
{
    int uiCultureID = 0;
    UICultureInfo ui = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        uiCultureID = QueryHelper.GetInteger("UIcultureID", 0);

        UICultureInfo dui = null;

        // Ty to get default UI culture
        try
        {
            dui = UICultureInfoProvider.GetUICultureInfo(CultureHelper.DefaultUICulture);
        }
        catch
        {
        }

        // Get requested culture
        ui = UICultureInfoProvider.GetUICultureInfo(uiCultureID);

        object[,] parameters = new object[2, 3];
        parameters[0, 0] = "@Culture";
        parameters[0, 1] = ui.UICultureID;
        parameters[1, 0] = "@DefaultUICultureID";
        if (dui != null)
        {
            parameters[1, 1] = dui.UICultureID;
        }
        else
        {
            parameters[1, 1] = 0;
        }

        // Setup the grid
        gridStrings.QueryParameters = parameters;
        gridStrings.OnAction += new OnActionEventHandler(UniGridUICultures_OnAction);
        gridStrings.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGridStrings_OnExternalDataBound);
        gridStrings.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        InitializeMasterPage();
    }


    protected override void Render(HtmlTextWriter writer)
    {
        if (gridStrings.GridView.HeaderRow != null)
        {
            gridStrings.GridView.HeaderRow.Cells[2].Text = String.Format(ResHelper.GetString("unigrid.uiculture_strings_list.columns.english"), CultureHelper.DefaultUICulture);
        }

        base.Render(writer);
    }


    protected void InitializeMasterPage()
    {
        // Set actions
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("Development-UICulture_Strings_List.NewString");
        actions[0, 3] = ResolveUrl("UICulture_String_New.aspx?UIcultureID=" + uiCultureID);
        actions[0, 5] = GetImageUrl("CMSModules/CMS_UICultures/addstring.png");

        CurrentMaster.HeaderActions.Actions = actions;
    }


    object UniGridStrings_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        if (sourceName.ToLower() == "edititem")
        {
            ImageButton ib = sender as ImageButton;
            if (ib != null)
            {
                GridViewRow gvr = parameter as GridViewRow;
                if (gvr != null)
                {
                    DataView dv = gvr.DataItem as DataView;
                    if (dv != null)
                    {
                        if (ui != null)
                        {
                            ResourceStringInfo ri = SqlResourceManager.GetResourceStringInfo(ValidationHelper.GetString(dv[0], ""), ui.UICultureCode);
                            if (ri != null)
                            {
                                ib.OnClientClick = "location.href='UICulture_Strings_Edit.aspx?stringID=" + ri.StringId + "&uicultureID=" + ui.UICultureID + "'; return false;";
                            }
                        }
                    }
                }
            }
        }

        return parameter;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that threw event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGridUICultures_OnAction(string actionName, object actionArgument)
    {
        ResourceStringInfo ri = SqlResourceManager.GetResourceStringInfo(actionArgument.ToString(), ui.UICultureCode);

        switch (actionName)
        {
            case "edit":
                UrlHelper.Redirect("UICulture_Strings_Edit.aspx?stringID=" + ri.StringId + "&uicultureID=" + ui.UICultureID);
                break;

            case "delete":
                SqlResourceManager.DeleteResourceStringInfo(actionArgument.ToString(), ui.UICultureCode);
                break;
        }
    }
}
