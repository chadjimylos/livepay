<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" AutoEventWireup="true"
    CodeFile="TagGroup_Edit_General.aspx.cs" Inherits="CMSSiteManager_Development_TagGroups_TagGroup_Edit_General"
    Title="Tag group - Properties" Theme="Default" %>

<%@ Register Src="~/CMSSiteManager/Development/TagGroups/TagEdit.ascx" TagName="TagGroupEdit"
    TagPrefix="cms" %>
<asp:Content ID="Content1" ContentPlaceHolderID="plcContent" runat="Server">
    <cms:TagGroupEdit ID="groupEdit" runat="server" IsEdit="true" />
</asp:Content>
