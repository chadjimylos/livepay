CREATE TABLE [Newsletter_SubscriberNewsletter] (
		[SubscriberID]       int NOT NULL,
		[NewsletterID]       int NOT NULL,
		[SubscribedWhen]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Newsletter_SubscriberNewsletter]
	ADD
	CONSTRAINT [PK_Newsletter_SubscriberNewsletter]
	PRIMARY KEY
	([SubscriberID], [NewsletterID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Newsletter_SubscriberNewsletter]') IS NOT NULL
	AND OBJECT_ID(N'[Newsletter_Newsletter]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_SubscriberNewsletter_NewsletterID_Newsletter_Newsletter]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_SubscriberNewsletter]
			ADD CONSTRAINT [FK_Newsletter_SubscriberNewsletter_NewsletterID_Newsletter_Newsletter]
			FOREIGN KEY ([NewsletterID]) REFERENCES [Newsletter_Newsletter] ([NewsletterID])
END
IF OBJECT_ID(N'[Newsletter_SubscriberNewsletter]') IS NOT NULL
	AND OBJECT_ID(N'[Newsletter_Subscriber]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Newsletter_SubscriberNewsletter_SubscriberID_Newsletter_Subscriber]') IS NULL
BEGIN
		ALTER TABLE [Newsletter_SubscriberNewsletter]
			ADD CONSTRAINT [FK_Newsletter_SubscriberNewsletter_SubscriberID_Newsletter_Subscriber]
			FOREIGN KEY ([SubscriberID]) REFERENCES [Newsletter_Subscriber] ([SubscriberID])
END
