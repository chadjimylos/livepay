<%@ Page Language="C#" AutoEventWireup="true" CodeFile="System.aspx.cs" Inherits="CMSSiteManager_Administration_System_System"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Site Manager - System" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent" EnableViewState="false">

    <script type="text/javascript">
        //<![CDATA[
        function alert() {
        }
        //]]>
    </script>

    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false" />
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />
    <asp:ScriptManager runat="server" ID="manScript" ScriptMode="Release" />
    <cms:CMSUpdatePanel runat="server" ID="pnlUpdate" CatchErrors="true">
        <ContentTemplate>
            <asp:Timer ID="timRefresh" runat="server" Interval="1000" />
            <table width="100%">
                <tr>
                    <td style="width: 40%; vertical-align: top">
                        <table width="400px">
                            <tr>
                                <td style="white-space: nowrap; width: 200px;">
                                    <asp:Label ID="lblMachineName" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblMachineNameValue" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAspAccount" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblValueAspAccount" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblIP" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblIPValue" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width: 40%; vertical-align: top">
                        <table width="400px">
                            <tr>
                                <td colspan="2">
                                    <strong>
                                        <asp:Label ID="lblMemory" runat="server" EnableViewState="false" /></strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap; width: 200px;">
                                    <asp:Label ID="lblAlocatedMemory" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblValueAlocatedMemory" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <asp:PlaceHolder runat="server" ID="plcAdvanced">
                                <tr>
                                    <td style="white-space: nowrap;">
                                        <asp:Label ID="lblPeakMemory" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblValuePeakMemory" runat="server" EnableViewState="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="white-space: nowrap;">
                                        <asp:Label ID="lblPhysicalMemory" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblValuePhysicalMemory" runat="server" EnableViewState="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="white-space: nowrap;">
                                        <asp:Label ID="lblVirtualMemory" runat="server" EnableViewState="false" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblValueVirtualMemory" runat="server" EnableViewState="false" />
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                        </table>
                    </td>
                    <td style="vertical-align: top;">
                        <table>
                            <tr>
                                <td colspan="2">
                                    <strong>
                                        <asp:Label ID="lblGC" runat="server" EnableViewState="false" /></strong>
                                </td>
                            </tr>
                            <asp:PlaceHolder runat="server" ID="plcGC"></asp:PlaceHolder>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">
                        <table>
                            <tr>
                                <td style="width: 200px;">
                                    <strong>
                                        <asp:Label ID="lblPageViews" runat="server" EnableViewState="false" /></strong>
                                </td>
                                <td>
                                    <strong>
                                        <asp:Label ID="lblPageViewsValues" runat="server" EnableViewState="false" /></strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblPages" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblValuePages" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblGetFilePages" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblValueGetFilePages" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblSystemPages" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblValueSystemPages" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblNonPages" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblValueNonPages" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblPagesNotFound" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblValuePagesNotFound" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblStart" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblStartValue" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblRunTime" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblRunTimeValue" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="vertical-align: top;">
                        <table>
                            <tr>
                                <td style="width: 200px;">
                                    <strong>
                                        <asp:Label ID="lblCache" runat="server" EnableViewState="false" /></strong>
                                </td>
                                <td>
                                    <strong>
                                        <asp:Label ID="lblCacheValue" runat="server" EnableViewState="false" /></strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblCacheItems" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblCacheItemsValue" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblCacheExpired" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblCacheExpiredValue" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblCacheRemoved" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblCacheRemovedValue" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblCacheDependency" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblCacheDependencyValue" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                            <tr>
                                <td style="white-space: nowrap;">
                                    <asp:Label ID="lblCacheUnderused" runat="server" EnableViewState="false" />
                                </td>
                                <td>
                                    <asp:Label ID="lblCacheUnderusedValue" runat="server" EnableViewState="false" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </cms:CMSUpdatePanel>
    <br />
    <cms:CMSButton ID="btnClear" runat="server" CssClass="LongSubmitButton" OnClick="btnClear_Click"
        EnableViewState="false" /><cms:CMSButton ID="btnClearCache" runat="server" CssClass="LongSubmitButton"
            OnClick="btnClearCache_Click" EnableViewState="false" /><cms:CMSButton ID="btnRestart"
                runat="server" CssClass="LongSubmitButton" OnClick="btnRestart_Click" EnableViewState="false" /><cms:CMSButton
                    ID="btnRestartWebfarm" runat="server" CssClass="XLongSubmitButton" OnClick="btnRestartWebfarm_Click"
                    EnableViewState="false" /><cms:CMSButton ID="btnHidden" runat="server" Visible="false"
                        EnableViewState="false" />
</asp:Content>
