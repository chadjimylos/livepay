CREATE PROCEDURE [Proc_Forums_ForumPost_UpdateThreadCounts]
	@path nvarchar(450)
AS
BEGIN
	/* Get number of posts */
    DECLARE @Posts int;
	DECLARE @AbsolutePosts int;
	DECLARE @IsThreadApproved bit;
	SET @IsThreadApproved = (SELECT CASE WHEN (SELECT TOP 1 PostID FROM [Forums_ForumPost] WHERE (PostApproved = 1)) is not null THEN 1 ELSE 0 END);
	
	SET @Posts = (SELECT COUNT(*) FROM [Forums_ForumPost] WHERE ([PostIDPath] LIKE @path+'%') AND (PostApproved = 1) AND (@IsThreadApproved = 1));
	SET @AbsolutePosts = (SELECT COUNT(*) FROM [Forums_ForumPost] WHERE ([PostIDPath] LIKE @path+'%'));
	/* Get last post */
	DECLARE @lastPost TABLE (
		PostTime datetime,
		PostUserName nvarchar(200)
	);
	INSERT INTO @lastPost SELECT TOP 1 [PostTime], [PostUserName] FROM [Forums_ForumPost] WHERE ([PostIDPath] LIKE @path+'%') AND (PostApproved = 1) AND (@IsThreadApproved = 1) ORDER BY PostTime DESC;
	DECLARE @LastPostTime datetime;
	DECLARE @LastPostUserName nvarchar(200);
	SET @LastPostTime = (SELECT TOP 1 PostTime FROM @lastPost);
	SET @LastPostUserName = (SELECT TOP 1 PostUserName FROM @lastPost);
	
	/* Get last absolute post */
	DECLARE @lastAbsolutePost TABLE (
		PostTime datetime,
		PostUserName nvarchar(200)
	);
	INSERT INTO @lastAbsolutePost SELECT TOP 1 [PostTime], [PostUserName] FROM [Forums_ForumPost] WHERE ([PostIDPath] LIKE @path+'%') ORDER BY PostTime DESC;
	DECLARE @LastAbsolutePostTime datetime;
	DECLARE @LastAbsolutePostUserName nvarchar(200);
	SET @LastAbsolutePostTime = (SELECT TOP 1 PostTime FROM @lastAbsolutePost);
	SET @LastAbsolutePostUserName = (SELECT TOP 1 PostUserName FROM @lastAbsolutePost);
	
	/* Update forum */
	UPDATE [Forums_ForumPost] SET  
		[PostThreadPosts] = @Posts,
		[PostThreadLastPostTime] = @LastPostTime,
		[PostThreadLastPostUserName] = @LastPostUserName,
		[PostThreadPostsAbsolute] = @AbsolutePosts,
		[PostThreadLastPostTimeAbsolute] = @LastAbsolutePostTime,
		[PostThreadLastPostUserNameAbsolute] = @LastAbsolutePostUserName
	WHERE ([PostIDPath] = @path AND PostParentID IS NULL)
END
