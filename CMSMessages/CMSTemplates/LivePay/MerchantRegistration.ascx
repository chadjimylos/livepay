﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MerchantRegistration.ascx.vb" Inherits="CMSTemplates_LivePay_MerchantRegistration" %>

<script language="javascript" type="text/javascript">

    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

    //--- Error Messages
    var MerchantReg_EmailErrorMes = '';
    var MerchantReg_AFMErrorMes = '';
    var MerchantReg_PhoneErrorMes = '';
    var MerchantReg_OfficerErrorMes = '';
    var MerchantReg_CompanyNameErrorMes = '';
    //-- Error Messages

    function MerchantReg_validateControls(oSrc, args) {
        var bIsValid = true
        var MyVal = args.Value

        if (MyVal.length == 0) {
            bIsValid = false
        }

        if (IsNumeric(MyVal) == false && oSrc.getAttribute('IsNumber') == 'yes') {
            bIsValid = false
        }

        var MaxLenght = MyVal.length
        if (oSrc.getAttribute('CheckLength') == 'yes' && MaxLenght != oSrc.getAttribute('LengthLimit')) {
            bIsValid = false
        }

        if (oSrc.getAttribute('IsEmail') == 'yes') {
            if (checkTheEmail(MyVal) == false) {
                bIsValid = false
                ShowTheCloud('#EmailMsg', MerchantReg_EmailErrorMes, true)
            } else {
                ShowTheCloud('#EmailMsg', MerchantReg_EmailErrorMes, false)
            }
        }

            if (oSrc.getAttribute('IsAFM') == 'yes') {
                if (checkAFM(MyVal) == false) {
                    bIsValid = false
                    ShowTheCloud('#AfmMsg', MerchantReg_AFMErrorMes, true)
                } else {
                    ShowTheCloud('#AfmMsg', MerchantReg_AFMErrorMes, false)
                }
            }

        
            if (oSrc.getAttribute('IsPhone') == 'yes') {
                if (bIsValid == false) 
                    ShowTheCloud('#ContactPhoneMsg', MerchantReg_PhoneErrorMes,true)
                 else 
                    ShowTheCloud('#ContactPhoneMsg', MerchantReg_PhoneErrorMes,false)
            }


            if (oSrc.getAttribute('IsOfficer') == 'yes') {
                if (bIsValid == false) 
                    ShowTheCloud('#CommunicationsOfficerMsg', MerchantReg_OfficerErrorMes,true)
                else
                    ShowTheCloud('#CommunicationsOfficerMsg', MerchantReg_OfficerErrorMes,false)

            }

            if (oSrc.getAttribute('IsCompName') == 'yes') {
                if (bIsValid == false) 
                    ShowTheCloud('#CompanyNameMsg', MerchantReg_CompanyNameErrorMes,true)
                else
                    ShowTheCloud('#CompanyNameMsg', MerchantReg_CompanyNameErrorMes,false)
            }

        args.IsValid = bIsValid;
    }



    function checkAFM(objVal) {
        var afm = objVal;
        var IsValidAfm = true
        if (!afm.match(/^\d{9}$/)) {
            IsValidAfm = false;
            return false;
        }

        afm = afm.split('').reverse().join('');

        var Num1 = 0;
        for (var iDigit = 1; iDigit <= 8; iDigit++) {
            Num1 += afm.charAt(iDigit) << iDigit;
        }

        if ((Num1 % 11) % 10 == afm.charAt(0)) {
            IsValidAfm = true;
            return true;
        }
        else {
            IsValidAfm = false;
            return false;
        }
    }


    function checkTheEmail(MailValue) {
        var filter = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
        if (!filter.test(MailValue))
            return false
        else
            return true
    }

    function IsNumeric(sText) {
        var ValidChars = "0123456789";
        var IsNumber = true;
        var Char;
        for (i = 0; i < sText.length && IsNumber == true; i++) {
            Char = sText.charAt(i);
            if (ValidChars.indexOf(Char) == -1) {
                IsNumber = false;
            }
        }
        return IsNumber;
    }


    function ShowTheCloud(CloudID, CloudContent, Show) {
        $(CloudID).poshytip('hide');

        var OffY = -25;
        if (is_firefox)
            OffY = -10

        $(CloudID).poshytip({
            className: 'tip-livepay',
            content: CloudContent,
            showOn: 'none',
            alignTo: 'target',
            alignX: 'right',
            offsetX: 245,
            offsetY: OffY, hideAniDuration: false
        });
        $(CloudID).poshytip('hide');
        if (Show)
            $(CloudID).poshytip('show');

    }

</script>

<div class="CUDContentBG">
<div class="UD_MainDiv">
<div class="UD_MainDivUnderSecBig">
<div class="UDS_LeftField"><label  id="CompanyName_FieldLabel" class="EditingFormLabel"><%=ResHelper.LocalizeString("{$=Επωνυμία Επιχείρησης:|en-us=Επωνυμία Επιχείρησης:$}") %></label></div>
<div class="UDS_RightFieldComOff"><div class="EditingFormControlNestedControl">
                <a id="CompanyNameMsg" href="#"></a><asp:TextBox TabIndex="1" maxlength="200" id="CompanyName" runat="server" class="ContactTxtName" ></asp:TextBox>
			</div>
            </div>
<div class="ContactStar">(*)</div>
<div class="Clear">&nbsp;</div>
<div class="HideIt"><asp:CustomValidator ID="ReqFldVal_CompanyName" ClientValidationFunction="MerchantReg_validateControls" ValidateEmptyText="true" runat="server" IsCompName="yes" ControlToValidate="CompanyName" text="*" ValidationGroup="MerchantRegistration"/></div> 
</div>

<div class="PT10"><img alt="" src="/app_themes/LivePay/SrcBorderForm.png" /></div>
<div class="UD_MainDivUnderSecBig">
<div class="UDS_LeftField"><label id="CommunicationsOfficer_FieldLabel" class="EditingFormLabel"><%=ResHelper.LocalizeString("{$=Υπεύθυνος Επικοινωνίας:|en-us=Υπεύθυνος Επικοινωνίας:$}") %></label></div>
<div class="UDS_RightFieldComOff"><div class="EditingFormControlNestedControl">
               <a id="CommunicationsOfficerMsg" href="#"></a><asp:TextBox TabIndex="2" maxlength="150" id="CommunicationsOfficer" runat="server" class="ContactTxtName" ></asp:TextBox>
			</div></div>
<div class="ContactStar">(*)</div>
<div class="Clear">&nbsp;</div>
<div class="HideIt"><asp:CustomValidator ID="ReqFldVal_CommunicationsOfficer" ClientValidationFunction="MerchantReg_validateControls" ValidateEmptyText="true" runat="server" IsOfficer="yes" ControlToValidate="CommunicationsOfficer" text="*" ValidationGroup="MerchantRegistration"/></div> 
</div>
<div class="PT5"><img alt="" src="/app_themes/LivePay/SrcBorderForm.png" /></div>
<div class="UD_MainDivUnderSecBig">
<div class="UDS_LeftField"><label id="ContactPhone_FieldLabel" class="EditingFormLabel"><%=ResHelper.LocalizeString("{$=Τηλέφωνο Επικοινωνίας:|en-us=Τηλέφωνο Επικοινωνίας:$}") %></label></div>
<div class="UDS_RightFieldPhone"><div class="EditingFormControlNestedControl">
                <a id="ContactPhoneMsg" href="#"></a><asp:TextBox TabIndex="3" maxlength="10" id="ContactPhone" runat="server" class="ContactTxtName" ></asp:TextBox>
			</div></div>
<div class="ContactStar">(*)</div>
<div class="Clear">&nbsp;</div>
<div class="HideIt"><asp:CustomValidator ID="ReqFldVal_ContactPhone" ClientValidationFunction="MerchantReg_validateControls" ValidateEmptyText="true" runat="server" IsPhone="yes" IsNumber="yes" CheckLength="yes" LengthLimit="10" ControlToValidate="ContactPhone" text="*" ValidationGroup="MerchantRegistration"/></div> 
</div>
<div class="PT5"><img alt="" src="/app_themes/LivePay/SrcBorderForm.png" /></div>
<div class="UD_MainDivUnderSecBig">
<div class="UDS_LeftField"><label id="Email_FieldLabel" class="EditingFormLabel" ><%=ResHelper.LocalizeString("{$=E-mail:|en-us=E-mail:$}") %></label></div>
<div class="UDS_RightFieldMail"><div class="EditingFormControlNestedControl">
				<div class="ContactTxtEmail">
                <a id="EmailMsg" href="#"></a><asp:TextBox TabIndex="4" maxlength="100" id="txtEmail" runat="server" class="ContactTxtName" ></asp:TextBox>
                </div>
			</div></div>
<div class="ContactStar">(*)</div>
<div class="Clear">&nbsp;</div>
<div class="HideIt"><asp:CustomValidator ID="ReqFldVal_Email" ClientValidationFunction="MerchantReg_validateControls" ValidateEmptyText="true" runat="server" IsEmail="yes" ControlToValidate="txtEmail" text="*" ValidationGroup="MerchantRegistration"/></div> 
</div>
<div class="PT5"><img alt="" src="/app_themes/LivePay/SrcBorderForm.png" /></div>
<div class="UD_MainDivUnderSecBig">
<div class="UDS_LeftField"><label id="AFM_FieldLabel" class="EditingFormLabel"><%=ResHelper.LocalizeString("{$=Α.Φ.Μ:|en-us=Α.Φ.Μ:$}") %></label></div>
<div class="UDS_RightFieldAfm"><div class="EditingFormControlNestedControl">
                <a id="AfmMsg" href="#"></a><asp:TextBox TabIndex="5" maxlength="9"  id="txtAFM" runat="server" class="ContactTxtName" ></asp:TextBox>
			</div></div>
<div class="ContactStar">(*)</div>
<div class="Clear">&nbsp;</div>
<div class="HideIt"><asp:CustomValidator ID="ReqFldVal_AFM" ClientValidationFunction="MerchantReg_validateControls" ValidateEmptyText="true" runat="server" IsNumber="yes" isAFM="yes" CheckLength="yes" LengthLimit="9" ControlToValidate="txtAFM" text="*" ValidationGroup="MerchantRegistration"/></div> 
</div>
<div class="PT5"><img alt="" src="/app_themes/LivePay/SrcBorderForm.png" /></div>
<div class="UD_MainDivUnderSecBig">
<div class="UDS_LeftField"><label id="Comments_FieldLabel" class="EditingFormLabel"><%=ResHelper.LocalizeString("{$=Σχόλια:|en-us=Σχόλια:$}") %></label></div>
<div class="UDS_RightField"><div class="EditingFormControlNestedControl">
                <asp:TextBox TextMode="MultiLine" TabIndex="6" Rows="3" Columns="20" id="Comments" runat="server" class="ContactTxtA" ></asp:TextBox>
			</div></div>
<div class="Clear">&nbsp;</div>
</div>
<div class="Clear">&nbsp;</div>
<div class="PT5 PB10">
<div class="UDS_LeftField">&nbsp;</div>
<div class="UD_ButtonBig">
<asp:ImageButton ID="btnOK" runat="server" TabIndex="7" ImageUrl="/App_Themes/LivePay/Contact/btnSend.png" CssClass="FormButton" ValidationGroup="MerchantRegistration" />
<div class="Clear">&nbsp;</div>
</div>
</div>
<div class="Clear">&nbsp;</div>
</div></div>


	

