<%@ Page Language="C#" AutoEventWireup="true" Theme="default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    CodeFile="User_Edit_Friends_Requested.aspx.cs" Inherits="CMSModules_Friends_Administration_Users_User_Edit_Friends_Requested" %>

<%@ Register Src="~/CMSModules/Friends/Controls/FriendsRequestedList.ascx" TagName="FriendsListRequested"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" Visible="false" EnableViewState="false" />
    <asp:PlaceHolder ID="plcTable" runat="server">
        <cms:FriendsListRequested ID="FriendsListRequested" runat="server" Visible="true"
            IsLiveSite="false" />
        <cms:LocalizedLabel ID="lblInfo" runat="server" ResourceString="friends.nouserrequestedfriends"
            EnableViewState="false" />
    </asp:PlaceHolder>
</asp:Content>
