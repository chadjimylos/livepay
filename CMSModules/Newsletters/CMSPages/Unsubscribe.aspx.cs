using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.Newsletter;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Newsletters_CMSPages_Unsubscribe : CMSPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Get data from query string
        Guid subscriberGuid = QueryHelper.GetGuid("subscriberguid", Guid.Empty);
        Guid newsletterGuid = QueryHelper.GetGuid("newsletterguid", Guid.Empty);
        int issueID = QueryHelper.GetInteger("issueid", 0);

        // Get site id
        int siteId = 0;
        if (CMSContext.CurrentSite != null)
        {
            siteId = CMSContext.CurrentSite.SiteID;
        }

        if ((subscriberGuid != Guid.Empty) && (newsletterGuid != Guid.Empty) && (siteId != 0))
        {
            // Check if subscriber with given GUID is subscribed to specified newsletter
            if (SubscriberProvider.IsSubscribed(subscriberGuid, newsletterGuid, siteId))
            {
                // Unsubscribe the subscriber from newsletter
                SubscriberProvider.Unsubscribe(subscriberGuid, newsletterGuid, siteId);
                lblInfo.Visible = true;
                lblInfo.Text = ResHelper.GetString("Unsubscribe.Unsubscribed");

                // If subscriber has unsubscribed from certain issue
                // increment number of unsubscribed persons of that issue by 1
                if (issueID > 0)
                {
                    Issue issue = IssueProvider.GetIssue(issueID);
                    if (issue != null)
                    {
                        issue.IncreaseUnsubscribedNumber();
                        IssueProvider.SetIssue(issue);
                    }
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Unsubscribe.NotSubscribed");
            }
        }
        else
        {
            lblError.Visible = true;
            if (subscriberGuid == Guid.Empty)
            {
                lblError.Text += ResHelper.GetString("Unsubscribe.SubscriberDoesNotExist");
            }
            if (newsletterGuid == Guid.Empty)
            {
                lblError.Text += " \n\r " + ResHelper.GetString("Unsubscribe.NewsletterDoesNotExist");
            }
        }
    }
}
