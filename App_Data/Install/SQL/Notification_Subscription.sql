CREATE TABLE [Notification_Subscription] (
		[SubscriptionID]                   int NOT NULL IDENTITY(1, 1),
		[SubscriptionGatewayID]            int NOT NULL,
		[SubscriptionTemplateID]           int NOT NULL,
		[SubscriptionEventSource]          nvarchar(100) NULL,
		[SubscriptionEventCode]            nvarchar(100) NULL,
		[SubscriptionEventDisplayName]     nvarchar(250) NOT NULL,
		[SubscriptionEventObjectID]        int NULL,
		[SubscriptionTime]                 datetime NOT NULL,
		[SubscriptionUserID]               int NOT NULL,
		[SubscriptionTarget]               nvarchar(250) NOT NULL,
		[SubscriptionLastModified]         datetime NOT NULL,
		[SubscriptionGUID]                 uniqueidentifier NOT NULL,
		[SubscriptionEventData1]           nvarchar(max) NULL,
		[SubscriptionEventData2]           nvarchar(max) NULL,
		[SubscriptionUseHTML]              bit NULL,
		[SubscriptionSiteID]               int NULL
)
ON [PRIMARY]
ALTER TABLE [Notification_Subscription]
	ADD
	CONSTRAINT [PK_Notification_Subscription]
	PRIMARY KEY
	([SubscriptionID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Notification_Subscription]
	ADD
	CONSTRAINT [DEFAULT_Notification_Subscription_SubscriptionEventDisplayName]
	DEFAULT ('') FOR [SubscriptionEventDisplayName]
ALTER TABLE [Notification_Subscription]
	ADD
	CONSTRAINT [DEFAULT_Notification_Subscription_SubscriptionSiteID]
	DEFAULT ((0)) FOR [SubscriptionSiteID]
ALTER TABLE [Notification_Subscription]
	ADD
	CONSTRAINT [DEFAULT_Notification_Subscription_SubscriptionUseHTML]
	DEFAULT ((0)) FOR [SubscriptionUseHTML]
IF OBJECT_ID(N'[Notification_Subscription]') IS NOT NULL
	AND OBJECT_ID(N'[Notification_Gateway]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Notification_Subscription_SubscriptionGatewayID_Notification_Gateway]') IS NULL
BEGIN
		ALTER TABLE [Notification_Subscription]
			ADD CONSTRAINT [FK_Notification_Subscription_SubscriptionGatewayID_Notification_Gateway]
			FOREIGN KEY ([SubscriptionGatewayID]) REFERENCES [Notification_Gateway] ([GatewayID])
END
IF OBJECT_ID(N'[Notification_Subscription]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Notification_Subscription_SubscriptionSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Notification_Subscription]
			ADD CONSTRAINT [FK_Notification_Subscription_SubscriptionSiteID_CMS_Site]
			FOREIGN KEY ([SubscriptionSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
IF OBJECT_ID(N'[Notification_Subscription]') IS NOT NULL
	AND OBJECT_ID(N'[Notification_Template]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Notification_Subscription_SubscriptionTemplateID_Notification_Template]') IS NULL
BEGIN
		ALTER TABLE [Notification_Subscription]
			ADD CONSTRAINT [FK_Notification_Subscription_SubscriptionTemplateID_Notification_Template]
			FOREIGN KEY ([SubscriptionTemplateID]) REFERENCES [Notification_Template] ([TemplateID])
END
IF OBJECT_ID(N'[Notification_Subscription]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Notification_Subscription_SubscriptionUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Notification_Subscription]
			ADD CONSTRAINT [FK_Notification_Subscription_SubscriptionUserID_CMS_User]
			FOREIGN KEY ([SubscriptionUserID]) REFERENCES [CMS_User] ([UserID])
END
CREATE INDEX [IX_Notification_Subscription_SubscriptionEventSource_SubscriptionEventCode_SubscriptionEventObjectID]
	ON [Notification_Subscription] ([SubscriptionEventSource], [SubscriptionEventCode], [SubscriptionEventObjectID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Notification_Subscription_SubscriptionGatewayID]
	ON [Notification_Subscription] ([SubscriptionGatewayID])
	ON [PRIMARY]
CREATE INDEX [IX_Notification_Subscription_SubscriptionSiteID]
	ON [Notification_Subscription] ([SubscriptionSiteID])
	ON [PRIMARY]
CREATE INDEX [IX_Notification_Subscription_SubscriptionTemplateID]
	ON [Notification_Subscription] ([SubscriptionTemplateID])
	ON [PRIMARY]
CREATE INDEX [IX_Notification_Subscription_SubscriptionUserID]
	ON [Notification_Subscription] ([SubscriptionUserID])
	ON [PRIMARY]
