﻿Imports CMS.DataEngine
Imports System.Data

Partial Class CMSTemplates_LivePay_DecryptedCard
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("updatecard") = 1 Then
                UpdateCards()
            ElseIf Request.QueryString("updatetypecards") = 1 Then
                UpdateTypeCards()
            End If
        End If

    End Sub

    Dim cn As GeneralConnection = ConnectionHelper.GetConnection()

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim EncryptedCard As String = Left(StripHTMLFunctions.StripTags(tbDecryptedCard.Text).ToString, 4) & CryptoFunctions.EncryptString(Right(StripHTMLFunctions.StripTags(tbDecryptedCard.Text).ToString, 12))

        Dim ds As DataSet = cn.ExecuteQuery("Eurobank.LivePay.selectCards", Nothing, "cardnumber = '" & EncryptedCard & "'")

        For Each row As DataRow In ds.Tables(0).Rows
            Dim DecryptedCard As String = Left(row("CardNumber").ToString, 4) & CryptoFunctions.DecryptString(Right(row("CardNumber").ToString, row("CardNumber").ToString.Length - 4))
            'Response.Write(EncryptedCard & "<br />")
        Next

        gvShowData.DataSource = ds
        gvShowData.DataBind()
    End Sub

    Private Sub UpdateCards()
        Dim ds As DataSet = Nothing
        Dim Where As String = String.Empty
        ds = cn.ExecuteQuery("Eurobank.LivePay.UpdateCards_DeleteUser", Nothing)
        Dim CardsIDs As String = String.Empty

        If ds IsNot Nothing Then
            For Each row As DataRow In ds.Tables(0).Rows
                CardsIDs = row("CardsIDs").ToString()
            Next
        End If
        ds = Nothing
        If CardsIDs.Trim.Length > 0 Then
            Where = "uc.cardid in (" & CardsIDs.Substring(0, CardsIDs.Length - 1) & ")"
            ds = cn.ExecuteQuery("Eurobank.LivePay.selectCards", Nothing, Where)
        End If

        gvShowData.DataSource = ds
        gvShowData.DataBind()
    End Sub

    Private Sub UpdateTypeCards()
        Dim dsCards As DataSet = cn.ExecuteQuery("Eurobank.LivePay.selectCards", Nothing, "status='a'")

        Dim CardLimitID As Integer = 0
        Dim CardTypeID As Integer = 0
        Dim CardIDs As Integer = 0
        Dim sbCardIDs As New StringBuilder

        For Each rowCard As DataRow In dsCards.Tables(0).Rows
            Dim dtCards As DataTable = DBConnection.GetMerchantCardType(rowCard("CardID").ToString(), Nothing)
            Dim DecryptedCard As String = String.Empty

            CardTypeID = Convert.ToInt32(ValidationHelper.GetInteger(rowCard("CardTypeID"), 0))
            CardIDs = Convert.ToInt32(ValidationHelper.GetInteger(rowCard("CardID"), 0))

            For Each row As DataRow In dtCards.Rows
                Dim Limits As String = row("Limit").ToString
                If Limits <> String.Empty Then
                    Dim LimitLength As Integer = Split(Limits, ",").Length - 1
                    For iCount As Integer = 0 To LimitLength
                        Dim CardLimitNo As String = Split(Limits, ",")(iCount)

                        DecryptedCard = Left(row("CardNo").ToString, 4) & CryptoFunctions.DecryptString(Right(row("CardNo").ToString, row("CardNo").ToString.Length - 4))

                        If DecryptedCard.StartsWith(CardLimitNo) Then
                            CardLimitID = row("itemid").ToString
                        End If
                    Next
                End If
            Next
            Dim parameters As Object(,) = New Object(1, 2) {}
            parameters(0, 0) = "@CardTypeID"
            parameters(0, 1) = CardLimitID
            parameters(1, 0) = "@CardID"
            parameters(1, 1) = CardIDs

            If CardTypeID <> CardLimitID Then

                cn.ExecuteQuery("Eurobank.LivePay.UpdateCardType", parameters)
                Response.Write("Update CardTypeID -> " & CardLimitID & " and CardID -> " & CardIDs & "<br />")
                If sbCardIDs.Length = 0 Then
                    sbCardIDs.Append(CardIDs)
                Else
                    sbCardIDs.Append("," & CardIDs)
                End If
            End If
        Next
        Response.Write("sbCardIDs -> " & sbCardIDs.ToString())

        'Dim gvds As DataSet = Nothing
        'If sbCardIDs.Length > 0 Then
        '    gvds = cn.ExecuteQuery("Eurobank.LivePay.selectCards", Nothing, "uc.cardid in (" & sbCardIDs.ToString() & ")")
        'End If

        '    gvShowData.DataSource = gvds
        '    gvShowData.DataBind()

    End Sub

    Private Function DecryptedCard(ByVal Card As String) As String
        Dim DecCard As String = Left(Card, 4) & CryptoFunctions.DecryptString(Right(Card, Card.ToString.Length - 4))
        Return DecCard
    End Function

    Protected Sub gvShowData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvShowData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim Card As Literal = CType(e.Row.FindControl("litCard"), Literal)
            If Card IsNot Nothing Then
                Dim DecCard As String = String.Empty
                DecCard = DecryptedCard(StripHTMLFunctions.StripTags(Card.Text))
                'If DecCard = tbDecryptedCard.Text Then
                Card.Text = DecCard
                'End If
            End If
        End If
    End Sub
End Class
