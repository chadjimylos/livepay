using System;
using System.Web.UI;

using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.TreeEngine;
using CMS.EventLog;
using CMS.SettingsProvider;
using CMS.WorkflowEngine;

public partial class CMSModules_Content_CMSDesk_Tree : CMSContentPage, IPostBackEventHandler
{
    #region "Request properties"

    /// <summary>
    /// Requested action
    /// </summary>
    protected string Action
    {
        get
        {
            return ValidationHelper.GetString(Request.Form["hdnAction"], "");
        }
    }


    /// <summary>
    /// Action parameter 1
    /// </summary>
    protected string Param1
    {
        get
        {
            return ValidationHelper.GetString(Request.Form["hdnParam1"], "");
        }
    }


    /// <summary>
    /// Action parameter 2
    /// </summary>
    protected string Param2
    {
        get
        {
            return ValidationHelper.GetString(Request.Form["hdnParam2"], "");
        }
    }

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register the dialog script
        ScriptHelper.RegisterDialogScript(this);

        AddScript("treeUrl = '" + ResolveUrl("~/CMSModules/Content/CMSDesk/Tree.aspx") + "'; var contentDir = '" + ResolveUrl("~/CMSModules/Content/CMSDesk/") + "';\n");

        treeContent.NodeTextTemplate = ContextMenuContainer.GetStartTag("nodeMenu", "##NODEID##", false, false) + "<span class=\"ContentTreeItem\" onclick=\"SelectNode(##NODEID##, this); return false;\">##ICON##<span class=\"Name\">##NODENAME##</span></span>" + ContextMenuContainer.GetEndTag(false);
        treeContent.SelectedNodeTextTemplate = ContextMenuContainer.GetStartTag("nodeMenu", "##NODEID##", false, false) + "<span id=\"treeSelectedNode\" class=\"ContentTreeSelectedItem\" onclick=\"SelectNode(##NODEID##, this); return false;\">##ICON##<span class=\"Name\">##NODENAME##</span></span>" + ContextMenuContainer.GetEndTag(false);
        treeContent.MaxTreeNodeText = "<span class=\"ContentTreeItem\" onclick=\"Listing(##PARENTNODEID##, this); return false;\"><span class=\"Name\" style=\"font-style: italic;\">" + ResHelper.GetString("ContentTree.SeeListing") + "</span></span>";
        treeContent.SelectPublishedData = false;

        if (!string.IsNullOrEmpty(CMSContext.CurrentUser.UserStartingAliasPath))
        {
            treeContent.Path = CMSContext.CurrentUser.UserStartingAliasPath;
        }

        if (!Page.IsCallback)
        {
            // If nodeId set, init the list of the nodes to expand
            int expandNodeId = QueryHelper.GetInteger("expandnodeid", 0);
            treeContent.ExpandNodeID = expandNodeId;

            // Current Node ID
            int nodeId = QueryHelper.GetInteger("nodeid", 0);
            treeContent.NodeID = nodeId;

            string script = null;

            // Setup the current node script
            if ((nodeId > 0) && !RequestHelper.IsPostBack())
            {
                script += "currentNodeId = " + nodeId + ";\n";
            }

            script += "function ProcessRequest(action, param1, param2){ document.getElementById('hdnAction').value = action; document.getElementById('hdnParam1').value = param1; document.getElementById('hdnParam2').value = param2; " + ClientScript.GetPostBackEventReference(this, null) + "}\n";

            AddScript(script);
        }
    }


    public void RaisePostBackEvent(string eventArgument)
    {
        CurrentUserInfo currentUser = CMSContext.CurrentUser;

        // Current Node ID
        int nodeId = ValidationHelper.GetInteger(Param1, 0);

        TreeProvider tree = new TreeProvider(currentUser);
        EventLogProvider log = new EventLogProvider(tree.Connection);
        string documentName = string.Empty;
        string action = Action.ToLower();

        // Process the request
        switch (action)
        {
            case "moveup":
            case "movedown":
            case "movetop":
            case "movebottom":
                // Move the document up (document order)
                try
                {
                    if (nodeId == 0)
                    {
                        AddAlert(ResHelper.GetString("ContentRequest.ErrorMissingSource"));
                        return;
                    }

                    // Get document to move
                    TreeNode node = tree.SelectSingleNode(nodeId);

                    // Check the permissions for document
                    if (currentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Allowed)
                    {
                        switch (action)
                        {
                            case "moveup":
                                node = tree.MoveNodeUp(nodeId);
                                break;

                            case "movedown":
                                node = tree.MoveNodeDown(nodeId);
                                break;

                            case "movetop":
                                node = tree.SelectSingleNode(nodeId);
                                tree.SetNodeOrder(nodeId, DocumentOrderEnum.First);
                                break;

                            case "movebottom":
                                node = tree.SelectSingleNode(nodeId);
                                tree.SetNodeOrder(nodeId, DocumentOrderEnum.Last);
                                break;
                        }

                        if (node != null)
                        {
                            // Log the synchronization tasks for the entire tree level
                            string siteName = CMSContext.CurrentSiteName;
                            if (SettingsKeyProvider.GetBoolValue(siteName + ".CMSStagingLogChanges"))
                            {
                                // Log the synchronization tasks for the entire tree level
                                DocumentHelper.LogChangeOrderSynchronization(siteName, node.NodeAliasPath, tree, true);
                            }

                            // Select the document in the tree
                            documentName = node.DocumentName;

                            treeContent.ExpandNodeID = node.NodeParentID;
                            treeContent.NodeID = node.NodeID;
                            AddScript("currentNodeId = " + node.NodeID + ";\n");
                        }
                        else
                        {
                            AddAlert(ResHelper.GetString("ContentRequest.MoveFailed"));
                        }
                    }
                    else
                    {
                        AddAlert(ResHelper.GetString("ContentRequest.MoveDenied"));
                    }
                }
                catch (Exception ex)
                {
                    log.LogEvent("E", DateTime.Now, "Content", "MOVE", currentUser.UserID, currentUser.UserName, nodeId, documentName, HTTPHelper.GetUserHostAddress(), EventLogProvider.GetExceptionLogMessage(ex), CMSContext.CurrentSite.SiteID, HTTPHelper.GetAbsoluteUri());
                    AddAlert(ResHelper.GetString("ContentRequest.MoveFailed") + " : " + ex.Message);
                }
                break;

            case "setculture":
                // Set the preferred culture code
                try
                {
                    // Set the culture code
                    string language = ValidationHelper.GetString(Param2, "");
                    if (!string.IsNullOrEmpty(language))
                    {
                        CMSContext.PreferredCultureCode = language;
                    }

                    // Refresh the document
                    if (nodeId > 0)
                    {
                        treeContent.NodeID = nodeId;

                        AddScript("SelectNode(" + nodeId + "); \n");
                    }
                }
                catch (Exception ex)
                {
                    log.LogEvent("E", DateTime.Now, "Content", "SETCULTURE", currentUser.UserID, currentUser.UserName, nodeId, documentName, HTTPHelper.GetUserHostAddress(), EventLogProvider.GetExceptionLogMessage(ex), CMSContext.CurrentSite.SiteID, HTTPHelper.GetAbsoluteUri());
                    AddAlert(ResHelper.GetString("ContentRequest.ErrorChangeLanguage"));
                }
                break;

            // Sorting
            case "sortalphaasc":
            case "sortalphadesc":
            case "sortdateasc":
            case "sortdatedesc":
                // Set the preferred culture code
                try
                {
                    // Get document to sort
                    TreeNode node = tree.SelectSingleNode(nodeId);

                    // Check the permissions for document
                    if ((currentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Modify) == AuthorizationResultEnum.Allowed)
                        && (currentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.ExploreTree) == AuthorizationResultEnum.Allowed))
                    {
                        switch (action)
                        {
                            case "sortalphaasc":
                                tree.OrderNodesAlphabetically(nodeId, true);
                                break;

                            case "sortalphadesc":
                                tree.OrderNodesAlphabetically(nodeId, false);
                                break;

                            case "sortdateasc":
                                tree.OrderNodesByDate(nodeId, true);
                                break;

                            case "sortdatedesc":
                                tree.OrderNodesByDate(nodeId, false);
                                break;
                        }
                    }
                    else
                    {
                        AddAlert(ResHelper.GetString("ContentRequest.SortDenied"));
                    }

                    // Refresh the tree
                    if (nodeId > 0)
                    {
                        treeContent.ExpandNodeID = nodeId;
                        treeContent.NodeID = nodeId;
                    }
                }
                catch (Exception ex)
                {
                    log.LogEvent("E", DateTime.Now, "Content", "SORT", currentUser.UserID, currentUser.UserName, nodeId, documentName, HTTPHelper.GetUserHostAddress(), EventLogProvider.GetExceptionLogMessage(ex), CMSContext.CurrentSite.SiteID, HTTPHelper.GetAbsoluteUri());
                    AddAlert(ResHelper.GetString("ContentRequest.ErrorSort"));
                }
                break;
        }
    }


    /// <summary>
    /// Adds the alert message to the output request window
    /// </summary>
    /// <param name="message">Message to display</param>
    private void AddAlert(string message)
    {
        ltlScript.Text += ScriptHelper.GetAlertScript(message);
    }


    /// <summary>
    /// Adds the script to the output request window
    /// </summary>
    /// <param name="script">Script to add</param>
    public override void AddScript(string script)
    {
        ltlScript.Text += ScriptHelper.GetScript(script);
    }
}
