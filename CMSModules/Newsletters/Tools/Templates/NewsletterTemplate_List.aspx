<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NewsletterTemplate_List.aspx.cs"
    Inherits="CMSModules_Newsletters_Tools_Templates_NewsletterTemplate_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Tools - Newsletter templates" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <cms:UniGrid runat="server" ID="UniGrid" GridName="NewsletterTemplate_List.xml" OrderBy="TemplateDisplayName"
        Columns="TemplateID, TemplateDisplayName, TemplateType" IsLiveSite="false" />
</asp:Content>
