using System;

using CMS.GlobalHelper;
using CMS.FormEngine;
using CMS.UIControls;

public partial class CMSModules_CustomTables_AlternativeForms_AlternativeForms_Fields : CMSCustomTablesPage
{
    protected int altFormId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        altFormId = QueryHelper.GetInteger("altformid", 0);

        altFormFieldEditor.AlternativeFormID = altFormId;
        altFormFieldEditor.DisplayedControls = FieldEditorControlsEnum.CustomTables;
    }
}
