﻿Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports CMS.GlobalHelper
Imports CMS.UIControls
Imports CMS.Controls.CMSAbstractTransformation
Imports LivePay_ESBBridge
Imports CMS.Controls

Partial Class CMSTemplates_LivePay_PopUp_UCReceipt
    Inherits CMSUserControl
#Region "Variables"
    Private sTransID As String = Nothing
    Private bIsForSearch As Boolean = False
#End Region

#Region "Properties"

    Public Property TransID() As String
        Get
            Return sTransID
        End Get
        Set(ByVal value As String)
            sTransID = value
        End Set
    End Property

    Public Property IsForSearch() As Boolean
        Get
            Return bIsForSearch
        End Get
        Set(ByVal value As Boolean)
            bIsForSearch = value
        End Set
    End Property
#End Region

#Region "Methods"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not StopProcessing AndAlso (Not RequestHelper.IsPostBack()) Then
            ReloadData()
        End If
    End Sub

    Public Overloads Sub ReloadData()
        If Me.TransID <> String.Empty Then
            If Me.IsForSearch Then
                Me.TransID = Replace(Me.TransID, " ", String.Empty)
                Me.TransID = Right(Me.TransID, (Me.TransID.Length - 2))
            End If
           

            Dim MerchantName As String = String.Empty
            Dim w As New LivePay_ESBBridge.Bridge
            Dim TransResponse As TransactionDetailsResponse = w.GetTransactionDetails(TransID)
            Dim TransInfo As TransactionDetailsInfo = TransResponse.TransactionDetails
            btnSaveToPDF.CommandArgument = Me.TransID
            Session("TransReceipt_TransIDForExport") = Me.TransID
            btnSaveToPDF.Attributes.Add("onclick", "return ExportDetails('/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=TransReceipt')")
            If Not IsNothing(TransInfo) Then

                tdDtm.InnerHtml = TransInfo.transactionDate
                tdPayCode.InnerHtml = TransInfo.transactionId
                tdCustCode.InnerHtml = TransInfo.customerId

                Dim amount As String = Replace(TransInfo.transactionAmount, ".", ",")
                Dim FinalAmount As String
                If amount.Split(",").Length > 1 Then
                    FinalAmount = amount & New String("0", 2 - amount.ToString().Split(",")(1).Length)
                ElseIf amount.Split(",").Length = 1 Then
                    FinalAmount = amount & ",00"
                Else
                    FinalAmount = amount
                End If
                tdPrice.InnerHtml = FinalAmount
                Dim CardNo As String = Replace(TransInfo.cardNumber, " ", String.Empty)
                tdCardNo.InnerHtml = String.Concat("xxxxxxxx-xxxx-", Right(CardNo, 4))

                Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(TransInfo.merchantId)
                Dim LivePay_MerchantID As Integer = 0
                If dtMerc.Rows.Count > 0 Then
                    LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
                End If

                Dim dt As DataTable = DBConnection.GetMerchantByID(LivePay_MerchantID)
                If dt.Rows.Count > 0 Then
                    MerchantName = dt.Rows(0)("CompanyName").ToString
                    Me.imgLogo.ImageUrl = "/getattachment/" & dt.Rows(0)("Logo").ToString & "/logo.aspx"

                End If
                TdCompanyTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=Πληροφορίες συναλλαγής - ", MerchantName, "|en-us=Πληροφορίες συναλλαγής - ", MerchantName, "$}")) ' MerchantName
                tdCompPay.InnerHtml = MerchantName
                CreateCustomFields(TransInfo.merchantId, TransInfo)
            End If
        End If
    End Sub

    Private Sub CreateCustomFields(ByVal MerchantID As String, ByVal TransInfo As TransactionDetailsInfo)
        Dim dtMerc As DataTable = DBConnection.GetMerchantByESBID(MerchantID)
        Dim LivePay_MerchantID As Integer = 0
        If dtMerc.Rows.Count > 0 Then
            LivePay_MerchantID = dtMerc.Rows(0)("MerchantID").ToString
        End If

        Dim ds As DataSet = DBConnection.GetMerchantCustomFields(LivePay_MerchantID)
        Dim dt As DataTable = ds.Tables(0)
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim CustomTR As New HtmlTableRow
            Dim CustomTDTitle As New HtmlTableCell
            Dim CustomTDValue As New HtmlTableCell
            CustomTDTitle.Attributes.Add("class", "PopTransRecTitle")
            CustomTDValue.Attributes.Add("class", "PopTransDetValue")
            CustomTDTitle.InnerHtml = ResHelper.LocalizeString(String.Concat("{$=", dt.Rows(i)("NameFieldGr").ToString, "|en-us=", dt.Rows(i)("NameFieldEn").ToString, "$}"))
            Select Case i
                Case 0
                    CustomTDValue.InnerHtml = TransInfo.info1
                Case 1
                    CustomTDValue.InnerHtml = TransInfo.info2
                Case 2
                    CustomTDValue.InnerHtml = TransInfo.info3
                Case 3
                    CustomTDValue.InnerHtml = TransInfo.info4
                Case 4
                    CustomTDValue.InnerHtml = TransInfo.info5
                Case 5
                    CustomTDValue.InnerHtml = TransInfo.info6
                Case 6
                    CustomTDValue.InnerHtml = TransInfo.info7
                Case 7
                    CustomTDValue.InnerHtml = TransInfo.info8
                Case 8
                    CustomTDValue.InnerHtml = TransInfo.info9
                Case 9
                    CustomTDValue.InnerHtml = TransInfo.info10
            End Select

            CustomTR.Controls.Add(CustomTDTitle)
            CustomTR.Controls.Add(CustomTDValue)
            tblCustomFields.Controls.Add(CustomTR)
        Next
    End Sub

    Private Sub JsScript(ByVal Script As String)
        Dim jsName As Guid = Guid.NewGuid
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), jsName.ToString, Script, True)
    End Sub

#End Region

   
    Protected Sub btnSaveToPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveToPDF.Click
        Session("TransReceipt_TransIDForExport") = btnSaveToPDF.CommandArgument
        Response.Redirect("/CMSTemplates/LivePay/ExportToPDF.aspx?ExportType=TransReceipt")
    End Sub
End Class
