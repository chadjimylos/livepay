﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.CMSHelper;
using CMS.SettingsProvider;

public partial class CMSFormControls_Selectors_QuicklyInsertImage_Default : CMSDeskPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Validate query string
        if (!QueryHelper.ValidateHash("hash"))
        {
            fileUploaderElem.StopProcessing = true;
        }
        else
        {
            CMSDialogHelper.RegisterDialogHelper(this.Page);

            // Initialize uploader control properties by query string values
            fileUploaderElem.ImageWidth = 20;
            fileUploaderElem.ImageHeight = 20;
            fileUploaderElem.ImageUrl = "~/CMSAdminControls/FCKeditor/editor/plugins/QuicklyInsertImage/fckIcon.gif";
            fileUploaderElem.InnerDivHtml = QueryHelper.GetString("innerdivhtml", String.Empty);
            fileUploaderElem.InnerDivClass = QueryHelper.GetString("innerdivclass", String.Empty);
            fileUploaderElem.LoadingImageUrl = GetImageUrl("Design/Preloaders/preload16.gif");

            fileUploaderElem.FormGUID = QueryHelper.GetGuid("formguid", Guid.Empty);
            fileUploaderElem.NodeParentNodeID = QueryHelper.GetInteger("parentid", 0);
            fileUploaderElem.DocumentID = QueryHelper.GetInteger("documentid", 0);

            fileUploaderElem.OnlyImages = true;
            fileUploaderElem.SourceType = MediaSourceEnum.DocumentAttachments;

            string siteName = CMSContext.CurrentSiteName;
            string allowed = QueryHelper.GetString("allowedextensions", "");
            if (allowed == "")
            {
                allowed = SettingsKeyProvider.GetStringValue(siteName + ".CMSUploadExtensions");
            }
            fileUploaderElem.AllowedExtensions = allowed;

            fileUploaderElem.ResizeToWidth = QueryHelper.GetInteger("autoresize_width", 0); ;
            fileUploaderElem.ResizeToHeight = QueryHelper.GetInteger("autoresize_height", 0); ;
            fileUploaderElem.ResizeToMaxSideSize = QueryHelper.GetInteger("autoresize_maxsidesize", 0);

            fileUploaderElem.AfterSaveJavascript = "InsertImage";
            fileUploaderElem.InsertMode = true;
        }
    }
}
