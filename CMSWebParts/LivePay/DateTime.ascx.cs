using System;
using System.Web.UI.WebControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.PortalEngine;

public partial class CMSWebParts_LivePay_DateTime: CMSAbstractWebPart
{
    #region "Javascript properties"

    /// <summary>
    /// Gets or sets the value that indicates whether to use server time or not
    /// </summary>
    public bool JsUseServerTime
    {
        get
        {
            return ValidationHelper.GetBoolean(GetValue("JsUseServerTime"), false);
        }
        set
        {
            SetValue("JsUseServerTime", value);
        }
    }


    /// <summary>
    /// Gets or sets the date time format (ie. "dd.mm.yy")
    /// </summary>
    public string JsFormat
    {
        get
        {
            return ValidationHelper.GetString(GetValue("JsFormat"), "dd.m.yy");
        }
        set
        {
            SetValue("JsFormat", value);
        }
    }

    #endregion


    #region "Page events"

    /// <summary>
    /// Page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!StopProcessing)
        {
            SetupControl();
        }
    }


    /// <summary>
    /// Page prerender
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (ScriptHelper.IsLightBoxRegistered(Page) && (CMSContext.ViewMode == ViewModeEnum.Design))
        {
           
        }
    }

    #endregion


    #region "Other methods"

    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        lblDateTime.Text = DateTime.Now.ToString("dddd, dd MMMM yyyy");
    }

    #endregion
}
