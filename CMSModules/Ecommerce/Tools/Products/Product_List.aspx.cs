using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.DataEngine;
using CMS.CMSHelper;
using CMS.Ecommerce;
using CMS.SiteProvider;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Products_Product_List : CMSEcommercePage
{
    //string reportFields = "SKUName;SKUNumber;SKUPrice;";
    protected string editToolTip = ResHelper.GetString("Product_List.EditToolTip");
    protected string deleteToolTip = ResHelper.GetString("Product_List.DeleteToolTip");
    private string selectedSite;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblStoreStatus.Text = ResHelper.GetString("Product_List.StoreStatus");
        lblInternalStatus.Text = ResHelper.GetString("Product_List.InternalStatus");
        lblDepartment.Text = ResHelper.GetString("Product_List.Department");
        lblName.Text = ResHelper.GetString("Product_List.Name");
        lblNumber.Text = ResHelper.GetString("Product_List.Number");
        btnFilter.Text = ResHelper.GetString("general.show");
        lblSites.Text = ResHelper.GetString("Product_List.Sites");

        // Set site selector        
        siteSelector.AllowAll = false;
        siteSelector.UseCodeNameForSelection = true;
        siteSelector.UniSelector.SpecialFields = new string[2, 2] { { ResHelper.GetString("general.selectall"), "" }, { ResHelper.GetString("Product_List.Sites.NotAssigned"), "0" } };

        // fill department dropdownlist
        if ((CMSContext.CurrentUser != null) && !CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            departmentElem.UserID = CMSContext.CurrentUser.UserID;
        }

        // Non global admin users will see only sites where they are members
        if (!CMSContext.CurrentUser.IsGlobalAdministrator)
        {
            siteSelector.UserId = CMSContext.CurrentUser.UserID;
        }

        if (!RequestHelper.IsPostBack())
        {
            // Preselect all sites
            selectedSite = "";
            siteSelector.Value = selectedSite;
        }
        else
        {
            selectedSite = ValidationHelper.GetString(siteSelector.Value, String.Empty);
        }

        gridData.OnAction += new OnActionEventHandler(gridData_OnAction);
        gridData.OnExternalDataBound += new OnExternalDataBoundEventHandler(gridData_OnExternalDataBound);
        gridData.ZeroRowsText = ResHelper.GetString("Product_List.NoProductAvailable");
        gridData.WhereCondition = GetWhereCondition();

        InitializeMasterPage();
    }


    /// <summary>
    /// Initializes master page elements
    /// </summary>
    private void InitializeMasterPage()
    {
        // Set the master page actions element        
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("Product_List.NewItem");
        actions[0, 3] = "~/CMSModules/Ecommerce/Tools/Products/Product_New.aspx";
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_SKU/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;

        // Set the master page title
        this.CurrentMaster.HeaderActions.HelpTopicName = "product_list";
        this.CurrentMaster.HeaderActions.HelpName = "helpTopic";
    }


    private string GetWhereCondition()
    {
        string where = "";

        // display ONLY products - not product options
        where += " (SKUOptionCategoryID IS NULL) AND ";
        // value from txtName
        if (txtName.Text != "")
        {
            where += "SKUName LIKE '%" + txtName.Text.Trim().Replace("'", "''") + "%' AND ";
        }
        // value from txtNumber
        if (txtNumber.Text != "")
        {
            where += "SKUNumber LIKE '%" + txtNumber.Text.Trim().Replace("'", "''") + "%' AND ";
        }
        // value from drpDepartment
        if (departmentElem.DepartmentID > 0)
        {
            where += "SKUDepartmentID=" + departmentElem.DepartmentID + " AND ";
        }
        // Internal status value
        if (internalStatusElem.InternalStatusID > 0)
        {
            where += "InternalStatusID = " + internalStatusElem.InternalStatusID + " AND ";
        }
        // Store status value
        if (publicStatusElem.PublicStatusID > 0)
        {
            where += "PublicStatusID = " + publicStatusElem.PublicStatusID + " AND ";
        }

        // Sites
        if (!String.IsNullOrEmpty(selectedSite))
        {
            // Not assigned products
            if (selectedSite == "0")
            {
                where += "SKUID NOT IN (SELECT NodeSKUID FROM View_CMS_Tree_Joined WHERE NodeSKUID IS NOT NULL) AND ";
            }
            // Products assigned to the specified site
            else
            {
                where += "SKUID IN (SELECT NodeSKUID FROM View_CMS_Tree_Joined WHERE SiteName = '" + selectedSite.Replace("'", "''") + "' AND NodeSKUID IS NOT NULL) AND ";
            }
        }

        if (CMSContext.CurrentUser != null)
        {
            if (CMSContext.CurrentUser.IsGlobalAdministrator)
            {
                // display products from all departments to Global administrator
                if (where != "")
                {
                    // trim ending ' AND'
                    where = where.Remove(where.Length - 4);
                }
            }
            else
            {
                // Complete where
                where += "SKUDepartmentID IN (SELECT DepartmentID FROM COM_UserDepartment WHERE UserID=" + CMSContext.CurrentUser.UserID + ")";
            }

            return where;
        }
        return null;
    }


    object gridData_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "publicstatusdisplayname":
                return HTMLHelper.HTMLEncode(ResHelper.LocalizeString(Convert.ToString(parameter)));
            case "skuenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }

        return parameter;
    }


    void gridData_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "edit":
                UrlHelper.Redirect("Product_Edit_Frameset.aspx?productid=" + ValidationHelper.GetInteger(actionArgument, 0));
                break;

            case "delete":

                // check 'EcommerceModify' permission
                if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
                {
                    RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
                }

                // Check dependencies
                if (SKUInfoProvider.CheckDependencies(ValidationHelper.GetInteger(actionArgument, 0)))
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                    return;
                }

                SKUInfoProvider.DeleteSKUInfo(ValidationHelper.GetInteger(actionArgument, 0));

                break;
        }
    }
}
