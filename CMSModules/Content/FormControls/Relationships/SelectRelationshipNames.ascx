<%@ Control Language="C#" AutoEventWireup="true" CodeFile="SelectRelationshipNames.ascx.cs"
    Inherits="CMSModules_Content_FormControls_Relationships_SelectRelationshipNames" %>
<%@ Register src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" tagname="UniSelector" tagprefix="cms" %>

<cms:CMSUpdatePanel ID="pnlUpdate" runat="server">
    <ContentTemplate>
        <cms:UniSelector ID="uniSelector" runat="server" ObjectType="cms.relationshipname"
            ResourcePrefix="unirelationshipnameselector" AllowAll="false" SelectionMode="SingleDropDownList"
            DisplayNameFormat="{%RelationshipDisplayName%}" />
    </ContentTemplate>
</cms:CMSUpdatePanel>
