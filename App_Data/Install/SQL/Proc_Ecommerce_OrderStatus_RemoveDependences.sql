-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Ecommerce_OrderStatus_RemoveDependences]
	@OrderStatusID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
    -- COM_Order
	UPDATE COM_Order SET [OrderStatusID] = NULL WHERE [OrderStatusID] = @OrderStatusID;
	
	COMMIT TRANSACTION;
END
