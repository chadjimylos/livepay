<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Modules.aspx.cs" Theme="default"
    Inherits="CMSDesk_Tools_Modules" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Tools Descprition" %>

<%@ Register Src="~/CMSAdminControls/UI/UIProfiles/UIGuide.ascx" TagName="UIGuide" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="plcContent" runat="server">
    <uc1:UIGuide ID="guide" runat="server" EnableViewState="false" ModuleName="CMS.Tools"
        ModuleAvailabilityForSiteRequired="true" />
</asp:Content>
