using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;

public partial class CMSModules_Reporting_Controls_FontSpec : System.Web.UI.UserControl
{
    private string mValue = "";

    /// <summary>
    /// Gets or sets the font specification 
    /// </summary>
    public string Value
    {
        get
        {
            string str = drpFontName.SelectedValue + ";" + drpFontSize.SelectedValue + ";" + chkBold.Checked + ";" + chkItalic.Checked;
            return str;
        }
        set
        {
            if (value != null)
            {
                string[] values = value.Split(';');
                if (values.Length >= 4)
                {
                    drpFontName.SelectedValue = ValidationHelper.GetString(values[0], "");
                    drpFontSize.SelectedValue = ValidationHelper.GetString(values[1], "");
                    chkBold.Checked = ValidationHelper.GetBoolean(values[2], false);
                    chkItalic.Checked = ValidationHelper.GetBoolean(values[3], false);
                }
                mValue = value; //save for later(LoadData) initialization
            }
        }
    }
	

    /// <summary>
    /// Handles page load
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RequestHelper.IsPostBack())
        {
            chkItalic.Text = ResHelper.GetString("rep.graph.fontitalic");
            chkBold.Text = ResHelper.GetString("rep.graph.fontbold");

            LoadData();
        }        
    }


    /// <summary>
    /// Prepares UI and load data from value
    /// </summary>    
    private void LoadData()
    {
        //Fill drop downs
        drpFontName.Items.Add(new ListItem(ResHelper.GetString("rep.graph.fontarial"), "arial"));
        // zed graph don't set correct comics sans
        //drpFontName.Items.Add(new ListItem(ResHelper.GetString("rep.graph.fontcomics"), "comics sans"));
        drpFontName.Items.Add(new ListItem(ResHelper.GetString("rep.graph.fontcourier"), "courier"));
        drpFontName.Items.Add(new ListItem(ResHelper.GetString("rep.graph.fonttahoma"), "tahoma"));
        drpFontName.Items.Add(new ListItem(ResHelper.GetString("rep.graph.fonttimes"), "times"));
        drpFontName.Items.Add(new ListItem(ResHelper.GetString("rep.graph.fontverdana"), "verdana"));

        // Arial, Comics Sans MS, Courier New , Tahoma, Times New Roman, Verdana

        for (int i=8; i <= 30; i++) {
            drpFontSize.Items.Add(new ListItem(i.ToString()));
        }

        // Set by mValue
        string[] values = mValue.Split(';');

        if (values.Length >= 4)
        {
            drpFontName.SelectedValue = values[0];
            drpFontSize.SelectedValue = values[1];
            chkBold.Checked = ValidationHelper.GetBoolean(values[2], false);
            chkItalic.Checked = ValidationHelper.GetBoolean(values[3], false);
        }
    }
}
