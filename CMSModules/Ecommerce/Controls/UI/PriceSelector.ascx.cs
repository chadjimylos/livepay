using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.Ecommerce;

public partial class CMSModules_Ecommerce_Controls_UI_PriceSelector : CMSUserControl
{
    #region "Public properties"

    /// <summary>
    /// Indicates whether the validators messages should be under the textbox. Default is false.
    /// </summary>
    public bool ValidatorOnNewLine
    {
        get
        {
            return !String.IsNullOrEmpty(this.ltlBreak.Text);
        }
        set
        {
            this.ltlBreak.Text = (value ? "<br/>" : "");
        }
    }


    /// <summary>
    /// Gets or sets the message which is displayed in required field validator of the price textbox.
    /// </summary>
    public string EmptyErrorMessage
    {
        get
        {
            return this.rfvPrice.ErrorMessage;
        }
        set
        {
            this.rfvPrice.ErrorMessage = value;
        }
    }


    /// <summary>
    /// Gets or sets the message which is displayed in range field validator of the price textbox.
    /// </summary>
    public string ValidationErrorMessage
    {
        get
        {
            return this.rvPrice.ErrorMessage;
        }
        set
        {
            this.rvPrice.ErrorMessage = value;
        }
    }


    /// <summary>
    /// Enables or disables the price textbox.
    /// </summary>
    public bool Enabled
    {
        get
        {
            return this.txtPrice.Enabled;
        }
        set
        {
            this.txtPrice.Enabled = value;
        }
    }


    /// <summary>
    /// Gets or sets the value with the number of digits according to main currency significant digits settings.
    /// </summary>
    public double Value
    {
        get
        {
            return ValidationHelper.GetDouble(FormatPrice(this.txtPrice.Text), 0);
        }
        set
        {
            this.txtPrice.Text = FormatPrice(value);
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Validates the price (to be the double value)
    /// </summary>
    public string ValidatePrice(bool isProductOption)
    {
        if (isProductOption)
        {
            // Product option can be negative
            if (!ValidationHelper.IsDouble(txtPrice.Text.Trim()))
            {
                return this.ValidationErrorMessage;
            }
        }
        else
        {
            // Basic product can't be negative
            if (ValidationHelper.GetDouble(txtPrice.Text.Trim(), -1) < 0)
            {
                return this.ValidationErrorMessage;
            }
        }
        return "";
    }


    /// <summary>
    /// Returns value with the number of digits according to main currency significant digits settings.
    /// </summary>
    /// <param name="value"></param>
    private string FormatPrice(string value)
    {
        value = (value != null ? value.Trim() : "");
        return FormatPrice(ValidationHelper.GetDouble(value, 0));
    }


    /// <summary>
    /// Returns value with the number of digits according to main currency significant digits settings.
    /// </summary>
    /// <param name="value">Value to be formated</param>
    private string FormatPrice(double value)
    {
        CurrencyInfo mainCurrency = CurrencyInfoProvider.GetMainCurrency();
        if (mainCurrency != null)
        {
            int digits = mainCurrency.CurrencyRoundTo;
            string format = "0." + new string('0', digits);
            return value.ToString(format);
        }

        return value.ToString();
    }

    #endregion
}
