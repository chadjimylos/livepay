<%@ Page Language="C#" AutoEventWireup="true" CodeFile="categories.aspx.cs" Inherits="CMSSiteManager_Settings_categories"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/EmptyPage.master" Title="SiteManager - Settings" %>

<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/Trees/TreeBorder.ascx" TagName="TreeBorder" TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:CMSUpdatePanel runat="server" ID="pnlUpdate" UpdateMode="Always">
        <ContentTemplate>
            <div class="MenuBox">
                <asp:Panel ID="pnlMenu" runat="server" CssClass="TreeMenu TreeMenuPadding SettingsMenu">
                    <asp:Panel ID="pnlMenuContent" runat="server" CssClass="TreeMenuContent">
                        <strong>
                            <asp:Label ID="lblSite" runat="server" CssClass="ContentLabel" EnableViewState="false" />
                        </strong>
                        <br />
                        <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" />
                    </asp:Panel>
                </asp:Panel>
            </div>
            <asp:Panel runat="server" ID="pnlBody" CssClass="ContentTree">
                <cms:TreeBorder ID="borderElem" runat="server" MinSize="10,*" FramesetName="colsFrameset" AllowMouseResizing="false" />
                <div class="TreeArea">
                    <div class="TreeMenuPadding TreeMenu SettingsMenu">
                        &nbsp;
                    </div>
                    <div class="TreeAreaTree">
                        <asp:TreeView ID="TreeViewCategories" runat="server" ShowLines="true" ShowExpandCollapse="true"
                            CssClass="ContentTree" EnableViewState="false" />
                    </div>
                </div>
            </asp:Panel>
            <input type="hidden" id="selectedCategoryId" name="selectedCategoryId" value="0" />
        </ContentTemplate>
    </cms:CMSUpdatePanel>

    <script type="text/javascript">
        //<![CDATA[
        function RefreshKeys(categoryId, siteId, nodeElem) {
            if ((currentNode != null) && (nodeElem != null)) {
                currentNode.className = 'ContentTreeItem';
            }

            var url = "keys.aspx?categoryid=" + categoryId;
            if (siteId > 0) {
                url = url + "&siteid=" + siteId;
            }

            parent.frames['keys'].location.href = url;
            document.getElementById('selectedCategoryId').value = categoryId;

            if (nodeElem != null) {
                currentNode = nodeElem;
                currentNode.className = 'ContentTreeSelectedItem';
            }
        }
        //]]>
    </script>

</asp:Content>
