using System;
using System.Data;
using System.Web.UI.WebControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;
using CMS.UIControls;
using CMS.TreeEngine;

public partial class CMSSiteManager_Development_TagGroups_Tags_Documents : SiteManagerPage
{
    #region "Variables"

    private int mTagId = 0;
    private TreeProvider tree = null;

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        // Get current tag ID
        mTagId = QueryHelper.GetInteger("tagid", 0);
        TagInfo ti = TagInfoProvider.GetTagInfo(mTagId);

        filterDocuments.OnSiteSelectionChanged += UniSelector_OnSelectionChanged;

        if (ti != null)
        {
            // Initialize TreeProvider
            tree = new TreeProvider(CMSContext.CurrentUser);

            int groupId = QueryHelper.GetInteger("groupid", 0);
            int siteId = QueryHelper.GetInteger("siteid", 0);

            string[,] pageTitleTabs = new string[2, 3];
            pageTitleTabs[0, 0] = ResHelper.GetString("taggroup_edit.itemlistlink");
            pageTitleTabs[0, 1] = "~/CMSSiteManager/Development/TagGroups/TagGroup_Edit_Tags.aspx?groupid=" + groupId + "&siteid=" + siteId;
            pageTitleTabs[0, 2] = "groupContent";
            pageTitleTabs[1, 0] = ti.TagName;
            pageTitleTabs[1, 1] = string.Empty;
            pageTitleTabs[1, 2] = string.Empty;

            CurrentMaster.Title.Breadcrumbs = pageTitleTabs;

            uniGridDocs.OnDataReload += new UniGrid.OnDataReloadEventHandler(uniGridDocs_OnDataReload);
            uniGridDocs.OnExternalDataBound += uniGridDocs_OnExternalDataBound;
            uniGridDocs.ZeroRowsText = ResHelper.GetString("taggroup_edit.documents.nodata");
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        if (filterDocuments.SelectedSite != TreeProvider.ALL_SITES)
        {
            uniGridDocs.GridView.Columns[4].Visible = false;
        }
    }

    #endregion


    #region "Events"

    protected DataSet uniGridDocs_OnDataReload(string completeWhere, string currentOrder, int currentTopN, int currentOffset, int currentPageSize, ref int totalRecords)
    {
        string where = "(DocumentID IN (SELECT CMS_DocumentTag.DocumentID FROM CMS_DocumentTag WHERE TagID = " + mTagId + "))";
        where = SqlHelperClass.AddWhereCondition(where, completeWhere);
        where = SqlHelperClass.AddWhereCondition(where, filterDocuments.WhereCondition);

        DataSet ds = tree.SelectNodes(filterDocuments.SelectedSite, TreeProvider.ALL_DOCUMENTS, TreeProvider.ALL_CULTURES,
            false, null, where, "DocumentName, SiteName", -1, true, currentTopN, "ClassName, NodeID,NodeClassID, DocumentName, DocumentNamePath, DocumentCulture, SiteName");

        totalRecords = DataHelper.GetItemsCount(ds);
        return ds;
    }


    protected object uniGridDocs_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        DataRowView drv = GetDataRowView(sender as DataControlFieldCell);
        string siteName = ValidationHelper.GetString(drv["SiteName"], string.Empty);
        SiteInfo siteInfo = SiteInfoProvider.GetSiteInfo(siteName);
        string documentCulture = null;
        int nodeId = 0;

        switch (sourceName.ToLower())
        {
            case "documentname":
                // Generate link to the document
                string documentName = TextHelper.LimitLength(ValidationHelper.GetString(drv["DocumentName"], string.Empty), 50);
                nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                documentCulture = ValidationHelper.GetString(drv["DocumentCulture"], string.Empty);
                return GetDocumentLink(documentName, siteInfo, nodeId, documentCulture);

            case "documentnametooltip":
                drv = (DataRowView)parameter;
                return UniGridFunctions.DocumentNameTooltip(drv);

            case "nodeclassid":
            case "nodeclassidtooltip":
                int nodeClassId = ValidationHelper.GetInteger(drv["NodeClassID"], 0);
                DataClassInfo dci = DataClassInfoProvider.GetDataClass(nodeClassId);
                string result = (dci != null) ? dci.ClassDisplayName : string.Empty;
                if (sourceName.ToLower() == "nodeclassid")
                {
                    result = TextHelper.LimitLength(result, 50);
                }
                result = HTMLHelper.HTMLEncode(result);
                return result;

            case "sitename":
                return HTMLHelper.HTMLEncode(siteInfo.DisplayName);

            case "culture":
                return UniGridFunctions.DocumentCultureFlag(drv, this.Page);

            case "icon":
                string className = ValidationHelper.GetString(drv["ClassName"], string.Empty);
                nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);
                documentCulture = ValidationHelper.GetString(drv["DocumentCulture"], string.Empty);
                return GetDocumentIconLink(siteInfo, nodeId, documentCulture, className);

            default:
                return parameter;
        }
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        pnlUpdate.Update();
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private object GetDocumentIconLink(SiteInfo si, int nodeId, string cultureCode, string className)
    {
        string url = GetDocumentUrl(si, nodeId, cultureCode);
        string toReturn = "<img style=\"border-style:none;\" src=\"" + GetDocumentTypeIconUrl(className) + "\" alt=\"" + className + "\" />";

        if (!string.IsNullOrEmpty(url))
        {
            return "<a href=\"" + url + "\" target=\"_blank\">" + toReturn + "</a>";
        }
        return toReturn;
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }


    /// <summary>
    /// Returns complete link opening the new window and displaying selected document
    /// </summary>
    private static object GetDocumentLink(string documentName, SiteInfo si, int nodeId, string cultureCode)
    {
        // If the document path is empty alter it with the default '/'
        if (string.IsNullOrEmpty(documentName))
        {
            documentName = "-";
        }

        string url = GetDocumentUrl(si, nodeId, cultureCode);
        string toReturn = HTMLHelper.HTMLEncode(documentName);

        if (!string.IsNullOrEmpty(url))
        {
            toReturn = "<a href=\"" + url + "\" target=\"_blank\">" + toReturn + "</a>";
        }
        return toReturn;
    }


    /// <summary>
    /// Generates URL for document
    /// </summary>
    private static string GetDocumentUrl(SiteInfo si, int nodeId, string cultureCode)
    {
        string url = string.Empty;

        if (si.Status == SiteStatusEnum.Running)
        {
            if (si.SiteName != CMSContext.CurrentSiteName)
            {
                // Make url for site in form 'http(s)://sitedomain/application/cmsdesk'.
                url = UrlHelper.GetApplicationUrl(si.DomainName) + "/cmsdesk/default.aspx?section=content&nodeid=" + nodeId + "&culture=" + cultureCode;
            }
            else
            {
                // Make url for current site (with port)
                url = UrlHelper.ResolveUrl("~/cmsdesk/default.aspx?section=content&nodeid=" + nodeId + "&culture=" + cultureCode);
            }
        }
        return url;
    }

    #endregion
}
