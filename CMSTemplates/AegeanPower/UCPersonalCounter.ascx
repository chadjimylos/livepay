﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCPersonalCounter.ascx.vb" Inherits="CMSTemplates_AegeanPower_UCPersonalCounter" %>

<style type="text/css">
#AegeanPowerRegistrationparticulars
{
    margin-left:10px;
    padding:12px 0px 0px 14px;
    width:480px;
    background-color:#F7F9F9;
    float:left;
    font-family:Tahoma;
    font-size:11px;
}

.SelectCounter{width:185px;height:23px;float:left;}
.NightlifeMeterReading{width:104px;float:left;height:16px;}
.moreinfoinputdata a{width:16px;height:16px;float:left;}
.ClassInputData
{
    height:19px;
    width:102px;
    margin:6px 0px 8px 182px;
}
.ObligatoryAreaCountAlone{float:left;width:150px;color:Red;margin:-18px 0px 0px 140px;}
.ObligatoryAreaStar
{
    float:left;
    color:#7ac142;
    width:6px;
    height:9px;
    font-weight:bold;
    background-image:url(/App_Themes/AegeanPower/Images/ObligatoryArea.png);
    background-position:left;
    background-repeat:no-repeat;
    margin-left:3px;
}
</style>

<div id="AegeanPowerRegistrationparticulars">
<div style="padding:10px;color:Red" id="DivNoActiveAFM" runat="server" visible="true"><%=ResHelper.LocalizeString("{$=Για να χρησιμοποιήσετε την υπηρεσία θα πρέπει να διαθέτετε ένα ενεργό Α.Φ.Μ.<br />Για την προσθήκη νέου Α.Φ.Μ. πατήστε|en-us=In order to use this service you need to have at least one active VAT number.<br>To add a new VAT number click$}") %>
     <a  href='<%=ResHelper.LocalizeString("{$=/Αρχική-Σελίδα/Κέντρο-Πελατών/Μέτρα-Μόνος-σου/Καταχωρήστε-το-ΑΦΜ-σας.aspx|en-us=/Αρχική-Σελίδα/Κέντρο-Πελατών/Μέτρα-Μόνος-σου/Καταχωρήστε-το-ΑΦΜ-σας.aspx$}") %>'><%=ResHelper.LocalizeString("{$=εδώ|en-us=here.$}") %></a> </div>

<div class="LineOfBizCountAlone">
    <div class="LabelOfBizContAloneFirst"><asp:Label ID="lblSelectCounter" runat="server"></asp:Label></div>
    <div class="TetxAreaOfBizContAloneFirst">
        <asp:DropDownList ID="ddlSelectCounter" CssClass="SelectCounter" runat="server" AutoPostBack="true"></asp:DropDownList>
        <span class="ObligatoryAreaStar"></span><br />
        <asp:RequiredFieldValidator ID="rfvSelectCounter"  runat="server" ValidationGroup="All" CssClass="ObligatoryAreaCountAlone" ControlToValidate="ddlSelectCounter" EnableViewState="false"></asp:RequiredFieldValidator>
    </div>
</div>

<div class="LineOfBizCountAlone">
<div class="LabelOfBizContAlone"><asp:Label ID="lblIndication" runat="server"></asp:Label></div>
<div class="TetxAreaOfBizContAlone">
    <asp:TextBox ID="tbIndication" CssClass="NightlifeMeterReading" MaxLength="10" runat="server" ></asp:TextBox>
    <div id="MoreInfoFirst" class="moreinfoinputdata">
        <a rel="583|185" class="iframe-popup" href="~/Αρχική-Σελίδα/Κέντρο-Πελατών/Μέτρα-Μόνος-σου/iButtons/Ένδειξη.aspx"></a>
    </div>
    <span class="ObligatoryAreaStar"></span><br />
    <asp:RequiredFieldValidator ID="rfvIndication" runat="server" ValidationGroup="All" CssClass="ObligatoryAreaCountAlone" ControlToValidate="tbIndication" EnableViewState="false"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="cvIndication" runat="server" Operator="DataTypeCheck" ValidationGroup="All" Type="Integer" CssClass="ObligatoryAreaCountAlone" ControlToValidate="tbIndication"></asp:CompareValidator>
    </div>
</div>

<div class="LineOfBizCountAlone">
<div class="LabelOfBizContAlone"><asp:Label ID="lblNightlifeMeterReading" runat="server"></asp:Label></div>
<div class="TetxAreaOfBizContAlone">
    <asp:TextBox ID="tbNightlifeMeterReading" CssClass="NightlifeMeterReading" MaxLength="10" runat="server"></asp:TextBox>
    <div id="MoreInfoSecond" class="moreinfoinputdata">
        <a rel="583|185" class="iframe-popup" href="~/Αρχική-Σελίδα/Κέντρο-Πελατών/Μέτρα-Μόνος-σου/iButtons/Νυχτερινή-ένδειξη.aspx"></a>
    </div>
    <asp:Image runat="server" Visible="false" ImageUrl="/App_Themes/AegeanPower/Images/ObligatoryArea.jpg" />
    <span id="Star3" runat="server" class="ObligatoryAreaStar" visible="false"></span><br />
    <asp:RequiredFieldValidator ID="rfvNightlifeMeterReading" ValidationGroup="All" CssClass="ObligatoryAreaCountAlone" runat="server" ControlToValidate="tbNightlifeMeterReading" EnableViewState="false"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="cvNightMeterReading" runat="server" Operator="DataTypeCheck" ValidationGroup="All" Type="Integer" CssClass="ObligatoryAreaCountAlone" ControlToValidate="tbNightlifeMeterReading"></asp:CompareValidator>
    </div>
</div>

<div class="LineOfBizCountAlone">
<div class="LabelOfBizContAlone"><asp:Label ID="lblReactiveMeterReading" runat="server"></asp:Label></div>
<div class="TetxAreaOfBizContAlone">
    <asp:TextBox ID="tbReactiveMeterReading" CssClass="NightlifeMeterReading" MaxLength="10" runat="server"></asp:TextBox>
    <div id="MoreInfoThird" class="moreinfoinputdata">
        <a rel="583|185" class="iframe-popup" href="~/Αρχική-Σελίδα/Κέντρο-Πελατών/Μέτρα-Μόνος-σου/iButtons/Άεργος-ένδειξη.aspx"></a>
    </div>
    <span id="Star4" runat="server" class="ObligatoryAreaStar" visible="false"></span><br />
    <asp:RequiredFieldValidator ID="rfvReactiveMeterReading" ValidationGroup="All" CssClass="ObligatoryAreaCountAlone" runat="server" ControlToValidate="tbReactiveMeterReading" EnableViewState="false"></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="cvReactiveMeter" runat="server" Operator="DataTypeCheck" ValidationGroup="All" Type="Integer" CssClass="ObligatoryAreaCountAlone" ControlToValidate="tbReactiveMeterReading"></asp:CompareValidator>
    </div>
</div>

<div class="LineOfBizCountAlone">
<div class="LabelOfBizContAlone"><asp:Label ID="lblMaximumDemand" runat="server"></asp:Label></div>
<div class="TetxAreaOfBizContAlone">
    <asp:TextBox ID="tbMaximumDemand" CssClass="NightlifeMeterReading" MaxLength="10" runat="server"></asp:TextBox>
    <div id="MoreInfoFourth" class="moreinfoinputdata">
        <a rel="583|185" class="iframe-popup" href="~/Αρχική-Σελίδα/Κέντρο-Πελατών/Μέτρα-Μόνος-σου/iButtons/Μέγιστη-ζήτηση.aspx"></a>
    </div>
    <span id="Star5" runat="server" class="ObligatoryAreaStar" visible="false"></span><br />
    <asp:RequiredFieldValidator ID="rfvMaximumDemand" ValidationGroup="All" CssClass="ObligatoryAreaCountAlone" runat="server" ControlToValidate="tbMaximumDemand" EnableViewState="false" ></asp:RequiredFieldValidator>
    <asp:CompareValidator ID="cvtbMaximumDemand" runat="server" Operator="DataTypeCheck" ValidationGroup="All" Type="Integer" CssClass="ObligatoryAreaCountAlone" ControlToValidate="tbMaximumDemand"></asp:CompareValidator>
    </div>
</div>

<asp:ImageButton runat="server" ValidationGroup="All" ImageUrl="/App_Themes/AegeanPower/Images/SubmitButtonInputData.jpg" ID="btnInputData" CssClass="ClassInputData" />

<div class="clear">&nbsp;</div>
</div>
<div class="LineOfCountAlone"></div>
