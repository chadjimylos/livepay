-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Proc_Staging_Server_RemoveDependences]
	@ServerID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION;
	-- Staging_SyncLog
    DELETE FROM Staging_SyncLog WHERE SyncLogServerID = @ServerID;
	-- Staging_Synchronization
	DELETE FROM Staging_Synchronization WHERE SynchronizationServerID = @ServerID;
	COMMIT TRANSACTION;
END
