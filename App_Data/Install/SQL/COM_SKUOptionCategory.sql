CREATE TABLE [COM_SKUOptionCategory] (
		[SKUID]          int NOT NULL,
		[CategoryID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [COM_SKUOptionCategory]
	ADD
	CONSTRAINT [PK_COM_SKUOptionCategory]
	PRIMARY KEY
	([SKUID], [CategoryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[COM_SKUOptionCategory]') IS NOT NULL
	AND OBJECT_ID(N'[COM_OptionCategory]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_SKUOptionCategory_CategoryID_COM_OptionCategory]') IS NULL
BEGIN
		ALTER TABLE [COM_SKUOptionCategory]
			ADD CONSTRAINT [FK_COM_SKUOptionCategory_CategoryID_COM_OptionCategory]
			FOREIGN KEY ([CategoryID]) REFERENCES [COM_OptionCategory] ([CategoryID])
END
IF OBJECT_ID(N'[COM_SKUOptionCategory]') IS NOT NULL
	AND OBJECT_ID(N'[COM_SKU]') IS NOT NULL
	AND OBJECT_ID(N'[FK_COM_SKUOptionCategory_SKUID_COM_SKU]') IS NULL
BEGIN
		ALTER TABLE [COM_SKUOptionCategory]
			ADD CONSTRAINT [FK_COM_SKUOptionCategory_SKUID_COM_SKU]
			FOREIGN KEY ([SKUID]) REFERENCES [COM_SKU] ([SKUID])
END
