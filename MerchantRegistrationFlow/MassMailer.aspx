﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="EuronetActivation.aspx.vb" Inherits="Kentico_MerchantActivationWorkflow.EuronetActivation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="width: 100%; height: 100%; font-family: Arial; background: #E0F0ff">
    <form id="MerchantMID" runat="server">
    <div>
        
            <div style="height: 80px">
            <asp:TextBox ID="txtGuids" TextMode="MultiLine" runat="server" Height="80px" 
                    Width="512px"></asp:TextBox>

                <div style="width: 90px; height: 30px;  background: #114c94; margin-right: 10px;
                    margin-top: 5px; border: 1px solid black">

                    

                    <div style="margin: 8px;vertical-align:middle;text-align:center">
                        <asp:LinkButton Text="Send" runat="server" Font-Bold="True" Font-Size="X-Small"
                            Font-Underline="True" ForeColor="White" OnClick="Send_Click" />
                    </div>
                </div>
            </div>
        
    </div>
    </form>
</body>
</html>
