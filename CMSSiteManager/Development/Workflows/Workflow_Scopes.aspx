<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Workflow_Scopes.aspx.cs"
    Inherits="CMSSiteManager_Development_Workflows_Workflow_Scopes" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    EnableEventValidation="false" Theme="Default" Title="Workflows - Workflow Scopes" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<%@ Register Src="~/CMSFormControls/Sites/SiteSelector.ascx" TagName="SiteSelector" TagPrefix="cms" %>
<asp:Content ContentPlaceHolderID="plcContent" ID="content" runat="server">
    <cms:LocalizedLabel ID="lblSite" runat="server" ResourceString="general.site" DisplayColon="true"
        EnableViewState="false" />
    <cms:SiteSelector ID="siteSelector" runat="server" IsLiveSite="false" />
    <br />
    <br />
    <cms:CMSUpdatePanel ID="pnlUpdate" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <cms:UniGrid ID="UniGridWorkflowScopes" runat="server" GridName="Workflow_Scopes.xml"
                OrderBy="ScopeID" IsLiveSite="false" />
        </ContentTemplate>
    </cms:CMSUpdatePanel>
</asp:Content>
