<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomTable_List.aspx.cs"
    Inherits="CMSModules_CustomTables_Tools_CustomTable_List" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Custom Tables List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <cms:UniGrid runat="server" ID="uniGrid" GridName="CustomTable_List.xml" OrderBy="ClassDisplayName"
        IsLiveSite="false" />
</asp:Content>
