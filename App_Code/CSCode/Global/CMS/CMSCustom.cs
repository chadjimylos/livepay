using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.Scheduler;
using CMS.EventLog;

/// <summary>
/// CMSCustom methods
/// </summary>
public static class CMSCustom
{
    /// <summary>
    /// Initializes the custom methods
    /// </summary>
    public static void Init()
    {
        MacroResolver.OnResolveCustomMacro += new MacroResolver.MacroHandler(ResolveCustomMacro);
        ClassHelper.OnGetCustomClass += new ClassHelper.GetClassEventHandler(GetCustomClass);
    }


    /// <summary>
    /// Custom macro handler
    /// </summary>
    /// <param name="sender">Sender (active macro resolver)</param>
    /// <param name="expression">Expression to resolve</param>
    /// <param name="match">Returns true if the macro matches (was resolved)</param>
    public static string ResolveCustomMacro(MacroResolver sender, string expression, out bool match)
    {
        match = false;
        string result = expression;

        // Add your custom macro evaluation
        /*
        switch (expression.ToLower())
        {
            case "someexpression":
                match = true;
                result = "Resolved expression";
                break;
        }
        */

        return result;
    }


    /// <summary>
    /// Gets the custom class object based on the given class name. This handler is called when the assembly name is App_Code
    /// </summary>
    /// <param name="className">Class name</param>
    public static object GetCustomClass(string className)
    {
        // Provide your custom classes
        switch (className)
        {
            // Define the class MyTask implementing ITask and you can provide your scheduled tasks out of App_Code
            case "Custom.MyTask":
                return new MyTask();
        }

        return null;
    }


    /// <summary>
    /// Sample task class
    /// </summary>
    public class MyTask : ITask
    {
        /// <summary>
        /// Executes the task
        /// </summary>
        /// <param name="ti">Task info</param>
        public string Execute(TaskInfo ti)
        {
            EventLogProvider ev = new EventLogProvider();
            ev.LogEvent("I", DateTime.Now, "MyTask", "Execute", null, "This task was executed from '~/App_Code/Global/CMS/CMSCustom.cs'.");

            return null;
        }
    }
}
