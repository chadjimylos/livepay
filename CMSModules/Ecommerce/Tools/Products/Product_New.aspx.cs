using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Products_Product_New : CMSEcommercePage
{    
    protected int optionCategoryId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        string currentSKU = "";
        string productList = "";
        string productListText = "";        

        // Get option category ID from url
        optionCategoryId = ValidationHelper.GetInteger(Request.QueryString["categoryid"], 0);
        
        // Creating new product option
        if (optionCategoryId > 0)
        {
            this.NewProduct1.OptionCategoryID = optionCategoryId;

            currentSKU = ResHelper.GetString("Product_New.NewProductOption");
            productList = "~/CMSModules/Ecommerce/Tools/ProductOptions/OptionCategory_Edit_Options.aspx?categoryid=" + optionCategoryId;
            productListText = ResHelper.GetString("Product_New.ProductOptionsLink");

            string titleText = "";
            string titleImage = "";

            // Initialize the master page elements
            InitializeMasterPage(titleText, titleImage, productListText, productList, currentSKU);
        }
        // Creating new product
        else
        {
            currentSKU = ResHelper.GetString("com_SKU_edit_general.NewItemCaption");
            productList = "~/CMSModules/Ecommerce/Tools/Products/Product_List.aspx"; ;
            productListText = ResHelper.GetString("com_SKU_edit_general.ItemListLink");

            string titleText = ResHelper.GetString("com_SKU_edit_general.NewItemCaption");
            string titleImage = GetImageUrl("Objects/Ecommerce_SKU/new.png");

            // Initialize the master page elements
            InitializeMasterPage(titleText, titleImage, productListText, productList, currentSKU);
        }

        NewProduct1.OnOkClickHandler += new EventHandler(NewProduct1_OnOkClickHandler);
    }


    /// <summary>
    /// Initialize the master page elements
    /// </summary>
    /// <param name="titleText">Text of the title</param>
    /// <param name="titleImage">Image URL for the title</param>
    private void InitializeMasterPage(string titleText, string titleImage, string productListText, string productList, string currentSKU)
    {
        // Set the master page title
        this.CurrentMaster.Title.HelpTopicName = "general_tab15";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        // Initializes page title control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = productListText;
        pageTitleTabs[0, 1] = productList;
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentSKU;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// On ok click redirect to edit page
    /// </summary>
    void NewProduct1_OnOkClickHandler(object sender, EventArgs e)
    {
        // Check 'EcommerceModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "EcommerceModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "EcommerceModify");
        }

        int productId = ValidationHelper.GetInteger(((CMSModules_Ecommerce_Controls_UI_ProductEdit)sender).ProductID, 0);
        if (productId > 0)
        {
            UrlHelper.Redirect("Product_Edit_Frameset.aspx?productID=" + productId + "&saved=1");
        }
    }
}
