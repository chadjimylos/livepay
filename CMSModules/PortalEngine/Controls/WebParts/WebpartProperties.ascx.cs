using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

using CMS.FormEngine;
using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.PortalEngine;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.EventLog;
using CMS.UIControls;
using CMS.PortalControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_PortalEngine_Controls_WebParts_WebpartProperties : CMSUserControl
{
    #region "Variables"

    protected string mAliasPath = null;
    protected string mZoneId = null;
    protected string mWebpartId = null;
    protected Guid mInstanceGUID = Guid.Empty;
    protected bool mWebPartIdChanged = false;
    protected string mBeforeFormDefinition = null;
    protected string mAfterFormDefinition = null;
    protected bool mIsNewWebPart = false;


    /// <summary>
    /// Current page info
    /// </summary>
    PageInfo pi = null;

    /// <summary>
    /// Page template info
    /// </summary>
    PageTemplateInfo pti = null;

    /// <summary>
    /// Current web part
    /// </summary>
    WebPartInstance webPartInstance = null;

    /// <summary>
    /// Current page template
    /// </summary>
    PageTemplateInstance templateInstance = null;

    /// <summary>
    /// Zone instance.
    /// </summary>
    //WebPartZoneInstance zone = null;

    /// <summary>
    /// Tree provider
    /// </summary>
    TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);

    #endregion


    #region "Public properties"

    /// <summary>
    /// Page alias path
    /// </summary>
    public string AliasPath
    {
        get
        {
            return mAliasPath;
        }
        set
        {
            mAliasPath = value;
        }
    }


    /// <summary>
    /// Zone ID
    /// </summary>
    public string ZoneId
    {
        get
        {
            return mZoneId;
        }
        set
        {
            mZoneId = value;
        }
    }


    /// <summary>
    /// Web part ID
    /// </summary>
    public string WebpartId
    {
        get
        {
            return mWebpartId;
        }
        set
        {
            mWebpartId = value;
        }
    }


    /// <summary>
    /// Instance GUID
    /// </summary>
    public Guid InstanceGUID
    {
        get
        {
            return mInstanceGUID;
        }
        set
        {
            mInstanceGUID = value;
        }
    }


    /// <summary>
    /// True if the web part ID has changed
    /// </summary>
    public bool WebPartIdChanged
    {
        get
        {
            return mWebPartIdChanged;
        }
    }


    /// <summary>
    /// Before form definition
    /// </summary>
    public string BeforeFormDefinition
    {
        get
        {
            return mBeforeFormDefinition;
        }
        set
        {
            mBeforeFormDefinition = value;
        }
    }


    /// <summary>
    /// After form definition
    /// </summary>
    public string AfterFormDefinition
    {
        get
        {
            return mAfterFormDefinition;
        }
        set
        {
            mAfterFormDefinition = value;
        }
    }


    /// <summary>
    /// Whether is webpart new or not.
    /// </summary>
    public bool IsNewWebPart
    {
        get
        {
            return mIsNewWebPart;
        }
        set
        {
            mIsNewWebPart = value;
        }
    }

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        // Load settings
        if (!string.IsNullOrEmpty(Request.Form[hdnIsNewWebPart.UniqueID]))
        {
            IsNewWebPart = ValidationHelper.GetBoolean(Request.Form[hdnIsNewWebPart.UniqueID], false);
        }
        if (!string.IsNullOrEmpty(Request.Form[hdnInstanceGUID.UniqueID]))
        {
            InstanceGUID = ValidationHelper.GetGuid(Request.Form[hdnInstanceGUID.UniqueID], Guid.Empty);
        }

        if (!string.IsNullOrEmpty(WebpartId) && !string.IsNullOrEmpty(AliasPath))
        {
            // Get pageinfo
            pi = PageInfoProvider.GetPageInfo(CMSContext.CurrentSiteName, AliasPath, CMSContext.PreferredCultureCode, null, CMSContext.CurrentSite.CombineWithDefaultCulture, tree.Connection);
            if (pi == null)
            {
                lblInfo.Text = ResHelper.GetString("WebPartProperties.WebPartNotFound");
                pnlFormArea.Visible = false;
                return;
            }

            // Get template
            pti = pi.PageTemplateInfo;

            // Get template instance
            templateInstance = pti.TemplateInstance;

            // Parent webpart
            WebPartInfo parentWpi = null;

            // Get web part from instance
            WebPartInfo wpi = null;
            if (!IsNewWebPart)
            {
                webPartInstance = pti.GetWebPart(InstanceGUID, WebpartId);
                if (webPartInstance == null)
                {
                    lblInfo.Text = ResHelper.GetString("WebPartProperties.WebPartNotFound");
                    pnlFormArea.Visible = false;
                    return;
                }

                wpi = WebPartInfoProvider.GetBaseWebPart(webPartInstance.WebPartType);
            }
            // Webpart instance hasn't created yet
            else
            {
                wpi = WebPartInfoProvider.GetWebPartInfo(ValidationHelper.GetInteger(WebpartId, 0));
                if (wpi != null)
                {
                    if (wpi.WebPartParentID > 0)
                    {
                        parentWpi = WebPartInfoProvider.GetWebPartInfo(wpi.WebPartParentID);
                    }
                }
            }

            // Get the form definition
            string wpProperties = "<form></form>";
            if (wpi != null)
            {
                wpProperties = wpi.WebPartProperties;

                // Use parent webpart if is defined
                if (parentWpi != null)
                {
                    wpProperties = parentWpi.WebPartProperties;
                }

                if (BeforeFormDefinition == null)
                {
                    BeforeFormDefinition = PortalHelper.GetWebPartProperties((WebPartTypeEnum)wpi.WebPartType, PropertiesPosition.Before);
                }
                if (AfterFormDefinition == null)
                {
                    AfterFormDefinition = PortalHelper.GetWebPartProperties((WebPartTypeEnum)wpi.WebPartType, PropertiesPosition.After);
                }
            }

            // Global form info
            FormInfo fi = new FormInfo();

            // 'Before' form properties
            if (!string.IsNullOrEmpty(BeforeFormDefinition))
            {
                // Load before form schema
                fi.LoadXmlDefinition(BeforeFormDefinition);
            }

            // Add 'General' category at the beginning if no one is specified
            if (!string.IsNullOrEmpty(wpProperties) && (!wpProperties.StartsWith("<form><category", StringComparison.InvariantCultureIgnoreCase)))
            {
                wpProperties = wpProperties.Insert(6, "<category name=\"" + ResHelper.GetString("general.general") + "\" />");
            }

            // Load default form schema
            if (!string.IsNullOrEmpty(fi.GetXmlDefinition()))
            {
                FormInfo dfi = new FormInfo();
                dfi.LoadXmlDefinition(wpProperties);
                // Combine with global schema, overwrite existing fields
                fi.CombineWithForm(dfi, true, null, true);
            }
            else
            {
                fi.LoadXmlDefinition(wpProperties);
            }

            // 'After' form properties
            if (!string.IsNullOrEmpty(AfterFormDefinition))
            {
                // Load after form schema
                if (!string.IsNullOrEmpty(fi.GetXmlDefinition()))
                {
                    FormInfo afi = new FormInfo();
                    afi.LoadXmlDefinition(AfterFormDefinition);
                    // Combine with global schema, don't overwrite existing
                    fi.CombineWithForm(afi, false, null, true, false);
                }
                else
                {
                    fi.LoadXmlDefinition(AfterFormDefinition);
                }
            }

            // Get datarow with required columns
            DataRow dr = fi.GetDataRow();

            // Load default values
            if (IsNewWebPart)
            {
                // Check if webpart is inherited, if isn't use normal load of default values 
                if (wpi.WebPartParentID > 0)
                {
                    fi.LoadDefaultValues(dr, wpi.WebPartProperties);
                }
                else
                {
                    fi.LoadDefaultValues(dr);
                }

                // Set control ID
                FormFieldInfo ffi = fi.GetFormField("WebPartControlID");
                if (ffi != null)
                {
                    ffi.DefaultValue = WebPartZoneInstance.GetUniqueWebPartId(wpi.WebPartName, templateInstance);
                    fi.UpdateFormField("WebPartControlID", ffi);
                }
            }

            // Load values from existing webpart
            LoadDataRowFromWebPart(dr, webPartInstance);

            // Init the form
            InitHTMLToobar(fi);
            InitForm(form, dr, fi);

            AddExportLink();
        }
    }


    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        btnOnOK.Click += btnOnOK_Click;
        btnOnApply.Click += btnOnApply_Click;

        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "ApplyButton", ScriptHelper.GetScript(
            "function SetRefresh(refreshpage) { document.getElementById('" + hidRefresh.ClientID + "').value = refreshpage; } \n" +
            "function GetRefresh() { return document.getElementById('" + hidRefresh.ClientID + "').value == 'true'; } \n" +
            "function OnApplyButton(refreshpage) { SetRefresh(refreshpage); " + Page.ClientScript.GetPostBackEventReference(btnOnApply, "") + "} \n" +
            "function OnOKButton(refreshpage) { SetRefresh(refreshpage); " + Page.ClientScript.GetPostBackEventReference(btnOnOK, "") + "} \n"
        ));

        // Reload parent page after closing, if needed
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "UnloadRefresh", ScriptHelper.GetScript("window.isPostBack = false; window.onunload = function() { if (!window.isPostBack && GetRefresh()) { RefreshPage(); }};"));
    }


    /// <summary>
    /// Control ID validation
    /// </summary>
    void formElem_OnItemValidation(object sender, ref string errorMessage)
    {
        Control ctrl = (Control)sender;
        if (ctrl.ID.ToLower() == "webpartcontrolid")
        {
            TextBox ctrlTextbox = (TextBox)ctrl;
            string newId = ctrlTextbox.Text;

            // Validate unique ID
            WebPartInstance existingPart = pti.GetWebPart(newId);
            if ((existingPart != null) && ((webPartInstance == null) || (existingPart.InstanceGUID != webPartInstance.InstanceGUID)))
            {
                // Error - duplicit IDs
                errorMessage = ResHelper.GetString("WebPartProperties.ErrorUniqueID");
            }
        }
    }


    /// <summary>
    /// Saves the given form
    /// </summary>
    /// <param name="form">Form to save</param>
    private static bool SaveForm(BasicForm form)
    {
        if ((form != null) && form.Visible)
        {
            return form.SaveData("");
        }

        return true;
    }


    /// <summary>
    /// Saves webpart properties.
    /// </summary>
    public bool Save()
    {
        // Save the data
        if ((pi != null) && (pti != null) && (templateInstance != null) && SaveForm(form))
        {
            // Add web part if new
            if (IsNewWebPart)
            {
                AddWebPart();
            }

            // Get basicform's datarow and update webpart
            SaveFormToWebPart(form);

            // Save the changes  
            CMSPortalManager.SaveTemplateChanges(pi, pti, templateInstance, WidgetZoneTypeEnum.None, ViewModeEnum.Design, tree);

            // Reload the form (because of macro values set only by JS)
            form.ReloadData();

            // Clear the cached web part
            if (InstanceGUID != null)
            {
                CacheHelper.TouchKey("webpartinstance|" + InstanceGUID.ToString().ToLower());
            }

            return true;
        }

        return false;
    }


    /// <summary>
    /// Initializes the HTML toolbar
    /// </summary>
    /// <param name="form">Form information</param>
    private void InitHTMLToobar(FormInfo form)
    {
        // Display / hide the HTML editor toolbar area
        if (form.UsesHtmlArea())
        {
            plcToolbar.Visible = true;
        }
    }


    /// <summary>
    /// Saves the given DataRow data to the web part properties
    /// </summary>
    /// <param name="form">Form to save</param>
    private void SaveFormToWebPart(BasicForm form)
    {
        if (form.Visible && (webPartInstance != null))
        {
            string oldId = webPartInstance.ControlID.ToLower();

            DataRow dr = form.DataRow;
            foreach (DataColumn column in dr.Table.Columns)
            {
                webPartInstance.MacroTable[column.ColumnName.ToLower()] = form.MacroTable[column.ColumnName.ToLower()];
                webPartInstance.SetValue(column.ColumnName, dr[column]);

                // If name changed, move the content
                if (column.ColumnName.ToLower() == "webpartcontrolid")
                {
                    try
                    {
                        string newId = ValidationHelper.GetString(dr[column], "").ToLower();

                        // Name changed, move the document content if present
                        if ((!string.IsNullOrEmpty(newId)) && (newId != oldId))
                        {
                            mWebPartIdChanged = true;
                            WebpartId = newId;
                            // Get the content
                            string currentContent = (string)(pi.EditableWebParts[oldId]);
                            if (currentContent != null)
                            {
                                TreeNode node = DocumentHelper.GetDocument(pi.DocumentId, tree);

                                // Move the content in the page info
                                pi.EditableWebParts[oldId] = null;
                                pi.EditableWebParts[newId] = currentContent;

                                // Update the document
                                node.SetValue("DocumentContent", pi.GetContentXml());
                                DocumentHelper.UpdateDocument(node, tree);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        EventLogProvider ev = new EventLogProvider();
                        ev.LogEvent("Content", "CHANGEWEBPART", ex);
                    }
                }
            }
        }
    }


    /// <summary>
    /// Loads the data row data from given web part instance
    /// </summary>
    /// <param name="dr">DataRow to fill</param>
    /// <param name="webPart">Source web part</param>
    private static void LoadDataRowFromWebPart(DataRow dr, WebPartInstance webPart)
    {
        foreach (DataColumn column in dr.Table.Columns)
        {
            try
            {
                object value = webPart.GetValue(column.ColumnName);
                if (column.DataType == typeof(decimal))
                {
                    value = ValidationHelper.GetDouble(value, 0, "en-us");
                }
                DataHelper.SetDataRowValue(dr, column.ColumnName, value);
            }
            catch
            {
            }
        }
    }


    /// <summary>
    /// Initializes the form
    /// </summary>
    /// <param name="form">Form</param>
    /// <param name="dr">Datarow with the data</param>
    /// <param name="fi">Form info</param>
    private void InitForm(BasicForm form, DataRow dr, FormInfo fi)
    {
        if (form != null)
        {
            form.DataRow = dr;
            if (webPartInstance != null)
            {
                form.MacroTable = webPartInstance.MacroTable;
            }
            else
            {
                form.MacroTable = new Hashtable();
            }

            form.SubmitButton.Visible = false;
            form.SiteName = CMSContext.CurrentSiteName;
            form.FormInformation = fi;
            form.ShowPrivateFields = true;
            form.OnItemValidation += formElem_OnItemValidation;
        }
    }


    /// <summary>
    /// Saves the webpart properties and closes the window.
    /// </summary>
    protected void btnOnOK_Click(object sender, EventArgs e)
    {
        // Save webpart properties
        if (Save())
        {
            bool refresh = ValidationHelper.GetBoolean(hidRefresh.Value, false);

            string script = "";
            if (WebPartIdChanged || refresh)
            {
                script = "RefreshPage(); \n";
            }

            // Close the window
            ltlScript.Text += ScriptHelper.GetScript(script + "top.window.close();");
        }
    }


    /// <summary>
    /// Saves the webpart properties
    /// </summary>
    protected void btnOnApply_Click(object sender, EventArgs e)
    {
        if (Save())
        {
            hdnIsNewWebPart.Value = "false";
            hdnInstanceGUID.Value = webPartInstance.InstanceGUID.ToString();
            if (WebPartIdChanged)
            {
                ltlScript.Text += ScriptHelper.GetScript("ChangeWebPart('" + ZoneId + "', '" + WebpartId + "', '" + AliasPath + "'); RefreshPage();");
            }

            AddExportLink();
        }
    }


    protected override void Render(HtmlTextWriter writer)
    {
        lblInfo.Visible = (lblInfo.Text != "");

        base.Render(writer);
    }


    /// <summary>
    /// Adds web part.
    /// </summary>
    private void AddWebPart()
    {
        int webpartID = ValidationHelper.GetInteger(WebpartId, 0);
        // Add web part to the currently selected zone under currently selected page
        if ((webpartID > 0) && (!string.IsNullOrEmpty(ZoneId)) && (!string.IsNullOrEmpty(AliasPath)))
        {
            // Get the web part by code name
            WebPartInfo wi = WebPartInfoProvider.GetWebPartInfo(webpartID);
            if (wi != null)
            {
                // Add the web part
                WebPartInstance newPart = pti.AddWebPart(ZoneId, webpartID);
                //zone = templateInstance.GetZone(ZoneId);

                if (newPart != null)
                {
                    // Prepare the form info to get the default properties
                    FormInfo fi = null;
                    if (wi.WebPartParentID > 0)
                    {
                        // Get from parent
                        WebPartInfo parentWi = WebPartInfoProvider.GetWebPartInfo(wi.WebPartParentID);
                        if (parentWi != null)
                        {
                            fi = new FormInfo(parentWi.WebPartProperties);
                        }
                    }
                    if (fi == null)
                    {
                        fi = new FormInfo(wi.WebPartProperties);
                    }

                    // Load the default values to the properties
                    if (fi != null)
                    {
                        DataRow dr = fi.GetDataRow();
                        fi.LoadDefaultValues(dr);

                        newPart.LoadProperties(dr);
                    }

                    // Add webpart to user's last recently used
                    CMSContext.CurrentUser.UserSettings.UpdateRecentlyUsedWebPart(wi.WebPartName);

                    // Add last selection date to webpart
                    wi.WebPartLastSelection = DateTime.Now;
                    WebPartInfoProvider.SetWebPartInfo(wi);

                    webPartInstance = newPart;
                }
            }
        }
    }


    private void AddExportLink()
    {
        if (webPartInstance != null)
        {
            ltlExport.Text = "&nbsp;<a href=\"GetWebPartProperties.aspx?webpartid=" + webPartInstance.ControlID + "&webpartguid=" + webPartInstance.InstanceGUID + "&aliaspath=" + AliasPath + "&zoneid=" + ZoneId + "\">" + ResHelper.GetString("WebpartProperties.Export") + "</a>";
        }
    }
}
