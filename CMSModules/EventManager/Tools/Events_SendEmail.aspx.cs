using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.EmailEngine;
using CMS.EventManager;
using CMS.SettingsProvider;
using CMS.TreeEngine;
using CMS.UIControls;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_EventManager_Tools_Events_SendEmail : CMSEventManagerPage
{
    protected int eventNodeId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        htmlEmail.AutoDetectLanguage = false;
        htmlEmail.DefaultLanguage = System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
        htmlEmail.EditorAreaCSS = "";

        // Init email info form settings
        if (!RequestHelper.IsPostBack())
        {
            string siteName = CMSContext.CurrentSiteName;
            txtSenderEmail.Text = SettingsKeyProvider.GetStringValue(siteName + ".CMSEventManagerInvitationFrom");
            txtSenderName.Text = SettingsKeyProvider.GetStringValue(siteName + ".CMSEventManagerSenderName");
            txtSubject.Text = SettingsKeyProvider.GetStringValue(siteName + ".CMSEventManagerInvitationSubject");
        }

        // Get event node id
        eventNodeId = QueryHelper.GetInteger("eventId", 0);

        // Disable form if no attendees present or user doesn't have modify permission
        if (CMSContext.CurrentUser.IsAuthorizedPerResource("cms.eventmanager", "Modify"))
        {
            DataSet ds = EventAttendeeInfoProvider.GetEventAttendees(eventNodeId, null, null, "AttendeeID", 1);
            if (DataHelper.DataSourceIsEmpty(ds))
            {
                DisableForm();
            }
        }
        else
        {
            DisableForm();
        }
    }


    /// <summary>
    /// Disable form.
    /// </summary>
    private void DisableForm()
    {
        txtSenderName.Enabled = false;
        txtSenderEmail.Enabled = false;
        txtSubject.Enabled = false;
        htmlEmail.Enabled = false;
        btnSend.Enabled = false;
        lblInfo.Text = ResHelper.GetString("Events_List.NoAttendees");
        lblInfo.Visible = true;
    }


    protected void btnSend_Click(object sender, EventArgs e)
    {
        // Check 'Modify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("cms.eventmanager", "Modify"))
        {
            RedirectToCMSDeskAccessDenied("cms.eventmanager", "Modify");
        }

        txtSenderName.Text = txtSenderName.Text.Trim();
        txtSenderEmail.Text = txtSenderEmail.Text.Trim();
        txtSubject.Text = txtSubject.Text.Trim();

        // Validate the fields
        string errorMessage = new Validator().NotEmpty(txtSenderName.Text, ResHelper.GetString("Events_SendEmail.EmptySenderName"))
            .NotEmpty(txtSenderEmail.Text, ResHelper.GetString("Events_SendEmail.EmptySenderEmail"))
            .NotEmpty(txtSubject.Text, ResHelper.GetString("Events_SendEmail.EmptyEmailSubject"))
            .IsEmail(txtSenderEmail.Text, ResHelper.GetString("Events_SendEmail.InvalidEmailFormat"))
            .Result;

        if (errorMessage != String.Empty)
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
            return;
        }

        string senderName = txtSenderName.Text;
        string senderEmail = txtSenderEmail.Text;
        string subject = txtSubject.Text;
        string emailBody = htmlEmail.ResolvedValue;

        // Get event node data
        TreeProvider mTree = new TreeProvider();
        TreeNode node = mTree.SelectSingleNode(eventNodeId);

        if (node != null && node.NodeClassName.ToLower() == "cms.bookingevent")
        {
            // Prepare data
            DataRow[] rows = new DataRow[1];
            rows[0] = node.DataRow;
            // Initialize macro resolver
            MacroResolver resolver = new MacroResolver();
            resolver.KeepUnresolvedMacros = true;
            resolver.SourceData = rows;
            //resolver.SetNamedSourceData("Event", node.DataRow);

            // Resolve e-mail body and subject macros and make links absolute
            emailBody = resolver.ResolveMacros(emailBody);
            emailBody = UrlHelper.MakeLinksAbsolute(emailBody);
            subject = TextHelper.LimitLength(resolver.ResolveMacros(subject), 450);

            // EventSendEmail manages sending e-mails to all attendees
            EventSendEmail ese = new EventSendEmail(eventNodeId, CMSContext.CurrentSiteName,
            subject, emailBody, senderName, senderEmail);

            lblInfo.Visible = true;
            lblInfo.Text = ResHelper.GetString("Events_SendEmail.EmailSent");
        }
    }
}
