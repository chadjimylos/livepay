﻿using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.IO;
using System.Data;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.MediaLibrary;
using CMS.ExtendedControls;
using CMS.SiteProvider;
using CMS.CMSHelper;


public partial class CMSModules_MediaLibrary_Controls_Dialogs_LinkMediaSelector : LinkMediaSelector
{
    #region "Private variables"

    private const string PERCENT_ESC_CHAR = "|";

    // Media library variables
    private string mFolderPath = "";
    private string mMediaLibraryRootFolder = null;

    private string mCurrentAction = null;

    private MediaLibraryInfo mLibraryInfo = null;
    private SiteInfo mLibrarySiteInfo = null;
    private Hashtable mFileList = null;

    private string mSortDirection = "ASC";
    private string mSortColumns = "FileName";

    private bool wasLoaded = false;
    private bool mLibraryChanged = false;

    #endregion


    #region "Private properties"

    /// <summary>
    /// Returns current properties (according to OutputFormat).
    /// </summary>
    protected override ItemProperties Properties
    {
        get
        {
            switch (Config.OutputFormat)
            {
                case OutputFormatEnum.HTMLMedia:
                    return htmlMediaProp;

                case OutputFormatEnum.HTMLLink:
                    return htmlLinkProp;

                case OutputFormatEnum.BBMedia:
                    return bbMediaProp;

                case OutputFormatEnum.BBLink:
                    return bbLinkProp;

                case OutputFormatEnum.URL:
                    if (Config.OutputFormat == OutputFormatEnum.NodeGUID)
                    {
                        return nodeGuidProp;
                    }
                    return urlProp;

                default:
                    return null;
            }
        }
    }


    /// <summary>
    /// Update panel where properties control resides.
    /// </summary>
    protected override UpdatePanel PropertiesUpdatePanel
    {
        get
        {
            return pnlUpdateProperties;
        }
    }


    /// <summary>
    /// Gets or sets last searched value.
    /// </summary>
    private string LastSearchedValue
    {
        get
        {
            return hdnLastSearchedValue.Value;
        }
        set
        {
            hdnLastSearchedValue.Value = value;
        }
    }


    /// <summary>
    /// Gets or sets last selected folder path.
    /// </summary>
    private string LastFolderPath
    {
        get
        {
            return hdnLastSelectedPath.Value;
        }
        set
        {
            hdnLastSelectedPath.Value = value;
        }
    }


    /// <summary>
    /// Indicates whether site received from dialog configuration exists.
    /// </summary>
    private bool ApplyDefaultConfig
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["DialogsApplyDefaultConfig"], false);
        }
        set
        {
            ViewState["DialogsApplyDefaultConfig"] = value;
        }
    }


    /// <summary>
    /// Gets or sets selected item to colorize.
    /// </summary>
    private Guid ItemToColorize
    {
        get
        {
            return ValidationHelper.GetGuid(ViewState["ItemToColorize"], Guid.Empty);
        }
        set
        {
            ViewState["ItemToColorize"] = value;
        }
    }


    /// <summary>
    /// Indicates if full listing mode is enabled. This mode enables navigation to child and parent folders/documents from current view.
    /// </summary>
    private bool IsFullListingMode
    {
        get
        {
            return mediaView.IsFullListingMode;
        }
        set
        {
            mediaView.IsFullListingMode = value;
        }
    }


    /// <summary>
    /// Current action name.
    /// </summary>
    private string CurrentAction
    {
        get
        {
            if (mCurrentAction == null)
            {
                mCurrentAction = hdnAction.Value.Trim().ToLower();
            }
            return mCurrentAction;
        }
    }


    /// <summary>
    /// Indicates whether the library has changed recently.
    /// </summary>
    private bool LibraryChanged
    {
        get
        {
            return mLibraryChanged;
        }
        set
        {
            mLibraryChanged = value;
        }
    }


    /// <summary>
    /// Gets or sets direction in which data should be ordered.
    /// </summary>
    private string SortDirection
    {
        get
        {
            return mSortDirection;
        }
        set
        {
            mSortDirection = value;
        }
    }


    /// <summary>
    /// Gets or sets sort columns data are ordered by.
    /// </summary>
    public string SortColumns
    {
        get
        {
            return mSortColumns;
        }
        set
        {
            mSortColumns = value;
        }
    }


    /// <summary>
    /// Gets or sets list of files from the currently selected folder.
    /// </summary>
    private Hashtable FileList
    {
        get
        {
            return mFileList;
        }
        set
        {
            mFileList = value;
        }
    }

    #endregion


    #region "Library properties"

    /// <summary>
    /// Gets or sets a folder path of the media library.
    /// </summary>
    private string FolderPath
    {
        get
        {
            return mFolderPath;
        }
        set
        {
            mFolderPath = (value ?? "");
            LastFolderPath = mFolderPath;
        }
    }


    /// <summary>
    /// Gets or sets an ID of the media library.
    /// </summary>
    private int LibraryID
    {
        get
        {
            return librarySelector.LibraryID;
        }
        set
        {
            librarySelector.LibraryID = value;
            folderTree.MediaLibraryID = value;
            menuElem.LibraryID = value;
            menuElem.UpdateActionsMenu();
            mLibraryInfo = null;
            mediaView.LibraryInfo = null;
            mLibrarySiteInfo = null;
            mMediaLibraryRootFolder = null;
        }
    }


    /// <summary>
    /// Current media library information.
    /// </summary>
    private MediaLibraryInfo LibraryInfo
    {
        get
        {
            if ((mLibraryInfo == null) && (LibraryID > 0))
            {
                mLibraryInfo = MediaLibraryInfoProvider.GetMediaLibraryInfo(LibraryID);
            }
            return mLibraryInfo;
        }
        set
        {
            mLibraryInfo = value;
        }
    }


    /// <summary>
    /// Gets info on site library is related to.
    /// </summary>
    private SiteInfo LibrarySiteInfo
    {
        get
        {
            if ((mLibrarySiteInfo == null) && (LibraryInfo != null))
            {
                mLibrarySiteInfo = SiteInfoProvider.GetSiteInfo(LibraryInfo.LibrarySiteID);
            }
            return mLibrarySiteInfo;
        }
    }


    /// <summary>
    /// Returns media library root folder path.
    /// </summary>
    private string MediaLibraryRootFolder
    {
        get
        {
            if ((mMediaLibraryRootFolder == null) && (LibrarySiteInfo != null))
            {
                mMediaLibraryRootFolder = MediaLibraryHelper.GetMediaRootFolderPath(LibrarySiteInfo.SiteName);
            }
            return mMediaLibraryRootFolder;
        }
    }


    /// <summary>
    /// Gets current starting path if set.
    /// </summary>
    private string StartingPath
    {
        get
        {
            if (Config.LibStartingPath != "")
            {
                string startingPath = Config.LibStartingPath.Replace('\\', '/');
                if (startingPath != "/")
                {
                    startingPath = startingPath.Trim('/') + "/";

                    return MediaLibraryHelper.EnsurePath(startingPath);
                }
            }
            return "";
        }
    }

    #endregion


    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        mediaView.OutputFormat = Config.OutputFormat;
        mediaView.Config = Config;
    }


    protected override void OnPreRender(EventArgs e)
    {
        // High-light item being edited
        if (ItemToColorize != Guid.Empty)
        {
            ColorizeRow(ItemToColorize.ToString());
        }

        // If full-listing mode is on
        ProcessIsFullListingMode();

        if (!wasLoaded && mediaView.Visible)
        {
            LoadData();
        }

        // Make sure properties are hidden for non-existing library
        if (LibraryInfo == null)
        {
            DisplayInfo(ResHelper.GetString("dialogs.libraries.nolibrary"));

            HideMediaElements();
        }

        base.OnPreRender(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!StopProcessing)
        {
            SetupControls();

            SetupProperties();

            InitializeDesignScripts();

            if (!UrlHelper.IsPostback())
            {
                InitializeControls();

                LoadSelector();

                // Is item selected in the editor MediaFile? Load it
                if ((MediaSource != null) && (MediaSource.MediaFileID > 0))
                {
                    LoadSelectedItem();
                }
                else
                {
                    LoadUserConfiguration();
                }

                // Clear properties if link dialog is opened and no link is edited
                bool isLink = (Config.OutputFormat == OutputFormatEnum.BBLink || Config.OutputFormat == OutputFormatEnum.HTMLLink);
                if (isLink && !IsItemLoaded)
                {
                    Properties.ClearProperties(true);
                }
            }
            else
            {
                FolderPath = LastFolderPath;
                mediaView.LibraryID = LibraryID;
                mediaView.ViewMode = menuElem.SelectedViewMode;
            }
        }
        else
        {
            Visible = false;
        }
    }


    #region "Public methods"

    /// <summary>
    /// Initializes its properties according to the URL parameters.
    /// </summary>
    public void InitFromQueryString()
    {
        switch (Config.OutputFormat)
        {
            case OutputFormatEnum.HTMLMedia:
                SelectableContent = SelectableContentEnum.OnlyMedia;
                break;

            case OutputFormatEnum.HTMLLink:
                SelectableContent = SelectableContentEnum.AllContent;
                break;

            case OutputFormatEnum.BBMedia:
                SelectableContent = SelectableContentEnum.OnlyImages;
                break;

            case OutputFormatEnum.BBLink:
                SelectableContent = SelectableContentEnum.AllContent;
                break;

            case OutputFormatEnum.URL:
            case OutputFormatEnum.NodeGUID:
                string content = QueryHelper.GetString("content", "");
                SelectableContent = CMSDialogHelper.GetSelectableContent(content);
                break;
        }
    }


    /// <summary>
    /// Returns selected item parameters as name-value collection.
    /// </summary>
    public void GetSelectedItem()
    {
        // Clear unused information from the session
        ClearSelectedItemInfo();

        if (Properties.Validate())
        {
            // Store tab information in the user's dialogs configuration
            StoreDialogsConfiguration();

            Hashtable properties = Properties.GetItemProperties();

            // Get JavaScript for inserting the item
            string insertItemScript = GetInsertItem(properties);
            if (!string.IsNullOrEmpty(insertItemScript))
            {
                ScriptHelper.RegisterStartupScript(Page, typeof(Page), "insertItemScript", ScriptHelper.GetScript(insertItemScript));
            }
        }
        else
        {
            pnlUpdateProperties.Update();
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Loads libraries according dialogs configuration.
    /// </summary>
    private void LoadLibraries()
    {
        librarySelector.LoadLibrarySelection();
    }


    /// <summary>
    /// Loads selector element.
    /// </summary>
    private void LoadSelector()
    {
        librarySelector.LoadData();
    }


    /// <summary>
    /// Initializes properties controls.
    /// </summary>
    private void SetupProperties()
    {
        htmlLinkProp.Visible = false;
        htmlMediaProp.Visible = false;
        bbLinkProp.Visible = false;
        bbMediaProp.Visible = false;
        urlProp.Visible = false;

        if (Properties != null)
        {
            Properties.Visible = true;
        }

        htmlLinkProp.StopProcessing = !htmlLinkProp.Visible;
        htmlMediaProp.StopProcessing = !htmlMediaProp.Visible;
        bbLinkProp.StopProcessing = !bbLinkProp.Visible;
        bbMediaProp.StopProcessing = !bbMediaProp.Visible;
        urlProp.StopProcessing = !urlProp.Visible;

        Properties.Config = Config;
    }


    /// <summary>
    /// Initializes additional controls
    /// </summary>
    private void SetupControls()
    {
        SourceType = MediaSourceEnum.MediaLibraries;
        htmlMediaProp.SourceType = MediaSourceEnum.MediaLibraries;
        bbMediaProp.SourceType = MediaSourceEnum.MediaLibraries;
        urlProp.SourceType = MediaSourceEnum.MediaLibraries;

        // Set editor client ID for the properties
        Properties.EditorClientID = Config.EditorClientID;
        Properties.SourceType = MediaSourceEnum.MediaLibraries;

        // Setup library selector
        InitializeLibrarySelector();

        // Set menu properties
        InitializeMenuElem();

        // Set media view properties
        InitializeViewElem();

        // Initialize helper scripts
        InitializeControlScripts();
    }


    /// <summary>
    /// Performs actions necessary to select particular item from a list.
    /// </summary>
    private void SelectMediaItem(string argument)
    {
        if (!string.IsNullOrEmpty(argument))
        {
            string[] argArr = argument.Split('|');
            if (argArr.Length >= 2)
            {

                Guid fileGuid = ValidationHelper.GetGuid(argArr[0], Guid.Empty);

                // Do not update properties when selecting recently edited image item
                bool avoidPropUpdate = (IsEditImage && (ItemToColorize == fileGuid));

                ItemToColorize = fileGuid;

                // Get information from argument
                string fileName = argArr[1];
                string fileExt = argArr[3];
                int imageWidth = ValidationHelper.GetInteger(argArr[4], 0);
                int imageHeight = ValidationHelper.GetInteger(argArr[5], 0);
                long fileSize = ValidationHelper.GetLong(argArr[7], 0);
                string fileUrl = argArr[argArr.Length - 1];

                fileName = UsePermanentUrls ? AttachmentHelper.GetFullFileName(fileName, fileExt) : fileName;

                string filePermanentUrl = (LibrarySiteInfo.SiteID != CMSContext.CurrentSiteID) ? MediaFileInfoProvider.GetMediaFileAbsoluteUrl(LibrarySiteInfo.SiteName, fileGuid, fileName) : MediaFileInfoProvider.GetMediaFileUrl(fileGuid, fileName);
                if (!fileUrl.StartsWith("~"))
                {
                    filePermanentUrl = UrlHelper.ResolveUrl(filePermanentUrl);
                }

                Properties.SiteDomainName = LibrarySiteInfo.DomainName;

                if (!avoidPropUpdate)
                {
                    SelectMediaItem(fileName, fileExt, imageWidth, imageHeight, fileSize, fileUrl, filePermanentUrl);
                }
            }
        }
    }

    #endregion


    #region "Helper methods"

    /// <summary>
    /// Returns file path without library root folder.
    /// </summary>
    /// <param name="argument">Original file path.</param>
    private string GetFilePath(string argument)
    {
        if (!string.IsNullOrEmpty(argument))
        {
            argument = argument.Replace('\\', '/');

            int rootFolderNameIndex = argument.IndexOf('/');

            argument = (rootFolderNameIndex > -1) ? argument.Substring(rootFolderNameIndex) : "";

            return argument.TrimStart('/');
        }

        return "";
    }


    /// <summary>
    /// Returns folder path based on the given file path.
    /// </summary>
    /// <param name="filePath">File path.</param>
    private string GetFolderPath(string filePath)
    {
        if (!string.IsNullOrEmpty(filePath))
        {
            int lastSlashIndex = filePath.LastIndexOf('/');

            return (lastSlashIndex > -1) ? filePath.Substring(0, lastSlashIndex) : "";
        }

        return "";
    }


    /// <summary>
    /// Returns complete folder path including library folder.
    /// </summary>
    /// <param name="path">Folder path.</param
    private string GetCompletePath(string path)
    {
        // Include starting path if used
        if (StartingPath != "")
        {
            path = StartingPath + path;
        }

        path = (LibraryInfo != null) ? LibraryInfo.LibraryFolder + '/' + path : path;

        return path.TrimEnd('/');
    }


    /// <summary>
    /// Ensures folder path comming from JavaScript to keep normalized form.
    /// </summary>
    /// <param name="path">Path to ensure.</param>
    private string EnsureFolderPath(string path)
    {
        char separator = folderTree.PathSeparator;

        return (!string.IsNullOrEmpty(path)) ? path.Replace('/', separator).Replace("\\\\", separator.ToString()).TrimStart(separator) : "";
    }


    /// <summary>
    /// Ensures that the text can be used in the WHERE condition.
    /// </summary>
    /// <param name="text">Text to ensure.</param>
    private string NormalizeForSql(string text)
    {
        if (!String.IsNullOrEmpty(text) && text.Contains("%"))
        {
            text = text.Replace("%", PERCENT_ESC_CHAR + "%");
        }

        return text;
    }


    /// <summary>
    /// Ensures that menu element updates its information.
    /// </summary>
    private void EnsureMenuInfo()
    {
        string updatedFolderDialog = UrlHelper.UpdateParameterInUrl(menuElem.NewFolderDialogUrl, "libraryid", menuElem.LibraryID.ToString());
        updatedFolderDialog = UrlHelper.UpdateParameterInUrl(updatedFolderDialog, "path", Server.UrlEncode(menuElem.LibraryFolderPath).Replace("'", "\\'"));

        string script = "$j(\"td[id$='menuLeft-000']\").attr('onclick', '').click(function () { modalDialog('" + UrlHelper.ResolveUrl(updatedFolderDialog) + "', 'New folder', 450, 200); return false; });";

        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "DialogsEnsureMenuInfo", ScriptHelper.GetScript(script));
    }


    /// <summary>
    /// Clears hidden control elements for the future use.
    /// </summary>
    private void ClearActionElems()
    {
        hdnAction.Value = "";
        hdnArgument.Value = "";
    }


    /// <summary>
    /// Hides or displays media elements as required.
    /// </summary>
    /// <param name="isDisplayed">Indicates whether the elements should display content.</param>
    private void HandleMediaElements(bool isDisplayed)
    {
        // Folder tree
        folderTree.StopProcessing = !isDisplayed;
        // Reload tree and hide it if not displayed (Ajax toolkit bug)
        if (!isDisplayed)
        {
            folderTree.ReloadData();
            pnlTree.CssClass = "Hidden";
        }
        else
        {
            pnlTree.CssClass = "";
        }
        lblNoLibraries.Visible = !isDisplayed;
        pnlUpdateTree.Update();

        // Media view
        mediaView.StopProcessing = !isDisplayed;
        mediaView.Visible = isDisplayed;
        pnlUpdateView.Update();

        // Properties
        Properties.StopProcessing = !isDisplayed;
        Properties.Visible = isDisplayed;
        pnlUpdateProperties.Update();

        lblEmpty.Text = CMSDialogHelper.GetSelectItemMessage(Config, MediaSourceEnum.MediaLibraries);
        pnlEmpty.Visible = !isDisplayed;
    }


    /// <summary>
    /// Hides media elements.
    /// </summary>
    private void HideMediaElements()
    {
        HandleMediaElements(false);
    }


    /// <summary>
    /// Displays media elements.
    /// </summary>
    private void DisplayMediaElements()
    {
        HandleMediaElements(true);
    }


    /// <summary>
    /// Displays properties in full size.
    /// </summary>
    private void DisplayFull()
    {
        // Change CSS class so properties are displayed in full size
        if (divDialogView.Attributes["class"] == "DialogViewContent")
        {
            divDialogView.Attributes["class"] = "DialogElementHidden";
            divDialogResizer.Attributes["class"] = "DialogElementHidden";
            divDialogProperties.Attributes["class"] = "DialogPropertiesFullSize";

            pnlUpdateContent.Update();
        }
    }


    /// <summary>
    /// Displays properties in default size.
    /// </summary>
    private void DisplayNormal()
    {
        // Change CSS class so properties are displayed in normal size
        if (divDialogView.Attributes["class"] == "DialogElementHidden")
        {
            divDialogView.Attributes["class"] = "DialogViewContent";
            divDialogResizer.Attributes["class"] = "DialogResizerVLine";
            divDialogProperties.Attributes["class"] = "DialogProperties";

            pnlUpdateContent.Update();
        }
    }


    /// <summary>
    /// Displays error message.
    /// </summary>
    /// <param name="err">Text of the error message.</param>
    private void DisplayError(string err)
    {
        lblInfo.Visible = false;

        lblError.Text = err;
        lblError.Visible = true;
        pnlInfo.Visible = true;

        HideMediaElements();
    }


    /// <summary>
    /// Displays info message.
    /// </summary>
    /// <param name="msg">Message to display.</param>
    private void DisplayInfo(string msg)
    {
        lblError.Visible = false;

        lblInfo.Text = msg;
        lblInfo.Visible = true;
        pnlInfo.Visible = true;
    }


    /// <summary>
    /// Ensures that filter is no more applied.
    /// </summary>
    private void ResetSearchFilter()
    {
        mediaView.ResetSearch();
        LastSearchedValue = "";
    }


    /// <summary>
    /// Ensures first page is displayed in the control displaying the content.
    /// </summary>
    private void ResetPageIndex()
    {
        mediaView.ResetPageIndex();
    }


    /// <summary>
    /// Returns full file path including library folder.
    /// </summary>
    /// <param name="path">File path to get full path for.</param>
    private string GetFullFilePath(string path)
    {
        if (path != null)
        {
            return (LibraryInfo.LibraryFolder + '/' + path).TrimEnd('/');
        }

        return "";
    }


    /// <summary>
    /// Gets folder path of the parent of the folder specified by its path.
    /// </summary>
    /// <param name="path">Path of the folder.</param
    private string GetParentFullPath(string path)
    {
        return GetParentFullPath(path, true);
    }


    /// <summary>
    /// Gets folder path of the parent of the folder specified by its path.
    /// </summary>
    /// <param name="path">Path of the folder.</param
    private string GetParentFullPath(string path, bool includeRoot)
    {
        if (!string.IsNullOrEmpty(path))
        {
            path = MediaLibraryHelper.EnsurePath(path);

            int lastSlash = path.LastIndexOf('/');
            if (lastSlash > -1)
            {
                path = path.Substring(0, lastSlash).Trim('/');
            }
            else
            {
                path = "";
            }

            if (includeRoot && (LibraryInfo != null))
            {
                path = LibraryInfo.LibraryFolder + '/' + path;
            }
        }

        return path.TrimEnd('/');
    }


    /// <summary>
    /// Ensures sorting of data set containing both files and folders.
    /// </summary>
    /// <param name="ds">DateSet to sort.</param>
    /// <param name="direction">Direction in which data should be sorted.</param>
    /// <param name="expression">Expresion used to sort the data.</param>
    private static void SortMixedDataSet(DataSet ds, string direction, string expression)
    {
        if ((ds != null) && !string.IsNullOrEmpty(direction) && !string.IsNullOrEmpty(expression) && !DataHelper.IsEmpty(ds))
        {
            string orderBy = expression.Trim() + " " + direction;

            DataHelper.SortDataTable(ds.Tables[0], orderBy);
            DataTable sortedResult = ds.Tables[0].DefaultView.ToTable();
            ds.Tables.RemoveAt(0);
            ds.Tables.Add(sortedResult);
        }
    }


    /// <summary>
    /// Imports folder details into given set of data.
    /// </summary>
    /// <param name="ds">DataSet folders information should be imported to.</param>
    private void IncludeFolders(DataSet ds, string searchText)
    {
        // Data object specified
        if ((ds != null) && (ds.Tables[0] != null))
        {
            // Get path to the currently selected folder
            string dirPath = MediaLibraryRootFolder.TrimEnd('\\') + "\\" + LibraryInfo.LibraryFolder + ((this.StartingPath != "") ? "\\" + this.StartingPath : "") + ((LastFolderPath != "") ? "\\" + LastFolderPath : "");
            dirPath = dirPath.TrimEnd('/').Replace('/', '\\');
            if (Directory.Exists(dirPath))
            {
                // Get directories in the current path
                string[] dirs = Directory.GetDirectories(dirPath);
                if (dirs != null)
                {
                    int lastFolderIndex = 0;

                    string hiddenFolder = MediaLibraryHelper.GetMediaFileHiddenFolder(LibrarySiteInfo.SiteName);
                    foreach (string folderPath in dirs)
                    {
                        if (!folderPath.EndsWith(hiddenFolder))
                        {
                            // Get directory info object to access additional information
                            DirectoryInfo dirInfo = new DirectoryInfo(folderPath);
                            if (dirInfo != null)
                            {
                                DataRow dirRow = ds.Tables[0].NewRow();

                                bool includeFolder = true;
                                string fileName = Path.GetFileName(dirInfo.FullName);
                                if (!string.IsNullOrEmpty(searchText) && !fileName.Contains(searchText))
                                {
                                    includeFolder = false;
                                }

                                // Insert new row
                                if (includeFolder)
                                {
                                    // Fill new row with data
                                    dirRow["FileGuid"] = Guid.Empty;
                                    dirRow["FileName"] = Path.GetFileName(dirInfo.FullName);
                                    dirRow["FilePath"] = dirPath;
                                    dirRow["FileExtension"] = "<dir>";
                                    dirRow["FileImageWidth"] = 0;
                                    dirRow["FileImageHeight"] = 0;
                                    dirRow["FileTitle"] = "";
                                    dirRow["FileSize"] = 0;
                                    dirRow["FileModifiedWhen"] = dirInfo.LastWriteTime;
                                    dirRow["FileSiteID"] = LibrarySiteInfo.SiteID;
                                    dirRow["FileID"] = 0;

                                    ds.Tables[0].Rows.InsertAt(dirRow, lastFolderIndex);

                                    lastFolderIndex++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    /// <summary>
    /// Gets file path based on its file name, recently selected folder path and library folder.
    /// </summary>
    /// <param name="fileName">Name of the file (including extension).</param>
    private string CreateFilePath(string fileName)
    {
        string filePath = "";
        if (String.IsNullOrEmpty(LastFolderPath))
        {
            // If root folder of library
            filePath = fileName;
        }
        else
        {
            filePath = LastFolderPath + "\\" + fileName;
        }

        return filePath;
    }


    /// <summary>
    /// Returns true if file information is not in database.
    /// </summary>
    /// <param name="fileName">File name</param>
    private DataRow FileIsNotInDatabase(string fileName)
    {
        if (FileList == null)
        {
            string where = "FilePath LIKE N'" + MediaLibraryHelper.EnsurePath(LastFolderPath.Replace("'", "''").Replace("[", "[[]").Replace("%", "[%]")).Trim('/') +
                "%' AND FilePath NOT LIKE N'" + MediaLibraryHelper.EnsurePath(LastFolderPath.Replace("'", "''").Replace("[", "[[]").Replace("%", "[%]")).Trim('/') +
                "_%/%' AND FileLibraryID = " + LibraryID;

            if (!string.IsNullOrEmpty(LastSearchedValue))
            {
                where += " AND ((FileName LIKE N'%" + LastSearchedValue.Replace("'", "''") + "%' {escape '" + PERCENT_ESC_CHAR + "'})  OR (FileExtension LIKE N'%" + LastSearchedValue.Replace("'", "''") + "%' {escape '" + PERCENT_ESC_CHAR + "'}))";
            }

            string columns = "FileID, FilePath, FileGUID, FileName, FileExtension, FileImageWidth, FileImageHeight, FileTitle, FileSize, FileLibraryID, FileSiteID";

            // Get all files from current folder
            DataSet ds = MediaFileInfoProvider.GetMediaFiles(where, "FileName", 0, columns);
            if (ds != null)
            {
                FileList = new Hashtable();
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    FileList[row["FilePath"].ToString()] = row;
                }
            }
        }

        if (FileList != null)
        {
            if (String.IsNullOrEmpty(LastFolderPath))
            {
                if (FileList.Contains(fileName))
                {
                    return (FileList[fileName] as DataRow);
                }
            }
            else
            {
                string filePath = MediaLibraryHelper.EnsurePath(LastFolderPath).Trim('/') + "/" + fileName;
                if (FileList.Contains(filePath))
                {
                    return (FileList[filePath] as DataRow);
                }
            }
        }

        return null;
    }

    #endregion


    #region "Dialog configuration"

    /// <summary>
    /// Loads selected item parameters into the selector.
    /// </summary>
    public void LoadSelectedItem()
    {
        if (MediaSource != null)
        {
            IsItemLoaded = true;

            // Try to pre-select media library
            if ((MediaSource.MediaFileLibraryID > 0) && (LibraryInfo != null))
            {
                librarySelector.SelectedLibraryID = MediaSource.MediaFileLibraryID;

                if (MediaSource.MediaFileLibraryGroupID > 0)
                {
                    librarySelector.SelectedGroupID = MediaSource.MediaFileLibraryGroupID;
                    librarySelector.GroupLibraryName = LibraryInfo.LibraryName;
                }
                else
                {
                    librarySelector.GlobalLibaryName = LibraryInfo.LibraryName;
                }
            }

            // Try to pre-select path
            if (!string.IsNullOrEmpty(MediaSource.MediaFilePath))
            {
                // Without library root
                FolderPath = GetFolderPath(MediaSource.MediaFilePath);

                if (StartingPath == "")
                {
                    FolderPath = GetCompletePath(FolderPath);
                }
                else
                {
                    folderTree.PathToSelect = FolderPath;

                    folderTree.StopProcessing = true;

                    // With library root
                    FolderPath = GetFullFilePath(FolderPath);
                }

                RequestStockHelper.Add("FolderPath", FolderPath);
            }

            // Reload HTML properties
            if (Config.OutputFormat == OutputFormatEnum.HTMLMedia)
            {
                // Force media properties control to load selected item
                htmlMediaProp.ViewMode = MediaSource.MediaType;
            }
        }

        // Ensure inserted media file URL
        string url = "";
        if (Config.OutputFormat == OutputFormatEnum.URL)
        {
            url = ValidationHelper.GetString(Parameters[DialogParameters.URL_URL], "");
        }
        else if (ImageHelper.IsImage(MediaSource.Extension))
        {
            url = ValidationHelper.GetString(Parameters[DialogParameters.IMG_URL], "");
        }
        else
        {
            url = ValidationHelper.GetString(Parameters[DialogParameters.AV_URL], "");
        }
        // Get permanent URL for media file
        if (url != "")
        {
            bool isDifferentSite = (MediaSource.SiteID != CMSContext.CurrentSiteID);
            string siteName = LibrarySiteInfo.SiteName;
            string fileName = UsePermanentUrls ? AttachmentHelper.GetFullFileName(MediaSource.FileName, MediaSource.Extension) : MediaSource.FileName;

            string filePermanentUrl = isDifferentSite ? MediaFileInfoProvider.GetMediaFileAbsoluteUrl(siteName, MediaSource.MediaFileGuid, fileName) : MediaFileInfoProvider.GetMediaFileUrl(MediaSource.MediaFileGuid, fileName);
            string fileDirectUrl = isDifferentSite ? MediaFileInfoProvider.GetMediaFileAbsoluteUrl(siteName, LibraryInfo.LibraryFolder, MediaSource.MediaFilePath) : MediaFileInfoProvider.GetMediaFileUrl(siteName, LibraryInfo.LibraryFolder, MediaSource.MediaFilePath);

            Parameters[DialogParameters.URL_PERMANENT] = UrlHelper.ResolveUrl(filePermanentUrl);
            Parameters[DialogParameters.URL_DIRECT] = UrlHelper.ResolveUrl(fileDirectUrl);
        }

        // Load properties
        Properties.LoadItemProperties(Parameters);
        pnlUpdateProperties.Update();

        // Remember item being edited for later high-lighting
        ItemToColorize = MediaSource.MediaFileGuid;

        ClearSelectedItemInfo();

        // Display properties in full size
        DisplayFull();
    }


    /// <summary>
    /// Stores current tab's configuration for the user.
    /// </summary>
    private void StoreDialogsConfiguration()
    {
        string path = GetCompletePath(LastFolderPath);

        // Actualize configuration
        UserInfo ui = CMSContext.CurrentUser;

        ui.UserSettings.UserDialogsConfiguration["media.sitename"] = LibrarySiteInfo.SiteName;
        ui.UserSettings.UserDialogsConfiguration["media.libraryname"] = LibraryInfo.LibraryName;
        ui.UserSettings.UserDialogsConfiguration["media.path"] = path;
        ui.UserSettings.UserDialogsConfiguration["media.viewmode"] = CMSDialogHelper.GetDialogViewMode(menuElem.SelectedViewMode);

        ui.UserSettings.UserDialogsConfiguration["selectedtab"] = CMSDialogHelper.GetMediaSource(MediaSourceEnum.MediaLibraries);

        // Update user info
        UserInfoProvider.SetUserInfo(ui);
    }


    /// <summary>
    /// Loads dialogs according user's configuration.
    /// </summary>
    private void LoadUserConfiguration()
    {
        if (CMSContext.CurrentUser.UserSettings.UserDialogsConfiguration != null)
        {
            XmlData dialogConfig = CMSContext.CurrentUser.UserSettings.UserDialogsConfiguration;

            string libraryName = (dialogConfig.ContainsColumn("media.libraryname") ? dialogConfig["media.libraryname"] : "");
            string path = (dialogConfig.ContainsColumn("media.path") ? dialogConfig["media.path"] : "");
            string siteName = (dialogConfig.ContainsColumn("media.sitename") ? dialogConfig["media.sitename"] : "");

            if ((libraryName != "") && (siteName != ""))
            {
                MediaLibraryInfo mli = MediaLibraryInfoProvider.GetMediaLibraryInfo(libraryName, siteName);
                if (mli != null)
                {
                    librarySelector.SelectedSiteName = siteName;
                    librarySelector.SelectedLibraryID = mli.LibraryID;
                    librarySelector.SelectedGroupID = mli.LibraryGroupID;
                }
            }

            if (path != "")
            {
                FolderPath = path;

                if (this.StartingPath != "")
                {
                    path = GetFilePath(path);
                }

                folderTree.PathToSelect = path;

                RequestStockHelper.Add("FolderPath", FolderPath);
            }
        }
    }


    /// <summary>
    /// Ensures that full-listing mode is displayed when necessary.
    /// </summary>
    private void ProcessIsFullListingMode()
    {
        bool rootHasMore = (LastFolderPath == "") && ((CurrentAction == "select") || ((!wasLoaded || LibraryChanged) && (CurrentAction == ""))) && folderTree.RootHasMore;
        if (IsFullListingMode || rootHasMore)
        {
            IsFullListingMode = (rootHasMore ? true : IsFullListingMode);

            string folderPath = this.LastFolderPath;
            if (this.StartingPath != "")
            {
                folderPath = this.StartingPath + folderPath;
                folderPath = GetFullFilePath(folderPath);
            }

            // Check path of the edited item
            if (!UrlHelper.IsPostback())
            {
                string editedPath = ValidationHelper.GetString(RequestStockHelper.GetItem("FolderPath"), "");
                folderPath = (editedPath != "") ? editedPath : folderPath;
            }

            string closeLink = "<span class=\"ListingClose\" style=\"cursor: pointer;\" onclick=\"SetAction('closelisting', ''); RaiseHiddenPostBack(); return false;\">" + ResHelper.GetString("general.close") + "</span>";
            string docNamePath = "<span class=\"ListingPath\">" + folderPath.Replace('\\', '/') + "</span>";

            string listingMsg = string.Format(ResHelper.GetString("media.libraryui.listingInfo"), docNamePath, closeLink);
            mediaView.DisplayListingInfo(listingMsg);
        }
        mediaView.ShowParentButton = (IsFullListingMode && (GetCompletePath(this.LastFolderPath) != GetFullFilePath(this.StartingPath)));
    }

    #endregion


    #region "Initialization methods"

    /// <summary>
    /// Initializes controls.
    /// </summary>
    private void InitializeControls()
    {
        ViewMode = menuElem.SelectedViewMode;

        // View mode obtained from the user's settings
        XmlData dialogConfig = CMSContext.CurrentUser.UserSettings.UserDialogsConfiguration;
        if (dialogConfig != null)
        {
            // Get user's view mode
            string viewMode = (dialogConfig.ContainsColumn("media.viewmode") ? dialogConfig["media.viewmode"] : "");
            if (viewMode != "")
            {
                ViewMode = CMSDialogHelper.GetDialogViewMode(viewMode);
            }
        }

        // Select default site
        SelectSite();

        mediaView.ViewMode = ViewMode;
        menuElem.SelectedViewMode = ViewMode;
    }


    /// <summary>
    /// Selects site based on according dialogs configuration.
    /// </summary>
    private void SelectSite()
    {
        // Current site by default
        string siteName = CMSContext.CurrentSiteName;

        // Select site based on dialog configuration
        if (!string.IsNullOrEmpty(Config.LibSelectedSite))
        {
            siteName = Config.LibSelectedSite;
        }
        if (Config.LibSites != AvailableSitesEnum.All)
        {
            librarySelector.SelectedSiteName = siteName;
        }

        // Select site based on selected item 
        if ((MediaSource != null) && (MediaSource.MediaFileLibraryID > 0))
        {
            LibraryInfo = MediaLibraryInfoProvider.GetMediaLibraryInfo(MediaSource.MediaFileLibraryID);
            if (LibraryInfo != null)
            {
                siteName = SiteInfoProvider.GetSiteInfo(LibraryInfo.LibrarySiteID).SiteName;
            }
        }
        // Select site based on user's configuration
        else
        {
            // Select side based on the user's configuration
            if (CMSContext.CurrentUser.UserSettings.UserDialogsConfiguration != null)
            {
                XmlData dialogConfig = CMSContext.CurrentUser.UserSettings.UserDialogsConfiguration;

                // Get site name
                string usersSiteName = (dialogConfig.ContainsColumn("media.sitename") ? dialogConfig["media.sitename"] : "");
                if (usersSiteName != "")
                {
                    siteName = usersSiteName;
                }
            }
        }

        // Apply previously obtained sitename only when all sites are available
        if (Config.LibSites == AvailableSitesEnum.All)
        {
            librarySelector.SelectedSiteName = siteName;
        }
    }


    /// <summary>
    /// Applies default dialog configuration for the selector.
    /// </summary>
    private void SetDefaultConfig()
    {
        librarySelector.Groups = AvailableGroupsEnum.All;
        librarySelector.GroupLibraries = AvailableLibrariesEnum.All;
        librarySelector.GlobalLibraries = AvailableLibrariesEnum.All;
    }


    /// <summary>
    /// Initializes view element.
    /// </summary>
    private void InitializeViewElem()
    {
        mediaView.LibraryID = LibraryID;
        mediaView.IsLiveSite = IsLiveSite;

        // Generate permanent URLs whenever node GUID output required        
        if (Config.OutputFormat != OutputFormatEnum.NodeGUID)
        {
            UsePermanentUrls = SettingsKeyProvider.GetBoolValue(CMSContext.CurrentSiteName + ".CMSMediaUsePermanentURLs");
        }
        mediaView.UsePermanentUrls = UsePermanentUrls;

        mediaView.ListViewControl.OnBeforeSorting += ListViewControl_OnBeforeSorting;
        mediaView.ListReloadRequired += mediaView_ListReloadRequired;
        mediaView.ListViewControl.DataSourceIsSorted = true;
        mediaView.GetInformation += new CMSModules_MediaLibrary_Controls_Dialogs_MediaView.OnGetInformation(mediaView_GetInformation);

        // Set media properties
        mediaView.SelectableContent = SelectableContent;
        mediaView.SourceType = SourceType;
        mediaView.ViewMode = menuElem.SelectedViewMode;
        pnlInfo.Visible = false;

        // Set autoresize parameters
        mediaView.ResizeToHeight = Config.ResizeToHeight;
        mediaView.ResizeToMaxSideSize = Config.ResizeToMaxSideSize;
        mediaView.ResizeToWidth = Config.ResizeToWidth;

        // If folder was changed reset current page index for control displaying content
        switch (this.CurrentAction)
        {
            case "folderselect":
            case "clickformorefolder":
            case "morefolderselect":
            case "parentselect":
            case "clickformorelink":
                ResetPageIndex();
                break;

            default:
                break;
        }
    }


    /// <summary>
    /// Initializes menu element.
    /// </summary>
    private void InitializeMenuElem()
    {
        menuElem.LibraryFolderPath = (StartingPath + this.LastFolderPath).Trim('/');

        menuElem.ResizeToHeight = Config.ResizeToHeight;
        menuElem.ResizeToMaxSideSize = Config.ResizeToMaxSideSize;
        menuElem.ResizeToWidth = Config.ResizeToWidth;

        menuElem.DisplayMode = DisplayMode;
        menuElem.IsLiveSite = IsLiveSite;
        menuElem.SourceType = SourceType;
        menuElem.LibraryID = LibraryID;

        menuElem.AllowFullscreen = false;
        menuElem.UpdateViewMenu();
    }


    /// <summary>
    /// Initialize design jQuery scripts
    /// </summary>
    private void InitializeDesignScripts()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("setTimeout('InitializeDesign();',200);");
        sb.Append("$j(window).resize(function() { InitializeDesign(); });");
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "designScript", ScriptHelper.GetScript(sb.ToString()));
    }


    /// <summary>
    /// Loads folder tree.
    /// </summary>
    private void InitializeTree()
    {
        // Initialize folder tree control
        folderTree.CustomSelectFunction = "SetAction('folderselect', '##NODEVALUE##'); RaiseHiddenPostBack();";
        folderTree.CustomClickForMoreFunction = "SetAction('clickformore##TYPE##', '##NODEVALUE##'); SetLastViewAction('list'); RaiseHiddenPostBack();";

        // Set starting path
        if (!string.IsNullOrEmpty(StartingPath))
        {
            folderTree.RootFolderPath = MediaLibraryRootFolder.TrimEnd('\\');
            folderTree.MediaLibraryFolder = Path.GetFileNameWithoutExtension(StartingPath.Trim('/'));
            folderTree.MediaLibraryPath = EnsureFolderPath(LibraryInfo.LibraryFolder + "\\" + StartingPath.Trim('/'));
        }
        else
        {
            folderTree.RootFolderPath = MediaLibraryRootFolder;
            folderTree.MediaLibraryFolder = LibraryInfo.LibraryFolder;
        }

        folderTree.ReloadData();
        pnlUpdateTree.Update();
    }


    /// <summary>
    /// Initializes library selector based on dialog configuration.
    /// </summary>
    private void InitializeLibrarySelector()
    {
        librarySelector.IsLiveSite = IsLiveSite;

        // Sites
        librarySelector.Sites = Config.LibSites;

        // Groups
        librarySelector.Groups = Config.LibGroups;
        librarySelector.GroupName = Config.LibGroupName;
        librarySelector.GroupLibraries = Config.LibGroupLibraries;
        librarySelector.GroupLibraryName = Config.LibGroupLibraryName;

        // Libraries
        librarySelector.GlobalLibraries = Config.LibGlobalLibraries;
        librarySelector.GlobalLibaryName = Config.LibGlobalLibraryName;
    }


    /// <summary>
    /// Initializes all the script required for communication between controls.
    /// </summary>
    private void InitializeControlScripts()
    {
        // SetAction function setting action name and passed argument
        string setAction = "function SetAction(action, argument) {                                              " +
                           "    var hdnAction = document.getElementById('" + hdnAction.ClientID + "');     " +
                           "    var hdnArgument = document.getElementById('" + hdnArgument.ClientID + "'); " +
                           "    if ((hdnAction != null) && (hdnArgument != null)) {                             " +
                           "        if (action != null) {                                                       " +
                           "            hdnAction.value = action;                                               " +
                           "        }                                                                           " +
                           "        if (argument != null) {                                                     " +
                           "            hdnArgument.value = argument;                                           " +
                           "        }                                                                           " +
                           "    }                                                                               " +
                           "}                                                                                   ";

        // Get reffernce causing postback to hidden button
        string postBackRef = ControlsHelper.GetPostBackEventReference(hdnButton, "");

        // RaiseOnAction function causing postback to the hidden button
        string raiseOnAction = "function RaiseHiddenPostBack(){" + postBackRef + ";}";

        // Prepare for upload
        string refreshType = CMSDialogHelper.GetMediaSource(MediaSourceEnum.MediaLibraries);

        string initRefresh = @" function InitRefresh_" + refreshType + "(message, fullRefresh, itemInfo, action) {      " +
                             @"  if((message != null) && (message != ''))                                   
                                     {                                                                          
                                        window.alert(message);                                                  
                                     }                                                                          
                                     else                                                                       
                                     {                                                                          
                                        SetAction('libraryfilecreated', itemInfo);                                 
                                        RaiseHiddenPostBack();                                                  
                                     }                                                                          
                                   }                                                                            ";

        setAction += initRefresh;

        string imgEditRefresh = @" function imageEdit_Refresh(guid){
                                    SetAction('edit', guid);                                                    
                                    RaiseHiddenPostBack();
                                }                                                                                       ";

        setAction += imgEditRefresh;

        ltlScript.Text = ScriptHelper.GetScript(setAction + raiseOnAction);
    }

    #endregion


    #region "Load object methods"

    /// <summary>
    /// Loads library applying current library infromation.
    /// </summary>
    private void SelectLibrary()
    {
        LibraryID = librarySelector.LibraryID;
        if (LibraryID > 0)
        {
            // Reload data using new information
            DisplayMediaElements();

            // Initialize tree
            InitializeTree();

            // Reload view element
            InitializeViewElem();
        }
    }


    /// <summary>
    /// Ensures that given path is selected when available in the tree.
    /// </summary>
    /// <param name="path">Path to select.</param>
    /// <param name="forceReload">Indicates if the reload on folder tree should be called explicitly.</param>
    private void SelectTreePath(string path, bool forceReload)
    {
        folderTree.PathToSelect = path;

        if (forceReload)
        {
            InitializeTree();
        }
    }


    /// <summary>
    /// Checks permissions for the current user taking specified action.
    /// </summary>
    private string CheckPermissions()
    {
        if (!MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, PERMISSION_READ) &&
            !MediaLibraryInfoProvider.IsUserAuthorizedPerLibrary(LibraryInfo, "LibraryAccess"))
        {
            return ResHelper.GetString("media.security.noaccess");
        }

        return "";
    }


    /// <summary>
    /// Loads all files for the view control.
    /// </summary>
    private void LoadData()
    {
        LoadData(false);
    }


    /// <summary>
    /// Loads all files for the view control.
    /// </summary>
    /// 
    private void LoadData(bool forceSetup)
    {
        mediaView.StopProcessing = false;

        LoadDataSource(LastSearchedValue);
        mediaView.Reload(forceSetup);

        wasLoaded = true;
    }


    /// <summary>
    /// Loads all files for the view control.
    /// </summary>
    /// <param name="searchText">Text to filter loaded files.</param>
    private void LoadDataSource(string searchText)
    {
        // Load media files data
        if ((FolderPath != null) && (LibraryID > 0))
        {
            string err = CheckPermissions();
            if (err == "")
            {
                string normFolderPath = NormalizeForSql(this.FolderPath);
                string filePath = StartingPath + MediaLibraryHelper.EnsurePath(normFolderPath).Trim('/').Replace("'", "''");
                // Create WHERE condition
                string where = "FilePath LIKE '" + filePath +
                    "%' {escape '" + PERCENT_ESC_CHAR + "'} AND FilePath NOT LIKE '" + filePath +
                    "_%/%' {escape '" + PERCENT_ESC_CHAR + "'} AND FileLibraryID = " + LibraryID;

                if (!string.IsNullOrEmpty(searchText))
                {
                    searchText = NormalizeForSql(searchText);

                    where += " AND ((FileName LIKE N'%" + searchText.Replace("'", "''") + "%' {escape '" + PERCENT_ESC_CHAR + "'})  OR (FileExtension LIKE N'%" + searchText.Replace("'", "''") + "%' {escape '" + PERCENT_ESC_CHAR + "'}))";
                }

                string columns = "FileGUID, FileName, FilePath, FileExtension, FileImageWidth, FileImageHeight, FileTitle, FileSize, FileModifiedWhen, FileSiteID, FileID";
                int topN = mediaView.CurrentTopN;

                DataSet result = MediaFileInfoProvider.GetMediaFiles(where, null, topN, columns);

                // Sort DataSet containing folders as well as files
                SortMixedDataSet(result, SortDirection, SortColumns);

                // If folders should be displayed as well include them in the DataSet
                if (IsFullListingMode)
                {
                    // Add folder
                    IncludeFolders(result, searchText);
                }

                // Get all files from current folder and pass it to the media view control
                mediaView.DataSource = result;
            }
            else
            {
                lblNoLibraries.ResourceString = "";
                lblNoLibraries.CssClass = "ErrorLabel";
                lblNoLibraries.Text = err;

                // Display error
                DisplayError(err);
            }
        }
    }

    #endregion


    #region "Common event methods"

    /// <summary>
    /// Handles actions related to the media folder.
    /// </summary>
    /// <param name="folderPath">Path of folder.</param>
    /// <param name="isNewFolder">Indicates whether the action is related to the new folder created action.</param>
    private void HandleFolderAction(string folderPath, bool isNewFolder)
    {
        HandleFolderAction(folderPath, isNewFolder, false);
    }


    /// <summary>
    /// Handles actions related to the media folder.
    /// </summary>
    /// <param name="folderPath">Path of folder.</param>
    /// <param name="isNewFolder">Indicates whether the action is related to the new folder created action.</param>
    private void HandleFolderAction(string folderPath, bool isNewFolder, bool selectAndDisplay)
    {
        // Update information on currently selected folder path
        FolderPath = GetFilePath(folderPath);

        // Reload tree if new folder was created     
        if (isNewFolder)
        {
            InitializeTree();

            ScriptHelper.RegisterStartupScript(Page, typeof(Page), "EnsureTopWindow", ScriptHelper.GetScript("if (self.focus) { self.focus(); }"));
        }

        // Bear in mind starting path
        if (StartingPath != "")
        {
            if (isNewFolder)
            {
                folderPath = FolderPath;
            }
            // If action occurs as result of library changed - select root folder
            else if (FolderPath == "")
            {
                folderPath = StartingPath.Trim('/').Trim('\\');
            }
        }

        string selectPath = EnsureFolderPath(folderPath);

        // Check if required folder exists
        string folderToCheck = folderPath;
        if (!isNewFolder && (this.StartingPath == ""))
        {
            folderToCheck = GetFilePath(folderPath);
        }
        else if (isNewFolder && (this.StartingPath == ""))
        {
            folderToCheck = GetFilePath(folderPath);
        }
        else if (!isNewFolder)
        {
            folderToCheck = GetFolderPath(this.StartingPath.TrimEnd('/')) + "/" + folderPath;
        }

        if (!this.folderTree.FolderExists(folderToCheck))
        {
            // Select root folder by default
            FolderPath = "";

            if (StartingPath == "")
            {
                // Library starting path
                selectPath = (LibraryInfo != null) ? LibraryInfo.LibraryFolder : "";
            }
            else
            {
                // Library root
                selectPath = folderTree.MediaLibraryFolder;
            }
        }

        if (isNewFolder && (this.StartingPath != ""))
        {
            //selectPath = GetFilePath(selectPath);
            selectPath = EnsureFolderPath(selectPath);
        }

        if (selectAndDisplay)
        {
            // Make sure the path is expanded in the tree
            folderTree.PathToSelect = selectPath;
            folderTree.ReloadData();
            this.pnlUpdateTree.Update();
        }

        // Select required folder if available
        folderTree.SelectPath(selectPath);

        // Update menu
        menuElem.LibraryID = LibraryID;
        menuElem.LibraryFolderPath = (!isNewFolder) ? (StartingPath + FolderPath).Trim('/') : FolderPath;
        menuElem.UpdateActionsMenu();

        // Load new data and reload view control's content
        LoadData(true);
        pnlUpdateView.Update();

        DisplayNormal();
    }


    /// <summary>
    /// Handles actions occuring when some text is searched.
    /// </summary>
    /// <param name="argument">Argument holding information on searched text.</param>
    private void HandleSearchAction(string argument)
    {
        LastSearchedValue = argument;

        // Load new data filtered by searched text and reload view control's content
        LoadData();

        pnlUpdateView.Update();

        // Keep focus in search text box
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "SetSearchFocus", ScriptHelper.GetScript("SetSearchFocus();"));
    }


    /// <summary>
    /// Handles actions occuring when some item is selected.
    /// </summary>
    /// <param name="argument">Argument holding information on selected item.</param>
    private void HandleSelectAction(string argument)
    {
        // Create new selected media item and pass it to the properties dialog
        SelectMediaItem(argument);

        ClearActionElems();
    }


    /// <summary>
    /// Handles display more action.
    /// </summary>
    private void HandleDisplayMore(string argument)
    {
        // Set display mode
        IsFullListingMode = true;

        argument = argument.Replace('|', '/');
        HandleFolderAction(argument, false);

        pnlUpdateProperties.Update();

        // Update Menu element
        menuElem.SelectedViewMode = DialogViewModeEnum.ListView;
        menuElem.UpdateViewMenu();
        pnlUpdateMenu.Update();

        ClearActionElems();
    }


    /// <summary>
    /// Handles attachment edit action.
    /// </summary>
    /// <param name="argument">Media file GUID comming from the view control.</param>
    private void HandleEdit(string argument)
    {
        IsEditImage = true;

        if (!string.IsNullOrEmpty(argument))
        {
            string[] argArr = argument.Split('|');

            string siteName = argArr[1];

            Guid mediaFileGuid = ValidationHelper.GetGuid(argArr[0], Guid.Empty);
            MediaFileInfo mfi = MediaFileInfoProvider.GetMediaFileInfo(mediaFileGuid, siteName);
            if (mfi != null)
            {
                string url = mediaView.GetItemUrl(LibrarySiteInfo, mfi.FileGUID, mfi.FileName, mfi.FileExtension, mfi.FilePath, false, 0, 0, 0);

                if (ItemToColorize == mediaFileGuid)
                {
                    SelectMediaItem(mfi.FileName, mfi.FileExtension, mfi.FileImageWidth, mfi.FileImageHeight, mfi.FileSize, url);
                }

                // Update select action to reflect changes made during editing
                UpdateSelectScript(mfi, url);
            }
        }

        ClearActionElems();
    }


    /// <summary>
    /// Updates JavaScript used to cause asynchronous post-back when item is selected in view.
    /// </summary>
    /// <param name="mediaFile">Updated media file.</param>
    /// <param name="url">URL of updated media file.</param>
    private void UpdateSelectScript(MediaFileInfo mediaFile, string url)
    {
        string thumbUrl = "";

        string argSet = mediaFile.FileGUID + "|" + mediaFile.FileName + "|" + mediaFile.FilePath + "|" + mediaFile.FileExtension +
            "|" + mediaFile.FileImageWidth + "|" + mediaFile.FileImageHeight + "|" + mediaFile.FileTitle + "|" + mediaFile.FileSize;

        string newViewAction = "javascript: window.open('@URL@'); return false;";
        string newSelectAction = "ColorizeRow('" + mediaFile.FileGUID + "'); SetSelectAction(" + ScriptHelper.GetString(argSet + "|" + url) + "); return false;";

        string fileName = mediaFile.FileName;

        string updateScript = "";
        if (menuElem.SelectedViewMode == DialogViewModeEnum.ListView)
        {
            string iconUrl = GetFileIconUrl(mediaFile.FileExtension, "list");

            // Get thumbnail dimensions
            int[] dims = ImageHelper.EnsureImageDimensions(0, 0, 200, mediaFile.FileImageWidth, mediaFile.FileImageHeight);

            updateScript = UPDT_SCRIPT_LIST;
            updateScript = updateScript.Replace("@THUMBWIDTH@", dims[0].ToString()).Replace("@THUMBHEIGHT@", dims[1].ToString());
            updateScript = updateScript.Replace("@NEWICONURL@", iconUrl);
        }
        else if (menuElem.SelectedViewMode == DialogViewModeEnum.TilesView)
        {
            // Get thumbnail dimensions
            int[] dims = ImageHelper.EnsureImageDimensions(0, 0, 200, mediaFile.FileImageWidth, mediaFile.FileImageHeight);

            updateScript = UPDT_SCRIPT_TILES;
            updateScript = updateScript.Replace("@THUMBWIDTH@", dims[0].ToString()).Replace("@THUMBHEIGHT@", dims[1].ToString());
            updateScript = updateScript.Replace("@MAXSIDESIZE@", "48");

            thumbUrl = this.mediaView.GetItemUrl(mediaFile, true, 0, 0, 48);
        }
        else
        {
            int[] dims = CMSDialogHelper.GetThumbImageDimensions(mediaFile.FileImageHeight, mediaFile.FileImageWidth, 95, 160);

            updateScript = UPDT_SCRIPT_THUMBS;
            updateScript = updateScript.Replace("@WIDTH@", dims[1].ToString()).Replace("@HEIGHT@", dims[0].ToString());

            thumbUrl = this.mediaView.GetItemUrl(mediaFile, true, dims[0], dims[1], 0);
        }
        updateScript = updateScript.Replace("@VIEWACTION@", newViewAction);
        updateScript = updateScript.Replace("@URL@", ScriptHelper.GetString(UrlHelper.RemoveQuery(url), false)).Replace("@NEWNAME@", fileName);
        updateScript = updateScript.Replace("@CUSTOMSCRIPT@", "");
        updateScript = updateScript.Replace("@GUID@", mediaFile.FileGUID.ToString()).Replace("@ACTION@", newSelectAction).Replace("@NEWGUID@", Guid.NewGuid().ToString());
        long size = ValidationHelper.GetLong(mediaFile.FileSize, 0);
        updateScript = updateScript.Replace("@NEWSIZE@", DataHelper.GetSizeString(size)).Replace("@NEWEXT@", mediaFile.FileExtension);

        // Update List & Tiles view specific information
        if ((menuElem.SelectedViewMode == DialogViewModeEnum.ThumbnailsView) || (menuElem.SelectedViewMode == DialogViewModeEnum.TilesView))
        {
            updateScript = updateScript.Replace("@THUMBURL@", UrlHelper.RemoveQuery(thumbUrl));
        }

        // Run update script
        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "UpdateSelectAction", ScriptHelper.GetScript(updateScript));
    }


    /// <summary>
    /// Hadnles actions occuring when new library file was created.
    /// </summary>
    /// <param name="argument">Argument holding information on new file path.</param>
    private void HandleFileCreatedAction(string argument)
    {
        string[] argArr = argument.Split('|');
        if (argArr.Length == 2)
        {
            string path = argArr[1];

            int mediaFileId = ValidationHelper.GetInteger(argArr[0], 0);

            MediaFileInfo fileInfo = MediaFileInfoProvider.GetMediaFileInfo(mediaFileId);
            if (fileInfo != null)
            {
                if (CMSDialogHelper.IsItemSelectable(SelectableContent, fileInfo.FileExtension))
                {
                    // Get file URL
                    string fileUrl = mediaView.GetItemUrl(LibrarySiteInfo, fileInfo.FileGUID, fileInfo.FileName, fileInfo.FileExtension, fileInfo.FilePath, false, 0, 0, 0);
                    string permUrl = mediaView.GetItemUrl(LibrarySiteInfo, fileInfo.FileGUID, fileInfo.FileName, fileInfo.FileExtension, fileInfo.FilePath, true, 0, 0, 0);

                    SelectMediaItem(fileInfo.FileName, fileInfo.FileExtension, fileInfo.FileImageWidth,
                        fileInfo.FileImageHeight, fileInfo.FileSize, fileUrl, permUrl);

                    ItemToColorize = fileInfo.FileGUID;

                    ColorizeRow(fileInfo.FileGUID.ToString());
                }
            }

            // Trim root folder when starting path is set
            if (StartingPath != "")
            {
                string startingPath = StartingPath.Trim('/');
                if (FolderPath.StartsWith(startingPath))
                {
                    FolderPath = FolderPath.Substring(startingPath.Length);
                }
            }

            InitializeMenuElem();

            // Load new data and reload view control's content
            LoadData();

            pnlUpdateView.Update();
        }
    }

    #endregion


    #region "Event handlers"

    /// <summary>
    /// Behaves as mediator in communication line between control taking action and the rest of the same level controls.
    /// </summary>
    protected void hdnButton_Click(object sender, EventArgs e)
    {
        // Get information on action causing postback        
        string argument = hdnArgument.Value;

        switch (CurrentAction)
        {
            case "insertitem":
                GetSelectedItem();
                break;

            case "search":
                HandleSearchAction(argument);
                break;

            case "select":
                HandleSelectAction(argument);
                break;

            case "clickformorefolder":
            case "clickformorelink":
                mediaView.ResetListSelection();

                ResetSearchFilter();
                HandleDisplayMore(argument);
                break;

            case "morefolderselect":
                string folderPath = CreateFilePath(argument);
                if (this.StartingPath != "")
                {
                    folderPath = this.StartingPath + folderPath;
                }
                else
                {
                    folderPath = GetFullFilePath(folderPath);
                }

                ResetSearchFilter();
                HandleFolderAction(folderPath, false, true);

                ClearActionElems();
                break;

            case "libraryfilecreated":
                HandleFileCreatedAction(argument);
                break;

            case "folderselect":
                ResetSearchFilter();
                argument = argument.Replace('|', '/');
                HandleFolderAction(argument, false);
                break;

            case "parentselect":
                string path = this.LastFolderPath;
                if (this.StartingPath != "")
                {
                    path = this.StartingPath + path;
                    path = GetParentFullPath(path, false);
                }
                else
                {
                    path = GetParentFullPath(path);
                }

                ResetSearchFilter();
                HandleFolderAction(path, false);

                ClearActionElems();
                break;

            case "closelisting":
                IsFullListingMode = false;
                folderTree.CloseListing = true;

                folderPath = LastFolderPath;
                if (this.StartingPath != "")
                {
                    folderPath = this.StartingPath + folderPath;
                }
                else
                {
                    folderPath = GetFullFilePath(folderPath);
                }

                // Reload folder
                HandleFolderAction(folderPath, false, false);
                break;

            case "newfolder":
                argument = argument.Replace('|', '/').Replace("//", "/");

                ResetSearchFilter();
                HandleFolderAction(argument, true, true);
                break;

            case "cancelfolder":
                ScriptHelper.RegisterStartupScript(Page, typeof(Page), "EnsureTopWindow", ScriptHelper.GetScript("if (self.focus) { self.focus(); }"));
                ClearActionElems();
                break;

            case "edit":
                HandleEdit(argument);
                break;

            default:
                ColorizeLastSelectedRow();

                pnlUpdateView.Update();
                break;
        }
    }


    protected void librarySelector_LibraryChanged(object sender, EventArgs e)
    {
        // Force control to reload library info
        LibraryInfo = null;
        FolderPath = "";
        IsFullListingMode = false;
        LibraryChanged = true;

        // Do not clear item info when editing
        if (UrlHelper.IsPostback())
        {
            ItemToColorize = Guid.Empty;
        }

        if (LibraryInfo != null)
        {
            // Load selected library
            SelectLibrary();

            // Select folder tree path obtained from the configuration
            string fullPath = "";
            if (!UrlHelper.IsPostback())
            {
                fullPath = ValidationHelper.GetString(RequestStockHelper.GetItem("FolderPath"), "");
            }
            else
            {
                fullPath = GetCompletePath(FolderPath);
            }

            if (fullPath.StartsWith(LibraryInfo.LibraryFolder))
            {
                // Remove library folder when tree starts at starting path different from the library root folder
                if (StartingPath != "")
                {
                    fullPath = fullPath.Replace(LibraryInfo.LibraryFolder, "").TrimStart('/');
                }
            }
            else
            {
                fullPath = LibraryInfo.LibraryFolder;
            }

            ProcessIsFullListingMode();

            HandleFolderAction(fullPath, false, true);

            // If loaded for the first time make sure info on library and selected path
            if (!UrlHelper.IsPostback())
            {
                EnsureMenuInfo();
            }

            // Clear properties if library changed
            if (UrlHelper.IsPostback())
            {
                // High-light item being edited
                if (ItemToColorize != Guid.Empty)
                {
                    ColorizeRow(ItemToColorize.ToString());
                }

                Properties.ClearProperties(true);
            }
        }
        else
        {
            DisplayInfo(ResHelper.GetString("dialogs.libraries.nolibrary"));

            HideMediaElements();
        }

        // Update library selection
        pnlUpdateSelectors.Update();
    }


    /// <summary>
    /// Handles event occuring when inner media view control requires load of the data.
    /// </summary>
    private void mediaView_ListReloadRequired()
    {
        LoadData();
    }


    protected object mediaView_GetInformation(string type, object parameter)
    {
        switch (type.ToLower())
        {
            case "fileisnotindatabase":
                string fileName = ValidationHelper.GetString(parameter, "");
                return FileIsNotInDatabase(fileName);

            case "siteidrequired":
                return LibrarySiteInfo.SiteID;

            default:
                return null;
        }
    }


    protected void ListViewControl_OnBeforeSorting(object sender, EventArgs e)
    {
        GridViewSortEventArgs sortArg = (e as GridViewSortEventArgs);
        if (sortArg != null)
        {
            SortDirection = (mediaView.ListViewControl.SortDirect.ToLower().EndsWith("desc") ? "DESC" : "ASC");

            SortColumns = sortArg.SortExpression;
        }
    }

    #endregion
}