﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ObjectRelationships.aspx.cs" Inherits="CMSModules_AdminControls_Pages_ObjectRelationships" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" %>
<%@ Register Src="~/CMSModules/AdminControls/Controls/ObjectRelationships/ObjectRelationships.ascx" TagName="ObjectRelationships"
    TagPrefix="cms" %>
<asp:Content runat="server" ContentPlaceHolderID="plcContent" ID="cntContent">
    <cms:ObjectRelationships runat="server" ID="relElem" />
</asp:Content>
