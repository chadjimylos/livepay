<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="CMSDesk_Tools_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>CMSDesk - Tools</title>
    
    <asp:Literal runat="server" ID="ltlScript" EnableViewState="false" />
</head>
<frameset border="0" rows="6,*">
		<frame name="toolsheader" src="header.aspx" scrolling="no" noresize="noresize" frameborder="0" />		
		<frameset border="0" cols="225,*" runat="server" id="colsFrameset" EnableViewState="false">
    		<frame name="frameTree" src="leftmenu.aspx" scrolling="no" frameborder="0" runat="server" id="toolstree"  />
    		<frame name="frameMain" src="modules.aspx" frameborder="0" runat="server" id="frameMain" />
		</frameset>
		<noframes>
			<p id="p1">
				This HTML frameset displays multiple Web pages. To view this frameset, use a 
				Web browser that supports HTML 4.0 and later.
			</p>
		</noframes>
	</frameset>
</html>
