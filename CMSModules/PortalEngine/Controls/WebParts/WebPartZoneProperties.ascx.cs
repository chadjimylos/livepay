using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using CMS.FormEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.PortalControls;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_Controls_WebParts_WebPartZoneProperties : CMSUserControl
{
    #region "Variables"

    /// <summary>
    /// Current page info
    /// </summary>
    private PageInfo pi = null;


    /// <summary>
    /// Page template info
    /// </summary>
    private PageTemplateInfo pti = null;


    /// <summary>
    /// Current web part zone
    /// </summary>
    private WebPartZoneInstance webPartZone = null;

    #endregion


    /// <summary>
    /// OnInit event (BasicForm initialization)
    /// </summary>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        string zoneId = QueryHelper.GetText("zoneid", "");
        string aliasPath = QueryHelper.GetText("aliaspath", "");
        if (!String.IsNullOrEmpty(zoneId) && !String.IsNullOrEmpty(aliasPath))
        {
            // Get pageinfo
            pi = PageInfoProvider.GetPageInfo(CMSContext.CurrentSiteName, aliasPath, CMSContext.PreferredCultureCode, null, CMSContext.CurrentSite.CombineWithDefaultCulture, null);
            if (pi == null)
            {
                this.lblInfo.Text = ResHelper.GetString("webpartzone.notfound");
                this.pnlFormArea.Visible = false;
                return;
            }

            // Get template
            pti = pi.GetInheritedTemplateInfo(CMSContext.PreferredCultureCode, CMSContext.CurrentSite.CombineWithDefaultCulture);

            // Get web part zone
            pti.EnsureZone(zoneId);
            webPartZone = pti.GetZone(zoneId);
            if (webPartZone == null)
            {
                this.lblInfo.Text = ResHelper.GetString("webpartzone.notfound");
                this.pnlFormArea.Visible = false;
                return;
            }

            FormInfo fi = BuildFormInfo(webPartZone);

            // Get the datarow and fill the data row with values
            DataRow dr = fi.GetDataRow();
            foreach (DataColumn column in dr.Table.Columns)
            {
                try
                {
                    DataHelper.SetDataRowValue(dr, column.ColumnName, webPartZone.GetValue(column.ColumnName));
                }
                catch { }
            }

            // Initialize Form
            formElem.DataRow = dr;
            formElem.MacroTable = webPartZone.MacroTable;
            formElem.SubmitButton.Visible = false;
            formElem.SiteName = CMSContext.CurrentSiteName;
            formElem.FormInformation = fi;
            formElem.ShowPrivateFields = true;

            // HTML editor toolbar
            if (fi.UsesHtmlArea())
            {
                plcToolbarPadding.Visible = true;
                plcToolbar.Visible = true;
                pnlFormArea.Height = 285;
            }
        }
    }


    /// <summary>
    /// Render event (set info label visibility).
    /// </summary>
    protected override void Render(HtmlTextWriter writer)
    {
        this.lblInfo.Visible = (this.lblInfo.Text != "");
        base.Render(writer);
    }


    #region "Public methods"

    /// <summary>
    /// Saves webpart properties.
    /// </summary>
    public bool Save()
    {
        // Save the data
        if (formElem.SaveData(""))
        {
            DataRow dr = formElem.DataRow;

            // Get basicform's datarow and update the fields
            if ((webPartZone != null) && (dr != null))
            {
                // If zone type changed, delete all webparts in the zone
                if (ValidationHelper.GetString(webPartZone.GetValue("WidgetZoneType"),"") != ValidationHelper.GetString(dr["WidgetZoneType"], ""))
                {
                    webPartZone.WebParts.Clear();
                }

                foreach (DataColumn column in dr.Table.Columns)
                {
                    webPartZone.MacroTable[column.ColumnName.ToLower()] = formElem.MacroTable[column.ColumnName.ToLower()];
                    webPartZone.SetValue(column.ColumnName, dr[column]);
                }

                // Update page template
                PageTemplateInfoProvider.SetPageTemplateInfo(pti);

                // Reload the form (because of macro values set only by JS)
                this.formElem.LoadData(dr);

                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Returns the form definition for the webpart zone properties.
    /// </summary>
    private FormInfo BuildFormInfo(WebPartZoneInstance webPartZone)
    {
        FormInfo fi = new FormInfo();

        string formDefinition = File.ReadAllText(Server.MapPath(ResolveUrl("WebPartZone_Properties.xml")));

        if (!String.IsNullOrEmpty(formDefinition))
        {
            // Load properties
            fi.LoadXmlDefinition(formDefinition);
            fi.UpdateExistingFields(fi);

            DataRow dr = fi.GetDataRow();
            LoadDataRowFromWebPartZone(dr, webPartZone);
        }

        return fi;
    }


    /// <summary>
    /// Loads the data row data from given web part zone instance
    /// </summary>
    /// <param name="dr">DataRow to fill</param>
    /// <param name="webPart">Source web part zone</param>
    private void LoadDataRowFromWebPartZone(DataRow dr, WebPartZoneInstance webPartZone)
    {
        foreach (DataColumn column in dr.Table.Columns)
        {
            try
            {
                object value = webPartZone.GetValue(column.ColumnName);
                if (column.DataType == typeof(decimal))
                {
                    value = ValidationHelper.GetDouble(value, 0, "en-us");
                }
                DataHelper.SetDataRowValue(dr, column.ColumnName, value);
            }
            catch
            {
            }
        }
    }

    #endregion
}
