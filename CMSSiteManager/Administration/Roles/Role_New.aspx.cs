using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSSiteManager_Administration_Roles_Role_New : CMSRolesPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Check "Modify" permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Roles", "Modify"))
        {
            RedirectToAccessDenied("CMS.Roles", "Modify");
        } 

        string roleListUrl = "~/CMSSiteManager/Administration/Roles/Role_List.aspx";
        if (!string.IsNullOrEmpty(Request.QueryString["siteid"]))
        {
            this.roleEditElem.SiteID = GetSiteID(Request.QueryString["siteid"]);
            roleListUrl += "?siteid=" + this.roleEditElem.SiteID;
        }
        else if (!string.IsNullOrEmpty(Request.QueryString["selectedsiteid"]))
        {
            this.roleEditElem.SiteID = GetSiteID(Request.QueryString["selectedsiteid"]);
            roleListUrl += "?selectedsiteid=" + this.roleEditElem.SiteID;
        }

        roleEditElem.OnSaved += new EventHandler(roleEditElem_OnSaved);

        // Pagetitle
        this.CurrentMaster.Title.HelpTopicName = "new_role";
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Administration-Role_New.Title");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_Role/new.png");

        // Initializes page title breadcrumbs
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("general.roles");
        pageTitleTabs[0, 1] = roleListUrl;        
        pageTitleTabs[1, 0] = ResHelper.GetString("Administration-Role_New.NewRole");
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
    }


    /// <summary>
    /// OnOK click event handler.
    /// </summary>
    void roleEditElem_OnSaved(object sender, EventArgs e)
    {
        //CMS desk site context
        string siteId = QueryHelper.GetText("siteid", "");
        if (siteId != String.Empty)
        {
            siteId = "&siteid=" + siteId;
        }

        //CMS site manager site context
        string selectedsiteId = QueryHelper.GetText("selectedsiteid", "");
        if (selectedsiteId != String.Empty)
        {
            selectedsiteId = "&selectedsiteid=" + selectedsiteId;
        }

        UrlHelper.Redirect("Role_Edit_Frameset.aspx?roleId=" + this.roleEditElem.ItemID + siteId + selectedsiteId);
    }
}
