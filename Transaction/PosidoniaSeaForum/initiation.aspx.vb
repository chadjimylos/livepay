Imports System.Data
Imports System.Data.SqlClient
Partial Class TRANSACTION_INITIATION
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnSubmit.Attributes("onClick") = "return payClicked();"
        merchantID.Value = System.Configuration.ConfigurationManager.AppSettings("PosidoniaSeaForumApacsUserID")
        APACSURL.Text = System.Configuration.ConfigurationManager.AppSettings("PosidoniaSeaForumAPACSURL")

        Dim remoteAddr As String = Request.UserHostAddress

        'Try
        Dim AmountEncrypt As String, email As String
        If Request.QueryString("Am") <> "" Then
            AmountEncrypt = CryptoFunctions.DecryptString(Request("Am"))
            email = CryptoFunctions.DecryptString(Request("em"))
        Else
            AmountEncrypt = Request("Amount")
        End If

        amount.Value = AmountEncrypt 'GET THE CORRECT AMOUNT HERE
        Var1.Value = email 'GET THE TRANSACTIONCODE
        lang.Value = "EN" '/EN GET THE CORRECT LANGUAGE
        merchantRef.Value = GetMerchantRef(amount.Value, Var1.Value)
        'Catch ex As Exception
        'Response.Redirect("OnlineCheckOut.aspx")
        'End Try
    End Sub

    Private Function GetMerchantRef(ByVal Amount As String, ByVal PaymentCode As String) As String
        Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()

        Dim objConnection As New SqlConnection(strConnection)
        objConnection.Open()

        Dim objCommand As New SqlCommand("PosidoniaSeaForum_GetMerchantRef", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@PaymentCode", PaymentCode)
        objCommand.Parameters.AddWithValue("@Amount", Amount)

        Dim merchantRef As String = objCommand.ExecuteScalar()

        objConnection.Close()
        Return merchantRef
    End Function
End Class
