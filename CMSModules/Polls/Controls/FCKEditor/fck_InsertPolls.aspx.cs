using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.Polls;
using CMS.UIControls;
using CMS.PortalEngine;

public partial class CMSModules_Polls_Controls_FCKEditor_fck_InsertPolls : CMSPage
{
    #region "Private variables"

    protected int groupId = 0;
    protected IInfoObject group = null;
    protected bool reload = true;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        SetBrowserClass();
        bodyElem.Attributes["class"] = mBodyClass;

        // Get group ID from querystring
        groupId = QueryHelper.GetInteger("groupid", 0);

        //// Setup header text
        uniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(uniGrid_OnExternalDataBound);
        uniGrid.OnPageChanged += new EventHandler<EventArgs>(uniGrid_OnPageChanged);

        uniGrid.HideControlForZeroRows = false;
        uniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (CMSContext.ViewMode == ViewModeEnum.LiveSite)
        {
            // Register custom css if exists
            RegisterDialogCSSLink();
        }

        if (reload)
        {
            this.ReloadData(uniGrid.GridView.PageIndex);
        }
    }


    void uniGrid_OnPageChanged(object sender, EventArgs e)
    {
        this.ReloadData(uniGrid.Pager.CurrentPage -1);
        reload = false;
    }


    /// <summary>
    /// Databound handler.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="sourceName"></param>
    /// <param name="parameter"></param>
    /// <returns></returns>
    object uniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "polldisplayname":
                HyperLink link = new HyperLink();
                link.NavigateUrl = "#";

                // Get DataRowView
                DataRowView drv = GetDataRowView(sender as DataControlFieldCell);

                link.Text = HTMLHelper.HTMLEncode(ValidationHelper.GetString(drv.Row["PollDisplayName"], null));
                link.ToolTip = HTMLHelper.HTMLEncode(ValidationHelper.GetString(drv.Row["PollDisplayName"], null));

                // Prepare script for hyperlink
                string script = "javascript:insertPolls(\"{^PollControl|(PollName)" + ValidationHelper.GetString(drv.Row["PollCodeName"], null);

                int pollSiteID = ValidationHelper.GetInteger(drv.Row["PollSiteID"], 0);
                if (pollSiteID > 0)
                {
                    // Append site name to the script
                    SiteInfo site = SiteInfoProvider.GetSiteInfo(pollSiteID);
                    if (site != null)
                    {
                        script += "|(SiteName)" + site.SiteName;
                    }
                }

                int pollGroupID = ValidationHelper.GetInteger(drv.Row["PollGroupID"], 0);
                if (pollGroupID > 0)
                {
                    // Get group if not loaded yet
                    if (group == null)
                    {
                        group = ModuleCommands.CommunityGetGroupInfo(pollGroupID);
                    }

                    // Append group name to the script
                    if (group != null)
                    {
                        script += "|(GroupName)" + ValidationHelper.GetString(group.GetValue(group.CodeNameColumn), "");
                    }
                }

                // Close script
                script += "^}\");";

                link.Attributes.Add("onclick", script);
                return link;
        }
        return null;
    }


    /// <summary>
    /// Gets whole row for given cell
    /// </summary>
    /// <param name="dcf">Row cell</param>
    /// <returns>Appropriate row</returns>
    protected static DataRowView GetDataRowView(DataControlFieldCell dcf)
    {
        return ((GridViewRow)(dcf).Parent).DataItem as DataRowView;
    }


    /// <summary>
    /// Reloads list of available polls.
    /// </summary>
    protected void ReloadData(int pageIndex)
    {
        int topN = uniGrid.GridView.PageSize * (pageIndex + 1 + uniGrid.GridView.PagerSettings.PageButtonCount);
        string orderBy = String.IsNullOrEmpty(uniGrid.SortDirect) ? "PollDisplayName" : uniGrid.SortDirect;

        // If group context exists then display group's polls
        if (groupId > 0)
        {
            uniGrid.DataSource = PollInfoProvider.GetGroupPolls(CMSContext.CurrentSiteID, uniGrid.WhereClause, groupId, orderBy, topN, "PollID, PollCodeName, PollDisplayName, PollSiteID, PollGroupID");
        }
        else
        {
            // Else select all global polls bound to current site
            uniGrid.DataSource = PollInfoProvider.GetGlobalPolls(CMSContext.CurrentSiteID, uniGrid.WhereClause, orderBy, topN, "PollID, PollCodeName, PollDisplayName, PollSiteID, PollGroupID");
        }

        uniGrid.ReloadData();
    }
}
