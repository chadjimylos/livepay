<%@ Page Language="C#" AutoEventWireup="true" CodeFile="System_DebugThreads.aspx.cs"
    Inherits="CMSSiteManager_Administration_System_Debug_System_DebugThreads" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="System - SQL" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">

    <script type="text/javascript">
        //<![CDATA[
        function alert() {
        }
        //]]>
    </script>

    <asp:HiddenField runat="server" ID="hdnGuid" EnableViewState="false" />
    <asp:Button runat="server" ID="btnCancel" CssClass="HiddenButton" OnClick="btnCancel_Click" />
    <asp:Label ID="lblError" runat="server" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label ID="lblInfo" runat="server" CssClass="InfoLabel" EnableViewState="false" />
    <div style="text-align: right; padding: 5px;">
        <cms:CMSButton runat="server" ID="btnRunDummy" OnClick="btnRunDummy_Click" CssClass="LongButton" />
    </div>
    <asp:ScriptManager runat="server" ID="manScript" ScriptMode="Release" />
    <asp:UpdatePanel runat="server" ID="pnlUpdate">
        <ContentTemplate>
            <asp:GridView runat="server" ID="gridThreads" EnableViewState="false" GridLines="Horizontal"
                AutoGenerateColumns="false" Width="100%" CellPadding="3" ShowFooter="true" CssClass="UniGridGrid">
                <HeaderStyle HorizontalAlign="Left" CssClass="UniGridHead" />
                <Columns>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <%# GetIndex() %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <%# GetActions(Eval("HasLog"), Eval("ThreadGUID"), Eval("Status")) %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <strong>
                                <%# Eval("MethodClassName") %></strong>.<%# Eval("MethodName") %><br />
                            <%# Eval("RequestUrl") %>
                        </ItemTemplate>
                        <FooterTemplate>
                            <strong>
                                <%# cmsVersion %></strong>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <%# Eval("ThreadID") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <%# Eval("Status") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <%# Eval("ThreadStarted") %></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Right" />
                        <ItemStyle HorizontalAlign="Right" />
                        <FooterStyle HorizontalAlign="Right" />
                        <ItemTemplate>
                            <%# GetDuration(Eval("ThreadStarted"))%></ItemTemplate>
                        <FooterTemplate>
                            <strong>
                                <%# GetDurationString(totalDuration) %></strong>
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Timer runat="server" ID="timRefresh" Enabled="true" Interval="1000" OnTick="timRefresh_Tick" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
