using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_Departments_Department_Header : CMSEcommerceConfigurationPage
{
    protected int departmentId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        departmentId = ValidationHelper.GetInteger(Request.QueryString["departmentId"], 0);

        if (!RequestHelper.IsPostBack())
        {
            InitalizeMenu();
        }

        string currentDepartmentName = string.Empty;
        DepartmentInfo di = DepartmentInfoProvider.GetDepartmentInfo(departmentId);
        if (di != null)
        {
            currentDepartmentName = di.DepartmentDisplayName;
        }

        // initializes page title
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("Department_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/Departments/Department_List.aspx";
        pageTitleTabs[0, 2] = "configEdit";
        pageTitleTabs[1, 0] = currentDepartmentName;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("Department_Edit.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_Department/object.png");

        this.CurrentMaster.Title.HelpTopicName = "new_department";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    /// <summary>
    /// Initializes edit menu
    /// </summary>
    protected void InitalizeMenu()
    {
        this.CurrentMaster.Tabs.Tabs = new string[3, 4];
        this.CurrentMaster.Tabs.Tabs[0, 0] = ResHelper.GetString("General.General");
        this.CurrentMaster.Tabs.Tabs[0, 1] = ""; // "SetHelpTopic('helpTopic', 'new_department');";
        this.CurrentMaster.Tabs.Tabs[0, 2] = "Department_General.aspx?departmentId=" + departmentId.ToString();
        this.CurrentMaster.Tabs.Tabs[1, 0] = ResHelper.GetString("general.users");
        this.CurrentMaster.Tabs.Tabs[1, 1] = ""; // "SetHelpTopic('helpTopic', 'departments_users');";
        this.CurrentMaster.Tabs.Tabs[1, 2] = "Department_Users.aspx?departmentId=" + departmentId.ToString();
        this.CurrentMaster.Tabs.Tabs[2, 0] = ResHelper.GetString("Department_Header.TaxClass");
        this.CurrentMaster.Tabs.Tabs[2, 1] = ""; // "SetHelpTopic('helpTopic', 'taxclass_department');";
        this.CurrentMaster.Tabs.Tabs[2, 2] = "Department_TaxClass.aspx?departmentId=" + departmentId.ToString();
        this.CurrentMaster.Tabs.UrlTarget = "content";
    }
}
