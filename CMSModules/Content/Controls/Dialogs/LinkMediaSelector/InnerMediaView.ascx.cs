﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Collections;

using CMS.UIControls;
using CMS.Controls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;
using CMS.TreeEngine;
using CMS.WorkflowEngine;
using CMS.CMSHelper;
using CMS.SettingsProvider;
using CMS.SiteProvider;

using Image = System.Web.UI.WebControls.Image;

using TreeNode = CMS.TreeEngine.TreeNode;
using TimeZoneInfo = CMS.SiteProvider.TimeZoneInfo;


public partial class CMSModules_Content_Controls_Dialogs_LinkMediaSelector_InnerMediaView : CMSUserControl
{
    #region "Private variables"

    private string mImagesPath = "";

    private const double MaxThumbImgWidth = 160.0;
    private const double MaxThumbImgHeight = 95.0;
    private const int PAGER_GROUP_SIZE = 10;

    private OutputFormatEnum mOutputFormat = OutputFormatEnum.HTMLMedia;
    private DialogConfiguration mConfig = null;
    private DialogViewModeEnum mViewMode = DialogViewModeEnum.ListView;
    private DataSet mDataSource = null;
    private SelectableContentEnum mSelectableContent = SelectableContentEnum.AllContent;
    private MediaSourceEnum mSourceType = MediaSourceEnum.Content;
    private TreeNode mTreeNodeObj = null;

    private int mVersionHistoryId = -1;

    private string mFileIdColumn = "";
    private string mFileNameColumn = "";
    private string mFileExtensionColumn = "";
    private string mFileSizeColumn = "";
    private string mFileWidthColumn = "";
    private string mFileHeightColumn = "";

    private string mAllowedExtensions = "";
    private int mResizeToHeight = 0;
    private int mResizeToWidth = 0;
    private int mResizeToMaxSideSize = 0;

    private string mInfoText = "";
    private int idCount = 0;
    private int mNodeParentNodeId = 0;

    private int mLastSiteId = 0;

    private bool mIsCopyMoveLinkDialog = false;

    #endregion


    #region "Events & delegates"

    /// <summary>
    /// Delegate for an event occuring when argument set is required.
    /// </summary>
    /// <param name="dr">DataRow holding information on currently processed file.</param>    
    public delegate string OnGetArgumentSet(DataRow dr);


    /// <summary>
    /// Event occuring when argument set is required.
    /// </summary>
    public event OnGetArgumentSet GetArgumentSet;


    /// <summary>
    /// Delegate for the event fired when URL for list image is required.
    /// </summary>
    /// <param name="dr">DataRow holding information on currently processed file.</param>   
    /// <param name="isPreview">Indicates whether the image is generated as part of preview.</param>
    /// <param name="notAttachment">Indicates whether the URL is required for non-attachment item.</param>
    public delegate string OnGetListItemUrl(DataRow dr, bool isPreview, bool notAttachment);


    /// <summary>
    /// Event occuring when URL for list item image is required.
    /// </summary>
    public event OnGetListItemUrl GetListItemUrl;


    /// <summary>
    /// Delegate for the event fired when URL for tiles & thumbnails image is required.
    /// </summary>
    /// <param name="dr">DataRow holding information on currently processed file.</param>  
    /// <param name="isPreview">Indicates whether the image is generated as part of preview.</param>
    /// <param name="maxSideSize">Maximum size of the preview image. If full-size required parameter gets zero value.</param>
    /// <param name="notAttachment">Indicates whether the URL is required for non-attachment item.</param>
    public delegate string OnGetTilesThumbsItemUrl(DataRow dr, bool isPreview, int height, int width, int maxSideSize, bool notAttachment);


    /// <summary>
    /// Event occuring when URL for tiles & thumbnails image is required.
    /// </summary>
    public event OnGetTilesThumbsItemUrl GetTilesThumbsItemUrl;


    /// <summary>
    /// Delegate for the event occuring when information on file import status is required.
    /// </summary>
    /// <param name="type">Type of the required information.</param>
    /// <param name="parameter">Parameter related.</param>
    public delegate object OnGetInformation(string type, object parameter);


    /// <summary>
    /// Event occuring when information on file import status is required.
    /// </summary>
    public event OnGetInformation GetInformation;


    /// <summary>
    /// Delegate for the event occuring when permission modify is required.
    /// </summary>
    /// <param name="dr">DataRow holding information on currently processed file.</param>
    public delegate bool OnGetModifyPermission(DataRow dr);


    /// <summary>
    /// Event occuring when permission modify is required.
    /// </summary>
    public event OnGetModifyPermission GetModifyPermission;

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the OutputFormat (needed for correct dialog type recognition)
    /// </summary>
    public OutputFormatEnum OutputFormat
    {
        get
        {
            return mOutputFormat;
        }
        set
        {
            mOutputFormat = value;
        }
    }


    /// <summary>
    /// Gets current dialog configuration.
    /// </summary>
    public DialogConfiguration Config
    {
        get
        {
            if (mConfig == null)
            {
                mConfig = DialogConfiguration.GetDialogConfiguration();
            }
            return mConfig;
        }
    }


    /// <summary>
    /// Gets a UniGrid control used to display files in LIST view mode.
    /// </summary>
    public UniGrid ListViewControl
    {
        get
        {
            return gridList;
        }
    }


    /// <summary>
    /// Gets a repeater control used to display files in TILES view mode.
    /// </summary>
    public BasicRepeater TilesViewControl
    {
        get
        {
            return repTilesView;
        }
    }


    /// <summary>
    /// Gets a repeater control used to display files in THUMBNAILS view mode.
    /// </summary>
    public BasicRepeater ThumbnailsViewControl
    {
        get
        {
            return repThumbnailsView;
        }
    }


    /// <summary>
    /// Gets list of names of selected files.
    /// </summary>
    public ArrayList SelectedItems
    {
        get
        {
            return GetSelectedItems();
        }
    }


    /// <summary>
    /// Gets or sets a view mode used to display files.
    /// </summary>
    public DialogViewModeEnum ViewMode
    {
        get
        {
            return mViewMode;
        }
        set
        {
            mViewMode = value;
        }
    }


    /// <summary>
    /// Gets or sets source of the data for view controls.
    /// </summary>
    public DataSet DataSource
    {
        get
        {
            return mDataSource;
        }
        set
        {
            mDataSource = value;
        }
    }


    /// <summary>
    /// Type of the content which can be selected.
    /// </summary>
    public SelectableContentEnum SelectableContent
    {
        get
        {
            return mSelectableContent;
        }
        set
        {
            mSelectableContent = value;
        }
    }


    /// <summary>
    /// Gets or sets name of the column holding information on the file identifier.
    /// </summary>
    public string FileIdColumn
    {
        get
        {
            return mFileIdColumn;
        }
        set
        {
            mFileIdColumn = value;
        }
    }

    /// <summary>
    /// Gets or sets name of the column holding information on file name.
    /// </summary>
    public string FileNameColumn
    {
        get
        {
            return mFileNameColumn;
        }
        set
        {
            mFileNameColumn = value;
        }
    }


    /// <summary>
    /// Gets or sets name of the column holding information on file extension.
    /// </summary>
    public string FileExtensionColumn
    {
        get
        {
            return mFileExtensionColumn;
        }
        set
        {
            mFileExtensionColumn = value;
        }
    }


    /// <summary>
    /// Gets or sets name of the column holding information on file width.
    /// </summary>
    public string FileWidthColumn
    {
        get
        {
            return mFileWidthColumn;
        }
        set
        {
            mFileWidthColumn = value;
        }
    }


    /// <summary>
    /// Gets or sets name of the column holding information on file height.
    /// </summary>
    public string FileHeightColumn
    {
        get
        {
            return mFileHeightColumn;
        }
        set
        {
            mFileHeightColumn = value;
        }
    }


    /// <summary>
    /// Gets or sets name of the column holding information on file size.
    /// </summary>
    public string FileSizeColumn
    {
        get
        {
            return mFileSizeColumn;
        }
        set
        {
            mFileSizeColumn = value;
        }
    }


    /// <summary>
    /// Gets or sets text of the information label.
    /// </summary>
    public string InfoText
    {
        get
        {
            return mInfoText;
        }
        set
        {
            mInfoText = value;
        }
    }


    /// <summary>
    /// Gets the node attachments are related to.
    /// </summary>
    public TreeNode TreeNodeObj
    {
        get
        {
            return mTreeNodeObj;
        }
        set
        {
            mTreeNodeObj = value;
        }
    }


    /// <summary>
    /// Gets or sets information on source type.
    /// </summary>
    public MediaSourceEnum SourceType
    {
        get
        {
            return mSourceType;
        }
        set
        {
            mSourceType = value;
        }
    }


    /// <summary>
    /// Height of attachment.
    /// </summary>
    public int ResizeToHeight
    {
        get
        {
            return mResizeToHeight;
        }
        set
        {
            mResizeToHeight = value;
        }
    }


    /// <summary>
    /// Width of attachment.
    /// </summary>
    public int ResizeToWidth
    {
        get
        {
            return mResizeToWidth;
        }
        set
        {
            mResizeToWidth = value;
        }
    }


    /// <summary>
    /// Max side size of attachment.
    /// </summary>
    public int ResizeToMaxSideSize
    {
        get
        {
            return mResizeToMaxSideSize;
        }
        set
        {
            mResizeToMaxSideSize = value;
        }
    }


    /// <summary>
    /// Currently topN value for selection.
    /// </summary>
    public int CurrentTopN
    {
        get
        {
            int pageSize = 0;
            int pageIndex = 0;
            int pageCount = 0;
            switch (ViewMode)
            {
                case DialogViewModeEnum.TilesView:
                    if (pageSizeTiles.SelectedValue.ToUpper() == "-1")
                    {
                        pageSize = 0;
                    }
                    else
                    {
                        pageSize = ValidationHelper.GetInteger(pageSizeTiles.SelectedValue, 20);
                    }
                    pageIndex = pagerElemTiles.GetCurrentPage();
                    pageCount = pagerElemTiles.PageCount;
                    break;

                case DialogViewModeEnum.ThumbnailsView:
                    if (pageSizeThumbs.SelectedValue.ToUpper() == "-1")
                    {
                        pageSize = 0;
                    }
                    else
                    {
                        pageSize = ValidationHelper.GetInteger(pageSizeThumbs.SelectedValue, 12);
                    }
                    pageIndex = pagerElemThumbnails.GetCurrentPage();
                    pageCount = pagerElemThumbnails.PageCount;
                    break;

                case DialogViewModeEnum.ListView:
                    if (gridList.Pager.CurrentPageSize == -1)
                    {
                        pageSize = 0;
                    }
                    else
                    {
                        pageSize = ValidationHelper.GetInteger(gridList.Pager.CurrentPageSize, 10);
                    }
                    pageIndex = gridList.Pager.CurrentPage - 1;
                    pageCount = gridList.Pager.CurrentPagesGroupSize;
                    break;
            }
            if (pageSize > 0)
            {
                pageCount = (pageCount > PAGER_GROUP_SIZE) ? pageCount : PAGER_GROUP_SIZE;

                return (pageSize * (pageIndex + 1 + pageCount + (PAGER_GROUP_SIZE - (pageCount % PAGER_GROUP_SIZE))));
            }
            else
            {
                return 0;
            }
        }
    }


    /// <summary>
    /// Indicates if full listing mode is enabled. This mode enables navigation to child and parent folders/documents from current view.
    /// </summary>
    public bool IsFullListingMode
    {
        get
        {
            return ValidationHelper.GetBoolean(ViewState["IsFullListingMode"], false);
        }
        set
        {
            ViewState["IsFullListingMode"] = value;
        }
    }


    /// <summary>
    /// Indicates whether the control is displayed as part of the copy/move dialog.
    /// </summary>
    public bool IsCopyMoveLinkDialog
    {
        get
        {
            return mIsCopyMoveLinkDialog;
        }
        set
        {
            mIsCopyMoveLinkDialog = value;
        }
    }


    /// <summary>
    /// ID of the related node version history.
    /// </summary>
    private int VersionHistoryID
    {
        get
        {
            if (mVersionHistoryId < 0)
            {
                mVersionHistoryId = 0;

                // Load the version history
                if (TreeNodeObj != null)
                {
                    // Get the node workflow
                    WorkflowManager wm = new WorkflowManager(TreeNodeObj.TreeProvider);
                    WorkflowInfo wi = wm.GetNodeWorkflow(TreeNodeObj);
                    if (wi != null)
                    {
                        // Ensure the document version
                        VersionManager vm = new VersionManager(TreeNodeObj.TreeProvider);
                        VersionHistoryID = vm.EnsureVersion(TreeNodeObj, TreeNodeObj.IsPublished);
                    }
                }
            }

            return mVersionHistoryId;
        }
        set
        {
            mVersionHistoryId = value;
        }
    }

    #endregion]


    #region "Private properties"

    /// <summary>
    /// Image relative path.
    /// </summary>
    private string ImagesPath
    {
        get
        {
            if (mImagesPath == "")
            {
                mImagesPath = GetImageUrl("Design/Controls/UniGrid/Actions/", IsLiveSite, true);
            }
            return mImagesPath;
        }
    }

    #endregion


    #region "Attachment properties"

    /// <summary>
    /// Gets all allowed extensions.
    /// </summary>
    public string AllowedExtensions
    {
        get
        {
            if (mAllowedExtensions == "")
            {
                mAllowedExtensions = QueryHelper.GetString("allowedextensions", "");
                if (mAllowedExtensions == "")
                {
                    mAllowedExtensions = SettingsKeyProvider.GetStringValue(CMSContext.CurrentSiteName + ".CMSUploadExtensions");
                }
            }
            return mAllowedExtensions;
        }
        private set
        {
            mAllowedExtensions = value;
        }
    }


    /// <summary>
    /// Gets or sets ID of the parent node.
    /// </summary>
    public int NodeParentID
    {
        get
        {
            return mNodeParentNodeId;
        }
        set
        {
            mNodeParentNodeId = value;
        }
    }

    #endregion


    protected override void OnPreRender(EventArgs e)
    {
        // Display information on empty data
        bool isEmpty = DataHelper.DataSourceIsEmpty(DataSource);
        if (isEmpty)
        {
            plcViewArea.Visible = false;
        }
        else
        {
            lblInfo.Visible = false;
            plcViewArea.Visible = true;
        }

        // If info text is set display it
        if (!string.IsNullOrEmpty(InfoText))
        {
            lblInfo.Text = InfoText;
            lblInfo.Visible = true;
        }
        else if (isEmpty)
        {
            lblInfo.Text = (IsCopyMoveLinkDialog ? ResHelper.GetString("media.copymove.empty") : CMSDialogHelper.GetNoItemsMessage(Config, SourceType));
            lblInfo.Visible = true;
        }

        // Reset pager control
        switch (ViewMode)
        {
            case DialogViewModeEnum.TilesView:
                pagerElemTiles.CurrentPage = 0;
                break;

            case DialogViewModeEnum.ThumbnailsView:
                pagerElemThumbnails.CurrentPage = 0;
                break;
        }

        base.OnPreRender(e);
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        Visible = !StopProcessing;
        if (!StopProcessing)
        {
            gridList.IsLiveSite = IsLiveSite;
            if (UrlHelper.IsPostback())
            {
                Reload(true);
            }
        }
    }


    #region "Public methods"

    /// <summary>
    /// Loads view controls according currently set view mode.
    /// </summary>
    public void LoadViewControls()
    {
        InfoText = "";

        // Select mode according required
        switch (ViewMode)
        {
            case DialogViewModeEnum.ListView:
                plcListView.Visible = true;

                // Stop processing                
                plcTilesView.Visible = false;
                plcThumbnailsView.Visible = false;
                break;

            case DialogViewModeEnum.TilesView:
                plcTilesView.Visible = true;
                repTilesView.DataBindByDefault = false;
                pagerElemTiles.PageControl = repTilesView.ID;

                // Stop processing
                plcListView.Visible = false;
                plcThumbnailsView.Visible = false;
                break;

            case DialogViewModeEnum.ThumbnailsView:
                plcThumbnailsView.Visible = true;
                repThumbnailsView.DataBindByDefault = false;
                pagerElemThumbnails.PageControl = repThumbnailsView.ID;

                // Stop processing
                plcListView.Visible = false;
                plcTilesView.Visible = false;
                break;
        }

        // Display mass actions drop-down list if displayed for MediaLibrary UI
        if (!IsCopyMoveLinkDialog && (DisplayMode == ControlDisplayModeEnum.Simple))
        {
            plcMassAction.Visible = true;

            InitializeMassActions();
        }
        else
        {
            plcMassAction.Visible = false;
        }
    }

    #endregion


    #region "Mass actions methods"

    /// <summary>
    /// Initializes mass actions drop-down list with available actions.
    /// </summary>
    private void InitializeMassActions()
    {
        string actionScript = "function RaiseMassAction(drpActionsClientId, drpActionFilesClientId) {   " +
            " var drpActions = document.getElementById(drpActionsClientId);                             " +
            " var drpActionFiles = document.getElementById(drpActionFilesClientId);                     " +
            " if((drpActions != null) && (drpActionFiles != null)) {                                    " +
            "   var selectedFiles = drpActionFiles.options[drpActionFiles.selectedIndex];               " +
            "   var selectedAction = drpActions.options[drpActions.selectedIndex];                      " +
            "   if((selectedAction != null) && (selectedFiles != null)) {                               " +
            "       var argument = selectedAction.value + '|' + selectedFiles.value;                    " +
            "       SetAction('massaction', argument);                                                  " +
            "       RaiseHiddenPostBack();                                                              " +
            "   }                                                                                       " +
            " }                                                                                         " +
            "                                                                               }           ";

        ScriptHelper.RegisterStartupScript(Page, typeof(Page), "LibraryActionScript", ScriptHelper.GetScript(actionScript));

        if (drpActionFiles.Items.Count == 0)
        {
            // Actions dropdown
            drpActionFiles.Items.Add(new ListItem(ResHelper.GetString("media.file.list.lblactions"), "selected"));
            drpActionFiles.Items.Add(new ListItem(ResHelper.GetString("media.file.list.filesall"), "all"));
        }

        if (drpActions.Items.Count == 0)
        {
            // Actions dropdown
            drpActions.Items.Add(new ListItem(ResHelper.GetString("General.SelectAction"), ""));
            drpActions.Items.Add(new ListItem(ResHelper.GetString("media.file.copy"), "copy"));
            drpActions.Items.Add(new ListItem(ResHelper.GetString("media.file.move"), "move"));
            drpActions.Items.Add(new ListItem(ResHelper.GetString("General.Delete"), "delete"));
            drpActions.Items.Add(new ListItem(ResHelper.GetString("media.file.import"), "import"));
        }

        btnActions.OnClientClick = "if(MassConfirm('" + drpActions.ClientID + "', '" + ResHelper.GetString("General.ConfirmGlobalDelete") + "')) { RaiseMassAction('" + drpActions.ClientID + "', '" + drpActionFiles.ClientID + "'); } return false;";
    }


    /// <summary>
    /// Returns list of names of selected files.
    /// </summary>
    private ArrayList GetSelectedItems()
    {
        switch (ViewMode)
        {
            case DialogViewModeEnum.ListView:
                return gridList.SelectedItems;

            case DialogViewModeEnum.TilesView:
            case DialogViewModeEnum.ThumbnailsView:
                return GetTilesThumbsSelectedItems();
        }

        return null;
    }


    /// <summary>
    /// Returns list of names of files selected in tiles view mode.
    /// </summary>
    private ArrayList GetTilesThumbsSelectedItems()
    {
        ArrayList result = new ArrayList();
        BasicRepeater repElem = (ViewMode == DialogViewModeEnum.TilesView) ? repTilesView : repThumbnailsView;

        // Go through all repeater items and look for selected ones
        foreach (RepeaterItem item in repElem.Items)
        {
            LocalizedCheckBox chkSelected = item.FindControl("chkSelected") as LocalizedCheckBox;
            if ((chkSelected != null) && chkSelected.Checked)
            {
                HiddenField hdnItemName = item.FindControl("hdnItemName") as HiddenField;
                if (hdnItemName != null)
                {
                    string alt = hdnItemName.Value;
                    result.Add(alt);
                }
            }
        }

        return result;
    }


    /// <summary>
    /// Ensures given file name in the way it is usable as ID.
    /// </summary>
    /// <param name="fileName">Name of the file to ensure.</param>
    private string EnsureFileName(string fileName)
    {
        if (!string.IsNullOrEmpty(fileName))
        {
            char[] specialChars = "#;&,.+*~':\"!^$[]()=>|/\\-%@`".ToCharArray();
            foreach (char specialChar in specialChars)
            {
                fileName = fileName.Replace(specialChar, '_');
            }
            return fileName.Replace(" ", "").ToLower();
        }

        return fileName;
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Initializes all nested controls.
    /// </summary>
    private void SetupControls()
    {
        // Initialize displaying controls according view mode
        LoadViewControls();

        InitializeControlScripts();

        // Select mode according required
        switch (ViewMode)
        {
            case DialogViewModeEnum.ListView:
                InitializeListView();
                ListViewControl.OnExternalDataBound += new OnExternalDataBoundEventHandler(ListViewControl_OnExternalDataBound);
                ListViewControl.GridView.RowDataBound += new GridViewRowEventHandler(GridView_RowDataBound);
                break;

            case DialogViewModeEnum.TilesView:
                InitializeTilesView();
                TilesViewControl.ItemDataBound += new RepeaterItemEventHandler(TilesViewControl_ItemDataBound);
                break;

            case DialogViewModeEnum.ThumbnailsView:
                InitializeThumbnailsView();
                ThumbnailsViewControl.ItemDataBound += new RepeaterItemEventHandler(ThumbnailsViewControl_ItemDataBound);
                break;
        }

        // Register delete confirmation script
        ltlScript.Text = ScriptHelper.GetScript("function DeleteConfirmation(){ return confirm('" + ResHelper.GetString("attach.deleteconfirmation") + "'); } function DeleteMediaFileConfirmation(){ return confirm('" + ResHelper.GetString("general.deleteconfirmation") + "'); }");
    }


    /// <summary>
    /// Initializes all the necessary JavaScript blocks.
    /// </summary>
    private void InitializeControlScripts()
    {
        string activeBackgroundPath = "CMSModules/CMS_Content/Dialogs/";


        string script = @"  var activeBackgroundList = 'url(" + ResolveUrl(GetImageUrl(activeBackgroundPath + "highlightline.png")) + @")'; 
                            var activeBackgroundTiles = 'url(" + ResolveUrl(GetImageUrl(activeBackgroundPath + "highlighttiles.png")) + @")';   
                            var activeBackgroundThumbs = 'url(" + ResolveUrl(GetImageUrl(activeBackgroundPath + "highlightthumbs.png")) + @")'; 
                            var attemptNo = 0;
                            function ColorizeRow(itemId) {
                                if (itemId != null) 
                                {   
                                    var hdnField = document.getElementById('" + hdnItemToColorize.ClientID + "');" +
                         @"         if (hdnField != null) 
                                    {
                                        // If some item was previously selected
                                        if ((hdnField.value != null) && (hdnField.value != '')) 
                                        {   
                                            // Get selected item and reset its selection
                                            var lastColorizedElem = document.getElementById(hdnField.value);
                                            if (lastColorizedElem != null) 
                                            {   
                                                ColorizeElement(lastColorizedElem, '', itemId);
                                            }
                                         }
                                        
                                         // Update field value
                                         hdnField.value = itemId;
                                     }                                                              

                                    // Colorize currently selected item
                                    var elem = document.getElementById(itemId);
                                    if (elem != null) 
                                    {
                                        ColorizeElement(elem, activeBackgroundList, itemId);
                                        attemptNo = 0;
                                    }
                                    else
                                    {
                                        if(attemptNo < 1)
                                        {
                                            setTimeout('ColorizeRow(\'' + itemId + '\')', 300);
                                            attemptNo = attemptNo + 1;
                                        }
                                        else
                                        {
                                            attemptNo = 0;
                                        }
                                    }
                                 }
                             }

                             function ColorizeLastRow() {
                                var hdnField = document.getElementById('" + hdnItemToColorize.ClientID + "');" +
                        @"      if (hdnField != null) 
                                {
                                    // If some item was previously selected
                                    if ((hdnField.value != null) && (hdnField.value != '')) 
                                    {               
                                        // Get selected item and reset its selection
                                        var lastColorizedElem = document.getElementById(hdnField.value);
                                        if (lastColorizedElem != null) 
                                        {    
                                            ColorizeElement(lastColorizedElem, activeBackgroundList);
                                        }
                                    }
                                }
                            }

                            function ColorizeElement(elem, bgImage, itemId) {
                                if((bgImage != null) && (bgImage != '')){
                                    if (elem.className == 'DialogTileItem') {
                                       bgImage = activeBackgroundTiles; 
                                    }
                                    else if (elem.className == 'DialogThumbnailItem') {
                                       bgImage = activeBackgroundThumbs; 
                                    }
                                    else {
                                       bgImage = activeBackgroundList; 
                                    }
                                }
                                elem.style.backgroundImage = bgImage;
                            }

                            function ClearColorizedRow()
                            {
                                var hdnField = document.getElementById('" + hdnItemToColorize.ClientID + "');" +
                        @"      if (hdnField != null) 
                                {
                                    // If some item was previously selected
                                    if ((hdnField.value != null) && (hdnField.value != '')) 
                                    {               
                                        // Get selected item and reset its selection
                                        var lastColorizedElem = document.getElementById(hdnField.value);
                                        if (lastColorizedElem != null) 
                                        {   
                                            ColorizeElement(lastColorizedElem, '');
             
                                            // Update field value
                                            hdnField.value = '';                                    
                                        }
                                    }
                                }                                
                            }
                          ";

        ScriptHelper.RegisterStartupScript(this, GetType(), "DialogsColorize", ScriptHelper.GetScript(script));

        // Dialog for editing image
        string url = "";
        if (SourceType == MediaSourceEnum.MediaLibraries)
        {
            if (IsLiveSite)
            {
                if (CMSContext.CurrentUser.IsAuthenticated())
                {
                    url = ResolveUrl("~/CMSModules/MediaLibrary/CMSPages/ImageEditor.aspx");
                }
                else
                {
                    url = ResolveUrl("~/CMSModules/MediaLibrary/CMSPages/PublicImageEditor.aspx");
                }
            }
            else
            {
                url = ResolveUrl("~/CMSModules/MediaLibrary/Controls/MediaLibrary/ImageEditor.aspx");
            }
        }
        else
        {
            if (IsLiveSite)
            {
                if (CMSContext.CurrentUser.IsAuthenticated())
                {
                    url = ResolveUrl("~/CMSFormControls/LiveSelectors/ImageEditor.aspx");
                }
                else
                {
                    url = ResolveUrl("~/CMSFormControls/LiveSelectors/PublicImageEditor.aspx");
                }
            }
            else
            {
                url = ResolveUrl("~/CMSModules/Content/CMSDesk/Edit/ImageEditor.aspx");
            }
        }

        string editImage = "function EditImage(param) {                                         " +
                "var form = ''; \n                                                              " +
                "if (param.indexOf('?') != 0) { param = '?'+param; }                            " +
                "modalDialog('" + url + "' + param, 'imageEditorDialog', 905, 670, undefined, true); return false; }";

        ScriptHelper.RegisterStartupScript(this, GetType(), "DialogsEditImage", ScriptHelper.GetScript(editImage));
    }


    /// <summary>
    /// Loads data from the data source property.
    /// </summary>
    public void ReloadData()
    {
        // Select mode according required
        switch (ViewMode)
        {
            case DialogViewModeEnum.ListView:
                ReloadListView();
                break;

            case DialogViewModeEnum.TilesView:
                ReloadTilesView();
                break;

            case DialogViewModeEnum.ThumbnailsView:
                ReloadThumbnailsView();
                break;
        }
    }


    /// <summary>
    /// Reloads control with data.
    /// </summary>
    /// <param name="forceSetup">Indicates whether the inner controls should be re-setuped.</param>
    public void Reload(bool forceSetup)
    {
        Visible = !StopProcessing;
        if (Visible)
        {
            if (forceSetup)
            {
                // Initialize controls
                SetupControls();
            }

            // Load passed data
            ReloadData();
        }
    }


    /// <summary>
    /// Returns the sitename according to item info.
    /// </summary>
    /// <param name="itemData">Row containing information on the current item.</param>
    /// <param name="isMediaFile">Indicates whether the file is media file.</param>
    private string GetSiteName(DataRow itemData, bool isMediaFile)
    {
        int siteId = 0;
        string result = "";

        if (isMediaFile)
        {
            if (itemData.Table.Columns.Contains("FileSiteID"))
            {
                // Imported media file
                siteId = ValidationHelper.GetInteger(itemData["FileSiteID"], 0);
            }
            else
            {
                // Npt imported yet
                siteId = RaiseOnSiteIdRequired();
            }
        }
        else
        {
            if (itemData.Table.Columns.Contains("SiteName"))
            {
                // Content file
                result = ValidationHelper.GetString(itemData["SiteName"], "");
            }
            else
            {
                if (itemData.Table.Columns.Contains("AttachmentSiteID"))
                {
                    // Non-versioned attachment
                    siteId = ValidationHelper.GetInteger(itemData["AttachmentSiteID"], 0);
                }
                else if (TreeNodeObj != null)
                {
                    // Versioned attachment
                    siteId = TreeNodeObj.NodeSiteID;
                }
            }
        }

        if (result == "")
        {
            if (!ContextStockHelper.Contains("DialogsSiteName") || (mLastSiteId != siteId))
            {
                SiteInfo si = SiteInfoProvider.GetSiteInfo(siteId);
                if (si != null)
                {
                    mLastSiteId = si.SiteID;
                    result = si.SiteName;
                    ContextStockHelper.Add("DialogsSiteName", result);
                }
            }
            else
            {
                result = ValidationHelper.GetString(ContextStockHelper.GetItem("DialogsSiteName"), "");
            }
        }

        return result;
    }


    /// <summary>
    /// Gets domain name of the site items are related to.
    /// </summary>
    /// <param name="siteId">ID of the site current item is related to.</param>
    private static object GetLastModified(int attachmentDocumentId)
    {
        object result = "";

        // Make sure information on site domain name is available in the context
        if (!ContextStockHelper.Contains("DialogsAttachmentLastModified"))
        {
            if (attachmentDocumentId > 0)
            {
                TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                TreeNode node = DocumentHelper.GetDocument(attachmentDocumentId, tree);
                if (node != null)
                {
                    result = node.GetValue("DocumentModifiedWhen");
                }
            }

            // Save site name information for future use
            ContextStockHelper.Add("DialogsAttachmentLastModified", result);
        }
        else
        {
            // Get it from stock
            result = ContextStockHelper.GetItem("DialogsAttachmentLastModified");
        }

        return result;
    }


    /// <summary>
    /// Initializes attachment update control according current attachment data.
    /// </summary>
    /// <param name="drv">DataRowView holding attachment data.</param>
    private void GetAttachmentUpdateControl(ref DirectFileUploader dfuElem, DataRowView drv)
    {
        if (dfuElem != null)
        {
            string refreshType = CMSDialogHelper.GetMediaSource(SourceType);
            Guid formGuid = Guid.Empty;
            int documentId = ValidationHelper.GetInteger(drv["AttachmentDocumentID"], 0);

            // If attachment is related to the workflow 'AttachmentFormGUID' information isn't present
            if (drv.DataView.Table.Columns.Contains("AttachmentFormGUID"))
            {
                formGuid = ValidationHelper.GetGuid(drv["AttachmentFormGUID"], Guid.Empty);
            }

            dfuElem.ID = "dfuElem";
            dfuElem.SourceType = MediaSourceEnum.DocumentAttachments;
            dfuElem.ForceLoad = true;
            dfuElem.FormGUID = formGuid;
            dfuElem.DocumentID = documentId;

            if (TreeNodeObj != null)
            {
                // if attachment node exists
                dfuElem.NodeParentNodeID = TreeNodeObj.NodeParentID;
                dfuElem.NodeClassName = TreeNodeObj.NodeClassName;
            }
            else
            {
                // if attachment node doesn't exist
                dfuElem.NodeParentNodeID = NodeParentID;
                dfuElem.NodeClassName = "cms.file";
            }

            dfuElem.ControlGroup = "MediaView";
            dfuElem.AttachmentGUID = ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty);
            dfuElem.ResizeToWidth = ResizeToWidth;
            dfuElem.ResizeToHeight = ResizeToHeight;
            dfuElem.ResizeToMaxSideSize = ResizeToMaxSideSize;
            dfuElem.AllowedExtensions = AllowedExtensions;
            dfuElem.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/upload.png"));
            dfuElem.LoadingImageUrl = GetImageUrl("Design/Preloaders/preload16.gif");
            dfuElem.ImageHeight = 16;
            dfuElem.ImageWidth = 16;
            dfuElem.InsertMode = false;
            dfuElem.ParentElemID = refreshType;
            dfuElem.IncludeNewItemInfo = true;
        }
    }


    /// <summary>
    /// Initializes upload control.
    /// </summary>
    /// <param name="dfuElem">Upload control to initialize.</param>
    /// <param name="dr">Data row with data on related media file.</param>
    public void GetLibraryUpdateControl(ref DirectFileUploader dfuElem, DataRow dr)
    {
        if (dfuElem != null)
        {
            string siteName = GetSiteName(dr, true);
            int fileId = ValidationHelper.GetInteger(dr["FileID"], 0);
            string fileName = EnsureFileName(Path.GetFileName(ValidationHelper.GetString(dr["FilePath"], "")));
            string folderPath = Path.GetDirectoryName(ValidationHelper.GetString(dr["FilePath"], ""));
            int libraryId = ValidationHelper.GetInteger(dr["FileLibraryID"], 0);

            AllowedExtensions = SettingsKeyProvider.GetStringValue(siteName + ".CMSMediaFileAllowedExtensions");

            // Initialize library info
            dfuElem.LibraryID = libraryId;
            dfuElem.MediaFileID = fileId;
            dfuElem.MediaFileName = fileName;
            dfuElem.LibraryFolderPath = folderPath;

            // Initialize general info
            dfuElem.SourceType = MediaSourceEnum.MediaLibraries;
            dfuElem.ID = "dfuElemLib";
            dfuElem.ForceLoad = true;
            dfuElem.ControlGroup = "MediaView";
            dfuElem.ResizeToWidth = ResizeToWidth;
            dfuElem.ResizeToHeight = ResizeToHeight;
            dfuElem.ResizeToMaxSideSize = ResizeToMaxSideSize;
            dfuElem.AllowedExtensions = AllowedExtensions;
            dfuElem.ImageUrl = GetImageUrl("Design/Controls/DirectUploader/upload.png");
            dfuElem.LoadingImageUrl = GetImageUrl("Design/Preloaders/preload16.gif");
            dfuElem.ImageHeight = 16;
            dfuElem.ImageWidth = 16;
            dfuElem.InsertMode = false;
            dfuElem.ParentElemID = "LibraryUpdate";
            dfuElem.IncludeNewItemInfo = true;
            dfuElem.RaiseOnClick = true;
        }
    }


    /// <summary>
    /// Returns correct ID for the item (for colorizing the item when selected).
    /// </summary>
    /// <param name="dataItem">Container.DataItem</param>
    protected string GetID(object dataItem)
    {
        DataRowView dr = dataItem as DataRowView;
        if (dr != null)
        {
            return GetColorizeID(dr.Row);
        }
        return "";
    }


    /// <summary>
    /// Returns correct ID for the given item (for colorizing the item when selected).
    /// </summary>
    /// <param name="dr">Item to get the ID of</param>
    protected string GetColorizeID(DataRow dr)
    {
        string id = "";
        if (dr.Table.Columns.Contains(FileIdColumn))
        {
            id = dr[FileIdColumn].ToString();
        }
        else
        {
            // Not imported media file
            id = EnsureFileName(dr["FileName"].ToString());
        }

        if (String.IsNullOrEmpty(id))
        {
            // Content file
            id = dr["NodeGUID"].ToString();
        }
        return id.ToLower();
    }


    /// <summary>
    /// Gets file name according available columns.
    /// </summary>
    /// <param name="drv">DataRowView containing data.</param>
    private string GetFileName(DataRow dr)
    {
        string fileName = "";

        if (dr != null)
        {
            if (dr.Table.Columns.Contains("FileExtension"))
            {
                fileName = dr["FileName"].ToString() + dr["FileExtension"].ToString();
            }
            else
            {
                fileName = dr[FileNameColumn].ToString();
            }
        }

        return fileName;
    }


    /// <summary>
    /// Ensures correct format of extension being displayed.
    /// </summary>
    /// <param name="extension">Extension to normalize.</param>
    private string NormalizeExtenison(string extension)
    {
        if (!string.IsNullOrEmpty(extension))
        {
            if (extension.Trim() != "<dir>")
            {
                extension = "." + extension.ToLower().TrimStart('.');
            }
            else
            {
                extension = HTMLHelper.HTMLEncode("<DIR>");
            }
        }

        return extension;
    }

    #endregion


    #region "List view methods"

    /// <summary>
    /// Initializes list view controls.
    /// </summary>
    private void InitializeListView()
    {
        switch (SourceType)
        {
            case MediaSourceEnum.Content:
                gridList.OrderBy = "NodeOrder";
                break;

            case MediaSourceEnum.MediaLibraries:
                gridList.OrderBy = "FileName";
                break;

            case MediaSourceEnum.DocumentAttachments:
                gridList.OrderBy = "AttachmentOrder, AttachmentName, AttachmentID";
                break;

            default:
                break;
        }

        gridList.OnBeforeDataReload += new OnBeforeDataReload(gridList_OnBeforeDataReload);
    }


    void gridList_OnBeforeDataReload()
    {
        gridList.PagerForceNumberOfResults = DataHelper.GetItemsCount(DataSource);
        gridList.DataSource = DataSource;
    }


    /// <summary>
    /// Reloads list view according source type.
    /// </summary>
    private void ReloadListView()
    {
        // Fill the grid data source
        if (!DataHelper.DataSourceIsEmpty(DataSource))
        {
            // Disable sorting if is copy/move dialog
            if ((IsCopyMoveLinkDialog) && (DisplayMode == ControlDisplayModeEnum.Simple))
            {
                gridList.GridView.AllowSorting = false;
            }
            gridList.ReloadData();
        }
    }


    /// <summary>
    /// Ensures no item is selected.
    /// </summary>
    public void ResetListSelection()
    {
        if (gridList != null)
        {
            gridList.ResetSelection();
        }
    }


    /// <summary>
    /// Ensures first page is displayed in the control displaying the content.
    /// </summary>
    public void ResetPageIndex()
    {
        if (this.ViewMode == DialogViewModeEnum.ListView)
        {
            this.ListViewControl.Pager.UniPager.CurrentPage = 1;
        }
    }


    /// <summary>
    /// Returns panel with image according extension of the processed file. 
    /// </summary>
    /// <param name="ext">Extension of the file used to determine icon.</param>
    /// <param name="url">Url of the original image.</param>
    /// <param name="item">Control inserted as a file name.</param>
    /// <param name="width">Image width</param>
    /// <param name="height">Image height</param>
    private Panel GetListItem(string ext, string className, string url, string previewUrl, int width, int height, Control item, bool isSelectable, bool isImported)
    {
        Panel pnl = new Panel();
        pnl.CssClass = "DialogListItem" + (isSelectable ? "" : "Unselectable");
        pnl.Controls.Add(new LiteralControl("<div class=\"DialogListItemNameRow\">"));

        // Cast media library folder as 'CMS.Folder'
        if ((SourceType == MediaSourceEnum.MediaLibraries) && ext.ToLower() == "<dir>")
        {
            className = "cms.folder";
        }

        if ((className == "cms.file") || (className == ""))
        {
            // Generate image based of file extension
            InlineUserControl fileImage = Page.LoadControl("~/CMSInlineControls/ImageControl.ascx") as InlineUserControl;
            if (fileImage != null)
            {
                if (!isSelectable)
                {
                    fileImage.SetValue("Style", "cursor: default;");
                }

                fileImage.IsLiveSite = false;
                fileImage.SetValue("ShowFileIcons", true);
                if (!String.IsNullOrEmpty(previewUrl))
                {
                    fileImage.SetValue("Url", ResolveUrl(previewUrl));
                }
                fileImage.SetValue("Extension", ext);

                if (isImported && ImageHelper.IsImage(ext))
                {
                    if ((width > 0) && (height > 0))
                    {
                        int[] dimensions = ImageHelper.EnsureImageDimensions(0, 0, 200, width, height);

                        fileImage.SetValue("Behavior", "hover");
                        fileImage.SetValue("MouseOverWidth", dimensions[0]);
                        fileImage.SetValue("MouseOverHeight", dimensions[1]);
                        fileImage.SetValue("ImageID", "Image" + idCount++);
                    }
                }
                if ((isSelectable) && (item is LinkButton))
                {
                    // Create clickabe compelte panel
                    pnl.Attributes["onclick"] = ((LinkButton)item).Attributes["onclick"];
                    ((LinkButton)item).Attributes["onclick"] = null;
                }
                pnl.Controls.Add(fileImage);
            }
        }
        else
        {
            Image docImg = new Image();

            docImg.ImageUrl = GetDocumentTypeIconUrl(className);
            docImg.Attributes["style"] = "width: 16px; height: 16px;";

            pnl.Controls.Add(docImg);

            if ((isSelectable) && (item is LinkButton))
            {
                // Create clickabe compelte panel
                pnl.Attributes["onclick"] = ((LinkButton)item).Attributes["onclick"];
                ((LinkButton)item).Attributes["onclick"] = null;
            }
        }

        // Add file name                  
        pnl.Controls.Add(new LiteralControl("&nbsp;<span class=\"DialogListItemName\" " + (!isSelectable ? "style=\"cursor:default;\"" : "") + ">"));
        pnl.Controls.Add(item);
        pnl.Controls.Add(new LiteralControl("</span></div>"));

        return pnl;
    }


    protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = (e.Row.DataItem as DataRowView);
            if (drv != null)
            {
                e.Row.Attributes["id"] = GetColorizeID(drv.Row);
            }
        }
    }


    protected object ListViewControl_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        object result = null;

        string argument = "";
        string url = "";

        bool isContent = (SourceType == MediaSourceEnum.Content);
        bool notAttachment = false;
        bool isContentFile = false;
        bool isSelectable = false;

        sourceName = sourceName.ToLower();

        switch (sourceName)
        {
            #region "Select"

            case "select":
                GridViewRow gvr = (parameter as GridViewRow);
                DataRowView drv = (DataRowView)gvr.DataItem;
                ImageButton btn = ((ImageButton)sender);

                string className = "";

                // Is current item CMS.File or attachment ?
                isContentFile = isContent ? (drv["ClassName"].ToString().ToLower() == "cms.file") : false;
                notAttachment = isContent && !(isContentFile && (ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty) != Guid.Empty));
                if (notAttachment)
                {
                    className = DataClassInfoProvider.GetDataClass((int)drv["NodeClassID"]).ClassDisplayName;
                }

                string ext = HTMLHelper.HTMLEncode(notAttachment ? className : drv[FileExtensionColumn].ToString().TrimStart('.'));

                // Get file name
                string fileName = GetFileName(drv.Row);
                bool libraryFolder = ((SourceType == MediaSourceEnum.MediaLibraries) && IsFullListingMode &&
                                        (drv[FileExtensionColumn].ToString().ToLower() == "<dir>"));

                // If media file not imported yet - display warning sign
                if ((SourceType == MediaSourceEnum.MediaLibraries) && ((RaiseOnFileIsNotInDatabase(fileName) == null) && (DisplayMode == ControlDisplayModeEnum.Simple)))
                {
                    // If folders are displayed as well show SELECT button icon
                    if (libraryFolder && !IsCopyMoveLinkDialog)
                    {
                        btn.ImageUrl = ResolveUrl(ImagesPath + "subdocument.png");
                        btn.ToolTip = ResHelper.GetString("dialogs.list.actions.showsubfolders");
                        btn.Attributes["onclick"] = "SetAction('morefolderselect', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                    }
                    else if (libraryFolder && IsCopyMoveLinkDialog)
                    {
                        btn.ImageUrl = ResolveUrl(ImagesPath + "next.png");
                        btn.ToolTip = ResHelper.GetString("general.select");
                        btn.Attributes["onclick"] = "SetAction('copymoveselect', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                    }
                    else
                    {
                        btn.ImageUrl = ResolveUrl(ImagesPath + "warning.png");
                        btn.ToolTip = ResHelper.GetString("media.file.import");
                        btn.Attributes["onclick"] = "SetAction('importfile', " + ScriptHelper.GetString(drv["FileName"].ToString()) + "); RaiseHiddenPostBack(); return false;";
                    }
                }
                else
                {
                    // Check if item is selectable, if not remove select action button
                    isSelectable = CMSDialogHelper.IsItemSelectable(SelectableContent, ext, isContentFile);
                    if (!isSelectable)
                    {
                        btn.ImageUrl = ResolveUrl(ImagesPath + "transparent.png");
                        btn.ToolTip = "";
                        btn.Attributes["style"] = "margin:0px 3px;cursor:default;";
                        btn.Enabled = false;
                    }
                    else
                    {
                        argument = RaiseOnGetArgumentSet(drv.Row);

                        // Get item URL
                        url = RaiseOnGetListItemUrl(drv.Row, false, notAttachment);

                        // Initialize command
                        btn.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(drv.Row)) + ");\n SetSelectAction(" + ScriptHelper.GetString(argument + "|" + url) + ");\n return false;";

                        result = btn;
                    }
                }
                break;

            #endregion

            #region "Select sub docs"

            case "selectsubdocs":
                drv = (DataRowView)(parameter as GridViewRow).DataItem;
                btn = ((ImageButton)sender);

                int nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);

                if (IsFullListingMode)
                {
                    // Check if item is selectable, if not remove select action button
                    // Initialize command
                    btn.Attributes["onclick"] = "SetParentAction('" + nodeId + "'); return false;";
                }
                else
                {
                    btn.Visible = false;
                }
                break;

            #endregion

            #region "Select sub folders"

            case "selectsubfolders":
                drv = (DataRowView)(parameter as GridViewRow).DataItem;

                btn = ((ImageButton)sender);
                if (btn != null)
                {
                    string folderName = ValidationHelper.GetString(drv["FileName"], "");
                    ext = ValidationHelper.GetString(drv[FileExtensionColumn], "");

                    if (IsCopyMoveLinkDialog || (IsFullListingMode && (DisplayMode == ControlDisplayModeEnum.Default) && (ext == "<dir>")))
                    {
                        // Initialize command
                        btn.Attributes["onclick"] = "SetLibParentAction(" + ScriptHelper.GetString(folderName) + "); return false;";
                    }
                    else
                    {
                        btn.Visible = false;
                    }
                }
                break;

            #endregion

            #region "View"

            case "view":
                gvr = (parameter as GridViewRow);
                drv = (DataRowView)gvr.DataItem;

                btn = ((ImageButton)sender);

                // Check if current item is library folder
                fileName = GetFileName(drv.Row);
                libraryFolder = ((SourceType == MediaSourceEnum.MediaLibraries) && IsFullListingMode && (drv[FileExtensionColumn].ToString().ToLower() == "<dir>"));
                bool libraryUiFolder = libraryFolder && ((RaiseOnFileIsNotInDatabase(fileName) == null) && (DisplayMode == ControlDisplayModeEnum.Simple));

                // Is current item CMS.File or attachment ?
                isContentFile = isContent ? (drv["ClassName"].ToString().ToLower() == "cms.file") : false;
                notAttachment = isContent && !(isContentFile && (ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty) != Guid.Empty));
                if (!notAttachment && !libraryFolder && !libraryUiFolder)
                {
                    argument = RaiseOnGetArgumentSet(drv.Row);

                    // Get item URL
                    url = RaiseOnGetListItemUrl(drv.Row, false, notAttachment);

                    if (String.IsNullOrEmpty(url))
                    {
                        btn.ImageUrl = GetImageUrl("/Design/Controls/UniGrid/Actions/Viewdisabled.png");
                        btn.OnClientClick = "return false;";
                        btn.Attributes["style"] = "margin:0px 3px;cursor:default;";
                        btn.Enabled = false;
                    }
                    else
                    {
                        // Add latest version requirement for live site
                        int versionHistoryId = VersionHistoryID;
                        if (IsLiveSite && (versionHistoryId > 0))
                        {
                            // Add requirement for latest version of files for current document
                            string newparams = "latestforhistoryid=" + versionHistoryId;
                            newparams += "&hash=" + ValidationHelper.GetHashString("h" + versionHistoryId);

                            url = UrlHelper.AppendQuery(url, newparams);
                        }

                        btn.OnClientClick = "javascript: window.open(" + UrlHelper.ResolveUrl(ScriptHelper.GetString(url)) + "); return false;";
                    }
                }
                else
                {
                    btn.Visible = false;
                }
                break;

            #endregion

            #region "Edit"

            case "edit":
                gvr = (parameter as GridViewRow);
                drv = (DataRowView)gvr.DataItem;

                // Is current item CMS.File or attachment ?
                isContentFile = isContent ? (drv["ClassName"].ToString().ToLower() == "cms.file") : false;
                notAttachment = isContent && !(isContentFile && (ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty) != Guid.Empty));

                // Get file extension
                ext = (notAttachment ? "" : drv[FileExtensionColumn].ToString().TrimStart('.'));
                Guid guid = Guid.Empty;
                ImageButton imgEdit = (ImageButton)sender;
                if (SourceType == MediaSourceEnum.MediaLibraries)
                {
                    libraryFolder = (IsFullListingMode && (drv[FileExtensionColumn].ToString().ToLower() == "<dir>"));

                    if (!libraryFolder)
                    {
                        guid = ValidationHelper.GetGuid(drv["FileGUID"], Guid.Empty);
                        imgEdit.AlternateText = ext + "|MediaFileGUID=" + guid + "&sitename=" + GetSiteName(drv.Row, true);
                        imgEdit.PreRender += new EventHandler(img_PreRender);
                        imgEdit.ImageUrl = ResolveUrl(ImagesPath + "edit.png");
                    }
                    else
                    {
                        imgEdit.Visible = false;
                    }
                }
                else if (!notAttachment)
                {
                    nodeId = ValidationHelper.GetInteger(drv["NodeID"], 0);

                    string nodeIdQuery = "";
                    if (SourceType == MediaSourceEnum.Content)
                    {
                        nodeIdQuery = "&nodeId=" + drv["NodeID"].ToString();
                    }

                    // Get the node workflow
                    VersionHistoryID = ValidationHelper.GetInteger(drv["DocumentCheckedOutVersionHistoryID"], 0);

                    guid = ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty);
                    imgEdit.AlternateText = ext + "|AttachmentGUID=" + guid + "&sitename=" + GetSiteName(drv.Row, false) + nodeIdQuery + "&versionHistoryId=" + VersionHistoryID;
                    imgEdit.PreRender += new EventHandler(img_PreRender);
                    imgEdit.ImageUrl = ResolveUrl(ImagesPath + "edit.png");
                }
                else
                {
                    imgEdit.Visible = false;
                }
                break;

            #endregion

            #region "Edit library ui"

            case "editlibraryui":
                imgEdit = (ImageButton)sender;
                if (imgEdit != null)
                {
                    gvr = (parameter as GridViewRow);
                    drv = (DataRowView)gvr.DataItem;

                    // Get file name
                    fileName = GetFileName(drv.Row);

                    bool notInDatabase = ((RaiseOnFileIsNotInDatabase(fileName) == null) && (DisplayMode == ControlDisplayModeEnum.Simple));

                    // Check if current item is library folder
                    libraryFolder = ((SourceType == MediaSourceEnum.MediaLibraries) && IsFullListingMode &&
                         notInDatabase && (drv[FileExtensionColumn].ToString().ToLower() == "<dir>"));

                    ext = "";
                    if (drv.DataView.Table.Columns.Contains("FileExtension"))
                    {
                        ext = drv["FileExtension"].ToString();
                    }
                    else
                    {
                        ext = drv["Extension"].ToString();
                    }

                    if (libraryFolder)
                    {
                        imgEdit.Visible = false;
                    }
                    else if (!ImageHelper.IsSupportedByImageEditor(ext) || notInDatabase)
                    {
                        imgEdit.ImageUrl = ResolveUrl(ImagesPath + "editdisabled.png");
                        imgEdit.Attributes["style"] = "margin:0px 3px;cursor:default;";
                        imgEdit.Enabled = false;
                    }
                    else
                    {
                        imgEdit.OnClientClick = "$j(\"#hdnFileOrigName\").attr('value', '" + EnsureFileName(fileName) + "'); SetAction('editlibraryui', '" + fileName + "'); RaiseHiddenPostBack(); return false;";
                    }
                }
                break;

            #endregion

            #region "Delete"

            case "delete":
                gvr = (parameter as GridViewRow);
                drv = (DataRowView)gvr.DataItem;

                // Get file name
                fileName = GetFileName(drv.Row);

                // Check if current item is library folder
                libraryFolder = ((SourceType == MediaSourceEnum.MediaLibraries) && IsFullListingMode &&
                    (drv[FileExtensionColumn].ToString().ToLower() == "<dir>"));

                ImageButton btnDelete = (ImageButton)sender;
                if (btnDelete != null)
                {
                    //btnDelete.ImageUrl = ResolveUrl(ImagesPath + "Delete.png");
                    btnDelete.ToolTip = ResHelper.GetString("general.delete");

                    if (((RaiseOnFileIsNotInDatabase(fileName) == null) && (DisplayMode == ControlDisplayModeEnum.Simple)))
                    {
                        if (libraryFolder)
                        {
                            btnDelete.Visible = false;
                        }
                        else
                        {
                            btnDelete.Attributes["onclick"] = "if(DeleteMediaFileConfirmation() == false){return false;} SetAction('deletefile','" + fileName + "'); RaiseHiddenPostBack(); return false;";
                        }
                    }
                    else
                    {
                        btnDelete.Attributes["onclick"] = "if(DeleteMediaFileConfirmation() == false){return false;} SetAction('deletefile','" + fileName + "'); RaiseHiddenPostBack(); return false;";
                    }
                }
                break;

            #endregion

            #region "Name"

            case "name":
                drv = (DataRowView)parameter;

                // Is current item CMS.File or attachment ?
                isContentFile = isContent ? (drv["ClassName"].ToString().ToLower() == "cms.file") : false;
                notAttachment = isContent && !(isContentFile && (ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty) != Guid.Empty));

                // Get name and extension                
                string fileNameColumn = FileNameColumn;
                if (notAttachment)
                {
                    fileNameColumn = "DocumentName";
                }
                string name = HTMLHelper.HTMLEncode(drv[fileNameColumn].ToString());
                ext = (notAttachment ? "" : drv[FileExtensionColumn].ToString().TrimStart('.'));

                if (this.SourceType == MediaSourceEnum.DocumentAttachments)
                {
                    name = Path.GetFileNameWithoutExtension(name);
                }

                // Width & height
                int width = 0;
                if (drv.DataView.Table.Columns.Contains(FileWidthColumn))
                {
                    width = ValidationHelper.GetInteger(drv[FileWidthColumn], 0);
                }
                int height = 0;
                if (drv.DataView.Table.Columns.Contains(FileHeightColumn))
                {
                    height = ValidationHelper.GetInteger(drv[FileHeightColumn], 0);
                }

                string cmpltExt = (notAttachment ? "" : "." + ext);
                fileName = (cmpltExt != "") ? name.Replace(cmpltExt, "") : name;
                if (isContent && !notAttachment)
                {
                    string attachmentName = Path.GetFileNameWithoutExtension(drv["AttachmentName"].ToString());
                    if (fileName.ToLower() != attachmentName.ToLower())
                    {
                        fileName += " (" + HTMLHelper.HTMLEncode(drv["AttachmentName"].ToString()) + ")";
                    }
                }

                className = isContent ? HTMLHelper.HTMLEncode(drv["ClassName"].ToString().ToLower()) : "";
                isContentFile = (className == "cms.file");

                // Check if item is selectable
                if (!CMSDialogHelper.IsItemSelectable(SelectableContent, ext, isContentFile))
                {
                    LiteralControl ltlName = new LiteralControl(fileName);

                    // Get final panel
                    result = GetListItem(ext, className, "", "", width, height, ltlName, false, false);
                }
                else
                {
                    // Make a file name link
                    LinkButton lnkBtn = new LinkButton();

                    // Escape chars for postback javascript event
                    lnkBtn.ID = fileName.Replace("'", "").Replace("$", "");
                    lnkBtn.Text = HTMLHelper.HTMLEncode(fileName);

                    // Is current item CMS.File or attachment ?
                    isContentFile = isContent ? (drv["ClassName"].ToString().ToLower() == "cms.file") : false;
                    notAttachment = isContent && !(isContentFile && (ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty) != Guid.Empty));

                    fileName = GetFileName(drv.Row);

                    // Try to get imported file row
                    bool isImported = true;
                    DataRow importedRow = null;
                    if (DisplayMode == ControlDisplayModeEnum.Simple)
                    {
                        importedRow = RaiseOnFileIsNotInDatabase(fileName);
                        isImported = (importedRow != null);
                    }
                    else
                    {
                        importedRow = drv.Row;
                    }

                    if (!isImported)
                    {
                        importedRow = drv.Row;
                    }
                    else
                    {
                        // Update WIDTH
                        if (importedRow.Table.Columns.Contains(FileWidthColumn))
                        {
                            width = ValidationHelper.GetInteger(importedRow[FileWidthColumn], 0);
                        }

                        // Update HEIGHT
                        if (importedRow.Table.Columns.Contains(FileHeightColumn))
                        {
                            height = ValidationHelper.GetInteger(importedRow[FileHeightColumn], 0);
                        }
                    }

                    argument = RaiseOnGetArgumentSet(drv.Row);
                    url = RaiseOnGetListItemUrl(importedRow, false, notAttachment);
                    string previewUrl = RaiseOnGetListItemUrl(importedRow, true, notAttachment);
                    if (!String.IsNullOrEmpty(previewUrl))
                    {
                        // Add chset
                        string chset = Guid.NewGuid().ToString();
                        previewUrl = UrlHelper.AddParameterToUrl(previewUrl, "chset", chset);
                    }
                    // Add latest version requirement for live site
                    int versionHistoryId = VersionHistoryID;
                    if (!String.IsNullOrEmpty(previewUrl) && IsLiveSite && (versionHistoryId > 0))
                    {
                        // Add requirement for latest version of files for current document
                        string newparams = "latestforhistoryid=" + versionHistoryId;
                        newparams += "&hash=" + ValidationHelper.GetHashString("h" + versionHistoryId.ToString());

                        //url = UrlHelper.AppendQuery(url, newparams);
                        previewUrl = UrlHelper.AppendQuery(previewUrl, newparams);
                    }

                    // Check if current item is library folder
                    libraryFolder = ((SourceType == MediaSourceEnum.MediaLibraries) && IsFullListingMode &&
                        (drv[FileExtensionColumn].ToString().ToLower() == "<dir>"));

                    if ((SourceType == MediaSourceEnum.MediaLibraries) && (!isImported && (DisplayMode == ControlDisplayModeEnum.Simple)))
                    {
                        if (libraryFolder && !IsCopyMoveLinkDialog)
                        {
                            lnkBtn.Attributes["onclick"] = "SetAction('morefolderselect', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                        }
                        else if (libraryFolder && IsCopyMoveLinkDialog)
                        {
                            lnkBtn.Attributes["onclick"] = "SetAction('copymoveselect', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                        }
                        else
                        {
                            lnkBtn.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(drv.Row)) + "); SetAction('importfile', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                        }
                    }
                    else
                    {
                        // Initialize command
                        lnkBtn.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(drv.Row)) + "); SetSelectAction(" + ScriptHelper.GetString(argument + "|" + url) + "); return false;";
                    }

                    // Get final panel
                    result = GetListItem(ext, className, url, previewUrl, width, height, lnkBtn, true, isImported);
                }
                break;

            #endregion

            #region "Type"

            case "type":
                drv = (DataRowView)parameter;

                if (isContent)
                {
                    // Is current item CMS.File or attachment ?
                    isContentFile = isContent ? (drv["ClassName"].ToString().ToLower() == "cms.file") : false;
                    notAttachment = isContent && !(isContentFile && (ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty) != Guid.Empty));

                    if (notAttachment || (OutputFormat == OutputFormatEnum.HTMLLink) || (OutputFormat == OutputFormatEnum.BBLink))
                    {
                        return HTMLHelper.HTMLEncode(drv["ClassDisplayName"].ToString());
                    }
                }
                result = NormalizeExtenison(drv[FileExtensionColumn].ToString());
                break;

            #endregion

            #region "Size"

            case "size":
                drv = (DataRowView)parameter;

                // Is current item CMS.File or attachment ?
                isContentFile = isContent ? (drv["ClassName"].ToString().ToLower() == "cms.file") : false;
                notAttachment = isContent && !(isContentFile && (ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty) != Guid.Empty));

                if (!notAttachment)
                {
                    long size = 0;
                    if (drv[FileExtensionColumn].ToString() != "<dir>")
                    {
                        if (drv.DataView.Table.Columns.Contains(FileSizeColumn))
                        {
                            size = ValidationHelper.GetLong(drv[FileSizeColumn], 0);
                        }
                        else if (drv.Row.Table.Columns.Contains("Size"))
                        {
                            DataRow importedRow = RaiseOnFileIsNotInDatabase(GetFileName(drv.Row));
                            if (importedRow != null)
                            {
                                size = ValidationHelper.GetLong(importedRow["FileSize"], 0);
                            }
                            else
                            {
                                size = ValidationHelper.GetLong(drv["Size"], 0);
                            }
                        }
                    }
                    else
                    {
                        return "";
                    }
                    result = DataHelper.GetSizeString(size);
                }
                break;

            #endregion

            #region "Attachment modified"

            case "attachmentmodified":
            case "attachmentmodifiedtooltip":
                drv = (DataRowView)parameter;

                // If attachment is related to the workflow 'AttachmentLastModified' information isn't present
                if (!drv.DataView.Table.Columns.Contains("AttachmentLastModified"))
                {
                    int attachmentDocumentId = ValidationHelper.GetInteger(drv["AttachmentDocumentID"], 0);

                    result = GetLastModified(attachmentDocumentId);
                }
                else
                {
                    result = drv["AttachmentLastModified"].ToString();
                }

                result = CMSContext.ConvertDateTime(ValidationHelper.GetDateTime(result, DataHelper.DATETIME_NOT_SELECTED), this).ToString();
                break;

            #endregion

            #region "Attachment update"

            case "attachmentupdate":
                drv = (DataRowView)parameter;

                // Dynamically load uploader control
                DirectFileUploader dfuElem = Page.LoadControl("~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploader.ascx") as DirectFileUploader;

                Panel updatePanel = new Panel();
                updatePanel.Attributes["style"] = "text-align:center;";

                // Initialize update control
                GetAttachmentUpdateControl(ref dfuElem, drv);

                updatePanel.Controls.Add(dfuElem);

                result = updatePanel;
                break;

            #endregion

            #region "Library update"

            case "libraryupdate":
                drv = (DataRowView)parameter;

                updatePanel = new Panel();
                updatePanel.Attributes["style"] = "text-align:center;";

                libraryFolder = ((SourceType == MediaSourceEnum.MediaLibraries) && IsFullListingMode &&
                        (drv[FileExtensionColumn].ToString().ToLower() == "<dir>"));

                // Get info on imported file
                fileName = GetFileName(drv.Row);
                DataRow existingRow = RaiseOnFileIsNotInDatabase(fileName);
                bool hasModifyPermission = RaiseOnGetModifyPermission(drv.Row);
                if (hasModifyPermission && (existingRow != null))
                {
                    // Dynamically load uploader control
                    dfuElem = Page.LoadControl("~/CMSModules/Content/Controls/Attachments/DirectFileUploader/DirectFileUploader.ascx") as DirectFileUploader;
                    if (dfuElem != null)
                    {
                        // Initialize update control
                        GetLibraryUpdateControl(ref dfuElem, existingRow);

                        updatePanel.Controls.Add(dfuElem);
                    }
                }
                else if (!libraryFolder || !hasModifyPermission)
                {
                    ImageButton imgBtn = new ImageButton();
                    imgBtn.Enabled = false;
                    imgBtn.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/uploaddisabled.png"));
                    imgBtn.Attributes["style"] = "cursor: default;";

                    updatePanel.Controls.Add(imgBtn);
                }
                else
                {
                    updatePanel.Visible = false;
                }

                result = updatePanel;
                break;

            #endregion

            #region "Attachment delete"

            case "attachmentdelete":
                gvr = (parameter as GridViewRow);
                drv = (DataRowView)gvr.DataItem;

                // Initialize DELETE button
                btn = ((ImageButton)sender);
                btn.OnClientClick = "if(DeleteConfirmation() == false){return false;} SetAction('attachmentdelete', '" + drv[FileIdColumn].ToString() + "'); RaiseHiddenPostBack(); return false;";
                break;

            #endregion

            #region "Attachment delete"

            case "attachmentmoveup":
                gvr = (parameter as GridViewRow);
                drv = (DataRowView)gvr.DataItem;

                // Initialize MOVE UP button
                btn = ((ImageButton)sender);

                // Get attachment ID
                Guid attachmentGuid = ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty);

                btn.OnClientClick = "SetAction('attachmentmoveup', '" + attachmentGuid.ToString() + "'); RaiseHiddenPostBack(); return false;";
                break;
            #endregion

            #region "Attachment movedown"

            case "attachmentmovedown":
                gvr = (parameter as GridViewRow);
                drv = (DataRowView)gvr.DataItem;

                // Initialize MOVE DOWN button
                btn = ((ImageButton)sender);

                // Get attachment ID
                attachmentGuid = ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty);

                btn.OnClientClick = "SetAction('attachmentmovedown', '" + attachmentGuid.ToString() + "'); RaiseHiddenPostBack(); return false;";
                break;

            #endregion

            #region "Attachment edit"

            case "attachmentedit":
                gvr = (parameter as GridViewRow);
                drv = (DataRowView)gvr.DataItem;

                // Get file extension
                string attExtension = ValidationHelper.GetString(drv["AttachmentExtension"], "").ToLower();
                Guid attGuid = ValidationHelper.GetGuid(drv["AttachmentGUID"], Guid.Empty);

                ImageButton attImg = (ImageButton)sender;
                attImg.AlternateText = attExtension + "|AttachmentGUID=" + attGuid + "&sitename=" + GetSiteName(drv.Row, false) + "&versionHistoryId=" + VersionHistoryID;
                attImg.PreRender += new EventHandler(img_PreRender);
                break;

            #endregion

            #region "Library extension"

            case "extension":
                drv = (parameter as DataRowView);

                if (drv.DataView.Table.Columns.Contains("FileExtension"))
                {
                    result = drv["FileExtension"].ToString();
                }
                else
                {
                    result = drv["Extension"].ToString();
                }
                result = NormalizeExtenison(result.ToString());
                break;

            #endregion

            #region "Modified"

            case "modified":
            case "modifiedtooltip":
                drv = (parameter as DataRowView);

                if (drv.DataView.Table.Columns.Contains("FileModifiedWhen"))
                {
                    result = drv["FileModifiedWhen"].ToString();
                }
                else
                {
                    result = drv["Modified"].ToString();
                }

                result = CMSContext.ConvertDateTime(ValidationHelper.GetDateTime(result, DataHelper.DATETIME_NOT_SELECTED), this).ToString();
                break;

            #endregion

            #region "Document modified when"

            case "documentmodifiedwhen":
            case "filemodifiedwhen":
            case "documentmodifiedwhentooltip":
            case "filemodifiedwhentooltip":
                result = CMSContext.ConvertDateTime(ValidationHelper.GetDateTime(parameter, DataHelper.DATETIME_NOT_SELECTED), this).ToString();
                break;

            #endregion
        }

        return result;
    }


    protected void img_PreRender(object sender, EventArgs e)
    {
        ImageButton img = (ImageButton)sender;

        string[] args = img.AlternateText.Split('|');
        if (args.Length == 2)
        {
            // If the file is not an image don't allow image editing
            if (!ImageHelper.IsSupportedByImageEditor(args[0]))
            {
                // Disable edit icon in case that attachment is not an image
                img.ImageUrl = ResolveUrl(ImagesPath + "editdisabled.png");
                img.Enabled = false;
                img.Style.Add("cursor", "default");
            }
            else
            {
                string refreshType = CMSDialogHelper.GetMediaSource(SourceType);
                int parentId = QueryHelper.GetInteger("parentId", 0);
                string query = "?clientid=" + HTMLHelper.HTMLEncode(refreshType) + ((parentId > 0) ? "&parentId=" + parentId : "") + "&refresh=1&refaction=0&" + args[1];
                // Get validation hash for current image
                query += "&hash=" + QueryHelper.GetHash(query);
                img.Attributes["onclick"] = "if(!($j(this).hasClass('Edited'))){ EditImage(\"" + query + "\"); } return false;";
            }

            img.ToolTip = ResHelper.GetString("general.edit");
            img.AlternateText = ResHelper.GetString("general.edit");
        }
    }

    #endregion


    #region "Tiles view methods"

    /// <summary>
    /// Initializes controls for the tiles view mode.
    /// </summary>
    private void InitializeTilesView()
    {
        // Initialize page size
        bool isAttachmentTab = SourceType == MediaSourceEnum.DocumentAttachments;
        if (isAttachmentTab)
        {
            pageSizeTiles.Items = new string[] { "20", "40", "100" };
        }
        else
        {
            pageSizeTiles.Items = new string[] { "16", "32", "100" };
        }
        pagerElemTiles.PageSize = (pageSizeTiles.SelectedValue == "-1") ? 0 : ValidationHelper.GetInteger(pageSizeTiles.SelectedValue, (isAttachmentTab ? 20 : 16));

        // Basic control properties
        repTilesView.HideControlForZeroRows = true;

        // UniPager properties        
        pagerElemTiles.GroupSize = PAGER_GROUP_SIZE;
        pagerElemTiles.DisplayFirstLastAutomatically = false;
        pagerElemTiles.DisplayPreviousNextAutomatically = false;
        pagerElemTiles.HidePagerForSinglePage = true;
        pagerElemTiles.PagerMode = UniPagerMode.PostBack;
    }


    /// <summary>
    /// Loads content for media libraries tile view element.
    /// </summary>
    private void ReloadTilesView()
    {
        // Connects repeater with data source
        if (!DataHelper.DataSourceIsEmpty(DataSource))
        {
            repTilesView.DataSource = DataSource;
            repTilesView.DataBind();
        }
    }


    protected void TilesViewControl_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        #region "Load item data"

        string className = "";
        string fileNameColumn = FileNameColumn;

        DataRow dr = ((DataRowView)e.Item.DataItem).Row;

        bool isContent = (SourceType == MediaSourceEnum.Content);

        bool isContentFile = isContent ? (dr["ClassName"].ToString().ToLower() == "cms.file") : false;
        bool notAttachment = isContent && !(isContentFile && (dr["AttachmentGUID"] != DBNull.Value));
        if (notAttachment)
        {
            className = DataClassInfoProvider.GetDataClass((int)dr["NodeClassID"]).ClassDisplayName;

            fileNameColumn = "DocumentName";
        }
        else
        {
            fileNameColumn = "AttachmentName";
        }

        // Get basic information on file (use field properties for CMS.File | attachment, document columns otherwise)
        string fileName = HTMLHelper.HTMLEncode((dr.Table.Columns.Contains(fileNameColumn) ? dr[fileNameColumn].ToString() : dr[FileNameColumn].ToString()));
        string ext = notAttachment ? className : dr[FileExtensionColumn].ToString().TrimStart('.');
        string argument = RaiseOnGetArgumentSet(dr);

        // Get full media library file name
        bool isInDatabase = true;
        string fullFileName = GetFileName(dr);
        DataRow importedMediaRow = null;

        if ((SourceType == MediaSourceEnum.MediaLibraries) && (DisplayMode == ControlDisplayModeEnum.Simple))
        {
            importedMediaRow = RaiseOnFileIsNotInDatabase(fullFileName);
            isInDatabase = (importedMediaRow != null);
        }

        // Width & height
        int width = 0;
        if (dr.Table.Columns.Contains(FileWidthColumn))
        {
            width = ValidationHelper.GetInteger(dr[FileWidthColumn], 0);
        }
        else if (isInDatabase && (DisplayMode == ControlDisplayModeEnum.Simple) && importedMediaRow.Table.Columns.Contains(FileWidthColumn))
        {
            width = ValidationHelper.GetInteger(importedMediaRow[FileWidthColumn], 0);
        }

        int height = 0;
        if (dr.Table.Columns.Contains(FileHeightColumn))
        {
            height = ValidationHelper.GetInteger(dr[FileHeightColumn], 0);
        }
        else if (isInDatabase && (DisplayMode == ControlDisplayModeEnum.Simple) && importedMediaRow.Table.Columns.Contains(FileHeightColumn))
        {
            height = ValidationHelper.GetInteger(importedMediaRow[FileHeightColumn], 0);
        }

        // Get item image URL from the parent set
        string previewUrl = "";
        if ((SourceType == MediaSourceEnum.MediaLibraries) && isInDatabase && (DisplayMode == ControlDisplayModeEnum.Simple))
        {
            previewUrl = RaiseOnGetTilesThumbsItemUrl(importedMediaRow, true, 0, 0, 48, notAttachment);
        }
        else
        {
            previewUrl = RaiseOnGetTilesThumbsItemUrl(dr, true, 0, 0, 48, notAttachment);
        }

        string selectUrl = RaiseOnGetTilesThumbsItemUrl(dr, false, 0, 0, 0, notAttachment);
        bool isSelectable = CMSDialogHelper.IsItemSelectable(SelectableContent, ext, isContentFile);

        bool libraryFolder = ((SourceType == MediaSourceEnum.MediaLibraries) && IsFullListingMode && (dr[FileExtensionColumn].ToString().ToLower() == "<dir>"));
        bool libraryUiFolder = libraryFolder && !((DisplayMode == ControlDisplayModeEnum.Simple) && isInDatabase);

        #endregion


        #region "Standard controls and actions"

        bool hideDocName = false;

        Label lblDocumentName = null;
        if (isContent && !notAttachment)
        {
            string docName = Path.GetFileNameWithoutExtension(dr[FileNameColumn].ToString());
            fileName = Path.GetFileNameWithoutExtension(fileName);

            hideDocName = (docName.ToLower() == fileName.ToLower());
            if (!hideDocName)
            {
                fileName += "." + ext;

                lblDocumentName = e.Item.FindControl("lblDocumentName") as Label;
                if (lblDocumentName != null)
                {
                    lblDocumentName.Attributes["class"] = "DialogTileTitleBold";
                    lblDocumentName.Text = HTMLHelper.HTMLEncode(docName);
                }
            }
            else
            {
                fileName = docName;
            }
        }

        // Do not display document name if the same as the file name
        if (hideDocName || (lblDocumentName == null) || string.IsNullOrEmpty(lblDocumentName.Text))
        {
            PlaceHolder plcDocumentName = e.Item.FindControl("plcDocumentName") as PlaceHolder;
            if (plcDocumentName != null)
            {
                plcDocumentName.Visible = false;
            }
        }

        // Load file name
        Label lblFileName = e.Item.FindControl("lblFileName") as Label;
        if (lblFileName != null)
        {
            if (this.SourceType == MediaSourceEnum.DocumentAttachments)
            {
                fileName = Path.GetFileNameWithoutExtension(fileName);
            }
            lblFileName.Text = fileName;
        }

        // Load file type
        Label lblType = e.Item.FindControl("lblTypeValue") as Label;
        if (lblType != null)
        {
            if (notAttachment)
            {
                lblType.Text = ext;
            }
            else
            {
                lblType.Text = NormalizeExtenison(ext);
            }
        }

        if (!notAttachment && !libraryFolder && !libraryUiFolder)
        {
            // Load file size
            Label lblSize = e.Item.FindControl("lblSizeValue") as Label;
            if (lblSize != null)
            {
                long size = 0;
                if (dr.Table.Columns.Contains(FileSizeColumn))
                {
                    size = ValidationHelper.GetLong(dr[FileSizeColumn], 0);
                }
                // Library files
                else if (dr.Table.Columns.Contains("Size"))
                {
                    // Imported
                    if (importedMediaRow != null)
                    {
                        size = ValidationHelper.GetLong(importedMediaRow["FileSize"], 0);
                    }
                    // Not-imported yet
                    else
                    {
                        size = ValidationHelper.GetLong(dr["Size"], 0);
                    }
                }
                lblSize.Text = DataHelper.GetSizeString(size);
            }
        }

        // Initialize SELECT button
        ImageButton btnSelect = e.Item.FindControl("btnSelect") as ImageButton;
        if (btnSelect != null)
        {
            // Check if item is selectable, if not remove select action button
            if (!isSelectable)
            {
                btnSelect.ImageUrl = ResolveUrl(ImagesPath + "transparent.png");
                btnSelect.ToolTip = "";
                btnSelect.Attributes.Remove("onclick");
                btnSelect.Attributes["style"] = "cursor:default;";
                btnSelect.Enabled = false;
            }
            else
            {
                // If media file not imported yet - display warning sign
                if ((SourceType == MediaSourceEnum.MediaLibraries) && ((DisplayMode == ControlDisplayModeEnum.Simple) && !isInDatabase && !libraryFolder && !libraryUiFolder))
                {
                    btnSelect.ImageUrl = ResolveUrl(ImagesPath + "warning.png");
                    btnSelect.ToolTip = ResHelper.GetString("media.file.import");
                    btnSelect.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(dr)) + "); SetAction('importfile'," + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                }
                else
                {
                    // Initialize command
                    if (libraryFolder || libraryUiFolder)
                    {
                        btnSelect.ImageUrl = ResolveUrl(ImagesPath + "subdocument.png");
                        btnSelect.ToolTip = ResHelper.GetString("dialogs.list.actions.showsubfolders");
                        btnSelect.Attributes["onclick"] = "SetAction('morefolderselect', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                    }
                    else
                    {
                        btnSelect.ImageUrl = ResolveUrl(ImagesPath + "next.png");
                        btnSelect.ToolTip = ResHelper.GetString("dialogs.list.actions.select");
                        btnSelect.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(dr)) + "); SetSelectAction(" + ScriptHelper.GetString(argument + "|" + selectUrl) + "); return false;";
                    }
                }
            }
        }

        // Initialize SELECTSUBDOCS button
        ImageButton btnSelectSubDocs = e.Item.FindControl("btnSelectSubDocs") as ImageButton;
        if (btnSelectSubDocs != null)
        {
            btnSelectSubDocs.ToolTip = ResHelper.GetString("dialogs.list.actions.showsubdocuments");

            if (IsFullListingMode && (SourceType == MediaSourceEnum.Content))
            {
                int nodeId = ValidationHelper.GetInteger(dr["NodeID"], 0);

                // Check if item is selectable, if not remove select action button
                // Initialize command
                btnSelectSubDocs.Attributes["onclick"] = "SetParentAction('" + nodeId + "'); return false;";
                btnSelectSubDocs.ImageUrl = ResolveUrl(ImagesPath + "subdocument.png");
            }
            else
            {
                PlaceHolder plcSelectSubDocs = e.Item.FindControl("plcSelectSubDocs") as PlaceHolder;
                if (plcSelectSubDocs != null)
                {
                    plcSelectSubDocs.Visible = false;
                }
            }
        }

        // Initialize VIEW button
        // Get media file URL according system settings
        argument = RaiseOnGetArgumentSet(dr);

        ImageButton btnView = e.Item.FindControl("btnView") as ImageButton;
        if (btnView != null)
        {
            if (!notAttachment && !libraryFolder && !libraryUiFolder)
            {
                if (String.IsNullOrEmpty(selectUrl))
                {
                    btnView.ImageUrl = ResolveUrl(ImagesPath + "viewdisabled.png");
                    btnView.OnClientClick = "return false;";
                    btnView.Attributes["style"] = "cursor:default;";
                    btnView.Enabled = false;
                }
                else
                {
                    btnView.ImageUrl = ResolveUrl(ImagesPath + "view.png");
                    btnView.ToolTip = ResHelper.GetString("dialogs.list.actions.view");
                    btnView.OnClientClick = "javascript: window.open(" + ScriptHelper.GetString(UrlHelper.ResolveUrl(selectUrl)) + "); return false;";
                }
            }
            else
            {
                btnView.Visible = false;
            }
        }

        // Initialize EDIT button
        ImageButton btnContentEdit = e.Item.FindControl("btnContentEdit") as ImageButton;
        if (btnContentEdit != null)
        {
            Guid guid = Guid.Empty;
            btnContentEdit.ToolTip = ResHelper.GetString("general.edit");

            if ((SourceType == MediaSourceEnum.MediaLibraries) && !libraryFolder && !libraryUiFolder)
            {
                // Media files coming from FS
                if (!dr.Table.Columns.Contains("FileGUID"))
                {
                    // Get file name
                    fileName = fullFileName;
                    ext = "";
                    if (dr.Table.Columns.Contains("FileExtension"))
                    {
                        ext = dr["FileExtension"].ToString();
                    }
                    else
                    {
                        ext = dr["Extension"].ToString();
                    }

                    if (!ImageHelper.IsSupportedByImageEditor(ext) || !((DisplayMode == ControlDisplayModeEnum.Simple) && isInDatabase))
                    {
                        btnContentEdit.ImageUrl = ResolveUrl(ImagesPath + "editdisabled.png");
                        btnContentEdit.Attributes["style"] = "cursor: default;";
                        btnContentEdit.Enabled = false;
                    }
                    else
                    {
                        btnContentEdit.ImageUrl = ResolveUrl(ImagesPath + "edit.png");
                        btnContentEdit.OnClientClick = "$j(\"#hdnFileOrigName\").attr('value', " + ScriptHelper.GetString(EnsureFileName(fileName)) + "); SetAction('editlibraryui', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                    }
                }
                else
                {
                    guid = ValidationHelper.GetGuid(dr["FileGUID"], Guid.Empty);
                    btnContentEdit.ImageUrl = ResolveUrl(ImagesPath + "edit.png");
                    btnContentEdit.AlternateText = ext + "|MediaFileGUID=" + guid + "&sitename=" + GetSiteName(dr, true);
                    btnContentEdit.PreRender += new EventHandler(img_PreRender);
                }
            }
            else if (!notAttachment && (SourceType != MediaSourceEnum.DocumentAttachments) && !libraryFolder && !libraryUiFolder)
            {
                string nodeid = "";
                if (SourceType == MediaSourceEnum.Content)
                {
                    nodeid = "&nodeId=" + dr["NodeID"].ToString();

                    // Get the node workflow
                    VersionHistoryID = ValidationHelper.GetInteger(dr["DocumentCheckedOutVersionHistoryID"], 0);
                }

                guid = ValidationHelper.GetGuid(dr["AttachmentGUID"], Guid.Empty);
                btnContentEdit.ImageUrl = ResolveUrl(ImagesPath + "edit.png");
                btnContentEdit.AlternateText = ext + "|AttachmentGUID=" + guid + "&sitename=" + GetSiteName(dr, false) + nodeid +
                    ((VersionHistoryID > 0) ? "&versionHistoryId=" + VersionHistoryID : "");
                btnContentEdit.PreRender += new EventHandler(img_PreRender);
            }
            else
            {
                btnContentEdit.Visible = false;
            }
        }

        #endregion


        #region "Special actions"

        // If attachments being displayed show additional actions
        if (SourceType == MediaSourceEnum.DocumentAttachments)
        {
            // Initialize UPDATE button
            DirectFileUploader dfuElem = e.Item.FindControl("dfuElem") as DirectFileUploader;
            if (dfuElem != null)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;

                GetAttachmentUpdateControl(ref dfuElem, drv);
            }

            // Initialize EDIT button
            ImageButton btnEdit = e.Item.FindControl("btnEdit") as ImageButton;
            if (btnEdit != null)
            {
                if (!notAttachment)
                {
                    btnEdit.ToolTip = ResHelper.GetString("general.edit");

                    // Get file extension
                    string extension = ValidationHelper.GetString(dr["AttachmentExtension"], "").ToLower();
                    Guid guid = ValidationHelper.GetGuid(dr["AttachmentGUID"], Guid.Empty);

                    btnEdit.ImageUrl = ResolveUrl(ImagesPath + "edit.png");
                    btnEdit.AlternateText = extension + "|AttachmentGUID=" + guid + "&sitename=" + GetSiteName(dr, false) + "&versionHistoryId=" + VersionHistoryID;
                    btnEdit.PreRender += new EventHandler(img_PreRender);
                }
            }

            // Initialize DELETE button
            ImageButton btnDelete = e.Item.FindControl("btnDelete") as ImageButton;
            if (btnDelete != null)
            {
                btnDelete.ImageUrl = ResolveUrl(ImagesPath + "delete.png");
                btnDelete.ToolTip = ResHelper.GetString("general.delete");

                // Initialize command
                btnDelete.Attributes["onclick"] = "if(DeleteConfirmation() == false){return false;} SetAction('attachmentdelete','" + dr["AttachmentGUID"] + "'); RaiseHiddenPostBack(); return false;";
            }

            PlaceHolder plcContentEdit = e.Item.FindControl("plcContentEdit") as PlaceHolder;
            if (plcContentEdit != null)
            {
                plcContentEdit.Visible = false;
            }
        }
        else if ((SourceType == MediaSourceEnum.MediaLibraries) && !dr.Table.Columns.Contains("FileGUID") && !((DisplayMode == ControlDisplayModeEnum.Simple) && (libraryFolder || libraryUiFolder)))
        {
            // Initialize DELETE button
            ImageButton btnDelete = e.Item.FindControl("btnDelete") as ImageButton;
            if (btnDelete != null)
            {
                btnDelete.ImageUrl = ResolveUrl(ImagesPath + "Delete.png");
                btnDelete.ToolTip = ResHelper.GetString("general.delete");
                btnDelete.Attributes["onclick"] = "if(DeleteMediaFileConfirmation() == false){return false;} SetAction('deletefile','" + fileName + "'); RaiseHiddenPostBack(); return false;";
            }

            // Hide attachment specific actions
            PlaceHolder plcAttachmentUpdtAction = e.Item.FindControl("plcAttachmentUpdtAction") as PlaceHolder;
            if (plcAttachmentUpdtAction != null)
            {
                plcAttachmentUpdtAction.Visible = false;
            }
        }
        else
        {
            PlaceHolder plcAttachmentActions = e.Item.FindControl("plcAttachmentActions") as PlaceHolder;
            if (plcAttachmentActions != null)
            {
                plcAttachmentActions.Visible = false;
            }
        }

        #endregion


        #region "Library action"

        if ((SourceType == MediaSourceEnum.MediaLibraries) && (DisplayMode == ControlDisplayModeEnum.Simple))
        {
            // Initialize UPDATE button
            DirectFileUploader dfuElemLib = e.Item.FindControl("dfuElemLib") as DirectFileUploader;
            if (dfuElemLib != null)
            {
                Panel pnlDisabledUpdate = (e.Item.FindControl("pnlDisabledUpdate") as Panel);
                if (pnlDisabledUpdate != null)
                {
                    if (isInDatabase)
                    {
                        GetLibraryUpdateControl(ref dfuElemLib, importedMediaRow);

                        pnlDisabledUpdate.Visible = false;
                    }
                    else
                    {
                        pnlDisabledUpdate.Controls.Clear();

                        ImageButton imgBtn = new ImageButton();
                        imgBtn.Enabled = false;
                        imgBtn.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/uploaddisabled.png"));
                        imgBtn.Attributes["style"] = "cursor: default;";

                        pnlDisabledUpdate.Controls.Add(imgBtn);

                        dfuElemLib.Visible = false;
                    }
                }
            }
        }
        else
        {
            PlaceHolder plcLibraryUpdtAction = e.Item.FindControl("plcLibraryUpdtAction") as PlaceHolder;
            if (plcLibraryUpdtAction != null)
            {
                plcLibraryUpdtAction.Visible = false;
            }
        }


        if ((SourceType == MediaSourceEnum.MediaLibraries) && (DisplayMode == ControlDisplayModeEnum.Default) && libraryFolder)
        {
            // Initialize SELECT SUB-FOLDERS button
            ImageButton btn = e.Item.FindControl("imgSelectSubFolders") as ImageButton;
            if (btn != null)
            {
                btn.Visible = true;
                btn.ImageUrl = ResolveUrl(ImagesPath + "subdocument.png");
                btn.ToolTip = ResHelper.GetString("dialogs.list.actions.showsubfolders");
                btn.Attributes["onclick"] = "SetLibParentAction(" + ScriptHelper.GetString(fileName) + "); return false;";
            }
        }
        else
        {
            PlaceHolder plcSelectSubFolders = e.Item.FindControl("plcSelectSubFolders") as PlaceHolder;
            if (plcSelectSubFolders != null)
            {
                plcSelectSubFolders.Visible = false;
            }
        }

        #endregion


        #region "Load file image"

        // Selectable area
        Panel pnlItemInageContainer = e.Item.FindControl("pnlTiles") as Panel;
        if (pnlItemInageContainer != null)
        {
            if (isSelectable)
            {
                if ((DisplayMode == ControlDisplayModeEnum.Simple) && !isInDatabase)
                {
                    if (libraryFolder || libraryUiFolder)
                    {
                        pnlItemInageContainer.Attributes["onclick"] = "SetAction('morefolderselect', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                    }
                    else
                    {
                        pnlItemInageContainer.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(dr)) + "); SetAction('importfile', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                    }
                }
                else
                {
                    pnlItemInageContainer.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(dr)) + "); SetSelectAction(" + ScriptHelper.GetString(argument + "|" + selectUrl) + "); return false;";
                }

                pnlItemInageContainer.Attributes["style"] = "cursor:pointer;";
            }
            else
            {
                pnlItemInageContainer.Attributes["style"] = "cursor:default;";
            }
        }

        // Image area
        InlineUserControl fileImage = e.Item.FindControl("imgElem") as InlineUserControl;
        if (fileImage != null)
        {
            string chset = Guid.NewGuid().ToString();
            previewUrl = UrlHelper.AddParameterToUrl(previewUrl, "chset", chset);

            // Add latest version requirement for live site
            int versionHistoryId = VersionHistoryID;
            if (IsLiveSite && (versionHistoryId > 0))
            {
                // Add requirement for latest version of files for current document
                string newparams = "latestforhistoryid=" + versionHistoryId;
                newparams += "&hash=" + ValidationHelper.GetHashString("h" + versionHistoryId.ToString());

                previewUrl += "&" + newparams;
            }

            fileImage.SetValue("Url", ResolveUrl(previewUrl));
            fileImage.SetValue("Tooltip", fileName);
            fileImage.SetValue("Alt", TextHelper.LimitLength(fileName, 8));
            fileImage.SetValue("Extension", ext);

            if (isInDatabase && ImageHelper.IsImage(ext))
            {
                if ((width > 0) && (height > 0))
                {
                    int[] dimensions = ImageHelper.EnsureImageDimensions(0, 0, 200, width, height);

                    fileImage.SetValue("Behavior", "hover");
                    fileImage.SetValue("MouseOverWidth", dimensions[0]);
                    fileImage.SetValue("MouseOverHeight", dimensions[1]);
                    fileImage.SetValue("ImageID", "Image" + idCount++);
                }
            }
        }

        #endregion


        // Display only for ML UI
        if ((DisplayMode == ControlDisplayModeEnum.Simple) && !libraryFolder)
        {
            PlaceHolder plcSelectionBox = e.Item.FindControl("plcSelectionBox") as PlaceHolder;
            if (plcSelectionBox != null)
            {
                plcSelectionBox.Visible = true;

                // Multiple selection check-box
                LocalizedCheckBox chkSelected = e.Item.FindControl("chkSelected") as LocalizedCheckBox;
                if (chkSelected != null)
                {
                    chkSelected.ToolTip = ResHelper.GetString("general.select");
                    chkSelected.InputAttributes["alt"] = fullFileName;

                    HiddenField hdnItemName = e.Item.FindControl("hdnItemName") as HiddenField;
                    if (hdnItemName != null)
                    {
                        hdnItemName.Value = fullFileName;
                    }
                }
            }
        }
    }

    #endregion


    #region "Thumbnails view methods"

    /// <summary>
    /// Initializes controls for the thumbnails view mode.
    /// </summary>
    private void InitializeThumbnailsView()
    {
        bool isAttachmentTab = SourceType == MediaSourceEnum.DocumentAttachments;

        // Initialize page size
        if (isAttachmentTab)
        {
            pageSizeThumbs.Items = new string[] { "12", "24", "48", "96" };
        }
        else
        {
            pageSizeThumbs.Items = new string[] { "10", "20", "50", "100" };
        }
        pagerElemThumbnails.PageSize = (pageSizeThumbs.SelectedValue == "-1") ? 0 : ValidationHelper.GetInteger(pageSizeThumbs.SelectedValue, (isAttachmentTab ? 12 : 10));

        // Basic control properties
        repThumbnailsView.HideControlForZeroRows = true;

        // UniPager properties        
        pagerElemThumbnails.GroupSize = PAGER_GROUP_SIZE;
        pagerElemThumbnails.DisplayFirstLastAutomatically = false;
        pagerElemThumbnails.DisplayPreviousNextAutomatically = false;
        pagerElemThumbnails.HidePagerForSinglePage = true;
        pagerElemThumbnails.PagerMode = UniPagerMode.PostBack;
    }


    /// <summary>
    /// Loads content for media libraries thumbnails view element.
    /// </summary>
    private void ReloadThumbnailsView()
    {
        // Connects repeater with data source
        if (!DataHelper.DataSourceIsEmpty(DataSource))
        {
            repThumbnailsView.DataSource = DataSource;
            repThumbnailsView.DataBind();
        }
    }


    protected void ThumbnailsViewControl_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        #region "Load the item data"

        DataRow dr = ((DataRowView)e.Item.DataItem).Row;

        string fileNameColumn = FileNameColumn;
        string className = "";

        bool isContent = (SourceType == MediaSourceEnum.Content);

        bool isContentFile = isContent ? (dr["ClassName"].ToString().ToLower() == "cms.file") : false;
        bool notAttachment = isContent && !(isContentFile && (dr["AttachmentGUID"] != DBNull.Value));
        if (notAttachment)
        {
            className = DataClassInfoProvider.GetDataClass((int)dr["NodeClassID"]).ClassDisplayName;

            fileNameColumn = "DocumentName";
        }

        // Get information on file
        string fileName = HTMLHelper.HTMLEncode(dr[fileNameColumn].ToString());
        string ext = HTMLHelper.HTMLEncode(notAttachment ? className : dr[FileExtensionColumn].ToString().TrimStart('.'));
        string argument = RaiseOnGetArgumentSet(dr);

        // Get full media library file name
        bool isInDatabase = true;
        string fullFileName = GetFileName(dr);
        DataRow importedMediaRow = null;

        if (SourceType == MediaSourceEnum.MediaLibraries)
        {
            importedMediaRow = RaiseOnFileIsNotInDatabase(fullFileName);
            isInDatabase = (importedMediaRow != null);
        }

        bool libraryFolder = ((SourceType == MediaSourceEnum.MediaLibraries) && IsFullListingMode && (dr[FileExtensionColumn].ToString().ToLower() == "<dir>"));
        bool libraryUiFolder = libraryFolder && !((DisplayMode == ControlDisplayModeEnum.Simple) && isInDatabase);

        // Get thumb preview image dimensions
        int[] thumbImgDimension = new int[] { 0, 0 };
        if (ImageHelper.IsImage(ext))
        {
            // Width & height
            int origWidth = 0;
            if (dr.Table.Columns.Contains(FileWidthColumn))
            {
                origWidth = ValidationHelper.GetInteger(dr[FileWidthColumn], 0);
            }
            else if (isInDatabase && importedMediaRow.Table.Columns.Contains(FileWidthColumn))
            {
                origWidth = ValidationHelper.GetInteger(importedMediaRow[FileWidthColumn], 0);
            }

            int origHeight = 0;
            if (dr.Table.Columns.Contains(FileHeightColumn))
            {
                origHeight = ValidationHelper.GetInteger(dr[FileHeightColumn], 0);
            }
            else if (isInDatabase && importedMediaRow.Table.Columns.Contains(FileHeightColumn))
            {
                origHeight = ValidationHelper.GetInteger(importedMediaRow[FileHeightColumn], 0);
            }

            thumbImgDimension = CMSDialogHelper.GetThumbImageDimensions(origHeight, origWidth, MaxThumbImgHeight, MaxThumbImgWidth);
        }

        // Preview URL
        string previewUrl = "";
        if ((SourceType == MediaSourceEnum.MediaLibraries) && isInDatabase)
        {
            previewUrl = RaiseOnGetTilesThumbsItemUrl(importedMediaRow, true, thumbImgDimension[0], thumbImgDimension[1], 0, notAttachment);
        }
        else
        {
            previewUrl = RaiseOnGetTilesThumbsItemUrl(dr, true, thumbImgDimension[0], thumbImgDimension[1], 0, notAttachment);
        }

        // Item URL
        string selectUrl = RaiseOnGetTilesThumbsItemUrl(dr, false, 0, 0, 0, notAttachment);
        bool isSelectable = CMSDialogHelper.IsItemSelectable(SelectableContent, ext, isContentFile);

        #endregion


        #region "Standard controls and actions"

        // Load file name
        Label lblName = e.Item.FindControl("lblFileName") as Label;
        if (lblName != null)
        {
            lblName.Text = fileName;
        }

        // Initialize SELECT button
        ImageButton btnSelect = e.Item.FindControl("btnSelect") as ImageButton;
        if (btnSelect != null)
        {
            // Check if item is selectable, if not remove select action button
            if (!isSelectable)
            {
                btnSelect.ImageUrl = ResolveUrl(ImagesPath + "transparent.png");
                btnSelect.ToolTip = "";
                btnSelect.Attributes.Remove("onclick");
                btnSelect.Attributes["style"] = "cursor: default;";
                btnSelect.Enabled = false;
            }
            else
            {
                // If media file not imported yet - display warning sign
                if ((SourceType == MediaSourceEnum.MediaLibraries) && ((DisplayMode == ControlDisplayModeEnum.Simple) && !isInDatabase && !libraryFolder && !libraryUiFolder))
                {
                    btnSelect.ImageUrl = ResolveUrl(ImagesPath + "warning.png");
                    btnSelect.ToolTip = ResHelper.GetString("media.file.import");
                    btnSelect.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(dr)) + "); SetAction('importfile'," + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                }
                else
                {
                    if (libraryFolder || libraryUiFolder)
                    {
                        btnSelect.ImageUrl = ResolveUrl(ImagesPath + "subdocument.png");
                        btnSelect.ToolTip = ResHelper.GetString("dialogs.list.actions.showsubfolders");
                        btnSelect.Attributes["onclick"] = "SetAction('morefolderselect', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                    }
                    else
                    {
                        btnSelect.ImageUrl = ResolveUrl(ImagesPath + "next.png");
                        btnSelect.ToolTip = ResHelper.GetString("dialogs.list.actions.select");
                        btnSelect.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(dr)) + "); SetSelectAction(" + ScriptHelper.GetString(argument + "|" + selectUrl) + "); return false;";
                    }
                }
            }
        }

        // Initialize SELECTSUBDOCS button
        ImageButton btnSelectSubDocs = e.Item.FindControl("btnSelectSubDocs") as ImageButton;
        if (btnSelectSubDocs != null)
        {
            if (IsFullListingMode && (SourceType == MediaSourceEnum.Content))
            {
                int nodeId = ValidationHelper.GetInteger(dr["NodeID"], 0);

                btnSelectSubDocs.ToolTip = ResHelper.GetString("dialogs.list.actions.showsubdocuments");

                // Check if item is selectable, if not remove select action button
                // Initialize command
                btnSelectSubDocs.Attributes["onclick"] = "SetParentAction('" + nodeId + "'); return false;";
                btnSelectSubDocs.ImageUrl = ResolveUrl(ImagesPath + "subdocument.png");
            }
            else
            {
                PlaceHolder plcSelectSubDocs = e.Item.FindControl("plcSelectSubDocs") as PlaceHolder;
                if (plcSelectSubDocs != null)
                {
                    plcSelectSubDocs.Visible = false;
                }
            }
        }

        // Initialize VIEW button
        ImageButton btnView = e.Item.FindControl("btnView") as ImageButton;
        if (btnView != null)
        {
            if (!notAttachment && !libraryFolder && !libraryUiFolder)
            {
                if (String.IsNullOrEmpty(selectUrl))
                {
                    btnView.ImageUrl = ResolveUrl(ImagesPath + "viewdisabled.png");
                    btnView.OnClientClick = "return false;";
                    btnView.Attributes["style"] = "cursor:default;";
                    btnView.Enabled = false;
                }
                else
                {
                    btnView.ImageUrl = ResolveUrl(ImagesPath + "view.png");
                    btnView.ToolTip = ResHelper.GetString("dialogs.list.actions.view");
                    btnView.OnClientClick = "javascript: window.open(" + ScriptHelper.GetString(UrlHelper.ResolveUrl(selectUrl)) + "); return false;";
                }
            }
            else
            {
                btnView.Visible = false;
            }
        }

        // Initialize EDIT button
        ImageButton btnContentEdit = e.Item.FindControl("btnContentEdit") as ImageButton;
        if (btnContentEdit != null)
        {
            btnContentEdit.ToolTip = ResHelper.GetString("general.edit");

            Guid guid = Guid.Empty;

            if (SourceType == MediaSourceEnum.MediaLibraries && !libraryFolder && !libraryUiFolder)
            {
                // Media files coming from FS
                if (!dr.Table.Columns.Contains("FileGUID"))
                {
                    if (!ImageHelper.IsSupportedByImageEditor(ext) || ((DisplayMode == ControlDisplayModeEnum.Simple) && !isInDatabase))
                    {
                        btnContentEdit.ImageUrl = ResolveUrl(ImagesPath + "editdisabled.png");
                        btnContentEdit.Attributes["style"] = "cursor: default;";
                        btnContentEdit.Enabled = false;
                    }
                    else
                    {
                        btnContentEdit.ImageUrl = ResolveUrl(ImagesPath + "edit.png");
                        btnContentEdit.OnClientClick = "$j(\"#hdnFileOrigName\").attr('value', " + ScriptHelper.GetString(EnsureFileName(fileName)) + "); SetAction('editlibraryui', '" + fileName + "'); RaiseHiddenPostBack(); return false;";
                    }
                }
                else
                {
                    int siteId = ValidationHelper.GetInteger(dr["FileSiteID"], 0);
                    guid = ValidationHelper.GetGuid(dr["FileGUID"], Guid.Empty);
                    btnContentEdit.ImageUrl = ResolveUrl(ImagesPath + "edit.png");
                    btnContentEdit.AlternateText = ext + "|MediaFileGUID=" + guid + "&sitename=" + GetSiteName(dr, true);
                    btnContentEdit.PreRender += new EventHandler(img_PreRender);
                }
            }
            else if (!notAttachment && !libraryFolder && !libraryUiFolder)
            {
                string nodeid = "";
                if (SourceType == MediaSourceEnum.Content)
                {
                    nodeid = "&nodeId=" + dr["NodeID"].ToString();

                    // Get the node workflow
                    VersionHistoryID = ValidationHelper.GetInteger(dr["DocumentCheckedOutVersionHistoryID"], 0);
                }

                guid = ValidationHelper.GetGuid(dr["AttachmentGUID"], Guid.Empty);
                btnContentEdit.ImageUrl = ResolveUrl(ImagesPath + "edit.png");
                btnContentEdit.AlternateText = ext + "|AttachmentGUID=" + guid + "&sitename=" + GetSiteName(dr, false) + nodeid +
                    ((VersionHistoryID > 0) ? "&versionHistoryId=" + VersionHistoryID : "");
                btnContentEdit.PreRender += new EventHandler(img_PreRender);
            }
            else
            {
                btnContentEdit.Visible = false;
            }
        }

        #endregion


        #region "Special actions"

        // If attachments being displayed show additional actions
        if (SourceType == MediaSourceEnum.DocumentAttachments)
        {
            // Initialize EDIT button
            ImageButton btnEdit = e.Item.FindControl("btnEdit") as ImageButton;
            if (btnEdit != null)
            {
                if (!notAttachment)
                {
                    btnEdit.ToolTip = ResHelper.GetString("general.edit");

                    // Get file extension
                    string extension = ValidationHelper.GetString(dr["AttachmentExtension"], "").ToLower();
                    Guid guid = ValidationHelper.GetGuid(dr["AttachmentGUID"], Guid.Empty);

                    btnEdit.ImageUrl = ResolveUrl(ImagesPath + "edit.png");
                    btnEdit.AlternateText = extension + "|AttachmentGUID=" + guid + "&sitename=" + GetSiteName(dr, false) + "&versionHistoryId=" + VersionHistoryID;
                    btnEdit.PreRender += new EventHandler(img_PreRender);
                }
            }

            // Initialize UPDATE button
            DirectFileUploader dfuElem = e.Item.FindControl("dfuElem") as DirectFileUploader;
            if (dfuElem != null)
            {
                DataRowView drv = (DataRowView)e.Item.DataItem;

                GetAttachmentUpdateControl(ref dfuElem, drv);
            }

            // Initialize DELETE button
            ImageButton btnDelete = e.Item.FindControl("btnDelete") as ImageButton;
            if (btnDelete != null)
            {
                btnDelete.ImageUrl = ResolveUrl(ImagesPath + "delete.png");
                btnDelete.ToolTip = ResHelper.GetString("general.delete");

                // Initialize command
                btnDelete.Attributes["onclick"] = "if(DeleteConfirmation() == false){return false;} SetAction('attachmentdelete','" + dr["AttachmentGUID"] + "'); RaiseHiddenPostBack(); return false;";
            }

            PlaceHolder plcContentEdit = e.Item.FindControl("plcContentEdit") as PlaceHolder;
            if (plcContentEdit != null)
            {
                plcContentEdit.Visible = false;
            }
        }
        else if ((SourceType == MediaSourceEnum.MediaLibraries) && !dr.Table.Columns.Contains("FileGUID") && ((DisplayMode == ControlDisplayModeEnum.Simple) && !libraryFolder && !libraryUiFolder))
        {
            // Initialize DELETE button
            ImageButton btnDelete = e.Item.FindControl("btnDelete") as ImageButton;
            if (btnDelete != null)
            {
                btnDelete.ImageUrl = ResolveUrl(ImagesPath + "Delete.png");
                btnDelete.ToolTip = ResHelper.GetString("general.delete");
                btnDelete.Attributes["onclick"] = "if(DeleteMediaFileConfirmation() == false){return false;} SetAction('deletefile'," + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
            }

            // Hide attachment specific actions
            PlaceHolder plcAttachmentUpdtAction = e.Item.FindControl("plcAttachmentUpdtAction") as PlaceHolder;
            if (plcAttachmentUpdtAction != null)
            {
                plcAttachmentUpdtAction.Visible = false;
            }
        }
        else
        {
            PlaceHolder plcAttachmentActions = e.Item.FindControl("plcAttachmentActions") as PlaceHolder;
            if (plcAttachmentActions != null)
            {
                plcAttachmentActions.Visible = false;
            }
        }

        #endregion


        #region "Library update action"

        if ((SourceType == MediaSourceEnum.MediaLibraries) && (DisplayMode == ControlDisplayModeEnum.Simple))
        {
            // Initialize UPDATE button
            DirectFileUploader dfuElemLib = e.Item.FindControl("dfuElemLib") as DirectFileUploader;
            if (dfuElemLib != null)
            {
                Panel pnlDisabledUpdate = (e.Item.FindControl("pnlDisabledUpdate") as Panel);
                if (pnlDisabledUpdate != null)
                {
                    if (isInDatabase)
                    {
                        GetLibraryUpdateControl(ref dfuElemLib, importedMediaRow);

                        pnlDisabledUpdate.Visible = false;
                    }
                    else
                    {
                        pnlDisabledUpdate.Controls.Clear();

                        ImageButton imgBtn = new ImageButton();
                        imgBtn.Enabled = false;
                        imgBtn.ImageUrl = ResolveUrl(GetImageUrl("Design/Controls/DirectUploader/uploaddisabled.png"));
                        imgBtn.Attributes["style"] = "cursor: default;";

                        pnlDisabledUpdate.Controls.Add(imgBtn);

                        dfuElemLib.Visible = false;
                    }
                }
            }
        }
        else
        {
            PlaceHolder plcLibraryUpdtAction = e.Item.FindControl("plcLibraryUpdtAction") as PlaceHolder;
            if (plcLibraryUpdtAction != null)
            {
                plcLibraryUpdtAction.Visible = false;
            }
        }

        if ((SourceType == MediaSourceEnum.MediaLibraries) && (DisplayMode == ControlDisplayModeEnum.Default) && libraryFolder)
        {
            // Initialize SELECT SUB-FOLDERS button
            ImageButton btn = e.Item.FindControl("imgSelectSubFolders") as ImageButton;
            if (btn != null)
            {
                btn.Visible = true;
                btn.ImageUrl = ResolveUrl(ImagesPath + "subdocument.png");
                btn.ToolTip = ResHelper.GetString("dialogs.list.actions.showsubfolders");
                btn.Attributes["onclick"] = "SetLibParentAction(" + ScriptHelper.GetString(fileName) + "); return false;";
            }
        }
        else
        {
            PlaceHolder plcSelectSubFolders = e.Item.FindControl("plcSelectSubFolders") as PlaceHolder;
            if (plcSelectSubFolders != null)
            {
                plcSelectSubFolders.Visible = false;
            }
        }

        #endregion


        #region "File image"

        // Selectable area
        Panel pnlItemInageContainer = e.Item.FindControl("pnlThumbnails") as Panel;
        if (pnlItemInageContainer != null)
        {
            if (isSelectable)
            {
                if ((DisplayMode == ControlDisplayModeEnum.Simple) && !isInDatabase)
                {
                    if (libraryFolder || libraryUiFolder)
                    {
                        pnlItemInageContainer.Attributes["onclick"] = "SetAction('morefolderselect', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                    }
                    else
                    {
                        pnlItemInageContainer.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(dr)) + "); SetAction('importfile', " + ScriptHelper.GetString(fileName) + "); RaiseHiddenPostBack(); return false;";
                    }
                }
                else
                {
                    pnlItemInageContainer.Attributes["onclick"] = "ColorizeRow(" + ScriptHelper.GetString(GetColorizeID(dr)) + "); SetSelectAction(" + ScriptHelper.GetString(argument + "|" + selectUrl) + "); return false;";
                }
                pnlItemInageContainer.Attributes["style"] = "cursor:pointer;";
            }
            else
            {
                pnlItemInageContainer.Attributes["style"] = "cursor:default;";
            }
        }

        // Image area
        Image imgFile = e.Item.FindControl("imgFile") as Image;
        if (imgFile != null)
        {
            // Add latest version requirement for live site
            int versionHistoryId = VersionHistoryID;
            if (IsLiveSite && (versionHistoryId > 0))
            {
                // Add requirement for latest version of files for current document
                string newparams = "latestforhistoryid=" + versionHistoryId;
                newparams += "&hash=" + ValidationHelper.GetHashString("h" + versionHistoryId.ToString());

                previewUrl += "&" + newparams;
            }

            imgFile.ImageUrl = previewUrl;
            imgFile.AlternateText = TextHelper.LimitLength(fileName, 10);
            imgFile.Attributes["title"] = fileName.Replace("\"", "\\\"");
        }

        #endregion


        // Display only for ML UI
        if ((DisplayMode == ControlDisplayModeEnum.Simple) && !libraryFolder)
        {
            PlaceHolder plcSelectionBox = e.Item.FindControl("plcSelectionBox") as PlaceHolder;
            if (plcSelectionBox != null)
            {
                plcSelectionBox.Visible = true;

                // Multiple selection check-box
                LocalizedCheckBox chkSelected = e.Item.FindControl("chkSelected") as LocalizedCheckBox;
                if (chkSelected != null)
                {
                    chkSelected.ToolTip = ResHelper.GetString("general.select");
                    chkSelected.InputAttributes["alt"] = fullFileName;

                    HiddenField hdnItemName = e.Item.FindControl("hdnItemName") as HiddenField;
                    if (hdnItemName != null)
                    {
                        hdnItemName.Value = fullFileName;
                    }
                }
            }
        }
    }

    #endregion


    #region "Raise events methods"

    /// <summary>
    /// Fires specific action and returns result provided by the parent control.
    /// </summary>
    /// <param name="dr">Data related to the action.</param>
    private string RaiseOnGetArgumentSet(DataRow dr)
    {
        if (GetArgumentSet != null)
        {
            return GetArgumentSet(dr);
        }
        return "";
    }


    /// <summary>
    /// Fires specific action and returns result provided by the parent control.
    /// </summary>
    /// <param name="dr">Data related to the action.</param>
    /// <param name="isPreview">Indicates whether the URL is required for preview item.</param>
    /// <param name="notAttachment">Indicates whether the URL is required for non-attachment item.</param>
    private string RaiseOnGetListItemUrl(DataRow dr, bool isPreview, bool notAttachment)
    {
        if (GetListItemUrl != null)
        {
            return GetListItemUrl(dr, isPreview, notAttachment);
        }
        return "";
    }

    /// <summary>
    /// Fires specific action and returns result provided by the parent control.
    /// </summary>
    /// <param name="dr">Data related to the action.</param>
    /// <param name="isPreview">Indicates whether the image is required as part of preview.</param>
    /// <param name="maxSideSize">Maximum size of the preview image. If full-size required parameter gets zero value.</param>
    /// <param name="notAttachment">Indicates whether the URL is required for non-attachment item.</param>
    private string RaiseOnGetTilesThumbsItemUrl(DataRow dr, bool isPreview, int height, int width, int maxSideSize, bool notAttachment)
    {
        if (GetTilesThumbsItemUrl != null)
        {
            return GetTilesThumbsItemUrl(dr, isPreview, height, width, maxSideSize, notAttachment);
        }
        return "";
    }


    /// <summary>
    /// Raises event when information on import status of specified file is required.
    /// </summary>
    /// <param name="fileName">Name of the file (including extension).</param>
    private DataRow RaiseOnFileIsNotInDatabase(string fileName)
    {
        if (GetInformation != null)
        {
            object result = GetInformation("fileisnotindatabase", fileName);
            if (result != null)
            {
                return (DataRow)result;
            }

            return null;
        }

        return null;
    }


    /// <summary>
    /// Raises event when ID of the current site is required.
    /// </summary>
    private int RaiseOnSiteIdRequired()
    {
        if (GetInformation != null)
        {
            return (int)GetInformation("siteidrequired", null);
        }

        return 0;
    }


    /// <summary>
    /// Raises event when modify permission is required.
    /// </summary>
    /// <param name="dr"></param>
    /// <returns></returns>
    private bool RaiseOnGetModifyPermission(DataRow dr)
    {
        if (GetModifyPermission != null)
        {
            return GetModifyPermission(dr);
        }
        return true;
    }

    #endregion
}
