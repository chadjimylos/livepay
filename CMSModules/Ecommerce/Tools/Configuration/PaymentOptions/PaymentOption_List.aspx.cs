using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.Ecommerce;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_PaymentOptions_PaymentOption_List : CMSEcommerceConfigurationPage
{
    private int siteId = 0;


    protected void Page_Load(object sender, EventArgs e)
    {
        siteId = CMSContext.CurrentSite.SiteID;

        this.CurrentMaster.Title.TitleText = ResHelper.GetString("PaymentOption_List.HeaderCaption");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_PaymentOption/object.png");
        this.CurrentMaster.Title.HelpTopicName = "payment_methods_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        UniGrid.OnAction += new OnActionEventHandler(uniGrid_OnAction);
        UniGrid.OnExternalDataBound += new OnExternalDataBoundEventHandler(UniGrid_OnExternalDataBound);
        UniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");
        UniGrid.WhereCondition = "PaymentOptionSiteID = " + siteId;

        // Prepare the new payment option header element
        string[,] actions = new string[1, 8];
        actions[0, 0] = "HyperLink";
        actions[0, 1] = ResHelper.GetString("PaymentOption_List.NewItemCaption");
        actions[0, 3] = "~/CMSModules/Ecommerce/Tools/Configuration/PaymentOptions/PaymentOption_Edit.aspx";
        actions[0, 5] = GetImageUrl("Objects/Ecommerce_PaymentOption/add.png");

        this.CurrentMaster.HeaderActions.Actions = actions;
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void uniGrid_OnAction(string actionName, object actionArgument)
    {
        if (actionName == "edit")
        {
            UrlHelper.Redirect("PaymentOption_Edit.aspx?paymentOptionId=" + Convert.ToString(actionArgument));
        }
        else if (actionName == "delete")
        {
            // check 'ConfigurationModify' permission
            if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
            {
                RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
            }

            if (PaymentOptionInfoProvider.CheckDependencies(ValidationHelper.GetInteger(actionArgument, 0)))
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Ecommerce.DeleteDisabled");
                return;
            }

            // delete PaymentOptionInfo object from database
            PaymentOptionInfoProvider.DeletePaymentOptionInfo(ValidationHelper.GetInteger(actionArgument, 0));
        }
    }


    protected object UniGrid_OnExternalDataBound(object sender, string sourceName, object parameter)
    {
        switch (sourceName.ToLower())
        {
            case "paymentoptenabled":
                return UniGridFunctions.ColoredSpanYesNo(parameter);
        }

        return parameter;
    }
}
