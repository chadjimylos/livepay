Option Strict Off
Imports System.Data
Imports System.Data.SqlClient
Partial Class TRANSACTION_CONFIRMATION
    Inherits Web.UI.Page
    Dim theRef As String = ""

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        theRef = Request("Ref")
        Try
            AegeanPower_DBConnection.TransactionLogAdd(theRef, "Confirmation Initiated")

            Dim theShop As String = Request("Shop")
            Dim thePassword As String = Request("Password")
            Dim theAmount As String = Request("Amount")

            Dim theTransactionID As String = Request("TransID")
            Dim theCurrency As String = Request("Currency")

            If theShop <> "" And thePassword <> "" And theAmount <> "" And theRef <> "" And theTransactionID <> "" And theCurrency <> "" Then
                If CInt(theCurrency) <> 978 Then
                    okOrBad.Text = "[BAD]"
                    AegeanPower_DBConnection.TransactionLogAdd(theRef, "Confirmation Error: Invalid Currency sent by APACS(" & theCurrency & ")")
                ElseIf thePassword.ToLower <> System.Configuration.ConfigurationManager.AppSettings("ApacsConfirmationPassword").ToLower Then
                    okOrBad.Text = "[BAD]"
                    AegeanPower_DBConnection.TransactionLogAdd(theRef, "Confirmation Error: Invalid Confirmation Password sent by APACS(Sent " & thePassword & ", correct is " & System.Configuration.ConfigurationManager.AppSettings("ApacsConfirmationPassword") & ")")
                Else
                    Dim ordDT As String = GetAmountByMerchantRef(theRef)
                    Dim correctAmount As String = GetAmountByMerchantRef(theRef)
                    If IsDBNull(correctAmount) Then
                        okOrBad.Text = "[BAD]"
                        AegeanPower_DBConnection.TransactionLogAdd(theRef, "Confirmation Error: No such RefID located in SHOP_Validation(" & theRef & ").")
                    Else
                        If theAmount.toString().Replace(".","").Replace(",","") <> correctAmount.toString().Replace(".","").Replace(",","") Then
                            okOrBad.Text = "[BAD]"
                            AegeanPower_DBConnection.TransactionLogAdd(theRef, "Confirmation Error: DB Amount and APACS Amount do not match. (" & theAmount & ", " & correctAmount & ",MerchantRef:" & theRef & ")")
                        Else
                            okOrBad.Text = "[OK]"
                            AegeanPower_DBConnection.TransactionLogAdd(theRef, "Confirmation OK.")
                        End If
                    End If
                End If
            Else
                okOrBad.Text = "[BAD]"
                AegeanPower_DBConnection.TransactionLogAdd(theRef, "Confirmation Error: Invalid QueryString sent by APACS (" & Request.QueryString.ToString & ").")
            End If
        Catch ex As Exception
            okOrBad.Text = "[BAD]"
            AegeanPower_DBConnection.TransactionLogAdd(theRef, "Confirmation Exception: " & ex.ToString)
        End Try
    End Sub


    Function GetAmountByMerchantRef(ByVal MerchantRef As String) As String
        Dim strConnection As String = System.Configuration.ConfigurationManager.ConnectionStrings("CMSConnectionString").ToString()

        Dim objConnection As New SqlConnection(strConnection)
        objConnection.Open()

        Dim objCommand As New SqlCommand("AegeanPower_GetAmountByMerchantRef", objConnection)
        objCommand.CommandType = CommandType.StoredProcedure
        objCommand.Parameters.AddWithValue("@MerchantRef", MerchantRef)

        Dim amount As String = objCommand.ExecuteScalar
        objConnection.Close()
        Return amount
    End Function
End Class
