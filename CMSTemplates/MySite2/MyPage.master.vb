﻿Imports System
Imports System.Web
Imports CMS.UIControls
Imports CMS.PortalControls

Partial Class CMSTemplates_MySite_MasterPage
    Inherits TemplateMasterPage

    Protected Overloads Overrides Sub CreateChildControls()

        MyBase.CreateChildControls()

        Me.PageManager = Me.CMSPageManager1
    End Sub

    Protected Overloads Overrides Sub OnPreRender(ByVal e As EventArgs)
        MyBase.OnPreRender(e)

        Me.ltlTags.Text = Me.HeaderTags
    End Sub

End Class

