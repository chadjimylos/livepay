using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.CMSHelper;
using CMS.Forums;
using CMS.LicenseProvider;
using CMS.UIControls;
using CMS.SettingsProvider;

public partial class CMSModules_Forums_Tools_Forums_Forum_Security : CMSForumsPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        this.forumSecurity.ForumID = QueryHelper.GetInteger("forumid", 0);
        this.forumSecurity.IsGroupForum = false;
        forumSecurity.IsLiveSite = false;

        this.InitializeMasterPage();
    }


    /// <summary>
    /// Initializes master page.
    /// </summary>
    protected void InitializeMasterPage()
    {
        this.Title = "Forums - Forum security";        
    }
}
