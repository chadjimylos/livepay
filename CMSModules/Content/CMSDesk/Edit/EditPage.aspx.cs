using System;

using CMS.SettingsProvider;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.TreeEngine;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_Content_CMSDesk_Edit_EditPage : CMSContentPage
{
    protected string viewpage = "../blank.htm";
    protected TreeNode node = null;
    protected string loadScript = "FocusFrame();";
    protected string height = "1200";

    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptHelper.RegisterProgress(this);

        string notAllowedScript = ScriptHelper.GetScript("var notAllowedAction = '" + ResHelper.GetString("editpage.actionnotallowed") + "';");
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), "notAllowedAction", notAllowedScript);

        // Get the node ID
        int nodeId = QueryHelper.GetInteger("nodeid", 0);

        viewpage = "edit.aspx" + Request.Url.Query;

        string action = QueryHelper.GetString("action", "").ToLower();
        string mode = QueryHelper.GetString("mode", "").ToLower();

        string elementToCheck = "page";
        switch (mode)
        {
            case "edit":
                elementToCheck = "Page";
                break;

            case "design":
                elementToCheck = "Design";
                break;

            case "editform":
                elementToCheck = "EditForm";
                break;
        }

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", elementToCheck))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", elementToCheck);
        }

        switch (action)
        {
            default:
                // Check if new document desired
                bool newdocument = (action == "new");
                DataClassInfo classInfo = null;
                if (newdocument)
                {
                    // Get the class ID
                    int classId = QueryHelper.GetInteger("classid", 0);
                    classInfo = DataClassInfoProvider.GetDataClass(classId);
                }
                else
                {
                    // Get the document
                    TreeProvider tree = new TreeProvider(CMSContext.CurrentUser);
                    node = tree.SelectSingleNode(nodeId);
                    if (node != null)
                    {
                        classInfo = DataClassInfoProvider.GetDataClass(node.NodeClassName);
                    }
                }

                // Check the editing page change
                if (classInfo != null)
                {
                    if (CMSContext.ViewMode == ViewModeEnum.EditForm || newdocument)
                    {
                        // If new document, check if new page URL is set
                        if (newdocument)
                        {
                            if (!string.IsNullOrEmpty(classInfo.ClassNewPageURL))
                            {
                                // Use special settings for new page
                                if (classInfo.ClassNewPageURL.ToLower().EndsWith("/cmsmodules/content/cmsdesk/new/newpage.aspx"))
                                {
                                    // Use full heigt and disable onload script
                                    height = "100%";
                                    loadScript = String.Empty;
                                }

                                viewpage = UrlHelper.AppendQuery(ResolveUrl(classInfo.ClassNewPageURL), Request.Url.Query);
                            }
                        }
                        // If existing document, check if editing page URL is set
                        else if (!string.IsNullOrEmpty(classInfo.ClassEditingPageURL))
                        {
                            viewpage = UrlHelper.AppendQuery(ResolveUrl(classInfo.ClassEditingPageURL), Request.Url.Query);
                        }
                    }
                    else if (CMSContext.ViewMode == ViewModeEnum.Edit)
                    {
                        // Check if view page URL is set
                        if (!string.IsNullOrEmpty(classInfo.ClassViewPageUrl))
                        {
                            viewpage = UrlHelper.AppendQuery(ResolveUrl(classInfo.ClassViewPageUrl), Request.Url.Query);
                        }
                        else
                        {
                            viewpage = ResolveUrl(TreePathUtils.GetPermanentDocUrl(node.NodeGUID, node.NodeAlias, CMSContext.CurrentSiteName, PageInfoProvider.PREFIX_CMS_GETDOC, ".aspx"));
                        }
                    }
                    else if (CMSContext.ViewMode == ViewModeEnum.Design)
                    {
                        // Use permanent URL to get proper design mode
                        viewpage = ResolveUrl(TreePathUtils.GetPermanentDocUrl(node.NodeGUID, node.NodeAlias, CMSContext.CurrentSiteName, PageInfoProvider.PREFIX_CMS_GETDOC, ".aspx"));
                    }
                    else
                    {
                        // Use standard URL to get other modes
                        viewpage = ResolveUrl(CMSContext.GetUrl(node.NodeAliasPath, node.DocumentUrlPath));
                    }
                }
                break;
        }
    }
}
