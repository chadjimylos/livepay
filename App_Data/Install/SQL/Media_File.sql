CREATE TABLE [Media_File] (
		[FileID]                   int NOT NULL IDENTITY(1, 1),
		[FileName]                 nvarchar(250) NOT NULL,
		[FileTitle]                nvarchar(250) NOT NULL,
		[FileDescription]          nvarchar(max) NOT NULL,
		[FileExtension]            nvarchar(50) NOT NULL,
		[FileMimeType]             nvarchar(100) NOT NULL,
		[FilePath]                 nvarchar(450) NOT NULL,
		[FileSize]                 bigint NOT NULL,
		[FileImageWidth]           int NULL,
		[FileImageHeight]          int NULL,
		[FileGUID]                 uniqueidentifier NOT NULL,
		[FileLibraryID]            int NOT NULL,
		[FileSiteID]               int NOT NULL,
		[FileCreatedByUserID]      int NULL,
		[FileCreatedWhen]          datetime NOT NULL,
		[FileModifiedByUserID]     int NULL,
		[FileModifiedWhen]         datetime NOT NULL,
		[FileCustomData]           nvarchar(max) NULL
)
ON [PRIMARY]
ALTER TABLE [Media_File]
	ADD
	CONSTRAINT [PK_Media_File]
	PRIMARY KEY
	NONCLUSTERED
	([FileID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [Media_File]
	ADD
	CONSTRAINT [DEFAULT_Media_File_FileCreatedWhen]
	DEFAULT ('11/11/2008 4:10:00 PM') FOR [FileCreatedWhen]
ALTER TABLE [Media_File]
	ADD
	CONSTRAINT [DEFAULT_Media_File_FileModifiedWhen]
	DEFAULT ('11/11/2008 4:11:15 PM') FOR [FileModifiedWhen]
ALTER TABLE [Media_File]
	ADD
	CONSTRAINT [DEFAULT_Media_File_FileSize]
	DEFAULT ((0)) FOR [FileSize]
ALTER TABLE [Media_File]
	ADD
	CONSTRAINT [DEFAULT_Media_File_FileTitle]
	DEFAULT ('') FOR [FileTitle]
IF OBJECT_ID(N'[Media_File]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Media_File_FileCreatedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Media_File]
			ADD CONSTRAINT [FK_Media_File_FileCreatedByUserID_CMS_User]
			FOREIGN KEY ([FileCreatedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Media_File]') IS NOT NULL
	AND OBJECT_ID(N'[Media_Library]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Media_File_FileLibraryID_Media_Library]') IS NULL
BEGIN
		ALTER TABLE [Media_File]
			ADD CONSTRAINT [FK_Media_File_FileLibraryID_Media_Library]
			FOREIGN KEY ([FileLibraryID]) REFERENCES [Media_Library] ([LibraryID])
END
IF OBJECT_ID(N'[Media_File]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Media_File_FileModifiedByUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Media_File]
			ADD CONSTRAINT [FK_Media_File_FileModifiedByUserID_CMS_User]
			FOREIGN KEY ([FileModifiedByUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Media_File]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Media_File_FileSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Media_File]
			ADD CONSTRAINT [FK_Media_File_FileSiteID_CMS_Site]
			FOREIGN KEY ([FileSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE INDEX [IX_Media_File_FileCreatedByUserID]
	ON [Media_File] ([FileCreatedByUserID])
	ON [PRIMARY]
CREATE INDEX [IX_Media_File_FileLibraryID]
	ON [Media_File] ([FileLibraryID])
	ON [PRIMARY]
CREATE INDEX [IX_Media_File_FileModifiedByUserID]
	ON [Media_File] ([FileModifiedByUserID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_Media_File_FilePath]
	ON [Media_File] ([FilePath])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Media_File_FileSiteID_FileGUID]
	ON [Media_File] ([FileSiteID], [FileGUID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
