CREATE TABLE [Polls_PollAnswer] (
		[AnswerID]               int NOT NULL IDENTITY(1, 1),
		[AnswerText]             nvarchar(200) NOT NULL,
		[AnswerOrder]            int NULL,
		[AnswerCount]            int NULL,
		[AnswerEnabled]          bit NULL,
		[AnswerPollID]           int NOT NULL,
		[AnswerGUID]             uniqueidentifier NOT NULL,
		[AnswerLastModified]     datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Polls_PollAnswer]
	ADD
	CONSTRAINT [PK_Polls_PollAnswer]
	PRIMARY KEY
	NONCLUSTERED
	([AnswerID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Polls_PollAnswer]') IS NOT NULL
	AND OBJECT_ID(N'[Polls_Poll]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Polls_PollAnswer_AnswerPollID_Polls_Poll]') IS NULL
BEGIN
		ALTER TABLE [Polls_PollAnswer]
			ADD CONSTRAINT [FK_Polls_PollAnswer_AnswerPollID_Polls_Poll]
			FOREIGN KEY ([AnswerPollID]) REFERENCES [Polls_Poll] ([PollID])
END
CREATE CLUSTERED INDEX [IX_Polls_PollAnswer_AnswerPollID_AnswerOrder_AnswerEnabled]
	ON [Polls_PollAnswer] ([AnswerOrder], [AnswerPollID], [AnswerEnabled])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
