﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UcContact.ascx.vb" Inherits="CMSTemplates_LivePay_UcContact" %>
<script language="javascript" type="text/javascript" >
</script>

<div class="CUDDarkBlueBGTitle">
     <div class="SvdCardsTopTitle">Επικοινωνία</div>
     <div class="CUDContentBG">
        <div class="ContactContent">Παρακαλούμε συμπληρώστε την παρακάτω φόρμα. Εδώ μπαίνει το κείμενο της φόρμας επικοινωνίας</div>
        <div>
             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:125px;padding-right:10px;text-align:right">Ονοματεπώνυμο:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtFullName" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Ονοματεπώνυμο" runat="server" ControlToValidate="txtFullName" text="*" ValidationGroup="ContactForm"/>
                </div> 
            </div>

             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:125px;padding-right:10px;text-align:right">E-mail:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox ID="txtEmail" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ErrorMessage="Email" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmail" text="*" ValidationGroup="ContactForm"/>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" 
                    ValidationExpression="^([a-zA-Z0-9_\-])+(\.([a-zA-Z0-9_\-])+)*@((\[(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5])))\.(((([0-1])?([0-9])?[0-9])|(2[0-4][0-9])|(2[0-5][0-5]))\]))|((([a-zA-Z0-9])+(([\-])+([a-zA-Z0-9])+)*\.)+([a-zA-Z])+(([\-])+([a-zA-Z0-9])+)*))$" 
                    Display="Dynamic" ErrorMessage="Συμπληρώστε έγκυρη E-mail διεύθυνση" Text="*" ValidationGroup="ContactForm"/>
                </div>
            </div>

             <div style="height:45px;line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:125px;padding-right:10px;text-align:right">Κατηγορία:</div>
                <div style="float:left;padding-top:14px;padding-top:expression(2)">
                   <asp:DropDownList ID="ddlCategory" runat="server" style="color:#43474a;font-size:12px;font-family:Tahoma" width="239px">
                        <asp:ListItem Text="Επιλογή" Value=""></asp:ListItem>
                        <asp:ListItem Text="Κατηγορία 1" Value="1"></asp:ListItem>
                    </asp:DropDownList> 
                   
                </div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ErrorMessage="Κατηγορία" runat="server" ControlToValidate="ddlCategory" text="*" ValidationGroup="ContactForm"/>
                </div> 
            </div>

            <div style="line-height:45px;vertical-align:middle">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:125px;padding-right:10px;text-align:right">Κείμενο:</div>
                <div style="float:left;padding-top:14px"><asp:TextBox TextMode="MultiLine" Rows="3" ID="txtText" runat="server" style="border:1px solid #094595;color:#43474a;font-size:12px;font-family:Tahoma" width="234px"/></div>
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;padding-left:10px;">(*)</div>
                <div class="Clear"></div>
                <div style="display:none">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ErrorMessage="Κείμενο" runat="server" ControlToValidate="txtText" text="*" ValidationGroup="ContactForm"/>
                </div> 
            </div>
             <div style="padding-top:5px;padding-bottom:10px">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:125px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px;font-size:10px;color:#094595">Τα πεδία με τον αστερίσκο (*) είναι υποχρεωτικά
                </div>
                <div class="Clear"></div>
            </div>
             <div style="padding-top:5px;padding-bottom:10px">
                <div style="float:left;color:#094595;font-size:12px;font-weight:bold;width:145px;padding-right:10px;text-align:right">&nbsp;</div>
                <div style="float:left;padding-top:2px;text-align:right;width:220px">
                   <asp:ImageButton  id="BtnInsert" runat="server" ValidationGroup="ContactForm"  />
                </div>
                <div class="Clear"></div>
            </div>
        </div>
     </div>
     <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
</div> 
<asp:ValidationSummary HeaderText="Παρακαλώ συμπληρώστε τα ακόλουθα πεδία:" ID="ValidationSummary1" runat="server" DisplayMode="BulletList"  ShowMessageBox="True" ShowSummary="false" ErrorMessage="" ValidationGroup="ContactForm"/>