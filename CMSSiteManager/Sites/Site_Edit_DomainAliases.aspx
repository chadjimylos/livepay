<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Site_Edit_DomainAliases.aspx.cs"
    Inherits="CMSSiteManager_Sites_Site_Edit_DomainAliases" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master"
    Title="Site Edit - Domain Aliases" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:UniGrid ID="UniGridAliases" runat="server" GridName="Aliases_List.xml" OrderBy="SiteDomainAliasName"
        HideControlForZeroRows="true" Columns="SiteDomainAliasID, SiteDomainAliasName, SiteDefaultVisitorCulture"
        IsLiveSite="false" />
</asp:Content>
