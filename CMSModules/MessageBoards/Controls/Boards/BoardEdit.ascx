<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BoardEdit.ascx.cs" Inherits="CMSModules_MessageBoards_Controls_Boards_BoardEdit" %>
<table class="GroupEditTable">
    <tr>
        <td colspan="2">
            <asp:Label ID="lblInfo" runat="server" Visible="false" EnableViewState="false" CssClass="InfoLabel" />
            <asp:Label ID="lblError" runat="server" Visible="false" EnableViewState="false" CssClass="ErrorLabel" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblBoardOwner" runat="server" EnableViewState="false" CssClass="FieldLabel" />
        </td>
        <td>
            <asp:Label ID="lblBoardOwnerText" runat="server" EnableViewState="true" CssClass="FieldLabel" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblBoardDisplayName" runat="server" EnableViewState="false" CssClass="FieldLabel" />
        </td>
        <td>
            <asp:TextBox ID="txtBoardDisplayName" MaxLength="250" runat="server" EnableViewState="false"
                CssClass="TextBoxField" />
            <asp:RequiredFieldValidator ID="rfvBoardDisplayName" ControlToValidate="txtBoardDisplayName"
                runat="server" Display="Dynamic" EnableViewState="false" />
        </td>
    </tr>
    <asp:PlaceHolder ID="plcCodeName" runat="Server">
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblBoardCodeName" runat="server" EnableViewState="false" CssClass="FieldLabel" />
            </td>
            <td>
                <asp:TextBox ID="txtBoardCodeName" MaxLength="250" runat="server" CssClass="TextBoxField"
                    EnableViewState="true" Enabled="false" />
                <asp:RequiredFieldValidator ID="rfvBoardCodeName" ControlToValidate="txtBoardCodeName"
                    runat="server" Display="Dynamic" EnableViewState="false" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblBoardDescription" runat="server" EnableViewState="false" CssClass="FieldLabel" />
        </td>
        <td>
            <asp:TextBox ID="txtBoardDescription" runat="server" TextMode="MultiLine" CssClass="TextAreaField"
                EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblBoardEnable" runat="server" EnableViewState="false" CssClass="FieldLabel" />
        </td>
        <td>
            <asp:CheckBox ID="chkBoardEnable" runat="server" CssClass="CheckBoxMovedLeft" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblBoardOpen" runat="server" Text="Label" EnableViewState="false"
                CssClass="FieldLabel" />
        </td>
        <td>
            <asp:CheckBox ID="chkBoardOpen" runat="server" CssClass="CheckBoxMovedLeft" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblBoardOpenFrom" runat="server" EnableViewState="false" CssClass="FieldLabel" />
        </td>
        <td>
            <cms:DateTimePicker ID="dtpBoardOpenFrom" runat="server" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblBoardOpenTo" runat="server" EnableViewState="false" CssClass="FieldLabel" />
        </td>
        <td>
            <cms:DateTimePicker ID="dtpBoardOpenTo" runat="server" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <cms:LocalizedLabel ID="lblSubscriptionsEnable" runat="server" DisplayColon="true"
                ResourceString="board.edit.enablesubscriptions" EnableViewState="false" />
        </td>
        <td>
            <asp:CheckBox ID="chkSubscriptionsEnable" runat="server" CssClass="CheckBoxMovedLeft"
                EnableViewState="false" />
        </td>
    </tr>
    <asp:PlaceHolder runat="server" ID="plcUnsubscription">
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblBaseUrl" runat="server" EnableViewState="false" CssClass="FieldLabel" />
            </td>
            <td>
                <asp:TextBox ID="txtBaseUrl" runat="server" CssClass="TextBoxField" EnableViewState="false" />
                <cms:LocalizedCheckBox runat="server" ID="chkInheritBaseUrl" Checked="true" ResourceString="boards.inheritbaseurl"
                    EnableViewState="false" />
            </td>
        </tr>
        <tr>
            <td class="FieldLabel">
                <asp:Label ID="lblUnsubscriptionUrl" runat="server" EnableViewState="false" CssClass="FieldLabel" />
            </td>
            <td>
                <asp:TextBox ID="txtUnsubscriptionUrl" runat="server" CssClass="TextBoxField" EnableViewState="false" />
                <cms:LocalizedCheckBox runat="server" ID="chkInheritUnsubUrl" Checked="true" ResourceString="boards.inheritbaseurl"
                    EnableViewState="false" />
            </td>
        </tr>
    </asp:PlaceHolder>
    <tr>
        <td class="FieldLabel">
            <asp:Label ID="lblBoardRequireEmail" runat="server" EnableViewState="false" CssClass="FieldLabel" />
        </td>
        <td>
            <asp:CheckBox ID="chkBoardRequireEmail" runat="server" CssClass="CheckBoxMovedLeft"
                EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            <cms:CMSButton ID="btnOk" runat="server" EnableViewState="false" OnClick="btnOk_Click"
                CssClass="SubmitButton" />
        </td>
    </tr>
</table>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />