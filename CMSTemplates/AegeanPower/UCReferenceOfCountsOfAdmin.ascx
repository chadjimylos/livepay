﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCReferenceOfCountsOfAdmin.ascx.vb" Inherits="CMSTemplates_AegeanPower_UCReferenceOfCountsOfAdmin" %>

<style type="text/css">
.ButtonSub
{
    background-image:url("../App_Themes/AegeanPower/Images/submitbutton.png");
    background-position:left;
    background-repeat:no-repeat;
    border:0pc;
    color:White;
    width:102px;
    height:19px;
    float:right;
    padding:0px 0px 3px 0px;
    margin-top:10px;
}

.BillBgBlue{margin-top:10px;}
.BillsGv_Title a {color:#004990;text-decoration:none;}
.ClassExportToExcel{width:20px;height:20px;margin:-15px 0px 0px 10px; cursor:pointer;float:right;}

</style>

<script language="javascript" type="text/javascript">
    function setURL(url) {
       document.getElementById("IfrExport").src = url
    }

</script>

<div style="float:left; width:727px; background-color:White; padding:20px; margin:10px 0px 20px 0px; padding:20px 20px 20px 100px;">

<div style="margin:10px 0px; float:left; width:350px;">
    <div class="AdminUploadTitle" style="float:left;width:150px;"><%=ResHelper.LocalizeString("{$=Αριθμός Παροχής:|en-us=Αριθμός Παροχής:$}") %></div>
<asp:TextBox ID="txtboxProviderNumber" runat="server"></asp:TextBox>
</div>

<div style="margin:10px 0px;float:left; width:350px;">
    <div class="AdminUploadTitle" style="float:left;width:80px;"><%= ResHelper.LocalizeString("{$=Πελάτης:|en-us=Customer:$}")%></div>
<asp:TextBox ID="txtboxCustomer" runat="server"></asp:TextBox>
</div>

<div style="margin-bottom:10px; margin-top:10px; float:left; width:700px; font-weight:bold;"><%= ResHelper.LocalizeString("{$=Περίοδος καταχωρήσεων|en-us=Περίοδος καταχωρήσεων$}")%></div>

<div style="margin:10px 0px; float:left; width:350px;">
    <div class="AdminUploadTitle" style="float:left;width:86px; margin-left:64px;"><%= ResHelper.LocalizeString("{$=Από:|en-us=From:$}")%></div>
<asp:TextBox ID="txtboxFrom" runat="server" MaxLength="10"></asp:TextBox>
</div>  

<div style="margin:10px 0px; float:left; width:350px;">
    <div class="AdminUploadTitle" style="float:left;width:64px; margin-left:18px;"><%= ResHelper.LocalizeString("{$=Έως:|en-us=To:$}")%></div>
<asp:TextBox ID="txtboxTo" runat="server" MaxLength="10"></asp:TextBox>
</div>       
        
<div style=" margin:10px 0px 10px 0px; float:left; width:395px;">
<asp:Button ID="btnOk" runat="server" CssClass="ButtonSub" EnableViewState="false" Text="Αναζήτηση" /> 
</div>
<div style="clear:both"></div>
</div>
<div style="clear:both"></div>


<div id="AnswerBanner" style="float:left; width:807px; background-color:White; padding:20px; margin-bottom:20px;" runat="server" visible="false" >
<asp:GridView ID="ReferenceOfCounts" runat="server" AutoGenerateColumns="false" GridLines="None" AllowPaging="true" AllowSorting="true" OnSorting ="TaskGridView_Sorting">
            <AlternatingRowStyle CssClass="BillsGv_RowB" />
            <RowStyle  CssClass="BillsGv_RowA" />
            <Columns>

    <asp:TemplateField HeaderText="Αριθμός Παροχής" HeaderStyle-Width="100px" HeaderStyle-CssClass="BillsGv_Title" SortExpression="SelectCounter">
        <ItemTemplate>
            <asp:Label ID="lblSelectCounter" runat="server" Text='<%#Eval("SelectCounter") %>'></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>
    
    <asp:TemplateField HeaderText="Επωνυμία Πελάτη" HeaderStyle-Width="150px" HeaderStyle-CssClass="BillsGv_Title" SortExpression="FullName">
        <ItemTemplate>
            <asp:Label ID="lblFullName" runat="server" Text='<%#Eval("FullName") %>' ></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="¨Ενδειξη Μετρητή" HeaderStyle-Width="100px" HeaderStyle-CssClass="BillsGv_Title" SortExpression="Indication">
        <ItemTemplate>
            <asp:Label ID="lblIndication" runat="server" Text='<%#Eval("Indication") %>' ></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="Άεργος ένδειξη μετρητή" HeaderStyle-Width="130px" HeaderStyle-CssClass="BillsGv_Title" SortExpression="ReactiveMeterReading">
        <ItemTemplate>
            <asp:Label ID="lblReactiveMeterReading" runat="server" Text='<%#Eval("ReactiveMeterReading") %>' ></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="Νυχτερινή ένδειξη μετρητή" HeaderStyle-Width="145px" HeaderStyle-CssClass="BillsGv_Title" SortExpression="NightlifeMeterReading">
        <ItemTemplate>
            <asp:Label ID="lblNightlifeMeterReading" runat="server" Text='<%#Eval("NightlifeMeterReading") %>' ></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

    <asp:TemplateField HeaderText="Ημερομηνία Καταχώρησης" HeaderStyle-Width="140px" HeaderStyle-CssClass="BillsGv_Title" SortExpression="FormInserted">
        <ItemTemplate>
            <asp:Label ID="lblFormInserted" runat="server" Text='<%#Eval("FormInserted") %>' ></asp:Label>
        </ItemTemplate>
    </asp:TemplateField>

</Columns>
<EmptyDataTemplate>
<div style="padding:10px 0px 5px 10px;"><%= ResHelper.LocalizeString("{$=Δεν βρέθηκαν Δεδομένα.|en-us=No Data found.$}")%></div>
</EmptyDataTemplate>
</asp:GridView>

<div id="ExcelExport" style="width:100px;float:right; font-family:Tahoma;color:#004990;cursor:pointer; margin:10px 35px 0px 0px;" runat="server" visible="false">
    <a title="Export to excel" onclick="setURL('/CMSTemplates/AegeanPower/ExportToExcel.aspx')">Export to excel
        <img alt="" class="ClassExportToExcel" title="Export to Excel" src="/App_Themes/AegeanPower/Images/ExportToExcel/excel-icon.png"/>
    </a>
</div>
</div>
<div style="clear:both"></div>

<iframe id="IfrExport" style="display:none;"></iframe>
