using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.EmailEngine;
using CMS.SiteProvider;
using CMS.DataEngine;
using CMS.UIControls;

public partial class CMSSiteManager_Development_EmailTemplates_EmailTemplate_List : SiteManagerPage
{
    protected int siteId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        // Setup page title text and image
        this.CurrentMaster.Title.TitleText = ResHelper.GetString("EmailTemplate_List.Title");
        this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/CMS_EmailTemplate/object.png");

        this.CurrentMaster.Title.HelpTopicName = "e_mail_templates_list";
        this.CurrentMaster.Title.HelpName = "helpTopic";

        string[,] actions = new string[1, 6];

        // New item link
        actions[0, 0] = HeaderActions.TYPE_HYPERLINK;
        actions[0, 1] = ResHelper.GetString("EmailTemplate_List.NewTemplateButton");
        actions[0, 2] = null;
        actions[0, 3] = "javascript: AddNewItem();";
        actions[0, 4] = null;
        actions[0, 5] = GetImageUrl("Objects/CMS_EmailTemplate/add.png");
        this.CurrentMaster.HeaderActions.Actions = actions;

        this.CurrentMaster.DisplayControlsPanel = true;

        this.RegisterExportScript();

        uniGrid.OnAction += new OnActionEventHandler(UniGrid_OnAction);

        // initializes dropdownlist
        InitializeDropDownListSites();

        siteId = ValidationHelper.GetInteger(siteSelector.Value, 0);

        uniGrid.WhereCondition = GenerateWhereCondition();
        uniGrid.ZeroRowsText = ResHelper.GetString("general.nodatafound");

        // Register correct script for new item
        ScriptHelper.RegisterClientScriptBlock(this.Page, typeof(string), "AddNewItem", ScriptHelper.GetScript(
            "function AddNewItem() { this.window.location = '" + ResolveUrl("EmailTemplate_Edit.aspx?siteid=" + siteId) + "'} "));
    }


    /// <summary>
    /// Loads control.
    /// </summary>
    private string GenerateWhereCondition()
    {
        // sets UniGrid "WhereCondition"
        if (siteId == 0)
        {
            return "EmailTemplateSiteID is NULL";
        }
        else
        {
            return "EmailTemplateSiteID = " + siteId;
        }
    }


    /// <summary>
    /// Handles the UniGrid's OnAction event.
    /// </summary>
    /// <param name="actionName">Name of item (button) that throws event.</param>
    /// <param name="actionArgument">ID (value of Primary key) of corresponding data row.</param>
    protected void UniGrid_OnAction(string actionName, object actionArgument)
    {
        switch (actionName.ToLower())
        {
            case "edit":
                UrlHelper.Redirect("EmailTemplate_Edit.aspx?templateid=" + actionArgument.ToString());
                return;
            case "delete":
                int objid = ValidationHelper.GetInteger(actionArgument, 0);

                // Delete all attached metafiles, if any
                MetaFileInfoProvider.DeleteFiles(objid, EmailObjectType.EMAILTEMPLATE, MetaFileInfoProvider.OBJECT_CATEGORY_TEMPLATE);
                EmailTemplateProvider.DeleteEmailTemplate(objid);
                return;
        }
    }


    /// <summary>
    /// Loads list of sites from DB into DropDownList
    /// </summary>
    protected void InitializeDropDownListSites()
    {
        // Set site selector
        siteSelector.OnlyRunningSites = false;
        siteSelector.DropDownSingleSelect.AutoPostBack = true;
        siteSelector.AllowAll = false;
        siteSelector.UniSelector.SpecialFields = new string[1, 2] { { ResHelper.GetString("general.global"), "0" } };
        siteSelector.UniSelector.OnSelectionChanged += new EventHandler(UniSelector_OnSelectionChanged);

        if (!RequestHelper.IsPostBack())
        {
            siteId = QueryHelper.GetInteger("siteid", 0);
            siteSelector.Value = siteId;
        }
    }


    /// <summary>
    /// Handles site selection change event
    /// </summary>
    protected void UniSelector_OnSelectionChanged(object sender, EventArgs e)
    {
        pnlUpdate.Update();
    }
}
