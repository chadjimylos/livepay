// Register the related command.
FCKCommands.RegisterCommand( 'InsertInlineControls', new FCKDialogCommand( 'InsertInlineControls', FCKLang.InsertInlineControlsTitle, FCKPlugins.Items['InsertInlineControls'].Path + 'fck_InsertInlineControls.aspx?' + FCKConfig.DialogParameters, 550, 420 ) ) ;

// Create the "Placeholder" toolbar button.
var oInsertInlineControls = new FCKToolbarButton( 'InsertInlineControls', FCKLang.InsertInlineControlsButton ) ;
oInsertInlineControls.IconPath = FCKPlugins.Items['InsertInlineControls'].Path + 'fckInlineControls.gif' ;
FCKToolbarItems.RegisterItem( 'InsertInlineControls', oInsertInlineControls ) ;
