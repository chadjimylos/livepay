﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeFile="Default.aspx.cs" Inherits="CMSFormControls_LiveSelectors_InsertImageOrMedia_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server" enableviewstate="false">
    <title>Insert</title>
</head>
<frameset border="0" rows="72, *, 43" id="rowsFrameset">
		<frame name="insertHeader" src="<%=mBlankUrl%>" scrolling="no" frameborder="0" noresize="noresize" id="menu" />
	    <frame name="insertContent" src="<%=mBlankUrl%>" scrolling="no" frameborder="0" id="content" />
	    <frame name="insertFooter" src="Footer.aspx<%=Request.Url.Query%>" scrolling="no" frameborder="0" noresize="noresize" id="footer" />
	<noframes>
    <body>
        <p id="p1">
            This HTML frameset displays multiple Web pages. To view this frameset, use a Web
            browser that supports HTML 4.0 and later.
        </p>
    </body>
</noframes>
</frameset>
</html>
