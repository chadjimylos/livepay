CREATE TABLE [CMS_Form] (
		[FormID]                            int NOT NULL IDENTITY(1, 1),
		[FormDisplayName]                   nvarchar(100) NOT NULL,
		[FormName]                          nvarchar(100) NOT NULL,
		[FormSendToEmail]                   nvarchar(400) NULL,
		[FormSendFromEmail]                 nvarchar(400) NULL,
		[FormEmailSubject]                  nvarchar(250) NULL,
		[FormEmailTemplate]                 nvarchar(max) NULL,
		[FormEmailAttachUploadedDocs]       bit NULL,
		[FormClassID]                       int NOT NULL,
		[FormItems]                         int NOT NULL,
		[FormReportFields]                  nvarchar(max) NULL,
		[FormRedirectToUrl]                 nvarchar(400) NULL,
		[FormDisplayText]                   nvarchar(max) NULL,
		[FormClearAfterSave]                bit NOT NULL,
		[FormSubmitButtonText]              nvarchar(400) NULL,
		[FormSiteID]                        int NOT NULL,
		[FormConfirmationEmailField]        nvarchar(100) NULL,
		[FormConfirmationTemplate]          nvarchar(max) NULL,
		[FormConfirmationSendFromEmail]     nvarchar(400) NULL,
		[FormConfirmationEmailSubject]      nvarchar(250) NULL,
		[FormAccess]                        int NULL,
		[FormSubmitButtonImage]             nvarchar(255) NULL,
		[FormGUID]                          uniqueidentifier NOT NULL,
		[FormLastModified]                  datetime NOT NULL
)
ON [PRIMARY]
ALTER TABLE [CMS_Form]
	ADD
	CONSTRAINT [PK_CMS_Form]
	PRIMARY KEY
	NONCLUSTERED
	([FormID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
ALTER TABLE [CMS_Form]
	ADD
	CONSTRAINT [DF__CMS_Form__FormCl__2645B050]
	DEFAULT ((0)) FOR [FormClearAfterSave]
ALTER TABLE [CMS_Form]
	ADD
	CONSTRAINT [DF__CMS_Form__FormIt__2739D489]
	DEFAULT ((0)) FOR [FormItems]
ALTER TABLE [CMS_Form]
	ADD
	CONSTRAINT [DF_CMS_Form_FormEmailAttachUploadedDocs]
	DEFAULT ((1)) FOR [FormEmailAttachUploadedDocs]
IF OBJECT_ID(N'[CMS_Form]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Class]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Form_FormClassID_CMS_Class]') IS NULL
BEGIN
		ALTER TABLE [CMS_Form]
			ADD CONSTRAINT [FK_CMS_Form_FormClassID_CMS_Class]
			FOREIGN KEY ([FormClassID]) REFERENCES [CMS_Class] ([ClassID])
END
IF OBJECT_ID(N'[CMS_Form]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_CMS_Form_FormSiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [CMS_Form]
			ADD CONSTRAINT [FK_CMS_Form_FormSiteID_CMS_Site]
			FOREIGN KEY ([FormSiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE INDEX [IX_CMS_Form_FormClassID]
	ON [CMS_Form] ([FormClassID])
	ON [PRIMARY]
CREATE CLUSTERED INDEX [IX_CMS_Form_FormDisplayName]
	ON [CMS_Form] ([FormDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_CMS_Form_FormSiteID]
	ON [CMS_Form] ([FormSiteID])
	ON [PRIMARY]
