using System;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.SettingsProvider;
using CMS.UIControls;
using CMS.SiteProvider;

public partial class CMSModules_Friends_Administration_Users_User_Edit_Friends_ToApprove : CMSUsersPage
{
    #region "Variables"

    protected int userId = 0;
    protected CurrentUserInfo currentUser = null;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        userId = QueryHelper.GetInteger("userId", 0);
        currentUser = CMSContext.CurrentUser;

        // Check 'read' permissions
        if (!currentUser.IsAuthorizedPerResource("CMS.Friends", "Read") && (currentUser.UserID != userId))
        {
            RedirectToAccessDenied("CMS.Friends", "Read");
        }

        // Check license
        if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), string.Empty) != string.Empty)
        {
            LicenseHelper.CheckFeatureAndRedirect(UrlHelper.GetCurrentDomain(), FeatureEnum.Friends);
        }

        // Check that only global administrator can edit global administrator's accounts
        UserInfo ui = UserInfoProvider.GetUserInfo(userId);
        if (!CheckGlobalAdminEdit(ui))
        {
            plcTable.Visible = false;
            lblError.Text = ResHelper.GetString("Administration-User_List.ErrorGlobalAdmin");
            lblError.Visible = true;
        }
        else
        {
            userId = QueryHelper.GetInteger("userId", 0);
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);
            FriendsListToApprove.UserID = userId;
            FriendsListToApprove.OnCheckPermissions += CheckPermissions;
        }
    }


    void CheckPermissions(string permissionType, CMSAdminControl sender)
    {
        CurrentUserInfo currentUser = CMSContext.CurrentUser;
        if ((!currentUser.IsAuthorizedPerResource("CMS.Friends", permissionType)) && (currentUser.UserID != userId))
        {
            RedirectToAccessDenied("CMS.Friends", permissionType);
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        lblInfo.Visible = !FriendsListToApprove.HasData();
    }
}
