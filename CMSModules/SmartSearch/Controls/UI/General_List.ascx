﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="General_List.ascx.cs" Inherits="CMSModules_SmartSearch_Controls_UI_General_List" %>
<%@ Register Src="~/CMSFormControls/Inputs/LargeTextArea.ascx" TagName="LargeTextArea" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" TagName="UniSelector" TagPrefix="cms" %>
<cms:LocalizedLabel runat="server" CssClass="InfoLabel" ID="lblInfo" EnableViewState="false" Visible="false"></cms:LocalizedLabel>
<table>
    <tr>
        <td class="FieldLabel">
            <cms:LocalizedLabel ID="lblVisible" runat="server" ResourceString="srch.index.objectname"
                EnableViewState="false" DisplayColon="true" />
        </td>
        <td>
            <cms:Uniselector runat="server" ID="selObjects" ReturnColumnName="ClassName" IsLiveSite="false" ObjectType="cms.class" AllowEmpty="false" AllowAll="false" WhereCondition="ClassIsDocumentType = 0 AND ClassIsCustomTable = 0 AND ClassIsCoupledClass = 1 AND ClassName NOT LIKE 'BizForm.%'" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
            <cms:LocalizedLabel ID="lblWhere" runat="server" ResourceString="srch.index.where"
                EnableViewState="false" DisplayColon="true" />
        </td>
        <td>
            <cms:LargeTextArea ID="txtWhere" AllowMacros="false" runat="server" />
        </td>
    </tr>
    <tr>
        <td class="FieldLabel">
        </td>
        <td>
            <cms:LocalizedButton runat="server" ID="btnOk" CssClass="SubmitButton" ResourceString="general.ok" OnClick="btnOk_Click" EnableViewState="false" />
        </td>
    </tr>
</table>