using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.Ecommerce;
using CMS.CMSHelper;
using CMS.UIControls;

public partial class CMSModules_Ecommerce_Tools_Configuration_ExchangeRates_ExchangeTable_Edit : CMSEcommerceConfigurationPage
{
    protected int exchangeid = 0;
    private Hashtable mTextBoxs = new Hashtable();
    private Hashtable mData = new Hashtable();
    private DataSet exchangeRates = null;


    protected void Page_Load(object sender, EventArgs e)
    {
        editGrid.RowDataBound += new GridViewRowEventHandler(editGrid_RowDataBound);


        rfvDsiplayName.ErrorMessage = ResHelper.GetString("general.requiresdisplayname");

        // control initializations				
        lblExchangeTableValidFrom.Text = ResHelper.GetString("ExchangeTable_Edit.ExchangeTableValidFromLabel");
        lblExchangeTableDisplayName.Text = ResHelper.GetString("ExchangeTable_Edit.ExchangeTableDisplayNameLabel");
        lblExchangeTableValidTo.Text = ResHelper.GetString("ExchangeTable_Edit.ExchangeTableValidToLabel");

        // Help image
        this.imgHelp.ImageUrl = GetImageUrl("General/HelpSmall.png");
        this.imgHelp.ToolTip = ResHelper.GetString("ExchangeTable_Edit.ExchangeRateHelp");

        lblRates.Text = ResHelper.GetString("ExchangeTable_Edit.ExchangeRates");

        btnOk.Text = ResHelper.GetString("General.OK");
        dtPickerExchangeTableValidFrom.SupportFolder = "~/CMSAdminControls/Calendar";
        dtPickerExchangeTableValidTo.SupportFolder = "~/CMSAdminControls/Calendar";

        string currentExchangeTable = ResHelper.GetString("ExchangeTable_Edit.NewItemCaption");

        // get exchangeTable id from querystring		
        exchangeid = ValidationHelper.GetInteger(Request.QueryString["exchangeid"], 0);
        if (exchangeid > 0)
        {
            exchangeRates = ExchangeTableInfoProvider.GetRatesByTableID(exchangeid);

            ExchangeTableInfo exchangeTableObj = ExchangeTableInfoProvider.GetExchangeTableInfo(exchangeid);
            if (exchangeTableObj != null)
            {
                currentExchangeTable = exchangeTableObj.ExchangeTableDisplayName;

                LoadData(exchangeTableObj);

                // fill editing form
                if (!RequestHelper.IsPostBack())
                {
                    // show that the exchangeTable was created or updated successfully
                    if (ValidationHelper.GetString(Request.QueryString["saved"], "") == "1")
                    {
                        lblInfo.Visible = true;
                        lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                    }
                }
            }
        }
        else
        {
            if (!RequestHelper.IsPostBack())
            {
                ExchangeTableInfo tableInfo = ExchangeTableInfoProvider.GetLastValidExchangeTableInfo();


                if (tableInfo != null)
                {
                    dtPickerExchangeTableValidFrom.SelectedDateTime = tableInfo.ExchangeTableValidTo;
                }
            }
            plcGrid.Visible = false;
        }

        // Initializes page title breadcrumbs control		
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = ResHelper.GetString("ExchangeTable_Edit.ItemListLink");
        pageTitleTabs[0, 1] = "~/CMSModules/Ecommerce/Tools/Configuration/ExchangeRates/ExchangeTable_List.aspx";
        pageTitleTabs[0, 2] = "";
        pageTitleTabs[1, 0] = currentExchangeTable;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";
        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;

        int mexchangeid = ValidationHelper.GetInteger(Request.QueryString["exchangeid"], 0);
        if (mexchangeid > 0)
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("ExchangeTable_Edit.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_ExchangeTable/object.png");
        }
        else
        {
            this.CurrentMaster.Title.TitleText = ResHelper.GetString("ExchangeTable_New.HeaderCaption");
            this.CurrentMaster.Title.TitleImage = GetImageUrl("Objects/Ecommerce_ExchangeTable/new.png");
        }

        this.CurrentMaster.Title.HelpTopicName = "new_rateexchange_rate_edit";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }


    void editGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItemIndex >= 0)
        {
            TextBox txt = new TextBox();
            txt.CssClass = "ShortTextBox";
            txt.TextChanged += new EventHandler(txt_TextChanged);
            txt.MaxLength = 10;

            int mCurId = ValidationHelper.GetInteger(((DataRowView)e.Row.DataItem)["CurrencyID"], 0);
            string mRateValue = "";

            if (!DataHelper.DataSourceIsEmpty(exchangeRates))
            {
                foreach (DataRow dr in exchangeRates.Tables[0].Rows)
                {
                    if (ValidationHelper.GetInteger(dr["ExchangeRateToCurrencyID"], -1) == mCurId)
                    {
                        mRateValue = ValidationHelper.GetDouble(dr["ExchangeRateValue"], -1).ToString();
                    }
                }
            }

            txt.Text = mRateValue;

            e.Row.Cells[1].Controls.Add(txt);

            mData[txt.ClientID] = e.Row.DataItem;
        }
    }


    void txt_TextChanged(object sender, EventArgs e)
    {
        mTextBoxs[((TextBox)sender).ClientID] = sender;
    }


    /// <summary>
    /// Load data of editing exchangeTable.
    /// </summary>
    /// <param name="exchangeTableObj">ExchangeTable object.</param>
    protected void LoadData(ExchangeTableInfo exchangeTableObj)
    {
        editGrid.Columns[0].HeaderText = ResHelper.GetString("ExchangeTable_Edit.ToCurrency");
        editGrid.Columns[1].HeaderText = ResHelper.GetString("ExchangeTable_Edit.RateValue");

        CurrencyInfo ci = CurrencyInfoProvider.GetMainCurrency();
        DataSet dsCurrency = CurrencyInfoProvider.GetAllCurrencies();
        int toRemove = -1;
        int i = 0;


        if (!DataHelper.DataSourceIsEmpty(dsCurrency))
        {
            if (ci != null)
            {
                foreach (DataRow dr in dsCurrency.Tables[0].Rows)
                {
                    if (ValidationHelper.GetInteger(dr["CurrencyID"], -1) == ci.CurrencyID)
                    {
                        toRemove = i;
                    }
                    i++;
                }
            }

            if (toRemove != -1)
            {
                dsCurrency.Tables[0].Rows[toRemove].Delete();
                dsCurrency.AcceptChanges();
            }

            editGrid.DataSource = dsCurrency;
            editGrid.DataBind();
        }

        if (!RequestHelper.IsPostBack())
        {
            dtPickerExchangeTableValidFrom.SelectedDateTime = exchangeTableObj.ExchangeTableValidFrom;
            txtExchangeTableDisplayName.Text = exchangeTableObj.ExchangeTableDisplayName;
            dtPickerExchangeTableValidTo.SelectedDateTime = exchangeTableObj.ExchangeTableValidTo;
        }
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // check 'ConfigurationModify' permission
        if (!CMSContext.CurrentUser.IsAuthorizedPerResource("CMS.Ecommerce", "ConfigurationModify"))
        {
            RedirectToAccessDenied("CMS.Ecommerce", "ConfigurationModify");
        }

        string errorMessage = new Validator().NotEmpty(txtExchangeTableDisplayName.Text, ResHelper.GetString("general.requiresdisplayname")).Result;

        // from/to date validation
        if (errorMessage == "")
        {
            if ((dtPickerExchangeTableValidFrom.SelectedDateTime != DateTime.MinValue) &&
            (dtPickerExchangeTableValidTo.SelectedDateTime != DateTime.MinValue) &&
            (dtPickerExchangeTableValidFrom.SelectedDateTime >= dtPickerExchangeTableValidTo.SelectedDateTime))
            {
                errorMessage = ResHelper.GetString("General.DateOverlaps");
            }
        }

        if (errorMessage == String.Empty)
        {
            foreach (TextBox txt in mTextBoxs.Values)
            {
                string tmp = txt.Text.Trim();
                if (tmp != String.Empty)
                {
                    if (!ValidationHelper.IsDouble(tmp))
                    {
                        errorMessage = ResHelper.GetString("ExchangeTable_Edit.DoubleFormatRequired");
                        break;
                    }
                    else if (!ValidationHelper.IsPositiveNumber(tmp))
                    {
                        errorMessage = ResHelper.GetString("ExchangeTable_Edit.errorRate");
                    }
                }
            }
        }

        if (errorMessage == "")
        {
            ExchangeTableInfo exchangeTableObj = ExchangeTableInfoProvider.GetExchangeTableInfo(txtExchangeTableDisplayName.Text.Trim());

            // if currencyName value is unique														
            if ((exchangeTableObj == null) || (exchangeTableObj.ExchangeTableID == exchangeid))
            {
                // if currencyName value is unique -> determine whether it is update or insert 
                if ((exchangeTableObj == null))
                {
                    // get CurrencyInfo object by primary key
                    exchangeTableObj = ExchangeTableInfoProvider.GetExchangeTableInfo(exchangeid);
                    if (exchangeTableObj == null)
                    {
                        // create new item -> insert
                        exchangeTableObj = new ExchangeTableInfo();
                    }
                }

                exchangeTableObj.ExchangeTableValidFrom = dtPickerExchangeTableValidFrom.SelectedDateTime;
                exchangeTableObj.ExchangeTableDisplayName = txtExchangeTableDisplayName.Text.Trim();
                exchangeTableObj.ExchangeTableValidTo = dtPickerExchangeTableValidTo.SelectedDateTime;

                ExchangeTableInfoProvider.SetExchangeTableInfo(exchangeTableObj);

                // Save rates on edit
                if (exchangeid > 0)
                {
                    DataSet mDs = ExchangeTableInfoProvider.GetRatesByTableID(exchangeid);

                    foreach (TextBox txt in mTextBoxs.Values)
                    {
                        bool isAffected = false;

                        if (mData[txt.ClientID] != null)
                        {
                            int mCurId = ValidationHelper.GetInteger(((DataRowView)mData[txt.ClientID])["CurrencyID"], 0);

                            if (!DataHelper.DataSourceIsEmpty(mDs))
                            {
                                foreach (DataRow dr in mDs.Tables[0].Rows)
                                {
                                    if (ValidationHelper.GetInteger(dr["ExchangeRateToCurrencyID"], -1) == mCurId)
                                    {
                                        string tmp = txt.Text.Trim();
                                        int exchangeRateId = ValidationHelper.GetInteger(dr["ExchagneRateID"], 0);
                                        if (tmp == String.Empty)
                                        {
                                            ExchangeTableInfoProvider.DeleteRateByRateId(mCurId, exchangeid);
                                        }
                                        else
                                        {
                                            ExchangeTableInfoProvider.SetRateByRateId(exchangeRateId, mCurId, ValidationHelper.GetDouble(tmp, 0), exchangeid);
                                        }
                                        isAffected = true;
                                    }
                                }
                            }

                            // Insert
                            if (!isAffected)
                            {
                                ExchangeTableInfoProvider.SetRateByRateId(0, mCurId, ValidationHelper.GetDouble(txt.Text.Trim(), 0), exchangeid);
                            }
                        }
                    }
                }

                UrlHelper.Redirect("ExchangeTable_Edit.aspx?exchangeid=" + Convert.ToString(exchangeTableObj.ExchangeTableID) + "&saved=1");
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("ExchangeTable_Edit.CurrencyNameExists");
            }
        }
        else
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
        }
    }
}
