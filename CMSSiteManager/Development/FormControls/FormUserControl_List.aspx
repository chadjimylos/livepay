<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FormUserControl_List.aspx.cs"
    Inherits="CMSSiteManager_Development_FormControls_FormUserControl_List" Theme="Default"
    MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Form User Controls - Form User Control List" %>
<%@ Register src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" tagname="UniGrid" tagprefix="cms" %>

<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:UniGrid ID="UniGrid1" runat="server" GridName="FormUserControl_List.xml" OrderBy="UserControlDisplayName"
        Columns="UserControlID, UserControlDisplayName" IsLiveSite="false" />
</asp:Content>
