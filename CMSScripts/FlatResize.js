﻿//<![CDATA[

// Initial body height
var initHeigh = document.body.clientHeight;
// Maximal timer calls. 10 = 2 second
var maxTimerCount = 40;
// Current timer count
var timerCounter = 0;
// Offset value
var offsetValue = 1;
// Minimal height
var minHeight = 370


var itemSelector = null;
var selectorTree = null;
var uniFlatContent = null;
var uniFlatSearchPanel = null;
var uniFlatPager = null;
var selectorFlatDescription = null;

// After DOM ready
$j(document.body).ready(initializeResize);

// Initialize resize
function initializeResize() {
    
    // Get items
    getItems(false);
    // Initial resize
    resizeareainternal();
    // Create timer
    //setTimeout(resizeChecker, 200);
    $j(window).resize(function() { resizeareainternal(); });
}


// Check whether document is ready and set correct values
function resizeChecker() {
    
    // Check whether page is ready => body height contains correct values
    if (document.body.clientHeight != initHeigh) {

        // Resize elements
        resizearea();

        // Set window resize handler
        $j(window).resize(function() { resizeareainternal(); });
    }
    else {

        // Check whether current call is smaller than max. calls count
        if (timerCounter < maxTimerCount) {
            timerCounter++;
            setTimeout(resizeChecker, 200);
        }
        else {
            // Set window resize handler
            $j(window).resize(function() { resizeareainternal(); });
            // Clear counter
            timerCounter = 0;
        }
    }
}

// Resize elements handler
function resizearea() {
    getItems(true);
    resizeareainternal();
}


// Resize elements
function resizeareainternal() {
    // Main height
    height = document.body.clientHeight - itemSelector.offset().top - $j("div .CopyLayoutPanel").outerHeight(true) - $j("#__ButtonsArea").outerHeight(true) - offsetValue;
    
    // Set minimal height
    if (height < minHeight) {
        height = minHeight;
    }
    
    // Selector container
    itemSelector.css("height", height);
    // Tree
    selectorTree.css("height", height);
    // Flat content height
    uniFlatContent.css("height", height - uniFlatSearchPanel.outerHeight(true) - selectorFlatDescription.outerHeight(true) - uniFlatPager.outerHeight(true));
}


// Ensures selector objects
function getItems(forceLoad) {
    if (forceLoad || (itemSelector == null)) {
        itemSelector = $j("div .ItemSelector");
        selectorTree = $j("div .SelectorTree");
        uniFlatContent = $j("div .UniFlatContent");
        uniFlatSearchPanel = $j("div  .UniFlatSearchPanel");
        uniFlatPager = $j("div .UniFlatPager");
        selectorFlatDescription = $j("div .SelectorFlatDescription");
    }
}

//]]>
       