<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WebPartMenu.ascx.cs" Inherits="CMSModules_PortalEngine_Controls_WebParts_WebPartMenu" %>
<%@ Register TagPrefix="cms" Namespace="CMS.UIControls" Assembly="CMS.UIControls" %>
<cms:ContextMenu runat="server" ID="menuMoveTo" MenuID="moveToMenu" VerticalPosition="Bottom"
    HorizontalPosition="Left" OffsetX="25" ActiveItemCssClass="ItemSelected" MenuLevel="1"
    ShowMenuOnMouseOver="true" MouseButton="Both">
    <asp:Panel runat="server" ID="pnlZoneMenu" CssClass="PortalContextMenu WebPartContextMenu">
        <asp:Repeater runat="server" ID="repZones">
            <ItemTemplate>
                <asp:Panel runat="server" ID="pnlZone" CssClass="Item" onclick='<%# "ContextMoveWebPartToZone(GetContextMenuParameter(\"webPartMenu\"), \"" + ((CMSWebPartZone)Container.DataItem).ID + "\", \"" + ((CMSWebPartZone)Container.DataItem).PagePlaceholder.PageInfo.NodeAliasPath + "\");" %>'>
                    <asp:Panel runat="server" ID="pnlZonePadding" CssClass="ItemPadding">
                        <%--<asp:Image runat="server" ID="imgZone" CssClass="Icon" EnableViewState="false" />--%>
                        &nbsp;<asp:Label runat="server" ID="lblZone" CssClass="Name" EnableViewState="false"
                            Text='<%# (((CMSWebPartZone)Container.DataItem).ZoneTitle != "" ? HTMLHelper.HTMLEncode(((CMSWebPartZone)Container.DataItem).ZoneTitle) : ((CMSWebPartZone)Container.DataItem).ID) %>' />
                    </asp:Panel>
                </asp:Panel>
            </ItemTemplate>
        </asp:Repeater>
    </asp:Panel>
</cms:ContextMenu>
<cms:ContextMenu runat="server" ID="menuUp" MenuID="upMenu" VerticalPosition="Bottom"
    HorizontalPosition="Left" OffsetX="25" ActiveItemCssClass="ItemSelected" MouseButton="Right"
    MenuLevel="1" ShowMenuOnMouseOver="true">
    <asp:Panel runat="server" ID="pnlUpMenu" CssClass="PortalContextMenu WebPartContextMenu">
        <asp:Panel runat="server" ID="pnlTop" CssClass="Item">
            <asp:Panel runat="server" ID="pnlTopPadding" CssClass="ItemPadding">
                <%--<asp:Image runat="server" ID="imgAlphaAsc" CssClass="Icon" EnableViewState="false" />--%>
                &nbsp;<asp:Label runat="server" ID="lblTop" CssClass="Name" EnableViewState="false"
                    Text="Top" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</cms:ContextMenu>
<cms:ContextMenu runat="server" ID="menuDown" MenuID="downMenu" VerticalPosition="Bottom"
    HorizontalPosition="Left" OffsetX="25" ActiveItemCssClass="ItemSelected" MouseButton="Right"
    MenuLevel="1" ShowMenuOnMouseOver="true">
    <asp:Panel runat="server" ID="pnlDownMenu" CssClass="PortalContextMenu WebPartContextMenu">
        <asp:Panel runat="server" ID="pnlBottom" CssClass="Item">
            <asp:Panel runat="server" ID="pnlBottomPadding" CssClass="ItemPadding">
                <%--<asp:Image runat="server" ID="imgAlphaAsc" CssClass="Icon" EnableViewState="false" />--%>
                &nbsp;<asp:Label runat="server" ID="lblBottom" CssClass="Name" EnableViewState="false"
                    Text="Bottom" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</cms:ContextMenu>
<asp:Panel runat="server" ID="pnlWebPartMenu" CssClass="PortalContextMenu WebPartContextMenu">
    <cms:UIPlaceHolder ID="pnlUIProperties" runat="server" ModuleName="CMS.Content" ElementName="Design.WebPartProperties">
        <asp:Panel runat="server" ID="pnlProperties" CssClass="ItemLast">
            <asp:Panel runat="server" ID="pnlPropertiesPadding" CssClass="ItemPadding">
                <asp:Image runat="server" ID="imgProperties" CssClass="Icon" EnableViewState="false" />&nbsp;
                <asp:Label runat="server" ID="lblProperties" CssClass="Name" EnableViewState="false"
                    Text="Properties" />
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlSep1" CssClass="Separator">
            &nbsp;
        </asp:Panel>
    </cms:UIPlaceHolder>
    <cms:ContextMenuContainer runat="server" ID="cmcUp" MenuID="upMenu">
        <asp:Panel runat="server" ID="pnlUp" CssClass="Item">
            <asp:Panel runat="server" ID="pnlUpPadding" CssClass="ItemPadding">
                <asp:Image runat="server" ID="imgUp" CssClass="Icon" EnableViewState="false" />&nbsp;<asp:Label
                    runat="server" ID="lblUp" CssClass="Name" EnableViewState="false" Text="Up" />
            </asp:Panel>
        </asp:Panel>
    </cms:ContextMenuContainer>
    <cms:ContextMenuContainer runat="server" ID="cmcDown" MenuID="downMenu">
        <asp:Panel runat="server" ID="pnlDown" CssClass="Item">
            <asp:Panel runat="server" ID="pnlDownPadding" CssClass="ItemPadding">
                <asp:Image runat="server" ID="imgDown" CssClass="Icon" EnableViewState="false" />&nbsp;<asp:Label
                    runat="server" ID="lblDown" CssClass="Name" EnableViewState="false" Text="Down" />
            </asp:Panel>
        </asp:Panel>
    </cms:ContextMenuContainer>
    <cms:ContextMenuContainer runat="server" ID="cmcMoveTo" MenuID="moveToMenu">
        <asp:Panel runat="server" ID="pnlMoveTo" CssClass="Item">
            <asp:Panel runat="server" ID="pnlMoveToPadding" CssClass="ItemPadding">
                <asp:Image runat="server" ID="imgMoveTo" CssClass="Icon" EnableViewState="false" />&nbsp;<asp:Label
                    runat="server" ID="lblMoveTo" CssClass="NameInactive" EnableViewState="false"
                    Text="MoveTo" />
            </asp:Panel>
        </asp:Panel>
    </cms:ContextMenuContainer>
    <cms:UIPlaceHolder ID="pnlUIClone" runat="server" ModuleName="CMS.Content" ElementName="Design.AddWebParts">
        <asp:Panel runat="server" ID="pnlClone" CssClass="ItemLast">
            <asp:Panel runat="server" ID="pnlClonePadding" CssClass="ItemPadding">
                <asp:Image runat="server" ID="imgClone" CssClass="Icon" EnableViewState="false" />&nbsp;
                <asp:Label runat="server" ID="lblClone" CssClass="Name" EnableViewState="false" Text="Down" />
            </asp:Panel>
        </asp:Panel>
    </cms:UIPlaceHolder>
    <asp:Panel runat="server" ID="pnlSep3" CssClass="Separator">
        &nbsp;
    </asp:Panel>
    <cms:UIPlaceHolder ID="pnlUIDelete" runat="server" ModuleName="CMS.Content" ElementName="Design.RemoveWebParts">
        <asp:Panel runat="server" ID="pnlDelete" CssClass="ItemLast">
            <asp:Panel runat="server" ID="pnlDeletePadding" CssClass="ItemPadding">
                <asp:Image runat="server" ID="imgDelete" CssClass="Icon" EnableViewState="false" />&nbsp;
                <asp:Label runat="server" ID="lblDelete" CssClass="Name" EnableViewState="false"
                    Text="Delete" />
            </asp:Panel>
        </asp:Panel>
    </cms:UIPlaceHolder>
</asp:Panel>

<script type="text/javascript">
    //<![CDATA[
    function ContextConfigureWebPart(definition) {
        ConfigureWebPart(escape(definition[0]), escape(definition[1]), escape(definition[2]), escape(definition[3]));
    }

    function ContextMoveWebPartUp(definition) {
        MoveWebPartUp(definition[0], definition[1], definition[2], definition[3]);
    }

    function ContextMoveWebPartDown(definition) {
        MoveWebPartDown(definition[0], definition[1], definition[2], definition[3]);
    }

    function ContextRemoveWebPart(definition) {
        RemoveWebPart(definition[0], definition[1], definition[2], definition[3]);
    }

    function ContextMoveWebPartToZone(definition, targetZoneId, targetAliasPath) {
        MoveWebPart(definition[0], definition[1], definition[2], definition[3], targetZoneId, targetAliasPath);
    }

    function ContextCloneWebPart(definition) {
        CloneWebPart(definition[0], definition[1], definition[2], definition[3]);
    }

    function ContextMoveWebPartTop(definition) {
        MoveWebPart(definition[0], definition[1], definition[2], definition[3], definition[0], 0);
    }

    function ContextMoveWebPartBottom(definition) {
        MoveWebPart(definition[0], definition[1], definition[2], definition[3], definition[0], 1000);
    }
    //]]>
</script>

