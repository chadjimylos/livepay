using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SettingsProvider;
using CMS.DataEngine;
using CMS.SiteProvider;
using CMS.CMSHelper;

/// <summary>
/// Custom request events
/// </summary>
public static class CMSRequest
{
    /// <summary>
    /// Fires before the request start event
    /// </summary>
    public static void BeforeBeginRequest(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires after the request start event
    /// </summary>
    public static void AfterBeginRequest(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before the request end event
    /// </summary>
    public static void BeforeEndRequest(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires after the request end event
    /// </summary>
    public static void AfterEndRequest(object sender, EventArgs e)
    {
        // Add your custom actions
        
    }


    /// <summary>
    /// Fires before the acquire request state event
    /// </summary>
    public static void BeforeAcquireRequestState(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires after the acquire request state event
    /// </summary>
    public static void AfterAcquireRequestState(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before the request authorization event
    /// </summary>
    public static void BeforeAuthorizeRequest(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires after the request authorization event
    /// </summary>
    public static void AfterAuthorizeRequest(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires before the request authentication event
    /// </summary>
    public static void BeforeAuthenticateRequest(object sender, EventArgs e)
    {
        // Add your custom actions
    }


    /// <summary>
    /// Fires after the request authentication event
    /// </summary>
    public static void AfterAuthenticateRequest(object sender, EventArgs e)
    {
        // Add your custom actions
    }
}
