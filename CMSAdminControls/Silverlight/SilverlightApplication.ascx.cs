﻿using System.Text;

using CMS.CMSHelper;
using CMS.GlobalHelper;
using CMS.UIControls;

public partial class CMSAdminControls_Silverlight_SilverlightApplication : CMSUserControl
{
    #region "Variables"

    private string mAlternateContent = null;
    private string mApplicationPath = "";
    private string mMinimumVersion = "";
    private string mParameters = "";
    private string mContainerWidth = "";
    private string mContainerHeight = "";
    private string mContainerBackground = "#FFFFFF";

    #endregion


    #region "Public properties"

    /// <summary>
    /// HTML content which is displayed to user when Silverlight plugin is not installed.
    /// </summary>
    public string AlternateContent
    {
        get
        {
            // Try to load custom alternate content
            if (string.IsNullOrEmpty(this.mAlternateContent))
            {
                this.mAlternateContent = "var altHtml = \"<a href='{1}' style='text-decoration: none;'><img src='{2}' alt='{3}' style='border-style: none'/></a>\";" +
                                         "altHtml = altHtml.replace('{1}', 'http://go.microsoft.com/fwlink/?LinkID=124807');" +
                                         "altHtml = altHtml.replace('{2}', 'http://go.microsoft.com/fwlink/?LinkId=108181');" +
                                         "altHtml = altHtml.replace('{3}', 'Get Microsoft Silverlight');";
            }
            else
            {
                this.mAlternateContent = "var altHtml = \"" + mAlternateContent.Replace('\"', '\'') + "\";";
            }
            return this.mAlternateContent;
        }
        set
        {
            this.mAlternateContent = value;
        }
    }


    /// <summary>
    /// Silverlight application path
    /// </summary>
    public string ApplicationPath
    {
        get
        {
            return this.mApplicationPath;
        }
        set
        {
            this.mApplicationPath = value;
        }
    }


    /// <summary>
    /// Minimum version of the Microsoft Silverlight which is required by the current silverlight application
    /// </summary>
    public string MinimumVersion
    {
        get
        {
            return this.mMinimumVersion;
        }
        set
        {
            this.mMinimumVersion = value;
        }
    }


    /// <summary>
    /// Silverlight application parameters.
    /// </summary>
    public string Parameters
    {
        get
        {
            return this.mParameters;
        }
        set
        {
            this.mParameters = value;
        }
    }


    /// <summary>
    /// Silverlight application container width.
    /// </summary>
    public string ContainerWidth
    {
        get
        {
            return this.mContainerWidth;
        }
        set
        {
            this.mContainerWidth = value;
        }
    }


    /// <summary>
    /// Silverlight application container height.
    /// </summary>
    public string ContainerHeight
    {
        get
        {
            return this.mContainerHeight;
        }
        set
        {
            this.mContainerHeight = value;
        }
    }


    /// <summary>
    /// Silverlight application container background.
    /// </summary>
    public string ContainerBackground
    {
        get
        {
            return this.mContainerBackground;
        }
        set
        {
            this.mContainerBackground = value;
        }
    }

    #endregion


    protected override void OnPreRender(System.EventArgs e)
    {
        base.OnPreRender(e);

        if (!this.StopProcessing)
        {
            ReloadData();
        }
    }


    /// <summary>
    /// Initializes silverlight application container.
    /// </summary>
    private void ReloadData()
    {
        // Register silverlight support script
        ScriptHelper.RegisterSilverlight(Page);

        StringBuilder sb = new StringBuilder();
        sb.Append("<div id=\"Silverlight_" + this.ClientID + "\"></div>");

        // Create script initiating silverlight application
        StringBuilder silverlightApplication = new StringBuilder();
        silverlightApplication.Append("function createSilverlight( controlHost ){");
        silverlightApplication.Append("Silverlight.createObjectEx({");
        silverlightApplication.Append("source: " + ScriptHelper.GetString(UrlHelper.ResolveUrl(this.ApplicationPath)) + ",");
        silverlightApplication.Append("parentElement: controlHost,");
        silverlightApplication.Append("id: \"silverlightControl\",");
        silverlightApplication.Append("properties: {");
        silverlightApplication.Append("autoUpgrade: \"true\",");
        silverlightApplication.Append("width: \"" + this.ContainerWidth + "\",");
        silverlightApplication.Append("alt: altHtml,");
        silverlightApplication.Append("height: \"" + this.ContainerHeight + "\",");
        silverlightApplication.Append("version: \"" + this.MinimumVersion + "\",");
        silverlightApplication.Append("background: \"" + this.ContainerBackground + "\",");
        silverlightApplication.Append("initParams: \"" + this.Parameters + "\",");
        silverlightApplication.Append("isWindowless: \"true\",");
        silverlightApplication.Append("enableHtmlAccess: \"true\"},events: {}});}");
        silverlightApplication.Append("var hostElement = document.getElementById('Silverlight_" + this.ClientID + "');");
        silverlightApplication.Append("createSilverlight(hostElement);");

        sb.Append(ScriptHelper.GetScript(this.AlternateContent + silverlightApplication));

        this.ltlSilverlight.Text = sb.ToString();
    }
}
