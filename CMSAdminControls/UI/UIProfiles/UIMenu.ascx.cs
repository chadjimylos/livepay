﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.IO;

using CMS.GlobalHelper;
using CMS.UIControls;
using CMS.SiteProvider;
using CMS.SettingsProvider;
using CMS.CMSHelper;

public partial class CMSAdminControls_UI_UIProfiles_UIMenu : CMSUserControl
{
    #region "Variables"

    protected string mElementName = null;
    protected string mModuleName = null;
    protected string mJavaScriptHandler = null;
    private string mRootTargetURL = String.Empty;

    private bool mModuleAvailabilityForSiteRequired = false;

    private UIElementInfo root = null;
    private CurrentUserInfo currentUser = null;

    private int totalNodes = 0;

    #endregion


    #region "Properties"


    /// <summary>
    /// Gets the root node of the tree.
    /// </summary>
    public TreeNode RootNode
    {
        get
        {
            return this.treeElem.CustomRootNode;
        }
    }


    /// <summary>
    /// Code name of the UIElement.
    /// </summary>
    public string ElementName
    {
        get
        {
            return this.mElementName;
        }
        set
        {
            this.mElementName = value;
        }
    }


    /// <summary>
    /// Code name of the module.
    /// </summary>
    public string ModuleName
    {
        get
        {
            return this.mModuleName;
        }
        set
        {
            this.mModuleName = value;
        }
    }


    /// <summary>
    /// Name of the javascript function which is called when specified tab (UI element) is clicked. 
    /// UI element code name is passed as parameter.
    /// </summary>
    public string JavaScriptHandler
    {
        get
        {
            return this.mJavaScriptHandler;
        }
        set
        {
            this.mJavaScriptHandler = value;
        }
    }


    /// <summary>
    /// Gets the value which indicates whether there is some tab displayed or not.
    /// </summary>
    public bool MenuEmpty
    {
        get
        {
            if ((this.treeElem.ProviderObject != null) && (this.treeElem.ProviderObject.RootNode != null))
            {
                return this.treeElem.ProviderObject.RootNode.HasChildNodes;
            }
            return false;
        }
    }


    /// <summary>
    /// Root node target URL.
    /// </summary>
    public string RootTargetURL
    {
        get
        {
            return this.mRootTargetURL;
        }
        set
        {
            this.mRootTargetURL = value;
        }
    }


    /// <summary>
    /// Indicates if site availability of the corresponding module (module with name in format "cms.[ElementName]") is required for each UI element in the menu. Takes effect only when corresponding module exists.
    /// </summary>
    public bool ModuleAvailabilityForSiteRequired
    {
        get
        {
            return mModuleAvailabilityForSiteRequired;
        }
        set
        {
            mModuleAvailabilityForSiteRequired = value;
        }
    }

    #endregion


    #region "Custom events"

    /// <summary>
    /// Node created delegate.
    /// </summary>
    public delegate TreeNode NodeCreatedEventHandler(UIElementInfo uiElement, TreeNode defaultNode);


    /// <summary>
    /// Node created event handler.
    /// </summary>
    public event NodeCreatedEventHandler OnNodeCreated;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        // Register JQuery
        ScriptHelper.RegisterJQuery(this.Page);

        // Get the info
        if (String.IsNullOrEmpty(this.ElementName))
        {
            // Get the root UI element
            root = UIElementInfoProvider.GetRootUIElementInfo(this.ModuleName);
        }
        else
        {
            // Get the specified element
            root = UIElementInfoProvider.GetUIElementInfo(this.ModuleName, this.ElementName);
        }
        if (root != null)
        {
            // Create and set category provider
            UniTreeProvider provider = new UniTreeProvider();
            provider.ObjectType = "cms.uielement";
            provider.DisplayNameColumn = "ElementDisplayName";
            provider.IDColumn = "ElementID";
            provider.LevelColumn = "ElementLevel";
            provider.OrderColumn = "ElementOrder";
            provider.ParentIDColumn = "ElementParentID";
            provider.PathColumn = "ElementIDPath";
            provider.ValueColumn = "ElementID";
            provider.ChildCountColumn = "ElementChildCount";
            provider.WhereCondition = "(ElementCaption IS NOT NULL) AND NOT (ElementCaption = '') AND (ElementLevel < " + (root.ElementLevel + 2) + ")";
            provider.Columns = "ElementID, ElementName, ElementDisplayName, ElementLevel, ElementOrder, ElementParentID, ElementIDPath, ElementCaption, ElementIconPath, ElementTargetURL, ElementResourceID, 0 AS ElementChildCount";

            if (String.IsNullOrEmpty(this.JavaScriptHandler))
            {
                this.treeElem.SelectedNodeTemplate = "<span id=\"node_##NODECODENAME##\" name=\"treeNode\" class=\"ContentTreeItem ContentTreeSelectedItem\" onclick=\"SelectNode('##NODECODENAME##');\">##ICON##<span class=\"Name\">##NODECUSTOMNAME##</span></span>";
                this.treeElem.NodeTemplate = "<span id=\"node_##NODECODENAME##\" name=\"treeNode\" class=\"ContentTreeItem\" onclick=\"SelectNode('##NODECODENAME##');\">##ICON##<span class=\"Name\">##NODECUSTOMNAME##</span></span>";
            }
            else
            {
                this.treeElem.SelectedNodeTemplate = "<span id=\"node_##NODECODENAME##\" name=\"treeNode\" class=\"ContentTreeItem ContentTreeSelectedItem\" onclick=\"SelectNode('##NODECODENAME##'); if (" + this.JavaScriptHandler + ") { " + this.JavaScriptHandler + "(##NODEJAVA##, ##NODETARGETURL##); }\">##ICON##<span class=\"Name\">##NODECUSTOMNAME##</span></span>";
                this.treeElem.NodeTemplate = "<span id=\"node_##NODECODENAME##\" name=\"treeNode\" class=\"ContentTreeItem\" onclick=\"SelectNode('##NODECODENAME##'); if (" + this.JavaScriptHandler + ") { " + this.JavaScriptHandler + "(##NODEJAVA##, ##NODETARGETURL##); }\">##ICON##<span class=\"Name\">##NODECUSTOMNAME##</span></span>";
            }
            this.treeElem.UsePostBack = false;
            this.treeElem.ProviderObject = provider;
            this.treeElem.ExpandPath = root.ElementIDPath;

            this.treeElem.OnGetImage += new CMSAdminControls_UI_Trees_UniTree.GetImageEventHandler(treeElem_OnGetImage);
            this.treeElem.OnNodeCreated += new CMSAdminControls_UI_Trees_UniTree.NodeCreatedEventHandler(treeElem_OnNodeCreated);

            // Create root node
            string rootName = HTMLHelper.HTMLEncode(ResHelper.LocalizeString(String.IsNullOrEmpty(root.ElementCaption) ? root.ElementDisplayName : root.ElementCaption));
            string rootIcon = "<img src=\"" + UrlHelper.ResolveUrl(GetImageUrl("/General/DefaultRoot.png")) + "\" style=\"border:none;height:10px;width:1px;\" />";
            string rootText = this.treeElem.ReplaceMacros(this.treeElem.SelectedNodeTemplate, root.ElementID, root.ElementChildCount, rootName, rootIcon, root.ElementParentID, null, null);
            
            rootText = rootText.Replace("##NODETARGETURL##", ScriptHelper.GetString(this.RootTargetURL));
            rootText = rootText.Replace("##NODECUSTOMNAME##", rootName);
            rootText = rootText.Replace("##NODECODENAME##", root.ElementName);
            
            this.treeElem.SetRoot(rootText, root.ElementID.ToString(), ResolveUrl(root.ElementIconPath), Request.Url + "#", null);

            currentUser = CMSContext.CurrentUser;
        }

        // Reserve log item
        DataRow sdr = SecurityHelper.ReserveSecurityLogItem("LoadUIMenu");

        this.treeElem.ReloadData();

        // Log the security
        if (sdr != null)
        {
            SecurityHelper.SetLogItemData(sdr, currentUser.UserName, this.ModuleName, this.ElementName, totalNodes, CMSContext.CurrentSiteName);
        }
    }


    protected TreeNode treeElem_OnNodeCreated(DataRow itemData, TreeNode defaultNode)
    {
        if (itemData != null)
        {
            CurrentUserInfo currentUser = CMSContext.CurrentUser;
            if (currentUser != null)
            {
                // Check permissions
                string elemName = ValidationHelper.GetString(itemData["ElementName"], "");
                if (currentUser.IsAuthorizedPerUIElement(this.ModuleName, elemName, this.ModuleAvailabilityForSiteRequired))
                {
                    // Ensure element caption
                    string caption = ValidationHelper.GetString(itemData["ElementCaption"], "");
                    if (String.IsNullOrEmpty(caption))
                    {
                        caption = ValidationHelper.GetString(itemData["ElementDisplayName"], "");
                    }

                    // Set caption
                    defaultNode.Text = defaultNode.Text.Replace("##NODECUSTOMNAME##", ResHelper.LocalizeString(caption));
                    defaultNode.Text = defaultNode.Text.Replace("##NODECODENAME##", elemName);

                    // Set URL
                    string url = CMSContext.ResolveMacros(ValidationHelper.GetString(itemData["ElementTargetURL"], ""));
                    if (String.IsNullOrEmpty(url))
                    {
                        return null;
                    }
                    defaultNode.Text = defaultNode.Text.Replace("##NODETARGETURL##", ScriptHelper.GetString(UrlHelper.ResolveUrl(url)));

                    totalNodes++;

                    if (OnNodeCreated != null)
                    {
                        return OnNodeCreated(new UIElementInfo(itemData), defaultNode);
                    }
                    else
                    {
                        return defaultNode;
                    }
                }
            }
        }

        return null;
    }


    protected string treeElem_OnGetImage(UniTreeNode node)
    {
        DataRow dr = node.ItemData as DataRow;
        if (dr != null)
        {
            // Get icon path
            string imgUrl = ValidationHelper.GetString(dr["ElementIconPath"], "");
            if (!String.IsNullOrEmpty(imgUrl))
            {
                if (!ValidationHelper.IsURL(imgUrl))
                {
                    imgUrl = UIHelper.GetImageUrl(this.Page, imgUrl);

                    // Try to get default icon if requested icon not found
                    if (!File.Exists(Server.MapPath(imgUrl)))
                    {
                        imgUrl = GetImageUrl("CMSModules/list.png");
                    }
                }

                return imgUrl;
            }
        }

        return GetImageUrl("CMSModules/list.png");
    }
}
