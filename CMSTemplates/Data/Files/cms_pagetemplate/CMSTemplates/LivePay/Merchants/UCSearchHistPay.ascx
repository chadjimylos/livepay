﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="UCSearchHistPay.ascx.vb" Inherits="CMSTemplates_LivePay_Merchants_UCSearchHistPay" %>
<%@ Register Src="~/CMSTemplates/livepay/PopUp/UCTransDetails.ascx" TagName="TransDetails" TagPrefix="uc"  %>
<script src="../../CMSScripts/LivePay/RegisteredUsers.js" type="text/javascript"></script>
<script type="text/javascript">
    var TestControl = '<%=txtRefund.clientID %>'
   


     function ShowPleaseWhait() {
        $.blockUI({
            message: '<b>Αποθήκευση σε PDF..</b>',
            fadeIn: 700,
            fadeOut: 700,
           
            showOverlay: false,
            centerY: false,
            css: {
                width: '190px',
                top: '10px',
                left: '',
                right: '10px',
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .6,
                color: '#fff'
            }
        });
    }


    

function integer(str) {
    if (isNaN(str.value))
        str.value = '';
//    var re = new RegExp("\\d{7}", "g");
//    var m = re.exec(re);
//    if (m)
//        str.value = str;      
//    else
//        str.value = "";

}

function ShowTransDetails() {
    $.blockUI({ css: { top: '20%', border: '0px', backgroundColor: 'transparent', cursor: 'default' }, message: $('#TransDetailsPopUp') });
}

function ShowTransRefunt() {
    $.blockUI({ css: { top: '20%', border: '0px', backgroundColor: 'transparent', cursor: 'default' }, message: $('#TransRefundPopup') });
}


function CloseTheBatch() {
    return confirm('<%=ResHelper.LocalizeString("{$=Επιθυμείτε το κλείσιμο του πακέτου?|en-us=Επιθυμείτε το κλείσιμο του πακέτου?$}") %>')
}


function ExportFile(url) {
    document.getElementById('<%=exportFrame.ClientID %>').src = '/CMSTemplates/livepay/' + url;
    return false
}



function Hist_RebindRpt(CachValue) {
    setTimeout("Hist_ExecSearch('" + CachValue + "')", 400)
}

function Hist_ExecSearch(CachValue) {
    var RealValue = document.getElementById('<%=txtMerchant.ClientID %>').value
    if (RealValue == CachValue) {
        var text = document.getElementById('<%=txtMerchant.clientID %>');
        if (text.value != null && text.value.length > 0) {
            GetMerchantHistData(RealValue)
        } else {
            document.getElementById('<%=txtMerchantID.clientID %>').value = ''
            Hist_ShowHideQuickRes(false, 0)
        }
    }
}

function GetMerchantHistData(val) {
    var x;
    if (window.ActiveXObject) {
        x = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else {
        x = new XMLHttpRequest();
    }
    x.open("GET", "/CMSTemplates/LivePay/xmlHttpMerchants.aspx?MerchantKeyWord=" + val, true);
    x.onreadystatechange = function () {
        if (x.readyState == 4) {
            var Results = x.responseText;
            if (Results.length > 0) {
                document.getElementById('Hist_QuickSrcResAll').innerHTML = Results;
                Hist_ShowHideQuickRes(true, 0);
            } else {
                Hist_ShowHideQuickRes(false, 0);
            }

        }
    }
    x.send(null);
}

function Hist_ShowHideQuickRes(IsVisible, RowCount) {
    if (IsVisible) {
        document.getElementById('Hist_QuickResDiv').style.display = '';
    }
    else {
        document.getElementById('Hist_QuickResDiv').style.display = 'none';
    }
}

function SetSuggestValue(txt, Val) {
    document.getElementById('<%=txtMerchant.clientID %>').value = txt;
    document.getElementById('<%=txtMerchantID.clientID %>').value = Val;
    document.getElementById('Hist_QuickResDiv').style.display = 'none';
}

</script>


<style type="text/css">
div.growlUI { background: url(check48.png) no-repeat 10px 10px;width:200px }
div.growlUI h1, div.growlUI h2 {font-size:12px;color: white;text-align: left;width:200px}
.test
{
    display:none;
}
.PT20{padding-top:0px}
</style>


<asp:UpdatePanel ID="uptPanel" runat="server" UpdateMode="Always" >
<ContentTemplate>
<asp:TextBox ID="txtRefund" runat="server" style="display:none" ></asp:TextBox>
<div id="TransDetailsPopUp" style="display:none">
        <uc:TransDetails ID="TransDetails" runat="server" />
</div>
<div id="TransRefundPopup" style="display:none">
        <uc:TransDetails ID="TransRefunt" runat="server" />
</div>
<asp:Label ID="lblError" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
<div class="SrHistPayBG">
	<div class="SrHistPayTitle"><%= ResHelper.LocalizeString("{$=Ιστορικό Συναλλαγών|en-us=Ιστορικό Συναλλαγών$}")%></div>
	<div class="SrHistPaySrcTitle"><%= ResHelper.LocalizeString("{$=Αναζήτηση Συναλλαγών|en-us=Αναζήτηση Συναλλαγών$}")%></div>
	<div class="SrHistPaySrcDescr"><%=ResHelper.LocalizeString("{$=Στη σελίδα αυτή μπορείτε να δείτε όλες τις συναλλαγές που έχουν πραγματοποιηθεί στην επιχείρησή σας μέσω του Livepay.gr. Χρησιμοποιείστε τα παρακάτω κριτήρια αναζήτησης για να περιορίσετε τα αποτελέσματα|en-us=Στη σελίδα αυτή μπορείτε να δείτε όλες τις συναλλαγές που έχουν πραγματοποιηθεί στην επιχείρησή σας μέσω του Livepay.gr. Χρησιμοποιείστε τα παρακάτω κριτήρια αναζήτησης για να περιορίσετε τα αποτελέσματα$}") %></div>
    <div style="padding-top:8px;padding-top:expression(5)"><img src="/app_themes/LivePay/SrcBorderForm.png"</div>
	<div>
	   <div class="SrHistPaySrcDtmTitle"><%=ResHelper.LocalizeString("{$=Αναζήτηση σε|en-us=Αναζήτηση σε$}") %>:</div>
	   <div class="SrHistPaySrcDtmRBL">
	    <asp:RadioButtonList ID="rbtTrancType" runat="server" RepeatDirection="Horizontal" cssClass="SrHistPayrbl" >
	     <asp:ListItem Text="Συναλλαγές Ανοικτού Πακέτου" Value="1" Selected ></asp:ListItem>
         <asp:ListItem Text="Όλες οι συναλλαγές" Value="2" ></asp:ListItem>
	    </asp:RadioButtonList>
	   </div>
       <asp:PlaceHolder ID="phAdmin" runat="server" Visible="false">
        <div class="Clear"></div>
	    <div class="PT5">
		   <div class="SrHistPaySrcDtmTitle">MerchantID:</div>
	       <div class="MerchSrcTopDLLDiv">
                <asp:TextBox CssClass="SrcHisPayTxt" ID="txtMerchantID" runat="server" style="border:1px solid #3b6db4;width:102px;"/>
           </div>
        </div>
        <div class="Clear"></div>
	    <div class="PT5">
		   <div class="SrHistPaySrcDtmTitle">e-mail:</div>
	       <div class="MerchSrcTopDLLDiv">
                <asp:TextBox CssClass="SrcHisPayTxt" ID="txtEmail" runat="server" style="border:1px solid #3b6db4;width:102px;"/>
           </div>
        </div>
        <div class="Clear"></div>
        <div class="PT5">
                <div class="SrHistPaySrcDtmTitle"><%=ResHelper.LocalizeString("{$=Πληρωμή Προς|en-us=Πληρωμή Προς$}") %>:</div>
                <div class="MerchSrcTopDLLDiv"><asp:textbox  CssClass="SrcHisPayTxt" id="txtMerchant" autocomplete="off" onkeyup="Hist_RebindRpt(this.value);" runat="server" style="border:1px solid #3b6db4;width:178px" /></div>
                <div class="Clear"></div><asp:Button style="display:none" Text="hidbtn" ID="Hist_hidBtn" runat="server" /><asp:TextBox ID="HiddentxtMerchantID" runat="server" style="display:none"></asp:TextBox>
                <div class="HistQuickSrcResTopMainNewPay" id="Hist_QuickResDiv" style="display:none">
                    <div class="QuickSrcResMain">
                        <div class="QuickSrcResAll" id="Hist_QuickSrcResAll">
                        
                        </div>
                    </div>
                </div>
            </div>
       </asp:PlaceHolder>
	   <div class="Clear"></div>
	   <div class="PT5">
		   <div class="SrHistPaySrcDtmTitle"><%=ResHelper.LocalizeString("{$=Αποτελέσματα ανά σελίδα|en-us=Αποτελέσματα ανά σελίδα$}") %>:</div>
	       <div class="MerchSrcTopDLLDiv">
	            <asp:DropDownList runat="server" ID="ddlRecordsPerPage" CssClass="SrcHisPayTxt" >
                    <asp:ListItem Text="10" Value="10" ></asp:ListItem>
                    <asp:ListItem Text="20" Value="20" ></asp:ListItem>
                    <asp:ListItem Text="40" Value="40" ></asp:ListItem>
                    <asp:ListItem Text="100" Value="100" ></asp:ListItem>
                </asp:DropDownList>
	       </div>
	       <div class="Clear"></div>
	   </div>
       <div><img src="/app_themes/LivePay/SrcBorderForm.png"</div>
       <div id="DivFullSrc" class="SrHistPayBigForm" style="display:none">
            <div class="PB10PF10">
                <div class="SrHistPayTableTitleMerch"><%=ResHelper.LocalizeString("{$=Ημερομηνία Κλεισίματος Πακέτου|en-us=Ημερομηνία Κλεισίματος Πακέτου$}") %>:</div>
                <div class="SrHistPayTableFrom"><%=ResHelper.LocalizeString("{$=Από|en-us=Από$}") %>:</div>
                <div class="SrHistPayTableTxt"><asp:textbox CssClass="SrcHisPayTxt" id="txtFrom" runat="server" style="border:1px solid #3b6db4;width:82px;" /></div>
                <div class="SrHistPayTableToMerc"><%=ResHelper.LocalizeString("{$=Εως|en-us=Εως$}") %>:</div>
                <div class="SrHistPayTableTxt"><asp:textbox CssClass="SrcHisPayTxt" id="txtTo" runat="server" style="border:1px solid #3b6db4;width:82px" /></div>
                <div class="Clear"></div>
            </div>
            <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitleMerchRpt"><%= ResHelper.LocalizeString("{$=Κατάσταση Συναλλαγής|en-us=Κατάσταση Συναλλαγής$}")%>:</div>
                <div class="FLEFT"><asp:DropDownList ID="drpTxnResult" runat="server" ></asp:DropDownList></div>
                <div class="Clear"></div>
            </div>
            <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitleMerchRpt"><%=ResHelper.LocalizeString("{$=Τύπος Συναλλαγής|en-us=Τύπος Συναλλαγής$}") %>:</div>
                <div class="FLEFT"><asp:DropDownList ID="drpTxnType" runat="server" ></asp:DropDownList></div>
                <div class="Clear"></div>
            </div>

            <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
            <div class="PB10PF10">
                <div class="SrHistPayTableTitleMerchRpt"><%= ResHelper.LocalizeString("{$=Ονοματεπώνυμο Πελάτη|en-us=Ονοματεπώνυμο Πελάτη$}")%>:</div>
                <div class="FLEFT"><asp:textbox CssClass="SrcHisPayTxt" id="TxtFullName" MaxLength="100" runat="server" style="border:1px solid #3b6db4;width:242px" /></div>
                <div class="Clear"></div>
            </div>

           <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
           <div class="PB10PF10">
                <div class="SrHistPayTableTitleSec"><%=ResHelper.LocalizeString("{$=Ποσό|en-us=Ποσό$}") %>:</div>
                <div class="SrHistPayTableFrom">Από:</div>
                <div class="SrHistPayTableTxt">
                    <asp:textbox CssClass="SrcHisPayTxt" id="txtAmountFrom" MaxLength="8" onkeyup="FixMoney(this,false)" onblur="FixMoney(this,true)" runat="server" style="border:1px solid #3b6db4;width:82px" />
                    <asp:RegularExpressionValidator ID="revAmountFrom" runat="server" 
                         ControlToValidate="txtAmountFrom" ErrorMessage="*" 
                         ValidationExpression="^[0-9]*(\.)?[0-9]?[0-9]?$" />
                </div>
                <div class="SrHistPayTableToSec">Εως:</div>
                <div class="SrHistPayTableTxt">
                    <asp:textbox CssClass="SrcHisPayTxt" id="txtAmountTo" MaxLength="8" onkeyup="FixMoney(this,false)" onblur="FixMoney(this,true)" runat="server" style="border:1px solid #3b6db4;width:82px" />
                    <asp:RegularExpressionValidator ID="revAmountTo" runat="server" 
                         ControlToValidate="txtAmountTo" ErrorMessage="*" 
                         ValidationExpression="^[0-9]*(\.)?[0-9]?[0-9]?$" />
                </div>
                <div class="Clear"></div>
            </div>
           <div class="PB10"><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
           <div class="PB10PF10">
                <div class="SrHistPayTableTitleSec"><%=ResHelper.LocalizeString("{$=Αριθμός Πακέτου|en-us=Αριθμός Πακέτου$}") %>:</div>
                <div class="SrHistPayTableFrom">Από:</div>
                <div class="SrHistPayTableTxt">
                    <asp:textbox CssClass="SrcHisPayTxt" id="txtBatchFrom" onkeyup="integer(this)" MaxLength="15" runat="server" style="border:1px solid #3b6db4;width:82px" />
                    <asp:RegularExpressionValidator ID="revBatchFrom" runat="server" 
                         ControlToValidate="txtBatchFrom" ErrorMessage="*" 
                         ValidationExpression="^([0-9]{0,15})$" />
                </div>
                <div class="SrHistPayTableToSec">Εως:</div>
                <div class="SrHistPayTableTxt">
                    <asp:textbox CssClass="SrcHisPayTxt" id="txtBatchTo" onkeyup="integer(this)" MaxLength="15" runat="server" style="border:1px solid #3b6db4;width:82px" />
                    <asp:RegularExpressionValidator ID="revBatchTo" runat="server" 
                         ControlToValidate="txtBatchTo" ErrorMessage="*" 
                         ValidationExpression="^([0-9]{0,15})$" />
                </div>
                <div class="Clear"></div>
            </div>
            <div ><img src="/app_themes/LivePay/SrcBorderForm.png" /></div>
       </div>
       <div class="SrHistPayResTitle" ><a onclick="ShowHideSrcForm();" class="Chand"><img border="0" id="imgPointSrcType" src="~/app_themes/LivePay/PointerDown.gif"/> <span ID="spnSrcType"><%=ResHelper.LocalizeString("{$=Αναλυτική Αναζήτηση|en-us=Αναλυτική Αναζήτηση$}") %></span></a></div>
	   <div class="MerchSrcBtnSearch"><asp:ImageButton ImageUrl="/app_themes/LivePay/btnSearch.png" runat="server" ID="btnSearchPayments" /></div>
	   <div class="Clear"></div>

       
       <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
	</div>
 </div>

 <div  style="background-color:White">&nbsp;</div>

 <div class="SrHistPayBG ">
    <div class="SrHistPayTitle" id="OpenBatchDIVMainHeader" runat="server"><%=ResHelper.LocalizeString("{$=Συνοπτική Εικόνα Ανοικτού Πακέτου|en-us=Συνοπτική Εικόνα Ανοικτού Πακέτου$}") %></div>
    <div class="SrHistPayTitle" id="AllTransactionDIVMainHeader" runat="server" visible="false" ><%= ResHelper.LocalizeString("{$=Συνοπτική Εικόνα|en-us=Συνοπτική Εικόνα$}")%></div>

    <div style="padding:6px 10px 0px 10px;">
        <div class="SrchHisTable_MainTitle">
            <div class="SrchHisTable_Main_Title" style="width:116px;font-size:11px"><%= ResHelper.LocalizeString("{$=Πλήθος Συναλλαγών|en-us=Πλήθος Συναλλαγών$}")%> :</div>
            <div class="SrchHisTable_Main_Value" style="font-size:11px"><asp:Literal runat="server" ID="litTotalTxn" /></div>
            <div class="Clear"></div>
            <div class="SrchHisTable_Main_SecTitle" style="width:116px;font-size:11px"><%=ResHelper.LocalizeString("{$=Πλήθος Εγκεκριμένων Πωλήσεων|en-us=Πλήθος Εγκεκριμένων Πωλήσεων$}") %> :</div>
            <div class="SrchHisTable_Main_SecValue" style="font-size:11px"><asp:Literal runat="server" ID="litTotalSales" /></div>
            <div class="Clear"></div>
            <div class="SrchHisTable_Main_SecTitle" style="width:116px;font-size:11px"><%= ResHelper.LocalizeString("{$=Πλήθος Επιστροφών|en-us=Πλήθος Επιστροφών$}")%> :</div>
            <div class="SrchHisTable_Main_SecValue" style="font-size:11px"><asp:Literal runat="server" ID="litTotalRefunds" /></div>
            <div class="Clear"></div>
        </div>
        <div style="float:left;border-right:1px solid #f8f6f6;border-left:1px solid #e3e3e3;padding-left:10px;height:90px;line-height:18px;width:190px;">
            <div class="SrchHisTable_Main_Title" style="width:116px;font-size:11px"><%=ResHelper.LocalizeString("{$=Αξία Συναλλαγών|en-us=Αξία Συναλλαγών$}") %>(€) :</div>
            <div class="SrchHisTable_Main_Value" style="font-size:11px"><asp:Literal runat="server" ID="litValueTxn" /></div>
            <div class="Clear"></div>
            <div class="SrchHisTable_Main_SecTitle" style="width:116px;font-size:11px"><%= ResHelper.LocalizeString("{$=Αξία Εγκεκριμένων Πωλήσεων|en-us=Αξία Εγκεκριμένων Πωλήσεων$}")%>(€) :</div>
            <div class="SrchHisTable_Main_SecValue" style="font-size:11px"><asp:Literal runat="server" ID="litValueSales" /></div>
            <div class="Clear"></div>
            <div class="SrchHisTable_Main_SecTitle" style="width:116px;font-size:11px"><%= ResHelper.LocalizeString("{$=Αξία Επιστροφών|en-us=Αξία Επιστροφών$}")%>(€) :</div>
            <div class="SrchHisTable_Main_SecValue" style="font-size:11px"><asp:Literal runat="server" ID="litValueRefunds" /></div>
            <div class="Clear"></div>
        </div>
        <div class="SrchHisTable_LastVal">
            <div class="SrchHisTable_LastValTitle" style="font-size:11px"><%=ResHelper.LocalizeString("{$=Προγραμματισμένη ημερομηνία & ώρα κλεισίματος πακέτου|en-us=Προγραμματισμένη ημερομηνία & ώρα κλεισίματος πακέτου$}") %>:</div>
            <div class="SrchHisTable_LastValValue" style="font-size:11px"><%= ResHelper.LocalizeString("{$=Τρίτη 8 Ιουνίου 2010, 2:00|en-us=Τρίτη 8 Ιουνίου 2010, 2:00$}")%></div>
        </div>
         <div class="Clear"></div>
    </div>
    <div class="GridMainGrid">
            <div class="SrcHisPayGridBGTop650">
                <div class="SrcHisPayGridTopTitle" id="OpenBatchDIVHeader" runat="server" visible="false"><%=ResHelper.LocalizeString("{$=Συναλλαγές Ανοικτού Πακέτου|en-us=Συναλλαγές Ανοικτού Πακέτου$}") %></div>
                <div class="SrcHisPayGridTopTitle" id="AllTransactionDIVHeader" runat="server" visible="false"><%= ResHelper.LocalizeString("{$=Συναλλαγές|en-us=Συναλλαγές$}")%></div>
                <div class="SrcHisPayGridDiv">
                    <asp:GridView ID="GridViewPayments" EmptyDataTemplate-CssClass="SrcHisPayPagerEmpty" PagerStyle-CssClass="SrcHisPayPager650" GridLines="None" runat="server" PageSize="8" AutoGenerateColumns="false" AllowPaging="true" Width="650px">
                    <HeaderStyle BackColor="#ffffff" ForeColor="#58595b" Height="35px" Font-Size="10px"  HorizontalAlign="Center"/>
                    <AlternatingRowStyle Height="40px" BackColor="#ffffff" ForeColor="#58595b" Font-Size="10px" Font-Names="tahoma" />
                    <RowStyle Height="40px" BackColor="#f7f7f7" ForeColor="#58595b" Font-Size="10px" Font-Names="tahoma" HorizontalAlign="Center" />
                        <Columns >
                            <asp:TemplateField HeaderText="Αριθμός<br>Πακέτου" HeaderStyle-CssClass="MerchSrcHeaderLeft MerchSrcHeader" ItemStyle-CssClass="MerchSrcItemLeft"  >
                                <ItemTemplate>
                                    <asp:label ID="lblBoxNo" runat="server" Text='<%#Eval("BatchID") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ημ/νια<br>Κλεισιμάτος<br>Πακέτου" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  ItemStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                    <asp:label ID="lblCloseDtm" runat="server" Text='<%#Eval("BatchDate").toString().split(" ")(0) & "<br />" & Eval("BatchDate").toString().split(" ")(1) %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Κατάσταση<br>Πακέτου" HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  ItemStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                    
                                    <asp:label ID="lblBatchClosed" runat="server" Text='<%#Eval("BatchClosed") %>' style="display:none" ></asp:label>
                                    <asp:label ID="lblBoxStatus" runat="server" Text='<%#IIF(Eval("BatchClosed")=true,"Κλειστό","Ανοιχτό") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Κωδικός<br>Συναλλαγής"    HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple"  >
                                <ItemTemplate>
                                    <asp:label ID="lblTransCode" runat="server" Text='<%#Eval("TransactionID") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ημ/νια<br>Συναλλαγής"  HeaderStyle-CssClass="MerchSrcHeaderSimple "  ItemStyle-CssClass="MerchSrcItemSimple " >
                                <ItemTemplate>
                                    <asp:label ID="lblTransDtm" runat="server" Text='<%#Eval("transactiondDte").toString().split(" ")(0) & "<br />" & Eval("transactiondDte").toString().split(" ")(1) %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Ονομ/επώνυμο<br>Πελάτη" HeaderStyle-CssClass="MerchSrcHeaderSimple W60"  ItemStyle-CssClass="MerchSrcItemSimple W60" >
                                <ItemTemplate>
                                    <asp:label ID="lblCustName" runat="server" Text='<%#Eval("CustomerName") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Ποσό(€)" HeaderStyle-CssClass="MerchSrcHeaderSimple" ItemStyle-HorizontalAlign="Right"  ItemStyle-CssClass="MerchSrcItemSimple" >
                                <ItemTemplate>
                                    <asp:HiddenField ID="hidAmount" runat="server" value='<%#Eval("transactioAamount") %>' />
                                    <asp:Label ID="lblAmount" runat="server" />&nbsp;&nbsp;
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Τύπος<br>Συν/γής"   HeaderStyle-CssClass="MerchSrcHeaderSimple"  ItemStyle-CssClass="MerchSrcItemSimple" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:label ID="lblTransType" runat="server" Text='<%#Eval("transactionType") %>' ></asp:label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Κατάσταση<br>Συναλλαγής" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="SrcHisPayGridHeaderLast W100" ItemStyle-CssClass="SrcHisPayGridItemLast W100" >
                                <ItemTemplate>
                                    <div style="width:100px">
                                        <div style="float:left;padding:0px 0px 5px 3px;width:100px"><asp:label ID="lblTransStatus" runat="server" Text='<%#GetTransactionStatusDescr(Eval("TransactionStatus")) %>' ></asp:label></div>
                                        <div style="float:left;padding-left:3px;width:100px">
                                            <asp:LinkButton ID="openPopup" runat="server" OnClick="openPopup_Click" CommandArgument='<%#Eval("transactionId") %>'><img border="0" src="/app_themes/LivePay/InfoBtn.png" /></asp:LinkButton>
                                            <asp:LinkButton ID="openRefund" runat="server" OnClick="openRefund_Click" CommandArgument='<%#Eval("transactionId") %>'><img border="0" src="/app_themes/LivePay/Refund.png" /></asp:LinkButton>
                                            <asp:LinkButton ID="openCancel" runat="server" OnClick="openCancel_Click" CommandArgument='<%#Eval("transactionId") %>'><img border="0" src="/app_themes/LivePay/imgDelete.png" /></asp:LinkButton></div>
                                        <div class="Clear"></div>
                                   </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            
                        </Columns>
                        <EmptyDataTemplate >
                            <div class="SrcHisPayPagerEmpty650">
                                <div style="padding-top:5px;padding-left:10px"><%=ResHelper.LocalizeString("{$=Δεν βρέθηκαν συναλλαγές|en-us=NoRecords$}") %></div>
                            </div>
                        </EmptyDataTemplate>
                        <PagerTemplate >
                        <div class="SrcHisPayPagerSpace" >
                            <div>
                                <div class="GridPagerNoLinkDivLeft5"><asp:linkbutton ID="lnkPrev" CssClass="PagerText" onclick="ChangePageByLinkNumber_Before" runat="server"><%= ResHelper.LocalizeString("{$=Προηγούμενη|en-us=Προηγούμενη$}")%></asp:linkbutton>&nbsp;</div>
                                <div class="GridPagerNoLinkDivCenter5" id="lblPageIndex" runat="server">&nbsp;</div>
                                <div class="GridPagerNoLinkDivRight">&nbsp;<asp:linkbutton ID="lnkNext" CssClass="PagerText"  onclick="ChangePageByLinkNumber_Next" runat="server"><%=ResHelper.LocalizeString("{$=Επόμενη|en-us=Επόμενη$}") %></asp:linkbutton></div>
                                <div class="GridPagerCloseBox"><asp:ImageButton ID="imgCloseBatch" OnClick="CloseBatch" OnClientClick="return CloseTheBatch()" runat="server" ImageUrl="/app_themes/LivePay/SearchTransMerchants/btnCloseBox.png" /></div>
                                <div class="GridPagerPDF"> <img class="Chand" OnClick="ExportFile('ExportToPDF.aspx?ExportType=MerchantsSrcHisPay');" src="/app_themes/LivePay/PDFIcon.png" /></div>
                                <div class="GridPagerExcel"><img class="Chand" OnClick="ExportFile('ExprotToExcel.aspx?ExportType=MerchantsSrcHisPay');" src="/app_themes/LivePay/ExcelIcon.png" /></div></div>
                            </div>
                       </div>
                         </PagerTemplate>
                    </asp:GridView>
                </div>
                <asp:LinkButton ID="HiddenPageBtn" style="display:none" runat="server" Text="0"></asp:LinkButton>
                <asp:TextBox ID="HiddenPage"  style="display:none" runat="server" Text="1"></asp:TextBox>
                <asp:TextBox ID="HiddenSearchType"  style="display:none" runat="server" Text="0"></asp:TextBox>
                <div id="BottomDiv" style="display:none" runat="server" class="SrcHisPayGridBGBottom"></div> 
            </div>
            
       </div>
    <div><img src="/app_themes/LivePay/SrcHistPayBotContBG.png" /></div>
 </div> 
 </div>
 <asp:Label ID="lblDebug" runat="server" />
 <div style="width:500px;height:100px;border:1px solid black;display:none" id="FramDiv" runat="server" >
   <iframe id="exportFrame"  runat="server"></iframe>
 </div>
</ContentTemplate>
 </asp:UpdatePanel>

 <script language="javascript" type="text/javascript" >

     function GoToPage(no) {
         document.getElementById('<%=HiddenPage.ClientID %>').value = no;
         document.getElementById('<%=HiddenPageBtn.ClientID %>').innerHTML = no;
         var GetID = '<%=HiddenPageBtn.ClientID %>'
         GetID = GetID.replace(/[_]/gi, "$")
         __doPostBack(GetID, '')
     }

     
     function ShowHideSrcForm() {
         var GetForm = document.getElementById('DivFullSrc');
         var GetimgPoint = document.getElementById('imgPointSrcType');
         var GetSrcType = document.getElementById('spnSrcType');

         if (GetForm.style.display == '') {
             GetForm.style.display = 'none'
             document.getElementById('<%=HiddenSearchType.ClientID %>').value = '0';
             GetimgPoint.src = '/app_themes/LivePay/PointerDown.gif'
             GetSrcType.innerHTML = 'Αναλυτική Αναζήτηση'
         } else {
             GetForm.style.display = ''
             document.getElementById('<%=HiddenSearchType.ClientID %>').value = '1';
             GetimgPoint.src = '/app_themes/LivePay/PointerUp.png'
             GetSrcType.innerHTML = 'Απλή Αναζήτηση'

         }
         PosBottom();
     }

     
     
     CheckAddtributes()
     function CheckAddtributes() {
         var GetSrcType = document.getElementById('<%=HiddenSearchType.ClientID %>').value
         var GetForm = document.getElementById('DivFullSrc');
         if (GetSrcType == '1') {
             GetForm.style.display = ''
         }
     }

  
    
 </script>
