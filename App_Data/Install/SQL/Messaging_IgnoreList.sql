CREATE TABLE [Messaging_IgnoreList] (
		[IgnoreListUserID]            int NOT NULL,
		[IgnoreListIgnoredUserID]     int NOT NULL
)
ON [PRIMARY]
ALTER TABLE [Messaging_IgnoreList]
	ADD
	CONSTRAINT [PK_Messaging_IgnoreList]
	PRIMARY KEY
	([IgnoreListUserID], [IgnoreListIgnoredUserID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Messaging_IgnoreList]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Messaging_IgnoreList_IgnoreListIgnoredUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Messaging_IgnoreList]
			ADD CONSTRAINT [FK_Messaging_IgnoreList_IgnoreListIgnoredUserID_CMS_User]
			FOREIGN KEY ([IgnoreListIgnoredUserID]) REFERENCES [CMS_User] ([UserID])
END
IF OBJECT_ID(N'[Messaging_IgnoreList]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_User]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Messaging_IgnoreList_IgnoreListUserID_CMS_User]') IS NULL
BEGIN
		ALTER TABLE [Messaging_IgnoreList]
			ADD CONSTRAINT [FK_Messaging_IgnoreList_IgnoreListUserID_CMS_User]
			FOREIGN KEY ([IgnoreListUserID]) REFERENCES [CMS_User] ([UserID])
END
