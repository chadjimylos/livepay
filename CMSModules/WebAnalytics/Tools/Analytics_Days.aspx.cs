using System;
using System.Data;
using System.Globalization;

using CMS.GlobalHelper;
using CMS.WebAnalytics;
using CMS.CMSHelper;
using CMS.UIControls;
using CMS.SettingsProvider;
using CMS.LicenseProvider;

public partial class CMSModules_WebAnalytics_Tools_Analytics_Days : CMSWebAnalyticsPage
{
    #region "Private variables"

    bool reportingModuleIsLoaded = false;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        // Check if reporting module is available
        reportingModuleIsLoaded = ModuleEntry.IsModuleLoaded(ModuleEntry.REPORTING);

        // Get values from querystring
        string statCodeName = QueryHelper.GetString("statCodeName", String.Empty);
        string dataCodeName = QueryHelper.GetString("dataCodeName", String.Empty);
        string reportCodeName = QueryHelper.GetString("reportCodeName", String.Empty);
        string additionalCodeName = QueryHelper.GetString("additionalCodeName", String.Empty);
        int year = QueryHelper.GetInteger("year", DateTime.Today.Year);
        int month = QueryHelper.GetInteger("month", DateTime.Today.Month);

        if (!RequestHelper.IsPostBack())
        {
            GUIInit(year, month);
        }

        // Get selected bars
        string selectedBar = QueryHelper.GetString("hdndaysel", String.Empty);
        int barIndex = -1;
        int tableIndex = -1;
        bool barclicked = QueryHelper.GetInteger("barclicked", 0) == 1;
        if (!barclicked && selectedBar != String.Empty)
        {
            int i = selectedBar.LastIndexOf('_');
            if (i > 0)
            {
                tableIndex = ValidationHelper.GetInteger(selectedBar.Substring(i + 1, selectedBar.Length - i - 1), -1);
                selectedBar = selectedBar.Remove(i);
                i = selectedBar.LastIndexOf('_');
                if (i > 0)
                {
                    barIndex = ValidationHelper.GetInteger(selectedBar.Substring(i + 1, selectedBar.Length - i - 1), -1);
                }
            }
        }

        // Week and day graphs
        ucGraphDays.AddGraph(25, "WeekGraph");
        string weekClick = LoadWeekData(year, month, statCodeName, dataCodeName, reportCodeName, additionalCodeName, tableIndex, barIndex);
        ucGraphDays.AddGraph(40, "DayGraph");
        string dayClick = LoadDayData(year, month, statCodeName, dataCodeName, reportCodeName, additionalCodeName, tableIndex, barIndex);

        ucGraphDays.SelectBar(tableIndex, barIndex, true);

        ucGraphDays.RenderGraph();

        if (tableIndex == 0 && weekClick != String.Empty)
        {
            ltlScript.Text = ScriptHelper.GetScript(" ucGraphDays_ShowDesktopContent('" + weekClick + "'); ");
        }

        if (tableIndex == 1 && dayClick != String.Empty)
        {
            ltlScript.Text = ScriptHelper.GetScript(" ucGraphDays_ShowDesktopContent('" + dayClick + "'); ");
        }
    }


    /// <summary>
    /// Page title initialization.
    /// </summary>
    /// <param name="year">Current (selected) year.</param>
    /// <param name="month">Current (selected) month.</param>
    public void GUIInit(int year, int month)
    {
        string title = ResHelper.GetString(DateTimeHelper.GetMonthName(month, CMSContext.CurrentUser.PreferredUICultureCode) + " " + year);
        title = title[0].ToString().ToUpper() + title.Substring(1);

        PageTitle.TitleText = title;
        PageTitle.TitleImage = GetImageUrl("CMSModules/CMS_WebAnalytics/month.png");
        PageTitle.SetWindowTitle = false;
    }


    #region "Loading stats data"

    /// <summary>
    /// Loads days statistics and creates graph.
    /// </summary>
    /// <param name="year">Year</param>
    /// <param name="month">Month</param>
    /// <param name="statCodeName">Analytics code name (pageviews, pageviews.multilingual...)</param>
    /// <param name="dataCodeName">Data code name (pageviews...)</param>
    /// <param name="reportCodeName">Report code name list (pageviews.yearreport, pageviews.monthreport...)</param>
    /// <param name="additionalCodeName">Additional code name (for visitors stats)</param>
    protected string LoadDayData(int year, int month, string statCodeName, string dataCodeName, string reportCodeName, string additionalCodeName, int selectedTable, int selectedBar)
    {
        // Prepare variables
        string result = String.Empty;
        DateTime dt = new DateTime(year, month, 1);
        DataSet ds = null;
        DataRow[] dr = null;
        string tmp = null;

        // Load day results
        if (!String.IsNullOrEmpty(additionalCodeName))
        {
            ds = HitsInfoProvider.GetAllHitsInfoBetween(
                 CMSContext.CurrentSite.SiteID,
                 HitsIntervalEnum.Day, new string[] { dataCodeName, additionalCodeName }, dt, dt.AddMonths(1), "HitsStartTime, HitsEndTime, HitsCount");
        }
        else
        {
            ds = HitsInfoProvider.GetAllHitsInfoBetween(
                 CMSContext.CurrentSite.SiteID,
                 HitsIntervalEnum.Day, new string[] { dataCodeName }, dt, dt.AddMonths(1), "HitsStartTime, HitsEndTime, HitsCount");
        }

        // Go through every day
        for (int i = 0; i < DateTime.DaysInMonth(year, month); i++)
        {
            int sum = 0;
            if (ds != null)
            {
                // Select single day results
                dr = ds.Tables[0].Select("HitsStartTime <= #" + dt.ToString(new CultureInfo(AnalyticsHelper.AnalyticsCulture)) + "# AND #" + dt.ToString(new CultureInfo(AnalyticsHelper.AnalyticsCulture)) + "# < HitsEndTime");

                foreach (DataRow row in dr)
                {
                    sum += ValidationHelper.GetInteger(row["HitsCount"], 0);
                }
            }

            // Do not show reporting when reporting module is not present
            if (reportingModuleIsLoaded)
            {
                tmp = ResolveUrl("~/CMSModules/Reporting/Tools/Analytics_Report.aspx?statcodename=") + statCodeName + "&datacodename=" + dataCodeName + "&reportCodeName=" + reportCodeName + "&year=" + year + "&month=" + dt.Month + "&day=" + dt.Day;
            }
            else
            {
                tmp = ResolveUrl("~/CMSDesk/blank.htm");
            }

            ucGraphDays.AddGraphData(sum,
                dt.Day.ToString(), sum.ToString(),
                tmp,
                String.Empty,
                1);

            if (i == selectedBar)
            {
                result = tmp;
            }

            dt = dt.AddDays(1);
        }
        return result;
    }


    /// <summary>
    /// Loads days statistics and creates graph.
    /// </summary>
    /// <param name="year">Year</param>
    /// <param name="month">Month</param>
    /// <param name="statCodeName">Analytics code name (pageviews, pageviews.multilingual...)</param>
    /// <param name="dataCodeName">Data code name (pageviews...)</param>
    /// <param name="reportCodeName">Report code name list (pageviews.yearreport, pageviews.monthreport...)</param>
    /// <param name="additionalCodeName">Additional code name (for visitors stats)</param>
    protected string LoadWeekData(int year, int month, string statCodeName, string dataCodeName, string reportCodeName, string additionalCodeName, int selectedTable, int selectedBar)
    {
        // Prepare variables
        string result = String.Empty;
        int numOfWeeks = DateTimeHelper.NumberOfWeeks(
            new DateTime(year, month, 1),
            new DateTime(year, month, DateTime.DaysInMonth(year, month)),
            AnalyticsHelper.AnalyticsCulture);
        int sum;
        DataSet ds = null;
        DataRow[] dr = null;
        string tmp = null;

        // Set start date
        DateTime dt = new DateTime(year, month, 1);
        DateTime dtStart = DateTimeHelper.GetWeekStart(dt, AnalyticsHelper.AnalyticsCulture);
        DateTime dtEnd = dtStart.AddDays(numOfWeeks * 7);
        TimeSpan span = dt.Subtract(dtStart);
        int increment = 7 - span.Days;

        // Get DS for complete month starting with week start date
        if (!String.IsNullOrEmpty(additionalCodeName))
        {
            ds = HitsInfoProvider.GetAllHitsInfoBetween(CMSContext.CurrentSite.SiteID, HitsIntervalEnum.Week,
                       new string[] { dataCodeName, additionalCodeName }, dtStart, dtEnd, "HitsStartTime, HitsEndTime, HitsCount");
        }
        else
        {
            ds = HitsInfoProvider.GetAllHitsInfoBetween(CMSContext.CurrentSite.SiteID, HitsIntervalEnum.Week,
                    new string[] { dataCodeName }, dtStart, dtEnd, "HitsStartTime, HitsEndTime, HitsCount");
        }

        // Loop by weeks
        for (int i = 0; i < numOfWeeks; i++)
        {
            sum = 0;
            if (ds != null)
            {
                // Select week
                dr = ds.Tables[0].Select("HitsStartTime <= #" + dt.ToString(new CultureInfo(AnalyticsHelper.AnalyticsCulture)) + "# AND #" + dt.ToString(new CultureInfo(AnalyticsHelper.AnalyticsCulture)) + "# < HitsEndTime");
                foreach (DataRow row in dr)
                {
                    sum += ValidationHelper.GetInteger(row["HitsCount"], 0);
                }
            }

            // Do not show reporting when reporting module is not present
            if (reportingModuleIsLoaded)
            {
                tmp = ResolveUrl("~/CMSModules/Reporting/Tools/Analytics_Report.aspx?statcodename=") + statCodeName + "&datacodename=" + dataCodeName + "&reportCodeName=" + reportCodeName + "&year=" + year + "&month=" + dt.Month + "&week=" + DateTimeHelper.GetWeekOfYear(dt, AnalyticsHelper.AnalyticsCulture) + "&day=" + dt.Day;
            }
            else
            {
                tmp = ResolveUrl("~/CMSDesk/blank.htm");
            }

            // Setup graph
            ucGraphDays.AddGraphData(sum,
                DateTimeHelper.GetWeekOfYear(dt, AnalyticsHelper.AnalyticsCulture).ToString(),
                sum.ToString(),
                tmp,
                String.Empty,
                increment);

            if (i == selectedBar)
            {
                result = tmp;
            }

            // Increase date by 1 week
            dt = dt.AddDays(increment);
            increment = 7;
        }
        return result;
    }

    #endregion
}
