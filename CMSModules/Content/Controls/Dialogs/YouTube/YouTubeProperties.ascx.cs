using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.UIControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls;

public partial class CMSModules_Content_Controls_Dialogs_YouTube_YouTubeProperties : ItemProperties
{
    #region "Private properties"

    /// <summary>
    /// Returns the default width of the YouTube video.
    /// </summary>
    private int DefaultWidth
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["DefaultHeight"], 0);
        }
        set
        {
            ViewState["DefaultHeight"] = value;
        }
    }


    /// <summary>
    /// Returns the default height of the YouTube video.
    /// </summary>
    private int DefaultHeight
    {
        get
        {
            return ValidationHelper.GetInteger(ViewState["DefaultWidth"], 0);
        }
        set
        {
            ViewState["DefaultWidth"] = value;
        }
    }

    #endregion


    #region "Page events"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.StopProcessing)
        {
            // Refresh button
            this.imgRefresh.ImageUrl = GetImageUrl("Design/Controls/Dialogs/refresh.png");
            this.imgRefresh.ToolTip = ResHelper.GetString("dialogs.web.refresh");

            // Color pickers
            this.colorElem1.SupportFolder = "~/CMSAdminControls/ColorPicker";
            this.colorElem2.SupportFolder = "~/CMSAdminControls/ColorPicker";

            // YouTube default colors control
            this.youTubeColors.OnSelectedItemClick = ControlsHelper.GetPostBackEventReference(this.btnDefaultColorsHidden, "");
            this.youTubeColors.LoadColors(new string[]{"#666666", "#EFEFEF", "#3A3A3A", "#999999", 
                "#2B405B", "#6B84B6", "#006699", "#54ABD6", "#234900", "#4E9E00", "#E1600F", 
                "#FEBD01", "#CC2550", "#E87A9F", "#402061", "#9461CA", "#5D1719", "#CD311B"});

            // YouTube default sizes control
            this.youTubeSizes.OnSelectedItemClick = ControlsHelper.GetPostBackEventReference(this.btnDefaultSizesHidden, "");
            if (this.chkShowBorder.Checked)
            {
                this.youTubeSizes.LoadSizes(new int[] { 445, 284, 500, 315, 580, 360, 660, 405 });
            }
            else
            {
                this.youTubeSizes.LoadSizes(new int[] { 425, 264, 480, 295, 560, 340, 640, 385 });
            }
            this.chkShowBorder.CheckedChanged += new EventHandler(chkShowBorder_CheckedChanged);
            this.btnDefaultColorsHidden.Click += new EventHandler(btnDefaultColorsHidden_Click);
            this.btnHiddenPreview.Click += new EventHandler(btnHiddenPreview_Click);
            this.btnHiddenInsert.Click += new EventHandler(btnHiddenInsert_Click);
            this.btnHiddenSizeRefresh.Click += new EventHandler(btnHiddenSizeRefresh_Click);
            this.btnDefaultSizesHidden.Click += new EventHandler(btnDefaultSizesHidden_Click);
            this.colorElem1.ColorChanged += new EventHandler(colorElem1_ColorChanged);
            this.colorElem2.ColorChanged += new EventHandler(colorElem2_ColorChanged);

            this.sizeElem.CustomRefreshCode = ControlsHelper.GetPostBackEventReference(this.btnHiddenSizeRefresh, "") + ";return false;";

            ScriptHelper.RegisterJQuery(this.Page);
            CMSDialogHelper.RegisterDialogHelper(this.Page);
            this.ltlScript.Text = ScriptHelper.GetScript("function insertItem(){" + Page.ClientScript.GetPostBackEventReference(this.btnHiddenInsert, "") + "}");

            ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "loading", ScriptHelper.GetScript("Loading('" + ResHelper.GetString("dialogs.youtube.preview").Replace("\'", "\\\'") + "','" + ResHelper.GetString("dialogs.youtube.previewloading").Replace("\'", "\\\'") + "');"));

            SetupOnChange();

            if (!RequestHelper.IsPostBack())
            {
                this.sizeElem.Locked = true;
                this.sizeElem.Width = this.DefaultWidth = 425;
                this.sizeElem.Height = this.DefaultHeight = 264;
                this.colorElem1.SelectedColor = "#666666";
                this.colorElem2.SelectedColor = "#EFEFEF";

                Hashtable dialogParameters = SessionHelper.GetValue("DialogParameters") as Hashtable;
                if ((dialogParameters != null) && (dialogParameters.Count > 0))
                {
                    LoadItemProperties(dialogParameters);
                    SessionHelper.SetValue("DialogParameters", null);
                }
            }
        }
    }


    protected void btnHiddenSizeRefresh_Click(object sender, EventArgs e)
    {
        this.sizeElem.Width = this.DefaultWidth;
        this.sizeElem.Height = this.DefaultHeight;

        LoadPreview();
    }


    protected void btnDefaultSizesHidden_Click(object sender, EventArgs e)
    {
        this.sizeElem.Width = this.youTubeSizes.SelectedWidth;
        this.sizeElem.Height = this.youTubeSizes.SelectedHeight;

        this.DefaultWidth = this.youTubeSizes.SelectedWidth;
        this.DefaultHeight = this.youTubeSizes.SelectedHeight;

        LoadPreview();
    }


    protected void chkShowBorder_CheckedChanged(object sender, EventArgs e)
    {
        if (this.chkShowBorder.Checked)
        {
            this.sizeElem.Width += 20;
            this.sizeElem.Height += 20;
        }
        else
        {
            this.sizeElem.Width -= 20;
            this.sizeElem.Height -= 20;
        }
        LoadPreview();
    }


    protected void btnHiddenPreview_Click(object sender, EventArgs e)
    {
        LoadPreview();
    }


    protected void colorElem1_ColorChanged(object sender, EventArgs e)
    {
        this.colorElem1.SelectedColor = this.colorElem1.ColorTextBox.Text.Trim();
        LoadPreview();
    }


    protected void colorElem2_ColorChanged(object sender, EventArgs e)
    {
        this.colorElem2.SelectedColor = this.colorElem2.ColorTextBox.Text.Trim();
        LoadPreview();
    }


    protected void btnHiddenInsert_Click(object sender, EventArgs e)
    {
        if (Validate())
        {
            Hashtable ytProperties = GetItemProperties();

            ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "insertYouTube", ScriptHelper.GetScript(CMSDialogHelper.GetYouTubeItem(ytProperties)));
        }
    }


    protected void btnDefaultColorsHidden_Click(object sender, EventArgs e)
    {
        this.colorElem1.SelectedColor = this.youTubeColors.SelectedColor1;
        this.colorElem2.SelectedColor = this.youTubeColors.SelectedColor2;
        LoadPreview();
    }

    protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
    {
        LoadPreview();
    }

    #endregion


    #region "Private methods"

    /// <summary>
    /// Loads the preview with proper data.
    /// </summary>
    private void LoadPreview()
    {
        if (!String.IsNullOrEmpty(this.txtLinkText.Text.Trim()))
        {
            this.previewElem.AutoPlay = false; // Always ignore autoplay in preview
            this.previewElem.PlayInHD = this.chkPlayInHD.Checked;
            this.previewElem.Border = this.chkShowBorder.Checked;
            this.previewElem.Color1 = this.colorElem1.SelectedColor;
            this.previewElem.Color2 = this.colorElem2.SelectedColor;
            this.previewElem.Cookies = this.chkEnableDelayed.Checked;
            this.previewElem.Fs = this.chkFullScreen.Checked;
            this.previewElem.Height = this.sizeElem.Height;
            this.previewElem.Width = this.sizeElem.Width;
            this.previewElem.Loop = this.chkLoop.Checked;
            this.previewElem.Rel = this.chkIncludeRelated.Checked;
            this.previewElem.Url = this.txtLinkText.Text.Trim();
        }
    }

    private void SetupOnChange()
    {
        string postBackRef = "setTimeout(" + ScriptHelper.GetString(ControlsHelper.GetPostBackEventReference(this.btnHiddenPreview, "")) + ",100);";

        this.txtLinkText.Attributes["onchange"] = postBackRef;
        this.sizeElem.HeightTextBox.Attributes["onchange"] = postBackRef;
        this.sizeElem.WidthTextBox.Attributes["onchange"] = postBackRef;
        this.chkPlayInHD.InputAttributes["onclick"] = postBackRef;
        this.chkAutoplay.InputAttributes["onclick"] = postBackRef;
        this.chkEnableDelayed.InputAttributes["onclick"] = postBackRef;
        this.chkFullScreen.InputAttributes["onclick"] = postBackRef;
        this.chkIncludeRelated.InputAttributes["onclick"] = postBackRef;
        this.chkLoop.InputAttributes["onclick"] = postBackRef;
    }

    #endregion


    #region "Overriden methods"

    /// <summary>
    /// Loads the properites into control
    /// </summary>
    /// <param name="properties"></param>
    public override void LoadItemProperties(Hashtable properties)
    {
        if (properties != null)
        {

            bool playInHd = ValidationHelper.GetBoolean(properties[DialogParameters.YOUTUBE_PLAYINHD], false);
            bool autoplay = ValidationHelper.GetBoolean(properties[DialogParameters.YOUTUBE_AUTOPLAY], false);
            bool border = ValidationHelper.GetBoolean(properties[DialogParameters.YOUTUBE_BORDER], false);
            bool cookies = ValidationHelper.GetBoolean(properties[DialogParameters.YOUTUBE_COOKIES], false);
            bool fullScreen = ValidationHelper.GetBoolean(properties[DialogParameters.YOUTUBE_FS], false);
            bool loop = ValidationHelper.GetBoolean(properties[DialogParameters.YOUTUBE_LOOP], false);
            bool relatedVideos = ValidationHelper.GetBoolean(properties[DialogParameters.YOUTUBE_REL], false);
            string url = ValidationHelper.GetString(properties[DialogParameters.YOUTUBE_URL], "");
            int width = ValidationHelper.GetInteger(properties[DialogParameters.YOUTUBE_WIDTH], 425);
            int height = ValidationHelper.GetInteger(properties[DialogParameters.YOUTUBE_HEIGHT], 264);
            string color1 = ValidationHelper.GetString(properties[DialogParameters.YOUTUBE_COLOR1], "#666666");
            string color2 = ValidationHelper.GetString(properties[DialogParameters.YOUTUBE_COLOR2], "#EFEFEF");
            if (String.IsNullOrEmpty(color1))
            {
                color1 = "#666666";
            }
            if (String.IsNullOrEmpty(color2))
            {
                color2 = "#EFEFEF";
            }

            this.DefaultWidth = width;
            this.DefaultHeight = height;

            this.chkPlayInHD.Checked = playInHd;
            this.chkAutoplay.Checked = autoplay;
            this.chkEnableDelayed.Checked = cookies;
            this.chkFullScreen.Checked = fullScreen;
            this.chkIncludeRelated.Checked = relatedVideos;
            this.chkLoop.Checked = loop;
            this.chkShowBorder.Checked = border;
            this.txtLinkText.Text = url;
            this.colorElem1.SelectedColor = color1;
            this.colorElem2.SelectedColor = color2;
            this.sizeElem.Width = width;
            this.sizeElem.Height = height;

            LoadPreview();
        }
    }


    /// <summary>
    /// Returns all parameters of the selected item as name – value collection.
    /// </summary>
    public override Hashtable GetItemProperties()
    {
        Hashtable retval = new Hashtable();

        retval[DialogParameters.YOUTUBE_PLAYINHD] = this.chkPlayInHD.Checked;
        retval[DialogParameters.YOUTUBE_AUTOPLAY] = this.chkAutoplay.Checked;
        retval[DialogParameters.YOUTUBE_BORDER] = this.chkShowBorder.Checked;
        retval[DialogParameters.YOUTUBE_COLOR1] = this.colorElem1.SelectedColor;
        retval[DialogParameters.YOUTUBE_COLOR2] = this.colorElem2.SelectedColor;
        retval[DialogParameters.YOUTUBE_COOKIES] = this.chkEnableDelayed.Checked;
        retval[DialogParameters.YOUTUBE_FS] = this.chkFullScreen.Checked;
        retval[DialogParameters.YOUTUBE_HEIGHT] = sizeElem.Height;
        retval[DialogParameters.YOUTUBE_LOOP] = this.chkLoop.Checked;
        retval[DialogParameters.YOUTUBE_REL] = this.chkIncludeRelated.Checked;
        retval[DialogParameters.YOUTUBE_URL] = this.txtLinkText.Text.Trim();
        retval[DialogParameters.YOUTUBE_WIDTH] = sizeElem.Width;

        return retval;
    }


    /// <summary>
    /// Clears the properties form.
    /// </summary>
    public override void ClearProperties(bool hideProperties)
    {
        this.sizeElem.Height = 425;
        this.sizeElem.Width = 264;

        this.chkPlayInHD.Checked = false;
        this.chkAutoplay.Checked = false;
        this.chkShowBorder.Checked = false;
        this.chkLoop.Checked = false;
        this.chkIncludeRelated.Checked = true;
        this.chkEnableDelayed.Checked = false;
        this.chkFullScreen.Checked = true;

        this.colorElem1.SelectedColor = "#666666";
        this.colorElem2.SelectedColor = "#EFEFEF";

        this.previewElem.Url = "";
    }


    /// <summary>
    /// Validates all the user input.
    /// </summary>
    public override bool Validate()
    {
        string errorMessage = "";

        if (!this.sizeElem.Validate())
        {
            errorMessage += " " + ResHelper.GetString("dialogs.youtube.invalidsize");
        }
        if ((this.colorElem1.ColorTextBox.Text.Trim() != "") && !ValidationHelper.IsColor(this.colorElem1.ColorTextBox.Text.Trim()))
        {
            errorMessage += " " + ResHelper.GetString("dialogs.youtube.invalidcolor1");
        }
        if ((this.colorElem2.ColorTextBox.Text.Trim() != "") && !ValidationHelper.IsColor(this.colorElem2.ColorTextBox.Text.Trim()))
        {
            errorMessage += " " + ResHelper.GetString("dialogs.youtube.invalidcolor2");
        }

        errorMessage = errorMessage.Trim();
        if (errorMessage != "")
        {
            ScriptHelper.RegisterStartupScript(this.Page, typeof(Page), "YouTubePropertiesError", ScriptHelper.GetAlertScript(errorMessage));
            return false;
        }
        return true;
    }

    #endregion
}
