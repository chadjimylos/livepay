using System;
using System.Data;
using System.Web.UI.WebControls;

using CMS.TreeEngine;
using CMS.GlobalHelper;
using CMS.CMSHelper;
using CMS.SiteProvider;
using CMS.LicenseProvider;
using CMS.EventLog;
using CMS.WorkflowEngine;
using CMS.UIControls;
using CMS.SettingsProvider;

using TreeNode = CMS.TreeEngine.TreeNode;

public partial class CMSModules_Content_CMSDesk_Properties_Security : CMSPropertiesPage
{
    #region "Variables"

    protected TreeNode node = null;
    protected int nodeId = 0;
    protected bool inheritsPermissions = false;
    protected AclProvider aclProv = null;
    protected DataSet dsAclItems = null;

    protected TreeProvider mTree = null;
    private EventLogProvider mEventLog = null;
    private int? mFilterLimit = null;
    private bool hasModifyPermission = false;

    #endregion


    #region "Properties"

    /// <summary>
    /// Minimal count of entries for display filter.
    /// </summary>
    public int FilterLimit
    {
        get
        {
            if (mFilterLimit == null)
            {
                mFilterLimit = ValidationHelper.GetInteger(SettingsHelper.AppSettings["CMSDefaultListingFilterLimit"], 25);
            }
            return (int)mFilterLimit;
        }
        set
        {
            mFilterLimit = value;
        }
    }


    /// <summary>
    /// Tree provider
    /// </summary>
    public TreeProvider Tree
    {
        get
        {
            if (mTree == null)
            {
                mTree = new TreeProvider(CMSContext.CurrentUser);
            }
            return mTree;
        }
    }


    /// <summary>
    /// Document name
    /// </summary>
    protected string DocumentName
    {
        get
        {
            if (node != null)
            {
                if (string.IsNullOrEmpty(node.DocumentName))
                {
                    return "/";
                }
                else
                {
                    return node.DocumentName;
                }
            }
            return "";
        }
    }


    /// <summary>
    /// Event log provider
    /// </summary>
    public EventLogProvider EventLog
    {
        get
        {
            if (mEventLog == null)
            {
                mEventLog = new EventLogProvider(Tree.Connection);
            }
            return mEventLog;
        }
    }

    #endregion


    #region "Page methods"

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);

        addUsers.StopProcessing = pnlUIPermissionsPart.IsHidden;
        addRoles.StopProcessing = pnlUIPermissionsPart.IsHidden;

        pnlAccessPart.Visible = !(pnlUIAuth.IsHidden && pnlUISsl.IsHidden);

        if (!CMSContext.CurrentUser.IsAuthorizedPerUIElement("CMS.Content", "Properties.Security"))
        {
            RedirectToCMSDeskUIElementAccessDenied("CMS.Content", "Properties.Security");
        }

        // Redirect to information page when no UI elements displayed
        if (pnlUIAuth.IsHidden && pnlUISsl.IsHidden && pnlUIPermissionsPart.IsHidden)
        {
            RedirectToUINotAvailable();
        }
    }


    protected void Page_Load(Object sender, EventArgs e)
    {
        // Get node
        nodeId = QueryHelper.GetInteger("nodeid", 0);
        node = Tree.SelectSingleNode(nodeId, CMSContext.PreferredCultureCode, Tree.CombineWithDefaultCulture);

        // Gets the node
        if (node != null)
        {
            UIContext.PropertyTab = PropertyTabEnum.Security;

            // Check read permissions
            if (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.Read) != AuthorizationResultEnum.Allowed)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoreaddocument"), node.NodeAliasPath));
            }

            // Check modify permissions
            hasModifyPermission = CanModifyPermission(false);
            if (!hasModifyPermission)
            {
                pnlAccessRights.Enabled = false;
                btnRemoveOperator.Enabled = false;
                btnOk.Enabled = false;
                pnlAccessPart.Enabled = false;
                btnRadOk.Enabled = false;
                pnlInheritance.Enabled = false;
                lblPermission.Visible = true;
                lblPermission.Text = String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath);
            }

            lnkInheritance.Visible = hasModifyPermission;

            // Check licence
            if (DataHelper.GetNotEmpty(UrlHelper.GetCurrentDomain(), "") != "")
            {
                if (!LicenseKeyInfoProvider.IsFeatureAvailable(UrlHelper.GetCurrentDomain(), FeatureEnum.DocumentLevelPermissions))
                {
                    if (UIHelper.IsUnavailableUIHidden())
                    {
                        pnlPermissionsPart.Visible = false;
                    }
                    else
                    {
                        pnlPermissions.Visible = false;
                        lblLicenseInfo.Visible = true;
                        lblLicenseInfo.Text = ResHelper.GetString("Security.NotAvailableInThisEdition");
                    }
                }
            }

            // Initialize controls
            SetupControls();

            // Register scripts
            LoadJavascript();
            ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

            aclProv = new AclProvider(Tree);

            // Set up roles control
            addRoles.NodeID = nodeId;
            addRoles.Changed += Selection_Changed;
            addRoles.CurrentSelector.IsLiveSite = false;
            addRoles.ShowSiteFilter = false;

            // Set up roles control
            addUsers.Node = node;
            addUsers.Changed += Selection_Changed;
            addUsers.IsLiveSite = false;
            addUsers.ShowSiteFilter = false;

            btnFilter.Click += btnFilter_Click;

            // Check if document inherits permissions and display info
            inheritsPermissions = aclProv.DoesNodeInheritPermissions(nodeId);
            lblInheritanceInfo.Text = inheritsPermissions ? ResHelper.GetString("Security.InheritsInfo.Inherits") : ResHelper.GetString("Security.InheritsInfo.DoesNotInherit");

            if (!RequestHelper.IsPostBack())
            {
                // Loads operators
                LoadOperators(true);

                // Set secured radio buttons
                switch (node.IsSecuredNode)
                {
                    case 0: radNo.Checked = true; break;
                    case 1: radYes.Checked = true; break;
                    default:
                        if (node.NodeParentID == 0)
                        {
                            radNo.Checked = true;
                        }
                        else
                        {
                            radParent.Checked = true;
                        }
                        break;
                }

                // Set secured radio buttons
                switch (node.RequiresSSL)
                {
                    case 0: radNoSSL.Checked = true; break;
                    case 1: radYesSSL.Checked = true; break;
                    case 2: radNeverSSL.Checked = true; break;
                    default:
                        if (node.NodeParentID == 0)
                        {
                            radNoSSL.Checked = true;
                        }
                        else
                        {
                            radParentSSL.Checked = true;
                        }
                        break;
                }
            }
            else
            {
                // Load values to uniselectors
                LoadOperators(false);
            }

            // Hide link to the inheritance settings if this is the root node
            if (node.NodeParentID == 0)
            {
                plcAuthParent.Visible = false;
                plcSSLParent.Visible = false;
                lnkInheritance.Visible = false;
            }

            // If there is no item in listbox, disables checkboxes
            if (lstOperators.Items.Count == 0)
            {
                ResetPermissions();
                DisableAllCheckBoxes();
            }
        }
        else
        {
            Visible = false;
        }
    }


    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);

        if (pnlAccessPart.Visible)
        {
            pnlAccessPart.Visible = !(pnlUIAuth.IsHidden && pnlUISsl.IsHidden);
        }

        if (!RequestHelper.IsPostBack())
        {
            // Hide filter
            if ((FilterLimit > 0) && (lstOperators.Items.Count <= FilterLimit))
            {
                pnlFilter.Visible = false;
            }
            else
            {
                pnlFilter.Visible = true;
            }
        }
    }

    #endregion


    #region "Methods"

    /// <summary>
    /// Checks if current use can moddify  the perrmission.
    /// </summary>
    /// <param name="redirect">If true and can't moddify the user is redirected to denied page</param>    
    private bool CanModifyPermission(bool redirect)
    {
        bool hasPermission = false;
        if (node != null)
        {
            hasPermission = (CMSContext.CurrentUser.IsAuthorizedPerDocument(node, NodePermissionsEnum.ModifyPermissions) == AuthorizationResultEnum.Allowed);

            // If hasn't permission and resirect enabled
            if (redirect && !hasPermission)
            {
                RedirectToAccessDenied(String.Format(ResHelper.GetString("cmsdesk.notauthorizedtoeditdocument"), node.NodeAliasPath));
            }
        }
        return hasPermission;
    }


    /// <summary>
    /// Initializes components
    /// </summary>
    private void SetupControls()
    {
        // Set labels
        pnlPermissionsPart.GroupingText = ResHelper.GetString("Security.Permissions");
        pnlAccessPart.GroupingText = ResHelper.GetString("Security.Access");

        lblReqAuthent.Text = ResHelper.GetString("Security.RadioCaption");
        radYes.Text = ResHelper.GetString("general.yes");
        radNo.Text = ResHelper.GetString("general.no");
        radParent.Text = ResHelper.GetString("Security.Parent");

        radYesSSL.Text = ResHelper.GetString("general.yes");
        radNoSSL.Text = ResHelper.GetString("general.no");
        radParentSSL.Text = ResHelper.GetString("Security.Parent");
        radNeverSSL.Text = ResHelper.GetString("Security.Never");

        lblReqSSL.Text = ResHelper.GetString("Security.RequiresSSL");
        lblAccessRights.Text = ResHelper.GetString("Security.AccessRights");
        lblUsersRoles.Text = ResHelper.GetString("Security.UsersRoles");
        lblAllow.Text = ResHelper.GetString("Security.Allow");
        lblDeny.Text = ResHelper.GetString("Security.Deny");
        lblFullControl.Text = ResHelper.GetString("Security.FullControl");
        lblRead.Text = ResHelper.GetString("Security.Read");
        lblModify.Text = ResHelper.GetString("Security.Modify");
        lblCreate.Text = ResHelper.GetString("Security.Create");
        lblDelete.Text = ResHelper.GetString("general.delete");
        lblDestroy.Text = ResHelper.GetString("Security.Destroy");
        lblExploreTree.Text = ResHelper.GetString("Security.ExploreTree");
        lblManagePermissions.Text = ResHelper.GetString("Security.ManagePermissions");
        btnOk.Text = ResHelper.GetString("general.ok");
        btnRadOk.Text = ResHelper.GetString("general.ok");
        lnkBreakWithClear.Text = ResHelper.GetString("Security.BreakWithClear");
        lnkBreakWithCopy.Text = ResHelper.GetString("Security.BreakWithCopy");
        lnkRestoreInheritance.Text = ResHelper.GetString("Security.RestoreInheritance");
        btnCancel.Text = ResHelper.GetString("general.cancel");
        lnkInheritance.Text = ResHelper.GetString("Security.Inheritance");

        // Adds additional events to controls        
        chkFullControlAllow.Attributes.Add("onclick", "CheckAllAllowCheckBoxes();");
        chkFullControlDeny.Attributes.Add("onclick", "CheckAllDenyCheckBoxes();");
        chkCreateAllow.Attributes.Add("onclick", "CheckFullCheck('allow');");
        chkCreateDeny.Attributes.Add("onclick", "CheckFullCheck('deny');");
        chkDeleteAllow.Attributes.Add("onclick", "CheckFullCheck('allow');");
        chkDeleteDeny.Attributes.Add("onclick", "CheckFullCheck('deny');");
        chkDestroyAllow.Attributes.Add("onclick", "CheckFullCheck('allow');");
        chkDestroyDeny.Attributes.Add("onclick", "CheckFullCheck('deny');");
        chkExploreTreeAllow.Attributes.Add("onclick", "CheckFullCheck('allow');");
        chkExploreTreeDeny.Attributes.Add("onclick", "CheckFullCheck('deny');");
        chkManagePermissionsAllow.Attributes.Add("onclick", "CheckFullCheck('allow');");
        chkManagePermissionsDeny.Attributes.Add("onclick", "CheckFullCheck('deny');");
        chkModifyAllow.Attributes.Add("onclick", "CheckFullCheck('allow');");
        chkModifyDeny.Attributes.Add("onclick", "CheckFullCheck('deny');");
        chkReadAllow.Attributes.Add("onclick", "CheckFullCheck('allow');");
        chkReadDeny.Attributes.Add("onclick", "CheckFullCheck('deny');");
    }


    /// <summary>
    /// Loads users and roles listed in the ACL of the selected document and displays them in the listbox.
    /// </summary>
    /// <param name="reload">Forces reload of listbox</param>
    private void LoadOperators(bool reload)
    {
        if (!pnlUIPermissionsPart.IsHidden)
        {
            string lastOperator = "";
            string roles = String.Empty;
            string users = String.Empty;

            if (reload)
            {
                lstOperators.Items.Clear();
            }

            LoadACLItems();
            if ((dsAclItems != null) && (dsAclItems.Tables.Count > 0))
            {
                foreach (DataRow drAclItem in dsAclItems.Tables[0].Rows)
                {
                    string op = ValidationHelper.GetString(drAclItem["Operator"], "");
                    if (op != lastOperator)
                    {
                        lastOperator = op;
                        string operName = ValidationHelper.GetString(drAclItem["OperatorName"], String.Empty);

                        if (!String.IsNullOrEmpty(op))
                        {
                            switch (op[0])
                            {
                                // Operator starts with 'R' - indicates role
                                case 'R':
                                    roles += op.Substring(1) + ";";
                                    break;
                                // Operator starts with 'U' - indicates user
                                case 'U':
                                    users += op.Substring(1) + ";";

                                    string fullName = ValidationHelper.GetString(drAclItem["OperatorFullName"], String.Empty);
                                    operName = Functions.GetFormattedUserName(operName, fullName);
                                    break;
                            }
                        }

                        if (reload)
                        {
                            lstOperators.Items.Add(new ListItem(operName, op));
                        }
                    }
                }
            }

            if (reload)
            {
                if (lstOperators.Items.Count > 0)
                {
                    lstOperators.SelectedIndex = 0;
                    DisplayOperatorPermissions(lstOperators.SelectedValue);
                }
                else
                {
                    ResetPermissions();
                    DisableAllCheckBoxes();
                }

                // Update selector values on full reload
                addRoles.CurrentSelector.Value = roles;
                addUsers.CurrentSelector.Value = users;
            }

            // Set values to selectors (to be able to distinguish new and old items for add/remove action)
            addRoles.CurrentValues = roles;
            addUsers.CurrentValues = users;
        }
    }


    /// <summary>
    /// Laod ACLItems for the selected document.
    /// </summary>
    private void LoadACLItems()
    {
        if (dsAclItems == null)
        {
            string where = GetWhereCondition();
            dsAclItems = aclProv.GetACLItems(nodeId, where, "operatorname, operator", 0, null);
        }
    }


    private string GetWhereCondition()
    {
        string where = null;
        if (!string.IsNullOrEmpty(txtFilter.Text))
        {
            where = "OperatorName LIKE '%" + txtFilter.Text.Replace("'", "''") + "%'";
        }
        return where;
    }


    /// <summary>
    /// Displays permissions of the selected operator (user or role).
    /// </summary>
    /// <param name="operatorID">OperatorID in format U123 or R123 where U/R indicates user/role and 123 is UserID/RoleID value.</param>
    private void DisplayOperatorPermissions(string operatorID)
    {
        if (!pnlUIPermissionsPart.IsHidden)
        {
            ResetPermissions();

            LoadACLItems();
            DataRow[] rows = null;
            bool hasNativePermissions = false;
            bool hasInheritedPermissions = false;

            if ((dsAclItems != null) && (dsAclItems.Tables.Count > 0))
            {
                int i = 0;
                int allowed = 0;
                int denied = 0;
                // process inherited permissions
                rows = dsAclItems.Tables[0].Select(" Operator = '" + operatorID + "' AND ACLOwnerNodeID <> '" + node.NodeID + "' ");
                for (i = 0; i <= rows.GetUpperBound(0); i++)
                {
                    hasInheritedPermissions = true;
                    allowed = Convert.ToInt32(rows[i]["Allowed"]);
                    denied = Convert.ToInt32(rows[i]["Denied"]);

                    // set "allow" check boxes for inherited permissions
                    chkFullControlAllow.Checked = chkFullControlAllow.Checked | (allowed == 127);
                    chkFullControlAllow.Enabled = chkFullControlAllow.Enabled & (allowed != 127);
                    chkReadAllow.Checked = chkReadAllow.Checked | IsPermissionTrue(allowed, NodePermissionsEnum.Read);
                    chkReadAllow.Enabled = chkReadAllow.Enabled & !(IsPermissionTrue(allowed, NodePermissionsEnum.Read));
                    chkModifyAllow.Checked = chkModifyAllow.Checked | IsPermissionTrue(allowed, NodePermissionsEnum.Modify);
                    chkModifyAllow.Enabled = chkModifyAllow.Enabled & !(IsPermissionTrue(allowed, NodePermissionsEnum.Modify));
                    chkCreateAllow.Checked = chkCreateAllow.Checked | IsPermissionTrue(allowed, NodePermissionsEnum.Create);
                    chkCreateAllow.Enabled = chkCreateAllow.Enabled & !(IsPermissionTrue(allowed, NodePermissionsEnum.Create));
                    chkDeleteAllow.Checked = chkDeleteAllow.Checked | IsPermissionTrue(allowed, NodePermissionsEnum.Delete);
                    chkDeleteAllow.Enabled = chkDeleteAllow.Enabled & !(IsPermissionTrue(allowed, NodePermissionsEnum.Delete));
                    chkDestroyAllow.Checked = chkDestroyAllow.Checked | IsPermissionTrue(allowed, NodePermissionsEnum.Destroy);
                    chkDestroyAllow.Enabled = chkDestroyAllow.Enabled & !(IsPermissionTrue(allowed, NodePermissionsEnum.Destroy));
                    chkExploreTreeAllow.Checked = chkExploreTreeAllow.Checked | IsPermissionTrue(allowed, NodePermissionsEnum.ExploreTree);
                    chkExploreTreeAllow.Enabled = chkExploreTreeAllow.Enabled & !(IsPermissionTrue(allowed, NodePermissionsEnum.ExploreTree));
                    chkManagePermissionsAllow.Checked = chkManagePermissionsAllow.Checked | IsPermissionTrue(allowed, NodePermissionsEnum.ModifyPermissions);
                    chkManagePermissionsAllow.Enabled = chkManagePermissionsAllow.Enabled & !(IsPermissionTrue(allowed, NodePermissionsEnum.ModifyPermissions));
                    // set "deny" checkboxes for inherited permissions
                    chkFullControlDeny.Checked = chkFullControlDeny.Checked | (denied == 127);
                    chkFullControlDeny.Enabled = chkFullControlDeny.Enabled & (denied != 127);
                    chkReadDeny.Checked = chkReadDeny.Checked | IsPermissionTrue(denied, NodePermissionsEnum.Read);
                    chkReadDeny.Enabled = chkReadDeny.Enabled & !(IsPermissionTrue(denied, NodePermissionsEnum.Read));
                    chkModifyDeny.Checked = chkModifyDeny.Checked | IsPermissionTrue(denied, NodePermissionsEnum.Modify);
                    chkModifyDeny.Enabled = chkModifyDeny.Enabled & !(IsPermissionTrue(denied, NodePermissionsEnum.Modify));
                    chkCreateDeny.Checked = chkCreateDeny.Checked | IsPermissionTrue(denied, NodePermissionsEnum.Create);
                    chkCreateDeny.Enabled = chkCreateDeny.Enabled & !(IsPermissionTrue(denied, NodePermissionsEnum.Create));
                    chkDeleteDeny.Checked = chkDeleteDeny.Checked | IsPermissionTrue(denied, NodePermissionsEnum.Delete);
                    chkDeleteDeny.Enabled = chkDeleteDeny.Enabled & !(IsPermissionTrue(denied, NodePermissionsEnum.Delete));
                    chkDestroyDeny.Checked = chkDestroyDeny.Checked | IsPermissionTrue(denied, NodePermissionsEnum.Destroy);
                    chkDestroyDeny.Enabled = chkDestroyDeny.Enabled & !(IsPermissionTrue(denied, NodePermissionsEnum.Destroy));
                    chkExploreTreeDeny.Checked = chkExploreTreeDeny.Checked | IsPermissionTrue(denied, NodePermissionsEnum.ExploreTree);
                    chkExploreTreeDeny.Enabled = chkExploreTreeDeny.Enabled & !(IsPermissionTrue(denied, NodePermissionsEnum.ExploreTree));
                    chkManagePermissionsDeny.Checked = chkManagePermissionsDeny.Checked | IsPermissionTrue(denied, NodePermissionsEnum.ModifyPermissions);
                    chkManagePermissionsDeny.Enabled = chkManagePermissionsDeny.Enabled & !(IsPermissionTrue(denied, NodePermissionsEnum.ModifyPermissions));
                }
                // process native permissions
                rows = dsAclItems.Tables[0].Select(" Operator = '" + operatorID + "' AND ACLOwnerNodeID = '" + node.NodeID + "' ");
                for (i = 0; i <= rows.GetUpperBound(0); i++)
                {
                    hasNativePermissions = true;
                    allowed = Convert.ToInt32(rows[i]["Allowed"]);
                    denied = Convert.ToInt32(rows[i]["Denied"]);

                    // set "allow" check boxes for native permissions
                    SetCheckBoxWithNativePermission(chkReadAllow, IsPermissionTrue(allowed, NodePermissionsEnum.Read));
                    SetCheckBoxWithNativePermission(chkModifyAllow, IsPermissionTrue(allowed, NodePermissionsEnum.Modify));
                    SetCheckBoxWithNativePermission(chkCreateAllow, IsPermissionTrue(allowed, NodePermissionsEnum.Create));
                    SetCheckBoxWithNativePermission(chkDeleteAllow, IsPermissionTrue(allowed, NodePermissionsEnum.Delete));
                    SetCheckBoxWithNativePermission(chkDestroyAllow, IsPermissionTrue(allowed, NodePermissionsEnum.Destroy));
                    SetCheckBoxWithNativePermission(chkExploreTreeAllow, IsPermissionTrue(allowed, NodePermissionsEnum.ExploreTree));
                    SetCheckBoxWithNativePermission(chkManagePermissionsAllow, IsPermissionTrue(allowed, NodePermissionsEnum.ModifyPermissions));

                    // set "deny" checkboxes for inherited permissions
                    SetCheckBoxWithNativePermission(chkReadDeny, IsPermissionTrue(denied, NodePermissionsEnum.Read));
                    SetCheckBoxWithNativePermission(chkModifyDeny, IsPermissionTrue(denied, NodePermissionsEnum.Modify));
                    SetCheckBoxWithNativePermission(chkCreateDeny, IsPermissionTrue(denied, NodePermissionsEnum.Create));
                    SetCheckBoxWithNativePermission(chkDeleteDeny, IsPermissionTrue(denied, NodePermissionsEnum.Delete));
                    SetCheckBoxWithNativePermission(chkDestroyDeny, IsPermissionTrue(denied, NodePermissionsEnum.Destroy));
                    SetCheckBoxWithNativePermission(chkExploreTreeDeny, IsPermissionTrue(denied, NodePermissionsEnum.ExploreTree));
                    SetCheckBoxWithNativePermission(chkManagePermissionsDeny, IsPermissionTrue(denied, NodePermissionsEnum.ModifyPermissions));
                }
            }

            btnRemoveOperator.Enabled = hasModifyPermission && (!(hasInheritedPermissions)) && hasNativePermissions;
            CheckFullControlBox();
        }
    }


    /// <summary>
    /// Parses permission value and return true if appropriate bit is 1.
    /// </summary>
    protected bool IsPermissionTrue(int permissionValue, NodePermissionsEnum permission)
    {
        return ((permissionValue >> Convert.ToInt32(permission)) % 2) == 1;
    }


    /// <summary>
    /// Checks checkbox if provided value is true.
    /// </summary>
    protected void SetCheckBoxWithNativePermission(CheckBox chkBox, bool value)
    {
        if (value)
        {
            chkBox.Checked = true;
        }
    }


    /// <summary>
    /// Returns integer value representing permission value of the given checkbox and permission name.
    /// </summary>
    protected int GetCheckBoxValue(CheckBox chkBox, NodePermissionsEnum permission)
    {
        if (chkBox.Enabled && chkBox.Checked)
        {
            return Convert.ToInt32(Math.Pow(2, Convert.ToInt32(permission)));
        }
        else
        {
            return 0;
        }
    }


    /// <summary>
    /// Resets all checkboxes representing permissions.
    /// </summary>
    protected void ResetPermissions()
    {
        ResetCheckBox(chkFullControlAllow);
        ResetCheckBox(chkFullControlDeny);
        ResetCheckBox(chkReadAllow);
        ResetCheckBox(chkReadDeny);
        ResetCheckBox(chkCreateAllow);
        ResetCheckBox(chkCreateDeny);
        ResetCheckBox(chkModifyAllow);
        ResetCheckBox(chkModifyDeny);
        ResetCheckBox(chkDeleteAllow);
        ResetCheckBox(chkDeleteDeny);
        ResetCheckBox(chkDestroyAllow);
        ResetCheckBox(chkDestroyDeny);
        ResetCheckBox(chkExploreTreeAllow);
        ResetCheckBox(chkExploreTreeDeny);
        ResetCheckBox(chkManagePermissionsAllow);
        ResetCheckBox(chkManagePermissionsDeny);
    }


    /// <summary>
    /// Resets given checkbox. Enables set to true, checked to false
    /// </summary>
    protected void ResetCheckBox(CheckBox chkBox)
    {
        chkBox.Enabled = true;
        chkBox.Checked = false;
    }



    private void SwitchBackToPermissionsMode()
    {
        pnlPermissionsPart.Visible = true;
        pnlAccessPart.Visible = true;
        pnlInheritance.Visible = false;
    }


    private static void CheckEnabledCheckbox(CheckBox chkBox, bool checkedIdent)
    {
        if (chkBox.Enabled)
        {
            chkBox.Checked = checkedIdent;
        }
    }


    private void CheckFullControlBox()
    {
        chkFullControlAllow.Checked = chkReadAllow.Checked && chkModifyAllow.Checked && chkCreateAllow.Checked && chkDeleteAllow.Checked && chkDestroyAllow.Checked && chkExploreTreeAllow.Checked && chkManagePermissionsAllow.Checked;
        chkFullControlDeny.Checked = chkReadDeny.Checked && chkModifyDeny.Checked && chkCreateDeny.Checked && chkDeleteDeny.Checked && chkDestroyDeny.Checked && chkExploreTreeDeny.Checked && chkManagePermissionsDeny.Checked;
    }


    protected void DisableAllCheckBoxes()
    {
        chkCreateAllow.Enabled = false;
        chkCreateDeny.Enabled = false;
        chkDeleteAllow.Enabled = false;
        chkDeleteDeny.Enabled = false;
        chkDestroyAllow.Enabled = false;
        chkDestroyDeny.Enabled = false;
        chkExploreTreeAllow.Enabled = false;
        chkExploreTreeDeny.Enabled = false;
        chkFullControlAllow.Enabled = false;
        chkFullControlDeny.Enabled = false;
        chkManagePermissionsAllow.Enabled = false;
        chkManagePermissionsDeny.Enabled = false;
        chkModifyAllow.Enabled = false;
        chkModifyDeny.Enabled = false;
        chkReadAllow.Enabled = false;
        chkReadDeny.Enabled = false;
    }


    /// <summary>
    /// Loads javacript
    /// </summary>
    private void LoadJavascript()
    {
        string javascript = @"   

        function CheckAllDenyCheckBoxes()
        {
            if(document.getElementById('" + chkFullControlDeny.ClientID + @"').checked == true)
            {
                if(document.getElementById('" + chkReadDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkReadDeny.ClientID + @"').checked = true;
                if(document.getElementById('" + chkCreateDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkCreateDeny.ClientID + @"').checked = true;
                if(document.getElementById('" + chkModifyDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkModifyDeny.ClientID + @"').checked = true;
                if(document.getElementById('" + chkDeleteDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkDeleteDeny.ClientID + @"').checked = true;
                if(document.getElementById('" + chkDestroyDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkDestroyDeny.ClientID + @"').checked = true;
                if(document.getElementById('" + chkExploreTreeDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkExploreTreeDeny.ClientID + @"').checked = true;
                if(document.getElementById('" + chkManagePermissionsDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkManagePermissionsDeny.ClientID + @"').checked = true;
           }
           else
           {
                if(document.getElementById('" + chkReadDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkReadDeny.ClientID + @"').checked = false;
                if(document.getElementById('" + chkCreateDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkCreateDeny.ClientID + @"').checked = false;
                if(document.getElementById('" + chkModifyDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkModifyDeny.ClientID + @"').checked = false;
                if(document.getElementById('" + chkDeleteDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkDeleteDeny.ClientID + @"').checked = false;
                if(document.getElementById('" + chkDestroyDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkDestroyDeny.ClientID + @"').checked = false;
                if(document.getElementById('" + chkExploreTreeDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkExploreTreeDeny.ClientID + @"').checked = false;
                if(document.getElementById('" + chkManagePermissionsDeny.ClientID + @"').disabled == false)
                    document.getElementById('" + chkManagePermissionsDeny.ClientID + @"').checked = false;
           }
        }

        function CheckAllAllowCheckBoxes()
        {
            if(document.getElementById('" + chkFullControlAllow.ClientID + @"').checked == true)
            {
                if(document.getElementById('" + chkReadAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkReadAllow.ClientID + @"').checked = true;
                if(document.getElementById('" + chkCreateAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkCreateAllow.ClientID + @"').checked = true;
                if(document.getElementById('" + chkModifyAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkModifyAllow.ClientID + @"').checked = true;
                if(document.getElementById('" + chkDeleteAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkDeleteAllow.ClientID + @"').checked = true;
                if(document.getElementById('" + chkDestroyAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkDestroyAllow.ClientID + @"').checked = true;
                if(document.getElementById('" + chkExploreTreeAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkExploreTreeAllow.ClientID + @"').checked = true;
                if(document.getElementById('" + chkManagePermissionsAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkManagePermissionsAllow.ClientID + @"').checked = true;
            }
            else
            {
                if(document.getElementById('" + chkReadAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkReadAllow.ClientID + @"').checked = false;
                if(document.getElementById('" + chkCreateAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkCreateAllow.ClientID + @"').checked = false;
                if(document.getElementById('" + chkModifyAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkModifyAllow.ClientID + @"').checked = false;
                if(document.getElementById('" + chkDeleteAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkDeleteAllow.ClientID + @"').checked = false;
                if(document.getElementById('" + chkDestroyAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkDestroyAllow.ClientID + @"').checked = false;
                if(document.getElementById('" + chkExploreTreeAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkExploreTreeAllow.ClientID + @"').checked = false;
                if(document.getElementById('" + chkManagePermissionsAllow.ClientID + @"').disabled == false)
                    document.getElementById('" + chkManagePermissionsAllow.ClientID + @"').checked = false;
            }
        }
       
        function CheckFullCheck(which)
        {
            if(which=='allow')
            {
                if((document.getElementById('" + chkReadAllow.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkCreateAllow.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkModifyAllow.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkDeleteAllow.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkDestroyAllow.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkExploreTreeAllow.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkManagePermissionsAllow.ClientID + @"').checked == true))
                {
                        document.getElementById('" + chkFullControlAllow.ClientID + @"').checked = true;
                }
                else
                {
                        document.getElementById('" + chkFullControlAllow.ClientID + @"').checked = false;                            
                }
            }
            else
            {
                if((document.getElementById('" + chkReadDeny.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkCreateDeny.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkModifyDeny.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkDeleteDeny.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkDestroyDeny.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkExploreTreeDeny.ClientID + @"').checked == true) &&
                   (document.getElementById('" + chkManagePermissionsDeny.ClientID + @"').checked == true))
                {
                        document.getElementById('" + chkFullControlDeny.ClientID + @"').checked = true;
                }
                else
                {
                        document.getElementById('" + chkFullControlDeny.ClientID + @"').checked = false;                            
                }
            }
        }    
        ";

        javascript = ScriptHelper.GetScript(javascript);
        ScriptHelper.RegisterClientScriptBlock(this, typeof(string), ClientID + "js", javascript);
    }

    #endregion


    #region "Events"

    /// <summary>
    /// On changed roles - update update panel.
    /// </summary>
    protected void Selection_Changed()
    {
        if (!pnlUIPermissionsPart.IsHidden)
        {
            dsAclItems = null;
            LoadOperators(true);
            pnlUpdate.Update();
        }
    }


    protected void btnFilter_Click(object sender, EventArgs e)
    {
        if (!pnlUIPermissionsPart.IsHidden)
        {
            LoadOperators(true);
        }
    }


    protected void lstOperators_SelectedIndexChanged(Object sender, EventArgs e)
    {
        if (lstOperators.SelectedItem != null)
        {
            DisplayOperatorPermissions(lstOperators.SelectedValue);
        }
        else
        {
            DisableAllCheckBoxes();
        }
    }


    /// <summary>
    /// Saves data.
    /// </summary>
    protected void btnOK_Click(Object sender, EventArgs e)
    {
        // Check permission
        CanModifyPermission(true);

        int allowed = 0;
        int denied = 0;
        string operatorID = null;

        if (lstOperators.SelectedItem == null)
        {
            return;
        }
        else
        {
            operatorID = lstOperators.SelectedValue;
            allowed += GetCheckBoxValue(chkReadAllow, NodePermissionsEnum.Read);
            allowed += GetCheckBoxValue(chkModifyAllow, NodePermissionsEnum.Modify);
            allowed += GetCheckBoxValue(chkCreateAllow, NodePermissionsEnum.Create);
            allowed += GetCheckBoxValue(chkDeleteAllow, NodePermissionsEnum.Delete);
            allowed += GetCheckBoxValue(chkDestroyAllow, NodePermissionsEnum.Destroy);
            allowed += GetCheckBoxValue(chkExploreTreeAllow, NodePermissionsEnum.ExploreTree);
            allowed += GetCheckBoxValue(chkManagePermissionsAllow, NodePermissionsEnum.ModifyPermissions);
            denied += GetCheckBoxValue(chkReadDeny, NodePermissionsEnum.Read);
            denied += GetCheckBoxValue(chkModifyDeny, NodePermissionsEnum.Modify);
            denied += GetCheckBoxValue(chkCreateDeny, NodePermissionsEnum.Create);
            denied += GetCheckBoxValue(chkDeleteDeny, NodePermissionsEnum.Delete);
            denied += GetCheckBoxValue(chkDestroyDeny, NodePermissionsEnum.Destroy);
            denied += GetCheckBoxValue(chkExploreTreeDeny, NodePermissionsEnum.ExploreTree);
            denied += GetCheckBoxValue(chkManagePermissionsDeny, NodePermissionsEnum.ModifyPermissions);

            string message = null;
            string operatorName = lstOperators.SelectedItem.Text;
            if (operatorID.StartsWith("U"))
            {
                int userId = int.Parse(operatorID.Substring(1));
                UserInfo ui = UserInfoProvider.GetUserInfo(userId);
                aclProv.SetUserPermissions(node, allowed, denied, ui);
                message = "security.documentuserpermissionschange";
            }
            else
            {
                aclProv.SetRolePermissions(node, allowed, denied, int.Parse(operatorID.Substring(1)));
                message = "security.documentrolepermissionschange";
            }
            lblInfo.Text = ResHelper.GetString("general.changessaved");

            // Insert information about this event to eventlog.
            if (Tree.LogEvents)
            {
                EventLog.LogEvent("I", DateTime.Now, "Content", "DOCPERMISSIONSMODIFIED", Tree.UserInfo.UserID, Tree.UserInfo.UserName, nodeId, DocumentName, HTTPHelper.GetUserHostAddress(), string.Format(ResHelper.GetAPIString(message, "Permissions of the operator '{0}' have been modified for the document."), operatorName), node.NodeSiteID, HTTPHelper.GetAbsoluteUri());
            }
        }
    }


    /// <summary>
    /// Removes selected operator from the ACL.
    /// </summary>
    protected void btnRemoveOperator_Click(Object sender, EventArgs e)
    {
        // Check permission
        CanModifyPermission(true);

        if (lstOperators.SelectedItem == null)
        {
            return;
        }

        string operatorName = lstOperators.SelectedItem.Text;
        string message = null;
        string operatorID = lstOperators.SelectedValue;
        if (operatorID.StartsWith("U"))
        {
            int userId = int.Parse(operatorID.Substring(1));
            UserInfo ui = UserInfoProvider.GetUserInfo(userId);
            aclProv.RemoveUser(nodeId, ui);
            message = "security.documentuserpermissionremoved";
        }
        else
        {
            aclProv.RemoveRole(nodeId, int.Parse(operatorID.Substring(1)));
            message = "security.documentrolepermissionremoved";
        }

        // Insert information about this event to eventlog.
        if (Tree.LogEvents)
        {
            EventLog.LogEvent("I", DateTime.Now, "Content", "DOCPERMISSIONSMODIFIED", Tree.UserInfo.UserID, Tree.UserInfo.UserName, nodeId, DocumentName, HTTPHelper.GetUserHostAddress(), string.Format(ResHelper.GetAPIString(message, "Operator '{0}' has been removed from the document permissions."), operatorName), node.NodeSiteID, HTTPHelper.GetAbsoluteUri());
        }

        dsAclItems = null;
        LoadOperators(true);
    }


    /// <summary>
    /// Displays inheritance settings.
    /// </summary>
    protected void lnkInheritance_Click(Object sender, EventArgs e)
    {
        // Check permission
        CanModifyPermission(true);

        pnlPermissionsPart.Visible = false;
        pnlAccessPart.Visible = false;
        pnlInheritance.Visible = true;

        // test if current document inherits permissions
        if (inheritsPermissions)
        {
            plcBreakClear.Visible = true;
            plcBreakCopy.Visible = true;
            plcRestore.Visible = false;
        }
        else
        {
            plcBreakClear.Visible = false;
            plcBreakCopy.Visible = false;
            plcRestore.Visible = true;
        }
    }


    protected void btnCancel_Click(Object sender, EventArgs e)
    {
        SwitchBackToPermissionsMode();
    }


    protected void lnkBreakWithCopy_Click(Object sender, EventArgs e)
    {
        // Check permission
        CanModifyPermission(true);

        // break permission inheritance and copy parent permissions
        aclProv.BreakInherintance(node, true);

        // Insert information about this event to eventlog.
        if (Tree.LogEvents)
        {
            EventLog.LogEvent("I", DateTime.Now, "Content", "DOCPERMISSIONSMODIFIED", Tree.UserInfo.UserID, Tree.UserInfo.UserName, nodeId, DocumentName, HTTPHelper.GetUserHostAddress(), ResHelper.GetAPIString("security.documentpermissionsbreakcopy", "Inheritance of the parent document permissions have been broken. Parent document permissions have been copied."), node.NodeSiteID, HTTPHelper.GetAbsoluteUri());
        }

        lblInheritanceInfo.Text = ResHelper.GetString("Security.InheritsInfo.DoesNotInherit");
        SwitchBackToPermissionsMode();

        // Clear and reload
        dsAclItems = null;
        LoadOperators(true);
    }


    protected void lnkBreakWithClear_Click(Object sender, EventArgs e)
    {
        // Check permission
        CanModifyPermission(true);

        // Break permission inheritance and clear permissions
        aclProv.BreakInherintance(node, false);

        // Insert information about this event to eventlog.
        if (Tree.LogEvents)
        {
            EventLog.LogEvent("I", DateTime.Now, "Content", "DOCPERMISSIONSMODIFIED", Tree.UserInfo.UserID, Tree.UserInfo.UserName, nodeId, DocumentName, HTTPHelper.GetUserHostAddress(), ResHelper.GetAPIString("security.documentpermissionsbreakclear", "Inheritance of the parent document permissions have been broken."), node.NodeSiteID, HTTPHelper.GetAbsoluteUri());
        }

        lblInheritanceInfo.Text = ResHelper.GetString("Security.InheritsInfo.DoesNotInherit");
        SwitchBackToPermissionsMode();

        // Clear and reload
        dsAclItems = null;
        LoadOperators(true);
    }


    protected void lnkRestoreInheritance_Click(Object sender, EventArgs e)
    {
        // Check permission
        CanModifyPermission(true);

        // Restore inheritance and add permissions inherited from the parent document
        aclProv.RestoreInheritance(node);

        // Insert information about this event to eventlog.
        if (Tree.LogEvents)
        {
            EventLog.LogEvent("I", DateTime.Now, "Content", "DOCPERMISSIONSMODIFIED", Tree.UserInfo.UserID, Tree.UserInfo.UserName, nodeId, DocumentName, HTTPHelper.GetUserHostAddress(), ResHelper.GetAPIString("security.documentpermissionsrestored", "Permissions have been restored to the parent document permissions."), node.NodeSiteID, HTTPHelper.GetAbsoluteUri());
        }

        lblInheritanceInfo.Text = ResHelper.GetString("Security.InheritsInfo.Inherits");
        SwitchBackToPermissionsMode();

        // Clear and reload
        dsAclItems = null;
        LoadOperators(true);
    }


    protected void chkFullControlAllow_CheckedChanged(Object sender, EventArgs e)
    {
        // Check permission
        CanModifyPermission(true);

        // Check all enabled checkboxes
        CheckEnabledCheckbox(chkReadAllow, chkFullControlAllow.Checked);
        CheckEnabledCheckbox(chkCreateAllow, chkFullControlAllow.Checked);
        CheckEnabledCheckbox(chkModifyAllow, chkFullControlAllow.Checked);
        CheckEnabledCheckbox(chkDeleteAllow, chkFullControlAllow.Checked);
        CheckEnabledCheckbox(chkDestroyAllow, chkFullControlAllow.Checked);
        CheckEnabledCheckbox(chkExploreTreeAllow, chkFullControlAllow.Checked);
        CheckEnabledCheckbox(chkManagePermissionsAllow, chkFullControlAllow.Checked);
    }


    protected void chkFullControlDeny_CheckedChanged(Object sender, EventArgs e)
    {
        // Check permission
        CanModifyPermission(true);

        // check all deny checkboxes
        CheckEnabledCheckbox(chkReadDeny, chkFullControlDeny.Checked);
        CheckEnabledCheckbox(chkCreateDeny, chkFullControlDeny.Checked);
        CheckEnabledCheckbox(chkModifyDeny, chkFullControlDeny.Checked);
        CheckEnabledCheckbox(chkDeleteDeny, chkFullControlDeny.Checked);
        CheckEnabledCheckbox(chkDestroyDeny, chkFullControlDeny.Checked);
        CheckEnabledCheckbox(chkExploreTreeDeny, chkFullControlDeny.Checked);
        CheckEnabledCheckbox(chkManagePermissionsDeny, chkFullControlDeny.Checked);
    }


    /// <summary>
    /// On click OK save secured settings
    /// </summary>
    protected void btnRadOk_Click(object sender, EventArgs e)
    {
        // Check permission
        CanModifyPermission(true);

        if (node != null)
        {
            string message = null;
            bool clearCache = false;
            // Authentication
            if (!pnlUIAuth.IsHidden)
            {
                int isSecuredNode = node.IsSecuredNode;

                if (radYes.Checked)
                {
                    isSecuredNode = 1;
                }
                else if (radNo.Checked)
                {
                    isSecuredNode = 0;
                }
                else if (radParent.Checked)
                {
                    isSecuredNode = -1;
                }

                // Set secured areas settings
                if (isSecuredNode != node.IsSecuredNode)
                {
                    node.IsSecuredNode = isSecuredNode;
                    clearCache = true;
                    message += ResHelper.GetAPIString("security.documentaccessauthchanged", "Document authentication settings have been modified.");
                }
            }

            // SSL
            if (!pnlUISsl.IsHidden)
            {
                int requiresSSL = node.RequiresSSL;

                if (radYesSSL.Checked)
                {
                    requiresSSL = 1;
                }
                else if (radNoSSL.Checked)
                {
                    requiresSSL = 0;
                }
                else if (radParentSSL.Checked)
                {
                    requiresSSL = -1;
                }
                else if (radNeverSSL.Checked)
                {
                    requiresSSL = 2;
                }

                // Set SSL settings
                if (requiresSSL != node.RequiresSSL)
                {
                    node.RequiresSSL = requiresSSL;
                    clearCache = true;
                    if (message != null)
                    {
                        message += "<br />";
                    }
                    message += ResHelper.GetAPIString("security.documentaccesssslchanged", "Document SSL settings have been modified.");
                }
            }

            node.Update();

            // Insert information about this event to eventlog.
            if (Tree.LogEvents && (message != null))
            {
                EventLog.LogEvent("I", DateTime.Now, "Content", "DOCACCESSMODIFIED", Tree.UserInfo.UserID, Tree.UserInfo.UserName, nodeId, DocumentName, HTTPHelper.GetUserHostAddress(), message, node.NodeSiteID, HTTPHelper.GetAbsoluteUri());
            }

            // Update search index for node
            if ((node.PublishedVersionExists) && (SearchIndexInfoProvider.SearchEnabled))
            {
                SearchTaskInfoProvider.CreateTask(SearchTaskTypeEnum.Update, PredefinedObjectType.DOCUMENT, SearchHelper.ID_FIELD, node.GetSearchID());
            }

            // Log synchronization
            DocumentHelper.LogSynchronization(node, CMS.Staging.TaskTypeEnum.UpdateDocument, Tree);

            // Clear cache if security settings changed
            if (clearCache)
            {
                CacheHelper.ClearCache(null);
            }

            lblAccessInfo.Visible = !pnlUIAuth.IsHidden;
            lblAccesInfo2.Visible = !pnlUISsl.IsHidden;
            lblAccessInfo.Text = ResHelper.GetString("general.changessaved");
            lblAccesInfo2.Text = !lblAccessInfo.Visible ? lblAccessInfo.Text : "<br/>";
        }
    }

    #endregion
}
