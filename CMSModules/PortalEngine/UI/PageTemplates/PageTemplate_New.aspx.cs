using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.SiteProvider;
using CMS.PortalEngine;
using CMS.UIControls;

public partial class CMSModules_PortalEngine_UI_PageTemplates_PageTemplate_New : SiteManagerPage
{
    protected int parentCategoryId = 0;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        parentCategoryId = QueryHelper.GetInteger("parentcategoryid", 0);
        
        if (!RequestHelper.IsPostBack())
        {
            lblTemplateDisplayName.Text = ResHelper.GetString("Administration-PageTemplate_General.TemplateDisplayName");
            lblTemplateDescription.Text = ResHelper.GetString("Administration-PageTemplate_General.TemplateDescription");
            lblTemplateCodeName.Text = ResHelper.GetString("Administration-PageTemplate_General.TemplateCodeName");
            btnOk.Text = ResHelper.GetString("General.OK");

            rfvTemplateDisplayName.ErrorMessage = ResHelper.GetString("Administration-PageTemplate_General.ErrorEmptyTemplateDisplayName");
            rfvTemplateCodeName.ErrorMessage = ResHelper.GetString("Administration-PageTemplate_General.ErrorEmptyTemplateCodeName");
            txtTemplateDisplayName.Focus();
        }
        string currentPageTemplate = ResHelper.GetString("Administration-PageTemplate_Header.NewPageTemplate");
        // Initialize page title
        string templates = ResHelper.GetString("Administration-PageTemplate_Header.Templates");
        string title = ResHelper.GetString("PageTemplate.NewTitle");
        
        string[,] pageTitleTabs = new string[2, 3];
        pageTitleTabs[0, 0] = templates;
        pageTitleTabs[0, 1] = ResolveUrl("PageTemplate_Frameset.aspx");
        pageTitleTabs[0, 2] = "_parent";
        pageTitleTabs[1, 0] = currentPageTemplate;
        pageTitleTabs[1, 1] = "";
        pageTitleTabs[1, 2] = "";

        this.CurrentMaster.Title.Breadcrumbs = pageTitleTabs;
        this.CurrentMaster.Title.TitleText = title;
        this.CurrentMaster.Title.TitleImage = GetImageUrl("CMSModules/CMS_PageTemplates/addpagetemplate.png");
        this.CurrentMaster.Title.HelpTopicName = "new_page_template";
        this.CurrentMaster.Title.HelpName = "helpTopic";
    }
    
    
    protected void btnOK_Click(object sender, EventArgs e)
    {
        // Limit text length
        txtTemplateCodeName.Text = TextHelper.LimitLength(txtTemplateCodeName.Text.Trim(), 100, "");
        txtTemplateDisplayName.Text = TextHelper.LimitLength(txtTemplateDisplayName.Text.Trim(), 200, "");

        // finds whether required fields are not empty
        string result = new Validator().NotEmpty(txtTemplateDisplayName.Text, ResHelper.GetString("Administration-PageTemplate_General.ErrorEmptyTemplateDisplayName")).NotEmpty(txtTemplateCodeName.Text, ResHelper.GetString("Administration-PageTemplate_General.ErrorEmptyTemplateCodeName"))
            .IsCodeName(txtTemplateCodeName.Text, ResHelper.GetString("general.invalidcodename"))
            .Result;

        if (result == "")
        {
            if (parentCategoryId > 0)
            {
                if (!PageTemplateInfoProvider.PageTemplateNameExists(txtTemplateCodeName.Text))
                {
                    //Insert page template info
                    PageTemplateInfo pi = new PageTemplateInfo();
                    pi.DisplayName = txtTemplateDisplayName.Text;
                    pi.CodeName = txtTemplateCodeName.Text;
                    pi.FileName = "";
                    pi.WebParts = "";
                    pi.Description = txtTemplateDescription.Text;
                    pi.CategoryID = parentCategoryId;
                    pi.IsReusable = true; //set default value for isReusable
                    PageTemplateInfoProvider.SetPageTemplateInfo(pi);
                    ltlScript.Text = "<script type=\"text/javascript\">";
                    ltlScript.Text += "parent.frames['pt_tree'].location.href = 'PageTemplate_Tree.aspx?templateid=" + pi.PageTemplateId + "';";
                    ltlScript.Text += "this.location.href = 'PageTemplate_Edit.aspx?templateid=" + pi.PageTemplateId + "';";
                    ltlScript.Text += "</script>";
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Administration-PageTemplate_New.TemplateExistsAlready");
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Administration-PageTemplate_New.NoParentIdGiven");
            }
        }
        else
        {
            rfvTemplateDisplayName.Visible = false;
            rfvTemplateCodeName.Visible = false;
            lblError.Visible = true;
            lblError.Text = result;
        }
    }
}
