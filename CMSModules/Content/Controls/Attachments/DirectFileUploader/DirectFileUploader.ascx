<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DirectFileUploader.ascx.cs"
    Inherits="CMSModules_Content_Controls_Attachments_DirectFileUploader_DirectFileUploader" %>
<div id="containerDiv" runat="server">
    <span runat="server" id="spnContainer">
        <iframe id="uploaderFrame" runat="server" frameborder="0" scrolling="no" marginheight="0"
            marginwidth="0" enableviewstate="false" />
    </span>
    <asp:Image ID="imgLoad" runat="server" Visible="false" EnableViewState="false" />
</div>
