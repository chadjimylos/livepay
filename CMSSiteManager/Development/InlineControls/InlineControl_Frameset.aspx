<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InlineControl_Frameset.aspx.cs" Inherits="CMSSiteManager_Development_InlineControls_InlineControl_Frameset" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server" enableviewstate="false">
    <title>Untitled Page</title>
</head>
	<frameset border="0" rows="102, *" id="rowsFrameset">
		<frame name="menu" src="InlineControl_Header.aspx?inlinecontrolid=<%=Request.QueryString["inlinecontrolid"]%> " scrolling="no" frameborder="0" noresize="noresize" />		
	    <frame name="content" src="InlineControl_General.aspx?inlinecontrolid=<%=Request.QueryString["inlinecontrolid"]%>&saved=<%=Request.QueryString["saved"]%> " frameborder="0" noresize="noresize" />
	<noframes>
		<body>
		 <p id="p1">
			This HTML frameset displays multiple Web pages. To view this frameset, use a 
			Web browser that supports HTML 4.0 and later.
		 </p>
		</body>
	</noframes>
</frameset>
</html>
