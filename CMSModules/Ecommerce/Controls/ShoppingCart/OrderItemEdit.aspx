<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrderItemEdit.aspx.cs" Inherits="CMSModules_Ecommerce_Controls_ShoppingCart_OrderItemEdit"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalDialogPage.master"
    Title="Shopping cart - Order item edit" %>

<%@ Register Src="~/CMSModules/Ecommerce/Controls/UI/PriceSelector.ascx" TagName="PriceSelector"
    TagPrefix="cms" %>
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">

    <script type="text/javascript">
        //<![CDATA[
        function CloseAndRefresh(url) {
            wopener.window.location.replace(url);
            window.close();
        }
        //]]>
    </script>

    <asp:Panel ID="pnlContent" runat="server" CssClass="PageContent">
        <asp:Label ID="lblError" runat="server" EnableViewState="false" Visible="false" CssClass="ErrorLabel" />
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblSKUName" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtSKUName" runat="server" CssClass="TextBoxField" MaxLength="450"
                        EnableViewState="false" /><br />
                    <asp:RequiredFieldValidator ID="rfvSKUName" runat="server" ControlToValidate="txtSKUName"
                        Display="Dynamic" EnableViewState="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSKUPrice" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <cms:PriceSelector ID="txtSKUPrice" runat="server" ValidatorOnNewLine="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSKUUnits" runat="server" EnableViewState="false" />
                </td>
                <td>
                    <asp:TextBox ID="txtSKUUnits" runat="server" CssClass="TextBoxField" MaxLength="9"
                        EnableViewState="false" /><br />
                    <asp:RequiredFieldValidator ID="rfvSKUUnits" runat="server" ControlToValidate="txtSKUUnits"
                        Display="Dynamic" EnableViewState="false" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
</asp:Content>
<asp:Content ID="plcFooter" runat="server" ContentPlaceHolderID="plcFooter" EnableViewState="false">
    <div class="FloatRight">
        <cms:CMSButton ID="btnOk" runat="server" OnClick="btnOk_Click" CssClass="SubmitButton"
            EnableViewState="false" />
    </div>
</asp:Content>
