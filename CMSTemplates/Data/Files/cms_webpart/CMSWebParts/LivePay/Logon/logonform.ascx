<%@ Control Language="C#" AutoEventWireup="true" CodeFile="~/CMSWebParts/LivePay/Logon/logonform.ascx.cs"
    Inherits="CMSWebParts_LivePay_Logon_logonform" %>

    <style>
        .DialogPosition{
	        margin-top:10px;
        }
        .LogonDialog{

        }
        .logon-form
        {
            font-size:11px;
            color:#fff;
            margin-top:10px;
        }
        .logon-form a
        {
            font-size:11px;
            color:#fff;
        }
        .logon-form-header{
	        background:transparent url(/App_Themes/LivePay/LoginForm/bgHeader.gif) no-repeat;
	        width:174px;
	        height:29px;
        }
        .logon-form-header .label
        {
            font-size:12px;
            font-weight:bold;
            padding:8px 10px 0px 10px;
        }
        .logon-form-body
        {
            background:transparent url(/App_Themes/LivePay/LoginForm/bgBody.gif) repeat-y;
	        width:174px;
	        padding-left:7px;
        }
        .logon-form-username
        {
            padding-top:10px;
        }
        .logon-form-username .label
        {
            float:left;
            width:52px;
            line-height:21px;
            font-weight:bold;
        }
        .logon-form-username .fullname
        {
            font-weight:bold;
        }
        .logon-form-password
        {
            padding-top:5px;
        }
        .logon-form-password .label
        {
            float:left;
            width:52px;
            line-height:21px;
            font-weight:bold;
        }
        .logon-form-links
        {
            padding-top:7px;
        }
        .logon-form-time
        {
            padding-top:7px;
            padding-bottom:7px;
        }
        .logon-form-time .label
        {
            float:left;
        }
        .logon-form-time .time
        {
            float:left;
            padding-left:5px;
        }
        .logon-form-rememberMe
        {
        }
        .logon-form-submit input
        {
            float:right;
            margin-right:9px;
            margin-top:7px;
            margin-bottom:7px;
            border:none;
            font-size:11px;
            color:#fff;
            font-weight:bold;
            background:transparent url(/App_Themes/LivePay/LoginForm/bgButton.gif) no-repeat;
            width:105px;
            height:23px;
            cursor:pointer;
        }
        .logon-form-logout
        {
            float:left;
            padding-left:27px;
            padding-top:10px;
        }
        .logon-form-logout input
        {
            background:transparent url(/App_Themes/LivePay/LoginForm/bgButton.gif) no-repeat;
            width:105px;
            height:23px;
            border:none;
            color:#fff;
            font-weight:bold;
            cursor:pointer;
        }
        .logon-form-bottom{
	        background:transparent url(/App_Themes/LivePay/LoginForm/bgBottom.gif) no-repeat;
	        width:174px;
	        height:6px;
        }
    </style>

    <script language=javascript>
        if (document.location.href.toLowerCase().indexOf('/cms/') == -1) {

            var lTotalSecondsToCountDown = 1200;
            var lCurrentSeconds = 0;

            function lCountDownToAutoRefresh() {
                var btnSignOut = $("input[id$=btnSignOut]")//document.getElementById('ctl00_ctl00_SiteContent_AppCntBody_btnRefreshData');
                if (lCurrentSeconds == lTotalSecondsToCountDown) {
                    btnSignOut.click();
                    lTotalSecondsToCountDown = 60;
                    lCurrentSeconds = 0;
                }
                else {
                    var i = document.getElementById('AutoRefreshCountDown');
                    var interv = lTotalSecondsToCountDown - lCurrentSeconds;
                    if (i) {
                        i.innerHTML = parseInt(interv / 60) + ':' + ((interv % 60).toString().length < 2 ? '0' : '') + (interv % 60)
                    }
                    lCurrentSeconds++;
                }

                setTimeout("lCountDownToAutoRefresh()", 1000);
            }

            window.onload = lCountDownToAutoRefresh;
        }
    </script>

<asp:Panel ID="Panel_Logout" runat="server" CssClass="LogoutPageBackground" Visible="false">
    <div class="logon-form">
        <div class="logon-form-header">
            <div class="label"><cms:LocalizedLabel ID="lblHeaderTitleLogout" runat="server" EnableViewState="false" /></div>
        </div>
        <div class="logon-form-body">
            <div class="logon-form-username">
                <div class="fullname"><asp:Label ID="lblFullName" runat="server" CssClass="CurrentUserLabel" EnableViewState="false" /></div>
            </div>
            <div class="logon-form-links">
                <asp:PlaceHolder ID="phOldLinks" runat="server" Visible="false">
                    <div class="label"><asp:LinkButton ID="lnkShort1" runat="server" EnableViewState="false" /></div>
                    <div class="label"><asp:LinkButton ID="lnkShort2" runat="server" EnableViewState="false" /></div>
                    <div class="label"><asp:LinkButton ID="lnkShort3" runat="server" EnableViewState="false" /></div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phUserLinks" runat="server" visible="false">
                    <div class="label"><a href="/Searchhistpayments.aspx">�������� ��������</a></div>
                    <div class="label"><a href="/ChangeUserDetails.aspx">�������� ������</a></div>
                    <div class="label"><a href="/SavedCards.aspx">������������� ������</a></div>
                    <div class="label"><a href="/SavedTransactions.aspx">���������� ����������</a></div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="phMerchantLinks" runat="server" Visible="false">
                    <div class="label"><a href="/Searchhistpayments.aspx">�������� ��������</a></div>
                    <div class="label"><a href="/merchants_srchistpayments.aspx">�������� ����������</a></div>
                    <div class="label"><a href="/SavedCards.aspx">������������� ������</a></div>
                </asp:PlaceHolder>
            </div>
            <div class="logon-form-time">
                <div class="label">���������� ��</div>
                <div class="time" id="AutoRefreshCountDown"></div>
            </div>
            <div class="logon-form-logout">
                <cms:CMSButton ID="btnSignOut" runat="server" OnClick="btnSignOut_Click" CssClass="signoutButton" Text="����������" EnableViewState="false" />
            </div>
        <div class="Clear"></div>
        </div>
        <div class="logon-form-bottom"></div>
    </div>
</asp:Panel>

<asp:Panel ID="Panel1" runat="server" CssClass="LogonPageBackground">
    <div class="logon-form">
        <div class="logon-form-header">
            <div class="label"><cms:LocalizedLabel ID="lblHeaderTitle" runat="server" EnableViewState="false" /></div>
        </div>
        <div class="logon-form-body">
                <asp:Login ID="Login1" runat="server" DestinationPageUrl="~/Default.aspx">
                    <LayoutTemplate>
                        <asp:Panel runat="server" ID="pnlLogin" DefaultButton="LoginButton" Width="167">
                        <div class="logon-form-username">
                            <div class="label">
                                <cms:LocalizedLabel ID="lblUserName" runat="server" AssociatedControlID="UserName"
                                    EnableViewState="false" />
                            </div>
                            <div class="textbox">
                                <asp:TextBox ID="UserName" runat="server" MaxLength="100" Width="100" CssClass="LogonTextBox" />
                                <asp:RequiredFieldValidator ID="rfvUserNameRequired" runat="server" ControlToValidate="UserName"
                                    ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1"
                                    EnableViewState="false">*</asp:RequiredFieldValidator>                           
                            </div>
                        </div>                        
                        <div class="logon-form-password">
                            <div class="label">
                                <cms:LocalizedLabel ID="lblPassword" runat="server" AssociatedControlID="Password"
                                    EnableViewState="false" />
                            </div>
                            <div class="textbox">
                                <asp:TextBox ID="Password" runat="server" TextMode="Password" MaxLength="110" Width="100" CssClass="LogonTextBox" />                            
                            </div>
                        </div>                                              

                        <div style="color: red"><cms:LocalizedLiteral ID="FailureText" runat="server" EnableViewState="False" /></div>
                                         

                        <div class="logon-form-submit">
                            <cms:LocalizedButton ID="LoginButton" runat="server" CommandName="Login" ValidationGroup="Login1"
                                EnableViewState="false" />
                        </div>
                                           

                        </asp:Panel>
                    </LayoutTemplate>
                </asp:Login>
        <div>
                <asp:LinkButton ID="lnkPasswdRetrieval" runat="server" EnableViewState="false" />
        </div>
        <div><asp:LinkButton ID="lnkMember" runat="server" EnableViewState="false" /></div>

        </div><!-- Class End Body -->
        <div class="logon-form-bottom"></div>

    </div>
</asp:Panel>
<asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
<asp:HiddenField runat="server" ID="hdnPasswDisplayed" />