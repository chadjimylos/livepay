<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategoryEdit.ascx.cs"
    Inherits="CMSAdminControls_Categories_CategoryEdit" %>
<asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
    Visible="false" />
<asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
    Visible="false" />
<table>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblDisplayName" runat="server" CssClass="FieldLabel" ResourceString="general.displayname"
                DisplayColon="true" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtDisplayName" runat="server" CssClass="TextBoxField" MaxLength="250" EnableViewState="false" />
            <asp:RequiredFieldValidator ID="rfvDisplayName" runat="server" ControlToValidate="txtDisplayName"
                Display="Dynamic" ValidationGroup="valGroupCategories" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblCodeName" runat="server" CssClass="FieldLabel" ResourceString="general.codename"
                DisplayColon="true" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtCodeName" runat="server" CssClass="TextBoxField" MaxLength="250" />
            <asp:RequiredFieldValidator ID="rfvCodeName" runat="server" ControlToValidate="txtCodeName"
                Display="Dynamic" ValidationGroup="valGroupCategories" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td style="vertical-align: top">
            <cms:LocalizedLabel ID="lblDescription" runat="server" CssClass="FieldLabel" ResourceString="general.description"
                DisplayColon="true" EnableViewState="false" />
        </td>
        <td>
            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="TextAreaField" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            <cms:LocalizedLabel ID="lblEnabled" runat="server" CssClass="FieldLabel" ResourceString="general.enabled"
                DisplayColon="true" EnableViewState="false" />
        </td>
        <td>
            <asp:CheckBox ID="chkEnabled" runat="server" Checked="true" EnableViewState="false" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            <cms:CMSButton ID="btnSaveCategory" runat="server" CssClass="SubmitButton" OnClick="btnSaveCategory_Click"
                ValidationGroup="valGroupCategories" EnableViewState="false" />
        </td>
    </tr>
</table>
