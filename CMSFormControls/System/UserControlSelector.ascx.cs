﻿using System;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using CMS.FormControls;
using CMS.GlobalHelper;
using CMS.ExtendedControls.Dialogs;
using CMS.ExtendedControls;

public partial class CMSFormControls_System_UserControlSelector : FormEngineUserControl, ICallbackEventHandler
{
    #region "Variables"

    private FileSystemDialogConfiguration mDialogConfig = null;
    private bool mAllowEmptyValue = true;
    private string mSelectedPathPrefix = String.Empty;
    private bool mEnabled = true;
    /// <summary>
    /// Hidden value field
    /// </summary>
    protected HiddenField mHiddenUrl = null;

    #endregion


    #region "Properties"

    /// <summary>
    /// Selector value: path of the file or folder.
    /// </summary>
    public override object Value
    {
        get
        {
            return this.txtPath.Text;
        }
        set
        {
            if (value != null)
            {
                this.txtPath.Text = value.ToString();
            }
            else
            {
                this.txtPath.Text = String.Empty;
            }
        }
    }


    /// <summary>
    /// Configuration of the dialog for inserting Images.
    /// </summary>
    public FileSystemDialogConfiguration DialogConfig
    {
        get
        {
            if (mDialogConfig == null)
            {
                mDialogConfig = new FileSystemDialogConfiguration();
            }
            return mDialogConfig;
        }
        set
        {
            mDialogConfig = value;
        }
    }


    /// <summary>
    /// Gets or sets if value of form control could be empty.
    /// </summary>
    public bool AllowEmptyValue
    {
        get
        {
            return this.mAllowEmptyValue;
        }
        set
        {
            this.mAllowEmptyValue = value;
        }
    }


    /// <summary>
    /// Gets or sets prefix for paths with preselected source folder(webparts,form controls,...)
    /// </summary>
    public string SelectedPathPrefix
    {
        get
        {
            return mSelectedPathPrefix;
        }
        set
        {
            mSelectedPathPrefix = value;
        }
    }


    /// <summary>
    /// Validates the return value of form control
    /// </summary>
    public override bool IsValid()
    {
        if (!AllowEmptyValue)
        {
            if (String.IsNullOrEmpty(txtPath.Text.Trim()))
            {
                this.ValidationError = this.FieldInfo.ValidationErrorMessage;
                return false;
            }
        }
        return true;
    }


    /// <summary>
    /// Gets or sets if value can be changed
    /// </summary>
    public override bool Enabled
    {
        get
        {
            return mEnabled;
        }
        set
        {
            mEnabled = value;
            this.txtPath.Enabled = value;
            this.btnSelect.Enabled = value;
            this.btnClear.Enabled = value;
            this.btnSelect.Enabled = value;
            this.btnClear.Enabled = value;
        }
    }

    #endregion


    #region "Control methods"

    /// <summary>
    /// Init event
    /// </summary>
    /// <param name="sender">Sender parameter</param>
    /// <param name="e">Arguments</param>
    protected void Page_Init(object sender, EventArgs e)
    {
        CreateChildControls();
    }


    /// <summary>
    /// Page load.
    /// </summary>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!StopProcessing)
        {
            SetupControls();
        }
    }


    /// <summary>
    /// Ensure creation of controls
    /// </summary>
    protected override void CreateChildControls()
    {
        base.CreateChildControls();

        if (mHiddenUrl == null)
        {
            mHiddenUrl = new HiddenField();
            mHiddenUrl.ID = "hidUrl";

            this.Controls.Add(mHiddenUrl);
        }

    }


    /// <summary>
    /// Setup all contained controls
    /// </summary>
    private void SetupControls()
    {
        ScriptHelper.RegisterJQuery(this.Page);
        this.btnSelect.Text = ResHelper.GetString("General.select");
        this.btnClear.Text = ResHelper.GetString("General.clear");

        if (Enabled)
        {
            // Configure FileSystem dialog
            string width = this.DialogConfig.DialogWidth.ToString();
            string height = this.DialogConfig.DialogHeight.ToString();
            if (this.DialogConfig.UseRelativeDimensions)
            {
                width += "%";
                height += "%";
            }

            this.DialogConfig.EditorClientID = txtPath.ClientID;
            this.DialogConfig.SelectedPath = txtPath.Text;

            string url = GetDialogURL(this.DialogConfig, this.SelectedPathPrefix);

            // Register the dialog script
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(string), ScriptHelper.DIALOG_SCRIPT_KEY, ScriptHelper.DialogScript);

            // Register the Path related javascript functions
            this.Page.ClientScript.RegisterClientScriptBlock(typeof(string), "FileSystemSelector_" + this.ClientID, ScriptHelper.GetScript(
                "function UpdateModalDialogURL_" + this.ClientID + "(newValue,context){ var item = document.getElementById(context + '_hidUrl'); if(item!=null && item.value!=null){item.value = newValue;}}\n" +
                "function SetMediaValue_" + this.ClientID + "(selectorId){if(window.Changed){Changed();} var newValue = document.getElementById(selectorId + '_txtPath').value;" + this.Page.ClientScript.GetCallbackEventReference(this, "newValue", "UpdateModalDialogURL_" + this.ClientID, "selectorId") + ";} \n" +
                "function ClearSelection_" + this.ClientID + "(selectorId){ if(window.Changed){Changed();}document.getElementById(selectorId + '_txtPath').value=''; var newValue='';" + this.Page.ClientScript.GetCallbackEventReference(this, "newValue", "UpdateModalDialogURL_" + this.ClientID, "selectorId") + "; }"));

            // Setup buttons
            this.txtPath.Attributes.Add("onchange", "SetMediaValue_" + this.ClientID + "('" + this.ClientID + "');");

            this.Page.ClientScript.RegisterStartupScript(typeof(string), this.DialogConfig.EditorClientID + "script", ScriptHelper.GetScript(
                "var hidField_" + this.ClientID + " = document.getElementById('" + this.ClientID + "' + '_hidUrl'); if(hidField_" + this.ClientID + ") {hidField_" + this.ClientID + ".value='" + url + "';}"));
            this.btnSelect.Attributes.Add("onclick", "var url_" + this.ClientID + " = document.getElementById('" + this.ClientID + "' + '_hidUrl').value; modalDialog(url_" + this.ClientID + ", 'Select file', '" + width + "', '" + height + "', null); return false;");
            this.btnClear.Attributes.Add("onclick", "ClearSelection_" + this.ClientID + "('" + this.ClientID + "'); return false;");
        }
    }

    #endregion


    #region "Helper methods"

    /// <summary>
    /// Returns query string which will be passed to the CMS dialogs (Insert image or media/Insert link).
    /// </summary>
    /// <param name="config">Dialog configuration.</param>  
    /// <param name="selectedPathPrefix">Path prefix of selected value</param>
    public string GetDialogURL(FileSystemDialogConfiguration config, string selectedPathPrefix)
    {
        StringBuilder builder = new StringBuilder();

        // Set constraints
        // Allowed files extensions            
        if (!String.IsNullOrEmpty(config.AllowedExtensions))
        {
            builder.Append("&allowed_extensions=" + Server.UrlEncode(config.AllowedExtensions));
        }

        // Excluded extensions
        if (!String.IsNullOrEmpty(config.ExcludedExtensions))
        {
            builder.Append("&excluded_extensions=" + Server.UrlEncode(config.ExcludedExtensions));
        }

        // Allowed folders 
        if (!String.IsNullOrEmpty(config.AllowedFolders))
        {
            builder.Append("&allowed_folders=" + Server.UrlEncode(config.AllowedFolders));
        }

        // Excluded folders
        if (!String.IsNullOrEmpty(config.ExcludedFolders))
        {
            builder.Append("&excluded_folders=" + Server.UrlEncode(config.ExcludedFolders));
        }

        // Default path-preselected path in filesystem tree
        if (!String.IsNullOrEmpty(config.DefaultPath))
        {
            builder.Append("&default_path=" + Server.UrlEncode(config.DefaultPath));
        }

        // SelectedPath - actual value of textbox
        if (!String.IsNullOrEmpty(config.SelectedPath))
        {
            string selectedPath = config.SelectedPath;
            if ((!selectedPath.StartsWith("~")) && ((String.IsNullOrEmpty(config.StartingPath)) || (config.StartingPath.StartsWith("~"))) && (!String.IsNullOrEmpty(selectedPathPrefix)))
            {
                selectedPath = selectedPathPrefix.TrimEnd('/') + "/" + selectedPath.TrimStart('/');
            }
            builder.Append("&selected_path=" + Server.UrlEncode(selectedPath));
        }

        // Starting path in filesystem
        if (!String.IsNullOrEmpty(config.StartingPath))
        {
            builder.Append("&starting_path=" + Server.UrlEncode(config.StartingPath));
        }

        // Show only folders|files
        builder.Append("&show_folders=" + Server.UrlEncode(config.ShowFolders.ToString()));

        // Editor client id
        if (!String.IsNullOrEmpty(config.EditorClientID))
        {
            builder.Append("&editor_clientid=" + Server.UrlEncode(config.EditorClientID));
        }

        // Get hash for complete query string
        string query = HttpUtility.UrlPathEncode("?" + builder.ToString().TrimStart('&'));
        string hash = QueryHelper.GetHash(query);

        // Get complete query string with attached hash
        string queryString = HttpUtility.UrlPathEncode(UrlHelper.EncodeQueryString("?" + builder.Append("&hash=" + hash).ToString().TrimStart('&')));

        string baseUrl = "~/CMSFormControls/Selectors/";

        // Get complet URL
        return UrlHelper.GetAbsoluteUrl(baseUrl + "SelectFileOrFolder/Default.aspx" + queryString);

    }

    #endregion


    #region "Callback handling"

    /// <summary>
    /// Raises the callback event
    /// </summary>
    public void RaiseCallbackEvent(string eventArgument)
    {
        //LoadDisplayValues(eventArgument);
        // Configure Image dialog
        string width = this.DialogConfig.DialogWidth.ToString();
        string height = this.DialogConfig.DialogHeight.ToString();
        if (this.DialogConfig.UseRelativeDimensions)
        {
            width += "%";
            height += "%";
        }
        this.DialogConfig.SelectedPath = eventArgument;
        string url = GetDialogURL(this.DialogConfig, this.SelectedPathPrefix);
        this.mHiddenUrl.Value = url;

    }


    /// <summary>
    /// Prepares the callback result
    /// </summary>
    public string GetCallbackResult()
    {
        return this.mHiddenUrl.Value;
    }

    #endregion
}
