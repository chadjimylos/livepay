// Register script displaying safe modal page dialog
function linkModalDialog(url, name, width, height, otherParams) {
    win = window;
    var dHeight = height;
    var dWidth = width;
    if (width.toString().indexOf('%') != -1) {
        dWidth = Math.round(screen.width * parseInt(width, 10) / 100);
    }
    if (height.toString().indexOf('%') != -1) {
        dHeight = Math.round(screen.height * parseInt(height, 10) / 100);
    }
    if ((document.all) && (navigator.appName != 'Opera')) {
        if (otherParams == undefined) {
            otherParams = 'resizable:yes;scroll:no';
        }
        var vopenwork = true;
        try {
            win = wopener.window;
        }
        catch (e) {
            vopenwork = false;
        }

        if (parseInt(navigator.appVersion.substr(22, 1)) < 7) {
            dWidth += 4;
            dHeight += 58;
        }

        try {
            dialog = win.showModalDialog(url, this, 'dialogWidth:' + dWidth + 'px;dialogHeight:' + dHeight + 'px;' + otherParams);
        }
        catch (e) {
            if (vopenwork) {
                window.showModalDialog(url, this, 'dialogWidth:' + dWidth + 'px;dialogHeight:' + dHeight + 'px;' + otherParams);
            }
        }
    }
    else {
        if (otherParams == undefined) {
            otherParams = 'toolbar=no,directories=no,menubar=no,modal=yes,dependent=yes,resizable=yes';
        }
        oWindow = win.open(url, name, 'width=' + dWidth + ',height=' + dHeight + ',' + otherParams);
        oWindow.opener = this;
        oWindow.focus();
    }
}


// Create and define new FCKEditor button command openning the popup window
var InsertLinkCommand = function() {
}

InsertLinkCommand.GetState = function() {
    if (FCK.EditMode != FCK_EDITMODE_WYSIWYG)
        return FCK_TRISTATE_DISABLED;
    else {
        return FCK_TRISTATE_OFF;
    }
}

InsertLinkCommand.Execute = function() {

    window.FCK_ACTIVE = window.FCK.Name;

    var url = FCKConfig["LinkDialogURL"];
    var width = FCKConfig["LinkDialogWidth"];
    var height = FCKConfig["LinkDialogHeight"];

    linkModalDialog(FCKPlugins.Items['InsertLink'].Path + url, FCKLang.InsertLinkTitle, width, height, null);
}


// Register the link command.
FCKCommands.RegisterCommand('InsertLink', InsertLinkCommand);


var oInsertLink = new FCKToolbarButton('InsertLink', FCKLang.InsertLinkButton);
oInsertLink.IconPath = FCKPlugins.Items['InsertLink'].Path + 'fckLink.gif';
FCKToolbarItems.RegisterItem('InsertLink', oInsertLink);

FCK.ContextMenu.RegisterListener({
    AddItems: function(contextMenu, tag, tagName) {
        if (tagName == 'A' || FCKSelection.HasAncestorNode('A')) {
            contextMenu.RemoveAllItems();
            contextMenu.AddItem('Cut', FCKLang.Cut, 7, FCKCommands.GetCommand('Cut').GetState() == FCK_TRISTATE_DISABLED);
            contextMenu.AddItem('Copy', FCKLang.Copy, 8, FCKCommands.GetCommand('Copy').GetState() == FCK_TRISTATE_DISABLED);
            contextMenu.AddItem('Paste', FCKLang.Paste, 9, FCKCommands.GetCommand('Paste').GetState() == FCK_TRISTATE_DISABLED);
            contextMenu.AddSeparator();
            contextMenu.AddItem('VisitLink', FCKLang.VisitLink);
            contextMenu.AddItem('Unlink', FCKLang.RemoveLink, 35);
            contextMenu.AddSeparator();
            contextMenu.AddItem('InsertLink', FCKLang.InsertLinkEdit, FCKPlugins.Items['InsertLink'].Path + 'fckLink.gif');
            var selElem = FCKSelection.GetSelectedElement()
            if (((selElem != null) && (selElem.nodeName.toLowerCase() == 'img')) &&
			((!tag.getAttribute('_fckfakelement')) ||
			((tag.getAttribute('_fckmedia')) && (!tag.getAttribute('_fckyoutube'))))) {
                contextMenu.AddItem('InsertImageOrMedia', FCKLang.InsertMediaOrImageEdit, FCKPlugins.Items['InsertImageOrMedia'].Path + 'fckImageOrMedia.gif');
            }
        }
    }
});

//==================================================================================================
//                                     FCK Link object
//==================================================================================================

var FCKLink = new Object();

FCKLink.Add = function(obj) {
    if ((obj.link_url) && (obj.link_url != '')) {
        // Save undo step
        FCKUndo.SaveUndoStep();
        FCKLink.InsertUrl(obj);
    }
    if ((obj.email_to) && (obj.email_to != '')) {
        // Save undo step
        FCKUndo.SaveUndoStep();
        FCKLink.InsertEmail(obj);
    }
    if ((obj.anchor_name) && (obj.anchor_name != '')) {
        // Save undo step
        FCKUndo.SaveUndoStep();
        FCKLink.InsertAnchor(obj);
    }
}

FCKLink.InsertUrl = function(obj) {
    var sUrl = (obj.link_protocol ? obj.link_protocol : '') + obj.link_url;
    var sInnerHtml;

    var oLink = FCK.Selection.MoveToAncestorNode('A');
    if (oLink)
        FCK.Selection.SelectNode(oLink);

    var aLinks = oLink ? [oLink] : FCK.CreateLink(sUrl, true);

    var aHasSelection = (aLinks.length > 0);
    if (!aHasSelection) {
        // Create a new (empty) anchor.
        aLinks = [FCK.InsertElement('a')];
        if ((obj.link_text != null) && (obj.link_text != '')) {
            sInnerHtml = FCKLink.EscapeText(obj.link_text);
        }
        else {
            sInnerHtml = FCKLink.EscapeText(obj.link_url);
        }
    }

    for (var i = 0; i < aLinks.length; i++) {
        oLink = aLinks[i];

        if (aHasSelection)
            sInnerHtml = oLink.innerHTML;
        oLink.href = sUrl;
        oLink.setAttribute('_fcksavedurl', sUrl);
        oLink.innerHTML = sInnerHtml;

        // Advances Attributes
        if ((obj.link_target) && (obj.link_target != '')) {
            oLink.setAttribute('target', obj.link_target);
        }
        else {
            oLink.removeAttribute('target');
        }
        if ((obj.link_id) && (obj.link_id != '')) {
            oLink.setAttribute('id', obj.link_id);
        }
        else {
            oLink.removeAttribute('id');
        }
        if ((obj.link_name) && (obj.link_name != '')) {
            oLink.setAttribute('name', obj.link_name);
        }
        else {
            oLink.removeAttribute('name');
        }
        if ((obj.link_class) && (obj.link_class != '')) {
            oLink.setAttribute('class', obj.link_class);
        }
        else {
            oLink.removeAttribute('class');
        }
        if ((obj.link_tooltip) && (obj.link_tooltip != '')) {
            oLink.setAttribute('title', obj.link_tooltip);
        }
        else {
            oLink.removeAttribute('title');
        }
        if ((obj.link_style) && (obj.link_style != '')) {
            oLink.setAttribute('style', obj.link_style);
            oLink.style.cssText = obj.link_style;
        }
        else {
            oLink.removeAttribute('style');
        }
    }
}

FCKLink.InsertAnchor = function(obj) {
    var sUrl = '#' + obj.anchor_name;
    var sInnerHtml;

    var oLink = FCK.Selection.MoveToAncestorNode('A');
    if (oLink)
        FCK.Selection.SelectNode(oLink);

    var aLinks = oLink ? [oLink] : FCK.CreateLink(sUrl, true);

    var aHasSelection = (aLinks.length > 0);
    if (!aHasSelection) {
        // Create a new (empty) anchor.
        aLinks = [FCK.InsertElement('a')];
        if ((obj.anchor_linktext != null) && (obj.anchor_linktext != '')) {
            sInnerHtml = FCKLink.EscapeText(obj.anchor_linktext);
        }
        else {
            sInnerHtml = FCKLink.EscapeText(obj.anchor_name);
        }
    }

    for (var i = 0; i < aLinks.length; i++) {
        oLink = aLinks[i];

        if (aHasSelection)
            sInnerHtml = oLink.innerHTML;
        oLink.href = sUrl;
        oLink.setAttribute('_fcksavedurl', sUrl);
        oLink.innerHTML = sInnerHtml;
    }
}

FCKLink.InsertEmail = function(obj) {
    var sInnerHtml;
    var sUrl = 'mailto:';
    // Create mailto href
    sUrl += obj.email_to;
    if ((obj.email_cc) || (obj.email_bcc) || (obj.email_subject) || (obj.email_body)) {
        sUrl += '?';
    }
    if (obj.email_cc) {
        sUrl += 'cc=' + obj.email_cc + '&';
    }
    if (obj.email_bcc) {
        sUrl += 'bcc=' + obj.email_bcc + '&';
    }
    if (obj.email_subject) {
        sUrl += 'subject=' + FCKLink.EncodeURL(obj.email_subject) + '&';
    }
    if (obj.email_body) {
        sUrl += 'body=' + FCKLink.EncodeURL(obj.email_body) + '&';
    }
    sUrl = sUrl.replace(/(&)$/, '');

    var oLink = FCK.Selection.MoveToAncestorNode('A');
    if (oLink)
        FCK.Selection.SelectNode(oLink);

    var aLinks = oLink ? [oLink] : FCK.CreateLink(sUrl, true);

    var aHasSelection = (aLinks.length > 0);
    if (!aHasSelection) {
        // Create a new (empty) anchor.
        aLinks = [FCK.InsertElement('a')];
        if ((obj.email_linktext != null) && (obj.email_linktext != '')) {
            sInnerHtml = FCKLink.EscapeText(obj.email_linktext);
        }
        else {
            sInnerHtml = FCKLink.EscapeText(obj.email_to);
        }
    }

    for (var i = 0; i < aLinks.length; i++) {
        oLink = aLinks[i];

        if (aHasSelection)
            sInnerHtml = oLink.innerHTML;
        oLink.href = sUrl;
        oLink.setAttribute('_fcksavedurl', sUrl);
        oLink.innerHTML = sInnerHtml;
    }
}

FCKLink.EncodeURL = function(text) {
    text = text.replace(/%/g, '%25');
    text = decodeURIComponent(text);
    return encodeURIComponent(text)
}

FCKLink.EscapeText = function(text) {
    return text.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\"/g, '&quot;');
}
