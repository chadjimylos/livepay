using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.PortalControls;
using CMS.GlobalHelper;
using CMS.CMSHelper;

public partial class CMSWebParts_Media_Video : CMSAbstractWebPart
{
    #region "Video properties"

    /// <summary>
    /// Gets or sets the value that indicates whether the vide is automatically activated
    /// </summary>
    public bool AutoActivation
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("AutoActivation"), false);
        }
        set
        {
            this.SetValue("AutoActivation", value);
        }
    }


    /// <summary>
    /// Gets or sets the URL of video to be displayed.
    /// </summary>
    public string VideoURL
    {
        get
        {
            return ValidationHelper.GetString(this.GetValue("VideoURL"), "");
        }
        set
        {
            this.SetValue("VideoURL", value);
        }
    }


    /// <summary>
    /// Gets or sets the width of video.
    /// </summary>
    public int Width
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("Width"), 400);
        }
        set
        {
            this.SetValue("Width", value);
        }
    }


    /// <summary>
    /// Gets or sets the height of video.
    /// </summary>
    public int Height
    {
        get
        {
            return ValidationHelper.GetInteger(this.GetValue("Height"), 300);
        }
        set
        {
            this.SetValue("Height", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether video is automatically started
    /// </summary>
    public bool Autostart
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("Autostart"), false);
        }
        set
        {
            this.SetValue("Autostart", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether video controller is displayed
    /// </summary>
    public bool ShowControls
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("ShowControls"), true);
        }
        set
        {
            this.SetValue("ShowControls", value);
        }
    }


    /// <summary>
    /// Gets or sets the value that indicates whether video after the end is automatically started again
    /// </summary>
    public bool Loop
    {
        get
        {
            return ValidationHelper.GetBoolean(this.GetValue("Loop"), false);
        }
        set
        {
            this.SetValue("Loop", value);
        }
    }

    #endregion

    /// <summary>
    /// Content loaded event handler
    /// </summary>
    public override void OnContentLoaded()
    {
        base.OnContentLoaded();
        SetupControl();
    }


    /// <summary>
    /// Reloads data
    /// </summary>
    public override void ReloadData()
    {
        base.ReloadData();
        SetupControl();
    }


    /// <summary>
    /// Initializes the control properties
    /// </summary>
    protected void SetupControl()
    {
        if (this.StopProcessing)
        {
            // Do nothing
        }
        else
        {
            //FF hack (must be 1 or 0 not true or false)
            int showControls = (this.ShowControls) ? 1 : 0;
            int autoStart = (this.Autostart) ? 1 : 0;
            int loop = (this.Loop) ? 1 : 0;

            // Auto activation hack
            if (this.AutoActivation)
            {
                ltlPlaceholder.Text = "<div class=\"VideoLikeContent\" id=\"VideoPlaceholder_" + ltlScript.ClientID + "\" ></div>";

                // Get external script function
                ltlScript.Text = "<script src=\"" + ResolveUrl("~/CMSWebParts/Media/Video_files/video.js") + "\" type=\"text/javascript\"></script>";
                // Call function for video object insertion
                ltlScript.Text += ScriptHelper.GetScript("LoadVideo('VideoPlaceholder_" + ltlScript.ClientID + "', '" + HttpContext.Current.Server.HtmlEncode(this.VideoURL) + "', " + this.Width + ", " + this.Height + ", '" + showControls + "', '" + autoStart + "', '" + loop + "', '" + ResHelper.GetString("Media.NotSupported") + "');");
            }
            else
            {
                StringBuilder builder = new StringBuilder(512);

                builder.Append("<div class=\"VideoLikeContent\" style=\"position:relative;\" >");
                builder.Append("<object id=\"" + this.ClientID + "\" classid=\"CLSID:22D6f312-B0F6-11D0-94AB-0080C74C7E95\" width=\"" + this.Width + "\" height=\"" + this.Height + "\" type=\"video/x-ms-wmv\" standby=\"Loading Windows Media Player components...\" codebase=\"http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=6,4,7,1112\" >");
                builder.Append("<param name=\"filename\" value=\"" + HttpContext.Current.Server.HtmlEncode(this.VideoURL) + "\" />");
                builder.Append("<param name=\"src\" value=\"" + HttpContext.Current.Server.HtmlEncode(this.VideoURL) + "\" />");
                builder.Append("<param name=\"animationatStart\" value=\"1\" />\n");
                builder.Append("<param name=\"windowlessvideo\" value=\"1\" />\n");
                builder.Append("<param name=\"wmode\" value=\"transparent\" />\n");
                builder.Append("<param name=\"transparentatStart\" value=\"1\" />\n");
                builder.Append("<param name=\"autostart\" value=\"" + autoStart + "\" />\n");
                builder.Append("<param name=\"showControls\" value=\"" + showControls + "\" />\n");
                builder.Append("<param name=\"loop\" value=\"" + loop + "\" />\n");
                if (!CMSContext.CurrentBodyClass.Contains("IE") && !CMSContext.CurrentBodyClass.Contains("Safari"))
                {
                    builder.Append("<object type=\"application/x-mplayer2\" src=\"" + HttpContext.Current.Server.HtmlEncode(this.VideoURL) + "\" name=\"" + this.ClientID + "\" width=\"" + this.Width + "\" height=\"" + this.Height + "\" autostart=\"" + autoStart + "\"  wmode=\"transparent\">\n");
                    builder.Append(ResHelper.GetString("Media.NotSupported") + "\n");
                    builder.Append("</object>\n");
                }
                builder.Append("</object></div>");

                this.ltlPlaceholder.Text = builder.ToString();
            }
        }
    }
}
