﻿<%@ Page Title="" Language="C#" Theme="Default" MasterPageFile="~/CMSMasterPages/UI/Dialogs/ModalSimplePage.master"
    AutoEventWireup="true" CodeFile="Content.aspx.cs" EnableEventValidation="false"
    Inherits="CMSFormControls_Selectors_InsertYouTubeVideo_Content" %>


<%@ Register Src="~/CMSModules/Content/Controls/Dialogs/YouTube/YouTubeProperties.ascx"
    TagName="YouTubeProperties" TagPrefix="cms" %>
<asp:Content ID="cntControls" ContentPlaceHolderID="plcControls" runat="Server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width: 100%">
                &nbsp;
            </td>
            <td style="white-space: nowrap;">
                <asp:HyperLink ID="lnkYouTube" runat="server" NavigateUrl="http://www.youtube.com"
                    Target="_blank" />
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="plcContent" runat="Server">
    <div class="PageContent">
        <asp:ScriptManager runat="server" ID="scriptManager" />
        <cms:YouTubeProperties ID="youTubeProp" runat="server" />
    </div>
</asp:Content>
