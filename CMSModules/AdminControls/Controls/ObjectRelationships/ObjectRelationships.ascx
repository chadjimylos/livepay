<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ObjectRelationships.ascx.cs"
    Inherits="CMSModules_AdminControls_Controls_ObjectRelationships_ObjectRelationships" %>
<%@ Register Src="~/CMSAdminControls/UI/UniGrid/UniGrid.ascx" TagName="UniGrid" TagPrefix="cms" %>
<%@ Register Src="~/CMSAdminControls/UI/UniSelector/UniSelector.ascx" TagName="UniSelector"
    TagPrefix="cms" %>

<script type="text/javascript">
    //<![CDATA[
    var currentOnLeft = true;

    function Show(id, show) {
        document.getElementById(id).style.display = (show ? 'block' : 'none');
    }

    function SetVal(id, value) {
        document.getElementById(id).value = value;
    }

    function SwitchSides() {
        currentOnLeft = (!currentOnLeft);
        if (currentOnLeft) {
            Show('leftCurrentObj', true);
            Show('leftSelectedObj', false);
            Show('rightCurrentObj', false);
            Show('rightSelectedObj', true);
            SetVal('currentOnLeft', 'true');
            SwitchSelectedValue(false);
        }
        else {
            Show('leftCurrentObj', false);
            Show('leftSelectedObj', true);
            Show('rightCurrentObj', true);
            Show('rightSelectedObj', false);
            SetVal('currentOnLeft', 'false');
            SwitchSelectedValue(true);
        }
        return false;
    }

    // Delete item action
    function SetItems(lObj, ltype, relname, rObj, rtype) {
        SetVal('leftObjId', lObj);
        SetVal('leftObjType', ltype);
        SetVal('relationshipId', relname);
        SetVal('rightObjId', rObj);
        SetVal('rightObjType', rtype);
    }
    //]]>
</script>

<asp:Panel ID="pnlHeader" runat="server" Visible="true">
    <asp:Panel runat="server" ID="pnlSelection" CssClass="PageHeaderLine">
        <table>
            <asp:PlaceHolder runat="server" ID="pnlTypes">
                <tr>
                    <td>
                        <cms:LocalizedLabel runat="server" ID="lblObjType" EnableViewState="false" ResourceString="ObjectRelationships.RelatedObjType" />
                    </td>
                    <td>
                        <asp:DropDownList ID="drpRelatedObjType" CssClass="DropDownField" runat="server"
                            AutoPostBack="True" OnSelectedIndexChanged="drpRelatedObjType_SelectedIndexChanged" />
                    </td>
                </tr>
            </asp:PlaceHolder>
            <asp:PlaceHolder runat="server" ID="pnlSite" Visible="false">
                <tr>
                    <td>
                        <cms:LocalizedLabel runat="server" ID="lblSite" EnableViewState="false" ResourceString="ObjectRelationships.RelatedObjectSite" />
                    </td>
                    <td>
                        <cms:UniSelector ID="siteSelector" runat="server" ObjectType="cms.site" ResourcePrefix="siteselect"
                            AllowEmpty="false" AllowAll="false" />
                    </td>
                </tr>
            </asp:PlaceHolder>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlNew" CssClass="PageHeaderLine">
        <asp:Image ID="imgNewRelationship" runat="server" CssClass="NewItemImage" />
        <asp:LinkButton ID="lnkNewRelationship" runat="server" OnClick="lnkNewRelationship_Click">LinkButton</asp:LinkButton>
    </asp:Panel>
</asp:Panel>
<asp:Panel runat="server" ID="pnlContent" CssClass="PageContent">
    <input type="hidden" id="leftObjId" name="leftObjId" />
    <input type="hidden" id="leftObjType" name="leftObjType" />
    <input type="hidden" id="relationshipId" name="relationshipId" />
    <input type="hidden" id="rightObjId" name="rightObjId" />
    <input type="hidden" id="rightObjType" name="rightObjType" />
    <asp:Label runat="server" ID="lblInfo" CssClass="InfoLabel" EnableViewState="false"
        Visible="false" />
    <asp:Label runat="server" ID="lblError" CssClass="ErrorLabel" EnableViewState="false"
        Visible="false" />
    <asp:Panel ID="pnlEditList" runat="server">
        <cms:UniGrid ID="gridExistingRelationships" runat="server" GridName="ObjectRelationships.xml"
            IsLiveSite="false" DelayedReload="true" />
    </asp:Panel>
    <asp:Panel ID="pnlAddNew" runat="server" Visible="false">
        <cms:CMSUpdatePanel runat="server" ID="pnlUpdate" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Table ID="TableRelationship" runat="server" CssClass="UniGridGrid" CellPadding="5"
                    CellSpacing="0" Width="100%">
                    <asp:TableHeaderRow CssClass="UniGridHead">
                        <asp:TableHeaderCell ID="leftCell" HorizontalAlign="Left" />
                        <asp:TableHeaderCell ID="middleCell" HorizontalAlign="Center" />
                        <asp:TableHeaderCell ID="rightCell" HorizontalAlign="Right" />
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left" Width="40%" VerticalAlign="Top">
                            <div id="leftCurrentObj" style="display: block; padding: 3px;">
                                <asp:Label ID="lblLeftObj" runat="server" />
                            </div>
                            <div id="leftSelectedObj" style="display: none">
                                <cms:UniSelector ID="selLeftObj" runat="server" SelectionMode="SingleDropDownList"
                                    AllowEmpty="false" IsLiveSite="false" />
                            </div>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="center" Width="20%">
                            <asp:DropDownList ID="drpRelationship" runat="server" CssClass="DropDownField" />
                            <div style="padding-top: 5px">
                                <cms:CMSButton ID="btnSwitchSides" runat="server" OnClientClick="SwitchSides();return false;"
                                    CssClass="LongButton" />
                            </div>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="right" Width="40%" VerticalAlign="Top">
                            <div id="rightCurrentObj" style="display: none; padding: 3px;">
                                <asp:Label ID="lblRightObj" runat="server" />
                            </div>
                            <div id="rightSelectedObj" style="display: block;">
                                <cms:UniSelector ID="selRightObj" runat="server" SelectionMode="SingleDropDownList"
                                    AllowEmpty="false" IsLiveSite="false" />
                            </div>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <br />
                <table width="100%">
                    <tr>
                        <td align="right">
                            <cms:CMSButton ID="btnOk" runat="server" CssClass="SubmitButton" OnClick="btnOk_Click" />
                            <cms:CMSButton ID="btnCancel" runat="server" CssClass="SubmitButton" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
                <input type="hidden" id="selectedObjId" name="selectedObjId" />
                <input type="hidden" id="currentOnLeft" name="currentOnLeft" value="true" />
                <asp:Literal ID="ltlScript" runat="server" EnableViewState="false" />
            </ContentTemplate>
        </cms:CMSUpdatePanel>
    </asp:Panel>
</asp:Panel>
