CREATE TABLE [Media_Library] (
		[LibraryID]               int NOT NULL IDENTITY(1, 1),
		[LibraryName]             nvarchar(250) NOT NULL,
		[LibraryDisplayName]      nvarchar(250) NOT NULL,
		[LibraryDescription]      nvarchar(max) NOT NULL,
		[LibraryFolder]           nvarchar(250) NOT NULL,
		[LibraryAccess]           int NOT NULL,
		[LibraryGroupID]          int NULL,
		[LibrarySiteID]           int NOT NULL,
		[LibraryGUID]             uniqueidentifier NOT NULL,
		[LibraryLastModified]     datetime NOT NULL,
		[LibraryTeaserPath]       nvarchar(450) NULL
)
ON [PRIMARY]
ALTER TABLE [Media_Library]
	ADD
	CONSTRAINT [PK_Media_Library]
	PRIMARY KEY
	NONCLUSTERED
	([LibraryID])
	WITH FILLFACTOR=80
	ON [PRIMARY]
IF OBJECT_ID(N'[Media_Library]') IS NOT NULL
	AND OBJECT_ID(N'[Community_Group]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Media_Library_LibraryGroupID_Community_Group]') IS NULL
BEGIN
		ALTER TABLE [Media_Library]
			ADD CONSTRAINT [FK_Media_Library_LibraryGroupID_Community_Group]
			FOREIGN KEY ([LibraryGroupID]) REFERENCES [Community_Group] ([GroupID])
END
IF OBJECT_ID(N'[Media_Library]') IS NOT NULL
	AND OBJECT_ID(N'[CMS_Site]') IS NOT NULL
	AND OBJECT_ID(N'[FK_Media_Library_LibrarySiteID_CMS_Site]') IS NULL
BEGIN
		ALTER TABLE [Media_Library]
			ADD CONSTRAINT [FK_Media_Library_LibrarySiteID_CMS_Site]
			FOREIGN KEY ([LibrarySiteID]) REFERENCES [CMS_Site] ([SiteID])
END
CREATE CLUSTERED INDEX [IX_Media_Library_LibraryDisplayName]
	ON [Media_Library] ([LibrarySiteID], [LibraryDisplayName])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
CREATE INDEX [IX_Media_Library_LibraryGroupID]
	ON [Media_Library] ([LibraryGroupID])
	ON [PRIMARY]
CREATE UNIQUE INDEX [IX_Media_Library_LibrarySiteID_LibraryName_LibraryGUID]
	ON [Media_Library] ([LibrarySiteID], [LibraryName], [LibraryGUID])
	WITH ( FILLFACTOR = 80)
	ON [PRIMARY]
