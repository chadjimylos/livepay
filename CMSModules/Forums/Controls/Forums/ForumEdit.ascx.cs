using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using CMS.GlobalHelper;
using CMS.LicenseProvider;
using CMS.CMSHelper;
using CMS.Forums;
using CMS.UIControls;

public partial class CMSModules_Forums_Controls_Forums_ForumEdit : CMSAdminEditControl
{
    #region "Variables"

    protected int mForumId = 0;
    protected int groupId = 0;
    protected string script = "";

    #endregion


    #region "Public properties"

    /// <summary>
    /// Gets or sets the ID of the forum to edit.
    /// </summary>
    public int ForumID
    {
        get
        {
            return this.mForumId;
        }
        set
        {
            this.mForumId = value;
        }
    }

    #endregion


    #region "Methods"

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Visible)
        {
            this.EnableViewState = false;
        }


        // Hide code name in simple mode
        if (DisplayMode == ControlDisplayModeEnum.Simple)
        {
            this.plcCodeName.Visible = false;
            this.plcUseHtml.Visible = false;
        }

        // Control initializations
        this.rfvForumDisplayName.ErrorMessage = ResHelper.GetString("Forum_General.EmptyDisplayName");
        this.rfvForumName.ErrorMessage = ResHelper.GetString("Forum_General.EmptyCodeName");

        this.lblForumOpen.Text = ResHelper.GetString("Forum_Edit.ForumOpenLabel");
        this.lblForumLocked.Text = ResHelper.GetString("Forum_Edit.ForumLockedLabel");
        this.lblForumDisplayEmails.Text = ResHelper.GetString("Forum_Edit.ForumDisplayEmailsLabel");
        this.lblForumRequireEmail.Text = ResHelper.GetString("Forum_Edit.ForumRequireEmailLabel");
        this.lblForumDisplayName.Text = ResHelper.GetString("Forum_Edit.ForumDisplayNameLabel");
        this.lblForumName.Text = ResHelper.GetString("Forum_Edit.ForumNameLabel");
        this.lblUseHTML.Text = ResHelper.GetString("Forum_Edit.UseHtml");
        this.lblBaseUrl.Text = ResHelper.GetString("Forum_Edit.lblBaseUrl");
        this.lblCaptcha.Text = ResHelper.GetString("Forum_Edit.useCaptcha");
        this.lblUnsubscriptionUrl.Text = ResHelper.GetString("Forum_Edit.lblUnsubscriptionUrl");

        this.chkInheritBaseUrl.Text = ResHelper.GetString("Forum_Edit.InheritBaseUrl");
        this.chkInheritUnsubscribeUrl.Text = ResHelper.GetString("Forum_Edit.InheritUnsupscriptionUrl");

        this.btnOk.Text = ResHelper.GetString("General.OK");        
        string currentForum = ResHelper.GetString("Forum_Edit.NewItemCaption");

        script = @"
                function LoadOption(clientId, value)
                {
                    var obj = document.getElementById(clientId);
                    if(obj!=null)
                    {
                        obj.checked = value;
                    }
                }                

                function LoadSetting(clientId, value, enabled, type)
                {
                    SetInheritance(clientId, value, type);
                    var obj = document.getElementById(clientId);
                    if (obj != null) {
                        obj.disabled = enabled;
                    }
                }

                function SetInheritance(clientIds, values, type)
                {
                    var idArray = clientIds.split(';');
                    var valueArray = values.toString().split(';');

                    for(var i = 0;i<idArray.length;i++)
                    {
                        var clientId = idArray[i];
                        var value = valueArray[i];
                        var obj = document.getElementById(clientId);
                        if (obj != null) {
                            obj.disabled = !obj.disabled;
                            if (obj.disabled)
                            {
                                if (type == 'txt') {
                                    obj.value = value;
                                } else {
                                    obj.checked = (value == 'true');
                                }
                            }
                        }
                    }
                }
                ";

        this.ltrScript.Text += ScriptHelper.GetScript(script);
        script = "";

        ForumInfo forumObj = ForumInfoProvider.GetForumInfo(this.mForumId);
        if (forumObj != null)
        {
            currentForum = forumObj.ForumName;
            groupId = forumObj.ForumGroupID;

            if (!this.IsLiveSite && !RequestHelper.IsPostBack())
            {
                ReloadData(forumObj);
            }
            else
            {
                // Handle base and unsubscription urls 
                txtBaseUrl.Enabled = !chkInheritBaseUrl.Checked;
                txtUnsubscriptionUrl.Enabled = !chkInheritUnsubscribeUrl.Checked;
                this.txtMaxAttachmentSize.Enabled = !chkInheritMaxAttachmentSize.Checked;
                this.txtIsAnswerLimit.Enabled = !chkInheritIsAnswerLimit.Checked;
                this.txtImageMaxSideSize.Enabled = !chkInheritMaxSideSize.Checked;
            }

        }

        this.ltrScript.Text += ScriptHelper.GetScript(script);

        // Show/hide URL textboxes
        plcBaseAndUnsubUrl.Visible = (DisplayMode != ControlDisplayModeEnum.Simple);        
    }


    /// <summary>
    /// Reloads the data of the editing forum.
    /// </summary>
    public override void ReloadData()
    {
        ForumInfo forumObj = ForumInfoProvider.GetForumInfo(this.mForumId);
        if (forumObj != null)
        {
            ReloadData(forumObj);
        }
    }


    /// <summary>
    /// Reloads the data of the editing forum.
    /// </summary>
    /// <param name="forumObj">Forum object.</param>
    private void ReloadData(ForumInfo forumObj)
    {
        this.chkForumOpen.Checked = forumObj.ForumOpen;
        this.chkForumLocked.Checked = forumObj.ForumIsLocked;
        this.txtForumDescription.Text = forumObj.ForumDescription;
        this.txtForumDisplayName.Text = forumObj.ForumDisplayName;
        this.txtForumName.Text = forumObj.ForumName;

        // Settings
        this.txtImageMaxSideSize.Text = forumObj.ForumImageMaxSideSize.ToString();
        this.txtIsAnswerLimit.Text = forumObj.ForumIsAnswerLimit.ToString();
        this.txtMaxAttachmentSize.Text = forumObj.ForumAttachmentMaxFileSize.ToString();
        this.txtBaseUrl.Text = forumObj.ForumBaseUrl;
        this.txtUnsubscriptionUrl.Text = forumObj.ForumUnsubscriptionUrl;

        bool inheritAuthorDelete = (forumObj.DataClass.GetValue("ForumAuthorDelete") == null);
        bool inheritAuthorEdit = (forumObj.DataClass.GetValue("ForumAuthorEdit") == null);
        bool inheritImageMaxSideSize = (forumObj.DataClass.GetValue("ForumImageMaxSideSize") == null);
        bool inheritMaxAttachmentSize = (forumObj.DataClass.GetValue("ForumAttachmentMaxFileSize") == null);
        bool inheritIsAnswerLimit = (forumObj.DataClass.GetValue("ForumIsAnswerLimit") == null);
        bool inheritType = (forumObj.DataClass.GetValue("ForumType") == null);
        bool inheritUseHTML = (forumObj.DataClass.GetValue("ForumHTMLEditor") == null);
        bool inheritDislayEmails = (forumObj.DataClass.GetValue("ForumDisplayEmails") == null);
        bool inheritRequireEmail = (forumObj.DataClass.GetValue("ForumRequireEmail") == null);
        bool inheritCaptcha = (forumObj.DataClass.GetValue("ForumUseCAPTCHA") == null);
        bool inheritBaseUrl = (forumObj.DataClass.GetValue("ForumBaseUrl") == null);
        bool inheritUnsubscriptionUrl = (forumObj.DataClass.GetValue("ForumUnsubscriptionUrl") == null);
        // Dsicussion
        bool inheritDiscussion = (forumObj.DataClass.GetValue("ForumDiscussionActions") == null);

        
        this.chkInheritUseHTML.Checked = inheritUseHTML;
        this.chkInheritDisplayEmails.Checked = inheritDislayEmails;
        this.chkInheritRequireEmail.Checked = inheritRequireEmail;
        this.chkInheritCaptcha.Checked = inheritCaptcha;
        this.chkInheritAuthorDelete.Checked = inheritAuthorDelete;
        this.chkInheritAuthorEdit.Checked = inheritAuthorEdit;
        this.txtImageMaxSideSize.Enabled = !inheritImageMaxSideSize;
        this.chkInheritMaxSideSize.Checked = inheritImageMaxSideSize;
        this.txtIsAnswerLimit.Enabled = !inheritIsAnswerLimit;
        this.chkInheritIsAnswerLimit.Checked = inheritIsAnswerLimit;
        this.chkInheritType.Checked = inheritType;
        this.txtMaxAttachmentSize.Enabled = !inheritMaxAttachmentSize;
        this.chkInheritMaxAttachmentSize.Checked = inheritMaxAttachmentSize;
        this.txtBaseUrl.Enabled = !inheritBaseUrl;
        this.chkInheritBaseUrl.Checked = inheritBaseUrl;
        this.txtUnsubscriptionUrl.Enabled = !inheritUnsubscriptionUrl;
        this.chkInheritUnsubscribeUrl.Checked = inheritUnsubscriptionUrl;
        // Discussion
        this.chkInheritDiscussion.Checked = inheritDiscussion;

        this.ltrScript.Text += ScriptHelper.GetScript(
            "LoadSetting('" + chkForumRequireEmail.ClientID + "', " + (forumObj.ForumRequireEmail ? "true" : "false") + ", " + (inheritRequireEmail ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkCaptcha.ClientID + "', " + (forumObj.ForumUseCAPTCHA ? "true" : "false") + ", " + (inheritCaptcha ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkForumDisplayEmails.ClientID + "', " + (forumObj.ForumDisplayEmails ? "true" : "false") + ", " + (inheritDislayEmails ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkUseHTML.ClientID + "', " + (forumObj.ForumHTMLEditor ? "true" : "false") + ", " + (inheritUseHTML ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkAuthorDelete.ClientID + "', " + (forumObj.ForumAuthorDelete ? "true" : "false") + ", " + (inheritAuthorDelete ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkAuthorEdit.ClientID + "', " + (forumObj.ForumAuthorEdit ? "true" : "false") + ", " + (inheritAuthorEdit ? "true" : "false") + ", 'chk');" +
            
            // Discussion
            "LoadSetting('" + radImageSimple.ClientID + "', " + (forumObj.ForumEnableImage ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'rad');" +
            "LoadSetting('" + radImageAdvanced.ClientID + "', " + (forumObj.ForumEnableAdvancedImage ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'rad');" +
            "LoadSetting('" + radImageNo.ClientID + "', " + (!(forumObj.ForumEnableAdvancedImage || forumObj.ForumEnableImage) ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'rad');" +
            "LoadSetting('" + radUrlSimple.ClientID + "', " + (forumObj.ForumEnableURL ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'rad');" +
            "LoadSetting('" + radUrlAdvanced.ClientID + "', " + (forumObj.ForumEnableAdvancedURL ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'rad');" +
            "LoadSetting('" + radUrlNo.ClientID + "', " + (!(forumObj.ForumEnableAdvancedURL || forumObj.ForumEnableURL) ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'rad');" +
            "LoadSetting('" + chkEnableQuote.ClientID + "', " + (forumObj.ForumEnableQuote ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkEnableBold.ClientID + "', " + (forumObj.ForumEnableFontBold ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkEnableItalic.ClientID + "', " + (forumObj.ForumEnableFontItalics ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkEnableStrike.ClientID + "', " + (forumObj.ForumEnableFontStrike ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkEnableUnderline.ClientID + "', " + (forumObj.ForumEnableFontUnderline ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkEnableCode.ClientID + "', " + (forumObj.ForumEnableCodeSnippet ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + chkEnableColor.ClientID + "', " + (forumObj.ForumEnableFontColor ? "true" : "false") + ", " + (inheritDiscussion ? "true" : "false") + ", 'chk');" +
            "LoadSetting('" + radTypeAnswer.ClientID + "', " + (forumObj.ForumType == 2 ? "true" : "false") + ", " + (inheritType ? "true" : "false") + ", 'rad');" +
            "LoadSetting('" + radTypeDiscussion.ClientID + "', " + (forumObj.ForumType == 1 ? "true" : "false") + ", " + (inheritType ? "true" : "false") + ", 'rad');" +
            "LoadSetting('" + radTypeChoose.ClientID + "', " + (forumObj.ForumType == 0 ? "true" : "false") + ", " + (inheritType ? "true" : "false") + ", 'rad');");

        ForumGroupInfo fgi = ForumGroupInfoProvider.GetForumGroupInfo(forumObj.ForumGroupID);

        this.chkInheritUnsubscribeUrl.Attributes.Add("onclick", "SetInheritance('" + txtUnsubscriptionUrl.ClientID + "', '" + fgi.GroupUnsubscriptionUrl.Replace("'","\\\'") + "', 'txt');");
        this.chkInheritBaseUrl.Attributes.Add("onclick", "SetInheritance('" + txtBaseUrl.ClientID + "', '" + fgi.GroupBaseUrl.Replace("'", "\\\'") + "', 'txt');");

        // Settings inheritance
        this.chkInheritUseHTML.Attributes.Add("onclick", "SetInheritance('" + chkUseHTML.ClientID + "', '" + fgi.GroupHTMLEditor.ToString().ToLower() + "', 'chk');");
        this.chkInheritCaptcha.Attributes.Add("onclick", "SetInheritance('" + chkCaptcha.ClientID + "', '" + fgi.GroupUseCAPTCHA.ToString().ToLower() + "', 'chk');");
        this.chkInheritRequireEmail.Attributes.Add("onclick", "SetInheritance('" + chkForumRequireEmail.ClientID + "', '" + fgi.GroupRequireEmail.ToString().ToLower() + "', 'chk');");
        this.chkInheritDisplayEmails.Attributes.Add("onclick", "SetInheritance('" + chkForumDisplayEmails.ClientID + "', '" + fgi.GroupDisplayEmails.ToString().ToLower() + "', 'chk');");
        this.chkInheritAuthorDelete.Attributes.Add("onclick", "SetInheritance('" + chkAuthorDelete.ClientID + "', '" + fgi.GroupAuthorDelete.ToString().ToLower() + "', 'chk');");
        this.chkInheritAuthorEdit.Attributes.Add("onclick", "SetInheritance('" + chkAuthorEdit.ClientID + "', '" + fgi.GroupAuthorEdit.ToString().ToLower() + "', 'chk');");
        this.chkInheritIsAnswerLimit.Attributes.Add("onclick", "SetInheritance('" + txtIsAnswerLimit.ClientID + "', '" + fgi.GroupIsAnswerLimit.ToString().Replace("'", "\\\'") + "', 'txt');");
        this.chkInheritMaxSideSize.Attributes.Add("onclick", "SetInheritance('" + txtImageMaxSideSize.ClientID + "', '" + fgi.GroupImageMaxSideSize.ToString().Replace("'", "\\\'") + "', 'txt');");
        this.chkInheritType.Attributes.Add("onclick", "SetInheritance('" + radTypeAnswer.ClientID + "', '" + (fgi.GroupType == 2 ? "true" : "false") + "', 'rad');" +
            "SetInheritance('" + radTypeDiscussion.ClientID + "', '" + (fgi.GroupType == 1 ? "true" : "false") + "', 'rad');" +
            "SetInheritance('" + radTypeChoose.ClientID + "', '" + (fgi.GroupType == 0 ? "true" : "false") + "', 'rad');");
        this.chkInheritMaxAttachmentSize.Attributes.Add("onclick", "SetInheritance('" + txtMaxAttachmentSize.ClientID + "','" + fgi.GroupAttachmentMaxFileSize.ToString().Replace("'", "\\\'") + "', 'txt');");
        // Discussion
        string chkList = "'" + radImageSimple.ClientID + ";" + chkEnableBold.ClientID + ";" + chkEnableCode.ClientID + ";" +
                               chkEnableColor.ClientID + ";" + radUrlSimple.ClientID + ";" + chkEnableItalic.ClientID + ";" +
                               radImageAdvanced.ClientID + ";" + radUrlAdvanced.ClientID + ";" + chkEnableQuote.ClientID + ";" +
                               chkEnableStrike.ClientID + ";" + chkEnableUnderline.ClientID + ";" + radImageNo.ClientID + ";" + radUrlNo.ClientID + "'";
        string chkListValues = "'" + fgi.GroupEnableImage.ToString() + ";" + fgi.GroupEnableFontBold.ToString() + ";" + fgi.GroupEnableCodeSnippet.ToString() + ";" +
                               fgi.GroupEnableFontColor.ToString() + ";" + fgi.GroupEnableURL.ToString() + ";" + fgi.GroupEnableFontItalics.ToString() + ";" +
                               fgi.GroupEnableAdvancedImage.ToString() + ";" + fgi.GroupEnableAdvancedURL.ToString() + ";" + fgi.GroupEnableQuote.ToString() + ";" + 
                               fgi.GroupEnableFontStrike.ToString() + ";" + fgi.GroupEnableFontUnderline.ToString() + ";" +
                               !(fgi.GroupEnableAdvancedImage || fgi.GroupEnableImage) + ";" + !(fgi.GroupEnableAdvancedURL || fgi.GroupEnableURL) + "'";
        this.chkInheritDiscussion.Attributes.Add("onclick", "SetInheritance(" + chkList + ", " + chkListValues.ToLower() + ", 'chk');");
    }


    /// <summary>
    /// Sets data to database.
    /// </summary>
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (!CheckPermissions("cms.forums", CMSAdminControl.PERMISSION_MODIFY))
        {
            return;
        }

        string errorMessage = new Validator().NotEmpty(txtForumDisplayName.Text, ResHelper.GetString("Forum_General.EmptyDisplayName")).Result;

        if ((errorMessage == String.Empty)&&(this.DisplayMode != ControlDisplayModeEnum.Simple))
        {
            errorMessage = new Validator().NotEmpty(txtForumName.Text, ResHelper.GetString("Forum_General.EmptyCodeName")).IsCodeName(txtForumName.Text.Trim(), ResHelper.GetString("general.errorcodenameinidentificatorformat")).Result;
        }

        if (errorMessage != "")
        {
            lblError.Visible = true;
            lblError.Text = errorMessage;
            return;
        }

        if (CMSContext.CurrentSite != null)
        {
            ForumInfo forumObj = ForumInfoProvider.GetForumInfo(txtForumName.Text.Trim(), CMSContext.CurrentSite.SiteID);

            if ((forumObj == null) || (forumObj.ForumID == this.mForumId))
            {
                if (forumObj == null)
                {
                    forumObj = ForumInfoProvider.GetForumInfo(this.mForumId);
                }

                if (forumObj != null)
                {
                    forumObj.ForumIsLocked = chkForumLocked.Checked;
                    forumObj.ForumOpen = chkForumOpen.Checked;
                    forumObj.ForumDisplayEmails = chkForumDisplayEmails.Checked;
                    forumObj.ForumDescription = txtForumDescription.Text.Trim();
                    forumObj.ForumRequireEmail = chkForumRequireEmail.Checked;
                    forumObj.ForumDisplayName = txtForumDisplayName.Text.Trim();
                    forumObj.ForumUseCAPTCHA = chkCaptcha.Checked;
                    
                    if (DisplayMode != ControlDisplayModeEnum.Simple)
                    {
                        forumObj.ForumHTMLEditor = chkUseHTML.Checked;
                        forumObj.ForumName = txtForumName.Text.Trim();

                        if (chkInheritBaseUrl.Checked)
                        {
                            forumObj.DataClass.SetValue("ForumBaseUrl", null);
                        }
                        else
                        {
                            forumObj.ForumBaseUrl = txtBaseUrl.Text;
                        }

                        if (chkInheritUnsubscribeUrl.Checked)
                        {
                            forumObj.DataClass.SetValue("ForumUnsubscriptionUrl", null);
                        }
                        else
                        {
                            forumObj.ForumUnsubscriptionUrl = txtUnsubscriptionUrl.Text.Trim();
                        }
                        
                    }

                    if (chkInheritRequireEmail.Checked)
                    {
                        forumObj.DataClass.SetValue("ForumRequireEmail", null);
                    }
                    else
                    {
                        forumObj.ForumRequireEmail = chkForumRequireEmail.Checked;
                    }

                    if (chkInheritDisplayEmails.Checked)
                    {
                        forumObj.DataClass.SetValue("ForumDisplayEmails", null);
                    }
                    else
                    {
                        forumObj.ForumDisplayEmails = chkForumDisplayEmails.Checked;
                    }

                    if (chkInheritUseHTML.Checked)
                    {
                        forumObj.DataClass.SetValue("ForumHTMLEditor", null);
                    }
                    else
                    {
                        forumObj.ForumHTMLEditor = chkUseHTML.Checked;
                    }

                    if (chkInheritCaptcha.Checked)
                    {
                        forumObj.DataClass.SetValue("ForumUseCAPTCHA", null);
                    }
                    else
                    {
                        forumObj.ForumUseCAPTCHA = chkCaptcha.Checked;
                    }

                    if (chkInheritAuthorDelete.Checked)
                    {
                        forumObj.DataClass.SetValue("ForumAuthorDelete", null);
                    }
                    else
                    {
                        forumObj.ForumAuthorDelete = chkAuthorDelete.Checked;
                    }

                    if (chkInheritAuthorEdit.Checked)
                    {
                        forumObj.DataClass.SetValue("ForumAuthorEdit", null);
                    }
                    else
                    {
                        forumObj.ForumAuthorEdit = chkAuthorEdit.Checked;
                    }

                    if (chkInheritMaxSideSize.Checked)
                    {
                        forumObj.DataClass.SetValue("ForumImageMaxSideSize", null);
                    }
                    else
                    {
                        forumObj.ForumImageMaxSideSize = ValidationHelper.GetInteger(txtImageMaxSideSize.Text, 400);
                    }

                    if (chkInheritIsAnswerLimit.Checked)
                    {
                        forumObj.DataClass.SetValue("ForumIsAnswerLimit", null);
                    }
                    else
                    {
                        forumObj.ForumIsAnswerLimit = ValidationHelper.GetInteger(txtIsAnswerLimit.Text, 5);
                    }

                    if (chkInheritType.Checked)
                    {
                        forumObj.DataClass.SetValue("ForumType", null);
                    }
                    else
                    {
                        forumObj.ForumType = (radTypeChoose.Checked ? 0 : (radTypeDiscussion.Checked ? 1 : 2));
                    }

                    if (chkInheritBaseUrl.Checked)
                    {
                        forumObj.ForumBaseUrl = null;
                    }

                    if (chkInheritUnsubscribeUrl.Checked)
                    {
                        forumObj.ForumUnsubscriptionUrl = null;
                    }

                    if (chkInheritMaxAttachmentSize.Checked)
                    {
                        forumObj.ForumAttachmentMaxFileSize = -1;
                    }
                    else
                    {
                        forumObj.ForumAttachmentMaxFileSize = ValidationHelper.GetInteger(this.txtMaxAttachmentSize.Text, 0);
                    }

                    #region "Discussion"

                    if (chkInheritDiscussion.Checked)
                    {
                        forumObj.DataClass.SetValue("ForumDiscussionActions", null);
                    }
                    else
                    {
                        forumObj.ForumEnableQuote = chkEnableQuote.Checked;
                        forumObj.ForumEnableFontBold = chkEnableBold.Checked;
                        forumObj.ForumEnableFontItalics = chkEnableItalic.Checked;
                        forumObj.ForumEnableFontUnderline = chkEnableUnderline.Checked;
                        forumObj.ForumEnableFontStrike = chkEnableStrike.Checked;
                        forumObj.ForumEnableFontColor = chkEnableColor.Checked;
                        forumObj.ForumEnableCodeSnippet = chkEnableCode.Checked;
                        forumObj.ForumEnableAdvancedImage = radImageAdvanced.Checked;
                        forumObj.ForumEnableAdvancedURL = radUrlAdvanced.Checked;
                        forumObj.ForumEnableImage = radImageSimple.Checked;
                        forumObj.ForumEnableURL = radUrlSimple.Checked;
                    }

                    #endregion


                    ForumInfoProvider.SetForumInfo(forumObj);

                    ReloadData();

                    this.RaiseOnSaved();

                    lblInfo.Visible = true;
                    lblInfo.Text = ResHelper.GetString("General.ChangesSaved");
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = ResHelper.GetString("Forum_Edit.ForumDoesNotExist");
                }
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = ResHelper.GetString("Forum_Edit.ForumAlreadyExists");
            }
        }
    }

    #endregion
}
