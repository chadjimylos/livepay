<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentType_Edit_Query_List.aspx.cs"
    Inherits="CMSSiteManager_Development_DocumentTypes_DocumentType_Edit_Query_List"
    Theme="Default" MasterPageFile="~/CMSMasterPages/UI/SimplePage.master" Title="Document Type Edit - Query List" %>

<%@ Register Src="~/CMSModules/AdminControls/Controls/Class/ClassQueries.ascx" TagName="ClassQueries" TagPrefix="cms" %>
    
<asp:Content ID="cntBody" runat="server" ContentPlaceHolderID="plcContent">
    <cms:ClassQueries ID="classEditQuery" runat="server" />
</asp:Content>
